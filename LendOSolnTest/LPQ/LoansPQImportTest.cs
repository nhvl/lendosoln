﻿namespace LendOSolnTest.LPQ
{
    using System.Linq;
    using Common;
    using DataAccess;
    using Fakes;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.Conversions.LoansPQ.Importer;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    /// <summary>
    /// Tests behaviors of <see cref="CLFImporter"/>.
    /// Mostly checks to make sure that weird condition and validity checks aren't changed.
    /// </summary>
    [TestFixture]
    public class LoansPQImportTest
    {
        /// <summary>
        /// Performs test setup.
        /// </summary>
        [TestFixtureSetUp]
        public void Setup() => LoginTools.LoginAs(TestAccountType.LoTest001);

        /// <summary>
        /// Performs test tear down.
        /// </summary>
        [TestFixtureTearDown]
        public void Teardown() => FakePageDataUtilities.Instance.Reset();

        /// <summary>
        /// Tests importing the employments provided.
        /// </summary>
        [Test]
        public void TestEmployments()
        {
            var loan = FakePageData.Create(bypassFieldSecurityCheck: true);
            loan.InitLoad();
            loan.SetFormatTarget(FormatTarget.MismoClosing);

            var app = loan.GetAppData(0);
            app.aBEmpCollection.ClearAll();
            app.aCEmpCollection.ClearAll();

            var importer = new CLFImporter(loan, TestUtilities.ReadFile("LPQ/TestEmployments.xml"));
            importer.PopulateLoan();

            /* The test file's borrower has;
             *  3 regular current employment records.
             *  The first of these regular current employment records should populate as the first employment (CurrEmp1). LPQ puts the primary employment as the first item in the collection.
             *  2 previous employments.
             * The coborrower has:
             *  1 current employment that should be assigned primary employment due to being the only current employment(CoCurrEmp1)
             */

            var primaryEmployment = app.aBEmpCollection.GetPrimaryEmp(forceCreate: false);
            Assert.AreEqual(app.aBEmpCollection.CountRegular, 4, "aBEmpCollection was not populated with expected employment records.");
            Assert.AreEqual(primaryEmployment?.EmplrNm, "CurrEmp1", "aBEmpCollection primary employment was not populated correctly.");

            primaryEmployment = app.aCEmpCollection.GetPrimaryEmp(forceCreate: false);
            Assert.AreEqual(app.aCEmpCollection.CountRegular, 0, "aCEmpCollection was not populated with expected employment records.");
            Assert.AreEqual(primaryEmployment?.EmplrNm, "CoCurrEmp1", "aCEmpCollection primary employment was not populated correctly.");
        }

        /// <summary>
        /// Tests importing other income.
        /// </summary>
        [Test]
        public void TestOtherIncomes()
        {
            var loan = FakePageData.Create(bypassFieldSecurityCheck: true);
            loan.InitLoad();
            loan.SetFormatTarget(FormatTarget.MismoClosing);

            var app = loan.GetAppData(0);
            app.aOtherIncomeList = new System.Collections.Generic.List<OtherIncome>();
            var initialIncomeCount = app.aOtherIncomeList.Count;

            /* The test file includes:
             *   3 valid other incomes
             *   1 without an income value, and thus should not be added.
             */
            var testFile = TestUtilities.ReadFile("LPQ/TestOtherIncome.xml");

            var importer = new CLFImporter(loan, testFile);
            importer.PopulateLoan();

            // The importer should've used the empty slots in the other income list.
            Assert.AreEqual(initialIncomeCount, app.aOtherIncomeList.Count, "aOtherIncomeList did not use empty slots");
        }

        [TestCase(false)]
        [TestCase(true)]
        public void MonthlyIncome_ForIncomeCollection_ImportsExpectedValues(bool useIncomeCollection)
        {
            using (var test = LoanForIntegrationTest.Create(TestFileType.TestFile))
            {
                var testFile = TestUtilities.ReadFile("LPQ/TestIncome.xml");
                var loan = test.CreateNewPageDataWithBypass();
                loan.InitSave();
                if (useIncomeCollection)
                {
                    loan.MigrateToUseLqbCollections();
                    loan.MigrateToIncomeSourceCollection();
                }

                Assert.AreEqual(useIncomeCollection, loan.sIsIncomeCollectionEnabled);
                loan.SetFormatTarget(FormatTarget.MismoClosing);

                var importer = new CLFImporter(loan, testFile);
                importer.PopulateLoan();

                var app = loan.GetAppData(0);
                Assert.AreEqual(3000, app.aBBaseI);
                Assert.AreEqual(400, app.aBOvertimeI);
                Assert.AreEqual(500, app.aBBonusesI);
                Assert.AreEqual(600, app.aBCommisionI);
                Assert.AreEqual(700, app.aBDividendI);
                Assert.AreEqual(3050, app.aCBaseI);
                Assert.AreEqual(450, app.aCOvertimeI);
                Assert.AreEqual(550, app.aCBonusesI);
                Assert.AreEqual(650, app.aCCommisionI);
                Assert.AreEqual(750, app.aCDividendI);
                Assert.AreEqual(6, app.aOtherIncomeList.Count);
                Assert.AreEqual(3, app.aOtherIncomeList.Count(i => !i.IsForCoBorrower));
                Assert.AreEqual(3, app.aOtherIncomeList.Count(i => i.IsForCoBorrower));
                CollectionAssert.AreEquivalent(new[] { 1, 2, 3 }, app.aOtherIncomeList.Where(i => !i.IsForCoBorrower).Select(i => i.Amount));
                CollectionAssert.AreEquivalent(new[] { "Test1", "Test2", "Test3" }, app.aOtherIncomeList.Where(i => !i.IsForCoBorrower).Select(i => i.Desc));
                CollectionAssert.AreEquivalent(new[] { 51, 52, 53 }, app.aOtherIncomeList.Where(i => i.IsForCoBorrower).Select(i => i.Amount));
                CollectionAssert.AreEquivalent(new[] { "Test51", "Test52", "Test53" }, app.aOtherIncomeList.Where(i => i.IsForCoBorrower).Select(i => i.Desc));
            }
        }

        [Test]
        public void MonthlyIncome_IncomeCollectionWithSubjectPropertyNetRent_SetsExpectedFields()
        {
            using (var test = LoanForIntegrationTest.Create(TestFileType.TestFile))
            {
                var testFile = TestUtilities.ReadFile("LPQ/TestIncomeSubjectPropertyNetRent.xml");
                var loan = test.CreateNewPageDataWithBypass();
                loan.InitSave();
                loan.MigrateToUseLqbCollections();
                loan.MigrateToIncomeSourceCollection();
                loan.SetFormatTarget(FormatTarget.MismoClosing);
                Assert.IsTrue(loan.sIsIncomeCollectionEnabled);

                var importer = new CLFImporter(loan, testFile);
                importer.PopulateLoan();

                Assert.IsTrue(loan.sPrimResid);
                Assert.IsTrue(loan.sSpCountRentalIForPrimaryResidToo);
                Assert.IsTrue(loan.sIsPrimAppHaveEntireSpRentalIAsI);
                Assert.AreEqual(10, loan.sSpGrossRent);
            }
        }

        /// <summary>
        /// Tests importing contacts.
        /// </summary>
        [Test]
        public void TestSystemPartyImport()
        {
            var loan = FakePageData.Create(bypassFieldSecurityCheck: true);
            loan.InitLoad();
            loan.SetFormatTarget(FormatTarget.MismoClosing);

            loan.sAgentCollection.sAgentDataSetClear();

            var testFile = TestUtilities.ReadFile("LPQ/TestContacts.xml");
            var importer = new CLFImporter(loan, testFile);
            importer.PopulateLoan();
            var contactsCount = loan.sAgentCollection.GetAgentRecordCount();

            // Import the same set into the loan again. Should result in no change to the record count because of the dedupe logic.
            importer = new CLFImporter(loan, testFile);
            importer.PopulateLoan();
            Assert.AreEqual(contactsCount, loan.sAgentCollection.GetAgentRecordCount(), "System parties not de-duped correctly.");

            // Change the processor and import again. Should result in a new record being created.
            var processor = loan.GetAgentOfRole(E_AgentRoleT.Processor, E_ReturnOptionIfNotExist.CreateNew);
            processor.AgentName = "NewProcessorName";

            var processorCount = loan.GetAgentsOfRole(E_AgentRoleT.Processor, E_ReturnOptionIfNotExist.CreateNew).Count;

            importer = new CLFImporter(loan, testFile);
            importer.PopulateLoan();

            Assert.AreEqual(processorCount + 1, loan.GetAgentsOfRole(E_AgentRoleT.Processor, E_ReturnOptionIfNotExist.CreateNew).Count);
        }

        /// <summary>
        /// Tests importing HMDA data.
        /// </summary>
        [Test]
        public void TestHmdaImport()
        {
            // I'll define the apps here myself but the normal importer would figure out the apps to create based on the XML.
            var loan = FakePageData.Create(bypassFieldSecurityCheck: true, appCount: 2);
            loan.InitLoad();
            loan.SetFormatTarget(FormatTarget.MismoClosing);

            var importer = new CLFImporter(loan, TestUtilities.ReadFile("LPQ/TestHmda.xml"));
            importer.PopulateLoan();

            /* Test file has 2 apps. 1st has a coborrower.
             * applicant_0 fields should map to app 1 borrower.
             * applicant_1 fields should map to app 1 coborrower.
             * applicant_2 fields should map to app 2 borrower
             * The other description fields will be used to figure out if the correct fields went tot he correct borrower.
             */

            var app1 = loan.GetAppData(0);
            Assert.AreEqual(app1.aBOtherAsianDescription, "APPLICANT1", "App1 aBOtherAsianDescription");
            Assert.True(app1.aBHispanicT == E_aHispanicT.BothHispanicAndNotHispanic, "App1 aBHispanicT");
            Assert.True(app1.aBGender == E_GenderT.MaleAndFemale, "App1 aBGender");

            Assert.AreEqual(app1.aCOtherAsianDescription, "COAPPLICANT1", "App1 aCOtherAsianDescription");
            Assert.True(app1.aCGender == E_GenderT.Male, "App1 aCGender");

            var app2 = loan.GetAppData(1);
            Assert.AreEqual(app2.aBOtherAsianDescription, "APPLICANT2", "App2 aBOtherAsianDescription");
            Assert.IsEmpty(app2.aCOtherAsianDescription, "App2 aCOtherAsianDescription");
            Assert.True(app2.aBIsWhite, "App2 aBIsWhite");
        }
    }
}
