﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using LendOSolnTest.Common;
using LendersOffice.Security;
using DataAccess;
using LendersOffice.DU;
using System.IO;

namespace LendOSolnTest.DU
{
    [TestFixture]
    public class XisCasefileExportResponseImportTest
    {
        private Guid m_loanId = Guid.Empty;

        //This can't be a [SetUp] method, because we need to log in first (and we won't know who to log in as until we're in a test method)
        public void SetupLoan()
        {
            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;

            CLoanFileCreator creator = CLoanFileCreator.GetCreator(principal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);

            m_loanId = creator.CreateBlankLoanFile();
        }
        [TearDown]
        public void TearDown()
        {
            //Have to make sure we're the broker user, since the pml user won't have delete loan permissions.
            LoginTools.LoginAs(TestAccountType.LoTest001);
            if (m_loanId != Guid.Empty)
            {
                Tools.DeclareLoanFileInvalid(BrokerUserPrincipal.CurrentPrincipal, m_loanId, false, false);
            }
        }

        private CPageData CreateDataObject(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(XisCasefileExportResponseImportTest));
        }

        [Test]
        public void ImportPublicRecordTest()
        {
            // 6/5/2009 dd - This test was create in response to OPM 31159.
            LoginTools.LoginAs(TestAccountType.LoTest001);
            DoImport();
        }

        [Test]
        public void ImportPublicRecordAsPmlUser()
        {
            //This test was created in response to OPM 80279.
            LoginTools.LoginAs(TestAccountType.Pml_Supervisor);
            DoImport();

        }

        public void DoImport()
        {
            SetupLoan();
            string rawData = string.Empty;
            using (StreamReader reader = new StreamReader(TestDataRootFolder.Location + "/DU/XIS_CaseFileExportResponse_OPM_31159.txt"))
            {
                rawData = reader.ReadToEnd();
            }
            FnmaXisCasefileExportResponse response = new FnmaXisCasefileExportResponse(rawData);

            response.ImportToLoanFile(m_loanId, PrincipalFactory.CurrentPrincipal, true, true, true);

            CPageData dataLoan = CreateDataObject(m_loanId);
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(0);

            Assert.AreEqual(1, dataApp.aPublicRecordCollection.CountRegular, "Public Record COunt");

            // 6/17/2009 dd - Re-import the data to loan file.
            response.ImportToLoanFile(m_loanId, PrincipalFactory.CurrentPrincipal, true, true, true);
            dataLoan = CreateDataObject(m_loanId);
            dataLoan.InitLoad();

            dataApp = dataLoan.GetAppData(0);
            // 6/17/2009 dd - There should still be only 1 public record after re-import.
            Assert.AreEqual(1, dataApp.aPublicRecordCollection.CountRegular, "Public Record COunt");
        }

    }
}
