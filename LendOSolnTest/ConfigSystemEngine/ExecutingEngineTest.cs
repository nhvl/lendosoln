﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

using CommonProjectLib.ConfigSystem;
using ConfigSystem.Engine;
using ConfigSystem;
using DataAccess;

namespace LendOSolnTest.ConfigSystemEngine
{
    [TestFixture]
    public class ExecutingEngineTest
    {
        private class FakeOperation : ISystemOperationIdentifier
        {
            public FakeOperation(string id)
            {
                this.Id = id;
            }

            public string Id { get; }
        }

        private class QuickCondition
        {
            public string Name { get; private set; }
            public Guid Id { get; set; }

            public IEnumerable<QuickParameter> ParameterList
            {
                get { return m_parameterList; }
            }
            private List<QuickParameter> m_parameterList = new List<QuickParameter>();

            public void AddCustomVar(string n, E_FunctionT f, bool v)
            {
                m_parameterList.Add(new QuickParameter(E_ParameterValueType.CustomVariable, n, f, v.ToString()));
            }
            public void AddSystemCustomVar(string n, E_FunctionT f, bool v)
            {
                m_parameterList.Add(new QuickParameter(E_ParameterValueType.CustomVariable, n, f, CustomVariable.E_Scope.System, v.ToString()));
            }

            public void AddCompareVar(E_ParameterValueType t, string lhs, E_FunctionT f, string rhs)
            {
                m_parameterList.Add(new QuickParameter(t, lhs, f, rhs));
            }
            public void Add(E_ParameterValueType t, string n, E_FunctionT f, params string[] args)
            {
                m_parameterList.Add(new QuickParameter(t, n, f, args));
            }
            public void Add(string n, E_FunctionT f, params string[] args)
            {
                Add(E_ParameterValueType.String, n, f, args);
            }
            public void Add(string n, E_FunctionT f, params int[] args)
            {
                string[] _args = new string[args.Length];
                for (int i = 0; i < args.Length; i++)
                {
                    _args[i] = args[i].ToString();
                }
                Add(E_ParameterValueType.Int, n, f, _args);
            }
            public void Add(string n, E_FunctionT f, params float[] args)
            {
                string[] _args = new string[args.Length];
                for (int i = 0; i < args.Length; i++)
                {
                    _args[i] = args[i].ToString();
                }
                Add(E_ParameterValueType.Float, n, f, _args);
            }
            public void Add(string n, E_FunctionT f, params bool[] args)
            {
                string[] _args = new string[args.Length];
                for (int i = 0; i < args.Length; i++)
                {
                    _args[i] = args[i].ToString();
                }
                Add(E_ParameterValueType.Bool, n, f, _args);
            }
            public void Add(string n, E_FunctionT f, params DateTime[] args)
            {
                string[] _args = new string[args.Length];
                for (int i = 0; i < args.Length; i++)
                {
                    _args[i] = args[i].ToString();
                }
                Add(E_ParameterValueType.DateTime, n, f, _args);
            }
            public QuickCondition(string name)
            {
                Name = name;
            }
        }

        private class QuickParameter
        {
            public string Name { get; set; }
            public E_FunctionT Function { get; private set; }
            public E_ParameterValueType ValueType { get; private set; }
            public IEnumerable<string> Value { get; private set; }
            public CustomVariable.E_Scope Scope { get; private set; }
            public QuickParameter(E_ParameterValueType t, string n, E_FunctionT f, CustomVariable.E_Scope scope, params string[] args)
            {
                Name = n;
                Function = f;
                ValueType = t;
                List<string> list = new List<string>();
                foreach (string o in args)
                {
                    list.Add(o);
                }

                Value = list;
                Scope = scope;
            }
            public QuickParameter(E_ParameterValueType t, string n, E_FunctionT f, params string[] args)
                : this(t, n, f, CustomVariable.E_Scope.Lender, args)
            {
            }
        }

        private ExecutingEngine Compile(List<QuickCondition> conditionList)
        {
            return Compile(conditionList, null, null);
        }

        private ExecutingEngine Compile(List<QuickCondition> conditionList, List<QuickCondition> restraintList , List<QuickCondition> customVariableList)
        {
            SystemConfig systemConfig = new SystemConfig();

            if (null != conditionList)
            {
                foreach (var quickCondition in conditionList)
                {
                    Condition condition = new Condition();
                    condition.SysOpSet.Add(new SystemOperation(quickCondition.Name));
                    quickCondition.Id = condition.Id;
                    foreach (var quickParameter in quickCondition.ParameterList)
                    {
                        Parameter parameter = new Parameter(quickParameter.Name, quickParameter.Function, quickParameter.ValueType, false, false);
                        parameter.Scope = quickParameter.Scope;
                        foreach (var o in quickParameter.Value)
                        {
                            parameter.AddValue(o, o);
                        }
                        condition.ParameterSet.AddParameter(parameter);
                    }
                    systemConfig.Conditions.AddCondition(condition);
                }
            }

            if (null != restraintList)
            {
                foreach (var quickCondition in restraintList)
                {
                    Restraint restraint = new Restraint();
                    restraint.SysOpSet.Add(new SystemOperation(quickCondition.Name));
                    quickCondition.Id = restraint.Id;
                    foreach (var quickParameter in quickCondition.ParameterList)
                    {
                        Parameter parameter = new Parameter(quickParameter.Name, quickParameter.Function, quickParameter.ValueType, false, false);
                        parameter.Scope = quickParameter.Scope;
                        foreach (var o in quickParameter.Value)
                        {
                            parameter.AddValue(o, o);
                        }

                        restraint.ParameterSet.AddParameter(parameter);
                    }
                    systemConfig.Restraints.AddRestraint(restraint);
                }
            }

            if (null != customVariableList)
            {
                foreach (var quickCustomVariable in customVariableList)
                {
                    CustomVariable customVariable = new CustomVariable(quickCustomVariable.Name, CustomVariable.E_Scope.Lender);
                    foreach (var quickParameter in quickCustomVariable.ParameterList)
                    {
                        Parameter parameter = new Parameter(quickParameter.Name, quickParameter.Function, quickParameter.ValueType, false, false);
                        parameter.Scope = quickParameter.Scope;
                        foreach (var o in quickParameter.Value)
                        {
                            parameter.AddValue(o, o);
                        }
                        customVariable.ParameterSet.AddParameter(parameter);
                    }
                    systemConfig.CustomVariables.Add(customVariable);
                }
            }
            byte[] content = ExecutingEngineCompiler.Compile(systemConfig, DateTime.Now);

            
            var engine = ExecutingEngine.LoadFromContent(content);

            Tools.LogInfo(engine.DisplayDebugCompiledCode());
            return engine;
        }

        [Test]
        public void TestOpCode_IsAnyValue()
        {
            #region Condition Rules
            string operation = "ALlow_0";
            List<QuickCondition> conditionList = new List<QuickCondition>();

            QuickCondition condition = new QuickCondition(operation);
            condition.Add(E_ParameterValueType.Int, "int_0", E_FunctionT.IsAnyValue, bool.TrueString);
            condition.Add(E_ParameterValueType.Int, "int_1", E_FunctionT.IsAnyValue, bool.FalseString);
            condition.Add(E_ParameterValueType.Float, "float_0", E_FunctionT.IsAnyValue, bool.TrueString);
            condition.Add(E_ParameterValueType.Float, "float_1", E_FunctionT.IsAnyValue, bool.FalseString);

            condition.Add(E_ParameterValueType.DateTime, "datetime_0", E_FunctionT.IsAnyValue, bool.TrueString);
            condition.Add(E_ParameterValueType.DateTime, "datetime_1", E_FunctionT.IsAnyValue, bool.FalseString);

            conditionList.Add(condition);
            #endregion

            ExecutingEngine engine = Compile(conditionList);

            Dictionary<string, object> args = null;
            args = new Dictionary<string, object>()
            {
                {"int_0", new int[] { 0}},
                {"float_0", new float[] {2}},
                {"datetime_0", new DateTime[] { DateTime.Now}}
            };
            AssertExecuting(true, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"float_0", new float[] {2}},
                {"datetime_0", new DateTime[] { DateTime.Now}}
            };
            AssertExecuting(false, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"int_1", new int[] {0}},
                {"float_0", new float[] {2}},
                {"datetime_0", new DateTime[] { DateTime.Now}}
            };
            AssertExecuting(false, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"int_0", new int[] {0}},
                {"datetime_0", new DateTime[] { DateTime.Now}}

            };
            AssertExecuting(false, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"int_0", new int[] { 0}},
                {"float_0", new float[] {2}},
                {"datetime_1", new DateTime[] { DateTime.Now}}
            };
            AssertExecuting(false, operation, engine, args);

        }
        [Test]
        public void TestOpCode_IsNonzeroValue()
        {
            #region Condition Rules
            string operation = "ALlow_0";
            List<QuickCondition> conditionList = new List<QuickCondition>();

            QuickCondition condition = new QuickCondition(operation);
            condition.Add(E_ParameterValueType.Int, "int_0", E_FunctionT.IsNonzeroValue, bool.TrueString);
            condition.Add(E_ParameterValueType.Int, "int_1", E_FunctionT.IsNonzeroValue, bool.FalseString);
            condition.Add(E_ParameterValueType.Float, "float_0", E_FunctionT.IsNonzeroValue, bool.TrueString);
            condition.Add(E_ParameterValueType.Float, "float_1", E_FunctionT.IsNonzeroValue, bool.FalseString);

            conditionList.Add(condition);
            #endregion

            ExecutingEngine engine = Compile(conditionList);

            Dictionary<string, object> args = null;

            args = new Dictionary<string, object>()
            {
                {"int_0", new int[] { 1}},
                {"int_1", new int[] { 0}},
                {"float_0", new float[] { 2.5F}},
                {"float_1", new float[] { 0}}
            };
            AssertExecuting(true, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"int_0", new int[] { 1}},
                {"float_0", new float[] { 2.5F}},
            };
            AssertExecuting(true, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"int_0", new int[] { 0}}, // False condition
                {"int_1", new int[] { 0}},
                {"float_0", new float[] { 2.5F}},
                {"float_1", new float[] { 0}}
            };
            AssertExecuting(false, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"int_0", new int[] { 1}},
                {"int_1", new int[] { 1}}, // False condition
                {"float_0", new float[] { 2.5F}},
                {"float_1", new float[] { 0}}
            };
            AssertExecuting(false, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"int_0", new int[] { 1}},
                {"int_1", new int[] { 0}},
                {"float_0", new float[] { 0}}, // False condition
                {"float_1", new float[] { 0}}
            };
            AssertExecuting(false, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"int_0", new int[] { 1}},
                {"int_1", new int[] { 0}},
                {"float_0", new float[] { 2.5F}},
                {"float_1", new float[] { 2.5F}} // False condition
            };
            AssertExecuting(false, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"int_1", new int[] { 0}},
                {"float_0", new float[] { 2.5F}},
                {"float_1", new float[] { 0}} 
            };
            AssertExecuting(false, operation, engine, args);

        }
        [Test]
        public void TestOpCode_IsNonblankStr()
        {
            #region Condition RUles
            string operation = "Allow_0";
            List<QuickCondition> conditionList = new List<QuickCondition>();

            QuickCondition condition = new QuickCondition(operation);
            condition.Add("string_0", E_FunctionT.IsNonblankStr, "True");
            condition.Add("string_1", E_FunctionT.IsNonblankStr, "False");

            conditionList.Add(condition);
            #endregion

            ExecutingEngine engine = Compile(conditionList);

            Dictionary<string, object> args = null;

            args = new Dictionary<string, object>()
            {
                {"string_0", new string[] {"Hello"}},
                {"string_1", new string[] { string.Empty}},
            };
            AssertExecuting(true, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"string_0", new string[] {"Hello"}},
                //{"string_1", new string[] { "DS"}},
            };
            AssertExecuting(true, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"string_0", new string[] {"Hello"}},
                {"string_1", new string[] { null}},
            };
            AssertExecuting(true, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"string_0", new string[] {string.Empty}},
                {"string_1", new string[] { null}},
            };
            AssertExecuting(false, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"string_0", new string[] {null}},
                {"string_1", new string[] { null}},
            };
            AssertExecuting(false, operation, engine, args);


        }
        [Test]
        public void TestOpCode_In_Bool()
        {
            #region Condition Rules
            string operation = "Allow_0";
            List<QuickCondition> conditionList = new List<QuickCondition>();

            QuickCondition condition = new QuickCondition(operation);
            condition.Add("bool_0", E_FunctionT.In, true);
            condition.Add("bool_1", E_FunctionT.In, false);

            conditionList.Add(condition);
            #endregion

            ExecutingEngine engine = Compile(conditionList);

            Dictionary<string, object> args = null;

            args = new Dictionary<string, object>() {
             {"BOOL_0", new bool[] { true } },
             {"BOOL_1", new bool[] { false } }
            };

            AssertExecuting(true, operation, engine, args);

            args = new Dictionary<string, object>() {
             {"BOOL_0", new bool[] { true } },
             {"BOOL_1", new bool[] { true } }
            };
            AssertExecuting(false, operation.ToLower(), engine, args);

            args = new Dictionary<string, object>() {
             {"BOOL_0", new bool[] { false } },
             {"bool_1", new bool[] { false } }
            };
            AssertExecuting(false, operation.ToUpper(), engine, args);


            args = new Dictionary<string, object>() {
             {"BOOL_0", new bool[] { false } },
             {"bool_1", new bool[] { true } }
            };
            AssertExecuting(false, operation.ToUpper(), engine, args);

        }

        [Test]
        public void TestOpCode_In_Int()
        {
            #region Condition Rules
            string operation = "Allow_0";
            List<QuickCondition> conditionList = new List<QuickCondition>();

            QuickCondition condition = new QuickCondition(operation);
            condition.Add("int_0", E_FunctionT.In, 0);
            condition.Add("int_1", E_FunctionT.In, 0, 2);
            condition.Add("int_2", E_FunctionT.In, 0, 2, 4, 6, 8, 10, 12);
            condition.Add("int_3", E_FunctionT.In, 0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36, 37, 38);
            conditionList.Add(condition);

            condition = new QuickCondition(operation);
            condition.Add("int_0", E_FunctionT.In, -1);
            condition.Add("int_1", E_FunctionT.In, -10, -20, -15, -2);
            conditionList.Add(condition);

            condition = new QuickCondition(operation);
            condition.Add("int_2", E_FunctionT.In, 256);
            condition.Add("int_3", E_FunctionT.In, 300, 400, 200, 500, 800, 600, 700);
            conditionList.Add(condition);

            condition = new QuickCondition(operation);
            condition.Add("int_0", E_FunctionT.In, 70000);
            condition.Add("int_2", E_FunctionT.In, 60000, 50000, 70000, 80000);
            conditionList.Add(condition);

            #endregion

            ExecutingEngine engine = Compile(conditionList);

            Dictionary<string, object> args = null;

            #region Verify
            args = new Dictionary<string, object>()
            {
                {"int_0", new int[] { 0 }},
                {"int_1", new int[] { 2 }},
                {"int_2", new int[] { 8 }},
                {"int_3", new int[] { 28}}
            };

            AssertExecuting(true, operation, engine, args);
            #endregion

            #region Verify
            args = new Dictionary<string, object>()
            {
                {"int_0", new int[] { 0 }},
                {"int_1", new int[] { 0, 1, 2, 3 }},
                {"int_2", new int[] { 0, 1, 2, 3, 8, 10, 11 }},
                {"int_3", new int[] { 28}}
            };

            AssertExecuting(true, operation, engine, args);
            #endregion

            #region Verify
            args = new Dictionary<string, object>()
            {
                {"int_0", new int[] { -9, -8, -1 }},
                {"int_1", new int[] { -30, -15, -2, 3 }},
                {"int_2", new int[] { 0, 11, 21, 31, 81, 101, 111 }},
                {"int_3", new int[] { 281}}
            };

            AssertExecuting(true, operation, engine, args);
            #endregion

            #region Verify
            args = new Dictionary<string, object>()
            {
                {"int_0", new int[] { -9, -8, -10 }},
                {"int_1", new int[] { -30, -15, -2, 3 }},
                {"int_2", new int[] { 256 }},
                {"int_3", new int[] { 800}}
            };

            AssertExecuting(true, operation, engine, args);
            #endregion

            #region Verify
            args = new Dictionary<string, object>()
            {
                {"int_0", new int[] { 70000 }},
                {"int_1", new int[] { -30, -15, -2, 3 }},
                {"int_2", new int[] { 50000 }},
                {"int_3", new int[] { 800}}
            };

            AssertExecuting(true, operation, engine, args);
            #endregion

            #region Verify
            args = new Dictionary<string, object>()
            {
                {"int_0", new int[] { 70001, 1001 }},
                {"int_1", new int[] { -30, -15, -2, 3 }},
                {"int_2", new int[] { 500001, 200 }},
                {"int_3", new int[] { 800}}
            };

            AssertExecuting(false, operation, engine, args);
            #endregion
        }

        [Test]
        public void TestOpCode_In_Float()
        {
            #region Condition Rules
            string operation = "Allow_0";
            List<QuickCondition> conditionList = new List<QuickCondition>();

            QuickCondition condition = new QuickCondition(operation);
            condition.Add("float_0", E_FunctionT.In, (float)0);
            condition.Add("float_1", E_FunctionT.In, 1.5F);
            condition.Add("float_2", E_FunctionT.In, 34.0F, 34.1F, 34.2F, 34.3F);
            conditionList.Add(condition);
            #endregion

            ExecutingEngine engine = Compile(conditionList);

            Dictionary<string, object> args;

            args = new Dictionary<string, object>()
            {
                { "float_0", new float[] { -1F, 0F, 2.5F}},
                {"float_1", new float[] { 1.5F}},
                {"float_2", new float[] { 34.3F}}
            };

            AssertExecuting(true, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                { "float_0", new float[] { -1F, 0F, 2.5F}},
                {"float_1", new float[] { 1.5F}},
                {"float_2", new float[] { 34.2F}}
            };

            AssertExecuting(true, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                { "float_0", new float[] { -1F, 0F, 2.5F}},
                {"float_1", new float[] { 1.5F}},
                {"float_2", new float[] { 34.21F}}
            };

            AssertExecuting(false, operation, engine, args);

        }

        [Test]
        public void TestOpCode_In_String()
        {
            #region Condition Rules
            string operation = "Allow_0";
            List<QuickCondition> conditionList = new List<QuickCondition>();

            QuickCondition condition = new QuickCondition(operation);
            condition.Add("string_0", E_FunctionT.In, "A", "b");
            condition.Add("string_1", E_FunctionT.In, "a0", "d0", "b2");
            condition.Add("string_2", E_FunctionT.In, "AbC");
            conditionList.Add(condition);
            #endregion

            ExecutingEngine engine = Compile(conditionList);

            Dictionary<string, object> args;

            args = new Dictionary<string, object>()
            {
                {"string_0", new string[] { "A"}},
                {"string_1", new string[] {"A1", "AbC", "b2"}},
                {"string_2", new string[] {"AbC"}},
            };
            AssertExecuting(true, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"string_0", new string[] { "a"}},
                {"string_1", new string[] {"A1", "ABC", "B2"}},
                {"string_2", new string[] {"ABC"}},
            };
            AssertExecuting(true, operation, engine, args);


            args = new Dictionary<string, object>()
            {
                {"string_0", new string[] { "a"}},
                {"string_1", new string[] {"A1", "ABC", "A"}},
                {"string_2", new string[] {"ABC"}},
            };
            AssertExecuting(false, operation, engine, args);

        }

        [Test]
        public void TestOpCode_In_DateTime()
        {
            DateTime today = DateTime.Today;
            #region Condition Rules
            string operation = "Allow_0";
            List<QuickCondition> conditionList = new List<QuickCondition>();

            QuickCondition condition = new QuickCondition(operation);
            condition.Add("datetime_0", E_FunctionT.In, today);
            condition.Add("datetime_1", E_FunctionT.In, new DateTime(2010, 04, 24), new DateTime(2010, 06, 24));
            condition.Add("datetime_2", E_FunctionT.In, new DateTime(2010, 04, 24, 19, 30, 0), new DateTime(1978, 12, 5));
            conditionList.Add(condition);
            #endregion

            ExecutingEngine engine = Compile(conditionList);

            Dictionary<string, object> args;

            args = new Dictionary<string, object>()
            {
                {"datetime_0", new DateTime[] { today}},
                {"datetime_1", new DateTime[] { new DateTime(2010, 04, 24)}},
                {"datetime_2", new DateTime[] {new DateTime(2010, 04, 24, 19, 30, 0)}}
            };
            AssertExecuting(true, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"datetime_0", new DateTime[] { today}},
                {"datetime_1", new DateTime[] { new DateTime(2010, 04, 24, 19, 30, 0)}},
                {"datetime_2", new DateTime[] {new DateTime(2010, 04, 24, 19, 30, 0)}}
            };
            AssertExecuting(false, operation, engine, args);
            args = new Dictionary<string, object>()
            {
                {"datetime_0", new DateTime[] { today}},
                {"datetime_1", new DateTime[] { new DateTime(2010, 04, 24)}},
                {"datetime_2", new DateTime[] {new DateTime(2010, 04, 24)}}
            };
            AssertExecuting(false, operation, engine, args);
        }

        [Test]
        public void TestOpCode_Between_Int()
        {
            #region Condition Rules
            string operation = "Allow_0";
            List<QuickCondition> conditionList = new List<QuickCondition>();

            QuickCondition condition = new QuickCondition(operation);
            condition.Add("int_0", E_FunctionT.Between, 10, 30);
            condition.Add("int_1", E_FunctionT.Between, 50, 60);
            condition.Add("int_2", E_FunctionT.Between, -10, -5);
            condition.Add("int_3", E_FunctionT.Between, 20, 21);
            condition.Add("int_4", E_FunctionT.Between, 1000, 2000);
            condition.Add("int_5", E_FunctionT.Between, 100000, 200000);

            conditionList.Add(condition);
            #endregion

            ExecutingEngine engine = Compile(conditionList);

            Dictionary<string, object> args;
            args = new Dictionary<string, object>()
            {
                {"int_0", new int[] {15}},
                {"int_1", new int[] {50}},
                {"int_2", new int[] {-5}},
                {"int_3", new int[] {10, 20, 30}},
                {"int_4", new int[] {1050}},
                {"int_5", new int[] {100000}}

            };
            AssertExecuting(true, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"int_0", new int[] {15}},
                {"int_1", new int[] {50}},
                {"int_2", new int[] {-5}},
                {"int_3", new int[] {10, 20, 30}},
                {"int_4", new int[] {2001}},
                {"int_5", new int[] {100000}}

            };
            AssertExecuting(false, operation, engine, args);
        }

        [Test]
        public void TestOpCode_Between_Float()
        {
            #region Condition Rules
            string operation = "Allow_0";
            List<QuickCondition> conditionList = new List<QuickCondition>();

            QuickCondition condition = new QuickCondition(operation);
            condition.Add("float_0", E_FunctionT.Between, 10.5F, 30.5F);
            condition.Add("float_1", E_FunctionT.Between, -10.4F, -10.0F);

            conditionList.Add(condition);

            #endregion
            ExecutingEngine engine = Compile(conditionList);

            Dictionary<string, object> args;

            args = new Dictionary<string, object>()
            {
                {"float_0", new float[] { 10.5F}},
                {"float_1", new float[] { -10F}}
            };
            AssertExecuting(true, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"float_0", new float[] { 12F}},
                {"float_1", new float[] { -9.99F}}
            };
            AssertExecuting(false, operation, engine, args);
        }

        [Test]
        public void TestOpCode_Between_DateTime()
        {
            #region Condition Rules
            string operation = "Allow_0";
            List<QuickCondition> conditionList = new List<QuickCondition>();

            QuickCondition condition = new QuickCondition(operation);
            condition.Add("datetime_0", E_FunctionT.Between, new DateTime(2010, 04, 24), new DateTime(2010, 04, 30));

            conditionList.Add(condition);

            #endregion
            ExecutingEngine engine = Compile(conditionList);

            Dictionary<string, object> args;

            args = new Dictionary<string, object>()
            {
                {"datetime_0", new DateTime[] { new DateTime(2010, 04, 23), new DateTime(2010, 04, 24)}},
            };
            AssertExecuting(true, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"datetime_0", new DateTime[] { new DateTime(2010, 04, 26)}},
            };
            AssertExecuting(true, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"datetime_0", new DateTime[] { new DateTime(2010, 04, 30)}},
            };
            AssertExecuting(true, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"datetime_0", new DateTime[] { new DateTime(2010, 05, 01)}},
            };
            AssertExecuting(false, operation, engine, args);

        }

        [Test]
        public void TestOpCode_Comparison_Bool()
        {
            #region Condition Rules
            string operation = "Allow_0";
            List<QuickCondition> conditionList = new List<QuickCondition>();

            QuickCondition condition = new QuickCondition(operation);
            condition.Add("bool_0", E_FunctionT.Equal, true);
            condition.Add("bool_1", E_FunctionT.Equal, false);

            conditionList.Add(condition);
            #endregion

            ExecutingEngine engine = Compile(conditionList);

            Dictionary<string, object> args;

            args = new Dictionary<string, object>()
            {
                {"bool_0", new bool[] { true } },
                {"bool_1", new bool[] { false } }
            };

            AssertExecuting(true, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"bool_0", new bool[] { false } },
                {"bool_1", new bool[] { false } }
            };
            AssertExecuting(false, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"bool_0", new bool[] { true } },
                {"bool_1", new bool[] { true } }
            };
            AssertExecuting(false, operation, engine, args);
        }
        [Test]
        public void TestOpCode_Comparison_Int()
        {
            #region Condition Rules
            string operation = "Allow_0";
            List<QuickCondition> conditionList = new List<QuickCondition>();

            QuickCondition condition = new QuickCondition(operation);
            condition.Add("int_0", E_FunctionT.Equal, 1);
            condition.Add("int_1", E_FunctionT.IsGreaterThan, 2);
            condition.Add("int_2", E_FunctionT.IsGreaterThanOrEqualTo, 3);
            condition.Add("int_3", E_FunctionT.IsLessThan, 4);
            condition.Add("int_4", E_FunctionT.IsLessThanOrEqualTo, 5);
            conditionList.Add(condition);
            #endregion

            ExecutingEngine engine = Compile(conditionList);

            Dictionary<string, object> args;

            args = new Dictionary<string, object>()
            {
                {"int_0", new int[] { 1, 100, -10}},
                {"int_1", new int[] { 4}},
                {"int_2", new int[] { 3}},
                {"int_3", new int[] { 3}},
                {"int_4", new int[] { 5}},
            };
            AssertExecuting(true, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"int_0", new int[] { 1, 100, -10}},
                {"int_1", new int[] { 4}},
                {"int_2", new int[] { 1, 100, 200}},
                {"int_3", new int[] { 3}},
                {"int_4", new int[] { 9, 4, 6}},
            };
            AssertExecuting(true, operation, engine, args);


            args = new Dictionary<string, object>()
            {
                {"int_0", new int[] { 0, 100, -10}}, // False condition
                {"int_1", new int[] { 4}},
                {"int_2", new int[] { 1, 100, 200}},
                {"int_3", new int[] { 3}},
                {"int_4", new int[] { 9, 4, 6}},
            };
            AssertExecuting(false, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"int_0", new int[] { 1, 100, -10}},
                {"int_1", new int[] { 2}}, // False condition
                {"int_2", new int[] { 1, 100, 200}},
                {"int_3", new int[] { 3}},
                {"int_4", new int[] { 9, 4, 6}},
            };
            AssertExecuting(false, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"int_0", new int[] { 1, 100, -10}},
                {"int_1", new int[] { 1}}, // False condition
                {"int_2", new int[] { 1, 100, 200}},
                {"int_3", new int[] { 3}},
                {"int_4", new int[] { 9, 4, 6}},
            };
            AssertExecuting(false, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"int_0", new int[] { 1, 100, -10}},
                {"int_1", new int[] { 4}},
                {"int_2", new int[] { 2}}, // false condition
                {"int_3", new int[] { 3}},
                {"int_4", new int[] { 9, 4, 6}},
            };
            AssertExecuting(false, operation, engine, args);


            args = new Dictionary<string, object>()
            {
                {"int_0", new int[] { 1, 100, -10}},
                {"int_1", new int[] { 4}},
                {"int_2", new int[] { 1, 100, 200}},
                {"int_3", new int[] { 4}}, // false condition
                {"int_4", new int[] { 9, 4, 6}},
            };
            AssertExecuting(false, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"int_0", new int[] { 1, 100, -10}},
                {"int_1", new int[] { 4}},
                {"int_2", new int[] { 1, 100, 200}},
                {"int_3", new int[] { 8}}, // false condition
                {"int_4", new int[] { 9, 4, 6}},
            };
            AssertExecuting(false, operation, engine, args);


            args = new Dictionary<string, object>()
            {
                {"int_0", new int[] { 1, 100, -10}},
                {"int_1", new int[] { 4}},
                {"int_2", new int[] { 3}},
                {"int_3", new int[] { 3}},
                {"int_4", new int[] { 6}}, // false condition
            };
            AssertExecuting(false, operation, engine, args);
        }

        [Test]
        public void TestOpCode_Comparison_Float()
        {
            #region Condition Rules
            string operation = "Allow_0";
            List<QuickCondition> conditionList = new List<QuickCondition>();

            QuickCondition condition = new QuickCondition(operation);
            condition.Add("float_0", E_FunctionT.Equal, 1.23F);
            condition.Add("float_1", E_FunctionT.IsGreaterThan, 2.23F);
            condition.Add("float_2", E_FunctionT.IsGreaterThanOrEqualTo, 3.23F);
            condition.Add("float_3", E_FunctionT.IsLessThan, 4.23F);
            condition.Add("float_4", E_FunctionT.IsLessThanOrEqualTo, 5.23F);
            conditionList.Add(condition);
            #endregion

            ExecutingEngine engine = Compile(conditionList);

            Dictionary<string, object> args;

            args = new Dictionary<string, object>()
            {
                {"float_0", new float[] { 1.23F, 100.23F, -10.23F}},
                {"float_1", new float[] { 4.23F}},
                {"float_2", new float[] { 3.23F}},
                {"float_3", new float[] { 3.23F}},
                {"float_4", new float[] { 5.23F}},
            };
            AssertExecuting(true, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"float_0", new float[] { 1.23F, 100.23F, -10.23F}},
                {"float_1", new float[] { 4.23F}},
                {"float_2", new float[] { 1.23F, 100.23F, 200.23F}},
                {"float_3", new float[] { 3.23F}},
                {"float_4", new float[] { 9.23F, 4.23F, 6.23F}},
            };
            AssertExecuting(true, operation, engine, args);


            args = new Dictionary<string, object>()
            {
                {"float_0", new float[] { 0.23F, 100.23F, -10.23F}}, // False condition
                {"float_1", new float[] { 4.23F}},
                {"float_2", new float[] { 1.23F, 100.23F, 200.23F}},
                {"float_3", new float[] { 3.23F}},
                {"float_4", new float[] { 9.23F, 4.23F, 6.23F}},
            };
            AssertExecuting(false, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"float_0", new float[] { 1.23F, 100.23F, -10.23F}},
                {"float_1", new float[] { 2.23F}}, // False condition
                {"float_2", new float[] { 1.23F, 100.23F, 200.23F}},
                {"float_3", new float[] { 3.23F}},
                {"float_4", new float[] { 9.23F, 4.23F, 6.23F}},
            };
            AssertExecuting(false, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"float_0", new float[] { 1.23F, 100.23F, -10.23F}},
                {"float_1", new float[] { 1.23F}}, // False condition
                {"float_2", new float[] { 1.23F, 100.23F, 200.23F}},
                {"float_3", new float[] { 3.23F}},
                {"float_4", new float[] { 9.23F, 4.23F, 6.23F}},
            };
            AssertExecuting(false, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"float_0", new float[] { 1.23F, 100.23F, -10.23F}},
                {"float_1", new float[] { 4.23F}},
                {"float_2", new float[] { 2.23F}}, // false condition
                {"float_3", new float[] { 3.23F}},
                {"float_4", new float[] { 9.23F, 4.23F, 6.23F}},
            };
            AssertExecuting(false, operation, engine, args);


            args = new Dictionary<string, object>()
            {
                {"float_0", new float[] { 1.23F, 100.23F, -10.23F}},
                {"float_1", new float[] { 4.23F}},
                {"float_2", new float[] { 1.23F, 100.23F, 200.23F}},
                {"float_3", new float[] { 4.23F}}, // false condition
                {"float_4", new float[] { 9.23F, 4.23F, 6.23F}},
            };
            AssertExecuting(false, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"float_0", new float[] { 1.23F, 100.23F, -10.23F}},
                {"float_1", new float[] { 4.23F}},
                {"float_2", new float[] { 1.23F, 100.23F, 200.23F}},
                {"float_3", new float[] { 8.23F}}, // false condition
                {"float_4", new float[] { 9.23F, 4.23F, 6.23F}},
            };
            AssertExecuting(false, operation, engine, args);


            args = new Dictionary<string, object>()
            {
                {"float_0", new float[] { 1.23F, 100.23F, -10.23F}},
                {"float_1", new float[] { 4.23F}},
                {"float_2", new float[] { 3.23F}},
                {"float_3", new float[] { 3.23F}},
                {"float_4", new float[] { 6.23F}}, // false condition
            };
            AssertExecuting(false, operation, engine, args);

        }

        [Test]
        public void TestOpCode_Comparison_DateTime()
        {
            #region Condition Rules
            string operation = "Allow_0";
            List<QuickCondition> conditionList = new List<QuickCondition>();

            QuickCondition condition = new QuickCondition(operation);
            condition.Add("datetime_0", E_FunctionT.Equal, new DateTime(2010, 4, 24));
            condition.Add("datetime_1", E_FunctionT.IsGreaterThan, new DateTime(2010, 2, 14));
            condition.Add("datetime_2", E_FunctionT.IsGreaterThanOrEqualTo, new DateTime(2010, 3, 14));
            condition.Add("datetime_3", E_FunctionT.IsLessThan, new DateTime(2010, 2, 28));
            condition.Add("datetime_4", E_FunctionT.IsLessThanOrEqualTo, new DateTime(2010, 3, 25));
            conditionList.Add(condition);
            #endregion

            ExecutingEngine engine = Compile(conditionList);

            Dictionary<string, object> args;

            args = new Dictionary<string, object>()
            {
                {"datetime_0", new DateTime[] { new DateTime(2010, 4, 24)}},
                {"datetime_1", new DateTime[] { new DateTime(2010, 4, 20)}},
                {"datetime_2", new DateTime[] { new DateTime(2010, 3, 14)}},
                {"datetime_3", new DateTime[] { new DateTime(2010, 2, 24)}},
                {"datetime_4", new DateTime[] { new DateTime(2010, 3, 25)}},

            };

            AssertExecuting(true, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"datetime_0", new DateTime[] { new DateTime(2010, 4, 24)}},
                {"datetime_1", new DateTime[] { new DateTime(2010, 4, 20)}},
                {"datetime_2", new DateTime[] { new DateTime(2010, 4, 14)}},
                {"datetime_3", new DateTime[] { new DateTime(2010, 2, 24)}},
                {"datetime_4", new DateTime[] { new DateTime(2000, 3, 25)}},

            };

            AssertExecuting(true, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"datetime_0", new DateTime[] { new DateTime(2010, 4, 20)}},
                {"datetime_1", new DateTime[] { new DateTime(2010, 4, 20)}},
                {"datetime_2", new DateTime[] { new DateTime(2010, 4, 14)}},
                {"datetime_3", new DateTime[] { new DateTime(2010, 2, 24)}},
                {"datetime_4", new DateTime[] { new DateTime(2000, 3, 25)}},

            };

            AssertExecuting(false, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"datetime_0", new DateTime[] { new DateTime(2010, 4, 24)}},
                {"datetime_1", new DateTime[] { new DateTime(2000, 4, 14)}}, // false condition
                {"datetime_2", new DateTime[] { new DateTime(2010, 4, 14)}},
                {"datetime_3", new DateTime[] { new DateTime(2010, 2, 24)}},
                {"datetime_4", new DateTime[] { new DateTime(2000, 3, 25)}},

            };

            AssertExecuting(false, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"datetime_0", new DateTime[] { new DateTime(2010, 4, 24)}},
                {"datetime_1", new DateTime[] { new DateTime(2010, 4, 20)}},
                {"datetime_2", new DateTime[] { new DateTime(2010, 2, 14)}},
                {"datetime_3", new DateTime[] { new DateTime(2010, 2, 24)}},
                {"datetime_4", new DateTime[] { new DateTime(2000, 3, 25)}},

            };

            AssertExecuting(false, operation, engine, args);

        }

        [Test]
        public void TestOpCode_ComparisonVar()
        {
            #region Condition Rules
            string operation = "Allow_0";
            List<QuickCondition> conditionList = new List<QuickCondition>();

            QuickCondition condition = new QuickCondition(operation);
            condition.AddCompareVar(E_ParameterValueType.Bool, "bool_0", E_FunctionT.EqualVar, "bool_1");

            condition.AddCompareVar(E_ParameterValueType.Int, "int_0", E_FunctionT.IsGreaterThanVar, "int_1");
            condition.AddCompareVar(E_ParameterValueType.DateTime, "datetime_0", E_FunctionT.IsGreaterThanOrEqualToVar, "datetime_1");
            condition.AddCompareVar(E_ParameterValueType.Int, "int_2", E_FunctionT.IsLessThanVar, "int_3");
            condition.AddCompareVar(E_ParameterValueType.Float, "float_0", E_FunctionT.IsLessThanOrEqualToVar, "float_1");
            conditionList.Add(condition);
            #endregion

            ExecutingEngine engine = Compile(conditionList);

            Dictionary<string, object> args;
            #region Test
            args = new Dictionary<string, object>()
            {
                {"bool_0", new bool[] { true } },
                {"bool_1", new bool[] { true } },

                {"int_0", new int[] { 10}},
                {"int_1", new int[] {9}},

                {"datetime_0", new DateTime[] { new DateTime(2010, 04, 24)}},
                {"datetime_1", new DateTime[] { new DateTime(2010, 4, 24)}},

                {"int_2", new int[] { 10}},
                {"int_3", new int[] { 100000}},

                {"float_0", new float[] {10.54F}},
                {"float_1", new float[] {10.54F}},

            };

            AssertExecuting(true, operation, engine, args);
            #endregion
            #region Test
            args = new Dictionary<string, object>()
            {
                {"bool_0", new bool[] { true } },
                {"bool_1", new bool[] { true } },

                {"int_0", new int[] { 10}},
                {"int_1", new int[] {9}},

                {"datetime_0", new DateTime[] { new DateTime(2010, 5, 24)}},
                {"datetime_1", new DateTime[] { new DateTime(2010, 4, 24)}},

                {"int_2", new int[] { 10}},
                {"int_3", new int[] { 100000}},

                {"float_0", new float[] {10.0F}},
                {"float_1", new float[] {10.54F}},

            };

            AssertExecuting(true, operation, engine, args);
            #endregion
            #region Test
            args = new Dictionary<string, object>()
            {
                {"bool_0", new bool[] { true } }, // False condition
                {"bool_1", new bool[] { false } },

                {"int_0", new int[] { 10}},
                {"int_1", new int[] {9}},

                {"datetime_0", new DateTime[] { new DateTime(2010, 04, 24)}},
                {"datetime_1", new DateTime[] { new DateTime(2010, 4, 24)}},

                {"int_2", new int[] { 10}},
                {"int_3", new int[] { 100000}},

                {"float_0", new float[] {10.54F}},
                {"float_1", new float[] {10.54F}},

            };

            AssertExecuting(false, operation, engine, args);
            #endregion
            #region Test
            args = new Dictionary<string, object>()
            {
                {"bool_0", new bool[] { true } },
                {"bool_1", new bool[] { true } },

                {"int_0", new int[] { 9}}, // False condition
                {"int_1", new int[] {9}},

                {"datetime_0", new DateTime[] { new DateTime(2010, 04, 24)}},
                {"datetime_1", new DateTime[] { new DateTime(2010, 4, 24)}},

                {"int_2", new int[] { 10}},
                {"int_3", new int[] { 100000}},

                {"float_0", new float[] {10.54F}},
                {"float_1", new float[] {10.54F}},

            };

            AssertExecuting(false, operation, engine, args);
            #endregion

            args = new Dictionary<string, object>()
            {
                {"bool_0", new bool[] { true } },
                {"bool_1", new bool[] { true } },

                {"int_0", new int[] { 10}},
                {"int_1", new int[] {9}},

                {"datetime_0", new DateTime[] { new DateTime(2009, 04, 24)}}, // Fail condition
                {"datetime_1", new DateTime[] { new DateTime(2010, 4, 24)}},

                {"int_2", new int[] { 10}},
                {"int_3", new int[] { 100000}},

                {"float_0", new float[] {10.54F}},
                {"float_1", new float[] {10.54F}},

            };

            AssertExecuting(false, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"bool_0", new bool[] { true } },
                {"bool_1", new bool[] { true } },

                {"int_0", new int[] { 10}},
                {"int_1", new int[] {9}},

                {"datetime_0", new DateTime[] { new DateTime(2010, 04, 24)}},
                {"datetime_1", new DateTime[] { new DateTime(2010, 4, 24)}},

                {"int_2", new int[] { 200000}}, // Fail Condition
                {"int_3", new int[] { 100000}},

                {"float_0", new float[] {10.54F}},
                {"float_1", new float[] {10.54F}},

            };

            AssertExecuting(false, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"bool_0", new bool[] { true } },
                {"bool_1", new bool[] { true } },

                {"int_0", new int[] { 10}},
                {"int_1", new int[] {9}},

                {"datetime_0", new DateTime[] { new DateTime(2010, 04, 24)}},
                {"datetime_1", new DateTime[] { new DateTime(2010, 4, 24)}},

                {"int_2", new int[] { 10}},
                {"int_3", new int[] { 100000}},

                {"float_0", new float[] {10.64F}}, // Fail condition
                {"float_1", new float[] {10.54F}},

            };

            AssertExecuting(false, operation, engine, args);

        }

        [Test]
        public void TestOpCode_CustomVar()
        {
            #region Condition Rules
            string operation = "Allow_0";
            List<QuickCondition> conditionList = new List<QuickCondition>();
            List<QuickCondition> customVariableList = new List<QuickCondition>();

            QuickCondition condition = new QuickCondition(operation);
            condition.AddCustomVar("custom_0", E_FunctionT.Equal, true);
            conditionList.Add(condition);

            QuickCondition custom_0 = new QuickCondition("custom_0");
            custom_0.Add("int_0", E_FunctionT.IsGreaterThan, 5);
            custom_0.Add("int_1", E_FunctionT.IsLessThan, 4);
            custom_0.Add("int_2", E_FunctionT.In, 0, 1, 2);
            customVariableList.Add(custom_0);

            #endregion

            ExecutingEngine engine = Compile(conditionList, null, customVariableList);

            Dictionary<string, object> args;

            args = new Dictionary<string, object>()
            {
                {"int_0", new int[] { 6}},
                {"int_1", new int[] { 3}},
                {"int_2", new int[] { 4, 2}}
            };

            AssertExecuting(true, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"int_0", new int[] { 6}},
                {"int_1", new int[] { 6}},
                {"int_2", new int[] { 4, 2}}
            };

            AssertExecuting(false, operation, engine, args);
        }

        [Test]
        public void TestOpCode_CustomVar_1()
        {
            #region Condition Rules
            string operation = "Allow_0";
            List<QuickCondition> conditionList = new List<QuickCondition>();
            List<QuickCondition> customVariableList = new List<QuickCondition>();

            QuickCondition condition = new QuickCondition(operation);
            condition.AddCustomVar("custom_0", E_FunctionT.Equal, true);
            conditionList.Add(condition);

            QuickCondition custom_0 = new QuickCondition("custom_0");
            custom_0.Add("int_0", E_FunctionT.IsGreaterThan, 5);
            custom_0.Add("int_1", E_FunctionT.IsLessThan, 4);
            custom_0.Add("int_2", E_FunctionT.In, 0, 1, 2);
            customVariableList.Add(custom_0);

            custom_0 = new QuickCondition("custom_0");
            custom_0.Add("int_0", E_FunctionT.IsLessThan, 3);
            customVariableList.Add(custom_0);
            #endregion

            ExecutingEngine engine = Compile(conditionList, null, customVariableList);

            Dictionary<string, object> args;

            args = new Dictionary<string, object>()
            {
                {"int_0", new int[] { 6}},
                {"int_1", new int[] { 3}},
                {"int_2", new int[] { 4, 2}}
            };

            AssertExecuting(true, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"int_0", new int[] { 2}},
                {"int_1", new int[] { 4}},
                {"int_2", new int[] { 4, 2}}
            };

            AssertExecuting(true, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"int_0", new int[] { 4}},
                {"int_1", new int[] { 4}},
                {"int_2", new int[] { 4, 2}}
            };

            AssertExecuting(false, operation, engine, args);
        }

        [Test]
        public void TestOpCode_CustomVar_2()
        {
            #region Condition Rules
            string operation = "Allow_0";
            List<QuickCondition> conditionList = new List<QuickCondition>();
            List<QuickCondition> customVariableList = new List<QuickCondition>();

            QuickCondition condition = new QuickCondition(operation);
            condition.AddCustomVar("custom_0", E_FunctionT.Equal, false);
            conditionList.Add(condition);

            QuickCondition custom_0 = new QuickCondition("custom_0");
            custom_0.Add("int_0", E_FunctionT.IsGreaterThan, 5);
            custom_0.Add("int_1", E_FunctionT.IsLessThan, 4);
            custom_0.Add("int_2", E_FunctionT.In, 0, 1, 2);
            customVariableList.Add(custom_0);

            custom_0 = new QuickCondition("custom_0");
            custom_0.Add("int_0", E_FunctionT.IsGreaterThan, 30);
            customVariableList.Add(custom_0);
            #endregion

            ExecutingEngine engine = Compile(conditionList, null, customVariableList);

            Dictionary<string, object> args;

            args = new Dictionary<string, object>()
            {
                {"int_0", new int[] { 5}},
                {"int_1", new int[] { 3}},
                {"int_2", new int[] { 4, 2}}
            };

            AssertExecuting(true, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"int_0", new int[] { 6}},
                {"int_1", new int[] { 4}},
                {"int_2", new int[] { 4, 2}}
            };

            AssertExecuting(true, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"int_0", new int[] { 40}},
                {"int_1", new int[] { 4}},
                {"int_2", new int[] { 4, 2}}
            };

            AssertExecuting(false, operation, engine, args);
        }

        [Test]
        public void Test_NestedCustomVar()
        {
            #region Condition Rules
            string operation = "Allow_0";
            List<QuickCondition> conditionList = new List<QuickCondition>();
            List<QuickCondition> customVariableList = new List<QuickCondition>();

            QuickCondition condition = new QuickCondition(operation);
            condition.AddCustomVar("custom_0", E_FunctionT.Equal, true);
            conditionList.Add(condition);

            QuickCondition custom_0 = new QuickCondition("custom_0");
            custom_0.Add("int_0", E_FunctionT.IsGreaterThan, 5);
            custom_0.Add("int_1", E_FunctionT.IsLessThan, 4);
            custom_0.Add("int_2", E_FunctionT.In, 0, 1, 2);
            custom_0.AddCustomVar("custom_1", E_FunctionT.Equal, true);
            customVariableList.Add(custom_0);

            QuickCondition custom_1 = new QuickCondition("custom_1");
            custom_1.Add("int_3", E_FunctionT.IsGreaterThan, 5);
            customVariableList.Add(custom_1);

            #endregion

            ExecutingEngine engine = Compile(conditionList, null, customVariableList);

            Dictionary<string, object> args;

            args = new Dictionary<string, object>()
            {
                {"int_0", new int[] { 6}},
                {"int_1", new int[] { 3}},
                {"int_2", new int[] { 4, 2}},
                {"int_3", new int[] { 7}}
            };

            AssertExecuting(true, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"int_0", new int[] { 6}},
                {"int_1", new int[] { 3}},
                {"int_2", new int[] { 4, 2}},
                {"int_3", new int[] { 4}}
            };

            AssertExecuting(false, operation, engine, args);

        }

        [Test]
        public void Test_GetDependencyVariableListForTrigger()
        {
            #region Condition Rules
            string operation = "Allow_0";
            List<QuickCondition> conditionList = new List<QuickCondition>();
            List<QuickCondition> customVariableList = new List<QuickCondition>();

            QuickCondition condition = new QuickCondition(operation);
            condition.AddCustomVar("custom_0", E_FunctionT.Equal, true);
            conditionList.Add(condition);

            QuickCondition custom_0 = new QuickCondition("custom_0");
            custom_0.Add("int_0", E_FunctionT.IsGreaterThan, 5);
            custom_0.Add("int_1", E_FunctionT.IsLessThan, 4);
            custom_0.Add("int_2", E_FunctionT.In, 0, 1, 2);
            custom_0.AddCustomVar("custom_1", E_FunctionT.Equal, true);
            customVariableList.Add(custom_0);

            QuickCondition custom_1 = new QuickCondition("custom_1");
            custom_1.Add("int_3", E_FunctionT.IsGreaterThan, 5);
            customVariableList.Add(custom_1);

            QuickCondition custom_2 = new QuickCondition("custom_2");
            custom_2.Add("Var_A", E_FunctionT.IsLessThan, 1);
            custom_2.Add("Var_B", E_FunctionT.Equal, 2);
            customVariableList.Add(custom_2);

            QuickCondition custom_2a = new QuickCondition("custom_2");
            custom_2a.Add("Var_A", E_FunctionT.IsLessThan, 3);
            custom_2a.Add("Var_C", E_FunctionT.Equal, 4);
            customVariableList.Add(custom_2a);

            #endregion

            ExecutingEngine engine = Compile(conditionList, null, customVariableList);
            string[] custom_0_dependencyFieldList = new string[] { "int_0", "int_1", "int_2","int_3" };
            string[] custom_1_dependencyFieldList = new string[] { "int_3" };
            string[] custom_2_dependencyFieldList = new string[] { "var_a", "var_b", "var_c"};
            AssertSameList(custom_0_dependencyFieldList, engine.GetDependencyVariableListForTrigger("custom_0"), "Custom_0");

            AssertSameList(custom_1_dependencyFieldList, engine.GetDependencyVariableListForTrigger("custom_1"), "Custom_1");


            AssertSameList(custom_2_dependencyFieldList, engine.GetDependencyVariableListForTrigger("Custom_2"), "Custom_2");

            Assert.IsNull(engine.GetDependencyVariableListForTrigger("NOT_EXISTED"), "Invalid Custom Variable");

        }

        [Test]
        public void Test_IsTriggerContainsDate()
        {
            #region Condition Rules
            string operation = "Allow_0";
            List<QuickCondition> conditionList = new List<QuickCondition>();
            List<QuickCondition> customVariableList = new List<QuickCondition>();

            QuickCondition condition = new QuickCondition(operation);
            condition.AddCustomVar("custom_0", E_FunctionT.Equal, true);
            conditionList.Add(condition);

            QuickCondition custom_0 = new QuickCondition("custom_0");
            custom_0.Add("int_0", E_FunctionT.IsGreaterThan, 5);
            custom_0.Add("int_1", E_FunctionT.IsLessThan, 4);
            custom_0.Add("int_2", E_FunctionT.Equal, 4);
            custom_0.AddCustomVar("custom_2", E_FunctionT.Equal, true);
            customVariableList.Add(custom_0);

            QuickCondition custom_1 = new QuickCondition("custom_1");
            custom_1.Add("date_0", E_FunctionT.Equal, DateTime.Now);
            customVariableList.Add(custom_1);

            QuickCondition custom_2 = new QuickCondition("custom_2");
            custom_2.Add("Var_A", E_FunctionT.IsLessThan, 1);
            custom_2.Add("Var_B", E_FunctionT.Equal, 2);
            customVariableList.Add(custom_2);

            QuickCondition custom_2a = new QuickCondition("custom_2");
            custom_2a.Add("Var_A", E_FunctionT.IsLessThan, 3);
            custom_2a.Add("Var_C", E_FunctionT.Equal, 4);
            customVariableList.Add(custom_2a);

            QuickCondition nested_custom = new QuickCondition("nested_custom");
            nested_custom.AddCustomVar("custom_1", E_FunctionT.Equal, true);
            customVariableList.Add(nested_custom);

            QuickCondition system_custom = new QuickCondition("system_custom");
            system_custom.AddCustomVar("system_custom_0", E_FunctionT.Equal, true);
            customVariableList.Add(system_custom);

            QuickCondition system_custom_nodate = new QuickCondition("system_custom_no_date");
            system_custom_nodate.AddCustomVar("system_custom_1", E_FunctionT.Equal, true);
            customVariableList.Add(system_custom_nodate);
            #endregion

            #region Generate System Configuration.
            List<QuickCondition> systemCustomVariableList = new List<QuickCondition>();
            condition = new QuickCondition("system_custom_0");
            condition.Add("date_4", E_FunctionT.Equal, DateTime.Now);
            systemCustomVariableList.Add(condition);

            condition = new QuickCondition("system_custom_1");
            condition.Add("int_4", E_FunctionT.Equal, 4);
            systemCustomVariableList.Add(condition);

            ExecutingEngine systemEngine = Compile(null, null, systemCustomVariableList);

            #endregion

            ExecutingEngine engine = Compile(null, null, customVariableList);
            engine.SetSystemExecutingEngineForNUnit(systemEngine);


            Assert.AreEqual(true, engine.IsTriggerContainDateParameters("custom_1"), "Custom 1");
            Assert.AreEqual(false, engine.IsTriggerContainDateParameters("custom_0"), "Custom 0");
            Assert.AreEqual(false, engine.IsTriggerContainDateParameters("NOT EXITED"), "NOT EXISTED");

            Assert.AreEqual(true, engine.IsTriggerContainDateParameters("nested_custom"), "Nested Custom");
        }

        [Test]
        public void Test_SystemCustomVariables()
        {
            #region Condition Rules
            string operation = "Allow_0";

            List<QuickCondition> conditionList = new List<QuickCondition>();
            List<QuickCondition> customVariableList = new List<QuickCondition>();

            QuickCondition condition = new QuickCondition(operation);
            condition.AddSystemCustomVar("custom_0", E_FunctionT.Equal, true);
            conditionList.Add(condition);
            #endregion

            #region Generate System Configuration.
            List<QuickCondition> systemCustomVariableList = new List<QuickCondition>();
            condition = new QuickCondition("custom_0");
            condition.Add("int_0", E_FunctionT.IsGreaterThan, 5);
            systemCustomVariableList.Add(condition);

            ExecutingEngine systemEngine = Compile(null, null, systemCustomVariableList);

            #endregion
           
            ExecutingEngine engine = Compile(conditionList, null, customVariableList);
            engine.SetSystemExecutingEngineForNUnit(systemEngine);


            Dictionary<string, object> args;

            args = new Dictionary<string, object>()
            {
                {"int_0", new int[] { 6}}
            };

            AssertExecuting(true, operation, engine, args);
        }

        [Test]
        public void Test_SystemCustomVariables2()
        {
            #region Condition Rules
            string operation = "Allow_0";

            List<QuickCondition> conditionList = new List<QuickCondition>();
            List<QuickCondition> customVariableList = new List<QuickCondition>();

            QuickCondition condition = new QuickCondition(operation);
            condition.AddSystemCustomVar("custom_0", E_FunctionT.Equal, true);
            conditionList.Add(condition);
            #endregion

            #region Generate System Configuration.
            List<QuickCondition> systemCustomVariableList = new List<QuickCondition>();
            condition = new QuickCondition("custom_0");
            condition.Add("int_0", E_FunctionT.IsGreaterThan, 5);
            condition.AddSystemCustomVar("custom_1", E_FunctionT.Equal, true); // This should execute custom_1 in the same engine.
            systemCustomVariableList.Add(condition);

            condition = new QuickCondition("custom_1");
            condition.Add("int_1", E_FunctionT.IsGreaterThan, 3);
            systemCustomVariableList.Add(condition);

            ExecutingEngine systemEngine = Compile(null, null, systemCustomVariableList);

            #endregion

            ExecutingEngine engine = Compile(conditionList, null, customVariableList);
            engine.SetSystemExecutingEngineForNUnit(systemEngine);


            Dictionary<string, object> args;

            args = new Dictionary<string, object>()
            {
                {"int_0", new int[] { 6}},
                {"int_1", new int[] { 4}}
            };

            AssertExecuting(true, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"int_0", new int[] { 6}},
                {"int_1", new int[] { 2}}
            };

            AssertExecuting(false, operation, engine, args);

        }

        [Test]
        public void Test_SystemCustomVariables3()
        {
            #region Condition Rules
            string operation = "Allow_0";

            List<QuickCondition> conditionList = new List<QuickCondition>();
            List<QuickCondition> customVariableList = new List<QuickCondition>();

            QuickCondition condition = new QuickCondition(operation);
            condition.AddSystemCustomVar("custom_0", E_FunctionT.Equal, true);
            conditionList.Add(condition);
            #endregion

            #region Generate System Configuration.
            List<QuickCondition> systemCustomVariableList = new List<QuickCondition>();
            condition = new QuickCondition("custom_0");
            condition.Add("int_0", E_FunctionT.IsGreaterThan, 5);
            condition.AddCustomVar("custom_1", E_FunctionT.Equal, true); 
            systemCustomVariableList.Add(condition);

            condition = new QuickCondition("custom_1");
            condition.Add("int_1", E_FunctionT.IsGreaterThan, 3);
            systemCustomVariableList.Add(condition);

            ExecutingEngine systemEngine = Compile(null, null, systemCustomVariableList);

            #endregion

            ExecutingEngine engine = Compile(conditionList, null, customVariableList);
            engine.SetSystemExecutingEngineForNUnit(systemEngine);


            Dictionary<string, object> args;

            args = new Dictionary<string, object>()
            {
                {"int_0", new int[] { 6}},
                {"int_1", new int[] { 4}}
            };

            AssertExecuting(true, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"int_0", new int[] { 6}},
                {"int_1", new int[] { 2}}
            };

            AssertExecuting(false, operation, engine, args);

        }

        [Test]
        public void Test_GetAllValidConditions()
        {
            #region Condition Rules
            string operation = "Allow_0";
            List<QuickCondition> conditionList = new List<QuickCondition>();

            QuickCondition condition_0 = new QuickCondition(operation);
            condition_0.Add("bool_0", E_FunctionT.Equal, true);
            conditionList.Add(condition_0);

            QuickCondition condition_1 = new QuickCondition(operation);
            condition_1.Add("int_0", E_FunctionT.IsGreaterThan, 5);
            condition_1.Add("int_1", E_FunctionT.IsLessThan, 4);
            condition_1.Add("int_2", E_FunctionT.In, 0, 1, 2);

            conditionList.Add(condition_1);

            QuickCondition condition_2 = new QuickCondition(operation);
            condition_2.Add("int_3", E_FunctionT.IsGreaterThan, 5);

            conditionList.Add(condition_2);
            #endregion

            ExecutingEngine engine = Compile(conditionList);

            Dictionary<string, object> args;
            ExecutingEngineTestEvaluator ticket;
            List<Guid> expectedConditionList;

            #region Test 1
            args = new Dictionary<string, object>()
            {
                {"int_0", new int[] { 6}},
                {"int_1", new int[] { 3}},
                {"int_2", new int[] { 4, 2}},
                {"int_3", new int[] { 7}}
            };

            ticket = CreateExecutingEngineTestEvaluator(args, new StringBuilder());
            expectedConditionList = new List<Guid>();
            expectedConditionList.Add(condition_1.Id);
            expectedConditionList.Add(condition_2.Id);

            AssertSameList(expectedConditionList, engine.GetAllPassingPrivilegeConditionIds(operation, ticket), "");
            #endregion

            #region Test 2
            args = new Dictionary<string, object>()
            {
                {"int_0", new int[] { 2}},
                {"int_1", new int[] { 3}},
                {"int_2", new int[] { 4, 2}},
                {"int_3", new int[] { 2}}
            };

            ticket = CreateExecutingEngineTestEvaluator(args, new StringBuilder());
            expectedConditionList = new List<Guid>();
            expectedConditionList.Add(condition_1.Id);
            expectedConditionList.Add(condition_2.Id);
            Assert.AreEqual(0, engine.GetAllPassingPrivilegeConditionIds(operation, ticket).Count());
            #endregion
        }

        [Test]
        public void Test_GetDependencyVariableList()
        {
            #region Condition Rules
            string operation = "Allow_0";
            List<QuickCondition> conditionList = new List<QuickCondition>();
            List<QuickCondition> customVariableList = new List<QuickCondition>();

            QuickCondition condition_0 = new QuickCondition(operation);
            condition_0.Add("bool_0", E_FunctionT.Equal, true);
            conditionList.Add(condition_0);

            QuickCondition condition_1 = new QuickCondition(operation);
            condition_1.Add("int_0", E_FunctionT.IsGreaterThan, 5);
            condition_1.Add("int_1", E_FunctionT.IsLessThan, 4);
            condition_1.Add("int_2", E_FunctionT.In, 0, 1, 2);
            conditionList.Add(condition_1);

            QuickCondition condition_2 = new QuickCondition(operation);
            condition_2.Add("int_3", E_FunctionT.IsGreaterThan, 5);
            conditionList.Add(condition_2);

            string operation_1 = "Allow_1";

            QuickCondition op_1_condition_0 = new QuickCondition(operation_1);
            op_1_condition_0.Add("op_1_bool_0", E_FunctionT.Equal, true);
            conditionList.Add(op_1_condition_0);

            QuickCondition op_1_condition_1 = new QuickCondition(operation_1);
            op_1_condition_1.AddCustomVar("op_1_custom_0", E_FunctionT.Equal, true);
            op_1_condition_1.Add("int_0", E_FunctionT.IsGreaterThan, 5);
            conditionList.Add(op_1_condition_1);

            QuickCondition op_1_custom_0 = new QuickCondition("op_1_custom_0");
            op_1_custom_0.Add("custom_0_1", E_FunctionT.Equal, true);
            op_1_custom_0.Add("int_2", E_FunctionT.Equal, 3);
            op_1_custom_0.AddCustomVar("op_1_custom_1", E_FunctionT.Equal, true);
            op_1_custom_0.AddCustomVar("op_1_custom_2", E_FunctionT.Equal, true);
            customVariableList.Add(op_1_custom_0);

            QuickCondition op_1_custom_1 = new QuickCondition("op_1_custom_1");
            op_1_custom_1.Add("bool_0", E_FunctionT.Equal, true);
            customVariableList.Add(op_1_custom_1);

            QuickCondition op_1_custom_2 = new QuickCondition("op_1_custom_2");
            op_1_custom_2.Add("int5", E_FunctionT.Equal, 3);
            op_1_custom_2.AddCustomVar("op_1_custom_3", E_FunctionT.Equal, true);
            customVariableList.Add(op_1_custom_2);

            QuickCondition op_1_custom_3 = new QuickCondition("op_1_custom_3");
            op_1_custom_3.Add("int6", E_FunctionT.Equal, 3);
            customVariableList.Add(op_1_custom_3);
            #endregion

            ExecutingEngine engine = Compile(conditionList, null, customVariableList);

            string[] allow_0_varList = { "bool_0", "int_0", "int_1", "int_2", "int_3" };
            string[] allow_1_varList = { "op_1_bool_0", "int_0", "custom_0_1", "int_2", "bool_0", "int5", "int6" };

            List<string> joint_list = new List<string>();
            joint_list.AddRange(allow_0_varList);
            joint_list.AddRange(allow_1_varList);

            AssertSameList(allow_0_varList, engine.GetDependencyVariableList(new string[] { "Allow_0" }), "Allow_0");
            AssertSameList(allow_1_varList, engine.GetDependencyVariableList(new string[] { "Allow_1" }), "Allow_1");
            AssertSameList(joint_list, engine.GetDependencyVariableList(new string[] { "Allow_0", "Allow_1" }), "Joint List");

        }

        [Test]
        public void Test_GetDependencyVariableListWithSystemCustomVariable()
        {
            #region Condition Rules
            string operation = "Allow_0";

            List<QuickCondition> conditionList = new List<QuickCondition>();
            List<QuickCondition> customVariableList = new List<QuickCondition>();

            QuickCondition condition = new QuickCondition(operation);
            condition.AddSystemCustomVar("custom_0", E_FunctionT.Equal, true);
            conditionList.Add(condition);
            #endregion

            #region Generate System Configuration.
            List<QuickCondition> systemCustomVariableList = new List<QuickCondition>();
            condition = new QuickCondition("custom_0");
            condition.Add("int_0", E_FunctionT.IsGreaterThan, 5);
            condition.Add("int_1", E_FunctionT.IsGreaterThan, 6);
            systemCustomVariableList.Add(condition);

            ExecutingEngine systemEngine = Compile(null, null, systemCustomVariableList);

            #endregion
           
            ExecutingEngine engine = Compile(conditionList, null, customVariableList);
            engine.SetSystemExecutingEngineForNUnit(systemEngine);

            string[] allow_0_varList = { "int_0", "int_1" };

            AssertSameList(allow_0_varList, engine.GetDependencyVariableList(new string[] { "Allow_0" }), "Allow_0");
        
        }

        /// <summary>
        /// Restraint test modified from TestOpCode_Comparison_Int
        /// </summary>
        [Test]
        public void TestRestraints()
        {
            #region Condition Rules
            string operation = "Allow_0";

            List<QuickCondition> conditionList = new List<QuickCondition>();
            QuickCondition condition = new QuickCondition(operation);
            condition.Add("AlwaysTrue", E_FunctionT.Equal, true);
            conditionList.Add(condition);

            List<QuickCondition> restraintList = new List<QuickCondition>();
            QuickCondition restraint = new QuickCondition(operation);
            restraint.Add("int_0", E_FunctionT.Equal, 1);
            restraint.Add("int_1", E_FunctionT.IsGreaterThan, 2);
            restraint.Add("int_2", E_FunctionT.IsGreaterThanOrEqualTo, 3);
            restraint.Add("int_3", E_FunctionT.IsLessThan, 4);
            restraint.Add("int_4", E_FunctionT.IsLessThanOrEqualTo, 5);
            restraintList.Add(restraint);
            #endregion

            ExecutingEngine engine = Compile(conditionList, restraintList, null);

            Dictionary<string, object> args;

            args = new Dictionary<string, object>()
            {
                {"AlwaysTrue", new bool[] { true } },
                {"int_0", new int[] { 1, 100, -10}},
                {"int_1", new int[] { 4}},
                {"int_2", new int[] { 3}},
                {"int_3", new int[] { 3}},
                {"int_4", new int[] { 5}},
            };
            AssertExecuting(false, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"AlwaysTrue", new bool[] { true } },
                {"int_0", new int[] { 1, 100, -10}},
                {"int_1", new int[] { 4}},
                {"int_2", new int[] { 1, 100, 200}},
                {"int_3", new int[] { 3}},
                {"int_4", new int[] { 9, 4, 6}},
            };
            AssertExecuting(false, operation, engine, args);


            args = new Dictionary<string, object>()
            {
                {"AlwaysTrue", new bool[] { true } },
                {"int_0", new int[] { 0, 100, -10}}, // False condition
                {"int_1", new int[] { 4}},
                {"int_2", new int[] { 1, 100, 200}},
                {"int_3", new int[] { 3}},
                {"int_4", new int[] { 9, 4, 6}},
            };
            AssertExecuting(true, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"AlwaysTrue", new bool[] { true } },
                {"int_0", new int[] { 1, 100, -10}},
                {"int_1", new int[] { 2}}, // False condition
                {"int_2", new int[] { 1, 100, 200}},
                {"int_3", new int[] { 3}},
                {"int_4", new int[] { 9, 4, 6}},
            };
            AssertExecuting(true, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"AlwaysTrue", new bool[] { true } },
                {"int_0", new int[] { 1, 100, -10}},
                {"int_1", new int[] { 1}}, // False condition
                {"int_2", new int[] { 1, 100, 200}},
                {"int_3", new int[] { 3}},
                {"int_4", new int[] { 9, 4, 6}},
            };
            AssertExecuting(true, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"AlwaysTrue", new bool[] { true } },
                {"int_0", new int[] { 1, 100, -10}},
                {"int_1", new int[] { 4}},
                {"int_2", new int[] { 2}}, // false condition
                {"int_3", new int[] { 3}},
                {"int_4", new int[] { 9, 4, 6}},
            };
            AssertExecuting(true, operation, engine, args);


            args = new Dictionary<string, object>()
            {
                {"AlwaysTrue", new bool[] { true } },
                {"int_0", new int[] { 1, 100, -10}},
                {"int_1", new int[] { 4}},
                {"int_2", new int[] { 1, 100, 200}},
                {"int_3", new int[] { 4}}, // false condition
                {"int_4", new int[] { 9, 4, 6}},
            };
            AssertExecuting(true, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"AlwaysTrue", new bool[] { true } },
                {"int_0", new int[] { 1, 100, -10}},
                {"int_1", new int[] { 4}},
                {"int_2", new int[] { 1, 100, 200}},
                {"int_3", new int[] { 8}}, // false condition
                {"int_4", new int[] { 9, 4, 6}},
            };
            AssertExecuting(true, operation, engine, args);

            args = new Dictionary<string, object>()
            {
                {"AlwaysTrue", new bool[] { true } },
                {"int_0", new int[] { 1, 100, -10}},
                {"int_1", new int[] { 4}},
                {"int_2", new int[] { 3}},
                {"int_3", new int[] { 3}},
                {"int_4", new int[] { 6}}, // false condition
            };
            AssertExecuting(true, operation, engine, args);
        }

        private void AssertSameList<T>(IEnumerable<T> expectedList, IEnumerable<T> actualList, string msg)
        {
            HashSet<T> expectedSet = new HashSet<T>(expectedList);
            HashSet<T> actualSet = new HashSet<T>(actualList);
            StringBuilder sb = new StringBuilder(msg);
            sb.Append(" Expected:[");
            foreach (var o in expectedSet)
            {
                sb.Append(o.ToString() + ",");
            }
            sb.Append("], Actual:[");
            foreach (var o in actualSet)
            {
                sb.Append(o.ToString() + ",");
            }
            sb.Append("]");
            Assert.AreEqual(true, expectedSet.SetEquals(actualSet), sb.ToString());
        }
        private ExecutingEngineTestEvaluator CreateExecutingEngineTestEvaluator(Dictionary<string, object> args, StringBuilder sb)
        {
            ExecutingEngineTestEvaluator ticket = new ExecutingEngineTestEvaluator();
            foreach (var o in args)
            {
                string str = string.Empty;

                Type t = o.Value.GetType();
                if (t == typeof(bool[]))
                {
                    ticket.Add(o.Key, (bool[])o.Value);
                    str = o.Value.ToString();
                }
                else if (t == typeof(int[]))
                {
                    ticket.Add(o.Key, (int[])o.Value);
                    foreach (var s in (int[])o.Value)
                    {
                        str += s + ",";
                    }
                }
                else if (t == typeof(float[]))
                {
                    ticket.Add(o.Key, (float[])o.Value);
                    foreach (var s in (float[])o.Value)
                    {
                        str += s + ",";
                    }

                }
                else if (t == typeof(string[]))
                {
                    ticket.Add(o.Key, (string[])o.Value);
                    foreach (var s in (string[])o.Value)
                    {
                        str += s + ",";
                    }

                }
                else if (t == typeof(DateTime[]))
                {
                    ticket.Add(o.Key, (DateTime[])o.Value);
                    foreach (var s in (DateTime[])o.Value)
                    {
                        str += s + ",";
                    }
                }
                else
                {
                    throw new Exception("Unhandled type=" + t);
                }

                sb.AppendFormat("{0}={1}; ", o.Key, str);

            }
            return ticket;

        }
        private void AssertExecuting(bool bExpectedResult, string operationName, ExecutingEngine engine, Dictionary<string, object> args)
        {
            StringBuilder sb = new StringBuilder();
            ExecutingEngineTestEvaluator ticket = CreateExecutingEngineTestEvaluator(args, sb);
            FakeOperation operation = new FakeOperation(operationName);

            bool bActualResult = !engine.HasPassingRestraint(operation, ticket) && engine.HasPassingPrivilege(operation, ticket);

            // Code bellow is to debug when you do not get expected result.
            if (bActualResult != bExpectedResult)
            {
                ExecutingEngineDebug debug = new ExecutingEngineDebug(operation.Id);

                bool result = true;
                if (engine.HasPassingRestraint(operation, ticket, debug))
                {
                    result = false;
                }

                result &= engine.HasPassingPrivilege(operation, ticket, debug);
                debug.CanPerform = result;

                StringBuilder debugString = new StringBuilder();
                int i = 0;
                foreach (var o in debug.ConditionList)
                {
                    sb.AppendLine("Condition #" + i + " Failed:" + o.FailedParameterCount + ", Successful:" + o.SuccessfulParameterCount);
                    foreach (var j in o.FailedParameterList)
                    {
                        sb.AppendLine("    " + j.ToString());
                    }
                    i++;
                }
                System.Console.Error.Write(debugString);
            }
            Assert.AreEqual(bExpectedResult, bActualResult, sb.ToString());
        }

    }

    public class ExecutingEngineTestEvaluator : IEngineValueEvaluator
    {
        private Dictionary<string, IList<bool>> m_boolHash = new Dictionary<string, IList<bool>>(StringComparer.OrdinalIgnoreCase);
        private Dictionary<string, IList<int>> m_intHash = new Dictionary<string, IList<int>>(StringComparer.OrdinalIgnoreCase);
        private Dictionary<string, IList<float>> m_floatHash = new Dictionary<string, IList<float>>(StringComparer.OrdinalIgnoreCase);
        private Dictionary<string, IList<string>> m_stringHash = new Dictionary<string, IList<string>>(StringComparer.OrdinalIgnoreCase);
        private Dictionary<string, IList<DateTime>> m_dateTimeHash = new Dictionary<string, IList<DateTime>>(StringComparer.OrdinalIgnoreCase);

        public void Add(string name, IList<bool> value)
        {
            m_boolHash.Add(name, value);
        }
        public void Add(string name, IList<int> value)
        {
            m_intHash.Add(name, value);
        }
        public void Add(string name, IList<float> value)
        {
            m_floatHash.Add(name, value);
        }
        public void Add(string name, IList<string> value)
        {
            m_stringHash.Add(name, value);
        }
        public void Add(string name, IList<DateTime> value)
        {
            m_dateTimeHash.Add(name, value);
        }
        #region IEngineValueEvaluator Members

        public IList<bool> GetBoolValues(string name)
        {
            return m_boolHash[name];
        }

        public IList<int> GetIntValues(string name)
        {
            try
            {
                return m_intHash[name];
            }
            catch
            {
                throw new Exception("Missing " + name);
            }
        }

        public IList<float> GetFloatValues(string name)
        {
            return m_floatHash[name];
        }

        public IList<string> GetStringValues(string name)
        {
            return m_stringHash[name];
        }

        #endregion

        #region IEngineValueEvaluator Members


        public IList<DateTime> GetDateTimeValues(string name)
        {
            return m_dateTimeHash[name];
        }

        #endregion

        public bool IsDataModified(string name)
        {
            return false;
        }
    }

}
