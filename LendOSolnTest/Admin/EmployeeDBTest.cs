﻿// <copyright file="EmployeeDBTest.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: geoffreyf
//    Date:   10/6/2014 9:12:15 AM 
// </summary>
namespace LendOSolnTest.Admin
{
    using System;
    using Common;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using NUnit.Framework;

    /// <summary>
    /// Tests for the EmployeeDB class.
    /// </summary>
    [TestFixture]
    public class EmployeeDBTest
    {
        private const char PmlUser = 'P';

        private readonly Random random = new Random();

        private readonly Guid TestId = new Guid("88888888-8888-8888-8888-888888888888");
        private readonly Guid PrimaryDefaultId = new Guid("99999999-9999-9999-9999-999999999999");
        private readonly Guid SecondaryDefaultId = new Guid("AAAAAAAA-AAAA-AAAA-AAAA-AAAAAAAAAAAA");

        private MockBrokerDb broker;
        
        [TestFixtureSetUp]
        public void Setup()
        {
            LoginTools.LoginAs(TestAccountType.LoanOfficer);
        }

        [SetUp]
        public void InitializeMockBroker()
        {
            this.broker = new MockBrokerDb();

            this.broker.UpdateDefaultBranchPriceGroupValues(
                null,
                this.PrimaryDefaultId,
                this.PrimaryDefaultId,
                this.PrimaryDefaultId,
                this.PrimaryDefaultId,
                this.PrimaryDefaultId,
                this.PrimaryDefaultId);
        }

        #region Branch Testing
        [Test]
        public void TestNoWholesaleBranchSet()
        {
            var employee = new MockEmployeeDB();

            employee.UserType = EmployeeDBTest.PmlUser;

            Assert.AreEqual(
                Guid.Empty,
                employee.BranchID,
                "EmployeeDB.BranchID should not return a valid BranchID when an " +
                "employee has not been assigned a wholesale branch.");
        }

        [Test]
        public void TestWholesaleBranch()
        {
            var employee = new MockEmployeeDB();

            employee.UserType = EmployeeDBTest.PmlUser;

            employee.BranchID = this.TestId;

            Assert.AreEqual(
                this.TestId,
                employee.BranchID,
                "The getter should return the same value that was set.");

            Assert.IsFalse(
                employee.UseOriginatingCompanyBranchId,
                "The employee should not be using the OC wholesale branch id.");
        }

        [Test]
        public void TestUseOCWholesaleBranch()
        {
            var employee = new MockEmployeeDB();

            employee.UserType = EmployeeDBTest.PmlUser;

            employee.BranchID = this.TestId;

            employee.UseOriginatingCompanyBranchId = true;

            Assert.AreEqual(
                MockEmployeeDB.OCWholesaleBranchID,
                employee.BranchID,
                "The employee should be using the branch id from the originating company.");
        }

        [Test]
        public void TestUseOCWholesaleDefaultBranch()
        {
            if (!ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                Assert.Ignore("ConstStage.EnableDefaultBranchesAndPriceGroups is false.");

                return;
            }

            var employee = new MockEmployeeDB();

            employee.UserType = EmployeeDBTest.PmlUser;

            employee.SetUseCustomCorporateBroker(this.broker);

            employee.UseOriginatingCompanyBranchId = true;

            Assert.AreEqual(
                this.PrimaryDefaultId,
                employee.BranchID,
                "If an employee is set to use their OC's wholesale Branch ID and " +
                "that ID has not been set but a TPO portal-level default exists, " +
                "the employee should be using the TPO portal-level default.");
        }

        [Test]
        public void TestUseOCWholesaleDefaultBranchUpdatesAfterChange()
        {
            if (!ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                Assert.Ignore("ConstStage.EnableDefaultBranchesAndPriceGroups is false.");

                return;
            }

            var employee = new MockEmployeeDB();

            employee.UserType = EmployeeDBTest.PmlUser;

            employee.SetUseCustomCorporateBroker(this.broker);

            employee.UseOriginatingCompanyBranchId = true;

            Assert.AreEqual(
                this.PrimaryDefaultId,
                employee.BranchID,
                "If an employee is set to use their OC's wholesale Branch ID and " +
                "that ID has not been set but a TPO portal-level default exists, " +
                "the employee should be using the TPO portal-level default.");

            this.broker.UpdateDefaultBranchPriceGroupValues(
                null,
                this.SecondaryDefaultId,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty);

            Assert.AreEqual(
                this.SecondaryDefaultId,
                employee.BranchID,
                "If an employee is set to use their OC's wholesale Branch ID, the OC is " +
                "using the default wholesale value, and that default changes, the employee's " +
                "wholesale branch should also change.");
        }

        [Test]
        public void TestNoMiniCorrBranchSet()
        {
            var employee = new MockEmployeeDB();

            employee.UserType = EmployeeDBTest.PmlUser;

            Assert.AreEqual(
                Guid.Empty,
                employee.MiniCorrespondentBranchID,
                "EmployeeDB.MiniCorrespondentBranchID should not return a valid BranchID when an " +
                "employee has not been assigned a Mini Correspondent branch.");
        }

        [Test]
        public void TestMiniCorrBranch()
        {
            var employee = new MockEmployeeDB();

            employee.UserType = EmployeeDBTest.PmlUser;

            employee.MiniCorrespondentBranchID = this.TestId;

            Assert.AreEqual(
                this.TestId,
                employee.MiniCorrespondentBranchID,
                "The getter should return the same value that was set.");
            Assert.That(
                !employee.UseOriginatingCompanyMiniCorrBranchId,
                "The employee should not be using the OC mini corr branch id.");
        }

        [Test]
        public void TestUseOCMiniCorrBranch()
        {
            var employee = new MockEmployeeDB();

            employee.UserType = EmployeeDBTest.PmlUser;

            employee.MiniCorrespondentBranchID = this.TestId;

            employee.UseOriginatingCompanyMiniCorrBranchId = true;

            Assert.AreEqual(
                MockEmployeeDB.OCMiniCorrespondentBranchID,
                employee.MiniCorrespondentBranchID,
                "The employee should be using the branch id from the originating company.");
        }

        [Test]
        public void TestUseOCMiniCorrDefaultBranch()
        {
            if (!ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                Assert.Ignore("ConstStage.EnableDefaultBranchesAndPriceGroups is false.");

                return;
            }

            var employee = new MockEmployeeDB();

            employee.UserType = EmployeeDBTest.PmlUser;

            employee.SetUseCustomCorporateBroker(this.broker);

            employee.UseOriginatingCompanyMiniCorrBranchId = true;

            Assert.AreEqual(
                this.PrimaryDefaultId,
                employee.MiniCorrespondentBranchID,
                "If an employee is set to use their OC's Mini Correspondent Branch ID and " +
                "that ID has not been set but a TPO portal-level default exists, " +
                "the employee should be using the TPO portal-level default.");
        }

        [Test]
        public void TestUseOCMiniCorrDefaultBranchUpdatesAfterChange()
        {
            if (!ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                Assert.Ignore("ConstStage.EnableDefaultBranchesAndPriceGroups is false.");

                return;
            }

            var employee = new MockEmployeeDB();

            employee.UserType = EmployeeDBTest.PmlUser;

            employee.SetUseCustomCorporateBroker(this.broker);

            employee.UseOriginatingCompanyMiniCorrBranchId = true;

            Assert.AreEqual(
                this.PrimaryDefaultId,
                employee.MiniCorrespondentBranchID,
                "If an employee is set to use their OC's Mini Correspondent Branch ID and " +
                "that ID has not been set but a TPO portal-level default exists, " +
                "the employee should be using the TPO portal-level default.");

            this.broker.UpdateDefaultBranchPriceGroupValues(
                null,
                Guid.Empty,
                Guid.Empty,
                this.SecondaryDefaultId,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty);

            Assert.AreEqual(
                this.SecondaryDefaultId,
                employee.MiniCorrespondentBranchID,
                "If an employee is set to use their OC's mini corr Branch ID, the OC is " +
                "using the default mini corr value, and that default changes, the employee's " +
                "mini corr branch should also change.");
        }

        [Test]
        public void TestNoCorrBranchSet()
        {
            var employee = new MockEmployeeDB();

            employee.UserType = EmployeeDBTest.PmlUser;

            Assert.AreEqual(
                Guid.Empty,
                employee.CorrespondentBranchID,
                "EmployeeDB.CorrespondentBranchID should not return a valid BranchID when an " +
                "employee has not been assigned a Correspondent branch.");
        }

        [Test]
        public void TestCorrBranch()
        {
            var employee = new MockEmployeeDB();

            employee.CorrespondentBranchID = this.TestId;

            employee.UserType = EmployeeDBTest.PmlUser;

            Assert.AreEqual(
                this.TestId,
                employee.CorrespondentBranchID,
                "The getter should return the same value that was set.");
            Assert.That(
                !employee.UseOriginatingCompanyCorrBranchId,
                "The employee should not be using the OC corr branch id.");
        }

        [Test]
        public void TestUseOCCorrBranch()
        {
            var employee = new MockEmployeeDB();

            employee.UserType = EmployeeDBTest.PmlUser;

            employee.CorrespondentBranchID = this.TestId;

            employee.UseOriginatingCompanyCorrBranchId = true;

            Assert.AreEqual(
                MockEmployeeDB.OCCorrespondentBranchID,
                employee.CorrespondentBranchID,
                "The employee should be using the branch id from the originating company.");
        }

        [Test]
        public void TestUseOCCorrDefaultBranch()
        {
            if (!ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                Assert.Ignore("ConstStage.EnableDefaultBranchesAndPriceGroups is false.");

                return;
            }

            var employee = new MockEmployeeDB();

            employee.UserType = EmployeeDBTest.PmlUser;

            employee.SetUseCustomCorporateBroker(this.broker);

            employee.UseOriginatingCompanyCorrBranchId = true;

            Assert.AreEqual(
                this.PrimaryDefaultId,
                employee.CorrespondentBranchID,                
                "If an employee is set to use their OC's Correspondent Branch ID and " +
                "that ID has not been set but a TPO portal-level default exists, " +
                "the employee should be using the TPO portal-level default.");
        }

        [Test]
        public void TestUseOCCorrDefaultBranchUpdatesAfterChange()
        {
            if (!ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                Assert.Ignore("ConstStage.EnableDefaultBranchesAndPriceGroups is false.");

                return;
            }

            var employee = new MockEmployeeDB();

            employee.UserType = EmployeeDBTest.PmlUser;

            employee.SetUseCustomCorporateBroker(this.broker);

            employee.UseOriginatingCompanyCorrBranchId = true;

            Assert.AreEqual(
                this.PrimaryDefaultId,
                employee.CorrespondentBranchID,
                "If an employee is set to use their OC's Correspondent Branch ID and " +
                "that ID has not been set but a TPO portal-level default exists, " +
                "the employee should be using the TPO portal-level default.");

            this.broker.UpdateDefaultBranchPriceGroupValues(
                null,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty,
                this.SecondaryDefaultId,
                Guid.Empty);

            Assert.AreEqual(
                this.SecondaryDefaultId,
                employee.CorrespondentBranchID,
                "If an employee is set to use their OC's corr Branch ID, the OC is " +
                "using the default corr value, and that default changes, the employee's " +
                "corr branch should also change.");
        }
        #endregion

        #region Price Group Testing
        [Test]
        public void TestNoWholesalePriceGroupSet()
        {
            var employee = new MockEmployeeDB();

            employee.UserType = EmployeeDBTest.PmlUser;

            Assert.AreEqual(
                Guid.Empty,
                employee.LpePriceGroupID,
                "EmployeeDB.LpePriceGroupID should not return a valid price group ID when an " +
                "employee has not been assigned a wholesale price group.");
        }

        [Test]
        public void TestWholesalePriceGroup()
        {
            var employee = new MockEmployeeDB();

            employee.UserType = EmployeeDBTest.PmlUser;

            employee.LpePriceGroupID = this.TestId;

            Assert.AreEqual(
                this.TestId,
                employee.LpePriceGroupID,
                "The getter should return the same value that was set.");
            Assert.That(
                !employee.UseOriginatingCompanyLpePriceGroupId,
                "The employee should not be using the OC wholesale price group id.");
        }

        [Test]
        public void TestUseOCWholesalePriceGroup()
        {
            var employee = new MockEmployeeDB();

            employee.UserType = EmployeeDBTest.PmlUser;

            employee.LpePriceGroupID = this.TestId;

            employee.UseOriginatingCompanyLpePriceGroupId = true;

            Assert.AreEqual(
                MockEmployeeDB.OCWholesalePriceGroupID,
                employee.LpePriceGroupID,
                "The employee should be using the branch id from the originating company.");
        }

        [Test]
        public void TestUseOCWholesaleDefaultPriceGroup()
        {
            if (!ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                Assert.Ignore("ConstStage.EnableDefaultBranchesAndPriceGroups is false.");

                return;
            }

            var employee = new MockEmployeeDB();

            employee.UserType = EmployeeDBTest.PmlUser;

            employee.SetUseCustomCorporateBroker(this.broker);

            employee.UseOriginatingCompanyLpePriceGroupId = true;

            Assert.AreEqual(
                this.PrimaryDefaultId,
                employee.LpePriceGroupID,
                "If an employee is set to use their OC's wholesale PG ID and " +
                "that ID has not been set but a TPO portal-level default exists, " +
                "the employee should be using the TPO portal-level default.");
        }

        [Test]
        public void TestUseOCWholesaleDefaultPGUpdatesAfterChange()
        {
            if (!ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                Assert.Ignore("ConstStage.EnableDefaultBranchesAndPriceGroups is false.");

                return;
            }

            var employee = new MockEmployeeDB();

            employee.UserType = EmployeeDBTest.PmlUser;

            employee.SetUseCustomCorporateBroker(this.broker);

            employee.UseOriginatingCompanyLpePriceGroupId = true;

            Assert.AreEqual(
                this.PrimaryDefaultId,
                employee.LpePriceGroupID,
                "If an employee is set to use their OC's wholesale PG ID and " +
                "that ID has not been set but a TPO portal-level default exists, " +
                "the employee should be using the TPO portal-level default.");

            this.broker.UpdateDefaultBranchPriceGroupValues(
                null,
                Guid.Empty,
                this.SecondaryDefaultId,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty);

            Assert.AreEqual(
                this.SecondaryDefaultId,
                employee.LpePriceGroupID,
                "If an employee is set to use their OC's wholesale PG ID, the OC is " +
                "using the default wholesale value, and that default changes, the employee's " +
                "wholesale PG should also change.");
        }

        [Test]
        public void TestNoMiniCorrPriceGroupSet()
        {
            var employee = new MockEmployeeDB();

            employee.UserType = EmployeeDBTest.PmlUser;

            Assert.AreEqual(
                Guid.Empty,
                employee.MiniCorrespondentPriceGroupID,
                "EmployeeDB.MiniCorrespondentPriceGroupID should not return a valid price group ID when an " +
                "employee has not been assigned a Mini Correspondent price group.");
        }

        [Test]
        public void TestMiniCorrPriceGroup()
        {
            var employee = new MockEmployeeDB();

            employee.UserType = EmployeeDBTest.PmlUser;

            Assert.AreEqual(
                Guid.Empty,
                employee.MiniCorrespondentPriceGroupID,
                "EmployeeDB.MiniCorrespondentPriceGroupID should not return a valid price group ID when an " +
                "employee has not been assigned a Mini Correspondent price group.");

            employee.MiniCorrespondentPriceGroupID = this.TestId;

            Assert.AreEqual(
                this.TestId,
                employee.MiniCorrespondentPriceGroupID,
                "The getter should return the same value that was set.");
            Assert.That(
                !employee.UseOriginatingCompanyMiniCorrPriceGroupId,
                "The employee should not be using the OC mini corr price group id.");
        }

        [Test]
        public void TestUseOCMiniCorrPriceGroup()
        {
            var employee = new MockEmployeeDB();

            employee.UserType = EmployeeDBTest.PmlUser;

            employee.MiniCorrespondentPriceGroupID = this.TestId;

            employee.UseOriginatingCompanyMiniCorrPriceGroupId = true;

            Assert.AreEqual(
                MockEmployeeDB.OCMiniCorrespondentPriceGroupID,
                employee.MiniCorrespondentPriceGroupID,
                "The employee should be using the branch id from the originating company.");
        }

        [Test]
        public void TestUseOCMiniCorrDefaultPriceGroup()
        {
            if (!ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                Assert.Ignore("ConstStage.EnableDefaultBranchesAndPriceGroups is false.");

                return;
            }

            var employee = new MockEmployeeDB();

            employee.UserType = EmployeeDBTest.PmlUser;

            employee.SetUseCustomCorporateBroker(this.broker);

            employee.UseOriginatingCompanyMiniCorrPriceGroupId = true;

            Assert.AreEqual(
                this.PrimaryDefaultId,
                employee.MiniCorrespondentPriceGroupID,
                "If an employee is set to use their OC's Mini Correspondent PG ID and " +
                "that ID has not been set but a TPO portal-level default exists, " +
                "the employee should be using the TPO portal-level default.");
        }

        [Test]
        public void TestUseOCMiniCorrDefaultPGUpdatesAfterChange()
        {
            if (!ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                Assert.Ignore("ConstStage.EnableDefaultBranchesAndPriceGroups is false.");

                return;
            }

            var employee = new MockEmployeeDB();

            employee.UserType = EmployeeDBTest.PmlUser;

            employee.SetUseCustomCorporateBroker(this.broker);

            employee.UseOriginatingCompanyMiniCorrPriceGroupId = true;

            Assert.AreEqual(
                this.PrimaryDefaultId,
                employee.MiniCorrespondentPriceGroupID,
                "If an employee is set to use their OC's Mini Correspondent PG ID and " +
                "that ID has not been set but a TPO portal-level default exists, " +
                "the employee should be using the TPO portal-level default.");

            this.broker.UpdateDefaultBranchPriceGroupValues(
                null,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty,
                this.SecondaryDefaultId,
                Guid.Empty,
                Guid.Empty);

            Assert.AreEqual(
                this.SecondaryDefaultId,
                employee.MiniCorrespondentPriceGroupID,
                "If an employee is set to use their OC's mini corr PG ID, the OC is " +
                "using the default wholesale value, and that default changes, the employee's " +
                "mini corr PG should also change.");
        }

        [Test]
        public void TestNoCorrPriceGroupSet()
        {
            var employee = new MockEmployeeDB();

            employee.UserType = EmployeeDBTest.PmlUser;

            Assert.AreEqual(
                Guid.Empty,
                employee.MiniCorrespondentPriceGroupID,
                "EmployeeDB.CorrespondentPriceGroupID should not return a valid price group ID when an " +
                "employee has not been assigned a Correspondent price group.");
        }

        [Test]
        public void TestCorrPriceGroup()
        {
            var employee = new MockEmployeeDB();

            employee.UserType = EmployeeDBTest.PmlUser;

            Assert.AreEqual(
                Guid.Empty,
                employee.MiniCorrespondentPriceGroupID,
                "EmployeeDB.CorrespondentPriceGroupID should not return a valid price group ID when an " +
                "employee has not been assigned a Correspondent price group.");

            employee.CorrespondentPriceGroupID = this.TestId;

            Assert.AreEqual(
                this.TestId,
                employee.CorrespondentPriceGroupID,
                "The getter should return the same value that was set.");
            Assert.That(
                !employee.UseOriginatingCompanyCorrPriceGroupId,
                "The employee should not be using the OC corr price group id.");
        }

        [Test]
        public void TestUseOCCorrPriceGroup()
        {
            var employee = new MockEmployeeDB();

            employee.UserType = EmployeeDBTest.PmlUser;

            employee.CorrespondentPriceGroupID = this.TestId;

            employee.UseOriginatingCompanyCorrPriceGroupId = true;

            Assert.AreEqual(
                MockEmployeeDB.OCCorrespondentPriceGroupID,
                employee.CorrespondentPriceGroupID,
                "The employee should be using the branch id from the originating company.");
        }

        [Test]
        public void TestUseOCCorrDefaultPriceGroup()
        {
            if (!ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                Assert.Ignore("ConstStage.EnableDefaultBranchesAndPriceGroups is false.");

                return;
            }

            var employee = new MockEmployeeDB();

            employee.UserType = EmployeeDBTest.PmlUser;

            employee.SetUseCustomCorporateBroker(this.broker);

            employee.UseOriginatingCompanyCorrPriceGroupId = true;

            Assert.AreEqual(
                this.PrimaryDefaultId,
                employee.CorrespondentPriceGroupID,
                "If an employee is set to use their OC's Mini Corr PG ID and " +
                "that ID has not been set but a TPO portal-level default exists, " +
                "the employee should be using the TPO portal-level default.");
        }

        [Test]
        public void TestUseOCCorrDefaultPGUpdatesAfterChange()
        {
            if (!ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                Assert.Ignore("ConstStage.EnableDefaultBranchesAndPriceGroups is false.");

                return;
            }

            var employee = new MockEmployeeDB();

            employee.UserType = EmployeeDBTest.PmlUser;

            employee.SetUseCustomCorporateBroker(this.broker);

            employee.UseOriginatingCompanyCorrPriceGroupId = true;

            Assert.AreEqual(
                this.PrimaryDefaultId,
                employee.CorrespondentPriceGroupID,
                "If an employee is set to use their OC's Mini Corr PG ID and " +
                "that ID has not been set but a TPO portal-level default exists, " +
                "the employee should be using the TPO portal-level default.");

            this.broker.UpdateDefaultBranchPriceGroupValues(
                null,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty,
                this.SecondaryDefaultId);

            Assert.AreEqual(
                this.SecondaryDefaultId,
                employee.CorrespondentPriceGroupID,
                "If an employee is set to use their OC's corr PG ID, the OC is " +
                "using the default wholesale value, and that default changes, the " +
                "employee's corr PG should also change.");
        }
        #endregion

        [Test]
        public void TestMiniCorrLandingPage()
        {
            var employee = new MockEmployeeDB();

            employee.UserType = EmployeeDBTest.PmlUser;

            employee.MiniCorrespondentLandingPageID = this.TestId;

            Assert.AreEqual(
                this.TestId,
                employee.MiniCorrespondentLandingPageID,
                "The getter should return the same value that was set.");
        }

        [Test]
        public void TestCorrLandingPage()
        {
            var employee = new MockEmployeeDB();

            employee.UserType = EmployeeDBTest.PmlUser;

            employee.CorrespondentLandingPageID = this.TestId;

            Assert.AreEqual(
                this.TestId,
                employee.CorrespondentLandingPageID,
                "The getter should return the same value that was set.");
        }

        #region Custom Policy Testing
        [Test]
        public void CustomPricingPolicyField1_SourceIndividual_ReturnsFixedValue()
        {
            var employee = new TestEmployeeDB();

            employee.CustomPricingPolicyFieldValueSource = DataAccess.E_PricingPolicyFieldValueSource.IndividualUser;
            var testPolicyValue = "test";
            employee.CustomPricingPolicyField1Fixed = testPolicyValue;

            Assert.AreEqual(testPolicyValue, employee.CustomPricingPolicyField1Fixed);
        }

        [Test]
        public void CustomPricingPolicyField1_SourceOC_ReturnsOCValue()
        {
            var employee = new TestEmployeeDB();
            var originatingCompany = PmlBroker.CreatePmlBroker(ConstAppDavid.SystemBrokerGuid);
            var testPolicyValue = "test";
            originatingCompany.CustomPricingPolicyField1 = testPolicyValue;

            employee.UserType = 'P';
            employee.CustomPricingPolicyFieldValueSource = DataAccess.E_PricingPolicyFieldValueSource.OriginatingCompanyOrBranch;
            employee.PmlBrokerId = this.TestId;
            employee.originatingCompany = originatingCompany;

            Assert.AreEqual(testPolicyValue, employee.CustomPricingPolicyField1Fixed);
        }

        [Test]
        public void CustomPricingPolicyField1_SourceBranch_ReturnsBranchValue()
        {
            var employee = new TestEmployeeDB();
            var branch = new BranchDB();
            var testPolicyValue = "test";
            branch.CustomPricingPolicyField1 = testPolicyValue;

            employee.CustomPricingPolicyFieldValueSource = DataAccess.E_PricingPolicyFieldValueSource.OriginatingCompanyOrBranch;
            employee.BranchID = this.TestId;
            employee.branch = branch;

            Assert.AreEqual(testPolicyValue, employee.CustomPricingPolicyField1Fixed);
        }

        [Test]
        public void CustomPricingPolicyField2_SourceIndividual_ReturnsFixedValue()
        {
            var employee = new TestEmployeeDB();

            employee.CustomPricingPolicyFieldValueSource = DataAccess.E_PricingPolicyFieldValueSource.IndividualUser;
            var testPolicyValue = "test";
            employee.CustomPricingPolicyField2Fixed = testPolicyValue;

            Assert.AreEqual(testPolicyValue, employee.CustomPricingPolicyField2Fixed);
        }

        [Test]
        public void CustomPricingPolicyField2_SourceOC_ReturnsOCValue()
        {
            var employee = new TestEmployeeDB();
            var originatingCompany = PmlBroker.CreatePmlBroker(ConstAppDavid.SystemBrokerGuid);
            var testPolicyValue = "test";
            originatingCompany.CustomPricingPolicyField2 = testPolicyValue;

            employee.UserType = 'P';
            employee.CustomPricingPolicyFieldValueSource = DataAccess.E_PricingPolicyFieldValueSource.OriginatingCompanyOrBranch;
            employee.PmlBrokerId = this.TestId;
            employee.originatingCompany = originatingCompany;

            Assert.AreEqual(testPolicyValue, employee.CustomPricingPolicyField2Fixed);
        }

        [Test]
        public void CustomPricingPolicyField2_SourceBranch_ReturnsBranchValue()
        {
            var employee = new TestEmployeeDB();
            var branch = new BranchDB();
            var testPolicyValue = "test";
            branch.CustomPricingPolicyField2 = testPolicyValue;

            employee.CustomPricingPolicyFieldValueSource = DataAccess.E_PricingPolicyFieldValueSource.OriginatingCompanyOrBranch;
            employee.BranchID = this.TestId;
            employee.branch = branch;

            Assert.AreEqual(testPolicyValue, employee.CustomPricingPolicyField2Fixed);
        }

        [Test]
        public void CustomPricingPolicyField3_SourceIndividual_ReturnsFixedValue()
        {
            var employee = new TestEmployeeDB();

            employee.CustomPricingPolicyFieldValueSource = DataAccess.E_PricingPolicyFieldValueSource.IndividualUser;
            var testPolicyValue = "test";
            employee.CustomPricingPolicyField3Fixed = testPolicyValue;

            Assert.AreEqual(testPolicyValue, employee.CustomPricingPolicyField3Fixed);
        }

        [Test]
        public void CustomPricingPolicyField3_SourceOC_ReturnsOCValue()
        {
            var employee = new TestEmployeeDB();
            var originatingCompany = PmlBroker.CreatePmlBroker(ConstAppDavid.SystemBrokerGuid);
            var testPolicyValue = "test";
            originatingCompany.CustomPricingPolicyField3 = testPolicyValue;

            employee.UserType = 'P';
            employee.CustomPricingPolicyFieldValueSource = DataAccess.E_PricingPolicyFieldValueSource.OriginatingCompanyOrBranch;
            employee.PmlBrokerId = this.TestId;
            employee.originatingCompany = originatingCompany;

            Assert.AreEqual(testPolicyValue, employee.CustomPricingPolicyField3Fixed);
        }

        [Test]
        public void CustomPricingPolicyField3_SourceBranch_ReturnsBranchValue()
        {
            var employee = new TestEmployeeDB();
            var branch = new BranchDB();
            var testPolicyValue = "test";
            branch.CustomPricingPolicyField3 = testPolicyValue;

            employee.CustomPricingPolicyFieldValueSource = DataAccess.E_PricingPolicyFieldValueSource.OriginatingCompanyOrBranch;
            employee.BranchID = this.TestId;
            employee.branch = branch;

            Assert.AreEqual(testPolicyValue, employee.CustomPricingPolicyField3Fixed);
        }


        [Test]
        public void CustomPricingPolicyField4_SourceIndividual_ReturnsFixedValue()
        {
            var employee = new TestEmployeeDB();

            employee.CustomPricingPolicyFieldValueSource = DataAccess.E_PricingPolicyFieldValueSource.IndividualUser;
            var testPolicyValue = "test";
            employee.CustomPricingPolicyField4Fixed = testPolicyValue;

            Assert.AreEqual(testPolicyValue, employee.CustomPricingPolicyField4Fixed);
        }

        [Test]
        public void CustomPricingPolicyField4_SourceOC_ReturnsOCValue()
        {
            var employee = new TestEmployeeDB();
            var originatingCompany = PmlBroker.CreatePmlBroker(ConstAppDavid.SystemBrokerGuid);
            var testPolicyValue = "test";
            originatingCompany.CustomPricingPolicyField4 = testPolicyValue;

            employee.UserType = 'P';
            employee.CustomPricingPolicyFieldValueSource = DataAccess.E_PricingPolicyFieldValueSource.OriginatingCompanyOrBranch;
            employee.PmlBrokerId = this.TestId;
            employee.originatingCompany = originatingCompany;

            Assert.AreEqual(testPolicyValue, employee.CustomPricingPolicyField4Fixed);
        }

        [Test]
        public void CustomPricingPolicyField4_SourceBranch_ReturnsBranchValue()
        {
            var employee = new TestEmployeeDB();
            var branch = new BranchDB();
            var testPolicyValue = "test";
            branch.CustomPricingPolicyField4 = testPolicyValue;

            employee.CustomPricingPolicyFieldValueSource = DataAccess.E_PricingPolicyFieldValueSource.OriginatingCompanyOrBranch;
            employee.BranchID = this.TestId;
            employee.branch = branch;

            Assert.AreEqual(testPolicyValue, employee.CustomPricingPolicyField4Fixed);
        }

        [Test]
        public void CustomPricingPolicyField5_SourceIndividual_ReturnsFixedValue()
        {
            var employee = new TestEmployeeDB();

            employee.CustomPricingPolicyFieldValueSource = DataAccess.E_PricingPolicyFieldValueSource.IndividualUser;
            var testPolicyValue = "test";
            employee.CustomPricingPolicyField5Fixed = testPolicyValue;

            Assert.AreEqual(testPolicyValue, employee.CustomPricingPolicyField5Fixed);
        }

        [Test]
        public void CustomPricingPolicyField5_SourceOC_ReturnsOCValue()
        {
            var employee = new TestEmployeeDB();
            var originatingCompany = PmlBroker.CreatePmlBroker(ConstAppDavid.SystemBrokerGuid);
            var testPolicyValue = "test";
            originatingCompany.CustomPricingPolicyField5 = testPolicyValue;

            employee.UserType = 'P';
            employee.CustomPricingPolicyFieldValueSource = DataAccess.E_PricingPolicyFieldValueSource.OriginatingCompanyOrBranch;
            employee.PmlBrokerId = this.TestId;
            employee.originatingCompany = originatingCompany;

            Assert.AreEqual(testPolicyValue, employee.CustomPricingPolicyField5Fixed);
        }

        [Test]
        public void CustomPricingPolicyField5_SourceBranch_ReturnsBranchValue()
        {
            var employee = new TestEmployeeDB();
            var branch = new BranchDB();
            var testPolicyValue = "test";
            branch.CustomPricingPolicyField5 = testPolicyValue;

            employee.CustomPricingPolicyFieldValueSource = DataAccess.E_PricingPolicyFieldValueSource.OriginatingCompanyOrBranch;
            employee.BranchID = this.TestId;
            employee.branch = branch;

            Assert.AreEqual(testPolicyValue, employee.CustomPricingPolicyField5Fixed);
        }

        [Test]
        public void ResolveTaskEmailOption_LqbUser()
        {
            var employeeTaskOptions = new[] { E_TaskRelatedEmailOptionT.DontReceiveEmail, E_TaskRelatedEmailOptionT.ReceiveEmail };
            var originatingCompanyTaskOptions = new[] { default(E_TaskRelatedEmailOptionT?), E_TaskRelatedEmailOptionT.DontReceiveEmail, E_TaskRelatedEmailOptionT.ReceiveEmail };
            var populationMethods = new[] { EmployeePopulationMethodT.IndividualUser, EmployeePopulationMethodT.OriginatingCompany };

            employeeTaskOptions.Shuffle(this.random);
            originatingCompanyTaskOptions.Shuffle(this.random);
            populationMethods.Shuffle(this.random);

            foreach (var employeeTaskOption in employeeTaskOptions)
            {
                foreach (var originatingCompanyTaskOption in originatingCompanyTaskOptions)
                {
                    foreach (var populationType in populationMethods)
                    {
                        var result = EmployeeDB.ResolveTaskEmailSetting(
                            "B",
                            employeeTaskOption,
                            populationType,
                            originatingCompanyTaskOption);

                        Assert.AreEqual(employeeTaskOption, result, "Expected " + employeeTaskOption + ", got " + result + " for " + employeeTaskOption + " " + populationType + " " + originatingCompanyTaskOption);
                    }
                }
            }
        }

        [Test]
        public void ResolveTaskEmailOption_PUserPopulateIndividual_UsesIndividualSettings()
        {
            var employeeTaskOptions = new[] { E_TaskRelatedEmailOptionT.DontReceiveEmail, E_TaskRelatedEmailOptionT.ReceiveEmail };
            var originatingCompanyTaskOptions = new[] { default(E_TaskRelatedEmailOptionT?), E_TaskRelatedEmailOptionT.DontReceiveEmail, E_TaskRelatedEmailOptionT.ReceiveEmail };

            employeeTaskOptions.Shuffle(this.random);
            originatingCompanyTaskOptions.Shuffle(this.random);

            foreach (var employeeTaskOption in employeeTaskOptions)
            {
                foreach (var originatingCompanyTaskOption in originatingCompanyTaskOptions)
                {
                    var result = EmployeeDB.ResolveTaskEmailSetting(
                            "P",
                            employeeTaskOption,
                            EmployeePopulationMethodT.IndividualUser,
                            originatingCompanyTaskOption);

                    Assert.AreEqual(employeeTaskOption, result, "Expected " + employeeTaskOption + ", got " + result + " for " + employeeTaskOption + " " + originatingCompanyTaskOption);
                }
            }
        }

        [Test]
        public void ResolveTaskEmailOption_PUserPopulateOriginatingCompany_UsesOriginatingCompanySettings()
        {
            var employeeTaskOptions = new[] { E_TaskRelatedEmailOptionT.DontReceiveEmail, E_TaskRelatedEmailOptionT.ReceiveEmail };
            var originatingCompanyTaskOptions = new[] { E_TaskRelatedEmailOptionT.DontReceiveEmail, E_TaskRelatedEmailOptionT.ReceiveEmail };

            employeeTaskOptions.Shuffle(this.random);
            originatingCompanyTaskOptions.Shuffle(this.random);

            foreach (var employeeTaskOption in employeeTaskOptions)
            {
                foreach (var originatingCompanyTaskOption in originatingCompanyTaskOptions)
                {
                    var result = EmployeeDB.ResolveTaskEmailSetting(
                            "P",
                            employeeTaskOption,
                            EmployeePopulationMethodT.OriginatingCompany,
                            originatingCompanyTaskOption);

                    Assert.AreEqual(originatingCompanyTaskOption, result, "Expected " + employeeTaskOption + ", got " + result + " for " + employeeTaskOption + originatingCompanyTaskOption);
                }
            }
        }

        [Test]
        public void ResolveTaskEmailOption_PUserPopulateOriginatingCompany_NoOcValueReturnsUserValue()
        {
            // This should be a rare case where we need to fall back to the user value.
            var result = EmployeeDB.ResolveTaskEmailSetting(
                "P",
                E_TaskRelatedEmailOptionT.ReceiveEmail,
                EmployeePopulationMethodT.OriginatingCompany,
                originatingCompanyTaskSetting: null);

            Assert.AreEqual(E_TaskRelatedEmailOptionT.ReceiveEmail, result);

            result = EmployeeDB.ResolveTaskEmailSetting(
                "P",
                E_TaskRelatedEmailOptionT.DontReceiveEmail,
                EmployeePopulationMethodT.OriginatingCompany,
                originatingCompanyTaskSetting: null);

            Assert.AreEqual(E_TaskRelatedEmailOptionT.DontReceiveEmail, result);
        }
        #endregion

        private class TestEmployeeDB : EmployeeDB
        {
            public PmlBroker originatingCompany;
            public BranchDB branch;

            public TestEmployeeDB()
            {
                this.UserType = 'B';
            }

            protected override PmlBroker PmlBroker
            {
                get { return originatingCompany; }
            }

            protected override BranchDB BranchDB
            {
                get { return this.branch; }
            }

            public override char UserType { get; set; }
        }
    }
}
