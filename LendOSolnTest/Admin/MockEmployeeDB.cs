﻿// <copyright file="MockEmployeeDB.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: geoffreyf
//    Date:   10/6/2014 9:49:27 AM 
// </summary>
namespace LendOSolnTest.Admin
{
    using System;
    using LendersOffice.Admin;
    using LendersOfficeApp.los.admin;

    /// <summary>
    /// A mock for the EmployeeDB class. Not strictly mocked.
    /// </summary>
    public class MockEmployeeDB : EmployeeDB
    {
        public static readonly Guid OCBrokerID = new Guid("11111111-1111-1111-1111-111111111111");
        public static readonly Guid OCWholesaleBranchID = new Guid("22222222-2222-2222-2222-222222222222");
        public static readonly Guid OCWholesalePriceGroupID = new Guid("33333333-3333-3333-3333-333333333333");
        public static readonly Guid OCMiniCorrespondentBranchID = new Guid("44444444-4444-4444-4444-444444444444");
        public static readonly Guid OCCorrespondentBranchID = new Guid("55555555-5555-5555-5555-555555555555");
        public static readonly Guid OCMiniCorrespondentPriceGroupID = new Guid("66666666-6666-6666-6666-666666666666");
        public static readonly Guid OCCorrespondentPriceGroupID = new Guid("77777777-7777-7777-7777-777777777777");

        // When set to true, the Pml Broker of this mock will not have
        // any of its branch or price groups set and will use a custom
        // corporate broker, which is handy for testing to force the use
        // the TPO portal defaults for branches and price groups.
        private bool useCustomCorporateBroker = false;

        private MockPmlBroker mockPmlBroker;

        public void SetUseCustomCorporateBroker(MockBrokerDb corporateBroker)
        {
            this.useCustomCorporateBroker = true;

            this.mockPmlBroker = MockPmlBroker.CreateCorporateDefaultInstance(corporateBroker);
        }

        protected override PmlBroker PmlBroker
        {
            get
            {
                if (this.useCustomCorporateBroker)
                {
                    return this.mockPmlBroker;
                }

                var originatingCompany = MockPmlBroker.CreateInstance(OCBrokerID);

                originatingCompany.BranchId = OCWholesaleBranchID;
                originatingCompany.LpePriceGroupId = OCWholesalePriceGroupID;
                originatingCompany.MiniCorrespondentBranchId = OCMiniCorrespondentBranchID;
                originatingCompany.CorrespondentBranchId = OCCorrespondentBranchID;
                originatingCompany.MiniCorrespondentLpePriceGroupId = OCMiniCorrespondentPriceGroupID;
                originatingCompany.CorrespondentLpePriceGroupId = OCCorrespondentPriceGroupID;
                return originatingCompany;
            }
        }

        public string GetRoles()
        {
            return this.m_roleSet;
        }
    }
}
