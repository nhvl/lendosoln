﻿// <copyright file="EmployeeRelationshipsTest.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: geoffreyf
//    Date:   10/6/2014 2:30:41 PM 
// </summary>
namespace LendOSolnTest.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using LendersOffice.Admin;
    using LendersOffice.ObjLib.Relationships;
    using LendersOffice.Security;
    using NUnit.Framework;

    /// <summary>
    /// Tests the EmployeeRelationships, RelationshipSet, and Relationship classes.
    /// </summary>
    [TestFixture]
    public class EmployeeRelationshipsTest
    {
        public static readonly Guid testUnderwriterEmployeeID = new Guid("11111111-1111-1111-1111-111111111111");
        public static readonly Guid testJuniorUnderwriterEmployeeID = new Guid("22222222-2222-2222-2222-222222222222");
        public static readonly Guid testProcessorEmployeeID = new Guid("33333333-3333-3333-3333-333333333333");
        public static readonly Guid testAccountExecEmployeeID = new Guid("44444444-4444-4444-4444-444444444444");

        private List<RelationshipInfo> additionTestRelationships = new List<RelationshipInfo>()
        {
            new RelationshipInfo(E_RoleT.Underwriter, E_RelationshipType.TeamHelper, testUnderwriterEmployeeID),
            new RelationshipInfo(E_RoleT.JuniorUnderwriter, E_RelationshipType.TeamHelper, testJuniorUnderwriterEmployeeID),
            new RelationshipInfo(E_RoleT.Processor, E_RelationshipType.TeamMember, testProcessorEmployeeID),
            new RelationshipInfo(E_RoleT.LenderAccountExecutive, E_RelationshipType.TeamMember, testAccountExecEmployeeID)
        };

        [Test]
        public void TestAdd()
        {
            var relationshipSet = new RelationshipSet();

            foreach (var relationshipInfo in this.additionTestRelationships)
            {
                relationshipSet.Add(
                    relationshipInfo.Role,
                    relationshipInfo.Type,
                    relationshipInfo.EmployeeID);

                Assert.That(
                    relationshipSet.Contains(relationshipInfo.Role),
                    "It should contain an " + relationshipInfo.Role + " relationship.");

                var addedRelationship = relationshipSet[relationshipInfo.Role];

                Assert.That(
                    addedRelationship.Type == relationshipInfo.Type,
                    "The relationship type should match.");

                Assert.That(
                    addedRelationship.EmployeeId == relationshipInfo.EmployeeID,
                    "The employee id should match.");
            }

        }

        private struct RelationshipInfo
        {
            public readonly E_RoleT Role;
            public readonly E_RelationshipType Type;
            public readonly Guid EmployeeID;

            public RelationshipInfo(
                E_RoleT role,
                E_RelationshipType type,
                Guid employeeID)
            {
                this.Role = role;
                this.Type = type;
                this.EmployeeID = employeeID;
            }

        }
    }
}
