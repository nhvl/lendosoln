﻿namespace LendOSolnTest.Admin
{
    using System;
    using System.Data.SqlClient;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Constants;
    using LendersOffice.Security;
    using LendOSolnTest.Common;
    using NUnit.Framework;

    [TestFixture]
    public class PmlBrokerTest
    {
        private const string NamePrefix = "PMLBROKERTEST_";
        private const string DefaultBrokerOriginatingCompanyGroupId = "TEST GROUP";
        private const string TaxId1 = "12-3456789";
        private const string TaxId2 = "98-7654321";

        private readonly Guid PmlBrokerExistingId = new Guid("11111111-1111-1111-1111-111111111111");
        private readonly Guid PrimaryDefaultBranchId = new Guid("22222222-2222-2222-2222-222222222222");
        private readonly Guid SecondaryDefaultBranchId = new Guid("33333333-3333-3333-3333-333333333333");
        private readonly Guid PrimaryDefaultPriceGroupId = new Guid("44444444-4444-4444-4444-444444444444");
        private readonly Guid SecondaryDefaultPriceGroupId = new Guid("55555555-5555-5555-5555-555555555555");

        private Guid? DefaultPriceGroupIdFromBroker;
        private Guid? DefaultBranchIdFromBroker;
        private AbstractUserPrincipal principal;
        private PmlBroker pmlBroker;
        private MockPmlBroker mockPmlBroker;
        private BrokerDB broker;

        #region Test Environment Setup
        [TestFixtureSetUp]
        public void Setup()
        {
            this.principal = LoginTools.LoginAs(TestAccountType.LoanOfficer);

            this.pmlBroker = PmlBroker.CreatePmlBroker(this.principal.BrokerId);

            this.pmlBroker.Name = PmlBrokerTest.NamePrefix + "TEST001";

            this.mockPmlBroker = MockPmlBroker.CreateInstance(this.principal.BrokerId);

            this.broker = this.mockPmlBroker.Broker;

            if (ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                // Because PriceGroup.RetrieveByID will throw a NotFoundException if we use 
                // a dummy default, we'll use an actual price group value if one exists.
                var brokerPriceGroups = this.principal.BrokerDB.GetPriceGroups();

                if (brokerPriceGroups.Any())
                {
                    this.DefaultPriceGroupIdFromBroker = brokerPriceGroups.First().Key;
                }

                var brokerBranches = this.principal.BrokerDB.GetBranches();

                if (brokerBranches.Any())
                {
                    this.DefaultBranchIdFromBroker = brokerBranches.First().Key;
                }
            }
        }

        [SetUp]
        public void Init()
        {
            this.mockPmlBroker.BranchId = null;
            this.mockPmlBroker.MiniCorrespondentBranchId = null;
            this.mockPmlBroker.CorrespondentBranchId = null;
            this.mockPmlBroker.LpePriceGroupId = null;
            this.mockPmlBroker.MiniCorrespondentLpePriceGroupId = null;
            this.mockPmlBroker.CorrespondentLpePriceGroupId = null;
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
            var toBeDeleteList = PmlBroker.ListAllPmlBrokerByBrokerId(this.principal.BrokerId).Where(o => o.Name.StartsWith(PmlBrokerTest.NamePrefix));
            foreach (var o in toBeDeleteList)
            {
                SqlParameter[] parameters = {
                                                new SqlParameter("@PmlBrokerId", o.PmlBrokerId)
                                                , new SqlParameter("@BrokerId", o.BrokerId)
                                            };
                StoredProcedureHelper.ExecuteNonQuery(this.principal.BrokerId, "INTERNAL_DeletePmlBroker", 3, parameters);
            }
        }
        #endregion

        [Test]
        public void MainTest()
        {
            Guid brokerId = this.principal.BrokerId;
            Guid pmlBrokerId = Guid.Empty;
            #region Create new Pml BrokerDB
            // Test Creation.
            Assert.AreNotEqual(Guid.Empty, this.pmlBroker.PmlBrokerId, "When create new pml broker, PmlBrokerId cannot be Guid.Empty");
            Assert.AreEqual(this.principal.BrokerId, this.pmlBroker.BrokerId, "When create new pml broker, BrokerId must be equal with parameters pass in.");

            this.pmlBroker.IsActive = true;
            this.pmlBroker.Addr = "ADDR";
            this.pmlBroker.City = "CITY";
            this.pmlBroker.State = "CA";
            this.pmlBroker.Zip = "92805";
            this.pmlBroker.Phone = "(111) 111-1111";
            this.pmlBroker.Fax = "(222) 222-2222";
            this.pmlBroker.LicenseInfoList.Add(new LicenseInfo() { License = "LICENSE #0", State = "CA", ExpD = "1/1/2009" });
            this.pmlBroker.LicenseInfoList.Add(new LicenseInfo() { License = "LICENSE #1", State = "NY", ExpD = "2/2/2009" });
            this.pmlBroker.LicenseInfoList.Add(new LicenseInfo() { License = "LICENSE #2", State = "AZ", ExpD = "3/3/2009" });
            string[] states = { "CA", "NY", "AZ" };

            this.pmlBroker.CompanyId = "COMPANYID";
            this.pmlBroker.TaxId = "123456789";
            this.pmlBroker.PrincipalName1 = "PRINCIPAL_NAME_1";
            this.pmlBroker.PrincipalName2 = "PRINCIPAL_NAME_2";
            this.pmlBroker.Roles.Add(E_OCRoles.Broker);
            this.pmlBroker.BrokerRoleStatusT = OCStatusType.Approved;
            this.pmlBroker.GroupIdList = PmlBrokerTest.DefaultBrokerOriginatingCompanyGroupId;
            this.pmlBroker.LpePriceGroupId = this.DefaultPriceGroupIdFromBroker;
            this.pmlBroker.MiniCorrespondentLpePriceGroupId = this.DefaultPriceGroupIdFromBroker;
            this.pmlBroker.CorrespondentLpePriceGroupId = this.DefaultPriceGroupIdFromBroker;
            this.pmlBroker.BranchId = this.DefaultBranchIdFromBroker;
            this.pmlBroker.MiniCorrespondentBranchId = this.DefaultBranchIdFromBroker;
            this.pmlBroker.CorrespondentBranchId = this.DefaultBranchIdFromBroker;
            this.pmlBroker.Save();
            pmlBrokerId = this.pmlBroker.PmlBrokerId;
            #endregion
            // Test Retrieve
            #region Retrieve PmlBroker and Verify
            this.pmlBroker = null;

            this.pmlBroker = PmlBroker.RetrievePmlBrokerById(pmlBrokerId, brokerId);
            Assert.AreEqual(brokerId, this.pmlBroker.BrokerId, "BrokerId");
            Assert.AreEqual(pmlBrokerId, this.pmlBroker.PmlBrokerId, "PmlBrokerId");
            Assert.AreEqual(PmlBrokerTest.NamePrefix + "TEST001", this.pmlBroker.Name, "Name");
            Assert.AreEqual(true, this.pmlBroker.IsActive, "IsActive");
            Assert.AreEqual("ADDR", this.pmlBroker.Addr, "Addr");
            Assert.AreEqual("CITY", this.pmlBroker.City, "City");
            Assert.AreEqual("CA", this.pmlBroker.State, "State");
            Assert.AreEqual("92805", this.pmlBroker.Zip, "Zip");
            Assert.AreEqual("(111) 111-1111", this.pmlBroker.Phone, "Phone");
            Assert.AreEqual("(222) 222-2222", this.pmlBroker.Fax, "Fax");
            Assert.AreEqual("COMPANYID", this.pmlBroker.CompanyId, "CompanyId");
            Assert.AreEqual("12-3456789", this.pmlBroker.TaxId, "TaxId");
            Assert.AreEqual("PRINCIPAL_NAME_1", this.pmlBroker.PrincipalName1, "PrincipalName1");
            Assert.AreEqual("PRINCIPAL_NAME_2", this.pmlBroker.PrincipalName2, "PrincipalName2");

            Assert.AreEqual(3, this.pmlBroker.LicenseInfoList.Count, "LicenseInfoListCount");

            for (int i = 0; i < this.pmlBroker.LicenseInfoList.Count; i++)
            {
                LicenseInfo license = (LicenseInfo)this.pmlBroker.LicenseInfoList.List[i];
                Assert.AreEqual("LICENSE #" + i, license.License, "License");
                Assert.AreEqual(states[i], license.State, "State");
                Assert.AreEqual((i+1) + "/" + (i+1) + "/2009", license.ExpD, "ExpD");
            }
            #endregion

            #region Edit PmlBroker and Verify
            // Modify this pml broker.
            this.pmlBroker.Name = PmlBrokerTest.NamePrefix + "TEST001_EDIT";
            this.pmlBroker.IsActive = false;
            this.pmlBroker.Addr = "ADDR_EDIT";
            this.pmlBroker.City = "CITY_EDIT";
            this.pmlBroker.State = "AZ";
            this.pmlBroker.Zip = "92806";
            this.pmlBroker.Phone = "(111) 111-3333";
            this.pmlBroker.Fax = "(222) 222-4444";
            this.pmlBroker.LicenseInfoList.Clear();
            this.pmlBroker.CompanyId = "COMPANYID_EDIT";
            this.pmlBroker.TaxId = "012345678";
            this.pmlBroker.PrincipalName1 = "PRINCIPAL_NAME_1_EDIT";
            this.pmlBroker.PrincipalName2 = "PRINCIPAL_NAME_2_EDIT";
            this.pmlBroker.GroupIdList = PmlBrokerTest.DefaultBrokerOriginatingCompanyGroupId;
            this.pmlBroker.Save();

            this.pmlBroker = null;

            this.pmlBroker = PmlBroker.RetrievePmlBrokerById(pmlBrokerId, brokerId);
            Assert.AreEqual(brokerId, this.pmlBroker.BrokerId, "BrokerId");
            Assert.AreEqual(pmlBrokerId, this.pmlBroker.PmlBrokerId, "PmlBrokerId");
            Assert.AreEqual(PmlBrokerTest.NamePrefix + "TEST001_EDIT", this.pmlBroker.Name, "Name");
            Assert.AreEqual(false, this.pmlBroker.IsActive, "IsActive");
            Assert.AreEqual("ADDR_EDIT", this.pmlBroker.Addr, "Addr");
            Assert.AreEqual("CITY_EDIT", this.pmlBroker.City, "City");
            Assert.AreEqual("AZ", this.pmlBroker.State, "State");
            Assert.AreEqual("92806", this.pmlBroker.Zip, "Zip");
            Assert.AreEqual("(111) 111-3333", this.pmlBroker.Phone, "Phone");
            Assert.AreEqual("(222) 222-4444", this.pmlBroker.Fax, "Fax");
            Assert.AreEqual("COMPANYID_EDIT", this.pmlBroker.CompanyId, "CompanyId");
            Assert.AreEqual("01-2345678", this.pmlBroker.TaxId, "TaxId");
            Assert.AreEqual("PRINCIPAL_NAME_1_EDIT", this.pmlBroker.PrincipalName1, "PrincipalName1");
            Assert.AreEqual("PRINCIPAL_NAME_2_EDIT", this.pmlBroker.PrincipalName2, "PrincipalName2");
            Assert.AreEqual(0, this.pmlBroker.LicenseInfoList.Count, "LicenseInfoListCount");
            #endregion
        }

        [Test]
        public void ListTest()
        {
            string randomNamePrefix = PmlBrokerTest.NamePrefix + Guid.NewGuid().ToString();

            int count = 10;
            for (int i = 0; i < count; i++)
            {
                PmlBroker pmlBroker = PmlBroker.CreatePmlBroker(this.principal.BrokerId);
                pmlBroker.Name = randomNamePrefix + "_" + i;
                pmlBroker.IsActive = i % 2 == 0;
                pmlBroker.Addr = "ADDR";
                pmlBroker.City = "CITY";
                pmlBroker.State = "CA";
                pmlBroker.Zip = "92805";
                pmlBroker.Phone = "(111) 111-1111";
                pmlBroker.Fax = "(222) 222-2222";
                pmlBroker.LicenseInfoList.Add(new LicenseInfo() { License = "LICENSE #0", State = "CA", ExpD = "1/1/2009" });
                pmlBroker.LicenseInfoList.Add(new LicenseInfo() { License = "LICENSE #1", State = "NY", ExpD = "2/2/2009" });
                pmlBroker.LicenseInfoList.Add(new LicenseInfo() { License = "LICENSE #2", State = "AZ", ExpD = "3/3/2009" });

                pmlBroker.CompanyId = "COMPANYID";
                pmlBroker.TaxId = "012345678";
                pmlBroker.PrincipalName1 = "PRINCIPAL_NAME_1";
                pmlBroker.PrincipalName2 = "PRINCIPAL_NAME_2";
                pmlBroker.Roles.Add(E_OCRoles.Broker);
                pmlBroker.BrokerRoleStatusT = OCStatusType.Approved;
                pmlBroker.GroupIdList = PmlBrokerTest.DefaultBrokerOriginatingCompanyGroupId;
                pmlBroker.LpePriceGroupId = DefaultPriceGroupIdFromBroker;
                pmlBroker.MiniCorrespondentLpePriceGroupId = DefaultPriceGroupIdFromBroker;
                pmlBroker.CorrespondentLpePriceGroupId = DefaultPriceGroupIdFromBroker;
                pmlBroker.BranchId = DefaultBranchIdFromBroker;
                pmlBroker.MiniCorrespondentBranchId = DefaultBranchIdFromBroker;
                pmlBroker.CorrespondentBranchId = DefaultBranchIdFromBroker;
                pmlBroker.Save();
            }

            var list = PmlBroker.ListAllPmlBrokerByBrokerId(this.principal.BrokerId);

            Assert.That(list.Count >= count);
        }

        [ExpectedException(typeof(PmlBrokerNotFoundException))]
        [Test]
        public void RetrieveInvalidTest()
        {
            PmlBroker.RetrievePmlBrokerById(Guid.Empty, this.principal.BrokerId);
        }

        [Test]
        public void UpdateTaxId_UpdatesValue()
        {
            var broker = PmlBroker.CreatePmlBroker(this.principal.BrokerId);

            #region Setup test broker
            broker.Name = PmlBrokerTest.NamePrefix + "TaxIdTest";
            broker.IsActive = true;
            broker.Addr = "ADDR";
            broker.City = "CITY";
            broker.State = "CA";
            broker.Zip = "92805";
            broker.Phone = "(111) 111-1111";
            broker.Fax = "(222) 222-2222";
            broker.LicenseInfoList.Add(new LicenseInfo() { License = "LICENSE #0", State = "CA", ExpD = "1/1/2009" });
            broker.LicenseInfoList.Add(new LicenseInfo() { License = "LICENSE #1", State = "NY", ExpD = "2/2/2009" });
            broker.LicenseInfoList.Add(new LicenseInfo() { License = "LICENSE #2", State = "AZ", ExpD = "3/3/2009" });
            string[] states = { "CA", "NY", "AZ" };

            broker.CompanyId = "COMPANYID";
            broker.PrincipalName1 = "PRINCIPAL_NAME_1";
            broker.PrincipalName2 = "PRINCIPAL_NAME_2";
            broker.Roles.Add(E_OCRoles.Broker);
            broker.BrokerRoleStatusT = OCStatusType.Approved;
            broker.GroupIdList = PmlBrokerTest.DefaultBrokerOriginatingCompanyGroupId;
            broker.LpePriceGroupId = this.DefaultPriceGroupIdFromBroker;
            broker.MiniCorrespondentLpePriceGroupId = this.DefaultPriceGroupIdFromBroker;
            broker.CorrespondentLpePriceGroupId = this.DefaultPriceGroupIdFromBroker;
            broker.BranchId = this.DefaultBranchIdFromBroker;
            broker.MiniCorrespondentBranchId = this.DefaultBranchIdFromBroker;
            broker.CorrespondentBranchId = this.DefaultBranchIdFromBroker;
            #endregion

            broker.TaxId = TaxId1;

            broker.Save();

            broker = PmlBroker.RetrievePmlBrokerById(broker.PmlBrokerId, this.principal.BrokerId);

            Assert.AreEqual(TaxId1, broker.TaxId);
            Assert.IsTrue(broker.TaxIdValid);

            broker.TaxId = TaxId2;

            broker.Save();

            broker = PmlBroker.RetrievePmlBrokerById(broker.PmlBrokerId, this.principal.BrokerId);

            Assert.AreEqual(TaxId2, broker.TaxId);
            Assert.IsTrue(broker.TaxIdValid);
        }

        #region Default Branch Tests
        [Test]
        public void TestWholesaleDefaultBranchNotUsedIfPmlBrokerValueSet()
        {
            if (!ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                Assert.Ignore("ConstStage.EnableDefaultBranchesAndPriceGroups is false.");

                return;
            }

            this.broker.UpdateDefaultBranchPriceGroupValues(
                null,
                this.PrimaryDefaultBranchId,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty);

            this.mockPmlBroker.BranchId = this.PmlBrokerExistingId;

            Assert.AreNotEqual(
                this.mockPmlBroker.BranchId,
                this.broker.DefaultWholesaleBranchId,
                "When the BranchId of a PML broker has been set, PmlBroker.BranchId " +
                "should not use the TPO portal-level default.");
        }
        
        [Test]
        public void TestUseLenderLevelWholesaleDefaultBranch()
        {
            if (!ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                Assert.Ignore("ConstStage.EnableDefaultBranchesAndPriceGroups is false.");

                return;
            }

            this.broker.UpdateDefaultBranchPriceGroupValues(
                null,
                this.PrimaryDefaultBranchId,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty);

            this.mockPmlBroker.UseDefaultWholesaleBranchId = true;

            Assert.AreEqual(
                this.broker.DefaultWholesaleBranchId,
                this.mockPmlBroker.BranchId,
                "When the BranchId of a PML broker has not been set but a TPO portal-level " +
                "wholesale default exists, PmlBroker.BranchId should be the TPO portal-level default.");
        }

        [Test]
        public void TestGetPmlBrokerWholesaleBranchNameIfDefault()
        {
            if (!ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                Assert.Ignore("ConstStage.EnableDefaultBranchesAndPriceGroups is false.");

                return;
            }

            Assert.AreEqual(
                string.Empty,
                this.mockPmlBroker.BranchNm,
                "When the BranchId of a PML broker has not been set and no TPO " +
                "portal-level wholesale default exists, the branch name of the " +
                "PML broker should be the empty string.");

            this.broker.UpdateDefaultBranchPriceGroupValues(
                null,
                this.PrimaryDefaultBranchId,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty);

            this.mockPmlBroker.UseDefaultWholesaleBranchId = true;

            Assert.AreNotEqual(
                string.Empty,
                this.mockPmlBroker.BranchNm,
                "When the BranchID of a PML broker has not been set but a TPO portal-level " +
                "wholesale default exists, the branch name of the broker should not be the " +
                "empty string.");
        }

        [Test]
        public void TestPmlBrokerUpdatesDefaultWholesaleBranchWhenDefaultChanged()
        {
            if (!ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                Assert.Ignore("ConstStage.EnableDefaultBranchesAndPriceGroups is false.");

                return;
            }

            this.broker.UpdateDefaultBranchPriceGroupValues(
                null,
                this.PrimaryDefaultBranchId,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty);

            this.mockPmlBroker.UseDefaultWholesaleBranchId = true;

            Assert.AreEqual(
                this.broker.DefaultWholesaleBranchId,
                this.mockPmlBroker.BranchId,
                "When the BranchId of a PML broker has not been set but a TPO portal-level " +
                "wholesale default exists, PmlBroker.BranchId should be the TPO portal-level default.");

            this.broker.UpdateDefaultBranchPriceGroupValues(
                null,
                this.SecondaryDefaultBranchId,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty);

            Assert.AreEqual(
                this.broker.DefaultWholesaleBranchId,
                this.mockPmlBroker.BranchId,
                "When a PML broker is using the TPO portal-level wholesale branch default and that default changes, " +
                "the wholesale branch of the PML broker should also be updated.");
        }

        [Test]
        public void TestMiniCorrDefaultBranchNotUsedIfPmlBrokerValueSet()
        {
            if (!ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                Assert.Ignore("ConstStage.EnableDefaultBranchesAndPriceGroups is false.");

                return;
            }

            this.broker.UpdateDefaultBranchPriceGroupValues(
                null,
                Guid.Empty,
                Guid.Empty,
                this.PrimaryDefaultBranchId,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty);

            this.mockPmlBroker.MiniCorrespondentBranchId = this.PmlBrokerExistingId;

            Assert.AreNotEqual(
                this.mockPmlBroker.MiniCorrespondentBranchId,
                this.broker.DefaultMiniCorrBranchId,
                "When the Mini Correspondent BranchId of a PML broker has been set, " +
                "PmlBroker.MiniCorrespondentBranchId should not use the TPO portal-level default.");
        }

        [Test]
        public void TestUseLenderLevelMiniCorrDefaultBranch()
        {
            if (!ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                Assert.Ignore("ConstStage.EnableDefaultBranchesAndPriceGroups is false.");

                return;
            }

            this.broker.UpdateDefaultBranchPriceGroupValues(
                null,
                Guid.Empty,
                Guid.Empty,
                this.PrimaryDefaultBranchId,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty);

            this.mockPmlBroker.UseDefaultMiniCorrBranchId = true;

            Assert.AreEqual(
                this.broker.DefaultMiniCorrBranchId,
                this.mockPmlBroker.MiniCorrespondentBranchId,
                "When the Mini Correspondent BranchId of a PML broker has not been set but a TPO portal-level " +
                "Mini Correspondent default exists, PmlBroker.MiniCorrespondentBranchId should be the TPO portal-level default.");
        }

        [Test]
        public void TestGetPmlBrokerMiniCorrBranchNameIfDefault()
        {
            if (!ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                Assert.Ignore("ConstStage.EnableDefaultBranchesAndPriceGroups is false.");

                return;
            }

            Assert.AreEqual(
                string.Empty,
                this.mockPmlBroker.MiniCorrespondentBranchNm,
                "When the BranchId of a PML broker has not been set and no TPO " +
                "portal-level Mini Correspondent default exists, the branch name of the " +
                "PML broker should be the empty string.");

            this.broker.UpdateDefaultBranchPriceGroupValues(
                null,
                Guid.Empty,
                Guid.Empty,
                this.PrimaryDefaultBranchId,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty);

            this.mockPmlBroker.UseDefaultMiniCorrBranchId = true;

            Assert.AreNotEqual(
                string.Empty,
                this.mockPmlBroker.MiniCorrespondentBranchNm,
                "When the BranchID of a PML broker has not been set but a TPO portal-level " +
                "Mini Correspondent default exists, the branch name of the broker should not be the " +
                "emtpy string.");
        }

        [Test]
        public void TestPmlBrokerUpdatesDefaultMiniCorrBranchWhenDefaultChanged()
        {
            if (!ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                Assert.Ignore("ConstStage.EnableDefaultBranchesAndPriceGroups is false.");

                return;
            }

            this.broker.UpdateDefaultBranchPriceGroupValues(
                null,
                Guid.Empty,
                Guid.Empty,
                this.PrimaryDefaultBranchId,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty);

            this.mockPmlBroker.UseDefaultMiniCorrBranchId = true;

            Assert.AreEqual(
                this.broker.DefaultMiniCorrBranchId,
                this.mockPmlBroker.MiniCorrespondentBranchId,
                "When the mini corr BranchId of a PML broker has not been set but a TPO portal-level " +
                "wholesale default exists, PmlBroker.MiniCorrespondentBranchId should be the TPO portal-level default.");

            this.broker.UpdateDefaultBranchPriceGroupValues(
                null,
                Guid.Empty,
                Guid.Empty,
                this.SecondaryDefaultBranchId,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty);

            Assert.AreEqual(
                this.broker.DefaultMiniCorrBranchId,
                this.mockPmlBroker.MiniCorrespondentBranchId,
                "When a PML broker is using the TPO portal-level mini corr branch default and that default changes, " +
                "the mini corr branch of the PML broker should also be updated.");
        }

        [Test]
        public void TestCorrDefaultBranchNotUsedIfPmlBrokerValueSet()
        {
            if (!ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                Assert.Ignore("ConstStage.EnableDefaultBranchesAndPriceGroups is false.");

                return;
            }

            this.broker.UpdateDefaultBranchPriceGroupValues(
                null,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty,
                this.PrimaryDefaultBranchId,
                Guid.Empty);

            this.mockPmlBroker.CorrespondentBranchId = this.PmlBrokerExistingId;

            Assert.AreNotEqual(
                this.mockPmlBroker.CorrespondentBranchId,
                this.broker.DefaultCorrBranchId,
                "When the Correspondent BranchId of a PML broker has been set, " +
                "PmlBroker.CorrespondentBranchId should not use the TPO portal-level default.");
        }

        [Test]
        public void TestUseLenderLevelCorrDefaultBranch()
        {
            if (!ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                Assert.Ignore("ConstStage.EnableDefaultBranchesAndPriceGroups is false.");

                return;
            }

            this.broker.UpdateDefaultBranchPriceGroupValues(
                null,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty,
                this.PrimaryDefaultBranchId,
                Guid.Empty);

            this.mockPmlBroker.UseDefaultCorrBranchId = true;

            Assert.AreEqual(
                this.broker.DefaultCorrBranchId,
                this.mockPmlBroker.CorrespondentBranchId,
                "When the Correspondent BranchId of a PML broker has not been set but a TPO portal-level " +
                "Correspondent default exists, PmlBroker.CorrespondentBranchId should be the TPO portal-level default.");
        }

        [Test]
        public void TestGetPmlBrokerCorrBranchNameIfDefault()
        {
            if (!ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                Assert.Ignore("ConstStage.EnableDefaultBranchesAndPriceGroups is false.");

                return;
            }

            Assert.AreEqual(
                string.Empty,
                this.mockPmlBroker.CorrespondentBranchNm,
                "When the BranchId of a PML broker has not been set and no TPO " +
                "portal-level Correspondent default exists, the branch name of the " +
                "PML broker should be the empty string.");

            this.broker.UpdateDefaultBranchPriceGroupValues(
                null,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty,
                this.PrimaryDefaultBranchId,
                Guid.Empty);

            this.mockPmlBroker.UseDefaultCorrBranchId = true;

            Assert.AreNotEqual(
                string.Empty,
                this.mockPmlBroker.CorrespondentBranchNm,
                "When the BranchID of a PML broker has not been set but a TPO portal-level " +
                "Correspondent default exists, the branch name of the broker should not be the " +
                "empty string.");
        }

        [Test]
        public void TestPmlBrokerUpdatesDefaultCorrBranchWhenDefaultChanged()
        {
            if (!ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                Assert.Ignore("ConstStage.EnableDefaultBranchesAndPriceGroups is false.");

                return;
            }

            this.broker.UpdateDefaultBranchPriceGroupValues(
                null,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty,
                this.PrimaryDefaultBranchId,
                Guid.Empty);

            this.mockPmlBroker.UseDefaultCorrBranchId = true;

            Assert.AreEqual(
                this.broker.DefaultCorrBranchId,
                this.mockPmlBroker.CorrespondentBranchId,
                "When the corr BranchId of a PML broker has not been set but a TPO portal-level " +
                "corr default exists, PmlBroker.CorrespondentBranchId should be the TPO portal-level default.");

            this.broker.UpdateDefaultBranchPriceGroupValues(
                null,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty,
                this.SecondaryDefaultBranchId,
                Guid.Empty);

            Assert.AreEqual(
                this.broker.DefaultCorrBranchId,
                this.mockPmlBroker.CorrespondentBranchId,
                "When a PML broker is using the TPO portal-level corr branch default and that default changes, " +
                "the corr branch of the PML broker should also be updated.");
        }
        #endregion

        #region Default Price Group Tests
        [Test]
        public void TestWholesaleDefaultPriceGroupNotUsedIfPmlBrokerValueSet()
        {
            if (!ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                Assert.Ignore("ConstStage.EnableDefaultBranchesAndPriceGroups is false.");

                return;
            }

            this.broker.UpdateDefaultBranchPriceGroupValues(
                null,
                Guid.Empty,
                this.PrimaryDefaultPriceGroupId,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty);

            this.mockPmlBroker.LpePriceGroupId = this.PmlBrokerExistingId;
            
            Assert.AreNotEqual(
                this.mockPmlBroker.LpePriceGroupId,
                this.broker.DefaultWholesalePriceGroupId,
                "When the LpePriceGroupId of a PML broker has been set, PmlBroker.LpePriceGroupId " +
                "should not use the TPO portal-level default.");
        }

        [Test]
        public void TestUseLenderLevelWholesaleDefaultPriceGroup()
        {
            if (!ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                Assert.Ignore("ConstStage.EnableDefaultBranchesAndPriceGroups is false.");

                return;
            }

            this.broker.UpdateDefaultBranchPriceGroupValues(
                null,
                Guid.Empty,
                this.PrimaryDefaultPriceGroupId,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty);

            this.mockPmlBroker.UseDefaultWholesalePriceGroupId = true;

            Assert.AreEqual(
                this.broker.DefaultWholesalePriceGroupId,
                this.mockPmlBroker.LpePriceGroupId,
                "When the LpePriceGroupId of a PML broker has not been set but a TPO portal-level " +
                "wholesale default exists, PmlBroker.LpePriceGroupId should be the TPO portal-level default.");
        }

        [Test]
        public void TestGetPmlBrokerWholesalePriceGroupNameIfDefault()
        {
            if (!ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                Assert.Ignore("ConstStage.EnableDefaultBranchesAndPriceGroups is false.");

                return;
            }

            Assert.AreEqual(
                string.Empty,
                this.mockPmlBroker.LpePriceGroupName,
                "When the LpePriceGroupId of a PML broker has not been set and no TPO " +
                "portal-level wholesale default exists, the PriceGroup name of the " +
                "PML broker should be the empty string.");

            if (this.DefaultPriceGroupIdFromBroker.HasValue)
            {

                this.broker.UpdateDefaultBranchPriceGroupValues(
                    null,
                    Guid.Empty,
                    this.DefaultPriceGroupIdFromBroker.Value,
                    Guid.Empty,
                    Guid.Empty,
                    Guid.Empty,
                    Guid.Empty);

                this.mockPmlBroker.UseDefaultWholesalePriceGroupId = true;

                Assert.AreNotEqual(
                    string.Empty,
                    this.mockPmlBroker.LpePriceGroupName,
                    "When the LpePriceGroupId of a PML broker has not been set but a TPO portal-level " +
                    "wholesale default exists, the PriceGroup name of the broker should not be the " +
                    "empty string.");
            }
        }

        [Test]
        public void TestPmlBrokerUpdatesDefaultWholesalePriceGroupWhenDefaultChanged()
        {
            if (!ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                Assert.Ignore("ConstStage.EnableDefaultBranchesAndPriceGroups is false.");

                return;
            }

            this.broker.UpdateDefaultBranchPriceGroupValues(
                null,
                Guid.Empty,
                this.PrimaryDefaultPriceGroupId,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty);

            this.mockPmlBroker.UseDefaultWholesalePriceGroupId = true;

            Assert.AreEqual(
                this.broker.DefaultWholesalePriceGroupId,
                this.mockPmlBroker.LpePriceGroupId,
                "When the LpePriceGroupId of a PML broker has not been set but a TPO portal-level " +
                "wholesale default exists, PmlBroker.LpePriceGroupId should be the TPO portal-level default.");

            this.broker.UpdateDefaultBranchPriceGroupValues(
                null,
                Guid.Empty,
                this.SecondaryDefaultPriceGroupId,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty);

            Assert.AreEqual(
                this.broker.DefaultWholesalePriceGroupId,
                this.mockPmlBroker.LpePriceGroupId,
                "When a PML broker is using the TPO portal-level wholesale price group default and that default changes, " +
                "the wholesale price group of the PML broker should also be updated.");
        }

        [Test]
        public void TestMiniCorrDefaultPriceGroupNotUsedIfPmlBrokerValueSet()
        {
            if (!ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                Assert.Ignore("ConstStage.EnableDefaultBranchesAndPriceGroups is false.");

                return;
            }

            this.broker.UpdateDefaultBranchPriceGroupValues(
                null,
                Guid.Empty,
                Guid.Empty,
                this.PrimaryDefaultPriceGroupId,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty);

            this.mockPmlBroker.MiniCorrespondentLpePriceGroupId = this.PmlBrokerExistingId;

            Assert.AreNotEqual(
                this.mockPmlBroker.MiniCorrespondentLpePriceGroupId,
                this.broker.DefaultMiniCorrPriceGroupId,
                "When the Mini Correspondent LpePriceGroupId of a PML broker has been set, " +
                "PmlBroker.MiniCorrespondentLpePriceGroupId should not use the TPO portal-level default.");
        }

        [Test]
        public void TestUseLenderLevelMiniCorrDefaultPriceGroup()
        {
            if (!ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                Assert.Ignore("ConstStage.EnableDefaultBranchesAndPriceGroups is false.");

                return;
            }

            this.broker.UpdateDefaultBranchPriceGroupValues(
                null,
                Guid.Empty,
                Guid.Empty,
                this.PrimaryDefaultPriceGroupId,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty);

            this.mockPmlBroker.UseDefaultMiniCorrPriceGroupId = true;

            Assert.AreEqual(
                this.broker.DefaultMiniCorrPriceGroupId,
                this.mockPmlBroker.MiniCorrespondentLpePriceGroupId,
                "When the Mini Correspondent LpePriceGroupId of a PML broker has not been set but a TPO portal-level " +
                "wholesale default exists, PmlBroker.MiniCorrespondentLpePriceGroupId should be the TPO portal-level default.");
        }

        [Test]
        public void TestGetPmlBrokerMiniCorrPriceGroupNameIfDefault()
        {
            if (!ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                Assert.Ignore("ConstStage.EnableDefaultBranchesAndPriceGroups is false.");

                return;
            }

            Assert.AreEqual(
                string.Empty,
                this.mockPmlBroker.MiniCorrespondentLpePriceGroupName,
                "When the LpePriceGroupId of a PML broker has not been set and no TPO " +
                "portal-level wholesale default exists, the PriceGroup name of the " +
                "PML broker should be the empty string.");

            if (this.DefaultPriceGroupIdFromBroker.HasValue)
            {

                this.broker.UpdateDefaultBranchPriceGroupValues(
                    null,
                    Guid.Empty,
                    Guid.Empty,
                    this.DefaultPriceGroupIdFromBroker.Value,
                    Guid.Empty,
                    Guid.Empty,
                    Guid.Empty);

                this.mockPmlBroker.UseDefaultMiniCorrPriceGroupId = true;

                Assert.AreNotEqual(
                    string.Empty,
                    this.mockPmlBroker.MiniCorrespondentLpePriceGroupName,
                    "When the Mini Correspondent LpePriceGroupName of a PML broker has not been set but a TPO portal-level " +
                    "wholesale default exists, the PriceGroup name of the broker should not be the empty string.");
            }
        }

        [Test]
        public void TestPmlBrokerUpdatesDefaultMiniCorrPriceGroupWhenDefaultChanged()
        {
            if (!ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                Assert.Ignore("ConstStage.EnableDefaultBranchesAndPriceGroups is false.");

                return;
            }

            this.broker.UpdateDefaultBranchPriceGroupValues(
                null,
                Guid.Empty,
                Guid.Empty,
                this.PrimaryDefaultPriceGroupId,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty);

            this.mockPmlBroker.UseDefaultMiniCorrPriceGroupId = true;

            Assert.AreEqual(
                this.broker.DefaultMiniCorrPriceGroupId,
                this.mockPmlBroker.MiniCorrespondentLpePriceGroupId,
                "When the mini corr LpePriceGroupId of a PML broker has not been set but a TPO portal-level " +
                "wholesale default exists, PmlBroker.MiniCorrespondentLpePriceGroupId should be the TPO portal-level default.");

            this.broker.UpdateDefaultBranchPriceGroupValues(
                null,
                Guid.Empty,
                Guid.Empty,
                this.SecondaryDefaultPriceGroupId,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty);

            Assert.AreEqual(
                this.broker.DefaultMiniCorrPriceGroupId,
                this.mockPmlBroker.MiniCorrespondentLpePriceGroupId,
                "When a PML broker is using the TPO portal-level mini corr price group default and that default changes, " +
                "the mini corr price group of the PML broker should also be updated.");
        }

        [Test]
        public void TestCorrDefaultPriceGroupNotUsedIfPmlBrokerValueSet()
        {
            if (!ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                Assert.Ignore("ConstStage.EnableDefaultBranchesAndPriceGroups is false.");

                return;
            }

            this.broker.UpdateDefaultBranchPriceGroupValues(
                null,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty,
                this.PrimaryDefaultPriceGroupId);

            this.mockPmlBroker.CorrespondentLpePriceGroupId = this.PmlBrokerExistingId;

            Assert.AreNotEqual(
                this.mockPmlBroker.MiniCorrespondentLpePriceGroupId,
                this.broker.DefaultCorrPriceGroupId,
                "When the Correspondent LpePriceGroupId of a PML broker has been set, " +
                "PmlBroker.CorrespondentLpePriceGroupId should not use the TPO portal-level default.");
        }

        [Test]
        public void TestUseLenderLevelCorrDefaultPriceGroup()
        {
            if (!ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                Assert.Ignore("ConstStage.EnableDefaultBranchesAndPriceGroups is false.");

                return;
            }

            this.broker.UpdateDefaultBranchPriceGroupValues(
                null,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty,
                this.PrimaryDefaultPriceGroupId);

            this.mockPmlBroker.UseDefaultCorrPriceGroupId = true;

            Assert.AreEqual(
                this.broker.DefaultCorrPriceGroupId,
                this.mockPmlBroker.CorrespondentLpePriceGroupId,
                "When the Correspondent LpePriceGroupId of a PML broker has not been set but a TPO portal-level " +
                "wholesale default exists, PmlBroker.CorrespondentLpePriceGroupId should be the TPO portal-level default.");
        }

        [Test]
        public void TestGetPmlBrokerCorrPriceGroupNameIfDefault()
        {
            if (!ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                Assert.Ignore("ConstStage.EnableDefaultBranchesAndPriceGroups is false.");

                return;
            }

            Assert.AreEqual(
                string.Empty,
                this.mockPmlBroker.CorrespondentLpePriceGroupName,
                "When the LpePriceGroupId of a PML broker has not been set and no TPO " +
                "portal-level wholesale default exists, the PriceGroup name of the " +
                "PML broker should be the empty string.");

            if (this.DefaultPriceGroupIdFromBroker.HasValue)
            {

                this.broker.UpdateDefaultBranchPriceGroupValues(
                    null,
                    Guid.Empty,
                    Guid.Empty,
                    Guid.Empty,
                    Guid.Empty,
                    Guid.Empty,
                    this.DefaultPriceGroupIdFromBroker.Value);

                this.mockPmlBroker.UseDefaultCorrPriceGroupId = true;

                Assert.AreNotEqual(
                    string.Empty,
                    this.mockPmlBroker.CorrespondentLpePriceGroupName,
                    "When the Mini Correspondent LpePriceGroupName of a PML broker has not been set but a TPO portal-level " +
                    "wholesale default exists, the PriceGroup name of the broker should not be the empty string.");
            }
        }

        [Test]
        public void TestPmlBrokerUpdatesDefaultCorrPriceGroupWhenDefaultChanged()
        {
            if (!ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                Assert.Ignore("ConstStage.EnableDefaultBranchesAndPriceGroups is false.");

                return;
            }

            this.broker.UpdateDefaultBranchPriceGroupValues(
                null,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty,
                this.PrimaryDefaultPriceGroupId);

            this.mockPmlBroker.UseDefaultCorrPriceGroupId = true;

            Assert.AreEqual(
                this.broker.DefaultCorrPriceGroupId,
                this.mockPmlBroker.CorrespondentLpePriceGroupId,
                "When the corr LpePriceGroupId of a PML broker has not been set but a TPO portal-level " +
                "wholesale default exists, PmlBroker.CorrespondentLpePriceGroupId should be the TPO portal-level default.");

            this.broker.UpdateDefaultBranchPriceGroupValues(
                null,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty,
                Guid.Empty,
                this.SecondaryDefaultPriceGroupId);

            Assert.AreEqual(
                this.broker.DefaultCorrPriceGroupId,
                this.mockPmlBroker.CorrespondentLpePriceGroupId,
                "When a PML broker is using the TPO portal-level corr price group default and that default changes, " +
                "the corr price group of the PML broker should also be updated.");
        }
    }
    #endregion
}
