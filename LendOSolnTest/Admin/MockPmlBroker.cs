﻿// <copyright file="MockPmlBroker.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Michael Leinweaver
//    Date:   9/23/2015
// </summary>
namespace LendOSolnTest.Admin
{
    using System;
    using LendersOffice.Admin;

    /// <summary>
    /// A mock for the PmlBroker class that uses the MockBrokerDB class
    /// for the MockPmlBroker.Broker property.
    /// </summary>
    public class MockPmlBroker : PmlBroker
    {
        private MockBrokerDb mockDb = new MockBrokerDb();

        protected MockPmlBroker() { }

        public static MockPmlBroker CreateInstance(Guid brokerId)
        {
            return new MockPmlBroker() { BrokerId = brokerId };
        }

        public static MockPmlBroker CreateCorporateDefaultInstance(MockBrokerDb mockBrokerDb)
        {
            var pmlBroker = new MockPmlBroker()
            {
                BrokerId = mockBrokerDb.BrokerID,
                mockDb = mockBrokerDb,
                UseDefaultWholesaleBranchId = true,
                UseDefaultWholesalePriceGroupId = true,
                UseDefaultMiniCorrBranchId = true,
                UseDefaultMiniCorrPriceGroupId = true,
                UseDefaultCorrBranchId = true,
                UseDefaultCorrPriceGroupId = true
            };

            return pmlBroker;
        }

        public override BrokerDB Broker
        {
            get { return mockDb; }
        }
    }
}
