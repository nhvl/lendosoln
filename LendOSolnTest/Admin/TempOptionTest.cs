﻿namespace LendOSolnTest.Admin
{
    using Common;
    using LendersOffice.Admin;
    using LendersOffice.ObjLib.Conversions;
    using LqbGrammar.DataTypes;
    using NSubstitute;
    using NUnit.Framework;

    /// <summary>
    /// Tests Broker Temp Options to verify they behave as expected with different TempOptionXmlContent values.
    /// </summary>
    [TestFixture]
    [Category(CategoryConstants.UnitTest)]
    public class TempOptionTest
    {
        /// <summary>
        /// A set of simple simple tests for creating a <see cref="UlddPhase3Settings"/> object with default, null, and empty strings for the xml content.
        /// </summary>
        [Test]
        public void UlddPhase3SettingsCreate_NullEmpty()
        {
            UlddPhase3Settings settings = UlddPhase3Settings.Create(default(string));
            Assert.IsNull(settings);

            settings = UlddPhase3Settings.Create(null);
            Assert.IsNull(settings);

            settings = UlddPhase3Settings.Create(string.Empty);
            AssertUlddPhase3SettingsAre(settings); // Assert default settings
        }

        /// <summary>
        /// A set of tests for creating a <see cref="UlddPhase3Settings"/> object with expected xml content examples.
        /// </summary>
        [Test]
        public void UlddPhase3SettingsCreate_Simple()
        {
            UlddPhase3Settings settings;

            // No part specified => Default settings
            settings = UlddPhase3Settings.Create("<option name=\"EnableULDDPhase3\" forGseTarget=\"\" part=\"\" value=\"true\" />");
            AssertUlddPhase3SettingsAre(settings);

            settings = UlddPhase3Settings.Create("<option name=\"EnableULDDPhase3\" forGseTarget=\"FannieMae\" part=\"\" value=\"true\" />");
            AssertUlddPhase3SettingsAre(settings);

            // Part specified, but no GSE target => set Fannie and Freddie.
            settings = UlddPhase3Settings.Create("<option name=\"EnableULDDPhase3\" forGseTarget=\"\" part=\"1\" value=\"true\" />");
            AssertUlddPhase3SettingsAre(settings, fannieRegular: UlddPhase3Part.Part1, freddieRegular: UlddPhase3Part.Part1);

            settings = UlddPhase3Settings.Create("<option name=\"EnableULDDPhase3\" forGseTarget=\"\" part=\"2\" value=\"true\" />");
            AssertUlddPhase3SettingsAre(settings, fannieRegular: UlddPhase3Part.Part2, freddieRegular: UlddPhase3Part.Part2);

            settings = UlddPhase3Settings.Create("<option name=\"EnableULDDPhase3ForTestFiles\" forGseTarget=\"\" part=\"2\" value=\"true\" />");
            AssertUlddPhase3SettingsAre(settings, fannieTest: UlddPhase3Part.Part2, freddieTest: UlddPhase3Part.Part2);

            // Specify GSE target
            settings = UlddPhase3Settings.Create("<option name=\"EnableULDDPhase3\" forGseTarget=\"FannieMae\" part=\"1\" value=\"true\" />");
            AssertUlddPhase3SettingsAre(settings, fannieRegular: UlddPhase3Part.Part1);

            settings = UlddPhase3Settings.Create("<option name=\"EnableULDDPhase3ForTestFiles\" forGseTarget=\"FreddieMac\" part=\"2\" value=\"true\" />");
            AssertUlddPhase3SettingsAre(settings, freddieTest: UlddPhase3Part.Part2);

            // Specify Multiple
            settings = UlddPhase3Settings.Create(
                "<option name=\"EnableULDDPhase3\" forGseTarget=\"FannieMae\" part=\"1\" value=\"true\" />\r\n" +
                "<option name=\"EnableULDDPhase3ForTestFiles\" forGseTarget=\"FannieMae\" part=\"1\" value=\"true\" />");
            AssertUlddPhase3SettingsAre(settings, fannieRegular: UlddPhase3Part.Part1, fannieTest: UlddPhase3Part.Part1);

            settings = UlddPhase3Settings.Create(
                "<option name=\"EnableULDDPhase3\" forGseTarget=\"FannieMae\" part=\"1\" value=\"true\" />\r\n" +
                "<option name=\"EnableULDDPhase3ForTestFiles\" forGseTarget=\"FannieMae\" part=\"2\" value=\"true\" />");
            AssertUlddPhase3SettingsAre(settings, fannieRegular: UlddPhase3Part.Part1, fannieTest: UlddPhase3Part.Part2);

            settings = UlddPhase3Settings.Create(
                "<option name=\"EnableULDDPhase3\" forGseTarget=\"\" part=\"1\" value=\"true\" />\r\n" +
                "<option name=\"EnableULDDPhase3\" forGseTarget=\"FannieMae\" part=\"2\" value=\"true\" />\r\n" +
                "<option name=\"EnableULDDPhase3ForTestFiles\" forGseTarget=\"\" part=\"\" value=\"true\" />");
            AssertUlddPhase3SettingsAre(settings, fannieRegular: UlddPhase3Part.Part2, freddieRegular: UlddPhase3Part.Part1);

            settings = UlddPhase3Settings.Create(
                "<option name=\"EnableULDDPhase3\" forGseTarget=\"\" part=\"1\" value=\"true\" />\r\n" +
                "<option name=\"EnableULDDPhase3\" forGseTarget=\"FannieMae\" part=\"2\" value=\"true\" />\r\n" +
                "<option name=\"EnableULDDPhase3ForTestFiles\" forGseTarget=\"\" part=\"2\" value=\"true\" />\r\n" +
                // Making sure to document this behavior - more specific value will override, even with empty part attribute.
                "<option name=\"EnableULDDPhase3ForTestFiles\" forGseTarget=\"FreddieMac\" part=\"\" value=\"true\" />");
            AssertUlddPhase3SettingsAre(settings, fannieRegular: UlddPhase3Part.Part2, freddieRegular: UlddPhase3Part.Part1, freddieTest: UlddPhase3Part.UsePhase2Instead, fannieTest: UlddPhase3Part.Part2);
        }

        /// <summary>
        /// A set of tests for creating a <see cref="UlddPhase3Settings"/> object with unexpected xml argument examples.
        /// </summary>
        [Test]
        public void UlddPhase3SettingsCreate_Unexpected()
        {
            UlddPhase3Settings settings;
            // Other GSE targets shouldn't be valid, but also shouldn't throw
            settings = UlddPhase3Settings.Create(
                "<option name=\"EnableULDDPhase3\" forGseTarget=\"GinnieMae\" part=\"1\" value=\"true\" />");
            AssertUlddPhase3SettingsAre(settings);

            settings = UlddPhase3Settings.Create(
                "<option name=\"EnableULDDPhase3\" forGseTarget=\"Other_None\" part=\"1\" value=\"true\" />");
            AssertUlddPhase3SettingsAre(settings);

            settings = UlddPhase3Settings.Create(
                "<option name=\"EnableULDDPhase3\" forGseTarget=\"somethingelse\" part=\"1\" value=\"true\" />");
            AssertUlddPhase3SettingsAre(settings);

            settings = UlddPhase3Settings.Create(
                "<option name=\"EnableULDDPhase3\" forGseTarget=\"18\" part=\"1\" value=\"true\" />");
            AssertUlddPhase3SettingsAre(settings);

            // Other part numbers shouldn't be valid, but also shouldn't throw
            settings = UlddPhase3Settings.Create(
                "<option name=\"EnableULDDPhase3\" forGseTarget=\"FreddieMac\" part=\"0\" value=\"true\" />");
            AssertUlddPhase3SettingsAre(settings);

            settings = UlddPhase3Settings.Create(
                "<option name=\"EnableULDDPhase3\" forGseTarget=\"FreddieMac\" part=\"400\" value=\"true\" />");
            AssertUlddPhase3SettingsAre(settings);

            settings = UlddPhase3Settings.Create(
                "<option name=\"EnableULDDPhase3\" forGseTarget=\"FreddieMac\" part=\"-1\" value=\"true\" />");
            AssertUlddPhase3SettingsAre(settings);

            settings = UlddPhase3Settings.Create(
                "<option name=\"EnableULDDPhase3\" forGseTarget=\"FreddieMac\" part=\"abcdefg\" value=\"true\" />");
            AssertUlddPhase3SettingsAre(settings);

            // Other option values should result in default settings
            settings = UlddPhase3Settings.Create(
                "<option name=\"EnableULDDPhase3\" forGseTarget=\"FreddieMac\" part=\"1\" value=\"false\" />");
            AssertUlddPhase3SettingsAre(settings);

            settings = UlddPhase3Settings.Create(
                "<option name=\"EnableULDDPhase3\" forGseTarget=\"FreddieMac\" part=\"1\" value=\"\" />");
            AssertUlddPhase3SettingsAre(settings);

            // Unfortunately, attributes omitted or out of order don't work.
            settings = UlddPhase3Settings.Create(
                "<option name=\"EnableULDDPhase3\" part=\"1\" value=\"true\" />");
            AssertUlddPhase3SettingsAre(settings);

            settings = UlddPhase3Settings.Create(
                "<option name=\"EnableULDDPhase3\" value=\"true\" forGseTarget=\"FreddieMac\" part=\"1\" />");
            AssertUlddPhase3SettingsAre(settings);
        }

        /// <summary>
        /// A set of tests for creating a <see cref="UlddPhase3Settings"/> object with fully populated temp option xml.
        /// </summary>
        [Test]
        public void UlddPhase3SettingsCreate_Realistic()
        {
            UlddPhase3Settings settings;
            settings = UlddPhase3Settings.Create(
                "<option name=\"AddUliTo1003Pages\" value=\"true\" />\r\n" +
                "<option name=\"EnableULDDPhase3\" forGseTarget=\"FannieMae\" part=\"2\" value=\"true\" />\r\n" +
                "<option name=\"TpoInitialDisclosureDocumentVendor\" vendorname=\"DocMagic - Stage\" packagenumber=\"Predisclosure\" enableedisclosure=\"Yes\" enableesign=\"Yes\" />\r\n" +
                "<option name=\"EnableULDDPhase3\"   forGseTarget=\"FreddieMac\" part=\"0\" value=\"true\" />\r\n\r\n" +
                "\r\n<option name=\"EnableNewFeeService\" value=\"true\" />\r\n<option name=\"ImportFirstAmericanBorrowerAndSellerFees\" />\r\n<option name=\"enabletotalscorecard\" value=\"true\" />\r\n" +
                "<option   name=\"EnableUlddphase3forTestFiles\" ForGseTarget=\"\" part=\"1\"  value=\"true\" />");
            AssertUlddPhase3SettingsAre(settings, fannieTest: UlddPhase3Part.Part1, fannieRegular: UlddPhase3Part.Part2, freddieTest: UlddPhase3Part.Part1, freddieRegular: UlddPhase3Part.UsePhase2Instead);
        }


        /// <summary>
        /// Asserts that the given <see cref="UlddPhase3Settings"/> is in the given state. By default, all queries should return <see cref="UlddPhase3Part.UsePhase2Instead"/>.
        /// </summary>
        /// <param name="settings">The settings object to check.</param>
        private static void AssertUlddPhase3SettingsAre(
            UlddPhase3Settings settings, 
            UlddPhase3Part fannieTest = UlddPhase3Part.UsePhase2Instead, 
            UlddPhase3Part fannieRegular = UlddPhase3Part.UsePhase2Instead,
            UlddPhase3Part freddieTest = UlddPhase3Part.UsePhase2Instead,
            UlddPhase3Part freddieRegular = UlddPhase3Part.UsePhase2Instead)
        {
            Assert.IsNotNull(settings);
            Assert.AreEqual(fannieTest, settings.GetPart(DataAccess.E_sGseDeliveryTargetT.FannieMae, true));
            Assert.AreEqual(fannieRegular, settings.GetPart(DataAccess.E_sGseDeliveryTargetT.FannieMae, false));
            Assert.AreEqual(freddieTest, settings.GetPart(DataAccess.E_sGseDeliveryTargetT.FreddieMac, true));
            Assert.AreEqual(freddieRegular, settings.GetPart(DataAccess.E_sGseDeliveryTargetT.FreddieMac, false));
            Assert.AreEqual(UlddPhase3Part.UsePhase2Instead, settings.GetPart(DataAccess.E_sGseDeliveryTargetT.GinnieMae, true));
            Assert.AreEqual(UlddPhase3Part.UsePhase2Instead, settings.GetPart(DataAccess.E_sGseDeliveryTargetT.GinnieMae, false));
            Assert.AreEqual(UlddPhase3Part.UsePhase2Instead, settings.GetPart(DataAccess.E_sGseDeliveryTargetT.Other_None, true));
            Assert.AreEqual(UlddPhase3Part.UsePhase2Instead, settings.GetPart(DataAccess.E_sGseDeliveryTargetT.Other_None, false));
        }
    }
}
