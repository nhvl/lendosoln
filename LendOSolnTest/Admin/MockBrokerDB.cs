﻿// <copyright file="MockBrokerDb.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Michael Leinweaver
//    Date:   9/23/2015
// </summary>
namespace LendOSolnTest.Admin
{
    using System;
    using DataAccess;
    using LendersOffice.Admin;

    /// <summary>
    /// A mock BrokerDB class that allows us to set the default branch and
    /// price group values to null for the purposes of resetting inbetween 
    /// tests.
    /// </summary>
    public class MockBrokerDb : BrokerDB
    {
        public bool IgnoreEmpty { get; set; }

        public MockBrokerDb(bool ignoreEmpty = true)
        {
            this.IgnoreEmpty = ignoreEmpty;
        }

        public override void UpdateDefaultBranchPriceGroupValues(
            CStoredProcedureExec executor, 
            Guid newDefaultWholesaleBranchId, 
            Guid newDefaultWholesalePriceGroupId, 
            Guid newDefaultMiniCorrBranchId, 
            Guid newDefaultMiniCorrPriceGroupId, 
            Guid newDefaultCorrBranchId, 
            Guid newDefaultCorrPriceGroupId)
        {
            if (this.IgnoreEmpty || this.IsValidNewDefaultValue("DefaultWholesaleBranchId", this.DefaultWholesaleBranchId, newDefaultWholesaleBranchId))
            {
                this.defaultWholesaleBranchId = newDefaultWholesaleBranchId;
            }

            if (this.IgnoreEmpty || this.IsValidNewDefaultValue("DefaultWholesalePriceGroupId", this.DefaultWholesalePriceGroupId, newDefaultWholesalePriceGroupId))
            {
                this.defaultWholesalePriceGroupId = newDefaultWholesalePriceGroupId;
            }

            if (this.IgnoreEmpty || this.IsValidNewDefaultValue("DefaultMiniCorrBranchId", this.DefaultMiniCorrBranchId, newDefaultMiniCorrBranchId))
            {
                this.defaultMiniCorrBranchId = newDefaultMiniCorrBranchId;
            }

            if (this.IgnoreEmpty || this.IsValidNewDefaultValue("DefaultMiniCorrPriceGroupId", this.DefaultMiniCorrPriceGroupId, newDefaultMiniCorrPriceGroupId))
            {
                this.defaultMiniCorrPriceGroupId = newDefaultMiniCorrPriceGroupId;
            }

            if (this.IgnoreEmpty || this.IsValidNewDefaultValue("DefaultCorrBranchId", this.DefaultCorrBranchId, newDefaultCorrBranchId))
            {
                this.defaultCorrBranchId = newDefaultCorrBranchId;
            }

            if (this.IgnoreEmpty || this.IsValidNewDefaultValue("DefaultCorrPriceGroupId", this.DefaultCorrPriceGroupId, newDefaultCorrPriceGroupId))
            {
                this.defaultCorrPriceGroupId = newDefaultCorrPriceGroupId;
            }
        }
    }
}
