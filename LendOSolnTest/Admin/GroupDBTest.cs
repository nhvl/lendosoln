﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using LendOSolnTest.Common;
using LendersOffice.Security;
using LendersOfficeApp.los.admin;
using LendersOffice.Admin;
using PriceMyLoan.Security;
using CommonLib;

namespace LendOSolnTest.Admin
{
    [TestFixture]
    public class GroupDBTest
    {
        Guid _employeeId = Guid.Empty;
        Guid _branchId = Guid.Empty;
        Guid _pmlBrokerId = Guid.Empty;
        Guid _brokerId = Guid.Empty;

        [TestFixtureSetUp]
        public void SetUp()
        {            
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.LoTest001);            
            _employeeId = principal.EmployeeId;
            _branchId = principal.BranchId;            
            _brokerId = principal.BrokerId;

            PriceMyLoanPrincipal pmlPrincipal = LoginTools.LoginAs(TestAccountType.Pml_LO1) as PriceMyLoanPrincipal;
            _pmlBrokerId = pmlPrincipal.PmlBrokerId;
        }

        [TearDown]
        public void FixtureTearDown()
        {
        }


        [Test]
        public void GroupsCRUDTest()
        {
            //Test Employee Group Creation 
            string sTestGroupName = "TestGroup" + Guid.NewGuid().ToString();
            
            GroupDB.CreateGroup(_brokerId, sTestGroupName, GroupType.Employee, "TestDescription");

            var groupList = GroupDB.GetAllGroups(_brokerId, GroupType.Employee);

            int count = 0;
            Guid newGroupId = Guid.Empty;
            foreach (var group in groupList)
            {
                if (group.GroupName == sTestGroupName && group.GroupType == GroupType.Employee)
                {
                    count++;
                    newGroupId = group.GroupId;
                }
            }
            Assert.AreNotEqual(0, count);

            //Test Delete group
            GroupDB.DeleteGroup(_brokerId, newGroupId);

            groupList = GroupDB.GetAllGroups(_brokerId, GroupType.Employee);
            count = 0;
            foreach (var group in groupList)
            {
                if (group.GroupName == sTestGroupName && group.GroupType == GroupType.Employee)
                {
                    count++;
                    newGroupId = group.GroupId;
                }
            }
            
            Assert.AreEqual(count, 0);
        }

        [Test]
        public void EmployeeGroupTest()
        {
            //Create Employee Group
            string sTestGroupName = "TestGroup" + Guid.NewGuid().ToString();
            Guid groupId = GroupDB.CreateGroup(_brokerId, sTestGroupName, GroupType.Employee, "TestDescription");            

            //Test Assignment of Employee to group
            GroupDB.UpdateGroupMembership(_brokerId, groupId, _employeeId.ToString());

            var groupList = GroupDB.GetEmployeesWithGroupMembership(_brokerId, groupId);

            int count = groupList.Count(o => o.EmployeeId == _employeeId && o.IsInGroup == 1);

            Assert.AreNotEqual(count, 0);

            //Clear out employees in the group
            GroupDB.UpdateGroupMembership(_brokerId, groupId, string.Empty);

            groupList = GroupDB.GetEmployeesWithGroupMembership(_brokerId, groupId);
            count = groupList.Count(o => o.EmployeeId == _employeeId && o.IsInGroup == 1);
            
            Assert.AreEqual(count, 0);

            //Delete group
            GroupDB.DeleteGroup(_brokerId, groupId);            
        }

        [Test]
        public void BranchGroupTest()
        {
            //Create Branch Group
            string sTestGroupName = "TestGroup" + Guid.NewGuid().ToString();
            Guid groupId = GroupDB.CreateGroup(_brokerId, sTestGroupName, GroupType.Branch, "TestDescription");

            //Test Assignment of Branch to group
            GroupDB.UpdateGroupMembership(_brokerId, groupId, _branchId.ToString());

            var groupList = GroupDB.GetBranchesWithGroupMembership(_brokerId, groupId);

            int count = groupList.Count(o => o.BranchId == _branchId && o.IsInGroup == 1);

            Assert.AreNotEqual(count, 0);

            //Clear out branches in the group
            GroupDB.UpdateGroupMembership(_brokerId, groupId, string.Empty);

            groupList = GroupDB.GetBranchesWithGroupMembership(_brokerId, groupId);

            count = groupList.Count(o => o.BranchId == _branchId && o.IsInGroup == 1);

            Assert.AreEqual(count, 0);

            //Delete group
            GroupDB.DeleteGroup(_brokerId, groupId);
        }


        [Test]
        public void PmlBrokerGroupTest()
        {
            //Create PmlBroker Group
            string sTestGroupName = "TestGroup" + Guid.NewGuid().ToString();
            Guid groupId = GroupDB.CreateGroup(_brokerId, sTestGroupName, GroupType.PmlBroker, "TestDescription");

            //Test Assignment of PmlBroker to group
            GroupDB.UpdateGroupMembership(_brokerId, groupId, _pmlBrokerId.ToString());

            var groupList = GroupDB.GetPmlBrokersWithGroupMembership(_brokerId, groupId);

            int count = groupList.Count(o => o.PmlBrokerId == _pmlBrokerId && o.IsInGroup == 1);

            Assert.AreNotEqual(count, 0);

            //Clear out pmlBrokers in the group
            GroupDB.UpdateGroupMembership(_brokerId, groupId, string.Empty);

            groupList = GroupDB.GetPmlBrokersWithGroupMembership(_brokerId, groupId);

            count = groupList.Count(o => o.PmlBrokerId == _pmlBrokerId && o.IsInGroup == 1);

            Assert.AreEqual(count, 0);

            //Delete group
            GroupDB.DeleteGroup(_brokerId, groupId);
        }
    }
}
