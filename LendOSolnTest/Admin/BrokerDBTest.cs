﻿namespace LendOSolnTest.Admin
{
    using System;
    using LendersOffice.Admin;
    using LendersOffice.Constants;
    using LendersOffice.Security;
    using LendersOfficeApp.los.admin;
    using LendOSolnTest.Common;
    using NUnit.Framework;

    [TestFixture]
    public class BrokerDBTest
    {        
        private readonly Guid defaultGuid = new Guid("11111111-1111-1111-1111-111111111111");

        private AbstractUserPrincipal principal;
        private MockBrokerDb brokerDb;

        [TestFixtureSetUp]
        public void Setup()
        {
            principal = LoginTools.LoginAs(TestAccountType.LoTest001);

            brokerDb = new MockBrokerDb(ignoreEmpty: false);
        }

        [Test]
        public void LendingLicenseTest()
        {
            LicenseInfoList licenseInfoList = new LicenseInfoList();
            licenseInfoList.Add(new LicenseInfo() { State = "CA", License = "12345", ExpD = "1/1/2009" });
            licenseInfoList.Add(new LicenseInfo() { State = "AZ", License = "2468", ExpD = "2/1/2009" });
            licenseInfoList.Add(new LicenseInfo() { State = "TX", License = "3690", ExpD = "3/1/2009" });

            BrokerDB brokerDb = BrokerDB.RetrieveById(principal.BrokerId);

            brokerDb.LicenseInformationList = licenseInfoList;

            brokerDb.Save();

            // Retrieve the list.
            brokerDb = BrokerDB.RetrieveById(principal.BrokerId);

            licenseInfoList = brokerDb.LicenseInformationList;

            Assert.AreEqual(3, licenseInfoList.Count, "LicenseInfoList.Count");

            brokerDb.LicenseInformationList = new LicenseInfoList(); // Clear out LicenseInformationList;

            brokerDb.Save();


            // Retrieve the list.
            brokerDb = BrokerDB.RetrieveById(principal.BrokerId);

            licenseInfoList = brokerDb.LicenseInformationList;

            Assert.AreEqual(0, licenseInfoList.Count, "LicenseInfoList.Count");

        }

        [Test]
        [ExpectedException(typeof(BrokerDBNotFoundException))]
        public void RetrieveInvalidBrokerDBTest()
        {
            BrokerDB brokerDB = BrokerDB.RetrieveById(Guid.Empty);
        }

        [Test]
        [ExpectedException(typeof(BrokerDBNotFoundException))]
        public void RetrieveRandomGuidBrokerDBTest()
        {
            BrokerDB brokerDB = BrokerDB.RetrieveById(Guid.NewGuid());
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void TestCannotUnsetDefaultWholesaleBranchId()
        {
            if (!ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                Assert.Ignore("ConstStage.EnableDefaultBranchesAndPriceGroups is false.");

                return;
            }

            brokerDb.UpdateDefaultBranchPriceGroupValues(
                null,
                this.defaultGuid,
                this.defaultGuid,
                this.defaultGuid,
                this.defaultGuid,
                this.defaultGuid,
                this.defaultGuid);

            // Exception should be thrown here.
            brokerDb.UpdateDefaultBranchPriceGroupValues(
                null,
                Guid.Empty,
                this.defaultGuid,
                this.defaultGuid,
                this.defaultGuid,
                this.defaultGuid,
                this.defaultGuid);

            Assert.Fail("Once BrokerDB.DefaultWholesaleBranchID has been set, it should not be reset.");
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void TestCannotUnsetDefaultWholesalePriceGroupId()
        {
            if (!ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                Assert.Ignore("ConstStage.EnableDefaultBranchesAndPriceGroups is false.");

                return;
            }

            brokerDb.UpdateDefaultBranchPriceGroupValues(
                null,
                this.defaultGuid,
                this.defaultGuid,
                this.defaultGuid,
                this.defaultGuid,
                this.defaultGuid,
                this.defaultGuid);

            // Exception should be thrown here.
            brokerDb.UpdateDefaultBranchPriceGroupValues(
                null,
                this.defaultGuid,
                Guid.Empty,
                this.defaultGuid,
                this.defaultGuid,
                this.defaultGuid,
                this.defaultGuid);

            Assert.Fail("Once BrokerDB.DefaultWholesalePriceGroupID has been set, it should not be reset.");
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void TestCannotUnsetDefaultMiniCorrBranchId()
        {
            if (!ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                Assert.Ignore("ConstStage.EnableDefaultBranchesAndPriceGroups is false.");

                return;
            }

            brokerDb.UpdateDefaultBranchPriceGroupValues(
                null,
                this.defaultGuid,
                this.defaultGuid,
                this.defaultGuid,
                this.defaultGuid,
                this.defaultGuid,
                this.defaultGuid);

            // Exception should be thrown here.
            brokerDb.UpdateDefaultBranchPriceGroupValues(
                null,
                this.defaultGuid,
                this.defaultGuid,
                Guid.Empty,
                this.defaultGuid,
                this.defaultGuid,
                this.defaultGuid);

            Assert.Fail("Once BrokerDB.DefaultMiniCorrBranchID has been set, it should not be reset.");
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void TestCannotUnsetDefaultMiniCorrPriceGroupId()
        {
            if (!ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                Assert.Ignore("ConstStage.EnableDefaultBranchesAndPriceGroups is false.");

                return;
            }

            brokerDb.UpdateDefaultBranchPriceGroupValues(
                null,
                this.defaultGuid,
                this.defaultGuid,
                this.defaultGuid,
                this.defaultGuid,
                this.defaultGuid,
                this.defaultGuid);

            // Exception should be thrown here.
            brokerDb.UpdateDefaultBranchPriceGroupValues(
                null,
                this.defaultGuid,
                this.defaultGuid,
                this.defaultGuid,
                Guid.Empty,
                this.defaultGuid,
                this.defaultGuid);

            Assert.Fail("Once BrokerDB.DefaultMiniCorrBranchID has been set, it should not be reset.");
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void TestCannotUnsetDefaultCorrBranchId()
        {
            if (!ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                Assert.Ignore("ConstStage.EnableDefaultBranchesAndPriceGroups is false.");

                return;
            }

            brokerDb.UpdateDefaultBranchPriceGroupValues(
                null,
                this.defaultGuid,
                this.defaultGuid,
                this.defaultGuid,
                this.defaultGuid,
                this.defaultGuid,
                this.defaultGuid);

            // Exception should be thrown here.
            brokerDb.UpdateDefaultBranchPriceGroupValues(
                null,
                this.defaultGuid,
                this.defaultGuid,
                this.defaultGuid,
                this.defaultGuid,
                Guid.Empty,
                this.defaultGuid);

            Assert.Fail("Once BrokerDB.DefaultCorrBranchID has been set, it should not be reset.");
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void TestCannotUnsetDefaultCorrPriceGroupId()
        {
            if (!ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                Assert.Ignore("ConstStage.EnableDefaultBranchesAndPriceGroups is false.");

                return;
            }

            brokerDb.UpdateDefaultBranchPriceGroupValues(
                null,
                this.defaultGuid,
                this.defaultGuid,
                this.defaultGuid,
                this.defaultGuid,
                this.defaultGuid,
                this.defaultGuid);

            // Exception should be thrown here.
            brokerDb.UpdateDefaultBranchPriceGroupValues(
                null,
                this.defaultGuid,
                this.defaultGuid,
                this.defaultGuid,
                this.defaultGuid,
                this.defaultGuid,
                Guid.Empty);

            Assert.Fail("Once BrokerDB.DefaultCorrBranchID has been set, it should not be reset.");
        }

        [Test]
        public void BrokerDbToDecimalStringTest()
        {
            Assert.AreEqual("3.14159", BrokerDB.ToDecimalString(3.14159m));
            Assert.AreEqual("-1.3686", BrokerDB.ToDecimalString(-1.3686m));
            Assert.AreEqual("0.125", BrokerDB.ToDecimalString(0.125m));
            Assert.AreEqual("-0.125", BrokerDB.ToDecimalString(-0.125m));
            Assert.AreEqual("0", BrokerDB.ToDecimalString(42m));
            Assert.AreEqual("42.0", BrokerDB.ToDecimalString(42.0m));
            Assert.AreEqual("0", BrokerDB.ToDecimalString(12345678901234567890123456789m));
            Assert.AreEqual("2147483648.0", BrokerDB.ToDecimalString(2147483648.0m));
        }
    }
}
