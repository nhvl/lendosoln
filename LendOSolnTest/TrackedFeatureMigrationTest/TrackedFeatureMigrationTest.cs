﻿namespace LendOSolnTest.TrackedFeatureMigrationTest
{
    using System.Collections.Generic;
    using DataAccess;
    using LendersOffice.Admin;
    using NUnit.Framework;

    /// <summary>
    /// Tests to verify that each FeatureType enum exists in the Tracked_Feature table.
    /// </summary>
    [TestFixture]
    public class TrackedFeatureMigrationTest
    {
        /// <summary>
        /// Tests to see if feature enum, db, and the switch statement to add the feature enums are synchronized.
        /// </summary>
        [Test]
        public void DoAllFeatureTypeEnumsMatchFeatureIdInDB()
        {
            List<FeatureData> featureDataList = FeatureData.GetAllTrackedFeatures();

            List<Feature> featureList = new List<Feature>();
            foreach (FeatureData featureData in featureDataList)
            {
                try
                {
                    featureList.Add(Feature.CreateNewFeature(featureData));
                }
                catch (CBaseException exc)
                {
                    Assert.Fail(exc.Message);
                }
            }
        }
    }
}
