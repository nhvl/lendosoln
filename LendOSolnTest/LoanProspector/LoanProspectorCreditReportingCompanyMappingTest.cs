﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using LendOSolnTest.Common;
using LendersOffice.Conversions.LoanProspector;
namespace LendOSolnTest.LoanProspector
{
    [TestFixture]
    public class LoanProspectorCreditReportingCompanyMappingTest
    {
        private string[,] LpCrcMapping = {
                                             {"1", "da8759e4-1607-422f-8838-b3282fe4ff35"}, // CBC Innovis Direct
                                             {"5", "ae23b898-b527-4ebe-b139-830d11d6b4bd"}, // Equifax Mortgage Services
                                             {"6", "da66612f-d574-408a-9785-6eafbf1a57dd"}, // Kroll Factual Data
                                             {"8", "f27cc198-78f9-4059-9b06-d6c94318c803"}, // First American Credco
                                             {"A", "7c6e1bdd-a963-48aa-8eca-2a4c0b355132"}, // LandAmerica Credit Info1
                                             {"B", "07dc117a-f9e8-4ddf-8f65-9ed4ae9a7a60"}, // LandSafe
                                             {"4", "58a078b5-839b-4153-bc49-c236683193b3"}, // FIS Credit Services
                                         };
        [Test]
        public void TestGetLendersOfficeCraIdWithPmlUser()
        {
            LoginTools.LoginAs(TestAccountType.Pml_User0);
            TestGetLendersOfficeCraId();
        }
        [Test]
        public void TestGetLendersOfficeCraIdWithBrokerUser()
        {
            LoginTools.LoginAs(TestAccountType.LenderAccountExec);
            TestGetLendersOfficeCraId();
        }
        [Test]
        public void TestGetLpCrcCodeWithPmlUser()
        {
            LoginTools.LoginAs(TestAccountType.Pml_User0);
            TestGetLpCrcCode();
        }
        [Test]
        public void TestGetLpCrcCodeWithBrokerUser()
        {
            LoginTools.LoginAs(TestAccountType.LenderAccountExec);
            TestGetLpCrcCode();
        }
        private void TestGetLendersOfficeCraId()
        {
            for (int i = 0; i < LpCrcMapping.Length / 2; i++)
            {
                string lpCrcCode = LpCrcMapping[i, 0];
                Guid expectedId = new Guid(LpCrcMapping[i, 1]);

                Guid id = LoanProspectorCreditReportingCompanyMapping.GetLendersOfficeCraId(lpCrcCode);
                Assert.AreEqual(expectedId, id);
            }
        }
        private void TestGetLpCrcCode()
        {
            for (int i = 0; i < LpCrcMapping.Length / 2; i++)
            {
                string expectedLpCrcCode = LpCrcMapping[i, 0];
                string id = LpCrcMapping[i, 1];

                string lpCrcCode = LoanProspectorCreditReportingCompanyMapping.GetLpCrcCode(id);
                Assert.AreEqual(expectedLpCrcCode, lpCrcCode);
            }

        }

    }
}
