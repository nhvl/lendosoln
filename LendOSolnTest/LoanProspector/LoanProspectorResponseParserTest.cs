﻿namespace LendOSolnTest.LoanProspector
{
    using LendersOffice.Conversions.LoanProspector;
    using NUnit.Framework;

    [TestFixture]
    public class LoanProspectorResponseParserTest
    {
        [Test]
        public static void Parse_NullString_ReturnsFailure()
        {
            string input = null;

            var result = LoanProspectorResponseParser.Parse(input);

            Assert.IsFalse(result.IsSuccessful);
        }

        [Test]
        public static void Parse_EmptyString_ReturnsFailure()
        {
            string input = string.Empty;

            var result = LoanProspectorResponseParser.Parse(input);

            Assert.IsFalse(result.IsSuccessful);
        }

        [Test]
        public static void Parse_IncompleteXml_ReturnsFailure()
        {
            string input = "<Invalid><Element />";

            var result = LoanProspectorResponseParser.Parse(input);

            Assert.IsFalse(result.IsSuccessful);
        }

        [Test]
        public static void Parse_EmptyRootElement_ReturnsSuccess()
        {
            string input = @"<SERVICE_ORDER_RESPONSE />";

            var result = LoanProspectorResponseParser.Parse(input);

            Assert.IsTrue(result.IsSuccessful);
        }

        [Test]
        public static void Parse_EmptyRootElementWithDtd_ReturnsSuccess()
        {
            string input = @"<?xml version=""1.0"" ?>
<!DOCTYPE SERVICE_ORDER_RESPONSE SYSTEM ""file:///LOANPROSPECTOR_RESPONSE_4.0.DTD"">
<SERVICE_ORDER_RESPONSE />";

            var result = LoanProspectorResponseParser.Parse(input);

            Assert.IsTrue(result.IsSuccessful);
        }

        [Test]
        public static void Parse_EmptyRootElementWithPartialDtd_ReturnsSuccess()
        {
            string input = @"<?xml version=""1.0"" ?>
<!DOCTYPE SERVICE_ORDER_RESPONSE>
<SERVICE_ORDER_RESPONSE />";

            var result = LoanProspectorResponseParser.Parse(input);

            Assert.IsTrue(result.IsSuccessful);
        }

        [Test]
        public static void Parse_MinimalData_ReturnsSuccess()
        {
            string input = @"<?xml version=""1.0"" ?>
<!DOCTYPE SERVICE_ORDER_RESPONSE SYSTEM ""file:///LOANPROSPECTOR_RESPONSE_4.0.DTD"">
<SERVICE_ORDER_RESPONSE _Date=""12-29-2016"">
  <KEY _Name=""LOANPROSPECTOR_REQUEST"" _Value=""4.0"" />
  <KEY _Name=""LOANPROSPECTOR_RESPONSE"" _Value=""4.0"" />
  <STATUS _Processing=""Completed"" />
</SERVICE_ORDER_RESPONSE>";

            var result = LoanProspectorResponseParser.Parse(input);

            Assert.IsTrue(result.IsSuccessful);
        }
    }
}
