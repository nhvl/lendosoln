﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using Adapter;
using DataAccess;
using LendOSolnTest.Common;
using NUnit.Framework;

namespace LendOSolnTest.DB
{
    [TestFixture]
    [Category(CategoryConstants.RunBeforePullRequest)]
    public class DuplicateLoanTest
    {
        //add a names of each field that are not meant to be duplicated here
        public List<string> whitelist = new List<string>() { 
            
            //--------- Intentionally-Ignored Fields -------------//
            // Please add to the below list for fields that are not
            // supposed to be duplicated.  And reason why the field is special.
            
            // OPM 126382. This is is a dummy field, it should be ignored.
            "DummyFieldIGNORED", 
            
            // OPM 175977
            // The feature that uses this is not going out yet. I'll remove this and add it to the dupe SPs once the feature is ready.
            "sVerifAssetAmtLckd",
            
            // OPM 126382. Disclosed Dates should not be duplicated because it could 
            // make files from templates never be evaluated for RESPA initial disclosures
            "sLastDisclosedD",
            "sLastDisclosedGFEArchiveD",
            "sAutoDisclosureFailed",
            
            // OPM 174045.  sCrmNowLeadId is unique to single loan, 
            // if duplicate Ids exist, we'd be sending multiple statuses to CRM Now.
            "sCrmNowLeadId",
            
            // OPM 175396.  Edocs are not copied in loan duplication, so sHasESignedDocumentsT
            // should go back to its default setting after a duplication.
            "sHasESignedDocumentsT",

            // OPM 149070.  This column is only on production stage.
            "sFHAADPAmortT",

            //OPM 197358 - Request says to not copy. 
            "sAppSubmitedToUsdaD",

            //OPM 190542. Set on loan officer assignment.
            "sPmlCompanyTierId",

            // OPM 179602. This field is set either on employee assignment 
            // or at loan creation. It is handled in the setters for pml roles
            // and in LoanFileCreator.LoanAssignment(). Since it is possible
            // for a file to be created without setting initial assignments
            // or duplicating source assignments, this field should not be copied.
            // The field is stored in LOAN_FILE_B and it is also in 
            // LOAN_FILE_CACHE.
            "sPmlBrokerId",

            // OPM 198237. Specified not to copy.
            "sLoanEstimateDatesJSONContent",

            // OPM 198237. Specified not to copy.
            "sClosingDisclosureDatesJSONContent",

            // OPM 209850. Similar to the other disclosure fields.
            "sLastDisclosedClosingDisclosureArchiveD",

            // OPM 214209. Exclude disclosure field IDs
            "sLastDisclosedGFEArchiveId",
            "sLastDisclosedClosingDisclosureArchiveId",

            // We copy the closing cost archives in the code in LoanFileCreation.
            // Otherwise, we'd be copying the same archive ids and file db keys.
            "sClosingCostArchiveXmlContent",

            // The tolerance archive date and id were being copied but should not
            // any longer becuase the archive ids will change between source and
            // desintation file. This is now handled in loan file creation.
            // The date isn't really used any more and cannot be set through the data
            // layer, so I'm removing that as well as the tolerance archive id.
            "sTolerance10BasisLEArchiveD",
            "sTolerance10BasisLEArchiveId",

            // OPM 220096 - sWasCCAutomationEnabledOnLastRegistration is used to prevent rebucketing depending on the value sFeeServiceApplicationHistoryXmlContent.
            // Since sFeeServiceApplicationHistoryXmlContent isn't currently being duplicated I am also excluding sWasCCAutomationEnabledOnLastRegistration
            // to prevent logic errors that might prevent rebucketing when it's supposed to happen.
            "sWasCCAutomationEnabledOnLastRegistration",

            // OPM 245700 - aUdnOrderId represents a specific UDN order. It should not be duplicated
            // as that could lead to duplicate GET or DEACTIVATE calls.
            "aUdnOrderId",

            // OPM 244087 - sOldLRefNm is only valid on invalid loan files. It serves a similar purpose to sOldLNm which is also whitelisted.
            "sOldLRefNm",

            // OPM 311311 - this field should come directly from the broker.
            nameof(CPageBase.sLegalEntityIdentifier),

            // OPM 311311 - this field should be unique within the broker.
            nameof(CPageBase.sUniversalLoanIdentifier),

            // OPM 311311 - this field should be reset whenever a loan is duplicated.
            nameof(CPageBase.sUniversalLoanIdentifierLckd),

            // OPM 240571 - CEID should be unique per loan.
            nameof(CPageBase.sComplianceEaseId),

            // OPM 470271 - These fields have been moved to FileDB.
            "sPreparerXmlContent",
            "sAgentXmlContent",

            // OPM 467075, 470271 - These fields must be decrypted at the application level 
            // before they are stored using the duplicate loan's encryption key.
            "aBSsn",
            "aCSsn",
            "aB4506TSsnTinEin",
            "aC4506TSsnTinEin",
            "aVaServ1Ssn",
            "aVaServ2Ssn",
            "aVaServ3Ssn",
            "aVaServ4Ssn",
            "sHmdaCoApplicantSourceSsn",
            "sCombinedBorSsn",
            "sCombinedCoborSsn",
            "sTax1098PayerSsn",
            "sFHASponsoredOriginatorEIN",

            "aBSsnEncrypted",
            "aCSsnEncrypted",
            "aB4506TSsnTinEinEncrypted",
            "aC4506TSsnTinEinEncrypted",
            "aVaServ1SsnEncrypted",
            "aVaServ2SsnEncrypted",
            "aVaServ3SsnEncrypted",
            "aVaServ4SsnEncrypted",
            "sHmdaCoApplicantSourceSsnEncrypted",
            "sCombinedBorSsnEncrypted",
            "sCombinedCoborSsnEncrypted",
            "sTax1098PayerSsnEncrypted",
            "sFhaSponsoredOriginatorEinEncrypted",

            // OPM 467981
            nameof(CAppBase.aBCreditReportAuthorizationProvidedVerbally),
            nameof(CAppBase.aCCreditReportAuthorizationProvidedVerbally),

            // OPM 467096 - these fields were moved to FileDB
            // and should not be duplicated in the database.
            "sFHACaseQueryResultXmlContent",
            "sFHACaseNumberResultXmlContent",
            "sFHACAVIRSResultXmlContent",
            "sProdPmlDataUsedForLastPricingXmlContent",
            "sPmlCertXmlContent",
            "sTotalScoreCertificateXmlContent",
            "sTotalScoreTempCertificateXmlContent",

            // OPM 467096 - these fields were moved to FileDB
            // and should not be duplicated in the database.
            "sFHACaseQueryResultXmlContent",
            "sFHACaseNumberResultXmlContent",
            "sFHACAVIRSResultXmlContent",
            "sProdPmlDataUsedForLastPricingXmlContent",
            "sPmlCertXmlContent",
            "sTotalScoreCertificateXmlContent",
            "sTotalScoreTempCertificateXmlContent",

            // The consumer id fields will eventually be primary keys for a
            // CONSUMER table, we don't want them to be duplicated.
            nameof(CAppBase.aBConsumerId),
            nameof(CAppBase.aCConsumerId),

            // This field was temporarily added back to the db to prevent
            // build issues with people testing hotfixes. It will be removed
            // again in the first release of 2019.
            "aCIsDefined",

            //----------------------------------------------------//
            
           // DO NOT ADD FIELDS HERE!  Add in section above if needed.

           
 
            //--------- Automatically Added Fields -------------//
            // Most of the fields below ARE needed to be a part of duplication,
            // and added to DuplicateLoanFile, but they need review first.
            // DO NOT ADD to this section.  Please add above if a new field needs to be ignored.
            // We want this section to get shorter, not longer.

            "aAsstLiaCompletedNotJointly", // APPLICATION_A
            "aBCountryMailT", // APPLICATION_A
            "aBH4HNonOccInterestT", // APPLICATION_A
            "aBHasFraudConvictionT", // APPLICATION_A
            "aCCountryMailT", // APPLICATION_A
            "aCH4HNonOccInterestT", // APPLICATION_A
            "aCHasFraudConvictionT", // APPLICATION_A
            "aVALAnalysisEconomicLifeYrs", // APPLICATION_A
            "aVALAnalysisExpirationD", // APPLICATION_A
            "aVALAnalysisValue", // APPLICATION_A

            "a1003InterviewD", // APPLICATION_B
            "a1003SignD", // APPLICATION_B
            "aB4506TNm", // APPLICATION_B
            "aB4506TNmLckd", // APPLICATION_B
            "aB4506TRequestYrHadIdentityTheft", // APPLICATION_B
            "aBEConsentDeclinedD", // APPLICATION_B
            "aBEConsentReceivedD", // APPLICATION_B
            "aBIdExpireD", // APPLICATION_B
            "aBIdIssueD", // APPLICATION_B
            "aBIsPrimaryWageEarner", // APPLICATION_B
            "aBMinFicoScore", // APPLICATION_B
            "aBOIFrom1008", // APPLICATION_B
            "aBRelationshipTitleOtherDesc", // APPLICATION_B
            "aC4506TNm", // APPLICATION_B
            "aC4506TNmLckd", // APPLICATION_B
            "aC4506TRequestYrHadIdentityTheft", // APPLICATION_B
            "aCCrRd", // APPLICATION_B
            "aCDenialCreditScoreD", // APPLICATION_B
            "aCEConsentDeclinedD", // APPLICATION_B
            "aCEConsentReceivedD", // APPLICATION_B
            "aCIdExpireD", // APPLICATION_B
            "aCIdIssueD", // APPLICATION_B
            "aCIsPrimaryWageEarner", // APPLICATION_B
            "aCMinFicoScore", // APPLICATION_B
            "aCOIFrom1008", // APPLICATION_B
            "aCRelationshipTitleOtherDesc", // APPLICATION_B
            "aFHABorrCertOccIsAsHome", // APPLICATION_B
            "aFHABorrCertOccIsAsHomeForActiveSpouse", // APPLICATION_B
            "aFHABorrCertOccIsAsHomePrev", // APPLICATION_B
            "aFHABorrCertOccIsAsHomePrevForActiveSpouse", // APPLICATION_B
            "aHas1stTimeBuyerTri", // APPLICATION_B
            "aIsBorrSpousePrimaryWageEarner", // APPLICATION_B
            "aIsBorrSpousePrimaryWageEarnerPelckd", // APPLICATION_B
            "aIsBorrSpousePrimaryWageEarnerPeval", // APPLICATION_B
            "aMORNETPlusCaseFileId", // APPLICATION_B
            "aOIFrom1008Desc", // APPLICATION_B
            "aOpNegCfPeval", // APPLICATION_B
            "aPresOHExpPelckd", // APPLICATION_B
            "aProdCrManual120MortLateCount", // APPLICATION_B
            "aProdCrManual150MortLateCount", // APPLICATION_B
            "aProdCrManual30MortLateCount", // APPLICATION_B
            "aProdCrManual60MortLateCount", // APPLICATION_B
            "aProdCrManual90MortLateCount", // APPLICATION_B
            "aProdCrManualBk13Has", // APPLICATION_B
            "aProdCrManualBk13RecentFileMon", // APPLICATION_B
            "aProdCrManualBk13RecentFileYr", // APPLICATION_B
            "aProdCrManualBk13RecentSatisfiedMon", // APPLICATION_B
            "aProdCrManualBk13RecentSatisfiedYr", // APPLICATION_B
            "aProdCrManualBk13RecentStatusT", // APPLICATION_B
            "aProdCrManualBk7Has", // APPLICATION_B
            "aProdCrManualBk7RecentFileMon", // APPLICATION_B
            "aProdCrManualBk7RecentFileYr", // APPLICATION_B
            "aProdCrManualBk7RecentSatisfiedMon", // APPLICATION_B
            "aProdCrManualBk7RecentSatisfiedYr", // APPLICATION_B
            "aProdCrManualBk7RecentStatusT", // APPLICATION_B
            "aProdCrManualForeclosureHas", // APPLICATION_B
            "aProdCrManualForeclosureRecentFileMon", // APPLICATION_B
            "aProdCrManualForeclosureRecentFileYr", // APPLICATION_B
            "aProdCrManualForeclosureRecentSatisfiedMon", // APPLICATION_B
            "aProdCrManualForeclosureRecentSatisfiedYr", // APPLICATION_B
            "aProdCrManualForeclosureRecentStatusT", // APPLICATION_B
            "aProdCrManualNonRolling30MortLateCount", // APPLICATION_B
            "aProdCrManualRolling60MortLateCount", // APPLICATION_B
            "aProdCrManualRolling90MortLateCount", // APPLICATION_B
            "aTransmOMonPmtPeval", // APPLICATION_B
            "aVaBorrCertHadVaLoanTri", // APPLICATION_B
            "aVaClaimNum", // APPLICATION_B
            "aVaDischargedDisabilityIs", // APPLICATION_B
            "aVaPastLXmlContent", // APPLICATION_B
            "aVaServ1BranchNum", // APPLICATION_B
            "aVaServ1EndD", // APPLICATION_B
            "aVaServ1FullNm", // APPLICATION_B
            "aVaServ1Num", // APPLICATION_B
            "aVaServ1StartD", // APPLICATION_B
            "aVaServ2BranchNum", // APPLICATION_B
            "aVaServ2EndD", // APPLICATION_B
            "aVaServ2FullNm", // APPLICATION_B
            "aVaServ2Num", // APPLICATION_B
            "aVaServ2StartD", // APPLICATION_B
            "aVaServ3BranchNum", // APPLICATION_B
            "aVaServ3EndD", // APPLICATION_B
            "aVaServ3FullNm", // APPLICATION_B
            "aVaServ3Num", // APPLICATION_B
            "aVaServ3StartD", // APPLICATION_B
            "aVaServ4BranchNum", // APPLICATION_B
            "aVaServ4EndD", // APPLICATION_B
            "aVaServ4FullNm", // APPLICATION_B
            "aVaServ4Num", // APPLICATION_B
            "aVaServ4StartD", // APPLICATION_B
            "aVaVestTitleODesc", // APPLICATION_B
            "aVaVestTitleT", // APPLICATION_B

            "IsValid", // LOAN_FILE_A
            "sAuditTrackedFieldChanges", // LOAN_FILE_A
            "sConsumerCanEditEndTime", // LOAN_FILE_A
            "sConsumerCanEditStartTime", // LOAN_FILE_A
            "sConsumerCanEditStatuses", // LOAN_FILE_A
            "sCreatedD", // LOAN_FILE_A
            //"sFileVersion", // LOAN_FILE_A  // opm 185732
            "sIsSection32", // LOAN_FILE_A
            "sIsSelfEmployed", // LOAN_FILE_A
            "sOpenedD", // LOAN_FILE_A

            "sLoanOfficerRateLockedD", // LOAN_FILE_B
            "sLoanOfficerRateLockStatusT", // LOAN_FILE_B
            "sConsumerPortalCreationD", // LOAN_FILE_B
            "sConsumerPortalCreationT", // LOAN_FILE_B

            "s30DayInterestOldLoan", // LOAN_FILE_C
            "sAcknowledgeCashAdvanceForNonHomestead", // LOAN_FILE_C
            "sAppraisalVendorId", // LOAN_FILE_C
            "sBillingABANum", // LOAN_FILE_C
            "sBillingAccountLocationT", // LOAN_FILE_C
            "sBillingAccountNum", // LOAN_FILE_C
            "sBillingAccountT", // LOAN_FILE_C
            "sBillingAdditionalAmt", // LOAN_FILE_C
            "sBillingMethodT", // LOAN_FILE_C
            "sBillingNameOnAccount", // LOAN_FILE_C
            "sCondoHO6InsPolicyExpirationD", // LOAN_FILE_C
            "sCondoHO6InsPolicyPayeeCode", // LOAN_FILE_C
            "sConsumerPortalSubmittedD", // LOAN_FILE_C
            "sCustomTrailingDoc1ReviewedD", // LOAN_FILE_C            
            "sCustomTrailingDoc2ReviewedD", // LOAN_FILE_C            
            "sCustomTrailingDoc3ReviewedD", // LOAN_FILE_C           
            "sCustomTrailingDoc4ReviewedD", // LOAN_FILE_C
            "sDaysInWarehouse", // LOAN_FILE_C
            "sDisclosureNeededT", // LOAN_FILE_C
            "sDisclosuresDueD", // LOAN_FILE_C
            "sDisclosuresDueD_New", // LOAN_FILE_C
            "sDocVendorPortalUrl", // LOAN_FILE_C
            "sFeeServiceApplicationHistoryXmlContent", // LOAN_FILE_C
            "sFhaAddendToHud1NonBorrSrcFunds", // LOAN_FILE_C
            "sFhaEndorsementD", // LOAN_FILE_C
            "sFhaUfmipAmt", // LOAN_FILE_C
            "sFinalFloodInsPolicyRequired", // LOAN_FILE_C
            "sFinalFloodInsPolicyReviewedD", // LOAN_FILE_C
            "sFinalHazInsPolicyRequired", // LOAN_FILE_C
            "sFinalHazInsPolicyReviewedD", // LOAN_FILE_C
            "sFinalHUD1SttlmtStmtRequired", // LOAN_FILE_C
            "sFinalHUD1SttlmtStmtReviewedD", // LOAN_FILE_C
            "sFirstLienRecordingD", // LOAN_FILE_C
            "sFundingCfmAgentRoleT", // LOAN_FILE_C
            "sFundingCfmIsLocked", // LOAN_FILE_C
            "sGfeEstScAvailTillDLckd", // LOAN_FILE_C
            "sGfeMaxPpmtPenaltyAmt", // LOAN_FILE_C
            "sGfeNoteIRAvailTillDLckd", // LOAN_FILE_C
            "sGfeRateLockPeriodLckd", // LOAN_FILE_C
            "sGseDeliveryPayeeT", // LOAN_FILE_C
            "sHasPiggyBackFinancing", // LOAN_FILE_C
            "sHasPrepaymentPenalty", // LOAN_FILE_C
            "sHasPrivateMortgageInsurance", // LOAN_FILE_C
            "sHistoricalPricingVersionD", // LOAN_FILE_C
            "sHmdaLienTLckd", // LOAN_FILE_C
            "sHmdaPurchaser2004TLckd", // LOAN_FILE_C
            "sIsPreserveGFEFees", // LOAN_FILE_C
            "sLayeredTotalForgivableBalance", // LOAN_FILE_C
            "sLoanPackageRequired", // LOAN_FILE_C
            "sLoanPackageReviewedD", // LOAN_FILE_C
            "sMiCertRequired", // LOAN_FILE_C
            "sMiCertReviewedD", // LOAN_FILE_C
            "sMiPmtCheckNumber", // LOAN_FILE_C
            "sMortgageDOTRequired", // LOAN_FILE_C
            "sMortgageDOTReviewedD", // LOAN_FILE_C
            "sNeedInitialDisc", // LOAN_FILE_C
            "sNeedRedisc", // LOAN_FILE_C
            "sNoteRequired", // LOAN_FILE_C
            "sNoteReviewedD", // LOAN_FILE_C
            "sNyPropertyStatementT", // LOAN_FILE_C
            "sOwnerTitleInsFPe", // LOAN_FILE_C
            "sPayoffStatementInterestCalcT", // LOAN_FILE_C
            "sPayoffStatementNotesToRecipient", // LOAN_FILE_C
            "sPayoffStatementPayoffD", // LOAN_FILE_C
            "sPayoffStatementRecipientAddressee", // LOAN_FILE_C
            "sPayoffStatementRecipientAttention", // LOAN_FILE_C
            "sPayoffStatementRecipientCity", // LOAN_FILE_C
            "sPayoffStatementRecipientState", // LOAN_FILE_C
            "sPayoffStatementRecipientStreetAddress", // LOAN_FILE_C
            "sPayoffStatementRecipientT", // LOAN_FILE_C
            "sPayoffStatementRecipientZip", // LOAN_FILE_C
            "sPdByBorrowerU5FRowLckd", // LOAN_FILE_C
            "sPdByBorrowerU6FRowLckd", // LOAN_FILE_C
            "sPmlLastGenerateResultD", // LOAN_FILE_C
            "sPrevAprDelta", // LOAN_FILE_C
            "sPurchaseAdviceBasePrice_AmountPriceCalcMode", // LOAN_FILE_C
            "sQMNonExcludableDiscountPointsPc", // LOAN_FILE_C
            "sRequestForAppraisalAppraiserId", // LOAN_FILE_C
            "sSecInstrAttorneyFeesPc", // LOAN_FILE_C
            "sSubservicerLoanNm", // LOAN_FILE_C
            "sSubservicerMersId", // LOAN_FILE_C
            "sTax1098OtherInfo2", // LOAN_FILE_C
            "sTitleInsPolicyRequired", // LOAN_FILE_C
            "sTitleInsPolicyReviewedD", // LOAN_FILE_C
            "sTotalCurrentPIAndMonMIP", // LOAN_FILE_C
            "sUfmipIsRefundableOnProRataBasis", // LOAN_FILE_C
            "sUsdaBorrowerId", // LOAN_FILE_C
            "sUsdaCountyCode", // LOAN_FILE_C
            "sUsdaStateCode", // LOAN_FILE_C
            "sWarehouseAdvanceClassification", // LOAN_FILE_C
            "sWarehouseLenderRolodexId", // LOAN_FILE_C
            "sWindInsPolicyExpirationD", // LOAN_FILE_C
            "sWindInsPolicyPayeeCode", // LOAN_FILE_C
            "sWindInsPolicyPaymentTypeT", // LOAN_FILE_C

            "sApprRprtPrepD", // LOAN_FILE_D
            "sCircumstanceChange1FeeId", // LOAN_FILE_D
            "sCircumstanceChange2FeeId", // LOAN_FILE_D
            "sCircumstanceChange3FeeId", // LOAN_FILE_D
            "sCircumstanceChange4FeeId", // LOAN_FILE_D
            "sCircumstanceChange5FeeId", // LOAN_FILE_D
            "sCircumstanceChange6FeeId", // LOAN_FILE_D
            "sCircumstanceChange7FeeId", // LOAN_FILE_D
            "sCircumstanceChange8FeeId", // LOAN_FILE_D
            "sComplianceEaseChecksum", // LOAN_FILE_D
            "sComplianceEaseErrorMessage", // LOAN_FILE_D
            "sComplianceEaseStatus", // LOAN_FILE_D
            "sCooperativeApartmentNumber", // LOAN_FILE_D
            "sCooperativeFormOfOwnershipT", // LOAN_FILE_D
            "sCooperativeLeaseAssignD", // LOAN_FILE_D
            "sCooperativeLeaseD", // LOAN_FILE_D
            "sCooperativeLienSearchD", // LOAN_FILE_D
            "sCooperativeLienSearchNumber", // LOAN_FILE_D
            "sCooperativeNumberOfShares", // LOAN_FILE_D
            "sCooperativeStockCertNumber", // LOAN_FILE_D
            "sCreditScoreEstimatePe", // LOAN_FILE_D
            "sDemandApprFeeBrokerPaidAmt", // LOAN_FILE_D
            "sDemandApprFeeLenderAmt", // LOAN_FILE_D
            "sDemandCreditReportFeeBrokerPaidAmt", // LOAN_FILE_D
            "sDemandCreditReportFeeLenderAmt", // LOAN_FILE_D
            "sDemandLDiscountLenderMb", // LOAN_FILE_D
            "sDemandLDiscountLenderPc", // LOAN_FILE_D
            "sDemandLoanDocFeeBorrowerAmt", // LOAN_FILE_D
            "sDemandLoanDocFeeBrokerPaidAmt", // LOAN_FILE_D
            "sDemandLoanDocFeeLenderAmt", // LOAN_FILE_D
            "sDemandLOrigFeeLenderMb", // LOAN_FILE_D
            "sDemandLOrigFeeLenderPc", // LOAN_FILE_D
            "sDemandProcessingFeeBrokerPaidAmt", // LOAN_FILE_D
            "sDemandProcessingFeeLenderAmt", // LOAN_FILE_D
            "sDemandU1BorrowerAmt", // LOAN_FILE_D
            "sDemandU1BrokerPaidAmt", // LOAN_FILE_D
            "sDemandU1LenderAmt", // LOAN_FILE_D
            "sDemandU1LenderDesc", // LOAN_FILE_D
            "sDemandU2BorrowerAmt", // LOAN_FILE_D
            "sDemandU2BrokerPaidAmt", // LOAN_FILE_D
            "sDemandU2LenderAmt", // LOAN_FILE_D
            "sDemandU2LenderDesc", // LOAN_FILE_D
            "sDemandU3BorrowerAmt", // LOAN_FILE_D
            "sDemandU3BrokerPaidAmt", // LOAN_FILE_D
            "sDemandU3LenderAmt", // LOAN_FILE_D
            "sDemandU3LenderDesc", // LOAN_FILE_D
            "sDemandU4BorrowerAmt", // LOAN_FILE_D
            "sDemandU4BrokerPaidAmt", // LOAN_FILE_D
            "sDemandU4LenderAmt", // LOAN_FILE_D
            "sDemandU4LenderDesc", // LOAN_FILE_D
            "sDemandU5BorrowerAmt", // LOAN_FILE_D
            "sDemandU5BrokerPaidAmt", // LOAN_FILE_D
            "sDemandU5LenderAmt", // LOAN_FILE_D
            "sDemandU5LenderDesc", // LOAN_FILE_D
            "sDemandU6BorrowerAmt", // LOAN_FILE_D
            "sDemandU6BrokerPaidAmt", // LOAN_FILE_D
            "sDemandU6LenderAmt", // LOAN_FILE_D
            "sDemandU6LenderDesc", // LOAN_FILE_D
            "sDemandU7BorrowerAmt", // LOAN_FILE_D
            "sDemandU7BrokerPaidAmt", // LOAN_FILE_D
            "sDemandU7LenderAmt", // LOAN_FILE_D
            "sDemandU7LenderDesc", // LOAN_FILE_D
            "sDemandYieldSpreadPremiumBorrowerMb", // LOAN_FILE_D
            "sDemandYieldSpreadPremiumBorrowerPc", // LOAN_FILE_D
            "sDemandYieldSpreadPremiumLenderMb", // LOAN_FILE_D
            "sDemandYieldSpreadPremiumLenderPc", // LOAN_FILE_D
            "sDocMagicDownloadedDocsJSON", // LOAN_FILE_D
            "sDocMagicPlanCodeId", // LOAN_FILE_D
            "sFloodCertId", // LOAN_FILE_D
            "sFloodCertificationCancellationOrderedD", // LOAN_FILE_D
            "sFloodCertificationCancellationReceivedD", // LOAN_FILE_D
            "sFloodCertificationCommunityNum", // LOAN_FILE_D
            "sFloodCertificationDesignationD", // LOAN_FILE_D
            "sFloodCertificationDeterminationD", // LOAN_FILE_D
            "sFloodCertificationDeterminationDTimeZoneT", // LOAN_FILE_D
            "sFloodCertificationIsCBRAorOPA", // LOAN_FILE_D
            "sFloodCertificationIsLOLUpgraded", // LOAN_FILE_D
            "sFloodCertificationIsNoMap", // LOAN_FILE_D
            "sFloodCertificationLOLUpgradeD", // LOAN_FILE_D
            "sFloodCertificationLOMChangedD", // LOAN_FILE_D
            "sFloodCertificationMapD", // LOAN_FILE_D
            "sFloodCertificationMapNum", // LOAN_FILE_D
            "sFloodCertificationPanelNums", // LOAN_FILE_D
            "sFloodCertificationPanelSuffix", // LOAN_FILE_D
            "sFloodCertificationParticipationStatus", // LOAN_FILE_D
            "sFloodCertificationPreparerOtherName", // LOAN_FILE_D
            "sFloodCertificationPreparerT", // LOAN_FILE_D
            "sFloridaIsAppFeeApplicableToCc", // LOAN_FILE_D
            "sFloridaIsAppFeeRefundable", // LOAN_FILE_D
            "sFundingABANumber", // LOAN_FILE_D
            "sFundingAccountName", // LOAN_FILE_D
            "sFundingAccountNumber", // LOAN_FILE_D
            "sFundingAdditionalInstructionsLine1", // LOAN_FILE_D
            "sFundingAdditionalInstructionsLine2", // LOAN_FILE_D
            "sFundingBankCityState", // LOAN_FILE_D
            "sFundingBankName", // LOAN_FILE_D
            "sGLNewServAccPmtD", // LOAN_FILE_D
            "sGLNewServFirstPmtSchedD", // LOAN_FILE_D
            "sGLServTransEffD", // LOAN_FILE_D
            "sGrossDueFromBorrPersonalProperty", // LOAN_FILE_D
            "sGrossDueFromBorrU1F", // LOAN_FILE_D
            "sGrossDueFromBorrU1FDesc", // LOAN_FILE_D
            "sGrossDueFromBorrU2F", // LOAN_FILE_D
            "sGrossDueFromBorrU2FDesc", // LOAN_FILE_D
            "sGrossDueToSellerU1F", // LOAN_FILE_D
            "sGrossDueToSellerU1FDesc", // LOAN_FILE_D
            "sGrossDueToSellerU2F", // LOAN_FILE_D
            "sGrossDueToSellerU2FDesc", // LOAN_FILE_D
            "sGrossDueToSellerU2FSameAsBorr", // LOAN_FILE_D
            "sGrossDueToSellerU3F", // LOAN_FILE_D
            "sGrossDueToSellerU3FDesc", // LOAN_FILE_D
            "sGrossDueToSellerU3FSameAsBorr", // LOAN_FILE_D
            "sGseFreddieMacProductT", // LOAN_FILE_D
            "sHelocBal", // LOAN_FILE_D
            "sHelocCreditLimit", // LOAN_FILE_D
            "sInitAPR", // LOAN_FILE_D
            "sInsReqPrepD", // LOAN_FILE_D
            "sIsConvertibleMortgage", // LOAN_FILE_D
            "sLastDiscAPR", // LOAN_FILE_D
            "sLeaseholdHolderName", // LOAN_FILE_D
            "sLeaseHoldInstrumentNumber", // LOAN_FILE_D
            "sLeaseholdLeaseD", // LOAN_FILE_D
            "sLeaseholdRecordD", // LOAN_FILE_D
            "sLenderNumVerif", // LOAN_FILE_D
            "sLenderRepCity", // LOAN_FILE_D
            "sLenderRepNm", // LOAN_FILE_D
            "sLenderRepStAddr", // LOAN_FILE_D
            "sLenderRepState", // LOAN_FILE_D
            "sLenderRepZip", // LOAN_FILE_D
            "sLoanProceedsToT", // LOAN_FILE_D
            "sManufacturedHomeAttachedToFoundation", // LOAN_FILE_D
            "sManufacturedHomeCertificateofTitle", // LOAN_FILE_D
            "sManufacturedHomeCertificateofTitleT", // LOAN_FILE_D
            "sManufacturedHomeConditionT", // LOAN_FILE_D
            "sManufacturedHomeHUDLabelNumber", // LOAN_FILE_D
            "sManufacturedHomeLengthFeet", // LOAN_FILE_D
            "sManufacturedHomeMake", // LOAN_FILE_D
            "sManufacturedHomeManufacturer", // LOAN_FILE_D
            "sManufacturedHomeModel", // LOAN_FILE_D
            "sManufacturedHomeSerialNumber", // LOAN_FILE_D
            "sManufacturedHomeWidthFeet", // LOAN_FILE_D
            "sManufacturedHomeYear", // LOAN_FILE_D
            "sMortgagePoolId", // LOAN_FILE_D
            "sMsgToConsumer", // LOAN_FILE_D
            "sPdByBorrowerU1F", // LOAN_FILE_D
            "sPdByBorrowerU1FDesc", // LOAN_FILE_D
            "sPdByBorrowerU1FHudline", // LOAN_FILE_D
            "sPdByBorrowerU2F", // LOAN_FILE_D
            "sPdByBorrowerU2FDesc", // LOAN_FILE_D
            "sPdByBorrowerU2FHudline", // LOAN_FILE_D
            "sPdByBorrowerU3F", // LOAN_FILE_D
            "sPdByBorrowerU3FDesc", // LOAN_FILE_D
            "sPdByBorrowerU3FHudline", // LOAN_FILE_D
            "sPdByBorrowerU4F", // LOAN_FILE_D
            "sPdByBorrowerU4FDesc", // LOAN_FILE_D
            "sPdByBorrowerU4FHudline", // LOAN_FILE_D
            "sPdByBorrowerU5F", // LOAN_FILE_D
            "sPdByBorrowerU5FDesc", // LOAN_FILE_D
            "sPdByBorrowerU5FHudline", // LOAN_FILE_D
            "sPdByBorrowerU6F", // LOAN_FILE_D
            "sPdByBorrowerU6FDesc", // LOAN_FILE_D
            "sPdByBorrowerU6FHudline", // LOAN_FILE_D
            "sPdBySellerAssessmentsTaxF", // LOAN_FILE_D
            "sPdBySellerAssessmentsTaxFEndD", // LOAN_FILE_D
            "sPdBySellerAssessmentsTaxFStartD", // LOAN_FILE_D
            "sPdBySellerCityTaxF", // LOAN_FILE_D
            "sPdBySellerCityTaxFEndD", // LOAN_FILE_D
            "sPdBySellerCityTaxFStartD", // LOAN_FILE_D
            "sPdBySellerCountyTaxF", // LOAN_FILE_D
            "sPdBySellerCountyTaxFEndD", // LOAN_FILE_D
            "sPdBySellerCountyTaxFStartD", // LOAN_FILE_D
            "sPdBySellerU1F", // LOAN_FILE_D
            "sPdBySellerU1FDesc", // LOAN_FILE_D
            "sPdBySellerU1FEndD", // LOAN_FILE_D
            "sPdBySellerU1FStartD", // LOAN_FILE_D
            "sPdBySellerU2F", // LOAN_FILE_D
            "sPdBySellerU2FDesc", // LOAN_FILE_D
            "sPdBySellerU2FEndD", // LOAN_FILE_D
            "sPdBySellerU2FStartD", // LOAN_FILE_D
            "sPdBySellerU3F", // LOAN_FILE_D
            "sPdBySellerU3FDesc", // LOAN_FILE_D
            "sPdBySellerU3FEndD", // LOAN_FILE_D
            "sPdBySellerU3FStartD", // LOAN_FILE_D
            "sPdBySellerU4F", // LOAN_FILE_D
            "sPdBySellerU4FDesc", // LOAN_FILE_D
            "sPdBySellerU4FEndD", // LOAN_FILE_D
            "sPdBySellerU4FStartD", // LOAN_FILE_D
            "sPrepmtPeriodMonths", // LOAN_FILE_D
            "sProHazInsPeval", // LOAN_FILE_D
            "sProHoAssocDuesPeval", // LOAN_FILE_D
            "sProMInsPeval", // LOAN_FILE_D
            "sRAdjFloorBaseT", // LOAN_FILE_D
            "sRealEstateBrokerFees1F", // LOAN_FILE_D
            "sRealEstateBrokerFees1FAgentName", // LOAN_FILE_D
            "sRealEstateBrokerFees2F", // LOAN_FILE_D
            "sRealEstateBrokerFees2FAgentName", // LOAN_FILE_D
            "sRealEstateBrokerFeesCommissionPdAtSettlementFFromBorr", // LOAN_FILE_D
            "sRealEstateBrokerFeesCommissionPdAtSettlementFFromSeller", // LOAN_FILE_D
            "sRealEstateBrokerFeesU1FDesc", // LOAN_FILE_D
            "sRealEstateBrokerFeesU1FFromBorr", // LOAN_FILE_D
            "sRealEstateBrokerFeesU1FFromSeller", // LOAN_FILE_D
            "sRedisclosureMethodT", // LOAN_FILE_D
            "sReductionsDueToSellerExcessDeposit", // LOAN_FILE_D
            "sReductionsDueToSellerPayoffOf1stMtgLoan", // LOAN_FILE_D
            "sReductionsDueToSellerPayoffOf2ndMtgLoan", // LOAN_FILE_D
            "sReductionsDueToSellerU1F", // LOAN_FILE_D
            "sReductionsDueToSellerU1FDesc", // LOAN_FILE_D
            "sReductionsDueToSellerU1FHudline", // LOAN_FILE_D
            "sReductionsDueToSellerU1SameAsBorr", // LOAN_FILE_D
            "sReductionsDueToSellerU2F", // LOAN_FILE_D
            "sReductionsDueToSellerU2FDesc", // LOAN_FILE_D
            "sReductionsDueToSellerU2FHudline", // LOAN_FILE_D
            "sReductionsDueToSellerU2SameAsBorr", // LOAN_FILE_D
            "sReductionsDueToSellerU3F", // LOAN_FILE_D
            "sReductionsDueToSellerU3FDesc", // LOAN_FILE_D
            "sReductionsDueToSellerU3FHudline", // LOAN_FILE_D
            "sReductionsDueToSellerU3SameAsBorr", // LOAN_FILE_D
            "sReductionsDueToSellerU4F", // LOAN_FILE_D
            "sReductionsDueToSellerU4FDesc", // LOAN_FILE_D
            "sReductionsDueToSellerU4FHudline", // LOAN_FILE_D
            "sReductionsDueToSellerU4SameAsBorr", // LOAN_FILE_D
            "sReductionsDueToSellerU5F", // LOAN_FILE_D
            "sReductionsDueToSellerU5FDesc", // LOAN_FILE_D
            "sReductionsDueToSellerU5FHudline", // LOAN_FILE_D
            "sReductionsDueToSellerU5SameAsBorr", // LOAN_FILE_D
            "sReductionsDueToSellerU6F", // LOAN_FILE_D
            "sReductionsDueToSellerU6FDesc", // LOAN_FILE_D
            "sReductionsDueToSellerU6FHudline", // LOAN_FILE_D
            "sReductionsDueToSellerU6SameAsBorr", // LOAN_FILE_D
            "sRLckdIsLocked", // LOAN_FILE_D
            "sSellerSettlementChargesU01F", // LOAN_FILE_D
            "sSellerSettlementChargesU01FDesc", // LOAN_FILE_D
            "sSellerSettlementChargesU01FHudline", // LOAN_FILE_D
            "sSellerSettlementChargesU02F", // LOAN_FILE_D
            "sSellerSettlementChargesU02FDesc", // LOAN_FILE_D
            "sSellerSettlementChargesU02FHudline", // LOAN_FILE_D
            "sSellerSettlementChargesU03F", // LOAN_FILE_D
            "sSellerSettlementChargesU03FDesc", // LOAN_FILE_D
            "sSellerSettlementChargesU03FHudline", // LOAN_FILE_D
            "sSellerSettlementChargesU04F", // LOAN_FILE_D
            "sSellerSettlementChargesU04FDesc", // LOAN_FILE_D
            "sSellerSettlementChargesU04FHudline", // LOAN_FILE_D
            "sSellerSettlementChargesU05F", // LOAN_FILE_D
            "sSellerSettlementChargesU05FDesc", // LOAN_FILE_D
            "sSellerSettlementChargesU05FHudline", // LOAN_FILE_D
            "sSellerSettlementChargesU06F", // LOAN_FILE_D
            "sSellerSettlementChargesU06FDesc", // LOAN_FILE_D
            "sSellerSettlementChargesU06FHudline", // LOAN_FILE_D
            "sSellerSettlementChargesU07F", // LOAN_FILE_D
            "sSellerSettlementChargesU07FDesc", // LOAN_FILE_D
            "sSellerSettlementChargesU07FHudline", // LOAN_FILE_D
            "sSellerSettlementChargesU08F", // LOAN_FILE_D
            "sSellerSettlementChargesU08FDesc", // LOAN_FILE_D
            "sSellerSettlementChargesU08FHudline", // LOAN_FILE_D
            "sSellerSettlementChargesU09F", // LOAN_FILE_D
            "sSellerSettlementChargesU09FDesc", // LOAN_FILE_D
            "sSellerSettlementChargesU09FHudline", // LOAN_FILE_D
            "sSellerSettlementChargesU10F", // LOAN_FILE_D
            "sSellerSettlementChargesU10FDesc", // LOAN_FILE_D
            "sSellerSettlementChargesU10FHudline", // LOAN_FILE_D
            "sSoftPrepmtPeriodMonths", // LOAN_FILE_D
            "sStipulations", // LOAN_FILE_D
            "sSubmitAmortDesc", // LOAN_FILE_D
            "sSubmitBrokerFax", // LOAN_FILE_D
            "sSubmitBuydownDesc", // LOAN_FILE_D
            "sSubmitDocFull", // LOAN_FILE_D
            "sSubmitDocOther", // LOAN_FILE_D
            "sSubmitImpoundFlood", // LOAN_FILE_D
            "sSubmitImpoundHazard", // LOAN_FILE_D
            "sSubmitImpoundMI", // LOAN_FILE_D
            "sSubmitImpoundOther", // LOAN_FILE_D
            "sSubmitImpoundOtherDesc", // LOAN_FILE_D
            "sSubmitImpoundTaxes", // LOAN_FILE_D
            "sSubmitInitPmtCapR", // LOAN_FILE_D
            "sSubmitMITypeDesc", // LOAN_FILE_D
            "sSubmitMIYes", // LOAN_FILE_D
            "sSubmitProgramCode", // LOAN_FILE_D
            "sSubmitPropTDesc", // LOAN_FILE_D
            "sSubmitRAdjustOtherDesc", // LOAN_FILE_D
            "sTilGfeDueD", // LOAN_FILE_D
            "sTilRedisclosuresReceivedD", // LOAN_FILE_D
            "sTitleInsuranceCostT", // LOAN_FILE_D
            "sTitleReqOwnerNm", // LOAN_FILE_D
            "sTitleReqOwnerPhone", // LOAN_FILE_D
            "sTitleReqPrepD", // LOAN_FILE_D
            "sTotalScorecardLastRunD", // LOAN_FILE_D
            "sTransferPercentLoansOfTheType", // LOAN_FILE_D
            "sUnPdBySellerAssessmentsTaxF", // LOAN_FILE_D
            "sUnPdBySellerAssessmentsTaxFEndD", // LOAN_FILE_D
            "sUnPdBySellerAssessmentsTaxFStartD", // LOAN_FILE_D
            "sUnPdBySellerCityTaxF", // LOAN_FILE_D
            "sUnPdBySellerCityTaxFEndD", // LOAN_FILE_D
            "sUnPdBySellerCityTaxFStartD", // LOAN_FILE_D
            "sUnPdBySellerCountyTaxF", // LOAN_FILE_D
            "sUnPdBySellerCountyTaxFEndD", // LOAN_FILE_D
            "sUnPdBySellerCountyTaxFStartD", // LOAN_FILE_D
            "sUnPdBySellerU1F", // LOAN_FILE_D
            "sUnPdBySellerU1FDesc", // LOAN_FILE_D
            "sUnPdBySellerU1FEndD", // LOAN_FILE_D
            "sUnPdBySellerU1FStartD", // LOAN_FILE_D
            "sUnPdBySellerU2F", // LOAN_FILE_D
            "sUnPdBySellerU2FDesc", // LOAN_FILE_D
            "sUnPdBySellerU2FEndD", // LOAN_FILE_D
            "sUnPdBySellerU2FStartD", // LOAN_FILE_D
            "sUnPdBySellerU3F", // LOAN_FILE_D
            "sUnPdBySellerU3FDesc", // LOAN_FILE_D
            "sUnPdBySellerU3FEndD", // LOAN_FILE_D
            "sUnPdBySellerU3FStartD", // LOAN_FILE_D
            "sUnPdBySellerU4F", // LOAN_FILE_D
            "sUnPdBySellerU4FDesc", // LOAN_FILE_D
            "sUnPdBySellerU4FEndD", // LOAN_FILE_D
            "sUnPdBySellerU4FStartD", // LOAN_FILE_D
            "sUnPdBySellerU5F", // LOAN_FILE_D
            "sUnPdBySellerU5FDesc", // LOAN_FILE_D
            "sUnPdBySellerU5FEndD", // LOAN_FILE_D
            "sUnPdBySellerU5FStartD", // LOAN_FILE_D
            "sUnPdBySellerU6F", // LOAN_FILE_D
            "sUnPdBySellerU6FDesc", // LOAN_FILE_D
            "sUnPdBySellerU6FEndD", // LOAN_FILE_D
            "sUnPdBySellerU6FStartD", // LOAN_FILE_D
            "sUnPdBySellerU7F", // LOAN_FILE_D
            "sUnPdBySellerU7FDesc", // LOAN_FILE_D
            "sUnPdBySellerU7FEndD", // LOAN_FILE_D
            "sUnPdBySellerU7FStartD", // LOAN_FILE_D

            "sFHABorrCertOtherPropCity", // LOAN_FILE_E
            "sFHABorrCertOtherPropCoveredByThisLoanTri", // LOAN_FILE_E
            "sFHABorrCertOtherPropOrigMAmt", // LOAN_FILE_E
            "sFHABorrCertOtherPropStAddr", // LOAN_FILE_E
            "sFHABorrCertOtherPropState", // LOAN_FILE_E
            "sFHABorrCertOtherPropZip", // LOAN_FILE_E
            "sFHABorrCertOwnMoreThan4DwellingsTri", // LOAN_FILE_E
            "sFHAFinancedDiscPtAmt", // LOAN_FILE_E
            "sOldLNm", // LOAN_FILE_E

            "sAccPeriodMon", // LOAN_FILE_F
            "sAccPeriodYr", // LOAN_FILE_F
            "sApprovalCertLastSentD", // LOAN_FILE_F
            "sApprovalCertPdfLastSavedD", // LOAN_FILE_F
            "sApprValPeval", // LOAN_FILE_F
            "sBlitzDocFolderId", // LOAN_FILE_F
            "sBranchIdForNewFileFromTemplate", // LOAN_FILE_F
            "sBrokComp1MldsLckd", // LOAN_FILE_F
            "sCloseProbability", // LOAN_FILE_F
            "sConditionLastModifiedD", // LOAN_FILE_F
            "sConditionsSortOrder", // LOAN_FILE_F
            "sConditionSummaryVersion", // LOAN_FILE_F
            "sConditionSummaryXmlContent", // LOAN_FILE_F
            "sCreditGrade", // LOAN_FILE_F
            "sDataTracLpId", // LOAN_FILE_F
            "sDocMagicFileId", // LOAN_FILE_F
            "sEncompassLoanId", // LOAN_FILE_F
            "sH4HCertificateXmlContent", // LOAN_FILE_F
            "sH4HOriginationReqMetT", // LOAN_FILE_F
            "sIncludeGfeDataForDocMagicComparison", // LOAN_FILE_F
            "sInvestorLockLpePriceGroupId", // LOAN_FILE_F
            "sLCreatedOnLendersOfficeD", // LOAN_FILE_F
            "sLpeNotesFromBrokerToUnderwriterNew", // LOAN_FILE_F
            "sLpeRateOptionIdOf1stLienIn8020", // LOAN_FILE_F
            "sMclCraCode", // LOAN_FILE_F
            "sNonOccCoBorrOnOrigNoteT", // LOAN_FILE_F
            "sPml1stSubmitD", // LOAN_FILE_F
            "sPml1stVisitApplicantStepD", // LOAN_FILE_F
            "sPml1stVisitCreditStepD", // LOAN_FILE_F
            "sPml1stVisitPropLoanStepD", // LOAN_FILE_F
            "sPml1stVisitResultStepD", // LOAN_FILE_F
            "sPmlSubmitStatusT", // LOAN_FILE_F
            "sPmlSummaryPdfLastSavedD", // LOAN_FILE_F
            "sPriceMyLoanFileId", // LOAN_FILE_F
            "sProdFilterDisplayRateMerge", // LOAN_FILE_F
            "sProdFilterDisplayUsingCurrentNoteRate", // LOAN_FILE_F
            "sProdFilterMatchCurrentTerm", // LOAN_FILE_F
            "sProdFilterRestrictResultToCurrentRegistered", // LOAN_FILE_F
            "sProdInvestorRLckdDays", // LOAN_FILE_F
            "sProdInvestorRLckdExpiredD", // LOAN_FILE_F
            "sProdInvestorRLckdModeT", // LOAN_FILE_F
            "sProdLpePriceGroupId", // LOAN_FILE_F
            "sRateLockConfirmationPdfLastSavedD", // LOAN_FILE_F
            "sSettlementChargesExportSource", // LOAN_FILE_F
            "sShippedToWarehouseD", // LOAN_FILE_F
            "sTempLpeTaskMessageXml", // LOAN_FILE_F
            "sTotalScorecardFirstRunD", // LOAN_FILE_F
            "sTotalScoreCreditRiskResultT", // LOAN_FILE_F
            "sTotalScoreDataConflictSourceT", // LOAN_FILE_F
            "sTotalScoreIsIdentityOfInterestException", // LOAN_FILE_F
            "sTotalScorePreReviewResultT", // LOAN_FILE_F
            "sTotalScoreReviewRules", // LOAN_FILE_F
            "sTotalScoreUploadDataFHATransmittal", // LOAN_FILE_F
            "sTotalScoreVersion", // LOAN_FILE_F
            "sLoanDataMigrationAuditHistoryKey", // LOAN_FILE_B - This is a key for file DB, so we never want to duplicate it.
            "sRawSelectedProductCodeFilterKey", // LOAN_FILE_B - Key for a file in FileDB, so we don't want to duplicate it.
            "sInvestorLockProgramId" // LOAN_FILE_F - Investor lock field, not relevant for duplication.

            // DO NOT ADD FIELDS HERE!
            //--------------------------------------------//
            // DO NOT ADD FIELDS HERE!
        };

        private enum TestMode
        {
            Database,
            DataLayer
        }

        public bool ignoreCaseContains(String container, string value)
        {
            return container.IndexOf(value, StringComparison.OrdinalIgnoreCase) >= 0;
        }

        [Test, Explicit]
        public void VerifyDuplicationSprocContainsAllDatabaseFields()
        {
            TestDuplicateSproc(TestMode.Database);
        }

        [Test]
        public void VerifyDuplicationSprocContainsAllDataLayerFields()
        {
            TestDuplicateSproc(TestMode.DataLayer);
        }

        private void TestDuplicateSproc(TestMode mode)
        {
            bool hasException = false;
            string duplicateStoredProcedure = "";

            try
            {
                using (var conn = DbAccessUtils.GetConnection(DataSrc.LOShare))
                {
                    conn.OpenWithRetry();
                    using (var command = conn.CreateCommand())
                    {
                        command.CommandText =
                        @"
                            select obj.Name spName, sc.TEXT spText
                            From sys.syscomments sc Inner Join sys.objects obj on sc.Id = obj.OBJECT_ID
                            Where obj.Name = 'DuplicateLoanFile' AND TYPE='P';";


                        string getSchema =
                        @"select TOP 0 * from Loan_File_A a
                            INNER JOIN Loan_File_B b ON a.sLID = b.sLID
                            INNER JOIN Loan_File_C c ON b.sLID = c.sLID
                            INNER JOIN Loan_File_D d ON c.sLID = d.sLID
                            INNER JOIN Loan_File_E e ON d.sLID = e.sLID
                            INNER JOIN Loan_File_F f ON e.sLID = f.sLID
                            INNER JOIN APPLICATION_A aA ON f.sLID = aA.sLID
                            INNER JOIN APPLICATION_B aB ON aA.sLID = aB.sLID";

                        var getEmptyTable = conn.CreateCommand();
                        getEmptyTable.CommandText = getSchema;
                        String columns = "";
                        using (DbDataReader reader = command.ExecuteReader(CommandBehavior.Default))
                        {
                            while (reader.Read())
                            {
                                duplicateStoredProcedure += reader.GetString(1);
                            }

                            string[] insertStatements = duplicateStoredProcedure.Split(new string[] { "INSERT INTO", "insert into" }, StringSplitOptions.RemoveEmptyEntries);
                            foreach (String insertStmt in insertStatements)
                            {
                                if (ignoreCaseContains(insertStmt, "LOAN_FILE_A ") ||
                                    ignoreCaseContains(insertStmt, "Loan_File_B ") ||
                                    ignoreCaseContains(insertStmt, "Loan_File_C ") ||
                                    ignoreCaseContains(insertStmt, "Loan_File_D ") ||
                                    ignoreCaseContains(insertStmt, "Loan_File_E ") ||
                                    ignoreCaseContains(insertStmt, "Loan_File_F ") ||
                                    ignoreCaseContains(insertStmt, "APPLICATION_A") ||
                                    ignoreCaseContains(insertStmt, "Application_B"))
                                {

                                    string[] columnArray = insertStmt.Split(new string[] { ")" }, StringSplitOptions.RemoveEmptyEntries);
                                    columns += columnArray[1];
                                }
                            }
                        }

                        DataRowCollection columnCollection;
                        using (DbDataReader reader = getEmptyTable.ExecuteReader(CommandBehavior.CloseConnection))
                        {
                            columnCollection = reader.GetSchemaTable().Rows;

                        }
                        conn.Close();
                        string missingCol = "";
                        foreach (DataRow col in columnCollection)
                        {
                            var columnName = col[0].ToString();
                            if (!ignoreCaseContains(columns, columnName) 
                                && !whitelist.Contains(columnName, StringComparer.OrdinalIgnoreCase)
                                && (mode == TestMode.Database || ColumnHasPropertyInDataLayer(columnName)))
                            {
                                missingCol += (missingCol.Length == 0 ? "" : ", " ) + columnName;
                                hasException = true;
                            }
                        }

                        if (hasException)
                        {
                            Assert.Fail("The following columns are not being duplicated by the stored procedure DuplicateLoanFile: " + missingCol);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Assert.Fail("Exception: " + e.ToString());
            }
        }

        private static bool ColumnHasPropertyInDataLayer(string columnName)
        {
            foreach (var testClass in DependencyUtils.DependencyClasses)
            {
                var prop = testClass.GetProperty(columnName, DependencyUtils.BindingFlags);

                if (prop != null)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
