﻿namespace LendOSolnTest.DB
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Text;
    using Common;
    using DataAccess;
    using NUnit.Framework;
    using LendersOffice.Admin;
    using LendersOffice.Constants;
    using LendersOfficeApp.LOAdmin;
    using LendersOfficeApp.LOAdmin.Manage;
    using LendersOfficeApp.los.RatePrice.Helpers;
    using LendersOfficeApp.los.RatePrice;
    using LendersOffice.Drivers.SqlServerDB;
    using LqbGrammar.DataTypes;
    using LendersOffice.Migration;
    using LendersOfficeApp.ObjLib.AcceptableRatesheet;
    using LendersOffice.Conversions.LON;
    using LendersOffice.ObjLib.Conversions.LoanFileImportExport;
    using LendersOfficeApp.ObjLib.Licensing;
    using LendersOffice.ConfigSystem;
    using LendersOffice.ObjLib.Loan.Security;
    using LendersOffice.ObjLib.LoanFileCache;
    using LendersOfficeApp.ObjLib.LoanLimitsData;
    using LendersOffice.Security;
    using LendersOffice.RatePrice;
    using LendersOfficeApp.newlos;
    using global::CommonProjectLib.Common.Lib;
    using System.Reflection;
    using System.Data.Common;
    using LendersOffice.ObjLib.DynaTree;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public class NewAdHocQueryCodeTest
    {
        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void LOApp_LOAdmin_Manage_Update_GatherLoansAndConnections()
        {
            string[] ids = new string[] { "D45B90EA-5C75-4F0E-8521-86235402DF2B", "5BB9BBC5-FD60-4537-B5A1-B82B4E47BEF3", "C01CCF39-82C6-4242-B452-C3DD23832399" };
            var loansAndConnections = new List<Tuple<DbConnectionInfo, Guid, Guid>>();
            Assert.IsTrue(loansAndConnections.Count == 0);

            UpdateForm.GatherLoansAndConnections(ids, loansAndConnections, false, null);
            Assert.IsTrue(loansAndConnections.Count > 0);
        }

        [Test]
        [Ignore("Snapshot failure")]
        public void LOApp_LOAdmin_PricePolicyLookup_RetrievePricePolicyList()
        {
            string search = "STATE_DE";
            DateTime version = new DateTime(2016, 08, 22);
            DateTime lastModified = new DateTime(2016, 08, 26);
            var policyList = new List<PricePolicyRow>();
            var lastRelease = new LendersOfficeApp.los.RatePrice.CLpeReleaseInfo(version, ConstAppThinh.LpeSnapshot1Id, lastModified, SHA256Checksum.Invalid);
            Assert.AreEqual(0, policyList.Count);

            PricePolicyLookup.RetrievePricePolicyList(search, lastRelease, policyList, false);
            Assert.IsTrue(policyList.Count > 0);
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void LOApp_los_RatePrice_PricePolicyListByProductService_CheckExistenceOfLoanProgramTemplate()
        {
            Guid priceGroupId = Guid.Parse("86F50754-4E69-4346-B596-000B56865A88");

            bool exist = PricePolicyListByProductService.CheckExistenceOfLoanProgramTemplate(priceGroupId);
            Assert.IsTrue(exist);
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void LOApp_los_RatePrice_PricePolicyListByProductService_ListPmiProductsInFolderByMasterId()
        {
            Guid pmiProductId = Guid.Parse("49D2148D-CC5A-40B0-AB68-140700979EB4");
            var folderContents = new List<string>();

            PricePolicyListByProductService.ListPmiProductsInFolderByMasterId(pmiProductId, folderContents);
            // lack of an exception is the signal that the sql query code works ok
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void LOApp_los_RatePrice_PricePolicyListByProductService_FillPmiAssociations()
        {
            Guid pmiProductId = Guid.Parse("49D2148D-CC5A-40B0-AB68-140700979EB4");

            using (CDataSet pmiAssociations = new CDataSet())
            {
                Assert.IsTrue(pmiAssociations.Tables.Count == 0);
                PricePolicyListByProductService.FillPmiAssociations(pmiProductId, pmiAssociations);
                Assert.IsTrue(pmiAssociations.Tables.Count > 0);
            }
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void LOApp_los_WebService_Loan_GetAppCodeForBrokerId()
        {
            Guid brokerId = Guid.Parse("EAA183AC-6EBD-4072-B4AC-01CB65C50E61");

            Guid? appCode = LendersOfficeApp.WebService.Loan.GetAppCodeForBrokerId(brokerId);
            Assert.IsTrue(appCode.HasValue);
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void LOApp_los_WebService_Loan_ClearLoanIdIfInvalid()
        {
            Guid loanId = Guid.NewGuid();
            Assert.AreNotEqual(Guid.Empty, loanId);

            Guid newLoanId = LendersOfficeApp.WebService.Loan.ClearLoanIdIfInvalid(loanId);
            Assert.AreEqual(Guid.Empty, newLoanId);
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void LOLib_DataAccess_CCTemplateData_PopulateDataSet()
        {
            Guid brokerId = Guid.Parse("E9F006C9-CB83-4531-8925-08FCCF4ADD63");
            Guid fileId = Guid.Parse("9B8356EB-BBBF-4C60-BA2C-0000595B024C");

            var sql = "SELECT * FROM CC_TEMPLATE WHERE cCcTemplateId = @fileid";
            var parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@fileid", fileId));

            var ds = new DataSet("TEMPLATE");
            Assert.IsTrue(ds.Tables.Count == 0);

            using (DbConnection conn = DbAccessUtils.GetConnection(brokerId))
            {
                CCcTemplateBase.PopulateDataSet(conn, sql, parameters, ds);
            }

            Assert.IsTrue(ds.Tables.Count > 0);
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void LOLib_DataAccess_LODataSet_UpdateData()
        {
            Guid brokerId = Guid.Parse("E9F006C9-CB83-4531-8925-08FCCF4ADD63");

            string sql = "select * from broker where brokerid = @brokerId";
            var listParams = new List<SqlParameter>();
            listParams.Add(new SqlParameter("@brokerId", brokerId));

            var loDS = new CDataSet();
            loDS.LoadWithSqlQueryDangerously(brokerId, sql, listParams);
            Assert.AreEqual(1, loDS.Tables.Count);

            string currentValue = Convert.ToString(loDS.Tables[0].Rows[0]["LeadStatusDesc"]);
            string newValue = "CRAZY DIAMOND";

            loDS.Tables[0].Rows[0]["LeadStatusDesc"] = newValue;
            int count = CDataSet.UpdateData(loDS);
            Assert.AreEqual(1, count);

            loDS.Tables[0].Rows[0]["LeadStatusDesc"] = currentValue;
            count = CDataSet.UpdateData(loDS);
            Assert.AreEqual(1, count);
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void LOLib_DataAccess_LoanProgramTemplateData_LoadAllProductFolders()
        {
            DataSet folders = new DataSet();
            Assert.IsTrue(folders.Tables.Count == 0);

            CLoanProductBase.LoadAllProductFolders(folders);
            Assert.IsTrue(folders.Tables.Count > 0);
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void LOLib_DataAccess_LoanProgramTemplateData_PopulateLoanProducts()
        {
            var fileId = Guid.Parse("85752322-73A2-4A80-9C41-00003A029B0F");
            var dataSource = DataSrc.LpeSrc;

            string sql = "select * from loan_program_template where lLpTemplateId = @fileId";
            var listParams = new List<SqlParameter>();
            listParams.Add(new SqlParameter("@fileId", fileId));

            var ds = new DataSet("TEMPLATE");
            CLoanProductBase.PopulateLoanProducts(dataSource, sql, listParams, ds);
            Assert.IsTrue(ds.Tables.Count > 0);
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void LOLib_DataAccess_LoanProgramTemplateData_InitForSaveLoanProducts()
        {
            var fileId = Guid.Parse("85752322-73A2-4A80-9C41-00003A029B0F");
            using (DbConnection conn = DbAccessUtils.GetConnection(DataSrc.LpeSrc))
            {
                var ds = new DataSet("TEMPLATE");

                string sql = "select * from loan_program_template where lLpTemplateId = @fileId";
                var listParams = new List<SqlParameter>();
                listParams.Add(new SqlParameter("@fileId", fileId));
                var tuple = new Tuple<string, List<SqlParameter>>(sql, listParams);

                CLoanProductBase.InitForSaveLoanProducts(conn, ds, tuple);
                Assert.AreEqual(1, ds.Tables.Count);
            }
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void LOLib_Migration_LoanDataMigrationsRunner_SelectLoanVersion()
        {
            Guid brokerId = Guid.Parse("E9F006C9-CB83-4531-8925-08FCCF4ADD63");
            var loanId = Guid.Parse("CFDC826B-D075-4C82-BBA1-19EAAEB4F7DF");

            string selectStatement = "SELECT sLoanVersionT FROM LOAN_FILE_B WHERE sLId=@sLId";
            SqlParameter[] parameters =
            {
                new SqlParameter("@sLId", loanId)
            };

            int? loanVersion = LoanDataMigrationsRunner.SelectLoanVersion(brokerId, selectStatement, parameters);
            Assert.IsTrue(loanVersion.HasValue);
            Assert.IsTrue(loanVersion.Value > 0);
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void LOLib_ObjLib_Conversions_LONXml_WriteLoanAgent()
        {
            using (System.Xml.XmlWriter writer = new System.Xml.XmlTextWriter(new System.IO.MemoryStream(), System.Text.Encoding.ASCII))
            {
                Guid brokerId = Guid.Parse("E9F006C9-CB83-4531-8925-08FCCF4ADD63");
                var loanId = Guid.Parse("80B566CD-6E74-49C8-BC98-000004A44928");

                bool loanOfficer = false;
                bool processor = false;
                bool sellingAgent = false;

                LoanLONXmlWriter.WriteLoanAgent(writer, loanId, brokerId, ref loanOfficer, ref processor, ref sellingAgent);
                // lack of an exception signals a successful test
            }
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void LOLib_ObjLib_Conversions_LoanFileImportExport_ShareImportExport_ProcessTokens()
        {
            string[] tokens = new string[] { "First", "Second", "Third" };
            var sb = ShareImportExport.GetQueryFromTableAndTokens(tokens, tokens.Length, "broker");
            Assert.IsTrue(sb.Length > 0);
        }

        [Test]
        [Ignore("THE METHOD UNDER TEST IS VERY OBSCURE AND IT WILL BE DIFFICULT TO CONSTRUCT THE INPUT ARGUMENTS -- MANUALLY TEST INSTEAD ==> LOAdmin")]
        public void LOLib_ObjLib_Conversions_LoanFileImportExport_ShareImportExport_HandleLOMain()
        {
            // VERIFIED MANUALLY
        }

        [Test]
        [Ignore("THE METHOD UNDER TEST IS VERY OBSCURE AND IT WILL BE DIFFICULT TO CONSTRUCT THE INPUT ARGUMENTS -- MANUALLY TEST INSTEAD ==> LOAdmin")]
        public void LOLib_ObjLib_Conversions_LoanFileImportExport_ShareImportExport_HandleDataSrc()
        {
            // VERIFIED MANUALLY (gets by the db access code and runs into the same exception is the old code does)
        }

        [Test]
        [Ignore("THIS METHOD WILL BE COMPLEX TO SETUP CORRECTLY, BUT THERE ARE NO CLIENTS CALLING IT - NOT WORTH THE TIME TO INVEST CREATING THE UNIT TEST ==> LOAdmin")]
        public void LOLib_ObjLib_Conversions_LoanFileImportExport_TableReplicateTool_AddIdKeyMapByUniqueIndexNEW()
        {
            // THE METHOD HAS ZERO REFERENCES AND A SEARCH ON THE METHOD NAME SHOWS NO CALLS FROM SCRIPT CODE.
            // THIS DOESN'T GUARANTEE THE METHOD ISN'T USED, BUT I WON'T BE ABLE TO MANUALLY TEST IT.  I'LL LEAVE AS-IS WITHOUT TESTING.
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void LOLib_ObjLib_Conversions_LoanFileImportExport_TableReplicateTool_CheckExist_DataSrc()
        {
            string sql = "select top 1 * from [loan_field] where LoanFieldDesc = @desc";
            var listParams = new List<SqlParameter>();
            listParams.Add(DbAccessUtils.SqlParameterForVarchar("@desc", "Loan Number"));

            bool exists = TableReplicateTool.CheckExist(DataSrc.LOTransient, Guid.Empty, false, listParams, sql);
            Assert.IsTrue(exists);
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void LOLib_ObjLib_Conversions_LoanFileImportExport_TableReplicateTool_CheckExist_BrokerId()
        {
            var brokerId = Guid.Parse("8796D0F4-B99E-4747-A124-005DD9E32BFB");

            string sql = "select top 1 * from [broker] where BrokerNm = @name";
            var listParams = new List<SqlParameter>();
            listParams.Add(DbAccessUtils.SqlParameterForVarchar("@name", "ALBORZI"));

            bool exists = TableReplicateTool.CheckExist(DataSrc.LOShare, brokerId, false, listParams, sql);
            Assert.IsTrue(exists);
        }

        [Test]
        [Ignore("THE SETUP CODE IS TOO COMPLICATED FOR A SIMPLE UNIT TEST - HANDLE VIA MANUAL TESTING ==> LOAdmin")]
        public void LOLib_ObjLib_Conversions_LoanFileImportExport_TableReplicateTool_NonIdentityTableNEW()
        {
            // REQUIRES A PRE-EXISTING XML FILE WITH DATA TO IMPORT BUT I DON'T SEE HOW TO GENERATE THIS FILE
            // AND IT WOULD BE PAINFUL TO CREATE BY HAND.  INSPECTING THE CODE SHOWS THAT PARSING THE FILE
            // IS THE SAME FOR OLD AND NEW CODE, THE DIFFERENCE IS AFTER THE SQL QUERY AND THE PARAMETERS ARE
            // EXTRACTED FROM THE XML.  THE CODE LOOKS LIKE IT IS GOOD GIVEN THE QUERY AND PARAMETERS SO
            // I'M GOING TO GIVE UP ON TRYING TO MANUALLY TEST THIS.
        }

        [Test]
        [Ignore("THE SETUP CODE IS TOO COMPLICATED FOR A SIMPLE UNIT TEST - HANDLE VIA MANUAL TESTING ==> LOAdmin")]
        public void LOLib_ObjLib_Conversions_LoanFileImportExport_TableReplicateTool_PushQueryResultNEW()
        {
            // THIS APPEARS TO BE CALLED BY CODE THAT WAS MANUALLY TESTED ABOVE SO I'M GOING TO SKIP THIS.
        }

        [Test]
        [Ignore("THE SETUP CODE IS TOO COMPLICATED FOR A SIMPLE UNIT TEST - HANDLE VIA MANUAL TESTING ==> LOAdmin")]
        public void LOLib_ObjLib_Conversions_LoanFileImportExport_TableReplicateTool_FindNextEntryListNEW()
        {
            // THIS APPEARS TO BE CALLED BY CODE THAT WAS MANUALLY TESTED ABOVE SO I'M GOING TO SKIP THIS.
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void LOLib_ObjLib_Licensing_BrokerUser_CheckUserMatchesLicense()
        {
            var brokerId = Guid.Parse("3DA66720-803A-4249-BDA0-0BE6D1E3A064");
            var userId = Guid.Parse("896936FC-F2B8-4F04-AAC8-0002091D5E2C");
            var ownerId = Guid.Parse("3DA66720-803A-4249-BDA0-0BE6D1E3A064");

            BrokerUser.CheckUserMatchesLicense(brokerId, userId, ownerId);
            // lack of exception signals test success
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void LOLib_ObjLib_Licensing_BrokerUser_UpdateLicense()
        {
            var brokerId = Guid.Parse("3DA66720-803A-4249-BDA0-0BE6D1E3A064");
            var userId = Guid.Parse("723921C5-851F-426E-A787-C39082F1EC9F");
            var testLicenseId = Guid.Parse("07E2F0B5-3859-45EE-B682-00F30C7D4E4E");

            var currentValue = GetCurrentLicense(brokerId, userId);

            BrokerUser.UpdateLicense(brokerId, userId, testLicenseId);
            var newValue = GetCurrentLicense(brokerId, userId);
            Assert.AreEqual(testLicenseId, newValue);

            SetCurrentLicenseId(brokerId, userId, currentValue);
            newValue = GetCurrentLicense(brokerId, userId);
            Assert.AreEqual(currentValue, newValue);
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void LOLib_ObjLib_Licensing_BrokerUser_RetrieveCurrentLicense()
        {
            var brokerId = Guid.Parse("3DA66720-803A-4249-BDA0-0BE6D1E3A064");
            var userId = Guid.Parse("723921C5-851F-426E-A787-C39082F1EC9F");

            var licenseId = BrokerUser.RetrieveCurrentLicense(brokerId, userId);
            Assert.AreNotEqual(Guid.Empty, licenseId);
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void LOLib_ObjLib_Licensing_BrokerUser_SetCurrentLicenseNull()
        {
            var brokerId = Guid.Parse("3DA66720-803A-4249-BDA0-0BE6D1E3A064");
            var userId = Guid.Parse("723921C5-851F-426E-A787-C39082F1EC9F");

            Guid current = GetCurrentLicense(brokerId, userId);
            Assert.AreNotEqual(Guid.Empty, current);

            BrokerUser.SetCurrentLicenseNull(brokerId, userId);
            Guid tempVal = GetCurrentLicense(brokerId, userId);
            Assert.AreEqual(Guid.Empty, tempVal);

            SetCurrentLicenseId(brokerId, userId, current);
            tempVal = GetCurrentLicense(brokerId, userId);
            Assert.AreEqual(current, tempVal);
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void LOLib_ObjLib_Licensing_BrokerUser_RetrieveEmployeeId()
        {
            var brokerId = Guid.Parse("3DA66720-803A-4249-BDA0-0BE6D1E3A064");
            var userId = Guid.Parse("723921C5-851F-426E-A787-C39082F1EC9F");

            var empId = BrokerUser.RetrieveEmployeeId(brokerId, userId);
            Assert.AreNotEqual(Guid.Empty, empId);
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void LOLib_ObjLib_Licensing_BrokerUser_CheckLastRenewalPage_NULL()
        {
            var brokerId = Guid.Parse("3DA66720-803A-4249-BDA0-0BE6D1E3A064");
            var userId = Guid.Parse("723921C5-851F-426E-A787-C39082F1EC9F");

            bool display = BrokerUser.CheckLastRenewalPage(brokerId, userId);
            Assert.IsTrue(display);
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void LOLib_ObjLib_Licensing_BrokerUser_CheckLastRenewalPage_HasDate()
        {
            var brokerId = Guid.Parse("3DA66720-803A-4249-BDA0-0BE6D1E3A064");
            var userId = Guid.Parse("A91C162F-174D-4499-A168-04C1427D06D7");

            bool display = BrokerUser.CheckLastRenewalPage(brokerId, userId);
            Assert.IsTrue(display);
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void LOLib_ObjLib_Licensing_Licensee_CreateLicenseRecord()
        {
            var brokerId = Guid.Parse("3DA66720-803A-4249-BDA0-0BE6D1E3A064");

            Guid licenseId = Licensee.CreateLicenseRecord(brokerId, 500, "TESTING", Guid.Empty, DateTime.Today.AddMonths(1));
            Assert.AreNotEqual(Guid.Empty, licenseId);

            int rowCount = DeleteLicenseRecord(brokerId, licenseId);
            Assert.AreEqual(1, rowCount);
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void LOLib_ObjLib_Licensing_Licensee_UpdateLicense()
        {
            var brokerId = Guid.Parse("3DA66720-803A-4249-BDA0-0BE6D1E3A064");
            var userId = Guid.Parse("A91C162F-174D-4499-A168-04C1427D06D7");

            var currLicenseId = GetCurrentLicense(brokerId, userId);
            var newLicenseId = AddLicenseRecord(brokerId);
            Assert.AreNotEqual(currLicenseId, newLicenseId);

            Licensee.UpdateLicense(brokerId, currLicenseId, newLicenseId);
            var nowId = GetCurrentLicense(brokerId, userId);
            Assert.AreEqual(newLicenseId, nowId);

            Licensee.UpdateLicense(brokerId, newLicenseId, currLicenseId);
            nowId = GetCurrentLicense(brokerId, userId);
            Assert.AreEqual(currLicenseId, nowId);

            int deletedCount = DeleteLicenseRecord(brokerId, newLicenseId);
            Assert.AreEqual(1, deletedCount);
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void LOLib_ObjLib_Licensing_Licensee_UpdatePaymentRecord()
        {
            var brokerId = Guid.Parse("3DA66720-803A-4249-BDA0-0BE6D1E3A064");
            var paymentId = Guid.Parse("3354C791-4C80-4DC4-B50B-2D5166EA0C08");

            var currLicenseId = GetPaymentLicenseId(brokerId, paymentId);
            var newLicenseId = AddLicenseRecord(brokerId);
            Assert.AreNotEqual(currLicenseId, newLicenseId);

            Licensee.UpdatePaymentRecord(brokerId, paymentId, newLicenseId);
            var nowId = GetPaymentLicenseId(brokerId, paymentId);
            Assert.AreEqual(newLicenseId, nowId);

            Licensee.UpdatePaymentRecord(brokerId, paymentId, currLicenseId);
            nowId = GetPaymentLicenseId(brokerId, paymentId);
            Assert.AreEqual(currLicenseId, nowId);

            int deletedCount = DeleteLicenseRecord(brokerId, newLicenseId);
            Assert.AreEqual(1, deletedCount);
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void LOLib_ObjLib_Loan_Security_LoanReadPermissionResolver_FillLoanList()
        {
            var brokerId = Guid.Parse("3DA66720-803A-4249-BDA0-0BE6D1E3A064");
            var fields = LendingQBExecutingEngine.GetDependencyFieldsByOperation(brokerId, LendersOffice.ConfigSystem.Operations.WorkflowOperations.ReadLoanOrTemplate);

            var loanIds = new Guid[]
            {
                Guid.Parse("80B566CD-6E74-49C8-BC98-000004A44928"),
                Guid.Parse("04ABDDA1-2AB5-4941-A43B-0000248BD096"),
                Guid.Parse("2E6862CC-1DDD-4FBC-B892-00003077DF2E")
            };

            DataSet ds = new DataSet();
            LoanReadPermissionResolver.FillLoanList(brokerId, loanIds, fields, ds, false);
            Assert.IsTrue(ds.Tables.Count > 0);
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void LOLib_ObjLib_LoanFileCache_LoanUpdater_UpdateFirstProdCurrPIPmtLckd_Zero()
        {
            // setup
            var brokerId = Guid.Parse("3DA66720-803A-4249-BDA0-0BE6D1E3A064");
            var loanId = Guid.Parse("271A5834-093D-4B36-A9D5-04F791FF73A3");

            int? currValue = GetProdCurrPIPmtLckd(brokerId, loanId);
            Assert.IsTrue(currValue.HasValue);
            Assert.IsTrue(currValue.Value > 0);

            // test
            LoanUpdater.UpdateFirstProdCurrPIPmtLckd(brokerId, loanId, false);
            int? nowValue = GetProdCurrPIPmtLckd(brokerId, loanId);
            Assert.IsTrue(nowValue.HasValue);
            Assert.IsTrue(0 == nowValue.Value);

            // restore
            SetProdCurrPIPmtLckd(brokerId, loanId, currValue.Value);
            nowValue = GetProdCurrPIPmtLckd(brokerId, loanId);
            Assert.IsTrue(nowValue.HasValue);
            Assert.AreEqual(currValue.Value, nowValue.Value);
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void LOLib_ObjLib_LoanFileCache_LoanUpdater_UpdateFirstProdCurrPIPmtLckd_Null()
        {
            // setup
            var brokerId = Guid.Parse("3DA66720-803A-4249-BDA0-0BE6D1E3A064");
            var loanId = Guid.Parse("271A5834-093D-4B36-A9D5-04F791FF73A3");

            int? currValue = GetProdCurrPIPmtLckd(brokerId, loanId);
            Assert.IsTrue(currValue.HasValue);
            Assert.IsTrue(currValue.Value > 0);

            // test
            LoanUpdater.UpdateFirstProdCurrPIPmtLckd(brokerId, loanId, true);
            int? nowValue = GetProdCurrPIPmtLckd(brokerId, loanId);
            Assert.IsFalse(nowValue.HasValue);

            // restore
            SetProdCurrPIPmtLckd(brokerId, loanId, currValue.Value);
            nowValue = GetProdCurrPIPmtLckd(brokerId, loanId);
            Assert.IsTrue(nowValue.HasValue);
            Assert.AreEqual(currValue.Value, nowValue.Value);
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void LOLib_ObjLib_LoanLimits_FHALoanLimits_HandleConformingLoanLimits()
        {
            var data = new Dictionary<int, SortedDictionary<DateTime, LoanLimit>>();

            LoanLimit.HandleConformingLoanLimits(data);
            Assert.IsTrue(data.Count > 0);
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void LOLib_ObjLib_LoanLimits_FHALoanLimits_HandleFhaLoanLimits()
        {
            var data = new Dictionary<int, SortedDictionary<DateTime, LoanLimit>>();

            LoanLimit.HandleFhaLoanLimits(data);
            Assert.IsTrue(data.Count > 0);
        }

        [Test]
        [Ignore("THE SETUP CODE IS TOO COMPLICATED FOR A SIMPLE UNIT TEST - HANDLE VIA MANUAL TESTING ==> LOAdmin or Webservice")]
        public void LOLib_ObjLib_LoanSearch_Basic_LoanSearch_FindLoansByApplicantsNEW()
        {
            // VERIFIED MANUALLY
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void LOLib_RatePrice_DerivationJob_CheckTargetFolderExists()
        {
            var targetBrokerId = Guid.Parse("C3953119-D9B6-4CC5-B322-29EC3566DBE7");
            var targetId = Guid.Parse("41CADEDD-A2F3-402F-AB40-0001200E7964");

            DerivationJob.CheckTargetFolderExists(targetBrokerId, targetId);
            // lack of exception is signal of test success
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void LOLib_RatePrice_LpGlobal_RetrieveGlobalPriceGroupAssociations()
        {
            var policyId = Guid.Parse("B857A327-EC3C-454E-A700-000598BC91F6");

            var sb = new StringBuilder();
            var parameters = new List<SqlParameter>();

            sb.Append("PricePolicyId = @policyId");
            parameters.Add(new SqlParameter("@policyId", policyId));

            var firstResult = new List<LpGlobalPriceGroupAssocItem>();
            CLpGlobal.RetrieveGlobalPriceGroupAssociations(parameters, sb, firstResult);
            Assert.IsTrue(firstResult.Count > 0);
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void LOLib_RatePrice_LpGlobal_RetrieveLpePriceGroupAssociations()
        {
            var sb = new StringBuilder();
            var parameters = new List<SqlParameter>();

            sb.Append("IsValid = @validFlag");
            parameters.Add(new SqlParameter("@validFlag", 1));

            var secondResult = new List<LpGlobalPriceGroupAssocItem>();
            CLpGlobal.RetrieveLpePriceGroupAssociations(parameters, sb, secondResult);
            Assert.IsTrue(secondResult.Count > 0);
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void LOLib_RatePrice_LpeDataVerify_RetrieveLoanProgramTemplates()
        {
            using (var ds = LpeDataVerify.RetrieveLoanProgramTemplates())
            {
                Assert.IsTrue(ds.Tables.Count > 0);
                Assert.IsTrue(ds.Tables[0].Rows.Count > 0);
            }
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void LOLib_RatePrice_LpeReleaseMonitor_PullMaxLogTime()
        {
            var pulled = LpeReleaseMonitor.PullMaxLogTime();
            Assert.IsFalse(pulled == DateTime.MinValue);
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void ScheduledExecutable_CheckExpiredPricing_GetEmailSender()
        {
            var list = new List<string>();

            GetEmailSender(list, "WELLSRS.XLS");
            Assert.IsTrue(list.Count > 0);
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void ScheduledExecutable_EDocBrokerExport_PolulateExportList()
        {
            Guid brokerId = Guid.Parse("839127EF-72D1-4873-BFDF-18A23416B146");
            bool hasData = ScheduledExecutable_EDocBrokerExport_PolulateExportList(brokerId);
            Assert.IsTrue(hasData);
        }

        [Test]
        [Ignore("Timing out")]
        public void ScheduledExecutable_Migrations_MigrateCCArchiveLoanDataToFileDB_GetLoanIdsWithNonEmptyClosingCostArchiveXmlNEW()
        {
            var list = GetLoanIdsWithNonEmptyClosingCostArchiveXmlNEW();
            var guidList = list as List<Guid>;
            Assert.IsTrue(guidList.Count > 0);
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void ScheduledExecutable_Migrations_MigrateCCArchiveLoanDataToFileDB_ListLoansForAprCalculationMigrationNEW()
        {
            var list = ListLoansForAprCalculationMigrationNEW();
            var guidList = list as List<Guid>;
            Assert.IsTrue(guidList.Count > 0);
        }

        [Test]
        [Ignore("THERE ARE CONSEQUENCES TO RUNNING THIS THAT MAY BE TOO DANGEROUS FOR A UNIT TEST - TEST MANUALLY")]
        public void ScheduledExecutable_Migrations_TaskMigration_RollbackToOldConditionNEW()
        {
            // THE CHANGE TO THE LOGIC IS VERY SIMPLE AND LOOKS GOOD, I WON'T RISK TESTING THIS ONE
        }

        [Test]
        [Ignore("THERE ARE CONSEQUENCES TO RUNNING THIS THAT MAY BE TOO DANGEROUS FOR A UNIT TEST - TEST MANUALLY")]
        public void ScheduledExecutable_Migrations_TaskMigration_RollbackNewTaskToOldTaskNEW()
        {
            // THE CHANGE TO THE LOGIC IS VERY SIMPLE AND LOOKS GOOD, I WON'T RISK TESTING THIS ONE
        }

        [Test]
        [Ignore("THERE ARE CONSEQUENCES TO RUNNING THIS THAT MAY BE TOO DANGEROUS FOR A UNIT TEST - TEST MANUALLY")]
        public void ScheduledExecutable_Migrations_TaskMigration_ConvertOldTaskToNewTaskNEW()
        {
            // THE CHANGES TO THE LOGIC ARE VERY SIMPLE AND LOOK GOOD, I WON'T RISK TESTING THIS ONE
        }

        [Test]
        [Ignore("THERE ARE CONSEQUENCES TO RUNNING THIS THAT MAY BE TOO DANGEROUS FOR A UNIT TEST - TEST MANUALLY")]
        public void ScheduledExecutable_Migrations_TaskMigration_CreateToDoListLoanNEW()
        {
            // THE CHANGES TO THE LOGIC ARE VERY SIMPLE AND LOOK GOOD, I WON'T RISK TESTING THIS ONE
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void ScheduledExecutable_Migrations_TaskMigration_ListActiveParticipantsNEW()
        {
            Guid brokerId = new Guid("EAA183AC-6EBD-4072-B4AC-01CB65C50E61");
            var discLogId = Guid.Parse("693B2772-9A75-4395-A534-C137D6AE4C82");

            var list = ListActiveParticipantsNEW(brokerId, discLogId);
            Assert.IsTrue(list.Count > 0);
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void ScheduledExecutable_Migrations_TaskMigration_GetLoanInfoNEW()
        {
            Guid brokerId = new Guid("EAA183AC-6EBD-4072-B4AC-01CB65C50E61");
            Guid lId = new Guid("E59001CF-34EE-49F8-8200-40387F510B76");
            var dictionary = new Dictionary<Guid, LoanInfo>();

            var loanInfo = GetLoanInfoNEW(brokerId, lId, dictionary);
            Assert.IsNotNull(loanInfo);
            Assert.IsTrue(dictionary.Count > 0);
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void ScheduledExecutable_Migrations_TaskMigration_ListActiveLoUsersNEW()
        {
            // NOTE: This seems like a transient method so the following brokerid may no longer work after the first run
            Guid brokerId = new Guid("320E8706-D026-4D1A-BB23-00F89A250220");

            var list = ListActiveLoUsersNEW(brokerId);
            Assert.IsTrue(list.Count > 0);
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void ScheduledExecutable_Migrations_TaskMigration_GetAccountOwnerNEW()
        {
            // NOTE: This seems like a transient method so the following brokerid may no longer work after the first run
            Guid brokerId = new Guid("8C6BD74A-D69C-413F-9261-D2C225341A12");

            var ownerId = GetAccountOwnerNEW(brokerId);
            Assert.IsTrue(ownerId != Guid.Empty);
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void ScheduledExecutable_Migrations_TaskMigration_GetGeneralTaskPermissionLevelIdNEW()
        {
            Guid brokerId = new Guid("8796D0F4-B99E-4747-A124-005DD9E32BFB");

            int permissionLevel = GetGeneralTaskPermissionLevelIdNEW(brokerId);
            Assert.IsTrue(permissionLevel > 0);
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void ScheduledExecutable_Migrations_TaskMigration_GetManagedTaskPermissionLevelIdNEW()
        {
            Guid brokerId = new Guid("8796D0F4-B99E-4747-A124-005DD9E32BFB");

            int permissionLevel = GetGeneralTaskPermissionLevelIdNEW(brokerId);
            Assert.IsTrue(permissionLevel > 0);
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void ScheduledExecutable_Migrations_TaskMigration_ListActiveLoansNEW()
        {
            // NOTE: This seems like a transient method so the following brokerid may no longer work after the first run
            Guid brokerId = new Guid("CCA3C74F-380C-48F0-A9FE-BE6470F55087");

            var list = ListActiveLoansNEW(brokerId);
            Assert.IsTrue(list.Count > 0);
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void ScheduledExecutable_Migrations_TaskMigration_GetUserIdByEmployeeFullNameNEW()
        {
            Guid brokerId = new Guid("3DA66720-803A-4249-BDA0-0BE6D1E3A064");
            string employee = "MICHELE YAMASHIRO";
            var dictionary = new Dictionary<string, Guid>();

            var userId = GetUserIdByEmployeeFullNameNEW(dictionary, brokerId, employee);
            Assert.AreNotEqual(Guid.Empty, userId);
            Assert.IsTrue(dictionary.Count > 0);
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void ScheduledExecutable_Migrations_UploadPdfToAmazon_GetRecentEDocs()
        {
            // NOTE: This seems like a transient method so the following brokerid may no longer work after the first run
            var brokerId = Guid.Parse("839127EF-72D1-4873-BFDF-18A23416B146");

            var list = GetRecentEDocs(brokerId);
            Assert.IsTrue(list.Count > 0);
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void ScheduledExecutable_Program_RetrieveCachedLoanFiles()
        {
            var brokerId = Guid.Parse("D116B2B3-D3CA-4181-BAF6-B663815DB67D");

            var list = RetrieveCachedLoanFiles(brokerId);
            Assert.IsTrue(list.Count > 0);
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void ScheduledExecutable_Program_FirstTechSequentialNumberingMigrator_IDRetrievalFunctionNEW()
        {
            var brokerId = Guid.Parse("839127EF-72D1-4873-BFDF-18A23416B146");

            var list = FirstTechSequentialNumberingMigrator_IDRetrievalFunctionNEW(brokerId);
            var guidList = (List<Guid>)list;
            Assert.IsTrue(guidList.Count > 0);
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void ScheduledExecutable_Program_AlterraCompanyRenameMigrator_IDRetrievalFunctionNEW()
        {
            var brokerId = Guid.Parse("31E9E37A-ECA4-402F-A3F7-02F7CBA5E5B5");

            E_sStatusT[] allowedStatuses = new E_sStatusT[]
            {
                //E_sStatusT.Loan_Open,
                //E_sStatusT.Loan_Prequal,
                //E_sStatusT.Loan_Registered,
                //E_sStatusT.Loan_Processing,
                //E_sStatusT.Loan_LoanSubmitted,
                //E_sStatusT.Loan_Underwriting,
                //E_sStatusT.Loan_Preapproval,
                //E_sStatusT.Loan_Approved,
                //E_sStatusT.Loan_FinalUnderwriting,
                //E_sStatusT.Loan_ClearToClose,
                //new
                //E_sStatusT.Loan_PreProcessing,
                //E_sStatusT.Loan_DocumentCheck,
                //E_sStatusT.Loan_PreUnderwriting,
                //E_sStatusT.Loan_ConditionReview,
                //E_sStatusT.Loan_PreDocQC,
                E_sStatusT.Loan_OnHold,
                E_sStatusT.Loan_Suspended,
                E_sStatusT.Loan_CounterOffer
            };

            var list = AlterraCompanyRenameMigrator_IDRetrievalFunctionNEW(brokerId, allowedStatuses);
            var guidList = (LinkedList<Guid>)list;
            Assert.IsTrue(guidList.Count > 0);
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void ScheduledExecutable_Program_RetrieveEmployeeLicenseContent()
        {
            var brokerId = Guid.Parse("FD6FD1EF-053A-434D-AA4B-565D7311699A");
            char userType = 'P';

            var list = RetrieveEmployeeLicenseContent(userType, brokerId);
            Assert.IsTrue(list.Count > 0);
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void ScheduledExecutable_Program_RetrieveLoanFileCache()
        {
            var sLIdList = new List<string>();
            var sb = new StringBuilder();

            RetrieveLoanFileCache(sLIdList, sb);
            Assert.IsTrue(sLIdList.Count > 0);
            Assert.IsTrue(sb.Length > 0);
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void ConstStage_NamesGood()
        {
            // reference a property to force creation of all properties
            var test = StageConfigDatum.AcceptedNonLoadminSmoketesters;
        }

        [Test]
        [Ignore("Suspend per OPM 365564")]
        public void ConstSite_NamesGood()
        {
            // reference a property to force creation of all properties
            var test = SiteConfigDatum.ConversationLogIP;
        }

        // -------------------------------- PRIVATE HELPER METHODS -------------------------------- //
        private class EDocInfo
        {
            public string LoanName { get; private set; }
            public Guid DocumentId { get; private set; }
            public string LastName { get; private set; }
            public string DocType { get; private set; }
            public string Folder { get; private set; }
            public string FileDBKey { get; private set; }
            public string PathFileName { get; set; }
            public string PathFolderName { get; set; }

            public EDocInfo(IDataReader reader)
            {
                LoanName = reader["slnm"].ToString();
                Folder = reader["FolderName"].ToString();
                DocType = reader["DocTypeName"].ToString();
                LastName = reader["ablastnm"].ToString();
                DocumentId = (Guid)reader["DocumentId"];
                FileDBKey = reader["fileDbKey_CurrentRevision"].ToString();

            }
        }

        private bool ScheduledExecutable_EDocBrokerExport_PolulateExportList(Guid brokerId)
        {
            // Copied from ScheduledExecutable.EDocBrokerExport.PopulateExportList, with handling of data removed

            string sql = @"select slnm, foldername, doctypename,  a.ablastnm, DocumentId, fileDbKey_CurrentRevision
                            from edocs_document e
	                        join loan_file_cache c on e.slid = c.slid 
	                        join application_a a on a.aappid = e.aappid
	                        join EDOCS_DOCUMENT_TYPE d on d.doctypeid = e.doctypeid 
	                        join edocs_folder f on f.folderid = d.folderid
                            where e.brokerid = @id and c.IsValid = 1";

            var listParams = new SqlParameter[] { new SqlParameter("@id", brokerId) };

            bool hasData = false;
            Action<IDataReader> readHandler = delegate (IDataReader reader)
            {
                while (reader.Read())
                {
                    EDocInfo info = new EDocInfo(reader);
                    hasData = true;
                }
            };

            DBSelectUtility.ProcessDBData(brokerId, sql, null, listParams, readHandler);
            return hasData;
        }

        private static void RetrieveLoanFileCache(List<string> sLIdList, StringBuilder sb)
        {
			// Copied from ScheduledExecutables.Program

			string sql = "SELECT c.sLId FROM LOAN_FILE_CACHE c WITH(NOLOCK) JOIN BROKER b WITH(NOLOCK) ON c.sBrokerId=b.BrokerId WHERE c.IsValid=1 AND b.Status=1 order by sopenedd desc";
			Action<IDataReader> readHandler = delegate (IDataReader reader)
			{
				while (reader.Read())
				{
					sLIdList.Add(reader["sLId"].ToString());
					sb.AppendLine(reader["sLId"].ToString());
				}
			};

			foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
			{
				DBSelectUtility.ProcessDBData(connInfo, sql, null, null, readHandler);
			}
		}

        private static List<Tuple<Guid, string, string>> RetrieveEmployeeLicenseContent(char userType, Guid brokerId)
        {
			// Copied from ScheduledExecutables.Program

			string sql = @"
select e.employeeid, loginnm, licensexmlcontent
    from view_employee_role ver 
        join EMPLOYEE e on e.employeeid = ver.employeeid 
        join broker_user bu on bu.employeeid = e.employeeid 
        join all_user au on au.userid = bu.userid 
    where roleid = '6DFC9B0E-11E3-48C1-B410-A1B0B5363B69' and brokerid = @brokerid and type = @usertype and e.isactive = 1";

			SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@brokerid", brokerId), DbAccessUtils.SqlParameterForSingleChar("@usertype", userType) };

			List<Tuple<Guid, string, string>> rows = new List<Tuple<Guid, string, string>>();
			Action<IDataReader> readHandler = delegate (IDataReader reader)
			{
				while (reader.Read())
				{
					rows.Add(Tuple.Create((Guid)reader["employeeid"], (string)reader["loginnm"], (string)reader["licensexmlcontent"]));
				}
			};

			var conInfo = DbConnectionInfo.GetConnectionInfo(brokerId);
			DBSelectUtility.ProcessDBData(conInfo, sql, null, parameters, readHandler);
			return rows;
		}

		private static IEnumerable<Guid> AlterraCompanyRenameMigrator_IDRetrievalFunctionNEW(Guid brokerId, E_sStatusT[] allowedStatuses)
        {
			// Copied from ScheduledExecutables.Program

			SqlParameter[] parameters = new SqlParameter[]
			{
							new SqlParameter("@BrokerId", brokerId)
			};

			string query = @"
select slid, sbrokerid, sstatust
from loan_file_cache 
where isvalid = 1 and sstatust in (
7, -- Loan On Hold
8, -- Loan Suspended
47 -- Counter Offer
) 
and sbrokerid = @BrokerId
";

			LinkedList<Guid> loanIds = new LinkedList<Guid>();
			Action<IDataReader> readHandler = delegate (IDataReader reader)
			{
				Guid loanBrokerId;
				while (reader.Read())
				{
					loanBrokerId = (Guid)reader["sbrokerid"];
					if (loanBrokerId != brokerId)
					{
						throw new Exception("Error: Unexpected BrokerId");
					}

					E_sStatusT status = (E_sStatusT)reader["sStatusT"];

					if (!allowedStatuses.Contains(status))
					{
						throw new Exception("unhandled status " + status.ToString());
					}


					loanIds.AddLast((Guid)reader["slid"]);
				}
			};

			DbConnectionInfo info = DbConnectionInfo.GetConnectionInfo(brokerId);
			DBSelectUtility.ProcessDBData(info, query, null, parameters, readHandler);

			return loanIds;
		}

		private static IEnumerable<Guid> FirstTechSequentialNumberingMigrator_IDRetrievalFunctionNEW(Guid brokerId)
        {
			// Copied from ScheduledExecutables.Program

			var loanIds = new List<Guid>();

			SqlParameter[] parameters = { new SqlParameter("@BrokerId", brokerId) };

			const string sql = @"
SELECT sLId
FROM LOAN_FILE_CACHE
WHERE 
    sBrokerId = @BrokerId AND -- Belongs to FirstTech
    IsTemplate = 0 AND
	IsValid = 1 AND
    sStatusT NOT IN (12, 15, 16, 17, 6, 11, 48, 9, 10, 27, 20) AND -- Not a lead and not funded, closed, withdrawn, canceled, denied, sold, or shipped
    datalength(sCustomField6Notes) > 0 -- Has a number assigned
";

			Action<IDataReader> readHandler = delegate (IDataReader reader)
			{
				while (reader.Read())
				{
					loanIds.Add((Guid)reader["sLId"]);
				}
			};

			DBSelectUtility.ProcessDBData(brokerId, sql, null, parameters, readHandler);

			return loanIds;
		}

		private static LinkedList<Tuple<string, Guid>> RetrieveCachedLoanFiles(Guid brokerId)
        {
			// Copied from ScheduledExecutables.Program

			string sql = @"
select slid, slnm, sbrokerid  
from loan_file_cache 
where sbrokerid = @brokerid and isvalid = 1 and sLoanFileT = 0 and IsTemplate = 0 
";
			var listParams = new SqlParameter[] { new SqlParameter("@BrokerId", brokerId) };

			var loans = new LinkedList<Tuple<string, Guid>>();
			Action<IDataReader> readHandler = delegate (IDataReader reader)
			{
				while (reader.Read())
				{
					Guid resultBrokerId = (Guid)reader["sbrokerid"];

					if (resultBrokerId != brokerId)
					{
						Console.WriteLine("Error in app cross broker.");
						return;
					}

					loans.AddLast(Tuple.Create((string)reader["slnm"], (Guid)reader["slid"]));
				}
			};

			DBSelectUtility.ProcessDBData(brokerId, sql, null, listParams, readHandler);
			return loans;
		}

		private static List<Guid> GetRecentEDocs(Guid brokerId)
        {
            // Copied from ScheduledExecutables.Migrations.UploadPdfToAmazon

            const string sql = "SELECT TOP 10 CreatedDate, DocumentId FROM edocs_document_original WHERE BrokerId=@BrokerId AND IsUploadedAws=0 order by createddate desc";

            SqlParameter[] parameters = { new SqlParameter("@BrokerId", brokerId) };

            List<Guid> documentIdList = new List<Guid>();
            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                while (reader.Read())
                {
                    Guid documentId = (Guid)reader["DocumentId"];
                    documentIdList.Add(documentId);
                }
            };

            DBSelectUtility.ProcessDBData(brokerId, sql, TimeoutInSeconds.Thirty, parameters, readHandler);
            return documentIdList;
        }

        private static Guid GetUserIdByEmployeeFullNameNEW(Dictionary<string, Guid> dictionary, Guid brokerId, string employeeName)
        {
            // Copied from ScheduledExecutables.Migrations.TaskMigration

            Guid userId = Guid.Empty;

            if (dictionary.TryGetValue(employeeName, out userId) == false)
            {
                string sql = "SELECT UserId FROM VIEW_ACTIVE_LO_USER WHERE BrokerId = @BrokerId AND UserFirstNm + ' ' + UserLastNm = @EmployeeName";

                SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId),
                                            new SqlParameter("@EmployeeName", employeeName)
                                        };

                Action<IDataReader> readHandler = delegate(IDataReader reader)
                {
                    if (reader.Read())
                    {
                        userId = (Guid)reader["UserId"];
                    }
                };

                DBSelectUtility.ProcessDBData(brokerId, sql, null, parameters, readHandler);

                dictionary.Add(employeeName, userId);
            }

            return userId;
        }

        private static List<Guid> ListActiveLoansNEW(Guid brokerId)
        {
            // Copied from ScheduledExecutables.Migrations.TaskMigration

            // 2/23/2015 dd - We only perform task migration of actual loans. We are not perform migration of sandbox or quicker pricer 2.0.
            string sql = "SELECT sLId FROM LOAN_FILE_CACHE WITH(NOLOCK) WHERE sBrokerId=@brokerid AND isvalid=1 AND (sLoanFileT IS NULL OR sLoanFileT=0)";
            var listParams = new SqlParameter[] { new SqlParameter("@brokerid", brokerId) };

            List<Guid> list = new List<Guid>();
            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                while (reader.Read())
                {
                    list.Add((Guid)reader["sLId"]);
                }
            };

            DBSelectUtility.ProcessDBData(brokerId, sql, null, listParams, readHandler);

            return list;
        }

        private static int GetManagedTaskPermissionLevelIdNEW(Guid brokerId)
        {
            // Copied from ScheduledExecutables.Migrations.TaskMigration

            string sql = "SELECT TaskPermissionLevelId FROM TASK_PERMISSION_LEVEL WHERE Name='Managed' AND BrokerId=@brokerid";
            var listParams = new SqlParameter[] { new SqlParameter("@brokerid", brokerId) };

            int? levelID = null;
            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                if (reader.Read())
                {
                    levelID = (int)reader["TaskPermissionLevelId"];
                }
            };

            DBSelectUtility.ProcessDBData(brokerId, sql, null, listParams, readHandler);

            if (levelID != null) return levelID.Value;

            throw new Exception("There is no 'Managed' task permission level id");
        }

        private static int GetGeneralTaskPermissionLevelIdNEW(Guid brokerId)
        {
            // Copied from ScheduledExecutables.Migrations.TaskMigration

            string sql = "SELECT TaskPermissionLevelId FROM TASK_PERMISSION_LEVEL WHERE Name='General' AND BrokerId=@brokerid";
            var listParams = new SqlParameter[] { new SqlParameter("@brokerid", brokerId) };

            int? levelID = null;
            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                if (reader.Read())
                {
                    levelID = (int)reader["TaskPermissionLevelId"];
                }
            };

            DBSelectUtility.ProcessDBData(brokerId, sql, null, listParams, readHandler);

            if (levelID != null) return levelID.Value;

            throw new Exception("There is no 'General' task permission level id");
        }

        private static Guid GetAccountOwnerNEW(Guid brokerId)
        {
            // Copied from ScheduledExecutables.Migrations.TaskMigration

            string sql = "SELECT UserId FROM VIEW_ACTIVE_LO_USER WHERE IsAccountOwner=1 AND BrokerId=@brokerid";
            var listParams = new SqlParameter[] { new SqlParameter("@brokerid", brokerId) };

            Guid userID = Guid.Empty;
            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                if (reader.Read())
                {
                    userID = (Guid)reader["UserId"];
                }
            };

            DBSelectUtility.ProcessDBData(brokerId, sql, null, listParams, readHandler);

            if (userID != Guid.Empty) return userID;

            throw new Exception("There is no active account owner");
        }

        class UserInfo
        {
            public Guid UserId;
            public Guid EmployeeId;
            public Guid BranchId;
            public string DisplayName;
            public string LoginName;
            public string UserFirstNm;
            public string UserLastNm;
        }

        private static Dictionary<Guid, UserInfo> ListActiveLoUsersNEW(Guid brokerId)
        {
            // Copied from ScheduledExecutables.Migrations.TaskMigration

            string sql = "SELECT UserId, BranchId, EmployeeId, LoginNm, UserFirstNm, UserLastNm FROM VIEW_ACTIVE_LO_USER WHERE BrokerId=@brokerid";
            var listParams = new SqlParameter[] { new SqlParameter("@brokerid", brokerId) };

            Dictionary<Guid, UserInfo> list = new Dictionary<Guid, UserInfo>();
            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                while (reader.Read())
                {
                    UserInfo user = new UserInfo();
                    user.UserId = (Guid)reader["UserId"];
                    user.EmployeeId = (Guid)reader["EmployeeId"];
                    user.BranchId = (Guid)reader["BranchId"];
                    user.LoginName = (string)reader["LoginNm"];
                    user.DisplayName = (string)reader["UserFirstNm"] + " " + (string)reader["UserLastNm"];
                    user.UserFirstNm = (string)reader["UserFirstNm"];
                    user.UserLastNm = (string)reader["UserLastNm"];
                    list.Add(user.UserId, user);
                }
            };

            DBSelectUtility.ProcessDBData(brokerId, sql, null, listParams, readHandler);

            return list;
        }

        class LoanInfo
        {
            public Guid sLId;
            public string sLNm;
            public E_sStatusT sStatusT;
            public DateTime sEstClosedD;
            public DateTime sRLckdExpiredD;
            public bool IsTemplate;
        }

        private static LoanInfo GetLoanInfoNEW(Guid brokerId, Guid sLId, Dictionary<Guid, LoanInfo> dictionary)
        {
            // Copied from ScheduledExecutables.Migrations.TaskMigration

            LoanInfo loanInfo;
            if (dictionary.TryGetValue(sLId, out loanInfo) == false)
            {
                string sql = "SELECT sStatusT, sEstCloseD,sRLckdExpiredD, sLNm, IsTemplate FROM LOAN_FILE_CACHE WHERE IsValid=1 AND sLId=@slid";
                var listParams = new SqlParameter[] { new SqlParameter("@slid", sLId) };

                Action<IDataReader> readHandler = delegate(IDataReader reader)
                {
                    loanInfo = new LoanInfo();
                    loanInfo.sLId = sLId;
                    if (reader.Read())
                    {
                        loanInfo.sStatusT = (E_sStatusT)reader["sStatusT"];
                        loanInfo.sLNm = (string)reader["sLNm"];
                        if (reader["sEstCloseD"] != DBNull.Value)
                        {
                            loanInfo.sEstClosedD = (DateTime)reader["sEstCloseD"];
                        }
                        else
                        {
                            loanInfo.sEstClosedD = DateTime.MinValue;
                        }
                        if (reader["sRLckdExpiredD"] != DBNull.Value)
                        {
                            loanInfo.sRLckdExpiredD = (DateTime)reader["sRLckdExpiredD"];
                        }
                        else
                        {
                            loanInfo.sRLckdExpiredD = DateTime.MinValue;
                        }
                        loanInfo.IsTemplate = (bool)reader["IsTemplate"];
                    }
                    else
                    {
                        loanInfo.sStatusT = E_sStatusT.Loan_Canceled;
                        loanInfo.sEstClosedD = DateTime.MinValue;
                        loanInfo.sRLckdExpiredD = DateTime.MinValue;
                        loanInfo.IsTemplate = true;
                    }
                };

                DBSelectUtility.ProcessDBData(brokerId, sql, null, listParams, readHandler);

                dictionary.Add(sLId, loanInfo);
            }
            return loanInfo;
        }

        private static List<Guid> ListActiveParticipantsNEW(Guid brokerId, Guid discLogId)
        {
            // Copied from ScheduledExecutables.Migrations.TaskMigration

            List<Guid> userList = new List<Guid>();
            string sql = "SELECT NotifReceiverUserId FROM DISCUSSION_NOTIFICATION WHERE NotifIsValid=1 AND DiscLogId=@disc";
            var listParams = new SqlParameter[] { new SqlParameter("@disc", discLogId) };

            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                while (reader.Read())
                {
                    userList.Add((Guid)reader["NotifReceiverUserId"]);
                }
            };

            DBSelectUtility.ProcessDBData(brokerId, sql, null, listParams, readHandler);

            return userList;
        }

        private static IEnumerable<Guid> ListLoansForAprCalculationMigrationNEW()
        {
            // Copied from ScheduledExecutables.Migrations.Opm235692_SetAprCalculationTypeBasedOnBrokerBit

            List<Guid> loanIds = new List<Guid>();
            string sql = @"
                    SELECT lfc.sLId
                    FROM LOAN_FILE_CACHE lfc
                        JOIN LOAN_FILE_B lfb ON lfc.sLId = lfb.sLId
                    WHERE lfc.IsValid = 1
                        AND lfb.sAprCalculationT = 0
                        AND lfc.sBrokerId IN
                            (SELECT BrokerId
                             FROM BROKER
                             WHERE Status = 1
                                 AND TempOptionXmlContent LIKE '%<option name=""UseIrregularFirstPeriodAprCalc_69485"" value =""true"" />%')";

            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                while (reader.Read())
                {
                    loanIds.Add((Guid)reader["sLId"]);
                    break; // added to speed up
                }
            };

            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                DBSelectUtility.ProcessDBData(connInfo, sql, null, null, readHandler);
                if (loanIds.Count > 0) break; // added to speed up
            }

            return loanIds;
        }

        private IEnumerable<Guid> GetLoanIdsWithNonEmptyClosingCostArchiveXmlNEW()
        {
            // Copied from ScheduledExecutables.Migrations.MigrateCCArchiveLoanDataToFileDB

            var loanIds = new List<Guid>();
            var selectAllValidLoansWithClosingCostArchives = @"
select b.slid
from loan_file_b b 
	join loan_file_cache lc on b.slid = lc.slid 
	join broker bro on lc.sbrokerid = bro.brokerid
where lc.isvalid = 1
	and bro.status = 1
	and b.sclosingcostarchivexmlcontent <> ''
	and lc.sloanfilet <> 2";

            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                while (reader.Read())
                {
                    loanIds.Add((Guid)reader["slid"]);
                    break; // added to speed up
                }
            };

            foreach (var connectionInfo in DbConnectionInfo.ListAll())
            {
                DBSelectUtility.ProcessDBData(connectionInfo, selectAllValidLoansWithClosingCostArchives, null, null, readHandler);
                if (loanIds.Count > 0) break; // added to speed up
            }

            return loanIds;
        }

        private static void GetEmailSender(List<string> reminderList, string customProduct)
        {
            // Copied from ScheduledExecutable.CheckExpiredPricing

            string query = "SELECT Sender FROM EMAIL_DOWNLOAD_LIST WHERE TargetFileName = @TargetFileName";
            SqlParameter[] customProductParameters = { DbAccessUtils.SqlParameterForVarchar("@TargetFileName", customProduct) };

            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                if (reader.Read())
                {
                    reminderList.Add((string)reader["Sender"]);
                }
            };

            DBSelectUtility.ProcessDBData(DataSrc.RateSheet, query, null, customProductParameters, readHandler);
        }

        private int? GetProdCurrPIPmtLckd(Guid brokerId, Guid loanId)
        {
            string sql = "select top 1 sProdCurrPIPmtLckd from loan_file_b where sLId = @loanId";
            SqlParameter[] parameters = { new SqlParameter("@loanId", loanId) };

            DataSet ds = new DataSet();
            DBSelectUtility.FillDataSet(brokerId, ds, sql, null, parameters);
            if (ds.Tables[0].Rows.Count == 0) return null;

            object obj = ds.Tables[0].Rows[0]["sProdCurrPIPmtLckd"];
            if (Convert.IsDBNull(obj)) return null;
            return Convert.ToInt32(obj);
        }

        private void SetProdCurrPIPmtLckd(Guid brokerId, Guid loanId, int value)
        {
            string sql = "update loan_file_b set sProdCurrPIPmtLckd = @value where sLId = @loanId";
            SqlParameter[] parameters = { new SqlParameter("@loanId", loanId), new SqlParameter("@value", value) };
            DBUpdateUtility.Update(brokerId, sql, null, parameters);
        }

        private Guid GetPaymentLicenseId(Guid brokerId, Guid paymentId)
        {
            string sql = "select LicenseId from payment where PaymentId = @payId";
            var parameters = new SqlParameter[] { new SqlParameter("@payId", paymentId) };

            var licenseId = Guid.Empty;
            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                if (reader.Read())
                {
                    if (!Convert.IsDBNull(reader["LicenseId"]))
                    {
                        licenseId = (Guid)reader["LicenseId"];
                    }
                }
            };

            DBSelectUtility.ProcessDBData(brokerId, sql, null, parameters, readHandler);
            return licenseId;
        }
        private Guid AddLicenseRecord(Guid brokerId)
        {
            return Licensee.CreateLicenseRecord(brokerId, 500, "TESTING", Guid.Empty, DateTime.Today.AddMonths(1));
        }

        private int DeleteLicenseRecord(Guid brokerId, Guid licenseId)
        {
            string sql = "delete from license where licenseId = @licenseId";
            var prms = new SqlParameter[] { new SqlParameter("@licenseId", licenseId) };
            var rowCount = DBDeleteUtility.Delete(brokerId, sql, null, prms);
            return rowCount.Value;
        }

        private Guid GetCurrentLicense(Guid brokerId, Guid userId)
        {
            string sql = "select CurrentLicenseId from broker_user where UserId = @userId";
            var parameters = new SqlParameter[] { new SqlParameter("@userId", userId) };

            Guid licenseId = Guid.Empty;
            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                if (reader.Read())
                {
                    if (!Convert.IsDBNull(reader["CurrentLicenseId"]))
                    {
                        licenseId = (Guid)reader["CurrentLicenseId"];
                    }
                }
            };

            DBSelectUtility.ProcessDBData(brokerId, sql, null, parameters, readHandler);
            return licenseId;
        }

        private void SetCurrentLicenseId(Guid brokerId, Guid userId, Guid currentLicenseId)
        {
            string sql = "update broker_user set CurrentLicenseId = @licenseId where UserId = @userId";
            var parameters = new SqlParameter[] { new SqlParameter("@licenseId", currentLicenseId), new SqlParameter("@userId", userId) };
            DBUpdateUtility.Update(brokerId, sql, null, parameters);
        }

        private void SetOpenSeatCount(Guid brokerId, Guid licenseId, int openCount)
        {
            string sql = "update license set OpenSeatCount = @count where licenseId = @licenseId";
            var parameters = new SqlParameter[] { new SqlParameter("@count", openCount), new SqlParameter("@licenseId", licenseId) };
            DBUpdateUtility.Update(brokerId, sql, null, parameters);
        }

        private int GetOpenSeatCount(Guid brokerId, Guid licenseId)
        {
            string sql = "select OpenSeatCount from license where licenseId = @licenseId";
            SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@licenseId", licenseId) };

            int seatsAvailable = 0;
            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                if (reader.Read())
                {
                    seatsAvailable = Convert.ToInt32(reader["OpenSeatCount"]);
                }
            };

            DBSelectUtility.ProcessDBData(brokerId, sql, null, parameters, readHandler);
            return seatsAvailable;
        }

        private int GetSeatUsedCount(Guid brokerId, Guid licenseId)
        {
            string sql = "SELECT COUNT(*) AS SeatUsed FROM BROKER_USER WHERE CurrentLicenseId = @licenseId";
            SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@licenseId", licenseId) };

            int seatsUsed = 0;
            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                if (reader.Read())
                {
                    seatsUsed = Convert.ToInt32(reader["SeatUsed"]);
                }
            };

            DBSelectUtility.ProcessDBData(brokerId, sql, null, parameters, readHandler);
            return seatsUsed;
        }
    }
}
