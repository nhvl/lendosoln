/// Author: David Dao
using System.Data;
using System.Data.Common;
using System.Linq;
using Adapter;
using DataAccess;
using LendersOffice.Security;
using LendOSolnTest.Common;
using NUnit.Framework;

namespace LendOSolnTest.DB
{
    [TestFixture]
	public class DBConnectionTest
	{
		public DBConnectionTest()
		{
		}

        [Test]
        public void TestMainConnection() 
        {
            ConnectionTest(DataSrc.LOShare);
        }

        [Test]
        public void TestMainROnlyConnection() 
        {
            ConnectionTest(DataSrc.LOShareROnly);
        }

        [Test]
        public void TestTransientConnection() 
        {
            ConnectionTest(DataSrc.LOTransient);
        }

        [Test]
        public void TestLpeSrcConnection() 
        {
            ConnectionTest(DataSrc.LpeSrc);
        }

        [Test]
        public void TestRateSheetConnection() 
        {
            ConnectionTest(DataSrc.RateSheet);
        }

        [Test]
        public void TestLoadBalanceConnection()
        {
            // 7/14/2014 dd - Temporary disable this test.
            var list = DbConnectionInfo.ListAll();

            Assert.Greater(list.Count(), 0, "There must be at least one record.");
            foreach (var dbInfo in DbConnectionInfo.ListAll())
            {
                if (dbInfo.Description == "Default")
                {
                    // 8/2/2014 dd - Only test default connection for now.
                    Tools.LogInfo("Connect to " + dbInfo.FriendlyDisplay);
                    ConnectionTest(dbInfo.GetReadOnlyConnection());
                    ConnectionTest(dbInfo.GetConnection());
                }
            }
        }

        [Test]
        public void TestGetConnectionForLoTestAccount()
        {
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.LoTest001);

            ConnectionTest(DbConnectionInfo.GetConnection(principal.BrokerId));
            ConnectionTest(DbConnectionInfo.GetReadOnlyConnection(principal.BrokerId));
        }

        private void ConnectionTest(DataSrc src) 
        {
            try 
            {
                ConnectionTest(DbAccessUtils.GetConnection(src));
            } 
            catch 
            {
                Assert.Fail("Cannot connect to DataSrc=" + src);
            }
        }

        private void ConnectionTest(DbConnection conn)
        {
            using (conn)
            {
                conn.OpenWithRetry();
                using (DbCommand command = conn.CreateCommand())
                {
                    command.CommandText = "SELECT GETDATE();";
                    using (DbDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        if (reader.Read() == false)
                        {
                            Assert.Fail("Unable to get DB Date");
                        }
                    }
                }
                conn.Close();
            }
        }
	}
}
