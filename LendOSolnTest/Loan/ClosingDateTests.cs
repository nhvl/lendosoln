﻿namespace LendOSolnTest.Loan
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DataAccess;
    using NUnit.Framework;

    [TestFixture]
    public class ClosingDateTests
    {
        private readonly CDateTime JulyFirst = CDateTime.Create("07/01/2015", null);
        private readonly CDateTime AugustFifteenth = CDateTime.Create("08/15/2015", null);
        private readonly CDateTime JulyEleventh = CDateTime.Create("07/11/2015", null);
        private readonly LosConvert losConvert = new LosConvert();

        [Test]
        public void CalculateDefaultEstimatedCloseDate_RateLockExpirationDateNotNull_ReturnsRateLockExpirationDate()
        {
            CDateTime sRLckdExpiredD = CDateTime.Create(this.JulyFirst.DateTimeForComputation);
            CDateTime sSubmitD = null;
            CDateTime sOpenedD = null;
            int sRLckdDays = 10;

            CDateTime sDefaultEstCloseD = CPageBase.CalculateDefaultEstimatedCloseDate(
                sRLckdExpiredD,
                sSubmitD,
                sOpenedD,
                sRLckdDays,
                null);

            Assert.AreEqual(sRLckdExpiredD.ToString(losConvert), sDefaultEstCloseD.ToString(losConvert));
        }

        [Test]
        public void CalculateDefaultEstimatedCloseDate_SubmissionDateNotNullRateLockedDaysZero_ReturnsSubmissionDatePlus45Days()
        {
            CDateTime sRLckdExpiredD = null;
            CDateTime sSubmitD = CDateTime.Create(this.JulyFirst.DateTimeForComputation);
            CDateTime sOpenedD = null;
            int sRLckdDays = 0;

            CDateTime sDefaultEstCloseD = CPageBase.CalculateDefaultEstimatedCloseDate(
                sRLckdExpiredD,
                sSubmitD,
                sOpenedD,
                sRLckdDays,
                null);

            Assert.AreEqual(this.AugustFifteenth.ToString(losConvert), sDefaultEstCloseD.ToString(losConvert));
        }

        [Test]
        public void CalculateDefaultEstimatedCloseDate_SubmissionDateNotNullRateLockedDays10_ReturnsSubmissionDatePlus10Days()
        {
            CDateTime sRLckdExpiredD = null;
            CDateTime sSubmitD = CDateTime.Create(this.JulyFirst.DateTimeForComputation);
            CDateTime sOpenedD = null;
            int sRLckdDays = 10;

            CDateTime sDefaultEstCloseD = CPageBase.CalculateDefaultEstimatedCloseDate(
                sRLckdExpiredD,
                sSubmitD,
                sOpenedD,
                sRLckdDays,
                null);
            CDateTime expected = CDateTime.Create(this.JulyFirst.DateTimeForComputation.AddDays(sRLckdDays));

            Assert.AreEqual(this.JulyEleventh.ToString(losConvert), sDefaultEstCloseD.ToString(losConvert));
        }

        [Test]
        public void CalculateDefaultEstimatedCloseDate_OpenedDateNotNullRateLockDaysZero_ReturnsOpenedDatePlus45Days()
        {
            CDateTime sRLckdExpiredD = null;
            CDateTime sSubmitD = null; 
            CDateTime sOpenedD = CDateTime.Create(this.JulyFirst.DateTimeForComputation);
            int sRLckdDays = 0;

            CDateTime sDefaultEstCloseD = CPageBase.CalculateDefaultEstimatedCloseDate(
                sRLckdExpiredD,
                sSubmitD,
                sOpenedD,
                sRLckdDays,
                null);

            Assert.AreEqual(this.AugustFifteenth.ToString(losConvert), sDefaultEstCloseD.ToString(losConvert));
        }

        [Test]
        public void CalculateDefaultEstimatedCloseDate_OpenedDateNotNullRateLockDay10_ReturnsOpenedDatePlus10Days()
        {
            CDateTime sRLckdExpiredD = null;
            CDateTime sSubmitD = null; 
            CDateTime sOpenedD = CDateTime.Create(this.JulyFirst.DateTimeForComputation);
            int sRLckdDays = 10;

            CDateTime sDefaultEstCloseD = CPageBase.CalculateDefaultEstimatedCloseDate(
                sRLckdExpiredD,
                sSubmitD,
                sOpenedD,
                sRLckdDays,
                null);

            Assert.AreEqual(this.JulyEleventh.ToString(losConvert), sDefaultEstCloseD.ToString(losConvert));
        }

        [Test]
        [ExpectedException(ExpectedException = typeof(CBaseException))]
        public void CalculateDefaultEstimatedCloseDate_AllDatesNull_ThrowsException()
        {
            CDateTime sRLckdExpiredD = null;
            CDateTime sSubmitD = null; 
            CDateTime sOpenedD = null;
            int sRLckdDays = 10;

            CDateTime sDefaultEstCloseD = CPageBase.CalculateDefaultEstimatedCloseDate(
                sRLckdExpiredD,
                sSubmitD,
                sOpenedD,
                sRLckdDays,
                null);
        }

        [Test]
        public void GetClosingDateMonth_DocMagicClosingDValid_ReturnsDocMagicClosingDMonth()
        {
            CDateTime sDocMagicClosingD = this.JulyFirst;
            CDateTime sEstCloseD = null;
            CDateTime sDefaultEstCloseD = null;

            int closingDateMonth = CPageBase.GetClosingDateMonth(
                sDocMagicClosingD,
                sEstCloseD,
                sDefaultEstCloseD);

            Assert.AreEqual(7, closingDateMonth);
        }

        [Test]
        public void GetClosingDateMonth_EstCloseDValid_ReturnsEstCloseDMonth()
        {
            CDateTime sDocMagicClosingD = null;
            CDateTime sEstCloseD = this.JulyFirst;
            CDateTime sDefaultEstCloseD = null;

            int closingDateMonth = CPageBase.GetClosingDateMonth(
                sDocMagicClosingD,
                sEstCloseD,
                sDefaultEstCloseD);

            Assert.AreEqual(7, closingDateMonth);
        }

        [Test]
        public void GetClosingDateMonth_DefeaultEstCloseDValid_ReturnsDefeaultEstCloseDMonth()
        {
            CDateTime sDocMagicClosingD = null;
            CDateTime sEstCloseD = null;
            CDateTime sDefaultEstCloseD = this.JulyFirst;

            int closingDateMonth = CPageBase.GetClosingDateMonth(
                sDocMagicClosingD,
                sEstCloseD,
                sDefaultEstCloseD);

            Assert.AreEqual(7, closingDateMonth);
        }

        [Test]
        [ExpectedException(ExpectedException = typeof(CBaseException))]
        public void GetClosingDateMonth_AllDatesNull_ThrowsException()
        {
            CDateTime sDocMagicClosingD = null;
            CDateTime sEstCloseD = null;
            CDateTime sDefaultEstCloseD = null;

            int closingDateMonth = CPageBase.GetClosingDateMonth(
                sDocMagicClosingD,
                sEstCloseD,
                sDefaultEstCloseD);
        }
    }
}
