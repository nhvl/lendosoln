﻿namespace LendOSolnTest.Loan
{
    using System;
    using DataAccess;
    using NUnit.Framework;

    [TestFixture, Ignore("This has been verified manually.")]
    public class FileDbMigrationTest
    {
        [TestFixtureSetUp]
        public void Setup()
        {
            System.Threading.Thread.CurrentPrincipal = LendersOffice.Security.SystemUserPrincipal.TaskSystemUser;
        }

        [Test]
        public void Test_sFHACaseQueryResultXmlContent()
        {
            var testLoanId = new Guid("CA3EBF15-A8D0-4F39-AD3A-A71A00E622A8");
            var loan = this.CreatePageData(testLoanId);

            var databaseValue = loan.sFHACaseQueryResultXmlContent;

            // Trigger a change to save the data to FileDB
            loan.sFHACaseQueryResultXmlContent += " ";
            loan.Save();

            loan = this.CreatePageData(testLoanId);
            Assert.AreEqual(databaseValue.Trim(), loan.sFHACaseQueryResultXmlContent.Trim());
        }

        [Test]
        public void Test_sFHACaseNumberResultXmlContent()
        {
            var testLoanId = new Guid("4E83D2FA-5058-40B9-9CBC-A80E00CFB566");
            var loan = this.CreatePageData(testLoanId);

            var databaseValue = loan.sFHACaseNumberResultXmlContent;

            // Trigger a change to save the data to FileDB
            loan.sFHACaseNumberResultXmlContent += " ";
            loan.Save();

            loan = this.CreatePageData(testLoanId);
            Assert.AreEqual(databaseValue.Trim(), loan.sFHACaseNumberResultXmlContent.Trim());
        }

        [Test]
        public void Test_sFHACAVIRSResultXmlContent()
        {
            var testLoanId = new Guid("F4AD42AA-119A-43F0-83EE-A80200A6A5C3");
            var loan = this.CreatePageData(testLoanId);

            var databaseValue = loan.sFHACAVIRSResultXmlContent;

            // Trigger a change to save the data to FileDB
            loan.sFHACAVIRSResultXmlContent += " ";
            loan.Save();

            loan = this.CreatePageData(testLoanId);
            Assert.AreEqual(databaseValue.Trim(), loan.sFHACAVIRSResultXmlContent.Trim());
        }

        /* These fields are private and were made public temporarily for unit testing.
        [Test]
        public void Test_sProdPmlDataUsedForLastPricingXmlContent()
        {
            var testLoanId = new Guid("0B244F1C-FEEF-452A-9EF8-A8410093C7B8");
            var loan = this.CreatePageData(testLoanId);

            var databaseValue = loan.sProdPmlDataUsedForLastPricingXmlContent;

            // Trigger a change to save the data to FileDB
            loan.sProdPmlDataUsedForLastPricingXmlContent += " ";
            loan.Save();

            loan = this.CreatePageData(testLoanId);
            Assert.AreEqual(databaseValue.Trim(), loan.sProdPmlDataUsedForLastPricingXmlContent.Trim());
        }

        [Test]
        public void Test_sPreparerXmlContent()
        {
            var testLoanId = new Guid("4BB89438-6AB7-4401-9ADA-46AE7934CC54");
            var loan = this.CreatePageData(testLoanId);

            var databaseValue = loan.sPreparerXmlContent;

            // Trigger a change to save the data to FileDB
            loan.sPreparerXmlContent += " ";
            loan.Save();

            loan = this.CreatePageData(testLoanId);
            Assert.AreEqual(databaseValue.Trim(), loan.sPreparerXmlContent.Trim());
        }
        */

        [Test]
        public void Test_sTotalScoreCertificateXmlContent()
        {
            var testLoanId = new Guid("45884920-E307-47EF-8F65-A4810173DC58");
            var loan = this.CreatePageData(testLoanId);

            var databaseValue = loan.sTotalScoreCertificateXmlContent;

            // Trigger a change to save the data to FileDB
            loan.sTotalScoreCertificateXmlContent += " ";
            loan.Save();

            loan = this.CreatePageData(testLoanId);
            Assert.AreEqual(databaseValue.Value.Trim(), loan.sTotalScoreCertificateXmlContent.Value.Trim());
        }

        [Test]
        public void Test_sTotalScoreTempCertificateXmlContent()
        {
            var testLoanId = new Guid("D164B9A1-14EE-47E0-B8FC-A82E0100D412");
            var loan = this.CreatePageData(testLoanId);

            var databaseValue = loan.sTotalScoreTempCertificateXmlContent;

            // Trigger a change to save the data to FileDB
            loan.sTotalScoreTempCertificateXmlContent += " ";
            loan.Save();

            loan = this.CreatePageData(testLoanId);
            Assert.AreEqual(databaseValue.Value.Trim(), loan.sTotalScoreTempCertificateXmlContent.Value.Trim());
        }

        private CPageData CreatePageData(Guid loanId)
        {
            var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(FileDbMigrationTest));
            loan.InitSave(LendersOffice.Constants.ConstAppDavid.SkipVersionCheck);
            return loan;
        }
    }
}
