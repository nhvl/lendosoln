﻿namespace LendOSolnTest.Loan
{
    using System;
    using DataAccess;
    using LendersOffice.Common;
    using NUnit.Framework;

    [TestFixture]
    public class ServicingPaymentTest
    {
        [Test]
        public void NewServicingPayment_HasId()
        {
            var payment = new CServicingPaymentFields();
            Assert.IsTrue(payment.Id.HasValue);
        }

        [Test]
        public void ServicingPaymentId_PersistsAcrossSerialization()
        {
            var payment = new CServicingPaymentFields();
            var id = payment.Id.Value;

            var serializedValue = SerializationHelper.XmlSerialize(payment);
            var deserializedValue = SerializationHelper.XmlDeserialize<CServicingPaymentFields>(serializedValue);

            Assert.IsTrue(deserializedValue.Id.HasValue);
            Assert.AreEqual(id, deserializedValue.Id.Value);
        }

        [Test]
        public void ExistingPayment_NoId_AddsIdOnDeserialize()
        {
            var existingXml = @"
<CServicingPaymentFields>
    <ServicingTransactionT>Monthly Payment</ServicingTransactionT>
    <DueD_rep>11/1/2013</DueD_rep>
    <DueAmt_rep>$1,677.01</DueAmt_rep>
    <PaymentD_rep>5/22/2014</PaymentD_rep>
    <PaymentAmt_rep>$325.74</PaymentAmt_rep>
    <Principal_rep>($1,244.00)</Principal_rep>
    <Interest_rep>$840.00</Interest_rep>
    <Escrow_rep>$404.00</Escrow_rep>
    <Other_rep>$324.51</Other_rep>
    <LateFee_rep>$1.23</LateFee_rep>
    <Notes>Some Notes</Notes>
    <LastColumnUpdated>-1</LastColumnUpdated>
    <RowNum>0</RowNum>
    <Payee>Joe Berry</Payee>
</CServicingPaymentFields>
";

            var deserializedValue = SerializationHelper.XmlDeserialize<CServicingPaymentFields>(existingXml);
            Assert.IsTrue(deserializedValue.Id.HasValue);
        }

        [Test]
        public void ExistingPayment_WithId_KeepsSameIdOnDeserialize()
        {
            var id = Guid.NewGuid();
            var existingXml = $@"
<CServicingPaymentFields>
    <Id>{id}</Id>
    <ServicingTransactionT>Monthly Payment</ServicingTransactionT>
    <DueD_rep>11/1/2013</DueD_rep>
    <DueAmt_rep>$1,677.01</DueAmt_rep>
    <PaymentD_rep>5/22/2014</PaymentD_rep>
    <PaymentAmt_rep>$325.74</PaymentAmt_rep>
    <Principal_rep>($1,244.00)</Principal_rep>
    <Interest_rep>$840.00</Interest_rep>
    <Escrow_rep>$404.00</Escrow_rep>
    <Other_rep>$324.51</Other_rep>
    <LateFee_rep>$1.23</LateFee_rep>
    <Notes>Some Notes</Notes>
    <LastColumnUpdated>-1</LastColumnUpdated>
    <RowNum>0</RowNum>
    <Payee>Joe Berry</Payee>
</CServicingPaymentFields>
";

            var deserializedValue = SerializationHelper.XmlDeserialize<CServicingPaymentFields>(existingXml);
            Assert.IsTrue(deserializedValue.Id.HasValue);
            Assert.AreEqual(id, deserializedValue.Id);
        }
    }
}
