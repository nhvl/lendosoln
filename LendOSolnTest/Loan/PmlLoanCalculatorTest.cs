/// Author: David Dao

using System;
using DataAccess;
using LendersOffice.Constants;
using LendOSolnTest.Common;
using LendOSolnTest.Fakes;
using NUnit.Framework;
// Test for OPM 25881.
namespace LendOSolnTest.Loan
{
    [TestFixture]
    public class PmlLoanCalculatorTest
    {
        [TestFixtureSetUp]
        public void Setup()
        {
            LoginTools.LoginAs(TestAccountType.LoTest001);
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
            FakePageDataUtilities.Instance.Reset();
        }

        [Test]
        public void TestHas2ndFinPe() 
        {
            CPageData dataLoan = CreateDataLoan();
            dataLoan.InitLoad();
            dataLoan.sHouseValPe = 100000;
            dataLoan.sProdCalcEntryT = E_sProdCalcEntryT.LAmtOtherFin;
            dataLoan.sProOFinBalPe = 0;

            Assert.AreEqual(false, dataLoan.sHas2ndFinPe, "sProOFinBalPe = 0");

            dataLoan.sProOFinBalPe = -1000;
            Assert.AreEqual(false, dataLoan.sHas2ndFinPe, "sProOFinBalPe = -1000");

            dataLoan.sProOFinBalPe = 10000;
            Assert.AreEqual(true, dataLoan.sHas2ndFinPe, "sProOFinBalPe = 10000");

            dataLoan.sProOFinBalPe = 0;
            dataLoan.sHas2ndFinPe = true;
            Assert.AreEqual(true, dataLoan.sHas2ndFinPe, "sHas2ndFinPe = true");
        }
        [Test]
        public void TestDecreaseDownPaymentWithHas2ndFin()
        {
            // 12/04/2008 - dd - This is to test the scenario in OPM 25881 comment by Lester (11/25/2008 11:35AM)
            CPageData dataLoan = CreateDataLoan();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sProdCalcEntryT = E_sProdCalcEntryT.HouseVal;
            dataLoan.sHouseValPe = 100000;
            dataLoan.sEquityPe = 20000;
            dataLoan.sLAmtCalcPe = 80000;
            dataLoan.sProOFinBalPe = 0;
            dataLoan.Save();

            dataLoan = CreateDataLoan();
            dataLoan.InitLoad();
            dataLoan.sProdCalcEntryT = E_sProdCalcEntryT.Equity;
            dataLoan.sEquityPe = 10000;
            dataLoan.sHas2ndFinPe = true;

            Assert.AreEqual(10000, dataLoan.sEquityPe, "sEquityPe");
            Assert.AreEqual(80000, dataLoan.sLAmtCalcPe, "sLAmtCalcPe");
            Assert.AreEqual(10000, dataLoan.sProOFinBalPe, "sProOFinBalPe");

        }
        [Test]
        public void TestHouseValueIsZero() 
        {
            CPageData dataLoan = CreateDataLoan();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sHouseValPe = 0;
            dataLoan.Save();

            dataLoan = CreateDataLoan();
            dataLoan.InitLoad();
            
            Assert.AreEqual(0, dataLoan.sHouseValPe, "sHouseValPe");
            Assert.AreEqual(0, dataLoan.sDownPmtPcPe, "sDownPmtPcPe");
            Assert.AreEqual(0, dataLoan.sEquityPe, "sEquityPe");
            Assert.AreEqual(0, dataLoan.sLtvRPe, "sLtvRPe");
            Assert.AreEqual(0, dataLoan.sLAmtCalcPe, "sLAmtCalcPe");
            Assert.AreEqual(0, dataLoan.sLtvROtherFinPe, "sLtvROtherFinPe");
            Assert.AreEqual(0, dataLoan.sProOFinBalPe, "sProOFinBalPe");
            Assert.AreEqual(0, dataLoan.sCltvRPe, "sCltvRPe");
        }

        [Test]
        public void TestInitial1stLien() 
        {
            // Initial House Value = 0.
            // User modify sale price.
            decimal[] initialValues = {
                                          0 /* sHouseValPe */, 
                                          0 /* sEquityPe */,
                                          0 /*sLAmtCalcPe */,
                                          0 /* sProOFinBalPe */
                                      };
            decimal[][] testCases = {
                                        //             Adj. HouseValue,    sHouseValPe,      sEquityPe,    sLAmtCalcPe,  sProOFinBalPe
                                        new decimal[] {         100000,         100000,              0,         100000,              0},
            };

            Verify(initialValues, E_sProdCalcEntryT.HouseVal, testCases);
        }

        [Test]
        public void TestModifyCltvValue1() 
        {
            // Initial CLTV <= 100 and there is 2nd financing.
            // User modify CLTV.
            decimal[] initialValues = {
                                          100000 /* sHouseValPe */, 
                                          10000 /* sEquityPe */,
                                          80000 /*sLAmtCalcPe */,
                                          10000 /* sProOFinBalPe */
                                      };

            decimal[][] testCases = {
                                        //                Adj. CltvRPe,    sHouseValPe,      sEquityPe,    sLAmtCalcPe,  sProOFinBalPe
                                        new decimal[] {              5,         100000,          95000,           5000,              0},
                                        new decimal[] {             10,         100000,          90000,          10000,              0},
                                        new decimal[] {             15,         100000,          85000,          15000,              0},
                                        new decimal[] {             20,         100000,          80000,          20000,              0},
                                        new decimal[] {             25,         100000,          75000,          25000,              0},
                                        new decimal[] {             30,         100000,          70000,          30000,              0},
                                        new decimal[] {             35,         100000,          65000,          35000,              0},
                                        new decimal[] {             40,         100000,          60000,          40000,              0},
                                        new decimal[] {             45,         100000,          55000,          45000,              0},
                                        new decimal[] {             50,         100000,          50000,          50000,              0},
                                        new decimal[] {             55,         100000,          45000,          55000,              0},
                                        new decimal[] {             60,         100000,          40000,          60000,              0},
                                        new decimal[] {             65,         100000,          35000,          65000,              0},
                                        new decimal[] {             70,         100000,          30000,          70000,              0},
                                        new decimal[] {             75,         100000,          25000,          75000,              0},
                                        new decimal[] {             80,         100000,          20000,          80000,              0},
                                        new decimal[] {             85,         100000,          15000,          80000,           5000},
                                        new decimal[] {             90,         100000,          10000,          80000,          10000},
                                        new decimal[] {             95,         100000,           5000,          80000,          15000},
                                        new decimal[] {            100,         100000,              0,          80000,          20000},
                                        new decimal[] {            105,         100000,          -5000,          80000,          25000},
                                        new decimal[] {            110,         100000,         -10000,          80000,          30000},
                                        new decimal[] {            115,         100000,         -15000,          80000,          35000},
            };

            Verify(initialValues, E_sProdCalcEntryT.CltvTarget, testCases);
        }
        [Test]
        public void TestModifyCltvValue2() 
        {
            // Initial CLTV > 100 and there is 2nd financing.
            // User modify CLTV.
            decimal[] initialValues = {
                                          100000 /* sHouseValPe */, 
                                          -5000 /* sEquityPe */,
                                          90000 /*sLAmtCalcPe */,
                                          15000 /* sProOFinBalPe */
                                      };

            decimal[][] testCases = {
                                        //                Adj. CltvRPe,    sHouseValPe,      sEquityPe,    sLAmtCalcPe,  sProOFinBalPe
                                        new decimal[] {              5,         100000,          95000,           5000,              0},
                                        new decimal[] {             10,         100000,          90000,          10000,              0},
                                        new decimal[] {             15,         100000,          85000,          15000,              0},
                                        new decimal[] {             20,         100000,          80000,          20000,              0},
                                        new decimal[] {             25,         100000,          75000,          25000,              0},
                                        new decimal[] {             30,         100000,          70000,          30000,              0},
                                        new decimal[] {             35,         100000,          65000,          35000,              0},
                                        new decimal[] {             40,         100000,          60000,          40000,              0},
                                        new decimal[] {             45,         100000,          55000,          45000,              0},
                                        new decimal[] {             50,         100000,          50000,          50000,              0},
                                        new decimal[] {             55,         100000,          45000,          55000,              0},
                                        new decimal[] {             60,         100000,          40000,          60000,              0},
                                        new decimal[] {             65,         100000,          35000,          65000,              0},
                                        new decimal[] {             70,         100000,          30000,          70000,              0},
                                        new decimal[] {             75,         100000,          25000,          75000,              0},
                                        new decimal[] {             80,         100000,          20000,          80000,              0},
                                        new decimal[] {             85,         100000,          15000,          85000,              0},
                                        new decimal[] {             90,         100000,          10000,          90000,              0},
                                        new decimal[] {             95,         100000,           5000,          90000,           5000},
                                        new decimal[] {            100,         100000,              0,          90000,          10000},
                                        new decimal[] {            105,         100000,          -5000,          90000,          15000},
                                        new decimal[] {            110,         100000,         -10000,          90000,          20000},
                                        new decimal[] {            115,         100000,         -15000,          90000,          25000},

            };

            Verify(initialValues, E_sProdCalcEntryT.CltvTarget, testCases);

        }
        [Test]
        public void TestModifyCltvValue3() 
        {
            // Initial CLTV <= 100 and there is NO 2nd financing.
            // User modify CLTV.
            decimal[] initialValues = {
                                          100000 /* sHouseValPe */, 
                                          20000 /* sEquityPe */,
                                          80000 /*sLAmtCalcPe */,
                                          0 /* sProOFinBalPe */
                                      };

            decimal[][] testCases = {
                                        //                Adj. CltvRPe,    sHouseValPe,      sEquityPe,    sLAmtCalcPe,  sProOFinBalPe
                                        new decimal[] {              5,         100000,          95000,           5000,              0},
                                        new decimal[] {             10,         100000,          90000,          10000,              0},
                                        new decimal[] {             15,         100000,          85000,          15000,              0},
                                        new decimal[] {             20,         100000,          80000,          20000,              0},
                                        new decimal[] {             25,         100000,          75000,          25000,              0},
                                        new decimal[] {             30,         100000,          70000,          30000,              0},
                                        new decimal[] {             35,         100000,          65000,          35000,              0},
                                        new decimal[] {             40,         100000,          60000,          40000,              0},
                                        new decimal[] {             45,         100000,          55000,          45000,              0},
                                        new decimal[] {             50,         100000,          50000,          50000,              0},
                                        new decimal[] {             55,         100000,          45000,          55000,              0},
                                        new decimal[] {             60,         100000,          40000,          60000,              0},
                                        new decimal[] {             65,         100000,          35000,          65000,              0},
                                        new decimal[] {             70,         100000,          30000,          70000,              0},
                                        new decimal[] {             75,         100000,          25000,          75000,              0},
                                        new decimal[] {             80,         100000,          20000,          80000,              0},
                                        new decimal[] {             85,         100000,          15000,          85000,              0},
                                        new decimal[] {             90,         100000,          10000,          90000,              0},
                                        new decimal[] {             95,         100000,           5000,          95000,              0},
                                        new decimal[] {            100,         100000,              0,         100000,              0},

            };

            Verify(initialValues, E_sProdCalcEntryT.CltvTarget, testCases);
        }


        [Test]
        public void TestModifyLAmtOtherFin1() 
        {
            // Initial CLTV < 100
            // User modify sProOFinBalPe.
            decimal[] initialValues = {
                                          100000 /* sHouseValPe */, 
                                          10000 /* sEquityPe */,
                                          80000 /*sLAmtCalcPe */,
                                          10000 /* sProOFinBalPe */
                                      };

            decimal[][] testCases = {
                                        //          Adj. sProOFinBalPe,    sHouseValPe,      sEquityPe,    sLAmtCalcPe,  sProOFinBalPe
                                        new decimal[] {          -5000,         100000,          20000,          80000,              0},
                                        new decimal[] {              0,         100000,          20000,          80000,              0},
                                        new decimal[] {           5000,         100000,          15000,          80000,           5000},
                                        new decimal[] {          10000,         100000,          10000,          80000,          10000},
                                        new decimal[] {          15000,         100000,           5000,          80000,          15000},
                                        new decimal[] {          20000,         100000,              0,          80000,          20000},
                                        new decimal[] {          25000,         100000,          -5000,          80000,          25000},
                                        new decimal[] {          30000,         100000,         -10000,          80000,          30000},
                                        new decimal[] {          35000,         100000,         -15000,          80000,          35000},
                                        new decimal[] {         105000,         100000,         -85000,          80000,         105000},


            };

            Verify(initialValues, E_sProdCalcEntryT.LAmtOtherFin, testCases);
        }
        [Test]
        public void TestModifyLAmtOtherFin2() 
        {
            // Initial CLTV > 100
            // User modify sProOFinBalPe.
            decimal[] initialValues = {
                                          100000 /* sHouseValPe */, 
                                          -10000 /* sEquityPe */,
                                          90000 /*sLAmtCalcPe */,
                                          20000 /* sProOFinBalPe */
                                      };
        
            decimal[][] testCases = {
                                        //          Adj. sProOFinBalPe,    sHouseValPe,      sEquityPe,    sLAmtCalcPe,  sProOFinBalPe
                                        new decimal[] {          -5000,         100000,          10000,          90000,              0},
                                        new decimal[] {              0,         100000,          10000,          90000,              0},
                                        new decimal[] {           5000,         100000,           5000,          90000,           5000},
                                        new decimal[] {          10000,         100000,              0,          90000,          10000},
                                        new decimal[] {          15000,         100000,          -5000,          90000,          15000},
                                        new decimal[] {          20000,         100000,         -10000,          90000,          20000},
                                        new decimal[] {          25000,         100000,         -15000,          90000,          25000},
                                        new decimal[] {          30000,         100000,         -20000,          90000,          30000},
                                        new decimal[] {          35000,         100000,         -25000,          90000,          35000},
                                        //new decimal[] {         105000,         100000,          -5000,              0,         105000},
        
        
            };
        
            Verify(initialValues, E_sProdCalcEntryT.LAmtOtherFin, testCases);
        }
        [Test]
        public void TestModifyLAmtOtherFin3() 
        {
            // Initial CLTV = 100 with sEquityPe = 0.
            // User modify increase sProOFinBalPe. Should decrease sLAmtCalcPe and keep sEquityPe = 0.
            decimal[] initialValues = {
                                          100000 /* sHouseValPe */, 
                                          0 /* sEquityPe */,
                                          80000 /*sLAmtCalcPe */,
                                          20000 /* sProOFinBalPe */
                                      };
        
            decimal[][] testCases = {
                                        //          Adj. sProOFinBalPe,    sHouseValPe,      sEquityPe,    sLAmtCalcPe,  sProOFinBalPe
                                        new decimal[] {          30000,         100000,         -10000,          80000,          30000},
        
        
            };
        
            Verify(initialValues, E_sProdCalcEntryT.LAmtOtherFin, testCases);
        }
        [Test]
        public void TestModifyLtvROther1() 
        {
            // Initial CLTV < 100
            // User modify LtvROther.
            decimal[] initialValues = {
                                          100000 /* sHouseValPe */, 
                                          10000 /* sEquityPe */,
                                          80000 /*sLAmtCalcPe */,
                                          10000 /* sProOFinBalPe */
                                      };

            decimal[][] testCases = {
                                        //              Adj. LtvROther,    sHouseValPe,      sEquityPe,    sLAmtCalcPe,  sProOFinBalPe
                                        new decimal[] {             -5,         100000,          20000,          80000,              0},
                                        new decimal[] {              0,         100000,          20000,          80000,              0},
                                        new decimal[] {              5,         100000,          15000,          80000,           5000},
                                        new decimal[] {             10,         100000,          10000,          80000,          10000},
                                        new decimal[] {             15,         100000,           5000,          80000,          15000},
                                        new decimal[] {             20,         100000,              0,          80000,          20000},
                                        new decimal[] {             25,         100000,          -5000,          80000,          25000},
                                        new decimal[] {             30,         100000,         -10000,          80000,          30000},
                                        new decimal[] {             35,         100000,         -15000,          80000,          35000},
                                        new decimal[] {            105,         100000,         -85000,          80000,         105000},



            };

            Verify(initialValues, E_sProdCalcEntryT.LtvTargetOtherFin, testCases);
        }
        [Test]
        public void TestModifyHouseValue1() 
        {
            // Initial CLTV <= 100 and there is 2nd financing.
            // User modify House Value.
            decimal[] initialValues = {
                                          100000 /* sHouseValPe */, 
                                          10000 /* sEquityPe */,
                                          80000 /*sLAmtCalcPe */,
                                          10000 /* sProOFinBalPe */
                                      };

            decimal[][] testCases = {
                                        //            Adj. sHouseValPe,    sHouseValPe,      sEquityPe,    sLAmtCalcPe,  sProOFinBalPe
                                        new decimal[] {          70000,          70000,          10000,          60000,              0},
                                        new decimal[] {          75000,          75000,          10000,          65000,              0},
                                        new decimal[] {          80000,          80000,          10000,          70000,              0},
                                        new decimal[] {          85000,          85000,          10000,          75000,              0},
                                        new decimal[] {          90000,          90000,          10000,          80000,              0},
                                        new decimal[] {          95000,          95000,          10000,          80000,           5000},
                                        new decimal[] {         100000,         100000,          10000,          80000,          10000},
                                        new decimal[] {         105000,         105000,          10000,          80000,          15000},
                                        new decimal[] {         110000,         110000,          10000,          80000,          20000},
                                        new decimal[] {         115000,         115000,          10000,          80000,          25000},
                                        new decimal[] {         120000,         120000,          10000,          80000,          30000},

            };

            Verify(initialValues, E_sProdCalcEntryT.HouseVal, testCases);
        }
        [Test]
        public void TestModifyHouseValue2() 
        {
            // Initial CLTV > 100 and there is 2nd financing.
            // User modify House Value.
            decimal[] initialValues = {
                                          100000 /* sHouseValPe */, 
                                          -10000 /* sEquityPe */,
                                          90000 /*sLAmtCalcPe */,
                                          20000 /* sProOFinBalPe */
                                      };

            decimal[][] testCases = {
                                        //            Adj. sHouseValPe,    sHouseValPe,      sEquityPe,    sLAmtCalcPe,  sProOFinBalPe
                                        new decimal[] {          70000,          70000,         -10000,          80000,              0},
                                        new decimal[] {          75000,          75000,         -10000,          85000,              0},
                                        new decimal[] {          80000,          80000,         -10000,          90000,              0},
                                        new decimal[] {          85000,          85000,         -10000,          90000,           5000},
                                        new decimal[] {          90000,          90000,         -10000,          90000,          10000},
                                        new decimal[] {          95000,          95000,         -10000,          90000,          15000},
                                        new decimal[] {         100000,         100000,         -10000,          90000,          20000},
                                        new decimal[] {         105000,         105000,         -10000,          90000,          25000},
                                        new decimal[] {         110000,         110000,         -10000,          90000,          30000},
                                        new decimal[] {         115000,         115000,         -10000,          90000,          35000},
                                        new decimal[] {         120000,         120000,         -10000,          90000,          40000},

            };

            Verify(initialValues, E_sProdCalcEntryT.HouseVal, testCases);
        }
        [Test]
        public void TestModifyHouseValue3() 
        {
            // Initial CLTV < 100 and there is NO 2nd financing.
            // User modify House Value.
            decimal[] initialValues = {
                                          100000 /* sHouseValPe */, 
                                          20000 /* sEquityPe */,
                                          80000 /*sLAmtCalcPe */,
                                          0 /* sProOFinBalPe */
                                      };

            decimal[][] testCases = {
                                        //            Adj. sHouseValPe,    sHouseValPe,      sEquityPe,    sLAmtCalcPe,  sProOFinBalPe
                                        new decimal[] {          70000,          70000,          20000,          50000,              0},
                                        new decimal[] {          75000,          75000,          20000,          55000,              0},
                                        new decimal[] {          80000,          80000,          20000,          60000,              0},
                                        new decimal[] {          85000,          85000,          20000,          65000,              0},
                                        new decimal[] {          90000,          90000,          20000,          70000,              0},
                                        new decimal[] {          95000,          95000,          20000,          75000,              0},
                                        new decimal[] {         100000,         100000,          20000,          80000,              0},
                                        new decimal[] {         105000,         105000,          20000,          85000,              0},
                                        new decimal[] {         110000,         110000,          20000,          90000,              0},
                                        new decimal[] {         115000,         115000,          20000,          95000,              0},
                                        new decimal[] {         120000,         120000,          20000,         100000,              0},

            };

            Verify(initialValues, E_sProdCalcEntryT.HouseVal, testCases);
        }

        [Test]
        public void TestModifyEquityValue1() 
        {
            // Initial CLTV < 100 and there is 2nd financing.
            // User modify Equity Value.
            decimal[] initialValues = {
                                          100000 /* sHouseValPe */, 
                                          0 /* sEquityPe */,
                                          85000 /*sLAmtCalcPe */,
                                          15000 /* sProOFinBalPe */
                                      };

            decimal[][] testCases = {
                                        //            Adj. Equity,    sHouseValPe,      sEquityPe,    sLAmtCalcPe,  sProOFinBalPe
                                        new decimal[] {         -20000,         100000,         -20000,          85000,          35000},
                                        new decimal[] {         -15000,         100000,         -15000,          85000,          30000},
                                        new decimal[] {         -10000,         100000,         -10000,          85000,          25000},
                                        new decimal[] {          -5000,         100000,          -5000,          85000,          20000},
                                        new decimal[] {              0,         100000,              0,          85000,          15000},
                                        new decimal[] {           5000,         100000,           5000,          85000,          10000},
                                        new decimal[] {          10000,         100000,          10000,          85000,           5000},
                                        new decimal[] {          15000,         100000,          15000,          85000,              0},
                                        new decimal[] {          20000,         100000,          20000,          80000,              0},
                                        new decimal[] {          25000,         100000,          25000,          75000,              0},
                                    };
            Verify(initialValues, E_sProdCalcEntryT.Equity, testCases);

        }
        [Test]
        public void TestModifyEquityValue2() 
        {
            // Initial CLTV > 100 and there is 2nd financing.
            // User modify Equity Value.
            decimal[] initialValues = {
                                          100000 /* sHouseValPe */, 
                                          -5000 /* sEquityPe */,
                                          90000 /*sLAmtCalcPe */,
                                          15000 /* sProOFinBalPe */
                                      };

            decimal[][] testCases = {
                                        //            Adj. Equity,    sHouseValPe,      sEquityPe,    sLAmtCalcPe,  sProOFinBalPe
                                        new decimal[] {         -20000,         100000,         -20000,          90000,          30000},
                                        new decimal[] {         -15000,         100000,         -15000,          90000,          25000},
                                        new decimal[] {         -10000,         100000,         -10000,          90000,          20000},
                                        new decimal[] {          -5000,         100000,          -5000,          90000,          15000},
                                        new decimal[] {              0,         100000,              0,          90000,          10000},
                                        new decimal[] {           5000,         100000,           5000,          90000,           5000},
                                        new decimal[] {          10000,         100000,          10000,          90000,              0},
                                        new decimal[] {          15000,         100000,          15000,          85000,              0},
                                        new decimal[] {          20000,         100000,          20000,          80000,              0},
                                        new decimal[] {          25000,         100000,          25000,          75000,              0},
                                    };
            Verify(initialValues, E_sProdCalcEntryT.Equity, testCases);

        }
        [Test]
        public void TestModifyEquityValue3() 
        {
            // Initial CLTV < 100 and there is NO 2nd financing.
            // User modify Equity Value.
            decimal[] initialValues = {
                                          100000 /* sHouseValPe */, 
                                          10000 /* sEquityPe */,
                                          90000 /*sLAmtCalcPe */,
                                          0 /* sProOFinBalPe */
                                      };

            decimal[][] testCases = {
                                        //            Adj. Equity,    sHouseValPe,      sEquityPe,    sLAmtCalcPe,  sProOFinBalPe
                                        new decimal[] {         -20000,         100000,         -20000,         120000,              0},
                                        new decimal[] {         -15000,         100000,         -15000,         115000,              0},
                                        new decimal[] {         -10000,         100000,         -10000,         110000,              0},
                                        new decimal[] {          -5000,         100000,          -5000,         105000,              0},
                                        new decimal[] {              0,         100000,              0,         100000,              0},
                                        new decimal[] {           5000,         100000,           5000,          95000,              0},
                                        new decimal[] {          10000,         100000,          10000,          90000,              0},
                                        new decimal[] {          15000,         100000,          15000,          85000,              0},
                                        new decimal[] {          20000,         100000,          20000,          80000,              0},
                                        new decimal[] {          25000,         100000,          25000,          75000,              0},
                                    };
            Verify(initialValues, E_sProdCalcEntryT.Equity, testCases);

        }


        [Test]
        public void TestModifyDownPaymentPcValue1() 
        {
            // Initial CLTV < 100 and there is 2nd financing.
            // User modify DownPaymentPc Value.
            decimal[] initialValues = {
                                          100000 /* sHouseValPe */, 
                                          0 /* sEquityPe */,
                                          85000 /*sLAmtCalcPe */,
                                          15000 /* sProOFinBalPe */
                                      };

            decimal[][] testCases = {
                                        //          Adj. DownPaymentPc,    sHouseValPe,      sEquityPe,    sLAmtCalcPe,  sProOFinBalPe
                                        new decimal[] {            -20,         100000,         -20000,          85000,          35000},
                                        new decimal[] {            -15,         100000,         -15000,          85000,          30000},
                                        new decimal[] {            -10,         100000,         -10000,          85000,          25000},
                                        new decimal[] {             -5,         100000,          -5000,          85000,          20000},
                                        new decimal[] {              0,         100000,              0,          85000,          15000},
                                        new decimal[] {              5,         100000,           5000,          85000,          10000},
                                        new decimal[] {             10,         100000,          10000,          85000,           5000},
                                        new decimal[] {             15,         100000,          15000,          85000,              0},
                                        new decimal[] {             20,         100000,          20000,          80000,              0},
                                        new decimal[] {             25,         100000,          25000,          75000,              0},
            };
            Verify(initialValues, E_sProdCalcEntryT.DwnPmtPc, testCases);

        }
        [Test]
        public void TestModifyDownPaymentPcValue2() 
        {
            // Initial CLTV > 100 and there is 2nd financing.
            // User modify DownPaymentPc Value.
            decimal[] initialValues = {
                                          100000 /* sHouseValPe */, 
                                          -5000 /* sEquityPe */,
                                          90000 /*sLAmtCalcPe */,
                                          15000 /* sProOFinBalPe */
                                      };

            decimal[][] testCases = {
                                        //          Adj. DownPaymentPc,    sHouseValPe,      sEquityPe,    sLAmtCalcPe,  sProOFinBalPe
                                        new decimal[] {            -20,         100000,         -20000,          90000,          30000},
                                        new decimal[] {            -15,         100000,         -15000,          90000,          25000},
                                        new decimal[] {            -10,         100000,         -10000,          90000,          20000},
                                        new decimal[] {             -5,         100000,          -5000,          90000,          15000},
                                        new decimal[] {              0,         100000,              0,          90000,          10000},
                                        new decimal[] {              5,         100000,           5000,          90000,           5000},
                                        new decimal[] {             10,         100000,          10000,          90000,              0},
                                        new decimal[] {             15,         100000,          15000,          85000,              0},
                                        new decimal[] {             20,         100000,          20000,          80000,              0},
                                        new decimal[] {             25,         100000,          25000,          75000,              0},
            };
            Verify(initialValues, E_sProdCalcEntryT.DwnPmtPc, testCases);

        }
        [Test]
        public void TestModifyDownPaymentPcValue3() 
        {
            // Initial CLTV < 100 and there is NO 2nd financing.
            // User modify DownPaymentPc Value.
            decimal[] initialValues = {
                                          100000 /* sHouseValPe */, 
                                          10000 /* sEquityPe */,
                                          90000 /*sLAmtCalcPe */,
                                          0 /* sProOFinBalPe */
                                      };

            decimal[][] testCases = {
                                        //          Adj. DownPaymentPc,    sHouseValPe,      sEquityPe,    sLAmtCalcPe,  sProOFinBalPe
                                        new decimal[] {            -20,         100000,         -20000,         120000,              0},
                                        new decimal[] {            -15,         100000,         -15000,         115000,              0},
                                        new decimal[] {            -10,         100000,         -10000,         110000,              0},
                                        new decimal[] {             -5,         100000,          -5000,         105000,              0},
                                        new decimal[] {              0,         100000,              0,         100000,              0},
                                        new decimal[] {              5,         100000,           5000,          95000,              0},
                                        new decimal[] {             10,         100000,          10000,          90000,              0},
                                        new decimal[] {             15,         100000,          15000,          85000,              0},
                                        new decimal[] {             20,         100000,          20000,          80000,              0},
                                        new decimal[] {             25,         100000,          25000,          75000,              0},
            };
            Verify(initialValues, E_sProdCalcEntryT.DwnPmtPc, testCases);

        }
        [Test]
        public void TestModifyLAmtValue1() 
        {
            // Initial CLTV < 100 and there is 2nd financing.
            // User modify LAmt Value.
            decimal[] initialValues = {
                                          100000 /* sHouseValPe */, 
                                          10000 /* sEquityPe */,
                                          80000 /*sLAmtCalcPe */,
                                          10000 /* sProOFinBalPe */
                                      };

            decimal[][] testCases = {
                                        //                   Adj. LAmt,    sHouseValPe,      sEquityPe,    sLAmtCalcPe,  sProOFinBalPe
                                        new decimal[] {          60000,         100000,          10000,          60000,          30000},
                                        new decimal[] {          65000,         100000,          10000,          65000,          25000},
                                        new decimal[] {          70000,         100000,          10000,          70000,          20000},
                                        new decimal[] {          75000,         100000,          10000,          75000,          15000},
                                        new decimal[] {          80000,         100000,          10000,          80000,          10000},
                                        new decimal[] {          85000,         100000,          10000,          85000,           5000},
                                        new decimal[] {          90000,         100000,          10000,          90000,              0},
                                        new decimal[] {          95000,         100000,           5000,          95000,              0},
                                        new decimal[] {         100000,         100000,              0,         100000,              0},
                                        new decimal[] {         105000,         100000,          -5000,         105000,              0},

                                    };
            Verify(initialValues, E_sProdCalcEntryT.LAmt, testCases);
        }

        [Test]
        public void TestModifyLAmtValue2() 
        {
            // Initial CLTV > 100 and there is 2nd financing.
            // User modify LAmt Value.
            decimal[] initialValues = {
                                          100000 /* sHouseValPe */, 
                                          -10000 /* sEquityPe */,
                                          90000 /*sLAmtCalcPe */,
                                          20000 /* sProOFinBalPe */
                                      };

            decimal[][] testCases = {
                                        //                   Adj. LAmt,    sHouseValPe,      sEquityPe,    sLAmtCalcPe,  sProOFinBalPe
                                        new decimal[] {          60000,         100000,         -10000,          60000,          50000},
                                        new decimal[] {          65000,         100000,         -10000,          65000,          45000},
                                        new decimal[] {          70000,         100000,         -10000,          70000,          40000},
                                        new decimal[] {          75000,         100000,         -10000,          75000,          35000},
                                        new decimal[] {          80000,         100000,         -10000,          80000,          30000},
                                        new decimal[] {          85000,         100000,         -10000,          85000,          25000},
                                        new decimal[] {          90000,         100000,         -10000,          90000,          20000},
                                        new decimal[] {          95000,         100000,         -10000,          95000,          15000},
                                        new decimal[] {         100000,         100000,         -10000,         100000,          10000},
                                        new decimal[] {         105000,         100000,         -10000,         105000,           5000},
                                        new decimal[] {         110000,         100000,         -10000,         110000,              0},
                                        new decimal[] {         115000,         100000,         -15000,         115000,              0},

                                    };
            Verify(initialValues, E_sProdCalcEntryT.LAmt, testCases);
        }

        [Test]
        public void TestModifyLAmtValue3() 
        {
            // Initial CLTV < 100 and there is NO 2nd financing.
            // User modify LAmt Value.
            decimal[] initialValues = {
                                          100000 /* sHouseValPe */, 
                                          20000 /* sEquityPe */,
                                          80000 /*sLAmtCalcPe */,
                                          0 /* sProOFinBalPe */
                                      };

            decimal[][] testCases = {
                                        //                   Adj. LAmt,    sHouseValPe,      sEquityPe,    sLAmtCalcPe,  sProOFinBalPe
                                        new decimal[] {          60000,         100000,          40000,          60000,              0},
                                        new decimal[] {          65000,         100000,          35000,          65000,              0},
                                        new decimal[] {          70000,         100000,          30000,          70000,              0},
                                        new decimal[] {          75000,         100000,          25000,          75000,              0},
                                        new decimal[] {          80000,         100000,          20000,          80000,              0},
                                        new decimal[] {          85000,         100000,          15000,          85000,              0},
                                        new decimal[] {          90000,         100000,          10000,          90000,              0},
                                        new decimal[] {          95000,         100000,           5000,          95000,              0},
                                        new decimal[] {         100000,         100000,              0,         100000,              0},
                                        new decimal[] {         105000,         100000,          -5000,         105000,              0},
                                        new decimal[] {         110000,         100000,         -10000,         110000,              0},

                                    };
            Verify(initialValues, E_sProdCalcEntryT.LAmt, testCases);
        }


        [Test]
        public void TestModifyLtvRValue1() 
        {
            // Initial CLTV < 100 and there is 2nd financing.
            // User modify LtvR Value.
            decimal[] initialValues = {
                                          100000 /* sHouseValPe */, 
                                          10000 /* sEquityPe */,
                                          80000 /*sLAmtCalcPe */,
                                          10000 /* sProOFinBalPe */
                                      };

            decimal[][] testCases = {
                                        //                   Adj. LtvR,    sHouseValPe,      sEquityPe,    sLAmtCalcPe,  sProOFinBalPe
                                        new decimal[] {             60,         100000,          10000,          60000,          30000},
                                        new decimal[] {             65,         100000,          10000,          65000,          25000},
                                        new decimal[] {             70,         100000,          10000,          70000,          20000},
                                        new decimal[] {             75,         100000,          10000,          75000,          15000},
                                        new decimal[] {             80,         100000,          10000,          80000,          10000},
                                        new decimal[] {             85,         100000,          10000,          85000,           5000},
                                        new decimal[] {             90,         100000,          10000,          90000,              0},
                                        new decimal[] {             95,         100000,           5000,          95000,              0},
                                        new decimal[] {            100,         100000,              0,         100000,              0},
                                        new decimal[] {            105,         100000,          -5000,         105000,              0},

            };
            Verify(initialValues, E_sProdCalcEntryT.LtvTarget, testCases);
        }

        [Test]
        public void TestModifyLtvRValue2() 
        {
            // Initial CLTV > 100 and there is 2nd financing.
            // User modify LtvR Value.
            decimal[] initialValues = {
                                          100000 /* sHouseValPe */, 
                                          -10000 /* sEquityPe */,
                                          90000 /*sLAmtCalcPe */,
                                          20000 /* sProOFinBalPe */
                                      };

            decimal[][] testCases = {
                                        //                   Adj. LtvR,    sHouseValPe,      sEquityPe,    sLAmtCalcPe,  sProOFinBalPe
                                        new decimal[] {             60,         100000,         -10000,          60000,          50000},
                                        new decimal[] {             65,         100000,         -10000,          65000,          45000},
                                        new decimal[] {             70,         100000,         -10000,          70000,          40000},
                                        new decimal[] {             75,         100000,         -10000,          75000,          35000},
                                        new decimal[] {             80,         100000,         -10000,          80000,          30000},
                                        new decimal[] {             85,         100000,         -10000,          85000,          25000},
                                        new decimal[] {             90,         100000,         -10000,          90000,          20000},
                                        new decimal[] {             95,         100000,         -10000,          95000,          15000},
                                        new decimal[] {            100,         100000,         -10000,         100000,          10000},
                                        new decimal[] {            105,         100000,         -10000,         105000,           5000},
                                        new decimal[] {            110,         100000,         -10000,         110000,              0},
                                        new decimal[] {            115,         100000,         -15000,         115000,              0},

            };
            Verify(initialValues, E_sProdCalcEntryT.LtvTarget, testCases);
        }

        [Test]
        public void TestModifyLtvRValue3() 
        {
            // Initial CLTV < 100 and there is NO 2nd financing.
            // User modify LtvR Value.
            decimal[] initialValues = {
                                          100000 /* sHouseValPe */, 
                                          20000 /* sEquityPe */,
                                          80000 /*sLAmtCalcPe */,
                                          0 /* sProOFinBalPe */
                                      };

            decimal[][] testCases = {
                                        //                   Adj. LtvR,    sHouseValPe,      sEquityPe,    sLAmtCalcPe,  sProOFinBalPe
                                        new decimal[] {             60,         100000,          40000,          60000,              0},
                                        new decimal[] {             65,         100000,          35000,          65000,              0},
                                        new decimal[] {             70,         100000,          30000,          70000,              0},
                                        new decimal[] {             75,         100000,          25000,          75000,              0},
                                        new decimal[] {             80,         100000,          20000,          80000,              0},
                                        new decimal[] {             85,         100000,          15000,          85000,              0},
                                        new decimal[] {             90,         100000,          10000,          90000,              0},
                                        new decimal[] {             95,         100000,           5000,          95000,              0},
                                        new decimal[] {            100,         100000,              0,         100000,              0},
                                        new decimal[] {            105,         100000,          -5000,         105000,              0},
                                        new decimal[] {            110,         100000,         -10000,         110000,              0},

            };
            Verify(initialValues, E_sProdCalcEntryT.LtvTarget, testCases);
        }

        private CPageData CreateDataLoan() 
        {
            CPageData dataLoan = FakePageData.Create();
            dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
            return dataLoan;
        }

        private void Verify(decimal[] initialSettings, E_sProdCalcEntryT sProdCalcEntryT, decimal[][] testCases) 
        {
            
            // Setup initial settings.
            CPageData dataLoan = CreateDataLoan();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sProdCalcEntryT = E_sProdCalcEntryT.HouseVal;
            dataLoan.sHouseValPe = initialSettings[0];
            dataLoan.sEquityPe = initialSettings[1];
            dataLoan.sLAmtCalcPe = initialSettings[2];
            dataLoan.sProOFinBalPe = initialSettings[3];
            dataLoan.Save();

            string initialSettingsDescription = string.Format("Initial: sHouseValPe={0}, sEquityPe={1},sLAmtCalcPe={2},sProOFinBalPe={3}",
                initialSettings[0], initialSettings[1], initialSettings[2], initialSettings[3]);
            int i = 0;
            foreach (decimal[] testCase in testCases) 
            {
                string msg = string.Format("{3} - {0}. Adjust {1}. New Value={2}. ", initialSettingsDescription, sProdCalcEntryT, testCase[0], i);

                dataLoan = CreateDataLoan();
                dataLoan.InitLoad();

                // Set new adjustment.
                dataLoan.sProdCalcEntryT = sProdCalcEntryT;
                switch (sProdCalcEntryT) 
                {
                    case E_sProdCalcEntryT.CltvTarget:
                        dataLoan.sCltvRPe = testCase[0];
                        break;
                    case E_sProdCalcEntryT.DwnPmtPc:
                        dataLoan.sDownPmtPcPe = testCase[0];
                        break;
                    case E_sProdCalcEntryT.Equity:
                        dataLoan.sEquityPe = testCase[0];
                        break;
                    case E_sProdCalcEntryT.HouseVal:
                        dataLoan.sHouseValPe = testCase[0];
                        break;
                    case E_sProdCalcEntryT.LAmt:
                        dataLoan.sLAmtCalcPe = testCase[0];
                        break;
                    case E_sProdCalcEntryT.LAmtOtherFin:
                        dataLoan.sProOFinBalPe = testCase[0];
                        break;
                    case E_sProdCalcEntryT.LtvTarget:
                        dataLoan.sLtvRPe = testCase[0];
                        break;
                    case E_sProdCalcEntryT.LtvTargetOtherFin:
                        dataLoan.sLtvROtherFinPe = testCase[0];
                        break;
                    default:
                        Assert.Fail("Unhandle sProdCalcEntryT." + sProdCalcEntryT);
                        break;
                }

                // Verify.
                Assert.AreEqual(testCase[1], Math.Round(dataLoan.sHouseValPe, 5), msg + "sHouseValPe");
                Assert.AreEqual(testCase[2], Math.Round(dataLoan.sEquityPe, 5), msg + "sEquityPe");
                Assert.AreEqual(testCase[3], Math.Round(dataLoan.sLAmtCalcPe, 5), msg + "sLAmtCalcPe");
                Assert.AreEqual(testCase[4], Math.Round(dataLoan.sProOFinBalPe, 5), msg + "sProOFinBalPe");

                
                Assert.AreEqual(100, Math.Round(dataLoan.sDownPmtPcPe + dataLoan.sCltvRPe, 5), msg + "sDownPmtPcPe + sCltvRPe = 100.");
                Assert.AreEqual(Math.Round(dataLoan.sCltvRPe, 5), Math.Round(dataLoan.sLtvRPe + dataLoan.sLtvROtherFinPe, 5), msg + "sCltvRPe = sLtvRPe + sLtvROtherFinPe");
                Assert.AreEqual(Math.Round(dataLoan.sEquityPe, 5), Math.Round(dataLoan.sHouseValPe - dataLoan.sLAmtCalcPe - dataLoan.sProOFinBalPe, 5), msg + "sEquityPe = sHouseValPe - sLAmtCalcPe - sProOFinBalPe "); //changed calc because of opm 104410
                Assert.AreEqual(Math.Round(dataLoan.sLAmtCalcPe, 5), Math.Round(dataLoan.sHouseValPe * dataLoan.sLtvRPe / 100M, 5), msg + "sLAmtCalcPe = sHouseValPe * sLtvRPe / 100");
                Assert.AreEqual(Math.Round(dataLoan.sProOFinBalPe, 5), Math.Round(dataLoan.sHouseValPe * dataLoan.sLtvROtherFinPe / 100M, 5), msg + "sProOFinBalPe = sHouseValPe * sLtvROtherFinPe / 100");

                i++;
            }
        }


    }
}
