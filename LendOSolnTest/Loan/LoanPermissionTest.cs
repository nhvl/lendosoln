/// Author: David Dao

using System;
using NUnit.Framework;

using LendOSolnTest.Common;
using DataAccess;
using LendersOffice.Security;
using LendersOffice.Audit;
using LendersOffice.Constants;

namespace LendOSolnTest.Loan
{
    [TestFixture]
    public class LoanPermissionTest
    {
        private Guid m_sLId = Guid.Empty;

        [TestFixtureSetUp]
        public void Setup() 
        {
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.LoanOfficer);
            Assert.IsNotNull(principal);

            CLoanFileCreator fileCreator = CLoanFileCreator.GetCreator(principal, E_LoanCreationSource.UserCreateFromBlank);
            m_sLId = fileCreator.CreateBlankLoanFile();
        }

        [TestFixtureTearDown]
        public void TearDown() 
        {
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.LoanOfficer);
            Assert.IsNotNull(principal);

            if (Guid.Empty != m_sLId) 
            {
                Tools.DeclareLoanFileInvalid(principal, m_sLId, false, false);
            }

        }

        [Test]
        public void InitLoadFromCreator() 
        {
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.LoanOfficer);

            CPageData dataLoan = CreateDataLoan(m_sLId);
            dataLoan.InitLoad();

        }
        [Test]
        [ExpectedException(typeof(PageDataLoadDenied))]
        public void InitLoadWithDifferentUser() 
        {
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.Processor);

            CPageData dataLoan = CreateDataLoan(m_sLId);
            dataLoan.InitLoad();
        }
        [Test]
        public void InitLoadWithUserWithSameBranchAccessLevel() 
        {
            // 11/6/2008 dd - Access loan from same branch
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.MainBranch_LoanOfficer);

            CPageData dataLoan = CreateDataLoan(m_sLId);
            dataLoan.InitLoad();
        }
        [Test]
        [ExpectedException(typeof(PageDataLoadDenied))]
        public void InitLoadWithUserWithDifferentBranch() 
        {
            // 11/6/2008 dd - Access loan from different branch
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.Branch1_LoanOfficer);

            CPageData dataLoan = CreateDataLoan(m_sLId);
            dataLoan.InitLoad();
        }

        [Test]
        public void InitLoadWithUserWithCorporateAccess() 
        {
            // 11/6/2008 dd - Access loan with corporate level.
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.Corporate_LoanOfficer);

            CPageData dataLoan = CreateDataLoan(m_sLId);
            dataLoan.InitLoad();
        }

        [Test]
        public void InitSaveFromCreator() 
        {
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.LoanOfficer);

            CPageData dataLoan = CreateDataLoan(m_sLId);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

        }
        [Test]
        [ExpectedException(typeof(PageDataLoadDenied))]
        public void InitSaveWithDifferentUser() 
        {
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.Processor);

            CPageData dataLoan = CreateDataLoan(m_sLId);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
        }
        [Test]
        public void InitSaveWithUserWithSameBranchAccessLevel() 
        {
            // 11/6/2008 dd - Access loan from same branch
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.MainBranch_LoanOfficer);

            CPageData dataLoan = CreateDataLoan(m_sLId);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
        }
        [Test]
        [ExpectedException(typeof(PageDataLoadDenied))]
        public void InitSaveWithUserWithDifferentBranch() 
        {
            // 11/6/2008 dd - Access loan from different branch
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.Branch1_LoanOfficer);

            CPageData dataLoan = CreateDataLoan(m_sLId);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
        }

        [Test]
        public void InitSaveWithUserWithCorporateAccess() 
        {
            // 11/6/2008 dd - Access loan with corporate level.
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.Corporate_LoanOfficer);

            CPageData dataLoan = CreateDataLoan(m_sLId);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
        }

        private CPageData CreateDataLoan(Guid sLId) 
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(LoanPermissionTest));
        }
	}
}
