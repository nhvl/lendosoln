﻿using DataAccess;
using LendOSolnTest.Common;
using LendOSolnTest.Fakes;
using NUnit.Framework;

namespace LendOSolnTest.Loan
{
    [TestFixture]
    public class Gfe2010LoanTest
    {
        [TestFixtureSetUp]
        public void Setup()
        {
            LoginTools.LoginAs(TestAccountType.LoanOfficer);
        }

        [Test]
        public void Test_sGfeFirstAdjProThisMPmtAndMIns_sGfeMaxProThisMPmtAndMIns_FixLoan()
        {
            CPageData dataLoan = CreatePageData();
            dataLoan.InitLoad();

            dataLoan.sLAmtCalc_rep = "300,000";
            dataLoan.sLAmtLckd = true;

            dataLoan.sNoteIR_rep = "5.000";
            dataLoan.sTerm_rep = "360";
            dataLoan.sDue_rep = "360";
            dataLoan.sFinMethT = E_sFinMethT.Fixed;

            Assert.AreEqual("$0.00", dataLoan.sGfeFirstAdjProThisMPmtAndMIns_rep, "sGfeFirstAdjProThisMPmtAndMIns_rep");
            Assert.AreEqual("$0.00", dataLoan.sGfeMaxProThisMPmtAndMIns_rep, "sGfeMaxProThisMPmtAndMIns_rep");
        }
        [Test]
        public void Test_sGfeFirstAdjProThisMPmtAndMIns_sGfeMaxProThisMPmtAndMIns_ArmLoan_NOMI()
        {
            CPageData dataLoan = CreatePageData();
            dataLoan.InitLoad();

            dataLoan.sLAmtCalc_rep = "300,000";
            dataLoan.sLAmtLckd = true;

            dataLoan.sNoteIR_rep = "5.000";
            dataLoan.sTerm_rep = "360";
            dataLoan.sDue_rep = "360";
            dataLoan.sFinMethT = E_sFinMethT.ARM;

            dataLoan.sRAdj1stCapR_rep = "2.000%";
            dataLoan.sRAdj1stCapMon_rep = "12";
            dataLoan.sRAdjCapR_rep = "2.000%";
            dataLoan.sRAdjCapMon_rep = "12";
            dataLoan.sRAdjLifeCapR_rep = "6.000%";

            Assert.AreEqual("$1,986.64", dataLoan.sGfeFirstAdjProThisMPmtAndMIns_rep, "sGfeFirstAdjProThisMPmtAndMIns_rep");
            Assert.AreEqual("$2,803.16", dataLoan.sGfeMaxProThisMPmtAndMIns_rep, "sGfeMaxProThisMPmtAndMIns_rep");
        }
        [Test]
        public void Test_sGfeFirstAdjProThisMPmtAndMIns_sGfeMaxProThisMPmtAndMIns_ArmLoan()
        {
            CPageData dataLoan = CreatePageData();
            dataLoan.InitLoad();

            dataLoan.sLAmtCalc_rep = "300,000";
            dataLoan.sLAmtLckd = true;

            dataLoan.sNoteIR_rep = "5.000";
            dataLoan.sTerm_rep = "360";
            dataLoan.sDue_rep = "360";
            dataLoan.sFinMethT = E_sFinMethT.ARM;
            dataLoan.sProMInsMb_rep = "$100";

            dataLoan.sRAdj1stCapR_rep = "2.000%";
            dataLoan.sRAdj1stCapMon_rep = "12";
            dataLoan.sRAdjCapR_rep = "2.000%";
            dataLoan.sRAdjCapMon_rep = "12";
            dataLoan.sRAdjLifeCapR_rep = "6.000%";

            Assert.AreEqual("$2,086.64", dataLoan.sGfeFirstAdjProThisMPmtAndMIns_rep, "sGfeFirstAdjProThisMPmtAndMIns_rep");
            Assert.AreEqual("$2,903.16", dataLoan.sGfeMaxProThisMPmtAndMIns_rep, "sGfeMaxProThisMPmtAndMIns_rep");
        }

        private CPageData CreatePageData()
        {
            return FakePageData.Create();
        }
    }
}
