﻿using System;
using System.Data;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Constants;
using LendOSolnTest.Common;
using LendOSolnTest.Fakes;
using NUnit.Framework;

namespace LendOSolnTest.Loan
{

    [TestFixture]
    public class LoanOfficerLendingLicenseTest
    {
        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            LoginTools.LoginAs(TestAccountType.LoTest001);
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
            FakePageDataUtilities.Instance.Reset();
        }

        private CPageData CreateDataObject()
        {
            var dataLoan = FakePageData.Create();

            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            DataSet ds = dataLoan.sAgentDataSet;
            ds.Clear();
            dataLoan.sAgentDataSet = ds;
            dataLoan.Save();

            return dataLoan;
        }

        [Test]
        public void LoanOfficerWithExpiredLicense()
        {
            // This test will make sure that we do not populate expire license number. We will blank out any existing license in Agent object.
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            CAgentFields agent = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.CreateNew);
            agent.AgentName = "LOAN_OFFICER";
            agent.LicenseInfoList.Add(new LicenseInfo() { License = "LICENSE001", State = "CA", ExpD = "1/1/2000" });
            agent.CompanyLicenseInfoList.Add(new LicenseInfo() { License = "COMPANYLICENSE001", State = "CA", ExpD = "1/1/2000" });

            agent.Update();
            dataLoan.Save();

            dataLoan = FakePageData.Create();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            dataLoan.sSpState = "CA";
            dataLoan.Save();

            // Verify.
            dataLoan = FakePageData.Create();
            dataLoan.InitLoad();
            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.CreateNew);

            Assert.IsTrue(agent.LicenseNumOfAgentIsExpired);
            Assert.IsTrue(agent.LicenseNumOfCompanyIsExpired);

            Assert.AreNotEqual(string.Empty, agent.LicenseNumOfAgent);
            Assert.AreNotEqual(string.Empty, agent.LicenseNumOfCompany);
        }
        [Test]
        public void LoanOfficerWithExpiredLicenseWithTodayDate()
        {
            // This test will make sure that we do not populate expire license number. We will blank out any existing license in Agent object.

            DateTime dt = DateTime.Now;
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            CAgentFields agent = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.CreateNew);
            agent.AgentName = "LOAN_OFFICER";
            agent.LicenseInfoList.Add(new LicenseInfo() { License = "LICENSE001", State = "CA", ExpD = dt.ToString("MM/dd/yyyy") });
            agent.CompanyLicenseInfoList.Add(new LicenseInfo() { License = "COMPANYLICENSE001", State = "CA", ExpD = dt.ToString("MM/dd/yyyy") });

            agent.Update();
            dataLoan.Save();

            dataLoan = FakePageData.Create();

            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            dataLoan.sSpState = "CA";
            dataLoan.Save();

            // Verify.
            dataLoan = FakePageData.Create();
            dataLoan.InitLoad();
            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.CreateNew);

            Assert.IsTrue(agent.LicenseNumOfAgentIsExpired);
            Assert.IsTrue(agent.LicenseNumOfCompanyIsExpired);

            Assert.AreNotEqual(string.Empty, agent.LicenseNumOfAgent);
            Assert.AreNotEqual(string.Empty, agent.LicenseNumOfCompany);
        }
        [Test]
        public void LoanOfficerWithNoLendingLicenseList()
        {
            // Set up empty loan officer with no license number.
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            CAgentFields agent = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.CreateNew);
            agent.AgentName = "LOAN_OFFICER";

            agent.Update();

            dataLoan.Save();

            // Set sSpState to AZ.
            dataLoan = FakePageData.Create();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            dataLoan.sSpState = "AZ";
            dataLoan.Save();

            // Verify that License Number still blank.
            dataLoan = FakePageData.Create();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.CreateNew);
            Assert.AreEqual("", agent.LicenseNumOfAgent);


        }
        [Test]
        public void LoanOfficerWithInitialLicenseNumber()
        {
            // Set up loan officer with initial license number.
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            CAgentFields agent = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.CreateNew);
            agent.AgentName = "LOAN_OFFICER";
            agent.LicenseInfoList.Add(new LicenseInfo() { License = "LICENSE001", State = "CA", ExpD = "1/1/2099" });
            agent.CompanyLicenseInfoList.Add(new LicenseInfo() { License = "COMPANYLICENSE001", State = "CA", ExpD = "1/1/2099" });

            agent.Update();

            dataLoan.Save();

            dataLoan = FakePageData.Create();

            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            dataLoan.sSpState = "CA";
            dataLoan.Save();

            dataLoan = FakePageData.Create();

            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.CreateNew);
            Assert.AreEqual("LICENSE001", agent.LicenseNumOfAgent);
            Assert.AreEqual("COMPANYLICENSE001", agent.LicenseNumOfCompany);
        }
        [Test]
        public void LoanOfficerWithNoInitialLicenseNumber()
        {
            // Set up empty loan officer with no license number.
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            CAgentFields agent = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.CreateNew);
            agent.AgentName = "LOAN_OFFICER";
            agent.LicenseInfoList.Add(new LicenseInfo() { License = "LICENSE001", State = "CA", ExpD = "1/1/2099" });
            agent.CompanyLicenseInfoList.Add(new LicenseInfo() { License = "COMPANYLICENSE001", State = "CA", ExpD = "1/1/2099" });

            agent.Update();

            dataLoan.Save();

            // Set sSpState to AZ.
            dataLoan = FakePageData.Create();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            dataLoan.sSpState = "AZ";
            dataLoan.Save();

            // Verify that License Number still blank.
            dataLoan = FakePageData.Create();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.CreateNew);
            Assert.AreEqual("", agent.LicenseNumOfAgent);
            Assert.AreEqual("", agent.LicenseNumOfCompany);

            dataLoan.sSpState = "CA";
            dataLoan.Save();

            // Verify that License Number is pull from Lending License list.
            dataLoan = FakePageData.Create();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.CreateNew);
            Assert.AreEqual("LICENSE001", agent.LicenseNumOfAgent);
            Assert.AreEqual("COMPANYLICENSE001", agent.LicenseNumOfCompany);

        }

        [Test]
        public void LoanOfficerWithNoInitialLicenseNumberAndWithSingleDefaultCompanyLicenseNumber()
        {
            // Set up empty loan officer with no license number.
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            CAgentFields agent = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.CreateNew);
            agent.AgentName = "LOAN_OFFICER";
            agent.LicenseInfoList.Add(new LicenseInfo() { License = "LICENSE001", State = "CA", ExpD = "1/1/2099" });
            agent.CompanyLicenseInfoList.Add(new LicenseInfo() { License = "COMPANYLICENSE001", State = "", ExpD = "1/1/2099" });

            agent.Update();

            dataLoan.Save();

            // Set sSpState to AZ.
            dataLoan = FakePageData.Create();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            dataLoan.sSpState = "AZ";
            dataLoan.Save();

            // Verify that License Number still blank.
            dataLoan = FakePageData.Create();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.CreateNew);
            Assert.AreEqual("", agent.LicenseNumOfAgent);
            Assert.AreEqual("COMPANYLICENSE001", agent.LicenseNumOfCompany);

            dataLoan.sSpState = "CA";
            dataLoan.Save();

            // Verify that License Number is pull from Lending License list.
            dataLoan = FakePageData.Create();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.CreateNew);
            Assert.AreEqual("LICENSE001", agent.LicenseNumOfAgent);
            Assert.AreEqual("COMPANYLICENSE001", agent.LicenseNumOfCompany);

        }

        [Test]
        public void LoanOfficerWithTwoLendingLicenses()
        {
            // 6/15/2009 dd - Test if loan officer has 2 lendinging license with different expiration date. Verify that
            // we pick the one with farthest expiration date.
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            CAgentFields agent = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.CreateNew);
            agent.AgentName = "LOAN_OFFICER";
            agent.LicenseInfoList.Add(new LicenseInfo() { License = "LICENSE001", State = "CA", ExpD = DateTime.Now.AddDays(10).ToString("MM/dd/yyyy") });
            agent.LicenseInfoList.Add(new LicenseInfo() { License = "LICENSE002", State = "CA", ExpD = DateTime.Now.AddDays(20).ToString("MM/dd/yyyy") });

            agent.CompanyLicenseInfoList.Add(new LicenseInfo() { License = "COMPANYLICENSE001", State = "CA", ExpD = DateTime.Now.AddDays(10).ToString("MM/dd/yyyy") });
            agent.CompanyLicenseInfoList.Add(new LicenseInfo() { License = "COMPANYLICENSE002", State = "CA", ExpD = DateTime.Now.AddDays(20).ToString("MM/dd/yyyy") });

            agent.Update();

            dataLoan.Save();

            dataLoan = FakePageData.Create();

            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            dataLoan.sSpState = "CA";
            dataLoan.Save();

            dataLoan = FakePageData.Create();

            dataLoan.InitLoad();

            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.CreateNew);
            Assert.AreEqual("LICENSE002", agent.LicenseNumOfAgent);
            Assert.AreEqual("COMPANYLICENSE002", agent.LicenseNumOfCompany);
        }

        [Test]
        public void LoanOfficerWithMultipleStatesLendingLicenses()
        {
            // 6/15/2009 dd - Test if loan officer has 2 lendinging license with different expiration date. Verify that
            // we pick the one with farthest expiration date.
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            CAgentFields agent = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.CreateNew);
            agent.AgentName = "LOAN_OFFICER";
            agent.LicenseInfoList.Add(new LicenseInfo() { License = "LICENSE001", State = "CA", ExpD = DateTime.Now.AddDays(10).ToString("MM/dd/yyyy") });
            agent.LicenseInfoList.Add(new LicenseInfo() { License = "LICENSE002", State = "AZ", ExpD = DateTime.Now.AddDays(20).ToString("MM/dd/yyyy") });

            agent.CompanyLicenseInfoList.Add(new LicenseInfo() { License = "COMPANYLICENSE001", State = "CA", ExpD = DateTime.Now.AddDays(10).ToString("MM/dd/yyyy") });
            agent.CompanyLicenseInfoList.Add(new LicenseInfo() { License = "COMPANYLICENSE002", State = "AZ", ExpD = DateTime.Now.AddDays(20).ToString("MM/dd/yyyy") });

            agent.Update();

            dataLoan.Save();

            dataLoan = FakePageData.Create();

            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            dataLoan.sSpState = "CA";
            dataLoan.Save();

            dataLoan = FakePageData.Create();

            dataLoan.InitLoad();
            // 6/15/2009 dd - Verify license are correct for CA.
            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.CreateNew);
            Assert.AreEqual("LICENSE001", agent.LicenseNumOfAgent);
            Assert.AreEqual("COMPANYLICENSE001", agent.LicenseNumOfCompany);


            // Set State to AZ
            dataLoan = FakePageData.Create();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            dataLoan.sSpState = "AZ";
            dataLoan.Save();

            // 6/15/2009 dd - Verify license are correct for AZ
            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.CreateNew);
            Assert.AreEqual("LICENSE002", agent.LicenseNumOfAgent);
            Assert.AreEqual("COMPANYLICENSE002", agent.LicenseNumOfCompany);
        }
    }
}
