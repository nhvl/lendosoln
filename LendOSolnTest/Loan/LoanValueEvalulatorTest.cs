﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using LendOSolnTest.Common;
using DataAccess;
using LendersOffice.Security;
using LendersOffice.Constants;
using LendersOffice.Common;

namespace LendOSolnTest.Loan
{
    [TestFixture]
    public class LoanValueEvalulatorTest
    {
        private Guid m_sLId = Guid.Empty;
        private string[] m_fieldList = {
                                     "aBNm",
                                     "aBLastNm",
                                     "aBFirstNm",
                                     "sPurchPrice",
                                     "sOpenedD",
                                     "sTerm",
                                     "sStatusT"
                                 };
        private AbstractUserPrincipal CurrentPrincipal
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }
        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            LoginTools.LoginAs(TestAccountType.LoanOfficer);

            BrokerUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;

            CLoanFileCreator creator = CLoanFileCreator.GetCreator(principal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);

            m_sLId = creator.CreateBlankLoanFile();

            CPageData dataLoan = CreateDataObject(m_sLId);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            dataLoan.sStatusT = E_sStatusT.Loan_Approved;
            dataLoan.sOpenedD_rep = "12/05/2010";
            dataLoan.sTerm_rep = "180";
            dataLoan.sPurchPrice_rep = "300,000";
            dataLoan.sEquityCalc_rep = "60,000";
            dataLoan.sVerifAssetAmt_rep = "2000.80";
            dataLoan.sFHAIncomeLckd = true;
            CAppData dataApp = dataLoan.GetAppData(0);
            dataApp.aBFirstNm = "aBFirstNm";
            dataApp.aBLastNm = "aBLastNm";

            dataLoan.Save();
        }
        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            if (m_sLId != Guid.Empty)
            {
                Tools.DeclareLoanFileInvalid(BrokerUserPrincipal.CurrentPrincipal, m_sLId, false, false);

            }
        }

        private CPageData CreateDataObject(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(LoanValueEvalulatorTest));
        }

        [Test]
        public void TestInvalidFields()
        {
            try
            {
                string[] fieldList = { "aBNm", "role", "abbbbbb" };
                LoanValueEvaluator evaluator = new LoanValueEvaluator(CurrentPrincipal.ConnectionInfo, fieldList, m_sLId);
                evaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(CurrentPrincipal));
                Assert.Fail("It should throw exception");
            }
            catch (NotFoundException)
            {
            }
        }
        [Test]
        public void TestMissingFields()
        {
            try
            {
                string[] fieldList = { "aBNm", "role" };
                LoanValueEvaluator evaluator = new LoanValueEvaluator(CurrentPrincipal.ConnectionInfo, fieldList, m_sLId);
                evaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(CurrentPrincipal));

                //evaluator.InitLoad();

                var list = evaluator.GetFloatValues("sPurchPrice");
                Assert.AreEqual(0, list.Count, "Should be empty list");
                //Assert.Fail("It should throw exception");
            }
            catch (CBaseException)
            {
            }
        }
        [Test]
        public void TestValidFields()
        {
            string[] fieldList = { "role", "abnm", "sstatust","sPurchPrice", "sVerifAssetAmt", "IsAssigned", "sFHAIncomeLckd", "aBFirstNm", "scope","sEmployeeCallCenterAgentId", "sEmployeeCloserId","sEmployeeLenderAccExecId","sEmployeeLoanRepId",
                "sEmployeeLoanOpenerId","sEmployeeLockDeskId","sEmployeeManagerId","sEmployeeProcessorId","sEmployeeRealEstateAgentId","sEmployeeUnderwriterId",
                "sEmployeeLoanRepId","sEmployeeBrokerProcessorId", "sBrokerId" };

            LoanValueEvaluator evaluator = new LoanValueEvaluator(CurrentPrincipal.ConnectionInfo, fieldList,  m_sLId);
            evaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(CurrentPrincipal));

            #region Scope
            IList<int> scope = evaluator.GetIntValues("scope");
            List<int> expectedScopeList = new List<int>();
            expectedScopeList.Add((int)E_ScopeT.InScope);
            AssertList(expectedScopeList, scope, "Scope");
            #endregion

            #region sStatusT
            IList<int> sStatusT = evaluator.GetIntValues("sstatust");
            List<int> expectedStatusTList = new List<int>();
            expectedStatusTList.Add((int)E_sStatusT.Loan_Approved);

            AssertList(expectedStatusTList, sStatusT, "sStatusT");
            #endregion

            #region Role
            IList<int> roleList = evaluator.GetIntValues("role");
            List<int> expectedRoleList = new List<int>();
            expectedRoleList.Add((int)E_RoleT.LoanOfficer);

            AssertList(expectedRoleList, roleList, "role");
            #endregion

            #region sPurchPrice
            IList<float> sPurchPriceList = evaluator.GetFloatValues("sPurchPrice");
            List<float> expectedPurchPriceList = new List<float>();
            expectedPurchPriceList.Add(30000);

            AssertList(expectedPurchPriceList, sPurchPriceList, "sPurchPrice");
            #endregion

            #region sVerifAssetAmt
            IList<float> sVerifAssetAmtList = evaluator.GetFloatValues("sverifassetamt"); // This property will get from DataLayer directly.
            List<float> expectedsVerifAssetAmtList = new List<float>();
            expectedsVerifAssetAmtList.Add(2000.80F);

            AssertList(expectedsVerifAssetAmtList, sVerifAssetAmtList, "sVerifAssetAmt");
            #endregion

            #region IsAssigned
            bool isAssigned = evaluator.GetBoolValues("isassigned")[0];
            Assert.AreEqual(true, isAssigned, "IsAssigned");
            #endregion

            #region sFHAIncomeLckd
            bool sFHAIncomeLckd = evaluator.GetBoolValues("sFHAIncomeLckd")[0];
            Assert.AreEqual(true, sFHAIncomeLckd, "sFHAIncomeLckd");
            #endregion

            #region aBFirstNm
            IList<string> aBFirstNmList = evaluator.GetStringValues("aBFirstNm");
            List<string> expectedaBFirstNmList = new List<string>();
            expectedaBFirstNmList.Add("aBFirstNm");

            AssertList(expectedaBFirstNmList, aBFirstNmList, "aBFirstNm");
            #endregion

        }

        private void AssertList<T>(IList<T> expectedList, IList<T> actualList, string message)
        {
            Assert.AreEqual(expectedList.Count, actualList.Count, message + ". Length mismatch");
        }
    }
}
