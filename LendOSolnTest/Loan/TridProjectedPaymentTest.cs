﻿// <copyright file="TridProjectedPaymentTest.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Douglas Telfer
//    Date:   6/18/2015
// </summary>
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using LendOSolnTest.Common;
using LendersOffice.Common;
using DataAccess;

namespace LendOSolnTest.Loan
{
    [TestFixture]
    public class TridProjectedPaymentTest
    {
        private AmortTableDataStub amortTableDataStub;
        private AmortTable amortTable;
        private AmortTable bestCase;
        private AmortTable worstCase;
        private List<TridProjectedPayment> tridProjectedPayments;

        private readonly List<string> tridProjectedPaymentsPropsToTest =
            new List<string>
            {
                "StartYear",
                "EndYear",
                "PIMin",
                "PIMax",
                "MI",
                "Escrow",
                "HasInterestOnly",
                "IsFinalBalloonPayment",
                "PaymentMin",
                "PaymentMax"
            };

        /// <summary>
        /// Make sure that we have clean input before starting each test.
        /// </summary>
        [SetUp]
        public void FixtureSetUp()
        {
            // Depending on the type defaults to be sane (i.e. 0) for most things...
            amortTableDataStub = new AmortTableDataStub(new LosConvert());

            // ...but I'd prefer to be explicit about the enums and dates
            amortTableDataStub.sFinMethT = E_sFinMethT.Fixed;
            amortTableDataStub.sLT = E_sLT.Conventional;
            amortTableDataStub.sOptionArmMinPayOptionT = E_sOptionArmMinPayOptionT.TeaserRate;
            amortTableDataStub.sRAdjRoundT = E_sRAdjRoundT.Normal;
            amortTableDataStub.sProMInsT = E_PercentBaseT.LoanAmount;
            amortTableDataStub.sSchedDueD1 = CDateTime.Create(new DateTime(635872032000000000));

            // ...and the data-layer doesn't allow these to be 0
            amortTableDataStub.sTerm = 360;
            amortTableDataStub.sDue = 360;
        }

        /// <summary>
        /// Make sure the AmortTables go away after each test.
        /// </summary>
        [TearDown]
        public void FixtureTearDown()
        {
            amortTableDataStub = null;
            amortTable = null;
            bestCase = null;
            worstCase = null;
        }

        /// <summary>
        /// Convenience function to shorten repetition of this code in the test cases.
        /// </summary>
        private void GenerateAmortTables()
        {
            try
            {
                amortTable = new AmortTable(amortTableDataStub, E_AmortizationScheduleT.Standard, false);
                bestCase = new AmortTable(amortTableDataStub, E_AmortizationScheduleT.BestCase, false);
                worstCase = new AmortTable(amortTableDataStub, E_AmortizationScheduleT.WorstCase, false);
            }
            catch (AmortTableInvalidArgumentException)
            {
            }
        }

        /// <summary>
        /// Function to dump the actual value of the parameters that we are interested in.
        /// Typically used with an Assert.Fail to generate seed expected data for a test case.
        /// </summary>
        /// <param name="actual">The list of projected payments that we are testing.</param>
        /// <param name="props">The names of the properties that we want to test.</param>
        /// <returns>
        /// A literal representation of the Dictionary that would make the test case
        /// pass, which can be pasted directly into the code.
        /// </returns>
        private string GenExpectedResultsFromActual(List<TridProjectedPayment> actual, List<string> props)
        {
            string s = "";

            foreach (TridProjectedPayment t in actual)
            {
                s += TestUtilities.ToLiteral(
                    TestUtilities.ExtractPropertiesAsDict(t, props))
                    + "," + Environment.NewLine;
            }

            return s;
        }

        /// <summary>
        /// Convenience method to test the actual list of projected payments against our list of 
        /// expected value dictionaries.
        /// </summary>
        /// <param name="expected">The list of expected value dictionaries.</param>
        /// <param name="actual">The list of actual projected payment objects.</param>
        private void TestPaymentDict(List<Dictionary<string, object>> expected, List<TridProjectedPayment> actual)
        {
            Assert.IsNotEmpty(actual);
            Assert.GreaterOrEqual(actual.Count,
                expected.Count,
                "Too few payments. Last available payment is: "
                  + ObsoleteSerializationHelper.JavascriptJsonSerialize(tridProjectedPayments[actual.Count - 1]));

            for (int i = 0; i < expected.Count; ++i)
            {
                Dictionary<string, object> unequalPropsActual = TestUtilities.FindUnequalProperties(expected[i], actual[i]);
                if (unequalPropsActual.Count > 0)
                {
                    Dictionary<string, object> unequalPropsExpected
                        = TestUtilities.FilterByKeys(expected[i], unequalPropsActual.Keys);
                    Assert.Fail(
                        "Properties are different between expected and actual: " + Environment.NewLine +
                        "Expected: " + Environment.NewLine +
                        TestUtilities.ToLiteral(unequalPropsExpected) + Environment.NewLine +
                        "Actual: " + Environment.NewLine +
                        TestUtilities.ToLiteral(unequalPropsActual));
                }
            }

            if (actual.Count > expected.Count)
            {
                Assert.LessOrEqual(
                    actual.Count,
                    1,
                    "Too many payments. First extra payment is: "
                      + ObsoleteSerializationHelper.JavascriptJsonSerialize(actual[expected.Count]));
            }
        }

        /// <summary>
        /// Tests to ensure we return null for empty data.
        /// </summary>
        [Test]
        public void EmptyLoanData()
        {
            // Empty loan
            GenerateAmortTables();

            tridProjectedPayments = TridProjectedPayment.GenerateProjectedPayments(
                amortTable, bestCase, worstCase, 0.00M, null);

            Assert.IsNull(tridProjectedPayments);
        }

        /// <summary>
        /// Tests to ensure we return null if the amortization table is in grouped mode.
        /// </summary>
        [Test]
        public void NoGroupedAmortTables()
        {
            amortTableDataStub.sFinMethT = E_sFinMethT.Fixed;
            amortTableDataStub.sLAmtCalc = 200000.00M;
            amortTableDataStub.sFinalLAmt = 200000.00M;
            amortTableDataStub.sHouseVal = 250000.00M;
            amortTableDataStub.sTerm = 360;
            amortTableDataStub.sDue = 360;
            amortTableDataStub.sNoteIR = 6.000M;
            amortTableDataStub.sProMIns = 0.000M;

            amortTable = new AmortTable(amortTableDataStub, E_AmortizationScheduleT.Standard, false);
            bestCase = new AmortTable(amortTableDataStub, E_AmortizationScheduleT.BestCase, true);
            worstCase = new AmortTable(amortTableDataStub, E_AmortizationScheduleT.WorstCase, false);

            tridProjectedPayments = TridProjectedPayment.GenerateProjectedPayments(
                amortTable, bestCase, worstCase, 0.00M, null);

            Assert.IsNull(tridProjectedPayments);
        }

        /// <summary>
        /// Tests to ensure we return null if the amortization table includes prepayment of principal.
        /// </summary>
        [Test]
        public void NoPrincipalPrepayment()
        {
            amortTableDataStub.sFinMethT = E_sFinMethT.Fixed;
            amortTableDataStub.sLAmtCalc = 200000.00M;
            amortTableDataStub.sFinalLAmt = 200000.00M;
            amortTableDataStub.sHouseVal = 250000.00M;
            amortTableDataStub.sTerm = 360;
            amortTableDataStub.sDue = 360;
            amortTableDataStub.sNoteIR = 6.000M;
            amortTableDataStub.sProMIns = 0.000M;

            amortTableDataStub.sPpmtAmt = 1000.00M;
            amortTableDataStub.sPpmtMon = 1;

            amortTable = new AmortTable(amortTableDataStub, E_AmortizationScheduleT.Standard, false);
            bestCase = new AmortTable(amortTableDataStub, E_AmortizationScheduleT.BestCase, false);
            worstCase = new AmortTable(amortTableDataStub, E_AmortizationScheduleT.WorstCase, false);

            tridProjectedPayments = TridProjectedPayment.GenerateProjectedPayments(
                amortTable, bestCase, worstCase, 0.00M, null);

            Assert.IsNull(tridProjectedPayments);
        }

        /// <summary>
        /// Fixed rate loan without MI.
        /// Tests the simplest, most basic scenario--a fully-amortizing fixed-rate loan without MI.
        /// </summary>
        [Test]
        public void FixedNoMI()
        {
            amortTableDataStub.sFinMethT = E_sFinMethT.Fixed;
            amortTableDataStub.sLAmtCalc = 200000.00M;
            amortTableDataStub.sFinalLAmt = 200000.00M;
            amortTableDataStub.sHouseVal = 250000.00M;
            amortTableDataStub.sTerm = 360;
            amortTableDataStub.sDue = 360;
            amortTableDataStub.sNoteIR = 6.000M;
            amortTableDataStub.sProMIns = 0.000M;

            GenerateAmortTables();

            tridProjectedPayments = TridProjectedPayment.GenerateProjectedPayments(
                amortTable, bestCase, worstCase, 0.00M, null);

            List<Dictionary<string, object>> expected =
                new List<Dictionary<string, object>>
                {
                    new Dictionary<string, object>
                    {
                        { "StartYear", 1 }, 
                        { "EndYear", 30 }, 
                        { "PIMin", 1199.10M }, 
                        { "PIMax", 1199.10M }, 
                        { "MI", 0.00M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", false }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 1199.10M }, 
                        { "PaymentMax", 1199.10M }, 
                    }
                };

            ///Assert.Fail(GenExpectedResultsFromActual(tridProjectedPayments, tridProjectedPaymentsPropsToTest));

            TestPaymentDict(expected, tridProjectedPayments);
        }

        /// <summary>
        /// Fixed rate loan with balloon payment, without MI.
        /// Tests the required special handling of the balloon payment.
        /// </summary>
        [Test]
        public void FixedBalloonNoMI()
        {
            amortTableDataStub.sFinMethT = E_sFinMethT.Fixed;
            amortTableDataStub.sLAmtCalc = 200000.00M;
            amortTableDataStub.sFinalLAmt = 200000.00M;
            amortTableDataStub.sHouseVal = 250000.00M;
            amortTableDataStub.sTerm = 360;
            amortTableDataStub.sDue = 180;
            amortTableDataStub.sNoteIR = 6.000M;
            amortTableDataStub.sProMIns = 0.000M;

            GenerateAmortTables();

            tridProjectedPayments = TridProjectedPayment.GenerateProjectedPayments(
                amortTable, bestCase, worstCase, 0.00M, null);

            List<Dictionary<string, object>> expected =
                new List<Dictionary<string, object>>
                {
                    new Dictionary<string, object>
                    {
                        { "StartYear", 1 }, 
                        { "EndYear", 15 }, 
                        { "PIMin", 1199.10M }, 
                        { "PIMax", 1199.10M }, 
                        { "MI", 0.00M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", false }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 1199.10M }, 
                        { "PaymentMax", 1199.10M }
                    }, 

                    new Dictionary<string, object>
                    {
                        { "StartYear", 0 }, 
                        { "EndYear", 0 }, 
                        { "PIMin", 143297.08M }, 
                        { "PIMax", 143297.08M }, 
                        { "MI", 0.00M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", false }, 
                        { "IsFinalBalloonPayment", true }, 
                        { "PaymentMin", 143297.08M }, 
                        { "PaymentMax", 143297.08M }
                    }
                };

            TestPaymentDict(expected, tridProjectedPayments);
        }

        /// <summary>
        /// ARM with more than 4 rate adjustments, but no MI.
        /// Tests the code that limits the number of P and I adjustments to 4.
        /// </summary>
        [Test]
        public void ArmNoMI1()
        {
            amortTableDataStub.sFinMethT = E_sFinMethT.ARM;
            amortTableDataStub.sLAmtCalc = 200000.00M;
            amortTableDataStub.sFinalLAmt = 200000.00M;
            amortTableDataStub.sHouseVal = 250000.00M;
            amortTableDataStub.sTerm = 360;
            amortTableDataStub.sDue = 360;
            amortTableDataStub.sNoteIR = 6.000M;
            amortTableDataStub.sProMIns = 0.000M;

            amortTableDataStub.sRAdj1stCapR = 1.000M;
            amortTableDataStub.sRAdj1stCapMon = 24;
            amortTableDataStub.sRAdjCapR = 1.000M;
            amortTableDataStub.sRAdjCapMon = 12;
            amortTableDataStub.sRAdjLifeCapR = 5.000M;
            amortTableDataStub.sRAdjMarginR = 2.500M;
            amortTableDataStub.sRAdjIndexR = 2.000M;
            amortTableDataStub.sRAdjFloorR = 2.500M;
            amortTableDataStub.sRAdjRoundToR = 0.125M;

            GenerateAmortTables();

            tridProjectedPayments = TridProjectedPayment.GenerateProjectedPayments(
                amortTable, bestCase, worstCase, 0.00M, null);

            List<Dictionary<string, object>> expected =
                new List<Dictionary<string, object>>
                {
                    new Dictionary<string, object>
                    {
                        { "StartYear", 1 }, 
                        { "EndYear", 2 }, 
                        { "PIMin", 1199.10M }, 
                        { "PIMax", 1199.10M }, 
                        { "MI", 0.00M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", false }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 1199.10M }, 
                        { "PaymentMax", 1199.10M },
                    },

                    new Dictionary<string, object>
                    {
                        { "StartYear", 3 }, 
                        { "EndYear", 3 }, 
                        { "PIMin", 1079.12M }, 
                        { "PIMax", 1324.81M }, 
                        { "MI", 0.00M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", false }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 1079.12M }, 
                        { "PaymentMax", 1324.81M },
                    },

                    new Dictionary<string, object>
                    {
                        { "StartYear", 4 }, 
                        { "EndYear", 4 }, 
                        { "PIMin", 968.28M }, 
                        { "PIMax", 1452.82M }, 
                        { "MI", 0.00M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", false }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 968.28M }, 
                        { "PaymentMax", 1452.82M },
                    },

                    new Dictionary<string, object>
                    {
                        { "StartYear", 5 }, 
                        { "EndYear", 30 }, 
                        { "PIMin", 820.04M }, 
                        { "PIMax", 1845.64M }, 
                        { "MI", 0.00M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", false }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 820.04M }, 
                        { "PaymentMax", 1845.64M },
                    }
                };

            TestPaymentDict(expected, tridProjectedPayments);
        }

        /// <summary>
        /// ARM with a single worst case/best case adjustment and no MI.
        /// Tests to ensure that extra payments aren't created when the range is the same
        /// and the payment happens at the beginning of the year.
        /// </summary>
        [Test]
        public void ArmNoMI2()
        {
            amortTableDataStub.sFinMethT = E_sFinMethT.ARM;
            amortTableDataStub.sLAmtCalc = 200000.00M;
            amortTableDataStub.sFinalLAmt = 200000.00M;
            amortTableDataStub.sHouseVal = 250000.00M;
            amortTableDataStub.sTerm = 360;
            amortTableDataStub.sDue = 360;
            amortTableDataStub.sNoteIR = 6.000M;
            amortTableDataStub.sProMIns = 0.000M;

            amortTableDataStub.sRAdj1stCapR = 5.000M;
            amortTableDataStub.sRAdj1stCapMon = 24;
            amortTableDataStub.sRAdjCapR = 1.000M;
            amortTableDataStub.sRAdjCapMon = 12;
            amortTableDataStub.sRAdjLifeCapR = 5.000M;
            amortTableDataStub.sRAdjMarginR = 2.500M;
            amortTableDataStub.sRAdjIndexR = 2.000M;
            amortTableDataStub.sRAdjFloorR = 2.500M;
            amortTableDataStub.sRAdjRoundToR = 0.125M;

            GenerateAmortTables();

            tridProjectedPayments = TridProjectedPayment.GenerateProjectedPayments(
                amortTable, bestCase, worstCase, 0.00M, null);

            List<Dictionary<string, object>> expected =
                new List<Dictionary<string, object>>
                {
                    new Dictionary<string, object>
                    {
                        { "StartYear", 1 }, 
                        { "EndYear", 2 }, 
                        { "PIMin", 1199.10M }, 
                        { "PIMax", 1199.10M }, 
                        { "MI", 0.00M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", false }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 1199.10M }, 
                        { "PaymentMax", 1199.10M },
                    },

                    new Dictionary<string, object>
                    {
                        { "StartYear", 3 }, 
                        { "EndYear", 30 }, 
                        { "PIMin", 807.31M }, 
                        { "PIMax", 1874.28M }, 
                        { "MI", 0.00M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", false }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 807.31M }, 
                        { "PaymentMax", 1874.28M },
                    }
                };

            TestPaymentDict(expected, tridProjectedPayments);
        }

        /// <summary>
        /// ARM with irregular rate changes and no MI.
        /// Tests the generation of projected payments for rate changes
        /// that don't occur in even, annual intervals.
        /// </summary>
        [Test]
        public void ArmIrregularNoMI1()
        {
            amortTableDataStub.sFinMethT = E_sFinMethT.ARM;
            amortTableDataStub.sLAmtCalc = 200000.00M;
            amortTableDataStub.sFinalLAmt = 200000.00M;
            amortTableDataStub.sHouseVal = 250000.00M;
            amortTableDataStub.sTerm = 360;
            amortTableDataStub.sDue = 360;
            amortTableDataStub.sNoteIR = 6.000M;
            amortTableDataStub.sProMIns = 0.000M;

            amortTableDataStub.sRAdj1stCapR = 1.000M;
            amortTableDataStub.sRAdj1stCapMon = 18;
            amortTableDataStub.sRAdjCapR = 1.000M;
            amortTableDataStub.sRAdjCapMon = 11;
            amortTableDataStub.sRAdjLifeCapR = 5.000M;
            amortTableDataStub.sRAdjMarginR = 2.500M;
            amortTableDataStub.sRAdjIndexR = 2.000M;
            amortTableDataStub.sRAdjFloorR = 2.500M;
            amortTableDataStub.sRAdjRoundToR = 0.125M;

            GenerateAmortTables();

            tridProjectedPayments = TridProjectedPayment.GenerateProjectedPayments(
                amortTable, bestCase, worstCase, 0.00M, null);

            List<Dictionary<string, object>> expected =
                new List<Dictionary<string, object>>
                {
                    new Dictionary<string, object>
                    {
                        { "StartYear", 1 }, 
                        { "EndYear", 2 }, 
                        { "PIMin", 1077.72M }, 
                        { "PIMax", 1326.29M }, 
                        { "MI", 0.00M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", false }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 1077.72M }, 
                        { "PaymentMax", 1326.29M }, 
                    },

                    new Dictionary<string, object>
                    {
                        { "StartYear", 3 }, 
                        { "EndYear", 3 }, 
                        { "PIMin", 965.31M }, 
                        { "PIMax", 1456.15M }, 
                        { "MI", 0.00M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", false }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 965.31M }, 
                        { "PaymentMax", 1456.15M }, 
                    },

                    new Dictionary<string, object>
                    {
                        { "StartYear", 4 }, 
                        { "EndYear", 4 }, 
                        { "PIMin", 862.16M }, 
                        { "PIMax", 1588.13M }, 
                        { "MI", 0.00M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", false }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 862.16M }, 
                        { "PaymentMax", 1588.13M }, 
                    },

                    new Dictionary<string, object>
                    {
                        { "StartYear", 5 }, 
                        { "EndYear", 30 }, 
                        { "PIMin", 814.45M }, 
                        { "PIMax", 1856.62M }, 
                        { "MI", 0.00M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", false }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 814.45M }, 
                        { "PaymentMax", 1856.62M }, 
                    },
                };

            TestPaymentDict(expected, tridProjectedPayments);
        }

        /// <summary>
        /// ARM with irregular first payment period, regular subsequent periods.
        /// Tests that projected payments are correctly generated when none
        /// of the changes occur on the anniversary of the first payment.
        /// </summary>
        [Test]
        public void ArmIrregularNoMI2()
        {
            amortTableDataStub.sFinMethT = E_sFinMethT.ARM;
            amortTableDataStub.sLAmtCalc = 200000.00M;
            amortTableDataStub.sFinalLAmt = 200000.00M;
            amortTableDataStub.sHouseVal = 250000.00M;
            amortTableDataStub.sTerm = 360;
            amortTableDataStub.sDue = 360;
            amortTableDataStub.sNoteIR = 6.000M;
            amortTableDataStub.sProMIns = 0.000M;

            amortTableDataStub.sRAdj1stCapR = 2.000M;
            amortTableDataStub.sRAdj1stCapMon = 25;
            amortTableDataStub.sRAdjCapR = 2.000M;
            amortTableDataStub.sRAdjCapMon = 24;
            amortTableDataStub.sRAdjLifeCapR = 6.000M;
            amortTableDataStub.sRAdjMarginR = 2.500M;
            amortTableDataStub.sRAdjIndexR = 2.000M;
            amortTableDataStub.sRAdjFloorR = 2.500M;
            amortTableDataStub.sRAdjRoundToR = 0.125M;

            GenerateAmortTables();

            tridProjectedPayments = TridProjectedPayment.GenerateProjectedPayments(
                amortTable, bestCase, worstCase, 0.00M, null);

            List<Dictionary<string, object>> expected =
                new List<Dictionary<string, object>>
                {
                    new Dictionary<string, object>
                    {
                        { "StartYear", 1 },
                        { "EndYear", 3 },
                        { "PIMin", 965.80M },
                        { "PIMax", 1455.19M },
                        { "MI", 0.00M },
                        { "Escrow", 0.00M },
                        { "HasInterestOnly", false },
                        { "IsFinalBalloonPayment", false },
                        { "PaymentMin", 965.80M },
                        { "PaymentMax", 1455.19M },
                    },

                    new Dictionary<string, object>
                    {
                        { "StartYear", 4 },
                        { "EndYear", 5 },
                        { "PIMin", 816.75M },
                        { "PIMax", 1718.76M },
                        { "MI", 0.00M },
                        { "Escrow", 0.00M },
                        { "HasInterestOnly", false },
                        { "IsFinalBalloonPayment", false },
                        { "PaymentMin", 816.75M },
                        { "PaymentMax", 1718.76M },
                    },

                    new Dictionary<string, object>
                    {
                        { "StartYear", 6 },
                        { "EndYear", 7 },
                        { "PIMin", 816.75M },
                        { "PIMax", 1986.20M },
                        { "MI", 0.00M },
                        { "Escrow", 0.00M },
                        { "HasInterestOnly", false },
                        { "IsFinalBalloonPayment", false },
                        { "PaymentMin", 816.75M },
                        { "PaymentMax", 1986.20M },
                    },

                    new Dictionary<string, object>
                    {
                        { "StartYear", 8 },
                        { "EndYear", 30 },
                        { "PIMin", 816.75M },
                        { "PIMax", 1986.20M },
                        { "MI", 0.00M },
                        { "Escrow", 0.00M },
                        { "HasInterestOnly", false },
                        { "IsFinalBalloonPayment", false },
                        { "PaymentMin", 816.75M },
                        { "PaymentMax", 1986.20M },
                    },
                };

            TestPaymentDict(expected, tridProjectedPayments);
        }

        /// <summary>
        /// ARM that has exactly 3 P and I changes in its range with MI that cancels
        /// Tests that MI termination generates a projected payment when there are
        /// exactly 3 P&I changes. Added to cover the issue from iOPM 231098.
        /// </summary>
        [Test]
        public void ARM3PIChangesMI()
        {
            amortTableDataStub.sFinMethT = E_sFinMethT.ARM;
            amortTableDataStub.sLAmtCalc = 200000.00M;
            amortTableDataStub.sFinalLAmt = 200000.00M;
            amortTableDataStub.sHouseVal = 200000.00M;
            amortTableDataStub.sTerm = 360;
            amortTableDataStub.sDue = 360;
            amortTableDataStub.sNoteIR = 6.000M;
            amortTableDataStub.sProMInsR = 0.600M;
            amortTableDataStub.sProMIns = 112.50M;
            amortTableDataStub.sProMInsCancelLtv = 78.000M;

            amortTableDataStub.sRAdj1stCapR = 2.000M;
            amortTableDataStub.sRAdj1stCapMon = 24;
            amortTableDataStub.sRAdjCapR = 2.000M;
            amortTableDataStub.sRAdjCapMon = 12;
            amortTableDataStub.sRAdjLifeCapR = 4.000M;
            amortTableDataStub.sRAdjMarginR = 2.500M;
            amortTableDataStub.sRAdjIndexR = 2.000M;
            amortTableDataStub.sRAdjFloorR = 2.000M;
            amortTableDataStub.sRAdjRoundToR = 0.125M;

            GenerateAmortTables();

            tridProjectedPayments = TridProjectedPayment.GenerateProjectedPayments(
                amortTable, bestCase, worstCase, 0.00M, null);

            ///Assert.Fail(GenExpectedResultsFromActual(tridProjectedPayments, tridProjectedPaymentsPropsToTest));

            List<Dictionary<string, object>> expected =
                new List<Dictionary<string, object>>
                {
                    new Dictionary<string, object>
                    {
                        { "StartYear", 1 },
                        { "EndYear", 2 },
                        { "PIMin", 1199.10m },
                        { "PIMax", 1199.10m },
                        { "MI", 112.50m },
                        { "Escrow", 0.00m },
                        { "HasInterestOnly", false },
                        { "IsFinalBalloonPayment", false },
                        { "PaymentMin", 1311.60m },
                        { "PaymentMax", 1311.60m },
                    },

                    new Dictionary<string, object>
                    {
                        { "StartYear", 3 },
                        { "EndYear", 3 },
                        { "PIMin", 965.35m },
                        { "PIMax", 1455.71m },
                        { "MI", 112.50m },
                        { "Escrow", 0.00m },
                        { "HasInterestOnly", false },
                        { "IsFinalBalloonPayment", false },
                        { "PaymentMin", 1077.85m },
                        { "PaymentMax", 1568.21m },
                    },

                    new Dictionary<string, object>
                    {
                        { "StartYear", 4 },
                        { "EndYear", 12 },
                        { "PIMin", 763.73m },
                        { "PIMax", 1725.54m },
                        { "MI", 112.50m },
                        { "Escrow", 0.00m },
                        { "HasInterestOnly", false },
                        { "IsFinalBalloonPayment", false },
                        { "PaymentMin", 876.23m },
                        { "PaymentMax", 1838.04m },
                    },

                    new Dictionary<string, object>
                    {
                        { "StartYear", 13 },
                        { "EndYear", 30 },
                        { "PIMin", 763.73m },
                        { "PIMax", 1725.54m },
                        { "MI", 0.00m },
                        { "Escrow", 0.00m },
                        { "HasInterestOnly", false },
                        { "IsFinalBalloonPayment", false },
                        { "PaymentMin", 763.73m },
                        { "PaymentMax", 1725.54m },
                    }
                };

            TestPaymentDict(expected, tridProjectedPayments);
        }

        /// <summary>
        /// ARM that has exactly 3 P and I changes in its range with MI that cancels
        /// No MI complement to the MI version of the same test
        /// </summary>
        [Test]
        public void ARM3PIChangesNoMI()
        {
            amortTableDataStub.sFinMethT = E_sFinMethT.ARM;
            amortTableDataStub.sLAmtCalc = 200000.00M;
            amortTableDataStub.sFinalLAmt = 200000.00M;
            amortTableDataStub.sHouseVal = 200000.00M;
            amortTableDataStub.sTerm = 360;
            amortTableDataStub.sDue = 360;
            amortTableDataStub.sNoteIR = 6.000M;
            amortTableDataStub.sProMIns = 0.00M;

            amortTableDataStub.sRAdj1stCapR = 2.000M;
            amortTableDataStub.sRAdj1stCapMon = 24;
            amortTableDataStub.sRAdjCapR = 2.000M;
            amortTableDataStub.sRAdjCapMon = 12;
            amortTableDataStub.sRAdjLifeCapR = 4.000M;
            amortTableDataStub.sRAdjMarginR = 2.500M;
            amortTableDataStub.sRAdjIndexR = 2.000M;
            amortTableDataStub.sRAdjFloorR = 2.000M;
            amortTableDataStub.sRAdjRoundToR = 0.125M;

            GenerateAmortTables();

            tridProjectedPayments = TridProjectedPayment.GenerateProjectedPayments(
                amortTable, bestCase, worstCase, 0.00M, null);

            ///Assert.Fail(GenExpectedResultsFromActual(tridProjectedPayments, tridProjectedPaymentsPropsToTest));

            List<Dictionary<string, object>> expected =
                new List<Dictionary<string, object>>
                {
                    new Dictionary<string, object>
                    {
                        { "StartYear", 1 },
                        { "EndYear", 2 },
                        { "PIMin", 1199.10m },
                        { "PIMax", 1199.10m },
                        { "MI", 0.00m },
                        { "Escrow", 0.00m },
                        { "HasInterestOnly", false },
                        { "IsFinalBalloonPayment", false },
                        { "PaymentMin", 1199.10m },
                        { "PaymentMax", 1199.10m },
                    },

                    new Dictionary<string, object>
                    {
                        { "StartYear", 3 },
                        { "EndYear", 3 },
                        { "PIMin", 965.35m },
                        { "PIMax", 1455.71m },
                        { "MI", 0.00m },
                        { "Escrow", 0.00m },
                        { "HasInterestOnly", false },
                        { "IsFinalBalloonPayment", false },
                        { "PaymentMin", 965.35m },
                        { "PaymentMax", 1455.71m },
                    },

                    new Dictionary<string, object>
                    {
                        { "StartYear", 4 },
                        { "EndYear", 30 },
                        { "PIMin", 763.73m },
                        { "PIMax", 1725.54m },
                        { "MI", 0.00m },
                        { "Escrow", 0.00m },
                        { "HasInterestOnly", false },
                        { "IsFinalBalloonPayment", false },
                        { "PaymentMin", 763.73m },
                        { "PaymentMax", 1725.54m },
                    }
                };

            TestPaymentDict(expected, tridProjectedPayments);
        }

        /// <summary>
        /// Fixed loan with MI that cancels.
        /// Tests that MI termination generates a projected payment for the
        /// year after the MI terminates.
        /// </summary>
        [Test]
        public void FixedMI()
        {
            amortTableDataStub.sFinMethT = E_sFinMethT.Fixed;
            amortTableDataStub.sLAmtCalc = 225000.00M;
            amortTableDataStub.sFinalLAmt = 225000.00M;
            amortTableDataStub.sHouseVal = 250000.00M;
            amortTableDataStub.sTerm = 360;
            amortTableDataStub.sDue = 360;
            amortTableDataStub.sNoteIR = 6.000M;
            amortTableDataStub.sProMInsR = 0.600M;
            amortTableDataStub.sProMIns = 112.50M;
            amortTableDataStub.sProMInsCancelLtv = 78.000M;

            GenerateAmortTables();

            tridProjectedPayments = TridProjectedPayment.GenerateProjectedPayments(
                amortTable, bestCase, worstCase, 0.00M, null);

            List<Dictionary<string, object>> expected =
                new List<Dictionary<string, object>>
                {
                    new Dictionary<string, object>
                    {
                        { "StartYear", 1 }, 
                        { "EndYear", 9 }, 
                        { "PIMin", 1348.99M }, 
                        { "PIMax", 1348.99M }, 
                        { "MI", 112.50M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", false }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 1461.49M }, 
                        { "PaymentMax", 1461.49M }, 
                    },

                    new Dictionary<string, object>
                    {
                        { "StartYear", 10 }, 
                        { "EndYear", 30 }, 
                        { "PIMin", 1348.99M }, 
                        { "PIMax", 1348.99M }, 
                        { "MI", 0.00M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", false }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 1348.99M }, 
                        { "PaymentMax", 1348.99M }, 
                    },
                };

            TestPaymentDict(expected, tridProjectedPayments);
        }

        /// <summary>
        /// ARM with more than 4 changes and MI.
        /// Tests to ensure that the MI termination is dropped when there
        /// are 4 or more P and I adjustments.
        /// </summary>
        [Test]
        public void ArmMI1()
        {
            amortTableDataStub.sFinMethT = E_sFinMethT.ARM;
            amortTableDataStub.sLAmtCalc = 200000.00M;
            amortTableDataStub.sFinalLAmt = 200000.00M;
            amortTableDataStub.sHouseVal = 222222.22M;
            amortTableDataStub.sTerm = 360;
            amortTableDataStub.sDue = 360;
            amortTableDataStub.sNoteIR = 6.000M;
            amortTableDataStub.sProMInsR = 0.600M;
            amortTableDataStub.sProMIns = 100.00M;
            amortTableDataStub.sProMInsCancelLtv = 78.000M;

            amortTableDataStub.sRAdj1stCapR = 1.000M;
            amortTableDataStub.sRAdj1stCapMon = 24;
            amortTableDataStub.sRAdjCapR = 1.000M;
            amortTableDataStub.sRAdjCapMon = 12;
            amortTableDataStub.sRAdjLifeCapR = 5.000M;
            amortTableDataStub.sRAdjMarginR = 2.500M;
            amortTableDataStub.sRAdjIndexR = 2.000M;
            amortTableDataStub.sRAdjFloorR = 2.500M;
            amortTableDataStub.sRAdjRoundToR = 0.125M;

            GenerateAmortTables();

            tridProjectedPayments = TridProjectedPayment.GenerateProjectedPayments(
                amortTable, bestCase, worstCase, 0.00M, null);

            List<Dictionary<string, object>> expected =
                new List<Dictionary<string, object>>
                {
                    new Dictionary<string, object>
                    {
                        { "StartYear", 1 }, 
                        { "EndYear", 2 }, 
                        { "PIMin", 1199.10M }, 
                        { "PIMax", 1199.10M }, 
                        { "MI", 100.00M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", false }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 1299.10M }, 
                        { "PaymentMax", 1299.10M }, 
                    },

                    new Dictionary<string, object>
                    {
                        { "StartYear", 3 }, 
                        { "EndYear", 3 }, 
                        { "PIMin", 1079.12M }, 
                        { "PIMax", 1324.81M }, 
                        { "MI", 100.00M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", false }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 1179.12M }, 
                        { "PaymentMax", 1424.81M }, 
                    },

                    new Dictionary<string, object>
                    {
                        { "StartYear", 4 }, 
                        { "EndYear", 4 }, 
                        { "PIMin", 968.28M }, 
                        { "PIMax", 1452.82M }, 
                        { "MI", 100.00M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", false }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 1068.28M }, 
                        { "PaymentMax", 1552.82M }, 
                    },

                    new Dictionary<string, object>
                                        {
                        { "StartYear", 5 }, 
                        { "EndYear", 30 }, 
                        { "PIMin", 820.04M }, 
                        { "PIMax", 1845.64M }, 
                        { "MI", 100.00M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", false }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 920.04M }, 
                        { "PaymentMax", 1945.64M }, 
                    },
                };

            TestPaymentDict(expected, tridProjectedPayments);
        }

        /// <summary>
        /// ARM with 2 changes and MI.
        /// Tests to ensure that MI termination is included if the ARM
        /// has fewer than 4 changes.
        /// </summary>
        [Test]
        public void ArmMI2()
        {
            amortTableDataStub.sFinMethT = E_sFinMethT.ARM;
            amortTableDataStub.sLAmtCalc = 200000.00M;
            amortTableDataStub.sFinalLAmt = 200000.00M;
            amortTableDataStub.sHouseVal = 222222.22M;
            amortTableDataStub.sTerm = 360;
            amortTableDataStub.sDue = 360;
            amortTableDataStub.sNoteIR = 6.000M;
            amortTableDataStub.sProMInsR = 0.600M;
            amortTableDataStub.sProMIns = 100.00M;
            amortTableDataStub.sProMInsCancelLtv = 78.000M;

            amortTableDataStub.sRAdj1stCapR = 5.000M;
            amortTableDataStub.sRAdj1stCapMon = 24;
            amortTableDataStub.sRAdjCapR = 1.000M;
            amortTableDataStub.sRAdjCapMon = 12;
            amortTableDataStub.sRAdjLifeCapR = 5.000M;
            amortTableDataStub.sRAdjMarginR = 2.500M;
            amortTableDataStub.sRAdjIndexR = 2.000M;
            amortTableDataStub.sRAdjFloorR = 2.500M;
            amortTableDataStub.sRAdjRoundToR = 0.125M;

            GenerateAmortTables();

            tridProjectedPayments = TridProjectedPayment.GenerateProjectedPayments(
                amortTable, bestCase, worstCase, 0.00M, null);

            List<Dictionary<string, object>> expected =
                new List<Dictionary<string, object>>
                {
                    new Dictionary<string, object>
                    {
                        { "StartYear", 1 }, 
                        { "EndYear", 2 }, 
                        { "PIMin", 1199.10M }, 
                        { "PIMax", 1199.10M }, 
                        { "MI", 100.00M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", false }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 1299.10M }, 
                        { "PaymentMax", 1299.10M }, 
                    },

                    new Dictionary<string, object>
                    {
                        { "StartYear", 3 }, 
                        { "EndYear", 8 }, 
                        { "PIMin", 807.31M }, 
                        { "PIMax", 1874.28M }, 
                        { "MI", 100.00M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", false }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 907.31M }, 
                        { "PaymentMax", 1974.28M }, 
                    },

                    new Dictionary<string, object>
                    {
                        { "StartYear", 9 }, 
                        { "EndYear", 30 }, 
                        { "PIMin", 807.31M }, 
                        { "PIMax", 1874.28M }, 
                        { "MI", 0.00M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", false }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 807.31M }, 
                        { "PaymentMax", 1874.28M }, 
                    },
                };

            TestPaymentDict(expected, tridProjectedPayments);
        }

        /// <summary>
        /// ARM balloon with more than 4 changes and without MI.
        /// Tests to ensure that an ARM with at least 4 P and I changes will
        /// drop one of those changes in order to include the balloon payment.
        /// </summary>
        [Test]
        public void ArmBalloonNoMI()
        {
            amortTableDataStub.sFinMethT = E_sFinMethT.ARM;
            amortTableDataStub.sLAmtCalc = 200000.00M;
            amortTableDataStub.sFinalLAmt = 200000.00M;
            amortTableDataStub.sHouseVal = 250000.00M;
            amortTableDataStub.sTerm = 360;
            amortTableDataStub.sDue = 180;
            amortTableDataStub.sNoteIR = 6.000M;
            amortTableDataStub.sProMIns = 0.000M;

            amortTableDataStub.sRAdj1stCapR = 1.000M;
            amortTableDataStub.sRAdj1stCapMon = 24;
            amortTableDataStub.sRAdjCapR = 1.000M;
            amortTableDataStub.sRAdjCapMon = 12;
            amortTableDataStub.sRAdjLifeCapR = 5.000M;
            amortTableDataStub.sRAdjMarginR = 2.500M;
            amortTableDataStub.sRAdjIndexR = 2.000M;
            amortTableDataStub.sRAdjFloorR = 2.500M;
            amortTableDataStub.sRAdjRoundToR = 0.125M;

            GenerateAmortTables();

            tridProjectedPayments = TridProjectedPayment.GenerateProjectedPayments(
                amortTable, bestCase, worstCase, 0.00M, null);

            List<Dictionary<string, object>> expected =
                new List<Dictionary<string, object>>
                {
                    new Dictionary<string, object>
                    {
                        { "StartYear", 1 }, 
                        { "EndYear", 2 }, 
                        { "PIMin", 1199.10M }, 
                        { "PIMax", 1199.10M }, 
                        { "MI", 0.00M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", false }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 1199.10M }, 
                        { "PaymentMax", 1199.10M }, 
                    },

                    new Dictionary<string, object>
                    {
                        { "StartYear", 3 }, 
                        { "EndYear", 3 }, 
                        { "PIMin", 1079.12M }, 
                        { "PIMax", 1324.81M }, 
                        { "MI", 0.00M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", false }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 1079.12M }, 
                        { "PaymentMax", 1324.81M }, 
                    },

                    new Dictionary<string, object>
                    {
                        { "StartYear", 4 }, 
                        { "EndYear", 15 }, 
                        { "PIMin", 820.04M }, 
                        { "PIMax", 1845.64M }, 
                        { "MI", 0.00M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", false }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 820.04M }, 
                        { "PaymentMax", 1845.64M }, 
                    },

                    new Dictionary<string, object>
                    {
                        { "StartYear", 0 }, 
                        { "EndYear", 0 }, 
                        { "PIMin", 134739.12M }, 
                        { "PIMax", 134739.12M }, 
                        { "MI", 0.00M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", false }, 
                        { "IsFinalBalloonPayment", true }, 
                        { "PaymentMin", 134739.12M }, 
                        { "PaymentMax", 134739.12M }, 
                    },
                };

            TestPaymentDict(expected, tridProjectedPayments);
        }

        /// <summary>
        /// ARM balloon with 3 or more changes and MI.
        /// Tests to ensure that an ARM will drop P and I changes to fit the
        /// balloon payment, but will not drop P and I for the MI.
        /// </summary>
        [Test]
        public void ArmBalloonMI()
        {
            amortTableDataStub.sFinMethT = E_sFinMethT.ARM;
            amortTableDataStub.sLAmtCalc = 200000.00M;
            amortTableDataStub.sFinalLAmt = 200000.00M;
            amortTableDataStub.sHouseVal = 222222.22M;
            amortTableDataStub.sTerm = 360;
            amortTableDataStub.sDue = 180;
            amortTableDataStub.sNoteIR = 6.000M;
            amortTableDataStub.sProMInsR = 0.600M;
            amortTableDataStub.sProMIns = 100.00M;
            amortTableDataStub.sProMInsCancelLtv = 78.000M;

            amortTableDataStub.sRAdj1stCapR = 1.000M;
            amortTableDataStub.sRAdj1stCapMon = 24;
            amortTableDataStub.sRAdjCapR = 1.000M;
            amortTableDataStub.sRAdjCapMon = 12;
            amortTableDataStub.sRAdjLifeCapR = 3.000M;
            amortTableDataStub.sRAdjMarginR = 2.500M;
            amortTableDataStub.sRAdjIndexR = 2.000M;
            amortTableDataStub.sRAdjFloorR = 2.500M;
            amortTableDataStub.sRAdjRoundToR = 0.125M;

            GenerateAmortTables();

            tridProjectedPayments = TridProjectedPayment.GenerateProjectedPayments(
                amortTable, bestCase, worstCase, 0.00M, null);

            List<Dictionary<string, object>> expected =
                new List<Dictionary<string, object>>
                {
                    new Dictionary<string, object>
                    {
                        { "StartYear", 1 }, 
                        { "EndYear", 2 }, 
                        { "PIMin", 1199.10M }, 
                        { "PIMax", 1199.10M }, 
                        { "MI", 100.00M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", false }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 1299.10M }, 
                        { "PaymentMax", 1299.10M }, 
                    },

                    new Dictionary<string, object>
                    {
                        { "StartYear", 3 }, 
                        { "EndYear", 3 }, 
                        { "PIMin", 1079.12M }, 
                        { "PIMax", 1324.81M }, 
                        { "MI", 100.00M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", false }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 1179.12M }, 
                        { "PaymentMax", 1424.81M }, 
                    },

                    new Dictionary<string, object>
                    {
                        { "StartYear", 4 }, 
                        { "EndYear", 15 }, 
                        { "PIMin", 820.04M }, 
                        { "PIMax", 1582.61M }, 
                        { "MI", 100.00M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", false }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 920.04M }, 
                        { "PaymentMax", 1682.61M }, 
                    },

                    new Dictionary<string, object>
                    {
                        { "StartYear", 0 }, 
                        { "EndYear", 0 }, 
                        { "PIMin", 134739.12M }, 
                        { "PIMax", 134739.12M }, 
                        { "MI", 0.00M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", false }, 
                        { "IsFinalBalloonPayment", true }, 
                        { "PaymentMin", 134739.12M }, 
                        { "PaymentMax", 134739.12M }, 
                    },
                };

            TestPaymentDict(expected, tridProjectedPayments);
        }

        /// <summary>
        /// IO ARM without MI and more than 4 payments, I/O expires first.
        /// Tests to ensure that the I/O period expiring is properly accounted
        /// for, and that the projected payments that include I/O payments are
        /// correctly marked as such.
        /// </summary>
        [Test]
        public void IOArmNoMI1()
        {
            amortTableDataStub.sFinMethT = E_sFinMethT.ARM;
            amortTableDataStub.sLAmtCalc = 200000.00M;
            amortTableDataStub.sFinalLAmt = 200000.00M;
            amortTableDataStub.sHouseVal = 250000.00M;
            amortTableDataStub.sTerm = 372;
            amortTableDataStub.sDue = 372;
            amortTableDataStub.sNoteIR = 6.000M;
            amortTableDataStub.sProMIns = 0.000M;

            amortTableDataStub.sIOnlyMon = 12;

            amortTableDataStub.sRAdj1stCapR = 1.000M;
            amortTableDataStub.sRAdj1stCapMon = 36;
            amortTableDataStub.sRAdjCapR = 1.000M;
            amortTableDataStub.sRAdjCapMon = 12;
            amortTableDataStub.sRAdjLifeCapR = 5.000M;
            amortTableDataStub.sRAdjMarginR = 2.500M;
            amortTableDataStub.sRAdjIndexR = 2.000M;
            amortTableDataStub.sRAdjFloorR = 2.500M;
            amortTableDataStub.sRAdjRoundToR = 0.125M;

            GenerateAmortTables();

            tridProjectedPayments = TridProjectedPayment.GenerateProjectedPayments(
                amortTable, bestCase, worstCase, 0.00M, null);

            List<Dictionary<string, object>> expected =
                new List<Dictionary<string, object>>
                {
                    new Dictionary<string, object>
                    {
                        { "StartYear", 1 }, 
                        { "EndYear", 1 }, 
                        { "PIMin", 1000.00M }, 
                        { "PIMax", 1000.00M }, 
                        { "MI", 0.00M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", true }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 1000.00M }, 
                        { "PaymentMax", 1000.00M }, 
                    },

                    new Dictionary<string, object>
                    {
                        { "StartYear", 2 }, 
                        { "EndYear", 3 }, 
                        { "PIMin", 1199.10M }, 
                        { "PIMax", 1199.10M }, 
                        { "MI", 0.00M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", false }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 1199.10M }, 
                        { "PaymentMax", 1199.10M }, 
                    },

                    new Dictionary<string, object>
                    {
                        { "StartYear", 4 }, 
                        { "EndYear", 4 }, 
                        { "PIMin", 1079.12M }, 
                        { "PIMax", 1324.81M }, 
                        { "MI", 0.00M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", false }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 1079.12M }, 
                        { "PaymentMax", 1324.81M }, 
                    },

                    new Dictionary<string, object>
                    {
                        { "StartYear", 5 }, 
                        { "EndYear", 31 }, 
                        { "PIMin", 820.04M }, 
                        { "PIMax", 1845.64M }, 
                        { "MI", 0.00M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", false }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 820.04M }, 
                        { "PaymentMax", 1845.64M }, 
                    },
                };

            TestPaymentDict(expected, tridProjectedPayments);
        }

        /// <summary>
        /// IO ARM without MI and more than 4 payments, I/O expires last
        /// Tests to ensure that the I/O period expiring is properly ignored
        /// when it occurs after more than 3 other P and I changes, and that the
        /// projected payments that include I/O payments are correctly marked
        /// as such.
        /// </summary>
        [Test]
        public void IOArmNoMI2()
        {
            amortTableDataStub.sFinMethT = E_sFinMethT.ARM;
            amortTableDataStub.sLAmtCalc = 200000.00M;
            amortTableDataStub.sFinalLAmt = 200000.00M;
            amortTableDataStub.sHouseVal = 250000.00M;
            amortTableDataStub.sTerm = 360;
            amortTableDataStub.sDue = 360;
            amortTableDataStub.sNoteIR = 6.000M;
            amortTableDataStub.sProMIns = 0.000M;

            amortTableDataStub.sIOnlyMon = 60;

            amortTableDataStub.sRAdj1stCapR = 1.000M;
            amortTableDataStub.sRAdj1stCapMon = 12;
            amortTableDataStub.sRAdjCapR = 1.000M;
            amortTableDataStub.sRAdjCapMon = 12;
            amortTableDataStub.sRAdjLifeCapR = 5.000M;
            amortTableDataStub.sRAdjMarginR = 2.500M;
            amortTableDataStub.sRAdjIndexR = 2.000M;
            amortTableDataStub.sRAdjFloorR = 2.500M;
            amortTableDataStub.sRAdjRoundToR = 0.125M;

            GenerateAmortTables();

            tridProjectedPayments = TridProjectedPayment.GenerateProjectedPayments(
                amortTable, bestCase, worstCase, 0.00M, null);

            List<Dictionary<string, object>> expected =
                new List<Dictionary<string, object>>
                {
                    new Dictionary<string, object>
                    {
                        { "StartYear", 1 }, 
                        { "EndYear", 1 }, 
                        { "PIMin", 1000.00M }, 
                        { "PIMax", 1000.00M }, 
                        { "MI", 0.00M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", true }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 1000.00M }, 
                        { "PaymentMax", 1000.00M }, 
                    },

                    new Dictionary<string, object>
                    {
                        { "StartYear", 2 }, 
                        { "EndYear", 2 }, 
                        { "PIMin", 833.33M }, 
                        { "PIMax", 1166.67M }, 
                        { "MI", 0.00M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", true }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 833.33M }, 
                        { "PaymentMax", 1166.67M }, 
                    },

                    new Dictionary<string, object>
                    {
                        { "StartYear", 3 }, 
                        { "EndYear", 3 }, 
                        { "PIMin", 666.67M }, 
                        { "PIMax", 1333.33M }, 
                        { "MI", 0.00M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", true }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 666.67M }, 
                        { "PaymentMax", 1333.33M }, 
                    },

                    new Dictionary<string, object>
                    {
                        { "StartYear", 4 }, 
                        { "EndYear", 30 }, 
                        { "PIMin", 416.67M }, 
                        { "PIMax", 1960.23M }, 
                        { "MI", 0.00M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", true }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 416.67M }, 
                        { "PaymentMax", 1960.23M }, 
                    },
                };

            TestPaymentDict(expected, tridProjectedPayments);
        }

        /// <summary>
        /// IO ARM without MI, and IO that expires in the middle of a year.
        /// Tests to ensure that the correct projected payments are
        /// generated when the min is the I/O payment, but the max is not.
        /// </summary>
        [Test]
        public void IOArmNoMI3()
        {
            amortTableDataStub.sFinMethT = E_sFinMethT.ARM;
            amortTableDataStub.sLAmtCalc = 200000.00M;
            amortTableDataStub.sFinalLAmt = 200000.00M;
            amortTableDataStub.sHouseVal = 250000.00M;
            amortTableDataStub.sTerm = 360;
            amortTableDataStub.sDue = 360;
            amortTableDataStub.sNoteIR = 6.000M;
            amortTableDataStub.sProMIns = 0.000M;

            amortTableDataStub.sIOnlyMon = 30;

            amortTableDataStub.sRAdj1stCapR = 1.000M;
            amortTableDataStub.sRAdj1stCapMon = 24;
            amortTableDataStub.sRAdjCapR = 1.000M;
            amortTableDataStub.sRAdjCapMon = 12;
            amortTableDataStub.sRAdjLifeCapR = 5.000M;
            amortTableDataStub.sRAdjMarginR = 2.500M;
            amortTableDataStub.sRAdjIndexR = 2.000M;
            amortTableDataStub.sRAdjFloorR = 2.500M;
            amortTableDataStub.sRAdjRoundToR = 0.125M;

            GenerateAmortTables();

            tridProjectedPayments = TridProjectedPayment.GenerateProjectedPayments(
                amortTable, bestCase, worstCase, 0.00M, null);

            List<Dictionary<string, object>> expected =
                new List<Dictionary<string, object>>
                {
                    new Dictionary<string, object>
                    {
                        { "StartYear", 1 }, 
                        { "EndYear", 2 }, 
                        { "PIMin", 1000.00M }, 
                        { "PIMax", 1000.00M }, 
                        { "MI", 0.00M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", true }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 1000.00M }, 
                        { "PaymentMax", 1000.00M }, 
                    },

                    new Dictionary<string, object>
                    {
                        { "StartYear", 3 }, 
                        { "EndYear", 3 }, 
                        { "PIMin", 833.33M }, 
                        { "PIMax", 1367.23M }, 
                        { "MI", 0.00M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", true }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 833.33M }, 
                        { "PaymentMax", 1367.23M }, 
                    },

                    new Dictionary<string, object>
                    {
                        { "StartYear", 4 }, 
                        { "EndYear", 4 }, 
                        { "PIMin", 1001.75M }, 
                        { "PIMax", 1499.35M }, 
                        { "MI", 0.00M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", false }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 1001.75M }, 
                        { "PaymentMax", 1499.35M }, 
                    },

                    new Dictionary<string, object>
                    {
                        { "StartYear", 5 }, 
                        { "EndYear", 30 }, 
                        { "PIMin", 848.39M }, 
                        { "PIMax", 1904.74M }, 
                        { "MI", 0.00M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", false }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 848.39M }, 
                        { "PaymentMax", 1904.74M }, 
                    },
                };

            TestPaymentDict(expected, tridProjectedPayments);
        }

        /// <summary>
        /// Fixed IO loan with entire principal balance due at the end.
        /// Tests to to ensure that the final balloon payment is generated
        /// even when sTerm == sDue.
        /// </summary>
        [Test]
        public void FixedIoBalloonNoMI()
        {
            amortTableDataStub.sFinMethT = E_sFinMethT.Fixed;
            amortTableDataStub.sLAmtCalc = 200000.00M;
            amortTableDataStub.sFinalLAmt = 200000.00M;
            amortTableDataStub.sHouseVal = 250000.00M;
            amortTableDataStub.sTerm = 60;
            amortTableDataStub.sDue = 60;
            amortTableDataStub.sNoteIR = 6.000M;
            amortTableDataStub.sProMIns = 0.000M;

            amortTableDataStub.sIOnlyMon = 60;

            GenerateAmortTables();

            tridProjectedPayments = TridProjectedPayment.GenerateProjectedPayments(
                amortTable, bestCase, worstCase, 0.00M, null);

            List<Dictionary<string, object>> expected =
                new List<Dictionary<string, object>>
                {
                    new Dictionary<string, object>
                    {
                        { "StartYear", 1 }, 
                        { "EndYear", 5 }, 
                        { "PIMin", 1000.00M }, 
                        { "PIMax", 1000.00M }, 
                        { "MI", 0.00M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", false }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 1000.00M }, 
                        { "PaymentMax", 1000.00M }, 
                    },

                    new Dictionary<string, object>
                    {
                        { "StartYear", 0 }, 
                        { "EndYear", 0 }, 
                        { "PIMin", 201000.00M }, 
                        { "PIMax", 201000.00M }, 
                        { "MI", 0.00M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", false }, 
                        { "IsFinalBalloonPayment", true }, 
                        { "PaymentMin", 201000.00M }, 
                        { "PaymentMax", 201000.00M }, 
                    },
                };

            TestPaymentDict(expected, tridProjectedPayments);
        }

        /// <summary>
        /// 45. Brothers shall fight  | and fell each other,
        ///     And sisters' sons     | shall kinship stain;
        ///     Hard is it on earth,  | with mighty whoredom;
        ///     Axe-time, sword-time, | shields are sundered,
        ///     Wind-time, wolf-time, | ere the world falls;
        ///     Nor ever shall men    | each other spare.
        /// http://www.sacred-texts.com/neu/poe/poe03.htm
        /// </summary>
        [Test]
        public void Ragnarok1()
        {
            amortTableDataStub.sFinMethT = E_sFinMethT.ARM;
            amortTableDataStub.sLAmtCalc = 200000.00M;
            amortTableDataStub.sFinalLAmt = 200000.00M;
            amortTableDataStub.sHouseVal = 250000.00M;
            amortTableDataStub.sTerm = 66;
            amortTableDataStub.sDue = 66;
            amortTableDataStub.sNoteIR = 6.000M;
            amortTableDataStub.sProMIns = 0.000M;

            amortTableDataStub.sIOnlyMon = 61;

            amortTableDataStub.sRAdj1stCapR = 1.000M;
            amortTableDataStub.sRAdj1stCapMon = 50;
            amortTableDataStub.sRAdjCapR = 1.000M;
            amortTableDataStub.sRAdjCapMon = 12;
            amortTableDataStub.sRAdjLifeCapR = 5.000M;
            amortTableDataStub.sRAdjMarginR = 2.500M;
            amortTableDataStub.sRAdjIndexR = 2.000M;
            amortTableDataStub.sRAdjFloorR = 2.500M;
            amortTableDataStub.sRAdjRoundToR = 0.125M;

            GenerateAmortTables();

            tridProjectedPayments = TridProjectedPayment.GenerateProjectedPayments(
                amortTable, bestCase, worstCase, 0.00M, null);

            List<Dictionary<string, object>> expected =
                new List<Dictionary<string, object>>
                {
                    new Dictionary<string, object>
                    {
                        { "StartYear", 1 }, 
                        { "EndYear", 5 }, 
                        { "PIMin", 833.33M }, 
                        { "PIMax", 1166.67M }, 
                        { "MI", 0.00M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", true }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 833.33M }, 
                        { "PaymentMax", 1166.67M }, 
                    },

                    new Dictionary<string, object>
                    {
                        { "StartYear", 6 }, 
                        { "EndYear", 6 }, 
                        { "PIMin", 833.33M }, 
                        { "PIMax", 40786.81M }, 
                        { "MI", 0.00M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", true }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 833.33M }, 
                        { "PaymentMax", 40786.81M }, 
                    },
                };

            TestPaymentDict(expected, tridProjectedPayments);
        }

        /// <summary>
        /// Tests that we can reproduce the scenario for this LE:
        /// http://files.consumerfinance.gov/f/201403_cfpb_loan-estimate_interest-only-adjustable-rate-loan-sample-H24C.pdf
        /// </summary>
        [Test]
        public void CFPBSampleLeIoArm()
        {
            amortTableDataStub.sFinMethT = E_sFinMethT.ARM;
            amortTableDataStub.sLAmtCalc = 211000.00M;
            amortTableDataStub.sFinalLAmt = 211000.00M;
            amortTableDataStub.sHouseVal = 240000.00M;
            amortTableDataStub.sTerm = 360;
            amortTableDataStub.sDue = 360;
            amortTableDataStub.sNoteIR = 4.000M;
            amortTableDataStub.sProMInsR = 0.620M;
            amortTableDataStub.sProMIns = 109.02M;
            amortTableDataStub.sProMInsCancelLtv = 78.000M;

            amortTableDataStub.sIOnlyMon = 60;

            amortTableDataStub.sRAdj1stCapR = 2.000M;
            amortTableDataStub.sRAdj1stCapMon = 60;
            amortTableDataStub.sRAdjCapR = 2.000M;
            amortTableDataStub.sRAdjCapMon = 36;
            amortTableDataStub.sRAdjLifeCapR = 8.000M;
            amortTableDataStub.sRAdjMarginR = 2.500M;
            amortTableDataStub.sRAdjIndexR = 2.000M;
            amortTableDataStub.sRAdjFloorR = 3.250M;
            amortTableDataStub.sRAdjRoundToR = 0.125M;

            GenerateAmortTables();

            tridProjectedPayments = TridProjectedPayment.GenerateProjectedPayments(
                amortTable, bestCase, worstCase, 0.00M, null);

            List<Dictionary<string, object>> expected =
                new List<Dictionary<string, object>>
                {
                    new Dictionary<string, object>
                    {
                        { "StartYear", 1 }, 
                        { "EndYear", 5 }, 
                        { "PIMin", 703.33M }, 
                        { "PIMax", 703.33M }, 
                        { "MI", 109.02M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", true }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 812.35M }, 
                        { "PaymentMax", 812.35M }, 
                    },

                    new Dictionary<string, object>
                    {
                        { "StartYear", 6 }, 
                        { "EndYear", 8 }, 
                        { "PIMin", 1028.24M }, 
                        { "PIMax", 1359.48M }, 
                        { "MI", 109.02M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", false }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 1137.26M }, 
                        { "PaymentMax", 1468.50M }, 
                    },

                    new Dictionary<string, object>
                    {
                        { "StartYear", 9 }, 
                        { "EndYear", 11 }, 
                        { "PIMin", 1028.24M }, 
                        { "PIMax", 1604.48M }, 
                        { "MI", 109.02M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", false }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 1137.26M }, 
                        { "PaymentMax", 1713.50M }, 
                    },

                    new Dictionary<string, object>
                    {
                        { "StartYear", 12 }, 
                        { "EndYear", 30 }, 
                        { "PIMin", 1028.24M }, 
                        { "PIMax", 2067.66M }, 
                        { "MI", 0.00M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", false }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 1028.24M }, 
                        { "PaymentMax", 2067.66M }, 
                    },
                };

            TestPaymentDict(expected, tridProjectedPayments);
        }

        /// <summary>
        /// Tests that we can reproduce the scenario for this LE:
        /// http://files.consumerfinance.gov/f/201403_cfpb_loan-estimate_refinance-sample-H24D.pdf
        /// </summary>
        [Test]
        public void CFPBSampleLeFixedMi()
        {
            amortTableDataStub.sFinMethT = E_sFinMethT.Fixed;
            amortTableDataStub.sLAmtCalc = 150000.00M;
            amortTableDataStub.sFinalLAmt = 150000.00M;
            amortTableDataStub.sHouseVal = 180000.00M;
            amortTableDataStub.sTerm = 360;
            amortTableDataStub.sDue = 360;
            amortTableDataStub.sNoteIR = 4.250M;
            amortTableDataStub.sProMInsR = 0.656M;
            amortTableDataStub.sProMIns = 82.00M;
            amortTableDataStub.sProMInsCancelLtv = 78.000M;

            GenerateAmortTables();

            tridProjectedPayments = TridProjectedPayment.GenerateProjectedPayments(
                amortTable, bestCase, worstCase, 206.00M, null);

            List<Dictionary<string, object>> expected =
                new List<Dictionary<string, object>>
                {
                    new Dictionary<string, object>
                    {
                        { "StartYear", 1 }, 
                        { "EndYear", 4 }, 
                        { "PIMin", 737.91M }, 
                        { "PIMax", 737.91M }, 
                        { "MI", 82.00M }, 
                        { "Escrow", 206.00M }, 
                        { "HasInterestOnly", false }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 1025.91M }, 
                        { "PaymentMax", 1025.91M }, 
                    },

                    new Dictionary<string, object>
                    {
                        { "StartYear", 5 }, 
                        { "EndYear", 30 }, 
                        { "PIMin", 737.91M }, 
                        { "PIMax", 737.91M }, 
                        { "MI", 0.00M }, 
                        { "Escrow", 206.00M }, 
                        { "HasInterestOnly", false }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 943.91M }, 
                        { "PaymentMax", 943.91M }, 
                    },
                };

            TestPaymentDict(expected, tridProjectedPayments);
        }

        /// <summary>
        /// Tests that we can reproduce the scenario for this LE:
        /// http://files.consumerfinance.gov/f/201403_cfpb_loan-estimate_baloon-payment-H24E.pdf
        /// </summary>
        [Test]
        public void CFPBSampleLeFixedBalloonMi()
        {
            amortTableDataStub.sFinMethT = E_sFinMethT.Fixed;
            amortTableDataStub.sLAmtCalc = 171000.00M;
            amortTableDataStub.sFinalLAmt = 171000.00M;
            amortTableDataStub.sHouseVal = 190000.00M;
            amortTableDataStub.sTerm = 360;
            amortTableDataStub.sDue = 84;
            amortTableDataStub.sNoteIR = 4.375M;
            amortTableDataStub.sProMInsR = 0.610M;
            amortTableDataStub.sProMIns = 86.93M;
            amortTableDataStub.sProMInsCancelLtv = 78.000M;

            GenerateAmortTables();

            tridProjectedPayments = TridProjectedPayment.GenerateProjectedPayments(
                amortTable, bestCase, worstCase, 309.00M, null);

            List<Dictionary<string, object>> expected =
                new List<Dictionary<string, object>>
                {
                    new Dictionary<string, object>
                    {
                        { "StartYear", 1 }, 
                        { "EndYear", 7 }, 
                        { "PIMin", 853.78M }, 
                        { "PIMax", 853.78M }, 
                        { "MI", 86.93M }, 
                        { "Escrow", 309.00M }, 
                        { "HasInterestOnly", false }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 1249.71M }, 
                        { "PaymentMax", 1249.71M }, 
                    },

                    new Dictionary<string, object>
                    {
                        { "StartYear", 0 }, 
                        { "EndYear", 0 }, 
                        { "PIMin", 149262.95M }, 
                        { "PIMax", 149262.95M }, 
                        { "MI", 0.00M }, 
                        { "Escrow", 0.00M }, 
                        { "HasInterestOnly", false }, 
                        { "IsFinalBalloonPayment", true }, 
                        { "PaymentMin", 149262.95M }, 
                        { "PaymentMax", 149262.95M }, 
                    },
                };

            TestPaymentDict(expected, tridProjectedPayments);
        }

        /// <summary>
        /// Tests that we can reproduce the scenario for this CD:
        /// http://files.consumerfinance.gov/f/201403_cfpb_closing-disclosure_cover-H25B.pdf
        /// </summary>
        [Test]
        public void CFPBSampleCdFixedMi()
        {
            amortTableDataStub.sFinMethT = E_sFinMethT.Fixed;
            amortTableDataStub.sLAmtCalc = 162000.00M;
            amortTableDataStub.sFinalLAmt = 162000.00M;
            amortTableDataStub.sHouseVal = 180000.00M;
            amortTableDataStub.sTerm = 360;
            amortTableDataStub.sDue = 360;
            amortTableDataStub.sNoteIR = 3.875M;
            amortTableDataStub.sProMInsR = 0.610M;
            amortTableDataStub.sProMIns = 82.35M;
            amortTableDataStub.sProMInsCancelLtv = 78.000M;

            GenerateAmortTables();

            tridProjectedPayments = TridProjectedPayment.GenerateProjectedPayments(
                amortTable, bestCase, worstCase, 206.13M, null);

            List<Dictionary<string, object>> expected =
                new List<Dictionary<string, object>>
                {
                    new Dictionary<string, object>
                    {
                        { "StartYear", 1 }, 
                        { "EndYear", 7 }, 
                        { "PIMin", 761.78M }, 
                        { "PIMax", 761.78M }, 
                        { "MI", 82.35M }, 
                        { "Escrow", 206.13M }, 
                        { "HasInterestOnly", false }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 1050.26M }, 
                        { "PaymentMax", 1050.26M }, 
                    },

                    new Dictionary<string, object>
                    {
                        { "StartYear", 8 }, 
                        { "EndYear", 30 }, 
                        { "PIMin", 761.78M }, 
                        { "PIMax", 761.78M }, 
                        { "MI", 0.00M }, 
                        { "Escrow", 206.13M }, 
                        { "HasInterestOnly", false }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 967.91M }, 
                        { "PaymentMax", 967.91M }, 
                    },
                };

            TestPaymentDict(expected, tridProjectedPayments);
        }

        /// <summary>
        /// Tests that we can reproduce the scenario for this CD:
        /// http://files.consumerfinance.gov/f/201403_cfpb_closing-disclosure_cover-H25E.pdf
        /// </summary>
        [Test]
        public void CFPBSampleCdFixedMi2()
        {
            amortTableDataStub.sFinMethT = E_sFinMethT.Fixed;
            amortTableDataStub.sLAmtCalc = 150000.00M;
            amortTableDataStub.sFinalLAmt = 150000.00M;
            amortTableDataStub.sHouseVal = 180000.00M;
            amortTableDataStub.sTerm = 360;
            amortTableDataStub.sDue = 360;
            amortTableDataStub.sNoteIR = 4.250M;
            amortTableDataStub.sProMInsR = 0.659M; // Careless CFPB, would need to be 0.6588 
            amortTableDataStub.sProMIns = 82.35M; // Careles CFPB part 2.
            amortTableDataStub.sProMInsCancelLtv = 78.000M;

            GenerateAmortTables();

            tridProjectedPayments = TridProjectedPayment.GenerateProjectedPayments(
                amortTable, bestCase, worstCase, 206.13M, null);

            List<Dictionary<string, object>> expected =
                new List<Dictionary<string, object>>
                {
                    new Dictionary<string, object>
                    {
                        { "StartYear", 1 }, 
                        { "EndYear", 4 }, 
                        { "PIMin", 737.91M }, 
                        { "PIMax", 737.91M }, 
                        { "MI", 82.35M }, 
                        { "Escrow", 206.13M }, 
                        { "HasInterestOnly", false }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 1026.39M }, 
                        { "PaymentMax", 1026.39M }, 
                    },
                    new Dictionary<string, object>
                    {
                        { "StartYear", 5 }, 
                        { "EndYear", 30 }, 
                        { "PIMin", 737.91M }, 
                        { "PIMax", 737.91M }, 
                        { "MI", 0.00M }, 
                        { "Escrow", 206.13M }, 
                        { "HasInterestOnly", false }, 
                        { "IsFinalBalloonPayment", false }, 
                        { "PaymentMin", 944.04M }, 
                        { "PaymentMax", 944.04M }, 
                    },
                };

            TestPaymentDict(expected, tridProjectedPayments);
        }

        /// <summary>
        /// Tests that we can reproduce the example from
        /// 12 CFR 1026.37(c)(1)(iii)(B)-1(i)
        /// </summary>
        [Test]
        public void RegulatoryExample1026_37_c_1_ii_B__1_i()
        {
            amortTableDataStub.sFinMethT = E_sFinMethT.ARM;
            amortTableDataStub.sLAmtCalc = 200000.00M;
            amortTableDataStub.sFinalLAmt = 200000.00M;
            amortTableDataStub.sHouseVal = 250000.00M;
            amortTableDataStub.sTerm = 360;
            amortTableDataStub.sDue = 360;
            amortTableDataStub.sNoteIR = 6.000M;
            amortTableDataStub.sProMIns = 0.000M;

            amortTableDataStub.sRAdj1stCapR = 1.000M;
            amortTableDataStub.sRAdj1stCapMon = 1;
            amortTableDataStub.sRAdjCapR = 1.000M;
            amortTableDataStub.sRAdjCapMon = 1;
            amortTableDataStub.sRAdjLifeCapR = 12.000M;
            amortTableDataStub.sRAdjMarginR = 2.500M;
            amortTableDataStub.sRAdjIndexR = 2.000M;
            amortTableDataStub.sRAdjFloorR = 2.500M;
            amortTableDataStub.sRAdjRoundToR = 0.125M;

            GenerateAmortTables();

            tridProjectedPayments = TridProjectedPayment.GenerateProjectedPayments(
                amortTable, bestCase, worstCase, 0.00M, null);

            List<Dictionary<string, object>> expected =
                new List<Dictionary<string, object>>
                {
                    new Dictionary<string, object>
                    {
                        { "StartYear", 1 },
                        { "EndYear", 1 },
                        { "PIMin", 791.92M },
                        { "PIMax", 2840.33M },
                        { "MI", 0.00M },
                        { "Escrow", 0.00M },
                        { "HasInterestOnly", false },
                        { "IsFinalBalloonPayment", false },
                        { "PaymentMin", 791.92M },
                        { "PaymentMax", 2840.33M },
                    },

                    new Dictionary<string, object>
                    {
                        { "StartYear", 2 },
                        { "EndYear", 30 },
                        { "PIMin", 791.92M },
                        { "PIMax", 3001.78M },
                        { "MI", 0.00M },
                        { "Escrow", 0.00M },
                        { "HasInterestOnly", false },
                        { "IsFinalBalloonPayment", false },
                        { "PaymentMin", 791.92M },
                        { "PaymentMax", 3001.78M },
                    }
                };

            TestPaymentDict(expected, tridProjectedPayments);
        }

        /// <summary>
        /// Tests that we can reproduce the example from
        /// 12 CFR 1026.37(c)(1)(iii)(B)-1(iii)
        /// </summary>
        [Test]
        public void RegulatoryExample1026_37_c_1_ii_B__1_ii()
        {
            amortTableDataStub.sFinMethT = E_sFinMethT.Fixed;
            amortTableDataStub.sLAmtCalc = 200000.00M;
            amortTableDataStub.sFinalLAmt = 200000.00M;
            amortTableDataStub.sHouseVal = 250000.00M;
            amortTableDataStub.sTerm = 360;
            amortTableDataStub.sDue = 360;
            amortTableDataStub.sNoteIR = 6.000M;
            amortTableDataStub.sProMIns = 0.00M;

            amortTableDataStub.sBuydwnMon1 = 3;
            amortTableDataStub.sBuydwnR1 = 3.000M;
            amortTableDataStub.sBuydwnMon2 = 3;
            amortTableDataStub.sBuydwnR2 = 2.000M;

            GenerateAmortTables();

            tridProjectedPayments = TridProjectedPayment.GenerateProjectedPayments(
                amortTable, bestCase, worstCase, 0.00M, null);

            List<Dictionary<string, object>> expected =
                new List<Dictionary<string, object>>
                {
                    new Dictionary<string, object>
                    {
                        { "StartYear", 1 },
                        { "EndYear", 1 },
                        { "PIMin", 843.21M },
                        { "PIMax", 1199.10M },
                        { "MI", 0.00M },
                        { "Escrow", 0.00M },
                        { "HasInterestOnly", false },
                        { "IsFinalBalloonPayment", false },
                        { "PaymentMin", 843.21M },
                        { "PaymentMax", 1199.10M },
                    },
                    new Dictionary<string, object>
                    {
                        { "StartYear", 2 },
                        { "EndYear", 30 },
                        { "PIMin", 1199.10M },
                        { "PIMax", 1199.10M },
                        { "MI", 0.00M },
                        { "Escrow", 0.00M },
                        { "HasInterestOnly", false },
                        { "IsFinalBalloonPayment", false },
                        { "PaymentMin", 1199.10M },
                        { "PaymentMax", 1199.10M },
                    },
                };

            TestPaymentDict(expected, tridProjectedPayments);
        }

        /// <summary>
        /// Tests that we can reproduce the example from
        /// 12 CFR 1026.37(c)(1)(iii)(B)-1(iii)
        /// </summary>
        [Test]
        public void RegulatoryExample1026_37_c_1_ii_B__1_iii()
        {
            amortTableDataStub.sFinMethT = E_sFinMethT.Fixed;
            amortTableDataStub.sLAmtCalc = 200000.00M;
            amortTableDataStub.sFinalLAmt = 200000.00M;
            amortTableDataStub.sHouseVal = 250000.00M;
            amortTableDataStub.sTerm = 360;
            amortTableDataStub.sDue = 360;
            amortTableDataStub.sNoteIR = 6.000M;
            amortTableDataStub.sProMIns = 0.00M;

            amortTableDataStub.sBuydwnMon1 = 3;
            amortTableDataStub.sBuydwnR1 = 3.000M;
            amortTableDataStub.sBuydwnMon2 = 3;
            amortTableDataStub.sBuydwnR2 = 2.000M;
            amortTableDataStub.sBuydwnMon3 = 12;
            amortTableDataStub.sBuydwnR3 = 1.000M;

            GenerateAmortTables();

            tridProjectedPayments = TridProjectedPayment.GenerateProjectedPayments(
                amortTable, bestCase, worstCase, 0.00M, null);

            List<Dictionary<string, object>> expected =
                new List<Dictionary<string, object>>
                {
                    new Dictionary<string, object>
                    {
                        { "StartYear", 1 },
                        { "EndYear", 1 },
                        { "PIMin", 843.21M },
                        { "PIMax", 1073.64M },
                        { "MI", 0.00M },
                        { "Escrow", 0.00M },
                        { "HasInterestOnly", false },
                        { "IsFinalBalloonPayment", false },
                        { "PaymentMin", 843.21M },
                        { "PaymentMax", 1073.64M },
                    },
                    new Dictionary<string, object>
                    {
                        { "StartYear", 2 },
                        { "EndYear", 2 },
                        { "PIMin", 1073.64M },
                        { "PIMax", 1073.64M },
                        { "MI", 0.00M },
                        { "Escrow", 0.00M },
                        { "HasInterestOnly", false },
                        { "IsFinalBalloonPayment", false },
                        { "PaymentMin", 1073.64M },
                        { "PaymentMax", 1073.64M },
                    },
                    new Dictionary<string, object>
                    {
                        { "StartYear", 3 },
                        { "EndYear", 30 },
                        { "PIMin", 1199.10M },
                        { "PIMax", 1199.10M },
                        { "MI", 0.00M },
                        { "Escrow", 0.00M },
                        { "HasInterestOnly", false },
                        { "IsFinalBalloonPayment", false },
                        { "PaymentMin", 1199.10M },
                        { "PaymentMax", 1199.10M },
                    }
                };

            TestPaymentDict(expected, tridProjectedPayments);
        }
    }
}
