﻿using System;
using System.Collections.Generic;
using System.Data;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Constants;
using LendOSolnTest.Common;
using LendOSolnTest.Fakes;
using NUnit.Framework;

namespace LendOSolnTest.Loan
{
    [TestFixture]
    public class AgentFieldsTest
    {
        private static readonly string[] testData = 
        {
            "AgentName",
            "CaseNum",
            "CompanyName",
            "LicenseNumOfCompany",
            "(714) 111-1111",
            "StreetAddr",
            "City",
            "CA",
            "92805",
            null,
            "12-1234567",
            "BranchName",
            "PayToBankName",
            "PayToBankCityState",
            "PayToABANumber",
            "PayToAccountNumber",
            "PayToAccountName",
            "FurtherCreditToAccountNumber",
            "FurtherCreditToAccountName"
        };

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            LoginTools.LoginAs(TestAccountType.LoTest001);
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
            FakePageDataUtilities.Instance.Reset();
        }

        private CPageData CreateDataObject()
        {
            var dataLoan = FakePageData.Create();

            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            // 5/22/2009 dd - This is a HACK way of clearing Official Contact List.
            DataSet ds = dataLoan.sAgentDataSet;
            ds.Clear();
            dataLoan.sAgentDataSet = ds;
            dataLoan.Save();

            return dataLoan;
        }

        [Test]
        public void AgentLendingLicenseTest()
        {
            List<LicenseInfo> licenseInfoList = new List<LicenseInfo>()
            {
                new LicenseInfo() { License = "LICENSE001", State = "", ExpD="1/1/9999"}
                , new LicenseInfo() { License = "LICENSE002", State = "CA", ExpD="1/1/1990" }
                , new LicenseInfo() { License = "LICENSE003", State = "AZ", ExpD="1/1/2001" }
            };
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            CAgentFields fields = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.CreateNew);
            fields.AgentName = "AgentName";

            Assert.AreEqual(0, fields.LicenseInfoList.Count, "Initial Lending Info Count");

            foreach (LicenseInfo licenseInfo in licenseInfoList)
            {
                LicenseInfo o = new LicenseInfo();
                o.License = licenseInfo.License;
                o.State = licenseInfo.State;
                o.ExpD = licenseInfo.ExpD;
                fields.LicenseInfoList.Add(o);
            }
            fields.Update();

            dataLoan.Save();

            dataLoan = FakePageData.Create();
            dataLoan.InitLoad();

            fields = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.CreateNew);

            Assert.AreEqual("AgentName", fields.AgentName);
            Assert.AreEqual(licenseInfoList.Count, fields.LicenseInfoList.Count, "After Lending Info Count");

            for (int i = 0; i < licenseInfoList.Count; i++)
            {
                LicenseInfo expectedLicenseInfo = licenseInfoList[i];
                LicenseInfo actualLicenseInfo = (LicenseInfo)fields.LicenseInfoList.List[i];
                Assert.AreEqual(expectedLicenseInfo.License, actualLicenseInfo.License, "License");
                Assert.AreEqual(expectedLicenseInfo.State, actualLicenseInfo.State, "State");
                Assert.AreEqual(expectedLicenseInfo.ExpD, actualLicenseInfo.ExpD, "ExpD");

            }

        }

        [Test]
        public void CompanyLendingLicenseTest()
        {
            List<LicenseInfo> licenseInfoList = new List<LicenseInfo>()
            {
                new LicenseInfo() { License = "LICENSE001", State = "", ExpD="1/1/9999"}
                , new LicenseInfo() { License = "LICENSE002", State = "CA", ExpD="1/1/1990" }
                , new LicenseInfo() { License = "LICENSE003", State = "AZ", ExpD="1/1/2001" }
            };
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            CAgentFields fields = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.CreateNew);
            fields.AgentName = "AgentName";

            Assert.AreEqual(0, fields.CompanyLicenseInfoList.Count, "Initial Company Lending Info Count");

            foreach (LicenseInfo licenseInfo in licenseInfoList)
            {
                LicenseInfo o = new LicenseInfo();
                o.License = licenseInfo.License;
                o.State = licenseInfo.State;
                o.ExpD = licenseInfo.ExpD;
                fields.CompanyLicenseInfoList.Add(o);
            }
            fields.Update();

            dataLoan.Save();

            dataLoan = FakePageData.Create();
            dataLoan.InitLoad();

            fields = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.CreateNew);

            Assert.AreEqual("AgentName", fields.AgentName);
            Assert.AreEqual(licenseInfoList.Count, fields.CompanyLicenseInfoList.Count, "After Lending Info Count");

            for (int i = 0; i < licenseInfoList.Count; i++)
            {
                LicenseInfo expectedLicenseInfo = licenseInfoList[i];
                LicenseInfo actualLicenseInfo = (LicenseInfo)fields.CompanyLicenseInfoList.List[i];
                Assert.AreEqual(expectedLicenseInfo.License, actualLicenseInfo.License, "License");
                Assert.AreEqual(expectedLicenseInfo.State, actualLicenseInfo.State, "State");
                Assert.AreEqual(expectedLicenseInfo.ExpD, actualLicenseInfo.ExpD, "ExpD");

            }

        }

        [Test]
        public void SimpleTest()
        {
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            // Test Add
            CAgentFields fields = dataLoan.sAgentCollection.GetAgentOfRole(E_AgentRoleT.Bank, E_ReturnOptionIfNotExist.CreateNew);
            Assert.AreEqual(E_AgentRoleT.Bank, fields.AgentRoleT, "AgentRoleT");

            this.PopulateTestAgentData(fields);
            Assert.AreEqual(fields.AgentSourceT, E_AgentSourceT.ManuallyEntered, "AgentSourceT");
            fields.AgentSourceT = E_AgentSourceT.NotYetDefined;
            Assert.AreEqual(fields.BrokerLevelAgentID, Guid.Empty);
            var testBrokerLevelAgentID = Guid.NewGuid();
            fields.BrokerLevelAgentID = testBrokerLevelAgentID;            
            fields.Update();
            dataLoan.Save();

            // Test Retrieve
            dataLoan = FakePageData.Create();
            dataLoan.InitLoad();
            fields = dataLoan.sAgentCollection.GetAgentOfRole(E_AgentRoleT.Bank, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            Assert.AreEqual(E_AgentRoleT.Bank, fields.AgentRoleT, "AgentRoleT");

            Assert.AreEqual(testData[0], fields.AgentName, "AgentName");
            Assert.AreEqual(testData[1], fields.CaseNum, "CaseNum");
            Assert.AreEqual(testData[2], fields.CompanyName, "CompanyName");
            Assert.AreEqual(testData[3], fields.LicenseNumOfCompany, "LicenseNumOfCompany");
            Assert.AreEqual(testData[4], fields.CellPhone, "CellPhone");
            Assert.AreEqual(testData[5], fields.StreetAddr, "StreetAddr");
            Assert.AreEqual(testData[6], fields.City, "City");
            Assert.AreEqual(testData[7], fields.State, "State");
            Assert.AreEqual(testData[8], fields.Zip, "Zip");
            Assert.AreEqual("", fields.DepartmentName, "DepartmentName");
            Assert.AreEqual(testData[10], fields.TaxId, "TaxId");
            Assert.AreEqual(testData[11], fields.BranchName, "BranchName");
            Assert.AreEqual(testData[12], fields.PayToBankName, "PayToBankName");
            Assert.AreEqual(testData[13], fields.PayToBankCityState, "PayToBankCityState");
            Assert.AreEqual(testData[14], fields.PayToABANumber.Value, "PayToABANumber");
            Assert.AreEqual(testData[15], fields.PayToAccountNumber.Value, "PayToAccountNumber");
            Assert.AreEqual(testData[16], fields.PayToAccountName, "PayToAccountName");
            Assert.AreEqual(testData[17], fields.FurtherCreditToAccountNumber.Value, "FurtherCreditToAccountNumber");
            Assert.AreEqual(testData[18], fields.FurtherCreditToAccountName, "FurtherCreditToAccountName");
            Assert.AreEqual(E_AgentSourceT.NotYetDefined, fields.AgentSourceT, "AgentsSourceT");
            Assert.AreEqual(testBrokerLevelAgentID, fields.BrokerLevelAgentID, "BrokerLevelAgentID");
            
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sAgentCollection.sAgentDataSetClear();
            dataLoan.Save();
            
            dataLoan.InitLoad();
            fields = dataLoan.sAgentCollection.GetAgentOfRole(E_AgentRoleT.Bank, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            Assert.AreEqual("", fields.AgentName, "AgentName");
            

        }

        [Test]
        public void AgentDeletion_SingleAgent_CanDelete()
        {
            var loan = this.CreateDataObject();
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            var newAgent = loan.GetAgentOfRole(E_AgentRoleT.Trustee, E_ReturnOptionIfNotExist.CreateNew);
            this.PopulateTestAgentData(newAgent);
            newAgent.Update();

            loan.Save();

            loan.InitLoad();

            Assert.IsTrue(loan.GetAgentOfRole(E_AgentRoleT.Trustee, E_ReturnOptionIfNotExist.ReturnEmptyObject).IsValid);

            loan.DeleteAgent(newAgent.RecordId);

            Assert.IsFalse(loan.GetAgentOfRole(E_AgentRoleT.Trustee, E_ReturnOptionIfNotExist.ReturnEmptyObject).IsValid);
        }

        [Test]
        public void AgentDeletionWithHashSet_SingleAgent_CanDelete()
        {
            var loan = this.CreateDataObject();
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            var newAgent = loan.GetAgentOfRole(E_AgentRoleT.Trustee, E_ReturnOptionIfNotExist.CreateNew);
            this.PopulateTestAgentData(newAgent);
            newAgent.Update();

            loan.Save();

            loan.InitLoad();

            Assert.IsTrue(loan.GetAgentOfRole(E_AgentRoleT.Trustee, E_ReturnOptionIfNotExist.ReturnEmptyObject).IsValid);

            var agentsToDelete = new HashSet<Guid>() { newAgent.RecordId };
            var agentsNotDeleted = new HashSet<Guid>();

            var wasDeleted = loan.DeleteAgents(agentsToDelete, out agentsNotDeleted);

            Assert.IsTrue(wasDeleted);
            Assert.AreEqual(0, agentsNotDeleted.Count);
        }

        [Test]
        public void AgentDeletionWithHashSet_MultipleAgents_CanDelete()
        {
            var loan = this.CreateDataObject();
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            var newProcessor = loan.GetAgentOfRole(E_AgentRoleT.Processor, E_ReturnOptionIfNotExist.CreateNew);
            this.PopulateTestAgentData(newProcessor);
            newProcessor.Update();

            var newRealtor = loan.GetAgentOfRole(E_AgentRoleT.Realtor, E_ReturnOptionIfNotExist.CreateNew);
            this.PopulateTestAgentData(newRealtor);
            newRealtor.Update();

            var newMortgagee = loan.GetAgentOfRole(E_AgentRoleT.Mortgagee, E_ReturnOptionIfNotExist.CreateNew);
            this.PopulateTestAgentData(newMortgagee);
            newMortgagee.Update();

            loan.Save();

            loan.InitLoad();

            Assert.IsTrue(loan.GetAgentOfRole(E_AgentRoleT.Processor, E_ReturnOptionIfNotExist.ReturnEmptyObject).IsValid);
            Assert.IsTrue(loan.GetAgentOfRole(E_AgentRoleT.Realtor, E_ReturnOptionIfNotExist.ReturnEmptyObject).IsValid);
            Assert.IsTrue(loan.GetAgentOfRole(E_AgentRoleT.Mortgagee, E_ReturnOptionIfNotExist.ReturnEmptyObject).IsValid);

            var agentsToDelete = new HashSet<Guid>() { newProcessor.RecordId, newRealtor.RecordId, newMortgagee.RecordId };
            var agentsNotDeleted = new HashSet<Guid>();

            var wasDeleted = loan.DeleteAgents(agentsToDelete, out agentsNotDeleted);

            Assert.IsTrue(wasDeleted);
            Assert.AreEqual(0, agentsNotDeleted.Count);
        }

        private void PopulateTestAgentData(CAgentFields fields)
        {
            fields.AgentName = testData[0];
            fields.CaseNum = testData[1];
            fields.CompanyName = testData[2];
            fields.LicenseNumOfCompany = testData[3];
            fields.CellPhone = testData[4];
            fields.StreetAddr = testData[5];
            fields.City = testData[6];
            fields.State = testData[7];
            fields.Zip = testData[8];
            fields.DepartmentName = testData[9];
            fields.TaxId = testData[10];
            fields.BranchName = testData[11];
            fields.PayToBankName = testData[12];
            fields.PayToBankCityState = testData[13];
            fields.PayToABANumber = testData[14];
            fields.PayToAccountNumber = testData[15];
            fields.PayToAccountName = testData[16];
            fields.FurtherCreditToAccountNumber = testData[17];
            fields.FurtherCreditToAccountName = testData[18];
        }
    }
}
