﻿// <copyright file="ToleranceCureTest.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Douglas Telfer
//    Date:   11/16/2015
// </summary>
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using System.Threading.Tasks;
using DataAccess;
using DataAccess.GFE;
using LendOSolnTest.Common;

namespace LendOSolnTest.Loan
{
    [TestFixture]
    public class ToleranceCureTest
    {
        private BorrowerClosingCostSet liveClosingCostSet;
        private BorrowerClosingCostSet archivedClosingCostSet;

        private GfeToleranceViolationCalculator tolCalc;
        private Func<BaseClosingCostFee, bool> feeFilter;

        [TestFixtureSetUp]
        public void FixtureInit()
        {
            tolCalc = new GfeToleranceViolationCalculator();
            feeFilter = bFee => ClosingCostSetUtils.ExOrigCompDiscountPrepsSecGMIFilter((BorrowerClosingCostFee)bFee);
        }

        [SetUp]
        public void FixtureSetUp()
        {
            liveClosingCostSet = new BorrowerClosingCostSet(string.Empty, null);
            archivedClosingCostSet = new BorrowerClosingCostSet(string.Empty, null);
        }

        private static BorrowerClosingCostFee NewTridFee(
            Guid closingCostFeeTypeId, 
            decimal baseAmount,
            bool isUSDALoan = false,
            bool isOptional = false,
            bool isApr = true,
            bool isFhaAllowable = true,
            E_AgentRoleT beneficiary = E_AgentRoleT.Lender,
            bool isThirdParty = false,
            E_GfeResponsiblePartyT gfeResponsiblePartyT = E_GfeResponsiblePartyT.Buyer,
            bool isAffiliate = false,
            bool canShop = false,
            bool didShop = false,
            E_IntegratedDisclosureSectionT integratedDisclosureSectionT = E_IntegratedDisclosureSectionT.LeaveBlank,
            E_ClosingCostFeeMismoFeeT mismoFeeT = E_ClosingCostFeeMismoFeeT.Undefined,
            E_GfeSectionT gfeSectionT = E_GfeSectionT.LeaveBlank,
            BorrowerClosingCostFeePayment[] payments = null)
        {
            BorrowerClosingCostFee newFee = new BorrowerClosingCostFee();
            newFee.ClosingCostFeeTypeId = closingCostFeeTypeId;
            newFee.BaseAmount = baseAmount;
            newFee.IsApr = isApr;
            newFee.IsFhaAllowable = isFhaAllowable;
            newFee.Beneficiary = beneficiary;
            newFee.IsThirdParty = isThirdParty;
            newFee.GfeResponsiblePartyT = gfeResponsiblePartyT;
            newFee.IsAffiliate = isAffiliate;
            newFee.CanShop = canShop;
            newFee.DidShop = didShop;
            newFee.IntegratedDisclosureSectionT = integratedDisclosureSectionT;
            newFee.MismoFeeT = mismoFeeT;
            newFee.GfeSectionT = gfeSectionT;

            if (integratedDisclosureSectionT == E_IntegratedDisclosureSectionT.LeaveBlank)
            {
                newFee.IntegratedDisclosureSectionT = ClosingCostSetUtils.DetermineConstantSectionForFee(closingCostFeeTypeId) ?? integratedDisclosureSectionT;
            }

            if (isOptional)
            {
                newFee.IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionH;
                newFee.IsOptional = isOptional;
            }

            if (mismoFeeT == E_ClosingCostFeeMismoFeeT.Undefined)
            {
                newFee.MismoFeeT = ClosingCostSetUtils.GetMismoFeeType(closingCostFeeTypeId, isUSDALoan) ?? mismoFeeT;
            }

            if (gfeSectionT == E_GfeSectionT.LeaveBlank)
            {
                newFee.GfeSectionT = ClosingCostSetUtils.GetGfeSectionType(closingCostFeeTypeId) ?? gfeSectionT;
            }

            if (payments == null)
            {
                BorrowerClosingCostFeePayment payment = new BorrowerClosingCostFeePayment();
                payment.Amount = baseAmount;
                payment.PaidByT = E_ClosingCostFeePaymentPaidByT.Borrower;
                payment.GfeClosingCostFeePaymentTimingT = E_GfeClosingCostFeePaymentTimingT.AtClosing;
                payment.PaymentDate = DateTime.Now;

                newFee.AddPayment(payment);
            }
            else
            {
                foreach (BorrowerClosingCostFeePayment payment in payments)
                {
                    newFee.AddPayment(payment);
                }
            }

            return newFee;
        }

        private static void AddFees(BorrowerClosingCostSet ccSet, params BorrowerClosingCostFee[] fees)
        {
            foreach (BorrowerClosingCostFee fee in fees)
            {
                ccSet.AddOrUpdate(fee);
            }
        }

        [Test]
        public void TridLenderCreditToleranceCure()
        {
            decimal cure =
                 tolCalc.GetZeroPercentToleranceCure(
                    archivedClosingCostSet, feeFilter,
                    liveClosingCostSet, feeFilter,
                    E_sDisclosureRegulationT.TRID, E_sClosingCostFeeVersionT.ClosingCostFee2015, -100.00M, -50.00M);

            Assert.AreEqual(50.00M, cure);
        }

        [Test]
        public void TridLenderCreditToleranceNoCure()
        {
            decimal cure =
                 tolCalc.GetZeroPercentToleranceCure(
                    archivedClosingCostSet, feeFilter,
                    liveClosingCostSet, feeFilter,
                    E_sDisclosureRegulationT.TRID, E_sClosingCostFeeVersionT.ClosingCostFee2015, -50.00M, -100.00M);

            Assert.AreEqual(0.00M, cure);
        }

        [Test]
        public void TridZeroToleranceCureSameFee()
        {
            AddFees(
                archivedClosingCostSet,
                NewTridFee(DefaultSystemClosingCostFee.Hud800LoanOriginationFeeTypeId, 1.00M));

            AddFees(
                liveClosingCostSet,
                NewTridFee(DefaultSystemClosingCostFee.Hud800LoanOriginationFeeTypeId, 10.00M));

            decimal cure =
                tolCalc.GetZeroPercentToleranceCure(
                    archivedClosingCostSet, feeFilter, 
                    liveClosingCostSet, feeFilter,
                    E_sDisclosureRegulationT.TRID, E_sClosingCostFeeVersionT.ClosingCostFee2015, 0, 0);

            Assert.AreEqual(9.00M, cure);
        }

        [Test]
        public void TridZeroToleranceNonCureSameFee()
        {
            AddFees(
                archivedClosingCostSet,
                NewTridFee(DefaultSystemClosingCostFee.Hud800LoanOriginationFeeTypeId, 10.00M));

            AddFees(
                liveClosingCostSet,
                NewTridFee(DefaultSystemClosingCostFee.Hud800LoanOriginationFeeTypeId, 1.00M));

            decimal cure =
                tolCalc.GetZeroPercentToleranceCure(
                    archivedClosingCostSet, feeFilter,
                    liveClosingCostSet, feeFilter,
                    E_sDisclosureRegulationT.TRID, E_sClosingCostFeeVersionT.ClosingCostFee2015, 0, 0);

            Assert.AreEqual(0.00M, cure);
        }

        [Test]
        public void TridZeroToleranceCureNewFee()
        {
            AddFees(
                archivedClosingCostSet,
                NewTridFee(DefaultSystemClosingCostFee.Hud800LoanOriginationFeeTypeId, 5.00M));

            AddFees(
                liveClosingCostSet,
                NewTridFee(DefaultSystemClosingCostFee.Hud800ProcessingFeeTypeId, 5.00M));

            decimal cure =
                tolCalc.GetZeroPercentToleranceCure(
                    archivedClosingCostSet, feeFilter,
                    liveClosingCostSet, feeFilter,
                    E_sDisclosureRegulationT.TRID, E_sClosingCostFeeVersionT.ClosingCostFee2015, 0, 0);

            Assert.AreEqual(5.00M, cure);
        }

        [Test]
        public void TridZeroToleranceCureMixedFees()
        {
            AddFees(
                archivedClosingCostSet,
                NewTridFee(DefaultSystemClosingCostFee.Hud800LoanOriginationFeeTypeId, 10.00M));

            AddFees(
                liveClosingCostSet,
                NewTridFee(DefaultSystemClosingCostFee.Hud800LoanOriginationFeeTypeId, 5.00M),
                NewTridFee(DefaultSystemClosingCostFee.Hud800ProcessingFeeTypeId, 5.00M));

            decimal cure =
                tolCalc.GetZeroPercentToleranceCure(
                    archivedClosingCostSet, feeFilter,
                    liveClosingCostSet, feeFilter,
                    E_sDisclosureRegulationT.TRID, E_sClosingCostFeeVersionT.ClosingCostFee2015, 0, 0);

            Assert.AreEqual(5.00M, cure);
        }

        [Test]
        public void TridTenToleranceCureSameFee()
        {
            AddFees(
                archivedClosingCostSet,
                NewTridFee(DefaultSystemClosingCostFee.Hud800AppraisalFeeFeeTypeId, 10.00M, isThirdParty: true, beneficiary: E_AgentRoleT.Appraiser, canShop: true));

            AddFees(
                liveClosingCostSet,
                NewTridFee(DefaultSystemClosingCostFee.Hud800AppraisalFeeFeeTypeId, 20.00M, isThirdParty: true, beneficiary: E_AgentRoleT.Appraiser, canShop: true));

            decimal cure =
                tolCalc.GetTenPercentToleranceCure(
                    archivedClosingCostSet, feeFilter,
                    liveClosingCostSet, feeFilter,
                    E_sDisclosureRegulationT.TRID, E_sClosingCostFeeVersionT.ClosingCostFee2015);

            Assert.AreEqual(9.00M, cure);
        }

        [Test]
        public void TridTenToleranceNoCureSameFee()
        {
            AddFees(
                archivedClosingCostSet,
                NewTridFee(DefaultSystemClosingCostFee.Hud800AppraisalFeeFeeTypeId, 10.00M, isThirdParty: true, beneficiary: E_AgentRoleT.Appraiser, canShop: true));

            AddFees(
                liveClosingCostSet,
                NewTridFee(DefaultSystemClosingCostFee.Hud800AppraisalFeeFeeTypeId, 11.00M, isThirdParty: true, beneficiary: E_AgentRoleT.Appraiser, canShop: true));

            decimal cure =
                tolCalc.GetTenPercentToleranceCure(
                    archivedClosingCostSet, feeFilter,
                    liveClosingCostSet, feeFilter,
                    E_sDisclosureRegulationT.TRID, E_sClosingCostFeeVersionT.ClosingCostFee2015);

            Assert.AreEqual(0.00M, cure);
        }

        [Test]
        public void TridTenToleranceNewFee()
        {
            AddFees(
                archivedClosingCostSet,
                NewTridFee(DefaultSystemClosingCostFee.Hud800AppraisalFeeFeeTypeId, 10.00M, isThirdParty: true, beneficiary: E_AgentRoleT.Appraiser, canShop: true)
            );

            AddFees(
                liveClosingCostSet,
                NewTridFee(DefaultSystemClosingCostFee.Hud800AppraisalFeeFeeTypeId, 5.00M, isThirdParty: true, beneficiary: E_AgentRoleT.Appraiser, canShop: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud1300PestInspectionFeeTypeId, 10.00M, isThirdParty: true, beneficiary: E_AgentRoleT.PestInspection, canShop: true)
            );

            decimal cure =
                tolCalc.GetTenPercentToleranceCure(
                    archivedClosingCostSet, feeFilter,
                    liveClosingCostSet, feeFilter,
                    E_sDisclosureRegulationT.TRID, E_sClosingCostFeeVersionT.ClosingCostFee2015);

            Assert.AreEqual(4.00M, cure);
        }

        [Test]
        public void TridTenToleranceNewFees()
        {
            AddFees(
                archivedClosingCostSet,
                NewTridFee(DefaultSystemClosingCostFee.Hud800AppraisalFeeFeeTypeId, 10.00M, isThirdParty: true, beneficiary: E_AgentRoleT.Appraiser, canShop: true)
            );

            AddFees(
                liveClosingCostSet,
                NewTridFee(DefaultSystemClosingCostFee.Hud1100DocPreparationFeeTypeId, 5.00M, isThirdParty: true, beneficiary: E_AgentRoleT.Title, canShop: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud1300PestInspectionFeeTypeId, 10.00M, isThirdParty: true, beneficiary: E_AgentRoleT.PestInspection, canShop: true)
            );

            decimal cure =
                tolCalc.GetTenPercentToleranceCure(
                    archivedClosingCostSet, feeFilter,
                    liveClosingCostSet, feeFilter,
                    E_sDisclosureRegulationT.TRID, E_sClosingCostFeeVersionT.ClosingCostFee2015);

            Assert.AreEqual(15.00M, cure);
        }

        [Test]
        public void TridTenToleranceRemovedFee()
        {
            AddFees(
                archivedClosingCostSet,
                NewTridFee(DefaultSystemClosingCostFee.Hud800AppraisalFeeFeeTypeId, 10.00M, isThirdParty: true, beneficiary: E_AgentRoleT.Appraiser, canShop: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud1300PestInspectionFeeTypeId, 100.00M, isThirdParty: true, beneficiary: E_AgentRoleT.PestInspection, canShop: true)
            );

            AddFees(
                liveClosingCostSet,
                NewTridFee(DefaultSystemClosingCostFee.Hud800AppraisalFeeFeeTypeId, 30.00M, isThirdParty: true, beneficiary: E_AgentRoleT.Appraiser, canShop: true)
            );

            decimal cure =
                tolCalc.GetTenPercentToleranceCure(
                    archivedClosingCostSet, feeFilter,
                    liveClosingCostSet, feeFilter,
                    E_sDisclosureRegulationT.TRID, E_sClosingCostFeeVersionT.ClosingCostFee2015);

            Assert.AreEqual(19.00M, cure);
        }

        [Test]
        public void TridTenToleranceChangingRecFees()
        {
            AddFees(
                archivedClosingCostSet,
                NewTridFee(DefaultSystemClosingCostFee.Hud1200DeedFeeTypeId, 10.00M, isThirdParty: true, beneficiary: E_AgentRoleT.Other, mismoFeeT: E_ClosingCostFeeMismoFeeT.DeedRecordingFee),
                NewTridFee(DefaultSystemClosingCostFee.Hud1200MortgageFeeTypeId, 10.00M, isThirdParty: true, beneficiary: E_AgentRoleT.Other, mismoFeeT: E_ClosingCostFeeMismoFeeT.MortgageRecordingFee)
            );

            AddFees(
                liveClosingCostSet,
                NewTridFee(DefaultSystemClosingCostFee.Hud1200ReleaseFeeTypeId, 30.00M, isThirdParty: true, beneficiary: E_AgentRoleT.Other, mismoFeeT: E_ClosingCostFeeMismoFeeT.ReleaseRecordingFee)
            );

            decimal cure =
                tolCalc.GetTenPercentToleranceCure(
                    archivedClosingCostSet, feeFilter,
                    liveClosingCostSet, feeFilter,
                    E_sDisclosureRegulationT.TRID, E_sClosingCostFeeVersionT.ClosingCostFee2015);

            Assert.AreEqual(8.00M, cure);
        }

        [Test]
        public void TridTenToleranceNewRecFee()
        {
            AddFees(
                archivedClosingCostSet,
                NewTridFee(DefaultSystemClosingCostFee.Hud1200DeedFeeTypeId, 10.00M, isThirdParty: true, beneficiary: E_AgentRoleT.Other)
            );

            AddFees(
                liveClosingCostSet,
                NewTridFee(DefaultSystemClosingCostFee.Hud1200DeedFeeTypeId, 5.00M, isThirdParty: true, beneficiary: E_AgentRoleT.Other),
                NewTridFee(DefaultSystemClosingCostFee.Hud1200MortgageFeeTypeId, 10.00M, isThirdParty: true, beneficiary: E_AgentRoleT.Other)
            );

            decimal cure =
                tolCalc.GetTenPercentToleranceCure(
                    archivedClosingCostSet, feeFilter,
                    liveClosingCostSet, feeFilter,
                    E_sDisclosureRegulationT.TRID, E_sClosingCostFeeVersionT.ClosingCostFee2015);

            Assert.AreEqual(4.00M, cure);
        }

        [Test]
        public void TridTenToleranceMixedDroppedAllRecFees()
        {
            AddFees(
                archivedClosingCostSet,
                NewTridFee(DefaultSystemClosingCostFee.Hud1200DeedFeeTypeId, 10.00M, isThirdParty: true, beneficiary: E_AgentRoleT.Other),
                NewTridFee(DefaultSystemClosingCostFee.Hud1200MortgageFeeTypeId, 20.00M, isThirdParty: true, beneficiary: E_AgentRoleT.Other),
                NewTridFee(DefaultSystemClosingCostFee.Hud800AppraisalFeeFeeTypeId, 40.00M, isThirdParty: true, beneficiary: E_AgentRoleT.Appraiser, canShop: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud1300PestInspectionFeeTypeId, 100.00M, isThirdParty: true, beneficiary: E_AgentRoleT.PestInspection, canShop: true)
            );

            AddFees(
                liveClosingCostSet,
                NewTridFee(DefaultSystemClosingCostFee.Hud800AppraisalFeeFeeTypeId, 70.00M, isThirdParty: true, beneficiary: E_AgentRoleT.Appraiser, canShop: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud1300PestInspectionFeeTypeId, 100.00M, isThirdParty: true, beneficiary: E_AgentRoleT.PestInspection, canShop: true)
            );

            decimal cure =
                tolCalc.GetTenPercentToleranceCure(
                    archivedClosingCostSet, feeFilter,
                    liveClosingCostSet, feeFilter,
                    E_sDisclosureRegulationT.TRID, E_sClosingCostFeeVersionT.ClosingCostFee2015);

            Assert.AreEqual(16.00M, cure);
        }

        [Test]
        public void TridTenToleranceMixedDroppedOneRecFee()
        {
            AddFees(
                archivedClosingCostSet,
                NewTridFee(DefaultSystemClosingCostFee.Hud1200DeedFeeTypeId, 10.00M, isThirdParty: true, beneficiary: E_AgentRoleT.Other),
                NewTridFee(DefaultSystemClosingCostFee.Hud1200MortgageFeeTypeId, 20.00M, isThirdParty: true, beneficiary: E_AgentRoleT.Other),
                NewTridFee(DefaultSystemClosingCostFee.Hud800AppraisalFeeFeeTypeId, 40.00M, isThirdParty: true, beneficiary: E_AgentRoleT.Appraiser, canShop: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud1300PestInspectionFeeTypeId, 100.00M, isThirdParty: true, beneficiary: E_AgentRoleT.PestInspection, canShop: true)
            );

            AddFees(
                liveClosingCostSet,
                NewTridFee(DefaultSystemClosingCostFee.Hud1200DeedFeeTypeId, 30.00M, isThirdParty: true, beneficiary: E_AgentRoleT.Other),
                NewTridFee(DefaultSystemClosingCostFee.Hud1300PestInspectionFeeTypeId, 140.00M, isThirdParty: true, beneficiary: E_AgentRoleT.PestInspection, canShop: true)
            );

            decimal cure =
                tolCalc.GetTenPercentToleranceCure(
                    archivedClosingCostSet, feeFilter,
                    liveClosingCostSet, feeFilter,
                    E_sDisclosureRegulationT.TRID, E_sClosingCostFeeVersionT.ClosingCostFee2015);

            Assert.AreEqual(27.00M, cure);
        }

        [Test]
        public void TridTenToleranceMixedZeroedOneRecFee()
        {
            AddFees(
                archivedClosingCostSet,
                NewTridFee(DefaultSystemClosingCostFee.Hud1200DeedFeeTypeId, 10.00M, isThirdParty: true, beneficiary: E_AgentRoleT.Other),
                NewTridFee(DefaultSystemClosingCostFee.Hud1200MortgageFeeTypeId, 20.00M, isThirdParty: true, beneficiary: E_AgentRoleT.Other),
                NewTridFee(DefaultSystemClosingCostFee.Hud800AppraisalFeeFeeTypeId, 40.00M, isThirdParty: true, beneficiary: E_AgentRoleT.Appraiser, canShop: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud1300PestInspectionFeeTypeId, 100.00M, isThirdParty: true, beneficiary: E_AgentRoleT.PestInspection, canShop: true)
            );

            AddFees(
                liveClosingCostSet,
                NewTridFee(DefaultSystemClosingCostFee.Hud1200DeedFeeTypeId, 30.00M, isThirdParty: true, beneficiary: E_AgentRoleT.Other),
                NewTridFee(DefaultSystemClosingCostFee.Hud1200MortgageFeeTypeId, 0.00M, isThirdParty: true, beneficiary: E_AgentRoleT.Other),
                NewTridFee(DefaultSystemClosingCostFee.Hud800AppraisalFeeFeeTypeId, 0.00M, isThirdParty: true, beneficiary: E_AgentRoleT.Appraiser, canShop: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud1300PestInspectionFeeTypeId, 140.00M, isThirdParty: true, beneficiary: E_AgentRoleT.PestInspection, canShop: true)
            );

            decimal cure =
                tolCalc.GetTenPercentToleranceCure(
                    archivedClosingCostSet, feeFilter,
                    liveClosingCostSet, feeFilter,
                    E_sDisclosureRegulationT.TRID, E_sClosingCostFeeVersionT.ClosingCostFee2015);

            Assert.AreEqual(27.00M, cure);
        }

        [Test]
        public void TridToleranceCureTest1()
        {
            AddFees(
                archivedClosingCostSet,
                NewTridFee(DefaultSystemClosingCostFee.Hud1000HazardInsuranceReserveFeeTypeId, 1.00M, beneficiary: E_AgentRoleT.Other, isThirdParty: false),
                NewTridFee(DefaultSystemClosingCostFee.Hud1000FloodInsuranceReserveFeeTypeId, 2.00M, beneficiary: E_AgentRoleT.Other, isThirdParty: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud1000WindStormInsuranceFeeTypeId, 4.00M, beneficiary: E_AgentRoleT.Lender, isThirdParty: false),
                NewTridFee(DefaultSystemClosingCostFee.Hud1000CondoHo6InsuranceFeeTypeId, 10.00M, beneficiary: E_AgentRoleT.Lender, isThirdParty: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud1000TaxReserveFeeTypeId, 20.00M, beneficiary: E_AgentRoleT.Other, isThirdParty: true, isAffiliate: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud1000SchoolTaxFeeTypeId, 40.00M, beneficiary: E_AgentRoleT.Lender, isThirdParty: true, isAffiliate: true),
                
                NewTridFee(DefaultSystemClosingCostFee.Hud1200Custom1FeeTypeId, 111.11M, mismoFeeT: E_ClosingCostFeeMismoFeeT.AssignmentRecordingFee, beneficiary: E_AgentRoleT.Other, isThirdParty: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud1200Custom2FeeTypeId, 222.22M, mismoFeeT: E_ClosingCostFeeMismoFeeT.DeedRecordingFee, beneficiary: E_AgentRoleT.Lender, isThirdParty: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud1200DeedFeeTypeId, 444.44M, beneficiary: E_AgentRoleT.Lender, isThirdParty: false),
                NewTridFee(DefaultSystemClosingCostFee.Hud1200MortgageFeeTypeId, 1111.11M, beneficiary: E_AgentRoleT.Other, isThirdParty: false),
                NewTridFee(DefaultSystemClosingCostFee.Hud1200Custom3FeeTypeId, 2222.22M, mismoFeeT: E_ClosingCostFeeMismoFeeT.MunicipalLienCertificateRecordingFee, beneficiary: E_AgentRoleT.Other, isThirdParty: true, isAffiliate: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud1200ReleaseFeeTypeId, 4444.44M, beneficiary: E_AgentRoleT.Lender, isThirdParty: true, isAffiliate: true),

                NewTridFee(DefaultSystemClosingCostFee.Hud1200StateTaxStampsFeeTypeId, 10000.00M, beneficiary: E_AgentRoleT.Other, isThirdParty: false)
            );

            AddFees(
                liveClosingCostSet,
                NewTridFee(DefaultSystemClosingCostFee.Hud1000HazardInsuranceReserveFeeTypeId, 2.00M, beneficiary: E_AgentRoleT.Other, isThirdParty: false),
                NewTridFee(DefaultSystemClosingCostFee.Hud1000FloodInsuranceReserveFeeTypeId, 4.00M, beneficiary: E_AgentRoleT.Other, isThirdParty: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud1000WindStormInsuranceFeeTypeId, 8.00M, beneficiary: E_AgentRoleT.Lender, isThirdParty: false),
                NewTridFee(DefaultSystemClosingCostFee.Hud1000CondoHo6InsuranceFeeTypeId, 20.00M, beneficiary: E_AgentRoleT.Lender, isThirdParty: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud1000TaxReserveFeeTypeId, 40.00M, beneficiary: E_AgentRoleT.Other, isThirdParty: true, isAffiliate: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud1000SchoolTaxFeeTypeId, 80.00M, beneficiary: E_AgentRoleT.Lender, isThirdParty: true, isAffiliate: true),

                NewTridFee(DefaultSystemClosingCostFee.Hud1200Custom1FeeTypeId, 222.22M, mismoFeeT: E_ClosingCostFeeMismoFeeT.AssignmentRecordingFee, beneficiary: E_AgentRoleT.Other, isThirdParty: false),
                NewTridFee(DefaultSystemClosingCostFee.Hud1200Custom2FeeTypeId, 444.44M, mismoFeeT: E_ClosingCostFeeMismoFeeT.DeedRecordingFee, beneficiary: E_AgentRoleT.Other, isThirdParty: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud1200DeedFeeTypeId, 888.88M, beneficiary: E_AgentRoleT.Lender, isThirdParty: false),
                NewTridFee(DefaultSystemClosingCostFee.Hud1200MortgageFeeTypeId, 2222.22M, beneficiary: E_AgentRoleT.Lender, isThirdParty: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud1200Custom3FeeTypeId, 4444.44M, mismoFeeT: E_ClosingCostFeeMismoFeeT.MunicipalLienCertificateRecordingFee, beneficiary: E_AgentRoleT.Other, isThirdParty: true, isAffiliate: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud1200ReleaseFeeTypeId, 8888.88M, beneficiary: E_AgentRoleT.Lender, isThirdParty: true, isAffiliate: true),

                NewTridFee(DefaultSystemClosingCostFee.Hud1200StateTaxStampsFeeTypeId, 20000.00M, beneficiary: E_AgentRoleT.Other, isThirdParty: false)
            );

            decimal zeroCure =
                 tolCalc.GetZeroPercentToleranceCure(
                    archivedClosingCostSet, feeFilter,
                    liveClosingCostSet, feeFilter,
                    E_sDisclosureRegulationT.TRID, E_sClosingCostFeeVersionT.ClosingCostFee2015, 0, 0);

            decimal tenCure =
                tolCalc.GetTenPercentToleranceCure(
                        archivedClosingCostSet, feeFilter,
                        liveClosingCostSet, feeFilter,
                        E_sDisclosureRegulationT.TRID, E_sClosingCostFeeVersionT.ClosingCostFee2015);

            Assert.AreEqual(10000.00M, zeroCure);
            decimal tenTarget = 7700.00M;
            Assert.GreaterOrEqual(tenCure, tenTarget - 0.06M);
            Assert.LessOrEqual(tenCure, tenTarget + 0.06M);
        }

        [Test]
        public void TridToleranceCureTest2()
        {
            AddFees(
                archivedClosingCostSet,
                NewTridFee(DefaultSystemClosingCostFee.Hud1200CountyTaxStampsFeeTypeId, 1.00M, beneficiary: E_AgentRoleT.Other, isThirdParty: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud1200Custom1FeeTypeId, 2.00M, mismoFeeT: E_ClosingCostFeeMismoFeeT.CountyDeedTaxStampFee, beneficiary: E_AgentRoleT.Other, isThirdParty: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud1200Custom2FeeTypeId, 4.00M, mismoFeeT: E_ClosingCostFeeMismoFeeT.DocumentaryStampFee, beneficiary: E_AgentRoleT.Lender, isThirdParty: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud1200Custom3FeeTypeId, 10.00M, mismoFeeT: E_ClosingCostFeeMismoFeeT.DocumentaryStampFee, beneficiary: E_AgentRoleT.Lender, isThirdParty: true, isAffiliate: false),
                NewTridFee(DefaultSystemClosingCostFee.Hud1200StateTaxStampsFeeTypeId, 20.00M, beneficiary: E_AgentRoleT.Lender, isThirdParty: true, isAffiliate: true),

                NewTridFee(DefaultSystemClosingCostFee.Hud800Custom1FeeTypeId, 40.00M, mismoFeeT: E_ClosingCostFeeMismoFeeT.DryWallInspectionFee, beneficiary: E_AgentRoleT.Other, isThirdParty: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud800LenderInspectionFeeTypeId, 100.00M, beneficiary: E_AgentRoleT.Other, isThirdParty: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud800MortgageBrokerFeeTypeId, 200.00M, beneficiary: E_AgentRoleT.Broker, isThirdParty: false),
                NewTridFee(DefaultSystemClosingCostFee.Hud800ProcessingFeeTypeId, 400.00M, beneficiary: E_AgentRoleT.Other, isThirdParty: false),
                NewTridFee(DefaultSystemClosingCostFee.Hud800UnderwritingFeeTypeId, 1000.00M, beneficiary: E_AgentRoleT.Other, isThirdParty: true, isAffiliate: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud800WireTransferFeeTypeId, 2000.00M, beneficiary: E_AgentRoleT.Lender, isThirdParty: true, isAffiliate: true),

                NewTridFee(DefaultSystemClosingCostFee.Hud1100Custom1FeeTypeId, 4000.00M, mismoFeeT: E_ClosingCostFeeMismoFeeT.TitleEndorsementFee, beneficiary: E_AgentRoleT.Other, isThirdParty: false, canShop: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud1100Custom2FeeTypeId, 11111.11M, mismoFeeT: E_ClosingCostFeeMismoFeeT.TitleFinalPolicyShortFormFee, beneficiary: E_AgentRoleT.Other, isThirdParty: true, canShop: true)
            );

            AddFees(
                liveClosingCostSet,
                NewTridFee(DefaultSystemClosingCostFee.Hud1200CountyTaxStampsFeeTypeId, 2.00M, beneficiary: E_AgentRoleT.Other, isThirdParty: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud1200Custom1FeeTypeId, 4.00M, mismoFeeT: E_ClosingCostFeeMismoFeeT.CountyDeedTaxStampFee, beneficiary: E_AgentRoleT.Lender, isThirdParty: false),
                NewTridFee(DefaultSystemClosingCostFee.Hud1200Custom2FeeTypeId, 8.00M, mismoFeeT: E_ClosingCostFeeMismoFeeT.DocumentaryStampFee, beneficiary: E_AgentRoleT.Lender, isThirdParty: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud1200Custom3FeeTypeId, 20.00M, mismoFeeT: E_ClosingCostFeeMismoFeeT.DocumentaryStampFee, beneficiary: E_AgentRoleT.Other, isThirdParty: true, isAffiliate: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud1200StateTaxStampsFeeTypeId, 40.00M, beneficiary: E_AgentRoleT.Lender, isThirdParty: true, isAffiliate: true),

                NewTridFee(DefaultSystemClosingCostFee.Hud800Custom1FeeTypeId, 80.00M, mismoFeeT: E_ClosingCostFeeMismoFeeT.DryWallInspectionFee, beneficiary: E_AgentRoleT.Other, isThirdParty: false),
                NewTridFee(DefaultSystemClosingCostFee.Hud800LenderInspectionFeeTypeId, 200.00M, beneficiary: E_AgentRoleT.Other, isThirdParty: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud800MortgageBrokerFeeTypeId, 400.00M, beneficiary: E_AgentRoleT.Broker, isThirdParty: false),
                NewTridFee(DefaultSystemClosingCostFee.Hud800ProcessingFeeTypeId, 800.00M, beneficiary: E_AgentRoleT.Broker, isThirdParty: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud800UnderwritingFeeTypeId, 2000.00M, beneficiary: E_AgentRoleT.Other, isThirdParty: true, isAffiliate: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud800WireTransferFeeTypeId, 4000.00M, beneficiary: E_AgentRoleT.Lender, isThirdParty: true, isAffiliate: true),

                NewTridFee(DefaultSystemClosingCostFee.Hud1100Custom1FeeTypeId, 8000.00M, mismoFeeT: E_ClosingCostFeeMismoFeeT.TitleEndorsementFee, beneficiary: E_AgentRoleT.Other, isThirdParty: false, canShop: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud1100Custom2FeeTypeId, 22222.22M, mismoFeeT: E_ClosingCostFeeMismoFeeT.TitleFinalPolicyShortFormFee, beneficiary: E_AgentRoleT.Other, isThirdParty: true, canShop: true)
            );

            decimal zeroCure =
                 tolCalc.GetZeroPercentToleranceCure(
                    archivedClosingCostSet, feeFilter,
                    liveClosingCostSet, feeFilter,
                    E_sDisclosureRegulationT.TRID, E_sClosingCostFeeVersionT.ClosingCostFee2015, 0, 0);

            decimal tenCure =
                tolCalc.GetTenPercentToleranceCure(
                        archivedClosingCostSet, feeFilter,
                        liveClosingCostSet, feeFilter,
                        E_sDisclosureRegulationT.TRID, E_sClosingCostFeeVersionT.ClosingCostFee2015);

            Assert.AreEqual(7777.00M, zeroCure);
            decimal tenTarget = 10000.00M;
            Assert.GreaterOrEqual(tenCure, tenTarget - 0.01M);
            Assert.LessOrEqual(tenCure, tenTarget + 0.01M);
        }

        [Test]
        public void TridToleranceCureTest3()
        {
            AddFees(
                archivedClosingCostSet,
                NewTridFee(DefaultSystemClosingCostFee.Hud1300Custom1FeeTypeId, 1.00M, mismoFeeT: E_ClosingCostFeeMismoFeeT.DryWallInspectionFee, beneficiary: E_AgentRoleT.Lender, isThirdParty: false, canShop: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud1300Custom2FeeTypeId, 2.00M, mismoFeeT: E_ClosingCostFeeMismoFeeT.ElectricalInspectionFee, beneficiary: E_AgentRoleT.Lender, isThirdParty: true, canShop: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud1300PestInspectionFeeTypeId, 4.00M, beneficiary: E_AgentRoleT.Other, isThirdParty: true, isAffiliate: true, canShop: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud1100Custom1FeeTypeId, 10.00M, mismoFeeT: E_ClosingCostFeeMismoFeeT.TitleNotaryFee, beneficiary: E_AgentRoleT.Lender, isThirdParty: true, isAffiliate: true, canShop: true),

                NewTridFee(DefaultSystemClosingCostFee.Hud1100Custom2FeeTypeId, 20.00M, mismoFeeT: E_ClosingCostFeeMismoFeeT.TitleServicesFeeTotal, beneficiary: E_AgentRoleT.Other, isThirdParty: false, canShop: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud1100AttorneyFeeTypeId, 40.00M, beneficiary: E_AgentRoleT.Other, isThirdParty: true, canShop: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud1100ClosingEscrowFeeTypeId, 100.00M, beneficiary: E_AgentRoleT.Broker, isThirdParty: false, canShop: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud1100DocPreparationFeeTypeId, 200.00M, beneficiary: E_AgentRoleT.Broker, isThirdParty: true, canShop: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud1100LenderTitleInsuranceFeeTypeId, 400.00M, beneficiary: E_AgentRoleT.Other, isThirdParty: true, isAffiliate: true, canShop: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud1100NotaryFeeTypeId, 1000.00M, beneficiary: E_AgentRoleT.Lender, isThirdParty: true, isAffiliate: true, canShop: true),

                NewTridFee(DefaultSystemClosingCostFee.Hud1300Custom3FeeTypeId, 2000.00M, mismoFeeT: E_ClosingCostFeeMismoFeeT.SmokeDetectorInspectionFee, beneficiary: E_AgentRoleT.Other, isThirdParty: false, isOptional: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud1300Custom4FeeTypeId, 4000.00M, mismoFeeT: E_ClosingCostFeeMismoFeeT.SepticInspectionFee, beneficiary: E_AgentRoleT.Other, isThirdParty: true, isOptional: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud1300Custom5FeeTypeId, 10000.00M, mismoFeeT: E_ClosingCostFeeMismoFeeT.StructuralInspectionFee, beneficiary: E_AgentRoleT.Lender, isThirdParty: false, isOptional: true)
            );

            AddFees(
                liveClosingCostSet,
                NewTridFee(DefaultSystemClosingCostFee.Hud1300Custom1FeeTypeId, 2.00M, mismoFeeT: E_ClosingCostFeeMismoFeeT.DryWallInspectionFee, beneficiary: E_AgentRoleT.Lender, isThirdParty: false, canShop: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud1300Custom2FeeTypeId, 4.00M, mismoFeeT: E_ClosingCostFeeMismoFeeT.ElectricalInspectionFee, beneficiary: E_AgentRoleT.Lender, isThirdParty: true, canShop: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud1300PestInspectionFeeTypeId, 8.00M, beneficiary: E_AgentRoleT.Other, isThirdParty: true, isAffiliate: true, canShop: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud1100Custom1FeeTypeId, 20.00M, mismoFeeT: E_ClosingCostFeeMismoFeeT.TitleNotaryFee, beneficiary: E_AgentRoleT.Lender, isThirdParty: true, isAffiliate: true, canShop: true),

                NewTridFee(DefaultSystemClosingCostFee.Hud1100Custom2FeeTypeId, 40.00M, mismoFeeT: E_ClosingCostFeeMismoFeeT.TitleServicesFeeTotal, beneficiary: E_AgentRoleT.Other, isThirdParty: false, canShop: true, didShop: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud1100AttorneyFeeTypeId, 80.00M, beneficiary: E_AgentRoleT.Other, isThirdParty: true, canShop: true, didShop: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud1100ClosingEscrowFeeTypeId, 200.00M, beneficiary: E_AgentRoleT.Broker, isThirdParty: false, canShop: true, didShop: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud1100DocPreparationFeeTypeId, 400.00M, beneficiary: E_AgentRoleT.Broker, isThirdParty: true, canShop: true, didShop: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud1100LenderTitleInsuranceFeeTypeId, 800.00M, beneficiary: E_AgentRoleT.Other, isThirdParty: true, isAffiliate: true, canShop: true, didShop: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud1100NotaryFeeTypeId, 2000.00M, beneficiary: E_AgentRoleT.Lender, isThirdParty: true, isAffiliate: true, canShop: true, didShop: true),

                NewTridFee(DefaultSystemClosingCostFee.Hud1300Custom3FeeTypeId, 4000.00M, mismoFeeT: E_ClosingCostFeeMismoFeeT.SmokeDetectorInspectionFee, beneficiary: E_AgentRoleT.Other, isThirdParty: false, isOptional: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud1300Custom4FeeTypeId, 8000.00M, mismoFeeT: E_ClosingCostFeeMismoFeeT.SepticInspectionFee, beneficiary: E_AgentRoleT.Other, isThirdParty: true, canShop: true, isOptional: true),
                NewTridFee(DefaultSystemClosingCostFee.Hud1300Custom5FeeTypeId, 20000.00M, mismoFeeT: E_ClosingCostFeeMismoFeeT.StructuralInspectionFee, beneficiary: E_AgentRoleT.Lender, isThirdParty: false, isOptional: true)
            );

            decimal zeroCure =
                 tolCalc.GetZeroPercentToleranceCure(
                    archivedClosingCostSet, feeFilter,
                    liveClosingCostSet, feeFilter,
                    E_sDisclosureRegulationT.TRID, E_sClosingCostFeeVersionT.ClosingCostFee2015, 0, 0);

            decimal tenCure =
                tolCalc.GetTenPercentToleranceCure(
                        archivedClosingCostSet, feeFilter,
                        liveClosingCostSet, feeFilter,
                        E_sDisclosureRegulationT.TRID, E_sClosingCostFeeVersionT.ClosingCostFee2015);

            Assert.AreEqual(17.00M, zeroCure);
            Assert.AreEqual(0.00M, tenCure);
        }
    }
}
