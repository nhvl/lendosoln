﻿namespace LendOSolnTest.Loan
{
    using System;
    using System.Collections.Generic;
    using Common;
    using DataAccess;
    using Fakes;
    using LendersOffice.Reports;
    using LendersOfficeApp.los.RatePrice;
    using NUnit.Framework;

    [TestFixture]
    public class DocumentationTypeTest
    {
        #region Test case sources
        private static object[] RequiredIncomeDocTypes = new object[]
        {
            E_sProdDocT.Alt,
            E_sProdDocT.Full,
            E_sProdDocT.Light,
            E_sProdDocT.SISA,
            E_sProdDocT.SIVA,
            E_sProdDocT.VISA,
            E_sProdDocT.VINA,
            E_sProdDocT.Streamline,
            E_sProdDocT._12MoPersonalBankStatements,
            E_sProdDocT._24MoPersonalBankStatements,
            E_sProdDocT._12MoBusinessBankStatements,
            E_sProdDocT._24MoBusinessBankStatements,
            E_sProdDocT.OtherBankStatements,
            E_sProdDocT._1YrTaxReturns,
            E_sProdDocT.AssetUtilization,
            E_sProdDocT.Voe
        };

        private static object[] NonRequiredIncomeDocTypes = new object[]
        {
            E_sProdDocT.NINA,
            E_sProdDocT.NINANE,
            E_sProdDocT.NISA,
            E_sProdDocT.NIVA,
            E_sProdDocT.NIVANE,
            E_sProdDocT.DebtServiceCoverage,
            E_sProdDocT.NoIncome
        };

        private static object[] sProdDocT_rep_Always_ReturnsExpectedFriendlyValue_Source = new[]
        {
            new object[] { E_sProdDocT.Full, "Full Document" },
            new object[] { E_sProdDocT.Alt, "Alt - 12 months bank stmts" },
            new object[] { E_sProdDocT.Light, "Lite - 6 months bank stmts" },
            new object[] { E_sProdDocT.SIVA, "NIV (SIVA) - Stated Income, Verified Assets" },
            new object[] { E_sProdDocT.VISA, "VISA - Verified Income, Stated Assets" },
            new object[] { E_sProdDocT.SISA, "NIV (SISA) - Stated Income, Stated Assets" },
            new object[] { E_sProdDocT.NIVA, "No Ratio - No Income, Verified Assets" },
            new object[] { E_sProdDocT.NINA, "NINA - No Income, No Assets" },
            new object[] { E_sProdDocT.NISA, "NISA - No Income, Stated Assets" },
            new object[] { E_sProdDocT.NINANE, "No Doc - No Income, No Assets, No Empl" },
            new object[] { E_sProdDocT.NIVANE, "NoDoc Verif Assets - No Income, Verified Assets, No Empl" },
            new object[] { E_sProdDocT.VINA, "VINA - Verified Income, No Assets" },
            new object[] { E_sProdDocT.Streamline, "Streamline" },
            new object[] { E_sProdDocT._12MoPersonalBankStatements, "12 Mo. Personal Bank Statements" },
            new object[] { E_sProdDocT._24MoPersonalBankStatements, "24 Mo. Personal Bank Statements" },
            new object[] { E_sProdDocT._12MoBusinessBankStatements, "12 Mo. Business Bank Statements" },
            new object[] { E_sProdDocT._24MoBusinessBankStatements, "24 Mo. Business Bank Statements" },
            new object[] { E_sProdDocT.OtherBankStatements, "Other Bank Statements" },
            new object[] { E_sProdDocT._1YrTaxReturns, "1 Yr. Tax Returns" },
            new object[] { E_sProdDocT.AssetUtilization, "Asset Utilization" },
            new object[] { E_sProdDocT.DebtServiceCoverage, "Debt Service Coverage (DSCR)" },
            new object[] { E_sProdDocT.NoIncome, "No Ratio" },
            new object[] { E_sProdDocT.Voe, "VOE" }
        };

        private static object[] UpdateProdDocTDependencies_Always_UpdatesProdAssetT_Source = new[]
        {
            new object[] { E_sProdDocT.Full, E_sProdAssetT.FullDoc },
            new object[] { E_sProdDocT.Alt, E_sProdAssetT.FullDoc },
            new object[] { E_sProdDocT.Light, E_sProdAssetT.FullDoc },
            new object[] { E_sProdDocT.Streamline, E_sProdAssetT.FullDoc },
            new object[] { E_sProdDocT.SISA, E_sProdAssetT.Stated },
            new object[] { E_sProdDocT.NISA, E_sProdAssetT.Stated },
            new object[] { E_sProdDocT.VISA, E_sProdAssetT.Stated },
            new object[] { E_sProdDocT.SIVA, E_sProdAssetT.Verify },
            new object[] { E_sProdDocT.NIVA, E_sProdAssetT.Verify },
            new object[] { E_sProdDocT.NIVANE, E_sProdAssetT.Verify },
            new object[] { E_sProdDocT.VINA, E_sProdAssetT.NotStated },
            new object[] { E_sProdDocT.NINANE, E_sProdAssetT.NotStated },
            new object[] { E_sProdDocT.NINA, E_sProdAssetT.NotStated }
        };

        private static object[] UpdateProdDocTDependencies_Always_UpdatesProdProdIT_Source = new[]
        {
            new object[] { E_sProdDocT.Full, E_sProdIT.FullDoc },
            new object[] { E_sProdDocT.Alt, E_sProdIT.FullDoc },
            new object[] { E_sProdDocT.Light, E_sProdIT.FullDoc },
            new object[] { E_sProdDocT.Streamline, E_sProdIT.FullDoc },
            new object[] { E_sProdDocT.SIVA, E_sProdIT.Stated },
            new object[] { E_sProdDocT.SISA, E_sProdIT.Stated },
            new object[] { E_sProdDocT.VISA, E_sProdIT.Verify },
            new object[] { E_sProdDocT.VINA, E_sProdIT.Verify },
            new object[] { E_sProdDocT.NIVA, E_sProdIT.NotStated },
            new object[] { E_sProdDocT.NINANE, E_sProdIT.NotStated },
            new object[] { E_sProdDocT.NINA, E_sProdIT.NotStated },
            new object[] { E_sProdDocT.NISA, E_sProdIT.NotStated },
            new object[] { E_sProdDocT.NIVANE, E_sProdIT.NotStated }
        };

        private static object[] sFannieDocT_Always_UpdatesWhenSettingsProdDocT_Source = new[]
        {
            new object[] { E_sProdDocT.Full, E_sFannieDocT.Full },
            new object[] { E_sProdDocT.Alt, E_sFannieDocT.Alternative },
            new object[] { E_sProdDocT.Light, E_sFannieDocT.LimitedDocumentation },
            new object[] { E_sProdDocT.SIVA, E_sFannieDocT.NoVerificationStatedIncome },
            new object[] { E_sProdDocT.VISA, E_sFannieDocT.NoVerificationStatedAssets },
            new object[] { E_sProdDocT.SISA, E_sFannieDocT.NoVerificationStatedIncomeAssets },
            new object[] { E_sProdDocT.NIVA, E_sFannieDocT.NoRatio },
            new object[] { E_sProdDocT.NINA, E_sFannieDocT.NoIncomeNoAssets },
            new object[] { E_sProdDocT.NISA, E_sFannieDocT.LeaveBlank },
            new object[] { E_sProdDocT.NINANE, E_sFannieDocT.NoDocumentation },
            new object[] { E_sProdDocT.NIVANE, E_sFannieDocT.NoIncomeNoEmployment },
            new object[] { E_sProdDocT.VINA, E_sFannieDocT.NoAssets },
            new object[] { E_sProdDocT._12MoPersonalBankStatements, E_sFannieDocT.Alternative },
            new object[] { E_sProdDocT._24MoPersonalBankStatements, E_sFannieDocT.Alternative },
            new object[] { E_sProdDocT._12MoBusinessBankStatements, E_sFannieDocT.Alternative },
            new object[] { E_sProdDocT._24MoBusinessBankStatements, E_sFannieDocT.Alternative },
            new object[] { E_sProdDocT.OtherBankStatements, E_sFannieDocT.Alternative },
            new object[] { E_sProdDocT._1YrTaxReturns, E_sFannieDocT.Alternative },
            new object[] { E_sProdDocT.Voe, E_sFannieDocT.Alternative },
            new object[] { E_sProdDocT.AssetUtilization, E_sFannieDocT.NoRatio },
            new object[] { E_sProdDocT.DebtServiceCoverage, E_sFannieDocT.NoRatio },
            new object[] { E_sProdDocT.NoIncome, E_sFannieDocT.NoDocumentation }
        };
        #endregion

        [TestFixtureSetUp]
        public void Setup()
        {
            LoginTools.LoginAs(TestAccountType.LoTest001);
        }

        [Test]
        [TestCaseSource(nameof(RequiredIncomeDocTypes))]
        public void sIsDocTypeRequireIncome_Always_ReturnsTrueForRequiredIncomeDocTypes(E_sProdDocT documentationType)
        {
            var loan = this.CreatePageData();
            loan.sProdDocT = documentationType;
            Assert.IsTrue(loan.sIsDocTypeRequireIncome);
        }

        [Test]
        [TestCaseSource(nameof(NonRequiredIncomeDocTypes))]
        public void sIsDocTypeRequireIncome_Always_ReturnsFalseForNonRequiredIncomeDocTypes(E_sProdDocT documentationType)
        {
            var loan = this.CreatePageData();
            loan.sProdDocT = documentationType;
            Assert.IsFalse(loan.sIsDocTypeRequireIncome);
        }

        [Test]
        [TestCaseSource(nameof(sProdDocT_rep_Always_ReturnsExpectedFriendlyValue_Source))]
        public void sProdDocT_rep_Always_ReturnsExpectedFriendlyValue(E_sProdDocT documentationType, string expectedFriendlyValue)
        {
            var loan = this.CreatePageData();
            loan.sProdDocT = documentationType;
            Assert.AreEqual(expectedFriendlyValue, loan.sProdDocT_rep);
        }

        [Test]
        [TestCaseSource(nameof(UpdateProdDocTDependencies_Always_UpdatesProdAssetT_Source))]
        public void UpdateProdDocTDependencies_Always_UpdatesProdAssetT(E_sProdDocT documentationType, E_sProdAssetT expectedAssetType)
        {
            var loan = this.CreatePageData();
            loan.sProdDocT = documentationType;
            Assert.AreEqual(expectedAssetType, loan.sProdAssetT);
        }

        [Test]
        [TestCaseSource(nameof(UpdateProdDocTDependencies_Always_UpdatesProdProdIT_Source))]
        public void UpdateProdDocTDependencies_Always_UpdatesProdIT(E_sProdDocT documentationType, E_sProdIT expectedIncomeType)
        {
            var loan = this.CreatePageData();
            loan.sProdDocT = documentationType;
            Assert.AreEqual(expectedIncomeType, loan.sProdIT);
        }

        [Test]
        [TestCase(E_sProdDocT.NINA)]
        [TestCase(E_sProdDocT.NINANE)]
        [TestCase(E_sProdDocT.NISA)]
        [TestCase(E_sProdDocT.NIVA)]
        [TestCase(E_sProdDocT.NIVANE)]
        [TestCase(E_sProdDocT.AssetUtilization)]
        [TestCase(E_sProdDocT.DebtServiceCoverage)]
        [TestCase(E_sProdDocT.NoIncome)]
        public void sQualTopR_Always_ReturnsNegativeOneForNoIncomeDocTypes(E_sProdDocT docType)
        {
            var loan = this.CreatePageData();
            loan.CalcModeT = E_CalcModeT.PriceMyLoan;

            loan.sProdDocT = docType;

            Assert.AreEqual(-1, loan.sQualTopR);
        }

        [Test]
        [TestCase(E_sProdDocT.NINA)]
        [TestCase(E_sProdDocT.NINANE)]
        [TestCase(E_sProdDocT.NISA)]
        [TestCase(E_sProdDocT.NIVA)]
        [TestCase(E_sProdDocT.NIVANE)]
        [TestCase(E_sProdDocT.AssetUtilization)]
        [TestCase(E_sProdDocT.DebtServiceCoverage)]
        [TestCase(E_sProdDocT.NoIncome)]
        public void sQualBottomR_Always_ReturnsNegativeOneForNoIncomeDocTypes(E_sProdDocT docType)
        {
            var loan = this.CreatePageData();
            loan.CalcModeT = E_CalcModeT.PriceMyLoan;

            loan.sProdDocT = docType;

            Assert.AreEqual(-1, loan.sQualBottomR);
        }

        [Test]
        [TestCaseSource(nameof(NonRequiredIncomeDocTypes))]
        public void sRespa6D_NoIncomeDocType_RemovesIncomeCollectedDateForApprValGreaterThanZero(E_sProdDocT docType)
        {
            var loan = this.CreatePageData();
            var firstCollectedDate = DateTime.Parse("9/1/2018");

            loan.sRespaIncomeCollectedD = CDateTime.Create(firstCollectedDate);
            loan.sRespaBorrowerNameCollectedD = CDateTime.Create(firstCollectedDate.AddDays(1));
            loan.sRespaBorrowerSsnCollectedD = CDateTime.Create(firstCollectedDate.AddDays(2));
            loan.sRespaPropAddressCollectedD = CDateTime.Create(firstCollectedDate.AddDays(3));
            loan.sRespaPropValueCollectedD = CDateTime.Create(firstCollectedDate.AddDays(4));
            loan.sRespaLoanAmountCollectedD = CDateTime.Create(firstCollectedDate.AddDays(5));

            loan.sApprVal = 100000M;
            loan.sProdDocT = docType;

            Assert.AreEqual(loan.sRespaBorrowerNameCollectedD, loan.sRespa6D);
        }

        [Test]
        [TestCaseSource(nameof(RequiredIncomeDocTypes))]
        public void sRespa6D_IncomeDocType_DoesNotRemoveIncomeCollectedDateForApprValGreaterThanZero(E_sProdDocT docType)
        {
            var loan = this.CreatePageData();
            var firstCollectedDate = DateTime.Parse("9/1/2018");

            loan.sRespaIncomeCollectedD = CDateTime.Create(firstCollectedDate);
            loan.sRespaBorrowerNameCollectedD = CDateTime.Create(firstCollectedDate.AddDays(1));
            loan.sRespaBorrowerSsnCollectedD = CDateTime.Create(firstCollectedDate.AddDays(2));
            loan.sRespaPropAddressCollectedD = CDateTime.Create(firstCollectedDate.AddDays(3));
            loan.sRespaPropValueCollectedD = CDateTime.Create(firstCollectedDate.AddDays(4));
            loan.sRespaLoanAmountCollectedD = CDateTime.Create(firstCollectedDate.AddDays(5));

            loan.sApprVal = 100000M;
            loan.sProdDocT = docType;

            Assert.AreEqual(loan.sRespaIncomeCollectedD, loan.sRespa6D);
        }

        [Test]
        [TestCase(E_sProdDocT.NINA)]
        [TestCase(E_sProdDocT.NINANE)]
        public void Confirm1stLienLoanTentativelyAccepted_Always_UpdatesProdAvailReserveMonthsWithNINADocType(E_sProdDocT docType)
        {
            var loan = this.CreatePageData();
            loan.sProdAvailReserveMonths = 60;
            loan.sProdDocT = docType;

            loan.Confirm1stLienLoanTentativelyAccepted(
                pmtAmtOf2ndLoan_rep: loan.m_convertLos.ToMoneyString(50000M, FormatDirection.ToRep),
                finMethT2ndLien: E_sFinMethT.Fixed,
                isIOnly2ndLien: false,
                isRequestingRateLock: false);

            Assert.AreEqual(-1, loan.sProdAvailReserveMonths);
        }

        [Test]
        [TestCase(E_sProdDocT.NINA)]
        [TestCase(E_sProdDocT.NINANE)]
        [TestCase(E_sProdDocT.NISA)]
        [TestCase(E_sProdDocT.NIVA)]
        [TestCase(E_sProdDocT.NIVANE)]
        [TestCase(E_sProdDocT.AssetUtilization)]
        [TestCase(E_sProdDocT.DebtServiceCoverage)]
        [TestCase(E_sProdDocT.NoIncome)]
        public void Confirm1stLienLoanTentativelyAccepted_Always_UpdatesPrimAppTotNonspIPeWithNoIncomeDocType(E_sProdDocT docType)
        {
            var loan = this.CreatePageData();
            loan.sPrimAppTotNonspIPe = 60M;
            loan.sProdDocT = docType;

            loan.Confirm1stLienLoanTentativelyAccepted(
                pmtAmtOf2ndLoan_rep: loan.m_convertLos.ToMoneyString(50000M, FormatDirection.ToRep),
                finMethT2ndLien: E_sFinMethT.Fixed,
                isIOnly2ndLien: false,
                isRequestingRateLock: false);

            Assert.AreEqual(0, loan.sPrimAppTotNonspIPe);
        }

        [Test]
        [TestCaseSource(nameof(sFannieDocT_Always_UpdatesWhenSettingsProdDocT_Source))]
        public void sFannieDocT_Always_UpdatesWhenSettingsProdDocT(E_sProdDocT docType, E_sFannieDocT expectedFannieDocType)
        {
            var loan = this.CreatePageData();
            loan.sProdDocT = docType;
            Assert.AreEqual(expectedFannieDocType, loan.sFannieDocT);
        }

        [Test]
        public void CSymbolTable_Entries_StoresAllDocTypeEntries()
        {
            var loan = this.CreatePageData();
            var table = new CSymbolTable(null);

            var expectedResults = new List<string>
            {
                "DocType_Full_" + E_sProdDocT.Full.ToString("D"),
                "DocType_Alt_" + E_sProdDocT.Alt.ToString("D"),
                "DocType_Light_" + E_sProdDocT.Light.ToString("D"),
                "DocType_SIVA_" + E_sProdDocT.SIVA.ToString("D"),
                "DocType_VISA_" + E_sProdDocT.VISA.ToString("D"),
                "DocType_SISA_" + E_sProdDocT.SISA.ToString("D"),
                "DocType_NIVA_" + E_sProdDocT.NIVA.ToString("D"),
                "DocType_NINA_" + E_sProdDocT.NINA.ToString("D"),
                "DocType_NINANE_" + E_sProdDocT.NINANE.ToString("D"),
                "DocType_NISA_" + E_sProdDocT.NISA.ToString("D"),
                "DocType_NIVANE_" + E_sProdDocT.NIVANE.ToString("D"),
                "DocType_VINA_" + E_sProdDocT.VINA.ToString("D"),
                "DocType_Streamline_" + E_sProdDocT.Streamline.ToString("D"),
                "DocType_12MoPersonalBankStatements_" + E_sProdDocT._12MoPersonalBankStatements.ToString("D"),
                "DocType_24MoPersonalBankStatements_" + E_sProdDocT._24MoPersonalBankStatements.ToString("D"),
                "DocType_12MoBusinessBankStatements_" + E_sProdDocT._12MoBusinessBankStatements.ToString("D"),
                "DocType_24MoBusinessBankStatements_" + E_sProdDocT._24MoBusinessBankStatements.ToString("D"),
                "DocType_OtherBankStatements_" + E_sProdDocT.OtherBankStatements.ToString("D"),
                "DocType_1YrTaxReturns_" + E_sProdDocT._1YrTaxReturns.ToString("D"),
                "DocType_AssetUtilization_" + E_sProdDocT.AssetUtilization.ToString("D"),
                "DocType_DebtServiceCoverage_" + E_sProdDocT.DebtServiceCoverage.ToString("D"),
                "DocType_NoIncome_" + E_sProdDocT.NoIncome.ToString("D"),
                "DocType_Voe_" + E_sProdDocT.Voe.ToString("D")
            };

            foreach (var expectedResult in expectedResults)
            {
                var entry = table.Entries[expectedResult];
                Assert.IsNotNull(entry);
            }
        }

        [Test]
        public void LoanReporting_Schema_StoresAllDocTypeEntries()
        {
            var expectedValues = new Dictionary<E_sProdDocT, string>
            {
                [E_sProdDocT.Full] = "Full Document",
                [E_sProdDocT.Alt] = "Alt",
                [E_sProdDocT.Light] = "Lite",
                [E_sProdDocT.NINA] = "NINA",
                [E_sProdDocT.NIVA] = "No Ratio - No Income, Verified Assets",
                [E_sProdDocT.NISA] = "NISA",
                [E_sProdDocT.SISA] = "NIV (SISA)",
                [E_sProdDocT.SIVA] = "NIV (SIVA)",
                [E_sProdDocT.VISA] = "VISA",
                [E_sProdDocT.NINANE] = "No Doc",
                [E_sProdDocT.NIVANE] = "No Doc Verif Assets",
                [E_sProdDocT.VINA] = "VINA",
                [E_sProdDocT.Streamline] = "Streamline",
                [E_sProdDocT._12MoPersonalBankStatements] = "12 Mo. Personal Bank Statements",
                [E_sProdDocT._24MoPersonalBankStatements] = "24 Mo. Personal Bank Statements",
                [E_sProdDocT._12MoBusinessBankStatements] = "12 Mo. Business Bank Statements",
                [E_sProdDocT._24MoBusinessBankStatements] = "24 Mo. Business Bank Statements",
                [E_sProdDocT.OtherBankStatements] = "Other Bank Statements",
                [E_sProdDocT._1YrTaxReturns] = "1 Yr. Tax Returns",
                [E_sProdDocT.AssetUtilization] = "Asset Utilization",
                [E_sProdDocT.DebtServiceCoverage] = "Debt Service Coverage (DSCR)",
                [E_sProdDocT.NoIncome] = "No Ratio",
                [E_sProdDocT.Voe] = "VOE"
            };

            var schema = LoanReporting.Schema;
            var entry = schema.LookupById("sProdDocT");

            Assert.IsNotNull(entry);

            foreach (var expectedValuePair in expectedValues)
            {
                var actualValue = entry.Mapping.ByKey[expectedValuePair.Key.ToString("D")];
                Assert.AreEqual(expectedValuePair.Value, actualValue);
            }
        }

        private CPageData CreatePageData()
        {
            var loan = FakePageData.Create();
            loan.InitLoad();
            return loan;
        }
    }
}
