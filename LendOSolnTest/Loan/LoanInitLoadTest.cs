﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LendOSolnTest.Loan
{
    using System;
    using DataAccess;
    using LendersOffice.Audit;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.Task;
    using LendersOffice.Security;
    using LendOSolnTest.Common;
    using NUnit.Framework;

    class LoanInitLoadTest
    {
        AbstractUserPrincipal x_principal = null;

        private AbstractUserPrincipal m_currentPrincipal
        {
            get
            {
                if (null == x_principal)
                {
                    x_principal = LoginTools.LoginAs(TestAccountType.LoTest001);
                }

                return x_principal;
            }

        }

        [Test]
        public void EmployeeFieldsTest()
        {
            // unit test for opm 471180: Remove SELECT * Employee from InitLoad() SQL statement
            // if removing is not correct, we will System.ArgumentException : ??? has not been loaded into the dictionary.
            // With ??? is some field name as UserFirstNm           

            System.Threading.Thread.CurrentPrincipal = m_currentPrincipal;

            Guid loanId = Tools.GetLoanIdByLoanName(m_currentPrincipal.BrokerId, "AUTOTEST");
            Assert.IsTrue(loanId != Guid.Empty, "Expected: having loan name AUTOTEST");

            CPageData dataLoan = CPageData.CreatePmlPageDataUsingSmartDependency(loanId, typeof(LoanInitLoadTest));
            dataLoan.InitLoad();

            CEmployeeFields[] assignedEmployees = new CEmployeeFields[] {
                                dataLoan.sEmployeeBrokerProcessor,
                                dataLoan.sEmployeeCallCenterAgent,
                                dataLoan.sEmployeeCloser,
                                dataLoan.sEmployeeCollateralAgent,
                                dataLoan.sEmployeeCreditAuditor,
                                dataLoan.sEmployeeDisclosureDesk,
                                dataLoan.sEmployeeDocDrawer,
                                dataLoan.sEmployeeExternalPostCloser,
                                dataLoan.sEmployeeExternalSecondary,
                                dataLoan.sEmployeeFunder,
                                dataLoan.sEmployeeInsuring,
                                dataLoan.sEmployeeJuniorProcessor,
                                dataLoan.sEmployeeJuniorUnderwriter,
                                dataLoan.sEmployeeLegalAuditor,
                                dataLoan.sEmployeeLenderAccExec,
                                dataLoan.sEmployeeLoanOfficerAssistant,
                                dataLoan.sEmployeeLoanOpener,
                                dataLoan.sEmployeeLoanRep,
                                dataLoan.sEmployeeLockDesk,
                                dataLoan.sEmployeeManager,
                                dataLoan.sEmployeePostCloser,
                                dataLoan.sEmployeeProcessor,
                                dataLoan.sEmployeePurchaser,
                                dataLoan.sEmployeeQCCompliance,
                                dataLoan.sEmployeeRealEstateAgent,
                                dataLoan.sEmployeeSecondary,
                                dataLoan.sEmployeeServicing,
                                dataLoan.sEmployeeShipper,
                                dataLoan.sEmployeeUnderwriter
                            };

            CEmployeeFields employee = null;
            foreach (CEmployeeFields field in assignedEmployees)
            {
                if (field.IsValid)
                {
                    employee = field;
                    break;
                }
            }

            Assert.IsNotNull(employee);
            object obj;

            // expect no expection;
            obj = employee.EmployeeId;
            obj = employee.Email;
            obj = employee.Fax;
            obj = employee.FirstName;
            obj = employee.FullName;
            obj = employee.LastName;
            obj = employee.Phone;
            obj = employee.PmlUserBrokerNm;
            obj = employee.Role;
            obj = employee.PmlBrokerCompanyId;
            obj = employee.EmployeeStartD;
            obj = employee.EmployeeTerminationD;
        }

    }
}
