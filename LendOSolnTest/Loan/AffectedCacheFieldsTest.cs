﻿namespace LendOSolnTest.Loan
{
    using System.Collections;
    using DataAccess;
    using NUnit.Framework;

    [TestFixture, Ignore("This test has been verified locally.")]
    public class AffectedCacheFieldsTest
    {
        [Test]
        public void EmptySet_ReturnsSameResult()
        {
            ICollection input = new string[0];

            var fieldInfoResult = this.GetFieldInfoResult(input);
            var selectStatementProviderResult = this.GetSelectStatementProviderResult(input);

            CollectionAssert.AreEqual(fieldInfoResult, selectStatementProviderResult);
        }

        [Test]
        public void SmallSet_NoSpecialDependencies_ReturnsSameResult()
        {
            ICollection input = new[]
            {
                "aBEmplrNm",
                "sPrelimRprtOd",
                "sPrelimRprtDueD",
                "sPrelimRprtRd",
                "sApprRprtOd"
            };

            var fieldInfoResult = this.GetFieldInfoResult(input);
            var selectStatementProviderResult = this.GetSelectStatementProviderResult(input);

            CollectionAssert.AreEqual(fieldInfoResult, selectStatementProviderResult);
        }

        [Test]
        public void LargeSet_NoSpecialDependencies_ReturnsSameResult()
        {
            ICollection input = new[]
            {
                "aBEmplrBusPhone",
                "aCEmplrBusPhone",
                "sArmIndexT",
                "sFreddieArmIndexT",
                "sIOnlyMon",
                "sBrokComp1Pc",
                "sApprF",
                "sCrF",
                "sInspectF",
                "sMBrokF",
                "sTxServF",
                "sUwF",
                "sWireF",
                "s800U1FDesc",
                "s800U1F",
                "s800U2FDesc",
                "s800U2F",
                "s800U3FDesc",
                "s800U3F",
                "s800U4FDesc",
                "s800U4F",
                "s800U5FDesc",
                "s800U5F",
                "sU1ScDesc",
                "sU1Sc",
                "sU2ScDesc",
                "sU2Sc",
                "sU3ScDesc"
            };

            var fieldInfoResult = this.GetFieldInfoResult(input);
            var selectStatementProviderResult = this.GetSelectStatementProviderResult(input);

            CollectionAssert.AreEqual(fieldInfoResult, selectStatementProviderResult);
        }

        [Test]
        public void SmallSet_WithSpecialDependencies_ReturnsSameResult()
        {
            ICollection input = new[]
            {
                "sLDiscnt",
                "aCAge",
                "sProMIns",
                "aProdBCitizenT",
                "aDischargedDOfLastBk7"
            };

            var fieldInfoResult = this.GetFieldInfoResult(input);
            var selectStatementProviderResult = this.GetSelectStatementProviderResult(input);

            CollectionAssert.AreEqual(fieldInfoResult, selectStatementProviderResult);
        }

        [Test]
        public void LargeSet_WithSpecialDependencies_ReturnsSameResult()
        {
            ICollection input = new[]
            {
                "sLT",
                "sProMInsT",
                "sGroundRentExpense",
                "sPurchaseAdviceBasePrice_Field1",
                "sProdInvestorRLckdDays",
                "sProdInvestorRLckdExpiredD",
                "sProdInvestorRLckdModeT",
                "sFHAPurposeIsStreamlineRefiWithAppr",
                "sHasAppraisal",
                "sGseSpT",
                "sUnitsNum",
                "sFannieSpT",
                "sSpT",
                "sfTransformLoPropertyTypeToPml",
                "sProdImpound",
                "sWillEscrowBeWaived",
                "sProdDocT",
                "sFannieDocT",
                "sProdAssetT",
                "sProdIT",
                "sTrustReceivableTotal",
                "sOnHoldN",
                "sEscrowF",
                "sEscrowFTable",
                "sAgencyCaseNum",
                "aCEmail",
                "sProdLpePriceGroupNm",
                "sFHALender5DigitsCompanyId",
                "sFHASponsorAgent5DigitsCompanyId",
                "sBrokerLockFinalBrokComp1PcPrice"
            };

            var fieldInfoResult = this.GetFieldInfoResult(input);
            var selectStatementProviderResult = this.GetSelectStatementProviderResult(input);

            CollectionAssert.AreEqual(fieldInfoResult, selectStatementProviderResult);
        }

        private IEnumerable GetFieldInfoResult(ICollection input)
        {
            var fieldInfoTable = CFieldInfoTable.GetInstance();
            return fieldInfoTable.GatherAffectedCacheFields_Obsolete(input).UpdateFields.Collection;
        }

        private IEnumerable GetSelectStatementProviderResult(ICollection input)
        {
            var providerType = E_ReadDBScenarioType.InputDirtyFields_NeedCachedFields;
            var provider = CSelectStatementProvider.GetProviderFor(providerType, input);
            return provider.InputFieldList;
        }
    }
}
