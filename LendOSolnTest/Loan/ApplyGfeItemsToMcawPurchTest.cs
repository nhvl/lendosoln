﻿using DataAccess;
using LendersOffice.Constants;
using LendOSolnTest.Common;
using LendOSolnTest.Fakes;
using NUnit.Framework;

namespace LendOSolnTest.Loan
{
    [TestFixture]
    public class ApplyGfeItemsToMcawPurchTest
    {
        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            LoginTools.LoginAs(TestAccountType.LoTest001);
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
            FakePageDataUtilities.Instance.Reset();
        }

        private CPageData CreateDataObject()
        {
            return FakePageData.Create();
        }
        [Test]
        public void Test()
        {
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.ApplyGfeItemsToMcawPurch();
            dataLoan.Save();
        }
    }
}
