﻿/// <copyright file="DependencyTest.cs" company="MeridianLink">
///     Copyright (c) MeridianLink. All rights reserved.
/// </copyright>
/// <summary>
/// Author: Michael Leinweaver
/// Date:   11/12/2015
/// </summary>
namespace LendOSolnTest.Loan.DependencyTest
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using Common;
    using DataAccess;
    using NUnit.Framework;

    /// <summary>
    /// Represents a testing class to determine if the properties and
    /// methods in CPageBase, CAppBase, and CPageHelocBase have all 
    /// required dependencies.
    /// </summary>
    [TestFixture]
    [Category(CategoryConstants.RunBeforePullRequest)]
    public class DependencyTest
    {
        private readonly DependencyBuilder dependencyBuilder = new DependencyBuilder();
        private StringBuilder errorBuilder;

        [SetUp]
        public void TestSetup()
        {
            this.errorBuilder = new StringBuilder();
        }

        [Test]
        public void TestPropertyDependencies()
        {
            var hasError = false;

            foreach (var testClass in DependencyUtils.DependencyClasses)
            {
                var testList = from property in this.dependencyBuilder.ReflectedTypes[testClass].GetProperties()
                               where !this.dependencyBuilder.IsSkippable(property)
                               select property;

                hasError |= this.DependencyTestImpl(testClass.Name, testList, dependencyBuilder);
            }

            Assert.IsFalse(hasError, message: errorBuilder.ToString());
        }

        [Test]
        public void TestMethodDependencies()
        {
            var hasError = false;

            foreach (var testClass in DependencyUtils.DependencyClasses)
            {
                var testList = from method in this.dependencyBuilder.ReflectedTypes[testClass].GetMethods()
                               where !dependencyBuilder.IsSkippable(method)
                               select method;

                hasError |= this.DependencyTestImpl(testClass, testList, dependencyBuilder);
            }

            Assert.IsFalse(hasError, message: errorBuilder.ToString());
        }

        [Test]
        public void VerifyMethodsDoNotMixDependencyStubsAndDependsOnAttributes()
        {
            var badMethods = new List<string>();

            foreach (var testClass in DependencyUtils.DependencyClasses)
            {
                var testList = from method in this.dependencyBuilder.ReflectedTypes[testClass].GetMethods()
                               where !dependencyBuilder.IsSkippable(method)
                               select method;

                foreach (var method in testList)
                {
                    var dependencyPrefix = dependencyBuilder.GetMethodDependencyPropertyPrefix(method);

                    if (!string.IsNullOrEmpty(dependencyPrefix)
                        && method.GetCustomAttribute<DependsOnAttribute>() != null)
                    {
                        badMethods.Add($"{testClass.Name}::{method.ToString()}");
                    }
                }
            }

            var errorMessage = "The following methods specify dependencies using both a stub property and using "
                + "a method-level DependsOn attribute. "
                + "Please update these methods to use only one style of dependency inclusion.\r\n"
                + string.Join(Environment.NewLine, badMethods);

            Assert.AreEqual(0, badMethods.Count, errorMessage);
        }

        [Test]
        public void VerifyAllMethodOverloadsIncludeDependsOnAttribute()
        {
            var badMethods = new List<string>();

            foreach (var testClass in DependencyUtils.DependencyClasses)
            {
                var type = this.dependencyBuilder.ReflectedTypes[testClass];
                var methodsByName = type
                    .GetMethods()
                    .Where(method => !type.IsPropertyGetOrSetMethod(method) && System.CodeDom.Compiler.CodeGenerator.IsValidLanguageIndependentIdentifier(method.Name)) // We only care about methods that we wrote
                    .ToLookup(method => method.Name, method => method);

                foreach (var methodGroup in methodsByName)
                {
                    if (methodGroup.Any(method => method.GetCustomAttribute<DependsOnAttribute>() != null))
                    {
                        var methodsWithoutDependsOn = methodGroup.Where(method => method.GetCustomAttribute<DependsOnAttribute>() == null)
                            .Select(method => $"{testClass.Name}::{method.ToString()}");

                        badMethods.AddRange(methodsWithoutDependsOn);
                    }
                }
            }

            var errorMessage = "The following methods do not specify a DependsOn attribute but one of its overloads does. "
                + "Please add a DependsOn attribute to the following methods:\r\n"
                + string.Join(Environment.NewLine, badMethods);

            Assert.AreEqual(0, badMethods.Count, errorMessage);
        }

        /// <summary>
        /// We give users the ability to change the primary application on a loan file. The loan file
        /// cache tables contain application level fields for the primary application. When the primary
        /// application is updated, we need to ensure that the dependent cache fields are updated. It looks
        /// like this was done in the past by adding a manual dependency to either aIsPrimary or
        /// PrimaryRankIndex on all of the fields we want updated in the cache. There were many fields
        /// that did not have this dependency, which led to cases where the cache would be stale and workflow
        /// would evaluate stale/bad data. This test ensures that all application fields that are stored
        /// in the loan file cache tables have a dependency on PrimaryRankIndex to make sure that the cache
        /// values are updated when the primary application switches.
        /// </summary>
        [Test]
        public void VerifyAllCachedAppFieldsDependOnPrimaryRankIndex()
        {
            var allFieldInfoFieldIds = CFieldDbInfoList.TheOnlyInstance.FieldNameList;
            var cachedAppFieldIds = new List<string>();
            foreach (var fieldId in allFieldInfoFieldIds)
            {
                var field = CFieldDbInfoList.TheOnlyInstance.Get(fieldId);
                if (field.IsAppField && field.IsCachedField)
                {
                    cachedAppFieldIds.Add(field.m_fieldName);
                }
            }

            var reflectedApp = this.dependencyBuilder.ReflectedTypes[typeof(CAppBase)];
            var cachedFieldsMissingPrimaryRankIndexDependency = new List<string>();
            foreach (var fieldId in cachedAppFieldIds)
            {
                var property = reflectedApp.GetProperty(fieldId);
                var dependsOnAttribute = property.GetCustomAttribute<DependsOnAttribute>();
                var dependsOnList = dependsOnAttribute?.GetList()?.Select(f => f.Name);

                if (dependsOnAttribute == null || !dependsOnList.Contains("PrimaryRankIndex"))
                {
                    cachedFieldsMissingPrimaryRankIndexDependency.Add(fieldId);
                }
            }

            if (cachedFieldsMissingPrimaryRankIndexDependency.Any())
            {
                Assert.Fail($"The following application fields are missing a dependency on PrimaryRankIndex:{Environment.NewLine}" + string.Join(Environment.NewLine, cachedFieldsMissingPrimaryRankIndexDependency));
            }
        }

        #region Test runners
        private bool DependencyTestImpl(string className, IEnumerable<PropertyInfo> testList, DependencyBuilder dependencyBuilder)
        {
            var seenMissingDependencies = new Dictionary<string, MissingDependencyInfo>();

            foreach (var property in testList)
            {
                HashSet<string> specialNotListedButIncludedDependencies = null;

                var dependenciesListedForProperty = dependencyBuilder.GetDependenciesFromDependsOn(
                    property, 
                    out specialNotListedButIncludedDependencies);

                var accessors = dependencyBuilder.GetAccessors(property);

                var missingDependencies = accessors.Except(dependenciesListedForProperty);

                if (missingDependencies.Any())
                {
                    accessors.ExceptWith(specialNotListedButIncludedDependencies);

                    // There are some cases where DependsOn attributes have dependencies listed 
                    // that may  not be explictly accessed in the getter or setter but should 
                    // still be included, e.g. CAppBase.aAppFullNm.
                    accessors.UnionWith(dependenciesListedForProperty);

                    var hasSpecialDependencies = specialNotListedButIncludedDependencies.Any() ||
                        missingDependencies.Any(missingDependency => DependencyUtils.IsSpecialDependency(missingDependency));

                    // We always want to include the name of the property when we're dealing
                    // with special dependencies as a failsafe to make the update graph aware.
                    // We don't modify the list of accessors until we save the missing dependency
                    // info because the late binding of the Except() call means missingDependencies
                    // will be modified if we modify the list of accessors.
                    seenMissingDependencies.Add(property.Name, new MissingDependencyInfo()
                    {
                        Accessors = hasSpecialDependencies ? new HashSet<string>(accessors.Union(new string[] { property.Name })) : accessors,
                        MissingDependencies = new HashSet<string>(missingDependencies),
                        HasSpecialIncludedDependencies = hasSpecialDependencies
                    });
                }
            }

            if (seenMissingDependencies.Count > 0)
            {
                var missingDependencyDescriptions = from missingDependency in seenMissingDependencies
                                                    select this.GetMissingDescription(missingDependency);

                this.errorBuilder.AppendFormat(string.Format(
                    "These {0} properties have dependency errors: {1}{2}{1}",
                    className,
                    Environment.NewLine + Environment.NewLine,
                    string.Join(Environment.NewLine, missingDependencyDescriptions)));

                return true;
            }

            return false;
        }

        private bool DependencyTestImpl(Type classType, IEnumerable<MethodInfo> testList, DependencyBuilder dependencyBuilder)
        {
            var seenMissingDependencies = new Dictionary<string, MissingDependencyInfo>();

            foreach (var method in testList)
            {
                HashSet<string> specialNotListedButIncludedDependencies = null;

                var dependenciesListedForMethod = dependencyBuilder.GetDependenciesFromDependsOn(
                    method, 
                    out specialNotListedButIncludedDependencies);

                var accessors = dependencyBuilder.GetAccessors(method);

                var missingDependencies = accessors.Except(dependenciesListedForMethod);

                if (missingDependencies.Any())
                {
                    accessors.ExceptWith(specialNotListedButIncludedDependencies);

                    // As with property dependencies, we want to preserve any dependencies
                    // listed that are not explicitly accessed.
                    accessors.UnionWith(dependenciesListedForMethod);

                    string methodName;
                    if (method.GetCustomAttribute<DependsOnAttribute>() != null)
                    {
                        methodName = method.ToString();
                    }
                    else
                    {
                        methodName = dependencyBuilder.GetMethodDependencyPropertyPrefix(method) + method.Name;
                    }

                    var hasSpecialDependencies = specialNotListedButIncludedDependencies.Any() ||
                        missingDependencies.Any(missingDependency => DependencyUtils.IsSpecialDependency(missingDependency));

                    if (seenMissingDependencies.ContainsKey(methodName))
                    {
                        seenMissingDependencies[methodName].Accessors.UnionWith(hasSpecialDependencies? accessors.Union(new string[] { methodName }) : accessors);
                        seenMissingDependencies[methodName].MissingDependencies.UnionWith(missingDependencies);
                        seenMissingDependencies[methodName].HasSpecialIncludedDependencies |= hasSpecialDependencies;
                    }
                    else
                    {
                        seenMissingDependencies.Add(methodName, new MissingDependencyInfo()
                        {
                            Accessors = hasSpecialDependencies ? new HashSet<string>(accessors.Union(new string[] { methodName })) : accessors,
                            MissingDependencies = new HashSet<string>(missingDependencies),
                            HasSpecialIncludedDependencies = hasSpecialDependencies
                        });
                    }
                }
            }

            if (seenMissingDependencies.Count > 0)
            {
                var missingDependencyDescriptions = from missingDependency in seenMissingDependencies
                                                    select this.GetMissingDescription(missingDependency);
                
                errorBuilder.AppendFormat(string.Format(
                    "These {0} methods have dependency errors: {1}{2}{1}",
                    classType.Name,
                    Environment.NewLine + Environment.NewLine,
                    string.Join(Environment.NewLine, missingDependencyDescriptions)));

                return true;
            }

            return false;
        }

        private string GetMissingDescription(KeyValuePair<string, MissingDependencyInfo> pair)
        {
            var suggestionMessage = pair.Value.HasSpecialIncludedDependencies ?
                "This member has special dependencies; carefully review this suggestion and consult with CFieldInfoTable.x_SpecialDependencyFieldList for more information" :
                "Suggestion to review";

            return string.Format(
                "{0}: Missing {1}. {2}: [DependsOn({3})]",
                pair.Key,
                string.Join(",", pair.Value.MissingDependencies),
                suggestionMessage,
                string.Join(", ", pair.Value.Accessors.Select(dep => "nameof(" + dep + ")")));
        }
        #endregion
    }
}
