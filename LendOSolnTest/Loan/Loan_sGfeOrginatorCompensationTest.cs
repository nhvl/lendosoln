﻿using System;
using DataAccess;
using LendersOffice.Constants;
using LendOSolnTest.Common;
using LendOSolnTest.Fakes;
using NUnit.Framework;

namespace LendOSolnTest.Loan
{
    [TestFixture]
    public class Loan_sGfeOrginatorCompensationTest
    {
        [TestFixtureSetUp]
        public void Setup()
        {
            LoginTools.LoginAs(TestAccountType.LoanOfficer);
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
            FakePageDataUtilities.Instance.Reset();
        }

        private CPageData CreatePageData()
        {
            var dataLoan = FakePageData.Create();

            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            dataLoan.sLAmtCalc_rep = "100,000.00"; // Set Loan Amount = 100K
            dataLoan.sLAmtLckd = true;

            dataLoan.sFfUfmipR_rep = "0.500%";
            dataLoan.sFfUfMipIsBeingFinanced = true;
            dataLoan.Save();
            Assert.AreEqual("$100,500.00", dataLoan.sFinalLAmt_rep);

            return dataLoan;
        }
        
        [Test]
        public void Test_PaidBy_Borrower()
        {
            var testCaseList = new[] {
                new { 
                    input_sGfeOriginatorCompFPc = "1.000%",
                    input_sGfeOriginatorCompFBaseT = E_PercentBaseT.LoanAmount,
                    input_sGfeOriginatorCompFMb = "$50.00",
                    expected_sGfeOriginatorCompF = "$1,050.00"
                },
                new { 
                    input_sGfeOriginatorCompFPc = "1.000%",
                    input_sGfeOriginatorCompFBaseT = E_PercentBaseT.TotalLoanAmount,
                    input_sGfeOriginatorCompFMb = "$50.00",
                    expected_sGfeOriginatorCompF = "$1,055.00"
                },

                new { 
                    input_sGfeOriginatorCompFPc = "1.000%",
                    input_sGfeOriginatorCompFBaseT = E_PercentBaseT.AllYSP,
                    input_sGfeOriginatorCompFMb = "$50.00",
                    expected_sGfeOriginatorCompF = ""
                }
            };

            foreach (var testCase in testCaseList)
            {
                CPageData dataLoan = CreatePageData();
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                dataLoan.SetOriginatorCompensation(E_sOriginatorCompensationPaymentSourceT.BorrowerPaid,
                    testCase.input_sGfeOriginatorCompFPc,
                    testCase.input_sGfeOriginatorCompFBaseT,
                    testCase.input_sGfeOriginatorCompFMb);

                dataLoan.Save();

                dataLoan = FakePageData.Create();
                dataLoan.InitLoad();
                Assert.AreEqual(testCase.expected_sGfeOriginatorCompF, dataLoan.sGfeOriginatorCompF_rep, testCase.ToString());
            }

        }

        [Test]
        public void Test_PaidBy_Lender()
        {
            var testCaseList = new[] {
                new { 
                    input_sOriginatorCompensationPercent = "0.000%",
                    input_sOriginatorCompensationBaseT = E_PercentBaseT.LoanAmount,
                    input_sOriginatorCompensationMinAmount = "$0.00",
                    input_sOriginatorCompensationMaxAmount = "$0.00",
                    input_sOriginatorCompensationFixedAmount = "$0.00",
                    expected_sGfeOriginatorCompFMb = "$0.00",
                    expected_sGfeOriginatorCompF = "$0.00"
                },
                new { 
                    input_sOriginatorCompensationPercent = "1.000%",
                    input_sOriginatorCompensationBaseT = E_PercentBaseT.LoanAmount,
                    input_sOriginatorCompensationMinAmount = "$0.00",
                    input_sOriginatorCompensationMaxAmount = "$0.00",
                    input_sOriginatorCompensationFixedAmount = "$0.00",
                    expected_sGfeOriginatorCompFMb = "$0.00",
                    expected_sGfeOriginatorCompF = "$1,000.00"
                },
                new { 
                    input_sOriginatorCompensationPercent = "1.000%",
                    input_sOriginatorCompensationBaseT = E_PercentBaseT.TotalLoanAmount,
                    input_sOriginatorCompensationMinAmount = "$0.00",
                    input_sOriginatorCompensationMaxAmount = "$0.00",
                    input_sOriginatorCompensationFixedAmount = "$0.00",
                    expected_sGfeOriginatorCompFMb = "$0.00",
                    expected_sGfeOriginatorCompF = "$1,005.00"
                },
                new { 
                    input_sOriginatorCompensationPercent = "1.000%",
                    input_sOriginatorCompensationBaseT = E_PercentBaseT.LoanAmount,
                    input_sOriginatorCompensationMinAmount = "$0.00",
                    input_sOriginatorCompensationMaxAmount = "$0.00",
                    input_sOriginatorCompensationFixedAmount = "$50.00",
                    expected_sGfeOriginatorCompFMb = "$50.00",
                    expected_sGfeOriginatorCompF = "$1,050.00"
                },
                new { 
                    input_sOriginatorCompensationPercent = "1.000%",
                    input_sOriginatorCompensationBaseT = E_PercentBaseT.TotalLoanAmount,
                    input_sOriginatorCompensationMinAmount = "$0.00",
                    input_sOriginatorCompensationMaxAmount = "$0.00",
                    input_sOriginatorCompensationFixedAmount = "$40.00",
                    expected_sGfeOriginatorCompFMb = "$40.00",
                    expected_sGfeOriginatorCompF = "$1,045.00"
                },

                new { 
                    input_sOriginatorCompensationPercent = "1.000%",
                    input_sOriginatorCompensationBaseT = E_PercentBaseT.LoanAmount,
                    input_sOriginatorCompensationMinAmount = "$500.00",
                    input_sOriginatorCompensationMaxAmount = "$0.00",
                    input_sOriginatorCompensationFixedAmount = "$0.00",
                    expected_sGfeOriginatorCompFMb = "$0.00",
                    expected_sGfeOriginatorCompF = "$1,000.00"
                },
                new { 
                    input_sOriginatorCompensationPercent = "1.000%",
                    input_sOriginatorCompensationBaseT = E_PercentBaseT.LoanAmount,
                    input_sOriginatorCompensationMinAmount = "$500.00",
                    input_sOriginatorCompensationMaxAmount = "$0.00",
                    input_sOriginatorCompensationFixedAmount = "$2,000.00",
                    expected_sGfeOriginatorCompFMb = "$2,000.00",
                    expected_sGfeOriginatorCompF = "$3,000.00"
                },
                new { 
                    input_sOriginatorCompensationPercent = "1.000%",
                    input_sOriginatorCompensationBaseT = E_PercentBaseT.TotalLoanAmount,
                    input_sOriginatorCompensationMinAmount = "$500.00",
                    input_sOriginatorCompensationMaxAmount = "$0.00",
                    input_sOriginatorCompensationFixedAmount = "$0.00",
                    expected_sGfeOriginatorCompFMb = "$0.00",
                    expected_sGfeOriginatorCompF = "$1,005.00"
                },
                new { 
                    input_sOriginatorCompensationPercent = "1.000%",
                    input_sOriginatorCompensationBaseT = E_PercentBaseT.TotalLoanAmount,
                    input_sOriginatorCompensationMinAmount = "$500.00",
                    input_sOriginatorCompensationMaxAmount = "$0.00",
                    input_sOriginatorCompensationFixedAmount = "$2,000.00",
                    expected_sGfeOriginatorCompFMb = "$2,000.00",
                    expected_sGfeOriginatorCompF = "$3,005.00"
                },

                new { 
                    input_sOriginatorCompensationPercent = "1.000%",
                    input_sOriginatorCompensationBaseT = E_PercentBaseT.LoanAmount,
                    input_sOriginatorCompensationMinAmount = "$1500.00",
                    input_sOriginatorCompensationMaxAmount = "$0.00",
                    input_sOriginatorCompensationFixedAmount = "$0.00",
                    expected_sGfeOriginatorCompFMb = "$500.00",
                    expected_sGfeOriginatorCompF = "$1,500.00"
                },
                new { 
                    input_sOriginatorCompensationPercent = "1.000%",
                    input_sOriginatorCompensationBaseT = E_PercentBaseT.LoanAmount,
                    input_sOriginatorCompensationMinAmount = "$1500.00",
                    input_sOriginatorCompensationMaxAmount = "$0.00",
                    input_sOriginatorCompensationFixedAmount = "$50.00",
                    expected_sGfeOriginatorCompFMb = "$550.00",
                    expected_sGfeOriginatorCompF = "$1,550.00"
                },

                new { 
                    input_sOriginatorCompensationPercent = "1.000%",
                    input_sOriginatorCompensationBaseT = E_PercentBaseT.LoanAmount,
                    input_sOriginatorCompensationMinAmount = "$0.00",
                    input_sOriginatorCompensationMaxAmount = "$500.00",
                    input_sOriginatorCompensationFixedAmount = "$0.00",
                    expected_sGfeOriginatorCompFMb = "($500.00)",
                    expected_sGfeOriginatorCompF = "$500.00"
                },


                new { 
                    input_sOriginatorCompensationPercent = "1.000%",
                    input_sOriginatorCompensationBaseT = E_PercentBaseT.LoanAmount,
                    input_sOriginatorCompensationMinAmount = "$0.00",
                    input_sOriginatorCompensationMaxAmount = "$500.00",
                    input_sOriginatorCompensationFixedAmount = "$1,000.00",
                    expected_sGfeOriginatorCompFMb = "$500.00",
                    expected_sGfeOriginatorCompF = "$1,500.00"
                },


                new { 
                    input_sOriginatorCompensationPercent = "1.000%",
                    input_sOriginatorCompensationBaseT = E_PercentBaseT.TotalLoanAmount,
                    input_sOriginatorCompensationMinAmount = "$0.00",
                    input_sOriginatorCompensationMaxAmount = "$500.00",
                    input_sOriginatorCompensationFixedAmount = "$1,000.00",
                    expected_sGfeOriginatorCompFMb = "$495.00",
                    expected_sGfeOriginatorCompF = "$1,500.00"
                },


                new { 
                    input_sOriginatorCompensationPercent = "1.000%",
                    input_sOriginatorCompensationBaseT = E_PercentBaseT.TotalLoanAmount,
                    input_sOriginatorCompensationMinAmount = "$200.00",
                    input_sOriginatorCompensationMaxAmount = "$500.00",
                    input_sOriginatorCompensationFixedAmount = "$1,000.00",
                    expected_sGfeOriginatorCompFMb = "$495.00",
                    expected_sGfeOriginatorCompF = "$1,500.00"
                }
            };


            foreach (var testCase in testCaseList)
            {
                CPageData dataLoan = CreatePageData();
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                dataLoan.sGfeIsTPOTransaction = true;
                dataLoan.ApplyCompensationManually(DateTime.Now.ToString(), DateTime.Now.ToString(),
                    testCase.input_sOriginatorCompensationPercent, testCase.input_sOriginatorCompensationBaseT,
                    testCase.input_sOriginatorCompensationMinAmount, testCase.input_sOriginatorCompensationMaxAmount,
                    testCase.input_sOriginatorCompensationFixedAmount, "", false);

                //dataLoan.sOriginatorCompensationPercent_rep = testCase.input_sOriginatorCompensationPercent;
                //dataLoan.sOriginatorCompensationBaseT = testCase.input_sOriginatorCompensationBaseT;
                //dataLoan.sOriginatorCompensationMinAmount_rep = testCase.input_sOriginatorCompensationMinAmount;
                //dataLoan.sOriginatorCompensationMaxAmount_rep = testCase.input_sOriginatorCompensationMaxAmount;
                //dataLoan.sOriginatorCompensationFixedAmount_rep = testCase.input_sOriginatorCompensationFixedAmount;
                dataLoan.sOriginatorCompensationLenderFeeOptionT = E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees;
                dataLoan.SetOriginatorCompensation(E_sOriginatorCompensationPaymentSourceT.LenderPaid,
                    "0.000%", E_PercentBaseT.LoanAmount, "$0.00");

                dataLoan.Save();

                dataLoan = FakePageData.Create();

                dataLoan.InitLoad();
                Assert.AreEqual(testCase.expected_sGfeOriginatorCompF, dataLoan.sGfeOriginatorCompF_rep, "sGfeOriginatorCompF: " +testCase.ToString());
                Assert.AreEqual(dataLoan.sOriginatorCompensationTotalAmount_rep, dataLoan.sGfeOriginatorCompF_rep, "sGfeOriginatorCompF: " + testCase.ToString());
                Assert.AreEqual(dataLoan.sOriginatorCompensationPercent_rep, dataLoan.sGfeOriginatorCompFPc_rep, "sGfeOriginatorCompFPc: " + testCase.ToString());
                Assert.AreEqual(dataLoan.sOriginatorCompensationBaseT, dataLoan.sGfeOriginatorCompFBaseT, "sGfeOriginatorCompFBaseT: " + testCase.ToString());
                Assert.AreEqual(testCase.expected_sGfeOriginatorCompFMb, dataLoan.sGfeOriginatorCompFMb_rep, "sGfeOriginatorCompFMb: " +testCase.ToString());

            }
        }

        [Test]
        public void Test_HasCompensationPlan()
        {
            // 2/22/2011 dd - This test to check when create a new loan there is no compensation plan
            // After apply the compensation plan then sHasOriginatorCompensationPlan should return true.

            CPageData dataLoan = CreatePageData();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            Assert.AreEqual(false, dataLoan.sHasOriginatorCompensationPlan, "Initial sHasOriginatorCompensationPlan");

            dataLoan.ApplyCompensationManually(DateTime.Now.ToString(), DateTime.Now.ToString(),
                "1.000%", E_PercentBaseT.LoanAmount, "$0.00", "$0.00", "$0.00", "", false);

            dataLoan.Save();

            dataLoan = FakePageData.Create();
            dataLoan.InitLoad();
            Assert.AreEqual(true, dataLoan.sHasOriginatorCompensationPlan, "After set compensation manually");
        }
    }
}
