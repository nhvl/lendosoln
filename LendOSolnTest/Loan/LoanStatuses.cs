﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using DataAccess;
using System.Data.SqlClient;
using System.Data;
using LqbGrammar.Drivers;
using LendersOffice.Drivers.SqlServerDB;
using LqbGrammar.DataTypes;

namespace LendOSolnTest.Loan
{

    [TestFixture]
    public class LoanStatuses
    {
        [Test]
        public void SPROC_GetLoanStatusName_Test()
        {
            foreach (var v in Enum.GetValues(typeof(E_sStatusT)))
            {
                var testStatus = (int)v;
                
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@t", testStatus));
                var result = new SqlParameter("@Result", "");
                result.Direction = ParameterDirection.ReturnValue;
                parameters.Add(result);
                StoredProcedureHelper.ExecuteScalar(DataSrc.LOShare, "GetLoanStatusName", parameters.ToArray());
                
                Assert.That(result.Value != DBNull.Value && !string.IsNullOrEmpty((string)result.Value),
                    "Please update sproc GetLoanStatusName to handle status: " + testStatus);
                  
            }
        }
        
        [Test]
        public void LosUtilsIsLeadTest()
        {
            try
            {
                foreach (var v in Enum.GetValues(typeof(E_sStatusT)))
                {
                    var testStatus = (E_sStatusT)v;
                    Tools.IsStatusLead(testStatus);
                }
            }
            catch (UnhandledEnumException e)
            {
                Assert.Fail("Please update LosUtils.IsStatusLead to fix " + e.DeveloperMessage);
            }
            
        }

        [Test]
        public void LosUtilsIsInactiveStatusTest()
        {
            try
            {
                foreach (var v in Enum.GetValues(typeof(E_sStatusT)))
                {
                    var testStatus = (E_sStatusT)v;
                    Tools.IsInactiveStatus(testStatus);
                }
            }
            catch (UnhandledEnumException e)
            {
                Assert.Fail("Please update LosUtils.IsInactiveStatus to fix " + e.DeveloperMessage);
            }
            
        }

        [Test]
        public void LoanOrdering_Test()
        {
            foreach (var v in Enum.GetValues(typeof(E_sStatusT)))
            {
                var status = (E_sStatusT)v;
                if (!CPageBase.s_sStatusT_Order.Contains(status))
                {
                    Assert.Fail(" LFF.s_sStatusT_Order is missing " + Enum.GetName(typeof(E_sStatusT), status));
                }
            }
        }

        [Test]
        public void DatalayerDBOrderMismatchTest()
        {
            try
            {
                var dbOrderedStatuses = new List<E_sStatusT>();

                string sql = "select * from Loan_Status order by OrderIndex";

                using (var sqlConnection = DbAccessUtils.GetConnection(DataSrc.LOShareROnly))
                {
                    var sqlQuery = SQLQueryString.Create(sql);

                    // We don't want to do any logging of the sql call, so don't bother with any decorators
                    var sqlDriver = SqlServerHelper.CreateSpecificDriver(TimeoutInSeconds.Default, SqlDecoratorType.None);
                    using (var reader = sqlDriver.Select(sqlConnection, null, sqlQuery.Value, null))
                    {
                        while (reader.Read())
                        {
                            E_sStatusT status = (E_sStatusT)reader["EnumValue"];
                            dbOrderedStatuses.Add(status);
                        }
                    }
                }

                if (dbOrderedStatuses.SequenceEqual(CPageBase.s_sStatusT_Order))
                {
                    return;
                }

                var extraDBStatuses = dbOrderedStatuses.Except(CPageBase.s_sStatusT_Order).ToArray();
                var extraDatalayerStatuses = CPageBase.s_sStatusT_Order.Except(dbOrderedStatuses).ToArray();

                string msg = "Loan_Status order does not match LFF.s_StatusT_Order.";

                if (extraDBStatuses.Length > 0)
                {
                    msg += Environment.NewLine + " db has extra statuses: "
                        + string.Join(", ", extraDBStatuses.Select(s => s.ToString("g") + ":" + s.ToString("d")).ToArray()); ;
                }
                if (extraDatalayerStatuses.Length > 0)
                {
                    msg += Environment.NewLine + " db has extra statuses: "
                       + string.Join(", ", extraDatalayerStatuses.Select(s => s.ToString("g") + ":" + s.ToString("d")).ToArray());
                }


                Assert.Fail(msg);
            }
            finally
            {
                // Since this test isn't registering any factories, don't call ClearFactories().
                //LqbGrammar.GenericLocator<ISqlAdapterFactory>.ClearFactories();
            }
        }
        //[Test]
        //public void s_OrderByStatusT_MultiThreaded_Test()
        //{
        //    TestUtilities.MultiThreadTest(s_OrderByStatusT_Test, 10);
        //}

        //[Test]
        //public void validatorTest()
        //{
        //    var x_principal = LoginTools.LoginAs(TestAccountType.LoTest001);
        //    var creator = CLoanFileCreator.GetCreator(x_principal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
        //    var slid = creator.BeginCreateBlankLoanFile();
        //    creator.CommitFileCreation(false);


        //    var dataLoan = new CFullAccessPageData(slid, new string[]{"sWithdrawnD"});
        //    dataLoan.ByPassFieldSecurityCheck = true;
        //    dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
        //    dataLoan.sStatusLckd = false;
        //    dataLoan.sWithdrawnD_rep = DateTime.Now.ToShortDateString();
        //    Console.WriteLine(dataLoan.sStatusT_rep);
        //    dataLoan.Save();

        //}
        
        //private void brokerDBTest()
        //{
        //    var broker = BrokerDB.RetrieveById(new Guid("BE9439A4-25AC-41B0-9A86-7DA324C2F966"));
        //    foreach (var v in Enum.GetValues(typeof(E_BranchChannelT)))
        //    {
        //        var channel = (E_BranchChannelT)v;
        //        if (!broker.EnabledStatusesByChannel.ContainsKey(channel))
        //        {
        //            Assert.Fail(" BrokerDB.EnabledStatusesByChannel is missing " + Enum.GetName(typeof(E_BranchChannelT), channel));
        //        }
        //    }
        //}

        //[Test]
        //public void brokerDB_multithread_Test()
        //{
        //    TestUtilities.MultiThreadTest(brokerDBTest, 10);
        //}


        //private readonly object brokerLockObj = new Object();
        //private void brokerDBTest2()
        //{


        //    var broker = BrokerDB.RetrieveById(new Guid("BE9439A4-25AC-41B0-9A86-7DA324C2F966"));


        //    var rand = new System.Random();

        //    for (int i = 0; i < 100; i++)
        //    {
        //        lock (brokerLockObj)
        //        {
        //            broker.DocMagicDefaultDocTypeID = i;
        //            var sleepTimeMs = (int)(rand.NextDouble() * 100.0d);
        //            Thread.Sleep(sleepTimeMs);
        //            Assert.That(broker.DocMagicDefaultDocTypeID == i);
        //            if (i % 10 == 0)
        //            {
        //                Console.WriteLine(i);
        //            }
        //        }
        //    }

        //}

        //[Test]
        //public void brokerDB_multithread_Test2()
        //{
        //    TestUtilities.MultiThreadTest(brokerDBTest2, 10);
        //}

    }
}
