﻿using DataAccess;
using LendersOffice.Constants;
using LendOSolnTest.Common;
using LendOSolnTest.Fakes;
using NUnit.Framework;

namespace LendOSolnTest.Loan
{
    [TestFixture]
    public class CPageBase_FindDataAppBySsnTest
    {
        [TestFixtureSetUp]
        public void Setup()
        {
            LoginTools.LoginAs(TestAccountType.LoTest001);
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
            FakePageDataUtilities.Instance.Reset();
        }

        private CPageData CreatePageData()
        {
            var dataLoan = FakePageData.Create(appCount: 4);

            var initialData = new[] {
                new { aBFirstNm = "BORR_01", aBSsn = "111-11-1111", aCFirstNm = "COBORR_01", aCSsn = "222-22-2222"}
                ,new { aBFirstNm = "BORR_02", aBSsn = "333-33-3333", aCFirstNm = "COBORR_02", aCSsn = "444-44-4444"}
                ,new { aBFirstNm = "BORR_03", aBSsn = "555-55-5555", aCFirstNm = "COBORR_03", aCSsn = "666-66-6666"}
                ,new { aBFirstNm = "BORR_04", aBSsn = "", aCFirstNm = "COBORR_04", aCSsn = ""}

            };
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            for (int i = 0; i < initialData.Length; i++)
            {
                CAppData dataApp = dataLoan.GetAppData(i);
                var data = initialData[i];

                dataApp.aBFirstNm = data.aBFirstNm;
                dataApp.aBSsn = data.aBSsn;
                dataApp.aCFirstNm = data.aCFirstNm;
                dataApp.aCSsn = data.aCSsn;
            }
            dataLoan.Save();

            return dataLoan;
        }

        [Test]
        public void Test_NullSsn()
        {
            CPageData dataLoan = CreatePageData();
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.FindDataAppBySsn(string.Empty);

            Assert.IsNull(dataApp, "Empty string SSN");

            dataApp = dataLoan.FindDataAppBySsn(null);

            Assert.IsNull(dataApp, "Null");

            dataApp = dataLoan.FindDataAppBySsn("   "); // 7/8/2009 dd - This test should return null instead of valid borrower.

            Assert.IsNull(dataApp, "SSN with all spaces");
        }

        [Test]
        public void Test_NormalScenarios()
        {
            var testCases = new[] {
                new { ssn = "111-11-1111", Expected_aFirstNm = "BORR_01" }
                , new { ssn = "222-22-2222", Expected_aFirstNm = "COBORR_01" }
                , new { ssn = "333-33-3333", Expected_aFirstNm = "BORR_02" }
                , new { ssn = "444-44-4444", Expected_aFirstNm = "COBORR_02" }
                , new { ssn = "555-55-5555", Expected_aFirstNm = "BORR_03" }
                , new { ssn = "666-66-6666", Expected_aFirstNm = "COBORR_03" }

            };

            CPageData dataLoan = CreatePageData();
            dataLoan.InitLoad();
            foreach (var testCase in testCases)
            {
                CAppData dataApp = dataLoan.FindDataAppBySsn(testCase.ssn);
                Assert.IsNotNull(dataApp);

                Assert.AreEqual(testCase.ssn, dataApp.aSsn, "SSN");
                Assert.AreEqual(testCase.Expected_aFirstNm, dataApp.aFirstNm, "aFirstNm");
            }
        }

        [Test]
        public void Test_NormalScenario_MismoFormat()
        {
            var testCases = new[] {
                new { ssn = "111111111", Expected_aFirstNm = "BORR_01" }
                , new { ssn = "222222222", Expected_aFirstNm = "COBORR_01" }
                , new { ssn = "333333333", Expected_aFirstNm = "BORR_02" }
                , new { ssn = "444444444", Expected_aFirstNm = "COBORR_02" }
                , new { ssn = "555555555", Expected_aFirstNm = "BORR_03" }
                , new { ssn = "666666666", Expected_aFirstNm = "COBORR_03" }

            };

            CPageData dataLoan = CreatePageData();
            dataLoan.InitLoad();
            dataLoan.SetFormatTarget(FormatTarget.MismoClosing);
            foreach (var testCase in testCases)
            {
                CAppData dataApp = dataLoan.FindDataAppBySsn(testCase.ssn);
                Assert.IsNotNull(dataApp);

                Assert.AreEqual(testCase.ssn, dataApp.aSsn, "SSN");
                Assert.AreEqual(testCase.Expected_aFirstNm, dataApp.aFirstNm, "aFirstNm");
            }

        }
    }
}
