﻿namespace LendOSolnTest.Loan
{
    using DataAccess;
    using LendOSolnTest.Common;
    using LendOSolnTest.Fakes;
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;

    [TestFixture]
    public class LoanCalculationTest
    {
        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            LoginTools.LoginAs(TestAccountType.LoTest001);
        }

        private CPageData CreateDataObject()
        {
            return FakePageData.Create();
        }

        [Test]
        public void sMaxRTest()
        {
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(0);
            dataApp.aBBaseI_rep = "$4000";

            dataLoan.sLAmtCalc_rep = "$300,000";
            dataLoan.sLAmtLckd = true;

            dataLoan.sTerm_rep = "360";
            dataLoan.sDue_rep = "360";

            dataLoan.sMaxRLckd = false;
            

            var testCases = new[] {
                new { Expected_sMaxR_rep = "4.970%", sMaxDti_rep = "40.125%"},
                new { Expected_sMaxR_rep = "8.935%", sMaxDti_rep ="60.000%"},
            };
            foreach (var o in testCases)
            {
                dataLoan.sMaxDti_rep = o.sMaxDti_rep;
                string sMaxR_rep = dataLoan.sMaxR_rep;

                // For Debugging Purpose.
                //dataLoan.InvalidateCache();
                //dataLoan.sNoteIR_rep = sMaxR_rep;
                //Tools.LogError("sMaxR=" + sMaxR_rep + ", sMaxDTI=" + o.sMaxDti_rep + ", sQualBottomR=" + dataLoan.sQualBottomR_rep);

                Assert.AreEqual(o.Expected_sMaxR_rep, sMaxR_rep, o.ToString());
            }
        }

        [Test]
        public void TestVaFundingAdustment()
        {
            var testCases = new[] {
               new { sLPurposeT = E_sLPurposeT.VaIrrrl,  sProdSpT = E_sProdSpT.Commercial,   aVServiceT = E_aVServiceT.Blank,                  aVEnt = E_aVEnt.Blank,                  DownPaymentPercentage = 0.05m, sIsNotPermanentlyAffixed = false, Result = 0.5m,  RuleDesc = "a1"}, //a
               new { sLPurposeT = E_sLPurposeT.VaIrrrl,  sProdSpT = E_sProdSpT.Manufactured, aVServiceT = E_aVServiceT.ReservistNationalGuard, aVEnt = E_aVEnt.FirstTimeUse,           DownPaymentPercentage = 0.05m, sIsNotPermanentlyAffixed = false, Result = 0.5m,  RuleDesc = "a2" }, //a
               new { sLPurposeT = E_sLPurposeT.VaIrrrl,  sProdSpT = E_sProdSpT.Rowhouse,     aVServiceT = E_aVServiceT.Veteran,                aVEnt = E_aVEnt.Blank,                  DownPaymentPercentage = 0.05m, sIsNotPermanentlyAffixed = false, Result = 0.5m,  RuleDesc = "a3" }, //a

               new { sLPurposeT = E_sLPurposeT.Purchase, sProdSpT = E_sProdSpT.Commercial,   aVServiceT = E_aVServiceT.Blank,                  aVEnt = E_aVEnt.Blank,                  DownPaymentPercentage = 0.05m, sIsNotPermanentlyAffixed = false, Result = 0m,    RuleDesc = "z0"}, //not mentioned in rules

               new { sLPurposeT = E_sLPurposeT.Purchase, sProdSpT = E_sProdSpT.Commercial,   aVServiceT = E_aVServiceT.Veteran,                aVEnt = E_aVEnt.FirstTimeUse,           DownPaymentPercentage = 0.00m, sIsNotPermanentlyAffixed = false, Result = 2.15m, RuleDesc = "b"}, //b
               new { sLPurposeT = E_sLPurposeT.Purchase, sProdSpT = E_sProdSpT.Commercial,   aVServiceT = E_aVServiceT.Veteran,                aVEnt = E_aVEnt.FirstTimeUse,           DownPaymentPercentage = 0.06m, sIsNotPermanentlyAffixed = false, Result = 1.50m, RuleDesc = "c"}, //c
               new { sLPurposeT = E_sLPurposeT.Purchase, sProdSpT = E_sProdSpT.Commercial,   aVServiceT = E_aVServiceT.Veteran,                aVEnt = E_aVEnt.FirstTimeUse,           DownPaymentPercentage = 0.20m, sIsNotPermanentlyAffixed = false, Result = 1.25m, RuleDesc = "d"}, //d
               new { sLPurposeT = E_sLPurposeT.Purchase, sProdSpT = E_sProdSpT.Commercial,   aVServiceT = E_aVServiceT.Veteran,                aVEnt = E_aVEnt.SecondAndSubsequentUse, DownPaymentPercentage = 0.00m, sIsNotPermanentlyAffixed = false, Result = 3.30m, RuleDesc = "e"},//e
               new { sLPurposeT = E_sLPurposeT.Purchase, sProdSpT = E_sProdSpT.Commercial,   aVServiceT = E_aVServiceT.Veteran,                aVEnt = E_aVEnt.SecondAndSubsequentUse, DownPaymentPercentage = 0.06m, sIsNotPermanentlyAffixed = false, Result = 1.50m, RuleDesc = "f"},//f
               new { sLPurposeT = E_sLPurposeT.Purchase, sProdSpT = E_sProdSpT.Commercial,   aVServiceT = E_aVServiceT.Veteran,                aVEnt = E_aVEnt.SecondAndSubsequentUse, DownPaymentPercentage = 0.20m, sIsNotPermanentlyAffixed = false, Result = 1.25m, RuleDesc = "g"},//g

               new { sLPurposeT = E_sLPurposeT.Purchase, sProdSpT = E_sProdSpT.Commercial,   aVServiceT = E_aVServiceT.ReservistNationalGuard, aVEnt = E_aVEnt.FirstTimeUse,           DownPaymentPercentage = 0.00m, sIsNotPermanentlyAffixed = false, Result = 2.40m, RuleDesc = "h"}, //h
               new { sLPurposeT = E_sLPurposeT.Purchase, sProdSpT = E_sProdSpT.Commercial,   aVServiceT = E_aVServiceT.ReservistNationalGuard, aVEnt = E_aVEnt.FirstTimeUse,           DownPaymentPercentage = 0.06m, sIsNotPermanentlyAffixed = false, Result = 1.75m, RuleDesc = "i"}, //i
               new { sLPurposeT = E_sLPurposeT.Purchase, sProdSpT = E_sProdSpT.Commercial,   aVServiceT = E_aVServiceT.ReservistNationalGuard, aVEnt = E_aVEnt.FirstTimeUse,           DownPaymentPercentage = 0.20m, sIsNotPermanentlyAffixed = false, Result = 1.50m, RuleDesc = "j"}, //j
               new { sLPurposeT = E_sLPurposeT.Purchase, sProdSpT = E_sProdSpT.Commercial,   aVServiceT = E_aVServiceT.ReservistNationalGuard, aVEnt = E_aVEnt.SecondAndSubsequentUse, DownPaymentPercentage = 0.00m, sIsNotPermanentlyAffixed = false, Result = 3.30m, RuleDesc = "k"}, //k
               new { sLPurposeT = E_sLPurposeT.Purchase, sProdSpT = E_sProdSpT.Commercial,   aVServiceT = E_aVServiceT.ReservistNationalGuard, aVEnt = E_aVEnt.SecondAndSubsequentUse, DownPaymentPercentage = 0.06m, sIsNotPermanentlyAffixed = false, Result = 1.75m, RuleDesc = "l"}, //l 
               new { sLPurposeT = E_sLPurposeT.Purchase, sProdSpT = E_sProdSpT.Commercial,   aVServiceT = E_aVServiceT.ReservistNationalGuard, aVEnt = E_aVEnt.SecondAndSubsequentUse, DownPaymentPercentage = 0.20m, sIsNotPermanentlyAffixed = false, Result = 1.50m, RuleDesc = "m"}, //m

               new { sLPurposeT = E_sLPurposeT.Purchase, sProdSpT = E_sProdSpT.Manufactured, aVServiceT = E_aVServiceT.ReservistNationalGuard, aVEnt = E_aVEnt.SecondAndSubsequentUse, DownPaymentPercentage = 0.20m, sIsNotPermanentlyAffixed = true,  Result = 1.00m, RuleDesc = "r1"}, //r 
               new { sLPurposeT = E_sLPurposeT.Purchase, sProdSpT = E_sProdSpT.Manufactured, aVServiceT = E_aVServiceT.Veteran,                aVEnt = E_aVEnt.FirstTimeUse,           DownPaymentPercentage = 0.20m, sIsNotPermanentlyAffixed = true,  Result = 1.00m, RuleDesc = "r2"}, //r 

               new { sLPurposeT = E_sLPurposeT.Refin,    sProdSpT = E_sProdSpT.Manufactured, aVServiceT = E_aVServiceT.Veteran,                aVEnt = E_aVEnt.FirstTimeUse,           DownPaymentPercentage = 0.20m, sIsNotPermanentlyAffixed = true,  Result = 1.00m, RuleDesc = "r3"}, //r 
                        
               new { sLPurposeT = E_sLPurposeT.Refin,    sProdSpT = E_sProdSpT.Commercial,   aVServiceT = E_aVServiceT.Veteran,                aVEnt = E_aVEnt.FirstTimeUse,           DownPaymentPercentage = 0.20m, sIsNotPermanentlyAffixed = false, Result = 2.15m, RuleDesc = "n"}, //n 
               new { sLPurposeT = E_sLPurposeT.Refin,    sProdSpT = E_sProdSpT.Commercial,   aVServiceT = E_aVServiceT.Veteran,                aVEnt = E_aVEnt.SecondAndSubsequentUse, DownPaymentPercentage = 0.20m, sIsNotPermanentlyAffixed = false, Result = 3.30m, RuleDesc = "p"}, //p 
               new { sLPurposeT = E_sLPurposeT.Refin,    sProdSpT = E_sProdSpT.Commercial,   aVServiceT = E_aVServiceT.Veteran,                aVEnt = E_aVEnt.Blank,                  DownPaymentPercentage = 0.20m, sIsNotPermanentlyAffixed = false, Result = 0m,    RuleDesc = "z1"}, // not in rules
               
               new { sLPurposeT = E_sLPurposeT.Refin,    sProdSpT = E_sProdSpT.Commercial,   aVServiceT = E_aVServiceT.ReservistNationalGuard, aVEnt = E_aVEnt.FirstTimeUse,           DownPaymentPercentage = 0.20m, sIsNotPermanentlyAffixed = false, Result = 2.4m,  RuleDesc = "o"}, //o
               new { sLPurposeT = E_sLPurposeT.Refin,    sProdSpT = E_sProdSpT.Commercial,   aVServiceT = E_aVServiceT.ReservistNationalGuard, aVEnt = E_aVEnt.SecondAndSubsequentUse, DownPaymentPercentage = 0.20m, sIsNotPermanentlyAffixed = false, Result = 3.3m,  RuleDesc = "q1"}, //q
               new { sLPurposeT = E_sLPurposeT.Refin,    sProdSpT = E_sProdSpT.Commercial,   aVServiceT = E_aVServiceT.Blank,                  aVEnt = E_aVEnt.SecondAndSubsequentUse, DownPaymentPercentage = 0.20m, sIsNotPermanentlyAffixed = false, Result = 3.3m,  RuleDesc = "q2"}, //q
            };

            foreach (var test in testCases)
            {
                Assert.That(CPageBase.GetVaFundingAdjustment(test.sLPurposeT, test.sProdSpT, test.aVServiceT, test.aVEnt, test.DownPaymentPercentage, test.sIsNotPermanentlyAffixed).Equals(test.Result));
            }
        }

        [Test]
        public void Test_sHmdaPropT()
        {
            var testCases = new[] {
                new { sGseSpT = E_sGseSpT.Attached, sUnitsNum = 1, Expected_sHmdaPropT = E_sHmdaPropT.OneTo4Family},
                new { sGseSpT = E_sGseSpT.Condominium, sUnitsNum = 1, Expected_sHmdaPropT = E_sHmdaPropT.OneTo4Family},
                new { sGseSpT = E_sGseSpT.Cooperative, sUnitsNum = 1, Expected_sHmdaPropT = E_sHmdaPropT.OneTo4Family},
                new { sGseSpT = E_sGseSpT.Detached, sUnitsNum = 1, Expected_sHmdaPropT = E_sHmdaPropT.OneTo4Family},
                new { sGseSpT = E_sGseSpT.DetachedCondominium, sUnitsNum = 1, Expected_sHmdaPropT = E_sHmdaPropT.OneTo4Family},
                new { sGseSpT = E_sGseSpT.HighRiseCondominium, sUnitsNum = 1, Expected_sHmdaPropT = E_sHmdaPropT.OneTo4Family},
                new { sGseSpT = E_sGseSpT.ManufacturedHomeCondominium, sUnitsNum = 1, Expected_sHmdaPropT = E_sHmdaPropT.ManufacturedHousing},
                new { sGseSpT = E_sGseSpT.ManufacturedHomeMultiwide, sUnitsNum = 1, Expected_sHmdaPropT = E_sHmdaPropT.ManufacturedHousing},
                new { sGseSpT = E_sGseSpT.ManufacturedHousing, sUnitsNum = 1, Expected_sHmdaPropT = E_sHmdaPropT.ManufacturedHousing},
                new { sGseSpT = E_sGseSpT.ManufacturedHousingSingleWide, sUnitsNum = 1, Expected_sHmdaPropT = E_sHmdaPropT.ManufacturedHousing},
                new { sGseSpT = E_sGseSpT.Modular, sUnitsNum = 1, Expected_sHmdaPropT = E_sHmdaPropT.OneTo4Family},
                new { sGseSpT = E_sGseSpT.PUD, sUnitsNum = 1, Expected_sHmdaPropT = E_sHmdaPropT.OneTo4Family},
                new { sGseSpT = E_sGseSpT.LeaveBlank, sUnitsNum = 1, Expected_sHmdaPropT = E_sHmdaPropT.LeaveBlank},

                new { sGseSpT = E_sGseSpT.Attached, sUnitsNum = 4, Expected_sHmdaPropT = E_sHmdaPropT.OneTo4Family},
                new { sGseSpT = E_sGseSpT.Condominium, sUnitsNum = 4, Expected_sHmdaPropT = E_sHmdaPropT.OneTo4Family},
                new { sGseSpT = E_sGseSpT.Cooperative, sUnitsNum = 4, Expected_sHmdaPropT = E_sHmdaPropT.OneTo4Family},
                new { sGseSpT = E_sGseSpT.Detached, sUnitsNum = 4, Expected_sHmdaPropT = E_sHmdaPropT.OneTo4Family},
                new { sGseSpT = E_sGseSpT.DetachedCondominium, sUnitsNum = 4, Expected_sHmdaPropT = E_sHmdaPropT.OneTo4Family},
                new { sGseSpT = E_sGseSpT.HighRiseCondominium, sUnitsNum = 4, Expected_sHmdaPropT = E_sHmdaPropT.OneTo4Family},
                new { sGseSpT = E_sGseSpT.ManufacturedHomeCondominium, sUnitsNum = 4, Expected_sHmdaPropT = E_sHmdaPropT.ManufacturedHousing},
                new { sGseSpT = E_sGseSpT.ManufacturedHomeMultiwide, sUnitsNum = 4, Expected_sHmdaPropT = E_sHmdaPropT.ManufacturedHousing},
                new { sGseSpT = E_sGseSpT.ManufacturedHousing, sUnitsNum = 4, Expected_sHmdaPropT = E_sHmdaPropT.ManufacturedHousing},
                new { sGseSpT = E_sGseSpT.ManufacturedHousingSingleWide, sUnitsNum = 4, Expected_sHmdaPropT = E_sHmdaPropT.ManufacturedHousing},
                new { sGseSpT = E_sGseSpT.Modular, sUnitsNum = 4, Expected_sHmdaPropT = E_sHmdaPropT.OneTo4Family},
                new { sGseSpT = E_sGseSpT.PUD, sUnitsNum = 4, Expected_sHmdaPropT = E_sHmdaPropT.OneTo4Family},
                new { sGseSpT = E_sGseSpT.LeaveBlank, sUnitsNum = 4, Expected_sHmdaPropT = E_sHmdaPropT.LeaveBlank},

                new { sGseSpT = E_sGseSpT.Attached, sUnitsNum = 5, Expected_sHmdaPropT = E_sHmdaPropT.MultiFamiliy},
                new { sGseSpT = E_sGseSpT.Condominium, sUnitsNum = 5, Expected_sHmdaPropT = E_sHmdaPropT.MultiFamiliy},
                new { sGseSpT = E_sGseSpT.Cooperative, sUnitsNum = 5, Expected_sHmdaPropT = E_sHmdaPropT.MultiFamiliy},
                new { sGseSpT = E_sGseSpT.Detached, sUnitsNum = 5, Expected_sHmdaPropT = E_sHmdaPropT.MultiFamiliy},
                new { sGseSpT = E_sGseSpT.DetachedCondominium, sUnitsNum = 5, Expected_sHmdaPropT = E_sHmdaPropT.MultiFamiliy},
                new { sGseSpT = E_sGseSpT.HighRiseCondominium, sUnitsNum = 5, Expected_sHmdaPropT = E_sHmdaPropT.MultiFamiliy},
                new { sGseSpT = E_sGseSpT.ManufacturedHomeCondominium, sUnitsNum = 5, Expected_sHmdaPropT = E_sHmdaPropT.ManufacturedHousing},
                new { sGseSpT = E_sGseSpT.ManufacturedHomeMultiwide, sUnitsNum = 5, Expected_sHmdaPropT = E_sHmdaPropT.ManufacturedHousing},
                new { sGseSpT = E_sGseSpT.ManufacturedHousing, sUnitsNum = 5, Expected_sHmdaPropT = E_sHmdaPropT.ManufacturedHousing},
                new { sGseSpT = E_sGseSpT.ManufacturedHousingSingleWide, sUnitsNum = 5, Expected_sHmdaPropT = E_sHmdaPropT.ManufacturedHousing},
                new { sGseSpT = E_sGseSpT.Modular, sUnitsNum = 5, Expected_sHmdaPropT = E_sHmdaPropT.MultiFamiliy},
                new { sGseSpT = E_sGseSpT.PUD, sUnitsNum = 5, Expected_sHmdaPropT = E_sHmdaPropT.MultiFamiliy},
                new { sGseSpT = E_sGseSpT.LeaveBlank, sUnitsNum = 5, Expected_sHmdaPropT = E_sHmdaPropT.LeaveBlank},

            };

            CPageData dataLoan = CreateDataObject();
            dataLoan.InitLoad();

            dataLoan.sHmdaPropTLckd = false;

            foreach (var testCase in testCases)
            {
                dataLoan.sGseSpT = testCase.sGseSpT;
                dataLoan.sUnitsNum = testCase.sUnitsNum;
                Assert.AreEqual(testCase.Expected_sHmdaPropT, dataLoan.sHmdaPropT, testCase.ToString());
            }

            dataLoan.sHmdaPropTLckd = true;
            dataLoan.sHmdaPropT = E_sHmdaPropT.ManufacturedHousing;
            foreach (var testCase in testCases)
            {
                dataLoan.sGseSpT = testCase.sGseSpT;
                dataLoan.sUnitsNum = testCase.sUnitsNum;
                Assert.AreEqual(E_sHmdaPropT.ManufacturedHousing, dataLoan.sHmdaPropT);
            }

        }

        [Test]
        public void sQualIR_461628()
        {
            // This is to test the new behaviors for case 461628.  The intent
            // of this case is to allow qual rate to take note rate value when
            // it is not being used.

            //Inputs Results
            //Test Scenario #	Loan Amount	Note Rate	Note Rate I/O?	Qual Rate	Qual Rate I/O?	Term	Payments Differ?	Note Payment	Qual Payment
            //1                 $400,000.00 4.000 %     F               4.000 %     F               360     F                   $1,909.66       Note Payment
            //2                 $300,000.00 4.000 %     F               4.000 %     T               360     T                   $1,432.25       $1,000.00
            //3                 $400,000.00 4.000 %     T               4.000 %     F               240     T                   $1,333.33       $2,423.92
            //4                 $300,000.00 4.000 %     T               4.000 %     T               240     F                   $1,000.00       Note Payment
            //5                 $400,000.00 4.000 %     F               6.000 %     F               360     T                   $1,909.66       $2,398.20
            //6                 $300,000.00 4.000 %     F               6.000 %     T               360     T                   $1,432.25       $1,500.00
            //7                 $400,000.00 4.000 %     T               6.000 %     F               240     T                   $1,333.33       $2,865.72
            //8                 $300,000.00 4.000 %     T               6.000 %     T               240     T                   $1,000.00       $1,500.00

            var testCases = new[] {
                new {
                    scenarioNumber = 1,
                    sLAmtCalc = 400000.00m,
                    sNoteIR = 4.000m,
                    sIsIOnly = false,
                    sQualIR = 4.000m,
                    sIsQRateIOnly = false,
                    sTerm = 360,
                    sIsProThisMQualDiffsProThisMPmt = false,
                    sMonthlyPmt = 1909.66m,
                    sMonthlyPmtQual = 1909.66m
                },
                
                new {
                    scenarioNumber = 2,
                    sLAmtCalc = 300000.00m,
                    sNoteIR = 4.000m,
                    sIsIOnly = false,
                    sQualIR = 4.000m,
                    sIsQRateIOnly = true,
                    sTerm = 360,
                    sIsProThisMQualDiffsProThisMPmt = true,
                    sMonthlyPmt = 1432.25m,
                    sMonthlyPmtQual = 1000.00m
                },

              new {
                    scenarioNumber = 3,
                    sLAmtCalc = 400000.00m,
                    sNoteIR = 4.000m,
                    sIsIOnly = true,
                    sQualIR = 4.000m,
                    sIsQRateIOnly = false,
                    sTerm = 240,
                    sIsProThisMQualDiffsProThisMPmt = true,
                    sMonthlyPmt = 1333.33m,
                    sMonthlyPmtQual = 2423.92m
                },

              new {
                    scenarioNumber = 4,
                    sLAmtCalc = 300000.00m,
                    sNoteIR = 4.000m,
                    sIsIOnly = true,
                    sQualIR = 4.000m,
                    sIsQRateIOnly = true,
                    sTerm = 240,
                    sIsProThisMQualDiffsProThisMPmt = false,
                    sMonthlyPmt = 1000.00m,
                    sMonthlyPmtQual = 1000.00m
                },

              new {
                    scenarioNumber = 5,
                    sLAmtCalc = 400000.00m,
                    sNoteIR = 4.000m,
                    sIsIOnly = false,
                    sQualIR = 6.000m,
                    sIsQRateIOnly = false,
                    sTerm = 360,
                    sIsProThisMQualDiffsProThisMPmt = true,
                    sMonthlyPmt = 1909.66m,
                    sMonthlyPmtQual = 2398.20m
                },

              new {
                    scenarioNumber = 6,
                    sLAmtCalc = 300000.00m,
                    sNoteIR = 4.000m,
                    sIsIOnly = false,
                    sQualIR = 6.000m,
                    sIsQRateIOnly = true,
                    sTerm = 360,
                    sIsProThisMQualDiffsProThisMPmt = true,
                    sMonthlyPmt = 1432.25m,
                    sMonthlyPmtQual = 1500.00m
                },

              new {
                    scenarioNumber = 7,
                    sLAmtCalc = 400000.00m,
                    sNoteIR = 4.000m,
                    sIsIOnly = true,
                    sQualIR = 6.000m,
                    sIsQRateIOnly = false,
                    sTerm = 240,
                    sIsProThisMQualDiffsProThisMPmt = true,
                    sMonthlyPmt = 1333.33m,
                    sMonthlyPmtQual = 2865.72m
                },

              new {
                    scenarioNumber = 8,
                    sLAmtCalc = 300000.00m,
                    sNoteIR = 4.000m,
                    sIsIOnly = true,
                    sQualIR = 6.000m,
                    sIsQRateIOnly = true,
                    sTerm = 240,
                    sIsProThisMQualDiffsProThisMPmt = true,
                    sMonthlyPmt = 1000.00m,
                    sMonthlyPmtQual = 1500.00m
                }
            };

            CPageData dataLoan = CreateDataObject();
            dataLoan.InitLoad();

            var errors = string.Empty;
            foreach (var testCase in testCases)
            {
                dataLoan.sUseQualRate = true;
                dataLoan.sLAmtLckd = true;
                dataLoan.sLAmtCalc = testCase.sLAmtCalc;
                dataLoan.sNoteIR = testCase.sNoteIR;
                dataLoan.sIOnlyMon = testCase.sIsIOnly ? 12 : 0;
                dataLoan.sQualIRLckd = true;
                dataLoan.sQualIR = testCase.sQualIR;
                dataLoan.sIsQRateIOnly = testCase.sIsQRateIOnly;
                dataLoan.sTerm = testCase.sTerm;

                if ( testCase.sIsProThisMQualDiffsProThisMPmt != dataLoan.sIsProThisMQualDiffsProThisMPmt)
                {
                    errors += ($"Fail test:{testCase.scenarioNumber}, Value: {nameof(testCase.sIsProThisMQualDiffsProThisMPmt)}. Expected: {testCase.sIsProThisMQualDiffsProThisMPmt}. But was: {dataLoan.sIsProThisMQualDiffsProThisMPmt}.{System.Environment.NewLine}");
                }

                if (testCase.sMonthlyPmt != testCase.sMonthlyPmt)
                {
                    errors += ($"Fail test:{testCase.scenarioNumber}, Value: {nameof(testCase.sMonthlyPmt)}. Expected: {testCase.sMonthlyPmt}. But was: {dataLoan.sMonthlyPmt}.{System.Environment.NewLine}");
                }

                if (testCase.sMonthlyPmtQual != testCase.sMonthlyPmtQual)
                {
                    errors += ($"Fail test:{testCase.scenarioNumber}, Value: {nameof(testCase.sMonthlyPmtQual)}. Expected: {testCase.sMonthlyPmtQual}. But was: {dataLoan.sMonthlyPmtQual}.{System.Environment.NewLine}");
                }
            }

            Assert.That(errors.Length == 0, errors);

        }
    }
}
