﻿using System.Collections.Generic;
using DataAccess;
using LendersOffice.Constants;
using LendOSolnTest.Common;
using LendOSolnTest.Fakes;
using NUnit.Framework;

namespace LendOSolnTest.Loan
{
    [TestFixture]
    public class IncomeCalculationsTest
    {
        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            LoginTools.LoginAs(TestAccountType.LoTest001);
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
            FakePageDataUtilities.Instance.Reset();
        }

        private void SetupBaseLoanAndApp0(CPageData dataLoan)
        {
            CAppData dataApp0 = dataLoan.GetAppData(0);

            dataApp0.aBFirstNm = "Alice";
            dataApp0.aCFirstNm = "Bob";

            dataApp0.aBBaseI = 40000.00M;
            dataApp0.aBOvertimeI = 20000.00M;
            dataApp0.aBBonusesI = 10000.00M;
            dataApp0.aBCommisionI = 4000.00M;
            dataApp0.aBDividendI = 2000.00M;
            dataApp0.aOtherIncomeList[0].Amount = 1000.00M;
            dataApp0.aOtherIncomeList[0].IsForCoBorrower = false;

            dataApp0.aCBaseI = 400.00M;
            dataApp0.aCOvertimeI = 200.00M;
            dataApp0.aCBonusesI = 100.00M;
            dataApp0.aCCommisionI = 40.00M;
            dataApp0.aCDividendI = 20.00M;
            dataApp0.aOtherIncomeList[1].Amount = 10.00M;
            dataApp0.aOtherIncomeList[1].IsForCoBorrower = true;

            dataLoan.sSpCountRentalIForPrimaryResidToo = true;
            dataLoan.sSpGrossRent_rep = "$100,000.00";
        }

        private void SetupApp1(CAppData dataApp)
        {
            dataApp.aBFirstNm = "Carol";

            dataApp.aBBaseI = 4.00M;
            dataApp.aBOvertimeI = 2.00M;
            dataApp.aBBonusesI = 1.00M;
            dataApp.aBCommisionI = 0.40M;
            dataApp.aBDividendI = 0.20M;
            dataApp.aOtherIncomeList[0].Amount = 0.10M;
            dataApp.aOtherIncomeList[0].IsForCoBorrower = false;
        }

        private void SetupApp2(CAppData dataApp)
        {
            dataApp.aBFirstNm = "Dennis";

            dataApp.aBBaseI      = 4000000.0M;
            dataApp.aBOvertimeI  = 2000000.00M;
            dataApp.aBBonusesI   = 1000000.00M;
            dataApp.aBCommisionI = 40000000.0M;
            dataApp.aBDividendI  = 20000000.00M;
            dataApp.aOtherIncomeList[0].Amount = 100000000.00M;
            dataApp.aOtherIncomeList[0].IsForCoBorrower = false;
        }

        private void SetupReoRecords(CPageData dataLoan)
        {
            CAppData dataApp0 = dataLoan.GetAppData(0);
            CAppData dataApp1 = dataLoan.GetAppData(1);

            IRealEstateOwned reo;

            reo = dataApp0.aReCollection.AddRegularRecord();
            reo.StatT = E_ReoStatusT.Rental;
            reo.OccR = 100;
            reo.ReOwnerT = E_ReOwnerT.Borrower;
            reo.GrossRentI = 0.07M;
            reo.HExp = 0.00M;

            reo = dataApp0.aReCollection.AddRegularRecord();
            reo.StatT = E_ReoStatusT.Rental;
            reo.OccR = 100;
            reo.ReOwnerT = E_ReOwnerT.Joint;
            reo.GrossRentI = 0.01M;
            reo.HExp = 0.02M;

            reo = dataApp0.aReCollection.AddRegularRecord();
            reo.StatT = E_ReoStatusT.Rental;
            reo.OccR = 100;
            reo.ReOwnerT = E_ReOwnerT.CoBorrower;
            reo.GrossRentI = 0.04M;
            reo.HExp = 0.02M;

            reo = dataApp1.aReCollection.AddRegularRecord();
            reo.StatT = E_ReoStatusT.Rental;
            reo.OccR = 100;
            reo.ReOwnerT = E_ReOwnerT.Borrower;
            reo.GrossRentI = 0.01M;
            reo.HExp = 0.02M;

            dataApp0.aReCollection.Flush();
            dataApp1.aReCollection.Flush();
        }

        [Test]
        public void IncomeNonReoSubtotalsTest()
        {
            CPageData dataLoan = FakePageData.Create(appCount: 2);
            dataLoan.InitLoad();

            CAppData dataApp0 = dataLoan.GetAppData(0);
            CAppData dataApp1 = dataLoan.GetAppData(1);

            SetupBaseLoanAndApp0(dataLoan);
            SetupApp1(dataApp1);

            Assert.AreEqual(177000.00M, dataApp0.aBTotI);
            Assert.AreEqual(770.00M, dataApp0.aCTotI);

            Assert.AreEqual(40400.00M, dataApp0.aTotBaseI);
            Assert.AreEqual(20200.00M, dataApp0.aTotOvertimeI);
            Assert.AreEqual(10100.00M, dataApp0.aTotBonusesI);
            Assert.AreEqual(4040.00M, dataApp0.aTotCommisionI);
            Assert.AreEqual(2020.00M, dataApp0.aTotDividendI);
            Assert.AreEqual(1010.00M, dataApp0.aTotOI);
            Assert.AreEqual(177770.00M, dataApp0.aTotI);

            Assert.AreEqual(7.70M, dataApp1.aBTotI);
            Assert.AreEqual(0, dataApp1.aCTotI);

            Assert.AreEqual(4.00M, dataApp1.aTotBaseI);
            Assert.AreEqual(2.00M, dataApp1.aTotOvertimeI);
            Assert.AreEqual(1.00M, dataApp1.aTotBonusesI);
            Assert.AreEqual(0.40M, dataApp1.aTotCommisionI);
            Assert.AreEqual(0.20M, dataApp1.aTotDividendI);
            Assert.AreEqual(0.10M, dataApp1.aTotOI);
            Assert.AreEqual(7.70M, dataApp1.aTotI);

            Assert.AreEqual(177777.70M, dataLoan.sLTotI);

            Assert.AreEqual(0, dataApp0.aBNetRentI);
            Assert.AreEqual(0, dataApp0.aCNetRentI);
            Assert.AreEqual(0, dataApp0.aTotNetRentI);
            Assert.AreEqual(0, dataApp1.aBNetRentI);

            Assert.AreEqual(37000.00M, dataApp0.aBNonbaseI);
            Assert.AreEqual(370.00M, dataApp0.aCNonbaseI);
            Assert.AreEqual(37370.00M, dataApp0.aTotNonbaseI);

            Assert.AreEqual(100000.00M, dataApp0.aBSpPosCf);
            Assert.AreEqual(0, dataApp0.aCSpPosCf);
            Assert.AreEqual(100000.00M, dataApp0.aTotSpPosCf);

            Assert.AreEqual(177000.00M, dataApp0.aTransmBTotI);
            Assert.AreEqual(770.00M, dataApp0.aTransmCTotI);
            Assert.AreEqual(177770.00M, dataApp0.aTransmTotI);

            Assert.AreEqual(3.70M, dataApp1.aBNonbaseI);
            Assert.AreEqual(0, dataApp1.aCNonbaseI);
            Assert.AreEqual(3.70M, dataApp1.aNonbaseI);

            Assert.AreEqual(0, dataApp1.aBSpPosCf);
            Assert.AreEqual(0, dataApp1.aCSpPosCf);
            Assert.AreEqual(0, dataApp1.aTotSpPosCf);

            Assert.AreEqual(7.70M, dataApp1.aTransmBTotI);
            Assert.AreEqual(0, dataApp1.aTransmCTotI);
            Assert.AreEqual(7.70M, dataApp1.aTransmTotI);

            Assert.AreEqual(0, dataLoan.sLTotNetRentI1003);
        }

        [Test]
        public void TestPrimAppNonReoTotals()
        {
            CPageData dataLoan = FakePageData.Create(appCount: 3);
            dataLoan.InitLoad();

            CAppData dataApp0 = dataLoan.GetAppData(0);
            CAppData dataApp1 = dataLoan.GetAppData(1);

            SetupBaseLoanAndApp0(dataLoan);
            SetupApp1(dataApp1);

            Assert.AreEqual(dataApp0.aTotBaseI, dataLoan.sPrimAppTotBaseI);
            Assert.AreEqual(dataApp0.aTotNonbaseI, dataLoan.sPrimAppTotNonbaseI);
            Assert.AreEqual(dataApp0.aTotSpPosCf, dataLoan.sPrimAppTotSpPosCf);
            Assert.AreEqual(dataApp0.aTotI, dataLoan.sPrimAppTotI);

            Assert.AreEqual(dataApp1.aTotBaseI, dataLoan.sNprimAppsTotBaseI);
            Assert.AreEqual(dataApp1.aTotNonbaseI, dataLoan.sNprimAppsTotNonbaseI);
            Assert.AreEqual(dataApp1.aTotSpPosCf, dataLoan.sNprimAppsTotSpPosCf);
            Assert.AreEqual(dataApp1.aTotI, dataLoan.sNprimAppsTotI);

            CAppData dataApp2 = dataLoan.GetAppData(2);

            SetupApp2(dataApp2);

            Assert.AreEqual(dataApp0.aTotBaseI, dataLoan.sPrimAppTotBaseI);
            Assert.AreEqual(dataApp0.aTotNonbaseI, dataLoan.sPrimAppTotNonbaseI);
            Assert.AreEqual(dataApp0.aTotSpPosCf, dataLoan.sPrimAppTotSpPosCf);
            Assert.AreEqual(dataApp0.aTotI, dataLoan.sPrimAppTotI);

            Assert.AreEqual(dataApp1.aTotBaseI + dataApp2.aTotBaseI, dataLoan.sNprimAppsTotBaseI);
            Assert.AreEqual(dataApp1.aTotNonbaseI + dataApp2.aTotNonbaseI, dataLoan.sNprimAppsTotNonbaseI);
            Assert.AreEqual(dataApp1.aTotSpPosCf + dataApp2.aTotSpPosCf, dataLoan.sNprimAppsTotSpPosCf);
            Assert.AreEqual(dataApp1.aTotI + dataApp2.aTotI, dataLoan.sNprimAppsTotI);
        }

        [Test]
        public void TestPrimAppReoTotals()
        {
            CPageData dataLoan = FakePageData.Create(appCount: 3);
            dataLoan.InitLoad();

            CAppData dataApp0 = dataLoan.GetAppData(0);
            CAppData dataApp1 = dataLoan.GetAppData(1);

            SetupBaseLoanAndApp0(dataLoan);
            SetupApp1(dataApp1);
            SetupReoRecords(dataLoan);

            Assert.AreEqual(dataApp0.aTotBaseI, dataLoan.sPrimAppTotBaseI);
            Assert.AreEqual(dataApp0.aTotNonbaseI, dataLoan.sPrimAppTotNonbaseI);
            Assert.AreEqual(dataApp0.aTotSpPosCf, dataLoan.sPrimAppTotSpPosCf);
            Assert.AreEqual(dataApp0.aTotI, dataLoan.sPrimAppTotI);

            Assert.AreEqual(dataApp1.aTotBaseI, dataLoan.sNprimAppsTotBaseI);
            Assert.AreEqual(dataApp1.aTotNonbaseI, dataLoan.sNprimAppsTotNonbaseI);
            Assert.AreEqual(dataApp1.aTotSpPosCf, dataLoan.sNprimAppsTotSpPosCf);
            Assert.AreEqual(dataApp1.aTotI, dataLoan.sNprimAppsTotI);

            CAppData dataApp2 = dataLoan.GetAppData(2);

            SetupApp2(dataApp2);

            Assert.AreEqual(dataApp0.aTotBaseI, dataLoan.sPrimAppTotBaseI);
            Assert.AreEqual(dataApp0.aTotNonbaseI, dataLoan.sPrimAppTotNonbaseI);
            Assert.AreEqual(dataApp0.aTotSpPosCf, dataLoan.sPrimAppTotSpPosCf);
            Assert.AreEqual(dataApp0.aTotI, dataLoan.sPrimAppTotI);

            Assert.AreEqual(dataApp1.aTotBaseI + dataApp2.aTotBaseI, dataLoan.sNprimAppsTotBaseI);
            Assert.AreEqual(dataApp1.aTotNonbaseI + dataApp2.aTotNonbaseI, dataLoan.sNprimAppsTotNonbaseI);
            Assert.AreEqual(dataApp1.aTotSpPosCf + dataApp2.aTotSpPosCf, dataLoan.sNprimAppsTotSpPosCf);
            Assert.AreEqual(dataApp1.aTotI + dataApp2.aTotI, dataLoan.sNprimAppsTotI);
        }

        [Test]
        public void ReoOnlyPrimaryBorrReoIncTest()
        {
            CPageData dataLoan = FakePageData.Create(appCount: 2);
            dataLoan.InitLoad();

            CAppData dataApp0 = dataLoan.GetAppData(0);
            CAppData dataApp1 = dataLoan.GetAppData(1);

            SetupBaseLoanAndApp0(dataLoan);
            SetupApp1(dataApp1);

            IRealEstateOwned reo;

            reo = dataApp0.aReCollection.AddRegularRecord();
            reo.StatT = E_ReoStatusT.Rental;
            reo.OccR = 100;
            reo.ReOwnerT = E_ReOwnerT.Borrower;
            reo.GrossRentI = 0.07M;
            reo.HExp = 0.00M;

            reo = dataApp0.aReCollection.AddRegularRecord();
            reo.StatT = E_ReoStatusT.Rental;
            reo.OccR = 100;
            reo.ReOwnerT = E_ReOwnerT.Joint;
            reo.GrossRentI = 0.01M;
            reo.HExp = 0.02M;

            dataApp0.aReCollection.Flush();

            Assert.AreEqual(177000.06M, dataApp0.aBTotI);
            Assert.AreEqual(770.00M, dataApp0.aCTotI);

            Assert.AreEqual(40400.00M, dataApp0.aTotBaseI);
            Assert.AreEqual(20200.00M, dataApp0.aTotOvertimeI);
            Assert.AreEqual(10100.00M, dataApp0.aTotBonusesI);
            Assert.AreEqual(4040.00M, dataApp0.aTotCommisionI);
            Assert.AreEqual(2020.00M, dataApp0.aTotDividendI);
            Assert.AreEqual(1010.00M, dataApp0.aTotOI);
            Assert.AreEqual(177770.06M, dataApp0.aTotI);

            Assert.AreEqual(7.70M, dataApp1.aBTotI);
            Assert.AreEqual(0, dataApp1.aCTotI);

            Assert.AreEqual(4.00M, dataApp1.aTotBaseI);
            Assert.AreEqual(2.00M, dataApp1.aTotOvertimeI);
            Assert.AreEqual(1.00M, dataApp1.aTotBonusesI);
            Assert.AreEqual(0.40M, dataApp1.aTotCommisionI);
            Assert.AreEqual(0.20M, dataApp1.aTotDividendI);
            Assert.AreEqual(0.10M, dataApp1.aTotOI);
            Assert.AreEqual(7.70M, dataApp1.aTotI);

            Assert.AreEqual(177777.76M, dataLoan.sLTotI);

            Assert.AreEqual(0.06M, dataApp0.aBNetRentI);
            Assert.AreEqual(0, dataApp0.aCNetRentI);
            Assert.AreEqual(0.06M, dataApp0.aTotNetRentI);
            Assert.AreEqual(0, dataApp1.aBNetRentI);

            Assert.AreEqual(37000.06M, dataApp0.aBNonbaseI);
            Assert.AreEqual(370.00M, dataApp0.aCNonbaseI);
            Assert.AreEqual(37370.06M, dataApp0.aTotNonbaseI);

            Assert.AreEqual(100000.00M, dataApp0.aBSpPosCf);
            Assert.AreEqual(0, dataApp0.aCSpPosCf);
            Assert.AreEqual(100000.00M, dataApp0.aTotSpPosCf);

            Assert.AreEqual(177000.06M, dataApp0.aTransmBTotI);
            Assert.AreEqual(770.00M, dataApp0.aTransmCTotI);
            Assert.AreEqual(177770.06M, dataApp0.aTransmTotI);

            Assert.AreEqual(3.70M, dataApp1.aBNonbaseI);
            Assert.AreEqual(0, dataApp1.aCNonbaseI);
            Assert.AreEqual(3.70M, dataApp1.aNonbaseI);

            Assert.AreEqual(0, dataApp1.aBSpPosCf);
            Assert.AreEqual(0, dataApp1.aCSpPosCf);
            Assert.AreEqual(0, dataApp1.aTotSpPosCf);

            Assert.AreEqual(7.70M, dataApp1.aTransmBTotI);
            Assert.AreEqual(0, dataApp1.aTransmCTotI);
            Assert.AreEqual(7.70M, dataApp1.aTransmTotI);

            Assert.AreEqual(0.06M, dataLoan.sLTotNetRentI1003);
        }

        /// <summary>
        /// The commented out lines represent the fields that are expected to change
        /// when the net rental income/expense is changed to net at the loan level
        /// instead of the app level. The expected values in the commented out asserts
        /// are the expected totals for after those changes are made.
        /// </summary>
        [Test]
        public void ReoOnlyVariousAllIncTest()
        {
            CPageData dataLoan = FakePageData.Create(appCount: 2);
            dataLoan.InitLoad();

            CAppData dataApp0 = dataLoan.GetAppData(0);
            CAppData dataApp1 = dataLoan.GetAppData(1);

            SetupBaseLoanAndApp0(dataLoan);
            SetupApp1(dataApp1);

            IRealEstateOwned reo;

            reo = dataApp0.aReCollection.AddRegularRecord();
            reo.StatT = E_ReoStatusT.Rental;
            reo.OccR = 100;
            reo.ReOwnerT = E_ReOwnerT.Borrower;
            reo.GrossRentI = 0.07M;
            reo.HExp = 0.03M;

            reo = dataApp0.aReCollection.AddRegularRecord();
            reo.StatT = E_ReoStatusT.Rental;
            reo.OccR = 100;
            reo.ReOwnerT = E_ReOwnerT.CoBorrower;
            reo.GrossRentI = 0.03M;
            reo.HExp = 0.01M;

            reo = dataApp1.aReCollection.AddRegularRecord();
            reo.StatT = E_ReoStatusT.Rental;
            reo.OccR = 100;
            reo.ReOwnerT = E_ReOwnerT.Borrower;
            reo.GrossRentI = 0.01M;
            reo.HExp = 0.00M;

            dataApp0.aReCollection.Flush();
            dataApp1.aReCollection.Flush();

            //Assert.AreEqual(177000.07M, dataApp0.aBTotI);
            //Assert.AreEqual(770.00M, dataApp0.aCTotI);

            Assert.AreEqual(40400.00M, dataApp0.aTotBaseI);
            Assert.AreEqual(20200.00M, dataApp0.aTotOvertimeI);
            Assert.AreEqual(10100.00M, dataApp0.aTotBonusesI);
            Assert.AreEqual(4040.00M, dataApp0.aTotCommisionI);
            Assert.AreEqual(2020.00M, dataApp0.aTotDividendI);
            Assert.AreEqual(1010.00M, dataApp0.aTotOI);
            //Assert.AreEqual(177770.07M, dataApp0.aTotI);

            //Assert.AreEqual(7.70M, dataApp1.aBTotI);
            Assert.AreEqual(0, dataApp1.aCTotI);

            Assert.AreEqual(4.00M, dataApp1.aTotBaseI);
            Assert.AreEqual(2.00M, dataApp1.aTotOvertimeI);
            Assert.AreEqual(1.00M, dataApp1.aTotBonusesI);
            Assert.AreEqual(0.40M, dataApp1.aTotCommisionI);
            Assert.AreEqual(0.20M, dataApp1.aTotDividendI);
            Assert.AreEqual(0.10M, dataApp1.aTotOI);
            //Assert.AreEqual(7.70M, dataApp1.aTotI);

            Assert.AreEqual(177777.77M, dataLoan.sLTotI);

            //Assert.AreEqual(0.07M, dataApp0.aBNetRentI);
            //Assert.AreEqual(0, dataApp0.aCNetRentI);
            //Assert.AreEqual(0, dataApp0.aTotNetRentI);
            //Assert.AreEqual(0, dataApp1.aBNetRentI);

            //Assert.AreEqual(37000.07M, dataApp0.aBNonbaseI);
            //Assert.AreEqual(370.00M, dataApp0.aCNonbaseI);
            //Assert.AreEqual(37370.07M, dataApp0.aTotNonbaseI);

            Assert.AreEqual(100000.00M, dataApp0.aBSpPosCf);
            Assert.AreEqual(0, dataApp0.aCSpPosCf);
            Assert.AreEqual(100000.00M, dataApp0.aTotSpPosCf);

            //Assert.AreEqual(177000.07M, dataApp0.aTransmBTotI);
            //Assert.AreEqual(770.00M, dataApp0.aTransmCTotI);
            //Assert.AreEqual(177770.07M, dataApp0.aTransmTotI);

            //Assert.AreEqual(3.70M, dataApp1.aBNonbaseI);
            Assert.AreEqual(0, dataApp1.aCNonbaseI);
            //Assert.AreEqual(3.70M, dataApp1.aNonbaseI);

            Assert.AreEqual(0, dataApp1.aBSpPosCf);
            Assert.AreEqual(0, dataApp1.aCSpPosCf);
            Assert.AreEqual(0, dataApp1.aTotSpPosCf);

            //Assert.AreEqual(7.70M, dataApp1.aTransmBTotI);
            Assert.AreEqual(0, dataApp1.aTransmCTotI);
            //Assert.AreEqual(7.70M, dataApp1.aTransmTotI);

            Assert.AreEqual(0.07M, dataLoan.sLTotNetRentI1003);
        }

        [Test]
        public void ReoOnlyAllExpenseReoTest()
        {
            CPageData dataLoan = FakePageData.Create(appCount: 2);
            dataLoan.InitLoad();

            CAppData dataApp0 = dataLoan.GetAppData(0);
            CAppData dataApp1 = dataLoan.GetAppData(1);

            SetupBaseLoanAndApp0(dataLoan);
            SetupApp1(dataApp1);

            IRealEstateOwned reo;

            reo = dataApp0.aReCollection.AddRegularRecord();
            reo.StatT = E_ReoStatusT.Rental;
            reo.OccR = 100;
            reo.ReOwnerT = E_ReOwnerT.Borrower;
            reo.GrossRentI = 0.00M;
            reo.HExp = 0.07M;

            reo = dataApp0.aReCollection.AddRegularRecord();
            reo.StatT = E_ReoStatusT.Rental;
            reo.OccR = 100;
            reo.ReOwnerT = E_ReOwnerT.Joint;
            reo.GrossRentI = 0.01M;
            reo.HExp = 0.02M;

            reo = dataApp0.aReCollection.AddRegularRecord();
            reo.StatT = E_ReoStatusT.Rental;
            reo.OccR = 100;
            reo.ReOwnerT = E_ReOwnerT.CoBorrower;
            reo.GrossRentI = 0.01M;
            reo.HExp = 0.02M;

            reo = dataApp1.aReCollection.AddRegularRecord();
            reo.StatT = E_ReoStatusT.Rental;
            reo.OccR = 100;
            reo.ReOwnerT = E_ReOwnerT.Borrower;
            reo.GrossRentI = 0.02M;
            reo.HExp = 0.04M;

            dataApp0.aReCollection.Flush();
            dataApp1.aReCollection.Flush();

            Assert.AreEqual(177000.00M, dataApp0.aBTotI);
            Assert.AreEqual(770.00M, dataApp0.aCTotI);

            Assert.AreEqual(40400.00M, dataApp0.aTotBaseI);
            Assert.AreEqual(20200.00M, dataApp0.aTotOvertimeI);
            Assert.AreEqual(10100.00M, dataApp0.aTotBonusesI);
            Assert.AreEqual(4040.00M, dataApp0.aTotCommisionI);
            Assert.AreEqual(2020.00M, dataApp0.aTotDividendI);
            Assert.AreEqual(1010.00M, dataApp0.aTotOI);
            Assert.AreEqual(177770.00M, dataApp0.aTotI);

            Assert.AreEqual(7.70M, dataApp1.aBTotI);
            Assert.AreEqual(0, dataApp1.aCTotI);

            Assert.AreEqual(4.00M, dataApp1.aTotBaseI);
            Assert.AreEqual(2.00M, dataApp1.aTotOvertimeI);
            Assert.AreEqual(1.00M, dataApp1.aTotBonusesI);
            Assert.AreEqual(0.40M, dataApp1.aTotCommisionI);
            Assert.AreEqual(0.20M, dataApp1.aTotDividendI);
            Assert.AreEqual(0.10M, dataApp1.aTotOI);
            Assert.AreEqual(7.70M, dataApp1.aTotI);

            Assert.AreEqual(177777.70M, dataLoan.sLTotI);

            Assert.AreEqual(0, dataApp0.aBNetRentI);
            Assert.AreEqual(0, dataApp0.aCNetRentI);
            Assert.AreEqual(0, dataApp0.aTotNetRentI);
            Assert.AreEqual(0, dataApp1.aBNetRentI);

            Assert.AreEqual(37000.00M, dataApp0.aBNonbaseI);
            Assert.AreEqual(370.00M, dataApp0.aCNonbaseI);
            Assert.AreEqual(37370.00M, dataApp0.aTotNonbaseI);

            Assert.AreEqual(100000.00M, dataApp0.aBSpPosCf);
            Assert.AreEqual(0, dataApp0.aCSpPosCf);
            Assert.AreEqual(100000.00M, dataApp0.aTotSpPosCf);

            Assert.AreEqual(177000.00M, dataApp0.aTransmBTotI);
            Assert.AreEqual(770.00M, dataApp0.aTransmCTotI);
            Assert.AreEqual(177770.00M, dataApp0.aTransmTotI);

            Assert.AreEqual(3.70M, dataApp1.aBNonbaseI);
            Assert.AreEqual(0, dataApp1.aCNonbaseI);
            Assert.AreEqual(3.70M, dataApp1.aNonbaseI);

            Assert.AreEqual(0, dataApp1.aBSpPosCf);
            Assert.AreEqual(0, dataApp1.aCSpPosCf);
            Assert.AreEqual(0, dataApp1.aTotSpPosCf);

            Assert.AreEqual(7.70M, dataApp1.aTransmBTotI);
            Assert.AreEqual(0, dataApp1.aTransmCTotI);
            Assert.AreEqual(7.70M, dataApp1.aTransmTotI);
        }

        // This test should pass with the new calculation enabled.
        ////[Test]
        ////public void ReoNetPerLoanTest()
        ////{
        ////    {
        ////        CPageData dataLoan = FakePageData.Create(appCount: 2);

        ////        SetupBaseLoan(dataLoan);
        ////        SetupReoRecords(dataLoan);

        ////        CAppData dataApp0 = dataLoan.GetAppData(0);
        ////        CAppData dataApp1 = dataLoan.GetAppData(1);

        ////        Assert.AreEqual(110003.77M, dataApp0.aBTotI);
        ////        Assert.AreEqual(20174.00M, dataApp0.aCTotI);
        ////        Assert.AreEqual(47600.00M, dataApp1.aBTotI);

        ////        Assert.AreEqual(130177.77M, dataApp0.aTotI);
        ////    }
        ////}
    }
}
