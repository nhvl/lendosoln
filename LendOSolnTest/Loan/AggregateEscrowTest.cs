﻿using System;
using System.Collections.Generic;
using DataAccess;
using LendOSolnTest.Common;
using LendOSolnTest.Fakes;
using NUnit.Framework;

namespace LendOSolnTest.Loan
{
    [TestFixture]
    public class AggregateEscrowTest
    {
        // Initial Escrow Account Setup is a matrix of 13x7. Each cell store # of months. Rows and columns order are very important
        //             Structure of Initial Escrow Account Screen. Use the following as default.
        //             
        //             #_months   Tax    Haz_Ins    Mtg_Ins    Flood_Ins   School_Tax    User_Define_1    User_Define_2    User_Define_3    User_Define_4
        //             ----------------------------------------------------------------------------------------------------------------------------------
        //             Cushion     2       2           1          2            2             2                  2                2                2
        //             Jan                             1
        //             Feb                             1
        //             Mar                             1
        //             Apr         6                   1
        //             May                             1
        //             Jun                12           1                                     12                12               12               12
        //             Jul                             1
        //             Aug                             1          12
        //             Sep                             1
        //             Oct                             1                       12
        //             Nov         6                   1
        //             Dec                             1

        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            LoginTools.LoginAs(TestAccountType.LoTest001);
        }

        private static CPageData CreateDataObject()
        {
            return FakePageData.Create();
        }

        [Test]
        public void TestRecommendReserveScenario1()
        {
            // 5/28/2009 dd - OPM 17554.
            CPageData dataLoan = CreateDataObject();
            dataLoan.sAggEscrowCalcModeT = E_AggregateEscrowCalculationModeT.Legacy;
            dataLoan.InitLoad();

            int[,] initialEscrowAccount = {
                                                           {2, 2, 1, 2, 2, 2, 2, 2, 2}, //Cushion
                                                           {0, 0, 1, 0, 0, 0, 0, 0, 0}, // Jan
                                                           {0, 0, 1, 0, 0, 0, 0, 0, 0}, // Feb
                                                           {0, 0, 1, 0, 0, 0, 0, 0, 0}, // Mar
                                                           {6, 0, 1, 0, 0, 0, 0, 0, 0}, // Apr
                                                           {0, 0, 1, 0, 0, 0, 0, 0, 0}, // May
                                                           {0, 12, 1, 0, 0, 12, 12, 12, 12}, // Jun
                                                           {0, 0, 1, 0, 0, 0, 0, 0, 0}, // Jul
                                                           {6, 0, 1, 12, 0, 0, 0, 0, 0}, // Aug
                                                           {0, 0, 1, 0, 0, 0, 0, 0, 0}, // Sep
                                                           {0, 0, 1, 0, 12, 0, 0, 0, 0}, // Oct
                                                           {0, 0, 1, 0, 0, 0, 0, 0, 0}, // Nov
                                                           {0, 0, 1, 0, 0, 0, 0, 0, 0} // Dec
                                                          };

            dataLoan.sInitialEscrowAcc = initialEscrowAccount;

            dataLoan.sSchedDueD1_rep = "1/1/2009";
            dataLoan.sSchedDueD1Lckd = true;
            dataLoan.sProHazInsMb_rep = "$100.00";
            dataLoan.sProRealETxMb_rep = "$100.00";

            Assert.AreEqual("8", dataLoan.sHazInsRsrvMonRecommended_rep, "sHazInsRsrvMonRecommended_rep");
            Assert.AreEqual("6", dataLoan.sRealETxRsrvMonRecommended_rep, "sRealETxRsrvMonRecommended_rep");

        }
        [Test]
        public void TestRecommendReserveScenario2()
        {
            // 5/28/2009 dd - OPM 17554.
            CPageData dataLoan = CreateDataObject();
            dataLoan.sAggEscrowCalcModeT = E_AggregateEscrowCalculationModeT.Legacy;
            dataLoan.InitLoad();

            int[,] initialEscrowAccount = {
                                                           {2, 2, 1, 2, 2, 2, 2, 2, 2}, //Cushion
                                                           {0, 0, 1, 0, 0, 0, 0, 0, 0}, // Jan
                                                           {12, 0, 1, 0, 0, 0, 0, 0, 0}, // Feb
                                                           {0, 0, 1, 0, 0, 0, 0, 0, 0}, // Mar
                                                           {0, 12, 1, 0, 0, 0, 0, 0, 0}, // Apr
                                                           {0, 0, 1, 0, 0, 0, 0, 0, 0}, // May
                                                           {0, 0, 1, 0, 0, 12, 12, 12, 12}, // Jun
                                                           {0, 0, 1, 0, 0, 0, 0, 0, 0}, // Jul
                                                           {0, 0, 1, 12, 0, 0, 0, 0, 0}, // Aug
                                                           {0, 0, 1, 0, 0, 0, 0, 0, 0}, // Sep
                                                           {0, 0, 1, 0, 12, 0, 0, 0, 0}, // Oct
                                                           {0, 0, 1, 0, 0, 0, 0, 0, 0}, // Nov
                                                           {0, 0, 1, 0, 0, 0, 0, 0, 0} // Dec
                                                          };
            dataLoan.sInitialEscrowAcc = initialEscrowAccount;
            dataLoan.sSchedDueD1_rep = "1/1/2009";
            dataLoan.sSchedDueD1Lckd = true;
            dataLoan.sProHazInsMb_rep = "$100.00";
            dataLoan.sProRealETxMb_rep = "$100.00";

            Assert.AreEqual("10", dataLoan.sHazInsRsrvMonRecommended_rep, "sHazInsRsrvMonRecommended_rep");
            Assert.AreEqual("12", dataLoan.sRealETxRsrvMonRecommended_rep, "sRealETxRsrvMonRecommended_rep");

        }

        [Test]
        public void TestMissingScheduleDate()
        {
            // 5/28/2009 dd - OPM 17554.
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitLoad();

            int[,] initialEscrowAccount = {
                                                           {2, 2, 1, 2, 2, 2, 2, 2, 2}, //Cushion
                                                           {0, 0, 1, 0, 0, 0, 0, 0, 0}, // Jan
                                                           {12, 0, 1, 0, 0, 0, 0, 0, 0}, // Feb
                                                           {0, 0, 1, 0, 0, 0, 0, 0, 0}, // Mar
                                                           {0, 12, 1, 0, 0, 0, 0, 0, 0}, // Apr
                                                           {0, 0, 1, 0, 0, 0, 0, 0, 0}, // May
                                                           {0, 0, 1, 0, 0, 12, 12, 12, 12}, // Jun
                                                           {0, 0, 1, 0, 0, 0, 0, 0, 0}, // Jul
                                                           {0, 0, 1, 12, 0, 0, 0, 0, 0}, // Aug
                                                           {0, 0, 1, 0, 0, 0, 0, 0, 0}, // Sep
                                                           {0, 0, 1, 0, 12, 0, 0, 0, 0}, // Oct
                                                           {0, 0, 1, 0, 0, 0, 0, 0, 0}, // Nov
                                                           {0, 0, 1, 0, 0, 0, 0, 0, 0} // Dec
                                                          };
            dataLoan.sInitialEscrowAcc = initialEscrowAccount;
            dataLoan.sSchedDueD1_rep = "";
            dataLoan.sSchedDueD1Lckd = true;
            dataLoan.sProHazInsMb_rep = "$100.00";
            dataLoan.sProRealETxMb_rep = "$100.00";

            Assert.AreEqual("0", dataLoan.sHazInsRsrvMonRecommended_rep, "sHazInsRsrvMonRecommended_rep");
            Assert.AreEqual("0", dataLoan.sRealETxRsrvMonRecommended_rep, "sRealETxRsrvMonRecommended_rep");

        }
        [Test]
        public void TestMissingMonthlyPayment()
        {
            // 5/28/2009 dd - OPM 17554.
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitLoad();

            int[,] initialEscrowAccount = {
                                                           {2, 2, 1, 2, 2, 2, 2, 2, 2}, //Cushion
                                                           {0, 0, 1, 0, 0, 0, 0, 0, 0}, // Jan
                                                           {12, 0, 1, 0, 0, 0, 0, 0, 0}, // Feb
                                                           {0, 0, 1, 0, 0, 0, 0, 0, 0}, // Mar
                                                           {0, 12, 1, 0, 0, 0, 0, 0, 0}, // Apr
                                                           {0, 0, 1, 0, 0, 0, 0, 0, 0}, // May
                                                           {0, 0, 1, 0, 0, 12, 12, 12, 12}, // Jun
                                                           {0, 0, 1, 0, 0, 0, 0, 0, 0}, // Jul
                                                           {0, 0, 1, 12, 0, 0, 0, 0, 0}, // Aug
                                                           {0, 0, 1, 0, 0, 0, 0, 0, 0}, // Sep
                                                           {0, 0, 1, 0, 12, 0, 0, 0, 0}, // Oct
                                                           {0, 0, 1, 0, 0, 0, 0, 0, 0}, // Nov
                                                           {0, 0, 1, 0, 0, 0, 0, 0, 0} // Dec
                                                          };
            dataLoan.sInitialEscrowAcc = initialEscrowAccount;
            dataLoan.sSchedDueD1_rep = "1/1/2009";


            Assert.AreEqual("0", dataLoan.sHazInsRsrvMonRecommended_rep, "sHazInsRsrvMonRecommended_rep");
            Assert.AreEqual("0", dataLoan.sRealETxRsrvMonRecommended_rep, "sRealETxRsrvMonRecommended_rep");

        }

        [Test]
        public void TRIDAggregateAnalysisTest1()
        {
            /*  OPM 468804 - Test a loan with the following parameters under Regulatory,
                             Customary, and Customary 2 Aggregate Escrow Calculation Mode.
                             In this test, we consider the reserves deposit, servicing payments,
                             aggregate adjustment, and total collected at closing.                                    
            First Payment Date: 7/1/2018
            Hazard Insurance:
              Cushion: 2 months
              Disbursements:
                12 months / $1,200.00 due 5/1/2019 from escrow
            Property Taxes:
              Cushion: 2 months
              Disbursements:
                6 months / $600.13 due 11/1/2018 from seller
                6 months / $600.13 due 4/1/2019 from escrow
            */
            CPageData dataLoan = PrepareLoanForTRIDAggregateAnalysisTest(firstPaymentDate: "7/1/2018");
            AddTRIDHousingExpense(dataLoan, housingExpenseType: E_HousingExpenseTypeT.HazardInsurance, cushion: 2,
                disbursements: new List<SingleDisbursement>()
                {
                    ToTRIDDisbursement(monthsCovered: 12, amount: 1200, dueDate: DateTime.Parse("5/1/2019"), paymentSource: E_DisbursementPaidByT.EscrowImpounds)
                });
            AddTRIDHousingExpense(dataLoan, housingExpenseType: E_HousingExpenseTypeT.RealEstateTaxes, cushion: 2,
                disbursements: new List<SingleDisbursement>()
                {
                    ToTRIDDisbursement(monthsCovered: 6, amount: 600.13M, dueDate: DateTime.Parse("11/1/2018"), paymentSource: E_DisbursementPaidByT.Seller),
                    ToTRIDDisbursement(monthsCovered: 6, amount: 600.13M, dueDate: DateTime.Parse("4/1/2019"), paymentSource: E_DisbursementPaidByT.EscrowImpounds)
                });

            List<string> failedTests = new List<string>();

            /* Expected Output for Regulatory Mode:
              Hazard Insurance
                Monthly Amount (Servicing): $100  
                Months Reserves: 3
                Reserves Amount: $300
              Property Taxes:
                Monthly Amount (Servicing): $50.01
                Months Reserves: 4
                Reserves Amount: $200.04
              Aggregate Escrow Adjustment: -$50
              Total Escrow Collected at Closing: $450.04
            */
            dataLoan.sAggEscrowCalcModeT = E_AggregateEscrowCalculationModeT.Regulatory;
            TestTRIDHousingExpense(dataLoan: dataLoan,
                expected: new Dictionary<string,object>()
                {
                    { nameof(BaseHousingExpense.MonthlyAmtServicing_rep), "$100.00" },
                    { nameof(BaseHousingExpense.ReserveMonths), 3 },
                    { nameof(BaseHousingExpense.ReserveAmt), 300.00M }
                },
                actual: dataLoan.sHazardExpense,
                failedTests: failedTests);
            TestTRIDHousingExpense(dataLoan: dataLoan,
                expected: new Dictionary<string, object>()
                {
                    { nameof(BaseHousingExpense.MonthlyAmtServicing_rep), "$50.01" },
                    { nameof(BaseHousingExpense.ReserveMonths), 4 },
                    { nameof(BaseHousingExpense.ReserveAmt), 200.04M }
                },
                actual: dataLoan.sRealEstateTaxExpense,
                failedTests: failedTests);
            TestTRIDEscrowTotal(expected: new Dictionary<string, object>()
                {
                    { nameof(CPageData.sAggregateAdjRsrv), -50.00M },
                    { nameof(CPageData.sGfeInitialImpoundDeposit_rep), "$450.04" }
                },
                dataLoan: dataLoan,
                failedTests: failedTests);

            /* Expected Output for Customary Mode, No impounds minimum
              Hazard Insurance
                Monthly Amount (Servicing): $100  
                Months Reserves: 3
                Reserves Amount: $300
              Property Taxes:
                Monthly Amount (Servicing): $100.02
                Months Reserves: -2
                Reserves Amount: -$200.04
              Aggregate Escrow Adjustment: -$100
              Total Escrow Collected at Closing: -$0.04
            */
            dataLoan.sAggEscrowCalcModeT = E_AggregateEscrowCalculationModeT.Customary;
            dataLoan.sCustomaryEscrowImpoundsCalcMinT = E_CustomaryEscrowImpoundsCalcMinT.NoMin;
            TestTRIDHousingExpense(dataLoan: dataLoan,
                expected: new Dictionary<string, object>()
                {
                    { nameof(BaseHousingExpense.MonthlyAmtServicing_rep), "$100.00" },
                    { nameof(BaseHousingExpense.ReserveMonths), 3 },
                    { nameof(BaseHousingExpense.ReserveAmt), 300.00M }
                },
                actual: dataLoan.sHazardExpense,
                failedTests: failedTests);
            TestTRIDHousingExpense(dataLoan: dataLoan,
                expected: new Dictionary<string, object>()
                {
                    { nameof(BaseHousingExpense.MonthlyAmtServicing_rep), "$100.02" },
                    { nameof(BaseHousingExpense.ReserveMonths), -2 },
                    { nameof(BaseHousingExpense.ReserveAmt), -200.04M }
                },
                actual: dataLoan.sRealEstateTaxExpense,
                failedTests: failedTests);
            TestTRIDEscrowTotal(expected: new Dictionary<string, object>()
                {
                    { nameof(CPageData.sAggregateAdjRsrv), -100.00M },
                    { nameof(CPageData.sGfeInitialImpoundDeposit_rep), "($0.04)" }
                },
                dataLoan: dataLoan,
                failedTests: failedTests);

            /* Expected Output for Customary Mode, Zero impounds minimum
              Hazard Insurance
                Monthly Amount (Servicing): $100  
                Months Reserves: 3
                Reserves Amount: $300
              Property Taxes:
                Monthly Amount (Servicing): $100.02
                Months Reserves: 0
                Reserves Amount: $0
              Aggregate Escrow Adjustment: -$100
              Total Escrow Collected at Closing: $200
            */
            dataLoan.sAggEscrowCalcModeT = E_AggregateEscrowCalculationModeT.Customary;
            dataLoan.sCustomaryEscrowImpoundsCalcMinT = E_CustomaryEscrowImpoundsCalcMinT.Zero;
            TestTRIDHousingExpense(dataLoan: dataLoan,
                expected: new Dictionary<string, object>()
                {
                    { nameof(BaseHousingExpense.MonthlyAmtServicing_rep), "$100.00" },
                    { nameof(BaseHousingExpense.ReserveMonths), 3 },
                    { nameof(BaseHousingExpense.ReserveAmt), 300.00M }
                },
                actual: dataLoan.sHazardExpense,
                failedTests: failedTests);
            TestTRIDHousingExpense(dataLoan: dataLoan,
                expected: new Dictionary<string, object>()
                {
                    { nameof(BaseHousingExpense.MonthlyAmtServicing_rep), "$100.02" },
                    { nameof(BaseHousingExpense.ReserveMonths), 0 },
                    { nameof(BaseHousingExpense.ReserveAmt), 0.00M }
                },
                actual: dataLoan.sRealEstateTaxExpense,
                failedTests: failedTests);
            TestTRIDEscrowTotal(expected: new Dictionary<string, object>()
                {
                    { nameof(CPageData.sAggregateAdjRsrv), -100.00M },
                    { nameof(CPageData.sGfeInitialImpoundDeposit_rep), "$200.00" }
                },
                dataLoan: dataLoan,
                failedTests: failedTests);

            /* Expected Output for Customary Mode, Cushion impounds minimum
              Hazard Insurance
                Monthly Amount (Servicing): $100  
                Months Reserves: 3
                Reserves Amount: $300
              Property Taxes:
                Monthly Amount (Servicing): $100.02
                Months Reserves: 2
                Reserves Amount: $200.04
              Aggregate Escrow Adjustment: -$100
              Total Escrow Collected at Closing: $400.04
            */
            dataLoan.sAggEscrowCalcModeT = E_AggregateEscrowCalculationModeT.Customary;
            dataLoan.sCustomaryEscrowImpoundsCalcMinT = E_CustomaryEscrowImpoundsCalcMinT.Cushion;
            TestTRIDHousingExpense(dataLoan: dataLoan,
                expected: new Dictionary<string, object>()
                {
                    { nameof(BaseHousingExpense.MonthlyAmtServicing_rep), "$100.00" },
                    { nameof(BaseHousingExpense.ReserveMonths), 3 },
                    { nameof(BaseHousingExpense.ReserveAmt), 300.00M }
                },
                actual: dataLoan.sHazardExpense,
                failedTests: failedTests);
            TestTRIDHousingExpense(dataLoan: dataLoan,
                expected: new Dictionary<string, object>()
                {
                    { nameof(BaseHousingExpense.MonthlyAmtServicing_rep), "$100.02" },
                    { nameof(BaseHousingExpense.ReserveMonths), 2 },
                    { nameof(BaseHousingExpense.ReserveAmt), 200.04M }
                },
                actual: dataLoan.sRealEstateTaxExpense,
                failedTests: failedTests);
            TestTRIDEscrowTotal(expected: new Dictionary<string, object>()
                {
                    { nameof(CPageData.sAggregateAdjRsrv), -100.00M },
                    { nameof(CPageData.sGfeInitialImpoundDeposit_rep), "$400.04" }
                },
                dataLoan: dataLoan,
                failedTests: failedTests);

            /* Expected Output for Customary Mode, Zero_AdjustAggregate impounds minimum
              Hazard Insurance
                Monthly Amount (Servicing): $100  
                Months Reserves: 3
                Reserves Amount: $300
              Property Taxes:
                Monthly Amount (Servicing): $100.02
                Months Reserves: 0
                Reserves Amount: $0
              Aggregate Escrow Adjustment: -$300.04
              Total Escrow Collected at Closing: -$0.04
            */
            dataLoan.sAggEscrowCalcModeT = E_AggregateEscrowCalculationModeT.Customary;
            dataLoan.sCustomaryEscrowImpoundsCalcMinT = E_CustomaryEscrowImpoundsCalcMinT.Zero_AdjustAggregate;
            TestTRIDHousingExpense(dataLoan: dataLoan,
                expected: new Dictionary<string, object>()
                {
                    { nameof(BaseHousingExpense.MonthlyAmtServicing_rep), "$100.00" },
                    { nameof(BaseHousingExpense.ReserveMonths), 3 },
                    { nameof(BaseHousingExpense.ReserveAmt), 300.00M }
                },
                actual: dataLoan.sHazardExpense,
                failedTests: failedTests);
            TestTRIDHousingExpense(dataLoan: dataLoan,
                expected: new Dictionary<string, object>()
                {
                    { nameof(BaseHousingExpense.MonthlyAmtServicing_rep), "$100.02" },
                    { nameof(BaseHousingExpense.ReserveMonths), 0 },
                    { nameof(BaseHousingExpense.ReserveAmt), 0.00M }
                },
                actual: dataLoan.sRealEstateTaxExpense,
                failedTests: failedTests);
            TestTRIDEscrowTotal(expected: new Dictionary<string, object>()
                {
                    { nameof(CPageData.sAggregateAdjRsrv), -300.04M },
                    { nameof(CPageData.sGfeInitialImpoundDeposit_rep), "($0.04)" }
                },
                dataLoan: dataLoan,
                failedTests: failedTests);

            /* Expected Output for Customary Mode, Cushion_AdjustAggregate impounds minimum
              Hazard Insurance
                Monthly Amount (Servicing): $100  
                Months Reserves: 3
                Reserves Amount: $300
              Property Taxes:
                Monthly Amount (Servicing): $100.02
                Months Reserves: 2
                Reserves Amount: $200.04
              Aggregate Escrow Adjustment: -$500.08
              Total Escrow Collected at Closing: -$0.04
            */
            dataLoan.sAggEscrowCalcModeT = E_AggregateEscrowCalculationModeT.Customary;
            dataLoan.sCustomaryEscrowImpoundsCalcMinT = E_CustomaryEscrowImpoundsCalcMinT.Cushion_AdjustAggregate;
            TestTRIDHousingExpense(dataLoan: dataLoan,
                expected: new Dictionary<string, object>()
                {
                    { nameof(BaseHousingExpense.MonthlyAmtServicing_rep), "$100.00" },
                    { nameof(BaseHousingExpense.ReserveMonths), 3 },
                    { nameof(BaseHousingExpense.ReserveAmt), 300.00M }
                },
                actual: dataLoan.sHazardExpense,
                failedTests: failedTests);
            TestTRIDHousingExpense(dataLoan: dataLoan,
                expected: new Dictionary<string, object>()
                {
                    { nameof(BaseHousingExpense.MonthlyAmtServicing_rep), "$100.02" },
                    { nameof(BaseHousingExpense.ReserveMonths), 2 },
                    { nameof(BaseHousingExpense.ReserveAmt), 200.04M }
                },
                actual: dataLoan.sRealEstateTaxExpense,
                failedTests: failedTests);
            TestTRIDEscrowTotal(expected: new Dictionary<string, object>()
                {
                    { nameof(CPageData.sAggregateAdjRsrv), -500.08M },
                    { nameof(CPageData.sGfeInitialImpoundDeposit_rep), "($0.04)" }
                },
                dataLoan: dataLoan,
                failedTests: failedTests);

            /* Expected Output for Customary2 Mode, No impounds minimum
              Hazard Insurance
                Monthly Amount (Servicing): $100  
                Months Reserves: 3
                Reserves Amount: $300
              Property Taxes:
                Monthly Amount (Servicing): $100.02
                Months Reserves: 1
                Reserves Amount: $100.02
              Aggregate Escrow Adjustment: -$200
              Total Escrow Collected at Closing: $200.02
            */
            dataLoan.sAggEscrowCalcModeT = E_AggregateEscrowCalculationModeT.Customary2;
            dataLoan.sCustomaryEscrowImpoundsCalcMinT = E_CustomaryEscrowImpoundsCalcMinT.NoMin;
            TestTRIDHousingExpense(dataLoan: dataLoan,
                expected: new Dictionary<string, object>()
                {
                    { nameof(BaseHousingExpense.MonthlyAmtServicing_rep), "$100.00" },
                    { nameof(BaseHousingExpense.ReserveMonths), 3 },
                    { nameof(BaseHousingExpense.ReserveAmt), 300.00M }
                },
                actual: dataLoan.sHazardExpense,
                failedTests: failedTests);
            TestTRIDHousingExpense(dataLoan: dataLoan,
                expected: new Dictionary<string, object>()
                {
                    { nameof(BaseHousingExpense.MonthlyAmtServicing_rep), "$100.02" },
                    { nameof(BaseHousingExpense.ReserveMonths), 1 },
                    { nameof(BaseHousingExpense.ReserveAmt), 100.02M }
                },
                actual: dataLoan.sRealEstateTaxExpense,
                failedTests: failedTests);
            TestTRIDEscrowTotal(expected: new Dictionary<string, object>()
                {
                    { nameof(CPageData.sAggregateAdjRsrv), -200.00M },
                    { nameof(CPageData.sGfeInitialImpoundDeposit_rep), "$200.02" }
                },
                dataLoan: dataLoan,
                failedTests: failedTests);

            /* Expected Output for Customary2 Mode, Zero impounds minimum
              Hazard Insurance
                Monthly Amount (Servicing): $100  
                Months Reserves: 3
                Reserves Amount: $300
              Property Taxes:
                Monthly Amount (Servicing): $100.02
                Months Reserves: 1
                Reserves Amount: $100.02
              Aggregate Escrow Adjustment: -$200
              Total Escrow Collected at Closing: $200.02
            */
            dataLoan.sAggEscrowCalcModeT = E_AggregateEscrowCalculationModeT.Customary2;
            dataLoan.sCustomaryEscrowImpoundsCalcMinT = E_CustomaryEscrowImpoundsCalcMinT.Zero;
            TestTRIDHousingExpense(dataLoan: dataLoan,
                expected: new Dictionary<string, object>()
                {
                    { nameof(BaseHousingExpense.MonthlyAmtServicing_rep), "$100.00" },
                    { nameof(BaseHousingExpense.ReserveMonths), 3 },
                    { nameof(BaseHousingExpense.ReserveAmt), 300.00M }
                },
                actual: dataLoan.sHazardExpense,
                failedTests: failedTests);
            TestTRIDHousingExpense(dataLoan: dataLoan,
                expected: new Dictionary<string, object>()
                {
                    { nameof(BaseHousingExpense.MonthlyAmtServicing_rep), "$100.02" },
                    { nameof(BaseHousingExpense.ReserveMonths), 1 },
                    { nameof(BaseHousingExpense.ReserveAmt), 100.02M }
                },
                actual: dataLoan.sRealEstateTaxExpense,
                failedTests: failedTests);
            TestTRIDEscrowTotal(expected: new Dictionary<string, object>()
                {
                    { nameof(CPageData.sAggregateAdjRsrv), -200.00M },
                    { nameof(CPageData.sGfeInitialImpoundDeposit_rep), "$200.02" }
                },
                dataLoan: dataLoan,
                failedTests: failedTests);

            /* Expected Output for Customary2 Mode, Cushion impounds minimum
              Hazard Insurance
                Monthly Amount (Servicing): $100  
                Months Reserves: 3
                Reserves Amount: $300
              Property Taxes:
                Monthly Amount (Servicing): $100
                Months Reserves: 2
                Reserves Amount: $200
              Aggregate Escrow Adjustment: -$200
              Total Escrow Collected at Closing: $300
            */
            dataLoan.sAggEscrowCalcModeT = E_AggregateEscrowCalculationModeT.Customary2;
            dataLoan.sCustomaryEscrowImpoundsCalcMinT = E_CustomaryEscrowImpoundsCalcMinT.Cushion;
            TestTRIDHousingExpense(dataLoan: dataLoan,
                expected: new Dictionary<string, object>()
                {
                    { nameof(BaseHousingExpense.MonthlyAmtServicing_rep), "$100.00" },
                    { nameof(BaseHousingExpense.ReserveMonths), 3 },
                    { nameof(BaseHousingExpense.ReserveAmt), 300.00M }
                },
                actual: dataLoan.sHazardExpense,
                failedTests: failedTests);
            TestTRIDHousingExpense(dataLoan: dataLoan,
                expected: new Dictionary<string, object>()
                {
                    { nameof(BaseHousingExpense.MonthlyAmtServicing_rep), "$100.02" },
                    { nameof(BaseHousingExpense.ReserveMonths), 2 },
                    { nameof(BaseHousingExpense.ReserveAmt), 200.04M }
                },
                actual: dataLoan.sRealEstateTaxExpense,
                failedTests: failedTests);
            TestTRIDEscrowTotal(expected: new Dictionary<string, object>()
                {
                    { nameof(CPageData.sAggregateAdjRsrv), -200.00M },
                    { nameof(CPageData.sGfeInitialImpoundDeposit_rep), "$300.04" }
                },
                dataLoan: dataLoan,
                failedTests: failedTests);

            /* Expected Output for Customary2 Mode, Zero_AdjustAggregate impounds minimum
              Hazard Insurance
                Monthly Amount (Servicing): $100  
                Months Reserves: 3
                Reserves Amount: $300
              Property Taxes:
                Monthly Amount (Servicing): $100.02
                Months Reserves: 1
                Reserves Amount: $100.02
              Aggregate Escrow Adjustment: $200
              Total Escrow Collected at Closing: $200.02
            */
            dataLoan.sAggEscrowCalcModeT = E_AggregateEscrowCalculationModeT.Customary2;
            dataLoan.sCustomaryEscrowImpoundsCalcMinT = E_CustomaryEscrowImpoundsCalcMinT.Zero_AdjustAggregate;
            TestTRIDHousingExpense(dataLoan: dataLoan,
                expected: new Dictionary<string, object>()
                {
                    { nameof(BaseHousingExpense.MonthlyAmtServicing_rep), "$100.00" },
                    { nameof(BaseHousingExpense.ReserveMonths), 3 },
                    { nameof(BaseHousingExpense.ReserveAmt), 300.00M }
                },
                actual: dataLoan.sHazardExpense,
                failedTests: failedTests);
            TestTRIDHousingExpense(dataLoan: dataLoan,
                expected: new Dictionary<string, object>()
                {
                    { nameof(BaseHousingExpense.MonthlyAmtServicing_rep), "$100.02" },
                    { nameof(BaseHousingExpense.ReserveMonths), 1 },
                    { nameof(BaseHousingExpense.ReserveAmt), 100.02M }
                },
                actual: dataLoan.sRealEstateTaxExpense,
                failedTests: failedTests);
            TestTRIDEscrowTotal(expected: new Dictionary<string, object>()
                {
                    { nameof(CPageData.sAggregateAdjRsrv), -200.00M },
                    { nameof(CPageData.sGfeInitialImpoundDeposit_rep), "$200.02" }
                },
                dataLoan: dataLoan,
                failedTests: failedTests);

            /* Expected Output for Customary2 Mode, Cushion_AdjustAggregate impounds minimum
              Hazard Insurance
                Monthly Amount (Servicing): $100  
                Months Reserves: 3
                Reserves Amount: $300
              Property Taxes:
                Monthly Amount (Servicing): $100.02
                Months Reserves: 2
                Reserves Amount: $200.04
              Aggregate Escrow Adjustment: -$300.02
              Total Escrow Collected at Closing: $200.02
            */
            dataLoan.sAggEscrowCalcModeT = E_AggregateEscrowCalculationModeT.Customary2;
            dataLoan.sCustomaryEscrowImpoundsCalcMinT = E_CustomaryEscrowImpoundsCalcMinT.Cushion_AdjustAggregate;
            TestTRIDHousingExpense(dataLoan: dataLoan,
                expected: new Dictionary<string, object>()
                {
                    { nameof(BaseHousingExpense.MonthlyAmtServicing_rep), "$100.00" },
                    { nameof(BaseHousingExpense.ReserveMonths), 3 },
                    { nameof(BaseHousingExpense.ReserveAmt), 300.00M }
                },
                actual: dataLoan.sHazardExpense,
                failedTests: failedTests);
            TestTRIDHousingExpense(dataLoan: dataLoan,
                expected: new Dictionary<string, object>()
                {
                    { nameof(BaseHousingExpense.MonthlyAmtServicing_rep), "$100.02" },
                    { nameof(BaseHousingExpense.ReserveMonths), 2 },
                    { nameof(BaseHousingExpense.ReserveAmt), 200.04M }
                },
                actual: dataLoan.sRealEstateTaxExpense,
                failedTests: failedTests);
            TestTRIDEscrowTotal(expected: new Dictionary<string, object>()
                {
                    { nameof(CPageData.sAggregateAdjRsrv), -300.02M },
                    { nameof(CPageData.sGfeInitialImpoundDeposit_rep), "$200.02" }
                },
                dataLoan: dataLoan,
                failedTests: failedTests);

            if (failedTests.Count > 0)
            {
                Assert.Fail(string.Join("\r\n\r\n", failedTests.ToArray()));
            }
        }

        [Test]
        public void TRIDAggregateAnalysisTest2()
        {
            /*  OPM 468804 - Test a loan with the following parameters under Regulatory,
                             Customary, and Customary 2 Aggregate Escrow Calculation Mode.
                             In this test, we consider aggregate adjustment
                             and total collected at closing.                                    
            First Payment Date: 7/1/2018
            Mortgage Insurance:
              Cushion: 2 months
              Disbursements: Every month / $123.45
            Hazard Insurance:
              Cushion: 2 months
              Disbursements:
                12 months / $1,234.56 due 5/1/2019 from Lender
            Flood Insurance
              Cushion: 2 months
              Disbursements:
                7 months / $1,641.92 due 7/1/2018 from Escrow
                5 months / $1,172.80 due 1/1/2019 from Lender
            Windstorm Insurance
              Cushion: 2 months
              Disbursements:
                4 months / $1,827.12 due 7/1/2018 from Escrow
                4 months / $1,827.12 due 11/1/2018 from Seller
                4 months / $1,827.12 due 3/1/2019 from Escrow
            Property Taxes:
              Cushion: 2 months
              Disbursements:
                3 months / $1,629.63 due 7/1/2018 from Borrower
                3 months / $1,629.63 due 10/1/2018 from Escrow
                3 months / $1,629.63 due 1/1/2019 from Escrow
                3 months / $1,629.63 due 4/1/2019 from Escrow
            School Taxes:
              Cushion: 2 months
              Disbursements:
                2 months / $1,308.64 due 8/1/2018 from Escrow
                2 months / $1,308.64 due 10/1/2018 from Lender
                2 months / $1,308.64 due 12/1/2018 from Escrow
                2 months / $1,308.64 due 2/1/2019 from Escrow
                2 months / $1,308.64 due 4/1/2019 from Escrow
                2 months / $1,308.64 due 6/1/2019 from Escrow
            Other Tax 1:
              Cushion: 2 months
              Disbursements:
                1 months / $111.11 due 7/1/2018 from Escrow
                4 months / $444.44 due 1/1/2019 from Escrow
                3 months / $333.33 due 2/1/2019 from Seller
                2 months / $222.22 due 3/1/2019 from Escrow
                1 months / $111.11 due 4/1/2019 from Escrow
                1 months / $111.11 due 6/1/2019 from Escrow
            Other Tax 2:
              Cushion: 2 months
              Disbursements:
                2 months / $444.44 due 7/1/2018 from Escrow
                3 months / $666.66 due 8/1/2018 from Escrow
                1 months / $222.22 due 10/1/2018 from Escrow
                2 months / $444.44 due 11/1/2018 from Seller
                3 months / $666.66 due 12/1/2018 from Escrow
                1 months / $222.22 due 6/1/2019 from Escrow
            Other Tax 3:
              Cushion: 2 months
              Disbursements:
                1 months / $333.33 due 12/1/2018 from Seller
                1 months / $333.33 due 1/1/2019 from Escrow
                10 months / $3,333.30 due 6/1/2019 from Escrow
            Other Tax 4:
              Cushion: 2 months
              Disbursements:
                11 months / $4,888.84 due 8/1/2018 from Lender
                1 months / $444.44 due 9/1/2018 from Escrow
            */
            CPageData dataLoan = PrepareLoanForTRIDAggregateAnalysisTest(firstPaymentDate: "7/1/2018");
            dataLoan.sProMInsLckd = true;
            dataLoan.sProMIns = 123.45M;
            dataLoan.sHousingExpenses.MIPaymentRepeat = E_DisbursementRepIntervalT.Annual;
            dataLoan.sHousingExpenses.MIEscrowSchedule = new int[13];
            dataLoan.sHousingExpenses.MIEscrowSchedule[1] = 1;
            dataLoan.sHousingExpenses.MIEscrowSchedule[2] = 1;
            dataLoan.sHousingExpenses.MIEscrowSchedule[3] = 1;
            dataLoan.sHousingExpenses.MIEscrowSchedule[4] = 1;
            dataLoan.sHousingExpenses.MIEscrowSchedule[5] = 1;
            dataLoan.sHousingExpenses.MIEscrowSchedule[6] = 1;
            dataLoan.sHousingExpenses.MIEscrowSchedule[7] = 1;
            dataLoan.sHousingExpenses.MIEscrowSchedule[8] = 1;
            dataLoan.sHousingExpenses.MIEscrowSchedule[9] = 1;
            dataLoan.sHousingExpenses.MIEscrowSchedule[10] = 1;
            dataLoan.sHousingExpenses.MIEscrowSchedule[11] = 1;
            dataLoan.sHousingExpenses.MIEscrowSchedule[12] = 1;
            dataLoan.sHousingExpenses.MIEscrowSchedule[0] = 2;
            AddTRIDHousingExpense(dataLoan, housingExpenseType: E_HousingExpenseTypeT.HazardInsurance, cushion: 2,
                disbursements: new List<SingleDisbursement>()
                {
                    ToTRIDDisbursement(monthsCovered: 12, amount: 1234.56M, dueDate: DateTime.Parse("5/1/2019"), paymentSource: E_DisbursementPaidByT.Lender)
                });
            AddTRIDHousingExpense(dataLoan, housingExpenseType: E_HousingExpenseTypeT.FloodInsurance, cushion: 2,
                disbursements: new List<SingleDisbursement>()
                {
                    ToTRIDDisbursement(monthsCovered: 7, amount: 1641.92M, dueDate: DateTime.Parse("7/1/2018"), paymentSource: E_DisbursementPaidByT.EscrowImpounds),
                    ToTRIDDisbursement(monthsCovered: 5, amount: 1172.80M, dueDate: DateTime.Parse("1/1/2019"), paymentSource: E_DisbursementPaidByT.Lender)
                });
            AddTRIDHousingExpense(dataLoan, housingExpenseType: E_HousingExpenseTypeT.WindstormInsurance, cushion: 2,
                disbursements: new List<SingleDisbursement>()
                {
                    ToTRIDDisbursement(monthsCovered: 4, amount: 1827.12M, dueDate: DateTime.Parse("7/1/2018"), paymentSource: E_DisbursementPaidByT.EscrowImpounds),
                    ToTRIDDisbursement(monthsCovered: 4, amount: 1827.12M, dueDate: DateTime.Parse("11/1/2018"), paymentSource: E_DisbursementPaidByT.Seller),
                    ToTRIDDisbursement(monthsCovered: 4, amount: 1827.12M, dueDate: DateTime.Parse("3/1/2019"), paymentSource: E_DisbursementPaidByT.EscrowImpounds)
                });
            AddTRIDHousingExpense(dataLoan, housingExpenseType: E_HousingExpenseTypeT.RealEstateTaxes, cushion: 2,
                disbursements: new List<SingleDisbursement>()
                {
                    ToTRIDDisbursement(monthsCovered: 3, amount: 1629.63M, dueDate: DateTime.Parse("7/1/2018"), paymentSource: E_DisbursementPaidByT.Borrower),
                    ToTRIDDisbursement(monthsCovered: 3, amount: 1629.63M, dueDate: DateTime.Parse("10/1/2018"), paymentSource: E_DisbursementPaidByT.EscrowImpounds),
                    ToTRIDDisbursement(monthsCovered: 3, amount: 1629.63M, dueDate: DateTime.Parse("1/1/2019"), paymentSource: E_DisbursementPaidByT.EscrowImpounds),
                    ToTRIDDisbursement(monthsCovered: 3, amount: 1629.63M, dueDate: DateTime.Parse("4/1/2019"), paymentSource: E_DisbursementPaidByT.EscrowImpounds)
                });
            AddTRIDHousingExpense(dataLoan, housingExpenseType: E_HousingExpenseTypeT.SchoolTaxes, cushion: 2,
                disbursements: new List<SingleDisbursement>()
                {
                    ToTRIDDisbursement(monthsCovered: 2, amount: 1308.64M, dueDate: DateTime.Parse("8/1/2018"), paymentSource: E_DisbursementPaidByT.EscrowImpounds),
                    ToTRIDDisbursement(monthsCovered: 2, amount: 1308.64M, dueDate: DateTime.Parse("10/1/2018"), paymentSource: E_DisbursementPaidByT.Lender),
                    ToTRIDDisbursement(monthsCovered: 2, amount: 1308.64M, dueDate: DateTime.Parse("12/1/2018"), paymentSource: E_DisbursementPaidByT.EscrowImpounds),
                    ToTRIDDisbursement(monthsCovered: 2, amount: 1308.64M, dueDate: DateTime.Parse("2/1/2019"), paymentSource: E_DisbursementPaidByT.EscrowImpounds),
                    ToTRIDDisbursement(monthsCovered: 2, amount: 1308.64M, dueDate: DateTime.Parse("4/1/2019"), paymentSource: E_DisbursementPaidByT.EscrowImpounds),
                    ToTRIDDisbursement(monthsCovered: 2, amount: 1308.64M, dueDate: DateTime.Parse("6/1/2019"), paymentSource: E_DisbursementPaidByT.EscrowImpounds)
                });
            AddTRIDHousingExpense(dataLoan, housingExpenseType: E_HousingExpenseTypeT.OtherTaxes1, cushion: 2,
                disbursements: new List<SingleDisbursement>()
                {
                    ToTRIDDisbursement(monthsCovered: 1, amount: 111.11M, dueDate: DateTime.Parse("7/1/2018"), paymentSource: E_DisbursementPaidByT.EscrowImpounds),
                    ToTRIDDisbursement(monthsCovered: 4, amount: 444.44M, dueDate: DateTime.Parse("1/1/2019"), paymentSource: E_DisbursementPaidByT.EscrowImpounds),
                    ToTRIDDisbursement(monthsCovered: 3, amount: 333.33M, dueDate: DateTime.Parse("2/1/2019"), paymentSource: E_DisbursementPaidByT.Seller),
                    ToTRIDDisbursement(monthsCovered: 2, amount: 222.22M, dueDate: DateTime.Parse("3/1/2019"), paymentSource: E_DisbursementPaidByT.EscrowImpounds),
                    ToTRIDDisbursement(monthsCovered: 1, amount: 111.11M, dueDate: DateTime.Parse("4/1/2019"), paymentSource: E_DisbursementPaidByT.EscrowImpounds),
                    ToTRIDDisbursement(monthsCovered: 1, amount: 111.11M, dueDate: DateTime.Parse("6/1/2019"), paymentSource: E_DisbursementPaidByT.EscrowImpounds)
                });
            AddTRIDHousingExpense(dataLoan, housingExpenseType: E_HousingExpenseTypeT.OtherTaxes2, cushion: 2,
                disbursements: new List<SingleDisbursement>()
                {
                    ToTRIDDisbursement(monthsCovered: 2, amount: 444.44M, dueDate: DateTime.Parse("7/1/2018"), paymentSource: E_DisbursementPaidByT.EscrowImpounds),
                    ToTRIDDisbursement(monthsCovered: 3, amount: 666.66M, dueDate: DateTime.Parse("8/1/2018"), paymentSource: E_DisbursementPaidByT.EscrowImpounds),
                    ToTRIDDisbursement(monthsCovered: 1, amount: 222.22M, dueDate: DateTime.Parse("10/1/2018"), paymentSource: E_DisbursementPaidByT.EscrowImpounds),
                    ToTRIDDisbursement(monthsCovered: 2, amount: 444.44M, dueDate: DateTime.Parse("11/1/2018"), paymentSource: E_DisbursementPaidByT.Seller),
                    ToTRIDDisbursement(monthsCovered: 3, amount: 666.66M, dueDate: DateTime.Parse("12/1/2018"), paymentSource: E_DisbursementPaidByT.EscrowImpounds),
                    ToTRIDDisbursement(monthsCovered: 1, amount: 222.22M, dueDate: DateTime.Parse("6/1/2019"), paymentSource: E_DisbursementPaidByT.EscrowImpounds)
                });
            AddTRIDHousingExpense(dataLoan, housingExpenseType: E_HousingExpenseTypeT.OtherTaxes3, cushion: 2,
                disbursements: new List<SingleDisbursement>()
                {
                    ToTRIDDisbursement(monthsCovered: 1, amount: 333.33M, dueDate: DateTime.Parse("12/1/2018"), paymentSource: E_DisbursementPaidByT.Seller),
                    ToTRIDDisbursement(monthsCovered: 1, amount: 333.33M, dueDate: DateTime.Parse("1/1/2019"), paymentSource: E_DisbursementPaidByT.EscrowImpounds),
                    ToTRIDDisbursement(monthsCovered: 10, amount: 3333.30M, dueDate: DateTime.Parse("6/1/2019"), paymentSource: E_DisbursementPaidByT.EscrowImpounds)
                });
            AddTRIDHousingExpense(dataLoan, housingExpenseType: E_HousingExpenseTypeT.OtherTaxes4, cushion: 2,
                disbursements: new List<SingleDisbursement>()
                {
                    ToTRIDDisbursement(monthsCovered: 11, amount: 4888.84M, dueDate: DateTime.Parse("8/1/2018"), paymentSource: E_DisbursementPaidByT.Lender),
                    ToTRIDDisbursement(monthsCovered: 1, amount: 444.44M, dueDate: DateTime.Parse("9/1/2018"), paymentSource: E_DisbursementPaidByT.EscrowImpounds)
                });

            List<string> failedTests = new List<string>();

            /* Expected Output for Regulatory Mode:
              Aggregate Escrow Adjustment: -$3,165.68
              Total Escrow Collected at Closing: $6,276.62
            */
            dataLoan.sAggEscrowCalcModeT = E_AggregateEscrowCalculationModeT.Regulatory;
            TestTRIDEscrowTotal(expected: new Dictionary<string, object>()
                {
                    { nameof(CPageData.sAggregateAdjRsrv), -3165.68M },
                    { nameof(CPageData.sGfeInitialImpoundDeposit_rep), "$6,276.62" }
                },
                dataLoan: dataLoan,
                failedTests: failedTests);

            /* Expected Output for Customary Mode, No impounds minimum:
              Aggregate Escrow Adjustment: -$2,876.51
              Total Escrow Collected at Closing: -$407.43
            */
            dataLoan.sAggEscrowCalcModeT = E_AggregateEscrowCalculationModeT.Customary;
            dataLoan.sCustomaryEscrowImpoundsCalcMinT = E_CustomaryEscrowImpoundsCalcMinT.NoMin;
            TestTRIDEscrowTotal(expected: new Dictionary<string, object>()
                {
                    { nameof(CPageData.sAggregateAdjRsrv), -2876.51M },
                    { nameof(CPageData.sGfeInitialImpoundDeposit_rep), "($407.43)" }
                },
                dataLoan: dataLoan,
                failedTests: failedTests);

            /* Expected Output for Customary Mode, Zero impounds minimum:
              Aggregate Escrow Adjustment: -$2,876.51
              Total Escrow Collected at Closing: $518.49
            */
            dataLoan.sAggEscrowCalcModeT = E_AggregateEscrowCalculationModeT.Customary;
            dataLoan.sCustomaryEscrowImpoundsCalcMinT = E_CustomaryEscrowImpoundsCalcMinT.Zero;
            TestTRIDEscrowTotal(expected: new Dictionary<string, object>()
                {
                    { nameof(CPageData.sAggregateAdjRsrv), -2876.51M },
                    { nameof(CPageData.sGfeInitialImpoundDeposit_rep), "$518.49" }
                },
                dataLoan: dataLoan,
                failedTests: failedTests);

            /* Expected Output for Customary Mode, Cushion impounds minimum:
              Aggregate Escrow Adjustment: -$2,876.51
              Total Escrow Collected at Closing: $4,477.31
            */
            dataLoan.sAggEscrowCalcModeT = E_AggregateEscrowCalculationModeT.Customary;
            dataLoan.sCustomaryEscrowImpoundsCalcMinT = E_CustomaryEscrowImpoundsCalcMinT.Cushion;
            TestTRIDEscrowTotal(expected: new Dictionary<string, object>()
                {
                    { nameof(CPageData.sAggregateAdjRsrv), -2876.51M },
                    { nameof(CPageData.sGfeInitialImpoundDeposit_rep), "$4,477.31" }
                },
                dataLoan: dataLoan,
                failedTests: failedTests);

            /* Expected Output for Customary Mode, Zero_AdjustAggregate impounds minimum:
              Aggregate Escrow Adjustment: -$3,802.43
              Total Escrow Collected at Closing: -$407.43
            */
            dataLoan.sAggEscrowCalcModeT = E_AggregateEscrowCalculationModeT.Customary;
            dataLoan.sCustomaryEscrowImpoundsCalcMinT = E_CustomaryEscrowImpoundsCalcMinT.Zero_AdjustAggregate;
            TestTRIDEscrowTotal(expected: new Dictionary<string, object>()
                {
                    { nameof(CPageData.sAggregateAdjRsrv), -3802.43M },
                    { nameof(CPageData.sGfeInitialImpoundDeposit_rep), "($407.43)" }
                },
                dataLoan: dataLoan,
                failedTests: failedTests);

            /* Expected Output for Customary Mode, Cushion_AdjustAggregate impounds minimum:
              Aggregate Escrow Adjustment: -$7,761.25
              Total Escrow Collected at Closing: -$407.43
            */
            dataLoan.sAggEscrowCalcModeT = E_AggregateEscrowCalculationModeT.Customary;
            dataLoan.sCustomaryEscrowImpoundsCalcMinT = E_CustomaryEscrowImpoundsCalcMinT.Cushion_AdjustAggregate;
            TestTRIDEscrowTotal(expected: new Dictionary<string, object>()
                {
                    { nameof(CPageData.sAggregateAdjRsrv), -7761.25M },
                    { nameof(CPageData.sGfeInitialImpoundDeposit_rep), "($407.43)" }
                },
                dataLoan: dataLoan,
                failedTests: failedTests);


            /* Expected Output for Customary2 Mode, No impounds minimum:
              Aggregate Escrow Adjustment: -$1,098.76
              Total Escrow Collected at Closing: $7,374.34
            */
            dataLoan.sAggEscrowCalcModeT = E_AggregateEscrowCalculationModeT.Customary2;
            dataLoan.sCustomaryEscrowImpoundsCalcMinT = E_CustomaryEscrowImpoundsCalcMinT.NoMin;
            TestTRIDEscrowTotal(expected: new Dictionary<string, object>()
                {
                    { nameof(CPageData.sAggregateAdjRsrv), -1098.76M },
                    { nameof(CPageData.sGfeInitialImpoundDeposit_rep), "$7,374.34" }
                },
                dataLoan: dataLoan,
                failedTests: failedTests);

            /* Expected Output for Customary2 Mode, Zero impounds minimum:
              Aggregate Escrow Adjustment: -$1,098.76
              Total Escrow Collected at Closing: $7,374.34
            */
            dataLoan.sAggEscrowCalcModeT = E_AggregateEscrowCalculationModeT.Customary2;
            dataLoan.sCustomaryEscrowImpoundsCalcMinT = E_CustomaryEscrowImpoundsCalcMinT.Zero;
            TestTRIDEscrowTotal(expected: new Dictionary<string, object>()
                {
                    { nameof(CPageData.sAggregateAdjRsrv), -1098.76M },
                    { nameof(CPageData.sGfeInitialImpoundDeposit_rep), "$7,374.34" }
                },
                dataLoan: dataLoan,
                failedTests: failedTests);

            /* Expected Output for Customary2 Mode, Cushion impounds minimum:
              Aggregate Escrow Adjustment: -$1,098.76
              Total Escrow Collected at Closing: $8,798.20
            */
            dataLoan.sAggEscrowCalcModeT = E_AggregateEscrowCalculationModeT.Customary2;
            dataLoan.sCustomaryEscrowImpoundsCalcMinT = E_CustomaryEscrowImpoundsCalcMinT.Cushion;
            TestTRIDEscrowTotal(expected: new Dictionary<string, object>()
                {
                    { nameof(CPageData.sAggregateAdjRsrv), -1098.76M },
                    { nameof(CPageData.sGfeInitialImpoundDeposit_rep), "$8,798.20" }
                },
                dataLoan: dataLoan,
                failedTests: failedTests);

            /* Expected Output for Customary2 Mode, Zero_AdjustAggregate impounds minimum:
              Aggregate Escrow Adjustment: -$1,098.76
              Total Escrow Collected at Closing: $7,374.34
            */
            dataLoan.sAggEscrowCalcModeT = E_AggregateEscrowCalculationModeT.Customary2;
            dataLoan.sCustomaryEscrowImpoundsCalcMinT = E_CustomaryEscrowImpoundsCalcMinT.Zero_AdjustAggregate;
            TestTRIDEscrowTotal(expected: new Dictionary<string, object>()
                {
                    { nameof(CPageData.sAggregateAdjRsrv), -1098.76M },
                    { nameof(CPageData.sGfeInitialImpoundDeposit_rep), "$7,374.34" }
                },
                dataLoan: dataLoan,
                failedTests: failedTests);

            /* Expected Output for Customary2 Mode, Cushion_AdjustAggregate impounds minimum:
              Aggregate Escrow Adjustment: -$2,522.62
              Total Escrow Collected at Closing: $7,374.34
            */
            dataLoan.sAggEscrowCalcModeT = E_AggregateEscrowCalculationModeT.Customary2;
            dataLoan.sCustomaryEscrowImpoundsCalcMinT = E_CustomaryEscrowImpoundsCalcMinT.Cushion_AdjustAggregate;
            TestTRIDEscrowTotal(expected: new Dictionary<string, object>()
                {
                    { nameof(CPageData.sAggregateAdjRsrv), -2522.62M },
                    { nameof(CPageData.sGfeInitialImpoundDeposit_rep), "$7,374.34" }
                },
                dataLoan: dataLoan,
                failedTests: failedTests);

            if (failedTests.Count > 0)
            {
                Assert.Fail(string.Join("\r\n\r\n", failedTests.ToArray()));
            }
        }
        private static CPageData PrepareLoanForTRIDAggregateAnalysisTest(string firstPaymentDate)
        {
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitLoad();
            dataLoan.sSchedDueD1Lckd = true;
            dataLoan.sSchedDueD1_rep = firstPaymentDate;
            dataLoan.sClosingCostFeeVersionT = E_sClosingCostFeeVersionT.ClosingCostFee2015;
            dataLoan.sDisclosureRegulationT = E_sDisclosureRegulationT.TRID;
            dataLoan.sIsHousingExpenseMigrated = true;
            HousingExpenses expenses = dataLoan.sHousingExpenses;

            return dataLoan;
        }

        private static void AddTRIDHousingExpense(CPageData dataLoan, E_HousingExpenseTypeT housingExpenseType, int cushion, List<SingleDisbursement> disbursements)
        {
            BaseHousingExpense housingExpense = dataLoan.GetExpense(housingExpenseType);
            housingExpense.IsEscrowedAtClosing = E_TriState.Yes;
            housingExpense.IsPrepaid = true;
            housingExpense.AnnualAmtCalcType = E_AnnualAmtCalcTypeT.Disbursements;
            housingExpense.DisbursementScheduleMonths[0] = cushion;
            foreach (var disbursement in disbursements)
            {
                housingExpense.AddDisbursement(disbursement);
            }
        }

        private static SingleDisbursement ToTRIDDisbursement(int monthsCovered, decimal amount, DateTime dueDate, E_DisbursementPaidByT paymentSource)
        {
            SingleDisbursement disbursement = new SingleDisbursement();
            disbursement.DisbursementType = E_DisbursementTypeT.Actual;
            disbursement.CoveredMonths = monthsCovered;
            disbursement.DisbursementAmt = amount;
            disbursement.DueDate = dueDate;
            disbursement.PaidBy = paymentSource;
            ////Assume escrow disbursements to be paid after closing, non-escrow disbursements paid at closing
            disbursement.DisbPaidDateType = paymentSource == E_DisbursementPaidByT.EscrowImpounds ? E_DisbPaidDateType.AfterClosing : E_DisbPaidDateType.AtClosing;
            return disbursement;
        }

        private static void TestTRIDHousingExpense(CPageData dataLoan, Dictionary<string, object> expected, BaseHousingExpense actual, List<string> failedTests)
        {
            var mismatchHousingExpProperties = TestUtilities.FindUnequalProperties(expected, actual);
            if (mismatchHousingExpProperties.Count > 0)
            {
                failedTests.Add($"Mismatch between expected and actual {actual.HousingExpenseType.ToString()} in {dataLoan.sAggEscrowCalcModeT.ToString()} - {dataLoan.sCustomaryEscrowImpoundsCalcMinT.ToString()} mode."
                    + $"\r\nExpected: { TestUtilities.FilterByKeys(expected, mismatchHousingExpProperties.Keys).ToLiteral()}"
                    + $"\r\nActual: {mismatchHousingExpProperties.ToLiteral()}");
            }
        }

        private static void TestTRIDEscrowTotal(Dictionary<string, object> expected, CPageData dataLoan, List<string> failedTests)
        {
            var mismatchEscrowTotals = TestUtilities.FindUnequalProperties(expected, dataLoan);
            if (mismatchEscrowTotals.Count > 0)
            {
                failedTests.Add($"Mismatch between expected and actual escrow totals in {dataLoan.sAggEscrowCalcModeT.ToString()} - {dataLoan.sCustomaryEscrowImpoundsCalcMinT.ToString()} mode."
                    + $"\r\nExpected: { TestUtilities.FilterByKeys(expected, mismatchEscrowTotals.Keys).ToLiteral()}"
                    + $"\r\nActual: {mismatchEscrowTotals.ToLiteral()}");
            }

        }
    }
}