/// Author: David Dao

using System;
using DataAccess;
using LendersOffice.Audit;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.ObjLib.Task;
using LendersOffice.Security;
using LendOSolnTest.Common;
using NUnit.Framework;

namespace LendOSolnTest.Loan
{
    [TestFixture]
    public class LoanCreateTest
    {
        Guid m_loanId = Guid.Empty;
        AbstractUserPrincipal x_principal = null;

        private AbstractUserPrincipal m_currentPrincipal 
        {
            get 
            {
                if (null == x_principal) 
                {
                    x_principal = LoginTools.LoginAs(TestAccountType.LoTest001);
                }

                return x_principal;
            }

        }

        private Guid GetAutoTestLoanId()
        {
            Guid loanId = Tools.GetLoanIdByLoanName(m_currentPrincipal.BrokerId, "AUTOTEST");

            if (loanId == Guid.Empty)
            {
                var loanCreator = CLoanFileCreator.GetCreator(m_currentPrincipal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
                loanId = loanCreator.CreateBlankLoanFile(); // at some point want this to be stored in the stage and cached?

                CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(LoanCreateTest));
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                dataLoan.sLNm = "AUTOTEST";
                dataLoan.Save();
            }

            return loanId;
        }

        private Guid GetTemplateTestLoanId()
        {
            Guid loanId = Tools.GetLoanIdByLoanName(m_currentPrincipal.BrokerId, "template1");

            if (loanId == Guid.Empty)
            {
                var loanCreator = CLoanFileCreator.GetCreator(m_currentPrincipal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
                loanId = loanCreator.CreateBlankLoanTemplate(); // at some point want this to be stored in the stage and cached?

                CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(LoanCreateTest));
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                dataLoan.sLNm = "template1";
                dataLoan.Save();
            }

            return loanId;
        }

        [TearDown]
        public void DeleteLoan()
        {
            if (Guid.Empty != m_loanId) 
            {
                Tools.DeclareLoanFileInvalid(m_currentPrincipal, m_loanId, false /* sendNotification */, false /*generateLinkLoanMsgEvent */);
                m_loanId = Guid.Empty;
            }
        }

        [SetUp]
        public void Init()
        {
            m_loanId = Guid.Empty;
            AbstractUserPrincipal principal = m_currentPrincipal;
            Assert.IsNotNull(principal);
            System.Threading.Thread.CurrentPrincipal = m_currentPrincipal;
        }

        [Test]
        public void LoadInvalidLoanFile() 
        {
            try 
            {
                Guid sLId = Guid.NewGuid();
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(LoanCreateTest));
                dataLoan.InitLoad();
                Assert.Fail("Load Invalid loan does not throw exception. LoanId=" + sLId);
            } 
            catch (LoanNotFoundException) 
            {
            }

            
        }

        [Test]
        [ExpectedException(typeof(CBaseException))]
        public void TestInvalidStateLoanFile() 
        {
            Guid srcLoanId = GetAutoTestLoanId();

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(srcLoanId, typeof(LoanCreateTest));

            dataLoan.InitLoad();

            dataLoan.sAgencyCaseNum = "Blah";
            dataLoan.Save(); // Should throw exception.
        }

        [Test]
        public void DeleteInvalidLoan()
        {
            try 
            {
                Guid sLId = Guid.NewGuid();
                Tools.DeclareLoanFileInvalid(m_currentPrincipal, sLId, false /* sendNotification */, false /*generateLinkLoanMsgEvent */);
                Assert.Fail("Delete Invalid Loan does not throw exception. LoanId=" + sLId);
            } 
            catch (LoanNotFoundException)
            {

            }
        }
        [Test]
        public void CreateLoanFromTemplate()
        {
            Guid templateId = GetTemplateTestLoanId();

            CLoanFileCreator fileCreator = CLoanFileCreator.GetCreator(m_currentPrincipal, LendersOffice.Audit.E_LoanCreationSource.FromTemplate);
            m_loanId = fileCreator.CreateLoanFromTemplate(templateId: templateId);

            Assert.AreNotEqual(Guid.Empty, m_loanId);

            var expectedTasks = Task.GetTasksInLoan(m_currentPrincipal.BrokerId, templateId);
            var actualTasks = Task.GetTasksInLoan(m_currentPrincipal.BrokerId, m_loanId);
            Assert.AreEqual(expectedTasks.Count, actualTasks.Count, "Template tasks not copied over");
        }

        [Test]
        public void CreateLoanFromTemplateAsPMLUser()
        {
            
            try
            {
                var PML_UserId = LoginTools.LoginAs(TestAccountType.Pml_User0);
                Guid templateId = GetTemplateTestLoanId();

                CLoanFileCreator fileCreator = CLoanFileCreator.GetCreator(PML_UserId, LendersOffice.Audit.E_LoanCreationSource.FromTemplate);
                m_loanId = fileCreator.CreateLoanFromTemplate(templateId: templateId);

                Assert.AreNotEqual(Guid.Empty, m_loanId);

                var expectedTasks = Task.GetTasksInLoan(m_currentPrincipal.BrokerId, templateId);
                var actualTasks = Task.GetTasksInLoan(m_currentPrincipal.BrokerId, m_loanId);
                Assert.AreEqual(expectedTasks.Count, actualTasks.Count, "Template tasks not copied over");
            }
            finally
            {
                //The first time we called this it actually sets the thread's current user principal, 
                //so we need to call it again before we leave.
                LoginTools.LoginAs(TestAccountType.LoTest001);
            }
        }

        [Test]
        public void DuplicateLoanFile()
        {
            Guid srcLoanId = GetAutoTestLoanId();

            CLoanFileCreator fileCreator = CLoanFileCreator.GetCreator(m_currentPrincipal, E_LoanCreationSource.FileDuplication);
            m_loanId = fileCreator.DuplicateLoanFile(sourceLoanId: srcLoanId);
            Assert.AreNotEqual( Guid.Empty, m_loanId, "Can not duplicate new loan." );

            CPageData srcLoan = CreatePageData(srcLoanId);
            srcLoan.InitLoad();

            CPageData newLoan = CreatePageData(m_loanId);
            newLoan.InitLoad();

            Assert.AreNotEqual(srcLoan.sTotalScoreUniqueLoanId, newLoan.sTotalScoreUniqueLoanId, "Source.sTotalScoreUniqueLoanId <> dest.sTotalScoreUniqueLoanId");
            Assert.That(!string.IsNullOrEmpty(newLoan.sTotalScoreUniqueLoanId), "sTotalScoreUniqueLoanId cannot be empty");

            var expectedTasks = Task.GetTasksInLoan(m_currentPrincipal.BrokerId, srcLoanId);
            var actualTasks = Task.GetTasksInLoan(m_currentPrincipal.BrokerId, m_loanId);
            Assert.AreEqual(expectedTasks.Count, actualTasks.Count, "Tasks not copied over to duplicate");
        }


        [Test]
        public void CreateLead() 
        {
            CLoanFileCreator fileCreator = CLoanFileCreator.GetCreator(m_currentPrincipal, E_LoanCreationSource.LeadCreate);
            m_loanId = fileCreator.CreateLead(templateId: Guid.Empty);

            Assert.AreNotEqual(Guid.Empty, m_loanId);
            
            CPageData dataLoan = CreatePageData(m_loanId);
            dataLoan.InitLoad();

            Assert.That(!string.IsNullOrEmpty(dataLoan.sTotalScoreUniqueLoanId), "sTotalScoreUniqueLoanId");
        }

        [Test]
        public void CreateLoanTemplate()
        {
            CLoanFileCreator fileCreator = CLoanFileCreator.GetCreator(m_currentPrincipal, E_LoanCreationSource.UserCreateFromBlank);
            m_loanId = fileCreator.CreateBlankLoanTemplate();

            Assert.AreNotEqual(Guid.Empty, m_loanId);

            CPageData dataLoan = CreatePageData(m_loanId);
            dataLoan.InitLoad();

            Assert.AreEqual(true, dataLoan.IsTemplate);
            Assert.That(!string.IsNullOrEmpty(dataLoan.sTotalScoreUniqueLoanId), "sTotalScoreUniqueLoanId");
        }

        [Test]
        public void CreateBlankLoan()
        {
            CLoanFileCreator fileCreator = CLoanFileCreator.GetCreator(m_currentPrincipal, E_LoanCreationSource.UserCreateFromBlank);
            m_loanId = fileCreator.CreateBlankLoanFile();

            Assert.AreNotEqual(Guid.Empty, m_loanId);

            CPageData dataLoan = CreatePageData(m_loanId);
            dataLoan.InitLoad();

            Assert.AreEqual(false, dataLoan.IsTemplate);
            Assert.That(!string.IsNullOrEmpty(dataLoan.sTotalScoreUniqueLoanId), "sTotalScoreUniqueLoanId");

        }

        private CPageData CreatePageData(Guid sLId)
        {
            return CPageData.CreatePmlPageDataUsingSmartDependency(sLId, typeof(LoanCreateTest));
        }
    }
}