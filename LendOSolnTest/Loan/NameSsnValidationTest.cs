﻿namespace LendOSolnTest.Loan
{
    using Fakes;
    using DataAccess;
    using DataAccess.LoanValidation;
    using NUnit.Framework;

    [TestFixture]
    public class NameSsnValidationTest
    {
        private const string FirstName = "Alice";
        private const string LastName = "Test";
        private const string Ssn = "123-12-1234";

        [Test]
        [TestCase(true, FirstName, LastName, Ssn, ExpectedResult = E_LoanValidationResultT.OK)]
        [TestCase(false, FirstName, LastName, Ssn, ExpectedResult = E_LoanValidationResultT.OK)]
        [TestCase(true, null, LastName, Ssn, ExpectedResult = E_LoanValidationResultT.Error)]
        [TestCase(false, null, LastName, Ssn, ExpectedResult = E_LoanValidationResultT.Error)]
        [TestCase(true, FirstName, null, Ssn, ExpectedResult = E_LoanValidationResultT.Error)]
        [TestCase(false, FirstName, null, Ssn, ExpectedResult = E_LoanValidationResultT.Error)]
        [TestCase(true, FirstName, LastName, null, ExpectedResult = E_LoanValidationResultT.OK)]
        [TestCase(false, FirstName, LastName, null, ExpectedResult = E_LoanValidationResultT.Error)]
        public E_LoanValidationResultT PmlBorrowerNameSsnValidationRule_ReturnsExpectedResult(
            bool userCanRunPricingWithoutCreditReport,
            string firstName,
            string lastName,
            string ssn)
        {
            var validationRule = new PmlBorrowerNameSsnValidationRule(userCanRunPricingWithoutCreditReport);

            var loan = this.GetInitializedLoan();
            this.SetBorrowerAppData(loan, firstName, lastName, ssn);

            var validationResult = validationRule.Validate(loan.PageBase);
            return validationResult.Result;
        }

        [Test]
        [TestCase(true, FirstName, LastName, Ssn, ExpectedResult = E_LoanValidationResultT.OK)]
        [TestCase(false, FirstName, LastName, Ssn, ExpectedResult = E_LoanValidationResultT.OK)]
        [TestCase(true, null, LastName, Ssn, ExpectedResult = E_LoanValidationResultT.Error)]
        [TestCase(false, null, LastName, Ssn, ExpectedResult = E_LoanValidationResultT.Error)]
        [TestCase(true, FirstName, null, Ssn, ExpectedResult = E_LoanValidationResultT.Error)]
        [TestCase(false, FirstName, null, Ssn, ExpectedResult = E_LoanValidationResultT.Error)]
        [TestCase(true, FirstName, LastName, null, ExpectedResult = E_LoanValidationResultT.OK)]
        [TestCase(false, FirstName, LastName, null, ExpectedResult = E_LoanValidationResultT.Error)]
        public E_LoanValidationResultT PmlCoborrowerNameSsnValidationRule_ReturnsExpectedResult(
            bool userCanRunPricingWithoutCreditReport,
            string firstName,
            string lastName,
            string ssn)
        {
            var validationRule = new PmlCoborrowerNameSsnValidationRule(userCanRunPricingWithoutCreditReport);

            var loan = this.GetInitializedLoan();
            this.SetCoborrowerAppData(loan, firstName, lastName, ssn);

            var validationResult = validationRule.Validate(loan.PageBase);
            return validationResult.Result;
        }

        private void SetBorrowerAppData(CPageData loan, string firstName, string lastName, string ssn)
        {
            var app = loan.GetAppData(0);
            app.aBFirstNm = firstName;
            app.aBLastNm = lastName;
            app.aBSsn = ssn;
        }

        private void SetCoborrowerAppData(CPageData loan, string firstName, string lastName, string ssn)
        {
            var app = loan.GetAppData(0);
            app.aCFirstNm = firstName;
            app.aCLastNm = lastName;
            app.aCSsn = ssn;
        }

        private FakePageData GetInitializedLoan()
        {
            var loan = FakePageData.Create();
            loan.InitLoad();
            return loan;
        }
    }
}
