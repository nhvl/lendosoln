﻿namespace LendOSolnTest.Loan
{
    using DataAccess;
    using LendersOffice.Common.SerializationTypes;
    using LendOSolnTest.Fakes;
    using NUnit.Framework;

    [TestFixture]
    public class GseTargetApplicationTypeTest
    {
        private const string PriorToUladRequirementDate = "1/1/2018";
        private const string OnUladRequirementDate = "2/1/2020";
        private const string AfterUladRequirementDate = "3/1/2020";

        private static readonly object[] sGseTargetApplicationT_AppSubmittedLoanOpenedOnFile_UsesAppSubmittedDate_Source = new object[]
        {
            // Loan opened date, app submitted date, expected target app type
            new object[] { PriorToUladRequirementDate, PriorToUladRequirementDate, GseTargetApplicationT.Legacy },
            new object[] { PriorToUladRequirementDate, OnUladRequirementDate, GseTargetApplicationT.Ulad2019 },
            new object[] { PriorToUladRequirementDate, AfterUladRequirementDate, GseTargetApplicationT.Ulad2019 },
            new object[] { OnUladRequirementDate, PriorToUladRequirementDate, GseTargetApplicationT.Legacy },
            new object[] { OnUladRequirementDate, OnUladRequirementDate, GseTargetApplicationT.Ulad2019 },
            new object[] { OnUladRequirementDate, AfterUladRequirementDate, GseTargetApplicationT.Ulad2019 },
            new object[] { AfterUladRequirementDate, PriorToUladRequirementDate, GseTargetApplicationT.Legacy },
            new object[] { AfterUladRequirementDate, OnUladRequirementDate, GseTargetApplicationT.Ulad2019 },
            new object[] { AfterUladRequirementDate, AfterUladRequirementDate, GseTargetApplicationT.Ulad2019 },
        };

        [Test]
        public void sGseTargetApplicationT_NoDates_ReturnsBlank()
        {
            var loan = this.GetInitializedLoan();
            loan.EnsureMigratedToAtLeastUladCollectionVersion(E_sLqbCollectionT.UseLqbCollections);

            loan.sAppSubmittedDLckd = true;
            loan.sAppSubmittedD_rep = string.Empty;
            loan.sOpenedD_rep = string.Empty;

            Assert.AreEqual(GseTargetApplicationT.Blank, loan.sGseTargetApplicationT);
        }

        [TestCase(PriorToUladRequirementDate, GseTargetApplicationT.Legacy)]
        [TestCase(OnUladRequirementDate, GseTargetApplicationT.Ulad2019)]
        [TestCase(AfterUladRequirementDate, GseTargetApplicationT.Ulad2019)]
        public void sGseTargetApplicationT_AppSubmittedDateOnly_UsesAppSubmittedDate(string appSubmittedDate, GseTargetApplicationT expectedTargetAppType)
        {
            var loan = this.GetInitializedLoan();
            loan.EnsureMigratedToAtLeastUladCollectionVersion(E_sLqbCollectionT.UseLqbCollections);

            loan.sAppSubmittedDLckd = true;
            loan.sAppSubmittedD_rep = appSubmittedDate;

            Assert.AreEqual(expectedTargetAppType, loan.sGseTargetApplicationT);
        }

        [TestCase(PriorToUladRequirementDate, GseTargetApplicationT.Legacy)]
        [TestCase(OnUladRequirementDate, GseTargetApplicationT.Ulad2019)]
        [TestCase(AfterUladRequirementDate, GseTargetApplicationT.Ulad2019)]
        public void sGseTargetApplicationT_LoanOpenedDateOnly_UsesLoanOpenedDate(string loanOpenedDate, GseTargetApplicationT expectedTargetAppType)
        {
            var loan = this.GetInitializedLoan();
            loan.EnsureMigratedToAtLeastUladCollectionVersion(E_sLqbCollectionT.UseLqbCollections);

            loan.sOpenedD_rep = loanOpenedDate;

            Assert.AreEqual(expectedTargetAppType, loan.sGseTargetApplicationT);
        }

        [TestCaseSource(nameof(sGseTargetApplicationT_AppSubmittedLoanOpenedOnFile_UsesAppSubmittedDate_Source))]
        public void sGseTargetApplicationT_AppSubmittedLoanOpenedOnFile_UsesAppSubmittedDate(
            string loanOpenedDate, 
            string appSubmittedDate,
            GseTargetApplicationT expectedTargetAppType)
        {
            var loan = this.GetInitializedLoan();
            loan.EnsureMigratedToAtLeastUladCollectionVersion(E_sLqbCollectionT.UseLqbCollections);

            loan.sAppSubmittedDLckd = true;
            loan.sAppSubmittedD_rep = appSubmittedDate;
            loan.sOpenedD_rep = loanOpenedDate;

            Assert.AreEqual(expectedTargetAppType, loan.sGseTargetApplicationT);
        }

        [Test]
        public void sGseTargetApplicationT_Always_HonorsLockValue(
            [Values(PriorToUladRequirementDate, OnUladRequirementDate, AfterUladRequirementDate)] string loanOpenedDate,
            [Values(PriorToUladRequirementDate, OnUladRequirementDate, AfterUladRequirementDate)] string appSubmittedDate,
            [Values(GseTargetApplicationT.Blank, GseTargetApplicationT.Legacy, GseTargetApplicationT.Ulad2019)] GseTargetApplicationT lockedValue)
        {
            var loan = this.GetInitializedLoan();
            loan.EnsureMigratedToAtLeastUladCollectionVersion(E_sLqbCollectionT.UseLqbCollections);

            loan.sAppSubmittedDLckd = true;
            loan.sAppSubmittedD_rep = appSubmittedDate;
            loan.sOpenedD_rep = loanOpenedDate;

            loan.sGseTargetApplicationTLckd = true;
            loan.sGseTargetApplicationT = lockedValue;

            Assert.AreEqual(lockedValue, loan.sGseTargetApplicationT);
        }

        [Test]
        public void sGseTargetApplicationT_Always_HonorsArchiveValue(
            [Values(GseTargetApplicationT.Blank, GseTargetApplicationT.Legacy)] GseTargetApplicationT archiveValue)
        {
            var loan = this.GetInitializedLoan();

            loan.EnsureMigratedToAtLeastUladCollectionVersion(E_sLqbCollectionT.UseLqbCollections);

            loan.sGseTargetApplicationTLckd = true;
            loan.sGseTargetApplicationT = archiveValue;

            var archive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed,
                ClosingCostArchive.E_ClosingCostArchiveSource.DocumentGeneration);

            loan = this.GetInitializedLoan();
            loan.ApplyClosingCostArchive(archive);
            Assert.AreEqual(archiveValue, loan.sGseTargetApplicationT);
        }

        [TestCase(E_sLqbCollectionT.Legacy, GseTargetApplicationT.Blank)]
        [TestCase(E_sLqbCollectionT.Legacy, GseTargetApplicationT.Legacy)]
        [TestCase(E_sLqbCollectionT.UseLqbCollections, GseTargetApplicationT.Blank)]
        [TestCase(E_sLqbCollectionT.UseLqbCollections, GseTargetApplicationT.Legacy)]
        [TestCase(E_sLqbCollectionT.UseLqbCollections, GseTargetApplicationT.Ulad2019)]
        public void sGseTargetApplicationT_SetValidValue_DoesNotThrow(E_sLqbCollectionT datalayerType, GseTargetApplicationT lockedValue)
        {
            var loan = this.GetInitializedLoan();

            loan.sBorrowerApplicationCollectionT = datalayerType;
            loan.sGseTargetApplicationT = lockedValue;
        }

        [Test]
        public void sGseTargetApplicationT_SetUladOnLegacyDatalayer_Throws()
        {
            var loan = this.GetInitializedLoan();
            loan.sBorrowerApplicationCollectionT = E_sLqbCollectionT.Legacy;
            Assert.Throws<FieldInvalidValueException>(() => loan.sGseTargetApplicationT = GseTargetApplicationT.Ulad2019);
        }

        [Test]
        public void sGseTargetApplicationT_NonUladDatalayer_ReturnsLegacy(
            [Values(PriorToUladRequirementDate, OnUladRequirementDate, AfterUladRequirementDate)] string loanOpenedDate,
            [Values(PriorToUladRequirementDate, OnUladRequirementDate, AfterUladRequirementDate)] string appSubmittedDate)
        {
            var loan = this.GetInitializedLoan();
            loan.sBorrowerApplicationCollectionT = E_sLqbCollectionT.Legacy;

            loan.sAppSubmittedDLckd = true;
            loan.sAppSubmittedD_rep = appSubmittedDate;
            loan.sOpenedD_rep = loanOpenedDate;

            Assert.AreEqual(GseTargetApplicationT.Legacy, loan.sGseTargetApplicationT);
        }

        private CPageData GetInitializedLoan()
        {
            var loan = FakePageData.Create();
            loan.InitLoad();
            // Initially, updating sBorrowerApplicationCollectionT is only being allowed for
            // test files, so we need to override the loan file type for this test (or ignore the test).
            loan.sLoanFileT = E_sLoanFileT.Test;
            return loan;
        }
    }
}
