﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using LendOSolnTest.Common;
using LendersOffice.Security;
using DataAccess;
using LendersOffice.Constants;

namespace LendOSolnTest.Loan
{
    [TestFixture]
    public class LoanBankerRoundingTest
    {
        private Guid m_sLId = Guid.Empty;

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            LoginTools.LoginAs(TestAccountType.LoTest001);

            BrokerUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;

            CLoanFileCreator creator = CLoanFileCreator.GetCreator(principal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
            m_sLId = creator.CreateBlankLoanFile();

            CPageData dataLoan = CreateDataObject(m_sLId);
            dataLoan.AddNewApp();
        }

        private CPageData CreateDataObject(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(LoanBankerRoundingTest));
        }
        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            if (m_sLId != Guid.Empty)
            {
                Tools.DeclareLoanFileInvalid(BrokerUserPrincipal.CurrentPrincipal, m_sLId, false, false);
            }
        }

        [Test]
        public void TestLoanBankerRounding()
        {
            var testCaseList = new[] {
                new { sNoteIR = 3.4565M, sPurchPrice= 100000.125M, expected_sNoteIR = 3.456M, expected_sPurchPrice = 100000.12M},
                new { sNoteIR = 3.1115M, sPurchPrice= 200000.335M, expected_sNoteIR = 3.112M, expected_sPurchPrice = 200000.34M},
                new { sNoteIR = 3.456500001M, sPurchPrice = 100000.12500001M, expected_sNoteIR = 3.457M, expected_sPurchPrice = 100000.13M},
                new { sNoteIR = 6.125M, sPurchPrice = 200000M, expected_sNoteIR = 6.125M, expected_sPurchPrice = 200000M}
            };

            foreach (var testCase in testCaseList)
            {
                CPageData dataLoan = CreateDataObject(m_sLId);
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                dataLoan.sLPurposeT = E_sLPurposeT.Purchase;
                dataLoan.sNoteIR = testCase.sNoteIR;
                dataLoan.sPurchPrice = testCase.sPurchPrice;
                dataLoan.Save();

                dataLoan = CreateDataObject(m_sLId);
                dataLoan.InitLoad();
                Assert.AreEqual(testCase.expected_sNoteIR, dataLoan.sNoteIR, "sNoteIR");
                Assert.AreEqual(testCase.expected_sPurchPrice, dataLoan.sPurchPrice, "sPurchPrice");
            }
        }

        [Test]
        public void TestLoanBankerRounding_MultipleApps()
        {
            var testCaseList = new[] {
                new { app0_aBBaseI = 4000.455M, app1_aBBaseI = 5000.455M, expected_app0_aBBaseI = 4000.46M, expected_app1_aBBaseI = 5000.46M},
                new { app0_aBBaseI = 4000.865M, app1_aBBaseI = 5000.865M, expected_app0_aBBaseI = 4000.86M, expected_app1_aBBaseI = 5000.86M}


            };

            foreach (var testCase in testCaseList)
            {
                CPageData dataLoan = CreateDataObject(m_sLId);
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                int nApps = dataLoan.nApps;
                Assert.AreEqual(2, nApps, "Expect exactly 2 apps");

                CAppData dataApp0 = dataLoan.GetAppData(0);
                dataApp0.aBBaseI = testCase.app0_aBBaseI;

                CAppData dataApp1 = dataLoan.GetAppData(1);
                dataApp1.aBBaseI = testCase.app1_aBBaseI;

                dataLoan.Save();

                dataLoan = CreateDataObject(m_sLId);
                dataLoan.InitLoad();

                dataApp0 = dataLoan.GetAppData(0);
                dataApp1 = dataLoan.GetAppData(1);

                Assert.AreEqual(testCase.expected_app0_aBBaseI, dataApp0.aBBaseI, "dataApp0.aBBaseI");
                Assert.AreEqual(testCase.expected_app1_aBBaseI, dataApp1.aBBaseI, "dataApp1.aBBaseI");
            }
        }
    }
}
