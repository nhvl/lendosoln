﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using LendOSolnTest.Common;
using LendersOffice.Security;
using DataAccess;

namespace LendOSolnTest.Loan
{
    [TestFixture]
    public class AverageOutstandingBalanceCalcTest
    {
        /// <summary>
        /// Test to ensure that we match the sample calculation from http://portal.hud.gov/hudportal/HUD?src=/program_offices/housing/comp/premiums/sfpcalc
        /// </summary>
        [Test]
        public void FhaAverageOutstandingBalanceOfficialSampleTest()
        {
            List<decimal> actualAOB = FhaPremiumCalculator.Calculate(106605, 7.5M, 0.5M, 2.25M, 745.40M, 360);

            Assert.AreEqual(43.26M, actualAOB[0]);
            Assert.AreEqual(42.85M, actualAOB[1]);
        }

        /// <summary>
        /// Test to ensure that we match the sample calculation from https://usdalinc.sc.egov.usda.gov/docs/rd/sfh/annualfee/Guaranteed_Annual_Fee_Calculation.pdf
        /// </summary>
        [Test]
        public void UsdaAverageOustandingBalanceOfficialSampleTest()
        {
            List<decimal> actualAOB = UsdaPremiumCalculator.Calculate(100000, 6.000M, 0.300M, 599.55M, 360);

            List<decimal> expectedAOB = new List<decimal>()
            {
                24.86M, // Pmt 12,   index 0
                24.55M, // Pmt 24,   index 1
                24.21M, // Pmt 36,   index 2
                23.85M, // Pmt 48,   index 3
                23.48M, // Pmt 60,   index 4
                23.08M, // Pmt 72,   index 5
                22.65M, // Pmt 84,   index 6
                22.20M, // Pmt 96,   index 7
                21.72M, // Pmt 108,  index 8
                21.21M, // Pmt 120,  index 9
                20.67M, // Pmt 132,  index 10
                20.09M, // Pmt 144,  index 11
                19.48M, // Pmt 156,  index 12
                18.84M, // Pmt 168,  index 13
                18.15M, // Pmt 180,  index 14
                17.42M, // Pmt 192,  index 15
                16.65M, // Pmt 204,  index 16
                15.82M, // Pmt 216,  index 17
                14.95M, // Pmt 228,  index 18
                14.02M, // Pmt 240,  index 19
                13.04M, // Pmt 252,  index 20
                12.00M, // Pmt 264,  index 21
                10.89M, // Pmt 276,  index 22
                9.71M,  // Pmt 288,  index 23
                8.46M,  // Pmt 300,  index 24
                7.13M,  // Pmt 312,  index 25
                5.72M,  // Pmt 324,  index 26
                4.23M,  // Pmt 336,  index 27
                2.64M,  // Pmt 348,  index 28
                0.95M,  // Pmt 360,  index 29
            };

            Assert.AreEqual(expectedAOB, actualAOB);
        }

        /// <summary>
        /// Test to ensure that we continue to pad the FHA AOB list with $0 when the balance runs out.
        /// </summary>
        [Test]
        public void FhaAobCalcEarlyAmortTableTerminationTest()
        {
            List<decimal> actualAOB = FhaPremiumCalculator.Calculate(106605, 7.5M, 0.5M, 2.25M, 745.40M, 480);

            Assert.AreEqual(43.26M, actualAOB[0], "Wrong AOB MI payment in year 1");
            Assert.AreEqual(0, actualAOB[30], "Wrong AOB MI payment in year 31");
            Assert.AreEqual(0, actualAOB[39], "Wrong AOB MI payment in year 40");
            Assert.AreEqual(40, actualAOB.Count, "Wrong number of years of MI premiums");
        }

        /// <summary>
        /// Test to ensure that the FHA AOB calculation continues to work as expected when the loan is
        /// not due in a whole number of years.
        /// </summary>
        [Test]
        public void FhaAobCalcOddMonthsTest()
        {
            List<decimal> actualAOB = FhaPremiumCalculator.Calculate(98188.4M, 3.75M, 0.85M, 1.75M, 458.20M, 355);

            List<decimal> expectedAOB = new List<decimal>()
            {
                67.77M, // 0
                66.46M, // 1
                65.10M, // 2
                63.69M, // 3
                62.23M, // 4
                60.71M, // 5
                59.13M, // 6
                57.49M, // 7
                55.79M, // 8
                54.02M, // 9
                52.19M, // 10
                50.29M, // 11
                48.31M, // 12
                46.26M, // 13
                44.13M, // 14
                41.92M, // 15
                39.63M, // 16
                37.24M, // 17
                34.77M, // 18
                32.20M, // 19
                29.54M, // 20
                26.77M, // 21
                23.90M, // 22
                20.91M, // 23
                17.82M, // 24
                14.60M, // 25
                11.26M, // 26
                7.80M,  // 27
                4.20M,  // 28
                0.74M,  // 29
            };

            Assert.AreEqual(expectedAOB, actualAOB);
        }

        /// <summary>
        /// Test to ensure that we continue to pad the USDA AOB list with $0 when the balance runs out.
        /// </summary>
        [Test]
        public void UsdaAobCalcEarlyAmortTableTerminationTest()
        {
            List<decimal> actualAOB = UsdaPremiumCalculator.Calculate(100000, 6.000M, 0.300M, 599.55M, 480);

            Assert.AreEqual(24.86M, actualAOB[0], "Wrong AOB MI payment in year 1");
            Assert.AreEqual(0, actualAOB[30], "Wrong AOB MI payment in year 31");
            Assert.AreEqual(0, actualAOB[39], "Wrong AOB MI payment in year 40");
            Assert.AreEqual(40, actualAOB.Count, "Wrong number of years of MI premiums");
        }

        /// <summary>
        /// Test to ensure that the USDA AOB calculation continues to work as expected when the loan is
        /// not due in a whole number of years.
        /// </summary>
        [Test]
        public void UsdaAobCalcOddMonthsTest()
        {
            List<decimal> actualAOB = UsdaPremiumCalculator.Calculate(209037, 12M, 1.2M, 2269.86M, 255);

            List<decimal> expectedAOB = new List<decimal>()
            {
                208.02M, // 0
                205.61M, // 1
                202.90M, // 2
                199.84M, // 3
                196.40M, // 4
                192.52M, // 5
                188.15M, // 6
                183.23M, // 7
                177.68M, // 8
                171.43M, // 9
                164.38M, // 10
                156.44M, // 11
                147.49M, // 12
                137.41M, // 13
                126.05M, // 14
                113.25M, // 15
                98.82M,  // 16
                82.57M,  // 17
                64.25M,  // 18
                43.61M,  // 19
                20.36M,  // 20
                1.12M,   // 21
            };

            Assert.AreEqual(expectedAOB, actualAOB);
        }

        /// <summary>
        /// Do a basic test of the GetFhaManuallyMipList function
        /// </summary>
        [Test]
        public void GetFhaManuallyMipListTest()
        {
            AmortTableDataStub amortTableDataStub;

            // Depending on the type defaults to be sane (i.e. 0) for most things...
            amortTableDataStub = new AmortTableDataStub(new LosConvert());

            amortTableDataStub.sFinMethT = E_sFinMethT.Fixed;
            amortTableDataStub.sLT = E_sLT.Conventional;
            amortTableDataStub.sOptionArmMinPayOptionT = E_sOptionArmMinPayOptionT.TeaserRate;
            amortTableDataStub.sRAdjRoundT = E_sRAdjRoundT.Normal;
            amortTableDataStub.sProMInsT = E_PercentBaseT.LoanAmount;
            amortTableDataStub.sSchedDueD1 = CDateTime.Create(new DateTime(635872032000000000));

            amortTableDataStub.sLAmtCalc = 104259.17M;
            amortTableDataStub.sFinalLAmt = 106605M;
            amortTableDataStub.sHouseVal = 250000.00M;
            amortTableDataStub.sTerm = 360;
            amortTableDataStub.sDue = 360;
            amortTableDataStub.sNoteIR = 7.500M;
            amortTableDataStub.sFfUfmipR = 2.250M;
            amortTableDataStub.sFfUfMipIsBeingFinanced = true;
            amortTableDataStub.sProMInsR = 0.500M;
            amortTableDataStub.sProMInsLckd = true;
            amortTableDataStub.sProMIns = 404;

            AmortTable amortTable = new AmortTable(amortTableDataStub, E_AmortizationScheduleT.Standard, false);

            List<decimal> actual = CPageBase.GetFhaManuallyMipListImpl(amortTableDataStub);

            Assert.AreEqual(43.44M, actual[0]);
            Assert.AreEqual(43.04M, actual[1]);
        }
    }
}
