﻿namespace LendOSolnTest.Loan
{
    using Fakes;
    using LendersOffice.Constants;
    using LendersOffice.Migration;
    using LendersOffice.ObjLib.Hmda;
    using NUnit.Framework;

    [TestFixture]
    public class HmdaDataTest
    {
        private static object[][] sHmdaLoanAmount_ReturnsExpectedValue_Source = new[]
        {
            new object[] { true, 100M, false, 200M, LoanVersionT.V26_ReplaceFreeformCreditModelsWithEnums, string.Empty, HmdaActionTaken.Blank, 300M, 400M, 500M, 100M },
            new object[] { false, 100M, true, 200M, LoanVersionT.V26_ReplaceFreeformCreditModelsWithEnums, string.Empty, HmdaActionTaken.Blank, 300M, 400M, 500M, 200M },
            new object[] { false, 100M, false, 200M, LoanVersionT.V25_ConsolidateMonthlyChildSupportPayments, ConstApp.HmdaApplicationDenied, HmdaActionTaken.Blank, 300M, 400M, 500M, 300M },
            new object[] { false, 100M, false, 200M, LoanVersionT.V25_ConsolidateMonthlyChildSupportPayments, string.Empty, HmdaActionTaken.Blank, 300M, 400M, 500M, 500M },
            new object[] { false, 100M, false, 200M, LoanVersionT.V26_ReplaceFreeformCreditModelsWithEnums, string.Empty, HmdaActionTaken.ApplicationDenied, 300M, 400M, 500M, 300M },
            new object[] { false, 100M, false, 200M, LoanVersionT.V26_ReplaceFreeformCreditModelsWithEnums, string.Empty, HmdaActionTaken.Blank, 300M, 400M, 500M, 500M },
            new object[] { false, 100M, false, 200M, LoanVersionT.V25_ConsolidateMonthlyChildSupportPayments, ConstApp.HmdaPurchasedActionTaken, HmdaActionTaken.Blank, 300M, 400M, 500M, 900M },
            new object[] { false, 100M, false, 200M, LoanVersionT.V26_ReplaceFreeformCreditModelsWithEnums, string.Empty, HmdaActionTaken.PurchasedLoan, 300M, 400M, 500M, 900M },
        };

        [Test, TestCaseSource(nameof(sHmdaLoanAmount_ReturnsExpectedValue_Source))]
        public void sHmdaLoanAmount_ReturnsExpectedValue(
            bool sHmdaLoanAmountLckd, 
            decimal sHmdaLoanAmount,
            bool sIsLineOfCredit,
            decimal sCreditLineAmt,
            LoanVersionT sLoanVersionT,
            string sHmdaActionTaken,
            HmdaActionTaken sHmdaActionTakenT,
            decimal sNMLSApplicationAmount,
            decimal sPurchaseAdviceAmortCurtail_Purchasing,
            decimal sPurchPrice,
            decimal expected)
        {
            var loan = FakePageData.Create();
            loan.InitLoad();

            loan.sLoanVersionT = sLoanVersionT;
            loan.sHmdaLoanAmount = sHmdaLoanAmount;
            loan.sHmdaLoanAmountLckd = sHmdaLoanAmountLckd;
            loan.sIsLineOfCredit = sIsLineOfCredit;
            loan.sCreditLineAmt = sCreditLineAmt;
            
            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(sLoanVersionT, LoanVersionT.V26_ReplaceFreeformCreditModelsWithEnums))
            {
                loan.sHmdaActionTakenT = sHmdaActionTakenT;
            }
            else
            {
                loan.sHmdaActionTaken = sHmdaActionTaken;
            }

            loan.sNMLSApplicationAmount_rep = loan.m_convertLos.ToMoneyString(sNMLSApplicationAmount, DataAccess.FormatDirection.ToRep);
            loan.sPurchaseAdviceAmortCurtail_Purchasing_rep = loan.m_convertLos.ToMoneyString(sPurchaseAdviceAmortCurtail_Purchasing, DataAccess.FormatDirection.ToRep);
            loan.sPurchPrice = sPurchPrice;

            Assert.AreEqual(expected, loan.sHmdaLoanAmount);
        }
    }
}
