﻿namespace LendOSolnTest.Loan
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Reflection;
    using System.Security.Principal;
    using System.Threading;
    using DataAccess;
    using LendersOffice.Constants;
    using LendersOffice.Security;
    using LendOSolnTest.Common;
    using NUnit.Framework;

    [TestFixture]
    public class QMCalcsTest
    {
        Guid loanID;
        AbstractUserPrincipal principal;
        IPrincipal oldPrincipal;
        string loanNumber { get { return "TEST_203703"; } }

        [TestFixtureSetUp]
        public void Setup()
        {
            this.oldPrincipal = Thread.CurrentPrincipal;
            this.principal = LoginTools.LoginAs(TestAccountType.Corporate_LoanOfficer);
            var loanID = Tools.GetLoanIdByLoanName(this.principal.BrokerId, this.loanNumber);

            if (loanID == Guid.Empty)
            {
                this.loanID = CreateLoan();
            }
            else
            {
                this.loanID = loanID;
            }
        }

        private Guid CreateLoan()
        {
            var loanCreator = CLoanFileCreator.GetCreator(principal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
            var loanID = loanCreator.CreateBlankLoanFile(); // at some point want this to be stored in the stage and cached?

            var loan = new CFullAccessPageData(
                loanID,
                new string[]
                {
                    "sFinancedAmt", 
                    "sQMTotFeeAmt",
                    "sQMTotFinFeeAmount",
                    "sLNm"
                });

            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            var loanType = typeof(CPageData);
            var privateBindingFlags = BindingFlags.NonPublic | BindingFlags.Instance;
            var baseLoanAsField = loanType.GetField("m_pageBase", privateBindingFlags);
            var baseLoan = (CPageBase)baseLoanAsField.GetValue(loan);
            var baseLoanType = typeof(CPageBase);
            var rowAsField = baseLoanType.GetField("m_rowLoan", privateBindingFlags);
            var row = (IDataContainer)rowAsField.GetValue(baseLoan);
            var dirtyFieldsAsField = baseLoanType.GetField("_m_dirtyFields", privateBindingFlags);
            dirtyFieldsAsField.SetValue(baseLoan, new Hashtable(30));
            var dirtyFields = (Hashtable)dirtyFieldsAsField.GetValue(baseLoan);

            loan.sLNm = "TEST_203703";
            loan.sDisclosureRegulationTLckd = true;
            loan.sDisclosureRegulationT = E_sDisclosureRegulationT.GFE;
            loan.sClosingCostFeeVersionT = E_sClosingCostFeeVersionT.Legacy;
            loan.sEstCloseDLckd = true;

            // scenario from loan "internal203703" in dougtestadmin 
            row["sLAmt"] = 200000.0000m;
            dirtyFields["sLAmt"] = SqlDbType.Decimal;
            row["sPurchPrice"] = 250000.0000m;
            dirtyFields["sPurchPrice"] = SqlDbType.Decimal;
            row["sEquity"] = 50000.0000m;
            dirtyFields["sEquity"] = SqlDbType.Decimal;
            row["sCltvRPeval"] = 100.000m;
            dirtyFields["sCltvRPeval"] = SqlDbType.Decimal;
            row["sApprVal"] = 250000.0000m;
            dirtyFields["sApprVal"] = SqlDbType.Decimal;
            row["sProdFhaUfmip"] = 1.750m;
            dirtyFields["sProdFhaUfmip"] = SqlDbType.Decimal;
            row["sIsOFinNew"] = true;
            dirtyFields["sIsOFinNew"] = SqlDbType.Bit;
            row["sMipPiaMon"] = 12;
            dirtyFields["sMipPiaMon"] = SqlDbType.Int;
            row["sFfUfmip1003IsMigrated"] = true;
            dirtyFields["sFfUfmip1003IsMigrated"] = SqlDbType.Bit;
            row["sPmlCompanyTierId"] = -1;
            dirtyFields["sPmlCompanyTierId"] = SqlDbType.Int;
            row["sOriginatorCompensationPaymentSourceT"] = 2;
            dirtyFields["sOriginatorCompensationPaymentSourceT"] = SqlDbType.Int;
            row["sGfeOriginatorCompFProps"] = 769;
            dirtyFields["sGfeOriginatorCompFProps"] = SqlDbType.Int;
            row["sProcF"] = 399.0000m;
            dirtyFields["sProcF"] = SqlDbType.Decimal;
            row["sProcFProps"] = 33025;
            dirtyFields["sProcFProps"] = SqlDbType.Int;
            row["sUwF"] = 1000.0000m;
            dirtyFields["sUwF"] = SqlDbType.Decimal;
            row["sUwFProps"] = 49409;
            dirtyFields["sUwFProps"] = SqlDbType.Int;
            row["sWireF"] = 1.0000m;
            dirtyFields["sWireF"] = SqlDbType.Decimal;
            row["sWireFProps"] = 1;
            dirtyFields["sWireFProps"] = SqlDbType.Int;
            row["s800U1F"] = 333.0000m;
            dirtyFields["s800U1F"] = SqlDbType.Decimal;
            row["s800U1FProps"] = 4865;
            dirtyFields["s800U1FProps"] = SqlDbType.Int;
            row["s800U2F"] = 222.0000m;
            dirtyFields["s800U2F"] = SqlDbType.Decimal;
            row["s800U2FProps"] = 54017;
            dirtyFields["s800U2FProps"] = SqlDbType.Int;
            row["sEscrowF"] = 495.0000m;
            dirtyFields["sEscrowF"] = SqlDbType.Decimal;
            row["sEscrowFProps"] = 49664;
            dirtyFields["sEscrowFProps"] = SqlDbType.Int;
            row["sDocPrepF"] = 37.0000m;
            dirtyFields["sDocPrepF"] = SqlDbType.Decimal;
            row["sDocPrepFProps"] = 49408;
            dirtyFields["sDocPrepFProps"] = SqlDbType.Int;
            row["sNotaryF"] = 50.0000m;
            dirtyFields["sNotaryF"] = SqlDbType.Decimal;
            row["sNotaryFProps"] = 49409;
            dirtyFields["sNotaryFProps"] = SqlDbType.Int;
            row["sAttorneyFProps"] = 256;
            dirtyFields["sAttorneyFProps"] = SqlDbType.Int;
            row["sTitleInsF"] = 199.0000m;
            dirtyFields["sTitleInsF"] = SqlDbType.Decimal;
            row["sTitleInsFProps"] = 49152;
            dirtyFields["sTitleInsFProps"] = SqlDbType.Int;
            row["sIPiaProps"] = 1;
            dirtyFields["sIPiaProps"] = SqlDbType.Int;
            row["sU1GovRtcGfeSection"] = 8;
            dirtyFields["sU1GovRtcGfeSection"] = SqlDbType.Int;
            row["sU2GovRtcGfeSection"] = 8;
            dirtyFields["sU2GovRtcGfeSection"] = SqlDbType.Int;
            row["sU3GovRtcGfeSection"] = 8;
            dirtyFields["sU3GovRtcGfeSection"] = SqlDbType.Int;
            row["sSpState"] = "CA";
            dirtyFields["sSpState"] = SqlDbType.VarChar;
            row["sSpStatePeval"] = "  ";
            dirtyFields["sSpStatePeval"] = SqlDbType.VarChar;
            row["sIPiaDy"] = 15;
            dirtyFields["sIPiaDy"] = SqlDbType.Int;
            row["sIPerDay"] = 33.3333m;
            dirtyFields["sIPerDay"] = SqlDbType.Decimal;
            row["sIsManuallySetThirdPartyAffiliateProps"] = true;
            dirtyFields["sIsManuallySetThirdPartyAffiliateProps"] = SqlDbType.Bit;
            row["sDaysInYr"] = 360;
            dirtyFields["sDaysInYr"] = SqlDbType.Int;
            row["sNoteIR"] = 6.000m;
            dirtyFields["sNoteIR"] = SqlDbType.Decimal;
            row["sProMInsRoundingIsMigrated"] = true;
            dirtyFields["sProMInsRoundingIsMigrated"] = SqlDbType.Bit;
            row["sTerm"] = 360;
            dirtyFields["sTerm"] = SqlDbType.Int;
            row["sLDiscntBaseT"] = 4;
            dirtyFields["sLDiscntBaseT"] = SqlDbType.Int;
            row["sHazInsRsrvEscrowedTri"] = 1;
            dirtyFields["sHazInsRsrvEscrowedTri"] = SqlDbType.Int;
            row["sMInsRsrvEscrowedTri"] = 1;
            dirtyFields["sMInsRsrvEscrowedTri"] = SqlDbType.Int;
            row["sRealETxRsrvEscrowedTri"] = 1;
            dirtyFields["sRealETxRsrvEscrowedTri"] = SqlDbType.Int;
            row["sSchoolTxRsrvEscrowedTri"] = 1;
            dirtyFields["sSchoolTxRsrvEscrowedTri"] = SqlDbType.Int;
            row["sFloodInsRsrvEscrowedTri"] = 1;
            dirtyFields["sFloodInsRsrvEscrowedTri"] = SqlDbType.Int;
            row["s1006RsrvEscrowedTri"] = 1;
            dirtyFields["s1006RsrvEscrowedTri"] = SqlDbType.Int;
            row["s1007RsrvEscrowedTri"] = 1;
            dirtyFields["s1007RsrvEscrowedTri"] = SqlDbType.Int;
            row["sU3RsrvEscrowedTri"] = 1;
            dirtyFields["sU3RsrvEscrowedTri"] = SqlDbType.Int;
            row["sU4RsrvEscrowedTri"] = 1;
            dirtyFields["sU4RsrvEscrowedTri"] = SqlDbType.Int;
            row["sProdImpound"] = true;
            dirtyFields["sProdImpound"] = SqlDbType.Bit;
            row["sMBrokFBaseT"] = 4;
            dirtyFields["sMBrokFBaseT"] = SqlDbType.Int;
            row["sUseGFEDataForSCFields"] = true;
            dirtyFields["sUseGFEDataForSCFields"] = SqlDbType.Bit;
            row["sSettlementMBrokFBaseT"] = 4;
            dirtyFields["sSettlementMBrokFBaseT"] = SqlDbType.Int;
            row["sDue"] = 360;
            dirtyFields["sDue"] = SqlDbType.Int;
            row["sSettlementLDiscntBaseT"] = 4;
            dirtyFields["sSettlementLDiscntBaseT"] = SqlDbType.Int;

            loan.Save();

            return loanID;
        }

        [Test]
        public void TestOneQMCalcsScenario()
        {
            var loan = new CFullAccessPageData(
               this.loanID,
               new string[]
                {
                    "sFinancedAmt", 
                    "sQMTotFeeAmt",
                    "sQMTotFinFeeAmount"
                });
            loan.ByPassFieldSecurityCheck = true;
            loan.InitLoad();

            Assert.AreEqual(loan.sFinancedAmt, 198050.00m);
            Assert.AreEqual(loan.sQMTotFeeAmt_rep, "$1,782.00");
            Assert.AreEqual(loan.sQMTotFinFeeAmount_rep, "$37.00");
            Assert.AreEqual(loan.sQMLAmt_rep, "$198,013.00");
        }

        [TestFixtureTearDown]
        public void TearDown()
        {
            Thread.CurrentPrincipal = oldPrincipal;
        }

        public void CodeGeneratorForTestOneQMCalcsScenario()
        {
            var loan = new CFullAccessPageData(
                new Guid("d2d4d2ee-a4e5-4e16-96df-a4440111f92a"), // dougtestadmin internal203073
                new string[]
                {
                    "sFinancedAmt", 
                    "sQMTotFeeAmt",
                    "sQMTotFinFeeAmount"
                });
            loan.ByPassFieldSecurityCheck = true;
            loan.InitLoad();

            var loanType = typeof(CPageData);
            var privateBindingFlags = BindingFlags.NonPublic | BindingFlags.Instance;
            var baseLoanAsField = loanType.GetField("m_pageBase", privateBindingFlags);
            var baseLoan = (CPageBase)baseLoanAsField.GetValue(loan);
            var baseLoanType = typeof(CPageBase);
            var rowAsField = baseLoanType.GetField("m_rowLoan", privateBindingFlags);
            var row = (IDataContainer)rowAsField.GetValue(baseLoan);

            var provider = CSelectStatementProvider.GetProviderForTargets(new string[]
                {
                    "sFinancedAmt", 
                    "sQMTotFeeAmt",
                    "sQMTotFinFeeAmount"
                }, false);

            foreach (var fieldName in provider.FieldSet)
            {
                if (FieldsBlackListedFromCopying.Contains(fieldName))
                {
                    continue;
                }

                object value;
                try
                {
                    value = row[fieldName];
                }
                catch (ArgumentException)
                {
                    continue;
                    //Console.WriteLine("skipping: " + fieldName);
                }

                WriteCodeFromDBRowField(value, fieldName);
            }
        }

        private void WriteCodeFromDBRowField(object value, string fieldName)
        {
            if (value == DBNull.Value)
            {
                return;
            }

            if (value is Int32)
            {
                if (0 != (int)value)
                {
                    Console.WriteLine("row[\"{0}\"] = {1};", fieldName, value);
                    Console.WriteLine("dirtyFields[\"{0}\"] = SqlDbType.Int;", fieldName);
                }
                return;
            }

            if (value is decimal)
            {
                if (0 != (decimal)value)
                {
                    Console.WriteLine("row[\"{0}\"] = {1}m;", fieldName, value);
                    Console.WriteLine("dirtyFields[\"{0}\"] = SqlDbType.Decimal;", fieldName);
                }
                return;
            }

            if (value is bool)
            {
                if (false != (bool)value)
                {
                    Console.WriteLine("row[\"{0}\"] = true;", fieldName);
                    Console.WriteLine("dirtyFields[\"{0}\"] = SqlDbType.Bit;", fieldName);
                }
                return;
            }

            if (value is string)
            {
                if ("" != (string)value)
                {
                    Console.WriteLine("row[\"{0}\"] = \"{1}\";", fieldName, value);
                    Console.WriteLine("dirtyFields[\"{0}\"] = SqlDbType.VarChar;", fieldName);
                }
                return;
            }

            if (value is Guid)
            {
                if (Guid.Empty != (Guid)value)
                {
                    Console.WriteLine("row[\"{0}\"] = new Guid(\"{1}\");", fieldName, value);
                    Console.WriteLine("dirtyFields[\"{0}\"] = SqlDbType.Uniqueidentifier;", fieldName);
                }
                return;
            }

            Console.WriteLine("row[\"{0}\"] = {1};", fieldName, value);
        }

        private static HashSet<string> FieldsBlackListedFromCopying
            = new HashSet<string>
                (new string[]
                {
                    "sBrokerId",
                    "sBranchId",
                    "sPmlBrokerId",
                    "sPreparerXmlContent",
                    "sAgentXmlContent",
                    "sOpenedD",
                    "sClosingCostSetJsonContent"
                });
    }
}
