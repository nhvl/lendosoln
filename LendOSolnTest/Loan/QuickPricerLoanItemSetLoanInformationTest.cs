﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using LendersOffice.QuickPricer;

namespace LendOSolnTest.Loan
{
    [TestFixture]
    public class QuickPricerLoanItemSetLoanInformationTest
    {
        [Test]
        public void SetLoanInformationTest()
        {
            var testData = new[] {
                new { Input_sHouseValPe = "", Input_sLAmtCalcPe = "", Input_sProOFinBalPe = "",
                    Expected_sHouseValPe = "0.00", Expected_sLAmtCalcPe = "0.00", Expected_sLtvRPe = "0.00",
                    Expected_sEquityPe = "0.00", Expected_sDownPmtPcPe="0.00",
                    Expected_sProOFinBalPe = "0.00", Expected_sLtvROtherFinPe = "0.00",
                    Expected_sCltvRPe = "0.00"
                },
                new { Input_sHouseValPe = default(string), Input_sLAmtCalcPe = default(string), Input_sProOFinBalPe = default(string),
                    Expected_sHouseValPe = "0.00", Expected_sLAmtCalcPe = "0.00", Expected_sLtvRPe = "0.00",
                    Expected_sEquityPe = "0.00", Expected_sDownPmtPcPe="0.00",
                    Expected_sProOFinBalPe = "0.00", Expected_sLtvROtherFinPe = "0.00",
                    Expected_sCltvRPe = "0.00"
                },

                new { Input_sHouseValPe = "100000", Input_sLAmtCalcPe = "", Input_sProOFinBalPe = "",
                    Expected_sHouseValPe = "100000", Expected_sLAmtCalcPe = "0", Expected_sLtvRPe = "0",
                    Expected_sEquityPe = "100000", Expected_sDownPmtPcPe="100",
                    Expected_sProOFinBalPe = "0", Expected_sLtvROtherFinPe = "0",
                    Expected_sCltvRPe = "0"
                },

                new { Input_sHouseValPe = "100000", Input_sLAmtCalcPe = "80000", Input_sProOFinBalPe = "",
                    Expected_sHouseValPe = "100000", Expected_sLAmtCalcPe = "80000", Expected_sLtvRPe = "80.0",
                    Expected_sEquityPe = "20000", Expected_sDownPmtPcPe="20.0",
                    Expected_sProOFinBalPe = "0", Expected_sLtvROtherFinPe = "0",
                    Expected_sCltvRPe = "80.0"
                },
                new { Input_sHouseValPe = "100000", Input_sLAmtCalcPe = "-10000", Input_sProOFinBalPe = "",
                    Expected_sHouseValPe = "100000", Expected_sLAmtCalcPe = "0", Expected_sLtvRPe = "0",
                    Expected_sEquityPe = "100000", Expected_sDownPmtPcPe="100",
                    Expected_sProOFinBalPe = "0", Expected_sLtvROtherFinPe = "0",
                    Expected_sCltvRPe = "0"
                },

                new { Input_sHouseValPe = "100000", Input_sLAmtCalcPe = "110000", Input_sProOFinBalPe = "",
                    Expected_sHouseValPe = "100000", Expected_sLAmtCalcPe = "110000", Expected_sLtvRPe = "110.0",
                    Expected_sEquityPe = "-10000", Expected_sDownPmtPcPe="-10",
                    Expected_sProOFinBalPe = "0", Expected_sLtvROtherFinPe = "0",
                    Expected_sCltvRPe = "110.0"
                },

                new { Input_sHouseValPe = "100500", Input_sLAmtCalcPe = "80000", Input_sProOFinBalPe = "",
                    Expected_sHouseValPe = "100500", Expected_sLAmtCalcPe = "80000", Expected_sLtvRPe = "79.602",
                    Expected_sEquityPe = "20500", Expected_sDownPmtPcPe="20.398",
                    Expected_sProOFinBalPe = "0", Expected_sLtvROtherFinPe = "0",
                    Expected_sCltvRPe = "79.602"
                },
                new { Input_sHouseValPe = "100000", Input_sLAmtCalcPe = "80000", Input_sProOFinBalPe = "10000",
                    Expected_sHouseValPe = "100000", Expected_sLAmtCalcPe = "80000", Expected_sLtvRPe = "80.0",
                    Expected_sEquityPe = "10000", Expected_sDownPmtPcPe="10.0",
                    Expected_sProOFinBalPe = "10000", Expected_sLtvROtherFinPe = "10.0",
                    Expected_sCltvRPe = "90.0"
                },
            };

            foreach (var o in testData)
            {
                QuickPricerLoanItem loanItem = new QuickPricerLoanItem();
                loanItem.SetLoanInformation(o.Input_sHouseValPe, o.Input_sLAmtCalcPe, o.Input_sProOFinBalPe);

                Assert.AreEqual(o.Expected_sHouseValPe, loanItem.sHouseValPe_rep, "sHouseValPe::" + o);
                Assert.AreEqual(o.Expected_sLAmtCalcPe, loanItem.sLAmtCalcPe_rep, "sLAmtCalcPe::" + o);
                Assert.AreEqual(o.Expected_sLtvRPe, loanItem.sLtvRPe_rep, "sLtvRPe::" + o);
                Assert.AreEqual(o.Expected_sEquityPe, loanItem.sEquityPe_rep, "sEquityPe::" + o);
                Assert.AreEqual(o.Expected_sProOFinBalPe, loanItem.sProOFinBalPe_rep, "sProOFinBalPe::" + o);
                Assert.AreEqual(o.Expected_sLtvROtherFinPe, loanItem.sLtvROtherFinPe_rep, "sLtvROtherFinPe::" + o);
                Assert.AreEqual(o.Expected_sCltvRPe, loanItem.sCltvRPe_rep, "sCltvRPe::" + o);

            }
        }
    }
}
