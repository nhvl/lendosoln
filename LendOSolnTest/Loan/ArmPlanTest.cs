﻿using DataAccess;
using LendOSolnTest.Common;
using LendOSolnTest.Fakes;
using NUnit.Framework;

namespace LendOSolnTest.Loan
{
    [TestFixture]
    public class ArmPlanTest
    {
        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            LoginTools.LoginAs(TestAccountType.LoTest001);
        }

        [Test]
        public void TestNonGenericArmPlans()
        {
            // Directly from Matrix Excel Sheet of case 238055.
            var testCases = new[] {
                // Bottom Initial Fixed Rate Interest Period
                new {  Description="760 - 6 MO COST OF FUNDS 1&5 Caps",Code="760",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.EleventhDistrictCOF ,sRAdj1stCapMon=4,sRAdjCapMon=6,sRAdj1stCapR=1,sRAdjCapR=1,sRAdjLifeCapR=5,sIsConvertibleMortgage=false},
                new {  Description="761 - 6 MO COST OF FUNDS,CONVERTIBLE 1&5 Caps",Code="761",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.EleventhDistrictCOF,sRAdj1stCapMon=4,sRAdjCapMon=6,sRAdj1stCapR=1,sRAdjCapR=1,sRAdjLifeCapR=5,sIsConvertibleMortgage=true},
                new {  Description="57 - 1 YR TREASURY,CONVERTIBLE 2&6 Caps",Code="57",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.WeeklyAvgCMT,sRAdj1stCapMon=0,sRAdjCapMon=12,sRAdj1stCapR=2,sRAdjCapR=2,sRAdjLifeCapR=6,sIsConvertibleMortgage=true},
                new {  Description="681 - 1 YR COST OF FUNDS 2&5 Caps",Code="681",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.EleventhDistrictCOF,sRAdj1stCapMon=0,sRAdjCapMon=12,sRAdj1stCapR=2,sRAdjCapR=2,sRAdjLifeCapR=5,sIsConvertibleMortgage=false},
                new {  Description="682 - 1 YR COST OF FUNDS,CONVERTIBLE 2&5 Caps",Code="682",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.EleventhDistrictCOF,sRAdj1stCapMon=0,sRAdjCapMon=12,sRAdj1stCapR=2,sRAdjCapR=2,sRAdjLifeCapR=5,sIsConvertibleMortgage=true},
                new {  Description="710 - 1 YR TREASURY 1&6 Caps",Code="710",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.WeeklyAvgCMT,sRAdj1stCapMon=0,sRAdjCapMon=12,sRAdj1stCapR=1,sRAdjCapR=1,sRAdjLifeCapR=6,sIsConvertibleMortgage=false},
                new {  Description="720 - 1 YR TREASURY 2&6 Caps",Code="720",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.WeeklyAvgCMT,sRAdj1stCapMon=0,sRAdjCapMon=12,sRAdj1stCapR=2,sRAdjCapR=2,sRAdjLifeCapR=6,sIsConvertibleMortgage=false},
                new {  Description="861 - 1 YR TREASURY,CONVERTIBLE 1&6 Caps",Code="861",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.WeeklyAvgCMT,sRAdj1stCapMon=0,sRAdjCapMon=12,sRAdj1stCapR=1,sRAdjCapR=1,sRAdjLifeCapR=6,sIsConvertibleMortgage=true},
                new {  Description="651 - 3/1 YR TREASURY 2&6 Caps",Code="651",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.WeeklyAvgCMT,sRAdj1stCapMon=19,sRAdjCapMon=12,sRAdj1stCapR=2,sRAdjCapR=2,sRAdjLifeCapR=6,sIsConvertibleMortgage=false},
                new {  Description="652 - 3/1 YR TREASURY,CONVERTIBLE 2&6 Caps",Code="652",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.WeeklyAvgCMT,sRAdj1stCapMon=19,sRAdjCapMon=12,sRAdj1stCapR=2,sRAdjCapR=2,sRAdjLifeCapR=6,sIsConvertibleMortgage=true},
                new {  Description="649 - 3/3 YR TREASURY 2&6 Caps",Code="649",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.WeeklyAvgCMT,sRAdj1stCapMon=30,sRAdjCapMon=36,sRAdj1stCapR=2,sRAdjCapR=2,sRAdjLifeCapR=6,sIsConvertibleMortgage=false},
                new {  Description="650 - 3/3 YR TREASURY,CONVERTIBLE 2&6 Caps",Code="650",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.WeeklyAvgCMT,sRAdj1stCapMon=30,sRAdjCapMon=36,sRAdj1stCapR=2,sRAdjCapR=2,sRAdjLifeCapR=6,sIsConvertibleMortgage=true},
                new {  Description="660 - 5/1 YR TREASURY 2&5 Caps",Code="660",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.WeeklyAvgCMT,sRAdj1stCapMon=43,sRAdjCapMon=12,sRAdj1stCapR=2,sRAdjCapR=2,sRAdjLifeCapR=5,sIsConvertibleMortgage=false},
                new {  Description="661 - 5/1 YR TREASURY,CONVERTIBLE 2&5 Caps",Code="661",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.WeeklyAvgCMT,sRAdj1stCapMon=43,sRAdjCapMon=12,sRAdj1stCapR=2,sRAdjCapR=2,sRAdjLifeCapR=5,sIsConvertibleMortgage=true},
                new {  Description="3252 - 5/1 LIBOR 2&5 Caps",Code="3252",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.WallStreetJournalLIBOR,sRAdj1stCapMon=54,sRAdjCapMon=12,sRAdj1stCapR=5,sRAdjCapR=2,sRAdjLifeCapR=5,sIsConvertibleMortgage=false},
                new {  Description="750 - 7/1 YR TREASURY 2&5 Caps",Code="750",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.WeeklyAvgCMT,sRAdj1stCapMon=67,sRAdjCapMon=12,sRAdj1stCapR=5,sRAdjCapR=2,sRAdjLifeCapR=5,sIsConvertibleMortgage=false},
                new {  Description="751 - 7/1 YR TREASURY,CONVERTIBLE 2&5 Caps",Code="751",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.WeeklyAvgCMT,sRAdj1stCapMon=67,sRAdjCapMon=12,sRAdj1stCapR=5,sRAdjCapR=2,sRAdjLifeCapR=5,sIsConvertibleMortgage=true},
                new {  Description="1423 - 10/1 YR TREASURY 2&5 Caps",Code="1423",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.WeeklyAvgCMT,sRAdj1stCapMon=91,sRAdjCapMon=12,sRAdj1stCapR=5,sRAdjCapR=2,sRAdjLifeCapR=5,sIsConvertibleMortgage=false},
                new {  Description="1437 - 10/1 YR TREASURY,CONVERTIBLE 2&5 Caps",Code="1437",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.WeeklyAvgCMT,sRAdj1stCapMon=91,sRAdjCapMon=12,sRAdj1stCapR=5,sRAdjCapR=2,sRAdjLifeCapR=5,sIsConvertibleMortgage=true},
                new {  Description="251 - FHA ARM: 1 YR TREASURY 1&5 Caps",Code ="251",sLT= E_sLT.FHA ,sArmIndexT = E_sArmIndexT.WeeklyAvgCMT,sRAdj1stCapMon = 0 ,sRAdjCapMon = 12,sRAdj1stCapR = 1,sRAdjCapR = 1,sRAdjLifeCapR = 5,sIsConvertibleMortgage = false},

                // Top Initial Fixed Rate Interest Period
                new {  Description="760 - 6 MO COST OF FUNDS 1&5 Caps",Code="760",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.EleventhDistrictCOF ,sRAdj1stCapMon=8,sRAdjCapMon=6,sRAdj1stCapR=1,sRAdjCapR=1,sRAdjLifeCapR=5,sIsConvertibleMortgage=false},
                new {  Description="761 - 6 MO COST OF FUNDS,CONVERTIBLE 1&5 Caps",Code="761",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.EleventhDistrictCOF,sRAdj1stCapMon=8,sRAdjCapMon=6,sRAdj1stCapR=1,sRAdjCapR=1,sRAdjLifeCapR=5,sIsConvertibleMortgage=true},
                new {  Description="57 - 1 YR TREASURY,CONVERTIBLE 2&6 Caps",Code="57",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.WeeklyAvgCMT,sRAdj1stCapMon=8,sRAdjCapMon=12,sRAdj1stCapR=2,sRAdjCapR=2,sRAdjLifeCapR=6,sIsConvertibleMortgage=true},
                new {  Description="681 - 1 YR COST OF FUNDS 2&5 Caps",Code="681",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.EleventhDistrictCOF,sRAdj1stCapMon=8,sRAdjCapMon=12,sRAdj1stCapR=2,sRAdjCapR=2,sRAdjLifeCapR=5,sIsConvertibleMortgage=false},
                new {  Description="682 - 1 YR COST OF FUNDS,CONVERTIBLE 2&5 Caps",Code="682",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.EleventhDistrictCOF,sRAdj1stCapMon=18,sRAdjCapMon=12,sRAdj1stCapR=2,sRAdjCapR=2,sRAdjLifeCapR=5,sIsConvertibleMortgage=true},
                new {  Description="710 - 1 YR TREASURY 1&6 Caps",Code="710",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.WeeklyAvgCMT,sRAdj1stCapMon=18,sRAdjCapMon=12,sRAdj1stCapR=1,sRAdjCapR=1,sRAdjLifeCapR=6,sIsConvertibleMortgage=false},
                new {  Description="720 - 1 YR TREASURY 2&6 Caps",Code="720",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.WeeklyAvgCMT,sRAdj1stCapMon=18,sRAdjCapMon=12,sRAdj1stCapR=2,sRAdjCapR=2,sRAdjLifeCapR=6,sIsConvertibleMortgage=false},
                new {  Description="861 - 1 YR TREASURY,CONVERTIBLE 1&6 Caps",Code="861",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.WeeklyAvgCMT,sRAdj1stCapMon=18,sRAdjCapMon=12,sRAdj1stCapR=1,sRAdjCapR=1,sRAdjLifeCapR=6,sIsConvertibleMortgage=true},
                new {  Description="651 - 3/1 YR TREASURY 2&6 Caps",Code="651",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.WeeklyAvgCMT,sRAdj1stCapMon=42,sRAdjCapMon=12,sRAdj1stCapR=2,sRAdjCapR=2,sRAdjLifeCapR=6,sIsConvertibleMortgage=false},
                new {  Description="652 - 3/1 YR TREASURY,CONVERTIBLE 2&6 Caps",Code="652",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.WeeklyAvgCMT,sRAdj1stCapMon=42,sRAdjCapMon=12,sRAdj1stCapR=2,sRAdjCapR=2,sRAdjLifeCapR=6,sIsConvertibleMortgage=true},
                new {  Description="649 - 3/3 YR TREASURY 2&6 Caps",Code="649",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.WeeklyAvgCMT,sRAdj1stCapMon=42,sRAdjCapMon=36,sRAdj1stCapR=2,sRAdjCapR=2,sRAdjLifeCapR=6,sIsConvertibleMortgage=false},
                new {  Description="650 - 3/3 YR TREASURY,CONVERTIBLE 2&6 Caps",Code="650",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.WeeklyAvgCMT,sRAdj1stCapMon=42,sRAdjCapMon=36,sRAdj1stCapR=2,sRAdjCapR=2,sRAdjLifeCapR=6,sIsConvertibleMortgage=true},
                new {  Description="660 - 5/1 YR TREASURY 2&5 Caps",Code="660",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.WeeklyAvgCMT,sRAdj1stCapMon=66,sRAdjCapMon=12,sRAdj1stCapR=2,sRAdjCapR=2,sRAdjLifeCapR=5,sIsConvertibleMortgage=false},
                new {  Description="661 - 5/1 YR TREASURY,CONVERTIBLE 2&5 Caps",Code="661",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.WeeklyAvgCMT,sRAdj1stCapMon=66,sRAdjCapMon=12,sRAdj1stCapR=2,sRAdjCapR=2,sRAdjLifeCapR=5,sIsConvertibleMortgage=true},
                new {  Description="3252 - 5/1 LIBOR 2&5 Caps",Code="3252",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.WallStreetJournalLIBOR,sRAdj1stCapMon=62,sRAdjCapMon=12,sRAdj1stCapR=5,sRAdjCapR=2,sRAdjLifeCapR=5,sIsConvertibleMortgage=false},
                new {  Description="750 - 7/1 YR TREASURY 2&5 Caps",Code="750",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.WeeklyAvgCMT,sRAdj1stCapMon=90,sRAdjCapMon=12,sRAdj1stCapR=5,sRAdjCapR=2,sRAdjLifeCapR=5,sIsConvertibleMortgage=false},
                new {  Description="751 - 7/1 YR TREASURY,CONVERTIBLE 2&5 Caps",Code="751",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.WeeklyAvgCMT,sRAdj1stCapMon=90,sRAdjCapMon=12,sRAdj1stCapR=5,sRAdjCapR=2,sRAdjLifeCapR=5,sIsConvertibleMortgage=true},
                new {  Description="1423 - 10/1 YR TREASURY 2&5 Caps",Code="1423",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.WeeklyAvgCMT,sRAdj1stCapMon=150,sRAdjCapMon=12,sRAdj1stCapR=5,sRAdjCapR=2,sRAdjLifeCapR=5,sIsConvertibleMortgage=false},
                new {  Description="1437 - 10/1 YR TREASURY,CONVERTIBLE 2&5 Caps",Code="1437",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.WeeklyAvgCMT,sRAdj1stCapMon=150,sRAdjCapMon=12,sRAdj1stCapR=5,sRAdjCapR=2,sRAdjLifeCapR=5,sIsConvertibleMortgage=true},
                new {  Description="251 - FHA ARM: 1 YR TREASURY 1&5 Caps",Code ="251",sLT= E_sLT.FHA ,sArmIndexT = E_sArmIndexT.WeeklyAvgCMT,sRAdj1stCapMon =18,sRAdjCapMon = 12,sRAdj1stCapR = 1,sRAdjCapR = 1,sRAdjLifeCapR = 5,sIsConvertibleMortgage = false},

                // Non-inclusive Initial Fixed Rate Interest Period
                new {  Description="760 - 6 MO COST OF FUNDS 1&5 Caps",Code="760",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.EleventhDistrictCOF ,sRAdj1stCapMon=5,sRAdjCapMon=6,sRAdj1stCapR=1,sRAdjCapR=1,sRAdjLifeCapR=5,sIsConvertibleMortgage=false},
                new {  Description="761 - 6 MO COST OF FUNDS,CONVERTIBLE 1&5 Caps",Code="761",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.EleventhDistrictCOF,sRAdj1stCapMon=5,sRAdjCapMon=6,sRAdj1stCapR=1,sRAdjCapR=1,sRAdjLifeCapR=5,sIsConvertibleMortgage=true},
                new {  Description="57 - 1 YR TREASURY,CONVERTIBLE 2&6 Caps",Code="57",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.WeeklyAvgCMT,sRAdj1stCapMon=1,sRAdjCapMon=12,sRAdj1stCapR=2,sRAdjCapR=2,sRAdjLifeCapR=6,sIsConvertibleMortgage=true},
                new {  Description="681 - 1 YR COST OF FUNDS 2&5 Caps",Code="681",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.EleventhDistrictCOF,sRAdj1stCapMon=1,sRAdjCapMon=12,sRAdj1stCapR=2,sRAdjCapR=2,sRAdjLifeCapR=5,sIsConvertibleMortgage=false},
                new {  Description="682 - 1 YR COST OF FUNDS,CONVERTIBLE 2&5 Caps",Code="682",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.EleventhDistrictCOF,sRAdj1stCapMon=1,sRAdjCapMon=12,sRAdj1stCapR=2,sRAdjCapR=2,sRAdjLifeCapR=5,sIsConvertibleMortgage=true},
                new {  Description="710 - 1 YR TREASURY 1&6 Caps",Code="710",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.WeeklyAvgCMT,sRAdj1stCapMon=1,sRAdjCapMon=12,sRAdj1stCapR=1,sRAdjCapR=1,sRAdjLifeCapR=6,sIsConvertibleMortgage=false},
                new {  Description="720 - 1 YR TREASURY 2&6 Caps",Code="720",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.WeeklyAvgCMT,sRAdj1stCapMon=1,sRAdjCapMon=12,sRAdj1stCapR=2,sRAdjCapR=2,sRAdjLifeCapR=6,sIsConvertibleMortgage=false},
                new {  Description="861 - 1 YR TREASURY,CONVERTIBLE 1&6 Caps",Code="861",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.WeeklyAvgCMT,sRAdj1stCapMon=1,sRAdjCapMon=12,sRAdj1stCapR=1,sRAdjCapR=1,sRAdjLifeCapR=6,sIsConvertibleMortgage=true},
                new {  Description="651 - 3/1 YR TREASURY 2&6 Caps",Code="651",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.WeeklyAvgCMT,sRAdj1stCapMon=20,sRAdjCapMon=12,sRAdj1stCapR=2,sRAdjCapR=2,sRAdjLifeCapR=6,sIsConvertibleMortgage=false},
                new {  Description="652 - 3/1 YR TREASURY,CONVERTIBLE 2&6 Caps",Code="652",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.WeeklyAvgCMT,sRAdj1stCapMon=20,sRAdjCapMon=12,sRAdj1stCapR=2,sRAdjCapR=2,sRAdjLifeCapR=6,sIsConvertibleMortgage=true},
                new {  Description="649 - 3/3 YR TREASURY 2&6 Caps",Code="649",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.WeeklyAvgCMT,sRAdj1stCapMon=31,sRAdjCapMon=36,sRAdj1stCapR=2,sRAdjCapR=2,sRAdjLifeCapR=6,sIsConvertibleMortgage=false},
                new {  Description="650 - 3/3 YR TREASURY,CONVERTIBLE 2&6 Caps",Code="650",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.WeeklyAvgCMT,sRAdj1stCapMon=31,sRAdjCapMon=36,sRAdj1stCapR=2,sRAdjCapR=2,sRAdjLifeCapR=6,sIsConvertibleMortgage=true},
                new {  Description="660 - 5/1 YR TREASURY 2&5 Caps",Code="660",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.WeeklyAvgCMT,sRAdj1stCapMon=44,sRAdjCapMon=12,sRAdj1stCapR=2,sRAdjCapR=2,sRAdjLifeCapR=5,sIsConvertibleMortgage=false},
                new {  Description="661 - 5/1 YR TREASURY,CONVERTIBLE 2&5 Caps",Code="661",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.WeeklyAvgCMT,sRAdj1stCapMon=44,sRAdjCapMon=12,sRAdj1stCapR=2,sRAdjCapR=2,sRAdjLifeCapR=5,sIsConvertibleMortgage=true},
                new {  Description="3252 - 5/1 LIBOR 2&5 Caps",Code="3252",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.WallStreetJournalLIBOR,sRAdj1stCapMon=55,sRAdjCapMon=12,sRAdj1stCapR=5,sRAdjCapR=2,sRAdjLifeCapR=5,sIsConvertibleMortgage=false},
                new {  Description="750 - 7/1 YR TREASURY 2&5 Caps",Code="750",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.WeeklyAvgCMT,sRAdj1stCapMon=68,sRAdjCapMon=12,sRAdj1stCapR=5,sRAdjCapR=2,sRAdjLifeCapR=5,sIsConvertibleMortgage=false},
                new {  Description="751 - 7/1 YR TREASURY,CONVERTIBLE 2&5 Caps",Code="751",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.WeeklyAvgCMT,sRAdj1stCapMon=68,sRAdjCapMon=12,sRAdj1stCapR=5,sRAdjCapR=2,sRAdjLifeCapR=5,sIsConvertibleMortgage=true},
                new {  Description="1423 - 10/1 YR TREASURY 2&5 Caps",Code="1423",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.WeeklyAvgCMT,sRAdj1stCapMon=92,sRAdjCapMon=12,sRAdj1stCapR=5,sRAdjCapR=2,sRAdjLifeCapR=5,sIsConvertibleMortgage=false},
                new {  Description="1437 - 10/1 YR TREASURY,CONVERTIBLE 2&5 Caps",Code="1437",sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.WeeklyAvgCMT,sRAdj1stCapMon=92,sRAdjCapMon=12,sRAdj1stCapR=5,sRAdjCapR=2,sRAdjLifeCapR=5,sIsConvertibleMortgage=true},
                new {  Description="251 - FHA ARM: 1 YR TREASURY 1&5 Caps",Code ="251",sLT= E_sLT.FHA ,sArmIndexT = E_sArmIndexT.WeeklyAvgCMT,sRAdj1stCapMon = 1,sRAdjCapMon = 12,sRAdj1stCapR = 1,sRAdjCapR = 1,sRAdjLifeCapR = 5,sIsConvertibleMortgage = false},
            };

            CPageData dataLoan = FakePageData.Create();
            dataLoan.InitLoad();
            foreach (var test in testCases)
            {
                dataLoan.sFinMethT = E_sFinMethT.ARM;
                dataLoan.sLT = test.sLT;
                dataLoan.sArmIndexT = test.sArmIndexT;
                dataLoan.sArmIndexTLckd = true;
                dataLoan.sRAdj1stCapMon = test.sRAdj1stCapMon;
                dataLoan.sRAdjCapMon = test.sRAdjCapMon;
                dataLoan.sRAdj1stCapR = test.sRAdj1stCapR;
                dataLoan.sRAdjCapR = test.sRAdjCapR;
                dataLoan.sRAdjLifeCapR = test.sRAdjLifeCapR;
                dataLoan.sIsConvertibleMortgage = test.sIsConvertibleMortgage;
                dataLoan.sFannieARMPlanNumLckd = false;

                Assert.AreEqual(test.Code, dataLoan.sFannieARMPlanNum, test.ToLiteral());
                Assert.AreEqual(test.Description, dataLoan.sFannieARMPlanNum_rep, test.ToLiteral());

                dataLoan.sFinMethT = E_sFinMethT.Fixed;
                Assert.AreEqual(string.Empty, dataLoan.sFannieARMPlanNum, test.ToLiteral());
                Assert.AreEqual(string.Empty, dataLoan.sFannieARMPlanNum_rep, test.ToLiteral());
            }
        }

        [Test]
        public void TestGenericArmPlans()
        {
            // Directly from Matrix Excel Sheet of case 238055.

            var testCases = new[] {
                // Bottom Initial Fixed Rate Interest Period
                new {  Description="FM GENERIC, 6 MONTH",Code="GEN06",sLT = E_sLT.Conventional,sRAdj1stCapMon=4,sRAdjCapR=0},
                new {  Description="FM GENERIC, 1 YR, 1% ANNUAL Cap",Code="GEN1A",sLT = E_sLT.Conventional,sRAdj1stCapMon=9,sRAdjCapR=1},
                new {  Description="FM GENERIC, 1 YR, 2% ANNUAL Cap",Code="GEN1B",sLT = E_sLT.Conventional,sRAdj1stCapMon=9,sRAdjCapR=2},
                new {  Description="FM GENERIC, 3 YR",Code="GEN3",sLT = E_sLT.Conventional,sRAdj1stCapMon=19,sRAdjCapR=0},
                new {  Description="FM GENERIC, 5 YR",Code="GEN5",sLT = E_sLT.Conventional,sRAdj1stCapMon=43,sRAdjCapR=0},
                new {  Description="FM GENERIC, 7 YR",Code="GEN7",sLT = E_sLT.Conventional,sRAdj1stCapMon=67,sRAdjCapR=0},
                new {  Description="FM GENERIC, 10 YR",Code="GEN10",sLT = E_sLT.Conventional,sRAdj1stCapMon=91,sRAdjCapR=0},
                new {  Description="FHA HYBRID ARM",Code="FHAHY",sLT = E_sLT.FHA,sRAdj1stCapMon=0,sRAdjCapR=0},
                new {  Description="VA HYBRID ARM",Code="VAARM",sLT = E_sLT.VA,sRAdj1stCapMon=19,sRAdjCapR=0},
                new {  Description="VA 1 YR ARM",Code="VA1YR",sLT = E_sLT.VA,sRAdj1stCapMon=0,sRAdjCapR=0},

                // Top Initial Fixed Rate Interest Period
                new {  Description="FM GENERIC, 6 MONTH",Code="GEN06",sLT = E_sLT.Conventional,sRAdj1stCapMon=8,sRAdjCapR=0},
                new {  Description="FM GENERIC, 1 YR, 1% ANNUAL Cap",Code="GEN1A",sLT = E_sLT.Conventional,sRAdj1stCapMon=18,sRAdjCapR=1},
                new {  Description="FM GENERIC, 1 YR, 2% ANNUAL Cap",Code="GEN1B",sLT = E_sLT.Conventional,sRAdj1stCapMon=18,sRAdjCapR=2},
                new {  Description="FM GENERIC, 3 YR",Code="GEN3",sLT = E_sLT.Conventional,sRAdj1stCapMon=42,sRAdjCapR=0},
                new {  Description="FM GENERIC, 5 YR",Code="GEN5",sLT = E_sLT.Conventional,sRAdj1stCapMon=66,sRAdjCapR=0},
                new {  Description="FM GENERIC, 7 YR",Code="GEN7",sLT = E_sLT.Conventional,sRAdj1stCapMon=90,sRAdjCapR=0},
                new {  Description="FM GENERIC, 10 YR",Code="GEN10",sLT = E_sLT.Conventional,sRAdj1stCapMon=150,sRAdjCapR=0},
                new {  Description="FHA HYBRID ARM",Code="FHAHY",sLT = E_sLT.FHA,sRAdj1stCapMon=0,sRAdjCapR=0},
                new {  Description="VA HYBRID ARM",Code="VAARM",sLT = E_sLT.VA,sRAdj1stCapMon=150,sRAdjCapR=0},
                new {  Description="VA 1 YR ARM",Code="VA1YR",sLT = E_sLT.VA,sRAdj1stCapMon=18,sRAdjCapR=0},

                // Non-inclusive Initial Fixed Rate Interest Period
                new {  Description="FM GENERIC, 6 MONTH",Code="GEN06",sLT = E_sLT.Conventional,sRAdj1stCapMon=5,sRAdjCapR=0},
                new {  Description="FM GENERIC, 1 YR, 1% ANNUAL Cap",Code="GEN1A",sLT = E_sLT.Conventional,sRAdj1stCapMon=10,sRAdjCapR=1},
                new {  Description="FM GENERIC, 1 YR, 2% ANNUAL Cap",Code="GEN1B",sLT = E_sLT.Conventional,sRAdj1stCapMon=10,sRAdjCapR=2},
                new {  Description="FM GENERIC, 3 YR",Code="GEN3",sLT = E_sLT.Conventional,sRAdj1stCapMon=20,sRAdjCapR=0},
                new {  Description="FM GENERIC, 5 YR",Code="GEN5",sLT = E_sLT.Conventional,sRAdj1stCapMon=44,sRAdjCapR=0},
                new {  Description="FM GENERIC, 7 YR",Code="GEN7",sLT = E_sLT.Conventional,sRAdj1stCapMon=68,sRAdjCapR=0},
                new {  Description="FM GENERIC, 10 YR",Code="GEN10",sLT = E_sLT.Conventional,sRAdj1stCapMon=92,sRAdjCapR=0},
                new {  Description="FHA HYBRID ARM",Code="FHAHY",sLT = E_sLT.FHA,sRAdj1stCapMon=0,sRAdjCapR=0},
                new {  Description="VA HYBRID ARM",Code="VAARM",sLT = E_sLT.VA,sRAdj1stCapMon=20,sRAdjCapR=0},
                new {  Description="VA 1 YR ARM",Code="VA1YR",sLT = E_sLT.VA,sRAdj1stCapMon=0,sRAdjCapR=0}
            };

            CPageData dataLoan = FakePageData.Create();
            dataLoan.InitLoad();
            foreach (var test in testCases)
            {
                dataLoan.sFinMethT = E_sFinMethT.ARM;
                dataLoan.sLT = test.sLT;
                dataLoan.sArmIndexT = E_sArmIndexT.LeaveBlank;
                dataLoan.sArmIndexTLckd = true;
                dataLoan.sRAdj1stCapMon = test.sRAdj1stCapMon;
                dataLoan.sRAdjCapMon = 0;
                dataLoan.sRAdj1stCapR = 0;
                dataLoan.sRAdjCapR = test.sRAdjCapR;
                dataLoan.sRAdjLifeCapR = 0;
                dataLoan.sIsConvertibleMortgage = false;
                dataLoan.sFannieARMPlanNumLckd = false;

                Assert.AreEqual(dataLoan.sFannieARMPlanNum, test.Code, test.ToLiteral());
                Assert.AreEqual(dataLoan.sFannieARMPlanNum_rep, test.Description, test.ToLiteral());

                dataLoan.sFinMethT = E_sFinMethT.Fixed;
                Assert.AreEqual(string.Empty, dataLoan.sFannieARMPlanNum, test.ToLiteral());
                Assert.AreEqual(string.Empty, dataLoan.sFannieARMPlanNum_rep, test.ToLiteral());
            }
        }

        [Test]
        public void TestGenericLenderArmPlan()
        {
            // Some test cases that should fall through to the generic lender ARM Plan
            var testCases = new[] {
                new {  sLT= E_sLT.Conventional, sArmIndexT=E_sArmIndexT.EleventhDistrictCOF ,sRAdj1stCapMon=3,sRAdjCapMon=6,sRAdj1stCapR=3,sRAdjCapR=3,sRAdjLifeCapR=5,sIsConvertibleMortgage=false},
                new {  sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.EleventhDistrictCOF,sRAdj1stCapMon=9,sRAdjCapMon=7,sRAdj1stCapR=5,sRAdjCapR=4,sRAdjLifeCapR=5,sIsConvertibleMortgage=true},
                new {  sLT= E_sLT.Conventional,sArmIndexT=E_sArmIndexT.WeeklyAvgCMT,sRAdj1stCapMon=160,sRAdjCapMon=9,sRAdj1stCapR=3,sRAdjCapR=2,sRAdjLifeCapR=6,sIsConvertibleMortgage=true},
                new {  sLT= E_sLT.VA,sArmIndexT=E_sArmIndexT.WeeklyAvgCMT,sRAdj1stCapMon=160,sRAdjCapMon=12,sRAdj1stCapR=2,sRAdjCapR=2,sRAdjLifeCapR=6,sIsConvertibleMortgage=false},
                new {  sLT= E_sLT.UsdaRural,sArmIndexT=E_sArmIndexT.WeeklyAvgCMT,sRAdj1stCapMon=0,sRAdjCapMon=12,sRAdj1stCapR=2,sRAdjCapR=2,sRAdjLifeCapR=6,sIsConvertibleMortgage=true},
                new {  sLT= E_sLT.Other, sArmIndexT=E_sArmIndexT.LeaveBlank ,sRAdj1stCapMon=0,sRAdjCapMon=0,sRAdj1stCapR=0,sRAdjCapR=0,sRAdjLifeCapR=0,sIsConvertibleMortgage=false},
            };

            CPageData dataLoan = FakePageData.Create();
            dataLoan.InitLoad();
            foreach (var test in testCases)
            {
                dataLoan.sFinMethT = E_sFinMethT.ARM;
                dataLoan.sLT = test.sLT;
                dataLoan.sArmIndexT = test.sArmIndexT;
                dataLoan.sArmIndexTLckd = true;
                dataLoan.sRAdj1stCapMon = test.sRAdj1stCapMon;
                dataLoan.sRAdjCapMon = test.sRAdjCapMon;
                dataLoan.sRAdj1stCapR = test.sRAdj1stCapR;
                dataLoan.sRAdjCapR = test.sRAdjCapR;
                dataLoan.sRAdjLifeCapR = test.sRAdjLifeCapR;
                dataLoan.sIsConvertibleMortgage = test.sIsConvertibleMortgage;
                dataLoan.sFannieARMPlanNumLckd = false;

                Assert.AreEqual("!FNMA", dataLoan.sFannieARMPlanNum, test.ToLiteral());
                Assert.AreEqual("LENDER ARM PLAN", dataLoan.sFannieARMPlanNum_rep, test.ToLiteral());

                dataLoan.sFinMethT = E_sFinMethT.Fixed;
                Assert.AreEqual(string.Empty, dataLoan.sFannieARMPlanNum, test.ToLiteral());
                Assert.AreEqual(string.Empty, dataLoan.sFannieARMPlanNum_rep, test.ToLiteral());
            }
        }

    }
}
