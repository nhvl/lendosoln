﻿using DataAccess;
using LendOSolnTest.Common;
using LendOSolnTest.Fakes;
using NUnit.Framework;

namespace LendOSolnTest.Loan
{
    [TestFixture]
    public class FHALenderIdTest
    {
        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            LoginTools.LoginAs(TestAccountType.LoTest001);
        }

        private CPageData CreateDataObject()
        {
            return FakePageData.Create();
        }

        [Test]
        public void TestRetrievingCompanyIdAndBranchId()
        {
            CPageData dataLoan = this.CreateDataObject();
            dataLoan.InitLoad();

            var testCases = new[] {
                new { sFHALenderIdCode = "", sFHALender5DigitsCompanyId="", sFHALender5DigitsBranchId=""},
                new { sFHALenderIdCode = "12345", sFHALender5DigitsCompanyId="12345", sFHALender5DigitsBranchId=""},
                new { sFHALenderIdCode = "1234567890", sFHALender5DigitsCompanyId="12345", sFHALender5DigitsBranchId="67890"},
                new { sFHALenderIdCode = "123", sFHALender5DigitsCompanyId="123", sFHALender5DigitsBranchId=""},
                new { sFHALenderIdCode = "123456", sFHALender5DigitsCompanyId="12345", sFHALender5DigitsBranchId="6"},
                
            };

            foreach (var testCase in testCases)
            {
                dataLoan.sFHALenderIdCode = testCase.sFHALenderIdCode;

                Assert.AreEqual(testCase.sFHALender5DigitsCompanyId, dataLoan.sFHALender5DigitsCompanyId, "sFHALender5DigitsCompanyId");
                Assert.AreEqual(testCase.sFHALender5DigitsBranchId, dataLoan.sFHALender5DigitsBranchId, "sFHALender5DigitsBranchId");
            }

            foreach (var testCase in testCases)
            {
                dataLoan.sFHASponsorAgentIdCode = testCase.sFHALenderIdCode;

                Assert.AreEqual(testCase.sFHALender5DigitsCompanyId, dataLoan.sFHASponsorAgent5DigitsCompanyId, "sFHASponsorAgent5DigitsCompanyId");
                Assert.AreEqual(testCase.sFHALender5DigitsBranchId, dataLoan.sFHASponsorAgent5DigitsBranchId, "sFHASponsorAgent5DigitsBranchId");
            }
        }
        [Test]
        public void TestSetCompanyIdAndBranchId()
        {
            CPageData dataLoan = this.CreateDataObject();
            dataLoan.InitLoad();

            var testCases = new[] {
                new { sFHALenderIdCode = "", sFHALender5DigitsCompanyId="", sFHALender5DigitsBranchId=""},
                new { sFHALenderIdCode = "12345", sFHALender5DigitsCompanyId="12345", sFHALender5DigitsBranchId=""},
                new { sFHALenderIdCode = "1234567890", sFHALender5DigitsCompanyId="12345", sFHALender5DigitsBranchId="67890"},
                
            };

            foreach (var testCase in testCases)
            {
                dataLoan.sFHALender5DigitsCompanyId = testCase.sFHALender5DigitsCompanyId;
                dataLoan.sFHALender5DigitsBranchId = testCase.sFHALender5DigitsBranchId;

                Assert.AreEqual(testCase.sFHALenderIdCode, dataLoan.sFHALenderIdCode);
            }
            foreach (var testCase in testCases)
            {
                dataLoan.sFHASponsorAgent5DigitsCompanyId = testCase.sFHALender5DigitsCompanyId;
                dataLoan.sFHASponsorAgent5DigitsBranchId = testCase.sFHALender5DigitsBranchId;

                Assert.AreEqual(testCase.sFHALenderIdCode, dataLoan.sFHASponsorAgentIdCode);
            }

        }

        [Test]
        public void TestSettingInvalidValues()
        {
            CPageData dataLoan = this.CreateDataObject();
            dataLoan.InitLoad();

            string[] invalidValues = { "1", "1234", "32" };
            foreach (string s in invalidValues)
            {
                try
                {
                    dataLoan.sFHALender5DigitsCompanyId = s;
                    Assert.Fail();
                }
                catch (CBaseException) { }
            }
            foreach (string s in invalidValues)
            {
                try
                {
                    dataLoan.sFHALender5DigitsBranchId = s;
                    Assert.Fail();
                }
                catch (CBaseException) { }
            }
            foreach (string s in invalidValues)
            {
                try
                {
                    dataLoan.sFHASponsorAgent5DigitsCompanyId = s;
                    Assert.Fail();
                }
                catch (CBaseException) { }
            }
            foreach (string s in invalidValues)
            {
                try
                {
                    dataLoan.sFHASponsorAgent5DigitsBranchId = s;
                    Assert.Fail();
                }
                catch (CBaseException) { }
            }
        }

    }
}
