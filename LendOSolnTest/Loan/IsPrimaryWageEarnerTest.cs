﻿using DataAccess;
using LendOSolnTest.Common;
using LendOSolnTest.Fakes;
using NUnit.Framework;

namespace LendOSolnTest.Loan
{
    [TestFixture]
    public class IsPrimaryWageEarnerTest
    {
        [TestFixtureSetUp]
        public void Setup()
        {
            LoginTools.LoginAs(TestAccountType.LoTest001);
        }

        [Test]
        public void Test()
        {
            CPageData dataLoan = CreatePageData();
            dataLoan.InitLoad();

            CAppData dataApp0 = dataLoan.GetAppData(0);
            CAppData dataApp1 = dataLoan.GetAppData(1);
            CAppData dataApp2 = dataLoan.GetAppData(2);

            #region Test 1.
            // Everything is false.
            dataApp0.aBIsPrimaryWageEarner = false;
            dataApp0.aCIsPrimaryWageEarner = false;

            dataApp1.aBIsPrimaryWageEarner = false;
            dataApp1.aCIsPrimaryWageEarner = false;

            dataApp2.aBIsPrimaryWageEarner = false;
            dataApp2.aCIsPrimaryWageEarner = false;

            Assert.AreEqual(true, dataApp0.aBIsPrimaryWageEarner, "Test 1::dataApp0.aBIsPrimaryWageEarner");
            Assert.AreEqual(false, dataApp0.aCIsPrimaryWageEarner, "Test 1::dataApp0.aCIsPrimaryWageEarner");
            Assert.AreEqual(false, dataApp1.aBIsPrimaryWageEarner, "Test 1::dataApp1.aBIsPrimaryWageEarner");
            Assert.AreEqual(false, dataApp1.aCIsPrimaryWageEarner, "Test 1::dataApp1.aCIsPrimaryWageEarner");
            Assert.AreEqual(false, dataApp2.aBIsPrimaryWageEarner, "Test 1::dataApp2.aBIsPrimaryWageEarner");
            Assert.AreEqual(false, dataApp2.aCIsPrimaryWageEarner, "Test 1::dataApp2.aCIsPrimaryWageEarner");

            #endregion

            #region Test 2.
            // Everything is false.
            dataApp0.aBIsPrimaryWageEarner = false;
            dataApp0.aCIsPrimaryWageEarner = false;
            dataApp0.aIsBorrSpousePrimaryWageEarner = true;
            dataApp1.aBIsPrimaryWageEarner = false;
            dataApp1.aCIsPrimaryWageEarner = false;

            dataApp2.aBIsPrimaryWageEarner = false;
            dataApp2.aCIsPrimaryWageEarner = false;

            Assert.AreEqual(false, dataApp0.aBIsPrimaryWageEarner, "Test 2::dataApp0.aBIsPrimaryWageEarner");
            Assert.AreEqual(true, dataApp0.aCIsPrimaryWageEarner, "Test 2::dataApp0.aCIsPrimaryWageEarner");
            Assert.AreEqual(false, dataApp1.aBIsPrimaryWageEarner, "Test 2::dataApp1.aBIsPrimaryWageEarner");
            Assert.AreEqual(false, dataApp1.aCIsPrimaryWageEarner, "Test 2::dataApp1.aCIsPrimaryWageEarner");
            Assert.AreEqual(false, dataApp2.aBIsPrimaryWageEarner, "Test 2::dataApp2.aBIsPrimaryWageEarner");
            Assert.AreEqual(false, dataApp2.aCIsPrimaryWageEarner, "Test 2::dataApp2.aCIsPrimaryWageEarner");

            #endregion
            #region Test 3.
            // Everything is false.
            dataApp0.aBIsPrimaryWageEarner = false;
            dataApp0.aCIsPrimaryWageEarner = false;
            dataApp0.aIsBorrSpousePrimaryWageEarner = false;
            dataApp1.aBIsPrimaryWageEarner = true;
            dataApp1.aCIsPrimaryWageEarner = false;

            dataApp2.aBIsPrimaryWageEarner = false;
            dataApp2.aCIsPrimaryWageEarner = false;

            Assert.AreEqual(false, dataApp0.aBIsPrimaryWageEarner, "Test 3::dataApp0.aBIsPrimaryWageEarner");
            Assert.AreEqual(false, dataApp0.aCIsPrimaryWageEarner, "Test 3::dataApp0.aCIsPrimaryWageEarner");
            Assert.AreEqual(true, dataApp1.aBIsPrimaryWageEarner, "Test 3::dataApp1.aBIsPrimaryWageEarner");
            Assert.AreEqual(false, dataApp1.aCIsPrimaryWageEarner, "Test 3::dataApp1.aCIsPrimaryWageEarner");
            Assert.AreEqual(false, dataApp2.aBIsPrimaryWageEarner, "Test 3::dataApp2.aBIsPrimaryWageEarner");
            Assert.AreEqual(false, dataApp2.aCIsPrimaryWageEarner, "Test 3::dataApp2.aCIsPrimaryWageEarner");

            #endregion

            #region Test 4.
            // Everything is false.
            dataApp0.aBIsPrimaryWageEarner = false;
            dataApp0.aCIsPrimaryWageEarner = false;
            dataApp0.aIsBorrSpousePrimaryWageEarner = false;
            dataApp1.aBIsPrimaryWageEarner = false;
            dataApp1.aCIsPrimaryWageEarner = true;

            dataApp2.aBIsPrimaryWageEarner = false;
            dataApp2.aCIsPrimaryWageEarner = false;

            Assert.AreEqual(false, dataApp0.aBIsPrimaryWageEarner, "Test 4::dataApp0.aBIsPrimaryWageEarner");
            Assert.AreEqual(false, dataApp0.aCIsPrimaryWageEarner, "Test 4::dataApp0.aCIsPrimaryWageEarner");
            Assert.AreEqual(false, dataApp1.aBIsPrimaryWageEarner, "Test 4::dataApp1.aBIsPrimaryWageEarner");
            Assert.AreEqual(true, dataApp1.aCIsPrimaryWageEarner, "Test 4::dataApp1.aCIsPrimaryWageEarner");
            Assert.AreEqual(false, dataApp2.aBIsPrimaryWageEarner, "Test 4::dataApp2.aBIsPrimaryWageEarner");
            Assert.AreEqual(false, dataApp2.aCIsPrimaryWageEarner, "Test 4::dataApp2.aCIsPrimaryWageEarner");

            #endregion
            #region Test 5.
            dataApp0.aBIsPrimaryWageEarner = false;
            dataApp0.aCIsPrimaryWageEarner = false;
            dataApp0.aIsBorrSpousePrimaryWageEarner = false;
            dataApp1.aBIsPrimaryWageEarner = false;
            dataApp1.aCIsPrimaryWageEarner = true;

            dataApp2.aBIsPrimaryWageEarner = true;
            dataApp2.aCIsPrimaryWageEarner = false;

            Assert.AreEqual(false, dataApp0.aBIsPrimaryWageEarner, "Test 5::dataApp0.aBIsPrimaryWageEarner");
            Assert.AreEqual(false, dataApp0.aCIsPrimaryWageEarner, "Test 5::dataApp0.aCIsPrimaryWageEarner");
            Assert.AreEqual(false, dataApp1.aBIsPrimaryWageEarner, "Test 5::dataApp1.aBIsPrimaryWageEarner");
            Assert.AreEqual(false, dataApp1.aCIsPrimaryWageEarner, "Test 5::dataApp1.aCIsPrimaryWageEarner");
            Assert.AreEqual(true, dataApp2.aBIsPrimaryWageEarner, "Test 5::dataApp2.aBIsPrimaryWageEarner");
            Assert.AreEqual(false, dataApp2.aCIsPrimaryWageEarner, "Test 5::dataApp2.aCIsPrimaryWageEarner");

            #endregion
        }

        private CPageData CreatePageData()
        {
            return FakePageData.Create(appCount: 3);
        }
    }
}
