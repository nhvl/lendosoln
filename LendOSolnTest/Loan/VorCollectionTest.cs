﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using LendOSolnTest.Fakes;
using DataAccess;
using LendersOffice.Constants;

namespace LendOSolnTest.Loan
{
    [TestFixture]
    class VorCollectionTest
    {
        private CPageData CreatePageData()
        {
            return FakePageData.Create();
        }

        [TearDown]
        public void TearDown()
        {
            FakePageDataUtilities.Instance.Reset();
        }

        [Test]
        public void CollectionTest()
        {
            // Simple test to make sure collection and interaction with 
            // loan file after removing the dataset use.
            var dataLoan = CreatePageData();
            
            // Add 2 entries to collection.
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            var dataApp = dataLoan.GetAppData(0);
            var vorCollection = dataApp.aVorCollection;
            var newRecord = vorCollection.GetRecordByRecordId(Guid.NewGuid());
            PopulateDefaultRecord(newRecord);
            newRecord.Update();
            newRecord = vorCollection.GetRecordByRecordId(Guid.NewGuid());
            PopulateDefaultRecord(newRecord);
            newRecord.Update();
            dataLoan.Save();
            
            // Verify survives load
            dataLoan = CreatePageData();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataApp = dataLoan.GetAppData(0);
            vorCollection = dataApp.aVorCollection;
            Assert.AreEqual(2, vorCollection.Count());
            VerifyRecordMatch(vorCollection.GetRecordByIndex(0));
            VerifyRecordMatch(vorCollection.GetRecordByIndex(1));

            // Verify Clear Collection occurs and is committed.
            vorCollection.Clear();
            Assert.AreEqual(0, vorCollection.Count());
            dataLoan.Save();

            dataLoan = CreatePageData();
            dataLoan.InitLoad();
            dataApp = dataLoan.GetAppData(0);
            Assert.AreEqual(0, dataApp.aVorCollection.Count());
        }

        private static void VerifyRecordMatch(IVerificationOfRent record)
        {
            Assert.AreEqual(record.LandlordCreditorName, "LL Properties, LLC.");
            Assert.AreEqual(record.Description, "Duplex on main st.");
            Assert.AreEqual(record.AddressFor, "123 Main St.");
            Assert.AreEqual(record.CityFor, "Long Beach");
            Assert.AreEqual(record.StateFor, "CA");
            Assert.AreEqual(record.ZipFor, "90090");
            Assert.AreEqual(record.AddressTo, "456 Land Lord St.");
            Assert.AreEqual(record.CityTo, "Costa Mesa");
            Assert.AreEqual(record.StateTo, "CA");
            Assert.AreEqual(record.ZipTo, "92626");
            Assert.AreEqual(record.PhoneTo, "5555555555");
            Assert.AreEqual(record.FaxTo, "7777777777");
            Assert.AreEqual(record.AccountName, "N3199");
            Assert.AreEqual(record.VerifSentD_rep, "3/20/2018");
            Assert.AreEqual(record.VerifRecvD_rep, "3/21/2018");
            Assert.AreEqual(record.VerifExpD_rep, "3/22/2018");
            Assert.AreEqual(record.VerifReorderedD_rep, "3/23/2018");
            Assert.AreEqual(record.VerifT, E_VorT.LandContract);
            Assert.AreEqual(record.Attention, "Larry Landlord");
            Assert.AreEqual(record.IsSeeAttachment, true);

        }

        private static void PopulateDefaultRecord(IVerificationOfRent record)
        {
            record.LandlordCreditorName = "LL Properties, LLC.";
            record.Description = "Duplex on main st.";
            record.AddressFor = "123 Main St.";
            record.CityFor = "Long Beach";
            record.StateFor = "CA";
            record.ZipFor = "90090";
            record.AddressTo = "456 Land Lord St.";
            record.CityTo = "Costa Mesa";
            record.StateTo = "CA";
            record.ZipTo = "92626";
            record.PhoneTo = "5555555555";
            record.FaxTo = "7777777777";
            record.AccountName = "N3199";
            record.VerifSentD_rep = "3/20/2018";
            record.VerifRecvD_rep = "3/21/2018";
            record.VerifExpD_rep = "3/22/2018";
            record.VerifReorderedD_rep = "3/23/2018";
            record.VerifT = E_VorT.LandContract;
            record.Attention = "Larry Landlord";
            record.IsSeeAttachment = true;
        }
    }
}
