﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using LendOSolnTest.Common;
using DataAccess;
using LendersOffice.Security;
using LendersOffice.Constants;
using LendersOffice.Common;

namespace LendOSolnTest.Loan
{
    [TestFixture]
    public class LoanFileCacheTest
    {
        private Guid m_sLId = Guid.Empty;
        private string[] m_fieldList = {
                                     "aBNm",
                                     "aBLastNm",
                                     "aBFirstNm",
                                     "sPurchPrice",
                                     "sOpenedD",
                                     "sTerm",
                                     "sStatusT",

                                                  "sEmployeeManagerId", "sEmployeeUnderwriterId","sEmployeeLockDeskId",
                                                  "sEmployeeProcessorId", "sEmployeeLoanOpenerId", "sEmployeeLoanRepId",
                                                  "sEmployeeLenderAccExecId", "sEmployeeRealEstateAgentId", "sEmployeeCallCenterAgentId",
                                                  "sEmployeeCloserId", "sEmployeeBrokerProcessorId", "sBrokerId", "sBranchId",
                                                  "sRateLockStatusT","sRateLockStatusT",
                                                  "PmlExternalManagerEmployeeId", "sPmlBrokerId", "PmlExternalBrokerProcessorManagerEmployeeId", "sLienPosT"

                                 };
        private AbstractUserPrincipal m_principal;
        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            LoginTools.LoginAs(TestAccountType.LoTest001);

            m_principal = BrokerUserPrincipal.CurrentPrincipal;

            CLoanFileCreator creator = CLoanFileCreator.GetCreator(m_principal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);

            m_sLId = creator.CreateBlankLoanFile();
        }
        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            if (m_sLId != Guid.Empty)
            {
                Tools.DeclareLoanFileInvalid(BrokerUserPrincipal.CurrentPrincipal, m_sLId, false, false);

                try
                {
                    LoanFileCache dataLoanCache = new LoanFileCache(m_sLId, m_fieldList);
                    dataLoanCache.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(m_principal));
                    //dataLoanCache.InitLoad();

                    Assert.Fail("Should not allow load loan cache if loan is delete.");
                }
                catch (LoanNotFoundException)
                {
                }
            }
        }

        private CPageData CreateDataObject(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(LoanFileCacheTest));
        }

        [SetUp]
        public void TestSetup()
        {
            CPageData dataLoan = CreateDataObject(m_sLId);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            dataLoan.sStatusT = E_sStatusT.Loan_Approved;
            dataLoan.sOpenedD_rep = "12/05/2010";
            dataLoan.sTerm_rep = "180";
            dataLoan.sPurchPrice_rep = "300,000";
            dataLoan.sEquityCalc_rep = "60,000";

            CAppData dataApp = dataLoan.GetAppData(0);
            dataApp.aBFirstNm = "aBFirstNm";
            dataApp.aBLastNm = "aBLastNm";

            dataLoan.Save();
        }

        [Test]
        public void TestRetrieveInvalidField()
        {
            LoanFileCache dataLoanCache = new LoanFileCache(m_sLId, m_fieldList);
            dataLoanCache.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(m_principal));
            //dataLoanCache.InitLoad();

            float floatValue;
            Assert.AreEqual(false, dataLoanCache.TryGetFloat("aBAssetsVerifREcv", out floatValue));
        }
        [Test]
        public void TestRetrieveLoanCacheValue()
        {
            CPageData dataLoan = CreateDataObject(m_sLId);
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(0);


            LoanFileCache dataLoanCache = new LoanFileCache(m_sLId, m_fieldList);
            dataLoanCache.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(m_principal));
            //dataLoanCache.InitLoad();

            Assert.AreEqual(true, dataLoanCache.IsAssignedToCurrentUser, "New loan should be assign to current user.");
            string stringValue = null;
            float floatValue;
            DateTime dateValue;
            int intValue;

            bool bRet = false;

            bRet = dataLoanCache.TryGetFloat("sPurchPrice", out floatValue);
            Assert.AreEqual(true, bRet, "sPurchPrice");

            Assert.AreEqual(Convert.ToSingle(dataLoan.sPurchPrice), floatValue, "sPurchPrice value");

            bRet = dataLoanCache.TryGetInt("sTerm", out intValue);
            Assert.AreEqual(true, bRet, "sTerm");

            Assert.AreEqual(dataLoan.sTerm, intValue, "sTerm value");

            bRet = dataLoanCache.TryGetDateTime("sOpenedD", out dateValue);
            Assert.AreEqual(true, bRet, "sOpenedD");
            Assert.AreEqual(dataLoan.sOpenedD.DateTimeForComputationWithTime, dateValue, "sOpenedD value");

            bRet = dataLoanCache.TryGetDateTime("sopenedd", out dateValue); // Test case insensitive
            Assert.AreEqual(true, bRet, "sOpenedD");
            Assert.AreEqual(dataLoan.sOpenedD.DateTimeForComputationWithTime, dateValue, "sOpenedD value");

            bRet = dataLoanCache.TryGetString("aBNm", out stringValue);
            Assert.AreEqual(true, bRet, "aBNm");
            Assert.AreEqual(dataApp.aBNm, stringValue, "aBNm value");
        }
    }
}
