﻿using DataAccess;
using LendersOffice.Constants;
using LendersOffice.Security;
using LendOSolnTest.Common;
using NUnit.Framework;
using System;

namespace LendOSolnTest.Loan
{
    [TestFixture]
    public class HelocDataTest
    {
        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            LoginTools.LoginAs(TestAccountType.LoTest001);
        }

        private Guid CreateLoan()
        {
            AbstractUserPrincipal brokerUser = BrokerUserPrincipal.CurrentPrincipal;
            CLoanFileCreator creator = CLoanFileCreator.GetCreator(brokerUser, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
            Guid sLId = creator.CreateBlankLoanFile();
            return sLId;
        }

        private CPageData CreateDataObject(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(HelocDataTest));
        }

        [Test]
        public void TestHelocPayments()
        {
            var loanId = CreateLoan();

            // We have to save as line of credit to get CPageHelocBase 
            // in next object init.
            CPageData dataLoan = CreateDataObject(loanId);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sIsLineOfCredit = true;
            dataLoan.Save();

            dataLoan = CreateDataObject(loanId);
            dataLoan.InitLoad();
            Assert.That(dataLoan.sIsLineOfCredit == true);

            dataLoan.sCreditLineAmt_rep = "100000.00";
            dataLoan.sLAmtLckd = true;
            dataLoan.sLAmtCalc_rep = "50000";
            dataLoan.sQualIRLckd = true;
            dataLoan.sQualIR_rep = "6.000";
            dataLoan.sNoteIR_rep = "4.500";
            dataLoan.sHelocDraw_rep = "120";
            dataLoan.sTerm_rep = "240";
            dataLoan.sDue_rep = "180";
            Assert.AreEqual(dataLoan.sHelocRepay_rep, "60");

            // These test cases provided by Doug Telfer in case 245256 spec
            var testCases = new[] 
            {
                new
                {
                    sHelocPmtFormulaT =  E_sHelocPmtFormulaT.InterestOnly,
                    sHelocPmtBaseT = E_sHelocPmtBaseT.LineAmount,
                    sHelocPmtFormulaRateT =  E_sHelocPmtFormulaRateT.NoteRate,
                    sHelocPmtAmortTermT = E_sHelocPmtAmortTermT.RepayMonths,
                    sHelocPmtMb_rep = "0.00",
                    sHelocPmtPcBase_rep = "0.000",
                    sProThisMPmt_rep = "$375.00"
                },
                new
                {
                    sHelocPmtFormulaT =  E_sHelocPmtFormulaT.InterestOnly,
                    sHelocPmtBaseT = E_sHelocPmtBaseT.InitialDraw,
                    sHelocPmtFormulaRateT =  E_sHelocPmtFormulaRateT.NoteRate,
                    sHelocPmtAmortTermT = E_sHelocPmtAmortTermT.RepayMonths,
                    sHelocPmtMb_rep = "5.00",
                    sHelocPmtPcBase_rep = "0.000",
                    sProThisMPmt_rep = "$192.50"
                },
                new
                {
                    sHelocPmtFormulaT =  E_sHelocPmtFormulaT.InterestOnly,
                    sHelocPmtBaseT = E_sHelocPmtBaseT.LineAmount,
                    sHelocPmtFormulaRateT =  E_sHelocPmtFormulaRateT.QualRate,
                    sHelocPmtAmortTermT = E_sHelocPmtAmortTermT.RepayMonths,
                    sHelocPmtMb_rep = "0.00",
                    sHelocPmtPcBase_rep = "0.000",
                    sProThisMPmt_rep = "$500.00"
                },
                new
                {
                    sHelocPmtFormulaT =  E_sHelocPmtFormulaT.FixedPercent,
                    sHelocPmtBaseT = E_sHelocPmtBaseT.LineAmount,
                    sHelocPmtFormulaRateT =  E_sHelocPmtFormulaRateT.NoteRate,
                    sHelocPmtAmortTermT = E_sHelocPmtAmortTermT.RepayMonths,
                    sHelocPmtMb_rep = "15.00",
                    sHelocPmtPcBase_rep = "6.000",
                    sProThisMPmt_rep = "$6,015.00"
                },
                new
                {
                    sHelocPmtFormulaT =  E_sHelocPmtFormulaT.Amortizing,
                    sHelocPmtBaseT = E_sHelocPmtBaseT.LineAmount,
                    sHelocPmtFormulaRateT =  E_sHelocPmtFormulaRateT.NoteRate,
                    sHelocPmtAmortTermT = E_sHelocPmtAmortTermT.DrawMonths,
                    sHelocPmtMb_rep = "0.00",
                    sHelocPmtPcBase_rep = "0.000",
                    sProThisMPmt_rep = "$1,036.38"
                },
                new
                {
                    sHelocPmtFormulaT =  E_sHelocPmtFormulaT.Amortizing,
                    sHelocPmtBaseT = E_sHelocPmtBaseT.InitialDraw,
                    sHelocPmtFormulaRateT =  E_sHelocPmtFormulaRateT.NoteRate,
                    sHelocPmtAmortTermT = E_sHelocPmtAmortTermT.RepayMonths,
                    sHelocPmtMb_rep = "0.00",
                    sHelocPmtPcBase_rep = "0.000",
                    sProThisMPmt_rep = "$932.15"
                },
                new
                {
                    sHelocPmtFormulaT =  E_sHelocPmtFormulaT.Amortizing,
                    sHelocPmtBaseT = E_sHelocPmtBaseT.LineAmount,
                    sHelocPmtFormulaRateT =  E_sHelocPmtFormulaRateT.QualRate,
                    sHelocPmtAmortTermT = E_sHelocPmtAmortTermT.TermMonths,
                    sHelocPmtMb_rep = "20.00",
                    sHelocPmtPcBase_rep = "0.000",
                    sProThisMPmt_rep = "$736.43"
                },
                new
                {
                    sHelocPmtFormulaT =  E_sHelocPmtFormulaT.Amortizing,
                    sHelocPmtBaseT = E_sHelocPmtBaseT.InitialDraw,
                    sHelocPmtFormulaRateT =  E_sHelocPmtFormulaRateT.QualRate,
                    sHelocPmtAmortTermT = E_sHelocPmtAmortTermT.DueMonths,
                    sHelocPmtMb_rep = "0.00",
                    sHelocPmtPcBase_rep = "0.000",
                    sProThisMPmt_rep = "$421.93"
                }
            };

            foreach (var testCase in testCases)
            {
                dataLoan.sHelocQualPmtFormulaT = testCase.sHelocPmtFormulaT;
                dataLoan.sHelocQualPmtBaseT = testCase.sHelocPmtBaseT;
                dataLoan.sHelocQualPmtFormulaRateT = testCase.sHelocPmtFormulaRateT;
                dataLoan.sHelocQualPmtAmortTermT = testCase.sHelocPmtAmortTermT;
                dataLoan.sHelocQualPmtMb_rep = testCase.sHelocPmtMb_rep;
                dataLoan.sHelocQualPmtPcBase_rep = testCase.sHelocPmtPcBase_rep;
                Assert.AreEqual(testCase.sProThisMPmt_rep, dataLoan.sProThisMQual_rep);

                dataLoan.sHelocPmtFormulaT = testCase.sHelocPmtFormulaT;
                dataLoan.sHelocPmtBaseT = testCase.sHelocPmtBaseT;
                dataLoan.sHelocPmtFormulaRateT = testCase.sHelocPmtFormulaRateT;
                dataLoan.sHelocPmtAmortTermT = testCase.sHelocPmtAmortTermT;
                dataLoan.sHelocPmtMb_rep = testCase.sHelocPmtMb_rep;
                dataLoan.sHelocPmtPcBase_rep = testCase.sHelocPmtPcBase_rep;
                Assert.AreEqual(testCase.sProThisMPmt_rep, dataLoan.sProThisMPmt_rep);
            }
        }
    }
}
