﻿using System;
using System.Collections.Generic;
using DataAccess;
using LendersOffice.Constants;
using LendersOffice.PmlSnapshot;
using LendOSolnTest.Common;
using LendOSolnTest.Fakes;
using NUnit.Framework;

namespace LendOSolnTest.Loan
{
    [TestFixture]
    public class PmlSnapshotTest
    {
        [TestFixtureSetUp]
        public void Setup()
        {
            LoginTools.LoginAs(TestAccountType.LoTest001);
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
            FakePageDataUtilities.Instance.Reset();
        }

        [Test]
        public void TakeSnapshotTest()
        {
            CPageData dataLoan = CreateDataLoan();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            CAppData dataApp = dataLoan.GetAppData(0);
            dataApp.aBFirstNm = "aBFirstNm";
            dataApp.aBMidNm = "aBMidNm";
            dataApp.aBLastNm = "aBLastNm";
            dataApp.aBSuffix = "aBSuffix";
            dataApp.aBSsn = "000-00-0001";

            dataApp.aCFirstNm = "aCFirstNm";
            dataApp.aCMidNm = "aCMidNm";
            dataApp.aCLastNm = "aCLastNm";
            dataApp.aCSuffix = "aCSuffix";
            dataApp.aCSsn = "000-00-0002";

            ILiabilityRegular liaRegular = dataApp.aLiaCollection.AddRegularRecord();
            liaRegular.ComNm = "ComNm";
            liaRegular.Bal_rep = "$8,000.00";
            liaRegular.Update();

            liaRegular = dataApp.aLiaCollection.AddRegularRecord();
            liaRegular.ComNm = "ComNm_02";
            liaRegular.Bal_rep = "$7,000.00";
            liaRegular.Update();

            dataLoan.sSpAddr = "sSpAddr";
            dataLoan.sSpCity = "sSpCity";
            dataLoan.sSpStatePe = "CA";
            dataLoan.sSpCounty = "sSpCounty";

            dataLoan.TakeSnapshotOfPmlDataBeforeRunningPricing();

            dataLoan.Save();

            dataLoan.sSpAddr = "sSpAddr_Edit";
            dataApp = dataLoan.GetAppData(0);
            dataApp.aBFirstNm = "aBFirstNm_Edit";

            var result = dataLoan.GetModifyPmlData();

            Assert.AreEqual(2, result.Count);
        }


        [Test]
        public void TestPmlSnapshotImportExport()
        {
            DataLoanPmlSnapshot originalSnapshot = new DataLoanPmlSnapshot();
            originalSnapshot.sCltvRPe_rep = "sCltvRPe_rep";
            originalSnapshot.sDownPmtPcPe_rep = "sDownPmtPcPe_rep";
            originalSnapshot.sEquityPe_rep = "sEquityPe_rep";

            DataAppPmlSnapshot dataAppSnapshot = originalSnapshot.CreateDataApp("App01");
            dataAppSnapshot.aBFirstNm = "aBFirstNm";
            dataAppSnapshot.aBLastNm = "aBFirstNm";

            LiabilityPmlSnapshot liabilitySnapshot = new LiabilityPmlSnapshot();
            liabilitySnapshot.Id = "Lia01";
            liabilitySnapshot.Bal_rep = "Bal_rep";
            liabilitySnapshot.ComNm = "ComNm";
            liabilitySnapshot.Pmt_rep = "Pmt_rep";
            liabilitySnapshot.UsedInRatio = true;
            liabilitySnapshot.WillBePdOff = false;

            dataAppSnapshot.AddLiability(liabilitySnapshot);

            liabilitySnapshot = new LiabilityPmlSnapshot();
            liabilitySnapshot.Id = "Lia02";
            liabilitySnapshot.Bal_rep = "Bal_rep";
            liabilitySnapshot.ComNm = "ComNm";
            liabilitySnapshot.Pmt_rep = "Pmt_rep";
            liabilitySnapshot.UsedInRatio = true;
            liabilitySnapshot.WillBePdOff = false;
            dataAppSnapshot.AddLiability(liabilitySnapshot);

            dataAppSnapshot = originalSnapshot.CreateDataApp("App02");
            dataAppSnapshot.aBFirstNm = "aBFirstNm";
            dataAppSnapshot.aBLastNm = "aBFirstNm";

            string xml = originalSnapshot.GenerateXml();
            DataLoanPmlSnapshot newSnapshot = new DataLoanPmlSnapshot();
            newSnapshot.LoadFromXml(xml);

            string newXml = newSnapshot.GenerateXml();
            Assert.AreEqual(xml, newXml, "GenerateXml");
            Assert.AreEqual("sCltvRPe_rep", newSnapshot.sCltvRPe_rep, "sCltvRPe_rep");

            var oList = originalSnapshot.Diff(newSnapshot);

            Assert.AreEqual(0, oList.Count, oList.GetDebugString());



        }

        [Test]
        public void TestPmlSnapshotLiabilityDifferent()
        {
            #region Setup Original Snapshot
            DataLoanPmlSnapshot originalSnapshot = new DataLoanPmlSnapshot();
            originalSnapshot.sCltvRPe_rep = "sCltvRPe_rep";
            originalSnapshot.sDownPmtPcPe_rep = "sDownPmtPcPe_rep";
            originalSnapshot.sEquityPe_rep = "sEquityPe_rep";
            originalSnapshot.sProdMIOptionT = E_sProdMIOptionT.BorrowerPdPmi;

            DataAppPmlSnapshot dataAppSnapshot = originalSnapshot.CreateDataApp("App01");
            dataAppSnapshot.aBFirstNm = "aBFirstNm";
            dataAppSnapshot.aBLastNm = "aBFirstNm";

            List<LiabilityPmlSnapshot> liabilityList = new List<LiabilityPmlSnapshot>()
            {
                new LiabilityPmlSnapshot() { Id = "Lia01", Bal_rep="Bal_rep_01", ComNm="ComNm_01", Pmt_rep="Pmt_rep_01", UsedInRatio=true, WillBePdOff=true}
                , new LiabilityPmlSnapshot() { Id = "Lia02", Bal_rep="Bal_rep_02", ComNm="ComNm_02", Pmt_rep="Pmt_rep_02", UsedInRatio=true, WillBePdOff=false}
                , new LiabilityPmlSnapshot() { Id = "Lia03", Bal_rep="Bal_rep_03", ComNm="ComNm_03", Pmt_rep="Pmt_rep_03", UsedInRatio=false, WillBePdOff=true}
                , new LiabilityPmlSnapshot() { Id = "Lia04", Bal_rep="Bal_rep_04", ComNm="ComNm_04", Pmt_rep="Pmt_rep_04", UsedInRatio=false, WillBePdOff=false}
            };
            foreach (var o in liabilityList)
            {
                dataAppSnapshot.AddLiability(o);
            }
            dataAppSnapshot = originalSnapshot.CreateDataApp("App02");
            dataAppSnapshot.aBFirstNm = "aBFirstNm";
            dataAppSnapshot.aBLastNm = "aBFirstNm";

            dataAppSnapshot = originalSnapshot.CreateDataApp("App03");
            dataAppSnapshot.aBFirstNm = "aBFirstNm";
            dataAppSnapshot.aBLastNm = "aBFirstNm";
            #endregion

            #region Setup New Snapshot
            DataLoanPmlSnapshot newSnapshot = new DataLoanPmlSnapshot();
            newSnapshot.sCltvRPe_rep = "sCltvRPe_rep";
            newSnapshot.sDownPmtPcPe_rep = "sDownPmtPcPe_rep";
            newSnapshot.sEquityPe_rep = "sEquityPe_rep";
            newSnapshot.sPrimAppTotNonspIPe_rep = "sPrimAppTotNonspIPe_rep";
            newSnapshot.sProdCrManual120MortLateCount_rep = "sProdCrManual120MortLateCount_rep";
            newSnapshot.sProdMIOptionT = E_sProdMIOptionT.NoPmi;

            DataAppPmlSnapshot newDataAppSnapshot = newSnapshot.CreateDataApp("App01");
            newDataAppSnapshot.aBFirstNm = "aBFirstNm";
            newDataAppSnapshot.aBLastNm = "aBFirstNm";

            liabilityList = new List<LiabilityPmlSnapshot>()
            {
                new LiabilityPmlSnapshot() { Id = "Lia02", Bal_rep="Bal_rep_02", ComNm="ComNm_02", Pmt_rep="Pmt_rep_02", UsedInRatio=true, WillBePdOff=false}
                , new LiabilityPmlSnapshot() { Id = "Lia03", Bal_rep="Bal_rep_03_Edit", ComNm="ComNm_03", Pmt_rep="Pmt_rep_03", UsedInRatio=false, WillBePdOff=true}
                , new LiabilityPmlSnapshot() { Id = "Lia04", Bal_rep="Bal_rep_04", ComNm="ComNm_04_Edit", Pmt_rep="Pmt_rep_04_Edit", UsedInRatio=false, WillBePdOff=false}
                , new LiabilityPmlSnapshot() { Id = "Lia05", Bal_rep="Bal_rep_05", ComNm="ComNm_05", Pmt_rep="Pmt_rep_05", UsedInRatio=true, WillBePdOff=true}

            };
            foreach (var o in liabilityList)
            {
                newDataAppSnapshot.AddLiability(o);
            }
            dataAppSnapshot = newSnapshot.CreateDataApp("App02");
            dataAppSnapshot.aBFirstNm = "aBFirstNm";
            dataAppSnapshot.aBLastNm = "aBFirstNm";

            dataAppSnapshot = newSnapshot.CreateDataApp("App04");
            dataAppSnapshot.aBFirstNm = "aBFirstNm";
            dataAppSnapshot.aBLastNm = "aBFirstNm";
            #endregion


            var oResult = originalSnapshot.Diff(newSnapshot);

            Assert.AreEqual(9, oResult.Count, oResult.GetDebugString());


        }

        [Test]
        public void TestPmlSnapshotCompareWhenCreditReportDifferent()
        {
            DataLoanPmlSnapshot originalSnapshot = new DataLoanPmlSnapshot();

            DataAppPmlSnapshot dataAppSnapshot = originalSnapshot.CreateDataApp("App01");

            dataAppSnapshot.aCreditReportFileDbKey = Guid.NewGuid();

            List<LiabilityPmlSnapshot> liabilityList = new List<LiabilityPmlSnapshot>()
            {
                new LiabilityPmlSnapshot() { Id = "Lia01", Bal_rep="Bal_rep_01", ComNm="ComNm_01", Pmt_rep="Pmt_rep_01", UsedInRatio=true, WillBePdOff=true}
            };
            foreach (var o in liabilityList)
            {
                dataAppSnapshot.AddLiability(o);
            }

            DataLoanPmlSnapshot newSnapshot = new DataLoanPmlSnapshot();

            dataAppSnapshot = newSnapshot.CreateDataApp("App01");

            dataAppSnapshot.aCreditReportFileDbKey = Guid.NewGuid();

            liabilityList = new List<LiabilityPmlSnapshot>()
            {
                new LiabilityPmlSnapshot() { Id = "Lia01", Bal_rep="Bal_rep_01_edit", ComNm="ComNm_01_edit", Pmt_rep="Pmt_rep_01", UsedInRatio=true, WillBePdOff=true}
                ,new LiabilityPmlSnapshot() { Id = "Lia02", Bal_rep="Bal_rep_01_edit", ComNm="ComNm_01_edit", Pmt_rep="Pmt_rep_01", UsedInRatio=true, WillBePdOff=true}

            };
            foreach (var o in liabilityList)
            {
                dataAppSnapshot.AddLiability(o);
            }

            var oResult = originalSnapshot.Diff(newSnapshot);
            Assert.AreEqual(1, oResult.Count, oResult.GetDebugString());


        }
        [Test]
        public void TestPmlSnapshotCompareWhenCreditReportSameButLiabilityDifferent()
        {
            Guid aCreditReportFileDbKey = Guid.NewGuid();
            DataLoanPmlSnapshot originalSnapshot = new DataLoanPmlSnapshot();

            DataAppPmlSnapshot dataAppSnapshot = originalSnapshot.CreateDataApp("App01");

            dataAppSnapshot.aCreditReportFileDbKey = aCreditReportFileDbKey;

            List<LiabilityPmlSnapshot> liabilityList = new List<LiabilityPmlSnapshot>()
            {
                new LiabilityPmlSnapshot() { Id = "Lia01", Bal_rep="Bal_rep_01", ComNm="ComNm_01", Pmt_rep="Pmt_rep_01", UsedInRatio=true, WillBePdOff=true}
            };
            foreach (var o in liabilityList)
            {
                dataAppSnapshot.AddLiability(o);
            }

            DataLoanPmlSnapshot newSnapshot = new DataLoanPmlSnapshot();

            dataAppSnapshot = newSnapshot.CreateDataApp("App01");

            dataAppSnapshot.aCreditReportFileDbKey = aCreditReportFileDbKey;

            liabilityList = new List<LiabilityPmlSnapshot>()
            {
                new LiabilityPmlSnapshot() { Id = "Lia01", Bal_rep="Bal_rep_01_edit", ComNm="ComNm_01_edit", Pmt_rep="Pmt_rep_01", UsedInRatio=true, WillBePdOff=true}
                , new LiabilityPmlSnapshot() { Id = "Lia02", Bal_rep="Bal_rep_01_edit", ComNm="ComNm_01_edit", Pmt_rep="Pmt_rep_01", UsedInRatio=true, WillBePdOff=true}

            };
            foreach (var o in liabilityList)
            {
                dataAppSnapshot.AddLiability(o);
            }

            var oResult = originalSnapshot.Diff(newSnapshot);
            Assert.AreEqual(2, oResult.Count, oResult.GetDebugString());

        }

        private void DebugSnapshotResult(PmlSnapshotDiffResult oResult)
        {
            foreach (string category in oResult.Categories)
            {
                Tools.LogError(category);
                foreach (var o in oResult.GetDiffByCategory(category))
                {
                    Tools.LogError("    " + o.Description + " - Old: " + o.OldValue + ", - New: " + o.NewValue);
                }
            }
        }
        private CPageData CreateDataLoan()
        {
            CPageData dataLoan = FakePageData.Create();
            dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
            return dataLoan;
        }
    }
}
