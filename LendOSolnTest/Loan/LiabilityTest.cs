﻿using System.Collections.Generic;
using DataAccess;
using LendersOffice.Constants;
using LendOSolnTest.Common;
using LendOSolnTest.Fakes;
using NUnit.Framework;

namespace LendOSolnTest.Loan
{
    [TestFixture]
    public class LiabilityTest
    {
        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            LoginTools.LoginAs(TestAccountType.LoTest001);
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
            FakePageDataUtilities.Instance.Reset();
        }

        private CPageData CreateDataObject()
        {
            return FakePageData.Create();
        }

        [TearDown]
        public void ClearReosAndLiabilities()
        {
            var dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            CAppData dataApp = dataLoan.GetAppData(0);
            dataApp.aReCollection.ClearAll();
            dataApp.aLiaCollection.ClearAll();
            dataApp.aReCollection.Flush();
            dataApp.aLiaCollection.Flush();

            dataLoan.Save();
        }

        [Test]
        public void NonMortgageTradelineTest()
        {
            #region Setup test data
            CPageData dataLoan = CreateDataObject();

            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            
            CAppData dataApp = dataLoan.GetAppData(0);

            ILiabilityRegular liaRegular = dataApp.aLiaCollection.AddRegularRecord();
            liaRegular.DebtT = E_DebtRegularT.Installment;
            liaRegular.ComNm = "Com1";
            liaRegular.AccNum = "111";
            liaRegular.Update();

            liaRegular = dataApp.aLiaCollection.AddRegularRecord();
            liaRegular.DebtT = E_DebtRegularT.Open;
            liaRegular.ComNm = "Com2";
            liaRegular.AccNum = "222";
            liaRegular.Update();

            liaRegular = dataApp.aLiaCollection.AddRegularRecord();
            liaRegular.DebtT = E_DebtRegularT.Other;
            liaRegular.ComNm = "Com3";
            liaRegular.AccNum = "333";
            liaRegular.Update();

            liaRegular = dataApp.aLiaCollection.AddRegularRecord();
            liaRegular.DebtT = E_DebtRegularT.Revolving;
            liaRegular.ComNm = "Com4";
            liaRegular.AccNum = "444";
            liaRegular.Update();

            dataLoan.Save();
            #endregion

            dataLoan = FakePageData.Create();
            dataLoan.InitLoad();

            dataApp = dataLoan.GetAppData(0);
            ILiaCollection aLiaCollection = dataApp.aLiaCollection;

            for (int i = 0; i < aLiaCollection.CountRegular; i++)
            {
                ILiabilityRegular field = aLiaCollection.GetRegularRecordAt(i);
                Assert.AreEqual(false, field.IsSubjectPropertyMortgage);
            }

        }

        [Test]
        public void MortgageTradelineWithNoReoTest()
        {
            #region Setup Test data
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            CAppData dataApp = dataLoan.GetAppData(0);

            ILiabilityRegular liaRegular = dataApp.aLiaCollection.AddRegularRecord();
            liaRegular.DebtT = E_DebtRegularT.Mortgage;
            liaRegular.Bal_rep = "$300,000";
            liaRegular.AccNum = "123";
            liaRegular.Update();

            dataLoan.Save();
            #endregion

            dataLoan = FakePageData.Create();
            dataLoan.InitLoad();
            liaRegular = dataApp.aLiaCollection.GetRegularRecordAt(0);

            Assert.AreEqual(E_DebtRegularT.Mortgage, liaRegular.DebtT);
            Assert.AreEqual(true, liaRegular.IsSubjectPropertyMortgage); // Default is true.

        }
        [Test]
        public void MortgageTradelineWithNoReoIsSubjFalseTest()
        {
            #region Setup Test data
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            CAppData dataApp = dataLoan.GetAppData(0);

            ILiabilityRegular liaRegular = dataApp.aLiaCollection.AddRegularRecord();
            liaRegular.DebtT = E_DebtRegularT.Mortgage;
            liaRegular.Bal_rep = "$300,000";
            liaRegular.AccNum = "123";
            liaRegular.IsSubjectPropertyMortgage = false;
            liaRegular.Update();

            dataLoan.Save();
            #endregion

            dataLoan = FakePageData.Create();
            dataLoan.InitLoad();
            liaRegular = dataApp.aLiaCollection.GetRegularRecordAt(0);

            Assert.AreEqual(E_DebtRegularT.Mortgage, liaRegular.DebtT);
            Assert.AreEqual(false, liaRegular.IsSubjectPropertyMortgage); 

        }
        [Test]
        public void MortgageTradelineWithNoReoIsSubjTrueTest()
        {
            #region Setup Test data
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            CAppData dataApp = dataLoan.GetAppData(0);

            ILiabilityRegular liaRegular = dataApp.aLiaCollection.AddRegularRecord();
            liaRegular.DebtT = E_DebtRegularT.Mortgage;
            liaRegular.Bal_rep = "$300,000";
            liaRegular.AccNum = "123";
            liaRegular.IsSubjectPropertyMortgage = true;
            liaRegular.Update();

            dataLoan.Save();
            #endregion

            dataLoan = FakePageData.Create();
            dataLoan.InitLoad();
            liaRegular = dataApp.aLiaCollection.GetRegularRecordAt(0);

            Assert.AreEqual(E_DebtRegularT.Mortgage, liaRegular.DebtT);
            Assert.AreEqual(true, liaRegular.IsSubjectPropertyMortgage);

        }

        [Test]
        public void MortgageTradelineWithReoIsSubjTrueTest()
        {
            #region Setup Test data
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            CAppData dataApp = dataLoan.GetAppData(0);

            var reField = dataApp.aReCollection.AddRegularRecord();
            reField.IsSubjectProp = true;
            reField.Update();

            ILiabilityRegular liaRegular = dataApp.aLiaCollection.AddRegularRecord();
            liaRegular.DebtT = E_DebtRegularT.Mortgage;
            liaRegular.Bal_rep = "$300,000";
            liaRegular.AccNum = "123";
            liaRegular.MatchedReRecordId = reField.RecordId;
            liaRegular.Update();

            dataLoan.Save();
            #endregion

            dataLoan = FakePageData.Create();
            dataLoan.InitLoad();
            liaRegular = dataApp.aLiaCollection.GetRegularRecordAt(0);

            Assert.AreEqual(E_DebtRegularT.Mortgage, liaRegular.DebtT);
            Assert.AreEqual(true, liaRegular.IsSubjectPropertyMortgage);

            reField = dataApp.aReCollection.GetRegularRecordAt(0);
            reField.IsSubjectProp = false;
            reField.Update();

            Assert.AreEqual(E_DebtRegularT.Mortgage, liaRegular.DebtT);
            Assert.AreEqual(false, liaRegular.IsSubjectPropertyMortgage); // This property is link with REO.
        }

        [Test]
        public void MortgageTradelineIsSubjectProperty1stMortgageTest()
        {
            #region Setup Test data
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            CAppData dataApp = dataLoan.GetAppData(0);

            var reField = dataApp.aReCollection.AddRegularRecord();
            reField.IsSubjectProp = true;
            reField.Update();

            ILiabilityRegular liaRegular = dataApp.aLiaCollection.AddRegularRecord();
            liaRegular.DebtT = E_DebtRegularT.Mortgage;
            liaRegular.Bal_rep = "$300,000";
            liaRegular.AccNum = "123";
            liaRegular.MatchedReRecordId = reField.RecordId;
            liaRegular.IsSubjectProperty1stMortgage = false;
            liaRegular.Update();



            liaRegular = dataApp.aLiaCollection.AddRegularRecord();
            liaRegular.DebtT = E_DebtRegularT.Mortgage;
            liaRegular.Bal_rep = "$200,000";
            liaRegular.AccNum = "123";
            liaRegular.MatchedReRecordId = reField.RecordId;
            liaRegular.IsSubjectProperty1stMortgage = true;
            liaRegular.Update();

            dataLoan.Save();
            #endregion

            dataLoan = FakePageData.Create();
            dataLoan.InitLoad();
            liaRegular = dataApp.aLiaCollection.GetRegularRecordAt(0);

            Assert.AreEqual(E_DebtRegularT.Mortgage, liaRegular.DebtT);
            Assert.AreEqual(true, liaRegular.IsSubjectPropertyMortgage);
            Assert.AreEqual(false, liaRegular.IsSubjectProperty1stMortgage);

            liaRegular = dataApp.aLiaCollection.GetRegularRecordAt(1);
            Assert.AreEqual(E_DebtRegularT.Mortgage, liaRegular.DebtT);
            Assert.AreEqual(true, liaRegular.IsSubjectPropertyMortgage);
            Assert.AreEqual(true, liaRegular.IsSubjectProperty1stMortgage);


            // If both mortgage tradeline set IsSubjectProperty1stMortgage=true then the last one win.
            liaRegular = dataApp.aLiaCollection.GetRegularRecordAt(0);
            liaRegular.IsSubjectProperty1stMortgage = true;
            liaRegular.Update();
            Assert.AreEqual(true, liaRegular.IsSubjectProperty1stMortgage);

            liaRegular = dataApp.aLiaCollection.GetRegularRecordAt(1);
            liaRegular.IsSubjectProperty1stMortgage = true;
            liaRegular.Update();


            liaRegular = dataApp.aLiaCollection.GetRegularRecordAt(0);
            Assert.AreEqual(false, liaRegular.IsSubjectProperty1stMortgage);

            liaRegular = dataApp.aLiaCollection.GetRegularRecordAt(1);
            Assert.AreEqual(true, liaRegular.IsSubjectProperty1stMortgage);


            reField = dataApp.aReCollection.GetRegularRecordAt(0);
            reField.IsSubjectProp = false;
            reField.Update();

            Assert.AreEqual(E_DebtRegularT.Mortgage, liaRegular.DebtT);
            Assert.AreEqual(false, liaRegular.IsSubjectPropertyMortgage); // This property is link with REO.
            Assert.AreEqual(false, liaRegular.IsSubjectProperty1stMortgage);
        }
        
        [Test]
        public void sMortgageLiaListTest()
        {
            #region Setup Test data
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            CAppData dataApp = dataLoan.GetAppData(0);

            var reField = dataApp.aReCollection.AddRegularRecord();
            reField.IsSubjectProp = true;
            reField.Update();

            ILiabilityRegular liaRegular = dataApp.aLiaCollection.AddRegularRecord();
            liaRegular.DebtT = E_DebtRegularT.Mortgage;
            liaRegular.ComNm = "Com1";
            liaRegular.Bal_rep = "$100,000";
            liaRegular.AccNum = "123";
            liaRegular.MatchedReRecordId = reField.RecordId;
            liaRegular.IsSubjectProperty1stMortgage = false;
            liaRegular.Update();
            
            liaRegular = dataApp.aLiaCollection.AddRegularRecord();
            liaRegular.ComNm = "Com2";
            liaRegular.DebtT = E_DebtRegularT.Mortgage;
            liaRegular.Bal_rep = "$200,000";
            liaRegular.AccNum = "123";
            liaRegular.MatchedReRecordId = reField.RecordId;
            liaRegular.IsSubjectProperty1stMortgage = true;
            liaRegular.Update();

            liaRegular = dataApp.aLiaCollection.AddRegularRecord();
            liaRegular.ComNm = "Com3";
            liaRegular.DebtT = E_DebtRegularT.Mortgage;
            liaRegular.Bal_rep = "$300,000";
            liaRegular.AccNum = "123";
            liaRegular.MatchedReRecordId = reField.RecordId;
            liaRegular.IsSubjectProperty1stMortgage = false;
            liaRegular.Update();

            dataLoan.Save();
            #endregion

            dataLoan = FakePageData.Create();
            dataLoan.InitLoad();

            List<ILiabilityRegular> list = dataLoan.sMortgageLiaList;
            Assert.AreEqual(3, list.Count);
            Assert.AreEqual("Com3", list[0].ComNm);
            Assert.AreEqual("Com2", list[1].ComNm);
            Assert.AreEqual("Com1", list[2].ComNm);

        }

        [Test]
        public void LinkedReoRecord_UpdateReoStatusFromResidenceToSold_UpdatesLiaBalTot()
        {
            var loan = this.CreateDataObject();
            loan.InitSave(ConstAppDavid.SkipVersionCheck);
            var reo = loan.GetAppData(0).aReCollection.AddRegularRecord();
            reo.Addr = "TEST";
            reo.StatT = E_ReoStatusT.Residence;

            var liability = loan.GetAppData(0).aLiaCollection.AddRegularRecord();
            liability.DebtT = E_DebtRegularT.Mortgage;
            liability.Bal = 100;
            liability.MatchedReRecordId = reo.RecordId;

            loan.Save();

            Assert.AreEqual(100m, loan.GetAppData(0).aLiaBalTot);

            loan = this.CreateDataObject();
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            reo = loan.GetAppData(0).aReCollection.GetRegRecordOf(reo.RecordId);
            reo.StatT = E_ReoStatusT.Sale;

            loan.Save();

            Assert.AreEqual(0, loan.GetAppData(0).aLiaBalTot);
        }
    }
}
