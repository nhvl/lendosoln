﻿namespace LendOSolnTest.Loan
{
    using DataAccess;
    using Fakes;
    using NUnit.Framework;

    [TestFixture]
    public class TridNegativeAmortizationTest
    {
        [Test]
        public void sTRIDNegativeAmortizationT_NoAmortTable_ReturnsNoNegativeAmort()
        {
            var loan = this.GetInitializedLoan();

            // Do not set term or due, which will cause amortization calculation
            // to fail and not calculate the table.

            Assert.AreEqual(E_sTRIDNegativeAmortizationT.NoNegativeAmortization, loan.sTRIDNegativeAmortizationT);
        }

        [Test]
        [TestCase(2, 500, E_sTRIDNegativeAmortizationT.Potential)]
        [TestCase(1, 500, E_sTRIDNegativeAmortizationT.NoNegativeAmortization)]
        [TestCase(0, 250, E_sTRIDNegativeAmortizationT.NoNegativeAmortization)]
        public void sTRIDNegativeAmortizationT_SingleRowInAmortTable_UsesGfeMaxLoanBalanceCalculation(
            decimal pmtAdjMaxBalPc,
            decimal finalLoanAmount,
            E_sTRIDNegativeAmortizationT expectedNegativeAmortValue)
        {
            var loan = this.GetInitializedLoan();

            loan.sTerm = 1;
            loan.sDue = 1;
            loan.sNoteIR = 4.0M;

            loan.sPmtAdjMaxBalPc = pmtAdjMaxBalPc;
            loan.sPurchPrice = finalLoanAmount; 

            Assert.AreEqual(expectedNegativeAmortValue, loan.sTRIDNegativeAmortizationT);
        }

        [Test]
        [TestCase(2, 500, E_sTRIDNegativeAmortizationT.Potential)]
        [TestCase(1, 500, E_sTRIDNegativeAmortizationT.NoNegativeAmortization)]
        [TestCase(0, 250, E_sTRIDNegativeAmortizationT.NoNegativeAmortization)]
        public void sTRIDNegativeAmortizationT_TableCheckIndicatesNoNegativeAmort_UsesGfeMaxLoanBalanceCalculation(
            decimal pmtAdjMaxBalPc,
            decimal finalLoanAmount,
            E_sTRIDNegativeAmortizationT expectedNegativeAmortValue)
        {
            var loan = this.GetInitializedLoan();

            loan.sTerm = 360;
            loan.sDue = 360;
            loan.sNoteIR = 4.0M;

            loan.sPmtAdjMaxBalPc = pmtAdjMaxBalPc;
            loan.sPurchPrice = finalLoanAmount;

            Assert.AreEqual(expectedNegativeAmortValue, loan.sTRIDNegativeAmortizationT);
        }

        [Test]
        public void sTRIDNegativeAmortizationT_TableCheckIndicatesScheduledAmortFirstRow_ReturnsScheduledNegativeAmort()
        {
            var loan = this.GetInitializedLoan();

            loan.sTerm = 360;
            loan.sDue = 360;
            loan.sNoteIR = 4.0M;
            loan.sPurchPrice = 400000.0M;

            loan.sIsOptionArm = true;
            loan.sOptionArmMinPayOptionT = E_sOptionArmMinPayOptionT.ByDiscountNoteIR;
            loan.sOptionArmNoteIRDiscount = 3.0M;
            loan.sOptionArmMinPayPeriod = 12;

            Assert.AreEqual(E_sTRIDNegativeAmortizationT.Scheduled, loan.sTRIDNegativeAmortizationT);
        }

        [Test]
        public void sTRIDNegativeAmortizationT_TableCheckIndicatesScheduledAmortNotFirstRow_ReturnsScheduledNegativeAmort()
        {
            var loan = this.GetInitializedLoan();

            loan.sTerm = 360;
            loan.sDue = 360;
            loan.sNoteIR = 12.0M;
            loan.sPurchPrice = 400000.0M;

            loan.sIsOptionArm = true;
            loan.sOptionArmMinPayOptionT = E_sOptionArmMinPayOptionT.TeaserRate;
            loan.sOptionArmTeaserR = 1.0M;
            loan.sOptionArmMinPayPeriod = 12;
            loan.sOptionArmIntroductoryPeriod = 3;
            loan.sOptionArmInitialFixMinPmtPeriod = 12;

            loan.sPmtAdjCapR = 12.0M;
            loan.sPmtAdjCapMon = 12;
            loan.sPmtAdjRecastPeriodMon = 60;
            loan.sPmtAdjRecastStop = 60;
            loan.sPmtAdjMaxBalPc = 110.0M;

            Assert.AreEqual(E_sTRIDNegativeAmortizationT.Scheduled, loan.sTRIDNegativeAmortizationT);
        }

        private CPageData GetInitializedLoan()
        {
            var loan = FakePageData.Create();
            loan.InitLoad();
            return loan;
        }
    }
}
