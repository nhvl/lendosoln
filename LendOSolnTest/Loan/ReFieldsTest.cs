﻿using System;
using DataAccess;
using LendersOffice.Constants;
using LendersOffice.Security;
using LendOSolnTest.Common;
using LendOSolnTest.Fakes;
using NUnit.Framework;

namespace LendOSolnTest.Loan
{
    [TestFixture]
    public class ReFieldsTest
    {
        private Guid loanId;

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            var principal = LoginTools.LoginAs(TestAccountType.LoTest001);

            var loanCreator = CLoanFileCreator.GetCreator(principal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
            this.loanId = loanCreator.CreateBlankLoanFile();
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
            FakePageDataUtilities.Instance.Reset();
            Tools.DeclareLoanFileInvalid(PrincipalFactory.CurrentPrincipal, this.loanId, sendNotifications: false, generateLinkLoanMsgEvent: false);
        }

        [TearDown]
        public void ClearExistingReos()
        {
            var fakeLoan = CreateDataObject();
            fakeLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            fakeLoan.GetAppData(0).aReCollection.ClearAll();
            fakeLoan.Save();
        }

        private CPageData CreateDataObject()
        {
            return FakePageData.Create();
        }

        [Test]
        public void Test()
        {
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            CAppData dataApp = dataLoan.GetAppData(0);

            var fields = dataApp.aReCollection.AddRegularRecord();
            fields.MPmt_rep = "$1,000.00";
            fields.HExp_rep = "";
            fields.StatT = E_ReoStatusT.PendingSale;

            dataLoan.Save();

            dataLoan = CreateDataObject();
            dataLoan.InitLoad();

            dataApp = dataLoan.GetAppData(0);

            fields = dataApp.aReCollection.GetRegularRecordAt(0);

            Assert.AreEqual("$1,000.00", fields.MPmt_rep, "MPmt");
            Assert.AreEqual("$0.00", fields.HExp_rep, "HExp");
            Assert.AreEqual(E_ReoStatusT.PendingSale, fields.StatT, "StatT");

        }

        [Test]
        public void IsStatusUpdated_AfterCreate_IsFalse()
        {
            CPageData loan = CreateDataObject();
            loan.InitSave(ConstAppDavid.SkipVersionCheck);
            var app = loan.GetAppData(0);

            var reo = app.aReCollection.AddRegularRecord();

            Assert.AreEqual(false, reo.IsStatusUpdated);
        }

        [Test]
        public void IsStatusUpdated_AfterStatusUpdate_IsTrue()
        {
            CPageData loan = CreateDataObject();
            loan.InitSave(ConstAppDavid.SkipVersionCheck);
            var app = loan.GetAppData(0);

            var reo = app.aReCollection.AddRegularRecord();
            Assert.AreEqual(E_ReoStatusT.Residence, reo.StatT);
            reo.StatT = E_ReoStatusT.Sale;

            Assert.AreEqual(true, reo.IsStatusUpdated);
        }

        [Test]
        public void IsStatusUpdated_RecordUpdatedAndThenRetrieved_PersistsAfterRetrieval()
        {
            CPageData loan = CreateDataObject();
            loan.InitSave(ConstAppDavid.SkipVersionCheck);
            var app = loan.GetAppData(0);

            var reo = app.aReCollection.AddRegularRecord();
            var reoId = reo.RecordId;
            Assert.AreEqual(E_ReoStatusT.Residence, reo.StatT);
            reo.StatT = E_ReoStatusT.Sale;

            reo = app.aReCollection.GetRegRecordOf(reoId);

            Assert.AreEqual(true, reo.IsStatusUpdated);
        }

        [Test]
        // This test exists to demonstrate behavior. I chose to avoid a reset
        // of this bit on Flush() because the liabilities will not be flushed
        // directly from the REO collection. That check occurs on loan save,
        // and there are several code paths which call Flush() on the collection
        // directly.
        // If a better alternative to this behavior is implemented in the future
        // and the concern I mentioned above is addressed by it, then it is fine
        // to remove this test.
        public void IsStatusUpdated_AfterFlush_DoesNotReset()
        {
            CPageData loan = CreateDataObject();
            loan.InitSave(ConstAppDavid.SkipVersionCheck);
            var app = loan.GetAppData(0);

            var reo = app.aReCollection.AddRegularRecord();
            var reoId = reo.RecordId;
            Assert.AreEqual(E_ReoStatusT.Residence, reo.StatT);
            reo.StatT = E_ReoStatusT.Sale;
            app.aReCollection.Flush();
            reo = app.aReCollection.GetRegRecordOf(reoId);

            Assert.AreEqual(true, reo.IsStatusUpdated);
        }

        [Test]
        public void IsStatusUpdated_AfterSaveAndInitSave_Resets()
        {
            // I want to test the behavior of CPageData here, so not using the fake.
            CPageData loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.loanId, typeof(ReFieldsTest));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);
            var app = loan.GetAppData(0);

            var reo = app.aReCollection.AddRegularRecord();
            var reoId = reo.RecordId;
            Assert.AreEqual(E_ReoStatusT.Residence, reo.StatT);
            reo.StatT = E_ReoStatusT.Sale;
            loan.Save();
            loan.InitSave(ConstAppDavid.SkipVersionCheck);
            // Old app reference has been replaced on InitSave.
            app = loan.GetAppData(0);
            reo = app.aReCollection.GetRegRecordOf(reoId);

            Assert.AreEqual(false, reo.IsStatusUpdated);
        }

        [Test]
        public void IsStatusUpdated_AfterSaveAndInitLoad_Resets()
        {
            // I want to test the behavior of CPageData here, so not using the fake.
            CPageData loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.loanId, typeof(ReFieldsTest));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);
            var app = loan.GetAppData(0);

            var reo = app.aReCollection.AddRegularRecord();
            var reoId = reo.RecordId;
            Assert.AreEqual(E_ReoStatusT.Residence, reo.StatT);
            reo.StatT = E_ReoStatusT.Sale;
            loan.Save();
            loan.InitLoad();
            // Old app reference has been replaced on InitLoad.
            app = loan.GetAppData(0);
            reo = app.aReCollection.GetRegRecordOf(reoId);

            Assert.AreEqual(false, reo.IsStatusUpdated);
        }
    }
}
