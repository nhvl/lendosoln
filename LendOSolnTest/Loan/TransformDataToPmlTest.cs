﻿// Author: David Dao
// Create because of OPM 27030

using DataAccess;
using LendOSolnTest.Common;
using LendOSolnTest.Fakes;
using NUnit.Framework;
namespace LendOSolnTest.Loan
{
    [TestFixture]
    public class TransformDataToPmlTest
    {
        [TestFixtureSetUp]
        public void Init()
        {
            LoginTools.LoginAs(TestAccountType.LoTest001);
        }

        [Test]
        public void TestTransformInPurchaseFirstLienOnly()
        {
            CPageData dataLoan = CreatePageData();
            dataLoan.InitLoad();
            dataLoan.sLPurposeT = E_sLPurposeT.Purchase;
            dataLoan.sPurchPrice = 100000;
            dataLoan.sEquityCalc = 20000;
            dataLoan.sIsOFinNew = false;
            dataLoan.sConcurSubFin = 0;

            Assert.AreEqual(80, dataLoan.sLtvR, "sLtvR");
            Assert.AreEqual(80, dataLoan.sCltvR, "sCltvR");

            dataLoan.TransformDataToPml(E_TransformToPmlT.FromScratch);

            dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
            Assert.AreEqual(100000, dataLoan.sHouseValPe, "sHouseValPe");
            Assert.AreEqual(20, dataLoan.sDownPmtPcPe, "sDownPmtPcPe");
            Assert.AreEqual(20000, dataLoan.sEquityPe, "sEquityPe");
            Assert.AreEqual(80, dataLoan.sLtvRPe, "sLtvRPe");
            Assert.AreEqual(80000, dataLoan.sLAmtCalcPe, "sLAmtCalcPe");
            Assert.AreEqual(0, dataLoan.sLtvROtherFinPe, "sLtvROtherFinPe");
            Assert.AreEqual(0, dataLoan.sProOFinBalPe, "sProOFinBalPe");
            Assert.AreEqual(80, dataLoan.sCltvRPe, "sCltvRPe");
        }

        [Test]
        public void TestTransformInPurchaseComboLoan()
        {
            CPageData dataLoan = CreatePageData();
            dataLoan.InitLoad();
            dataLoan.sLPurposeT = E_sLPurposeT.Purchase;
            dataLoan.sPurchPrice = 100000;
            dataLoan.sEquityCalc = 20000;
            dataLoan.sIsOFinNew = true;
            dataLoan.sConcurSubFin = 10000;

            Assert.AreEqual(80, dataLoan.sLtvR, "sLtvR");
            Assert.AreEqual(90, dataLoan.sCltvR, "sCltvR");

            dataLoan.TransformDataToPml(E_TransformToPmlT.FromScratch);

            dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
            Assert.AreEqual(100000, dataLoan.sHouseValPe, "sHouseValPe");
            Assert.AreEqual(10, dataLoan.sDownPmtPcPe, "sDownPmtPcPe");
            Assert.AreEqual(10000, dataLoan.sEquityPe, "sEquityPe");
            Assert.AreEqual(80, dataLoan.sLtvRPe, "sLtvRPe");
            Assert.AreEqual(80000, dataLoan.sLAmtCalcPe, "sLAmtCalcPe");
            Assert.AreEqual(10, dataLoan.sLtvROtherFinPe, "sLtvROtherFinPe");
            Assert.AreEqual(10000, dataLoan.sProOFinBalPe, "sProOFinBalPe");
            Assert.AreEqual(90, dataLoan.sCltvRPe, "sCltvRPe");
        }

        [Test]
        public void TestTransformInRefinanceFirstLienOnly()
        {
            CPageData dataLoan = CreatePageData();
            dataLoan.InitLoad();
            dataLoan.sLPurposeT = E_sLPurposeT.Refin;
            dataLoan.sApprVal = 100000;
            dataLoan.sLAmtLckd = true;
            dataLoan.sLAmtCalc = 80000;

            dataLoan.sIsOFinNew = false;
            dataLoan.sConcurSubFin = 0;

            Assert.AreEqual(80, dataLoan.sLtvR, "sLtvR");
            Assert.AreEqual(80, dataLoan.sCltvR, "sCltvR");

            dataLoan.TransformDataToPml(E_TransformToPmlT.FromScratch);

            dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
            Assert.AreEqual(100000, dataLoan.sHouseValPe, "sHouseValPe");
            Assert.AreEqual(20, dataLoan.sDownPmtPcPe, "sDownPmtPcPe");
            Assert.AreEqual(20000, dataLoan.sEquityPe, "sEquityPe");
            Assert.AreEqual(80, dataLoan.sLtvRPe, "sLtvRPe");
            Assert.AreEqual(80000, dataLoan.sLAmtCalcPe, "sLAmtCalcPe");
            Assert.AreEqual(0, dataLoan.sLtvROtherFinPe, "sLtvROtherFinPe");
            Assert.AreEqual(0, dataLoan.sProOFinBalPe, "sProOFinBalPe");
            Assert.AreEqual(80, dataLoan.sCltvRPe, "sCltvRPe");
        }

        [Test]
        public void TestTransformInRefinanceComboLoan()
        {
            CPageData dataLoan = CreatePageData();
            dataLoan.InitLoad();
            dataLoan.sLPurposeT = E_sLPurposeT.Refin;
            dataLoan.sApprVal = 100000;
            dataLoan.sLAmtLckd = true;
            dataLoan.sLAmtCalc = 80000;

            dataLoan.sIsOFinNew = true;
            dataLoan.sConcurSubFin = 10000;

            Assert.AreEqual(80, dataLoan.sLtvR, "sLtvR");
            Assert.AreEqual(90, dataLoan.sCltvR, "sCltvR");

            dataLoan.TransformDataToPml(E_TransformToPmlT.FromScratch);

            dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
            Assert.AreEqual(100000, dataLoan.sHouseValPe, "sHouseValPe");
            Assert.AreEqual(10, dataLoan.sDownPmtPcPe, "sDownPmtPcPe");
            Assert.AreEqual(10000, dataLoan.sEquityPe, "sEquityPe");
            Assert.AreEqual(80, dataLoan.sLtvRPe, "sLtvRPe");
            Assert.AreEqual(80000, dataLoan.sLAmtCalcPe, "sLAmtCalcPe");
            Assert.AreEqual(10, dataLoan.sLtvROtherFinPe, "sLtvROtherFinPe");
            Assert.AreEqual(10000, dataLoan.sProOFinBalPe, "sProOFinBalPe");
            Assert.AreEqual(90, dataLoan.sCltvRPe, "sCltvRPe");
        }

        [Test]
        public void TestTransformInRefinanceComboLoanCltvGreaterThan100()
        {
            CPageData dataLoan = CreatePageData();
            dataLoan.InitLoad();
            dataLoan.sLPurposeT = E_sLPurposeT.Refin;
            dataLoan.sApprVal = 100000;
            dataLoan.sLAmtLckd = true;
            dataLoan.sLAmtCalc = 80000;

            dataLoan.sIsOFinNew = true;
            dataLoan.sConcurSubFin = 25000;

            Assert.AreEqual(80, dataLoan.sLtvR, "sLtvR");
            Assert.AreEqual(105, dataLoan.sCltvR, "sCltvR");

            dataLoan.TransformDataToPml(E_TransformToPmlT.FromScratch);

            dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
            Assert.AreEqual(100000, dataLoan.sHouseValPe, "sHouseValPe");
            Assert.AreEqual(-5, dataLoan.sDownPmtPcPe, "sDownPmtPcPe");
            Assert.AreEqual(-5000, dataLoan.sEquityPe, "sEquityPe");
            Assert.AreEqual(80, dataLoan.sLtvRPe, "sLtvRPe");
            Assert.AreEqual(80000, dataLoan.sLAmtCalcPe, "sLAmtCalcPe");
            Assert.AreEqual(25, dataLoan.sLtvROtherFinPe, "sLtvROtherFinPe");
            Assert.AreEqual(25000, dataLoan.sProOFinBalPe, "sProOFinBalPe");
            Assert.AreEqual(105, dataLoan.sCltvRPe, "sCltvRPe");
        }
        [Test]
        public void TestTransformInRefinanceComboLoanCltvEqual100()
        {
            CPageData dataLoan = CreatePageData();
            dataLoan.InitLoad();
            dataLoan.sLPurposeT = E_sLPurposeT.Refin;
            dataLoan.sApprVal = 100000;
            dataLoan.sLAmtLckd = true;
            dataLoan.sLAmtCalc = 80000;

            dataLoan.sIsOFinNew = true;
            dataLoan.sConcurSubFin = 20000;

            Assert.AreEqual(80, dataLoan.sLtvR, "sLtvR");
            Assert.AreEqual(100, dataLoan.sCltvR, "sCltvR");

            dataLoan.TransformDataToPml(E_TransformToPmlT.FromScratch);

            dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
            Assert.AreEqual(100000, dataLoan.sHouseValPe, "sHouseValPe");
            Assert.AreEqual(0, dataLoan.sDownPmtPcPe, "sDownPmtPcPe");
            Assert.AreEqual(0, dataLoan.sEquityPe, "sEquityPe");
            Assert.AreEqual(80, dataLoan.sLtvRPe, "sLtvRPe");
            Assert.AreEqual(80000, dataLoan.sLAmtCalcPe, "sLAmtCalcPe");
            Assert.AreEqual(20, dataLoan.sLtvROtherFinPe, "sLtvROtherFinPe");
            Assert.AreEqual(20000, dataLoan.sProOFinBalPe, "sProOFinBalPe");
            Assert.AreEqual(100, dataLoan.sCltvRPe, "sCltvRPe");
        }

        private CPageData CreatePageData()
        {
            return FakePageData.Create();
        }
    }
}
