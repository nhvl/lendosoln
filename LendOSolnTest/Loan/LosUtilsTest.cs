﻿namespace LendOSolnTest.Loan
{
    using System;
    using DataAccess;
    using NUnit.Framework;

    [TestFixture]
    public class LosUtilsTest
    {
        [Test]
        public void GetTimeZonePartTest()
        {
            // daylight savings starts early march.  so feb1 should be PST, and april1 should be PDT.
            var april1Utc = DateTime.SpecifyKind(new DateTime(2017, 4, 1), DateTimeKind.Utc);
            var feb1Utc = DateTime.SpecifyKind(new DateTime(2017, 2, 1), DateTimeKind.Utc);

            Assert.That(Tools.GetTimeZonePart(april1Utc, Tools.PacificStandardVariantOffset) == "PDT");
            Assert.That(Tools.GetTimeZonePart(feb1Utc, Tools.PacificStandardVariantOffset) == "PST");

            Assert.That(Tools.GetTimeZonePart(april1Utc, TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time")) == "EDT");
            Assert.That(Tools.GetTimeZonePart(feb1Utc, TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time")) == "EST");
        }

        [Test]
        public void GetZonedTimeFromUtcTimeAndCurrentTimeTest()
        {
            // daylight savings starts early march.  so feb1 should be PST, and april1 should be PDT.
            var april1Utc = DateTime.SpecifyKind(new DateTime(2017, 4, 1), DateTimeKind.Utc);
            var feb1Utc = DateTime.SpecifyKind(new DateTime(2017, 2, 1), DateTimeKind.Utc);

            var pacificZone = Tools.PacificStandardVariantOffset;

            var april1UtcInPST = Tools.GetZonedTimeFromUtcTimeAndCurrentTime(feb1Utc, pacificZone, april1Utc);
            var feb1UtcInPDT = Tools.GetZonedTimeFromUtcTimeAndCurrentTime(april1Utc, pacificZone, feb1Utc);

            Assert.That(april1UtcInPST.Kind == DateTimeKind.Unspecified);
            Assert.That(feb1UtcInPDT.Kind == DateTimeKind.Unspecified);

            Assert.That(april1UtcInPST.Hour == 16); // subtract 8 for PST
            Assert.That(feb1UtcInPDT.Hour == 17);   // subtract 7 for PDT.
        }

        [Test]
        public void GetZonedTimeFromUtcTimeAndViewerTimeZone()
        {
            // daylight savings starts early march.  so feb1 should be PST, and april1 should be PDT.
            var april1Utc = DateTime.SpecifyKind(new DateTime(2017, 4, 1), DateTimeKind.Utc);
            var feb1Utc = DateTime.SpecifyKind(new DateTime(2017, 2, 1), DateTimeKind.Utc);

            var pacificZone = Tools.PacificStandardVariantOffset;

            var april1UtcInPT = Tools.GetZonedTimeFromUtcTimeAndViewerTimeZone(april1Utc, pacificZone);
            var feb1UtcInPT = Tools.GetZonedTimeFromUtcTimeAndViewerTimeZone(feb1Utc, pacificZone);

            Assert.That(april1UtcInPT.Kind == DateTimeKind.Unspecified);
            Assert.That(feb1UtcInPT.Kind == DateTimeKind.Unspecified);

            Assert.That(april1UtcInPT.Hour == 17); // subtract 7 for PDT
            Assert.That(feb1UtcInPT.Hour == 16);   // subtract 8 for PST.
        }
    }
}
