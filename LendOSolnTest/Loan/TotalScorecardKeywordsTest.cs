﻿using DataAccess;
using LendersOffice.Constants;
using LendOSolnTest.Common;
using LendOSolnTest.Fakes;
using NUnit.Framework;

namespace LendOSolnTest.Loan
{
    [TestFixture]
    public class TotalScorecardKeywordsTest
    {
        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            LoginTools.LoginAs(TestAccountType.LoTest001);
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            FakePageDataUtilities.Instance.Reset();
        }

        private CPageData CreateDataObject()
        {
            return FakePageData.Create();
        }

        [Test]
        public void SingleBorrowerTest()
        {
            #region Setup scenario
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            CAppData dataApp = dataLoan.GetAppData(0);

            IAssetCollection aAssetCollection = dataApp.aAssetCollection;

            var cashDeposit = aAssetCollection.GetCashDeposit1(true);
            cashDeposit.Val_rep = "$10,000.01";
            cashDeposit.Update();

            cashDeposit = aAssetCollection.GetCashDeposit2(true);
            cashDeposit.Val_rep = "$1,000.02";
            cashDeposit.Update();

            var retirement = aAssetCollection.GetRetirement(true);
            retirement.Val_rep = "$3,000.03";
            retirement.Update();

            var lifeInsurance = aAssetCollection.GetLifeInsurance(true);
            lifeInsurance.Val_rep = "$5,000.04";
            lifeInsurance.Update();

            var business = aAssetCollection.GetBusinessWorth(true);
            business.Val_rep = "$7,000.05";
            business.Update();

            var list = new[] {
                new { AssetT = E_AssetRegularT.Auto, Val = "$10,900.10"},
                new { AssetT = E_AssetRegularT.Auto, Val = "$20,300.11"},

                new { AssetT = E_AssetRegularT.Bonds, Val = "$1,900.10"},
                new { AssetT = E_AssetRegularT.Bonds, Val = "$2,300.11"},

                new { AssetT = E_AssetRegularT.Checking, Val = "$900.06"},
                new { AssetT = E_AssetRegularT.Checking, Val = "$1,100.07"},

                new { AssetT = E_AssetRegularT.GiftFunds, Val = "$901.06"},
                new { AssetT = E_AssetRegularT.GiftFunds, Val = "$1,101.07"},

                new { AssetT = E_AssetRegularT.GiftEquity, Val = "$1,301.08"},
                new { AssetT = E_AssetRegularT.GiftEquity, Val = "$1,701.09"},

                new { AssetT = E_AssetRegularT.Savings, Val = "$1,300.08"},
                new { AssetT = E_AssetRegularT.Savings, Val = "$1,700.09"},

                new { AssetT = E_AssetRegularT.Stocks, Val = "$1,901.10"},
                new { AssetT = E_AssetRegularT.Stocks, Val = "$2,301.11"},

                new { AssetT = E_AssetRegularT.PendingNetSaleProceedsFromRealEstateAssets, Val = "$1,903.10"},
                new { AssetT = E_AssetRegularT.PendingNetSaleProceedsFromRealEstateAssets, Val = "$2,303.11"},

                new { AssetT = E_AssetRegularT.OtherIlliquidAsset, Val = "$1,305.08"},
                new { AssetT = E_AssetRegularT.OtherIlliquidAsset, Val = "$1,705.09"},

                new { AssetT = E_AssetRegularT.OtherLiquidAsset, Val = "$1,307.08"},
                new { AssetT = E_AssetRegularT.OtherLiquidAsset, Val = "$1,707.09"},
            };

            foreach (var o in list)
            {
                var asset = aAssetCollection.AddRegularRecord();
                asset.AssetT = o.AssetT;
                asset.Val_rep = o.Val;
                asset.Update();
                
            }

            dataLoan.Save();


            #endregion

            dataLoan = CreateDataObject();
            dataLoan.InitLoad();

            dataApp = dataLoan.GetAppData(0);

            Assert.AreEqual(dataLoan.sAssetTotalX(""), dataLoan.sAssetTotalX("any"), "any");
            Assert.AreEqual(dataLoan.sAssetTotalX("any"), dataApp.aAsstValTot);

            Assert.AreEqual(31200.21M, dataLoan.sAssetTotalX("auto"), "auto");
            Assert.AreEqual(4200.21M, dataLoan.sAssetTotalX("bonds"), "bonds");
            Assert.AreEqual(2000.13M, dataLoan.sAssetTotalX("checking"), "checking");
            Assert.AreEqual(2002.13M, dataLoan.sAssetTotalX("giftfunds"), "giftfunds");
            Assert.AreEqual(5000.3M, dataLoan.sAssetTotalX("checking&savings"), "checking&savings");
        }
    }
}
