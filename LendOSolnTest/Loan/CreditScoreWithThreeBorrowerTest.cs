﻿using DataAccess;
using LendOSolnTest.Common;
using LendOSolnTest.Fakes;
using NUnit.Framework;

namespace LendOSolnTest.Loan
{
    /// <summary>
    /// Set up test to verify sCreditScoreType1, sCreditScoreType2, sCreditScoreType2Soft, sCreditScoreType3.
    /// This test is only use one set of applicant.
    /// </summary>
    [TestFixture]
    public class CreditScoreWithThreeBorrowerTest
    {
        [TestFixtureSetUp]
        public void Setup()
        {
            LoginTools.LoginAs(TestAccountType.LoTest001);
        }

        [Test]
        public void TestInLendersOfficeMode()
        {

            var testCases = new[] {
                new {
                    name = "Test001",
                    dataApp = new[] {
                        new {
                            aBSsn="111-11-1111", aBBaseI="", aBExperian="600", aBTransUnion="700", aBEquifax="800",
                            aCSsn="222-22-2222", aCBaseI="", aCExperian="",    aCTransUnion="",    aCEquifax=""
                        },
                        new {
                            aBSsn="333-33-3333", aBBaseI="", aBExperian="620", aBTransUnion="720", aBEquifax="820",
                            aCSsn="444-44-4444", aCBaseI="", aCExperian="",    aCTransUnion="",    aCEquifax=""
                        },
                        new {
                            aBSsn="555-55-5555", aBBaseI="", aBExperian="640", aBTransUnion="740", aBEquifax="840",
                            aCSsn="666-66-6666", aCBaseI="", aCExperian="",    aCTransUnion="",    aCEquifax=""
                        },

                    },
                    sCreditScoreType1 = "700", sCreditScoreType2 = "", sCreditScoreType2Soft="700", sCreditScoreType3="700"
                },

                new {
                    name = "Test002",
                    dataApp = new[] {
                        new {
                            aBSsn="111-11-1111", aBBaseI="", aBExperian="600", aBTransUnion="700", aBEquifax="800",
                            aCSsn="222-22-2222", aCBaseI="", aCExperian="610",    aCTransUnion="710",    aCEquifax=""
                        },
                        new {
                            aBSsn="333-33-3333", aBBaseI="", aBExperian="620", aBTransUnion="720", aBEquifax="820",
                            aCSsn="444-44-4444", aCBaseI="", aCExperian="",    aCTransUnion="",    aCEquifax=""
                        },
                        new {
                            aBSsn="555-55-5555", aBBaseI="", aBExperian="640", aBTransUnion="740", aBEquifax="840",
                            aCSsn="666-66-6666", aCBaseI="", aCExperian="",    aCTransUnion="",    aCEquifax=""
                        },

                    },
                    sCreditScoreType1 = "700", sCreditScoreType2 = "", sCreditScoreType2Soft="610", sCreditScoreType3="700"
                },

                new {
                    name = "Test003",
                    dataApp = new[] {
                        new {
                            aBSsn="111-11-1111", aBBaseI="", aBExperian="600", aBTransUnion="700", aBEquifax="800",
                            aCSsn="222-22-2222", aCBaseI="", aCExperian="610",    aCTransUnion="710",    aCEquifax="810"
                        },
                        new {
                            aBSsn="333-33-3333", aBBaseI="", aBExperian="620", aBTransUnion="720", aBEquifax="820",
                            aCSsn="444-44-4444", aCBaseI="", aCExperian="630",    aCTransUnion="730",    aCEquifax="830"
                        },
                        new {
                            aBSsn="555-55-5555", aBBaseI="", aBExperian="640", aBTransUnion="740", aBEquifax="840",
                            aCSsn="666-66-6666", aCBaseI="", aCExperian="650",    aCTransUnion="750",    aCEquifax="850"
                        },

                    },
                    sCreditScoreType1 = "700", sCreditScoreType2 = "700", sCreditScoreType2Soft="700", sCreditScoreType3="700"
                },

                new {
                    name = "Test004",
                    dataApp = new[] {
                        new {
                            aBSsn="111-11-1111", aBBaseI="$100", aBExperian="600", aBTransUnion="700", aBEquifax="800",
                            aCSsn="222-22-2222", aCBaseI="$110", aCExperian="610",    aCTransUnion="710",    aCEquifax="810"
                        },
                        new {
                            aBSsn="333-33-3333", aBBaseI="", aBExperian="620", aBTransUnion="720", aBEquifax="820",
                            aCSsn="444-44-4444", aCBaseI="", aCExperian="630",    aCTransUnion="730",    aCEquifax="830"
                        },
                        new {
                            aBSsn="555-55-5555", aBBaseI="$300", aBExperian="640", aBTransUnion="740", aBEquifax="840",
                            aCSsn="666-66-6666", aCBaseI="$310", aCExperian="650",    aCTransUnion="750",    aCEquifax="850"
                        },

                    },
                    sCreditScoreType1 = "750", sCreditScoreType2 = "700", sCreditScoreType2Soft="700", sCreditScoreType3="700"
                },
            };

            #region Verify Test Cases
            CPageData dataLoan = CreatePageData();
            dataLoan.InitLoad();

            foreach (var o in testCases)
            {
                for (int i = 0; i < dataLoan.nApps; i++)
                {
                    CAppData dataApp = dataLoan.GetAppData(i);
                    dataApp.aBSsn = o.dataApp[i].aBSsn;
                    dataApp.aBBaseI_rep = o.dataApp[i].aBBaseI;
                    dataApp.aBExperianScore_rep = o.dataApp[i].aBExperian;
                    dataApp.aBTransUnionScore_rep = o.dataApp[i].aBTransUnion;
                    dataApp.aBEquifaxScore_rep = o.dataApp[i].aBEquifax;

                    dataApp.aCSsn = o.dataApp[i].aCSsn;
                    dataApp.aCBaseI_rep = o.dataApp[i].aCBaseI;
                    dataApp.aCExperianScore_rep = o.dataApp[i].aCExperian;
                    dataApp.aCTransUnionScore_rep = o.dataApp[i].aCTransUnion;
                    dataApp.aCEquifaxScore_rep = o.dataApp[i].aCEquifax;
                }

                Assert.AreEqual(o.sCreditScoreType1, dataLoan.sCreditScoreType1_rep, "sCreditScoreType1_rep::" + o.name);
                Assert.AreEqual(o.sCreditScoreType2, dataLoan.sCreditScoreType2_rep, "sCreditScoreType2_rep::" + o.name);
                Assert.AreEqual(o.sCreditScoreType2Soft, dataLoan.sCreditScoreType2Soft_rep, "sCreditScoreType2Soft_rep::" + o.name);
                Assert.AreEqual(o.sCreditScoreType3, dataLoan.sCreditScoreType3_rep, "sCreditScoreType3_rep::" + o.name);
            }
            #endregion

        }

        [Test]
        public void TestInPricingMode()
        {

            var testCases = new[] {
                new {
                    name = "Test001",
                    dataApp = new[] {
                        new {
                            aBSsn="111-11-1111", aBBaseI="", aBExperian="600", aBTransUnion="700", aBEquifax="800",
                            aCSsn="222-22-2222", aCBaseI="", aCExperian="",    aCTransUnion="",    aCEquifax=""
                        },
                        new {
                            aBSsn="333-33-3333", aBBaseI="", aBExperian="620", aBTransUnion="720", aBEquifax="820",
                            aCSsn="444-44-4444", aCBaseI="", aCExperian="",    aCTransUnion="",    aCEquifax=""
                        },
                        new {
                            aBSsn="555-55-5555", aBBaseI="", aBExperian="640", aBTransUnion="740", aBEquifax="840",
                            aCSsn="666-66-6666", aCBaseI="", aCExperian="",    aCTransUnion="",    aCEquifax=""
                        },

                    },
                    sCreditScoreType1 = "700", sCreditScoreType2 = "0", sCreditScoreType2Soft="700", sCreditScoreType3="700"
                },

                new {
                    name = "Test002",
                    dataApp = new[] {
                        new {
                            aBSsn="111-11-1111", aBBaseI="", aBExperian="600", aBTransUnion="700", aBEquifax="800",
                            aCSsn="222-22-2222", aCBaseI="", aCExperian="610",    aCTransUnion="710",    aCEquifax=""
                        },
                        new {
                            aBSsn="333-33-3333", aBBaseI="", aBExperian="620", aBTransUnion="720", aBEquifax="820",
                            aCSsn="444-44-4444", aCBaseI="", aCExperian="",    aCTransUnion="",    aCEquifax=""
                        },
                        new {
                            aBSsn="555-55-5555", aBBaseI="", aBExperian="640", aBTransUnion="740", aBEquifax="840",
                            aCSsn="666-66-6666", aCBaseI="", aCExperian="",    aCTransUnion="",    aCEquifax=""
                        },

                    },
                    sCreditScoreType1 = "700", sCreditScoreType2 = "0", sCreditScoreType2Soft="610", sCreditScoreType3="700"
                },

                new {
                    name = "Test003",
                    dataApp = new[] {
                        new {
                            aBSsn="111-11-1111", aBBaseI="", aBExperian="600", aBTransUnion="700", aBEquifax="800",
                            aCSsn="222-22-2222", aCBaseI="", aCExperian="610",    aCTransUnion="710",    aCEquifax="810"
                        },
                        new {
                            aBSsn="333-33-3333", aBBaseI="", aBExperian="620", aBTransUnion="720", aBEquifax="820",
                            aCSsn="444-44-4444", aCBaseI="", aCExperian="630",    aCTransUnion="730",    aCEquifax="830"
                        },
                        new {
                            aBSsn="555-55-5555", aBBaseI="", aBExperian="640", aBTransUnion="740", aBEquifax="840",
                            aCSsn="666-66-6666", aCBaseI="", aCExperian="650",    aCTransUnion="750",    aCEquifax="850"
                        },

                    },
                    sCreditScoreType1 = "700", sCreditScoreType2 = "700", sCreditScoreType2Soft="700", sCreditScoreType3="700"
                },

                new {
                    name = "Test004",
                    dataApp = new[] {
                        new {
                            aBSsn="111-11-1111", aBBaseI="$100", aBExperian="600", aBTransUnion="700", aBEquifax="800",
                            aCSsn="222-22-2222", aCBaseI="$110", aCExperian="610",    aCTransUnion="710",    aCEquifax="810"
                        },
                        new {
                            aBSsn="333-33-3333", aBBaseI="", aBExperian="620", aBTransUnion="720", aBEquifax="820",
                            aCSsn="444-44-4444", aCBaseI="", aCExperian="630",    aCTransUnion="730",    aCEquifax="830"
                        },
                        new {
                            aBSsn="555-55-5555", aBBaseI="$300", aBExperian="640", aBTransUnion="740", aBEquifax="840",
                            aCSsn="666-66-6666", aCBaseI="$310", aCExperian="650",    aCTransUnion="750",    aCEquifax="850"
                        },

                    },
                    sCreditScoreType1 = "700", sCreditScoreType2 = "700", sCreditScoreType2Soft="700", sCreditScoreType3="700"
                },
            };

            #region Verify Test Cases
            CPageData dataLoan = CreatePageData();
            dataLoan.InitLoad();
            dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;

            foreach (var o in testCases)
            {
                for (int i = 0; i < dataLoan.nApps; i++)
                {
                    CAppData dataApp = dataLoan.GetAppData(i);
                    dataApp.aBSsn = o.dataApp[i].aBSsn;
                    dataApp.aBBaseI_rep = o.dataApp[i].aBBaseI;
                    dataApp.aBExperianScorePe_rep = o.dataApp[i].aBExperian;
                    dataApp.aBTransUnionScorePe_rep = o.dataApp[i].aBTransUnion;
                    dataApp.aBEquifaxScorePe_rep = o.dataApp[i].aBEquifax;

                    dataApp.aCSsn = o.dataApp[i].aCSsn;
                    dataApp.aCBaseI_rep = o.dataApp[i].aCBaseI;
                    dataApp.aCExperianScorePe_rep = o.dataApp[i].aCExperian;
                    dataApp.aCTransUnionScorePe_rep = o.dataApp[i].aCTransUnion;
                    dataApp.aCEquifaxScorePe_rep = o.dataApp[i].aCEquifax;
                }

                Assert.AreEqual(o.sCreditScoreType1, dataLoan.sCreditScoreType1_rep, "sCreditScoreType1_rep::" + o.name);
                Assert.AreEqual(o.sCreditScoreType2, dataLoan.sCreditScoreType2_rep, "sCreditScoreType2_rep::" + o.name);
                Assert.AreEqual(o.sCreditScoreType2Soft, dataLoan.sCreditScoreType2Soft_rep, "sCreditScoreType2Soft_rep::" + o.name);
                Assert.AreEqual(o.sCreditScoreType3, dataLoan.sCreditScoreType3_rep, "sCreditScoreType3_rep::" + o.name);
            }
            #endregion

        }
        private CPageData CreatePageData()
        {
            return FakePageData.Create(appCount: 3);
        }
    }
}
