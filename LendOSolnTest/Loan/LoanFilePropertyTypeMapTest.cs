﻿using System.Linq;
using DataAccess;
using LendOSolnTest.Common;
using LendOSolnTest.Fakes;
using NUnit.Framework;
namespace LendOSolnTest.Loan
{
    [TestFixture]
    public class LoanFilePropertyTypeMapTest
    {
        [TestFixtureSetUp]
        public void Setup()
        {
            LoginTools.LoginAs(TestAccountType.LoTest001);
        }

        [Test]
        public void MapTest()
        {
            var map = new[]
                 {
                    new { sFannieSpt = E_sFannieSpT.Detached, sGseSpt = new[] { E_sGseSpT.Detached, E_sGseSpT.Modular } },
                    new { sFannieSpt = E_sFannieSpT.Attached, sGseSpt = new[] { E_sGseSpT.Attached } },
                    new { sFannieSpt = E_sFannieSpT.LeaveBlank, sGseSpt = new[] { E_sGseSpT.LeaveBlank } },
                    new { sFannieSpt = E_sFannieSpT.Condo, sGseSpt = new[] { E_sGseSpT.Condominium } },
                    new { sFannieSpt = E_sFannieSpT.PUD, sGseSpt = new[] { E_sGseSpT.PUD } },
                    new { sFannieSpt = E_sFannieSpT.CoOp, sGseSpt = new[] { E_sGseSpT.Cooperative } },
                    new { sFannieSpt = E_sFannieSpT.HighRiseCondo, sGseSpt = new[] { E_sGseSpT.HighRiseCondominium } },
                    new { sFannieSpt = E_sFannieSpT.DetachedCondo, sGseSpt = new[] { E_sGseSpT.DetachedCondominium } },
                    new { sFannieSpt = E_sFannieSpT.ManufacturedCondoPudCoop, sGseSpt = new[] { E_sGseSpT.ManufacturedHomeCondominium } },
                    new { sFannieSpt = E_sFannieSpT.Manufactured, sGseSpt = new[] { E_sGseSpT.ManufacturedHousing, E_sGseSpT.ManufacturedHomeMultiwide, E_sGseSpT.ManufacturedHousingSingleWide } },
                };

            CPageData pageData = CreateCPageData();
            pageData.InitLoad();
            foreach (var mapTest in map)
            {
                pageData.sFannieSpT = mapTest.sFannieSpt;
                Assert.That(mapTest.sGseSpt.Contains(pageData.sGseSpT), Is.True);
            }
        }

        protected CPageData CreateCPageData()
        {
            return FakePageData.Create();
        }
    }
}
