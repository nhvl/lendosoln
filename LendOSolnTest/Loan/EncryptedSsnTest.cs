﻿namespace LendOSolnTest.Loan
{
    using System;
    using Common;
    using DataAccess;
    using LendersOffice.Constants;
    using LendersOffice.Security;
    using NUnit.Framework;

    [TestFixture, Explicit("This test is long running and has been verified locally.")]
    public class EncryptedSsnTest
    {
        private const string Ssn1 = "123-45-6789";
        private const string Ssn2 = "987-65-4321";
        private const string Ein1 = "12-3456789";
        private const string Ein2 = "98-7654321";

        private AbstractUserPrincipal principal;
        private Guid loanId;

        [TestFixtureSetUp]
        public void Setup()
        {
            this.principal = LoginTools.LoginAs(TestAccountType.LoTest001);

            var creator = CLoanFileCreator.GetCreator(principal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
            this.loanId = creator.CreateBlankLoanFile();
        }

        [TestFixtureTearDown]
        public void TearDown()
        {
            Tools.SystemDeclareLoanFileInvalid(this.principal, this.loanId, sendNotifications: false, generateLinkLoanMsgEvent: false);
        }

        [Test]
        public void Verify_aBSsn()
        {
            var loan = CPageData.CreateUsingSmartDependency(this.loanId, typeof(EncryptedSsnTest));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);
            
            var app = loan.GetAppData(0);
            app.aBSsn = Ssn1;

            loan.Save();

            loan = CPageData.CreateUsingSmartDependency(this.loanId, typeof(EncryptedSsnTest));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            app = loan.GetAppData(0);
            Assert.AreEqual(Ssn1, app.aBSsn);

            app.aBSsn = Ssn2;

            loan.Save();

            loan = CPageData.CreateUsingSmartDependency(this.loanId, typeof(EncryptedSsnTest));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            app = loan.GetAppData(0);
            Assert.AreEqual(Ssn2, app.aBSsn);
        }

        [Test]
        public void Verify_aCSsn()
        {
            var loan = CPageData.CreateUsingSmartDependency(this.loanId, typeof(EncryptedSsnTest));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            var app = loan.GetAppData(0);
            app.aCSsn = Ssn1;

            loan.Save();

            loan = CPageData.CreateUsingSmartDependency(this.loanId, typeof(EncryptedSsnTest));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            app = loan.GetAppData(0);
            Assert.AreEqual(Ssn1, app.aCSsn);

            app.aCSsn = Ssn2;

            loan.Save();

            loan = CPageData.CreateUsingSmartDependency(this.loanId, typeof(EncryptedSsnTest));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            app = loan.GetAppData(0);
            Assert.AreEqual(Ssn2, app.aCSsn);
        }

        [Test]
        public void Verify_aB4506TSsnTinEin()
        {
            var loan = CPageData.CreateUsingSmartDependency(this.loanId, typeof(EncryptedSsnTest));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            var app = loan.GetAppData(0);
            app.aB4506TSsnTinEin = Ssn1;

            loan.Save();

            loan = CPageData.CreateUsingSmartDependency(this.loanId, typeof(EncryptedSsnTest));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            app = loan.GetAppData(0);
            Assert.AreEqual(Ssn1, app.aB4506TSsnTinEin);

            app.aB4506TSsnTinEin = Ssn2;

            loan.Save();

            loan = CPageData.CreateUsingSmartDependency(this.loanId, typeof(EncryptedSsnTest));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            app = loan.GetAppData(0);
            Assert.AreEqual(Ssn2, app.aB4506TSsnTinEin);
        }

        [Test]
        public void Verify_aC4506TSsnTinEin()
        {
            var loan = CPageData.CreateUsingSmartDependency(this.loanId, typeof(EncryptedSsnTest));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            var app = loan.GetAppData(0);
            app.aC4506TSsnTinEin = Ssn1;

            loan.Save();

            loan = CPageData.CreateUsingSmartDependency(this.loanId, typeof(EncryptedSsnTest));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            app = loan.GetAppData(0);
            Assert.AreEqual(Ssn1, app.aC4506TSsnTinEin);

            app.aC4506TSsnTinEin = Ssn2;

            loan.Save();

            loan = CPageData.CreateUsingSmartDependency(this.loanId, typeof(EncryptedSsnTest));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            app = loan.GetAppData(0);
            Assert.AreEqual(Ssn2, app.aC4506TSsnTinEin);
        }

        [Test]
        public void Verify_aVaServ1Ssn()
        {
            var loan = CPageData.CreateUsingSmartDependency(this.loanId, typeof(EncryptedSsnTest));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            var app = loan.GetAppData(0);
            app.aVaServ1Ssn = Ssn1;

            loan.Save();

            loan = CPageData.CreateUsingSmartDependency(this.loanId, typeof(EncryptedSsnTest));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            app = loan.GetAppData(0);
            Assert.AreEqual(Ssn1, app.aVaServ1Ssn);

            app.aVaServ1Ssn = Ssn2;

            loan.Save();

            loan = CPageData.CreateUsingSmartDependency(this.loanId, typeof(EncryptedSsnTest));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            app = loan.GetAppData(0);
            Assert.AreEqual(Ssn2, app.aVaServ1Ssn);
        }

        [Test]
        public void Verify_aVaServ2Ssn()
        {
            var loan = CPageData.CreateUsingSmartDependency(this.loanId, typeof(EncryptedSsnTest));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            var app = loan.GetAppData(0);
            app.aVaServ2Ssn = Ssn1;

            loan.Save();

            loan = CPageData.CreateUsingSmartDependency(this.loanId, typeof(EncryptedSsnTest));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            app = loan.GetAppData(0);
            Assert.AreEqual(Ssn1, app.aVaServ2Ssn);

            app.aVaServ2Ssn = Ssn2;

            loan.Save();

            loan = CPageData.CreateUsingSmartDependency(this.loanId, typeof(EncryptedSsnTest));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            app = loan.GetAppData(0);
            Assert.AreEqual(Ssn2, app.aVaServ2Ssn);
        }

        [Test]
        public void Verify_aVaServ3Ssn()
        {
            var loan = CPageData.CreateUsingSmartDependency(this.loanId, typeof(EncryptedSsnTest));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            var app = loan.GetAppData(0);
            app.aVaServ3Ssn = Ssn1;

            loan.Save();

            loan = CPageData.CreateUsingSmartDependency(this.loanId, typeof(EncryptedSsnTest));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            app = loan.GetAppData(0);
            Assert.AreEqual(Ssn1, app.aVaServ3Ssn);

            app.aVaServ3Ssn = Ssn2;

            loan.Save();

            loan = CPageData.CreateUsingSmartDependency(this.loanId, typeof(EncryptedSsnTest));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            app = loan.GetAppData(0);
            Assert.AreEqual(Ssn2, app.aVaServ3Ssn);
        }

        [Test]
        public void Verify_aVaServ4Ssn()
        {
            var loan = CPageData.CreateUsingSmartDependency(this.loanId, typeof(EncryptedSsnTest));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            var app = loan.GetAppData(0);
            app.aVaServ4Ssn = Ssn1;

            loan.Save();

            loan = CPageData.CreateUsingSmartDependency(this.loanId, typeof(EncryptedSsnTest));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            app = loan.GetAppData(0);
            Assert.AreEqual(Ssn1, app.aVaServ4Ssn);

            app.aVaServ4Ssn = Ssn2;

            loan.Save();

            loan = CPageData.CreateUsingSmartDependency(this.loanId, typeof(EncryptedSsnTest));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            app = loan.GetAppData(0);
            Assert.AreEqual(Ssn2, app.aVaServ4Ssn);
        }

        [Test]
        public void Verify_sCombinedBorSsn()
        {
            var loan = CPageData.CreateUsingSmartDependency(this.loanId, typeof(EncryptedSsnTest));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            loan.sCombinedBorInfoLckd = true;
            loan.sCombinedBorSsn = Ssn1;
            loan.Save();

            loan = CPageData.CreateUsingSmartDependency(this.loanId, typeof(EncryptedSsnTest));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            Assert.AreEqual(Ssn1, loan.sCombinedBorSsn);
            
            loan.sCombinedBorSsn = Ssn2;
            loan.Save();

            loan = CPageData.CreateUsingSmartDependency(this.loanId, typeof(EncryptedSsnTest));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            Assert.AreEqual(Ssn2, loan.sCombinedBorSsn);
        }

        [Test]
        public void Verify_sCombinedCoborSsn()
        {
            var loan = CPageData.CreateUsingSmartDependency(this.loanId, typeof(EncryptedSsnTest));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            loan.sCombinedBorInfoLckd = true;
            loan.sCombinedCoborSsn = Ssn1;
            loan.Save();

            loan = CPageData.CreateUsingSmartDependency(this.loanId, typeof(EncryptedSsnTest));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            Assert.AreEqual(Ssn1, loan.sCombinedCoborSsn);
            
            loan.sCombinedCoborSsn = Ssn2;
            loan.Save();

            loan = CPageData.CreateUsingSmartDependency(this.loanId, typeof(EncryptedSsnTest));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            Assert.AreEqual(Ssn2, loan.sCombinedCoborSsn);
        }

        [Test]
        public void Verify_sTax1098PayerSsn()
        {
            var loan = CPageData.CreateUsingSmartDependency(this.loanId, typeof(EncryptedSsnTest));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            loan.sTax1098PayerSsnLckd = true;
            loan.sTax1098PayerSsn = Ssn1;
            loan.Save();

            loan = CPageData.CreateUsingSmartDependency(this.loanId, typeof(EncryptedSsnTest));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            Assert.AreEqual(Ssn1, loan.sTax1098PayerSsn);
            
            loan.sTax1098PayerSsn = Ssn2;
            loan.Save();

            loan = CPageData.CreateUsingSmartDependency(this.loanId, typeof(EncryptedSsnTest));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            Assert.AreEqual(Ssn2, loan.sTax1098PayerSsn);
        }

        [Test]
        public void Verify_sFHASponsoredOriginatorEIN()
        {
            var loan = CPageData.CreateUsingSmartDependency(this.loanId, typeof(EncryptedSsnTest));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);
            
            loan.sFhaSponsoredOriginatorEinLckd = true;
            loan.sFHASponsoredOriginatorEIN = Ein1;
            loan.Save();

            loan = CPageData.CreateUsingSmartDependency(this.loanId, typeof(EncryptedSsnTest));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            Assert.AreEqual(Ein1, loan.sFHASponsoredOriginatorEIN);

            loan.sFHASponsoredOriginatorEIN = Ein2;
            loan.Save();

            loan = CPageData.CreateUsingSmartDependency(this.loanId, typeof(EncryptedSsnTest));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            Assert.AreEqual(Ein2, loan.sFHASponsoredOriginatorEIN);
        }
    }
}
