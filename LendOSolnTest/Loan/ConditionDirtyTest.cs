﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using LendersOffice.Security;
using LendOSolnTest.Common;
using DataAccess;
using LendersOffice.Constants;
using LendersOffice.Reminders;

namespace LendOSolnTest.Loan
{
    [TestFixture]
    public class ConditionDirtyTest 
    {


        private Guid m_sLId;
        private AbstractUserPrincipal UserPrincipal = LoginTools.LoginAs(TestAccountType.Manager);

        private CPageData CreatePageData() 
        {
            return CPageData.CreateUsingSmartDependency(m_sLId, typeof(ConditionDirtyTest));
        }


        [SetUp]
        public void Setup()
        {
            CLoanFileCreator creator = CLoanFileCreator.GetCreator(UserPrincipal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
            m_sLId = creator.CreateBlankLoanFile();

            //Add some conditions to test out with
            LoanConditionSetObsolete ds = GetConditionSet();
            ds.AddUniqueCondition("Condition1", "Descrition1");
            ds.AddUniqueCondition("Condition2", "Description2");
            ds.AddUniqueCondition("Condition3", "Description3");
            ds.Save(UserPrincipal.BrokerId);

        }

        [TearDown]
        public void TearDown()
        {
            if (m_sLId != Guid.Empty)
            {
                Tools.DeclareLoanFileInvalid(UserPrincipal, m_sLId, false, false);
            }
        }


        [Test]
        public void OrderConditionsAndCheckDirtyBit() 
        {
            CPageData dataLoan = CreatePageData();
            dataLoan.InitLoad();
            DateTime conditionDate = dataLoan.sConditionLastModifiedD;

            LoanConditionSetObsolete ds = GetConditionSet();
            Random r = new Random();
            int order = r.Next(100, 2000);
            foreach (CLoanConditionObsolete condition in ds)
            {
                Assert.That(condition.CondOrderedPosition, Is.Not.EqualTo(order), "Random Roder Failed"); //sanity check 
                condition.CondOrderedPosition = order;
                order++;
            }
            ds.Save(UserPrincipal.BrokerId);

            dataLoan = CreatePageData();
            dataLoan.InitLoad();
            Assert.That(dataLoan.sConditionLastModifiedD, Is.GreaterThan(conditionDate)); 
    
        }


        [Test]
        public void MarkConditionAsDoneShouldNotTriggerDirty()
        {
            CPageData dataLoan = CreatePageData();
            dataLoan.InitLoad();
            DateTime conditionDate = dataLoan.sConditionLastModifiedD;


            LoanConditionSetObsolete ds = GetConditionSet();

            Assert.That(ds.Count, Is.GreaterThan(0));

            CLoanConditionObsolete cl = (CLoanConditionObsolete)ds[0];
            cl.CondStatus = E_CondStatus.Done;
            cl.CondCompletedByUserNm = UserPrincipal.DisplayName;
            cl.CondStatusDate = DateTime.Now;
            ds.Save(UserPrincipal.BrokerId);

            dataLoan = CreatePageData();
            dataLoan.InitLoad();
            Assert.That(dataLoan.sConditionLastModifiedD, Is.EqualTo(conditionDate));
        }

        [Test]
        public void SoftDeleteConditionShouldTriggerDirty()
        {
            CPageData dataLoan = CreatePageData();
            dataLoan.InitLoad();
            DateTime conditionDate = dataLoan.sConditionLastModifiedD;

            LoanConditionSetObsolete ds = GetConditionSet();
            Assert.That(ds.Count, Is.GreaterThan(0));
            CLoanConditionObsolete cl = (CLoanConditionObsolete)ds[0];
            cl.CondDeleteByUserNm = UserPrincipal.DisplayName;
            cl.CondDeleteD = DateTime.Now;
            ds.Save(UserPrincipal.BrokerId);

            dataLoan = CreatePageData();
            dataLoan.InitLoad();
            Assert.That(dataLoan.sConditionLastModifiedD, Is.GreaterThan(conditionDate));
    

        }



        private LoanConditionSetObsolete GetConditionSet()
        {
            LoanConditionSetObsolete ds = new LoanConditionSetObsolete();
            ds.RetrieveAll(m_sLId, true);
            return ds;
        }


    }
}
