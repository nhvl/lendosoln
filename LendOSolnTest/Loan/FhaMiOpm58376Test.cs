﻿using System;
using DataAccess;
using LendersOffice.Constants;
using LendOSolnTest.Common;
using LendOSolnTest.Fakes;
using NUnit.Framework;

namespace LendOSolnTest.Loan
{
    [TestFixture]
    public class FhaMiOpm58376Test
    {
        private CDateTime m_before2015Jan26 = CDateTime.Create(new DateTime(2015, 1, 25));
        private CDateTime m_on2015Jan26 = CDateTime.Create(new DateTime(2015, 1, 26));

        [TestFixtureSetUp]
        public void Setup()
        {
            LoginTools.LoginAs(TestAccountType.LoTest001);
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
            FakePageDataUtilities.Instance.Reset();
        }

        [Test]
        public void TestHighTermLowLtv()
        {
            CPageData dataLoan = CreatePageData();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sLPurposeT = E_sLPurposeT.Purchase;

            dataLoan.sLT = E_sLT.FHA;
            dataLoan.sLAmtLckd = true;
            dataLoan.sPurchPrice = 300000.00M;
            dataLoan.sLAmtCalc = 200000.00M;
            dataLoan.sCaseAssignmentD = m_before2015Jan26;
            decimal ltv = dataLoan.sLtvRPe;
            decimal sterm = dataLoan.sTermInYr;
            dataLoan.sNoteIR = 6; 
            dataLoan.sTerm = 360;
            dataLoan.sDue = 360;
            dataLoan.sProMInsR = 0;
            dataLoan.Update_sProMinsR(ltv, sterm);
            dataLoan.Save();

            dataLoan = FakePageData.Create();
            dataLoan.InitLoad();

            Assert.That(dataLoan.sProMInsR, Is.EqualTo(1.30M));
        }


        [Test]
        public void TestHighTermLowLtvHighLamt()
        {
            CPageData dataLoan = CreatePageData();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sLPurposeT = E_sLPurposeT.Purchase;

            dataLoan.sLT = E_sLT.FHA;
            dataLoan.sLAmtLckd = true;
            dataLoan.sPurchPrice = 900000.00M;
            dataLoan.sLAmtCalc = 670000.00M;
            dataLoan.sCaseAssignmentD = m_before2015Jan26;
            decimal ltv = dataLoan.sLtvRPe;
            decimal sterm = dataLoan.sTermInYr;
            dataLoan.sNoteIR = 6;
            dataLoan.sTerm = 360;
            dataLoan.sDue = 360;
            dataLoan.sProMInsR = 0;
            dataLoan.Update_sProMinsR(ltv, sterm);
            dataLoan.Save();

            dataLoan = FakePageData.Create();
            dataLoan.InitLoad();

            Assert.That(dataLoan.sProMInsR, Is.EqualTo(1.50M));
        }


        [Test]
        public void TestHighTermHighLtv()
        {
            CPageData dataLoan = CreatePageData();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            dataLoan.sLPurposeT = E_sLPurposeT.Purchase;
            dataLoan.sLT = E_sLT.FHA;
            dataLoan.sLAmtLckd = true;
            dataLoan.sPurchPrice = 300000.00M;
            dataLoan.sLAmtCalc = 290000.00M;
            dataLoan.sCaseAssignmentD = m_before2015Jan26;
            decimal ltv = dataLoan.sLtvRPe;
            decimal sterm = dataLoan.sTermInYr;
            dataLoan.sNoteIR = 6;
            dataLoan.sTerm = 360;
            dataLoan.sDue = 360;
            dataLoan.sProMInsR = 0;
            dataLoan.Update_sProMinsR(ltv, sterm);
            dataLoan.Save();

            dataLoan = FakePageData.Create();
            dataLoan.InitLoad();

            Assert.That(dataLoan.sProMInsR, Is.EqualTo(1.35M));
        }

        [Test]
        public void TestHighTermHighLtvHighLAmt()
        {
            CPageData dataLoan = CreatePageData();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            dataLoan.sLPurposeT = E_sLPurposeT.Purchase;
            dataLoan.sLT = E_sLT.FHA;
            dataLoan.sLAmtLckd = true;
            dataLoan.sPurchPrice = 900000.00M;
            dataLoan.sLAmtCalc = 890000.00M;
            dataLoan.sCaseAssignmentD = m_before2015Jan26;
            decimal ltv = dataLoan.sLtvRPe;
            decimal sterm = dataLoan.sTermInYr;
            dataLoan.sNoteIR = 6;
            dataLoan.sTerm = 360;
            dataLoan.sDue = 360;
            dataLoan.sProMInsR = 0;
            dataLoan.Update_sProMinsR(ltv, sterm);
            dataLoan.Save();

            dataLoan = FakePageData.Create();
            dataLoan.InitLoad();

            Assert.That(dataLoan.sProMInsR, Is.EqualTo(1.55M));
        }

        [Test]
        public void TestHighTermLowLtvPost26Jan2015()
        {
            CPageData dataLoan = CreatePageData();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sLPurposeT = E_sLPurposeT.Purchase;

            dataLoan.sLT = E_sLT.FHA;
            dataLoan.sLAmtLckd = true;
            dataLoan.sPurchPrice = 300000.00M;
            dataLoan.sLAmtCalc = 200000.00M;
            dataLoan.sCaseAssignmentD = m_on2015Jan26;
            decimal ltv = dataLoan.sLtvRPe;
            decimal sterm = dataLoan.sTermInYr;
            dataLoan.sNoteIR = 6;
            dataLoan.sTerm = 360;
            dataLoan.sDue = 360;
            dataLoan.sProMInsR = 0;
            dataLoan.Update_sProMinsR(ltv, sterm);
            dataLoan.Save();

            dataLoan = FakePageData.Create();
            dataLoan.InitLoad();

            Assert.That(dataLoan.sProMInsR, Is.EqualTo(0.80M));
        }


        [Test]
        public void TestHighTermLowLtvHighLamtPost26Jan2015()
        {
            CPageData dataLoan = CreatePageData();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sLPurposeT = E_sLPurposeT.Purchase;

            dataLoan.sLT = E_sLT.FHA;
            dataLoan.sLAmtLckd = true;
            dataLoan.sPurchPrice = 900000.00M;
            dataLoan.sLAmtCalc = 670000.00M;
            dataLoan.sCaseAssignmentD = m_on2015Jan26;
            decimal ltv = dataLoan.sLtvRPe;
            decimal sterm = dataLoan.sTermInYr;
            dataLoan.sNoteIR = 6;
            dataLoan.sTerm = 360;
            dataLoan.sDue = 360;
            dataLoan.sProMInsR = 0;
            dataLoan.Update_sProMinsR(ltv, sterm);
            dataLoan.Save();

            dataLoan = FakePageData.Create();
            dataLoan.InitLoad();

            Assert.That(dataLoan.sProMInsR, Is.EqualTo(1.00M));
        }


        [Test]
        public void TestHighTermHighLtvPost26Jan2015()
        {
            CPageData dataLoan = CreatePageData();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            dataLoan.sLPurposeT = E_sLPurposeT.Purchase;
            dataLoan.sLT = E_sLT.FHA;
            dataLoan.sLAmtLckd = true;
            dataLoan.sPurchPrice = 300000.00M;
            dataLoan.sLAmtCalc = 290000.00M;
            dataLoan.sCaseAssignmentD = m_on2015Jan26;
            decimal ltv = dataLoan.sLtvRPe;
            decimal sterm = dataLoan.sTermInYr;
            dataLoan.sNoteIR = 6;
            dataLoan.sTerm = 360;
            dataLoan.sDue = 360;
            dataLoan.sProMInsR = 0;
            dataLoan.Update_sProMinsR(ltv, sterm);
            dataLoan.Save();

            dataLoan = FakePageData.Create();
            dataLoan.InitLoad();

            Assert.That(dataLoan.sProMInsR, Is.EqualTo(0.85M));
        }

        [Test]
        public void TestHighTermHighLtvHighLAmtPost26Jan2015()
        {
            CPageData dataLoan = CreatePageData();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            dataLoan.sLPurposeT = E_sLPurposeT.Purchase;
            dataLoan.sLT = E_sLT.FHA;
            dataLoan.sLAmtLckd = true;
            dataLoan.sPurchPrice = 900000.00M;
            dataLoan.sLAmtCalc = 890000.00M;
            dataLoan.sCaseAssignmentD = m_on2015Jan26;
            decimal ltv = dataLoan.sLtvRPe;
            decimal sterm = dataLoan.sTermInYr;
            dataLoan.sNoteIR = 6;
            dataLoan.sTerm = 360;
            dataLoan.sDue = 360;
            dataLoan.sProMInsR = 0;
            dataLoan.Update_sProMinsR(ltv, sterm);
            dataLoan.Save();

            dataLoan = FakePageData.Create();
            dataLoan.InitLoad();

            Assert.That(dataLoan.sProMInsR, Is.EqualTo(1.05M));
        }

        [Test]
        public void TestLowTermmHighLtv()
        {
            CPageData dataLoan = CreatePageData();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            dataLoan.sLPurposeT = E_sLPurposeT.Purchase;
            dataLoan.sLT = E_sLT.FHA;
            dataLoan.sLAmtLckd = true;
            dataLoan.sPurchPrice = 300000.00M;
            dataLoan.sLAmtCalc = 290000.00M;
            dataLoan.sCaseAssignmentD = m_before2015Jan26;
            decimal ltv = dataLoan.sLtvRPe;
            decimal sterm = dataLoan.sTermInYr;
            dataLoan.sNoteIR = 6;
            dataLoan.sTerm = 180;
            dataLoan.sDue = 180;
            dataLoan.sProMInsR = 0;
            dataLoan.Update_sProMinsR(ltv, sterm);
            dataLoan.Save();

            dataLoan = FakePageData.Create();
            dataLoan.InitLoad();

            Assert.That(dataLoan.sProMInsR, Is.EqualTo(.70M));
        }


        [Test]
        public void TestLowTermHighLtvHighLamt()
        {
            CPageData dataLoan = CreatePageData();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            dataLoan.sLPurposeT = E_sLPurposeT.Purchase;
            dataLoan.sLT = E_sLT.FHA;
            dataLoan.sLAmtLckd = true;
            dataLoan.sPurchPrice = 900000.00M;
            dataLoan.sLAmtCalc = 890000.00M;
            dataLoan.sCaseAssignmentD = m_before2015Jan26;
            decimal ltv = dataLoan.sLtvRPe;
            decimal sterm = dataLoan.sTermInYr;
            dataLoan.sNoteIR = 6;
            dataLoan.sTerm = 180;
            dataLoan.sDue = 180;
            dataLoan.sProMInsR = 0;
            dataLoan.Update_sProMinsR(ltv, sterm);
            dataLoan.Save();

            dataLoan = FakePageData.Create();
            dataLoan.InitLoad();

            Assert.That(dataLoan.sProMInsR, Is.EqualTo(.95M));
        }

        [Test]
        public void TestLowTermLowLtv()
        {
            CPageData dataLoan = CreatePageData();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
      
            dataLoan.sLPurposeT = E_sLPurposeT.Purchase;
            dataLoan.sLT = E_sLT.FHA;
            dataLoan.sLAmtLckd = true;
            dataLoan.sPurchPrice = 300000.00M;
            dataLoan.sLAmtCalc   = 200000.00M;
            dataLoan.sCaseAssignmentD = m_before2015Jan26;
            decimal ltv = dataLoan.sLtvRPe;
            decimal sterm = dataLoan.sTermInYr;
            dataLoan.sNoteIR = 6;
            dataLoan.sTerm = 180;
            dataLoan.sDue = 180;
            dataLoan.sProMInsR = 0;
            dataLoan.Update_sProMinsR(ltv, sterm);
            dataLoan.Save();

            dataLoan = FakePageData.Create();
            dataLoan.InitLoad();
                                                         //ltv is less than 78 so should be 0
            Assert.That(dataLoan.sProMInsR, Is.EqualTo(.0M));
        }


        [Test]
        public void TestHighTermHighLtvProtection()
        {
            CPageData dataLoan = CreatePageData();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            dataLoan.sLPurposeT = E_sLPurposeT.Purchase;
            dataLoan.sLT = E_sLT.FHA;
            dataLoan.sLAmtLckd = true;
            dataLoan.sPurchPrice = 300000.00M;
            dataLoan.sLAmtCalc = 290000.00M;
            dataLoan.sCaseAssignmentD = m_before2015Jan26;
            decimal ltv = dataLoan.sLtvRPe;
            decimal sterm = dataLoan.sTermInYr;
            dataLoan.sNoteIR = 6;
            dataLoan.sTerm = 360;
            dataLoan.sDue = 360;
            dataLoan.sProMInsR = 83M;
            dataLoan.Update_sProMinsR(ltv, sterm);
            dataLoan.Save();
            
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            Assert.That(dataLoan.sProMInsR, Is.EqualTo(83M));
            dataLoan.Update_sProMinsR(ltv, sterm);
            dataLoan.Save();

            dataLoan = FakePageData.Create();
            dataLoan.InitLoad();
            Assert.That(dataLoan.sProMInsR, Is.EqualTo(83M));

            
        }

        private CPageData CreatePageData()
        {
            return FakePageData.Create();
        }
    }
}
