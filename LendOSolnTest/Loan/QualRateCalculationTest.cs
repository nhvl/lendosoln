﻿namespace LendOSolnTest.Loan
{
    using System;
    using Common;
    using DataAccess;
    using Fakes;
    using LendersOffice.ObjLib.Loan;
    using NUnit.Framework;

    public class QualRateCalculationTest
    {
        private const decimal LpeUploadValueRate = 4.5M;
        private const decimal NoteRate = 3.0M;
        private const decimal IndexRate = 0.75M;
        private const decimal FullyIndexedRate = 3.25M;
        private const decimal MarginRate = 2.5M;

        private const decimal FirstAdjustment = 0.5M;
        private const decimal SecondAdjustment = 1.25M;

        [TestFixtureSetUp]
        public void Setup()
        {
            LoginTools.LoginAs(TestAccountType.LoTest001);
        }

        [Test]
        public void UseQualRateFalse_CalcReturnsZero()
        {
            var loan = FakePageData.Create();
            loan.InitLoad();

            loan.sFinMethT = E_sFinMethT.ARM;
            loan.sUseQualRate = false;
            
            loan.sQualRateCalculationT = QualRateCalculationT.FlatValue;
            loan.sQualRateCalculationFieldT1 = QualRateCalculationFieldT.Index;
            loan.sRAdjIndexR = IndexRate;
            loan.sQualRateCalculationAdjustment1 = FirstAdjustment;

            var result = this.GetResult(loan);
            Assert.AreEqual(0M, result, "Expected zero qual rate value when loan is not using qual rate.");

            loan.sQualRateCalculationT = QualRateCalculationT.MaxOf;
            loan.sQualRateCalculationFieldT2 = QualRateCalculationFieldT.Margin;
            loan.sRAdjMarginR = MarginRate;
            loan.sQualRateCalculationAdjustment2 = SecondAdjustment;

            result = this.GetResult(loan);
            Assert.AreEqual(0M, result, "Expected zero qual rate value when loan is not using qual rate.");
        }

        [Test]
        public void FlatValue_OnlyUsesFirstFields()
        {
            var loan = FakePageData.Create();
            loan.InitLoad();

            loan.sFinMethT = E_sFinMethT.ARM;
            loan.sRAdjIndexR = IndexRate;

            loan.sUseQualRate = true;
            loan.sQualRateCalculationT = QualRateCalculationT.FlatValue;
            loan.sQualRateCalculationFieldT1 = QualRateCalculationFieldT.Index;
            loan.sQualRateCalculationAdjustment1 = FirstAdjustment;
            loan.sQualRateCalculationFieldT2 = QualRateCalculationFieldT.Margin;
            loan.sQualRateCalculationAdjustment2 = SecondAdjustment;

            var result = this.GetResult(loan);
            Assert.AreEqual(IndexRate + FirstAdjustment, result, "Expected qual rate value to be index rate with adjustment and not use second calculation fields.");
        }

        [Test]
        public void FlatValueCalculationTest()
        {
            var loan = FakePageData.Create();
            loan.InitLoad();

            loan.sFinMethT = E_sFinMethT.ARM;
            loan.sUseQualRate = true;
            loan.sQualRateCalculationT = QualRateCalculationT.FlatValue;

            var testCases = new[]
            {
                new
                {
                    Field = QualRateCalculationFieldT.Index,
                    FieldGetter = new Func<CPageData, decimal>(l => l.sRAdjIndexR),
                    FieldSetter = new Action<CPageData>(l => l.sRAdjIndexR = IndexRate),
                    Adjustment = 0M,
                    Expected = IndexRate
                },
                new
                {
                    Field = QualRateCalculationFieldT.Index,
                    FieldGetter = new Func<CPageData, decimal>(l => l.sRAdjIndexR),
                    FieldSetter = new Action<CPageData>(l => l.sRAdjIndexR = IndexRate),
                    Adjustment = FirstAdjustment,
                    Expected = IndexRate + FirstAdjustment
                },
                new
                {
                    Field = QualRateCalculationFieldT.NoteRate,
                    FieldGetter = new Func<CPageData, decimal>(l => l.sNoteIR),
                    FieldSetter = new Action<CPageData>(l => l.sNoteIR = NoteRate),
                    Adjustment = 0M,
                    Expected = NoteRate
                },
                new
                {
                    Field = QualRateCalculationFieldT.NoteRate,
                    FieldGetter = new Func<CPageData, decimal>(l => l.sNoteIR),
                    FieldSetter = new Action<CPageData>(l => l.sNoteIR = NoteRate),
                    Adjustment = SecondAdjustment,
                    Expected = NoteRate + SecondAdjustment
                },
                new
                {
                    Field = QualRateCalculationFieldT.Margin,
                    FieldGetter = new Func<CPageData, decimal>(l => l.sRAdjMarginR),
                    FieldSetter = new Action<CPageData>(l => l.sRAdjMarginR = MarginRate),
                    Adjustment = 0M,
                    Expected = MarginRate
                },
                new
                {
                    Field = QualRateCalculationFieldT.Margin,
                    FieldGetter = new Func<CPageData, decimal>(l => l.sRAdjMarginR),
                    FieldSetter = new Action<CPageData>(l => l.sRAdjMarginR = MarginRate),
                    Adjustment = FirstAdjustment,
                    Expected = MarginRate + FirstAdjustment
                },
                new
                {
                    Field = QualRateCalculationFieldT.FullyIndexedRate,
                    FieldGetter = new Func<CPageData, decimal>(l => l.sFullyIndexedR),
                    FieldSetter = new Action<CPageData>(l => { l.sRAdjMarginR = MarginRate; l.sRAdjIndexR = IndexRate; }),
                    Adjustment = 0M,
                    Expected = IndexRate + MarginRate
                },
                new
                {
                    Field = QualRateCalculationFieldT.FullyIndexedRate,
                    FieldGetter = new Func<CPageData, decimal>(l => l.sFullyIndexedR),
                    FieldSetter = new Action<CPageData>(l => { l.sRAdjMarginR = MarginRate; l.sRAdjIndexR = IndexRate; }),
                    Adjustment = SecondAdjustment,
                    Expected = IndexRate + MarginRate + SecondAdjustment
                }
            };

            foreach (var test in testCases)
            {
                loan.sQualRateCalculationFieldT1 = test.Field;
                test.FieldSetter(loan);
                loan.sQualRateCalculationAdjustment1 = test.Adjustment;

                var result = this.GetResult(loan);
                Assert.AreEqual(test.Expected, result, "Test: " + test + Environment.NewLine + "Field value: " + test.FieldGetter(loan));
            }
        }

        [Test]
        public void MaxOfCalculationTest()
        {
            var loan = FakePageData.Create();
            loan.InitLoad();

            loan.sFinMethT = E_sFinMethT.ARM;
            loan.sUseQualRate = true;
            loan.sQualRateCalculationT = QualRateCalculationT.MaxOf;

            var testCases = new[]
            {
                new
                {
                    Field1 = QualRateCalculationFieldT.Index,
                    Field1Getter = new Func<CPageData, decimal>(l => l.sRAdjIndexR),
                    Field1Setter = new Action<CPageData>(l => l.sRAdjIndexR = IndexRate),
                    Adjustment1 = 0M,

                    Field2 = QualRateCalculationFieldT.Margin,
                    Field2Getter = new Func<CPageData, decimal>(l => l.sRAdjMarginR),
                    Field2Setter = new Action<CPageData>(l => l.sRAdjMarginR = MarginRate),
                    Adjustment2 = 0M,

                    Expected = MarginRate
                },
                new
                {
                    Field1 = QualRateCalculationFieldT.Index,
                    Field1Getter = new Func<CPageData, decimal>(l => l.sRAdjIndexR),
                    Field1Setter = new Action<CPageData>(l => l.sRAdjIndexR = IndexRate),
                    Adjustment1 = 2.0M,

                    Field2 = QualRateCalculationFieldT.Margin,
                    Field2Getter = new Func<CPageData, decimal>(l => l.sRAdjMarginR),
                    Field2Setter = new Action<CPageData>(l => l.sRAdjMarginR = MarginRate),
                    Adjustment2 = 0M,

                    Expected = IndexRate + 2.0M
                },
                new
                {
                    Field1 = QualRateCalculationFieldT.FullyIndexedRate,
                    Field1Getter = new Func<CPageData, decimal>(l => l.sFullyIndexedR),
                    Field1Setter = new Action<CPageData>(l => { l.sRAdjMarginR = MarginRate; l.sRAdjIndexR = IndexRate; }),
                    Adjustment1 = 0M,

                    Field2 = QualRateCalculationFieldT.NoteRate,
                    Field2Getter = new Func<CPageData, decimal>(l => l.sNoteIR),
                    Field2Setter = new Action<CPageData>(l => l.sNoteIR = NoteRate),
                    Adjustment2 = 0M,

                    Expected = IndexRate + MarginRate
                },
                new
                {
                    Field1 = QualRateCalculationFieldT.FullyIndexedRate,
                    Field1Getter = new Func<CPageData, decimal>(l => l.sFullyIndexedR),
                    Field1Setter = new Action<CPageData>(l => { l.sRAdjMarginR = MarginRate; l.sRAdjIndexR = IndexRate; }),
                    Adjustment1 = 0M,

                    Field2 = QualRateCalculationFieldT.NoteRate,
                    Field2Getter = new Func<CPageData, decimal>(l => l.sNoteIR),
                    Field2Setter = new Action<CPageData>(l => l.sNoteIR = NoteRate),
                    Adjustment2 = 1.0M,

                    Expected = NoteRate + 1.0M
                },
            };

            foreach (var test in testCases)
            {
                loan.sQualRateCalculationFieldT1 = test.Field1;
                test.Field1Setter(loan);
                loan.sQualRateCalculationAdjustment1 = test.Adjustment1;

                loan.sQualRateCalculationFieldT2 = test.Field2;
                test.Field2Setter(loan);
                loan.sQualRateCalculationAdjustment2 = test.Adjustment2;

                var result = this.GetResult(loan);
                Assert.AreEqual(test.Expected, result, "Test: " + test + Environment.NewLine + "Field 1 value: " + test.Field1Getter(loan) + Environment.NewLine + "Field 2 value: " + test.Field2Getter(loan));
            }
        }

        private decimal GetResult(CPageData loan)
        {
            var options = new QualRateCalculationOptions()
            {
                UseQualRate = loan.sUseQualRate,
                CalculationType = loan.sQualRateCalculationT,
                CalculationField1 = loan.sQualRateCalculationFieldT1,
                CalculationField2 = loan.sQualRateCalculationFieldT2,
                Adjustment1 = loan.sQualRateCalculationAdjustment1,
                Adjustment2 = loan.sQualRateCalculationAdjustment2,
                NoteRate = loan.sNoteIR,
                IndexRate = loan.sRAdjIndexR,
                MarginRate = loan.sRAdjMarginR
            };

            return Tools.CalculateQualRate(options);
        }
    }
}
