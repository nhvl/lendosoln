﻿namespace LendOSolnTest.Loan
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Audit;
    using LendersOffice.Constants;
    using LendersOffice.Security;
    using LendOSolnTest.Common;
    using NUnit.Framework;

    [TestFixture]
    public class OriginatorCompensationAutoApplicationTest
    {
        private Guid loanId = Guid.Empty;
        CPageData m_dataLoan = null;

        private CPageData DataLoan
        {
            get
            {
                if (m_dataLoan == null)
                {
                    this.m_dataLoan = CPageData.CreateUsingSmartDependency(this.loanId, typeof(OriginatorCompensationAutoApplicationTest));

                    this.m_dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                    this.m_dataLoan.sIsBranchActAsOriginatorForFileTri = E_TriState.Yes;
                    this.m_dataLoan.sIsBranchActAsLenderForFileTri = E_TriState.Yes;

                    this.m_dataLoan.sLAmtCalc_rep = "100,000.00";
                    this.m_dataLoan.sLAmtLckd = true;

                    this.m_dataLoan.sFfUfmipR_rep = "0.500%";
                    this.m_dataLoan.sFfUfMipIsBeingFinanced = true;

                    this.m_dataLoan.sSpAddr = "1124 Bristol St.";
                    this.m_dataLoan.sSpCity = "Costa Mesa";
                    this.m_dataLoan.sSpState = "CA";
                    this.m_dataLoan.sSpZip = "92626";

                    CAppData app = m_dataLoan.Apps.FirstOrDefault();

                    app.aBFirstNm = "Originator Comp";
                    app.aBLastNm = "Auto Application Test";
                    app.aBSsn = "000-00-0000";
                    app.aBBaseI_rep = "10000";

                    this.m_dataLoan.Save();
                }

                return m_dataLoan;
            }
        }

        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.LoanOfficer);
            Assert.IsNotNull(principal);

            CLoanFileCreator fileCreator = CLoanFileCreator.GetCreator(principal, E_LoanCreationSource.UserCreateFromBlank);
            this.loanId = fileCreator.CreateBlankLoanFile();
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.LoanOfficer);
            Assert.IsNotNull(principal);

            if (Guid.Empty != this.loanId)
            {
                Tools.DeclareLoanFileInvalid(principal, this.loanId, false, false);
            }
        }

        [TearDown]
        public void TearDown()
        {
            if (this.m_dataLoan != null)
            {
                this.m_dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                this.m_dataLoan.sPurchPrice = 0;
                this.m_dataLoan.sDocumentCheckD_rep = string.Empty;
                this.m_dataLoan.ClearDisclosureWithoutMarking();
                this.m_dataLoan.BreakRateLock("bob", "IDK, just bob being bob");
                this.m_dataLoan.Save();
            }
        }

        [Test]
        public void ApplyCompOnRateLock()
        {
            AbstractUserPrincipal testingPrincipal = LoginTools.LoginAs(TestAccountType.LoTest001);

            this.ClearCompPopulationOptions(this.DataLoan);

            var testSequence = new List<Tuple<string, bool, Action>>()
            {
                new Tuple<string, bool, Action>("RESPA 6 trigger", false, () => { this.DataLoan.sPurchPrice = 250000; }),
                new Tuple<string, bool, Action>("Document Check trigger", false, () => this.DataLoan.sDocumentCheckD_rep = "6/13/2017"),
                new Tuple<string, bool, Action>("Rate Lock trigger", true, () => this.DataLoan.LockRate("bob", "IDK, just bob being bob", false)),
            };

            foreach (var test in testSequence)
            {
                this.TestAction(test.Item1, test.Item2, test.Item3);
            }
        }

        [Test]
        public void DontApplyCompOnRateLock()
        {
            AbstractUserPrincipal testingPrincipal = LoginTools.LoginAs(TestAccountType.LoTest001);

            this.ClearCompPopulationOptions(this.DataLoan);
            this.DataLoan.BrokerDB.TempOptionXmlContent = this.DataLoan.BrokerDB.TempOptionXmlContent.Value + "<option name=\"ApplyCompAtRateLock\" value=\"false\" />";

            var testSequence = new List<Tuple<string, bool, Action>>()
            {
                new Tuple<string, bool, Action>("RESPA 6 trigger", false, () => { this.DataLoan.sPurchPrice = 250000; }),
                new Tuple<string, bool, Action>("Document Check trigger", false, () => this.DataLoan.sDocumentCheckD_rep = "6/13/2017"),
                new Tuple<string, bool, Action>("Rate Lock trigger", false, () => this.DataLoan.LockRate("bob", "IDK, just bob being bob", false)),
            };

            foreach (var test in testSequence)
            {
                this.TestAction(test.Item1, test.Item2, test.Item3);
            }
        }

        [Test]
        public void ApplyCompOnDocumentCheckOnly()
        {
            AbstractUserPrincipal testingPrincipal = LoginTools.LoginAs(TestAccountType.LoTest001);

            this.ClearCompPopulationOptions(this.DataLoan);
            this.DataLoan.BrokerDB.TempOptionXmlContent = this.DataLoan.BrokerDB.TempOptionXmlContent.Value + "<option name=\"ApplyCompWhenDocCheckDateEntered\" value=\"true\" />";

            // Uncomment this once all clients with the ApplyCompWhenDocCheckEntered temp option have the rate lock and registered temp options set to false
            //this.DataLoan.BrokerDB.TempOptionXmlContent = this.DataLoan.BrokerDB.TempOptionXmlContent.Value + "<option name=\"ApplyCompAtRateLock\" value=\"false\" />";
            //this.DataLoan.BrokerDB.TempOptionXmlContent = this.DataLoan.BrokerDB.TempOptionXmlContent.Value + "<option name=\"ApplyCompAtRegistration\" value=\"false\" />";

            var testSequence = new List<Tuple<string, bool, Action>>()
            {
                new Tuple<string, bool, Action>("RESPA 6 trigger", false, () => { this.DataLoan.sPurchPrice = 250000; }),
                new Tuple<string, bool, Action>("Document Check trigger", true, () => this.DataLoan.sDocumentCheckD_rep = "6/13/2017"),
                new Tuple<string, bool, Action>("Rate Lock trigger", false, () => this.DataLoan.LockRate("bob", "IDK, just bob being bob", false)),
            };

            foreach (var test in testSequence)
            {
                this.TestAction(test.Item1, test.Item2, test.Item3);
            }
        }

        [Test]
        public void ApplyCompOnRespa6Only()
        {
            AbstractUserPrincipal testingPrincipal = LoginTools.LoginAs(TestAccountType.LoTest001);

            this.ClearCompPopulationOptions(this.DataLoan);
            this.DataLoan.BrokerDB.TempOptionXmlContent = this.DataLoan.BrokerDB.TempOptionXmlContent.Value + "<option name=\"ApplyCompWhenRespa6Collected\" value=\"true\" />";
            this.DataLoan.BrokerDB.TempOptionXmlContent = this.DataLoan.BrokerDB.TempOptionXmlContent.Value + "<option name=\"ApplyCompAtRateLock\" value=\"false\" />";
            this.DataLoan.BrokerDB.TempOptionXmlContent = this.DataLoan.BrokerDB.TempOptionXmlContent.Value + "<option name=\"ApplyCompAtRegistration\" value=\"false\" />";

            var testSequence = new List<Tuple<string, bool, Action>>()
            {
                new Tuple<string, bool, Action>("RESPA 6 trigger", true, () => { this.DataLoan.sPurchPrice = 250000; }),
                new Tuple<string, bool, Action>("Document Check trigger", false, () => this.DataLoan.sDocumentCheckD_rep = "6/13/2017"),
                new Tuple<string, bool, Action>("Rate Lock trigger", false, () => this.DataLoan.LockRate("bob", "IDK, just bob being bob", false)),
            };

            foreach (var test in testSequence)
            {
                this.TestAction(test.Item1, test.Item2, test.Item3);
            }
        }

        [Test]
        public void ApplyCompForRespa6AndRateLock()
        {
            AbstractUserPrincipal testingPrincipal = LoginTools.LoginAs(TestAccountType.LoTest001);

            this.ClearCompPopulationOptions(this.DataLoan);
            this.DataLoan.BrokerDB.TempOptionXmlContent = this.DataLoan.BrokerDB.TempOptionXmlContent.Value + "<option name=\"ApplyCompWhenRespa6Collected\" value=\"true\" />";

            var testSequence = new List<Tuple<string, bool, Action>>()
            {
                new Tuple<string, bool, Action>("RESPA 6 trigger", true, () => { this.DataLoan.sPurchPrice = 250000; }),
                new Tuple<string, bool, Action>("Document Check trigger", false, () => this.DataLoan.sDocumentCheckD_rep = "6/13/2017"),
                new Tuple<string, bool, Action>("Rate Lock trigger", true, () => this.DataLoan.LockRate("bob", "IDK, just bob being bob", false)),
            };

            foreach (var test in testSequence)
            {
                this.TestAction(test.Item1, test.Item2, test.Item3);
            }
        }

        private void ClearCompPopulationOptions(CPageData dataLoan)
        {
            dataLoan.BrokerDB.TempOptionXmlContent = dataLoan.BrokerDB.TempOptionXmlContent.Value.Replace("<option name=\"ApplyCompWhenDocCheckDateEntered\" value=\"true\" />", "");
            dataLoan.BrokerDB.TempOptionXmlContent = dataLoan.BrokerDB.TempOptionXmlContent.Value.Replace("<option name=\"ApplyCompWhenRespa6Collected\" value=\"true\" />", "");
            dataLoan.BrokerDB.TempOptionXmlContent = dataLoan.BrokerDB.TempOptionXmlContent.Value.Replace("<option name=\"ApplyCompAtRegistration\" value=\"false\" />", "");
            dataLoan.BrokerDB.TempOptionXmlContent = dataLoan.BrokerDB.TempOptionXmlContent.Value.Replace("<option name=\"ApplyCompAtRateLock\" value=\"false\" />", "");
        }

        private void TestAction(string triggerType, bool result, Action action)
        {
            this.DataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            this.DataLoan.ClearCompensationPlan();
            action();
            this.DataLoan.Save();
            Assert.That(result == this.DataLoan.sHasOriginatorCompensationPlan, triggerType + ": sHasOriginatorCompensationPlan expected to be " + result + ", but was " + this.DataLoan.sHasOriginatorCompensationPlan);
        }
    }
}
