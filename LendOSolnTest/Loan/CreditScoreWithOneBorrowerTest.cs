﻿using DataAccess;
using LendOSolnTest.Common;
using LendOSolnTest.Fakes;
using NUnit.Framework;

namespace LendOSolnTest.Loan
{
    /// <summary>
    /// Set up test to verify sCreditScoreType1, sCreditScoreType2, sCreditScoreType2Soft, sCreditScoreType3.
    /// This test is only use one set of applicant.
    /// </summary>
    [TestFixture]
    public class CreditScoreWithOneBorrowerTest
    {
        [TestFixtureSetUp]
        public void Setup()
        {
            LoginTools.LoginAs(TestAccountType.LoTest001);
        }

        [Test]
        public void TestInLendersOfficeMode()
        {

            var testCases = new[] {
                new { 
                    aBSsn="111-11-1111", aBBaseI="", aBExperian="600", aBTransUnion="700", aBEquifax="800",
                    aCSsn="222-22-2222", aCBaseI="", aCExperian="",    aCTransUnion="",    aCEquifax="",
                    sCreditScoreType1 = "700", sCreditScoreType2 = "", sCreditScoreType2Soft="700", sCreditScoreType3="700"
                },

                new { 
                    aBSsn="111-11-1111", aBBaseI="", aBExperian="600", aBTransUnion="700", aBEquifax="800",
                    aCSsn="222-22-2222", aCBaseI="", aCExperian="650", aCTransUnion="750", aCEquifax="850",
                    sCreditScoreType1 = "700", sCreditScoreType2 = "700", sCreditScoreType2Soft="700", sCreditScoreType3="700"
                },

                new { 
                    aBSsn="111-11-1111", aBBaseI="", aBExperian="", aBTransUnion="", aBEquifax="",
                    aCSsn="222-22-2222", aCBaseI="", aCExperian="650", aCTransUnion="750", aCEquifax="850",
                    sCreditScoreType1 = "", sCreditScoreType2 = "", sCreditScoreType2Soft="750", sCreditScoreType3=""
                },

                new { 
                    aBSsn="111-11-1111", aBBaseI="", aBExperian="600", aBTransUnion="700", aBEquifax="800",
                    aCSsn="",            aCBaseI="", aCExperian="",    aCTransUnion="",    aCEquifax="",
                    sCreditScoreType1 = "700", sCreditScoreType2 = "700", sCreditScoreType2Soft="700", sCreditScoreType3="700"
                },

                new { 
                    aBSsn="111-11-1111", aBBaseI="",     aBExperian="600", aBTransUnion="700", aBEquifax="800",
                    aCSsn="222-22-2222", aCBaseI="$100", aCExperian="650", aCTransUnion="750", aCEquifax="850",
                    sCreditScoreType1 = "750", sCreditScoreType2 = "700", sCreditScoreType2Soft="700", sCreditScoreType3="700"
                },

                new { 
                    aBSsn="111-11-1111", aBBaseI="",     aBExperian="600", aBTransUnion="700", aBEquifax="800",
                    aCSsn="222-22-2222", aCBaseI="$100", aCExperian="650", aCTransUnion="750", aCEquifax="850",
                    sCreditScoreType1 = "750", sCreditScoreType2 = "700", sCreditScoreType2Soft="700", sCreditScoreType3="700"
                },

            };

            #region Verify Test Cases
            CPageData dataLoan = CreatePageData();
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(0);

            foreach (var o in testCases)
            {
                dataApp.aBSsn = o.aBSsn;
                dataApp.aBBaseI_rep = o.aBBaseI;
                dataApp.aBExperianScore_rep = o.aBExperian;
                dataApp.aBTransUnionScore_rep = o.aBTransUnion;
                dataApp.aBEquifaxScore_rep = o.aBEquifax;

                dataApp.aCSsn = o.aCSsn;
                dataApp.aCBaseI_rep = o.aCBaseI;
                dataApp.aCExperianScore_rep = o.aCExperian;
                dataApp.aCTransUnionScore_rep = o.aCTransUnion;
                dataApp.aCEquifaxScore_rep = o.aCEquifax;

                Assert.AreEqual(o.sCreditScoreType1, dataLoan.sCreditScoreType1_rep, "sCreditScoreType1_rep::" + o);
                Assert.AreEqual(o.sCreditScoreType2, dataLoan.sCreditScoreType2_rep, "sCreditScoreType2_rep::" + o);
                Assert.AreEqual(o.sCreditScoreType2Soft, dataLoan.sCreditScoreType2Soft_rep, "sCreditScoreType2Soft_rep::" + o);
                Assert.AreEqual(o.sCreditScoreType3, dataLoan.sCreditScoreType3_rep, "sCreditScoreType3_rep::" + o);
            }
            #endregion

        }

        [Test]
        public void TestInPricingMode()
        {

            var testCases = new[] {
                new { 
                    aBSsn="111-11-1111", aBBaseI="", aBExperian="600", aBTransUnion="700", aBEquifax="800",
                    aCSsn="222-22-2222", aCBaseI="", aCExperian="",    aCTransUnion="",    aCEquifax="", aIsBorrSpousePrimaryWageEarner=false,
                    sCreditScoreType1 = "700", sCreditScoreType2 = "0", sCreditScoreType2Soft="700", sCreditScoreType3="700"
                },

                new { 
                    aBSsn="111-11-1111", aBBaseI="", aBExperian="600", aBTransUnion="700", aBEquifax="800",
                    aCSsn="222-22-2222", aCBaseI="", aCExperian="650", aCTransUnion="750", aCEquifax="850", aIsBorrSpousePrimaryWageEarner=false,
                    sCreditScoreType1 = "700", sCreditScoreType2 = "700", sCreditScoreType2Soft="700", sCreditScoreType3="700"
                },

                new { 
                    aBSsn="111-11-1111", aBBaseI="", aBExperian="", aBTransUnion="", aBEquifax="",
                    aCSsn="222-22-2222", aCBaseI="", aCExperian="650", aCTransUnion="750", aCEquifax="850", aIsBorrSpousePrimaryWageEarner=false,
                    sCreditScoreType1 = "0", sCreditScoreType2 = "0", sCreditScoreType2Soft="750", sCreditScoreType3=""
                },

                new { 
                    aBSsn="111-11-1111", aBBaseI="", aBExperian="600", aBTransUnion="700", aBEquifax="800",
                    aCSsn="",            aCBaseI="", aCExperian="",    aCTransUnion="",    aCEquifax="", aIsBorrSpousePrimaryWageEarner=false,
                    sCreditScoreType1 = "700", sCreditScoreType2 = "700", sCreditScoreType2Soft="700", sCreditScoreType3="700"
                },

                new { 
                    aBSsn="111-11-1111", aBBaseI="",     aBExperian="600", aBTransUnion="700", aBEquifax="800",
                    aCSsn="222-22-2222", aCBaseI="$100", aCExperian="650", aCTransUnion="750", aCEquifax="850", aIsBorrSpousePrimaryWageEarner=false,
                    sCreditScoreType1 = "700", sCreditScoreType2 = "700", sCreditScoreType2Soft="700", sCreditScoreType3="700"
                },
                new { 
                    aBSsn="111-11-1111", aBBaseI="",     aBExperian="600", aBTransUnion="700", aBEquifax="800",
                    aCSsn="222-22-2222", aCBaseI="$100", aCExperian="650", aCTransUnion="750", aCEquifax="850", aIsBorrSpousePrimaryWageEarner=true,
                    sCreditScoreType1 = "750", sCreditScoreType2 = "700", sCreditScoreType2Soft="700", sCreditScoreType3="700"
                },


            };

            #region Verify Test Cases
            CPageData dataLoan = CreatePageData();
            dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(0);

            foreach (var o in testCases)
            {
                dataApp.aBSsn = o.aBSsn;
                dataApp.aBBaseI_rep = o.aBBaseI;
                dataApp.aBExperianScorePe_rep = o.aBExperian;
                dataApp.aBTransUnionScorePe_rep = o.aBTransUnion;
                dataApp.aBEquifaxScorePe_rep = o.aBEquifax;

                dataApp.aIsBorrSpousePrimaryWageEarner = o.aIsBorrSpousePrimaryWageEarner;

                dataApp.aCSsn = o.aCSsn;
                dataApp.aCBaseI_rep = o.aCBaseI;
                dataApp.aCExperianScorePe_rep = o.aCExperian;
                dataApp.aCTransUnionScorePe_rep = o.aCTransUnion;
                dataApp.aCEquifaxScorePe_rep = o.aCEquifax;

                Assert.AreEqual(o.sCreditScoreType1, dataLoan.sCreditScoreType1_rep, "sCreditScoreType1_rep::" + o);
                Assert.AreEqual(o.sCreditScoreType2, dataLoan.sCreditScoreType2_rep, "sCreditScoreType2_rep::" + o);
                Assert.AreEqual(o.sCreditScoreType2Soft, dataLoan.sCreditScoreType2Soft_rep, "sCreditScoreType2Soft_rep::" + o);
                Assert.AreEqual(o.sCreditScoreType3, dataLoan.sCreditScoreType3_rep, "sCreditScoreType3_rep::" + o);
            }
            #endregion

        }

        private CPageData CreatePageData()
        {
            return FakePageData.Create();
        }
    }
}
