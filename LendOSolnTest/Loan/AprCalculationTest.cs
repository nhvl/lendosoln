﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;
using DataAccess.Core.Construction;
using LendersOffice.Constants;
using LendOSolnTest.Common;
using LendOSolnTest.Fakes;
using LqbGrammar.DataTypes;
using NUnit.Framework;

namespace LendOSolnTest.Loan
{
    [TestFixture]
    public class AprCalculationTest
    {
        [TestFixtureSetUp]
        public void Setup()
        {
            LoginTools.LoginAs(TestAccountType.LoTest001);
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
            // We don't save, so there should be no need to reset. Uncomment if that ever changes.
            //// FakePageDataUtilities.Instance.Reset();
        }

        [TestCase("1/1/2018", "1/1/2019", 12, 0, 30, "Months equal, one year")]
        [TestCase("1/1/2018", "1/1/2021", 36, 0, 30, "Months equal, three years, including a leap year")]
        [TestCase("8/1/2018", "12/1/2018", 4, 0, 30, "Start month < end month")]
        [TestCase("8/1/2018", "12/1/2020", 28, 0, 30, "Start month < end month, start year < end year")]
        [TestCase("1/1/2018", "1/2/2019", 12, 1, 30, "Start day of month < end day of month")]
        [TestCase("1/30/2018", "1/1/2021", 35, 2, 30, "Start day of month > end day of month, start month = end month, start year < end year")]
        [TestCase("8/1/2018", "1/16/2019", 5, 15, 30, "Start day of month < end day of month, start month > end month, start year < end year")]
        [TestCase("8/31/2018", "1/16/2021", 28, 16, 30, "Start day of month > end day of month, start month > end month, start year < end year")]
        public void FedCalendarPeriodsConstructionPermTest(string startDate, string endDate, int expectedMonths, int expectedDays, int expectedDaysPerMonth, string testDesc)
        {
            DateTime start = DateTime.Parse(startDate);
            DateTime end = DateTime.Parse(endDate);
            var result = FedCalendarPeriods.ConstructionPermPeriodsBetween(start, end);

            Assert.AreEqual(expectedMonths, result.UnitPeriods);
            Assert.AreEqual(expectedDays, result.FractionalPeriods);
            Assert.AreEqual(expectedDaysPerMonth, result.FractionsPerUnitPeriod);
        }

        [TestCase("05/01/2018", "01/01/2019", "02/28/2019", 6, 0)]
        [TestCase("05/01/2018", "01/01/2019", "02/27/2019", 5, 26)]
        [TestCase("05/01/2018", "01/01/2019", "03/01/2019", 6, 0)]
        [TestCase("05/01/2018", "12/01/2018", "02/28/2019", 6, 15)]
        [TestCase("05/01/2018", "12/01/2018", "02/27/2019", 6, 11)]
        [TestCase("05/01/2018", "12/01/2018", "03/01/2019", 6, 15)]
        [TestCase("05/01/2018", "03/01/2019", "04/30/2019", 7, 0)]
        [TestCase("05/01/2018", "03/01/2019", "04/29/2019", 6, 28)]
        [TestCase("05/01/2018", "03/01/2019", "05/01/2019", 7, 0)]
        [TestCase("05/01/2018", "02/01/2019", "04/30/2019", 7, 12)]
        [TestCase("05/01/2018", "02/01/2019", "04/29/2019", 7, 12)]
        [TestCase("05/01/2018", "02/01/2019", "05/01/2019", 7, 15)]
        [TestCase("05/01/2018", "04/01/2019", "05/31/2019", 7, 14)]
        [TestCase("05/01/2018", "04/01/2019", "05/30/2019", 7, 14)]
        [TestCase("05/01/2018", "04/01/2019", "06/01/2019", 7, 15)]
        [TestCase("05/01/2018", "03/01/2019", "05/31/2019", 8, 0)]
        [TestCase("05/01/2018", "03/01/2019", "05/30/2019", 7, 29)]
        [TestCase("05/01/2018", "03/01/2019", "06/01/2019", 8, 0)]

        [TestCase("05/01/2018", "12/31/2018", "02/28/2019", 6, 0)]
        [TestCase("05/01/2018", "12/31/2018", "02/27/2019", 5, 27)]
        [TestCase("05/01/2018", "12/31/2018", "03/01/2019", 6, 1)]
        [TestCase("05/01/2018", "12/21/2018", "02/28/2019", 6, 5)]
        [TestCase("05/01/2018", "12/21/2018", "02/27/2019", 6, 1)]
        [TestCase("05/01/2018", "12/21/2018", "03/01/2019", 6, 6)]

        [TestCase("04/30/2018", "12/31/2018", "02/28/2019", 6, 0)]
        [TestCase("04/30/2018", "12/31/2018", "02/27/2019", 5, 27)]
        [TestCase("04/30/2018", "12/31/2018", "03/01/2019", 6, 1)]
        [TestCase("04/30/2018", "12/30/2018", "02/28/2019", 6, 1)]
        [TestCase("04/30/2018", "12/30/2018", "02/27/2019", 5, 28)]
        [TestCase("04/30/2018", "12/30/2018", "03/01/2019", 6, 2)]
        [TestCase("04/30/2018", "12/29/2018", "02/28/2019", 6, 2)]
        [TestCase("04/30/2018", "12/29/2018", "02/27/2019", 5, 29)]
        [TestCase("04/30/2018", "12/29/2018", "03/01/2019", 6, 3)]
        [TestCase("04/30/2018", "12/21/2018", "02/28/2019", 6, 6)]
        [TestCase("04/30/2018", "12/21/2018", "02/27/2019", 6, 2)]
        [TestCase("04/30/2018", "12/21/2018", "03/01/2019", 6, 7)]

        [TestCase("05/31/2018", "2/28/2019", "04/01/2019", 5, 16)]
        [TestCase("05/30/2018", "2/28/2019", "04/01/2019", 5, 17)]

        [TestCase("01/15/2018", "01/01/2019", "02/01/2019", 6, 24)]
        [TestCase("01/15/2018", "12/31/2018", "02/01/2019", 6, 24)]
        [TestCase("01/15/2018", "12/30/2018", "02/01/2019", 6, 25)]
        [TestCase("01/15/2018", "12/29/2018", "02/01/2019", 6, 25)]
        [TestCase("01/15/2018", "12/28/2018", "02/01/2019", 6, 26)]
        [TestCase("01/15/2018", "12/27/2018", "02/01/2019", 6, 26)]

        [TestCase("02/15/2018", "01/01/2019", "02/01/2019", 6, 7)]
        [TestCase("02/15/2018", "12/31/2018", "02/01/2019", 6, 8)]
        [TestCase("02/15/2018", "12/30/2018", "02/01/2019", 6, 9)]
        [TestCase("02/15/2018", "12/29/2018", "02/01/2019", 6, 10)]
        [TestCase("02/15/2018", "12/28/2018", "02/01/2019", 6, 11)]
        [TestCase("02/15/2018", "12/27/2018", "02/01/2019", 6, 11)]

        [TestCase("05/01/2018", "01/01/2019", "02/01/2019", 5, 0)]
        [TestCase("05/01/2018", "01/01/2019", "01/31/2019", 5, 0)]
        [TestCase("05/01/2018", "01/01/2019", "01/30/2019", 4, 29)]
        [TestCase("05/01/2018", "03/01/2019", "04/01/2019", 6, 0)]
        [TestCase("05/01/2018", "03/01/2019", "03/31/2019", 6, 0)]
        [TestCase("05/01/2018", "03/01/2019", "03/30/2019", 5, 29)]
        [TestCase("05/01/2018", "02/01/2019", "03/01/2019", 5, 15)]
        [TestCase("05/01/2018", "02/01/2019", "02/28/2019", 5, 12)]
        [TestCase("05/01/2018", "02/01/2019", "02/27/2019", 5, 11)]
        public void ConstructionPermFedCalendarTest(string constructionStartDate, string constructionEndDate, string firstPaymentDate, int aprWinUnitPeriods, int aprWinOddDays)
        {
            DateTime start = DateTime.Parse(constructionStartDate);
            DateTime end = DateTime.Parse(constructionEndDate);
            DateTime first = DateTime.Parse(firstPaymentDate);

            var constructionPhasePeriods = FedCalendarPeriods.ConstructionPermPeriodsBetween(start, end);
            var betweenPeriods = FedCalendarPeriods.ConstructionPermPeriodsBetween(end, first).AddUnitPeriods(-1);

            var result = constructionPhasePeriods.HalvePeriods().AddFedCalendarPeriods(betweenPeriods);

            Assert.AreEqual(new Tuple<int, int>(aprWinUnitPeriods, aprWinOddDays), new Tuple<int, int>(result.UnitPeriods + 1, result.FractionalPeriods));
        }

        [TestCase("1/1/2018", "1/1/2019", ConstructionPhaseIntAccrual.Monthly_360_360, 1, 0, 12, false, "Months equal, one year")]
        [TestCase("1/1/2018", "1/1/2021", ConstructionPhaseIntAccrual.Monthly_360_360, 3, 0, 12, false, "Months equal, three years, including a leap year")]
        [TestCase("8/1/2018", "12/1/2018", ConstructionPhaseIntAccrual.Monthly_360_360, 0, 4, 12, false, "Start month < end month")]
        [TestCase("8/1/2018", "12/1/2020", ConstructionPhaseIntAccrual.Monthly_360_360, 2, 4, 12, false, "Start month < end month, start year < end year")]
        [TestCase("1/1/2018", "1/2/2019", ConstructionPhaseIntAccrual.Monthly_360_360, 1, 0, 12, true, "Start day of month < end day of month")]
        [TestCase("1/31/2018", "1/1/2021", ConstructionPhaseIntAccrual.Monthly_360_360, 2, 11, 12, true, "Start day of month > end day of month, start month = end month, start year < end year")]
        [TestCase("8/1/2018", "1/16/2019", ConstructionPhaseIntAccrual.Monthly_360_360, 0, 5, 12, true, "Start day of month < end day of month, start month > end month, start year < end year")]
        [TestCase("8/31/2018", "1/16/2021", ConstructionPhaseIntAccrual.Monthly_360_360, 2, 4, 12, true, "Start day of month > end day of month, start month > end month, start year < end year")]
        [TestCase("2/15/2016", "3/1/2017", ConstructionPhaseIntAccrual.ActualDays_365_365, 1, 15, 365, false, "Leap year test")]
        public void FedCalendarPeriodsConstructionOnlyTest(string startDate, string endDate, ConstructionPhaseIntAccrual accrualType, int expectedUnitPeriods, int expectedFractionalPeriods, int expectedFractionsPerUnitPeriod, bool expectedHasAdditionalOddDays, string testDesc)
        {
            var start = DateTime.Parse(startDate);
            var end = DateTime.Parse(endDate);

            var result = FedCalendarPeriods.ConstructionOnlyPeriodBetween(start, end, accrualType);

            Assert.AreEqual(expectedUnitPeriods, result.UnitPeriods);
            Assert.AreEqual(expectedFractionalPeriods, result.FractionalPeriods);
            Assert.AreEqual(expectedFractionsPerUnitPeriod, result.FractionsPerUnitPeriod);
            Assert.AreEqual(expectedHasAdditionalOddDays, result.HasAdditionalOddDays);
        }

        // Move these somewhere else if we ever use the solvers for something other than APR.
        [Test]
        public void BisectionSolverTest()
        {
            double sqrRoot2 = AprCalculator.SolveForValueUsingBisection(0, 2, 100, x => x * x, 2, 0.00001, 0.00001, "BisectionSolverTest, sqrRoot2 test1");
            Assert.Less(Math.Abs(Math.Sqrt(2) - sqrRoot2), 0.0001);
            double cubeRoot2 = AprCalculator.SolveForValueUsingBisection(0, 2, 100, x => x * x * x, 2, 0.00001, 0.00001, "BisectionSolverTest, cubeRoot2 test1");
            Assert.Less(Math.Abs(Math.Pow(2, 1.0 / 3.0) - cubeRoot2), 0.0001);
            sqrRoot2 = AprCalculator.SolveForValueUsingBisection(0, 2, 100, x => -x * x, -2, 0.00001, 0.00001, "BisectionSolverTest, sqrRoot2 test2");
            Assert.Less(Math.Abs(Math.Sqrt(2) - sqrRoot2), 0.0001);
            cubeRoot2 = AprCalculator.SolveForValueUsingBisection(0, 2, 100, x => -x * x * x, -2, 0.00001, 0.00001, "BisectionSolverTest, cubeRoot2 test2");
            Assert.Less(Math.Abs(Math.Pow(2, 1.0 / 3.0) - cubeRoot2), 0.0001);
        }

        [Test]
        public void BisectionSolverErrorTest()
        {
            try
            {
                double sqrRoot2 = AprCalculator.SolveForValueUsingBisection(-2, 2, 100, x => x * x, 2, 0.00001, 0.00001, "BisectionSolverErrorTest");
            }
            catch (CBaseException ex)
            {
                Assert.That(ex.DeveloperMessage.StartsWith("Bisection bounds do not guarantee a solution"), $"Unexpected message: {ex.Message}, expected 'Bisection bounds do not guarantee a solution'");
                return;
            }

            Assert.Fail("Did not throw the expected exception for a bisection initial bounds check failure.");
        }

        [TestCase(nameof(AprTestCase.FixedNoPpfc))]
        [TestCase(nameof(AprTestCase.FixedWithPpfc))]
        [TestCase(nameof(AprTestCase.FixedWithPpfcAndIrreg1st))]
        [TestCase(nameof(AprTestCase.FixedWithMi))]
        public void AprCalculatorGeneralEquation(string testName)
        {
            var testCase = AprTestCase.GetTestCase(testName);
            var amortTable = new AmortTable(testCase, E_AmortizationScheduleT.Standard, false);

            var pmtList = new List<AprAmountData>();

            int n = 1;
            for (int i = 0; i < amortTable.nRows; ++i)
            {
                AmortItem item = amortTable.Items[i];

                for (int j = 0; j < item.Count; ++j)
                {
                    pmtList.Add(new AprAmountData(item.Pmt, n, testCase.oddDays));
                    ++n;
                }
            }

            double unroundedCalcResult = AprCalculator.CalculateUsingGeneralEquation(
                new List<AprAmountData>() { new AprAmountData(testCase.amountFinanced, 0, 0) },
                pmtList,
                30);

            Assert.AreEqual(testCase.APRWINResult, Math.Round(unroundedCalcResult, 4, MidpointRounding.AwayFromZero), testCase.ComputeAprWinInputs());
        }

        [TestCase(nameof(AprTestCase.FixedNoPpfc))]
        [TestCase(nameof(AprTestCase.FixedWithPpfc))]
        [TestCase(nameof(AprTestCase.FixedWithPpfcAndIrreg1st))]
        [TestCase(nameof(AprTestCase.FixedWithMi))]
        public void AprCalculatorOptimizedStandardLoan(string testName)
        {
            var testCase = AprTestCase.GetTestCase(testName);
            var amortTable = new AmortTable(testCase, E_AmortizationScheduleT.Standard, false);

            var pmtList = new List<decimal>();

            for (int i = 0; i < amortTable.nRows; ++i)
            {
                AmortItem item = amortTable.Items[i];

                for (int j = 0; j < item.Count; ++j)
                {
                    pmtList.Add(item.Pmt);
                }
            }

            double unroundedCalcResult = AprCalculator.CalculateSingleAdvanceRegularRepaymentLoan(
                pmtList,
                testCase.amountFinanced,
                testCase.oddDays,
                30,
                0,
                true,
                (double)testCase.sNoteIR / 1200.0);

            Assert.AreEqual(testCase.APRWINResult, Math.Round(unroundedCalcResult, 4, MidpointRounding.AwayFromZero), testCase.ComputeAprWinInputs());
        }

        [TestCase(nameof(AprTestCase.FixedNoPpfc), 1e17)]
        [TestCase(nameof(AprTestCase.FixedWithPpfc), 1e-17)]
        [TestCase(nameof(AprTestCase.FixedWithPpfcAndIrreg1st), 1e17)]
        [TestCase(nameof(AprTestCase.FixedWithMi), 1e-17)]
        public void AprCalculatorOptimizedStandardLoanFallthrough(string testName, double badGuess)
        {
            var testCase = AprTestCase.GetTestCase(testName);
            var amortTable = new AmortTable(testCase, E_AmortizationScheduleT.Standard, false);

            var pmtList = new List<decimal>();

            for (int i = 0; i < amortTable.nRows; ++i)
            {
                AmortItem item = amortTable.Items[i];

                for (int j = 0; j < item.Count; ++j)
                {
                    pmtList.Add(item.Pmt);
                }
            }

            double unroundedCalcResult = AprCalculator.CalculateSingleAdvanceRegularRepaymentLoan(
                pmtList,
                testCase.amountFinanced,
                testCase.oddDays,
                30,
                0,
                true,
                badGuess);

            Assert.AreEqual(testCase.APRWINResult, Math.Round(unroundedCalcResult, 4, MidpointRounding.AwayFromZero), testCase.ComputeAprWinInputs());
        }

        [Test]
        public void AprConstructionPermCalculation()
        {
            decimal loanAmount = 50000.00M;
            decimal prepaidFinanceCharge = 1000.00M;
            decimal constructionInterestRate = 10.5M;
            int constructionPeriodMonths = 5;
            decimal constructionPhaseInterestFull = (constructionInterestRate / 1200) * constructionPeriodMonths * loanAmount;
            decimal constructionPhaseInterestHalf = constructionPhaseInterestFull / 2;

            var pmtList = new List<decimal>();

            for (int i = 0; i < 360; ++i)
            {
                pmtList.Add(457.37M);
            }

            double unroundedCalcResult;

            unroundedCalcResult = AprCalculator.CalculateOneDisclosureConstructionToPermLoanWithUnknownAdvanceSchedule(
                permPhasePayments: pmtList,
                amountFinanced: loanAmount - prepaidFinanceCharge,
                estimatedConstructionPhaseInterest: constructionPhaseInterestFull,
                constructionStartDate: new DateTime(2018, 01, 01),
                constructionEndDate: new DateTime(2018, 06, 01),
                firstPaymentDate: new DateTime(2018, 07, 01));

            Assert.AreEqual(11.0342M, Math.Round(unroundedCalcResult, 4, MidpointRounding.AwayFromZero));

            unroundedCalcResult = AprCalculator.CalculateOneDisclosureConstructionToPermLoanWithUnknownAdvanceSchedule(
                permPhasePayments: pmtList,
                amountFinanced: loanAmount - prepaidFinanceCharge,
                estimatedConstructionPhaseInterest: constructionPhaseInterestHalf,
                constructionStartDate: new DateTime(2018, 01, 01),
                constructionEndDate: new DateTime(2018, 06, 01),
                firstPaymentDate: new DateTime(2018, 07, 01));

            Assert.AreEqual(10.7523M, Math.Round(unroundedCalcResult, 4, MidpointRounding.AwayFromZero));
        }

        [TestCase(1000.00, 1080.00, 0, 255, 365, 11.4510)] // Example (i) with extra expected decimals from APRWIN
        [TestCase(1000.00, 1044.00, 0, 6, 12, 8.8000)] // Example (ii) with extra expected decimals from APRWIN
        [TestCase(1000.00, 1135.19, 1, 6, 12, 8.7570)] // Example (iii) with extra expected decimals from APRWIN
        [TestCase(1000.00, 1240.00, 2, 0, 1, 11.3553)] // Example (iv) with extra expected decimals from APRWIN
        public void SingleAdvanceSinglePaymentCalculation(double advanceAmount, double paymentAmount, int unitPeriods, int oddDays, int daysPerUnitPeriod, double expectedApr)
        {
            double unroundedResult = AprCalculator.CalculateSingleDrawSinglePaymentLoan((decimal)advanceAmount, (decimal)paymentAmount, unitPeriods, oddDays, daysPerUnitPeriod);

            Assert.AreEqual(Math.Round((decimal)expectedApr, 4, MidpointRounding.AwayFromZero), Math.Round(unroundedResult, 4, MidpointRounding.AwayFromZero));
        }

        [TestCase(50000.00, 1000.00, 0, 1093.75, "2018-01-01", "2018-06-01", ConstructionPhaseIntAccrual.Monthly_360_360, 20.9375)] // Begin dougt test cases
        [TestCase(50000.00, 1000.00, 0, 2187.50, "2018-01-01", "2018-06-01", ConstructionPhaseIntAccrual.Monthly_360_360, 31.8750)]
        public void ConstructionOnlyCalculation(double commitmentAmount, double prepaidFinanceCharges, double subsequentFinanceChargesAmt, double estimatedConstructionPhaseInterest, string startDate, string endDate, ConstructionPhaseIntAccrual accrualType, double expectedApr)
        {
            DateTime start = DateTime.Parse(startDate);
            DateTime end = DateTime.Parse(endDate);

            double unroundedResult = AprCalculator.CalculateConstructionPhaseOnly((decimal)commitmentAmount, (decimal)prepaidFinanceCharges, (decimal)(estimatedConstructionPhaseInterest + subsequentFinanceChargesAmt), start, end, accrualType);

            Assert.AreEqual(Math.Round((decimal)expectedApr, 4, MidpointRounding.AwayFromZero), Math.Round(unroundedResult, 4, MidpointRounding.AwayFromZero));
        }

        [TestCase(nameof(AprConstructionTestCase.aloCase001))]
        [TestCase(nameof(AprConstructionTestCase.aloCase002))]
        [TestCase(nameof(AprConstructionTestCase.aloCase003))]
        [TestCase(nameof(AprConstructionTestCase.aloCase004))]
        [TestCase(nameof(AprConstructionTestCase.aloCase005))]
        [TestCase(nameof(AprConstructionTestCase.aloCase006))]
        [TestCase(nameof(AprConstructionTestCase.aloCase007))]
        [TestCase(nameof(AprConstructionTestCase.aloCase008))]
        [TestCase(nameof(AprConstructionTestCase.aloCase009))]
        [TestCase(nameof(AprConstructionTestCase.aloCase010))]
        [TestCase(nameof(AprConstructionTestCase.aloCase011))]
        [TestCase(nameof(AprConstructionTestCase.aloCase012))]
        [TestCase(nameof(AprConstructionTestCase.aloCase013))]
        [TestCase(nameof(AprConstructionTestCase.aloCase014))]
        [TestCase(nameof(AprConstructionTestCase.aloCase015))]
        [TestCase(nameof(AprConstructionTestCase.aloCase016))]
        [TestCase(nameof(AprConstructionTestCase.aloCase017))]
        [TestCase(nameof(AprConstructionTestCase.aloCase018))]
        [TestCase(nameof(AprConstructionTestCase.aloCase019))]
        [TestCase(nameof(AprConstructionTestCase.aloCase020))]
        [TestCase(nameof(AprConstructionTestCase.aloCase021))]
        [TestCase(nameof(AprConstructionTestCase.aloCase022))]
        [TestCase(nameof(AprConstructionTestCase.aloCase023))]
        [TestCase(nameof(AprConstructionTestCase.aloCase024))]
        [TestCase(nameof(AprConstructionTestCase.aloCase025))]
        [TestCase(nameof(AprConstructionTestCase.aloCase026))]
        [TestCase(nameof(AprConstructionTestCase.aloCase027))]
        [TestCase(nameof(AprConstructionTestCase.aloCase028))]
        [TestCase(nameof(AprConstructionTestCase.aloCase029))]
        [TestCase(nameof(AprConstructionTestCase.aloCase030))]
        [TestCase(nameof(AprConstructionTestCase.aloCase031))]
        //[TestCase(nameof(AprConstructionTestCase.aloCase032))] This was testing something else, and will always fail.
        [TestCase(nameof(AprConstructionTestCase.aloCase033))]
        [TestCase(nameof(AprConstructionTestCase.aloCase034))]
        [TestCase(nameof(AprConstructionTestCase.aloCase035))]
        [TestCase(nameof(AprConstructionTestCase.aloCase036))]
        [TestCase(nameof(AprConstructionTestCase.aloCase037))]
        [TestCase(nameof(AprConstructionTestCase.aloCase038))]
        [TestCase(nameof(AprConstructionTestCase.aloCase039))]
        [TestCase(nameof(AprConstructionTestCase.aloCase040))]
        [TestCase(nameof(AprConstructionTestCase.aloCase041))]
        [TestCase(nameof(AprConstructionTestCase.aloCase042))]

        [TestCase(nameof(AprConstructionTestCase.aloCase043))]
        [TestCase(nameof(AprConstructionTestCase.aloCase044))]
        [TestCase(nameof(AprConstructionTestCase.aloCase045))]
        [TestCase(nameof(AprConstructionTestCase.aloCase046))]
        [TestCase(nameof(AprConstructionTestCase.aloCase047))]
        [TestCase(nameof(AprConstructionTestCase.aloCase048))]
        [TestCase(nameof(AprConstructionTestCase.aloCase049))]
        [TestCase(nameof(AprConstructionTestCase.aloCase050))]
        [TestCase(nameof(AprConstructionTestCase.aloCase051))]
        [TestCase(nameof(AprConstructionTestCase.aloCase052))]
        [TestCase(nameof(AprConstructionTestCase.aloCase053))]
        [TestCase(nameof(AprConstructionTestCase.aloCase054))]
        [TestCase(nameof(AprConstructionTestCase.aloCase055))]
        [TestCase(nameof(AprConstructionTestCase.aloCase056))]
        [TestCase(nameof(AprConstructionTestCase.aloCase057))]
        [TestCase(nameof(AprConstructionTestCase.aloCase058))]
        [TestCase(nameof(AprConstructionTestCase.aloCase059))]
        [TestCase(nameof(AprConstructionTestCase.aloCase060))]
        [TestCase(nameof(AprConstructionTestCase.aloCase061))]
        [TestCase(nameof(AprConstructionTestCase.aloCase062))]
        [TestCase(nameof(AprConstructionTestCase.aloCase063))]
        [TestCase(nameof(AprConstructionTestCase.aloCase064))]
        [TestCase(nameof(AprConstructionTestCase.aloCase065))]
        [TestCase(nameof(AprConstructionTestCase.aloCase066))]

        [TestCase(nameof(AprConstructionTestCase.aloCase068))]
        [TestCase(nameof(AprConstructionTestCase.aloCase070))]
        [TestCase(nameof(AprConstructionTestCase.aloCase072))]
        [TestCase(nameof(AprConstructionTestCase.aloCase074))]
        [TestCase(nameof(AprConstructionTestCase.aloCase076))]
        [TestCase(nameof(AprConstructionTestCase.aloCase078))]
        [TestCase(nameof(AprConstructionTestCase.aloCase080))]
        [TestCase(nameof(AprConstructionTestCase.aloCase082))]
        [TestCase(nameof(AprConstructionTestCase.aloCase084))]
        [TestCase(nameof(AprConstructionTestCase.aloCase086))]
        public void ConstructionOnlyCalculationAloTests(string testID)
        {
            var testCase = AprConstructionTestCase.GetTestCase(testID);

            var start = DateTime.Parse(testCase.StartDate);
            var end = DateTime.Parse(testCase.EndDate);
            var accrualType = testCase.sConstructionPhaseIntAccrualT;
            var periods = FedCalendarPeriods.ConstructionOnlyPeriodBetween(start, end, accrualType);

            var constructionMonthsPeriods = FedCalendarPeriods.WholeMonthsAndDaysBetween(start, end);

            int sConstructionPeriodMon = constructionMonthsPeriods.UnitPeriods;
            DateTime sConstructionIntAccrualD = start.AddDays(constructionMonthsPeriods.FractionalPeriods);

            var expectedKey = new Tuple<DateTime, DateTime, ConstructionPhaseIntAccrual>(start, end, accrualType == ConstructionPhaseIntAccrual.Monthly_360_360 ? ConstructionPhaseIntAccrual.Monthly_360_360 : ConstructionPhaseIntAccrual.ActualDays_365_360);

            Assert.IsTrue(FedCalendarExpectedResultsConstructionOnly.Value.ContainsKey(expectedKey), $"Could not find the expected Fed Calendar result for test ID {testID}");

            var expectedResult = FedCalendarExpectedResultsConstructionOnly.Value[expectedKey];

            Assert.AreEqual(expectedResult.Item1, periods.UnitPeriods, $"Mismatched unit periods for test ID {testID}, start: {testCase.StartDate}, end: {testCase.EndDate}");
            Assert.AreEqual(expectedResult.Item2, periods.FractionalPeriods, $"Mismatched fractional periods for test ID {testID}, start: {testCase.StartDate}, end: {testCase.EndDate}");
            Assert.AreEqual(expectedResult.Item3, periods.FractionsPerUnitPeriod, $"Mismatched fractions per unit periods for test ID {testID}, start: {testCase.StartDate}, end: {testCase.EndDate}");
            Assert.AreEqual(expectedResult.Item4, periods.HasAdditionalOddDays, $"Mismatched additional days indicator for test ID {testID}, start: {testCase.StartDate}, end: {testCase.EndDate}");

            var dataLoan = FakePageData.Create();
            dataLoan.InitLoad();

            dataLoan.sLPurposeT = E_sLPurposeT.Construct;
            dataLoan.sLOrigFMb = testCase.sAprIncludedCc;
            dataLoan.sLOrigFProps_Apr = true;
            dataLoan.sIPiaDyLckd = true;
            dataLoan.sIPiaDy = 0;
            dataLoan.sSubsequentlyPaidFinanceChargeAmt_rep = dataLoan.m_convertLos.ToMoneyString(testCase.sSubsequentlyPaidFinanceChargeAmt, FormatDirection.ToRep);
            dataLoan.sLAmtLckd = true;
            dataLoan.sLAmtCalc = testCase.sLAmtCalc;
            dataLoan.sConstructionPeriodIR = testCase.sConstructionPeriodIR;
            dataLoan.sConstructionIntCalcT = testCase.sConstructionIntCalcT;
            dataLoan.sConstructionPhaseIntAccrualT = testCase.sConstructionPhaseIntAccrualT;
            dataLoan.sConstructionLoanDLckd = true;
            dataLoan.sConstructionLoanD_rep = testCase.StartDate;
            dataLoan.sConstructionIntAccrualDLckd = true;
            dataLoan.sConstructionIntAccrualD_rep = dataLoan.m_convertLos.ToDateTimeString(sConstructionIntAccrualD);
            dataLoan.sConstructionPeriodMon = sConstructionPeriodMon;
            dataLoan.sIsIntReserveRequired = testCase.sIsIntReserveRequried;

            Assert.AreEqual(end, DateTime.Parse(dataLoan.sConstructionPeriodEndD_rep));

            Assert.AreEqual(dataLoan.m_convertLos.ToMoneyString(testCase.sConstructionIntAmount, FormatDirection.ToRep), dataLoan.sConstructionIntAmount_rep, $"Construction interest amount mismatch for test ID {testID}");

            decimal unroundedResult = dataLoan.sAprUnrounded;

            Assert.AreEqual(Math.Round(testCase.APRWINResult, 4, MidpointRounding.AwayFromZero), Math.Round(unroundedResult, 4, MidpointRounding.AwayFromZero));
        }

        [TestCase(nameof(AprConstructionPermTestCase.aloCase100))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase101))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase102))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase103))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase104))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase105))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase106))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase107))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase108))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase109))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase110))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase111))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase112))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase113))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase114))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase115))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase116))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase117))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase118))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase119))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase120))]

        [TestCase(nameof(AprConstructionPermTestCase.aloCase121))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase122))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase123))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase124))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase125))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase126))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase127))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase128))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase129))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase130))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase131))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase132))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase133))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase134))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase135))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase136))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase137))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase138))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase139))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase140))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase141))]

        [TestCase(nameof(AprConstructionPermTestCase.aloCase142))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase143))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase144))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase145))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase146))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase147))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase148))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase149))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase150))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase151))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase152))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase153))]
        [TestCase(nameof(AprConstructionPermTestCase.aloCase154))]
        public void ConstructionPermCalculationAloTests(string testID)
        {
            var testCase = AprConstructionPermTestCase.GetTestCase(testID);

            var start = DateTime.Parse(testCase.StartDate);
            var end = DateTime.Parse(testCase.EndDate);
            var accrualType = testCase.sConstructionPhaseIntAccrualT;
            var periods = FedCalendarPeriods.ConstructionPermPeriodsBetween(start, end);

            var constructionMonthsPeriods = FedCalendarPeriods.WholeMonthsAndDaysBetween(start, end);

            int sConstructionPeriodMon = constructionMonthsPeriods.UnitPeriods;
            DateTime sConstructionIntAccrualD = start.AddDays(constructionMonthsPeriods.FractionalPeriods);

            var expectedKey = new Tuple<DateTime, DateTime>(start, end);

            Assert.IsTrue(FedCalendarExpectedResultsConstructionPerm.Value.ContainsKey(expectedKey), $"Could not find the expected Fed Calendar result for test ID {testID}");

            var expectedResult = FedCalendarExpectedResultsConstructionPerm.Value[expectedKey];

            Assert.AreEqual(expectedResult.Item1, periods.UnitPeriods, $"Mismatched unit periods for test ID {testID}, start: {testCase.StartDate}, end: {testCase.EndDate}");
            Assert.AreEqual(expectedResult.Item2, periods.FractionalPeriods, $"Mismatched fractional periods for test ID {testID}, start: {testCase.StartDate}, end: {testCase.EndDate}");
            Assert.AreEqual(expectedResult.Item3, periods.FractionsPerUnitPeriod, $"Mismatched fractions per unit periods for test ID {testID}, start: {testCase.StartDate}, end: {testCase.EndDate}");
            Assert.AreEqual(expectedResult.Item4, periods.HasAdditionalOddDays, $"Mismatched additional days indicator for test ID {testID}, start: {testCase.StartDate}, end: {testCase.EndDate}");

            var dataLoan = FakePageData.Create();
            dataLoan.InitLoad();

            dataLoan.sLPurposeT = E_sLPurposeT.ConstructPerm;
            dataLoan.sLOrigFMb = testCase.sAprIncludedCc;
            dataLoan.sLOrigFProps_Apr = true;
            dataLoan.sIPiaDyLckd = true;
            dataLoan.sIPiaDy = 0;
            dataLoan.sSubsequentlyPaidFinanceChargeAmt_rep = dataLoan.m_convertLos.ToMoneyString(testCase.sSubsequentlyPaidFinanceChargeAmt, FormatDirection.ToRep);
            dataLoan.sLAmtLckd = true;
            dataLoan.sLAmtCalc = testCase.sLAmtCalc;
            dataLoan.sConstructionPeriodIR = testCase.sConstructionPeriodIR;
            dataLoan.sConstructionIntCalcT = testCase.sConstructionIntCalcT;
            dataLoan.sConstructionPhaseIntAccrualT = testCase.sConstructionPhaseIntAccrualT;
            dataLoan.sConstructionLoanDLckd = true;
            dataLoan.sConstructionLoanD_rep = testCase.StartDate;
            dataLoan.sConstructionIntAccrualDLckd = true;
            dataLoan.sConstructionIntAccrualD_rep = dataLoan.m_convertLos.ToDateTimeString(sConstructionIntAccrualD);
            dataLoan.sConstructionPeriodMon = sConstructionPeriodMon;
            dataLoan.sSchedDueD1Lckd = true;
            dataLoan.sSchedDueD1_rep = dataLoan.m_convertLos.ToDateTimeString(DateTime.Parse(dataLoan.sConstructionPeriodEndD_rep).AddMonths(1));
            dataLoan.sIsIntReserveRequired = testCase.sIsIntReserveRequried;

            dataLoan.sLAmtLckd = true;
            dataLoan.sLAmtCalc = testCase.sLAmtCalc;
            dataLoan.sNoteIR = testCase.sNoteIR;
            dataLoan.sTerm = testCase.sTerm;
            dataLoan.sDue = testCase.sTerm;

            Assert.AreEqual(end, DateTime.Parse(dataLoan.sConstructionPeriodEndD_rep));

            Assert.AreEqual(testCase.FirstPmt, dataLoan.sAmortTable.Items[0].Payment);
            Assert.That(Math.Abs(dataLoan.sAmortTable.Items[1].Payment- testCase.LastPmt) < 1.00M);

            Assert.AreEqual(dataLoan.m_convertLos.ToMoneyString(testCase.sConstructionIntAmount, FormatDirection.ToRep), dataLoan.sConstructionIntAmount_rep, $"Construction interest amount mismatch for test ID {testID}");

            decimal unroundedResult = dataLoan.sAprUnrounded;

            Assert.AreEqual(Math.Round(testCase.APRWINResult, 4, MidpointRounding.AwayFromZero), Math.Round(unroundedResult, 4, MidpointRounding.AwayFromZero));
        }

        [Test]
        public void ConstructionAprNotAvailableIfIllegalOddDays()
        {
            var dataLoan = FakePageData.Create();

            dataLoan.sLPurposeT = E_sLPurposeT.Construct;
            dataLoan.sLAmtLckd = true;
            dataLoan.sLAmtCalc = 100000.00M;
            dataLoan.sConstructionPeriodIR = 6.000M;
            dataLoan.sConstructionIntCalcT = ConstructionIntCalcType.FullCommitment;
            dataLoan.sConstructionPhaseIntAccrualT = ConstructionPhaseIntAccrual.Monthly_360_360;
            dataLoan.sConstructionLoanDLckd = true;
            dataLoan.sConstructionLoanD_rep = "1/1/2018";
            dataLoan.sConstructionIntAccrualDLckd = true;
            dataLoan.sConstructionIntAccrualD_rep = "1/1/2018";
            dataLoan.sConstructionPeriodMon = 6;

            Assert.IsTrue(!string.IsNullOrWhiteSpace(dataLoan.sApr_rep));

            dataLoan.sConstructionIntAccrualD_rep = "1/15/2018";

            Assert.AreEqual(string.Empty, dataLoan.sApr_rep, "Construction only APR should be blank with illegal odd days");

            dataLoan.sLPurposeT = E_sLPurposeT.ConstructPerm;
            dataLoan.sLAmtLckd = true;
            dataLoan.sLAmtCalc = 100000.00M;
            dataLoan.sNoteIR = 6.000M;
            dataLoan.sConstructionIntAccrualD_rep = "1/1/2018";

            Assert.IsTrue(!string.IsNullOrWhiteSpace(dataLoan.sApr_rep));

            dataLoan.sConstructionIntAccrualD_rep = "1/15/2018";

            Assert.AreEqual(string.Empty, dataLoan.sApr_rep, "Construction-Perm APR should be blank with illegal odd days");
        }

        [TestCase(nameof(AprTestCase.FixedNoPpfc))]
        [TestCase(nameof(AprTestCase.FixedWithPpfc))]
        [TestCase(nameof(AprTestCase.FixedWithPpfcAndIrreg1st))]
        [TestCase(nameof(AprTestCase.FixedWithMi))]
        public void MainLoanApr(string testName)
        {
            FakePageData dataLoan = FakePageData.Create();
            dataLoan.InitLoad();

            var testCase = AprTestCase.GetTestCase(testName);

            Assert.AreEqual(testCase.APRWINResult, Math.Round(testCase.ComputeLoanApr(dataLoan), 4, MidpointRounding.AwayFromZero), testCase.ComputeAprWinInputs());
        }

        [TestCase(nameof(AprTestCase.FixedNoPpfc))]
        [TestCase(nameof(AprTestCase.FixedWithPpfc))]
        [TestCase(nameof(AprTestCase.FixedWithPpfcAndIrreg1st))]
        [TestCase(nameof(AprTestCase.FixedWithMi))]
        public void TestPricingLoanApr(string testName)
        {
            FakePageData dataLoan = FakePageData.Create();
            dataLoan.InitLoad();

            var testCase = AprTestCase.GetTestCase(testName);

            Assert.AreEqual(testCase.APRWINResult, Math.Round(testCase.ComputeLoanAprPricing(dataLoan), 4, MidpointRounding.AwayFromZero), testCase.ComputeAprWinInputs());
        }

        [Test]
        public void TestAprWinRegularInput()
        {
            var inputs = new AprWinInputs.AprWin_6_2();

            inputs.RegularInputs.AmountFinanced = 99000.00M;
            inputs.RegularInputs.DisclosedOrEstimatedApr = 10.000M;
            inputs.RegularInputs.DisclosedFinanceCharge = -10.00M;

            inputs.RegularInputs.InstallmentLoan.PaymentFrequency = AprWinInputs.AprWin_6_2.PaymentFrequencyOptions.Monthly;

            inputs.RegularInputs.PaymentSchedule.LoanDate = UnzonedDate.Create("2015-12-31").Value;
            inputs.RegularInputs.PaymentSchedule.PaymentAdvanceDate = UnzonedDate.Create("2016-02-01").Value;

            inputs.RegularInputs.PaymentSchedule.PaymentList = new List<AprAmountData>() { new AprAmountData(500, 1, 15), new AprAmountData(500, 2, 15), new AprAmountData(500, 3, 15), new AprAmountData(501, 4, 15), new AprAmountData(501, 5, 15), new AprAmountData(502, 6, 15), new AprAmountData(500, 7, 15) };

            Assert.NotNull(inputs.GenerateInputs(0));
        }

        [Test]
        public void TestAprWinConstructionInput()
        {
            var inputs = new AprWinInputs.AprWin_6_2();

            inputs.ConstructionInputs.LoanCommitmentAmount = 100000.00M;
            inputs.ConstructionInputs.AnnualSimpleInterestRate = 10.000M;
            inputs.ConstructionInputs.PrepaidFinanceCharge = 1000.00M;
            inputs.ConstructionInputs.SubsequentlyPaidFinanceCharge = 100.00M;
            inputs.ConstructionInputs.InterestPayable = AprWinInputs.AprWin_6_2.InterestPayableOptions.OnEntireCommitment;

            inputs.ConstructionInputs.ConstructionSeparately.ConstructionPhaseInterestAccrual = AprWinInputs.AprWin_6_2.ConstructionPhaseInterestAccrualOptions.ActualDays365_365;
            inputs.ConstructionInputs.ConstructionSeparately.RequiredInterestReserve = true;
            inputs.ConstructionInputs.ConstructionSeparately.LoanDate = UnzonedDate.Create("2013-02-01").Value;
            inputs.ConstructionInputs.ConstructionSeparately.ConstructionPeriodEndDate = UnzonedDate.Create("2013-07-01").Value;
            inputs.ConstructionInputs.ConstructionSeparately.NumberOfDays = 180;
            inputs.ConstructionInputs.ConstructionSeparately.NumberOfUnitPeriods = 1;
            inputs.ConstructionInputs.ConstructionSeparately.NumberOfOddDays = 0;
            inputs.ConstructionInputs.ConstructionSeparately.EstimatedConstructionInterest = 5000.00M;
            inputs.ConstructionInputs.ConstructionSeparately.DisclosedOrEstimatedApr = 10.000M;
            inputs.ConstructionInputs.ConstructionSeparately.DisclosedFinanceCharge = -1.23M;

            Assert.NotNull(inputs.GenerateInputs(0));
        }

        [Test]
        public void TestAprWinConstructionPermInput()
        {
            var inputs = new AprWinInputs.AprWin_6_2();

            inputs.ConstructionInputs.LoanCommitmentAmount = 100000.00M;
            inputs.ConstructionInputs.AnnualSimpleInterestRate = 10.000M;
            inputs.ConstructionInputs.PrepaidFinanceCharge = 1000.00M;
            inputs.ConstructionInputs.SubsequentlyPaidFinanceCharge = 100.00M;
            inputs.ConstructionInputs.InterestPayable = AprWinInputs.AprWin_6_2.InterestPayableOptions.OnAdvancesWhenMade;

            inputs.ConstructionInputs.ConstructionAndPermanent.ConstructionPhaseInterestAccrual = AprWinInputs.AprWin_6_2.ConstructionPhaseInterestAccrualOptions.WholeMonths360_360;
            inputs.ConstructionInputs.ConstructionAndPermanent.RequiredInterestReserve = false;
            inputs.ConstructionInputs.ConstructionAndPermanent.LoanDate = UnzonedDate.Create("2016-09-01").Value;
            inputs.ConstructionInputs.ConstructionAndPermanent.ConstructionPeriodEndDate = UnzonedDate.Create("2017-06-01").Value;
            inputs.ConstructionInputs.ConstructionAndPermanent.NumberOfMonths = 9;
            inputs.ConstructionInputs.ConstructionAndPermanent.EstimatedConstructionInterest = 9000.00M;
            inputs.ConstructionInputs.ConstructionAndPermanent.DisclosedOrEstimatedApr = 10.000M;
            inputs.ConstructionInputs.ConstructionAndPermanent.DisclosedFinanceCharge = 123.45M;
            inputs.ConstructionInputs.ConstructionAndPermanent.LoanType = AprWinInputs.AprWin_6_2.LoanPaymentTypeOptions.Installment;

            inputs.ConstructionInputs.ConstructionAndPermanent.InstallmentLoan.PaymentFrequency = AprWinInputs.AprWin_6_2.PaymentFrequencyOptions.MultiplesOfAMonth;
            inputs.ConstructionInputs.ConstructionAndPermanent.InstallmentLoan.MonthsInAUnitPeriod = 3;

            inputs.ConstructionInputs.ConstructionAndPermanent.PaymentSchedule.LoanDate = UnzonedDate.Create("2016-09-01").Value;
            inputs.ConstructionInputs.ConstructionAndPermanent.PaymentSchedule.ConstructionPeriodEndDate = UnzonedDate.Create("2017-06-01").Value;
            inputs.ConstructionInputs.ConstructionAndPermanent.PaymentSchedule.AmortizationPaymentDate = UnzonedDate.Create("2017-07-01").Value;

            inputs.ConstructionInputs.ConstructionAndPermanent.PaymentSchedule.PaymentList = new List<AprAmountData>() { new AprAmountData(650, 5, 15), new AprAmountData(650, 6, 15), new AprAmountData(700, 7, 15), new AprAmountData(700, 8, 15), new AprAmountData(650, 9, 15), new AprAmountData(700, 10, 15), new AprAmountData(700, 11, 15) };

            Assert.NotNull(inputs.GenerateInputs(0));
        }


        private static Lazy<Dictionary<Tuple<DateTime, DateTime, ConstructionPhaseIntAccrual>, Tuple<int, int, int, bool>>> FedCalendarExpectedResultsConstructionOnly = new Lazy<Dictionary<Tuple<DateTime, DateTime, ConstructionPhaseIntAccrual>, Tuple<int, int, int, bool>>>(() =>
        {
            var result = new Dictionary<Tuple<DateTime, DateTime, ConstructionPhaseIntAccrual>, Tuple<int, int, int, bool>>();

            result.Add(new Tuple<DateTime, DateTime, ConstructionPhaseIntAccrual>(DateTime.Parse("4/20/2018"), DateTime.Parse("11/1/2018"), ConstructionPhaseIntAccrual.ActualDays_365_360), new Tuple<int, int, int, bool>(0, 195, 365, false));
            result.Add(new Tuple<DateTime, DateTime, ConstructionPhaseIntAccrual>(DateTime.Parse("5/1/2018"), DateTime.Parse("11/1/2018"), ConstructionPhaseIntAccrual.ActualDays_365_360), new Tuple<int, int, int, bool>(0, 184, 365, false));
            result.Add(new Tuple<DateTime, DateTime, ConstructionPhaseIntAccrual>(DateTime.Parse("4/20/2018"), DateTime.Parse("2/1/2019"), ConstructionPhaseIntAccrual.ActualDays_365_360), new Tuple<int, int, int, bool>(0, 287, 365, false));
            result.Add(new Tuple<DateTime, DateTime, ConstructionPhaseIntAccrual>(DateTime.Parse("5/1/2018"), DateTime.Parse("2/1/2019"), ConstructionPhaseIntAccrual.ActualDays_365_360), new Tuple<int, int, int, bool>(0, 276, 365, false));
            result.Add(new Tuple<DateTime, DateTime, ConstructionPhaseIntAccrual>(DateTime.Parse("4/20/2018"), DateTime.Parse("4/1/2019"), ConstructionPhaseIntAccrual.ActualDays_365_360), new Tuple<int, int, int, bool>(0, 346, 365, false));
            result.Add(new Tuple<DateTime, DateTime, ConstructionPhaseIntAccrual>(DateTime.Parse("5/1/2018"), DateTime.Parse("4/1/2019"), ConstructionPhaseIntAccrual.ActualDays_365_360), new Tuple<int, int, int, bool>(0, 335, 365, false));
            result.Add(new Tuple<DateTime, DateTime, ConstructionPhaseIntAccrual>(DateTime.Parse("4/20/2018"), DateTime.Parse("5/1/2019"), ConstructionPhaseIntAccrual.ActualDays_365_360), new Tuple<int, int, int, bool>(1, 11, 365, false));
            result.Add(new Tuple<DateTime, DateTime, ConstructionPhaseIntAccrual>(DateTime.Parse("5/1/2018"), DateTime.Parse("5/1/2019"), ConstructionPhaseIntAccrual.ActualDays_365_360), new Tuple<int, int, int, bool>(1, 0, 365, false));
            result.Add(new Tuple<DateTime, DateTime, ConstructionPhaseIntAccrual>(DateTime.Parse("4/20/2018"), DateTime.Parse("11/1/2019"), ConstructionPhaseIntAccrual.ActualDays_365_360), new Tuple<int, int, int, bool>(1, 195, 365, false));
            result.Add(new Tuple<DateTime, DateTime, ConstructionPhaseIntAccrual>(DateTime.Parse("5/1/2018"), DateTime.Parse("11/1/2019"), ConstructionPhaseIntAccrual.ActualDays_365_360), new Tuple<int, int, int, bool>(1, 184, 365, false));
            result.Add(new Tuple<DateTime, DateTime, ConstructionPhaseIntAccrual>(DateTime.Parse("4/20/2018"), DateTime.Parse("5/1/2020"), ConstructionPhaseIntAccrual.ActualDays_365_360), new Tuple<int, int, int, bool>(2, 11, 365, false));
            result.Add(new Tuple<DateTime, DateTime, ConstructionPhaseIntAccrual>(DateTime.Parse("5/1/2018"), DateTime.Parse("5/1/2020"), ConstructionPhaseIntAccrual.ActualDays_365_360), new Tuple<int, int, int, bool>(2, 0, 365, false));
            result.Add(new Tuple<DateTime, DateTime, ConstructionPhaseIntAccrual>(DateTime.Parse("11/12/2010"), DateTime.Parse("6/1/2011"), ConstructionPhaseIntAccrual.ActualDays_365_360), new Tuple<int, int, int, bool>(0, 201, 365, false));
            result.Add(new Tuple<DateTime, DateTime, ConstructionPhaseIntAccrual>(DateTime.Parse("4/6/2007"), DateTime.Parse("11/1/2007"), ConstructionPhaseIntAccrual.ActualDays_365_360), new Tuple<int, int, int, bool>(0, 209, 365, false));
            result.Add(new Tuple<DateTime, DateTime, ConstructionPhaseIntAccrual>(DateTime.Parse("5/20/2018"), DateTime.Parse("5/20/2020"), ConstructionPhaseIntAccrual.ActualDays_365_360), new Tuple<int, int, int, bool>(2, 0, 365, false));
            result.Add(new Tuple<DateTime, DateTime, ConstructionPhaseIntAccrual>(DateTime.Parse("4/20/2018"), DateTime.Parse("5/20/2020"), ConstructionPhaseIntAccrual.ActualDays_365_360), new Tuple<int, int, int, bool>(2, 30, 365, false));
            result.Add(new Tuple<DateTime, DateTime, ConstructionPhaseIntAccrual>(DateTime.Parse("1/20/2016"), DateTime.Parse("2/1/2017"), ConstructionPhaseIntAccrual.ActualDays_365_360), new Tuple<int, int, int, bool>(1, 12, 365, false));
            result.Add(new Tuple<DateTime, DateTime, ConstructionPhaseIntAccrual>(DateTime.Parse("2/1/2016"), DateTime.Parse("2/1/2017"), ConstructionPhaseIntAccrual.ActualDays_365_360), new Tuple<int, int, int, bool>(1, 0, 365, false));
            result.Add(new Tuple<DateTime, DateTime, ConstructionPhaseIntAccrual>(DateTime.Parse("1/20/2016"), DateTime.Parse("8/1/2016"), ConstructionPhaseIntAccrual.ActualDays_365_360), new Tuple<int, int, int, bool>(0, 194, 365, false));
            result.Add(new Tuple<DateTime, DateTime, ConstructionPhaseIntAccrual>(DateTime.Parse("2/1/2016"), DateTime.Parse("8/1/2016"), ConstructionPhaseIntAccrual.ActualDays_365_360), new Tuple<int, int, int, bool>(0, 182, 365, false));

            result.Add(new Tuple<DateTime, DateTime, ConstructionPhaseIntAccrual>(DateTime.Parse("5/1/2018"), DateTime.Parse("11/1/2018"), ConstructionPhaseIntAccrual.Monthly_360_360), new Tuple<int, int, int, bool>(0, 6, 12, false));
            result.Add(new Tuple<DateTime, DateTime, ConstructionPhaseIntAccrual>(DateTime.Parse("5/1/2018"), DateTime.Parse("2/1/2019"), ConstructionPhaseIntAccrual.Monthly_360_360), new Tuple<int, int, int, bool>(0, 9, 12, false));
            result.Add(new Tuple<DateTime, DateTime, ConstructionPhaseIntAccrual>(DateTime.Parse("5/1/2018"), DateTime.Parse("5/1/2018"), ConstructionPhaseIntAccrual.Monthly_360_360), new Tuple<int, int, int, bool>(1, 0, 12, false));
            result.Add(new Tuple<DateTime, DateTime, ConstructionPhaseIntAccrual>(DateTime.Parse("5/1/2018"), DateTime.Parse("11/1/2019"), ConstructionPhaseIntAccrual.Monthly_360_360), new Tuple<int, int, int, bool>(1, 6, 12, false));
            result.Add(new Tuple<DateTime, DateTime, ConstructionPhaseIntAccrual>(DateTime.Parse("5/1/2018"), DateTime.Parse("5/1/2019"), ConstructionPhaseIntAccrual.Monthly_360_360), new Tuple<int, int, int, bool>(1, 0, 12, false));
            result.Add(new Tuple<DateTime, DateTime, ConstructionPhaseIntAccrual>(DateTime.Parse("5/1/2018"), DateTime.Parse("5/1/2020"), ConstructionPhaseIntAccrual.Monthly_360_360), new Tuple<int, int, int, bool>(2, 0, 12, false));
            result.Add(new Tuple<DateTime, DateTime, ConstructionPhaseIntAccrual>(DateTime.Parse("4/20/2018"), DateTime.Parse("11/1/2018"), ConstructionPhaseIntAccrual.Monthly_360_360), new Tuple<int, int, int, bool>(0, 6, 12, true));
            result.Add(new Tuple<DateTime, DateTime, ConstructionPhaseIntAccrual>(DateTime.Parse("4/20/2018"), DateTime.Parse("2/1/2019"), ConstructionPhaseIntAccrual.Monthly_360_360), new Tuple<int, int, int, bool>(0, 9, 12, true));
            result.Add(new Tuple<DateTime, DateTime, ConstructionPhaseIntAccrual>(DateTime.Parse("4/20/2018"), DateTime.Parse("5/1/2018"), ConstructionPhaseIntAccrual.Monthly_360_360), new Tuple<int, int, int, bool>(1, 0, 12, true));
            result.Add(new Tuple<DateTime, DateTime, ConstructionPhaseIntAccrual>(DateTime.Parse("4/20/2018"), DateTime.Parse("11/1/2019"), ConstructionPhaseIntAccrual.Monthly_360_360), new Tuple<int, int, int, bool>(1, 6, 12, true));
            result.Add(new Tuple<DateTime, DateTime, ConstructionPhaseIntAccrual>(DateTime.Parse("4/20/2018"), DateTime.Parse("5/1/2020"), ConstructionPhaseIntAccrual.Monthly_360_360), new Tuple<int, int, int, bool>(2, 0, 12, true));

            return result;
        });

        private static Lazy<Dictionary<Tuple<DateTime, DateTime>, Tuple<int, int, int, bool>>> FedCalendarExpectedResultsConstructionPerm = new Lazy<Dictionary<Tuple<DateTime, DateTime>, Tuple<int, int, int, bool>>>(() =>
        {
            var result = new Dictionary<Tuple<DateTime, DateTime>, Tuple<int, int, int, bool>>();

            result.Add(new Tuple<DateTime, DateTime>(DateTime.Parse("4/20/2018"), DateTime.Parse("11/1/2018")), new Tuple<int, int, int, bool>(6, 11, 30, false));
            result.Add(new Tuple<DateTime, DateTime>(DateTime.Parse("5/1/2018"), DateTime.Parse("11/1/2018")), new Tuple<int, int, int, bool>(6, 0, 30, false));
            result.Add(new Tuple<DateTime, DateTime>(DateTime.Parse("4/20/2018"), DateTime.Parse("2/1/2019")), new Tuple<int, int, int, bool>(9, 11, 30, false));
            result.Add(new Tuple<DateTime, DateTime>(DateTime.Parse("5/1/2018"), DateTime.Parse("2/1/2019")), new Tuple<int, int, int, bool>(9, 0, 30, false));
            result.Add(new Tuple<DateTime, DateTime>(DateTime.Parse("4/20/2018"), DateTime.Parse("4/1/2019")), new Tuple<int, int, int, bool>(11, 11, 30, false));
            result.Add(new Tuple<DateTime, DateTime>(DateTime.Parse("5/1/2018"), DateTime.Parse("4/1/2019")), new Tuple<int, int, int, bool>(11, 0, 30, false));
            result.Add(new Tuple<DateTime, DateTime>(DateTime.Parse("4/20/2018"), DateTime.Parse("5/1/2019")), new Tuple<int, int, int, bool>(12, 11, 30, false));
            result.Add(new Tuple<DateTime, DateTime>(DateTime.Parse("5/1/2018"), DateTime.Parse("5/1/2019")), new Tuple<int, int, int, bool>(12, 0, 30, false));
            result.Add(new Tuple<DateTime, DateTime>(DateTime.Parse("4/20/2018"), DateTime.Parse("11/1/2019")), new Tuple<int, int, int, bool>(18, 11, 30, false));
            result.Add(new Tuple<DateTime, DateTime>(DateTime.Parse("5/1/2018"), DateTime.Parse("11/1/2019")), new Tuple<int, int, int, bool>(18, 0, 30, false));
            result.Add(new Tuple<DateTime, DateTime>(DateTime.Parse("4/20/2018"), DateTime.Parse("5/1/2020")), new Tuple<int, int, int, bool>(24, 11, 30, false));
            result.Add(new Tuple<DateTime, DateTime>(DateTime.Parse("5/1/2018"), DateTime.Parse("5/1/2020")), new Tuple<int, int, int, bool>(24, 0, 30, false));

            return result;
        });

        public class AprConstructionTestCase
        {
            public AprConstructionTestCase()
            {
            }

            public AprConstructionTestCase(
                string testID,
                ConstructionPhaseIntAccrual sConstructionPhaseIntAccrualT,
                decimal sConstructionPeriodIR,
                decimal sLAmtCalc,
                decimal sAprIncludedCc,
                decimal sSubsequentlyPaidFinanceChargeAmt,
                bool sIsIntReserveRequried,
                ConstructionIntCalcType sConstructionIntCalcT,
                string StartDate,
                string EndDate,
                decimal sConstructionIntAmount,
                decimal APRWINResult)
            {
                this.testID = testID;
                this.sConstructionPhaseIntAccrualT = sConstructionPhaseIntAccrualT;
                this.sConstructionPeriodIR = sConstructionPeriodIR;
                this.sLAmtCalc = sLAmtCalc;
                this.sAprIncludedCc = sAprIncludedCc;
                this.sSubsequentlyPaidFinanceChargeAmt = sSubsequentlyPaidFinanceChargeAmt;
                this.sIsIntReserveRequried = sIsIntReserveRequried;
                this.sConstructionIntCalcT = sConstructionIntCalcT;
                this.StartDate = StartDate;
                this.EndDate = EndDate;
                this.sConstructionIntAmount = sConstructionIntAmount;
                this.APRWINResult = APRWINResult;
            }

            public static AprConstructionTestCase aloCase001 = new AprConstructionTestCase("aloCase001", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 2000M, 0M, false, ConstructionIntCalcType.HalfCommitment, "04/20/2018", "11/01/2018", 2437.5M, 8.4756M);
            public static AprConstructionTestCase aloCase002 = new AprConstructionTestCase("aloCase002", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 2000M, 1000M, false, ConstructionIntCalcType.HalfCommitment, "04/20/2018", "11/01/2018", 2437.5M, 10.3856M);
            public static AprConstructionTestCase aloCase003 = new AprConstructionTestCase("aloCase003", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 2000M, 0M, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "11/01/2018", 2300M, 8.704M);
            public static AprConstructionTestCase aloCase004 = new AprConstructionTestCase("aloCase004", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 0M, 0M, false, ConstructionIntCalcType.HalfCommitment, "04/20/2018", "11/01/2018", 2437.5M, 4.5625M);
            public static AprConstructionTestCase aloCase005 = new AprConstructionTestCase("aloCase005", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 0M, 0M, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "11/01/2018", 2300M, 4.5625M);
            public static AprConstructionTestCase aloCase006 = new AprConstructionTestCase("aloCase006", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 2000M, 0M, false, ConstructionIntCalcType.HalfCommitment, "04/20/2018", "02/01/2019", 3587.5M, 7.2511M);
            public static AprConstructionTestCase aloCase007 = new AprConstructionTestCase("aloCase007", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 2000M, 0M, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "02/01/2019", 3450M, 7.3545M);
            public static AprConstructionTestCase aloCase008 = new AprConstructionTestCase("aloCase008", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 0M, 0M, false, ConstructionIntCalcType.HalfCommitment, "04/20/2018", "02/01/2019", 3587.5M, 4.5625M);
            public static AprConstructionTestCase aloCase009 = new AprConstructionTestCase("aloCase009", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 0M, 0M, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "02/01/2019", 3450M, 4.5625M);
            public static AprConstructionTestCase aloCase010 = new AprConstructionTestCase("aloCase010", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 2000M, 0M, false, ConstructionIntCalcType.HalfCommitment, "04/20/2018", "04/01/2019", 4325M, 6.8085M);
            public static AprConstructionTestCase aloCase011 = new AprConstructionTestCase("aloCase011", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 2000M, 0M, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "04/01/2019", 4187.5M, 6.8792M);
            public static AprConstructionTestCase aloCase012 = new AprConstructionTestCase("aloCase012", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 0M, 0M, false, ConstructionIntCalcType.HalfCommitment, "04/20/2018", "04/01/2019", 4325M, 4.5625M);
            public static AprConstructionTestCase aloCase013 = new AprConstructionTestCase("aloCase013", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 0M, 0M, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "04/01/2019", 4187.5M, 4.5625M);
            public static AprConstructionTestCase aloCase014 = new AprConstructionTestCase("aloCase014", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 2000M, 0M, false, ConstructionIntCalcType.HalfCommitment, "04/20/2018", "05/01/2019", 4700M, 6.6239M);
            public static AprConstructionTestCase aloCase015 = new AprConstructionTestCase("aloCase015", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 2000M, 1000M, false, ConstructionIntCalcType.HalfCommitment, "04/20/2018", "05/01/2019", 4700M, 7.6103M);
            public static AprConstructionTestCase aloCase016 = new AprConstructionTestCase("aloCase016", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 2000M, 0M, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "05/01/2019", 4562.5M, 6.6964M);
            public static AprConstructionTestCase aloCase017 = new AprConstructionTestCase("aloCase017", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 2000M, 1000M, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "05/01/2019", 4562.5M, 7.7168M);
            public static AprConstructionTestCase aloCase018 = new AprConstructionTestCase("aloCase018", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 0M, 0M, false, ConstructionIntCalcType.HalfCommitment, "04/20/2018", "05/01/2019", 4700M, 4.5564M);
            public static AprConstructionTestCase aloCase019 = new AprConstructionTestCase("aloCase019", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 0M, 0M, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "05/01/2019", 4562.5M, 4.5625M);
            public static AprConstructionTestCase aloCase020 = new AprConstructionTestCase("aloCase020", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 0M, 0M, false, ConstructionIntCalcType.FullCommitment, "05/01/2018", "05/01/2019", 9125M, 9.125M);
            public static AprConstructionTestCase aloCase021 = new AprConstructionTestCase("aloCase021", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 2000M, 0M, false, ConstructionIntCalcType.HalfCommitment, "04/20/2018", "11/01/2019", 7000M, 5.866M);
            public static AprConstructionTestCase aloCase022 = new AprConstructionTestCase("aloCase022", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 2000M, 0M, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "11/01/2019", 6862.5M, 5.8959M);
            public static AprConstructionTestCase aloCase023 = new AprConstructionTestCase("aloCase023", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 0M, 0M, false, ConstructionIntCalcType.HalfCommitment, "04/20/2018", "11/01/2019", 7000M, 4.4922M);
            public static AprConstructionTestCase aloCase024 = new AprConstructionTestCase("aloCase024", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 0M, 0M, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "11/01/2019", 6862.5M, 4.4948M);
            public static AprConstructionTestCase aloCase025 = new AprConstructionTestCase("aloCase025", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 2000M, 0M, false, ConstructionIntCalcType.HalfCommitment, "04/20/2018", "05/01/2020", 9262.5M, 5.5025M);
            public static AprConstructionTestCase aloCase026 = new AprConstructionTestCase("aloCase026", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 2000M, 1000M, false, ConstructionIntCalcType.HalfCommitment, "04/20/2018", "05/01/2020", 9262.5M, 5.9766M);
            public static AprConstructionTestCase aloCase027 = new AprConstructionTestCase("aloCase027", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 2000M, 0M, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "05/01/2020", 9125M, 5.5235M);
            public static AprConstructionTestCase aloCase028 = new AprConstructionTestCase("aloCase028", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 0M, 0M, false, ConstructionIntCalcType.HalfCommitment, "04/20/2018", "05/01/2020", 9262.5M, 4.4585M);
            public static AprConstructionTestCase aloCase029 = new AprConstructionTestCase("aloCase029", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 0M, 0M, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "05/01/2020", 9125M, 4.4629M);
            public static AprConstructionTestCase aloCase030 = new AprConstructionTestCase("aloCase030", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 0M, 0M, true, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "05/01/2020", 9541.33M, 4.662M);
            public static AprConstructionTestCase aloCase031 = new AprConstructionTestCase("aloCase031", ConstructionPhaseIntAccrual.ActualDays_365_360, 7.75M, 650000M, 17M, 0M, false, ConstructionIntCalcType.HalfCommitment, "11/12/2010", "06/01/2011", 14063.02M, 7.8675M);
            public static AprConstructionTestCase aloCase032 = new AprConstructionTestCase("aloCase032", ConstructionPhaseIntAccrual.ActualDays_365_360, 7.75M, 650000M, 17M, 0M, false, ConstructionIntCalcType.HalfCommitment, "11/12/2010", "06/01/2011", 14063.02M, 7.7894M);
            public static AprConstructionTestCase aloCase033 = new AprConstructionTestCase("aloCase033", ConstructionPhaseIntAccrual.ActualDays_365_360, 7.75M, 650000M, 0M, 0M, false, ConstructionIntCalcType.HalfCommitment, "04/06/2007", "11/01/2007", 14622.74M, 7.8576M);
            public static AprConstructionTestCase aloCase034 = new AprConstructionTestCase("aloCase034", ConstructionPhaseIntAccrual.ActualDays_365_360, 7.75M, 650000M, 0M, 0M, false, ConstructionIntCalcType.HalfCommitment, "04/06/2007", "11/01/2007", 14622.74M, 7.8576M);
            public static AprConstructionTestCase aloCase035 = new AprConstructionTestCase("aloCase035", ConstructionPhaseIntAccrual.ActualDays_365_360, 10M, 100000M, 1000M, 0M, false, ConstructionIntCalcType.HalfCommitment, "05/20/2018", "05/20/2020", 10138.89M, 10.7847M);
            public static AprConstructionTestCase aloCase036 = new AprConstructionTestCase("aloCase036", ConstructionPhaseIntAccrual.ActualDays_365_360, 10M, 100000M, 0M, 0M, false, ConstructionIntCalcType.HalfCommitment, "05/20/2018", "05/20/2020", 10138.89M, 9.6712M);
            public static AprConstructionTestCase aloCase037 = new AprConstructionTestCase("aloCase037", ConstructionPhaseIntAccrual.ActualDays_365_360, 10M, 100000M, 1000M, 0M, false, ConstructionIntCalcType.HalfCommitment, "04/20/2018", "05/01/2020", 10291.67M, 10.7461M);
            public static AprConstructionTestCase aloCase038 = new AprConstructionTestCase("aloCase038", ConstructionPhaseIntAccrual.ActualDays_365_360, 10M, 100000M, 0M, 0M, false, ConstructionIntCalcType.HalfCommitment, "04/20/2018", "05/01/2020", 10291.67M, 9.6511M);
            public static AprConstructionTestCase aloCase039 = new AprConstructionTestCase("aloCase039", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 2000M, 0M, false, ConstructionIntCalcType.HalfCommitment, "01/20/2016", "02/01/2017", 4712.5M, 6.6175M);
            public static AprConstructionTestCase aloCase040 = new AprConstructionTestCase("aloCase040", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 2000M, 0M, false, ConstructionIntCalcType.HalfCommitment, "02/01/2016", "02/01/2017", 4562.5M, 6.6964M);
            public static AprConstructionTestCase aloCase041 = new AprConstructionTestCase("aloCase041", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 0M, 0M, false, ConstructionIntCalcType.HalfCommitment, "01/20/2016", "02/01/2017", 4712.5M, 4.5559M);
            public static AprConstructionTestCase aloCase042 = new AprConstructionTestCase("aloCase042", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 0M, 0M, false, ConstructionIntCalcType.HalfCommitment, "02/01/2016", "02/01/2017", 4562.5M, 4.5625M);

            public static AprConstructionTestCase aloCase043 = new AprConstructionTestCase("aloCase043", ConstructionPhaseIntAccrual.ActualDays_365_365, 4.5M, 200000M, 2000M, 0M, false, ConstructionIntCalcType.HalfCommitment, "04/20/2018", "11/01/2018", 2404.11M, 8.4118M);
            public static AprConstructionTestCase aloCase044 = new AprConstructionTestCase("aloCase044", ConstructionPhaseIntAccrual.ActualDays_365_365, 4.5M, 200000M, 2000M, 0M, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "11/01/2018", 2268.49M, 8.6402M);
            public static AprConstructionTestCase aloCase045 = new AprConstructionTestCase("aloCase045", ConstructionPhaseIntAccrual.ActualDays_365_365, 4.5M, 200000M, 0M, 0M, false, ConstructionIntCalcType.HalfCommitment, "04/20/2018", "11/01/2018", 2404.11M, 4.5M);
            public static AprConstructionTestCase aloCase046 = new AprConstructionTestCase("aloCase046", ConstructionPhaseIntAccrual.ActualDays_365_365, 4.5M, 200000M, 0M, 0M, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "11/01/2018", 2268.49M, 4.5M);
            public static AprConstructionTestCase aloCase047 = new AprConstructionTestCase("aloCase047", ConstructionPhaseIntAccrual.ActualDays_365_365, 4.5M, 200000M, 2000M, 0M, false, ConstructionIntCalcType.HalfCommitment, "04/20/2018", "05/01/2019", 4635.62M, 6.5604M);
            public static AprConstructionTestCase aloCase048 = new AprConstructionTestCase("aloCase048", ConstructionPhaseIntAccrual.ActualDays_365_365, 4.5M, 200000M, 2000M, 0M, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "05/01/2019", 4500M, 6.6327M);
            public static AprConstructionTestCase aloCase049 = new AprConstructionTestCase("aloCase049", ConstructionPhaseIntAccrual.ActualDays_365_365, 4.5M, 200000M, 0M, 0M, false, ConstructionIntCalcType.HalfCommitment, "04/20/2018", "05/01/2019", 4635.62M, 4.4941M);
            public static AprConstructionTestCase aloCase050 = new AprConstructionTestCase("aloCase050", ConstructionPhaseIntAccrual.ActualDays_365_365, 4.5M, 200000M, 0M, 0M, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "05/01/2019", 4500M, 4.5M);
            public static AprConstructionTestCase aloCase051 = new AprConstructionTestCase("aloCase051", ConstructionPhaseIntAccrual.ActualDays_365_365, 4.5M, 200000M, 2000M, 0M, false, ConstructionIntCalcType.HalfCommitment, "04/20/2018", "11/01/2019", 6904.11M, 5.8047M);
            public static AprConstructionTestCase aloCase052 = new AprConstructionTestCase("aloCase052", ConstructionPhaseIntAccrual.ActualDays_365_365, 4.5M, 200000M, 2000M, 0M, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "11/01/2019", 6768.49M, 5.8346M);
            public static AprConstructionTestCase aloCase053 = new AprConstructionTestCase("aloCase053", ConstructionPhaseIntAccrual.ActualDays_365_365, 4.5M, 200000M, 0M, 0M, false, ConstructionIntCalcType.HalfCommitment, "04/20/2018", "11/01/2019", 6904.11M, 4.4316M);
            public static AprConstructionTestCase aloCase054 = new AprConstructionTestCase("aloCase054", ConstructionPhaseIntAccrual.ActualDays_365_365, 4.5M, 200000M, 0M, 0M, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "11/01/2019", 6768.49M, 4.4341M);
            public static AprConstructionTestCase aloCase055 = new AprConstructionTestCase("aloCase055", ConstructionPhaseIntAccrual.ActualDays_365_365, 4.5M, 200000M, 2000M, 0M, false, ConstructionIntCalcType.HalfCommitment, "04/20/2018", "05/01/2020", 9135.62M, 5.4422M);
            public static AprConstructionTestCase aloCase056 = new AprConstructionTestCase("aloCase056", ConstructionPhaseIntAccrual.ActualDays_365_365, 4.5M, 200000M, 2000M, 0M, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "05/01/2020", 9000M, 5.463M);
            public static AprConstructionTestCase aloCase057 = new AprConstructionTestCase("aloCase057", ConstructionPhaseIntAccrual.ActualDays_365_365, 4.5M, 200000M, 0M, 0M, false, ConstructionIntCalcType.HalfCommitment, "04/20/2018", "05/01/2020", 9135.62M, 4.3988M);
            public static AprConstructionTestCase aloCase058 = new AprConstructionTestCase("aloCase058", ConstructionPhaseIntAccrual.ActualDays_365_365, 4.5M, 200000M, 0M, 0M, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "05/01/2020", 9000M, 4.4031M);
            public static AprConstructionTestCase aloCase059 = new AprConstructionTestCase("aloCase059", ConstructionPhaseIntAccrual.ActualDays_365_365, 4.5M, 200000M, 2000M, 0M, false, ConstructionIntCalcType.HalfCommitment, "01/20/2016", "02/01/2017", 4647.95M, 6.554M);
            public static AprConstructionTestCase aloCase060 = new AprConstructionTestCase("aloCase060", ConstructionPhaseIntAccrual.ActualDays_365_365, 4.5M, 200000M, 2000M, 0M, false, ConstructionIntCalcType.HalfCommitment, "02/01/2016", "02/01/2017", 4500M, 6.6327M);
            public static AprConstructionTestCase aloCase061 = new AprConstructionTestCase("aloCase061", ConstructionPhaseIntAccrual.ActualDays_365_365, 4.5M, 200000M, 0M, 0M, false, ConstructionIntCalcType.HalfCommitment, "01/20/2016", "02/01/2017", 4647.95M, 4.4936M);
            public static AprConstructionTestCase aloCase062 = new AprConstructionTestCase("aloCase062", ConstructionPhaseIntAccrual.ActualDays_365_365, 4.5M, 200000M, 0M, 0M, false, ConstructionIntCalcType.HalfCommitment, "02/01/2016", "02/01/2017", 4500M, 4.5M);
            public static AprConstructionTestCase aloCase063 = new AprConstructionTestCase("aloCase063", ConstructionPhaseIntAccrual.ActualDays_365_365, 4.5M, 200000M, 2000M, 0M, false, ConstructionIntCalcType.HalfCommitment, "01/20/2016", "08/01/2016", 2391.78M, 8.4315M);
            public static AprConstructionTestCase aloCase064 = new AprConstructionTestCase("aloCase064", ConstructionPhaseIntAccrual.ActualDays_365_365, 4.5M, 200000M, 2000M, 0M, false, ConstructionIntCalcType.HalfCommitment, "02/01/2016", "08/01/2016", 2243.84M, 8.6847M);
            public static AprConstructionTestCase aloCase065 = new AprConstructionTestCase("aloCase065", ConstructionPhaseIntAccrual.ActualDays_365_365, 4.5M, 200000M, 0M, 0M, false, ConstructionIntCalcType.HalfCommitment, "01/20/2016", "08/01/2016", 2391.78M, 4.5M);
            public static AprConstructionTestCase aloCase066 = new AprConstructionTestCase("aloCase066", ConstructionPhaseIntAccrual.ActualDays_365_365, 4.5M, 200000M, 0M, 0M, false, ConstructionIntCalcType.HalfCommitment, "02/01/2016", "08/01/2016", 2243.84M, 4.5M);

            public static AprConstructionTestCase aloCase068 = new AprConstructionTestCase("aloCase068", ConstructionPhaseIntAccrual.Monthly_360_360, 4.5M, 200000M, 2000M, 0M, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "11/01/2018", 2250M, 8.6735M);
            public static AprConstructionTestCase aloCase070 = new AprConstructionTestCase("aloCase070", ConstructionPhaseIntAccrual.Monthly_360_360, 4.5M, 200000M, 0M, 0M, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "11/01/2018", 2250M, 4.5M);
            public static AprConstructionTestCase aloCase072 = new AprConstructionTestCase("aloCase072", ConstructionPhaseIntAccrual.Monthly_360_360, 4.5M, 200000M, 2000M, 0M, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "02/01/2019", 3375M, 7.3129M);
            public static AprConstructionTestCase aloCase074 = new AprConstructionTestCase("aloCase074", ConstructionPhaseIntAccrual.Monthly_360_360, 4.5M, 200000M, 0M, 0M, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "02/01/2019", 3375M, 4.5M);
            public static AprConstructionTestCase aloCase076 = new AprConstructionTestCase("aloCase076", ConstructionPhaseIntAccrual.Monthly_360_360, 4.5M, 200000M, 2000M, 0M, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "05/01/2019", 4500M, 6.6327M);
            public static AprConstructionTestCase aloCase078 = new AprConstructionTestCase("aloCase078", ConstructionPhaseIntAccrual.Monthly_360_360, 4.5M, 200000M, 0M, 0M, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "05/01/2019", 4500M, 4.5M);
            public static AprConstructionTestCase aloCase080 = new AprConstructionTestCase("aloCase080", ConstructionPhaseIntAccrual.Monthly_360_360, 4.5M, 200000M, 2000M, 0M, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "11/01/2019", 6750M, 5.8387M);
            public static AprConstructionTestCase aloCase082 = new AprConstructionTestCase("aloCase082", ConstructionPhaseIntAccrual.Monthly_360_360, 4.5M, 200000M, 0M, 0M, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "11/01/2019", 6750M, 4.4345M);
            public static AprConstructionTestCase aloCase084 = new AprConstructionTestCase("aloCase084", ConstructionPhaseIntAccrual.Monthly_360_360, 4.5M, 200000M, 2000M, 0M, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "05/01/2020", 9000M, 5.463M);
            public static AprConstructionTestCase aloCase086 = new AprConstructionTestCase("aloCase086", ConstructionPhaseIntAccrual.Monthly_360_360, 4.5M, 200000M, 0M, 0M, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "05/01/2020", 9000M, 4.4031M);

            public string testID { get; set; }
            public ConstructionPhaseIntAccrual sConstructionPhaseIntAccrualT { get; set; }
            public decimal sConstructionPeriodIR { get; set; }
            public decimal sLAmtCalc { get; set; }
            public decimal sAprIncludedCc { get; set; }
            public decimal sSubsequentlyPaidFinanceChargeAmt { get; set; }
            public bool sIsIntReserveRequried { get; set; }
            public ConstructionIntCalcType sConstructionIntCalcT { get; set; }
            public string StartDate { get; set; }
            public string EndDate { get; set; }
            public decimal sConstructionIntAmount { get; set; }
            public decimal APRWINResult { get; set; }

            public static AprConstructionTestCase GetTestCase(string testID)
            {
                return (AprConstructionTestCase)typeof(AprConstructionTestCase).GetField(testID, System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static).GetValue(null);
            }
        }

        public class AprConstructionPermTestCase : AprConstructionTestCase
        {
            public AprConstructionPermTestCase()
            {
            }

            public AprConstructionPermTestCase(
                string testID,
                ConstructionPhaseIntAccrual sConstructionPhaseIntAccrualT,
                decimal sConstructionPeriodIR,
                decimal sLAmtCalcConstruction,
                decimal sAprIncludedCc,
                decimal sSubsequentlyPaidFinanceChargeAmt,
                bool sIsIntReserveRequried,
                ConstructionIntCalcType sConstructionIntCalcT,
                string StartDate,
                string EndDate,
                decimal sConstructionIntAmount,
                decimal APRWINResult,
                decimal sNoteIR,
                decimal sLAmtCalcPerm,
                int sTerm,
                decimal FirstPmt,
                decimal LastPmt)
            {
                this.testID = testID;
                this.sConstructionPhaseIntAccrualT = sConstructionPhaseIntAccrualT;
                this.sConstructionPeriodIR = sConstructionPeriodIR;
                this.sLAmtCalc = sLAmtCalcConstruction;
                this.sAprIncludedCc = sAprIncludedCc;
                this.sSubsequentlyPaidFinanceChargeAmt = sSubsequentlyPaidFinanceChargeAmt;
                this.sIsIntReserveRequried = sIsIntReserveRequried;
                this.sConstructionIntCalcT = sConstructionIntCalcT;
                this.StartDate = StartDate;
                this.EndDate = EndDate;
                this.sConstructionIntAmount = sConstructionIntAmount;
                this.APRWINResult = APRWINResult;
                this.sNoteIR = sNoteIR;
                this.sLAmtCalcPerm = sLAmtCalcPerm;
                this.sTerm = sTerm;
                this.FirstPmt = FirstPmt;
                this.LastPmt = LastPmt;
            }

            public static AprConstructionPermTestCase aloCase100 = new AprConstructionPermTestCase("aloCase100", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 2000M, 0, false, ConstructionIntCalcType.HalfCommitment, "04/20/2018", "11/01/2018", 2437.5M, 4.5874M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase101 = new AprConstructionPermTestCase("aloCase101", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 2000M, 0, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "11/01/2018", 2300M, 4.5879M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase102 = new AprConstructionPermTestCase("aloCase102", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 0M, 0, false, ConstructionIntCalcType.HalfCommitment, "04/20/2018", "11/01/2018", 2437.5M, 4.5024M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase103 = new AprConstructionPermTestCase("aloCase103", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 0M, 0, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "11/01/2018", 2300M, 4.5028M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase104 = new AprConstructionPermTestCase("aloCase104", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 2000M, 0, false, ConstructionIntCalcType.HalfCommitment, "04/20/2018", "02/01/2019", 3587.5M, 4.5889M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase105 = new AprConstructionPermTestCase("aloCase105", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 2000M, 0, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "02/01/2019", 3450M, 4.5893M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase106 = new AprConstructionPermTestCase("aloCase106", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 0M, 0, false, ConstructionIntCalcType.HalfCommitment, "04/20/2018", "02/01/2019", 3587.5M, 4.5042M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase107 = new AprConstructionPermTestCase("aloCase107", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 0M, 0, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "02/01/2019", 3450M, 4.5046M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase108 = new AprConstructionPermTestCase("aloCase108", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 2000M, 0, false, ConstructionIntCalcType.HalfCommitment, "04/20/2018", "04/01/2019", 4325M, 4.5887M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase109 = new AprConstructionPermTestCase("aloCase109", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 2000M, 0, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "04/01/2019", 4187.5M, 4.5892M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase110 = new AprConstructionPermTestCase("aloCase110", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 0M, 0, false, ConstructionIntCalcType.HalfCommitment, "04/20/2018", "04/01/2019", 4325M, 4.5043M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase111 = new AprConstructionPermTestCase("aloCase111", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 0M, 0, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "04/01/2019", 4187.5M, 4.5047M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase112 = new AprConstructionPermTestCase("aloCase112", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 2000M, 0, false, ConstructionIntCalcType.HalfCommitment, "04/20/2018", "05/01/2019", 4700M, 4.589M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase113 = new AprConstructionPermTestCase("aloCase113", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 2000M, 0, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "05/01/2019", 4562.5M, 4.5894M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase114 = new AprConstructionPermTestCase("aloCase114", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 0M, 0, false, ConstructionIntCalcType.HalfCommitment, "04/20/2018", "05/01/2019", 4700M, 4.5047M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase115 = new AprConstructionPermTestCase("aloCase115", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 0M, 0, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "05/01/2019", 4562.5M, 4.5051M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase116 = new AprConstructionPermTestCase("aloCase116", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 0M, 0, false, ConstructionIntCalcType.FullCommitment, "05/01/2018", "05/01/2019", 9125M, 4.6994M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase117 = new AprConstructionPermTestCase("aloCase117", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 0M, 0, true, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "05/01/2019", 4666.58M, 4.5094M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase118 = new AprConstructionPermTestCase("aloCase118", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 0M, 0, true, ConstructionIntCalcType.FullCommitment, "05/01/2018", "05/01/2019", 9333.16M, 4.7084M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase119 = new AprConstructionPermTestCase("aloCase119", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 0M, 0, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "05/01/2020", 9137.5M, 4.5145M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase120 = new AprConstructionPermTestCase("aloCase120", ConstructionPhaseIntAccrual.ActualDays_365_360, 4.5M, 200000M, 0M, 0, false, ConstructionIntCalcType.FullCommitment, "05/01/2018", "05/01/2020", 18275M, 4.9045M, 4.5M, 200000, 360, 1013.37M, 1013.99M);

            public static AprConstructionPermTestCase aloCase121 = new AprConstructionPermTestCase("aloCase121", ConstructionPhaseIntAccrual.ActualDays_365_365, 4.5M, 200000M, 2000M, 0, false, ConstructionIntCalcType.HalfCommitment, "04/20/2018", "11/01/2018", 2404.11M, 4.586M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase122 = new AprConstructionPermTestCase("aloCase122", ConstructionPhaseIntAccrual.ActualDays_365_365, 4.5M, 200000M, 2000M, 0, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "11/01/2018", 2268.49M, 4.5866M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase123 = new AprConstructionPermTestCase("aloCase123", ConstructionPhaseIntAccrual.ActualDays_365_365, 4.5M, 200000M, 0M, 0, false, ConstructionIntCalcType.HalfCommitment, "04/20/2018", "11/01/2018", 2404.11M, 4.501M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase124 = new AprConstructionPermTestCase("aloCase124", ConstructionPhaseIntAccrual.ActualDays_365_365, 4.5M, 200000M, 0M, 0, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "11/01/2018", 2268.49M, 4.5015M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase125 = new AprConstructionPermTestCase("aloCase125", ConstructionPhaseIntAccrual.ActualDays_365_365, 4.5M, 200000M, 2000M, 0, false, ConstructionIntCalcType.HalfCommitment, "04/20/2018", "02/01/2019", 3538.36M, 4.5868M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase126 = new AprConstructionPermTestCase("aloCase126", ConstructionPhaseIntAccrual.ActualDays_365_365, 4.5M, 200000M, 2000M, 0, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "02/01/2019", 3402.74M, 4.5873M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase127 = new AprConstructionPermTestCase("aloCase127", ConstructionPhaseIntAccrual.ActualDays_365_365, 4.5M, 200000M, 0M, 0, false, ConstructionIntCalcType.HalfCommitment, "04/20/2018", "02/01/2019", 3538.36M, 4.5021M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase128 = new AprConstructionPermTestCase("aloCase128", ConstructionPhaseIntAccrual.ActualDays_365_365, 4.5M, 200000M, 0M, 0, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "02/01/2019", 3402.74M, 4.5026M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase129 = new AprConstructionPermTestCase("aloCase129", ConstructionPhaseIntAccrual.ActualDays_365_365, 4.5M, 200000M, 2000M, 0, false, ConstructionIntCalcType.HalfCommitment, "04/20/2018", "04/01/2019", 4265.75M, 4.5862M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase130 = new AprConstructionPermTestCase("aloCase130", ConstructionPhaseIntAccrual.ActualDays_365_365, 4.5M, 200000M, 2000M, 0, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "04/01/2019", 4130.14M, 4.5867M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase131 = new AprConstructionPermTestCase("aloCase131", ConstructionPhaseIntAccrual.ActualDays_365_365, 4.5M, 200000M, 0M, 0, false, ConstructionIntCalcType.HalfCommitment, "04/20/2018", "04/01/2019", 4265.75M, 4.5018M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase132 = new AprConstructionPermTestCase("aloCase132", ConstructionPhaseIntAccrual.ActualDays_365_365, 4.5M, 200000M, 0M, 0, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "04/01/2019", 4130.14M, 4.5023M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase133 = new AprConstructionPermTestCase("aloCase133", ConstructionPhaseIntAccrual.ActualDays_365_365, 4.5M, 200000M, 2000M, 0, false, ConstructionIntCalcType.HalfCommitment, "04/20/2018", "05/01/2019", 4635.62M, 4.5863M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase134 = new AprConstructionPermTestCase("aloCase134", ConstructionPhaseIntAccrual.ActualDays_365_365, 4.5M, 200000M, 2000M, 0, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "05/01/2019", 4500M, 4.5868M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase135 = new AprConstructionPermTestCase("aloCase135", ConstructionPhaseIntAccrual.ActualDays_365_365, 4.5M, 200000M, 0M, 0, false, ConstructionIntCalcType.HalfCommitment, "04/20/2018", "05/01/2019", 4635.62M, 4.502M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase136 = new AprConstructionPermTestCase("aloCase136", ConstructionPhaseIntAccrual.ActualDays_365_365, 4.5M, 200000M, 0M, 0, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "05/01/2019", 4500M, 4.5024M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase137 = new AprConstructionPermTestCase("aloCase137", ConstructionPhaseIntAccrual.ActualDays_365_365, 4.5M, 200000M, 0M, 0, false, ConstructionIntCalcType.FullCommitment, "05/01/2018", "05/01/2019", 9000M, 4.694M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase138 = new AprConstructionPermTestCase("aloCase138", ConstructionPhaseIntAccrual.ActualDays_365_365, 4.5M, 200000M, 0M, 0, true, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "05/01/2019", 4601.25M, 4.5067M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase139 = new AprConstructionPermTestCase("aloCase139", ConstructionPhaseIntAccrual.ActualDays_365_365, 4.5M, 200000M, 0M, 0, true, ConstructionIntCalcType.FullCommitment, "05/01/2018", "05/01/2019", 9202.5M, 4.7028M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase140 = new AprConstructionPermTestCase("aloCase140", ConstructionPhaseIntAccrual.ActualDays_365_365, 4.5M, 200000M, 0M, 0, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "05/01/2020", 9012.33M, 4.5094M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase141 = new AprConstructionPermTestCase("aloCase141", ConstructionPhaseIntAccrual.ActualDays_365_365, 4.5M, 200000M, 0M, 0, false, ConstructionIntCalcType.FullCommitment, "05/01/2018", "05/01/2020", 18024.66M, 4.8935M, 4.5M, 200000, 360, 1013.37M, 1013.99M);

            public static AprConstructionPermTestCase aloCase142 = new AprConstructionPermTestCase("aloCase142", ConstructionPhaseIntAccrual.Monthly_360_360, 4.5M, 200000M, 2000M, 0, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "11/01/2018", 2250M, 4.5858M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase143 = new AprConstructionPermTestCase("aloCase143", ConstructionPhaseIntAccrual.Monthly_360_360, 4.5M, 200000M, 0M, 0, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "11/01/2018", 2250M, 4.5007M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase144 = new AprConstructionPermTestCase("aloCase144", ConstructionPhaseIntAccrual.Monthly_360_360, 4.5M, 200000M, 2000M, 0, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "02/01/2019", 3375M, 4.5861M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase145 = new AprConstructionPermTestCase("aloCase145", ConstructionPhaseIntAccrual.Monthly_360_360, 4.5M, 200000M, 0M, 0, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "02/01/2019", 3375M, 4.5014M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase146 = new AprConstructionPermTestCase("aloCase146", ConstructionPhaseIntAccrual.Monthly_360_360, 4.5M, 200000M, 2000M, 0, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "04/01/2019", 4125M, 4.5865M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase147 = new AprConstructionPermTestCase("aloCase147", ConstructionPhaseIntAccrual.Monthly_360_360, 4.5M, 200000M, 0M, 0, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "04/01/2019", 4125M, 4.5021M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase148 = new AprConstructionPermTestCase("aloCase148", ConstructionPhaseIntAccrual.Monthly_360_360, 4.5M, 200000M, 2000M, 0, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "05/01/2019", 4500M, 4.5868M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase149 = new AprConstructionPermTestCase("aloCase149", ConstructionPhaseIntAccrual.Monthly_360_360, 4.5M, 200000M, 0M, 0, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "05/01/2019", 4500M, 4.5024M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase150 = new AprConstructionPermTestCase("aloCase150", ConstructionPhaseIntAccrual.Monthly_360_360, 4.5M, 200000M, 0M, 0, false, ConstructionIntCalcType.FullCommitment, "05/01/2018", "05/01/2019", 9000M, 4.694M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase151 = new AprConstructionPermTestCase("aloCase151", ConstructionPhaseIntAccrual.Monthly_360_360, 4.5M, 200000M, 0M, 0, true, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "05/01/2019", 4601.25M, 4.5067M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase152 = new AprConstructionPermTestCase("aloCase152", ConstructionPhaseIntAccrual.Monthly_360_360, 4.5M, 200000M, 0M, 0, true, ConstructionIntCalcType.FullCommitment, "05/01/2018", "05/01/2019", 9202.5M, 4.7028M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase153 = new AprConstructionPermTestCase("aloCase153", ConstructionPhaseIntAccrual.Monthly_360_360, 4.5M, 200000M, 0M, 0, false, ConstructionIntCalcType.HalfCommitment, "05/01/2018", "05/01/2020", 9000M, 4.5089M, 4.5M, 200000, 360, 1013.37M, 1013.99M);
            public static AprConstructionPermTestCase aloCase154 = new AprConstructionPermTestCase("aloCase154", ConstructionPhaseIntAccrual.Monthly_360_360, 4.5M, 200000M, 0M, 0, false, ConstructionIntCalcType.FullCommitment, "05/01/2018", "05/01/2020", 18000M, 4.8924M, 4.5M, 200000, 360, 1013.37M, 1013.99M);

            public decimal sNoteIR { get; set; }
            public decimal sLAmtCalcPerm { get; set; }
            public int sTerm { get; set; }
            public decimal FirstPmt { get; set; }
            public decimal LastPmt { get; set; }

            public static new AprConstructionPermTestCase GetTestCase(string testID)
            {
                return (AprConstructionPermTestCase)typeof(AprConstructionPermTestCase).GetField(testID, System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static).GetValue(null);
            }
        }

        public class AprTestCase : IAmortizationDataProvider
        {
            public static AprTestCase FixedNoPpfc = new AprTestCase(nameof(FixedNoPpfc))
            {
                sFinalLAmt = 100000.00M,
                sTerm = 360,
                sDue = 360,
                sFinMethT = E_sFinMethT.Fixed,
                sNoteIR = 6.000M,
                oddDays = 0,
                APRWINResult = 6.0000M
            };

            public static AprTestCase FixedWithPpfc = new AprTestCase(nameof(FixedWithPpfc))
            {
                sFinalLAmt = 100000.00M,
                sTerm = 360,
                sDue = 360,
                sFinMethT = E_sFinMethT.Fixed,
                sNoteIR = 6.000M,
                oddDays = 0,
                prepaidFinanceCharges = 1000.00M,
                APRWINResult = 6.0940M
            };

            public static AprTestCase FixedWithPpfcAndIrreg1st = new AprTestCase(nameof(FixedWithPpfcAndIrreg1st))
            {
                sFinalLAmt = 100000.00M,
                sTerm = 360,
                sDue = 360,
                sFinMethT = E_sFinMethT.Fixed,
                sNoteIR = 6.000M,
                oddDays = 9,
                prepaidFinanceCharges = 2000.00M,
                APRWINResult = 6.1749M
            };

            public static AprTestCase FixedWithMi = new AprTestCase(nameof(FixedWithMi))
            {
                sFinalLAmt = 162000.00M,
                sHouseVal = 180000.00M,
                sTerm = 360,
                sDue = 360,
                sFinMethT = E_sFinMethT.Fixed,
                sNoteIR = 3.875M,
                sProMInsR = 0.610M,
                sProMIns = 82.35M,
                sProMInsCancelLtv = 78.000M,
                oddDays = 15,
                prepaidFinanceCharges = 261.56M,
                APRWINResult = 4.1732M
            };

            public static AprTestCase GetTestCase(string name)
            {
                return (AprTestCase)typeof(AprTestCase).GetField(name, System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static).GetValue(null);
            }

            // Disable "Field is never assigned to, and will always have its default value..." warnings.
#pragma warning disable 0649

            public int sConstructionPeriodMon;
            public decimal sConstructionPeriodIR;
            public decimal sConstructionImprovementAmt;

            public CDateTime sEstCloseD;
            public bool sIPiaDyLckd;
            public int sIPiaDy;

            public int unitPeriods;
            public int oddDays;

            public E_sLPurposeT sLPurposeT;
            public decimal prepaidFinanceCharges;

            public E_sLpProductT sLpProductT;
            public Guid sLpTemplateId;
            public E_sAprCalculationT sAprCalculationT;

            public string Name { get; }

            public decimal sNoteIR { get; set; }
            public int sTerm { get; set; }
            public int sDue { get; set; }
            public decimal sFinalLAmt { get; set; }
            public E_sFinMethT sFinMethT { get; set; }
            public LosConvert m_convertLos { get; set; }
            public CDateTime sSchedDueD1 { get; set; }
            public bool sIsOptionArm { get; set; }
            public int sIOnlyMon { get; set; }

            public int sPpmtMon { get; set; }
            public decimal sPpmtAmt { get; set; }
            public decimal sPpmtOneAmt { get; set; }
            public int sPpmtStartMon { get; set; }
            public int sPpmtOneMon { get; set; }

            public int sBuydwnMon1 { get; set; }
            public int sBuydwnMon2 { get; set; }
            public int sBuydwnMon3 { get; set; }
            public int sBuydwnMon4 { get; set; }
            public int sBuydwnMon5 { get; set; }
            public decimal sBuydwnR1 { get; set; }
            public decimal sBuydwnR2 { get; set; }
            public decimal sBuydwnR3 { get; set; }
            public decimal sBuydwnR4 { get; set; }
            public decimal sBuydwnR5 { get; set; }

            public int sProMInsMon { get; set; }
            public int sProMIns2Mon { get; set; }
            public decimal sProMIns2 { get; set; }
            public decimal sProMInsCancelLtv { get; set; }
            public decimal sProMInsCancelAppraisalLtv { get; set; }
            public bool sProMInsMidptCancel { get; set; }
            public int sProMInsCancelMinPmts { get; set; }
            public decimal sFfUfmipR { get; set; }
            public bool sFfUfMipIsBeingFinanced { get; set; }
            public decimal sProMInsR { get; set; }
            public decimal sFfUfmipFinanced { get; set; }
            public E_PercentBaseT sProMInsT { get; set; }
            public bool sProMInsLckd { get; set; }
            public decimal sProMIns { get; set; }

            public decimal sHouseVal { get; set; }
            public decimal sApprValFor_LTV_CLTV_HCLTV { get; set; }
            public decimal sLAmtCalc { get; set; }
            public E_sLT sLT { get; set; }

            public int sGradPmtYrs { get; set; }
            public decimal sGradPmtR { get; set; }

            public decimal sRAdj1stCapR { get; set; }
            public int sRAdj1stCapMon { get; set; }
            public decimal sRAdjCapR { get; set; }
            public int sRAdjCapMon { get; set; }
            public decimal sRAdjLifeCapR { get; set; }
            public decimal sRAdjIndexR { get; set; }
            public decimal sRAdjMarginR { get; set; }
            public decimal sRAdjFloorR { get; set; }
            public decimal sRAdjRoundToR { get; set; }
            public E_sRAdjRoundT sRAdjRoundT { get; set; }

            public decimal sPmtAdjCapR { get; set; }
            public int sPmtAdjCapMon { get; set; }
            public int sPmtAdjRecastPeriodMon { get; set; }
            public int sPmtAdjRecastStop { get; set; }
            public decimal sPmtAdjMaxBalPc { get; set; }

            public int sOptionArmMinPayPeriod { get; set; }
            public int sOptionArmInitialFixMinPmtPeriod { get; set; }
            public int sOptionArmIntroductoryPeriod { get; set; }
            public bool sOptionArmMinPayIsIOOnly { get; set; }
            public bool sIsFullAmortAfterRecast { get; set; }

            public E_sOptionArmMinPayOptionT sOptionArmMinPayOptionT { get; set; }
            public decimal sOptionArmTeaserR { get; set; }
            public decimal sOptionArmNoteIRDiscount { get; set; }
            public decimal sOptionArmPmtDiscount { get; set; }

            public bool sIsIntReserveRequired { get; set; }
            public ConstructionIntCalcType sConstructionIntCalcT { get; set; }
            public ConstructionPhaseIntAccrual sConstructionPhaseIntAccrualT { get; set; }
            public int sConstructionInitialOddDays { get; set; }

            public decimal APRWINResult { get; set; }

#pragma warning restore 0649

            public decimal amountFinanced
            {
                get { return this.sFinalLAmt - this.prepaidFinanceCharges; }
            }

            public AprTestCase(string name)
            {
                this.Name = name;
                this.sLT = E_sLT.Conventional;
                this.sLPurposeT = E_sLPurposeT.Purchase;
                this.sLpProductT = E_sLpProductT.Conforming;
                this.sProMInsT = E_PercentBaseT.LoanAmount;
                this.sAprCalculationT = E_sAprCalculationT.Actuarial_AccountForIrregularFirstPeriod;
                this.m_convertLos = new LosConvert();
                this.unitPeriods = 1;
            }

            public decimal ComputeLoanApr(CPageData dataLoan)
            {
                dataLoan.CalcModeT = E_CalcModeT.LendersOfficePageEditing;
                
                this.ApplyTestDataToLoan(dataLoan);

                return dataLoan.sAprUnrounded;
            }

            public decimal ComputeLoanAprPricing(CPageData dataLoan)
            {
                dataLoan.CalcModeT = E_CalcModeT.LendersOfficePageEditing;

                this.ApplyTestDataToLoan(dataLoan);

                dataLoan.TransformDataToPml(E_TransformToPmlT.FromScratch);

                return dataLoan.sAprUnrounded;
            }
            
            public string ComputeAprWinInputs()
            {
                var amortTable = new AmortTable(this, E_AmortizationScheduleT.Standard, true);

                StringBuilder sb = new StringBuilder();

                if (this.sLPurposeT == E_sLPurposeT.Construct || this.sLPurposeT == E_sLPurposeT.ConstructPerm)
                {
                    sb.AppendLine("Construction Loan");
                }
                else
                {
                    sb.AppendLine("Regular APR");
                    sb.AppendLine($"Amount Financed {this.amountFinanced}");
                    sb.AppendLine($"Disclosed APR: {this.sNoteIR}");
                    sb.AppendLine($"Loan type: Installment Loan");
                    sb.AppendLine($"Payment Frequency: Monthly");
                    sb.AppendLine($"Loan Secured by Real Estate or Dwelling: true");
                    sb.AppendLine($"Loan Date Earlier than 9/30/1995: false");
                    sb.AppendLine();
                    sb.AppendLine("Payment Schedule:");
                    sb.AppendLine("Payment Stream #, Payment Amount, Number of Payments, Unit Periods, Odd Days");

                    foreach (AmortItem row in amortTable.Items)
                    {
                        int pmtStreamNum = row.Index + 1;
                        sb.AppendLine($"{pmtStreamNum}, {row.Pmt}, {row.Count}, {this.unitPeriods + row.Index}, {this.oddDays}");
                    }
                }

                return sb.ToString();
            }

            private void ApplyTestDataToLoan(CPageData dataLoan)
            {
                dataLoan.sLAmtLckd = true;
                dataLoan.sInitialEscrowAccMICushion_rep = "0";

                dataLoan.sPurchPrice = this.sHouseVal;
                dataLoan.sApprVal = this.sHouseVal;

                // Base
                dataLoan.sNoteIR = this.sNoteIR;
                dataLoan.sTerm = this.sTerm;
                dataLoan.sDue = this.sDue;

                if (this.sFinalLAmt > 0 && this.sLAmtCalc == 0)
                {
                    dataLoan.sLAmtCalc = this.sFinalLAmt;
                }
                else
                {
                    dataLoan.sLAmtCalc = this.sLAmtCalc;
                }

                dataLoan.sFinMethT = this.sFinMethT;

                //dataLoan.m_convertLos = this.m_convertLos;
                if (this.sSchedDueD1 != null && this.sSchedDueD1.IsValid)
                {
                    dataLoan.sSchedDueD1Lckd = true;
                    dataLoan.sSchedDueD1 = this.sSchedDueD1;
                }

                dataLoan.sIsOptionArm = this.sIsOptionArm;
                dataLoan.sIOnlyMon = this.sIOnlyMon;

                // Ignoring pre-payments

                // Buydowns
                dataLoan.sBuydwnMon1 = this.sBuydwnMon1;
                dataLoan.sBuydwnMon2 = this.sBuydwnMon2;
                dataLoan.sBuydwnMon3 = this.sBuydwnMon3;
                dataLoan.sBuydwnMon4 = this.sBuydwnMon4;
                dataLoan.sBuydwnMon5 = this.sBuydwnMon5;
                dataLoan.sBuydwnR1 = this.sBuydwnR1;
                dataLoan.sBuydwnR2 = this.sBuydwnR2;
                dataLoan.sBuydwnR3 = this.sBuydwnR3;
                dataLoan.sBuydwnR4 = this.sBuydwnR4;
                dataLoan.sBuydwnR5 = this.sBuydwnR5;

                // MI
                dataLoan.sProMInsMon = this.sProMInsMon;
                dataLoan.sProMIns2Mon = this.sProMIns2Mon;

                if (this.sProMIns2 > 0)
                {
                    dataLoan.sProMInsR2 = this.sProMIns2 * 100 / dataLoan.sProMInsBaseAmt;
                }

                dataLoan.sProMInsCancelLtv = this.sProMInsCancelLtv;
                dataLoan.sProMInsMidptCancel = this.sProMInsMidptCancel;
                dataLoan.sProMInsCancelMinPmts = this.sProMInsCancelMinPmts;
                dataLoan.sFfUfmipR = this.sFfUfmipR;
                dataLoan.sFfUfMipIsBeingFinanced = this.sFfUfMipIsBeingFinanced;
                dataLoan.sProMInsR = this.sProMInsR;
                //dataLoan.sFfUfmipFinanced = this.sFfUfmipFinanced;
                dataLoan.sProMInsT = this.sProMInsT;
                dataLoan.sProMInsLckd = this.sProMInsLckd;
                dataLoan.sProMIns = this.sProMIns;

                dataLoan.sLT = this.sLT;

                // GPM
                dataLoan.sGradPmtYrs = this.sGradPmtYrs;
                dataLoan.sGradPmtR = this.sGradPmtR;

                // ARM
                dataLoan.sRAdj1stCapR = this.sRAdj1stCapR;
                dataLoan.sRAdj1stCapMon = this.sRAdj1stCapMon;
                dataLoan.sRAdjCapR = this.sRAdjCapR;
                dataLoan.sRAdjCapMon = this.sRAdjCapMon;
                dataLoan.sRAdjLifeCapR = this.sRAdjLifeCapR;
                dataLoan.sRAdjIndexR = this.sRAdjIndexR;
                dataLoan.sRAdjMarginR = this.sRAdjMarginR;
                dataLoan.sRAdjFloorR = this.sRAdjFloorR;
                dataLoan.sRAdjRoundToR = this.sRAdjRoundToR;
                dataLoan.sRAdjRoundT = this.sRAdjRoundT;

                // Neg-Am
                dataLoan.sPmtAdjCapR = this.sPmtAdjCapR;
                dataLoan.sPmtAdjCapMon = this.sPmtAdjCapMon;
                dataLoan.sPmtAdjRecastPeriodMon = this.sPmtAdjRecastPeriodMon;
                dataLoan.sPmtAdjRecastStop = this.sPmtAdjRecastStop;
                dataLoan.sPmtAdjMaxBalPc = this.sPmtAdjMaxBalPc;

                dataLoan.sOptionArmMinPayPeriod = this.sOptionArmMinPayPeriod;
                dataLoan.sOptionArmInitialFixMinPmtPeriod = this.sOptionArmInitialFixMinPmtPeriod;
                dataLoan.sOptionArmIntroductoryPeriod = this.sOptionArmIntroductoryPeriod;
                dataLoan.sOptionArmMinPayIsIOOnly = this.sOptionArmMinPayIsIOOnly;
                dataLoan.sIsFullAmortAfterRecast = this.sIsFullAmortAfterRecast;

                dataLoan.sOptionArmMinPayOptionT = this.sOptionArmMinPayOptionT;
                dataLoan.sOptionArmTeaserR = this.sOptionArmTeaserR;
                dataLoan.sOptionArmNoteIRDiscount = this.sOptionArmNoteIRDiscount;
                dataLoan.sOptionArmPmtDiscount = this.sOptionArmPmtDiscount;

                if (this.sLPurposeT == E_sLPurposeT.ConstructPerm || this.sLPurposeT == E_sLPurposeT.Construct)
                {
                    dataLoan.sConstructionPeriodMon = this.sConstructionPeriodMon;
                    dataLoan.sConstructionPeriodIR = this.sConstructionPeriodIR;
                    dataLoan.sConstructionImprovementAmt = this.sConstructionImprovementAmt;
                }

                if (this.sEstCloseD != null && this.sEstCloseD.IsValid)
                {
                    dataLoan.sEstCloseDLckd = true;
                    dataLoan.sEstCloseD = this.sEstCloseD;
                }

                dataLoan.sIPiaProps_Apr = true;
                dataLoan.sIPiaDyLckd = true;
                dataLoan.sIPiaDy = this.oddDays;

                dataLoan.sLPurposeT = this.sLPurposeT;
                dataLoan.sLpProductT = this.sLpProductT;
                dataLoan.sLpTemplateId = this.sLpTemplateId;
                dataLoan.sAprCalculationT = this.sAprCalculationT;

                dataLoan.sLOrigFProps_Apr = true;
                dataLoan.sLOrigFMb = this.prepaidFinanceCharges - dataLoan.sIPia;                
            }

            public decimal Get_sProMIns_ByMonth(int months)
            {
                return this.sProMIns;
            }
        }
    }
}
