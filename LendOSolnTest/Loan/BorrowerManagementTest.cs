﻿// Author: Peter Anargirou
using System;
using System.Linq;
using DataAccess;
using LendersOffice.Audit;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Security;
using LendingQB.Core.Data;
using LendOSolnTest.Common;
using LendOSolnTest.Fakes;
using LqbGrammar.DataTypes;
using NUnit.Framework;

namespace LendOSolnTest.Loan
{
    [TestFixture]
    public class BorrowerManagementTest
	{
        private enum Operation
        {
            DeleteCoborrower,
            SwapBorrowers
        }

        #region member variables
        private Guid m_loanId => this.collectionType == E_sLqbCollectionT.Legacy ? this.legacyLoanId.Value : this.lqbCollectionsLoanId.Value;
        private E_sLqbCollectionT collectionType = E_sLqbCollectionT.Legacy;
        private Lazy<Guid> legacyLoanId;
        private Lazy<Guid> lqbCollectionsLoanId;
        private Guid m_borrowerPreviousEmployerRecord;
        private Guid m_coborrowerPreviousEmployerRecord;
		private Guid m_borrowerLiabilityId;
		private Guid m_coborrowerLiabilityId;
		private Guid m_jointLiabilityId;
		private Guid m_borrowerAssetId;
		private Guid m_coborrowerAssetId;
		private Guid m_jointAssetId;
        private CPageData m_dataLoan;
		private string m_bEmplrBusPhone;
		private string m_cEmplrBusPhone;
		private AbstractUserPrincipal x_principal = null;
		private AbstractUserPrincipal m_currentPrincipal
		{
			get
			{
				if (null == x_principal)
				{
					x_principal = LoginTools.LoginAs(TestAccountType.LoTest001);
				}

				return x_principal;
			}
		}
		#endregion

		private const bool NET_RENT_LOCK = true;

		#region constants for borrower information
		private const string B_FIRST_NAME = "Martin";
		private const string B_MIDDLE_NAME = "Luther";
		private const string B_LAST_NAME = "King";
		private const string B_SUFFIX = "Jr.";
		private const string B_SSN = "123-45-6789";
		private const string B_DOB = "1/15/1929";
		private const string B_YEARS_OF_SCHOOL = "8";
		private const E_aBMaritalStatT B_MARITAL_STATUS = E_aBMaritalStatT.Married;
		private const string B_NUMBER_OF_DEPENDENTS = "1";
		private const string B_DEPENDENTS_AGES = "12";
		private const string B_HOME_PHONE = "(111) 111-1122";
		private const string B_CELL_PHONE = "(111) 111-1133";
		private const string B_WORK_PHONE = "(111) 111-1144";
		private const string B_FAX = "(111) 111-1155";
		private const string B_EMAIL = "mlkj@fake.com";
		private const string B_ADDRESS = "123 Test Street";
		private const string B_CITY = "Irvine";
		private const string B_STATE = "AL";
		private const string B_ZIP = "92603";
		private const E_aBAddrT B_ADDRESS_TYPE = E_aBAddrT.Own;
		private const string B_YEARS_AT_ADDRESS = "10";

		// If this constant is changed to false, updates will need to be made to
		// ConfirmBorrowerInfoWasCoborrowerInfo, ConfirmCoborrowerInfoWasBorrowerInfo, and
		// ConfirmBorrowerInfoUnchanged.
        private const E_aAddrMailSourceT B_MAIL_ADDRESS_SOURCE = E_aAddrMailSourceT.PresentAddress;
        private const bool B_POST_ADDRESS_SOURCE_LOCKED = false;
        private const E_aAddrPostSourceT B_POST_ADDRESS_SOURCE = E_aAddrPostSourceT.MailingAddress;

        private const string B_MAIL_ADDRESS = "23423 Not Gonna Use Street";
		private const string B_MAIL_CITY = "Does Not Matter";
		private const string B_MAIL_STATE = "AK";
		private const string B_MAIL_ZIP = "95222";
        private const string B_POST_ADDRESS = "23423 Not PostClosing Street";
        private const string B_POST_CITY = "Does Not Post";
        private const string B_POST_STATE = "MT";
        private const string B_POST_ZIP = "59001";
        private const string B_PREV1_ADDRESS = "233 Not Real";
		private const string B_PREV1_CITY = "Rancho Mirage";
		private const string B_PREV1_STATE = "NM";
		private const string B_PREV1_ZIP = "92270";
		private const E_aBPrev1AddrT B_PREV1_ADDRESS_TYPE = E_aBPrev1AddrT.Rent;
		private const string B_PREV1_YEARS_AT_ADDRESS = "2";
		private const string B_PREV2_ADDRESS = "938 Fakes Place";
		private const string B_PREV2_CITY = "Anaheim";
		private const string B_PREV2_STATE = "AZ";
		private const string B_PREV2_ZIP = "92806";
		private const E_aBPrev2AddrT B_PREV2_ADDRESS_TYPE = E_aBPrev2AddrT.LivingRentFree;
		private const string B_PREV2_YEARS_AT_ADDRESS = "7";
        private const bool B_PREFERRED_NAME_LOCKED = true;
        private const string B_PREFERRED_NAME = "Fakey McFakerson";
		#endregion

		#region constants for coborrower
		private const string C_FIRST_NAME = "Janet";
		private const string C_MIDDLE_NAME = "Ellen";
		private const string C_LAST_NAME = "Smith";
		private const string C_SUFFIX = "III";
		private const string C_SSN = "111-22-3333";
		private const string C_DOB = "2/16/1959";
		private const string C_YEARS_OF_SCHOOL = "3";
		private const E_aCMaritalStatT C_MARITAL_STATUS = E_aCMaritalStatT.NotMarried;
		private const string C_NUMBER_OF_DEPENDENTS = "2";
		private const string C_DEPENDENTS_AGES = "17, 13";
		private const string C_HOME_PHONE = "(222) 333-4444";
		private const string C_CELL_PHONE = "(555) 666-7777";
		private const string C_WORK_PHONE = "(888) 999-0000";
		private const string C_FAX = "(111) 222-3333";
		private const string C_EMAIL = "janet@notReal.net";
		private const string C_ADDRESS = "234 Yummy Lane";
		private const string C_CITY = "Bisbee";
		private const string C_STATE = "AR";
		private const string C_ZIP = "85603";
		private const E_aCAddrT C_ADDRESS_TYPE = E_aCAddrT.LivingRentFree;
		private const string C_YEARS_AT_ADDRESS = "1";

		// If this constant is changed to true, updates will need to be made to
		// ConfirmBorrowerInfoWasCoborrowerInfo, and ConfirmCoborrowerInfoWasBorrowerInfo.
        private const E_aAddrMailSourceT C_MAIL_ADDRESS_SOURCE = E_aAddrMailSourceT.Other;
        private const bool C_POST_ADDRESS_SOURCE_LOCKED = true;
        private const E_aAddrPostSourceT C_POST_ADDRESS_SOURCE = E_aAddrPostSourceT.Other;

        private const string C_MAIL_ADDRESS = "2342 Funny Road";
		private const string C_MAIL_CITY = "Dragoon";
		private const string C_MAIL_STATE = "CA";
		private const string C_MAIL_ZIP = "85609";
        private const string C_POST_ADDRESS = "59466 Cob Roast Road";
        private const string C_POST_CITY = "Cheyenne";
        private const string C_POST_STATE = "WY";
        private const string C_POST_ZIP = "82005";
        private const string C_PREV1_ADDRESS = "121212 What Lane";
		private const string C_PREV1_CITY = "South Webster";
		private const string C_PREV1_STATE = "CO";
		private const string C_PREV1_ZIP = "45682";
		private const E_aCPrev1AddrT C_PREV1_ADDRESS_TYPE = E_aCPrev1AddrT.Own;
		private const string C_PREV1_YEARS_AT_ADDRESS = "24";
		private const string C_PREV2_ADDRESS = "1212312 Where Am I Place";
		private const string C_PREV2_CITY = "Cincinnati";
		private const string C_PREV2_STATE = "CT";
		private const string C_PREV2_ZIP = "45221";
		private const E_aCPrev2AddrT C_PREV2_ADDRESS_TYPE = E_aCPrev2AddrT.LeaveBlank;
		private const string C_PREV2_YEARS_AT_ADDRESS = "5";
        private const bool C_PREFERRED_NAME_LOCKED = false;
		#endregion

		#region borrower employment
		private const bool B_SELF_EMPLOYED = false;
		private const string B_E_EMPLOYER = "Some employer for the borrower";
		private const string B_E_ADDRESS = "434543 Employment Lane";
		private const string B_E_CITY = "Employmentville";
		private const string B_E_STATE = "DE";
		private const string B_E_ZIP = "32854";
		private const string B_E_POSITION = "Lead Employee";
		private const string B_E_YEARS_ON_JOB = "4";
		private const string B_E_YEARS_IN_PROFESSION = "10";
		private const bool B_E_PHONE_LOCKED = true;
		private const string B_E_PHONE = "(395) 473-4832";
		private const string B_E_FAX = "(345) 098-0004";
		private const string B_E_VERIF_SENT = "1/11/2009";
		private const string B_E_REORDER = "1/12/2009";
		private const string B_E_RECEIVED = "1/13/2009";
		private const string B_E_EXPECTED = "1/14/2009";

		private const bool B_P_SELF_EMPLOYED = false;
		private const string B_P_EMPLOYER = "Some other employer";
		private const string B_P_ADDRESS = "254234 Some Place";
		private const string B_P_CITY = "Some City In Which To Work";
		private const string B_P_STATE = "FL";
		private const string B_P_ZIP = "34459";
		private const string B_P_POSITION = "Really Cool Job";
		private const string B_P_DATE_FROM = "1/11/2007";
		private const string B_P_DATE_TO = "1/12/2008";
		private const string B_P_MONTHLY_INCOME = "$800.00";
		private const string B_P_PHONE = "(843) 345-0003";
		private const string B_P_FAX = "(776) 909-0000";
		private const string B_P_VERIF_SENT = "2/2/1998";
		private const string B_P_REORDER = "2/3/1998";
		private const string B_P_RECEIVED = "2/4/1998";
		private const string B_P_EXPECTED = "2/5/1998";
		#endregion

		#region coborrower's employment
		private const bool C_SELF_EMPLOYED = true;
		private const string C_E_EMPLOYER = "A Place To Work";
		private const string C_E_ADDRESS = "3453 Something Drive";
		private const string C_E_CITY = "City of Doom";
		private const string C_E_STATE = "GA";
		private const string C_E_ZIP = "39583";
		private const string C_E_POSITION = "Some Employee";
		private const string C_E_YEARS_ON_JOB = "3";
		private const string C_E_YEARS_IN_PROFESSION = "9";
		private const bool C_E_PHONE_LOCKED = false;
		private const string C_E_PHONE = "(231) 487-5456";
		private const string C_E_FAX = "(234) 432-1112";
		private const string C_E_VERIF_SENT = "4/11/2002";
		private const string C_E_REORDER = "5/12/2003";
		private const string C_E_RECEIVED = "6/13/2004";
		private const string C_E_EXPECTED = "7/14/2005";

		private const bool C_P_SELF_EMPLOYED = true;
		private const string C_P_EMPLOYER = "Blah blah employer";
		private const string C_P_ADDRESS = "324 Somes";
		private const string C_P_CITY = "CITYCITYcity";
		private const string C_P_STATE = "HI";
		private const string C_P_ZIP = "45643";
		private const string C_P_POSITION = "Awesome Job";
		private const string C_P_DATE_FROM = "5/11/2007";
		private const string C_P_DATE_TO = "5/12/2008";
		private const string C_P_MONTHLY_INCOME = "$9,700.00";
		private const string C_P_PHONE = "(456) 664-4367";
		private const string C_P_FAX = "(876) 223-3243";
		private const string C_P_VERIF_SENT = "6/2/1998";
		private const string C_P_REORDER = "7/3/1998";
		private const string C_P_RECEIVED = "8/4/1998";
		private const string C_P_EXPECTED = "9/5/1998";
		#endregion

		#region borrower monthly income
		private const string B_BASE_INCOME = "$3,033.33";
		private const string B_OVERTIME = "$500.00";
		private const string B_BONUSES = "$321.34";
		private const string B_COMMISSION = "$254.43";
		private const string B_DIVIDENDS = "$34.43";
		private const string B_NET_RENT = "$500.33";

		// If this field is changed to true, ConfirmOtherMonthlyIncomeCoborrowerDeleted
		// will need to be changed.
		private const bool OTHER_INCOME_1_FOR_COBORROWER = false;

		private const string OTHER_INCOME_1_DESC = "Trust Income";
		private const decimal OTHER_INCOME_1_AMOUNT = 567.65M;

		// If this field is changed to true, ConfirmOtherMonthlyIncomeCoborrowerDeleted
		// will need to be changed.
		private const bool OTHER_INCOME_2_FOR_COBORROWER = false;

		private const string OTHER_INCOME_2_DESC = "Foster Care";
		private const decimal OTHER_INCOME_2_AMOUNT = 605.65M;
		#endregion

		#region coborrower monthly income
		private const string C_BASE_INCOME = "$2,023.56";
		private const string C_OVERTIME = "$456.05";
		private const string C_BONUSES = "$345.44";
		private const string C_COMMISSION = "$678.34";
		private const string C_DIVIDENDS = "$234.12";
		private const string C_NET_RENT = "$423.46";

		// If this field is changed to false, ConfirmOtherMonthlyIncomeCoborrowerDeleted
		// will need to be changed.
		private const bool OTHER_INCOME_3_FOR_COBORROWER = true;

		private const string OTHER_INCOME_3_DESC = "Military Base Pay";
		private const decimal OTHER_INCOME_3_AMOUNT = 2343.34M;
		#endregion

		#region borrower liabilities
		private const E_LiaOwnerT B_L_OWNER = E_LiaOwnerT.Borrower;
		private const E_DebtRegularT B_L_DEBT_TYPE = E_DebtRegularT.Installment;
		private const string B_L_COMPANY = "That One Company";
		private const string B_L_ADDRESS = "234 One Company's Street";
		private const string B_L_CITY = "Cityplaceville";
		private const string B_L_STATE = "ID";
		private const string B_L_ZIP = "49678";
		private const string B_L_PHONE = "(493) 496-3059";
		private const string B_L_FAX = "(394) 345-3939";
		private const string B_L_DESC = "Taxes";
		private const string B_L_ACCOUNT_HOLDER_NAME = "Good Guy Jack";
		private const string B_L_ACCOUNT_NUMBER = "49495695";
		private const string B_L_BALANCE = "$3,045.49";
		private const string B_L_PMT = "$305.00";
		private const string B_L_MONTHS_LEFT = "4";
		private const string B_L_RATE = "3.345%";
		private const string B_L_TERM = "0";
		private const string B_L_DUE_IN = "3";
		private const string B_L_LATE_30 = "0";
		private const string B_L_LATE_60 = "1";
		private const string B_L_LATE_90 = "2";
		private const bool B_L_WILL_BE_PAID_OFF = false;
		private const bool B_L_DEBT_SHOULD_BE_INCLUDED_IN_RATIOS = true;
		private const bool B_L_DEBT_IS_NEW_SUBJECT_TO_PIGGYBACK = false;
		private const bool B_L_INCLUDE_IN_REPOSSESSION = false;
		private const bool B_L_INCLUDE_IN_BANKRUPTCY = false;
		private const bool B_L_INCLUDE_IN_FORECLOSURE = false;
		private const bool B_L_EXCLUDE_FROM_UNDERWRITING = false;
		private const string B_L_VERIF_SENT = "2/13/2008";
		private const string B_L_REORDER = "2/14/2008";
		private const string B_L_RECEIVED = "2/15/2008";
		private const string B_L_EXPECTED = "2/16/2008";
		#endregion

		#region coborrower liabilities
		private const E_LiaOwnerT C_L_OWNER = E_LiaOwnerT.CoBorrower;
		private const E_DebtRegularT C_L_DEBT_TYPE = E_DebtRegularT.Mortgage;
		private const string C_L_COMPANY = "One More Company";
		private const string C_L_ADDRESS = "23423 More Company's Drive";
		private const string C_L_CITY = "Thisisacity";
		private const string C_L_STATE = "IL";
		private const string C_L_ZIP = "48625";
		private const string C_L_PHONE = "(123) 753-4965";
		private const string C_L_FAX = "(548) 546-3545";
		private const string C_L_DESC = "Installment Loan";
		private const string C_L_ACCOUNT_HOLDER_NAME = "Bad Dude Jack";
		private const string C_L_ACCOUNT_NUMBER = "8974654";
		private const string C_L_BALANCE = "$1,569.62";
		private const string C_L_PMT = "$564.32";
		private const string C_L_MONTHS_LEFT = "16";
		private const string C_L_RATE = "1.952%";
		private const string C_L_TERM = "1";
		private const string C_L_DUE_IN = "4";
		private const string C_L_LATE_30 = "5";
		private const string C_L_LATE_60 = "4";
		private const string C_L_LATE_90 = "3";
		private const bool C_L_WILL_BE_PAID_OFF = true;
		private const bool C_L_DEBT_SHOULD_BE_INCLUDED_IN_RATIOS = false;
		private const bool C_L_DEBT_IS_NEW_SUBJECT_TO_PIGGYBACK = true;
		private const bool C_L_INCLUDE_IN_REPOSSESSION = true;
		private const bool C_L_INCLUDE_IN_BANKRUPTCY = true;
		private const bool C_L_INCLUDE_IN_FORECLOSURE = true;
		private const bool C_L_EXCLUDE_FROM_UNDERWRITING = true;
		private const string C_L_VERIF_SENT = "3/17/2007";
		private const string C_L_REORDER = "3/18/2007";
		private const string C_L_RECEIVED = "3/19/2008";
		private const string C_L_EXPECTED = "3/20/2008";
		#endregion

		#region joint liabilities
		private const E_LiaOwnerT J_L_OWNER = E_LiaOwnerT.Joint;
		private const E_DebtRegularT J_L_DEBT_TYPE = E_DebtRegularT.Revolving;
		private const string J_L_COMPANY = "Super Company";
		private const string J_L_ADDRESS = "2343333 More Drive Street";
		private const string J_L_CITY = "Another string";
		private const string J_L_STATE = "IN";
		private const string J_L_ZIP = "65892";
		private const string J_L_PHONE = "(586) 125-9856";
		private const string J_L_FAX = "(845) 326-6488";
		private const string J_L_DESC = "Wow Loan";
		private const string J_L_ACCOUNT_HOLDER_NAME = "Mister Guy";
		private const string J_L_ACCOUNT_NUMBER = "4556871";
		private const string J_L_BALANCE = "$12,354.21";
		private const string J_L_PMT = "$659.02";
		private const string J_L_MONTHS_LEFT = "23";
		private const string J_L_RATE = "2.654%";
		private const string J_L_TERM = "3";
		private const string J_L_DUE_IN = "3";
		private const string J_L_LATE_30 = "2";
		private const string J_L_LATE_60 = "4";
		private const string J_L_LATE_90 = "2";
		private const bool J_L_WILL_BE_PAID_OFF = true;
		private const bool J_L_DEBT_SHOULD_BE_INCLUDED_IN_RATIOS = false;
		private const bool J_L_DEBT_IS_NEW_SUBJECT_TO_PIGGYBACK = true;
		private const bool J_L_INCLUDE_IN_REPOSSESSION = true;
		private const bool J_L_INCLUDE_IN_BANKRUPTCY = true;
		private const bool J_L_INCLUDE_IN_FORECLOSURE = true;
		private const bool J_L_EXCLUDE_FROM_UNDERWRITING = true;
		private const string J_L_VERIF_SENT = "5/17/2007";
		private const string J_L_REORDER = "5/19/2007";
		private const string J_L_RECEIVED = "6/2/2008";
		private const string J_L_EXPECTED = "7/6/2008";
		#endregion

		#region borrower's assets
		private const E_AssetOwnerT B_A_OWNER = E_AssetOwnerT.Borrower;
		private const E_AssetRegularT B_A_TYPE = E_AssetRegularT.Checking;
		private const string B_A_OTHER_TYPE = "";
		private const string B_A_COMPANY = "BorrowerAssetCompany";
		private const string B_A_DEPARTMENT = "Dept. of Assets";
		private const string B_A_ADDRESS = "40408 Sunshine";
		private const string B_A_CITY = "Rancho City";
		private const string B_A_STATE = "IA";
		private const string B_A_ZIP = "09834";
		private const string B_A_DESC = "This is my description.";
		private const string B_A_VALUE = "$45,000.00";
		private const string B_A_ACCOUNT_HOLDER_NAME = "Coca Cola, III";
		private const string B_A_ACCOUNT_NUMBER = "38938";
		private const string B_A_VERIF_SENT = "6/21/2008";
		private const string B_A_REORDER = "6/22/2008";
		private const string B_A_RECEIVED = "6/23/2008";
		private const string B_A_EXPECTED = "6/24/2008";
		#endregion

		#region coborrower's assets
		private const E_AssetOwnerT C_A_OWNER = E_AssetOwnerT.CoBorrower;
		private const E_AssetRegularT C_A_TYPE = E_AssetRegularT.OtherLiquidAsset;
		private const string C_A_OTHER_TYPE = "Ultra necklace collection";
		private const string C_A_COMPANY = "They Like Necklaces";
		private const string C_A_DEPARTMENT = "HLS";
		private const string C_A_ADDRESS = "445537 Goldie";
		private const string C_A_CITY = "Long Viejo";
		private const string C_A_STATE = "KS";
		private const string C_A_ZIP = "45452";
		private const string C_A_DESC = "Yup.";
		private const string C_A_VALUE = "$156,458.21";
		private const string C_A_ACCOUNT_HOLDER_NAME = "That Guy, Jr.";
		private const string C_A_ACCOUNT_NUMBER = "987564";
		private const string C_A_VERIF_SENT = "9/11/2006";
		private const string C_A_REORDER = "9/12/2006";
		private const string C_A_RECEIVED = "9/13/2006";
		private const string C_A_EXPECTED = "9/14/2006";
		#endregion

		#region joint assets
		private const E_AssetOwnerT J_A_OWNER = E_AssetOwnerT.Joint;
		private const E_AssetRegularT J_A_TYPE = E_AssetRegularT.Savings;
		private const string J_A_OTHER_TYPE = "";
		private const string J_A_COMPANY = "Joint Asset Company";
		private const string J_A_DEPARTMENT = "JAC";
		private const string J_A_ADDRESS = "47 Someplace";
		private const string J_A_CITY = "Mission Beach";
		private const string J_A_STATE = "KY";
		private const string J_A_ZIP = "26698";
		private const string J_A_DESC = "Nope.";
		private const string J_A_VALUE = "$15,488.37";
		private const string J_A_ACCOUNT_HOLDER_NAME = "Some One";
		private const string J_A_ACCOUNT_NUMBER = "959598423";
		private const string J_A_VERIF_SENT = "2/1/2002";
		private const string J_A_REORDER = "3/11/2003";
		private const string J_A_RECEIVED = "4/13/2004";
		private const string J_A_EXPECTED = "4/14/2005";
		#endregion

        private CPageData CreatePageData()
        {
            return CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.m_loanId, typeof(BorrowerManagementTest));
        }

        [SetUp]
        public void Setup()
        {
            System.Threading.Thread.CurrentPrincipal = m_currentPrincipal;
            var fileCreator = CLoanFileCreator.GetCreator(m_currentPrincipal, E_LoanCreationSource.UserCreateFromBlank);
            this.collectionType = E_sLqbCollectionT.Legacy;

            this.legacyLoanId = new Lazy<Guid>(() => fileCreator.CreateBlankLoanFile());
            this.lqbCollectionsLoanId = new Lazy<Guid>(() => fileCreator.CreateBlankUladLoanFile());
        }

        [TearDown]
        public void TearDown()
        {
            if (this.legacyLoanId.IsValueCreated)
            {
                Tools.DeclareLoanFileInvalid(m_currentPrincipal, this.legacyLoanId.Value, sendNotifications: false, generateLinkLoanMsgEvent: false);
            }

            if (this.lqbCollectionsLoanId.IsValueCreated)
            {
                Tools.DeclareLoanFileInvalid(m_currentPrincipal, this.lqbCollectionsLoanId.Value, sendNotifications: false, generateLinkLoanMsgEvent: false);
            }
        }

        private void PopulateBorrowerAndCoborrower()
        {
            m_dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            CAppData dataApp = m_dataLoan.GetAppData(0);
            if (m_dataLoan.sBorrowerApplicationCollectionT != E_sLqbCollectionT.Legacy)
            {
                var appIdentifier = DataObjectIdentifier<DataObjectKind.LegacyApplication, Guid>.Create(dataApp.aAppId);
                m_dataLoan.AddCoborrowerToLegacyApplication(appIdentifier);
            }

            dataApp.aNetRentI1003Lckd = NET_RENT_LOCK;

            #region set borrower information
            dataApp.aBFirstNm = B_FIRST_NAME;
            dataApp.aBMidNm = B_MIDDLE_NAME;
            dataApp.aBLastNm = B_LAST_NAME;
            dataApp.aBSuffix = B_SUFFIX;
            dataApp.aBSsn = B_SSN;
            dataApp.aBDob_rep = B_DOB;
            dataApp.aBSchoolYrs_rep = B_YEARS_OF_SCHOOL;
            dataApp.aBMaritalStatT = B_MARITAL_STATUS;
            dataApp.aBDependNum_rep = B_NUMBER_OF_DEPENDENTS;
            dataApp.aBDependAges = B_DEPENDENTS_AGES;
            dataApp.aBHPhone = B_HOME_PHONE;
            dataApp.aBCellPhone = B_CELL_PHONE;
            dataApp.aBBusPhone = B_WORK_PHONE;
            dataApp.aBFax = B_FAX;
            dataApp.aBEmail = B_EMAIL;
            dataApp.aBAddr = B_ADDRESS;
            dataApp.aBCity = B_CITY;
            dataApp.aBState = B_STATE;
            dataApp.aBZip = B_ZIP;
            dataApp.aBAddrT = B_ADDRESS_TYPE;
            dataApp.aBAddrYrs = B_YEARS_AT_ADDRESS;
            dataApp.aBAddrMailSourceT = B_MAIL_ADDRESS_SOURCE;
            dataApp.aBAddrMail = B_MAIL_ADDRESS;
            dataApp.aBCityMail = B_MAIL_CITY;
            dataApp.aBStateMail = B_MAIL_STATE;
            dataApp.aBZipMail = B_MAIL_ZIP;
            dataApp.aBAddrPostSourceTLckd = B_POST_ADDRESS_SOURCE_LOCKED;
            dataApp.aBAddrPostSourceT = B_POST_ADDRESS_SOURCE;
            dataApp.aBAddrPost = B_POST_ADDRESS;
            dataApp.aBCityPost = B_POST_CITY;
            dataApp.aBStatePost = B_POST_STATE;
            dataApp.aBZipPost = B_POST_ZIP;
            dataApp.aBPrev1Addr = B_PREV1_ADDRESS;
            dataApp.aBPrev1City = B_PREV1_CITY;
            dataApp.aBPrev1State = B_PREV1_STATE;
            dataApp.aBPrev1Zip = B_PREV1_ZIP;
            dataApp.aBPrev1AddrT = B_PREV1_ADDRESS_TYPE;
            dataApp.aBPrev1AddrYrs = B_PREV1_YEARS_AT_ADDRESS;
            dataApp.aBPrev2Addr = B_PREV2_ADDRESS;
            dataApp.aBPrev2City = B_PREV2_CITY;
            dataApp.aBPrev2State = B_PREV2_STATE;
            dataApp.aBPrev2Zip = B_PREV2_ZIP;
            dataApp.aBPrev2AddrT = B_PREV2_ADDRESS_TYPE;
            dataApp.aBPrev2AddrYrs = B_PREV2_YEARS_AT_ADDRESS;
            dataApp.aBPreferredNmLckd = B_PREFERRED_NAME_LOCKED;
            dataApp.aBPreferredNm = B_PREFERRED_NAME;
            #endregion

            #region set coborrower information
            dataApp.aCFirstNm = C_FIRST_NAME;
            dataApp.aCMidNm = C_MIDDLE_NAME;
            dataApp.aCLastNm = C_LAST_NAME;
            dataApp.aCSuffix = C_SUFFIX;
            dataApp.aCSsn = C_SSN;
            dataApp.aCDob_rep = C_DOB;
            dataApp.aCSchoolYrs_rep = C_YEARS_OF_SCHOOL;
            dataApp.aCMaritalStatT = C_MARITAL_STATUS;
            dataApp.aCDependNum_rep = C_NUMBER_OF_DEPENDENTS;
            dataApp.aCDependAges = C_DEPENDENTS_AGES;
            dataApp.aCHPhone = C_HOME_PHONE;
            dataApp.aCCellPhone = C_CELL_PHONE;
            dataApp.aCBusPhone = C_WORK_PHONE;
            dataApp.aCFax = C_FAX;
            dataApp.aCEmail = C_EMAIL;
            dataApp.aCAddr = C_ADDRESS;
            dataApp.aCCity = C_CITY;
            dataApp.aCState = C_STATE;
            dataApp.aCZip = C_ZIP;
            dataApp.aCAddrT = C_ADDRESS_TYPE;
            dataApp.aCAddrYrs = C_YEARS_AT_ADDRESS;
            dataApp.aCAddrMailSourceT = C_MAIL_ADDRESS_SOURCE;
            dataApp.aCAddrMail = C_MAIL_ADDRESS;
            dataApp.aCCityMail = C_MAIL_CITY;
            dataApp.aCStateMail = C_MAIL_STATE;
            dataApp.aCZipMail = C_MAIL_ZIP;
            dataApp.aCAddrPostSourceTLckd = C_POST_ADDRESS_SOURCE_LOCKED;
            dataApp.aCAddrPostSourceT = C_POST_ADDRESS_SOURCE;
            dataApp.aCAddrPost = C_POST_ADDRESS;
            dataApp.aCCityPost = C_POST_CITY;
            dataApp.aCStatePost = C_POST_STATE;
            dataApp.aCZipPost = C_POST_ZIP;
            dataApp.aCPrev1Addr = C_PREV1_ADDRESS;
            dataApp.aCPrev1City = C_PREV1_CITY;
            dataApp.aCPrev1State = C_PREV1_STATE;
            dataApp.aCPrev1Zip = C_PREV1_ZIP;
            dataApp.aCPrev1AddrT = C_PREV1_ADDRESS_TYPE;
            dataApp.aCPrev1AddrYrs = C_PREV1_YEARS_AT_ADDRESS;
            dataApp.aCPrev2Addr = C_PREV2_ADDRESS;
            dataApp.aCPrev2City = C_PREV2_CITY;
            dataApp.aCPrev2State = C_PREV2_STATE;
            dataApp.aCPrev2Zip = C_PREV2_ZIP;
            dataApp.aCPrev2AddrT = C_PREV2_ADDRESS_TYPE;
            dataApp.aCPrev2AddrYrs = C_PREV2_YEARS_AT_ADDRESS;
            dataApp.aCPreferredNmLckd = C_PREFERRED_NAME_LOCKED;
            #endregion

            #region borrower employment
            IEmpCollection employmentCollectionB = dataApp.aBEmpCollection;
            IPrimaryEmploymentRecord primaryEmpB = employmentCollectionB.GetPrimaryEmp(true);
            primaryEmpB.IsSelfEmplmt = B_SELF_EMPLOYED;
            primaryEmpB.EmplrNm = B_E_EMPLOYER;
            primaryEmpB.EmplrAddr = B_E_ADDRESS;
            primaryEmpB.EmplrCity = B_E_CITY;
            primaryEmpB.EmplrState = B_E_STATE;
            primaryEmpB.EmplrZip = B_E_ZIP;
            primaryEmpB.JobTitle = B_E_POSITION;
            primaryEmpB.EmplmtLen_rep = B_E_YEARS_ON_JOB;
            primaryEmpB.ProfLen_rep = B_E_YEARS_IN_PROFESSION;
            dataApp.aBEmplrBusPhoneLckd = B_E_PHONE_LOCKED;
            primaryEmpB.EmplrBusPhone = B_E_PHONE;
            m_bEmplrBusPhone = primaryEmpB.EmplrBusPhone;
            primaryEmpB.EmplrFax = B_E_FAX;
            primaryEmpB.VerifSentD_rep = B_E_VERIF_SENT;
            primaryEmpB.VerifReorderedD_rep = B_E_REORDER;
            primaryEmpB.VerifRecvD_rep = B_E_RECEIVED;
            primaryEmpB.VerifExpD_rep = B_E_EXPECTED;
            employmentCollectionB.Flush();

            IRegularEmploymentRecord empB = employmentCollectionB.AddRegularRecord();
            m_borrowerPreviousEmployerRecord = empB.RecordId;
            empB.IsSelfEmplmt = B_P_SELF_EMPLOYED;
            empB.EmplrNm = B_P_EMPLOYER;
            empB.EmplrAddr = B_P_ADDRESS;
            empB.EmplrCity = B_P_CITY;
            empB.EmplrState = B_P_STATE;
            empB.EmplrZip = B_P_ZIP;
            empB.JobTitle = B_P_POSITION;
            empB.EmplmtStartD_rep = B_P_DATE_FROM;
            empB.EmplmtEndD_rep = B_P_DATE_TO;
            empB.MonI_rep = B_P_MONTHLY_INCOME;
            empB.EmplrBusPhone = B_P_PHONE;
            empB.EmplrFax = B_P_FAX;
            empB.VerifSentD_rep = B_P_VERIF_SENT;
            empB.VerifReorderedD_rep = B_P_REORDER;
            empB.VerifRecvD_rep = B_P_RECEIVED;
            empB.VerifExpD_rep = B_P_EXPECTED;
            employmentCollectionB.Flush();
            #endregion

            #region coborrower employment
            IEmpCollection employmentCollectionC = dataApp.aCEmpCollection;
            IPrimaryEmploymentRecord primaryEmpC = employmentCollectionC.GetPrimaryEmp(true);
            primaryEmpC.IsSelfEmplmt = C_SELF_EMPLOYED;
            primaryEmpC.EmplrNm = C_E_EMPLOYER;
            primaryEmpC.EmplrAddr = C_E_ADDRESS;
            primaryEmpC.EmplrCity = C_E_CITY;
            primaryEmpC.EmplrState = C_E_STATE;
            primaryEmpC.EmplrZip = C_E_ZIP;
            primaryEmpC.JobTitle = C_E_POSITION;
            primaryEmpC.EmplmtLen_rep = C_E_YEARS_ON_JOB;
            primaryEmpC.ProfLen_rep = C_E_YEARS_IN_PROFESSION;
            dataApp.aCEmplrBusPhoneLckd = C_E_PHONE_LOCKED;
            primaryEmpC.EmplrBusPhone = C_E_PHONE;
            m_cEmplrBusPhone = primaryEmpC.EmplrBusPhone;
            primaryEmpC.EmplrFax = C_E_FAX;
            primaryEmpC.VerifSentD_rep = C_E_VERIF_SENT;
            primaryEmpC.VerifReorderedD_rep = C_E_REORDER;
            primaryEmpC.VerifRecvD_rep = C_E_RECEIVED;
            primaryEmpC.VerifExpD_rep = C_E_EXPECTED;
            employmentCollectionC.Flush();

            IRegularEmploymentRecord empC = employmentCollectionC.AddRegularRecord();
            m_coborrowerPreviousEmployerRecord = empC.RecordId;
            empC.IsSelfEmplmt = C_P_SELF_EMPLOYED;
            empC.EmplrNm = C_P_EMPLOYER;
            empC.EmplrAddr = C_P_ADDRESS;
            empC.EmplrCity = C_P_CITY;
            empC.EmplrState = C_P_STATE;
            empC.EmplrZip = C_P_ZIP;
            empC.JobTitle = C_P_POSITION;
            empC.EmplmtStartD_rep = C_P_DATE_FROM;
            empC.EmplmtEndD_rep = C_P_DATE_TO;
            empC.MonI_rep = C_P_MONTHLY_INCOME;
            empC.EmplrBusPhone = C_P_PHONE;
            empC.EmplrFax = C_P_FAX;
            empC.VerifSentD_rep = C_P_VERIF_SENT;
            empC.VerifReorderedD_rep = C_P_REORDER;
            empC.VerifRecvD_rep = C_P_RECEIVED;
            empC.VerifExpD_rep = C_P_EXPECTED;
            employmentCollectionC.Flush();
            #endregion

            PopulateIncome(dataApp);

            #region borrower liabilities
            ILiaCollection liabilityCollection = dataApp.aLiaCollection;
            ILiabilityRegular recordB = liabilityCollection.AddRegularRecord();
            m_borrowerLiabilityId = recordB.RecordId;
            recordB.OwnerT = B_L_OWNER;
            recordB.DebtT = B_L_DEBT_TYPE;
            recordB.ComNm = B_L_COMPANY;
            recordB.ComAddr = B_L_ADDRESS;
            recordB.ComCity = B_L_CITY;
            recordB.ComState = B_L_STATE;
            recordB.ComZip = B_L_ZIP;
            recordB.ComPhone = B_L_PHONE;
            recordB.ComFax = B_L_FAX;
            recordB.Desc = B_L_DESC;
            recordB.AccNm = B_L_ACCOUNT_HOLDER_NAME;
            recordB.AccNum = B_L_ACCOUNT_NUMBER;
            recordB.Bal_rep = B_L_BALANCE;
            recordB.Pmt_rep = B_L_PMT;
            recordB.RemainMons_rep = B_L_MONTHS_LEFT;
            recordB.R_rep = B_L_RATE;
            recordB.OrigTerm_rep = B_L_TERM;
            recordB.Due_rep = B_L_DUE_IN;
            recordB.Late30_rep = B_L_LATE_30;
            recordB.Late60_rep = B_L_LATE_60;
            recordB.Late90Plus_rep = B_L_LATE_90;
            recordB.WillBePdOff = B_L_WILL_BE_PAID_OFF;
            recordB.NotUsedInRatio = !B_L_DEBT_SHOULD_BE_INCLUDED_IN_RATIOS;
            recordB.IsPiggyBack = B_L_DEBT_IS_NEW_SUBJECT_TO_PIGGYBACK;
            recordB.IncInReposession = B_L_INCLUDE_IN_REPOSSESSION;
            recordB.IncInBankruptcy = B_L_INCLUDE_IN_BANKRUPTCY;
            recordB.IncInForeclosure = B_L_INCLUDE_IN_FORECLOSURE;
            recordB.ExcFromUnderwriting = B_L_EXCLUDE_FROM_UNDERWRITING;
            recordB.VerifSentD_rep = B_L_VERIF_SENT;
            recordB.VerifReorderedD_rep = B_L_REORDER;
            recordB.VerifRecvD_rep = B_L_RECEIVED;
            recordB.VerifExpD_rep = B_L_EXPECTED;
            liabilityCollection.Flush();
            #endregion

            #region coborrower liabilities
            ILiabilityRegular recordC = liabilityCollection.AddRegularRecord();
            m_coborrowerLiabilityId = recordC.RecordId;
            recordC.OwnerT = C_L_OWNER;
            recordC.DebtT = C_L_DEBT_TYPE;
            recordC.ComNm = C_L_COMPANY;
            recordC.ComAddr = C_L_ADDRESS;
            recordC.ComCity = C_L_CITY;
            recordC.ComState = C_L_STATE;
            recordC.ComZip = C_L_ZIP;
            recordC.ComPhone = C_L_PHONE;
            recordC.ComFax = C_L_FAX;
            recordC.Desc = C_L_DESC;
            recordC.AccNm = C_L_ACCOUNT_HOLDER_NAME;
            recordC.AccNum = C_L_ACCOUNT_NUMBER;
            recordC.Bal_rep = C_L_BALANCE;
            recordC.Pmt_rep = C_L_PMT;
            recordC.RemainMons_rep = C_L_MONTHS_LEFT;
            recordC.R_rep = C_L_RATE;
            recordC.OrigTerm_rep = C_L_TERM;
            recordC.Due_rep = C_L_DUE_IN;
            recordC.Late30_rep = C_L_LATE_30;
            recordC.Late60_rep = C_L_LATE_60;
            recordC.Late90Plus_rep = C_L_LATE_90;
            recordC.WillBePdOff = C_L_WILL_BE_PAID_OFF;
            recordC.NotUsedInRatio = !C_L_DEBT_SHOULD_BE_INCLUDED_IN_RATIOS;
            recordC.IsPiggyBack = C_L_DEBT_IS_NEW_SUBJECT_TO_PIGGYBACK;
            recordC.IncInReposession = C_L_INCLUDE_IN_REPOSSESSION;
            recordC.IncInBankruptcy = C_L_INCLUDE_IN_BANKRUPTCY;
            recordC.IncInForeclosure = C_L_INCLUDE_IN_FORECLOSURE;
            recordC.ExcFromUnderwriting = C_L_EXCLUDE_FROM_UNDERWRITING;
            recordC.VerifSentD_rep = C_L_VERIF_SENT;
            recordC.VerifReorderedD_rep = C_L_REORDER;
            recordC.VerifRecvD_rep = C_L_RECEIVED;
            recordC.VerifExpD_rep = C_L_EXPECTED;
            liabilityCollection.Flush();
            #endregion

            #region joint liabilities
            ILiabilityRegular recordJ = liabilityCollection.AddRegularRecord();
            m_jointLiabilityId = recordJ.RecordId;
            recordJ.OwnerT = J_L_OWNER;
            recordJ.DebtT = J_L_DEBT_TYPE;
            recordJ.ComNm = J_L_COMPANY;
            recordJ.ComAddr = J_L_ADDRESS;
            recordJ.ComCity = J_L_CITY;
            recordJ.ComState = J_L_STATE;
            recordJ.ComZip = J_L_ZIP;
            recordJ.ComPhone = J_L_PHONE;
            recordJ.ComFax = J_L_FAX;
            recordJ.Desc = J_L_DESC;
            recordJ.AccNm = J_L_ACCOUNT_HOLDER_NAME;
            recordJ.AccNum = J_L_ACCOUNT_NUMBER;
            recordJ.Bal_rep = J_L_BALANCE;
            recordJ.Pmt_rep = J_L_PMT;
            recordJ.RemainMons_rep = J_L_MONTHS_LEFT;
            recordJ.R_rep = J_L_RATE;
            recordJ.OrigTerm_rep = J_L_TERM;
            recordJ.Due_rep = J_L_DUE_IN;
            recordJ.Late30_rep = J_L_LATE_30;
            recordJ.Late60_rep = J_L_LATE_60;
            recordJ.Late90Plus_rep = J_L_LATE_90;
            recordJ.WillBePdOff = J_L_WILL_BE_PAID_OFF;
            recordJ.NotUsedInRatio = !J_L_DEBT_SHOULD_BE_INCLUDED_IN_RATIOS;
            recordJ.IsPiggyBack = J_L_DEBT_IS_NEW_SUBJECT_TO_PIGGYBACK;
            recordJ.IncInReposession = J_L_INCLUDE_IN_REPOSSESSION;
            recordJ.IncInBankruptcy = J_L_INCLUDE_IN_BANKRUPTCY;
            recordJ.IncInForeclosure = J_L_INCLUDE_IN_FORECLOSURE;
            recordJ.ExcFromUnderwriting = J_L_EXCLUDE_FROM_UNDERWRITING;
            recordJ.VerifSentD_rep = J_L_VERIF_SENT;
            recordJ.VerifReorderedD_rep = J_L_REORDER;
            recordJ.VerifRecvD_rep = J_L_RECEIVED;
            recordJ.VerifExpD_rep = J_L_EXPECTED;
            liabilityCollection.Flush();
            #endregion

            #region borrower assets
            IAssetCollection assetCollection = dataApp.aAssetCollection;
            var assetB = assetCollection.AddRegularRecord();
            m_borrowerAssetId = assetB.RecordId;
            assetB.OwnerT = B_A_OWNER;
            assetB.AssetT = B_A_TYPE;
            assetB.OtherTypeDesc = B_A_OTHER_TYPE;
            assetB.ComNm = B_A_COMPANY;
            assetB.DepartmentName = B_A_DEPARTMENT;
            assetB.StAddr = B_A_ADDRESS;
            assetB.City = B_A_CITY;
            assetB.State = B_A_STATE;
            assetB.Zip = B_A_ZIP;
            assetB.Desc = B_A_DESC;
            assetB.Val_rep = B_A_VALUE;
            assetB.AccNm = B_A_ACCOUNT_HOLDER_NAME;
            assetB.AccNum = B_A_ACCOUNT_NUMBER;
            assetB.VerifSentD_rep = B_A_VERIF_SENT;
            assetB.VerifReorderedD_rep = B_A_REORDER;
            assetB.VerifRecvD_rep = B_A_RECEIVED;
            assetB.VerifExpD_rep = B_A_EXPECTED;
            assetCollection.Flush();
            #endregion

            #region coborrower asset
            var assetC = assetCollection.AddRegularRecord();
            m_coborrowerAssetId = assetC.RecordId;
            assetC.OwnerT = C_A_OWNER;
            assetC.AssetT = C_A_TYPE;
            assetC.OtherTypeDesc = C_A_OTHER_TYPE;
            assetC.ComNm = C_A_COMPANY;
            assetC.DepartmentName = C_A_DEPARTMENT;
            assetC.StAddr = C_A_ADDRESS;
            assetC.City = C_A_CITY;
            assetC.State = C_A_STATE;
            assetC.Zip = C_A_ZIP;
            assetC.Desc = C_A_DESC;
            assetC.Val_rep = C_A_VALUE;
            assetC.AccNm = C_A_ACCOUNT_HOLDER_NAME;
            assetC.AccNum = C_A_ACCOUNT_NUMBER;
            assetC.VerifSentD_rep = C_A_VERIF_SENT;
            assetC.VerifReorderedD_rep = C_A_REORDER;
            assetC.VerifRecvD_rep = C_A_RECEIVED;
            assetC.VerifExpD_rep = C_A_EXPECTED;
            assetCollection.Flush();
            #endregion

            #region joint asset
            var assetJ = assetCollection.AddRegularRecord();
            m_jointAssetId = assetJ.RecordId;
            assetJ.OwnerT = J_A_OWNER;
            assetJ.AssetT = J_A_TYPE;
            assetJ.OtherTypeDesc = J_A_OTHER_TYPE;
            assetJ.ComNm = J_A_COMPANY;
            assetJ.DepartmentName = J_A_DEPARTMENT;
            assetJ.StAddr = J_A_ADDRESS;
            assetJ.City = J_A_CITY;
            assetJ.State = J_A_STATE;
            assetJ.Zip = J_A_ZIP;
            assetJ.Desc = J_A_DESC;
            assetJ.Val_rep = J_A_VALUE;
            assetJ.AccNm = J_A_ACCOUNT_HOLDER_NAME;
            assetJ.AccNum = J_A_ACCOUNT_NUMBER;
            assetJ.VerifSentD_rep = J_A_VERIF_SENT;
            assetJ.VerifReorderedD_rep = J_A_REORDER;
            assetJ.VerifRecvD_rep = J_A_RECEIVED;
            assetJ.VerifExpD_rep = J_A_EXPECTED;
            assetCollection.Flush();
            #endregion

            m_dataLoan.Save();
        }

        private void RemoveCoborrowerAssociations()
        {
            m_dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            var dataApp = m_dataLoan.GetAppData(0);
            var spouseId = dataApp.aCConsumerId.ToIdentifier<DataObjectKind.Consumer>();

            var spouseIncomeIds = m_dataLoan.GetIncomeSourcesForConsumer(spouseId).Select(a => a.Key).ToArray();
            foreach (var incomeId in spouseIncomeIds)
            {
                m_dataLoan.RemoveIncomeSource(incomeId);
            }

            var spouseEmploymentIds = m_dataLoan.GetEmploymentRecordsForConsumer(spouseId).Select(a => a.Key).ToArray();
            foreach (var empId in spouseEmploymentIds)
            {
                m_dataLoan.RemoveEmploymentRecord(empId);
            }

            var spouseLiabilityIds = m_dataLoan.GetLiabilitiesForConsumer(spouseId).Select(a => a.Key).ToArray();
            foreach (var liabilityId in spouseLiabilityIds)
            {
                if (m_dataLoan.ConsumerLiabilities.Where(a => a.Value.LiabilityId == liabilityId).Count() == 1)
                {
                    m_dataLoan.RemoveLiability(liabilityId);
                }
                else
                {
                    var ownerIds = m_dataLoan.ConsumerLiabilities.Where(a => a.Value.LiabilityId == liabilityId && a.Value.ConsumerId != spouseId).Select(a => a.Value.ConsumerId).ToArray();
                    m_dataLoan.SetLiabilityOwnership(liabilityId, ownerIds.First(), ownerIds.Skip(1));
                }
            }

            var spouseAssetIds = m_dataLoan.GetAssetsForConsumer(spouseId).Select(a => a.Key).ToArray();
            foreach (var assetId in spouseAssetIds)
            {
                if (m_dataLoan.ConsumerAssets.Where(a => a.Value.AssetId == assetId).Count() == 1)
                {
                    m_dataLoan.RemoveAsset(assetId);
                }
                else
                {
                    var ownerIds = m_dataLoan.ConsumerAssets.Where(a => a.Value.AssetId == assetId && a.Value.ConsumerId != spouseId).Select(a => a.Value.ConsumerId).ToArray();
                    m_dataLoan.SetAssetOwnership(assetId, ownerIds.First(), ownerIds.Skip(1));
                }
            }

            foreach (var id in this.m_dataLoan.GetHousingHistoryEntriesForConsumer(spouseId).Select(a => a.Key).ToArray())
            {
                this.m_dataLoan.RemoveHousingHistoryEntry(id);
            }

            m_dataLoan.Save();
        }

        private void PopulateIncome(CAppData dataApp)
        {
            dataApp.aBNetRentI1003_rep = B_NET_RENT;
            dataApp.aCNetRentI1003_rep = C_NET_RENT;

            System.Collections.Generic.List<OtherIncome> otherIncomeList = dataApp.aOtherIncomeList;
            otherIncomeList.RemoveAll(o => o.Amount == 0M);
            otherIncomeList.Add(new OtherIncome
            {
                IsForCoBorrower = OTHER_INCOME_1_FOR_COBORROWER,
                Desc = OTHER_INCOME_1_DESC,
                Amount = OTHER_INCOME_1_AMOUNT,
            });
            otherIncomeList.Add(new OtherIncome
            {
                IsForCoBorrower = OTHER_INCOME_2_FOR_COBORROWER,
                Desc = OTHER_INCOME_2_DESC,
                Amount = OTHER_INCOME_2_AMOUNT,
            });
            otherIncomeList.Add(new OtherIncome
            {
                IsForCoBorrower = OTHER_INCOME_3_FOR_COBORROWER,
                Desc = OTHER_INCOME_3_DESC,
                Amount = OTHER_INCOME_3_AMOUNT,
            });

            if (m_dataLoan.sIsIncomeCollectionEnabled)
            {
                var borrowerId = dataApp.aBConsumerId.ToIdentifier<DataObjectKind.Consumer>();
                var coborrowerId = dataApp.aCConsumerId.ToIdentifier<DataObjectKind.Consumer>();

                m_dataLoan.AddIncomeSource(borrowerId, new IncomeSource
                {
                    IncomeType = IncomeType.BaseIncome,
                    MonthlyAmountData = Money.Create(B_BASE_INCOME)
                });
                m_dataLoan.AddIncomeSource(borrowerId, new IncomeSource
                {
                    IncomeType = IncomeType.Overtime,
                    MonthlyAmountData = Money.Create(B_OVERTIME)
                });
                m_dataLoan.AddIncomeSource(borrowerId, new IncomeSource
                {
                    IncomeType = IncomeType.Bonuses,
                    MonthlyAmountData = Money.Create(B_BONUSES)
                });
                m_dataLoan.AddIncomeSource(borrowerId, new IncomeSource
                {
                    IncomeType = IncomeType.Commission,
                    MonthlyAmountData = Money.Create(B_COMMISSION)
                });
                m_dataLoan.AddIncomeSource(borrowerId, new IncomeSource
                {
                    IncomeType = IncomeType.DividendsOrInterest,
                    MonthlyAmountData = Money.Create(B_DIVIDENDS)
                });

                m_dataLoan.AddIncomeSource(coborrowerId, new IncomeSource
                {
                    IncomeType = IncomeType.BaseIncome,
                    MonthlyAmountData = Money.Create(C_BASE_INCOME)
                });
                m_dataLoan.AddIncomeSource(coborrowerId, new IncomeSource
                {
                    IncomeType = IncomeType.Overtime,
                    MonthlyAmountData = Money.Create(C_OVERTIME)
                });
                m_dataLoan.AddIncomeSource(coborrowerId, new IncomeSource
                {
                    IncomeType = IncomeType.Bonuses,
                    MonthlyAmountData = Money.Create(C_BONUSES)
                });
                m_dataLoan.AddIncomeSource(coborrowerId, new IncomeSource
                {
                    IncomeType = IncomeType.Commission,
                    MonthlyAmountData = Money.Create(C_COMMISSION)
                });
                m_dataLoan.AddIncomeSource(coborrowerId, new IncomeSource
                {
                    IncomeType = IncomeType.DividendsOrInterest,
                    MonthlyAmountData = Money.Create(C_DIVIDENDS)
                });

                foreach (var consumerIdOtherIncomePair in IncomeCollectionMigration.GetMigratedOtherIncome(otherIncomeList, dataApp.aBConsumerId, dataApp.aCConsumerId))
                {
                    m_dataLoan.AddIncomeSource(consumerIdOtherIncomePair.Item1, consumerIdOtherIncomePair.Item2);
                }
            }
            else
            {
                dataApp.aBBaseI_rep = B_BASE_INCOME;
                dataApp.aBOvertimeI_rep = B_OVERTIME;
                dataApp.aBBonusesI_rep = B_BONUSES;
                dataApp.aBCommisionI_rep = B_COMMISSION;
                dataApp.aBDividendI_rep = B_DIVIDENDS;

                dataApp.aCBaseI_rep = C_BASE_INCOME;
                dataApp.aCOvertimeI_rep = C_OVERTIME;
                dataApp.aCBonusesI_rep = C_BONUSES;
                dataApp.aCCommisionI_rep = C_COMMISSION;
                dataApp.aCDividendI_rep = C_DIVIDENDS;

                dataApp.aOtherIncomeList = otherIncomeList;
            }
        }

        [TestCase(E_sLqbCollectionT.Legacy)]
        [TestCase(E_sLqbCollectionT.UseLqbCollections)]
        public void DeleteSpouse_CorrectCollectionHandling_Succeeds(E_sLqbCollectionT borrowerApplicationCollectionType)
        {
            this.collectionType = borrowerApplicationCollectionType;
            if (ConstStage.DisableBorrowerApplicationCollectionTUpdates
                && borrowerApplicationCollectionType != E_sLqbCollectionT.Legacy)
            {
                Assert.Ignore();
            }

            m_dataLoan = CreatePageData();
			PopulateBorrowerAndCoborrower();

            if (borrowerApplicationCollectionType != E_sLqbCollectionT.Legacy)
            {
                // This is the "corrrect collection handling" for Lqb collections
                m_dataLoan = CreatePageData();
                RemoveCoborrowerAssociations();
            }

            m_dataLoan = CreatePageData();
            m_dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
			CAppData dataApp = m_dataLoan.GetAppData(0);
			dataApp.DelMarriedCobor();
			m_dataLoan.Save();

            m_dataLoan = CreatePageData();

            m_dataLoan.InitLoad();
			dataApp = m_dataLoan.GetAppData(0);

            //// Temporary for case 475067 until the code changes match the DB schema changes.
            var skipEmployerBusinessPhoneAndMonthlyIncomeChecks = borrowerApplicationCollectionType == E_sLqbCollectionT.UseLqbCollections;

            ConfirmBorrowerInfoUnchanged(dataApp);
			ConfirmCoborrowerInfoDeleted(dataApp);
			ConfirmBorrowerEmploymentUnchanged(dataApp, skipEmployerBusinessPhoneAndMonthlyIncomeChecks);
			ConfirmCoborrowerEmploymentDeleted(dataApp);
			ConfirmBorrowerMonthlyIncomeUnchanged(dataApp);
			ConfirmCoborrowerMonthlyIncomeDeleted(dataApp);
			ConfirmOtherMonthlyIncomeCoborrowerDeleted(dataApp);
			ConfirmLiabilitiesCoborrowerDeleted(dataApp);
			ConfirmAssetsCoborrowerDeleted(dataApp);
		}

        [Test]
        public void DeleteSpouse_LqbCollectionsWithAssociations_Throws()
        {
            if (ConstStage.DisableBorrowerApplicationCollectionTUpdates)
            {
                Assert.Ignore();
            }

            this.collectionType = E_sLqbCollectionT.UseLqbCollections;

            m_dataLoan = CreatePageData();
            PopulateBorrowerAndCoborrower();

            m_dataLoan = CreatePageData();
            m_dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            CAppData dataApp = m_dataLoan.GetAppData(0);
            Assert.Throws<CBaseException>(() => dataApp.DelMarriedCobor());
        }

        private void ConfirmAssetsCoborrowerDeleted(CAppData dataApp)
		{
			IAssetCollection assetCollection = dataApp.aAssetCollection;
			var asset1 = assetCollection.GetRegRecordOf(m_borrowerAssetId);
			var asset2 = assetCollection.GetRegRecordOf(m_jointAssetId);

			ConfirmBorrowerAssetsUnchanged(asset1);

			bool coborrowerAssetsDeleted = ! assetCollection.HasRecordOf(m_coborrowerAssetId);
			Assert.AreEqual(coborrowerAssetsDeleted, true, "coborrower assets");

			ConfirmJointAssetsUnchanged(Operation.DeleteCoborrower, asset2, dataApp.LoanData.sBorrowerApplicationCollectionT);
		}

		private void ConfirmBorrowerAssetsUnchanged(IAssetRegular asset)
		{
			Assert.AreEqual(E_AssetOwnerT.Borrower, asset.OwnerT, "borrower asset - OwnerT");
			Assert.AreEqual(B_A_TYPE, asset.AssetT, "borrower asset - AssetT");
			Assert.AreEqual(B_A_OTHER_TYPE, asset.OtherTypeDesc, "borrower asset - OtherTypeDesc");
			Assert.AreEqual(B_A_COMPANY, asset.ComNm, "borrower asset - ComNm");
			Assert.AreEqual(B_A_DEPARTMENT, asset.DepartmentName, "borrower asset - DepartmentName");
			Assert.AreEqual(B_A_ADDRESS, asset.StAddr, "borrower asset - StAddr");
			Assert.AreEqual(B_A_CITY, asset.City, "borrower asset - City");
			Assert.AreEqual(B_A_STATE, asset.State, "borrower asset - State");
			Assert.AreEqual(B_A_ZIP, asset.Zip, "borrower asset - Zip");
			Assert.AreEqual(B_A_DESC, asset.Desc, "borrower asset - Desc");
			Assert.AreEqual(B_A_VALUE, asset.Val_rep, "borrower asset - Val_rep");
			Assert.AreEqual(B_A_ACCOUNT_HOLDER_NAME, asset.AccNm, "borrower asset - AccNm");
			Assert.AreEqual(B_A_ACCOUNT_NUMBER, asset.AccNum.Value, "borrower asset - AccNum");
			Assert.AreEqual(B_A_VERIF_SENT, asset.VerifSentD_rep, "borrower asset - VerifSentD_rep");
			Assert.AreEqual(B_A_REORDER, asset.VerifReorderedD_rep, "borrower asset - VerifReorderedD_rep");
			Assert.AreEqual(B_A_RECEIVED, asset.VerifRecvD_rep, "borrower asset - VerifRecvD_rep");
			Assert.AreEqual(B_A_EXPECTED, asset.VerifExpD_rep, "borrower asset - VerifExpD_rep");
		}

		private void ConfirmLiabilitiesCoborrowerDeleted(CAppData dataApp)
		{
			ILiaCollection liabilityCollection = dataApp.aLiaCollection;
			ILiabilityRegular record1 = liabilityCollection.GetRegRecordOf(m_borrowerLiabilityId);
			ILiabilityRegular record2 = liabilityCollection.GetRegRecordOf(m_jointLiabilityId);
			
			ConfirmBorrowerLiabilityUnchanged(record1);

			bool coborrowerLiabilityDeleted = ! liabilityCollection.HasRecordOf(m_coborrowerLiabilityId);
			Assert.AreEqual(coborrowerLiabilityDeleted, true, "coborrower liability");

			ConfirmJointLiabilityUnchanged(Operation.DeleteCoborrower, record2, dataApp.LoanData.sBorrowerApplicationCollectionT);
		}

		private void ConfirmBorrowerLiabilityUnchanged(ILiabilityRegular record)
		{
			Assert.AreEqual(E_LiaOwnerT.Borrower, record.OwnerT, "borrower liability - OwnerT");
			Assert.AreEqual(B_L_DEBT_TYPE, record.DebtT, "borrower liability - DebtT");
			Assert.AreEqual(B_L_COMPANY, record.ComNm, "borrower liability - ComNm");
			Assert.AreEqual(B_L_ADDRESS, record.ComAddr, "borrower liability - ComAddr");
			Assert.AreEqual(B_L_CITY, record.ComCity, "borrower liability - ComCity");
			Assert.AreEqual(B_L_STATE, record.ComState, "borrower liability - ComState");
			Assert.AreEqual(B_L_ZIP, record.ComZip, "borrower liability - ComZip");
			Assert.AreEqual(B_L_PHONE, record.ComPhone, "borrower liability - ComPhone");
			Assert.AreEqual(B_L_FAX, record.ComFax, "borrower liability - ComFax");
			Assert.AreEqual(B_L_DESC, record.Desc, "borrower liability - Desc");
			Assert.AreEqual(B_L_ACCOUNT_HOLDER_NAME, record.AccNm, "borrower liability - AccNm");
			Assert.AreEqual(B_L_ACCOUNT_NUMBER, record.AccNum.Value, "borrower liability - AccNum");
			Assert.AreEqual(B_L_BALANCE, record.Bal_rep, "borrower liability - Bal_rep");
			Assert.AreEqual(B_L_PMT, record.Pmt_rep, "borrower liability - Pmt_rep");
			Assert.AreEqual(B_L_MONTHS_LEFT, record.RemainMons_rep, "borrower liability - RemainMons_rep");
			Assert.AreEqual(B_L_RATE, record.R_rep, "borrower liability - R_rep");
			Assert.AreEqual(B_L_TERM, record.OrigTerm_rep, "borrower liability - OrigTerm_rep");
			Assert.AreEqual(B_L_DUE_IN, record.Due_rep, "borrower liability - Due_rep");
			Assert.AreEqual(B_L_LATE_30, record.Late30_rep, "borrower liability - Late30_rep");
			Assert.AreEqual(B_L_LATE_60, record.Late60_rep, "borrower liability - Late60_rep");
			Assert.AreEqual(B_L_LATE_90, record.Late90Plus_rep, "borrower liability - Late90Plus_rep");
			Assert.AreEqual(B_L_WILL_BE_PAID_OFF, record.WillBePdOff, "borrower liability - WillBePdOff");
			Assert.AreEqual(!B_L_DEBT_SHOULD_BE_INCLUDED_IN_RATIOS, record.NotUsedInRatio, "borrower liability - NotUsedInRatio");
			Assert.AreEqual(B_L_DEBT_IS_NEW_SUBJECT_TO_PIGGYBACK, record.IsPiggyBack, "borrower liability - IsPiggyBack");
			Assert.AreEqual(B_L_INCLUDE_IN_REPOSSESSION, record.IncInReposession, "borrower liability - IncInReposession");
			Assert.AreEqual(B_L_INCLUDE_IN_BANKRUPTCY, record.IncInBankruptcy, "borrower liability - IncInBankruptcy");
			Assert.AreEqual(B_L_INCLUDE_IN_FORECLOSURE, record.IncInForeclosure, "borrower liability - IncInForeclosure");
			Assert.AreEqual(B_L_EXCLUDE_FROM_UNDERWRITING, record.ExcFromUnderwriting, "borrower liability - ExcFromUnderwriting");
			Assert.AreEqual(B_L_VERIF_SENT, record.VerifSentD_rep, "borrower liability - VerifSentD_rep");
			Assert.AreEqual(B_L_REORDER, record.VerifReorderedD_rep, "borrower liability - VerifReorderedD_rep");
			Assert.AreEqual(B_L_RECEIVED, record.VerifRecvD_rep, "borrower liability - VerifRecvD_rep");
			Assert.AreEqual(B_L_EXPECTED, record.VerifExpD_rep, "borrower liability - VerifExpD_rep");
		}

		private void ConfirmOtherMonthlyIncomeCoborrowerDeleted(CAppData dataApp)
		{
            Assert.AreEqual(3, dataApp.aOtherIncomeList.Count);

            Assert.AreEqual(OTHER_INCOME_1_FOR_COBORROWER, dataApp.aOtherIncomeList[0].IsForCoBorrower, "aOtherIncomeList[0].IsForCoBorrower");
            Assert.AreEqual(OTHER_INCOME_1_DESC, dataApp.aOtherIncomeList[0].Desc, "aOtherIncomeList[0].Desc");
            Assert.AreEqual(OTHER_INCOME_1_AMOUNT, dataApp.aOtherIncomeList[0].Amount, "aOtherIncomeList[0].Amount");

            Assert.AreEqual(OTHER_INCOME_2_FOR_COBORROWER, dataApp.aOtherIncomeList[1].IsForCoBorrower, "aOtherIncomeList[1].IsForCoBorrower");
            Assert.AreEqual(OTHER_INCOME_2_DESC, dataApp.aOtherIncomeList[1].Desc, "aOtherIncomeList[1].Desc");
            Assert.AreEqual(OTHER_INCOME_2_AMOUNT, dataApp.aOtherIncomeList[1].Amount, "aOtherIncomeList[1].Amount");

            Assert.AreEqual(false, dataApp.aOtherIncomeList[2].IsForCoBorrower, "aOtherIncomeList[2].IsForCoBorrower");
            Assert.AreEqual("", dataApp.aOtherIncomeList[2].Desc, "aOtherIncomeList[2].Desc");
            Assert.AreEqual(0.00M, dataApp.aOtherIncomeList[2].Amount, "aOtherIncomeList[2].Amount");
        }

        private void ConfirmCoborrowerMonthlyIncomeDeleted(CAppData dataApp)
		{
			Assert.AreEqual("$0.00", dataApp.aCBaseI_rep, "coborrower monthly income - aCBaseI_rep");
			Assert.AreEqual("$0.00", dataApp.aCOvertimeI_rep, "coborrower monthly income - aCOvertimeI_rep");
			Assert.AreEqual("$0.00", dataApp.aCBonusesI_rep, "coborrower monthly income - aCBonusesI_rep");
			Assert.AreEqual("$0.00", dataApp.aCCommisionI_rep, "coborrower monthly income - aCCommisionI_rep");
			Assert.AreEqual("$0.00", dataApp.aCDividendI_rep, "coborrower monthly income - aCDividendI_rep");
			Assert.AreEqual("$0.00", dataApp.aCNetRentI1003_rep, "coborrower monthly income - aCNetRentI1003_rep");
		}

		private void ConfirmBorrowerMonthlyIncomeUnchanged(CAppData dataApp)
		{
			Assert.AreEqual(B_BASE_INCOME, dataApp.aBBaseI_rep, "borrower monthly income - aBBaseI_rep");
			Assert.AreEqual(B_OVERTIME, dataApp.aBOvertimeI_rep, "borrower monthly income - aBOvertimeI_rep");
			Assert.AreEqual(B_BONUSES, dataApp.aBBonusesI_rep, "borrower monthly income - aBBonusesI_rep");
			Assert.AreEqual(B_COMMISSION, dataApp.aBCommisionI_rep, "borrower monthly income - aBCommisionI_rep");
			Assert.AreEqual(B_DIVIDENDS, dataApp.aBDividendI_rep, "borrower monthly income - aBDividendI_rep");
			Assert.AreEqual(B_NET_RENT, dataApp.aBNetRentI1003_rep, "borrower monthly income - aBNetRentI1003_rep");
		}

		private void ConfirmCoborrowerEmploymentDeleted(CAppData dataApp)
		{
			IEmpCollection employmentCollection = dataApp.aCEmpCollection;
            IPrimaryEmploymentRecord primaryEmp = employmentCollection.GetPrimaryEmp(false);
			Assert.AreEqual(null, primaryEmp, "coborrower employmentCollection.GetPrimaryEmp");
			Assert.AreEqual(0, employmentCollection.CountRegular, "coborrower employmentCollection.CountRegular");
			Assert.AreEqual(0, employmentCollection.CountSpecial, "coborrower employmentCollection.CountSpecial");
		}

		private void ConfirmBorrowerEmploymentUnchanged(CAppData dataApp, bool skipEmployerBusinessPhoneAndMonthlyIncomeChecks)
		{
			IEmpCollection employmentCollection = dataApp.aBEmpCollection;
            IPrimaryEmploymentRecord primaryEmp = employmentCollection.GetPrimaryEmp(false);
			Assert.AreEqual(B_SELF_EMPLOYED, primaryEmp.IsSelfEmplmt, "borrower primary employment - IsSelfEmplmt");
			Assert.AreEqual(B_E_EMPLOYER, primaryEmp.EmplrNm, "borrower primary employment - EmplrNm");
			Assert.AreEqual(B_E_ADDRESS, primaryEmp.EmplrAddr, "borrower primary employment - EmplrAddr");
			Assert.AreEqual(B_E_CITY, primaryEmp.EmplrCity, "borrower primary employment - EmplrCity");
			Assert.AreEqual(B_E_STATE, primaryEmp.EmplrState, "borrower primary employment - EmplrState");
			Assert.AreEqual(B_E_ZIP, primaryEmp.EmplrZip, "borrower primary employment - EmplrZip");
			Assert.AreEqual(B_E_POSITION, primaryEmp.JobTitle, "borrower primary employment - JobTitle");
			Assert.AreEqual(B_E_YEARS_ON_JOB, primaryEmp.EmplmtLen_rep, "borrower primary employment - EmplmtLen_rep");
			Assert.AreEqual(B_E_YEARS_IN_PROFESSION, primaryEmp.ProfLen_rep, "borrower primary employment - ProfLen_rep");
			Assert.AreEqual(B_E_PHONE_LOCKED, dataApp.aBEmplrBusPhoneLckd, "borrower primary employment - aBEmplrBusPhoneLckd");
			
            if (!skipEmployerBusinessPhoneAndMonthlyIncomeChecks)
            {
                Assert.AreEqual(m_bEmplrBusPhone, primaryEmp.EmplrBusPhone, "borrower primary employment - EmplrBusPhone");
            }

			Assert.AreEqual(B_E_FAX, primaryEmp.EmplrFax, "borrower primary employment - EmplrFax");
			Assert.AreEqual(B_E_VERIF_SENT, primaryEmp.VerifSentD_rep, "borrower primary employment - VerifSentD_rep");
			Assert.AreEqual(B_E_REORDER, primaryEmp.VerifReorderedD_rep, "borrower primary employment - VerifReorderedD_rep");
			Assert.AreEqual(B_E_RECEIVED, primaryEmp.VerifRecvD_rep, "borrower primary employment - VerifRecvD_rep");
			Assert.AreEqual(B_E_EXPECTED, primaryEmp.VerifExpD_rep, "borrower primary employment - VerifExpD_rep");

            IRegularEmploymentRecord emp = employmentCollection.GetRegRecordOf(m_borrowerPreviousEmployerRecord);
			Assert.AreEqual(B_P_SELF_EMPLOYED, emp.IsSelfEmplmt, "borrower previous employment - IsSelfEmplmt");
			Assert.AreEqual(B_P_EMPLOYER, emp.EmplrNm, "borrower previous employment - EmplrNm");
			Assert.AreEqual(B_P_ADDRESS, emp.EmplrAddr, "borrower previous employment - EmplrAddr");
			Assert.AreEqual(B_P_CITY, emp.EmplrCity, "borrower previous employment - EmplrCity");
			Assert.AreEqual(B_P_STATE, emp.EmplrState, "borrower previous employment - EmplrState");
			Assert.AreEqual(B_P_ZIP, emp.EmplrZip, "borrower previous employment - EmplrZip");
			Assert.AreEqual(B_P_POSITION, emp.JobTitle, "borrower previous employment - JobTitle");
			Assert.AreEqual(B_P_DATE_FROM, emp.EmplmtStartD_rep, "borrower previous employment - EmplmtStartD_rep");
			Assert.AreEqual(B_P_DATE_TO, emp.EmplmtEndD_rep, "borrower previous employment - EmplmtEndD_rep");

            if (!skipEmployerBusinessPhoneAndMonthlyIncomeChecks)
            {
                Assert.AreEqual(B_P_MONTHLY_INCOME, emp.MonI_rep, "borrower previous employment - MonI_rep");
                Assert.AreEqual(B_P_PHONE, emp.EmplrBusPhone, "borrower previous employment - EmplrBusPhone");
            }

			Assert.AreEqual(B_P_FAX, emp.EmplrFax, "borrower previous employment - EmplrFax");
			Assert.AreEqual(B_P_VERIF_SENT, emp.VerifSentD_rep, "borrower previous employment - VerifSentD_rep");
			Assert.AreEqual(B_P_REORDER, emp.VerifReorderedD_rep, "borrower previous employment - VerifReorderedD_rep");
			Assert.AreEqual(B_P_RECEIVED, emp.VerifRecvD_rep, "borrower previous employment - VerifRecvD_rep");
			Assert.AreEqual(B_P_EXPECTED, emp.VerifExpD_rep, "borrower previous employment - VerifExpD_rep");
		}

		private void ConfirmCoborrowerInfoDeleted(CAppData dataApp)
		{
			Assert.AreEqual("", dataApp.aCFirstNm, "coborrower's first name");
			Assert.AreEqual("", dataApp.aCMidNm, "coborrower's middle name");
			Assert.AreEqual("", dataApp.aCLastNm, "coborrower's last name");
			Assert.AreEqual("", dataApp.aCSuffix, "coborrower's suffix");
			Assert.AreEqual("", dataApp.aCSsn, "coborrower's SSN");
			Assert.AreEqual("", dataApp.aCDob_rep, "coborrower's DOB");
			Assert.AreEqual("0", dataApp.aCSchoolYrs_rep, "coborrower's years of school");
			Assert.AreEqual(E_aCMaritalStatT.LeaveBlank.ToString(), dataApp.aCMaritalStatT.ToString(), "coborrower's marital status");
			Assert.AreEqual("0", dataApp.aCDependNum_rep, "coborrower's number of dependents");
			Assert.AreEqual("", dataApp.aCDependAges, "coborrower's dependents' ages");
			Assert.AreEqual("", dataApp.aCHPhone, "coborrower's home phone");
			Assert.AreEqual("", dataApp.aCCellPhone, "coborrower's cell phone");
			Assert.AreEqual("", dataApp.aCBusPhone, "coborrower's work phone");
			Assert.AreEqual("", dataApp.aCFax, "coborrower's fax");
			Assert.AreEqual("", dataApp.aCEmail, "coborrower's email");
			Assert.AreEqual("", dataApp.aCAddr, "coborrower's address");
			Assert.AreEqual("", dataApp.aCCity, "coborrower's city");
			Assert.AreEqual("", dataApp.aCState, "coborrower's state");
			Assert.AreEqual("", dataApp.aCZip, "coborrower's zip code");
			Assert.AreEqual(E_aCAddrT.LeaveBlank.ToString(), dataApp.aCAddrT.ToString(), "coborrower's address type");
			Assert.AreEqual("", dataApp.aCAddrYrs, "coborrower's years at address");
            Assert.AreEqual(E_aAddrMailSourceT.PresentAddress, dataApp.aCAddrMailSourceT, "coborrower's mail address is his/her present address");
			Assert.AreEqual("", dataApp.aCAddrMail, "coborrower's mail address");
			Assert.AreEqual("", dataApp.aCCityMail, "coborrower's mail city");
			Assert.AreEqual("", dataApp.aCStateMail, "coborrower's mail state");
			Assert.AreEqual("", dataApp.aCZipMail, "coborrower's mail zip");
            Assert.AreEqual("", dataApp.aCAddrPost, "coborrower's post-closing address");
            Assert.AreEqual("", dataApp.aCCityPost, "coborrower's post-closing city");
            Assert.AreEqual("", dataApp.aCStatePost, "coborrower's post-closing state");
            Assert.AreEqual("", dataApp.aCZipPost, "coborrower's post-closing zip");
            Assert.AreEqual("", dataApp.aCPrev1Addr, "coborrower's first previous address");
			Assert.AreEqual("", dataApp.aCPrev1City, "coborrower's first previous city");
			Assert.AreEqual("", dataApp.aCPrev1State, "coborrower's first previous state");
			Assert.AreEqual("", dataApp.aCPrev1Zip, "coborrower's first previous zip code");
			Assert.AreEqual(E_aCPrev1AddrT.LeaveBlank.ToString(), dataApp.aCPrev1AddrT.ToString(), "coborrower's first previous address type");
			Assert.AreEqual("", dataApp.aCPrev1AddrYrs, "coborrowers years at first previous address");
			Assert.AreEqual("", dataApp.aCPrev2Addr, "coborrower's second previous address");
			Assert.AreEqual("", dataApp.aCPrev2City, "coborrower's second previous city");
			Assert.AreEqual("", dataApp.aCPrev2State, "coborrower's second previous state");
			Assert.AreEqual("", dataApp.aCPrev2Zip, "coborrower's second previous zip code");
			Assert.AreEqual(E_aCPrev2AddrT.LeaveBlank.ToString(), dataApp.aCPrev2AddrT.ToString(), "coborrower's second previous address type");
			Assert.AreEqual("", dataApp.aCPrev2AddrYrs, "coborrower's years at second previous address");
		}

		private void ConfirmBorrowerInfoUnchanged(CAppData dataApp)
		{
			Assert.AreEqual(B_FIRST_NAME, dataApp.aBFirstNm, "borrower's first name");
			Assert.AreEqual(B_MIDDLE_NAME, dataApp.aBMidNm, "borrower's middle name");
			Assert.AreEqual(B_LAST_NAME, dataApp.aBLastNm, "borrower's last name");
			Assert.AreEqual(B_SUFFIX, dataApp.aBSuffix, "borrower's suffix");
			Assert.AreEqual(B_SSN, dataApp.aBSsn, "borrower's SSN");
			Assert.AreEqual(B_DOB, dataApp.aBDob_rep, "borrower's DOB");
			Assert.AreEqual(B_YEARS_OF_SCHOOL, dataApp.aBSchoolYrs_rep, "borrower's years of school");
			Assert.AreEqual(B_MARITAL_STATUS.ToString(), dataApp.aBMaritalStatT.ToString(), "borrower's marital status");
			Assert.AreEqual(B_NUMBER_OF_DEPENDENTS, dataApp.aBDependNum_rep, "borrower's number of dependents");
			Assert.AreEqual(B_DEPENDENTS_AGES, dataApp.aBDependAges, "borrower's dependents' ages");
			Assert.AreEqual(B_HOME_PHONE, dataApp.aBHPhone, "borrower's home phone");
			Assert.AreEqual(B_CELL_PHONE, dataApp.aBCellPhone, "borrower's cell phone");
			Assert.AreEqual(B_WORK_PHONE, dataApp.aBBusPhone, "borrower's work phone");
			Assert.AreEqual(B_FAX, dataApp.aBFax, "borrower's fax");
			Assert.AreEqual(B_EMAIL, dataApp.aBEmail, "borrower's email");
			Assert.AreEqual(B_ADDRESS, dataApp.aBAddr, "borrower's address");
			Assert.AreEqual(B_CITY, dataApp.aBCity, "borrower's city");
			Assert.AreEqual(B_STATE, dataApp.aBState, "borrower's state");
			Assert.AreEqual(B_ZIP, dataApp.aBZip, "borrower's zip code");
			Assert.AreEqual(B_ADDRESS_TYPE.ToString(), dataApp.aBAddrT.ToString(), "borrower's address type");
			Assert.AreEqual(B_YEARS_AT_ADDRESS, dataApp.aBAddrYrs, "borrower's years at address");

            Assert.AreEqual(B_MAIL_ADDRESS_SOURCE, dataApp.aBAddrMailSourceT, "borrower's mail address is his/her present address");
            Assert.AreEqual(B_ADDRESS, dataApp.aBAddrMail, "borrower's mail address");
            Assert.AreEqual(B_CITY, dataApp.aBCityMail, "borrower's mail city");
            Assert.AreEqual(B_STATE, dataApp.aBStateMail, "borrower's mail state");
            Assert.AreEqual(B_ZIP, dataApp.aBZipMail, "borrower's mail zip");

            Assert.AreEqual(B_POST_ADDRESS_SOURCE_LOCKED, dataApp.aBAddrPostSourceTLckd, "borrower's post-closing address source is not locked");
            Assert.AreEqual(B_POST_ADDRESS_SOURCE, dataApp.aBAddrPostSourceT, "borrower's post-closing address is his/her mailing address");
            Assert.AreEqual(B_ADDRESS, dataApp.aBAddrMail, "borrower's post-closing address");
            Assert.AreEqual(B_CITY, dataApp.aBCityMail, "borrower's post-closing city");
            Assert.AreEqual(B_STATE, dataApp.aBStateMail, "borrower's post-closing state");
            Assert.AreEqual(B_ZIP, dataApp.aBZipMail, "borrower's post-closing zip");

            // 2019-01 tj - Per conversation with Doug, the order of these via the shim doesn't matter, but they have to be consistent
            if (dataApp.LoanData.sIsHousingHistoryEntriesCollectionEnabled && dataApp.aBPrev1Addr == B_PREV2_ADDRESS)
            {
                Assert.AreEqual(B_PREV2_ADDRESS, dataApp.aBPrev1Addr, "borrower's first previous address");
                Assert.AreEqual(B_PREV2_CITY, dataApp.aBPrev1City, "borrower's first previous city");
                Assert.AreEqual(B_PREV2_STATE, dataApp.aBPrev1State, "borrower's first previous state");
                Assert.AreEqual(B_PREV2_ZIP, dataApp.aBPrev1Zip, "borrower's first previous zip code");
                Assert.AreEqual(B_PREV2_ADDRESS_TYPE.ToString(), dataApp.aBPrev1AddrT.ToString(), "borrower's first previous address type");
                Assert.AreEqual(B_PREV2_YEARS_AT_ADDRESS, dataApp.aBPrev1AddrYrs, "borrowers years at first previous address");
                Assert.AreEqual(B_PREV1_ADDRESS, dataApp.aBPrev2Addr, "borrower's second previous address");
                Assert.AreEqual(B_PREV1_CITY, dataApp.aBPrev2City, "borrower's second previous city");
                Assert.AreEqual(B_PREV1_STATE, dataApp.aBPrev2State, "borrower's second previous state");
                Assert.AreEqual(B_PREV1_ZIP, dataApp.aBPrev2Zip, "borrower's second previous zip code");
                Assert.AreEqual(B_PREV1_ADDRESS_TYPE.ToString(), dataApp.aBPrev2AddrT.ToString(), "borrower's second previous address type");
                Assert.AreEqual(B_PREV1_YEARS_AT_ADDRESS, dataApp.aBPrev2AddrYrs, "borrower's years at second previous address");
            }
            else
            {
                Assert.AreEqual(B_PREV1_ADDRESS, dataApp.aBPrev1Addr, "borrower's first previous address");
                Assert.AreEqual(B_PREV1_CITY, dataApp.aBPrev1City, "borrower's first previous city");
                Assert.AreEqual(B_PREV1_STATE, dataApp.aBPrev1State, "borrower's first previous state");
                Assert.AreEqual(B_PREV1_ZIP, dataApp.aBPrev1Zip, "borrower's first previous zip code");
                Assert.AreEqual(B_PREV1_ADDRESS_TYPE.ToString(), dataApp.aBPrev1AddrT.ToString(), "borrower's first previous address type");
                Assert.AreEqual(B_PREV1_YEARS_AT_ADDRESS, dataApp.aBPrev1AddrYrs, "borrowers years at first previous address");
                Assert.AreEqual(B_PREV2_ADDRESS, dataApp.aBPrev2Addr, "borrower's second previous address");
                Assert.AreEqual(B_PREV2_CITY, dataApp.aBPrev2City, "borrower's second previous city");
                Assert.AreEqual(B_PREV2_STATE, dataApp.aBPrev2State, "borrower's second previous state");
                Assert.AreEqual(B_PREV2_ZIP, dataApp.aBPrev2Zip, "borrower's second previous zip code");
                Assert.AreEqual(B_PREV2_ADDRESS_TYPE.ToString(), dataApp.aBPrev2AddrT.ToString(), "borrower's second previous address type");
                Assert.AreEqual(B_PREV2_YEARS_AT_ADDRESS, dataApp.aBPrev2AddrYrs, "borrower's years at second previous address");
            }
        }

        [TestCase(E_sLqbCollectionT.Legacy)]
        [TestCase(E_sLqbCollectionT.UseLqbCollections)]
        public void SwapBorrowerAndCoborrowerTest(E_sLqbCollectionT borrowerApplicationCollectionType)
        {
            this.collectionType = borrowerApplicationCollectionType;
            if (ConstStage.DisableBorrowerApplicationCollectionTUpdates
                && borrowerApplicationCollectionType != E_sLqbCollectionT.Legacy)
            {
                Assert.Ignore();
            }

            m_dataLoan = CreatePageData();
			PopulateBorrowerAndCoborrower();

            m_dataLoan = CreatePageData();
            m_dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
			CAppData dataApp = m_dataLoan.GetAppData(0);
			dataApp.SwapMarriedBorAndCobor();
			m_dataLoan.Save();

            m_dataLoan = CreatePageData();

            m_dataLoan.InitLoad();
			dataApp = m_dataLoan.GetAppData(0);

            //// Temporary for case 475067 until the code changes match the DB schema changes.
            var skipEmployerBusinessPhoneAndMonthlyIncomeChecks = borrowerApplicationCollectionType == E_sLqbCollectionT.UseLqbCollections;

            ConfirmBorrowerInfoWasCoborrowerInfo(dataApp);
			ConfirmCoborrowerInfoWasBorrowerInfo(dataApp);
			ConfirmBorrowerEmploymentWasCoborrowerEmployment(dataApp, skipEmployerBusinessPhoneAndMonthlyIncomeChecks);
			ConfirmCoborrowerEmploymentWasBorrowerEmployment(dataApp, skipEmployerBusinessPhoneAndMonthlyIncomeChecks);
			ConfirmBorrowerMonthlyIncomeWasCoborrowerMonthlyIncome(dataApp);
			ConfirmCoborrowerMonthlyIncomeWasBorrowerMonthlyIncome(dataApp);
			ConfirmOtherMonthlyIncomeSwapped(dataApp);
			ConfirmLiabilitiesSwapped(dataApp);
			ConfirmAssetsSwapped(dataApp);
		}

        [Test]
        public void SwapBorrowerAndCoborrower_Always_SwapsTheirConsumerIds()
        {
            var loan = this.CreatePageData();
            loan.InitLoad();
            var app = loan.GetAppData(0);
            var borrowerId = app.aBConsumerId;
            var coborrowerId = app.aCConsumerId;

            app.SwapMarriedBorAndCobor();

            Assert.AreEqual(coborrowerId, app.aBConsumerId);
            Assert.AreEqual(borrowerId, app.aCConsumerId);
        }

        [Test]
        public void DeleteSpouse_Always_GeneratesNewConsumerId()
        {
            var loan = this.CreatePageData();
            loan.InitLoad();
            var app = loan.GetAppData(0);
            var borrowerId = app.aBConsumerId;
            var coborrowerId = app.aCConsumerId;

            app.DelMarriedCobor();

            Assert.AreEqual(borrowerId, app.aBConsumerId);
            Assert.AreNotEqual(coborrowerId, app.aCConsumerId);
            // Just as a sanity check.
            Assert.AreNotEqual(borrowerId, app.aCConsumerId);
        }

		private void ConfirmBorrowerInfoWasCoborrowerInfo(CAppData dataApp)
		{
			Assert.AreEqual(C_FIRST_NAME, dataApp.aBFirstNm, "borrower's first name");
			Assert.AreEqual(C_MIDDLE_NAME, dataApp.aBMidNm, "borrower's middle name");
			Assert.AreEqual(C_LAST_NAME, dataApp.aBLastNm, "borrower's last name");
			Assert.AreEqual(C_SUFFIX, dataApp.aBSuffix, "borrower's suffix");
			Assert.AreEqual(C_SSN, dataApp.aBSsn, "borrower's SSN");
			Assert.AreEqual(C_DOB, dataApp.aBDob_rep, "borrower's DOB");
			Assert.AreEqual(C_YEARS_OF_SCHOOL, dataApp.aBSchoolYrs_rep, "borrower's years of school");
			Assert.AreEqual(C_MARITAL_STATUS.ToString(), dataApp.aBMaritalStatT.ToString(), "borrower's marital status");
			Assert.AreEqual(C_NUMBER_OF_DEPENDENTS, dataApp.aBDependNum_rep, "borrower's number of dependents");
			Assert.AreEqual(C_DEPENDENTS_AGES, dataApp.aBDependAges, "borrower's dependents' ages");
			Assert.AreEqual(C_HOME_PHONE, dataApp.aBHPhone, "borrower's home phone");
			Assert.AreEqual(C_CELL_PHONE, dataApp.aBCellPhone, "borrower's cell phone");
			Assert.AreEqual(C_WORK_PHONE, dataApp.aBBusPhone, "borrower's work phone");
			Assert.AreEqual(C_FAX, dataApp.aBFax, "borrower's fax");
			Assert.AreEqual(C_EMAIL, dataApp.aBEmail, "borrower's email");
			Assert.AreEqual(C_ADDRESS, dataApp.aBAddr, "borrower's address");
			Assert.AreEqual(C_CITY, dataApp.aBCity, "borrower's city");
			Assert.AreEqual(C_STATE, dataApp.aBState, "borrower's state");
			Assert.AreEqual(C_ZIP, dataApp.aBZip, "borrower's zip code");
			Assert.AreEqual(C_ADDRESS_TYPE.ToString(), dataApp.aBAddrT.ToString(), "borrower's address type");
			Assert.AreEqual(C_YEARS_AT_ADDRESS, dataApp.aBAddrYrs, "borrower's years at address");

            Assert.AreEqual(C_MAIL_ADDRESS_SOURCE, dataApp.aBAddrMailSourceT, "borrower's mail address is manually entered");
            Assert.AreEqual(C_MAIL_ADDRESS, dataApp.aBAddrMail, "borrower's mail address");
            Assert.AreEqual(C_MAIL_CITY, dataApp.aBCityMail, "borrower's mail city");
            Assert.AreEqual(C_MAIL_STATE, dataApp.aBStateMail, "borrower's mail state");
            Assert.AreEqual(C_MAIL_ZIP, dataApp.aBZipMail, "borrower's mail zip");

            Assert.AreEqual(C_POST_ADDRESS_SOURCE_LOCKED, dataApp.aBAddrPostSourceTLckd, "borrower's post-closing address source is locked");
            Assert.AreEqual(C_POST_ADDRESS_SOURCE, dataApp.aBAddrPostSourceT, "borrower's post-closing address is manually entered");
            Assert.AreEqual(C_POST_ADDRESS, dataApp.aBAddrPost, "borrower's post-closing address");
            Assert.AreEqual(C_POST_CITY, dataApp.aBCityPost, "borrower's post-closing city");
            Assert.AreEqual(C_POST_STATE, dataApp.aBStatePost, "borrower's post-closing state");
            Assert.AreEqual(C_POST_ZIP, dataApp.aBZipPost, "borrower's post-closing zip");

            // 2019-01 tj - Per conversation with Doug, the order of these via the shim doesn't matter, but they have to be consistent
            if (dataApp.LoanData.sIsHousingHistoryEntriesCollectionEnabled && dataApp.aBPrev1Addr == C_PREV2_ADDRESS)
            {
                Assert.AreEqual(C_PREV2_ADDRESS, dataApp.aBPrev1Addr, "borrower's first previous address");
                Assert.AreEqual(C_PREV2_CITY, dataApp.aBPrev1City, "borrower's first previous city");
                Assert.AreEqual(C_PREV2_STATE, dataApp.aBPrev1State, "borrower's first previous state");
                Assert.AreEqual(C_PREV2_ZIP, dataApp.aBPrev1Zip, "borrower's first previous zip code");
                Assert.AreEqual(C_PREV2_ADDRESS_TYPE.ToString(), dataApp.aBPrev1AddrT.ToString(), "borrower's first previous address type");
                Assert.AreEqual(C_PREV2_YEARS_AT_ADDRESS, dataApp.aBPrev1AddrYrs, "borrowers years at first previous address");
                Assert.AreEqual(C_PREV1_ADDRESS, dataApp.aBPrev2Addr, "borrower's second previous address");
                Assert.AreEqual(C_PREV1_CITY, dataApp.aBPrev2City, "borrower's second previous city");
                Assert.AreEqual(C_PREV1_STATE, dataApp.aBPrev2State, "borrower's second previous state");
                Assert.AreEqual(C_PREV1_ZIP, dataApp.aBPrev2Zip, "borrower's second previous zip code");
                Assert.AreEqual(C_PREV1_ADDRESS_TYPE.ToString(), dataApp.aBPrev2AddrT.ToString(), "borrower's second previous address type");
                Assert.AreEqual(C_PREV1_YEARS_AT_ADDRESS, dataApp.aBPrev2AddrYrs, "borrower's years at second previous address");
            }
            else
            {
                Assert.AreEqual(C_PREV1_ADDRESS, dataApp.aBPrev1Addr, "borrower's first previous address");
                Assert.AreEqual(C_PREV1_CITY, dataApp.aBPrev1City, "borrower's first previous city");
                Assert.AreEqual(C_PREV1_STATE, dataApp.aBPrev1State, "borrower's first previous state");
                Assert.AreEqual(C_PREV1_ZIP, dataApp.aBPrev1Zip, "borrower's first previous zip code");
                Assert.AreEqual(C_PREV1_ADDRESS_TYPE.ToString(), dataApp.aBPrev1AddrT.ToString(), "borrower's first previous address type");
                Assert.AreEqual(C_PREV1_YEARS_AT_ADDRESS, dataApp.aBPrev1AddrYrs, "borrowers years at first previous address");
                Assert.AreEqual(C_PREV2_ADDRESS, dataApp.aBPrev2Addr, "borrower's second previous address");
                Assert.AreEqual(C_PREV2_CITY, dataApp.aBPrev2City, "borrower's second previous city");
                Assert.AreEqual(C_PREV2_STATE, dataApp.aBPrev2State, "borrower's second previous state");
                Assert.AreEqual(C_PREV2_ZIP, dataApp.aBPrev2Zip, "borrower's second previous zip code");
                Assert.AreEqual(C_PREV2_ADDRESS_TYPE.ToString(), dataApp.aBPrev2AddrT.ToString(), "borrower's second previous address type");
                Assert.AreEqual(C_PREV2_YEARS_AT_ADDRESS, dataApp.aBPrev2AddrYrs, "borrower's years at second previous address");
            }

            Assert.AreEqual(C_PREFERRED_NAME_LOCKED, dataApp.aBPreferredNmLckd, "borrower's preferred name locked");
        }

		private void ConfirmCoborrowerInfoWasBorrowerInfo(CAppData dataApp)
		{
			Assert.AreEqual(B_FIRST_NAME, dataApp.aCFirstNm, "coborrower's first name");
			Assert.AreEqual(B_MIDDLE_NAME, dataApp.aCMidNm, "coborrower's middle name");
			Assert.AreEqual(B_LAST_NAME, dataApp.aCLastNm, "coborrower's last name");
			Assert.AreEqual(B_SUFFIX, dataApp.aCSuffix, "coborrower's suffix");
			Assert.AreEqual(B_SSN, dataApp.aCSsn, "coborrower's SSN");
			Assert.AreEqual(B_DOB, dataApp.aCDob_rep, "coborrower's DOB");
			Assert.AreEqual(B_YEARS_OF_SCHOOL, dataApp.aCSchoolYrs_rep, "coborrower's years of school");
			Assert.AreEqual(B_MARITAL_STATUS.ToString(), dataApp.aCMaritalStatT.ToString(), "coborrower's marital status");
			Assert.AreEqual(B_NUMBER_OF_DEPENDENTS, dataApp.aCDependNum_rep, "coborrower's number of dependents");
			Assert.AreEqual(B_DEPENDENTS_AGES, dataApp.aCDependAges, "coborrower's dependents' ages");
			Assert.AreEqual(B_HOME_PHONE, dataApp.aCHPhone, "coborrower's home phone");
			Assert.AreEqual(B_CELL_PHONE, dataApp.aCCellPhone, "coborrower's cell phone");
			Assert.AreEqual(B_WORK_PHONE, dataApp.aCBusPhone, "coborrower's work phone");
			Assert.AreEqual(B_FAX, dataApp.aCFax, "coborrower's fax");
			Assert.AreEqual(B_EMAIL, dataApp.aCEmail, "coborrower's email");
			Assert.AreEqual(B_ADDRESS, dataApp.aCAddr, "coborrower's address");
			Assert.AreEqual(B_CITY, dataApp.aCCity, "coborrower's city");
			Assert.AreEqual(B_STATE, dataApp.aCState, "coborrower's state");
			Assert.AreEqual(B_ZIP, dataApp.aCZip, "coborrower's zip code");
			Assert.AreEqual(B_ADDRESS_TYPE.ToString(), dataApp.aCAddrT.ToString(), "coborrower's address type");
			Assert.AreEqual(B_YEARS_AT_ADDRESS, dataApp.aCAddrYrs, "coborrower's years at address");

            Assert.AreEqual(B_MAIL_ADDRESS_SOURCE, dataApp.aCAddrMailSourceT, "coborrower's mail address is his/her present address");
            Assert.AreEqual(B_ADDRESS, dataApp.aCAddrMail, "coborrower's mail address");
            Assert.AreEqual(B_CITY, dataApp.aCCityMail, "coborrower's mail city");
            Assert.AreEqual(B_STATE, dataApp.aCStateMail, "coborrower's mail state");
            Assert.AreEqual(B_ZIP, dataApp.aCZipMail, "coborrower's mail zip");

            Assert.AreEqual(B_POST_ADDRESS_SOURCE_LOCKED, dataApp.aCAddrPostSourceTLckd, "coborrower's post-closing address source is not locked");
            Assert.AreEqual(B_POST_ADDRESS_SOURCE, dataApp.aCAddrPostSourceT, "coborrower's post-closing address is his/her mailing address");
            Assert.AreEqual(B_ADDRESS, dataApp.aCAddrPost, "coborrower's post-closing address");
            Assert.AreEqual(B_CITY, dataApp.aCCityPost, "coborrower's post-closing city");
            Assert.AreEqual(B_STATE, dataApp.aCStatePost, "coborrower's post-closing state");
            Assert.AreEqual(B_ZIP, dataApp.aCZipPost, "coborrower's post-closing zip");

            // 2019-01 tj - Per conversation with Doug, the order of these via the shim doesn't matter, but they have to be consistent
            if (dataApp.LoanData.sIsHousingHistoryEntriesCollectionEnabled && dataApp.aCPrev1Addr == B_PREV2_ADDRESS)
            {
                Assert.AreEqual(B_PREV2_ADDRESS, dataApp.aCPrev1Addr, "coborrower's first previous address");
                Assert.AreEqual(B_PREV2_CITY, dataApp.aCPrev1City, "coborrower's first previous city");
                Assert.AreEqual(B_PREV2_STATE, dataApp.aCPrev1State, "coborrower's first previous state");
                Assert.AreEqual(B_PREV2_ZIP, dataApp.aCPrev1Zip, "coborrower's first previous zip code");
                Assert.AreEqual(B_PREV2_ADDRESS_TYPE.ToString(), dataApp.aCPrev1AddrT.ToString(), "coborrower's first previous address type");
                Assert.AreEqual(B_PREV2_YEARS_AT_ADDRESS, dataApp.aCPrev1AddrYrs, "borrowers years at first previous address");
                Assert.AreEqual(B_PREV1_ADDRESS, dataApp.aCPrev2Addr, "coborrower's second previous address");
                Assert.AreEqual(B_PREV1_CITY, dataApp.aCPrev2City, "coborrower's second previous city");
                Assert.AreEqual(B_PREV1_STATE, dataApp.aCPrev2State, "coborrower's second previous state");
                Assert.AreEqual(B_PREV1_ZIP, dataApp.aCPrev2Zip, "coborrower's second previous zip code");
                Assert.AreEqual(B_PREV1_ADDRESS_TYPE.ToString(), dataApp.aCPrev2AddrT.ToString(), "coborrower's second previous address type");
                Assert.AreEqual(B_PREV1_YEARS_AT_ADDRESS, dataApp.aCPrev2AddrYrs, "coborrower's years at second previous address");
            }
            else
            {
                Assert.AreEqual(B_PREV1_ADDRESS, dataApp.aCPrev1Addr, "coborrower's first previous address");
                Assert.AreEqual(B_PREV1_CITY, dataApp.aCPrev1City, "coborrower's first previous city");
                Assert.AreEqual(B_PREV1_STATE, dataApp.aCPrev1State, "coborrower's first previous state");
                Assert.AreEqual(B_PREV1_ZIP, dataApp.aCPrev1Zip, "coborrower's first previous zip code");
                Assert.AreEqual(B_PREV1_ADDRESS_TYPE.ToString(), dataApp.aCPrev1AddrT.ToString(), "coborrower's first previous address type");
                Assert.AreEqual(B_PREV1_YEARS_AT_ADDRESS, dataApp.aCPrev1AddrYrs, "borrowers years at first previous address");
                Assert.AreEqual(B_PREV2_ADDRESS, dataApp.aCPrev2Addr, "coborrower's second previous address");
                Assert.AreEqual(B_PREV2_CITY, dataApp.aCPrev2City, "coborrower's second previous city");
                Assert.AreEqual(B_PREV2_STATE, dataApp.aCPrev2State, "coborrower's second previous state");
                Assert.AreEqual(B_PREV2_ZIP, dataApp.aCPrev2Zip, "coborrower's second previous zip code");
                Assert.AreEqual(B_PREV2_ADDRESS_TYPE.ToString(), dataApp.aCPrev2AddrT.ToString(), "coborrower's second previous address type");
                Assert.AreEqual(B_PREV2_YEARS_AT_ADDRESS, dataApp.aCPrev2AddrYrs, "coborrower's years at second previous address");
            }

            Assert.AreEqual(B_PREFERRED_NAME_LOCKED, dataApp.aCPreferredNmLckd, "coborrower's preferred name locked");
            Assert.AreEqual(B_PREFERRED_NAME, dataApp.aCPreferredNm, "coborrower's preferred name");
        }

		private void ConfirmBorrowerEmploymentWasCoborrowerEmployment(CAppData dataApp, bool skipEmployerBusinessPhoneAndMonthlyIncomeChecks)
		{
			IEmpCollection employmentCollection = dataApp.aBEmpCollection;
            IPrimaryEmploymentRecord primaryEmp = employmentCollection.GetPrimaryEmp(false);
			Assert.AreEqual(C_SELF_EMPLOYED, primaryEmp.IsSelfEmplmt, "borrower primary employment - IsSelfEmplmt");
			Assert.AreEqual(C_E_EMPLOYER, primaryEmp.EmplrNm, "borrower primary employment - EmplrNm");
			Assert.AreEqual(C_E_ADDRESS, primaryEmp.EmplrAddr, "borrower primary employment - EmplrAddr");
			Assert.AreEqual(C_E_CITY, primaryEmp.EmplrCity, "borrower primary employment - EmplrCity");
			Assert.AreEqual(C_E_STATE, primaryEmp.EmplrState, "borrower primary employment - EmplrState");
			Assert.AreEqual(C_E_ZIP, primaryEmp.EmplrZip, "borrower primary employment - EmplrZip");
			Assert.AreEqual(C_E_POSITION, primaryEmp.JobTitle, "borrower primary employment - JobTitle");
			Assert.AreEqual(C_E_YEARS_ON_JOB, primaryEmp.EmplmtLen_rep, "borrower primary employment - EmplmtLen_rep");
			Assert.AreEqual(C_E_YEARS_IN_PROFESSION, primaryEmp.ProfLen_rep, "borrower primary employment - ProfLen_rep");
			Assert.AreEqual(C_E_PHONE_LOCKED, dataApp.aBEmplrBusPhoneLckd, "borrower primary employment - aBEmplrBusPhoneLckd");
			
            if (!skipEmployerBusinessPhoneAndMonthlyIncomeChecks)
            {
                Assert.AreEqual(m_cEmplrBusPhone, primaryEmp.EmplrBusPhone, "borrower primary employment - EmplrBusPhone");
            }

			Assert.AreEqual(C_E_FAX, primaryEmp.EmplrFax, "borrower primary employment - EmplrFax");
			Assert.AreEqual(C_E_VERIF_SENT, primaryEmp.VerifSentD_rep, "borrower primary employment - VerifSentD_rep");
			Assert.AreEqual(C_E_REORDER, primaryEmp.VerifReorderedD_rep, "borrower primary employment - VerifReorderedD_rep");
			Assert.AreEqual(C_E_RECEIVED, primaryEmp.VerifRecvD_rep, "borrower primary employment - VerifRecvD_rep");
			Assert.AreEqual(C_E_EXPECTED, primaryEmp.VerifExpD_rep, "borrower primary employment - VerifExpD_rep");

            IRegularEmploymentRecord emp = employmentCollection.GetRegRecordOf(m_coborrowerPreviousEmployerRecord);
			Assert.AreEqual(C_P_SELF_EMPLOYED, emp.IsSelfEmplmt, "borrower previous employment - IsSelfEmplmt");
			Assert.AreEqual(C_P_EMPLOYER, emp.EmplrNm, "borrower previous employment - EmplrNm");
			Assert.AreEqual(C_P_ADDRESS, emp.EmplrAddr, "borrower previous employment - EmplrAddr");
			Assert.AreEqual(C_P_CITY, emp.EmplrCity, "borrower previous employment - EmplrCity");
			Assert.AreEqual(C_P_STATE, emp.EmplrState, "borrower previous employment - EmplrState");
			Assert.AreEqual(C_P_ZIP, emp.EmplrZip, "borrower previous employment - EmplrZip");
			Assert.AreEqual(C_P_POSITION, emp.JobTitle, "borrower previous employment - JobTitle");
			Assert.AreEqual(C_P_DATE_FROM, emp.EmplmtStartD_rep, "borrower previous employment - EmplmtStartD_rep");
			Assert.AreEqual(C_P_DATE_TO, emp.EmplmtEndD_rep, "borrower previous employment - EmplmtEndD_rep");

            if (!skipEmployerBusinessPhoneAndMonthlyIncomeChecks)
            {
                Assert.AreEqual(C_P_MONTHLY_INCOME, emp.MonI_rep, "borrower previous employment - MonI_rep");
                Assert.AreEqual(C_P_PHONE, emp.EmplrBusPhone, "borrower previous employment - EmplrBusPhone");
            }

			Assert.AreEqual(C_P_FAX, emp.EmplrFax, "borrower previous employment - EmplrFax");
			Assert.AreEqual(C_P_VERIF_SENT, emp.VerifSentD_rep, "borrower previous employment - VerifSentD_rep");
			Assert.AreEqual(C_P_REORDER, emp.VerifReorderedD_rep, "borrower previous employment - VerifReorderedD_rep");
			Assert.AreEqual(C_P_RECEIVED, emp.VerifRecvD_rep, "borrower previous employment - VerifRecvD_rep");
			Assert.AreEqual(C_P_EXPECTED, emp.VerifExpD_rep, "borrower previous employment - VerifExpD_rep");
		}

		private void ConfirmCoborrowerEmploymentWasBorrowerEmployment(CAppData dataApp, bool skipEmployerBusinessPhoneAndMonthlyIncomeChecks)
		{
			IEmpCollection employmentCollection = dataApp.aCEmpCollection;
            IPrimaryEmploymentRecord primaryEmp = employmentCollection.GetPrimaryEmp(false);
			Assert.AreEqual(B_SELF_EMPLOYED, primaryEmp.IsSelfEmplmt, "coborrower primary employment - IsSelfEmplmt");
			Assert.AreEqual(B_E_EMPLOYER, primaryEmp.EmplrNm, "coborrower primary employment - EmplrNm");
			Assert.AreEqual(B_E_ADDRESS, primaryEmp.EmplrAddr, "coborrower primary employment - EmplrAddr");
			Assert.AreEqual(B_E_CITY, primaryEmp.EmplrCity, "coborrower primary employment - EmplrCity");
			Assert.AreEqual(B_E_STATE, primaryEmp.EmplrState, "coborrower primary employment - EmplrState");
			Assert.AreEqual(B_E_ZIP, primaryEmp.EmplrZip, "coborrower primary employment - EmplrZip");
			Assert.AreEqual(B_E_POSITION, primaryEmp.JobTitle, "coborrower primary employment - JobTitle");
			Assert.AreEqual(B_E_YEARS_ON_JOB, primaryEmp.EmplmtLen_rep, "coborrower primary employment - EmplmtLen_rep");
			Assert.AreEqual(B_E_YEARS_IN_PROFESSION, primaryEmp.ProfLen_rep, "coborrower primary employment - ProfLen_rep");
			Assert.AreEqual(B_E_PHONE_LOCKED, dataApp.aCEmplrBusPhoneLckd, "coborrower primary employment - aBEmplrBusPhoneLckd");
			
            if (!skipEmployerBusinessPhoneAndMonthlyIncomeChecks)
            {
                Assert.AreEqual(m_bEmplrBusPhone, primaryEmp.EmplrBusPhone, "coborrower primary employment - EmplrBusPhone");
            }

			Assert.AreEqual(B_E_FAX, primaryEmp.EmplrFax, "coborrower primary employment - EmplrFax");
			Assert.AreEqual(B_E_VERIF_SENT, primaryEmp.VerifSentD_rep, "coborrower primary employment - VerifSentD_rep");
			Assert.AreEqual(B_E_REORDER, primaryEmp.VerifReorderedD_rep, "coborrower primary employment - VerifReorderedD_rep");
			Assert.AreEqual(B_E_RECEIVED, primaryEmp.VerifRecvD_rep, "coborrower primary employment - VerifRecvD_rep");
			Assert.AreEqual(B_E_EXPECTED, primaryEmp.VerifExpD_rep, "coborrower primary employment - VerifExpD_rep");

            IRegularEmploymentRecord emp = employmentCollection.GetRegRecordOf(m_borrowerPreviousEmployerRecord);
			Assert.AreEqual(B_P_SELF_EMPLOYED, emp.IsSelfEmplmt, "coborrower previous employment - IsSelfEmplmt");
			Assert.AreEqual(B_P_EMPLOYER, emp.EmplrNm, "coborrower previous employment - EmplrNm");
			Assert.AreEqual(B_P_ADDRESS, emp.EmplrAddr, "coborrower previous employment - EmplrAddr");
			Assert.AreEqual(B_P_CITY, emp.EmplrCity, "coborrower previous employment - EmplrCity");
			Assert.AreEqual(B_P_STATE, emp.EmplrState, "coborrower previous employment - EmplrState");
			Assert.AreEqual(B_P_ZIP, emp.EmplrZip, "coborrower previous employment - EmplrZip");
			Assert.AreEqual(B_P_POSITION, emp.JobTitle, "coborrower previous employment - JobTitle");
			Assert.AreEqual(B_P_DATE_FROM, emp.EmplmtStartD_rep, "coborrower previous employment - EmplmtStartD_rep");
			Assert.AreEqual(B_P_DATE_TO, emp.EmplmtEndD_rep, "coborrower previous employment - EmplmtEndD_rep");

            if (!skipEmployerBusinessPhoneAndMonthlyIncomeChecks)
            {
                Assert.AreEqual(B_P_MONTHLY_INCOME, emp.MonI_rep, "coborrower previous employment - MonI_rep");
                Assert.AreEqual(B_P_PHONE, emp.EmplrBusPhone, "coborrower previous employment - EmplrBusPhone");
            }

			Assert.AreEqual(B_P_FAX, emp.EmplrFax, "coborrower previous employment - EmplrFax");
			Assert.AreEqual(B_P_VERIF_SENT, emp.VerifSentD_rep, "coborrower previous employment - VerifSentD_rep");
			Assert.AreEqual(B_P_REORDER, emp.VerifReorderedD_rep, "coborrower previous employment - VerifReorderedD_rep");
			Assert.AreEqual(B_P_RECEIVED, emp.VerifRecvD_rep, "coborrower previous employment - VerifRecvD_rep");
			Assert.AreEqual(B_P_EXPECTED, emp.VerifExpD_rep, "coborrower previous employment - VerifExpD_rep");
		}

		private void ConfirmBorrowerMonthlyIncomeWasCoborrowerMonthlyIncome(CAppData dataApp)
		{
			Assert.AreEqual(C_BASE_INCOME, dataApp.aBBaseI_rep, "borrower monthly income - aBBaseI_rep");
			Assert.AreEqual(C_OVERTIME, dataApp.aBOvertimeI_rep, "borrower monthly income - aBOvertimeI_rep");
			Assert.AreEqual(C_BONUSES, dataApp.aBBonusesI_rep, "borrower monthly income - aBBonusesI_rep");
			Assert.AreEqual(C_COMMISSION, dataApp.aBCommisionI_rep, "borrower monthly income - aBCommisionI_rep");
			Assert.AreEqual(C_DIVIDENDS, dataApp.aBDividendI_rep, "borrower monthly income - aBDividendI_rep");
			Assert.AreEqual(C_NET_RENT, dataApp.aBNetRentI1003_rep, "borrower monthly income - aBNetRentI1003_rep");
		}

		private void ConfirmCoborrowerMonthlyIncomeWasBorrowerMonthlyIncome(CAppData dataApp)
		{
			Assert.AreEqual(B_BASE_INCOME, dataApp.aCBaseI_rep, "coborrower monthly income - aCBaseI_rep");
			Assert.AreEqual(B_OVERTIME, dataApp.aCOvertimeI_rep, "coborrower monthly income - aCOvertimeI_rep");
			Assert.AreEqual(B_BONUSES, dataApp.aCBonusesI_rep, "coborrower monthly income - aCBonusesI_rep");
			Assert.AreEqual(B_COMMISSION, dataApp.aCCommisionI_rep, "coborrower monthly income - aCCommisionI_rep");
			Assert.AreEqual(B_DIVIDENDS, dataApp.aCDividendI_rep, "coborrower monthly income - aCDividendI_rep");
			Assert.AreEqual(B_NET_RENT, dataApp.aCNetRentI1003_rep, "coborrower monthly income - aCNetRentI1003_rep");
		}

		private void ConfirmOtherMonthlyIncomeSwapped(CAppData dataApp)
		{
            // Obsolete Fields
            //Assert.AreEqual(!OTHER_INCOME_1_FOR_COBORROWER, dataApp.aO1IForC, "aO1IForC");
            //Assert.AreEqual(OTHER_INCOME_1_DESC, dataApp.aO1IDesc, "aO1IDesc");
            //Assert.AreEqual(OTHER_INCOME_1_AMOUNT, dataApp.aO1I_rep, "aO1I_rep");
            //Assert.AreEqual(!OTHER_INCOME_2_FOR_COBORROWER, dataApp.aO2IForC, "aO2IForC");
            //Assert.AreEqual(OTHER_INCOME_2_DESC, dataApp.aO2IDesc, "aO2IDesc");
            //Assert.AreEqual(OTHER_INCOME_2_AMOUNT, dataApp.aO2I_rep, "aO2I_rep");
            //Assert.AreEqual(!OTHER_INCOME_3_FOR_COBORROWER, dataApp.aO3IForC, "aO3IForC");
            //Assert.AreEqual(OTHER_INCOME_3_DESC, dataApp.aO3IDesc, "aO3IDesc");
            //Assert.AreEqual(OTHER_INCOME_3_AMOUNT, dataApp.aO3I_rep, "aO3I_rep");
		}

		private void ConfirmLiabilitiesSwapped(CAppData dataApp)
		{
			ILiaCollection liabilityCollection = dataApp.aLiaCollection;
			ILiabilityRegular record0 = liabilityCollection.GetRegRecordOf(m_coborrowerLiabilityId);
			ILiabilityRegular record1 = liabilityCollection.GetRegRecordOf(m_borrowerLiabilityId);
			ILiabilityRegular record2 = liabilityCollection.GetRegRecordOf(m_jointLiabilityId);

			ConfirmBorrowerLiabilityHoldsPreviousCoborrowerLiability(record0);
			ConfirmCoborrowerLiabilityHoldsPreviousBorrowerLiability(record1);
			ConfirmJointLiabilityUnchanged(Operation.SwapBorrowers, record2, dataApp.LoanData.sBorrowerApplicationCollectionT);
		}

		private static void ConfirmJointLiabilityUnchanged(Operation operation, ILiabilityRegular record, E_sLqbCollectionT borrowerApplicationCollectionType)
		{
            if (operation == Operation.DeleteCoborrower && borrowerApplicationCollectionType != E_sLqbCollectionT.Legacy)
            {
                // The old behavior would leave the ownership as joint even if the coborrower was deleted.
                // The new data layer will not do this.
                Assert.AreEqual(E_LiaOwnerT.Borrower, record.OwnerT);
            }
            else
            {
                Assert.AreEqual(E_LiaOwnerT.Joint, record.OwnerT, "joint liability - OwnerT");
            }

            Assert.AreEqual(J_L_DEBT_TYPE, record.DebtT, "joint liability - DebtT");
			Assert.AreEqual(J_L_COMPANY, record.ComNm, "joint liability - ComNm");
			Assert.AreEqual(J_L_ADDRESS, record.ComAddr, "joint liability - ComAddr");
			Assert.AreEqual(J_L_CITY, record.ComCity, "joint liability - ComCity");
			Assert.AreEqual(J_L_STATE, record.ComState, "joint liability - ComState");
			Assert.AreEqual(J_L_ZIP, record.ComZip, "joint liability - ComZip");
			Assert.AreEqual(J_L_PHONE, record.ComPhone, "joint liability - ComPhone");
			Assert.AreEqual(J_L_FAX, record.ComFax, "joint liability - ComFax");
			Assert.AreEqual(J_L_DESC, record.Desc, "joint liability - Desc");
			Assert.AreEqual(J_L_ACCOUNT_HOLDER_NAME, record.AccNm, "joint liability - AccNm");
			Assert.AreEqual(J_L_ACCOUNT_NUMBER, record.AccNum.Value, "joint liability - AccNum");
			Assert.AreEqual(J_L_BALANCE, record.Bal_rep, "joint liability - Bal_rep");
			Assert.AreEqual(J_L_PMT, record.Pmt_rep, "joint liability - Pmt_rep");
			Assert.AreEqual(J_L_MONTHS_LEFT, record.RemainMons_rep, "joint liability - RemainMons_rep");
			Assert.AreEqual(J_L_RATE, record.R_rep, "joint liability - R_rep");
			Assert.AreEqual(J_L_TERM, record.OrigTerm_rep, "joint liability - OrigTerm_rep");
			Assert.AreEqual(J_L_DUE_IN, record.Due_rep, "joint liability - Due_rep");
			Assert.AreEqual(J_L_LATE_30, record.Late30_rep, "joint liability - Late30_rep");
			Assert.AreEqual(J_L_LATE_60, record.Late60_rep, "joint liability - Late60_rep");
			Assert.AreEqual(J_L_LATE_90, record.Late90Plus_rep, "joint liability - Late90Plus_rep");
			Assert.AreEqual(J_L_WILL_BE_PAID_OFF, record.WillBePdOff, "joint liability - WillBePdOff");
			Assert.AreEqual(!J_L_DEBT_SHOULD_BE_INCLUDED_IN_RATIOS, record.NotUsedInRatio, "joint liability - NotUsedInRatio");
			Assert.AreEqual(J_L_DEBT_IS_NEW_SUBJECT_TO_PIGGYBACK, record.IsPiggyBack, "joint liability - IsPiggyBack");
			Assert.AreEqual(J_L_INCLUDE_IN_REPOSSESSION, record.IncInReposession, "joint liability - IncInReposession");
			Assert.AreEqual(J_L_INCLUDE_IN_BANKRUPTCY, record.IncInBankruptcy, "joint liability - IncInBankruptcy");
			Assert.AreEqual(J_L_INCLUDE_IN_FORECLOSURE, record.IncInForeclosure, "joint liability - IncInForeclosure");
			Assert.AreEqual(J_L_EXCLUDE_FROM_UNDERWRITING, record.ExcFromUnderwriting, "joint liability - ExcFromUnderwriting");
			Assert.AreEqual(J_L_VERIF_SENT, record.VerifSentD_rep, "joint liability - VerifSentD_rep");
			Assert.AreEqual(J_L_REORDER, record.VerifReorderedD_rep, "joint liability - VerifReorderedD_rep");
			Assert.AreEqual(J_L_RECEIVED, record.VerifRecvD_rep, "joint liability - VerifRecvD_rep");
			Assert.AreEqual(J_L_EXPECTED, record.VerifExpD_rep, "joint liability - VerifExpD_rep");
		}

		private static void ConfirmCoborrowerLiabilityHoldsPreviousBorrowerLiability(ILiabilityRegular record)
		{
			Assert.AreEqual(E_LiaOwnerT.CoBorrower, record.OwnerT, "coborrower liability - OwnerT");
			Assert.AreEqual(B_L_DEBT_TYPE, record.DebtT, "coborrower liability - DebtT");
			Assert.AreEqual(B_L_COMPANY, record.ComNm, "coborrower liability - ComNm");
			Assert.AreEqual(B_L_ADDRESS, record.ComAddr, "coborrower liability - ComAddr");
			Assert.AreEqual(B_L_CITY, record.ComCity, "coborrower liability - ComCity");
			Assert.AreEqual(B_L_STATE, record.ComState, "coborrower liability - ComState");
			Assert.AreEqual(B_L_ZIP, record.ComZip, "coborrower liability - ComZip");
			Assert.AreEqual(B_L_PHONE, record.ComPhone, "coborrower liability - ComPhone");
			Assert.AreEqual(B_L_FAX, record.ComFax, "coborrower liability - ComFax");
			Assert.AreEqual(B_L_DESC, record.Desc, "coborrower liability - Desc");
			Assert.AreEqual(B_L_ACCOUNT_HOLDER_NAME, record.AccNm, "coborrower liability - AccNm");
			Assert.AreEqual(B_L_ACCOUNT_NUMBER, record.AccNum.Value, "coborrower liability - AccNum");
			Assert.AreEqual(B_L_BALANCE, record.Bal_rep, "coborrower liability - Bal_rep");
			Assert.AreEqual(B_L_PMT, record.Pmt_rep, "coborrower liability - Pmt_rep");
			Assert.AreEqual(B_L_MONTHS_LEFT, record.RemainMons_rep, "coborrower liability - RemainMons_rep");
			Assert.AreEqual(B_L_RATE, record.R_rep, "coborrower liability - R_rep");
			Assert.AreEqual(B_L_TERM, record.OrigTerm_rep, "coborrower liability - OrigTerm_rep");
			Assert.AreEqual(B_L_DUE_IN, record.Due_rep, "coborrower liability - Due_rep");
			Assert.AreEqual(B_L_LATE_30, record.Late30_rep, "coborrower liability - Late30_rep");
			Assert.AreEqual(B_L_LATE_60, record.Late60_rep, "coborrower liability - Late60_rep");
			Assert.AreEqual(B_L_LATE_90, record.Late90Plus_rep, "coborrower liability - Late90Plus_rep");
			Assert.AreEqual(B_L_WILL_BE_PAID_OFF, record.WillBePdOff, "coborrower liability - WillBePdOff");
			Assert.AreEqual(!B_L_DEBT_SHOULD_BE_INCLUDED_IN_RATIOS, record.NotUsedInRatio, "coborrower liability - NotUsedInRatio");
			Assert.AreEqual(B_L_DEBT_IS_NEW_SUBJECT_TO_PIGGYBACK, record.IsPiggyBack, "coborrower liability - IsPiggyBack");
			Assert.AreEqual(B_L_INCLUDE_IN_REPOSSESSION, record.IncInReposession, "coborrower liability - IncInReposession");
			Assert.AreEqual(B_L_INCLUDE_IN_BANKRUPTCY, record.IncInBankruptcy, "coborrower liability - IncInBankruptcy");
			Assert.AreEqual(B_L_INCLUDE_IN_FORECLOSURE, record.IncInForeclosure, "coborrower liability - IncInForeclosure");
			Assert.AreEqual(B_L_EXCLUDE_FROM_UNDERWRITING, record.ExcFromUnderwriting, "coborrower liability - ExcFromUnderwriting");
			Assert.AreEqual(B_L_VERIF_SENT, record.VerifSentD_rep, "coborrower liability - VerifSentD_rep");
			Assert.AreEqual(B_L_REORDER, record.VerifReorderedD_rep, "coborrower liability - VerifReorderedD_rep");
			Assert.AreEqual(B_L_RECEIVED, record.VerifRecvD_rep, "coborrower liability - VerifRecvD_rep");
			Assert.AreEqual(B_L_EXPECTED, record.VerifExpD_rep, "coborrower liability - VerifExpD_rep");
		}

		private static void ConfirmBorrowerLiabilityHoldsPreviousCoborrowerLiability(ILiabilityRegular record)
		{
			Assert.AreEqual(E_LiaOwnerT.Borrower, record.OwnerT, "borrower liability - OwnerT");
			Assert.AreEqual(C_L_DEBT_TYPE, record.DebtT, "borrower liability - DebtT");
			Assert.AreEqual(C_L_COMPANY, record.ComNm, "borrower liability - ComNm");
			Assert.AreEqual(C_L_ADDRESS, record.ComAddr, "borrower liability - ComAddr");
			Assert.AreEqual(C_L_CITY, record.ComCity, "borrower liability - ComCity");
			Assert.AreEqual(C_L_STATE, record.ComState, "borrower liability - ComState");
			Assert.AreEqual(C_L_ZIP, record.ComZip, "borrower liability - ComZip");
			Assert.AreEqual(C_L_PHONE, record.ComPhone, "borrower liability - ComPhone");
			Assert.AreEqual(C_L_FAX, record.ComFax, "borrower liability - ComFax");
			Assert.AreEqual(C_L_DESC, record.Desc, "borrower liability - Desc");
			Assert.AreEqual(C_L_ACCOUNT_HOLDER_NAME, record.AccNm, "borrower liability - AccNm");
			Assert.AreEqual(C_L_ACCOUNT_NUMBER, record.AccNum.Value, "borrower liability - AccNum");
			Assert.AreEqual(C_L_BALANCE, record.Bal_rep, "borrower liability - Bal_rep");
			Assert.AreEqual(C_L_PMT, record.Pmt_rep, "borrower liability - Pmt_rep");
			Assert.AreEqual(C_L_MONTHS_LEFT, record.RemainMons_rep, "borrower liability - RemainMons_rep");
			Assert.AreEqual(C_L_RATE, record.R_rep, "borrower liability - R_rep");
			Assert.AreEqual(C_L_TERM, record.OrigTerm_rep, "borrower liability - OrigTerm_rep");
			Assert.AreEqual(C_L_DUE_IN, record.Due_rep, "borrower liability - Due_rep");
			Assert.AreEqual(C_L_LATE_30, record.Late30_rep, "borrower liability - Late30_rep");
			Assert.AreEqual(C_L_LATE_60, record.Late60_rep, "borrower liability - Late60_rep");
			Assert.AreEqual(C_L_LATE_90, record.Late90Plus_rep, "borrower liability - Late90Plus_rep");
			Assert.AreEqual(C_L_WILL_BE_PAID_OFF, record.WillBePdOff, "borrower liability - WillBePdOff");
			Assert.AreEqual(!C_L_DEBT_SHOULD_BE_INCLUDED_IN_RATIOS, record.NotUsedInRatio, "borrower liability - NotUsedInRatio");
			Assert.AreEqual(C_L_DEBT_IS_NEW_SUBJECT_TO_PIGGYBACK, record.IsPiggyBack, "borrower liability - IsPiggyBack");
			Assert.AreEqual(C_L_INCLUDE_IN_REPOSSESSION, record.IncInReposession, "borrower liability - IncInReposession");
			Assert.AreEqual(C_L_INCLUDE_IN_BANKRUPTCY, record.IncInBankruptcy, "borrower liability - IncInBankruptcy");
			Assert.AreEqual(C_L_INCLUDE_IN_FORECLOSURE, record.IncInForeclosure, "borrower liability - IncInForeclosure");
			Assert.AreEqual(C_L_EXCLUDE_FROM_UNDERWRITING, record.ExcFromUnderwriting, "borrower liability - ExcFromUnderwriting");
			Assert.AreEqual(C_L_VERIF_SENT, record.VerifSentD_rep, "borrower liability - VerifSentD_rep");
			Assert.AreEqual(C_L_REORDER, record.VerifReorderedD_rep, "borrower liability - VerifReorderedD_rep");
			Assert.AreEqual(C_L_RECEIVED, record.VerifRecvD_rep, "borrower liability - VerifRecvD_rep");
			Assert.AreEqual(C_L_EXPECTED, record.VerifExpD_rep, "borrower liability - VerifExpD_rep");
		}

		private void ConfirmAssetsSwapped(CAppData dataApp)
		{
			IAssetCollection assetCollection = dataApp.aAssetCollection;
			var asset0 = assetCollection.GetRegRecordOf(m_coborrowerAssetId);
			var asset1 = assetCollection.GetRegRecordOf(m_borrowerAssetId);
			var asset2 = assetCollection.GetRegRecordOf(m_jointAssetId);

			ConfirmBorrowerAssetsHoldsPreviousCoborrowerAssets(asset0);
			ConfirmCoborrowerAssetsHoldsPreviousBorrowersAssets(asset1);
			ConfirmJointAssetsUnchanged(Operation.SwapBorrowers, asset2, dataApp.LoanData.sBorrowerApplicationCollectionT);
		}

		private static void ConfirmJointAssetsUnchanged(Operation operation, IAssetRegular asset, E_sLqbCollectionT borrowerApplicationCollectionType)
		{
            if (operation == Operation.DeleteCoborrower && borrowerApplicationCollectionType != E_sLqbCollectionT.Legacy)
            {
                // The old behavior would leave the ownership as joint even if the coborrower was deleted.
                // The new data layer will not do this.
                Assert.AreEqual(E_AssetOwnerT.Borrower, asset.OwnerT);
            }
            else
            {
                Assert.AreEqual(E_AssetOwnerT.Joint, asset.OwnerT, "joint asset - OwnerT");
            }

            Assert.AreEqual(J_A_TYPE, asset.AssetT, "joint asset - AssetT");
			Assert.AreEqual(J_A_OTHER_TYPE, asset.OtherTypeDesc, "joint asset - OtherTypeDesc");
			Assert.AreEqual(J_A_COMPANY, asset.ComNm, "joint asset - ComNm");
			Assert.AreEqual(J_A_DEPARTMENT, asset.DepartmentName, "joint asset - DepartmentName");
			Assert.AreEqual(J_A_ADDRESS, asset.StAddr, "joint asset - StAddr");
			Assert.AreEqual(J_A_CITY, asset.City, "joint asset - City");
			Assert.AreEqual(J_A_STATE, asset.State, "joint asset - State");
			Assert.AreEqual(J_A_ZIP, asset.Zip, "joint asset - Zip");
			Assert.AreEqual(J_A_DESC, asset.Desc, "joint asset - Desc");
			Assert.AreEqual(J_A_VALUE, asset.Val_rep, "joint asset - Val_rep");
			Assert.AreEqual(J_A_ACCOUNT_HOLDER_NAME, asset.AccNm, "joint asset - AccNm");
			Assert.AreEqual(J_A_ACCOUNT_NUMBER, asset.AccNum.Value, "joint asset - AccNum");
			Assert.AreEqual(J_A_VERIF_SENT, asset.VerifSentD_rep, "joint asset - VerifSentD_rep");
			Assert.AreEqual(J_A_REORDER, asset.VerifReorderedD_rep, "joint asset - VerifReorderedD_rep");
			Assert.AreEqual(J_A_RECEIVED, asset.VerifRecvD_rep, "joint asset - VerifRecvD_rep");
			Assert.AreEqual(J_A_EXPECTED, asset.VerifExpD_rep, "joint asset - VerifExpD_rep");
		}

		private static void ConfirmCoborrowerAssetsHoldsPreviousBorrowersAssets(IAssetRegular asset)
		{
			Assert.AreEqual(E_AssetOwnerT.CoBorrower, asset.OwnerT, "coborrower asset - OwnerT");
			Assert.AreEqual(B_A_TYPE, asset.AssetT, "coborrower asset - AssetT");
			Assert.AreEqual(B_A_OTHER_TYPE, asset.OtherTypeDesc, "coborrower asset - OtherTypeDesc");
			Assert.AreEqual(B_A_COMPANY, asset.ComNm, "coborrower asset - ComNm");
			Assert.AreEqual(B_A_DEPARTMENT, asset.DepartmentName, "coborrower asset - DepartmentName");
			Assert.AreEqual(B_A_ADDRESS, asset.StAddr, "coborrower asset - StAddr");
			Assert.AreEqual(B_A_CITY, asset.City, "coborrower asset - City");
			Assert.AreEqual(B_A_STATE, asset.State, "coborrower asset - State");
			Assert.AreEqual(B_A_ZIP, asset.Zip, "coborrower asset - Zip");
			Assert.AreEqual(B_A_DESC, asset.Desc, "coborrower asset - Desc");
			Assert.AreEqual(B_A_VALUE, asset.Val_rep, "coborrower asset - Val_rep");
			Assert.AreEqual(B_A_ACCOUNT_HOLDER_NAME, asset.AccNm, "coborrower asset - AccNm");
			Assert.AreEqual(B_A_ACCOUNT_NUMBER, asset.AccNum.Value, "coborrower asset - AccNum");
			Assert.AreEqual(B_A_VERIF_SENT, asset.VerifSentD_rep, "coborrower asset - VerifSentD_rep");
			Assert.AreEqual(B_A_REORDER, asset.VerifReorderedD_rep, "coborrower asset - VerifReorderedD_rep");
			Assert.AreEqual(B_A_RECEIVED, asset.VerifRecvD_rep, "coborrower asset - VerifRecvD_rep");
			Assert.AreEqual(B_A_EXPECTED, asset.VerifExpD_rep, "coborrower asset - VerifExpD_rep");
		}

		private static void ConfirmBorrowerAssetsHoldsPreviousCoborrowerAssets(IAssetRegular asset)
		{
			Assert.AreEqual(E_AssetOwnerT.Borrower, asset.OwnerT, "borrower asset - OwnerT");
			Assert.AreEqual(C_A_TYPE, asset.AssetT, "borrower asset - AssetT");
			Assert.AreEqual(C_A_OTHER_TYPE, asset.OtherTypeDesc, "borrower asset - OtherTypeDesc");
			Assert.AreEqual(C_A_COMPANY, asset.ComNm, "borrower asset - ComNm");
			Assert.AreEqual(C_A_DEPARTMENT, asset.DepartmentName, "borrower asset - DepartmentName");
			Assert.AreEqual(C_A_ADDRESS, asset.StAddr, "borrower asset - StAddr");
			Assert.AreEqual(C_A_CITY, asset.City, "borrower asset - City");
			Assert.AreEqual(C_A_STATE, asset.State, "borrower asset - State");
			Assert.AreEqual(C_A_ZIP, asset.Zip, "borrower asset - Zip");
			Assert.AreEqual(C_A_DESC, asset.Desc, "borrower asset - Desc");
			Assert.AreEqual(C_A_VALUE, asset.Val_rep, "borrower asset - Val_rep");
			Assert.AreEqual(C_A_ACCOUNT_HOLDER_NAME, asset.AccNm, "borrower asset - AccNm");
			Assert.AreEqual(C_A_ACCOUNT_NUMBER, asset.AccNum.Value, "borrower asset - AccNum");
			Assert.AreEqual(C_A_VERIF_SENT, asset.VerifSentD_rep, "borrower asset - VerifSentD_rep");
			Assert.AreEqual(C_A_REORDER, asset.VerifReorderedD_rep, "borrower asset - VerifReorderedD_rep");
			Assert.AreEqual(C_A_RECEIVED, asset.VerifRecvD_rep, "borrower asset - VerifRecvD_rep");
			Assert.AreEqual(C_A_EXPECTED, asset.VerifExpD_rep, "borrower asset - VerifExpD_rep");
		}
	}
}
