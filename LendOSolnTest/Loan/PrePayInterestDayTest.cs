﻿using DataAccess;
using LendOSolnTest.Common;
using LendOSolnTest.Fakes;
using NUnit.Framework;

namespace LendOSolnTest.Loan
{
    [TestFixture]
    public class PrePayInterestDayTest
    {
        [TestFixtureSetUp]
        public void Setup()
        {
            LoginTools.LoginAs(TestAccountType.LoanOfficer);
        }

        private CPageData CreatePageData()
        {
            return FakePageData.Create();
        }

        [Test]
        public void Test()
        {
            var testCases = new[] {
                new { sEstCloseD_rep = "", sSchedDueD1_rep = "", sLPurposeT = E_sLPurposeT.Purchase, sConsummationD_rep="", sIPiaDy_rep = "15"}
                ,new { sEstCloseD_rep = "4/13/2010", sSchedDueD1_rep = "4/1/2010", sLPurposeT = E_sLPurposeT.Purchase, sConsummationD_rep="4/13/2010", sIPiaDy_rep = "0"}
                ,new { sEstCloseD_rep = "4/13/2010", sSchedDueD1_rep = "", sLPurposeT = E_sLPurposeT.Purchase, sConsummationD_rep="4/13/2010", sIPiaDy_rep = "15"}
                ,new { sEstCloseD_rep = "", sSchedDueD1_rep = "5/1/2010", sLPurposeT = E_sLPurposeT.Purchase, sConsummationD_rep="", sIPiaDy_rep = "15"}
                ,new { sEstCloseD_rep = "3/15/2010", sSchedDueD1_rep = "5/1/2010", sLPurposeT = E_sLPurposeT.Purchase, sConsummationD_rep="3/15/2010", sIPiaDy_rep = "17"}

                ,new { sEstCloseD_rep = "", sSchedDueD1_rep = "", sLPurposeT = E_sLPurposeT.Refin, sConsummationD_rep="", sIPiaDy_rep = "15"}
                ,new { sEstCloseD_rep = "4/13/2010", sSchedDueD1_rep = "4/1/2010", sLPurposeT = E_sLPurposeT.Refin, sConsummationD_rep="4/19/2010", sIPiaDy_rep = "0"}
                ,new { sEstCloseD_rep = "4/13/2010", sSchedDueD1_rep = "", sLPurposeT = E_sLPurposeT.Refin, sConsummationD_rep="4/19/2010", sIPiaDy_rep = "15"}
                ,new { sEstCloseD_rep = "", sSchedDueD1_rep = "5/1/2010", sLPurposeT = E_sLPurposeT.Refin, sConsummationD_rep="", sIPiaDy_rep = "15"}
                ,new { sEstCloseD_rep = "3/15/2010", sSchedDueD1_rep = "5/1/2010", sLPurposeT = E_sLPurposeT.Refin, sConsummationD_rep="3/19/2010", sIPiaDy_rep = "13"}

            };
            foreach (var o in testCases)
            {
                CPageData dataLoan = CreatePageData();
                dataLoan.InitLoad();

                dataLoan.sEstCloseD_rep = o.sEstCloseD_rep;
                dataLoan.sEstCloseDLckd = true;
                dataLoan.sSchedDueD1_rep = o.sSchedDueD1_rep;
                dataLoan.sSchedDueD1Lckd = true;

                dataLoan.sLPurposeT = o.sLPurposeT;
                dataLoan.sIPiaDyLckd = false;

                Assert.AreEqual(o.sConsummationD_rep, dataLoan.sConsummationD_rep, o.ToString());
                Assert.AreEqual(o.sIPiaDy_rep, dataLoan.sIPiaDy_rep, o.ToString());
            }
        }
    }
}
