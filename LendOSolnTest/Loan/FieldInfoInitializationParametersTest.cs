﻿namespace LendOSolnTest.Loan
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.ObjLib.Loan;
    using NUnit.Framework;

    [TestFixture]
    public class FieldInfoInitializationParametersTest
    {
        private FieldInfoTableInitializationParameters parametersWithoutOptimization;
        private FieldInfoTableInitializationParameters parametersWithOptimization;

        [TestFixtureSetUp]
        public void Setup()
        {
            this.parametersWithoutOptimization = Tools.GetFieldInfoInitializationParametersFromTypeInfo(usePredefinedObsoletePropertyOptimization: false);
            this.parametersWithOptimization = Tools.GetFieldInfoInitializationParametersFromTypeInfo(usePredefinedObsoletePropertyOptimization: true);
        }

        [Test]
        public void ObsoletePropertyNameList_WithOptimization_HasAllFields()
        {
            var listWithoutOptimization = this.parametersWithoutOptimization.ObsoletePropertyNameList;
            var listWithOptimization = this.parametersWithOptimization.ObsoletePropertyNameList;

            var missingFields = listWithoutOptimization.Except(listWithOptimization);
            Assert.IsFalse(missingFields.Any(), "Missing obsolete fields: " + string.Join(", ", missingFields));
        }

        [Test]
        public void ObsoletePropertyNameList_WithOptimization_HasNoExtraFields()
        {
            var listWithoutOptimization = this.parametersWithoutOptimization.ObsoletePropertyNameList;
            var listWithOptimization = this.parametersWithOptimization.ObsoletePropertyNameList;

            var extraFields = listWithOptimization.Except(listWithoutOptimization);
            Assert.IsFalse(extraFields.Any(), "Extra obsolete fields: " + string.Join(", ", extraFields));
        }

        [Test]
        public void DependsOnList_WithOptimization_HasAllFields()
        {
            var listWithoutOptimization = this.parametersWithoutOptimization.DependsOnAttributes;
            var listWithOptimization = this.parametersWithOptimization.DependsOnAttributes;

            var missingFields = listWithoutOptimization.Except(listWithOptimization, DependsOnComparer);
            Assert.IsFalse(missingFields.Any(), "The following DependsOn attributes are missing: " + string.Join(", ", missingFields.Select(kvp => kvp.Key)));
        }

        [Test]
        public void DependsOnList_WithOptimization_HasNoExtraFields()
        {
            var listWithoutOptimization = this.parametersWithoutOptimization.DependsOnAttributes;
            var listWithOptimization = this.parametersWithOptimization.DependsOnAttributes;

            var extraFields = listWithOptimization.Except(listWithoutOptimization, DependsOnComparer);
            Assert.IsFalse(extraFields.Any(), "The following extra DependsOn attributes exist: " + string.Join(", ", extraFields.Select(kvp => kvp.Key)));
        }

        private static DependsOnListComparer DependsOnComparer = new DependsOnListComparer(new FieldSpecComparer());

        private class DependsOnListComparer : IEqualityComparer<KeyValuePair<string, DependsOnAttribute>>
        {
            private readonly FieldSpecComparer fieldSpecComparer;

            public DependsOnListComparer(FieldSpecComparer comparer)
            {
                this.fieldSpecComparer = comparer;
            }

            public bool Equals(KeyValuePair<string, DependsOnAttribute> x, KeyValuePair<string, DependsOnAttribute> y)
            {
                if (!string.Equals(x.Key, y.Key, StringComparison.Ordinal))
                {
                    return false;
                }

                var firstDependsOn = x.Value.GetList();
                var secondDependsOn = y.Value.GetList();

                if (firstDependsOn.Count() != secondDependsOn.Count())
                {
                    return false;
                }

                return !firstDependsOn.Except(secondDependsOn, this.fieldSpecComparer).Any();
            }

            public int GetHashCode(KeyValuePair<string, DependsOnAttribute> obj)
            {
                return obj.GetHashCode();
            }
        }

        private class FieldSpecComparer : IEqualityComparer<FieldSpec>
        {
            public bool Equals(FieldSpec x, FieldSpec y)
            {
                return string.Equals(x?.Name, y?.Name, StringComparison.OrdinalIgnoreCase);
            }

            public int GetHashCode(FieldSpec obj)
            {
                return obj.Name.GetHashCode();
            }
        }
    }
}
