﻿using DataAccess;
using LendersOffice.Constants;
using LendOSolnTest.Common;
using LendOSolnTest.Fakes;
using NUnit.Framework;

namespace LendOSolnTest.Loan
{
    [TestFixture]
    public class BorrowerEmploymentTest
    {
        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            LoginTools.LoginAs(TestAccountType.LoTest001);
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
            FakePageDataUtilities.Instance.Reset();
        }

        private CPageData CreateDataObject()
        {
            CPageData dataLoan = FakePageData.Create();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            CAppData dataApp = dataLoan.GetAppData(0);
            dataApp.aBEmpCollection.ClearAll();
            dataApp.aCEmpCollection.ClearAll();

            dataApp.aBEmpCollection.Flush();
            dataApp.aCEmpCollection.Flush();

            dataLoan.Save();

            return dataLoan;
        }

        [Test]
        public void BasicTest()
        {
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            CAppData dataApp = dataLoan.GetAppData(0);

            IEmpCollection empCollection = dataApp.aBEmpCollection;

            IPrimaryEmploymentRecord primaryEmployment =  empCollection.GetPrimaryEmp(true);
            primaryEmployment.EmplrNm = "PRIMARY EMPL";
            primaryEmployment.Update();

            var previousEmploymentList = new[] {
                new { IsCurrent = true, EmplrNm = "SECOND JOB", EmplmtStartD = "1/1/2010", EmplmtEndD = "", Expected_EmplmtEndD = "PRESENT"},
                new { IsCurrent = false, EmplrNm = "TEST", EmplmtStartD = "1/1/1990", EmplmtEndD="1/1/2000", Expected_EmplmtEndD= "1/1/2000"}
            };

            foreach (var o in previousEmploymentList)
            {
                IRegularEmploymentRecord employment1 = empCollection.AddRegularRecord();
                employment1.IsCurrent = o.IsCurrent;
                employment1.EmplrNm = o.EmplrNm;
                employment1.EmplmtStartD_rep = o.EmplmtStartD;
                employment1.EmplmtEndD_rep = o.EmplmtEndD;
                employment1.Update();
            }
            dataLoan.Save();

            dataLoan = FakePageData.Create();
            dataLoan.InitLoad();

            dataApp = dataLoan.GetAppData(0);

            empCollection = dataApp.aBEmpCollection;

            primaryEmployment = empCollection.GetPrimaryEmp(false);
            Assert.AreEqual("PRIMARY EMPL", primaryEmployment.EmplrNm);
            Assert.AreEqual(true, primaryEmployment.IsCurrent);

            for (int i = 0; i < previousEmploymentList.Length; i++)
            {
                var o = previousEmploymentList[i];
                IRegularEmploymentRecord employment1 = empCollection.GetRegularRecordAt(i);
                Assert.AreEqual(o.IsCurrent, employment1.IsCurrent);
                Assert.AreEqual(o.EmplrNm, employment1.EmplrNm);
                Assert.AreEqual(o.EmplmtStartD, employment1.EmplmtStartD_rep);
                Assert.AreEqual(o.Expected_EmplmtEndD, employment1.EmplmtEndD_rep);

            }
        }
    }
}
