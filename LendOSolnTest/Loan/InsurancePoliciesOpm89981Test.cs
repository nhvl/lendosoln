﻿using DataAccess;
using LendersOffice.Constants;
using LendOSolnTest.Common;
using LendOSolnTest.Fakes;
using NUnit.Framework;

namespace LendOSolnTest.Loan
{
    [TestFixture]
    public class InsurancePoliciesOpm89981Test
    {
        [TestFixtureSetUp]
        public void Setup()
        {
            LoginTools.LoginAs(TestAccountType.LockDesk_CloserPerFieldPerm);
        }

        [TestFixtureTearDown]
        public void TearDown()
        {
            FakePageDataUtilities.Instance.Reset();
        }

        [Test]
        public void TestHazardInsurancePolicyFields()
        {
            CPageData dataLoan = CreatePageData();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            string dateStr = "12/20/2012";
            decimal amt = 1000.00M;
            dataLoan.sHazInsCompanyNm = "CN";
            dataLoan.sHazInsPolicyNum = "ABC123";
            dataLoan.sHazInsPolicyActivationD_rep = dateStr;
            dataLoan.sHazInsPaidByT = E_sInsPaidByT.Lender;
            dataLoan.sHazInsCoverageAmt = amt;
            dataLoan.sHazInsDeductibleAmt = amt;
            dataLoan.sHazInsPaymentDueD_rep = dateStr;
            dataLoan.sHazInsPaymentMadeD_rep = dateStr;
            dataLoan.sHazInsPaidThroughD_rep = dateStr;

            dataLoan.Save();

            dataLoan = CreatePageData();
            dataLoan.InitLoad();

            Assert.AreEqual("CN", dataLoan.sHazInsCompanyNm, "InsCompanyNm");
            Assert.AreEqual("ABC123", dataLoan.sHazInsPolicyNum, "InsPolicyNum");
            Assert.AreEqual(dateStr, dataLoan.sHazInsPolicyActivationD_rep, "InsPolicyActivationD");
            Assert.AreEqual(E_sInsPaidByT.Lender, dataLoan.sHazInsPaidByT, "InsPaidByT");
            Assert.AreEqual(amt, dataLoan.sHazInsCoverageAmt, "InsCoverageAmt");
            Assert.AreEqual(amt, dataLoan.sHazInsDeductibleAmt, "InsDeductibleAmt");
            Assert.AreEqual(dateStr, dataLoan.sHazInsPaymentDueD_rep, "InsPolicyPaymentDueD");
            Assert.AreEqual(dateStr, dataLoan.sHazInsPaymentMadeD_rep, "InsPolicyPaymentMadeD");
            Assert.AreEqual(dateStr, dataLoan.sHazInsPaidThroughD_rep, "InsPolicyPaidThroughD");
        }

        [Test]
        public void TestHazInsPaymentDueAmt()
        {
            CPageData dataLoan = CreatePageData();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            dataLoan.CalcModeT = E_CalcModeT.LendersOfficePageEditing;
            dataLoan.sProHazInsR = 1M;
            dataLoan.sProHazInsT = E_PercentBaseT.LoanAmount;
            dataLoan.sLAmtLckd = true;            
            dataLoan.sLAmtCalc = 120000;
            // dataLoan.sProHazInsBaseAmt == sLAmCalc 
            dataLoan.sProHazInsMb = 0;           
            // dataLoan.sProHazIns == sProHazInsMb + (sProHazInsR * sProHazInsBaseAmt / 1200.00M)

            // Set the hazard billing period
            int[,] billingPeriods = dataLoan.sInitialEscrowAcc;
            billingPeriods[1, (int)E_sBillingPeriodT.HazardInsurance] = 2;
            dataLoan.sInitialEscrowAcc = billingPeriods;
            
            dataLoan.Save();

            dataLoan = CreatePageData();
            dataLoan.InitLoad();

            // Test won't be valid if this no longer holds
            Assert.AreEqual(2, dataLoan.sInitialEscrowAcc[1, (int)E_sBillingPeriodT.HazardInsurance], "BillingPeriod");
            Assert.AreEqual(100M, dataLoan.sProHazIns, "ProHazIns"); 
            decimal amtDue = 100 * 2;
            
            // sHazInsPaymentDueAmt == sProHazIns * hazardBillingPeriod
            Assert.AreEqual(amtDue, dataLoan.sHazInsPaymentDueAmt, "PaymentDueAmt");
        }

        [Test]
        public void TestHazInsPaymentDueAmtLckd()
        {
            CPageData dataLoan = CreatePageData();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            dataLoan.CalcModeT = E_CalcModeT.LendersOfficePageEditing;
            dataLoan.sProHazInsR = 1M;
            dataLoan.sProHazInsT = E_PercentBaseT.LoanAmount;
            dataLoan.sLAmtLckd = true;
            dataLoan.sLAmtCalc = 120000;
            dataLoan.sProHazInsMb = 0;

            int[,] billingPeriods = dataLoan.sInitialEscrowAcc;
            billingPeriods[1, (int)E_sBillingPeriodT.HazardInsurance] = 2;
            dataLoan.sInitialEscrowAcc = billingPeriods;
            
            dataLoan.sHazInsPaymentDueAmtLckd = true;
            dataLoan.sHazInsPaymentDueAmt = 50M;

            dataLoan.Save();

            dataLoan = CreatePageData();
            dataLoan.InitLoad();

            Assert.AreEqual(50M, dataLoan.sHazInsPaymentDueAmt, "PaymentDueAmt");
        }

        [Test]
        public void TestFloodInsurancePolicyFields()
        {
            CPageData dataLoan = CreatePageData();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            string dateStr = "12/20/2012";
            decimal amt = 1000.00M;
            dataLoan.sFloodInsCompanyNm = "CN";
            dataLoan.sFloodInsPolicyNum = "ABC123";
            dataLoan.sFloodInsPolicyActivationD_rep = dateStr;
            dataLoan.sFloodInsPaidByT = E_sInsPaidByT.Lender;
            dataLoan.sFloodInsCoverageAmt = amt;
            dataLoan.sFloodInsDeductibleAmt = amt;
            dataLoan.sFloodInsPaymentDueD_rep = dateStr;
            dataLoan.sFloodInsPaymentMadeD_rep = dateStr;
            dataLoan.sFloodInsPaidThroughD_rep = dateStr;

            dataLoan.Save();

            dataLoan = CreatePageData();
            dataLoan.InitLoad();

            Assert.AreEqual("CN", dataLoan.sFloodInsCompanyNm, "InsCompanyNm");
            Assert.AreEqual("ABC123", dataLoan.sFloodInsPolicyNum, "InsPolicyNum");
            Assert.AreEqual(dateStr, dataLoan.sFloodInsPolicyActivationD_rep, "InsPolicyActivationD");
            Assert.AreEqual(E_sInsPaidByT.Lender, dataLoan.sFloodInsPaidByT, "InsPaidByT");
            Assert.AreEqual(amt, dataLoan.sFloodInsCoverageAmt, "InsCoverageAmt");
            Assert.AreEqual(amt, dataLoan.sFloodInsDeductibleAmt, "InsDeductibleAmt");
            Assert.AreEqual(dateStr, dataLoan.sFloodInsPaymentDueD_rep, "InsPolicyPaymentDueD");
            Assert.AreEqual(dateStr, dataLoan.sFloodInsPaymentMadeD_rep, "InsPolicyPaymentMadeD");
            Assert.AreEqual(dateStr, dataLoan.sFloodInsPaidThroughD_rep, "InsPolicyPaidThroughD");
        }

        [Test]
        public void TestFloodInsPaymentDueAmt()
        {
            CPageData dataLoan = CreatePageData();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            dataLoan.sProFloodIns = 100M;

            int[,] billingPeriods = dataLoan.sInitialEscrowAcc;
            billingPeriods[1, (int)E_sBillingPeriodT.FloodInsurance] = 2;
            dataLoan.sInitialEscrowAcc = billingPeriods;

            dataLoan.Save();

            dataLoan = CreatePageData();
            dataLoan.InitLoad();

            Assert.AreEqual(2, dataLoan.sInitialEscrowAcc[1, (int)E_sBillingPeriodT.FloodInsurance], "BillingPeriod");
            Assert.AreEqual(100M, dataLoan.sProFloodIns, "ProFloodIns");
            decimal amtDue = 100 * 2;

            Assert.AreEqual(amtDue, dataLoan.sFloodInsPaymentDueAmt, "PaymentDueAmt");
        }

        [Test]
        public void TestFloodInsPaymentDueAmtLckd()
        {
            CPageData dataLoan = CreatePageData();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            dataLoan.sProFloodIns = 100M;

            int[,] billingPeriods = dataLoan.sInitialEscrowAcc;
            billingPeriods[1, (int)E_sBillingPeriodT.FloodInsurance] = 2;
            dataLoan.sInitialEscrowAcc = billingPeriods;

            dataLoan.sFloodInsPaymentDueAmtLckd = true;
            dataLoan.sFloodInsPaymentDueAmt = 50M;

            dataLoan.Save();

            dataLoan = CreatePageData();
            dataLoan.InitLoad();

            Assert.AreEqual(50M, dataLoan.sFloodInsPaymentDueAmt, "PaymentDueAmt");
        }

        [Test]
        public void TestWindInsurancePolicyFields()
        {
            CPageData dataLoan = CreatePageData();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            string dateStr = "12/20/2012";
            decimal amt = 1000.00M;
            dataLoan.sWindInsCompanyNm = "CN";
            dataLoan.sWindInsPolicyNum = "ABC123";
            dataLoan.sWindInsPolicyActivationD_rep = dateStr;
            dataLoan.sWindInsPaidByT = E_sInsPaidByT.Lender;
            dataLoan.sWindInsCoverageAmt = amt;
            dataLoan.sWindInsDeductibleAmt = amt;
            dataLoan.sWindInsPaymentDueD_rep = dateStr;
            dataLoan.sWindInsPaymentMadeD_rep = dateStr;
            dataLoan.sWindInsPaidThroughD_rep = dateStr;

            dataLoan.Save();

            dataLoan = CreatePageData();
            dataLoan.InitLoad();

            Assert.AreEqual("CN", dataLoan.sWindInsCompanyNm, "InsCompanyNm");
            Assert.AreEqual("ABC123", dataLoan.sWindInsPolicyNum, "InsPolicyNum");
            Assert.AreEqual(dateStr, dataLoan.sWindInsPolicyActivationD_rep, "InsPolicyActivationD");
            Assert.AreEqual(E_sInsPaidByT.Lender, dataLoan.sWindInsPaidByT, "InsPaidByT");
            Assert.AreEqual(amt, dataLoan.sWindInsCoverageAmt, "InsCoverageAmt");
            Assert.AreEqual(amt, dataLoan.sWindInsDeductibleAmt, "InsDeductibleAmt");
            Assert.AreEqual(dateStr, dataLoan.sWindInsPaymentDueD_rep, "InsPolicyPaymentDueD");
            Assert.AreEqual(dateStr, dataLoan.sWindInsPaymentMadeD_rep, "InsPolicyPaymentMadeD");
            Assert.AreEqual(dateStr, dataLoan.sWindInsPaidThroughD_rep, "InsPolicyPaidThroughD");
        }

        [Test]
        public void TestWindInsPaymentDueAmt1008()
        {
            CPageData dataLoan = CreatePageData();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            dataLoan.sWindInsSettlementChargeT = E_sInsSettlementChargeT.Line1008;
            dataLoan.s1006ProHExp = 100M;

            int[,] billingPeriods = dataLoan.sInitialEscrowAcc;
            billingPeriods[1, (int)E_sBillingPeriodT.UserDefined1] = 4;
            dataLoan.sInitialEscrowAcc = billingPeriods;

            dataLoan.Save();

            dataLoan = CreatePageData();
            dataLoan.InitLoad();

            Assert.AreEqual(4, dataLoan.sInitialEscrowAcc[1, (int)E_sBillingPeriodT.UserDefined1], "BillingPeriod");
            Assert.AreEqual(100M, dataLoan.s1006ProHExp, "s1006ProHExp");
            decimal amtDue = 100 * 4;

            Assert.AreEqual(amtDue, dataLoan.sWindInsPaymentDueAmt, "PaymentDueAmt");
        }

        [Test]
        public void TestWindInsPaymentDueAmt1009()
        {
            CPageData dataLoan = CreatePageData();
            dataLoan.sAggEscrowCalcModeT = E_AggregateEscrowCalculationModeT.Legacy;
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            dataLoan.sWindInsSettlementChargeT = E_sInsSettlementChargeT.Line1009;
            dataLoan.s1007ProHExp = 100M;


            int[,] billingPeriods = dataLoan.sInitialEscrowAcc;
            billingPeriods[2, (int)E_sBillingPeriodT.UserDefined2] = 5;
            dataLoan.sInitialEscrowAcc = billingPeriods;

            dataLoan.Save();

            dataLoan = CreatePageData();
            dataLoan.InitLoad();

            Assert.AreEqual(5, dataLoan.sInitialEscrowAcc[2, (int)E_sBillingPeriodT.UserDefined2], "BillingPeriod");
            Assert.AreEqual(100M, dataLoan.s1007ProHExp, "s1007ProHExp");
            decimal amtDue = 100 * 5;

            Assert.AreEqual(amtDue, dataLoan.sWindInsPaymentDueAmt, "PaymentDueAmt");
        }

        [Test]
        public void TestCondoHO6InsurancePolicyFields()
        {
            CPageData dataLoan = CreatePageData();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            string dateStr = "12/20/2012";
            decimal amt = 1000.00M;
            dataLoan.sCondoHO6InsCompanyNm = "CN";
            dataLoan.sCondoHO6InsPolicyNum = "ABC123";
            dataLoan.sCondoHO6InsPolicyActivationD_rep = dateStr;
            dataLoan.sCondoHO6InsPaidByT = E_sInsPaidByT.Lender;
            dataLoan.sCondoHO6InsCoverageAmt = amt;
            dataLoan.sCondoHO6InsDeductibleAmt = amt;
            dataLoan.sCondoHO6InsPaymentDueD_rep = dateStr;
            dataLoan.sCondoHO6InsPaymentMadeD_rep = dateStr;
            dataLoan.sCondoHO6InsPaidThroughD_rep = dateStr;

            dataLoan.Save();

            dataLoan = CreatePageData();
            dataLoan.InitLoad();

            Assert.AreEqual("CN", dataLoan.sCondoHO6InsCompanyNm, "InsCompanyNm");
            Assert.AreEqual("ABC123", dataLoan.sCondoHO6InsPolicyNum, "InsPolicyNum");
            Assert.AreEqual(dateStr, dataLoan.sCondoHO6InsPolicyActivationD_rep, "InsPolicyActivationD");
            Assert.AreEqual(E_sInsPaidByT.Lender, dataLoan.sCondoHO6InsPaidByT, "InsPaidByT");
            Assert.AreEqual(amt, dataLoan.sCondoHO6InsCoverageAmt, "InsCoverageAmt");
            Assert.AreEqual(amt, dataLoan.sCondoHO6InsDeductibleAmt, "InsDeductibleAmt");
            Assert.AreEqual(dateStr, dataLoan.sCondoHO6InsPaymentDueD_rep, "InsPolicyPaymentDueD");
            Assert.AreEqual(dateStr, dataLoan.sCondoHO6InsPaymentMadeD_rep, "InsPolicyPaymentMadeD");
            Assert.AreEqual(dateStr, dataLoan.sCondoHO6InsPaidThroughD_rep, "InsPolicyPaidThroughD");
        }

        [Test]
        public void TestCondoHO6InsPaymentDueAmt1008()
        {
            CPageData dataLoan = CreatePageData();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            dataLoan.sCondoHO6InsSettlementChargeT = E_sInsSettlementChargeT.Line1008;
            dataLoan.s1006ProHExp = 100M;

            int[,] billingPeriods = dataLoan.sInitialEscrowAcc;
            billingPeriods[1, (int)E_sBillingPeriodT.UserDefined1] = 4;
            dataLoan.sInitialEscrowAcc = billingPeriods;

            dataLoan.Save();

            dataLoan = CreatePageData();
            dataLoan.InitLoad();

            Assert.AreEqual(4, dataLoan.sInitialEscrowAcc[1, (int)E_sBillingPeriodT.UserDefined1], "BillingPeriod");
            Assert.AreEqual(100M, dataLoan.s1006ProHExp, "s1006ProHExp");
            decimal amtDue = 100 * 4;

            Assert.AreEqual(amtDue, dataLoan.sCondoHO6InsPaymentDueAmt, "PaymentDueAmt");
        }

        [Test]
        public void TestCondoHO6InsPaymentDueAmt1009()
        {
            CPageData dataLoan = CreatePageData();
            dataLoan.sAggEscrowCalcModeT = E_AggregateEscrowCalculationModeT.Legacy;
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            dataLoan.sCondoHO6InsSettlementChargeT = E_sInsSettlementChargeT.Line1009;
            dataLoan.s1007ProHExp = 100M;

            int[,] billingPeriods = dataLoan.sInitialEscrowAcc;
            billingPeriods[2, (int)E_sBillingPeriodT.UserDefined2] = 5;
            dataLoan.sInitialEscrowAcc = billingPeriods;

            dataLoan.Save();

            dataLoan = CreatePageData();
            dataLoan.InitLoad();

            Assert.AreEqual(5, dataLoan.sInitialEscrowAcc[2, (int)E_sBillingPeriodT.UserDefined2], "BillingPeriod");
            Assert.AreEqual(100M, dataLoan.s1007ProHExp, "s1007ProHExp");
            decimal amtDue = 100 * 5;

            Assert.AreEqual(amtDue, dataLoan.sCondoHO6InsPaymentDueAmt, "PaymentDueAmt");
        }

        private CPageData CreatePageData()
        {
            return FakePageData.Create();
        }
    }

}