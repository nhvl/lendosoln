﻿using System;
using System.Data.SqlClient;
using DataAccess;
using DataAccess.Core.Construction;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Security;
using LendOSolnTest.Common;
using LendOSolnTest.Fakes;
using NUnit.Framework;

namespace LendOSolnTest.Loan
{
    [TestFixture]
    public class TotalScorecardWaringMessageTest
    {
        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            LoginTools.LoginAs(TestAccountType.LoTest001);
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            FakePageDataUtilities.Instance.Reset();
        }

        private CPageData CreateDataObject(int numberOfApps = 1)
        {
            return FakePageData.Create(appCount: numberOfApps);
        }

        [SetUp]
        public void Setup()
        {
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            int nApps = dataLoan.nApps;
            for (int i = nApps - 1; i > 0; i--)
            {
                dataLoan.DelApp(i);
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            }
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            CAppData dataApp = dataLoan.GetAppData(0);
            dataApp.aLiaCollection.ClearAll();
            dataApp.aReCollection.ClearAll();
            
            dataApp.aBFirstNm = "";
            dataApp.aBLastNm = "";
            dataApp.aCFirstNm = "";
            dataApp.aCLastNm = "";
            dataLoan.Save();
        }

        [Test]
        public void SsnTotalScorecardWarningTest()
        {
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(0);

            dataApp.aBFirstNm = "aBFirstNm";
            dataApp.aBLastNm = "aBLastNm";
            dataApp.aBSsn = "";
            dataApp.aCFirstNm = "aCFirstNm";
            dataApp.aCLastNm = "aCLastNm";
            dataApp.aCSsn = "";

            Assert.AreEqual(ErrorMessages.TotalScorecardWarning_MissingSsn, dataApp.aBSsnTotalScorecardWarning, "aBSsnTotalScorecardWarning");
            Assert.AreEqual(ErrorMessages.TotalScorecardWarning_MissingSsn, dataApp.aCSsnTotalScorecardWarning, "aCSsnTotalScorecardWarning");


            dataApp.aBFirstNm = "aBFirstNm";
            dataApp.aBLastNm = "aBLastNm";
            dataApp.aBSsn = "123-44-5678";
            dataApp.aCFirstNm = "aCFirstNm";
            dataApp.aCLastNm = "";
            dataApp.aCSsn = "444-44-4444";

            Assert.AreEqual(string.Empty, dataApp.aBSsnTotalScorecardWarning, "aBSsnTotalScorecardWarning");
            Assert.AreEqual(string.Empty, dataApp.aCSsnTotalScorecardWarning, "aCSsnTotalScorecardWarning");

            dataApp.aBFirstNm = "";
            dataApp.aBLastNm = "";
            dataApp.aBSsn = "";
            dataApp.aCFirstNm = "";
            dataApp.aCLastNm = "";
            dataApp.aCSsn = "";

            Assert.AreEqual(string.Empty, dataApp.aBSsnTotalScorecardWarning, "aBSsnTotalScorecardWarning");
            Assert.AreEqual(string.Empty, dataApp.aCSsnTotalScorecardWarning, "aCSsnTotalScorecardWarning");
        }

        [Test]
        public void DecResidencyTotalScorecardWarningTest()
        {
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(0);

            dataApp.aBFirstNm = "aBFirstNm";
            dataApp.aBLastNm = "aBLastNm";
            dataApp.aBDecResidency = "";
            dataApp.aCFirstNm = "aCFirstNm";
            dataApp.aCLastNm = "aCLastNm";
            dataApp.aCDecResidency = "";

            Assert.AreEqual(ErrorMessages.TotalScorecardWarning_MissingCitizenshipStatus, dataApp.aBDecResidencyTotalScorecardWarning, "aBDecResidencyTotalScorecardWarning");
            Assert.AreEqual(ErrorMessages.TotalScorecardWarning_MissingCitizenshipStatus, dataApp.aCDecResidencyTotalScorecardWarning, "aCDecResidencyTotalScorecardWarning");

            dataApp.aBFirstNm = "aBFirstNm";
            dataApp.aBLastNm = "aBLastNm";
            dataApp.aBDecResidency = "Y";
            dataApp.aCFirstNm = "aCFirstNm";
            dataApp.aCLastNm = "aCLastNm";
            dataApp.aCDecResidency = "Y";

            Assert.AreEqual(ErrorMessages.TotalScorecardWarning_MissingCitizenshipStatus, dataApp.aBDecResidencyTotalScorecardWarning, "aBDecResidencyTotalScorecardWarning");
            Assert.AreEqual(ErrorMessages.TotalScorecardWarning_MissingCitizenshipStatus, dataApp.aCDecResidencyTotalScorecardWarning, "aCDecResidencyTotalScorecardWarning");

            dataApp.aBFirstNm = "aBFirstNm";
            dataApp.aBLastNm = "aBLastNm";
            dataApp.aBDecResidency = "Y";
            dataApp.aBDecCitizen = "Y";
            dataApp.aCFirstNm = "aCFirstNm";
            dataApp.aCLastNm = "aCLastNm";
            dataApp.aCDecResidency = "Y";
            dataApp.aCDecCitizen = "Y";

            Assert.AreEqual(string.Empty, dataApp.aBDecResidencyTotalScorecardWarning, "aBDecResidencyTotalScorecardWarning");
            Assert.AreEqual(string.Empty, dataApp.aCDecResidencyTotalScorecardWarning, "aCDecResidencyTotalScorecardWarning");

            dataApp.aBFirstNm = "";
            dataApp.aBLastNm = "";
            dataApp.aBDecResidency = "";
            dataApp.aCFirstNm = "";
            dataApp.aCLastNm = "";
            dataApp.aCDecResidency = "";

            Assert.AreEqual(string.Empty, dataApp.aBDecResidencyTotalScorecardWarning, "aBDecResidencyTotalScorecardWarning");
            Assert.AreEqual(string.Empty, dataApp.aCDecResidencyTotalScorecardWarning, "aCDecResidencyTotalScorecardWarning");

        }

        [Test]
        public void DecOccTotalScorecardWarningTest()
        {
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(0);

            dataApp.aBFirstNm = "aBFirstNm";
            dataApp.aBLastNm = "aBLastNm";
            dataApp.aBDecOcc = "";
            dataApp.aCFirstNm = "aCFirstNm";
            dataApp.aCLastNm = "aCLastNm";
            dataApp.aCDecOcc = "";

            Assert.AreEqual(ErrorMessages.TotalScorecardWarning_MissingIntentToOccupy, dataApp.aBDecOccTotalScorecardWarning, "aBDecOccTotalScorecardWarning");
            Assert.AreEqual(ErrorMessages.TotalScorecardWarning_MissingIntentToOccupy, dataApp.aCDecOccTotalScorecardWarning, "aCDecOccTotalScorecardWarning");

            dataApp.aBFirstNm = "aBFirstNm";
            dataApp.aBLastNm = "aBLastNm";
            dataApp.aBDecOcc = "Y";
            dataApp.aCFirstNm = "aCFirstNm";
            dataApp.aCLastNm = "aCLastNm";
            dataApp.aCDecOcc = "Y";

            Assert.AreEqual(string.Empty, dataApp.aBDecOccTotalScorecardWarning, "aBDecOccTotalScorecardWarning");
            Assert.AreEqual(string.Empty, dataApp.aCDecOccTotalScorecardWarning, "aCDecOccTotalScorecardWarning");

            dataApp.aBFirstNm = "";
            dataApp.aBLastNm = "";
            dataApp.aBDecOcc = "";
            dataApp.aCFirstNm = "";
            dataApp.aCLastNm = "";
            dataApp.aCDecOcc = "";

            Assert.AreEqual(string.Empty, dataApp.aBDecOccTotalScorecardWarning, "aBDecOccTotalScorecardWarning");
            Assert.AreEqual(string.Empty, dataApp.aCDecOccTotalScorecardWarning, "aCDecOccTotalScorecardWarning");

        }

        [Test]
        public void sLTotITotalScorecardWarningTest()
        {
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(0);
            dataApp.aBBaseI_rep = "";
            dataApp.aCBaseI_rep = "";
            Assert.AreEqual(ErrorMessages.TotalScorecardWarning_IncomeGreaterThanZero, dataLoan.sLTotITotalScorecardWarning);

            dataApp.aBBaseI_rep = "$1000.00";
            Assert.AreEqual(string.Empty, dataLoan.sLTotITotalScorecardWarning);
            Assert.AreEqual(true, dataLoan.sHasTotalScorecardWarning, "sHasTotalScorecardWarning");

        }

        [Test]
        public void sPurchPriceTotalScorecardWarningTest()
        {
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitLoad();
            

            var testcases = new[] {
                new { sLPurposeT = E_sLPurposeT.RefinCashout, sPurchPrice = "", sConstructionPurposeT = ConstructionPurpose.Blank, ExpectedMsg = string.Empty},
                new { sLPurposeT = E_sLPurposeT.RefinCashout, sPurchPrice = "1000", sConstructionPurposeT = ConstructionPurpose.Blank, ExpectedMsg = string.Empty},

                new { sLPurposeT = E_sLPurposeT.Purchase, sPurchPrice = "", sConstructionPurposeT = ConstructionPurpose.Blank, ExpectedMsg = ErrorMessages.TotalScorecardWarning_PurchasePrice},
                new { sLPurposeT = E_sLPurposeT.Purchase, sPurchPrice = "$0.00", sConstructionPurposeT = ConstructionPurpose.Blank, ExpectedMsg = ErrorMessages.TotalScorecardWarning_PurchasePrice},
                new { sLPurposeT = E_sLPurposeT.Purchase, sPurchPrice = "$1000", sConstructionPurposeT = ConstructionPurpose.Blank, ExpectedMsg = string.Empty},

                new { sLPurposeT = E_sLPurposeT.Construct, sPurchPrice = "", sConstructionPurposeT = ConstructionPurpose.ConstructionAndLotPurchase, ExpectedMsg = ErrorMessages.TotalScorecardWarning_PurchasePrice},
                new { sLPurposeT = E_sLPurposeT.Construct, sPurchPrice = "$1000", sConstructionPurposeT = ConstructionPurpose.ConstructionAndLotPurchase, ExpectedMsg = string.Empty},
                new { sLPurposeT = E_sLPurposeT.ConstructPerm, sPurchPrice = "", sConstructionPurposeT = ConstructionPurpose.ConstructionAndLotPurchase, ExpectedMsg = ErrorMessages.TotalScorecardWarning_PurchasePrice},
                new { sLPurposeT = E_sLPurposeT.ConstructPerm, sPurchPrice = "$1000", sConstructionPurposeT = ConstructionPurpose.ConstructionAndLotPurchase, ExpectedMsg = string.Empty},

                new { sLPurposeT = E_sLPurposeT.Construct, sPurchPrice = "", sConstructionPurposeT = ConstructionPurpose.ConstructionAndLotRefinance, ExpectedMsg = string.Empty},
                new { sLPurposeT = E_sLPurposeT.Construct, sPurchPrice = "$1000", sConstructionPurposeT = ConstructionPurpose.ConstructionAndLotRefinance, ExpectedMsg = string.Empty},
                new { sLPurposeT = E_sLPurposeT.ConstructPerm, sPurchPrice = "", sConstructionPurposeT = ConstructionPurpose.ConstructionAndLotRefinance, ExpectedMsg = string.Empty},
                new { sLPurposeT = E_sLPurposeT.ConstructPerm, sPurchPrice = "$1000", sConstructionPurposeT = ConstructionPurpose.ConstructionAndLotRefinance, ExpectedMsg = string.Empty},

            };

            foreach (var testcase in testcases)
            {
                dataLoan.sConstructionPurposeT = testcase.sConstructionPurposeT;
                dataLoan.sLPurposeT = testcase.sLPurposeT;

                if (testcase.sConstructionPurposeT == ConstructionPurpose.ConstructionAndLotPurchase)
                {
                    dataLoan.sPurchPrice_rep = "0";
                    dataLoan.sLotImprovC_rep = testcase.sPurchPrice;
                }
                else
                {
                    dataLoan.sPurchPrice_rep = testcase.sPurchPrice;
                    dataLoan.sLotImprovC_rep = "0";
                }
                Assert.AreEqual(testcase.ExpectedMsg, dataLoan.sPurchPriceTotalScorecardWarning, testcase.ToString());
                Assert.AreEqual(true, dataLoan.sHasTotalScorecardWarning, "sHasTotalScorecardWarning");
            }
        }

        [Test]
        public void sApprValTotalScorecardWarningTest()
        {
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitLoad();

            var testcases = new[] {
                new { sLPurposeT = E_sLPurposeT.RefinCashout, sApprVal = "", sConstructionPurposeT = ConstructionPurpose.Blank, ExpectedMsg = ErrorMessages.TotalScorecardWarning_AppraisedValue},
                new { sLPurposeT = E_sLPurposeT.RefinCashout, sApprVal = "$0.00", sConstructionPurposeT = ConstructionPurpose.Blank, ExpectedMsg = ErrorMessages.TotalScorecardWarning_AppraisedValue},
                new { sLPurposeT = E_sLPurposeT.RefinCashout, sApprVal = "1000", sConstructionPurposeT = ConstructionPurpose.Blank, ExpectedMsg = string.Empty},

                new { sLPurposeT = E_sLPurposeT.Refin, sApprVal = "", sConstructionPurposeT = ConstructionPurpose.Blank, ExpectedMsg = ErrorMessages.TotalScorecardWarning_AppraisedValue},
                new { sLPurposeT = E_sLPurposeT.Refin, sApprVal = "$0.00", sConstructionPurposeT = ConstructionPurpose.Blank, ExpectedMsg = ErrorMessages.TotalScorecardWarning_AppraisedValue},
                new { sLPurposeT = E_sLPurposeT.Refin, sApprVal = "1000", sConstructionPurposeT = ConstructionPurpose.Blank, ExpectedMsg = string.Empty},

                new { sLPurposeT = E_sLPurposeT.FhaStreamlinedRefinance, sApprVal = "", sConstructionPurposeT = ConstructionPurpose.Blank, ExpectedMsg = ErrorMessages.TotalScorecardWarning_AppraisedValue},
                new { sLPurposeT = E_sLPurposeT.FhaStreamlinedRefinance, sApprVal = "$0.00", sConstructionPurposeT = ConstructionPurpose.Blank, ExpectedMsg = ErrorMessages.TotalScorecardWarning_AppraisedValue},
                new { sLPurposeT = E_sLPurposeT.FhaStreamlinedRefinance, sApprVal = "1000", sConstructionPurposeT = ConstructionPurpose.Blank, ExpectedMsg = string.Empty},

                new { sLPurposeT = E_sLPurposeT.Purchase, sApprVal = "", sConstructionPurposeT = ConstructionPurpose.Blank, ExpectedMsg = string.Empty},
                new { sLPurposeT = E_sLPurposeT.Purchase, sApprVal = "$1000", sConstructionPurposeT = ConstructionPurpose.Blank, ExpectedMsg = string.Empty},

                new { sLPurposeT = E_sLPurposeT.Construct, sApprVal = "", sConstructionPurposeT = ConstructionPurpose.ConstructionAndLotPurchase, ExpectedMsg = string.Empty},
                new { sLPurposeT = E_sLPurposeT.Construct, sApprVal = "$1000", sConstructionPurposeT = ConstructionPurpose.ConstructionAndLotPurchase, ExpectedMsg = string.Empty},
                new { sLPurposeT = E_sLPurposeT.ConstructPerm, sApprVal = "", sConstructionPurposeT = ConstructionPurpose.ConstructionAndLotPurchase, ExpectedMsg = string.Empty},
                new { sLPurposeT = E_sLPurposeT.ConstructPerm, sApprVal = "$1000", sConstructionPurposeT = ConstructionPurpose.ConstructionAndLotPurchase, ExpectedMsg = string.Empty},

                new { sLPurposeT = E_sLPurposeT.Construct, sApprVal = "", sConstructionPurposeT = ConstructionPurpose.ConstructionAndLotRefinance, ExpectedMsg = ErrorMessages.TotalScorecardWarning_AppraisedValue},
                new { sLPurposeT = E_sLPurposeT.Construct, sApprVal = "$1000", sConstructionPurposeT = ConstructionPurpose.ConstructionAndLotRefinance, ExpectedMsg = string.Empty},
                new { sLPurposeT = E_sLPurposeT.ConstructPerm, sApprVal = "", sConstructionPurposeT = ConstructionPurpose.ConstructionAndLotRefinance, ExpectedMsg = ErrorMessages.TotalScorecardWarning_AppraisedValue},
                new { sLPurposeT = E_sLPurposeT.ConstructPerm, sApprVal = "$1000", sConstructionPurposeT = ConstructionPurpose.ConstructionAndLotRefinance, ExpectedMsg = string.Empty},
            };

            foreach (var testcase in testcases)
            {
                dataLoan.sLPurposeT = testcase.sLPurposeT;
                dataLoan.sApprVal_rep = testcase.sApprVal;
                dataLoan.sConstructionPurposeT = testcase.sConstructionPurposeT;
                Assert.AreEqual(testcase.ExpectedMsg, dataLoan.sApprValTotalScorecardWarning, testcase.ToString());
                Assert.AreEqual(true, dataLoan.sHasTotalScorecardWarning, "sHasTotalScorecardWarning");
            }
        }

        [Test]
        public void sLAmtCalcTotalScorecardWarningTest()
        {
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitLoad();

            dataLoan.sLAmtLckd = true;
            dataLoan.sLAmtCalc_rep = "";

            Assert.AreEqual(ErrorMessages.TotalScorecardWarning_LoanAmountNegative, dataLoan.sLAmtCalcTotalScorecardWarning);

            dataLoan.sLAmtLckd = true;
            dataLoan.sLAmtCalc_rep = "($1000.00)";

            Assert.AreEqual(ErrorMessages.TotalScorecardWarning_LoanAmountNegative, dataLoan.sLAmtCalcTotalScorecardWarning);

            dataLoan.sLAmtLckd = true;
            dataLoan.sLAmtCalc_rep = "$1000.00";

            Assert.AreEqual(string.Empty, dataLoan.sLAmtCalcTotalScorecardWarning);

        }

        [Test]
        public void sFfUfmip1003TotalScorecardWarningTest()
        {
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitLoad();

            dataLoan.sFfUfmip1003Lckd = true;
            dataLoan.sFfUfmip1003_rep = "";
            Assert.AreEqual(string.Empty, dataLoan.sFfUfmip1003TotalScorecardWarning);

            dataLoan.sFfUfmip1003Lckd = true;
            dataLoan.sFfUfmip1003_rep = "1000";
            Assert.AreEqual(string.Empty, dataLoan.sFfUfmip1003TotalScorecardWarning);

            //dataLoan.sFfUfmip1003Lckd = true;  // opm 140627, no longer allow setting negative sFfUfmip1003
            //dataLoan.sFfUfmip1003_rep = "($1000)";
            //Assert.AreEqual(ErrorMessages.TotalScorecardWarning_UpfrontMip, dataLoan.sFfUfmip1003TotalScorecardWarning);

        }

        [Test]
        public void EmplrJobTitleTotalScorecardWarningTest()
        {
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(0);
            dataApp.aBFirstNm = "aBFirstNm";
            IPrimaryEmploymentRecord primary = dataApp.aBEmpCollection.GetPrimaryEmp(true);

            primary.JobTitle = "";
            dataApp.aBBaseI_rep = "";
            primary.Update();

            Assert.AreEqual(string.Empty, dataApp.aBEmplrJobTitleTotalScorecardWarning);

            primary.JobTitle = "";
            dataApp.aBBaseI_rep = "$4000";
            primary.Update();

            Assert.AreEqual(ErrorMessages.TotalScorecardWarning_MissingJobTitle, dataApp.aBEmplrJobTitleTotalScorecardWarning);

            primary.JobTitle = "Hello";
            dataApp.aBBaseI_rep = "$4000";
            primary.Update();

            Assert.AreEqual(string.Empty, dataApp.aBEmplrJobTitleTotalScorecardWarning);


            primary = dataApp.aCEmpCollection.GetPrimaryEmp(true);
            dataApp.aCFirstNm = "aCFirstNm";
            primary.JobTitle = "";
            dataApp.aCBaseI_rep = "";
            primary.Update();

            Assert.AreEqual(string.Empty, dataApp.aCEmplrJobTitleTotalScorecardWarning);

            primary.JobTitle = "";
            dataApp.aCBaseI_rep = "$4000";
            primary.Update();

            Assert.AreEqual(ErrorMessages.TotalScorecardWarning_MissingJobTitle, dataApp.aCEmplrJobTitleTotalScorecardWarning);

            primary.JobTitle = "Hello";
            dataApp.aCBaseI_rep = "$4000";

            Assert.AreEqual(string.Empty, dataApp.aCEmplrJobTitleTotalScorecardWarning);

        }

        [Test]
        public void sBorrCountTotalScorecardWarningTest_3SetsOfBorrower()
        {
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            Assert.AreEqual(ErrorMessages.TotalScorecardWarning_NoBorrowerCount, dataLoan.sBorrCountTotalScorecardWarning);
            dataLoan.Save();

            // Add 2 more sets of borrower.
            for (int i = 0; i < 2; i++)
            {
                dataLoan.AddNewApp();
            }
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            CAppData dataApp = dataLoan.GetAppData(0);
            dataApp.aBFirstNm = "aBFirstNm";
            dataApp.aCFirstNm = "aCFirstNm";
            dataApp.aBSsn = "111-11-1111";

            dataApp = dataLoan.GetAppData(1);
            dataApp.aBFirstNm = "aBFirstNm";
            dataApp.aCFirstNm = "aCFirstNm";

            dataLoan.Save();

            dataLoan = CreateDataObject(numberOfApps: 3);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            Assert.AreEqual(string.Empty, dataLoan.sBorrCountTotalScorecardWarning);

            dataApp = dataLoan.GetAppData(2);
            dataApp.aBFirstNm = "aBFirstNm";

            dataLoan.Save();

            dataLoan = CreateDataObject(numberOfApps: 3);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataApp = dataLoan.GetAppData(2);
            dataApp.aCFirstNm = "aCFirstNm";
            Assert.AreEqual(ErrorMessages.TotalScorecardWarning_ExceedBorrowerCount, dataLoan.sBorrCountTotalScorecardWarning);

        }
        [Test]
        public void sBorrCountTotalScorecardWarningTest_5IndividualBorrowers()
        {
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            Assert.AreEqual(ErrorMessages.TotalScorecardWarning_NoBorrowerCount, dataLoan.sBorrCountTotalScorecardWarning);
            dataLoan.Save();

            for (int i = 0; i < 4; i++)
            {
                dataLoan.AddNewApp();
            }
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            CAppData dataApp = null;

            for (int i = 0; i < 5; i++)
            {
                dataApp = dataLoan.GetAppData(i);
                dataApp.aBFirstNm = "aBFirstNm";
                dataApp.aBSsn = "111-11-1111";
                dataApp.aCFirstNm = "";
            }
            dataLoan.Save();

            dataLoan = CreateDataObject(numberOfApps: 5);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            Assert.AreEqual(string.Empty, dataLoan.sBorrCountTotalScorecardWarning);
            dataLoan.Save();

            int appIndex = dataLoan.AddNewApp();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataApp = dataLoan.GetAppData(appIndex);
            dataApp.aBFirstNm = "aBFirstNm";

            Assert.AreEqual(ErrorMessages.TotalScorecardWarning_ExceedBorrowerCount, dataLoan.sBorrCountTotalScorecardWarning);
        }


        [Test]
        public void sHasTotalScorecardWarningTest()
        {
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitLoad();
            dataLoan.sLPurposeT = E_sLPurposeT.Purchase;
            dataLoan.sPurchPrice_rep = "$1,000.00";
            dataLoan.sLAmtLckd = true;
            dataLoan.sLAmtCalc_rep = "$40000.00";

            CAppData dataApp = dataLoan.GetAppData(0);
            dataApp.aBFirstNm = "aBFirstNm";
            dataApp.aBSsn = "111-11-1111";
            dataApp.aBDecResidency = "Y";
            dataApp.aBDecCitizen = "Y";
            dataApp.aBDecOcc = "Y";
            dataApp.aBBaseI_rep = "$4,000";

            // Add Credit Report so dataApp.CreditReportView won't be null
            Guid dbFileKey = Guid.NewGuid();
            string path = TestDataRootFolder.Location + "SampleCreditReports/TestCredit01.xml.config";
            FileDBTools.WriteFile(E_FileDB.Normal, dbFileKey.ToString(), path);

            SqlParameter[] parameters = new SqlParameter[] {
                                            new SqlParameter("@Owner", dataApp.aAppId),
                                            new SqlParameter("@ExternalFileId", "TEST"),
                                            new SqlParameter("@DbFileKey", dbFileKey),
                                            new SqlParameter("@ComId", new Guid("D0CDB7A2-9040-4CCF-8D72-F67CAB3252E8")), // Only work on local DB only.
                                            new SqlParameter("@UserID", BrokerUserPrincipal.CurrentPrincipal.UserId),
                                            new SqlParameter("@CrAccProxyId", DBNull.Value),
                                                               new SqlParameter("@HowDidItGetHere", "Test")

                                        };
            StoredProcedureHelper.ExecuteNonQuery(BrokerUserPrincipal.CurrentPrincipal.BrokerId, "InsertCreditReportFile", 0, parameters);

            IEmpCollection empCollection = dataApp.aBEmpCollection;
            IPrimaryEmploymentRecord record = empCollection.GetPrimaryEmp(true);
            record.JobTitle = "JOBTITLE";
            record.Update();
            Assert.AreEqual(false, dataLoan.sHasTotalScorecardWarning);
        }
    }
}
