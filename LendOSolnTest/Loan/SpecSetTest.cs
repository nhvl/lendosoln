﻿namespace LendOSolnTest.Loan
{
    using System.Linq;
    using DataAccess;
    using NUnit.Framework;

    [TestFixture]
    public class SpecSetTest
    {
        [Test]
        public void SpecSet_DoesNotContainValue_ReturnsFalse()
        {
            var openedDateFieldSpec = FieldSpec.Create(nameof(CPageData.sOpenedD));
            var loanTypeFieldSpec = FieldSpec.Create(nameof(CPageData.sLT));

            var specSet = new SpecSet();

            Assert.IsFalse(specSet.Has(loanTypeFieldSpec, useOptimization: false));
            Assert.IsFalse(specSet.Has(loanTypeFieldSpec, useOptimization: true));

            specSet.Add(loanTypeFieldSpec);

            Assert.IsFalse(specSet.Has(openedDateFieldSpec, useOptimization: false));
            Assert.IsFalse(specSet.Has(openedDateFieldSpec, useOptimization: true));
        }

        [Test]
        public void SpecSet_ContainsValue_ReturnsTrue()
        {
            var fieldSpec = FieldSpec.Create(nameof(CPageData.sLT));

            var specSet = new SpecSet();
            specSet.Add(fieldSpec);

            Assert.IsTrue(specSet.Has(fieldSpec, useOptimization: false));
            Assert.IsTrue(specSet.Has(fieldSpec, useOptimization: true));
        }

        [Test]
        public void SpecSet_RemoveContainedValue_ReturnsFalse()
        {
            var fieldSpec = FieldSpec.Create(nameof(CPageData.sLT));

            var specSet = new SpecSet();
            specSet.Add(fieldSpec);

            Assert.IsTrue(specSet.Has(fieldSpec, useOptimization: false));
            Assert.IsTrue(specSet.Has(fieldSpec, useOptimization: true));

            specSet.RemoveAt(0);

            Assert.IsFalse(specSet.Has(fieldSpec, useOptimization: false));
            Assert.IsFalse(specSet.Has(fieldSpec, useOptimization: true));
        }

        [Test]
        public void SpecSetMultipleValues_ContainsValue_ReturnsTrue()
        {
            var fieldSpecs = new[]
            {
                nameof(CPageData.sLT),
                nameof(CPageData.sLRefNm),
                nameof(CPageData.sLNm),
                nameof(CPageData.sRateLockStatusT),
                nameof(CPageData.sRateMergeGroupName),
                nameof(CPageData.sQM1006Rsrv),
                nameof(CPageData.sHmda2018AprRateSpread),
                nameof(CPageData.sCustomField10D),
                nameof(CPageData.sOpenedD),
                nameof(CPageData.sLT),
            }.Select(spec => FieldSpec.Create(spec));

            var specSet = new SpecSet();
            foreach (var fieldSpec in fieldSpecs)
            {
                specSet.Add(fieldSpec);
            }

            foreach (var fieldSpec in fieldSpecs)
            {
                Assert.IsTrue(specSet.Has(fieldSpec, useOptimization: false));
                Assert.IsTrue(specSet.Has(fieldSpec, useOptimization: true));
            }
        }

        [Test]
        public void SpecSetMultipleValues_DoesNotContainsValue_ReturnsFalse()
        {
            var fieldSpecs = new[]
            {
                nameof(CPageData.sLT),
                nameof(CPageData.sLRefNm),
                nameof(CPageData.sLNm),
                nameof(CPageData.sRateLockStatusT),
                nameof(CPageData.sRateMergeGroupName),
                nameof(CPageData.sQM1006Rsrv),
                nameof(CPageData.sHmda2018AprRateSpread),
                nameof(CPageData.sCustomField10D),
                nameof(CPageData.sOpenedD),
                nameof(CPageData.sLT),
            }.Select(spec => FieldSpec.Create(spec));

            var specSet = new SpecSet();
            foreach (var fieldSpec in fieldSpecs)
            {
                specSet.Add(fieldSpec);
            }

            var docsBackDateFieldSpec = FieldSpec.Create(nameof(CPageData.sDocsBackD));

            Assert.IsFalse(specSet.Has(docsBackDateFieldSpec, useOptimization: false));
            Assert.IsFalse(specSet.Has(docsBackDateFieldSpec, useOptimization: true));
        }

        [Test]
        public void SpecSetMultipleValues_RemoveValue_ReturnsFalse()
        {
            var fieldSpecs = new[]
            {
                nameof(CPageData.sLT),
                nameof(CPageData.sLRefNm),
                nameof(CPageData.sLNm),
                nameof(CPageData.sRateLockStatusT),
                nameof(CPageData.sRateMergeGroupName),
                nameof(CPageData.sQM1006Rsrv),
                nameof(CPageData.sHmda2018AprRateSpread),
                nameof(CPageData.sCustomField10D),
                nameof(CPageData.sOpenedD),
                nameof(CPageData.sLT),
            }.Select(spec => FieldSpec.Create(spec));

            var specSet = new SpecSet();
            foreach (var fieldSpec in fieldSpecs)
            {
                specSet.Add(fieldSpec);
            }

            specSet.RemoveAt(2/*sLNm*/);

            Assert.IsFalse(specSet.Has(FieldSpec.Create(nameof(CPageData.sLNm)), useOptimization: false));
            Assert.IsFalse(specSet.Has(FieldSpec.Create(nameof(CPageData.sLNm)), useOptimization: true));
        }

        [Test]
        public void UpdateGraph_SpecSets_ReturnSameResultsWithOptimization()
        {
            var initializationParameters = Tools.GetFieldInfoInitializationParametersFromTypeInfo(usePredefinedObsoletePropertyOptimization: false);

            var updateGraph = new UpdateGraph();
            updateGraph.InitializeGraph(initializationParameters.DependsOnAttributes, useSpecSetOptimization: false);

            foreach (var key in updateGraph.Keys)
            {
                var node = updateGraph[key];

                foreach (var spec in node.DependsOn.GetList())
                {
                    Assert.IsTrue(node.DependsOn.Has(spec, useOptimization: true));
                }

                foreach (var spec in node.Affecting.GetList())
                {
                    Assert.IsTrue(node.Affecting.Has(spec, useOptimization: true));
                }
            }
        }
    }
}
