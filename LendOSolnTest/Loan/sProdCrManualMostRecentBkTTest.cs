﻿using DataAccess;
using LendersOffice.CreditReport;
using LendOSolnTest.Common;
using LendOSolnTest.Fakes;
using NUnit.Framework;

namespace LendOSolnTest.Loan
{
    [TestFixture]
    public class sProdCrManualMostRecentBkTTest
    {
        [TestFixtureSetUp]
        public void Setup()
        {
            LoginTools.LoginAs(TestAccountType.LoTest001);
        }

        [TestFixtureTearDown]
        public void TearDown()
        {
            FakePageDataUtilities.Instance.Reset();
        }

        private CPageData CreateDataLoan()
        {
            return FakePageData.Create();
        }

        [Test]
        public void Test_sProdCrManualMostRecentBkT_NoBk()
        {
            CPageData dataLoan = CreateDataLoan();
            dataLoan.InitLoad();
            dataLoan.sProdCrManualBk7Has = false;
            dataLoan.sProdCrManualBk13Has = false;

            Assert.AreEqual(E_BkType.UnknownType, dataLoan.sProdCrManualMostRecentBkT);
            Assert.AreEqual("0", dataLoan.sProdCrManualMostRecentFileMon_rep);
            Assert.AreEqual("0", dataLoan.sProdCrManualMostRecentFileYr_rep);
            Assert.AreEqual("0", dataLoan.sProdCrManualMostRecentSatisfiedMon_rep);
            Assert.AreEqual("0", dataLoan.sProdCrManualMostRecentSatisfiedYr_rep);
        }
        [Test]
        public void Test_sProdCrManualMostRecentBkT_OnlyBk7()
        {
            CPageData dataLoan = CreateDataLoan();
            dataLoan.InitLoad();
            dataLoan.sProdCrManualBk7Has = true;
            dataLoan.sProdCrManualBk7RecentFileYr_rep = "2000";
            dataLoan.sProdCrManualBk7RecentFileMon_rep = "2";
            dataLoan.sProdCrManualBk13Has = false;

            Assert.AreEqual(E_BkType.Ch7, dataLoan.sProdCrManualMostRecentBkT);
            Assert.AreEqual("2000", dataLoan.sProdCrManualMostRecentFileYr_rep);
            Assert.AreEqual("2", dataLoan.sProdCrManualMostRecentFileMon_rep);
        }
        [Test]
        public void Test_sProdCrManualMostRecentBkT_OnlyBk13()
        {
            CPageData dataLoan = CreateDataLoan();
            dataLoan.InitLoad();
            dataLoan.sProdCrManualBk7Has = false;
            dataLoan.sProdCrManualBk7RecentFileYr_rep = "2000";
            dataLoan.sProdCrManualBk7RecentFileMon_rep = "2";
            dataLoan.sProdCrManualBk13Has = true;
            dataLoan.sProdCrManualBk13RecentFileYr_rep = "2001";
            dataLoan.sProdCrManualBk13RecentFileMon_rep = "3";

            Assert.AreEqual(E_BkType.Ch13, dataLoan.sProdCrManualMostRecentBkT);
            Assert.AreEqual("3", dataLoan.sProdCrManualMostRecentFileMon_rep);
            Assert.AreEqual("2001", dataLoan.sProdCrManualMostRecentFileYr_rep);
        }
        [Test]
        public void Test_sProdCrManualMostRecentBkT_HasBothBk_OnlyBk13Satisfied()
        {
            CPageData dataLoan = CreateDataLoan();
            dataLoan.InitLoad();
            dataLoan.sProdCrManualBk7Has = true;
            dataLoan.sProdCrManualBk7RecentFileYr_rep = "2000";
            dataLoan.sProdCrManualBk7RecentFileMon_rep = "2";
            dataLoan.sProdCrManualBk7RecentStatusT = E_sProdCrManualDerogRecentStatusT.NotSatisfied;
            dataLoan.sProdCrManualBk7RecentSatisfiedYr_rep = "2004";
            dataLoan.sProdCrManualBk7RecentSatisfiedMon_rep = "2";
            dataLoan.sProdCrManualBk13Has = true;
            dataLoan.sProdCrManualBk13RecentFileYr_rep = "2001";
            dataLoan.sProdCrManualBk13RecentFileMon_rep = "3";
            dataLoan.sProdCrManualBk13RecentStatusT = E_sProdCrManualDerogRecentStatusT.Discharged;
            dataLoan.sProdCrManualBk13RecentSatisfiedYr_rep = "2005";
            dataLoan.sProdCrManualBk13RecentSatisfiedMon_rep = "3";

            Assert.AreEqual(E_BkType.Ch7, dataLoan.sProdCrManualMostRecentBkT);
            Assert.AreEqual("2", dataLoan.sProdCrManualMostRecentFileMon_rep);
            Assert.AreEqual("2000", dataLoan.sProdCrManualMostRecentFileYr_rep);
            Assert.AreEqual(E_sProdCrManualDerogRecentStatusT.NotSatisfied, dataLoan.sProdCrManualMostRecentStatusT);
        }
        [Test]
        public void Test_sProdCrManualMostRecentBkT_HasBothBk_OnlyBk7Satisfied()
        {
            CPageData dataLoan = CreateDataLoan();
            dataLoan.InitLoad();
            dataLoan.sProdCrManualBk7Has = true;
            dataLoan.sProdCrManualBk7RecentFileYr_rep = "2000";
            dataLoan.sProdCrManualBk7RecentFileMon_rep = "2";
            dataLoan.sProdCrManualBk7RecentStatusT = E_sProdCrManualDerogRecentStatusT.Dismissed;
            dataLoan.sProdCrManualBk7RecentSatisfiedYr_rep = "2004";
            dataLoan.sProdCrManualBk7RecentSatisfiedMon_rep = "2";
            dataLoan.sProdCrManualBk13Has = true;
            dataLoan.sProdCrManualBk13RecentFileYr_rep = "2001";
            dataLoan.sProdCrManualBk13RecentFileMon_rep = "3";
            dataLoan.sProdCrManualBk13RecentStatusT = E_sProdCrManualDerogRecentStatusT.NotSatisfied;
            dataLoan.sProdCrManualBk13RecentSatisfiedYr_rep = "2005";
            dataLoan.sProdCrManualBk13RecentSatisfiedMon_rep = "3";

            Assert.AreEqual(E_BkType.Ch13, dataLoan.sProdCrManualMostRecentBkT);
            Assert.AreEqual("3", dataLoan.sProdCrManualMostRecentFileMon_rep);
            Assert.AreEqual("2001", dataLoan.sProdCrManualMostRecentFileYr_rep);
            Assert.AreEqual(E_sProdCrManualDerogRecentStatusT.NotSatisfied, dataLoan.sProdCrManualMostRecentStatusT);

        }
        [Test]
        public void Test_sProdCrManualMostRecentBkT_HasBothBk_BothNotSatisfied_Bk13()
        {
            CPageData dataLoan = CreateDataLoan();
            dataLoan.InitLoad();
            dataLoan.sProdCrManualBk7Has = true;
            dataLoan.sProdCrManualBk7RecentFileYr_rep = "2000";
            dataLoan.sProdCrManualBk7RecentFileMon_rep = "2";
            dataLoan.sProdCrManualBk7RecentStatusT = E_sProdCrManualDerogRecentStatusT.NotSatisfied;
            dataLoan.sProdCrManualBk7RecentSatisfiedYr_rep = "2004";
            dataLoan.sProdCrManualBk7RecentSatisfiedMon_rep = "2";
            dataLoan.sProdCrManualBk13Has = true;
            dataLoan.sProdCrManualBk13RecentFileYr_rep = "2001";
            dataLoan.sProdCrManualBk13RecentFileMon_rep = "3";
            dataLoan.sProdCrManualBk13RecentStatusT = E_sProdCrManualDerogRecentStatusT.NotSatisfied;
            dataLoan.sProdCrManualBk13RecentSatisfiedYr_rep = "2005";
            dataLoan.sProdCrManualBk13RecentSatisfiedMon_rep = "3";

            Assert.AreEqual(E_BkType.Ch13, dataLoan.sProdCrManualMostRecentBkT);
            Assert.AreEqual("3", dataLoan.sProdCrManualMostRecentFileMon_rep);
            Assert.AreEqual("2001", dataLoan.sProdCrManualMostRecentFileYr_rep);
            Assert.AreEqual(E_sProdCrManualDerogRecentStatusT.NotSatisfied, dataLoan.sProdCrManualMostRecentStatusT);

        }
        [Test]
        public void Test_sProdCrManualMostRecentBkT_HasBothBk_BothNotSatisfied_Bk7()
        {
            CPageData dataLoan = CreateDataLoan();
            dataLoan.InitLoad();
            dataLoan.sProdCrManualBk7Has = true;
            dataLoan.sProdCrManualBk7RecentFileYr_rep = "2002";
            dataLoan.sProdCrManualBk7RecentFileMon_rep = "2";
            dataLoan.sProdCrManualBk7RecentStatusT = E_sProdCrManualDerogRecentStatusT.NotSatisfied;
            dataLoan.sProdCrManualBk7RecentSatisfiedYr_rep = "2004";
            dataLoan.sProdCrManualBk7RecentSatisfiedMon_rep = "2";
            dataLoan.sProdCrManualBk13Has = true;
            dataLoan.sProdCrManualBk13RecentFileYr_rep = "2001";
            dataLoan.sProdCrManualBk13RecentFileMon_rep = "3";
            dataLoan.sProdCrManualBk13RecentStatusT = E_sProdCrManualDerogRecentStatusT.NotSatisfied;
            dataLoan.sProdCrManualBk13RecentSatisfiedYr_rep = "2005";
            dataLoan.sProdCrManualBk13RecentSatisfiedMon_rep = "3";

            Assert.AreEqual(E_BkType.Ch7, dataLoan.sProdCrManualMostRecentBkT);
            Assert.AreEqual("2", dataLoan.sProdCrManualMostRecentFileMon_rep);
            Assert.AreEqual("2002", dataLoan.sProdCrManualMostRecentFileYr_rep);
            Assert.AreEqual(E_sProdCrManualDerogRecentStatusT.NotSatisfied, dataLoan.sProdCrManualMostRecentStatusT);

        }
        [Test]
        public void Test_sProdCrManualMostRecentBkT_HasBothBk_BothNotSatisfied_Bk7_2()
        {
            CPageData dataLoan = CreateDataLoan();
            dataLoan.InitLoad();
            dataLoan.sProdCrManualBk7Has = true;
            dataLoan.sProdCrManualBk7RecentFileYr_rep = "2001";
            dataLoan.sProdCrManualBk7RecentFileMon_rep = "3";
            dataLoan.sProdCrManualBk7RecentStatusT = E_sProdCrManualDerogRecentStatusT.NotSatisfied;
            dataLoan.sProdCrManualBk7RecentSatisfiedYr_rep = "2004";
            dataLoan.sProdCrManualBk7RecentSatisfiedMon_rep = "2";
            dataLoan.sProdCrManualBk13Has = true;
            dataLoan.sProdCrManualBk13RecentFileYr_rep = "2001";
            dataLoan.sProdCrManualBk13RecentFileMon_rep = "3";
            dataLoan.sProdCrManualBk13RecentStatusT = E_sProdCrManualDerogRecentStatusT.NotSatisfied;
            dataLoan.sProdCrManualBk13RecentSatisfiedYr_rep = "2005";
            dataLoan.sProdCrManualBk13RecentSatisfiedMon_rep = "3";

            Assert.AreEqual(E_BkType.Ch7, dataLoan.sProdCrManualMostRecentBkT);
            Assert.AreEqual("3", dataLoan.sProdCrManualMostRecentFileMon_rep);
            Assert.AreEqual("2001", dataLoan.sProdCrManualMostRecentFileYr_rep);
            Assert.AreEqual(E_sProdCrManualDerogRecentStatusT.NotSatisfied, dataLoan.sProdCrManualMostRecentStatusT);

        }


        [Test]
        public void Test_sProdCrManualMostRecentBkT_HasBothBk_BothSatisfied_Bk13()
        {
            CPageData dataLoan = CreateDataLoan();
            dataLoan.InitLoad();
            dataLoan.sProdCrManualBk7Has = true;
            dataLoan.sProdCrManualBk7RecentFileYr_rep = "2000";
            dataLoan.sProdCrManualBk7RecentFileMon_rep = "2";
            dataLoan.sProdCrManualBk7RecentStatusT = E_sProdCrManualDerogRecentStatusT.Dismissed;
            dataLoan.sProdCrManualBk7RecentSatisfiedYr_rep = "2004";
            dataLoan.sProdCrManualBk7RecentSatisfiedMon_rep = "2";
            dataLoan.sProdCrManualBk13Has = true;
            dataLoan.sProdCrManualBk13RecentFileYr_rep = "2001";
            dataLoan.sProdCrManualBk13RecentFileMon_rep = "3";
            dataLoan.sProdCrManualBk13RecentStatusT = E_sProdCrManualDerogRecentStatusT.Discharged;
            dataLoan.sProdCrManualBk13RecentSatisfiedYr_rep = "2005";
            dataLoan.sProdCrManualBk13RecentSatisfiedMon_rep = "3";

            Assert.AreEqual(E_BkType.Ch13, dataLoan.sProdCrManualMostRecentBkT);
            Assert.AreEqual("3", dataLoan.sProdCrManualMostRecentFileMon_rep);
            Assert.AreEqual("2001", dataLoan.sProdCrManualMostRecentFileYr_rep);
            Assert.AreEqual("2005", dataLoan.sProdCrManualMostRecentSatisfiedYr_rep);
            Assert.AreEqual("3", dataLoan.sProdCrManualMostRecentSatisfiedMon_rep);
            Assert.AreEqual(E_sProdCrManualDerogRecentStatusT.Discharged, dataLoan.sProdCrManualMostRecentStatusT);

        }
        [Test]
        public void Test_sProdCrManualMostRecentBkT_HasBothBk_BothSatisfied_Bk7()
        {
            CPageData dataLoan = CreateDataLoan();
            dataLoan.InitLoad();
            dataLoan.sProdCrManualBk7Has = true;
            dataLoan.sProdCrManualBk7RecentFileYr_rep = "2002";
            dataLoan.sProdCrManualBk7RecentFileMon_rep = "2";
            dataLoan.sProdCrManualBk7RecentStatusT = E_sProdCrManualDerogRecentStatusT.Discharged;
            dataLoan.sProdCrManualBk7RecentSatisfiedYr_rep = "2006";
            dataLoan.sProdCrManualBk7RecentSatisfiedMon_rep = "4";
            dataLoan.sProdCrManualBk13Has = true;
            dataLoan.sProdCrManualBk13RecentFileYr_rep = "2001";
            dataLoan.sProdCrManualBk13RecentFileMon_rep = "3";
            dataLoan.sProdCrManualBk13RecentStatusT = E_sProdCrManualDerogRecentStatusT.Discharged;
            dataLoan.sProdCrManualBk13RecentSatisfiedYr_rep = "2005";
            dataLoan.sProdCrManualBk13RecentSatisfiedMon_rep = "3";

            Assert.AreEqual(E_BkType.Ch7, dataLoan.sProdCrManualMostRecentBkT);
            Assert.AreEqual("2", dataLoan.sProdCrManualMostRecentFileMon_rep);
            Assert.AreEqual("2002", dataLoan.sProdCrManualMostRecentFileYr_rep);
            Assert.AreEqual("2006", dataLoan.sProdCrManualMostRecentSatisfiedYr_rep);
            Assert.AreEqual("4", dataLoan.sProdCrManualMostRecentSatisfiedMon_rep);
            Assert.AreEqual(E_sProdCrManualDerogRecentStatusT.Discharged, dataLoan.sProdCrManualMostRecentStatusT);

        }
        [Test]
        public void Test_sProdCrManualMostRecentBkT_HasBothBk_BothSatisfied_Bk7_2()
        {
            CPageData dataLoan = CreateDataLoan();
            dataLoan.InitLoad();
            dataLoan.sProdCrManualBk7Has = true;
            dataLoan.sProdCrManualBk7RecentFileYr_rep = "2001";
            dataLoan.sProdCrManualBk7RecentFileMon_rep = "3";
            dataLoan.sProdCrManualBk7RecentStatusT = E_sProdCrManualDerogRecentStatusT.Discharged;
            dataLoan.sProdCrManualBk7RecentSatisfiedYr_rep = "2004";
            dataLoan.sProdCrManualBk7RecentSatisfiedMon_rep = "2";
            dataLoan.sProdCrManualBk13Has = true;
            dataLoan.sProdCrManualBk13RecentFileYr_rep = "2001";
            dataLoan.sProdCrManualBk13RecentFileMon_rep = "3";
            dataLoan.sProdCrManualBk13RecentStatusT = E_sProdCrManualDerogRecentStatusT.Dismissed;
            dataLoan.sProdCrManualBk13RecentSatisfiedYr_rep = "2004";
            dataLoan.sProdCrManualBk13RecentSatisfiedMon_rep = "2";

            Assert.AreEqual(E_BkType.Ch7, dataLoan.sProdCrManualMostRecentBkT);
            Assert.AreEqual("2001", dataLoan.sProdCrManualMostRecentFileYr_rep);
            Assert.AreEqual("3", dataLoan.sProdCrManualMostRecentFileMon_rep);
            Assert.AreEqual("2004", dataLoan.sProdCrManualMostRecentSatisfiedYr_rep);
            Assert.AreEqual("2", dataLoan.sProdCrManualMostRecentSatisfiedMon_rep);

            Assert.AreEqual(E_sProdCrManualDerogRecentStatusT.Discharged, dataLoan.sProdCrManualMostRecentStatusT);

        }
    }
}
