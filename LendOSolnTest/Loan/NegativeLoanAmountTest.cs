﻿using DataAccess;
using LendersOffice.Constants;
using LendOSolnTest.Common;
using LendOSolnTest.Fakes;
using NUnit.Framework;

namespace LendOSolnTest.Loan
{
    [TestFixture]
    public class NegativeLoanAmountTest
    {
        [TestFixtureSetUp]
        public void Setup()
        {
            LoginTools.LoginAs(TestAccountType.LoTest001);
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
            FakePageDataUtilities.Instance.Reset();
        }

        [Test]
        public void TestCalculateLoanAmount()
        {
            CPageData dataLoan = CreatePageData();

            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            dataLoan.sPurchPrice = 0;
            dataLoan.sEquityCalc = 5000;

            dataLoan.sLAmtLckd = false;
            dataLoan.sLPurposeT = E_sLPurposeT.Purchase;

            dataLoan.sNoteIR = 5.5M; // Set zero later.
            dataLoan.sProHazInsMb = 0;
            dataLoan.sProRealETxMb = 0;

            dataLoan.Save();

            Assert.AreEqual(5.5, dataLoan.sNoteIR);
            dataLoan = FakePageData.Create();
            dataLoan.InitLoad();

            Assert.AreEqual("$0.00", dataLoan.sLAmtCalc_rep, "sLAmtCalc");
            Assert.AreEqual("$0.00", dataLoan.sMonthlyPmt_rep, "sMonthlyPmt");
        }

        [Test]
        public void TestManualLoanAmount()
        {
            CPageData dataLoan = CreatePageData();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sLPurposeT = E_sLPurposeT.Refin;
            dataLoan.sLAmtLckd = true;
            dataLoan.sLAmtCalc = -10000;
            dataLoan.sNoteIR = 5.5M;
            dataLoan.sProHazInsMb = 0;
            dataLoan.sProRealETxMb = 0;
            dataLoan.Save();

            dataLoan = FakePageData.Create();
            dataLoan.InitLoad();

            Assert.AreEqual("$0.00", dataLoan.sLAmtCalc_rep, "sLAmtCalc");
            Assert.AreEqual("$0.00", dataLoan.sMonthlyPmt_rep, "sMonthlyPmt");


        }
        [Test]
        public void TestNegativeInterestRate()
        {
            CPageData dataLoan = CreatePageData();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sLPurposeT = E_sLPurposeT.Refin;
            dataLoan.sLAmtLckd = true;
            dataLoan.sLAmtCalc = 10000;
            dataLoan.sNoteIR = -5;
            dataLoan.sTerm = 360;
            dataLoan.sDue = 360;
            dataLoan.Save();

            dataLoan = FakePageData.Create();
            dataLoan.InitLoad();

            Assert.AreEqual("$10,000.00", dataLoan.sLAmtCalc_rep, "sLAmtCalc");
            Assert.AreEqual("0.000%", dataLoan.sNoteIR_rep, "sNoteIR");
            
            Assert.AreEqual("$27.78", dataLoan.sMonthlyPmt_rep, "sMonthlyPmt");

        }
        private CPageData CreatePageData()
        {
            return FakePageData.Create();
        }
    }
}
