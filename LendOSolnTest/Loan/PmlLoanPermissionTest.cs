/// Author: David Dao

using System;

using NUnit.Framework;
using LendOSolnTest.Common;
using DataAccess;
using LendersOffice.Security;
using LendersOffice.Audit;
using LendersOffice.Constants;

namespace LendOSolnTest.Loan
{

    [TestFixture]
	public class PmlLoanPermissionTest
	{

        [Test]
        public void TestPmlSupervisorAccessLoan() 
        {
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.Pml_User0);
            Assert.IsNotNull(principal);

            Guid sLId = Guid.Empty;
            CPageData dataLoan = null;

            try 
            {
                // Step 1 - Create New Loan.
                CLoanFileCreator fileCreator = CLoanFileCreator.GetCreator(principal, E_LoanCreationSource.UserCreateFromBlank);
                sLId = fileCreator.CreateBlankLoanFile();

                try 
                {
                    // 11/6/2008 dd - First test. Test to make sure another pml user will not be able to retrieve the file.
                    principal = LoginTools.LoginAs(TestAccountType.Pml_User1);
                    dataLoan = CreateDataLoan(sLId);
                    dataLoan.InitLoad();

                    Assert.Fail("Pml_User1 could access loan created by Pml_User0");
                } 
                catch (PageDataLoadDenied) 
                {
                }

                // 11/6/2008 dd - Second Test. Supervisor will be able to retrieve loan of PML_User0
                principal = LoginTools.LoginAs(TestAccountType.Pml_Supervisor);
                dataLoan = CreateDataLoan(sLId);
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            } 
            finally 
            {
                if (Guid.Empty != sLId) 
                {
                    // 11/6/2008 dd - Clean up.
                    principal = LoginTools.LoginAs(TestAccountType.Corporate_Manager);
                    Assert.IsNotNull(principal);

                    Tools.DeclareLoanFileInvalid(principal, sLId, false, false);
                }
            }
        }

        [Test]
        public void TestNonSupervisorAccess() 
        {
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.Pml_User2);
            Assert.IsNotNull(principal);

            Guid sLId = Guid.Empty;
            CPageData dataLoan = null;

            try 
            {
                // Step 1 - Create New Loan.
                CLoanFileCreator fileCreator = CLoanFileCreator.GetCreator(principal, E_LoanCreationSource.UserCreateFromBlank);
                sLId = fileCreator.CreateBlankLoanFile();

                principal = LoginTools.LoginAs(TestAccountType.Pml_Supervisor);
                Assert.IsNotNull(principal);

                dataLoan = CreateDataLoan(sLId);
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                Assert.Fail("PML_Supervisor could access loan of 'P' user he is not supervised.");
            } 
            catch (PageDataLoadDenied) 
            {
                // 11/6/2008 dd - This exception is expected.
            }
            finally 
            {
                if (Guid.Empty != sLId) 
                {
                    // 11/6/2008 dd - Clean up.
                    principal = LoginTools.LoginAs(TestAccountType.Corporate_Manager);
                    Assert.IsNotNull(principal);

                    Tools.DeclareLoanFileInvalid(principal, sLId, false, false);
                }
            }

        }

        [Test]
        public void TestBPRelationship()
        {
            Guid sLId = Guid.Empty;
            AbstractUserPrincipal principal = null;
            try
            {
                 principal = LoginTools.LoginAs(TestAccountType.Pml_LO1);

                // Step 1 - Create New Loan.
                CLoanFileCreator fileCreator = CLoanFileCreator.GetCreator(principal, E_LoanCreationSource.UserCreateFromBlank);
                 sLId = fileCreator.CreateBlankLoanFile();
                CPageData dataLoan = CreateDataLoan(sLId);
                dataLoan.InitLoad();

                Assert.That(dataLoan.sEmployeeBrokerProcessorName.ToLower(), Is.EqualTo("pml_bp1 pml_bp1"), "BP Name does not match");
                Guid id = dataLoan.sEmployeeBrokerProcessorId;

                principal = LoginTools.LoginAs(TestAccountType.Pml_BP1);
                dataLoan = CreateDataLoan(sLId);
                dataLoan.InitLoad();

                Assert.That(id, Is.EqualTo(principal.EmployeeId), "Assigned BP id does not match.");


            }
            finally
            {
                if (Guid.Empty != sLId && principal != null)
                {
                    principal = LoginTools.LoginAs(TestAccountType.Corporate_Manager);
                    Tools.DeclareLoanFileInvalid(principal, sLId, false, false);

                }
            }

        }
        private CPageData CreateDataLoan(Guid sLId) 
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(PmlLoanPermissionTest));
        }
	}
}
