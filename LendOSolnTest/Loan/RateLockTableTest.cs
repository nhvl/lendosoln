﻿using System;
using System.Collections.Generic;
using DataAccess;
using LendOSolnTest.Common;
using LendOSolnTest.Fakes;
using NUnit.Framework;

namespace LendOSolnTest.Loan
{
    [TestFixture]
    public class RateLockTableTest
    {
        private CPageData CreateDataLoan()
        {
            return FakePageData.Create();
        }

        [TestFixtureSetUp]
        public void Setup()
        {
            LoginTools.LoginAs(TestAccountType.LoTest001);

        }

        [Test]
        public void BrokerRateLockTableTest()
        {
            CPageData dataLoan = CreateDataLoan();
            dataLoan.InitLoad();

            // Setup Initial values
            dataLoan.sLAmtCalc_rep = "200,000.00";
            dataLoan.sLAmtLckd = true;
            dataLoan.sNoteIR = 5.750M;
            dataLoan.sBrokComp1Pc = -.125M;

            // No adjustments/comp
            Assert.AreEqual("5.750%", dataLoan.sBrokerLockBaseNoteIR_rep,                       "1. sBrokerLockBaseNoteIR calculation in rate lock table");
            Assert.AreEqual("-0.125%", dataLoan.sBrokerLockBaseBrokComp1PcFee_rep,              "2. sBrokerLockBaseBrokComp1PcFee calculation in rate lock table");
            Assert.AreEqual("5.750%", dataLoan.sBrokerLockBrokerBaseNoteIR_rep,                 "3. sBrokerLockBrokerBaseNoteIR calculation in rate lock table");
            Assert.AreEqual("-0.125%", dataLoan.sBrokerLockBrokerBaseBrokComp1PcFee_rep,        "4. sBrokerLockBrokerBaseBrokComp1PcFee calculation in rate lock table");
            Assert.AreEqual("5.750%", dataLoan.sBrokerLockOriginatorPriceNoteIR_rep,            "5. sBrokerLockOriginatorPriceNoteIR calculation in rate lock table");
            Assert.AreEqual("-0.125%", dataLoan.sBrokerLockOriginatorPriceBrokComp1PcFee_rep,   "6. sBrokerLockOriginatorPriceBrokComp1PcFee calculation in rate lock table");

            // Add adjustments and some originator comp.
            List<PricingAdjustment> adjustments = dataLoan.sBrokerLockAdjustments;
            adjustments.Add(new PricingAdjustment() { Rate = "0.200%", Fee = "0.300%", IsHidden = true });
            adjustments.Add(new PricingAdjustment() { Rate = "0.300%", Fee = "0.200%", IsHidden = false });
            dataLoan.sBrokerLockAdjustments = adjustments;
            dataLoan.sOriginatorCompensationPaymentSourceT = E_sOriginatorCompensationPaymentSourceT.LenderPaid;
            dataLoan.sOriginatorCompensationLenderFeeOptionT = E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees;
            dataLoan.ApplyCompensationManually(
                DateTime.Now.ToString()
                , DateTime.Now.ToString()
                ,"1.540%"
                , E_PercentBaseT.TotalLoanAmount
                , "$150.00"
                , "$10,000.00" 
                , "$0.00"
                , ""
                , false);

            dataLoan.LoadBrokerLockData();

            // Table having adjustments and originator comp.
            Assert.AreEqual("5.250%", dataLoan.sBrokerLockBaseNoteIR_rep,                       "7. sBrokerLockBaseNoteIR calculation in rate lock table");
            Assert.AreEqual("-0.625%", dataLoan.sBrokerLockBaseBrokComp1PcFee_rep,              "8. sBrokerLockBaseBrokComp1PcFee calculation in rate lock table");
            Assert.AreEqual("5.450%", dataLoan.sBrokerLockBrokerBaseNoteIR_rep,                 "9. sBrokerLockBrokerBaseNoteIR calculation in rate lock table");
            Assert.AreEqual("-0.325%", dataLoan.sBrokerLockBrokerBaseBrokComp1PcFee_rep,        "10. sBrokerLockBrokerBaseBrokComp1PcFee calculation in rate lock table");
            Assert.AreEqual("5.750%", dataLoan.sBrokerLockOriginatorPriceNoteIR_rep,            "11. sBrokerLockOriginatorPriceNoteIR calculation in rate lock table");
            Assert.AreEqual("1.415%", dataLoan.sBrokerLockOriginatorPriceBrokComp1PcFee_rep,    "12. sBrokerLockOriginatorPriceBrokComp1PcFee calculation in rate lock table");
        }

    }
}

