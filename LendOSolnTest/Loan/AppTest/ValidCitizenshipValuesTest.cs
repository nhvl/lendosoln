﻿namespace LendOSolnTest.Loan.AppTest
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using NUnit.Framework;

    [TestFixture]
    public class ValidCitizenshipValuesTest
    {
        private static readonly object[][] GetValidCitizenshipValues_Ulad_Always_ReturnsAllCitizenshipTypes_Source =
            (from citizenshipValue in new[] { string.Empty, "Y", "N" }
             from residencyValue in new[] { string.Empty, "Y", "N" }
             select new[] { citizenshipValue, residencyValue }).ToArray();

        [Test]
        [TestCase(GseTargetApplicationT.Blank)]
        [TestCase(GseTargetApplicationT.Legacy)]
        public void GetValidCitizenshipValues_NonUlad_CitizenBlankResidencyBlank_ReturnsExpectedDefault(GseTargetApplicationT targetApplicationType)
        {
            var citizenship = "";
            var residency = "";

            var config = CAppBase.GetValidCitizenshipValues(targetApplicationType, citizenship, residency);

            Assert.AreEqual(null, config.DefaultValue);
        }

        [Test]
        [TestCase(GseTargetApplicationT.Blank)]
        [TestCase(GseTargetApplicationT.Legacy)]
        public void GetValidCitizenshipValues_NonUlad_CitizenBlankResidencyBlank_ReturnsExpectedValidValues(GseTargetApplicationT targetApplicationType)
        {
            var citizenship = "";
            var residency = "";
            var allValues = Enum.GetValues(typeof(E_aProdCitizenT));

            var config = CAppBase.GetValidCitizenshipValues(targetApplicationType, citizenship, residency);

            CollectionAssert.AreEquivalent(allValues, config.ValidValues);
        }

        [Test]
        [TestCase(GseTargetApplicationT.Blank)]
        [TestCase(GseTargetApplicationT.Legacy)]
        public void GetValidCitizenshipValues_NonUlad_CitizenBlankResidencyNo_ReturnsExpectedDefault(GseTargetApplicationT targetApplicationType)
        {
            var citizenship = "";
            var residency = "N";

            var config = CAppBase.GetValidCitizenshipValues(targetApplicationType, citizenship, residency);

            Assert.AreEqual(E_aProdCitizenT.NonpermanentResident, config.DefaultValue);
        }

        [Test]
        [TestCase(GseTargetApplicationT.Blank)]
        [TestCase(GseTargetApplicationT.Legacy)]
        public void GetValidCitizenshipValues_NonUlad_CitizenBlankResidencyNo_ReturnsExpectedValidValues(GseTargetApplicationT targetApplicationType)
        {
            var citizenship = "";
            var residency = "N";
            var expected = new List<E_aProdCitizenT>
            {
                E_aProdCitizenT.USCitizen,
                E_aProdCitizenT.NonpermanentResident,
                E_aProdCitizenT.ForeignNational
            };

            var config = CAppBase.GetValidCitizenshipValues(targetApplicationType, citizenship, residency);

            CollectionAssert.AreEquivalent(expected, config.ValidValues);
        }

        [Test]
        [TestCase(GseTargetApplicationT.Blank)]
        [TestCase(GseTargetApplicationT.Legacy)]
        public void GetValidCitizenshipValues_NonUlad_CitizenBlankResidencyYes_ReturnsExpectedDefault(GseTargetApplicationT targetApplicationType)
        {
            var citizenship = "";
            var residency = "Y";

            var config = CAppBase.GetValidCitizenshipValues(targetApplicationType, citizenship, residency);

            Assert.AreEqual(E_aProdCitizenT.PermanentResident, config.DefaultValue);
        }

        [Test]
        [TestCase(GseTargetApplicationT.Blank)]
        [TestCase(GseTargetApplicationT.Legacy)]
        public void GetValidCitizenshipValues_NonUlad_CitizenBlankResidencyYes_ReturnsExpectedValidValues(GseTargetApplicationT targetApplicationType)
        {
            var citizenship = "";
            var residency = "Y";
            var expected = new List<E_aProdCitizenT>
            {
                E_aProdCitizenT.USCitizen,
                E_aProdCitizenT.PermanentResident,
            };

            var config = CAppBase.GetValidCitizenshipValues(targetApplicationType, citizenship, residency);

            CollectionAssert.AreEquivalent(expected, config.ValidValues);
        }

        [Test]
        [TestCase(GseTargetApplicationT.Blank)]
        [TestCase(GseTargetApplicationT.Legacy)]
        public void GetValidCitizenshipValues_NonUlad_CitizenNoResidencyBlank_ReturnsExpectedDefault(GseTargetApplicationT targetApplicationType)
        {
            var citizenship = "N";
            var residency = "";

            var config = CAppBase.GetValidCitizenshipValues(targetApplicationType, citizenship, residency);

            Assert.AreEqual(E_aProdCitizenT.PermanentResident, config.DefaultValue);
        }

        [Test]
        [TestCase(GseTargetApplicationT.Blank)]
        [TestCase(GseTargetApplicationT.Legacy)]
        public void GetValidCitizenshipValues_NonUlad_CitizenNoResidencyBlank_ReturnsExpectedValidValues(GseTargetApplicationT targetApplicationType)
        {
            var citizenship = "N";
            var residency = "";
            var expected = new List<E_aProdCitizenT>
            {
                E_aProdCitizenT.PermanentResident,
                E_aProdCitizenT.NonpermanentResident,
                E_aProdCitizenT.ForeignNational
            };

            var config = CAppBase.GetValidCitizenshipValues(targetApplicationType, citizenship, residency);

            CollectionAssert.AreEquivalent(expected, config.ValidValues);
        }

        [Test]
        [TestCase(GseTargetApplicationT.Blank)]
        [TestCase(GseTargetApplicationT.Legacy)]
        public void GetValidCitizenshipValues_NonUlad_CitizenNoResidencyNo_ReturnsExpectedDefault(GseTargetApplicationT targetApplicationType)
        {
            var citizenship = "N";
            var residency = "N";

            var config = CAppBase.GetValidCitizenshipValues(targetApplicationType, citizenship, residency);

            Assert.AreEqual(E_aProdCitizenT.NonpermanentResident, config.DefaultValue);
        }

        [Test]
        [TestCase(GseTargetApplicationT.Blank)]
        [TestCase(GseTargetApplicationT.Legacy)]
        public void GetValidCitizenshipValues_NonUlad_CitizenNoResidencyNo_ReturnsExpectedValidValues(GseTargetApplicationT targetApplicationType)
        {
            var citizenship = "N";
            var residency = "N";
            var expected = new List<E_aProdCitizenT>
            {
                E_aProdCitizenT.NonpermanentResident,
                E_aProdCitizenT.ForeignNational
            };

            var config = CAppBase.GetValidCitizenshipValues(targetApplicationType, citizenship, residency);

            CollectionAssert.AreEquivalent(expected, config.ValidValues);
        }

        [Test]
        [TestCase(GseTargetApplicationT.Blank)]
        [TestCase(GseTargetApplicationT.Legacy)]
        public void GetValidCitizenshipValues_NonUlad_CitizenNoResidencyYes_ReturnsExpectedDefault(GseTargetApplicationT targetApplicationType)
        {
            var citizenship = "N";
            var residency = "Y";

            var config = CAppBase.GetValidCitizenshipValues(targetApplicationType, citizenship, residency);

            Assert.AreEqual(E_aProdCitizenT.PermanentResident, config.DefaultValue);
        }

        [Test]
        [TestCase(GseTargetApplicationT.Blank)]
        [TestCase(GseTargetApplicationT.Legacy)]
        public void GetValidCitizenshipValues_NonUlad_CitizenNoResidencyYes_ReturnsExpectedValidValues(GseTargetApplicationT targetApplicationType)
        {
            var citizenship = "N";
            var residency = "Y";
            var expected = new List<E_aProdCitizenT>
            {
                E_aProdCitizenT.PermanentResident,
            };

            var config = CAppBase.GetValidCitizenshipValues(targetApplicationType, citizenship, residency);

            CollectionAssert.AreEquivalent(expected, config.ValidValues);
        }

        [Test]
        [TestCase(GseTargetApplicationT.Blank)]
        [TestCase(GseTargetApplicationT.Legacy)]
        public void GetValidCitizenshipValues_NonUlad_CitizenYesResidencyNo_ReturnsExpectedDefault(GseTargetApplicationT targetApplicationType)
        {
            var citizenship = "Y";
            var residency = "N";

            var config = CAppBase.GetValidCitizenshipValues(targetApplicationType, citizenship, residency);

            Assert.AreEqual(E_aProdCitizenT.USCitizen, config.DefaultValue);
        }

        [Test]
        [TestCase(GseTargetApplicationT.Blank)]
        [TestCase(GseTargetApplicationT.Legacy)]
        public void GetValidCitizenshipValues_NonUlad_CitizenYesResidencyNo_ReturnsExpectedValidValues(GseTargetApplicationT targetApplicationType)
        {
            var citizenship = "Y";
            var residency = "N";
            var expected = new List<E_aProdCitizenT>
            {
                E_aProdCitizenT.USCitizen,
            };

            var config = CAppBase.GetValidCitizenshipValues(targetApplicationType, citizenship, residency);

            CollectionAssert.AreEquivalent(expected, config.ValidValues);
        }

        [Test]
        [TestCase(GseTargetApplicationT.Blank)]
        [TestCase(GseTargetApplicationT.Legacy)]
        public void GetValidCitizenshipValues_NonUlad_CitizenYesResidencyYes_ReturnsExpectedDefault(GseTargetApplicationT targetApplicationType)
        {
            var citizenship = "Y";
            var residency = "Y";

            var config = CAppBase.GetValidCitizenshipValues(targetApplicationType, citizenship, residency);

            Assert.AreEqual(E_aProdCitizenT.USCitizen, config.DefaultValue);
        }

        [Test]
        [TestCase(GseTargetApplicationT.Blank)]
        [TestCase(GseTargetApplicationT.Legacy)]
        public void GetValidCitizenshipValues_NonUlad_CitizenYesResidencyYes_ReturnsExpectedValidValues(GseTargetApplicationT targetApplicationType)
        {
            var citizenship = "Y";
            var residency = "Y";
            var expected = new List<E_aProdCitizenT>
            {
                E_aProdCitizenT.USCitizen,
                E_aProdCitizenT.PermanentResident,
            };

            var config = CAppBase.GetValidCitizenshipValues(targetApplicationType, citizenship, residency);

            CollectionAssert.AreEquivalent(expected, config.ValidValues);
        }

        [Test]
        [TestCase(GseTargetApplicationT.Blank)]
        [TestCase(GseTargetApplicationT.Legacy)]
        public void GetValidCitizenshipValues_NonUlad_CitizenYesResidencyBlank_ReturnsExpectedDefault(GseTargetApplicationT targetApplicationType)
        {
            var citizenship = "Y";
            var residency = "";

            var config = CAppBase.GetValidCitizenshipValues(targetApplicationType, citizenship, residency);

            Assert.AreEqual(E_aProdCitizenT.USCitizen, config.DefaultValue);
        }

        [Test]
        [TestCase(GseTargetApplicationT.Blank)]
        [TestCase(GseTargetApplicationT.Legacy)]
        public void GetValidCitizenshipValues_NonUlad_CitizenYesResidencyBlank_ReturnsExpectedValidValues(GseTargetApplicationT targetApplicationType)
        {
            var citizenship = "Y";
            var residency = "";
            var expected = new List<E_aProdCitizenT>
            {
                E_aProdCitizenT.USCitizen,
                E_aProdCitizenT.PermanentResident
            };

            var config = CAppBase.GetValidCitizenshipValues(targetApplicationType, citizenship, residency);

            CollectionAssert.AreEquivalent(expected, config.ValidValues);
        }

        [Test, TestCaseSource(nameof(GetValidCitizenshipValues_Ulad_Always_ReturnsAllCitizenshipTypes_Source))]
        public void GetValidCitizenshipValues_Ulad_Always_ReturnsAllCitizenshipTypes(string citizenship, string residency)
        {
            var expected = Enum.GetValues(typeof(E_aProdCitizenT)).Cast<E_aProdCitizenT>();
            var config = CAppBase.GetValidCitizenshipValues(GseTargetApplicationT.Ulad2019, citizenship, residency);
            CollectionAssert.AreEquivalent(expected, config.ValidValues);
        }
    }
}
