﻿using System.Collections.Generic;
using DataAccess;
using LendOSolnTest.Common;
using LendOSolnTest.Fakes;
using NUnit.Framework;

namespace LendOSolnTest.Loan.AppTest
{
    [TestFixture]
    public class OtherIncomeImportTest
    {
        [TestFixtureSetUp]
        public void Setup()
        {
            LoginTools.LoginAs(TestAccountType.LoTest001);
        }

        [Test]
        public void ImportLessThan3Incomes()
        {


            var testData = new[] { 
                new { 
                    Data = new [] {
                        new { Amount = 100M, IsForC = false, Desc = "Desc1" } 
                    }, 
                    TestDesc = "Test1 For B Single",
                }, 
                
                new { 
                    Data = new [] {
                        new { Amount = 100M, IsForC = false, Desc = "Desc1" },
                        new { Amount = 200M, IsForC = false, Desc = "Desc@" }, 
 
                    }, 
                    TestDesc = "Test1 For B Double"
                }, 
                new { 
                    Data = new [] {
                        new { Amount = 100M, IsForC = false, Desc = "Desc1" } ,
                        new { Amount = 200M, IsForC = false, Desc = "Desc2" } ,
                        new { Amount = 300M, IsForC = false, Desc = "Desc3" } ,
                    }, 
                    TestDesc = "Test1 For B Tripple"
                }, 
                new { 
                    Data = new [] {
                        new { Amount = 100M, IsForC = true, Desc = "Desc1" } 
                    }, 
                    TestDesc = "Test1 For C Single"
                }, 
                
                new { 
                    Data = new [] {
                        new { Amount = 100M, IsForC = true, Desc = "Desc1" },
                        new { Amount = 200M, IsForC = true, Desc = "Desc@" }, 
 
                    }, 
                    TestDesc = "Test1 For C Double"
                }, 
                new { 
                    Data = new [] {
                        new { Amount = 100M, IsForC = true, Desc = "Desc1" } ,
                        new { Amount = 200M, IsForC = true, Desc = "Desc2" } ,
                        new { Amount = 300M, IsForC = true, Desc = "Desc3" } ,
                    }, 
                    TestDesc = "Test1 For C Tripple"
                }, 
                new { 
                    Data = new [] {
                        new { Amount = 100M, IsForC = true, Desc = "Desc1" },
                        new { Amount = 200M, IsForC = false, Desc = "Desc@" }, 
 
                    }, 
                    TestDesc = "Test1 For BC Double"
                }, 
                new { 
                    Data = new [] {
                        new { Amount = 100M, IsForC = true, Desc = "Desc1" } ,
                        new { Amount = 200M, IsForC = false, Desc = "Desc2" } ,
                        new { Amount = 300M, IsForC = true, Desc = "Desc3" } ,
                    }, 
                    TestDesc = "Test1 For BC Tripple"
                }, 

                new { 
                    Data = new [] {
                        new { Amount = 100M, IsForC = true, Desc = "Desc1" } ,
                        new { Amount = 200M, IsForC = false, Desc = "Desc2" } ,
                        new { Amount = 300M, IsForC = false, Desc = "Desc3" } ,
                    }, 
                    TestDesc = "Test1 For BC Tripple"
                }, 


           
            };
            CPageData data = CreateDataObject();
            data.InitLoad();
            CAppData app = data.GetAppData(0);

            foreach (var test in testData)
            {
                List<OtherIncome> otherIncomes = new List<OtherIncome>();

                foreach (var income in test.Data)
                {
                    otherIncomes.Add(new OtherIncome()
                    {
                        Amount = income.Amount,
                        Desc = income.Desc,
                        IsForCoBorrower = income.IsForC
                    });
                }

                app.aOtherIncomeList = otherIncomes;
                var appIncomes = app.aOtherIncomeList;

                for (var i = 0; i < otherIncomes.Count; i++)
                {
                    Assert.That(otherIncomes[i].Amount, Is.EqualTo(appIncomes[i].Amount), test.TestDesc);
                    Assert.That(otherIncomes[i].Desc, Is.EqualTo(appIncomes[i].Desc), test.TestDesc);
                    Assert.That(otherIncomes[i].IsForCoBorrower, Is.EqualTo(appIncomes[i].IsForCoBorrower), test.TestDesc);
                }

                for (var i = otherIncomes.Count; i < 3; i++)
                {
                    Assert.That(appIncomes[i].Amount, Is.EqualTo(0));
                    Assert.That(appIncomes[i].Desc, Is.EqualTo(string.Empty));
                }
            }
        }

        [Test]
        public void ImportManyBorrowerTest()
        {
            var testData = new[] { 
                new {
                    Data = new [] { 
                        new { Amount = 100M, IsForC = false, Desc = "Desc1" } ,
                        new { Amount = 200M, IsForC = false, Desc = "Desc2" } ,
                        new { Amount = 300M, IsForC = false, Desc = "Desc3" } ,
                        new { Amount = 400M, IsForC = false, Desc = "Desc4" } ,
                    },
                    Desc = "Import 4 Borrower Other Income Records" ,
                    Total = 1000M
                },

                new {
                    Data = new [] { 
                        new { Amount = 100M, IsForC = false, Desc = "Desc1" } ,
                        new { Amount = 200M, IsForC = false, Desc = "Desc2" } ,
                        new { Amount = 300M, IsForC = false, Desc = "Desc3" } ,
                        new { Amount = 400M, IsForC = false, Desc = "Desc4" } ,
                        new { Amount = 500M, IsForC = false, Desc = "Desc5" } ,
                        new { Amount = 600M, IsForC = false, Desc = "Desc6" } ,
                    },
                    Desc = "Import 6 Borrower Other Income Records",
                    Total = 2100M
                },
       
            };

            CPageData data = CreateDataObject();
            data.InitLoad();
            CAppData app = data.GetAppData(0);
            foreach (var test in testData)
            {
                List<OtherIncome> otherIncomes = new List<OtherIncome>();
                foreach (var income in test.Data)
                {
                    otherIncomes.Add(new OtherIncome()
                    {
                        Amount = income.Amount,
                        Desc = income.Desc,
                        IsForCoBorrower = income.IsForC
                    });
                }

                app.aOtherIncomeList = otherIncomes;
                var appIncomes = app.aOtherIncomeList;

                Assert.That(app.aBTotOI, Is.EqualTo(test.Total));
            }
        }

        [Test]
        public void ImportManyCoBorrowerTest()
        {
            var testData = new[] { 
                new {
                    Data = new [] { 
                        new { Amount = 100M, IsForC = true, Desc = "Desc1" } ,
                        new { Amount = 200M, IsForC = true, Desc = "Desc2" } ,
                        new { Amount = 300M, IsForC = true, Desc = "Desc3" } ,
                        new { Amount = 400M, IsForC = true, Desc = "Desc4" } ,
                    },
                    Desc = "Import 4 CoBorrower Other Income Records",
                    Total = 1000M
                },

                new {
                    Data = new [] { 
                        new { Amount = 100M, IsForC = true, Desc = "Desc1" } ,
                        new { Amount = 200M, IsForC = true, Desc = "Desc2" } ,
                        new { Amount = 300M, IsForC = true, Desc = "Desc3" } ,
                        new { Amount = 400M, IsForC = true, Desc = "Desc4" } ,
                        new { Amount = 500M, IsForC = true, Desc = "Desc5" } ,
                        new { Amount = 600M, IsForC = true, Desc = "Desc6" } ,
                    },
                    Desc = "Import 6 CoBorrower Other Income Records",
                    Total = 2100M
                },
       
            };

            CPageData data = CreateDataObject();
            data.InitLoad();
            CAppData app = data.GetAppData(0);
            foreach (var test in testData)
            {
                List<OtherIncome> otherIncomes = new List<OtherIncome>();
                foreach (var income in test.Data)
                {
                    otherIncomes.Add(new OtherIncome()
                    {
                        Amount = income.Amount,
                        Desc = income.Desc,
                        IsForCoBorrower = income.IsForC
                    });
                }

                app.aOtherIncomeList = otherIncomes;

                var appIncomes = app.aOtherIncomeList;

                Assert.That(app.aCTotOI, Is.EqualTo(test.Total));

            }
        }

        /// <summary>
        /// Yes I make a lot of assumtions sadly I need to hurry on these cases.
        /// </summary>
        [Test]
        public void ImportManyTest()
        {
            var testData = new[] { 
                new {
                    Data = new [] { 
                        new { Amount = 100M, IsForC = true, Desc = "Desc1" } ,
                        new { Amount = 200M, IsForC = true, Desc = "Desc2" } ,
                        new { Amount = 300M, IsForC = false, Desc = "Desc3" } ,
                        new { Amount = 400M, IsForC = false, Desc = "Desc4" } ,
                    },
                    Desc = "Import 4 Mix Records" ,
                    TotalC = 300M,
                    TotalB = 700M,
                },

                new {
                    Data = new [] { 
                        new { Amount = 100M, IsForC = true, Desc = "Desc1" } ,
                        new { Amount = 200M, IsForC = true, Desc = "Desc2" } ,
                        new { Amount = 300M, IsForC = true, Desc = "Desc3" } ,
                        new { Amount = 400M, IsForC = false, Desc = "Desc4" } ,
                        new { Amount = 500M, IsForC = false, Desc = "Desc5" } ,
                        new { Amount = 600M, IsForC = false, Desc = "Desc6" } ,
                    },
                    Desc = "Import 6 Mix Records",
                    TotalC = 600M,
                    TotalB = 1500M,
                },
       
            };

            CPageData data = CreateDataObject();
            data.InitLoad();
            CAppData app = data.GetAppData(0);
            foreach (var test in testData)
            {
                List<OtherIncome> otherIncomes = new List<OtherIncome>();
                foreach (var income in test.Data)
                {
                    otherIncomes.Add(new OtherIncome()
                    {
                        Amount = income.Amount,
                        Desc = income.Desc,
                        IsForCoBorrower = income.IsForC
                    });
                }

                app.aOtherIncomeList = otherIncomes;
                var appIncomes = app.aOtherIncomeList;

                Assert.That(app.aBTotOI, Is.EqualTo(test.TotalB));
                Assert.That(app.aCTotOI, Is.EqualTo(test.TotalC));

            }
        }

        private CPageData CreateDataObject()
        {
            return FakePageData.Create();
        }
    }
}
