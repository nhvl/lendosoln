﻿namespace LendOSolnTest.Loan
{
    using System;
    using Common;
    using DataAccess;
    using NUnit.Framework;

    [Category(CategoryConstants.UnitTest)]
    [TestFixture]
    public class LoanFileCreatorTest
    {
        [Test]
        public void GetLoanCopyName_NoBaseSuffix_AppendsSeparatorAnd4CharsToSource()
        {
            var loanCreator = new MockLoanFileCreator();
            var baseSuffix = "";
            var separator = "-";

            var copy = loanCreator.GetLoanCopyName(Guid.Empty, baseSuffix, separator);

            var expected = $"{MockLoanFileCreator.FakeLoanName}{separator}{new string(MockLoanFileCreator.FriendlyIdReplacementChar, 4)}";
            Assert.AreEqual(expected, copy);
        }

        [Test]
        public void GetLoanCopyName_WithBaseSuffix_AppendsSeparatorBaseSuffixSeparatorAndTwoCharsToSource()
        {
            var loanCreator = new MockLoanFileCreator();
            var baseSuffix = "Sandbox";
            var separator = "-";

            var copy = loanCreator.GetLoanCopyName(Guid.Empty, baseSuffix, separator);

            var expected = $"{MockLoanFileCreator.FakeLoanName}{separator}{baseSuffix}{separator}{new string(MockLoanFileCreator.FriendlyIdReplacementChar, 2)}";
            Assert.AreEqual(expected, copy);
        }

        [Test]
        public void GetLoanCopyName_WithBaseSuffixAndXSeparator_UsesXSeparator()
        {
            var loanCreator = new MockLoanFileCreator();
            var baseSuffix = "2nd";
            var separator = "x";

            var copy = loanCreator.GetLoanCopyName(Guid.Empty, baseSuffix, separator);

            var expected = $"{MockLoanFileCreator.FakeLoanName}{separator}{baseSuffix}{separator}{new string(MockLoanFileCreator.FriendlyIdReplacementChar, 2)}";
            Assert.AreEqual(expected, copy);
        }

        private class MockLoanFileCreator : CLoanFileCreator
        {
            public const string FakeLoanName = "FakeLoanName";
            public const char FriendlyIdReplacementChar = 'A';

            public MockLoanFileCreator() : base()
            {
            }

            protected override string GetLoanNameByLoanId(Guid sourceFileId)
            {
                return FakeLoanName;
            }

            protected override string GenerateFriendlyIdForCopyName(string baseSuffix)
            {
                var baseFriendlyId = base.GenerateFriendlyIdForCopyName(baseSuffix);
                return new string(FriendlyIdReplacementChar, baseFriendlyId.Length);
            }

            public new string GetLoanCopyName(Guid sourceFileId, string baseSuffix, string separator)
            {
                return base.GetLoanCopyName(sourceFileId, separator, baseSuffix);
            }
        }
    }
}
