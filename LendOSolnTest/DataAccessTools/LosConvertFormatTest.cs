/// Author: David Dao

using System;
using NUnit.Framework;
using DataAccess;

namespace LendOSolnTest.DataAccessTools
{
    [TestFixture]
	public class LosConvertFormatTest
	{
        [Test]
        public void ToSsnFormatTest() 
        {
            // 10/21/2008 dd - Test has following format { FormatTarget, FormatDirection, inputString, expectedOutput}
            object[][] testCases = 
            {
                new object[] { FormatTarget.Webform, FormatDirection.ToRep, "", ""},
                new object[] { FormatTarget.Webform, FormatDirection.ToRep, null, ""},
                new object[] { FormatTarget.Webform, FormatDirection.ToRep, "111-11-1111", "111-11-1111"},
                new object[] { FormatTarget.Webform, FormatDirection.ToRep, " 111 -11-1111 ", "111-11-1111"},
                new object[] { FormatTarget.Webform, FormatDirection.ToRep, "   111-11", "111-11"},
                new object[] { FormatTarget.PrintoutNormalFields, FormatDirection.ToRep, "111-11-1111", "111-11-1111"},
                new object[] { FormatTarget.FannieMaeMornetPlus, FormatDirection.ToDb, "123456789", "123-45-6789"},
                new object[] { FormatTarget.FannieMaeMornetPlus, FormatDirection.ToRep, "123456789", "123456789"},
                new object[] { FormatTarget.FannieMaeMornetPlus, FormatDirection.ToDb, "123-45-6789", "123-45-6789"},
                new object[] { FormatTarget.FannieMaeMornetPlus, FormatDirection.ToRep, "123-45-6789", "123456789"},

            };

            foreach (object[] testCase in testCases) 
            {
                FormatTarget target = (FormatTarget) testCase[0];
                FormatDirection direction = (FormatDirection) testCase[1];
                string input = testCase[2] as string;
                string expectedOutput = testCase[3] as string;

                string desc = string.Format("FormatTarget.{0}, FormatDirection{1}, Input=[{2}]", target, direction, input);

                LosConvert testConveter = new LosConvert(target);
                string output = testConveter.ToSsnFormat(input, direction);

                Assert.AreEqual(expectedOutput, output, desc);
                
            }
        }

        [Test]
        public void ToPhoneNumFormatTest() 
        {
            // 10/21/2008 dd - Test has following format { FormatTarget, FormatDirection, inputString, expectedOutput}
            object[][] testCases = 
            {
                new object[] { FormatTarget.Webform, FormatDirection.ToRep, null, ""},
                new object[] { FormatTarget.Webform, FormatDirection.ToRep, "", ""},
                new object[] { FormatTarget.Webform, FormatDirection.ToDb, "1234567890", "(123) 456-7890"},
                new object[] { FormatTarget.Webform, FormatDirection.ToRep, "1234567890", "1234567890"},

                new object[] { FormatTarget.PointNative, FormatDirection.ToDb, "1234567890", "(123) 456-7890"},
                new object[] { FormatTarget.PointNative, FormatDirection.ToRep, "1234567890", "123-456-7890"},

            };

            foreach (object[] testCase in testCases) 
            {
                FormatTarget target = (FormatTarget) testCase[0];
                FormatDirection direction = (FormatDirection) testCase[1];
                string input = testCase[2] as string;
                string expectedOutput = testCase[3] as string;

                string desc = string.Format("FormatTarget.{0}, FormatDirection{1}, Input=[{2}]", target, direction, input);

                LosConvert testConveter = new LosConvert(target);
                string output = testConveter.ToPhoneNumFormat(input, direction);

                Assert.AreEqual(expectedOutput, output, desc);
                
            }

        }


        [Test]
        public void ToDateTimeStringTest() 
        {
                        // 10/21/2008 dd - Test has following format { FormatTarget, input, expectedOutput }
            object[][] testCases = 
            {
                new object[] { FormatTarget.Webform, DateTime.MinValue, ""},
            };

            foreach (object[] testCase in testCases) 
            {
                FormatTarget target = (FormatTarget) testCase[0];

                DateTime input = (DateTime) testCase[1];
                string expectedOutput = (string) testCase[2];

                string desc = string.Format("FormatTarget.{0}, Input=[{1}]", target, input);
                LosConvert testConveter = new LosConvert(target);

                string output = testConveter.ToDateTimeString(input);

                Assert.AreEqual(expectedOutput, output, desc);
            }
        }

        [Test]
        public void ToDateTimeVarCharStringTest() 
        {
            // 10/21/2008 dd - Test has following format { FormatTarget, input, expectedOutput }
            object[][] testCases = 
            {
                new object[] { FormatTarget.Webform, null, ""},
            };

            foreach (object[] testCase in testCases) 
            {
                FormatTarget target = (FormatTarget) testCase[0];

                string input = testCase[1] as string;
                string expectedOutput = (string) testCase[2];

                string desc = string.Format("FormatTarget.{0}, Input=[{1}]", target, input);
                LosConvert testConveter = new LosConvert(target);

                string output = testConveter.ToDateTimeVarCharString(input);

                Assert.AreEqual(expectedOutput, output, desc);
            }
        }

        [Test]
        public void ToMoneyStringTest() 
        {
            // 10/21/2008 dd - Test has following format { FormatTarget, input, expectedOutput }
            object[][] testCases = 
            {
                new object[] { FormatTarget.Webform, FormatDirection.ToRep, 0.0M, "$0.00"},
                new object[] { FormatTarget.Webform, FormatDirection.ToRep, 1000.999M, "$1,001.00"},
                new object[] { FormatTarget.Webform, FormatDirection.ToRep, 1000.96M, "$1,000.96"},
                new object[] { FormatTarget.Webform, FormatDirection.ToRep, -1000.96M, "($1,000.96)"},
                new object[] { FormatTarget.Webform, FormatDirection.ToRep, -1234M, "($1,234.00)"},
                new object[] { FormatTarget.PrintoutNormalFields, FormatDirection.ToRep, 0.0M, ""},
                new object[] { FormatTarget.PrintoutImportantFields, FormatDirection.ToRep, 0.0M, "0.00"},
                new object[] { FormatTarget.FannieMaeMornetPlus, FormatDirection.ToRep, 0.0M, ""},
                new object[] { FormatTarget.FannieMaeMornetPlus, FormatDirection.ToRep, 123.0M,   "         123.00"},
                new object[] { FormatTarget.FannieMaeMornetPlus, FormatDirection.ToRep, 1234.0M,  "        1234.00"},
                new object[] { FormatTarget.FannieMaeMornetPlus, FormatDirection.ToRep, -123.0M,  "        -123.00"},
                new object[] { FormatTarget.FannieMaeMornetPlus, FormatDirection.ToRep, -1234.0M, "       -1234.00"},
                new object[] { FormatTarget.MismoClosing, FormatDirection.ToRep, 0.0M, "0.00"},
                new object[] { FormatTarget.MismoClosing, FormatDirection.ToRep, 123.0M,   "123.00"},
                new object[] { FormatTarget.MismoClosing, FormatDirection.ToRep, 1234.0M,  "1234.00"},
                new object[] { FormatTarget.MismoClosing, FormatDirection.ToRep, -123.0M,  "-123.00"},
                new object[] { FormatTarget.MismoClosing, FormatDirection.ToRep, -1234.0M, "-1234.00"},
                new object[] { FormatTarget.FreddieMacAUS, FormatDirection.ToRep, 0.0M, "0.00"},
                new object[] { FormatTarget.FreddieMacAUS, FormatDirection.ToRep, 123.0M,   "123.00"},
                new object[] { FormatTarget.FreddieMacAUS, FormatDirection.ToRep, 1234.0M,  "1234.00"},
                new object[] { FormatTarget.FreddieMacAUS, FormatDirection.ToRep, -123.0M,  "-123.00"},
                new object[] { FormatTarget.FreddieMacAUS, FormatDirection.ToRep, -1234.0M, "-1234.00"},

                // 2/4/2011 dd - These test are for banker rounding.
                new object[] { FormatTarget.Webform, FormatDirection.ToRep, 10.125M, "$10.12"},
                new object[] { FormatTarget.Webform, FormatDirection.ToRep, 10.135M, "$10.14"},
                new object[] { FormatTarget.Webform, FormatDirection.ToRep, 10.125001M, "$10.13"},
                new object[] { FormatTarget.Webform, FormatDirection.ToRep, 10.135001M, "$10.14"},

            };
            
            foreach (object[] testCase in testCases) 
            {
                FormatTarget target = (FormatTarget) testCase[0];
                FormatDirection direction = (FormatDirection) testCase[1];
                decimal input = (decimal) testCase[2];
                string expectedOutput = (string) testCase[3];

                string desc = string.Format("FormatTarget.{0}, FormatDirection.{1}, Input=[{2}]", target, direction, input);
                LosConvert testConveter = new LosConvert(target);

                string output = testConveter.ToMoneyString(input, direction);

                Assert.AreEqual(expectedOutput, output, desc);
            }
        }

        [Test]
        public void ToMoneyVarCharStringRepTest() 
        {
            // 10/21/2008 dd - Test has following format { FormatTarget, input, expectedOutput }
            object[][] testCases = 
            {
                new object[] { FormatTarget.Webform, null, "$0.00"},
            };

            foreach (object[] testCase in testCases) 
            {
                FormatTarget target = (FormatTarget) testCase[0];

                string input = testCase[1] as string;
                string expectedOutput = (string) testCase[2];

                string desc = string.Format("FormatTarget.{0}, Input=[{1}]", target, input);
                LosConvert testConveter = new LosConvert(target);

                string output = testConveter.ToMoneyVarCharStringRep(input);

                Assert.AreEqual(expectedOutput, output, desc);
            }
        }

        [Test]
        public void ToRateStringTest() 
        {
            // 10/21/2008 dd - Test has following format { FormatTarget, input, expectedOutput }
            object[][] testCases = 
            {
                new object[] { FormatTarget.Webform, 0.0M, "0.000%"},
                new object[] { FormatTarget.Webform, 97.0M, "97.000%"},
                new object[] { FormatTarget.Webform, -97.0M, "-97.000%"},

                // 2/4/2011 dd - Add for banker rounding test
                new object[] { FormatTarget.Webform, 97.1235M, "97.124%"},
                new object[] { FormatTarget.Webform, 97.1245M, "97.124%"},
                new object[] { FormatTarget.Webform, 97.1235001M, "97.124%"},
                new object[] { FormatTarget.Webform, 97.1245001M, "97.125%"}

            };

            foreach (object[] testCase in testCases) 
            {
                FormatTarget target = (FormatTarget) testCase[0];

                decimal input = (decimal) testCase[1] ;
                string expectedOutput = (string) testCase[2];

                string desc = string.Format("FormatTarget.{0}, Input=[{1}]", target, input);
                LosConvert testConveter = new LosConvert(target);

                string output = testConveter.ToRateString(input);

                Assert.AreEqual(expectedOutput, output, desc);
            }
        }

        [Test]
        public void ToCountStringTest() 
        {
            // 10/21/2008 dd - Test has following format { FormatTarget, input, expectedOutput }
            object[][] testCases = 
            {
                new object[] { FormatTarget.Webform, 0, "0"},
                new object[] { FormatTarget.Webform, 123, "123"},
                new object[] { FormatTarget.Webform, 1234, "1234"},
                new object[] { FormatTarget.PrintoutImportantFields, 0, "0"},
                new object[] { FormatTarget.PrintoutNormalFields, 0, ""},
            };

            foreach (object[] testCase in testCases) 
            {
                FormatTarget target = (FormatTarget) testCase[0];

                int input = (int) testCase[1];
                string expectedOutput = (string) testCase[2];

                string desc = string.Format("FormatTarget.{0}, Input=[{1}]", target, input);
                LosConvert testConveter = new LosConvert(target);

                string output = testConveter.ToCountString(input);

                Assert.AreEqual(expectedOutput, output, desc);
            }
        }

        [Test]
        public void ToBigCountStringTest() 
        {
            // 10/21/2008 dd - Test has following format { FormatTarget, input, expectedOutput }
            object[][] testCases = 
            {
                new object[] { FormatTarget.Webform, 0L, "0"},
                new object[] { FormatTarget.Webform, 10L, "10"},
                
            };

            foreach (object[] testCase in testCases) 
            {
                FormatTarget target = (FormatTarget) testCase[0];

                long input = (long) testCase[1];
                string expectedOutput = (string) testCase[2];

                string desc = string.Format("FormatTarget.{0}, Input=[{1}]", target, input);
                LosConvert testConveter = new LosConvert(target);

                string output = testConveter.ToBigCountString(input);

                Assert.AreEqual(expectedOutput, output, desc);
            }
        }


        [Test]
        public void ToMoneyTest() 
        {
            // 10/21/2008 dd - Test has following format { FormatTarget, input, expectedOutput }
            object[][] testCases = 
            {
                new object[] { FormatTarget.Webform, null, 0.0M},
                new object[] { FormatTarget.Webform, "", 0.0M},
                new object[] { FormatTarget.Webform, "Blah", 0.0M},
                new object[] { FormatTarget.Webform, "$100.12", 100.12M},
                new object[] { FormatTarget.Webform, "$1,000.12", 1000.12M},
                new object[] { FormatTarget.Webform, "1000.12", 1000.12M},
                new object[] { FormatTarget.Webform, "($1,000.12)", -1000.12M},
                new object[] { FormatTarget.Webform, "(1000.12)", -1000.12M},
                new object[] { FormatTarget.Webform, "$0.00", 0M},
                new object[] { FormatTarget.Webform, "($0.00)", 0M},
            };

            foreach (object[] testCase in testCases) 
            {
                FormatTarget target = (FormatTarget) testCase[0];

                string input = testCase[1] as string;
                decimal expectedOutput = (decimal) testCase[2];

                string desc = string.Format("FormatTarget.{0}, Input=[{1}]", target, input);
                LosConvert testConveter = new LosConvert(target);

                decimal output = testConveter.ToMoney(input);

                Assert.AreEqual(expectedOutput, output, desc);
            }
        }

        [Test]
        public void ToRateTest() 
        {
            var testCases = new[] {
                new { Target = FormatTarget.Webform, Input=(string) null, ExpectedValue = 0.0M}
                , new { Target = FormatTarget.Webform, Input="", ExpectedValue = 0.0M}
                , new { Target = FormatTarget.Webform, Input="BLAH", ExpectedValue = 0.0M}
                , new { Target = FormatTarget.Webform, Input="100.12", ExpectedValue = 100.12M}
                , new { Target = FormatTarget.Webform, Input="100.12%", ExpectedValue = 100.12M}
                , new { Target = FormatTarget.Webform, Input="10.345%", ExpectedValue = 10.345M}
                , new { Target = FormatTarget.Webform, Input="100.0% ", ExpectedValue = 100M}
                , new { Target = FormatTarget.Webform, Input="  ", ExpectedValue = 0.0M}
                , new { Target = FormatTarget.Webform, Input="  100.123% ", ExpectedValue = 100.123M}
                , new { Target = FormatTarget.Webform, Input="  $100.246", ExpectedValue = 100.246M}
                , new { Target = FormatTarget.Webform, Input="0.000%", ExpectedValue = 0.0M}
                , new { Target = FormatTarget.Webform, Input="-1.000", ExpectedValue = -1M}
                , new { Target = FormatTarget.Webform, Input="-1.000%", ExpectedValue = -1M}
                , new { Target = FormatTarget.Webform, Input="(1.000%)", ExpectedValue = 0.0M}
            };


            foreach (var o in testCases) 
            {
                LosConvert testConveter = new LosConvert(o.Target);
                Assert.AreEqual(o.ExpectedValue, testConveter.ToRate(o.Input), o.ToString());
            }
        }


        [Test]
        public void ToCountTest() 
        {
            var testCases = new[] {
                new { Target = FormatTarget.Webform, Input=(string) null, ExpectedValue = 0}
                , new { Target = FormatTarget.Webform, Input="", ExpectedValue = 0}
                , new { Target = FormatTarget.Webform, Input="BLAH", ExpectedValue = 0}
                , new { Target = FormatTarget.Webform, Input="100.12", ExpectedValue = 100}
                , new { Target = FormatTarget.Webform, Input="100.99", ExpectedValue = 101}
                , new { Target = FormatTarget.Webform, Input="100.50", ExpectedValue = 100}
                , new { Target = FormatTarget.Webform, Input="100.51", ExpectedValue = 101}
                , new { Target = FormatTarget.Webform, Input="100.49", ExpectedValue = 100}
                , new { Target = FormatTarget.Webform, Input="10.345%", ExpectedValue = 10}
                , new { Target = FormatTarget.Webform, Input="100.0% ", ExpectedValue = 100}
                , new { Target = FormatTarget.Webform, Input="  ", ExpectedValue = 0}
                , new { Target = FormatTarget.Webform, Input="  100.123% ", ExpectedValue = 100}
                , new { Target = FormatTarget.Webform, Input="  $100.246", ExpectedValue = 100}
                , new { Target = FormatTarget.Webform, Input="  $99", ExpectedValue = 99}
                , new { Target = FormatTarget.Webform, Input="$100", ExpectedValue = 100}
                , new { Target = FormatTarget.Webform, Input="-12", ExpectedValue = -12}
                , new { Target = FormatTarget.Webform, Input="-1,001", ExpectedValue = -1001}
                , new { Target = FormatTarget.Webform, Input="-1,001-12", ExpectedValue = 0}

            };

            foreach (var o in testCases) 
            {
                LosConvert testConveter = new LosConvert(o.Target);

                Assert.AreEqual(o.ExpectedValue, testConveter.ToCount(o.Input), o.ToString());
            }
        }

        [Test]
        public void ToBigCountTest() 
        {
            // 10/21/2008 dd - Test has following format { FormatTarget, input, expectedOutput }
            var testCases = new[] {
                new { Target = FormatTarget.Webform, Input=(string) null, ExpectedValue = 0L}
                , new { Target = FormatTarget.Webform, Input="", ExpectedValue = 0L}
                , new { Target = FormatTarget.Webform, Input="BLAH", ExpectedValue = 0L}
                , new { Target = FormatTarget.Webform, Input="100.12", ExpectedValue = 100L}
                , new { Target = FormatTarget.Webform, Input="100.99", ExpectedValue = 101L}
                , new { Target = FormatTarget.Webform, Input="100.50", ExpectedValue = 100L}
                , new { Target = FormatTarget.Webform, Input="100.51", ExpectedValue = 101L}
                , new { Target = FormatTarget.Webform, Input="100.49", ExpectedValue = 100L}
                , new { Target = FormatTarget.Webform, Input="10.345%", ExpectedValue = 10L}
                , new { Target = FormatTarget.Webform, Input="100.0% ", ExpectedValue = 100L}
                , new { Target = FormatTarget.Webform, Input="  ", ExpectedValue = 0L}
                , new { Target = FormatTarget.Webform, Input="  100.123% ", ExpectedValue = 100L}
                , new { Target = FormatTarget.Webform, Input="  $1,000.246", ExpectedValue = 1000L}
                , new { Target = FormatTarget.Webform, Input="  $99", ExpectedValue = 99L}
                , new { Target = FormatTarget.Webform, Input="$100", ExpectedValue = 100L}
                , new { Target = FormatTarget.Webform, Input="-12", ExpectedValue = -12L}
                , new { Target = FormatTarget.Webform, Input="-1,001", ExpectedValue = -1001L}
                , new { Target = FormatTarget.Webform, Input="-1,001-12", ExpectedValue = 0L}


            };

            foreach (var o in testCases) 
            {
                LosConvert testConveter = new LosConvert(o.Target);

                Assert.AreEqual(o.ExpectedValue, testConveter.ToBigCount(o.Input), o.ToString());
            }
        }
	}
}
