﻿using System;
using System.Collections.Generic;
using CommonProjectLib.Common.Lib;
using DataAccess;
using LendersOffice.Constants;
using LendOSolnTest.Common;
using LendOSolnTest.Fakes;
using NUnit.Framework;

namespace LendOSolnTest.DataAccessTools
{
    [TestFixture]
    public class LoanFileFieldValidatorTest
    {
        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            LoginTools.LoginAs(TestAccountType.LoTest001);
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
            FakePageDataUtilities.Instance.Reset();
        }

        private LoanFileFieldValidator CreateProtectionValidator(Guid loanId)
        {
            Dictionary<string, WriteFieldRule> fieldWriteSet = new Dictionary<string, WriteFieldRule>(StringComparer.OrdinalIgnoreCase);
            Dictionary<string, IList<ProtectFieldRule>> fieldProtectionSet = new Dictionary<string, IList<ProtectFieldRule>>();
            fieldProtectionSet.Add("sSubmitD", new List<ProtectFieldRule>() { new ProtectFieldRule("sSubmitD") { CanBeUpdated = true } });
            fieldProtectionSet.Add("sOpenedD", new List<ProtectFieldRule>() { new ProtectFieldRule("sOpenedD") { CanBeUpdated = false } });
            fieldProtectionSet.Add("sPpmtPenaltyMon", new List<ProtectFieldRule>() { new ProtectFieldRule("sPpmtPenaltyMon") { CanBeUpdated = true, SpecificValues = new HashSet<string>() { "0", "2" } } });
            fieldProtectionSet.Add("sSubmitN", new List<ProtectFieldRule>() { new ProtectFieldRule("sSubmitN") { CanBeUpdated = false } });
            fieldProtectionSet.Add("sTrNotes", new List<ProtectFieldRule>() { new ProtectFieldRule("sTrNotes") { CanBeUpdated = true } });
            fieldProtectionSet.Add("sPurchPrice", new List<ProtectFieldRule>() { new ProtectFieldRule("sPurchPrice") { CanBeUpdated = false } });
            fieldProtectionSet.Add("sLAmtLckd", new List<ProtectFieldRule>() { new ProtectFieldRule("sLAmtLckd") { CanBeUpdated = false } });
            fieldProtectionSet.Add("sLT", new List<ProtectFieldRule>() { new ProtectFieldRule("sLT") { CanBeUpdated = true, SpecificValues = new HashSet<string>() { "4", "3", "2" } } });
            return new LoanFileFieldValidator(loanId, fieldWriteSet, fieldProtectionSet);
        }

        private LoanFileFieldValidator CreateWriteFieldValidator(Guid loanId)
        {
            Dictionary<string, IList<ProtectFieldRule>> fieldProtectionSet = new Dictionary<string, IList<ProtectFieldRule>>();
            Dictionary<string, WriteFieldRule> fieldWriteSet = new Dictionary<string, WriteFieldRule>(StringComparer.OrdinalIgnoreCase);
            fieldWriteSet.Add("sLT", new WriteFieldRule("sLT") { SpecificValues = new HashSet<string>(){"0" }});
            return new LoanFileFieldValidator(loanId, fieldWriteSet, fieldProtectionSet);
        }

        private CPageData CreateDataObject()
        {
            return FakePageData.Create();
        }
        
        [Test]
        public void TestIrrelevantFieldProtectionChanges()
        {

            //asume setting cpage data will not throw errors
            CPageData data = CreateDataObject();
            data.InitSave(ConstAppDavid.SkipVersionCheck);
            var validator = CreateProtectionValidator(data.sLId);
            validator.TryToWriteField("sUnderwritingD_rep", () => data.sUnderwritingD_rep, t => data.sUnderwritingD_rep = t, "10/10/2010");
            Assert.That(data.sUnderwritingD_rep, Is.EqualTo("10/10/2010"));
            validator.TryToWriteField("sUnderwritingD_rep", () => data.sUnderwritingD_rep, t => data.sUnderwritingD_rep = t, "");
            Assert.That(data.sUnderwritingD_rep, Is.EqualTo(""));

            validator.TryToWriteField("sIsBranchActAsLenderForFileTri", () => data.sIsBranchActAsLenderForFileTri, t => data.sIsBranchActAsLenderForFileTri = t, E_TriState.No);
            Assert.That(data.sIsBranchActAsLenderForFileTri, Is.EqualTo(E_TriState.No));

            validator.TryToWriteField("sIsBranchActAsOriginatorForFileTri", () => data.sIsBranchActAsOriginatorForFileTri, t => data.sIsBranchActAsOriginatorForFileTri = t, E_TriState.Yes);
            Assert.That(data.sIsBranchActAsOriginatorForFileTri, Is.EqualTo(E_TriState.Yes));

            //validator.TryToWriteField("sBranchChannelT", () => data.sBranchChannelT, t => data.sBranchChannelT = t, E_BranchChannelT.Blank);
            //Assert.That(data.sBranchChannelT, Is.EqualTo(E_BranchChannelT.Blank));

            //validator.TryToWriteField("sBranchChannelT", () => data.sBranchChannelT, t => data.sBranchChannelT = t, E_BranchChannelT.Broker);
            //Assert.That(data.sBranchChannelT, Is.EqualTo(E_BranchChannelT.Broker));

            validator.TryToWriteField("sApprVal_rep", () => data.sApprVal_rep, t => data.sApprVal_rep = t, "");
            Assert.That(data.sApprVal_rep,Is.EqualTo( "$0.00"));

            validator.TryToWriteField("sApprVal_rep", () => data.sApprVal_rep, t => data.sApprVal_rep = t, "100");
            Assert.That(data.sApprVal_rep, Is.EqualTo("$100.00"));

            validator.TryToWriteField("sFinMethDesc", () => data.sFinMethDesc, t => data.sFinMethDesc = t, "");
            Assert.That(data.sFinMethDesc, Is.EqualTo(""));

            validator.TryToWriteField("sFinMethDesc", () => data.sFinMethDesc, t => data.sFinMethDesc = t, "Test");
            Assert.That(data.sFinMethDesc, Is.EqualTo("Test"));

            validator.TryToWriteField("sIsOptionArm", () => data.sIsOptionArm, t => data.sIsOptionArm = t, true);
            Assert.That(data.sIsOptionArm, Is.EqualTo(true));

            validator.TryToWriteField("sIsOptionArm", () => data.sIsOptionArm, t => data.sIsOptionArm = t, false);

            Assert.That(data.sIsOptionArm, Is.EqualTo(false));

            data.Save();

            data.InitLoad();
            Assert.That(data.sUnderwritingD_rep, Is.EqualTo(""));
            Assert.That(data.sBranchChannelT, Is.EqualTo(E_BranchChannelT.Broker));

            Assert.That(data.sApprVal_rep, Is.EqualTo("$100.00"));
            Assert.That(data.sFinMethDesc, Is.EqualTo("Test"));
            Assert.That(data.sIsOptionArm, Is.EqualTo(false));
            
        }

        [Test]
        public void TestRelevantFieldProtectionChangesThatShouldPass()
        {
            //asume setting cpage data will not throw errors
            CPageData data = CreateDataObject();
            data.InitSave(ConstAppDavid.SkipVersionCheck);
            var validator = CreateProtectionValidator(data.sLId);

            validator.TryToWriteField("sSubmitD_rep", () => data.sSubmitD_rep, t => data.sSubmitD_rep = t, "10/10/2010");  //this date can be updatdd 
            Assert.That(data.sSubmitD_rep, Is.EqualTo("10/10/2010"));



            validator.TryToWriteField("sPpmtPenaltyMon", () => data.sPpmtPenaltyMon, t => data.sPpmtPenaltyMon = t, 1);  //this enum can be changed to 1 
            Assert.That(data.sPpmtPenaltyMon, Is.EqualTo(1));



            validator.TryToWriteField("sLT", () => data.sLT, t => data.sLT = t, E_sLT.FHA);  //this enum can be changed to 0 
            Assert.That(data.sLT, Is.EqualTo(E_sLT.FHA));


            validator.TryToWriteField("sTrNotes", () => data.sTrNotes, t => data.sTrNotes = t, "asd");
            Assert.That(data.sTrNotes, Is.EqualTo("asd"));

            validator.TryToWriteField("sTrNotes", () => data.sTrNotes, t => data.sTrNotes = t, "asdjsd");
            Assert.That(data.sTrNotes, Is.EqualTo("asdjsd"));

            Assert.That(validator.CanSave, Is.True);

        }


        [Test]
        public void TestSettingProtectedBool()
        {
            //asume setting cpage data will not throw errors
            CPageData data = CreateDataObject();
            data.InitSave(ConstAppDavid.SkipVersionCheck);
            var validator = CreateProtectionValidator(data.sLId);

            data.sLAmtLckd = false;
            validator.TryToWriteField("sLAmtLckd", () => data.sLAmtLckd, t => data.sLAmtLckd = t, true);
            validator.TryToWriteField("sLAmtLckd", () => data.sLAmtLckd, t => data.sLAmtLckd = t, true);
            //validator.CheckProtectedFields(new KeyValuePair<string, string>[] { new KeyValuePair<string, string>("sLAmtLckd", "false") });
            validator.CheckProtectedFields(new Tuple<string, FieldOriginalCurrentItem>[] { Tuple.Create("sLAmtLckd", new FieldOriginalCurrentItem() { Current = "false", Original="True"})});
            Assert.That(validator.CanSave, Is.False);
        }


        [Test]
        public void TestClearingProtectedDate()
        {
            //asume setting cpage data will not throw errors
            CPageData data = CreateDataObject();
            data.InitSave(ConstAppDavid.SkipVersionCheck);
            data.sSubmitD_rep = "10/10/2003";
            var validator = CreateProtectionValidator(data.sLId);

            validator.TryToWriteField("sSubmitD_rep", () => data.sSubmitD_rep, t => data.sSubmitD_rep = t, "");
            validator.CheckProtectedFields(new Tuple<string, FieldOriginalCurrentItem>[] { Tuple.Create("sSubmitD_rep", new FieldOriginalCurrentItem() { Current = "", Original = "10/10/2003" }) });
            //validator.CheckProtectedFields(new KeyValuePair<string, string>[] { new KeyValuePair<string, string>("sSubmitD_rep", "") });
            Assert.That(validator.CanSave, Is.False);
        }

        [Test]
        public void TestClearingProtectedTextField()
        {
            //asume setting cpage data will not throw errors
            CPageData data = CreateDataObject();
            data.InitSave(ConstAppDavid.SkipVersionCheck);
            data.sSubmitN = "asdasdsad";
            var validator = CreateProtectionValidator(data.sLId);

            validator.TryToWriteField("sSubmitN", () => data.sSubmitN, t => data.sSubmitN = t, "");
            validator.CheckProtectedFields(new Tuple<string, FieldOriginalCurrentItem>[] { Tuple.Create("sSubmitD_rep", new FieldOriginalCurrentItem() { Current = "", Original = "asdasdsad" }) });
            Assert.That(validator.CanSave, Is.False);

        }

        [Test]
        public void TestNotChangingProtectedField()
        {
            //asume setting cpage data will not throw errors
            CPageData data = CreateDataObject();
            data.InitSave(ConstAppDavid.SkipVersionCheck);
            data.sPpmtPenaltyMon = 0;
            var validator = CreateProtectionValidator(data.sLId);

            validator.TryToWriteField("sPpmtPenaltyMon", () => data.sPpmtPenaltyMon, t => data.sPpmtPenaltyMon = t, 0);  //this enum cannot be change to 0
            Assert.That(validator.CanSave, Is.True);

        }


        [Test]
        public void TestSettingMoneyValueNotFormmatedToSameAmmount()
        {
            //asume setting cpage data will not throw errors
            CPageData data = CreateDataObject();
            data.InitSave(ConstAppDavid.SkipVersionCheck);
            data.sPurchPrice_rep = "100000";

            Assert.That(data.sPurchPrice_rep, Is.Not.EqualTo("100000"));
            var validator = CreateProtectionValidator(data.sLId);

            validator.TryToWriteField("sPurchPrice_rep", () => data.sPurchPrice_rep, t => data.sPurchPrice_rep = t, "100000");  //this enum cannot be change to 0
            Assert.That(validator.CanSave, Is.True);

        }

        [Test]
        public void settingsOptionArmTeaserRShouldNotFail()
        {
            CPageData data = CreateDataObject();
            data.InitSave(ConstAppDavid.SkipVersionCheck);
            data.sIsOptionArm = false;
            var validator = CreateProtectionValidator(data.sLId);

                var a = data.sOptionArmTeaserR;
            validator.TryToWriteField("sOptionArmTeaserR", () => data.sOptionArmTeaserR, t => data.sOptionArmTeaserR = t, 10m);
            Assert.That(validator.CanSave, Is.True);


        }


        [Test]
        public void EnsureUpdateToSltWriteFieldProtection()
        {
            CPageData data = CreateDataObject();
            data.InitSave(ConstAppDavid.SkipVersionCheck);

            var validator = CreateWriteFieldValidator(data.sLId);
            validator.TryToWriteField("sPurchPrice_rep", () => data.sPurchPrice_rep, t => data.sPurchPrice_rep = t, "100000");
            Assert.That(validator.CanSave, Is.False);
        }

        [Test]
        public void EnsureUpdateToSltGoesThroughWriteFieldProtection()
        {
            CPageData data = CreateDataObject();
            data.InitSave(ConstAppDavid.SkipVersionCheck);
            data.sLT = E_sLT.FHA;

            var validator = CreateWriteFieldValidator(data.sLId);
            validator.TryToWriteField("sLT", () => data.sLT, t => data.sLT = t, E_sLT.Conventional);
            Assert.That(data.sLT, Is.EqualTo(E_sLT.Conventional));
        }

        [Test]
        public void WriteEnumsSHouldNotBeSetToDiffValues()
        {
            CPageData data = CreateDataObject();
            data.InitSave(ConstAppDavid.SkipVersionCheck);
            data.sLT = E_sLT.UsdaRural;
            var validator = CreateWriteFieldValidator(data.sLId);
            validator.TryToWriteField("sLT", () => data.sLT, t => data.sLT = t, E_sLT.FHA);
            Assert.That(validator.CanSave, Is.False);
        }
    }
}
