/// Author: David Dao

using System;

using NUnit.Framework;
using DataAccess;
namespace LendOSolnTest.DataAccessTools
{
    [TestFixture]
	public class HashOfPasswordTest
	{
        [Test]
        public void HashOfPassword() 
        {
            string pwd = "HelloWorld";

            string hash = DataAccess.Tools.HashOfPassword(pwd);
            string hashUpperCase = Tools.HashOfPassword(pwd.ToUpper());
            string hashLowerCase = Tools.HashOfPassword(pwd.ToLower());
            string hashWithSpace = Tools.HashOfPassword(pwd + " ");

            Assert.AreEqual(hash, hashUpperCase, "HashOfPassword must be insensitive for upper case.");
            Assert.AreEqual(hash, hashLowerCase, "HashOfPassword must be insensitive for lower case.");
            Assert.AreNotEqual(hash, hashWithSpace, "Space in password must make different.");

        }
        [Test]
        public void PreserveExistingHashPassword() 
        {
            string hash = "nriMP1Mjp4KBydZdalC4zpF8PZYmDNzgH+iKZXLcpI4="; // This is a hash for 'lodblodb2'.

            string pwd = "lodblodb2";

            string newHash = Tools.HashOfPassword(pwd);

            Assert.AreEqual(hash, newHash, "HashOfPassword must maintain existing value.");
        }
	}
}
