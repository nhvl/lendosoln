﻿namespace LendOSolnTest.DataAccessTools
{
    using System.Linq;
    using DataAccess;
    using DataAccess.PathDispatch;
    using LendersOffice.Constants;
    using LendOSolnTest.Common;
    using LendOSolnTest.Fakes;
    using NUnit.Framework;
    using LqbGrammar.DataTypes.PathDispatch;

    [TestFixture]
    public class PathResolverTest
    {
        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            LoginTools.LoginAs(TestAccountType.LoTest001);
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
            FakePageDataUtilities.Instance.Reset();
        }

        [Test]
        public void SimplePathTest()
        {
            CPageData dataLoan = FakePageData.Create();

            dataLoan.InitLoad();
            dataLoan.sLAmtLckd = true;
            dataLoan.sLAmtCalc = 100000.00M;

            string loanAmtRep = dataLoan.sLAmtCalc_rep;

            Assert.AreEqual("$100,000.00", loanAmtRep);
            Assert.AreEqual(loanAmtRep, PathResolver.Get(dataLoan, DataPath.Create("sLAmtCalc")));
        }

        [Test]
        public void PreparerPathTest()
        {
            CPageData dataLoan = FakePageData.Create();

            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.ReturnEmptyObject);

            preparer.IsLocked = true;
            preparer.CellPhone = "(714) 957-6334";
            preparer.Update();
            dataLoan.Save();

            dataLoan.InitLoad();

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.ReturnEmptyObject);

            string preparerCell = preparer.CellPhone;

            Assert.AreEqual(preparerCell, PathResolver.Get(dataLoan, DataPath.Create("Preparer[App1003Interviewer].CellPhone")));
            Assert.AreEqual("(714) 957-6334", preparerCell);
        }

        [Test]
        public void AgentPathTest()
        {
            CPageData dataLoan = FakePageData.Create();

            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            CAgentFields agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Appraiser, E_ReturnOptionIfNotExist.CreateNew);

            agent.City = "Costa Mesa";
            agent.Update();
            dataLoan.Save();

            dataLoan.InitLoad();

            Assert.AreEqual("Costa Mesa", PathResolver.Get(dataLoan, DataPath.Create("Agent[Appraiser].City")));
        }

        [Test]
        public void AppPathTest()
        {
            CPageData dataLoan = FakePageData.Create(appCount: 2);

            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            CAppData app1 = dataLoan.GetAppData(0);

            app1.aBSsn = "123-45-6789";
            app1.aBFirstNm = "Alice";
            app1.aBLastNm = "America";

            CAppData app2 = dataLoan.GetAppData(1);

            app2.aBSsn = "987-65-4321";
            app2.aBFirstNm = "Jonathan";
            app2.aBLastNm = "Consumer";

            dataLoan.Save();

            dataLoan.InitLoad();

            Assert.AreEqual("Alice America", PathResolver.Get(dataLoan, DataPath.Create("App[0].aBNm")));
            Assert.AreEqual("Jonathan Consumer", PathResolver.Get(dataLoan, DataPath.Create("App[1].aBNm")));   
        }

        [Test]
        public void BorrowerClosingCostTest()
        {
            CPageData dataLoan = FakePageData.Create();

            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sClosingCostFeeVersionT = E_sClosingCostFeeVersionT.ClosingCostFee2015;
            dataLoan.sLOrigFMb = 1500;
            dataLoan.Save();
            dataLoan.InitLoad();

            var closingCostSet = dataLoan.sClosingCostSet;
            var brokerFee = (LoanClosingCostFee)closingCostSet.FindFeeByHudline(801).First();

            brokerFee.BaseAmount = 1500;

            Assert.AreEqual("Loan origination fee", PathResolver.Get(dataLoan, DataPath.Create($"BorrowerClosingCostSet[{DefaultSystemClosingCostFee.Hud800LoanOriginationFeeTypeId}].Description")));
            Assert.AreEqual("$1,500.00", PathResolver.Get(dataLoan, DataPath.Create($"BorrowerClosingCostSet[{DefaultSystemClosingCostFee.Hud800LoanOriginationFeeTypeId}].TotalAmount")));
            Assert.AreEqual("$1,500.00", PathResolver.Get(dataLoan, DataPath.Create($"BorrowerClosingCostSet[{DefaultSystemClosingCostFee.Hud800LoanOriginationFeeTypeId}].Payment[0].Amount")));
        }
    }
}
