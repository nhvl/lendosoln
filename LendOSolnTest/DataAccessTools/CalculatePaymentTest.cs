﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using DataAccess;

namespace LendOSolnTest.DataAccessTools
{
    [TestFixture]
    public class CalculatePaymentTest
    {
        [Test]
        public void PaymentTest()
        {
            var testData = new[] {
                new { LoanAmount = 106605M, Interest = 7.5M, Term = 360, ExpectedPayment = 745.4M}
                , new { LoanAmount = 109003.61M, Interest = 7.5M, Term = 360, ExpectedPayment = 762.17M}
            };

            foreach (var o in testData)
            {
                Assert.AreEqual(o.ExpectedPayment, Tools.CalculatePayment(o.LoanAmount, o.Interest, o.Term), "LoanAmt=" + o.LoanAmount + ", Interest=" + o.Interest + ", Term=" + o.Term);
            }
        }
    }
}
