﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using LendOSolnTest.Common;
using LendersOffice.Security;
using DataAccess;

namespace LendOSolnTest.DataAccessTools
{
    [TestFixture]
    public class CreateUsingSmartDependencyTest
    {
        Guid m_loanId = Guid.Empty;

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            LoginTools.LoginAs(TestAccountType.LoTest001);

            BrokerUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;

            CLoanFileCreator creator = CLoanFileCreator.GetCreator(principal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);

            m_loanId = creator.CreateBlankLoanFile();
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            if (m_loanId != Guid.Empty)
            {
                Tools.DeclareLoanFileInvalid(BrokerUserPrincipal.CurrentPrincipal, m_loanId, false, false);
            }
        }

        [Test]
        public void NormalTest()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(m_loanId, typeof(CreateUsingSmartDependencyTest));
            dataLoan.InitLoad();

            string sLNm = dataLoan.sLNm;
        }

        [Test]
        public void EmptyDependencyTest()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(m_loanId, typeof(CreateUsingSmartDependencyTest));
            dataLoan.InitLoad();
        }

        [Test]
        public void InterclassDependencyTest()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(m_loanId, typeof(CreateUsingSmartDependencyTest));
            dataLoan.InitLoad();
            Foo.Print(dataLoan);
        }

        [Test]
        public void CircularDependencyTest()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(m_loanId, typeof(CreateUsingSmartDependencyTest));
            dataLoan.InitLoad();
            Foo.DoSomething(dataLoan);
        }

        [Test]
        public void ConstructorDependencyTest()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(m_loanId, typeof(CreateUsingSmartDependencyTest));
            dataLoan.InitLoad();
            new Baz(dataLoan);
        }

        private class Foo
        {
            static public void Print(CPageData pageData)
            {
                var tmp = pageData.sLAmtCalc_rep;
                Tools.LogInfo($"Foo.Print(CPageData): sLNm = {pageData.sLNm}");
                Bar.Print(pageData);
            }

            // Circular calls: Foo.DoSomething() calls Bar.DoSomething() and Bar.DoSomething() calls back Foo.DoSomething()
            static public void DoSomething(CPageData pageData, int counter = 2)
            {
                if (counter > 0)
                {
                    var tmp = pageData.sFannieProdDesc;
                    Bar.DoSomething(pageData, counter - 1);
                }
            }

        }

        private class Bar
        {
            static public void Print(CPageData pageData)
            {
                Tools.LogInfo($"Bar.Print(CPageData): sFinalLAmt={pageData.sFinalLAmt}");
            }
            static public void DoSomething(CPageData pageData, int counter)
            {
                if (counter > 0)
                {
                    var tmp = pageData.sFannieMsa;
                    Foo.DoSomething(pageData, counter - 1);
                }
            }
        }

        private class BazBase
        {
            public BazBase(CPageData pageData)
            {
                var sIsFannieNeighbors = pageData.sIsFannieNeighbors;
            }
        }

        private class Baz: BazBase
        {
            public Baz(CPageData pageData) : base(pageData)
            {
                var sIsCommunitySecond = pageData.sIsCommunitySecond;
            }
        }
    }
}
