/// Author: David Dao

using System;

using NUnit.Framework;

using LendersOfficeApp.los.RatePrice;

namespace LendOSolnTest.DataAccessTools
{
    [TestFixture]
    public class SnapupOneEightTest
    {
        [Test]
        public void SnapupOneEight() 
        {
            // Format {input, expectedValue}
            decimal[,] testCases = {
                {-5.8263M, -5.75M}
                                       ,{-5.826299223M, -5.75M}
                                       ,{-5.76245611M, -5.75M}
                                       ,{-5.69415816M, -5.625M}
                                       ,{-5.613701592M, -5.5M}
                                       ,{-5.51848397M, -5.5M}
                                       ,{-5.41641994M, -5.375M}
                                       ,{-5.31222372M, -5.25M}
                                       ,{-5.195436045M, -5.125M}
                                       ,{-4.951080676M, -4.875M}
                                       ,{-4.704407095M, -4.625M}
                                       ,{-4.454557053M, -4.375M}
                                       ,{-4.191322032M, -4.125M}
                                       ,{-3.91229371M, -3.875M}
                                       ,{-3.61430952M, -3.5M}
                                       ,{-3.293102956M, -3.25M}
                                       ,{-2.7344375M, -2.625M}
                                       ,{-2.2031875M, -2.125M}
                                       ,{-1.6719375M, -1.625M}
                                       ,{-1.1094375M, -1M}
                                       ,{-0.5469375M, -0.5M}
                                       ,{0.0780625M, 0.125M}
                                       ,{105.8262992M, 105.875M}
                                       ,{105.7624561M, 105.875M}
                                       ,{105.6941582M, 105.75M}
                                       ,{105.6137016M, 105.625M}
                                       ,{105.518484M, 105.625M}
                                       ,{105.4164199M, 105.5M}
                                       ,{105.3122237M, 105.375M}
                                       ,{105.195436M, 105.25M}
                                       ,{104.9510807M, 105.0M}
                                       ,{104.7044071M, 104.75M}
                                       ,{104.4545571M, 104.5M}
                                       ,{104.191322M, 104.25M}
                                       ,{103.9122937M, 104.0M}
                                       ,{103.0M, 103.0M}
                                       ,{103.125M, 103.125M}
                                       ,{-2.128M, -2.125M}
                                       ,{1.123M, 1.125M}
                                   };

            int numOfTestCases = testCases.Length / 2;
            for (int i = 0; i < numOfTestCases; i++) 
            {
                Assert.AreEqual(testCases[i,1], CApplicantPrice.Snapup(testCases[i, 0], .125M), "Input:" + testCases[i, 0]);
            }

        }
    }
}
