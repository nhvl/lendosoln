﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace LendOSolnTest.DataAccessTools
{
    [TestFixture]
    public class UserDataValidationTest
    {
        [Test]
        public void BlankOrNullEmailIsNotValid()
        {
            Assert.That(DataAccess.UserDataValidation.IsValidEmailAddress(""), Is.False, "Empty string is not suppose to be a valid email");
            Assert.That(DataAccess.UserDataValidation.IsValidEmailAddress(null), Is.False, "Null string is not suppose to be a valid email");
        }

        [Test]
        public void EmailsThatAreInvalid()
        {
            Assert.That(DataAccess.UserDataValidation.IsValidEmailAddress("bob"), Is.False, "Single word bob is not suppose to be a valid email address.");
            Assert.That(DataAccess.UserDataValidation.IsValidEmailAddress("bob@"), Is.False, "Single word bob is not suppose to be a valid email address.");
            Assert.That(DataAccess.UserDataValidation.IsValidEmailAddress("bob@meridianlink"), Is.False, "Single word bob is not suppose to be a valid email address.");
            
        }

        [Test]
        public void EmailIsValid()
        {
            Assert.That(DataAccess.UserDataValidation.IsValidEmailAddress("bob@meridianlink.com"), Is.True, "bob@meridianlink.com is a valid email");
            Assert.That(DataAccess.UserDataValidation.IsValidEmailAddress("bob@gmaiil.com"), Is.True, "bob@gmaiil.com is valid");
            Assert.That(DataAccess.UserDataValidation.IsValidEmailAddress("bob@yahoo.com"), Is.True, "bob@yahoo.com is valid");

        }
        
    }

}
