﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using System.Xml;
using DataAccess;

namespace LendOSolnTest.DataAccessTools
{
    [TestFixture]
    public class CreateXmlDocTest
    {
        [Test]
        public void TestCreateXmlDoc()
        {
            var testCaseList = new[] {
                new { Xml = "<test>abcdef</test>", ExpectedXml = "<test>abcdef</test>"},
                new { Xml = "<test>abcdef</test>", ExpectedXml = "<test>abcdef</test>"}
            };

            foreach (var testCase in testCaseList)
            {
                XmlDocument doc = Tools.CreateXmlDoc(testCase.Xml);
                Assert.AreEqual(testCase.ExpectedXml, doc.OuterXml);
            }
        }
        [Test]
        [ExpectedException(ExpectedException =typeof(System.Xml.XmlException))]
        public void TestCreateXmlDocException()
        {
            Tools.CreateXmlDoc("<test>abc");
        }
    }
}
