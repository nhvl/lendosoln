﻿using DataAccess;
using LendOSolnTest.Common;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LendOSolnTest.DataAccessTools
{
    [TestFixture]
    public class AutoExpiredTextCacheTest
    {
        [Test]
        public void TestAddShortStringToCacheDB()
        {
            string data = "This is a small cache entry that should fit in the database.";
            string id = AutoExpiredTextCache.AddToCache(data, TimeSpan.FromMinutes(5));

            string cacheData = AutoExpiredTextCache.GetFromCache(id);

            Assert.That(data.Equals(cacheData));

            id = Guid.NewGuid().ToString();
            AutoExpiredTextCache.AddToCache(data, TimeSpan.FromMinutes(5), id);
            cacheData = AutoExpiredTextCache.GetFromCache(id);

            Assert.That(data.Equals(cacheData));

        }

        [Test]
        public void TestAddLongStringToCacheDB()
        {
            string path = TestUtilities.GetFilePath("sarah-testcase.xml");
            string data = File.ReadAllText(path);

            Assert.That(data.Length > 5500, "Data is not bigger than cache limit.");

            string id = AutoExpiredTextCache.AddToCache(data, TimeSpan.FromMinutes(5));

            string cacheData = AutoExpiredTextCache.GetFromCache(id);
            Assert.That(data.Equals(cacheData));

            id = Guid.NewGuid().ToString();
            AutoExpiredTextCache.AddToCache(data, TimeSpan.FromMinutes(5), id);

            cacheData = AutoExpiredTextCache.GetFromCache(id);
            Assert.That(data.Equals(cacheData));
        }

        [Test]
        public void TestAddFileToCache()
        {
            string fileLocation = TestUtilities.GetFilePath("TestWithOwnerPassword.pdf");
            string id = Guid.NewGuid().ToString();
            AutoExpiredTextCache.AddFileToCache(fileLocation, TimeSpan.FromMinutes(5), id);

            byte[] data = AutoExpiredTextCache.GetBytesFromCache(id);
            var tempFile = TempFileUtils.NewTempFile();
            File.WriteAllBytes(tempFile.Value, data);

            Assert.That(FilesAreEqual(new FileInfo(fileLocation), new FileInfo(tempFile.Value)));
        }

        [Test]
        [ExpectedException]
        public void TestCacheIdLimit()
        {
            string id = RandomString(200);
            AutoExpiredTextCache.AddToCache("tesT", TimeSpan.FromMinutes(5), id);
        }


        [Test]
        [ExpectedException]
        public void TestCacheTimeLimit()
        {
            string id = Guid.NewGuid().ToString();
            AutoExpiredTextCache.AddToCache("tesT", TimeSpan.FromDays(10), id);
        }


        /// <summary>
        /// Ensure two files are identical. This is from SO http://stackoverflow.com/questions/1358510/how-to-compare-2-files-fast-using-net
        /// </summary>
        /// <param name="first">The first file to compare.</param>
        /// <param name="second">The second file to compare.</param>
        /// <returns>A bool indicating whether the two files match.</returns>
        private static bool FilesAreEqual(FileInfo first, FileInfo second)
        {
            int bytesToRead = 4096;
            if (first.Length != second.Length)
                return false;

            int iterations = (int)Math.Ceiling((double)first.Length / bytesToRead);

            using (FileStream fs1 = first.OpenRead())
            using (FileStream fs2 = second.OpenRead())
            {
                byte[] one = new byte[bytesToRead];
                byte[] two = new byte[bytesToRead];

                for (int i = 0; i < iterations; i++)
                {
                    fs1.Read(one, 0, bytesToRead);
                    fs2.Read(two, 0, bytesToRead);

                    if (BitConverter.ToInt64(one, 0) != BitConverter.ToInt64(two, 0))
                        return false;
                }
            }

            return true;
        }

        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
