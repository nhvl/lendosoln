/// Author: David Dao

using System;
using DataAccess;
using NUnit.Framework;

namespace LendOSolnTest.DataAccessTools
{

    [TestFixture]
    public class LosConvertGfeItemPropsTest
    {
        [Test]
        public void CombinationTest() 
        {
            // 10/20/2008 dd - Test all possible combinations.
            bool[] aprChoices = {true, false};
            bool[] brChoices = {true, false};
            int[] payerChoices = {0, 1, 2, 3};
            bool[] fhaAllowChoices = {true, false};
            bool[] pocChoices = {true, false};
            bool[] dflpChoices = { true, false };
            bool[] trdPtyChoices = { true, false };
            bool[] affChoices = { true, false };
            bool[] qmWarnChoices = { true, false };


            foreach (bool apr in aprChoices) 
            {
                foreach (bool br in brChoices) 
                {
                    foreach (int payer in payerChoices) 
                    {
                        foreach (bool fhaAllow in fhaAllowChoices) 
                        {
                            foreach (bool poc in pocChoices) 
                            {
                                foreach (bool dflp in dflpChoices)
                                {
                                    foreach (bool trdPty in trdPtyChoices)
                                    {
                                        foreach (bool aff in affChoices)
                                        {
                                            foreach (bool qmWarn in qmWarnChoices)
                                            {
                                                LosConvert convert = new LosConvert();
                                                int result = LosConvert.GfeItemProps_Pack(apr, br, payer, fhaAllow, poc, dflp, trdPty, aff, qmWarn);

                                                Verify(apr, br, payer, fhaAllow, poc, dflp, trdPty, aff, qmWarn, result, convert);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        [Test]
        public void CompatibilityTest() 
        {
            // 10/20/2008 dd - This test will verify parsing against pre-generate result. This to make sure new algorithm will not break old value.
            // The format is {apr,br,payer,fhaAllow,poc,result}
            #region Data
            object[][] existingValues = {
                                            new object[] {true, true, 0, true, true, false, 4115},
                                            new object[] {true, true, 0, true, false, false, 4113},
                                            new object[] {true, true, 0, false, true, false, 19},
                                            new object[] {true, true, 0, false, false, false, 17},
                                            new object[] {true, true, 1, true, true, false, 4371},
                                            new object[] {true, true, 1, true, false, false, 4369},
                                            new object[] {true, true, 1, false, true, false, 275},
                                            new object[] {true, true, 1, false, false, false, 273},
                                            new object[] {true, true, 2, true, true, false, 4627},
                                            new object[] {true, true, 2, true, false, false, 4625},
                                            new object[] {true, true, 2, false, true, false, 531},
                                            new object[] {true, true, 2, false, false, false, 529},
                                            new object[] {true, true, 3, true, true, false, 4883},
                                            new object[] {true, true, 3, true, false, false, 4881},
                                            new object[] {true, true, 3, false, true, false, 787},
                                            new object[] {true, true, 3, false, false, false, 785},
                                            new object[] {true, false, 0, true, true, false, 4099},
                                            new object[] {true, false, 0, true, false, false, 4097},
                                            new object[] {true, false, 0, false, true, false, 3},
                                            new object[] {true, false, 0, false, false, false, 1},
                                            new object[] {true, false, 1, true, true, false, 4355},
                                            new object[] {true, false, 1, true, false, false, 4353},
                                            new object[] {true, false, 1, false, true, false, 259},
                                            new object[] {true, false, 1, false, false, false, 257},
                                            new object[] {true, false, 2, true, true, false, 4611},
                                            new object[] {true, false, 2, true, false, false, 4609},
                                            new object[] {true, false, 2, false, true, false, 515},
                                            new object[] {true, false, 2, false, false, false, 513},
                                            new object[] {true, false, 3, true, true, false, 4867},
                                            new object[] {true, false, 3, true, false, false, 4865},
                                            new object[] {true, false, 3, false, true, false, 771},
                                            new object[] {true, false, 3, false, false, false, 769},
                                            new object[] {false, true, 0, true, true, false, 4114},
                                            new object[] {false, true, 0, true, false, false, 4112},
                                            new object[] {false, true, 0, false, true, false, 18},
                                            new object[] {false, true, 0, false, false, false, 16},
                                            new object[] {false, true, 1, true, true, false, 4370},
                                            new object[] {false, true, 1, true, false, false, 4368},
                                            new object[] {false, true, 1, false, true, false, 274},
                                            new object[] {false, true, 1, false, false, false, 272},
                                            new object[] {false, true, 2, true, true, false, 4626},
                                            new object[] {false, true, 2, true, false, false, 4624},
                                            new object[] {false, true, 2, false, true, false, 530},
                                            new object[] {false, true, 2, false, false, false, 528},
                                            new object[] {false, true, 3, true, true, false, 4882},
                                            new object[] {false, true, 3, true, false, false, 4880},
                                            new object[] {false, true, 3, false, true, false, 786},
                                            new object[] {false, true, 3, false, false, false, 784},
                                            new object[] {false, false, 0, true, true, false, 4098},
                                            new object[] {false, false, 0, true, false, false, 4096},
                                            new object[] {false, false, 0, false, true, false, 2},
                                            new object[] {false, false, 0, false, false, false, 0},
                                            new object[] {false, false, 1, true, true, false, 4354},
                                            new object[] {false, false, 1, true, false, false, 4352},
                                            new object[] {false, false, 1, false, true, false, 258},
                                            new object[] {false, false, 1, false, false, false, 256},
                                            new object[] {false, false, 2, true, true, false, 4610},
                                            new object[] {false, false, 2, true, false, false, 4608},
                                            new object[] {false, false, 2, false, true, false, 514},
                                            new object[] {false, false, 2, false, false, false, 512},
                                            new object[] {false, false, 3, true, true, false, 4866},
                                            new object[] {false, false, 3, true, false, false, 4864},
                                            new object[] {false, false, 3, false, true, false, 770},
                                            new object[] {false, false, 3, false, false, false, 768},

            };
            #endregion;

            for (int i = 0; i < existingValues.Length; i++) 
            {
                object[] record = existingValues[i];
                int result = (int) record[6];
                bool apr = (bool) record[0];
                bool br = (bool) record[1];
                int payer = (int) record[2];
                bool fhaAllow = (bool) record[3];
                bool poc = (bool) record[4];
                bool dflp = (bool)record[5];
                LosConvert convert = new LosConvert();

                Verify(apr, br, payer, fhaAllow, poc, dflp, false, false, false, result, convert);
            }
        }


        private void Verify(bool apr, bool br, int payer, bool fhaAllow, bool poc, bool dflp, bool trdPty, bool aff, bool qmWarn, int result, LosConvert convert) 
        {
            string inputs = string.Format("apr={0}, br={1}, payer={2}, fhaAllow={3}, poc={4}, dflp={6}, thirdParty={7}, aff={8}, result={5}", apr, br, payer, fhaAllow, poc, result, dflp, trdPty, aff);

            Assert.AreEqual(apr, LosConvert.GfeItemProps_Apr(result), "Apr Failed. " + inputs);
            Assert.AreEqual(br, LosConvert.GfeItemProps_ToBr(result), "Br Failed. " + inputs);
            Assert.AreEqual(payer, LosConvert.GfeItemProps_Payer(result), "Payer Failed. " + inputs);
            Assert.AreEqual(fhaAllow, LosConvert.GfeItemProps_FhaAllow(result), "Fha Allow Failed. " + inputs);
            Assert.AreEqual(poc, LosConvert.GfeItemProps_Poc(result), "POC Failed. " + inputs);
            Assert.AreEqual(dflp, LosConvert.GfeItemProps_Dflp(result), "DFLP Failed. " + inputs);
            Assert.AreEqual(trdPty, LosConvert.GfeItemProps_PaidToThirdParty(result), "TrdPty Failed. " + inputs);
            Assert.AreEqual(trdPty ? aff : false, LosConvert.GfeItemProps_ThisPartyIsAffiliate(result), "Sff Failed. " + inputs);
            Assert.AreEqual(qmWarn, LosConvert.GfeItemProps_ShowQmWarning(result), "QmWarn Failed. " + inputs);
        }
    }


}
