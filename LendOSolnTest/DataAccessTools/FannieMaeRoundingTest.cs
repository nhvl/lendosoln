﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using DataAccess;

namespace LendOSolnTest.DataAccessTools
{
    [TestFixture]
    public class FannieMaeRoundingTest
    {
        [Test]
        public void MainTest()
        {
            var testCases = new[] {
                new { Value=80.001M, ExpectedValue= 80M}
                , new { Value=96.01M, ExpectedValue = 97M}
                , new { Value=80M, ExpectedValue= 80M}
                , new { Value= 85.2345M, ExpectedValue = 86M}
                , new { Value = 96.00999M, ExpectedValue = 96M}
            };

            foreach (var o in testCases)
            {
                Assert.AreEqual(o.ExpectedValue, Tools.FannieMaeLtvRounding(o.Value), "Value=" + o.Value);
            }
        }
    }
}
