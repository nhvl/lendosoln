﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendOSolnTest.Common;
using LendOSolnTest.Fakes;
using NUnit.Framework;

namespace LendOSolnTest.DataAccessTools
{
    [TestFixture]
    public class PageDataUtilitiesTest
    {
        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            LoginTools.LoginAs(TestAccountType.LoTest001);
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
            FakePageDataUtilities.Instance.Reset();
        }

        [Test]
        public void InvalidFieldTestTest()
        {
            string[] fieldList = { "", null, "abcdef"
                                     , "sGfeItems" // Private Fields
                                     , "InitLoad" // Method
                                     , "m_dsLoan" // Protected Member Variable
                                     , "sAgentReSellingName" // Private property
                                 };

            foreach (string id in fieldList)
            {
                E_PageDataFieldType type = E_PageDataFieldType.Unknown;
                bool ret = PageDataUtilities.GetFieldType(id, out type);

                Assert.AreEqual(false, ret, id);
            }
        }

        [Test]
        public void ValidFieldTest()
        {
            var list = new[] {
                new { FieldId = "sLNm", ExpectedFieldType = E_PageDataFieldType.String}
                , new { FieldId = "aBSsn", ExpectedFieldType = E_PageDataFieldType.Ssn}
                , new { FieldId = "sNoteIR", ExpectedFieldType = E_PageDataFieldType.Percent}
                , new { FieldId = "sLAmtCalc", ExpectedFieldType = E_PageDataFieldType.Money}
                , new { FieldId = "aBHPhone", ExpectedFieldType = E_PageDataFieldType.Phone}
                , new { FieldId = "sServicingOtherPmts", ExpectedFieldType = E_PageDataFieldType.Money}
                , new { FieldId = "sGfeTilPrepareDate", ExpectedFieldType = E_PageDataFieldType.DateTime}

            };

            foreach (var o in list)
            {
                E_PageDataFieldType type = E_PageDataFieldType.Unknown;

                bool ret = PageDataUtilities.GetFieldType(o.FieldId, out type);
                Assert.AreEqual(true, ret, o.ToString());
                Assert.AreEqual(o.ExpectedFieldType, type, o.ToString());
            }
        }
        [Test]
        public void TestAllAvailableProperties()
        {
            List<string> ids = new List<string>();

            Type[] typeList = { typeof(CAppData), typeof(CPageBase) };

            foreach (Type type in typeList)
            {
                var propertyList = type.GetProperties(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                foreach (PropertyInfo prop in propertyList)
                {
                    if (prop.Name.EndsWith("_rep"))
                    {
                        // 5/14/2010 dd - Skip _rep name.
                        continue;
                    }
                    ids.Add(prop.Name);
                }
            }

            List<KeyValuePair<string, E_PageDataFieldType>> validList = new List<KeyValuePair<string, E_PageDataFieldType>>();
            List<string> invalidList = new List<string>();
            foreach (string id in ids)
            {
                E_PageDataFieldType type;
                bool ret = PageDataUtilities.GetFieldType(id, out type); // 5/14/2010 dd - There should be no exception.
                if (ret)
                {
                    validList.Add(new KeyValuePair<string, E_PageDataFieldType>(id, type));
                }
                else
                {
                    invalidList.Add(id);
                }
            }

            validList.Sort((a, b) => a.Key.CompareTo(b.Key));
            invalidList.Sort();

            using (StreamWriter stream = new StreamWriter(@"c:\lotemp\fields.txt"))
            {
                foreach (var o in validList)
                {
                    stream.WriteLine(o.Key + " - " + o.Value);
                }
                stream.WriteLine("---------------------------");
                foreach (var o in invalidList)
                {
                    stream.WriteLine(o);
                }
            }

        }

        [Test]
        public void TestInvalidSetFieldIdTest()
        {
            string[] fieldList = { "abcdef"
                                     , "sGfeItems" // Private Fields
                                     , "InitLoad" // Method
                                     , "m_dsLoan" // Protected Member Variable
                                     , "sAgentReSellingName" // Private property
                                 };
            CPageData dataLoan = FakePageData.Create();

            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            CAppData dataApp = dataLoan.GetAppData(0);

            foreach (string id in fieldList)
            {
                try
                {
                    PageDataUtilities.SetValue(dataLoan, dataApp, id, "");
                    Assert.Fail("Set [" + id + "] Should not get here");
                }
                catch (NotFoundException)
                {
                }
            }
        }
        [Test]
        public void TestSetInvalidEnumValueTest()
        {
            string[] fieldList = { "sLT" };

            CPageData dataLoan = FakePageData.Create();

            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            CAppData dataApp = dataLoan.GetAppData(0);

            foreach (string id in fieldList)
            {
                try
                {
                    PageDataUtilities.SetValue(dataLoan, dataApp, id, "999");
                    Assert.Fail("Should not get here");
                }
                catch (FieldInvalidValueException)
                {
                }
            }

        }
        [Test]
        public void TestGetValue()
        {
            var testCases = new[] {
                new { FieldId="sSpAddr", ExpectedValue="111 ADDR"}
                , new { FieldId="sNoteIR", ExpectedValue="6.125"}
                , new { FieldId = "sOpenedD", ExpectedValue="1/1/2010"}
                , new { FieldId = "aBFirstNm", ExpectedValue = "aBFirstNm"}
                , new { FieldId = "aBBaseI", ExpectedValue = "10,000.00"}
                , new { FieldId = "sLPurposeT:2", ExpectedValue = "Yes"}
                , new { FieldId = "sLPurposeT:3", ExpectedValue = "No"}
                , new { FieldId = "aBIsAsian", ExpectedValue = "Yes"}
                , new { FieldId = "aBIsAsian:0", ExpectedValue = "No"}
                , new { FieldId = "aBIsAsian:1", ExpectedValue = "Yes"}
                , new { FieldId = "aCIsAsian", ExpectedValue = "No"}
                , new { FieldId = "aCIsAsian:1", ExpectedValue = "No"}
                , new { FieldId = "aCIsAsian:0", ExpectedValue = "Yes"}
                , new { FieldId = "sLPurposeT", ExpectedValue = E_sLPurposeT.RefinCashout.ToString("D")}
                , new { FieldId = "sLT", ExpectedValue = E_sLT.UsdaRural.ToString("D")}
                , new { FieldId = "sLT_rep", ExpectedValue = "USDA Rural"}
                , new { FieldId = "sTodayDate", ExpectedValue = DateTime.Today.ToShortDateString().ToString()}
                , new { FieldId="GetPreparerOfForm[App1003Interviewer].PreparerName", ExpectedValue="App1003 PreparerName"}
                , new { FieldId="GetPreparerOfForm[App1003Interviewer].LicenseNumOfAgent", ExpectedValue="App1003 LicenseNumOfAgent"}
                , new { FieldId="GetAgentOfRole[Underwriter].AgentName", ExpectedValue="Underwriter Agent Name"}

                            };

            string[] inputFields = new string[testCases.Length];
            for (int i = 0; i < testCases.Length; i++)
            {
                if (testCases[i].FieldId.StartsWith("GetPreparerOfForm"))
                {
                    inputFields[i] = "sfGetPreparerOfForm";
                }
                else if (testCases[i].FieldId.StartsWith("GetAgentOfRole"))
                {
                    inputFields[i] = "sfGetAgentOfRole";
                }
                else
                {
                    inputFields[i] = testCases[i].FieldId;
                }
            }
            CPageData dataLoan = FakePageData.Create();

            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sSpAddr = "111 ADDR";
            dataLoan.sNoteIR_rep = "6.125%";
            dataLoan.sOpenedD_rep = "1/1/2010";
            dataLoan.sLPurposeT = E_sLPurposeT.RefinCashout;
            dataLoan.sLT = E_sLT.UsdaRural;

            IPreparerFields field = dataLoan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.CreateNew);
            field.IsLocked = true;
            field.PreparerName = "App1003 PreparerName";
            field.LicenseNumOfAgent = "App1003 LicenseNumOfAgent";
            field.Update();

            CAgentFields agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Underwriter, E_ReturnOptionIfNotExist.CreateNew);
            agent.AgentName = "Underwriter Agent Name";
            agent.Update();

            CAppData dataApp = dataLoan.GetAppData(0);
            dataApp.aBFirstNm = "aBFirstNm";
            dataApp.aBBaseI_rep = "10,000";
            dataApp.aBIsAsian = true;
            dataApp.aCIsAsian = false;
            
            dataLoan.Save();

            dataLoan = FakePageData.Create();
            dataLoan.InitLoad();
            dataLoan.SetFormatTarget(FormatTarget.PrintoutNormalFields);

            dataApp = dataLoan.GetAppData(0);



            foreach (var o in testCases)
            {
                string ret = PageDataUtilities.GetValue(dataLoan, dataApp, o.FieldId);
                Assert.AreEqual(o.ExpectedValue, ret, o.ToString());
            }

        }

        [TestCase(true, "True")]
        [TestCase(false, "False")]
        public void GetValue_BoolFieldWithLqbDataServiceFormatTarget_ReturnsTrueFalseInsteadOfYesNo(bool isManualUw, string expected)
        {
            var loan = FakePageData.Create();
            loan.SetFormatTarget(FormatTarget.LqbDataService);
            loan.InitSave(ConstAppDavid.SkipVersionCheck);
            loan.sIsManualUw = isManualUw;

            string retrieved = PageDataUtilities.GetValue(loan, null, "sIsManualUw");

            Assert.AreEqual(expected, retrieved);
        }

        [Test]
        public void TestSetValue()
        {
            var testCases = new[] {
                new { FieldId="sSpAddr", ExpectedValue="111 ADDR"}
                , new { FieldId="sNoteIR", ExpectedValue="6.125"}
                , new { FieldId = "sOpenedD", ExpectedValue="1/1/2010"}
                , new { FieldId = "aBFirstNm", ExpectedValue = "aBFirstNm"}
                , new { FieldId = "aBBaseI", ExpectedValue = "10,000.00"}
                , new { FieldId = "sLPurposeT", ExpectedValue = "2"}
                , new { FieldId = "aBIsAsian", ExpectedValue = "True"}
                , new { FieldId = "aCIsAsian", ExpectedValue = "False"}
                , new { FieldId = "sLPurposeT", ExpectedValue = E_sLPurposeT.RefinCashout.ToString("D")}
                , new { FieldId = "sLT", ExpectedValue = E_sLT.UsdaRural.ToString("D")}
                , new { FieldId="GetPreparerOfForm[App1003Interviewer].PreparerName", ExpectedValue="App1003 PreparerName"}
                , new { FieldId="GetPreparerOfForm[App1003Interviewer].LicenseNumOfAgent", ExpectedValue="App1003 LicenseNumOfAgent"}
                , new { FieldId="GetAgentOfRole[Underwriter].AgentName", ExpectedValue="Underwriter Agent Name"}

                            };

            string[] inputFields = new string[testCases.Length];
            for (int i = 0; i < testCases.Length; i++)
            {
                if (testCases[i].FieldId.StartsWith("GetPreparerOfForm"))
                {
                    inputFields[i] = "sfGetPreparerOfForm";
                }
                else if (testCases[i].FieldId.StartsWith("GetAgentOfRole"))
                {
                    inputFields[i] = "sfGetAgentOfRole";
                }
                else
                {
                    inputFields[i] = testCases[i].FieldId;
                }
            }
            CPageData dataLoan = FakePageData.Create();

            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            CAppData dataApp = dataLoan.GetAppData(0);
            
            IPreparerFields App1003Interviewer = dataLoan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.CreateNew);
            App1003Interviewer.IsLocked = true;

            foreach (var testCase in testCases)
            {
                PageDataUtilities.SetValue(dataLoan, dataApp, testCase.FieldId, testCase.ExpectedValue);

            }
            dataLoan.Save();

            dataLoan = FakePageData.Create();
            dataLoan.InitLoad();
            dataLoan.SetFormatTarget(FormatTarget.PrintoutNormalFields);

            dataApp = dataLoan.GetAppData(0);
            foreach (var o in testCases)
            {
                string ret = PageDataUtilities.GetValue(dataLoan, dataApp, o.FieldId);

                string expectedValue = o.ExpectedValue;

                // 10/10/2011 dd - Due to the fact the GetValue return "Yes"/"No" instead of "True"/"False" I have to modify the expected value.
                if (expectedValue == "True")
                {
                    expectedValue = "Yes";
                }
                else if (expectedValue == "False")
                {
                    expectedValue = "No";
                }
                Assert.AreEqual(expectedValue, ret, o.ToString());
            }

        }

    }
}
