/// Author: David Dao

using System;
using NUnit.Framework;

using DataAccess;
namespace LendOSolnTest.DataAccessTools
{
    [TestFixture]
	public class CDateTimeTest
	{

        [Test]
        public void IsValidTest() 
        {
            string[] invalidDatetimeStrings = {null, "", "abc", "12/48"};

            foreach (string str in invalidDatetimeStrings) 
            {
                CDateTime dt = CDateTime.Create(str, null);
                Assert.IsFalse(dt.IsValid, str);
            }
        }

        [Test]
        public void AMPMTest()
        {
            CDateTime cdt1 = CDateTime.Create("12/12/12 2:51 PM", null);
            CDateTime cdt2 = CDateTime.Create("12/12/12 2:51 AM", null);
            Assert.AreNotEqual(cdt1.DateTimeForComputationWithTime, cdt2.DateTimeForComputationWithTime);
            Assert.AreEqual(cdt1.DateTimeForComputation, cdt2.DateTimeForComputation);

            DateTime dt = DateTime.Parse("12/12/12 2:51 PM");
            Assert.AreEqual(cdt1.DateTimeForComputationWithTime, dt);
        }

        [Test]
        public void EqualityTest()
        {
            CDateTime startDay = CDateTime.Create(new DateTime(2006, 4, 3));
            CDateTime startDay2 = CDateTime.Create(new DateTime(2006, 4, 3));
            CDateTime nullDt = null;
            CDateTime invalidDt = CDateTime.Create("", new LosConvert(FormatTarget.DocMagic));

            Assert.IsFalse(startDay == nullDt);
            Assert.IsTrue(default(CDateTime) == nullDt);
            Assert.IsTrue(startDay == startDay2);
            Assert.IsTrue(invalidDt == CDateTime.InvalidWrapValue);

            Assert.IsTrue(startDay != nullDt);
            Assert.IsFalse(default(CDateTime) != nullDt);
            Assert.IsFalse(startDay != startDay2);
            Assert.IsFalse(invalidDt != CDateTime.InvalidWrapValue);

            Assert.IsTrue(startDay.Equals(startDay2));
            Assert.IsTrue(startDay.Equals((object)startDay2));
            Assert.IsFalse(startDay.Equals(null));
            Assert.IsFalse(startDay.Equals(nullDt));
            Assert.IsFalse(startDay.Equals(invalidDt));
        }

        [Test]
        public void ParsingTest()
        {
            LosConvert fannie = new LosConvert(FormatTarget.FannieMaeMornetPlus);
            LosConvert webform = new LosConvert(FormatTarget.Webform);

            // Sensible behaviors

            Assert.AreEqual(new DateTime(2017, 5, 1, 23, 58, 36), CDateTime.Create("05/01/2017 11:58:36 PM", null).DateTimeForComputationWithTime);
            Assert.AreEqual(new DateTime(2017, 5, 1), CDateTime.Create("05/01/2017 ", null).DateTimeForComputationWithTime);
            Assert.AreEqual(new DateTime(2017, 5, 1), CDateTime.Create("20170501", fannie).DateTimeForComputationWithTime);
            Assert.AreEqual(CDateTime.InvalidWrapValue, CDateTime.Create("05/01/2017 @NY R@nd0m g@rb@93", null));
            Assert.AreEqual(CDateTime.InvalidWrapValue, CDateTime.Create("20170501 11:58:36 PM", fannie));

            //   yyyyMMdd is not supported by the default FormatTargets.
            Assert.AreEqual(CDateTime.InvalidWrapValue, CDateTime.Create("20170501", null));

            //   MMddyy and MMddyyyy are supported, but they should probably be restricted to Webform.
            Assert.AreEqual(new DateTime(2017, 5, 1), CDateTime.Create("050117", webform).DateTimeForComputationWithTime);
            Assert.AreEqual(new DateTime(2017, 5, 1), CDateTime.Create("05012017", webform).DateTimeForComputationWithTime);

            //   Dates out of range are at least always invalid, if not *the* invalid value.
            Assert.IsFalse(CDateTime.Create("11121211", null).IsValid);

            //   We do support ISO 8601, but it looks almost like an accident that we do.
            Assert.AreEqual(new DateTime(2017, 5, 1, 23, 58, 36), CDateTime.Create("2017-05-01T23:58:36", null).DateTimeForComputationWithTime);

            //   MMddyy and MMddyyyy dates now don't allow anything other than themselves.
            Assert.AreEqual(CDateTime.InvalidWrapValue, CDateTime.Create("050117 11:58:36 PM", null));

            //   ...particularly if that is garbage.
            Assert.AreEqual(CDateTime.InvalidWrapValue, CDateTime.Create("050117 @NY R@nd0m g@rb@93", null));

            //   Out of range dates now produce CDateTime.InvalidWrapValue instead of bombs
            Assert.AreEqual(CDateTime.Create("11121211", null), CDateTime.InvalidWrapValue);
            Assert.AreEqual(CDateTime.Create("12111112", fannie), CDateTime.InvalidWrapValue);

            //   Out of range values produce InvaldWrapValue, which are equal to each other, even if the source strings are different.
            Assert.AreEqual(CDateTime.Create("11121211", null), CDateTime.Create("12111112", null));

            // Surprising behaviors

            //   Fannie/Freddie/Ginnie FormatTargets don't require that dates be strictly yyyyMMdd, they just allow it.
            Assert.AreEqual(new DateTime(2017, 5, 1), CDateTime.Create("05/01/17", fannie).DateTimeForComputationWithTime);
            Assert.AreEqual(new DateTime(2017, 5, 1, 23, 58, 36), CDateTime.Create("05/01/17 11:58:36 PM", fannie).DateTimeForComputationWithTime);
            Assert.AreEqual(new DateTime(2017, 5, 1), CDateTime.Create("05/01/2017", fannie).DateTimeForComputationWithTime);
            Assert.AreEqual(new DateTime(2017, 5, 1, 23, 58, 36), CDateTime.Create("05/01/2017 11:58:36 PM", fannie).DateTimeForComputationWithTime);

            //    Fannie/Freddie/Ginnie even supports MMddyyyy if the yyyyMMdd interpretation would be invalid.
            Assert.AreEqual(new DateTime(2017, 5, 1), CDateTime.Create("05012017", fannie).DateTimeForComputationWithTime);

            //   We take anything that looks date-ish.
            Assert.AreEqual(new DateTime(2017, 5, 1), CDateTime.Create("Monday, May 1, 2017", webform).DateTimeForComputationWithTime);

            //     Though if you do specify a day of the week, you must specify the correct one.
            Assert.AreEqual(CDateTime.InvalidWrapValue, CDateTime.Create("Friday, May 1, 2017", webform));

            //     NOTE: There is a very small chance that this will fail if it executes exactly at midnight between 31 Dec and 1 Jan.
            Assert.AreEqual(new DateTime(DateTime.Today.Year, 5, 1), CDateTime.Create("5-1", fannie).DateTimeForComputationWithTime);

            //     NOTE: There is a very small chance that this will fail if it executes exactly at midnight.
            Assert.AreEqual(new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, 1, 1, 0),
                CDateTime.Create("1:1", webform).DateTimeForComputationWithTime);
            Assert.AreEqual(new DateTime(2013, 5, 1), CDateTime.Create("2013-5", null).DateTimeForComputationWithTime);
            Assert.AreEqual(new DateTime(2001, 3, 2, 1, 2, 3), CDateTime.Create("#3.2.1  \t  1:2:3#", webform).DateTimeForComputationWithTime);
        }
    }
}
