using System;
using System.Text;
using System.Collections;
using System.Xml;

using LendersOfficeApp.los.RatePrice;
using NUnit.Framework;

namespace LendOSolnTest
{
    [TestFixture]
    public class CConditionTest
    {

        static void ExpectExceptionWhenValidate( string condStr, CEvaluationException.ErrorTypeEnum expectErrType )
        {
            try
            {
                ConditionValidate( condStr );
            }
            catch( CConditionException exc )
            {
                Assert.AreEqual( expectErrType, exc.ErrorType );
                return;
            }
            Assert.Fail("The CConditionException ( " + expectErrType + " ) was expected for expression : " + condStr);
        }


        private static bool ConditionValidate( string condStr )
        {
            CCondition condition = CCondition.CreateTestCondition( condStr );

            XmlDocument xmlDocEval = new XmlDocument();
            return condition.Evaluate(xmlDocEval, CRuleEvalTool.DefaultTable); 
        }

        [Test]
        [ExpectedException(typeof(CEvaluationException))]
        public void UnexcpectExtraDataForEqualOperator()
        {
            string data = " 1 = 2 3"; 
            ConditionValidate( data ); 
        }


        [Test]
        public void AndExprTest()
        {
            string data = "( TERM <= 360 ) AND ( PropertyPurpose = PropertyPurpose_Investment_2 )" +
                " AND ( NUMBEROFUNITS = 1 )";
            ConditionValidate( data);
        }

        [Test]
        public void MissingAndOperatorTest()
        {
            string data = "( TERM <= 360 ) AND ( PropertyPurpose = PropertyPurpose_Investment_2 )" +
                " ( NUMBEROFUNITS = 1 )";
            ExpectExceptionWhenValidate( data, CEvaluationException.ErrorTypeEnum.MissingAndOp);
        }

        [Test]
        public void UnexpectedAndOperatorTest()
        {
            string data = "( TERM <= 360 ) AND AND ( PropertyPurpose = PropertyPurpose_Investment_2 )";
            ExpectExceptionWhenValidate( data, CEvaluationException.ErrorTypeEnum.UnexpectOp);
        }

        [Test]
        public void UnexpectedColonInArgument_Opm18952()
        {
            
            string []ArrData = 
            {
                "0 <= TradeCountX:status<=none:",
                "( 0 < TradeCountX:status<=none:ClosedDate<=24 )",
                "0 <= TradeCountX:status<=none:aaa",


            };                                                                         

            foreach( string data in ArrData )
                ExpectExceptionWhenValidate( data, CEvaluationException.ErrorTypeEnum.UnexpectColonChar);
        }


        [Test]
        public void OrExprTest()
        {
            string data = "( TERM <= 360 ) OR ( PropertyPurpose = PropertyPurpose_Investment_2 )" +
                " OR ( NUMBEROFUNITS = 1 )";
            ConditionValidate( data);
        }


        [Test]
        public void MissingOrOperatorTest()
        {
            string data = "( TERM <= 360 ) OR ( PropertyPurpose = PropertyPurpose_Investment_2 )" +
                " ( NUMBEROFUNITS = 1 )";
            ExpectExceptionWhenValidate( data, CEvaluationException.ErrorTypeEnum.MissingOrOp);
        }

        [Test]
        public void UnexpectedOrOperatorTest()
        {
            string data = "( TERM <= 360 ) OR OR ( PropertyPurpose = PropertyPurpose_Investment_2 )";
            ExpectExceptionWhenValidate( data, CEvaluationException.ErrorTypeEnum.UnexpectOp);
        }

        [Test]
        public void OnlyOneFieldValueInGroupTest() // (fieldValue) : opm 5523
        {
            string data = " ( PropertyPurpose_PrimaryResidence_0 ) ";
            ExpectExceptionWhenValidate( data, CEvaluationException.ErrorTypeEnum.MissingVariableName);
        }

        [Test]
        public void HaveFieldValueButNoFieldNameInExpTest() // (fieldValue) : opm 5523
        {
            string []ArrData = 
            {
                "  ( 2 *  PropertyType_Coop_3 ) <> 0 ",
                "  ( 2 /  PropertyType_Coop_3 ) <> 0 ",
                "  ( 2 +  PropertyType_Coop_3 ) <> 0 ",
                "  ( 2 -  PropertyType_Coop_3 ) <> 0 ",
                "  2 >  PropertyType_Coop_3 ",
                "  2 >= PropertyType_Coop_3 ",
                "  2 <  PropertyType_Coop_3 ",
                "  2 <= PropertyType_Coop_3 ",
                "  2 =  PropertyType_Coop_3 ",
                "  2 <> PropertyType_Coop_3 ",
                "  ( State ) = PropertyType_Coop_3  ",
            };                                                                         

            for( int step = 0; step < 2; step++)
            {
                if( step == 0 )
                {
                    CCondition.RestrictSyntax = true;
                    foreach( string data in ArrData )
                        ExpectExceptionWhenValidate( data, CEvaluationException.ErrorTypeEnum.DontAllowFieldValue);
                }
                else
                {
                    CCondition.RestrictSyntax = false; // if don't restrict syntax, the above expressions are ok.
                    foreach( string data in ArrData )
                        ConditionValidate( data);
                    CCondition.RestrictSyntax = true;
                }
            }
        }

        [Test]
        public void MismatchFieldNameAndFieldValueTest() 
        {
            string []ArrData = 
            {
                "  DocType = PropertyType_Coop_3  ",
                "  State = PropertyType_Coop_3  ",

            };                                                                         

            foreach( string data in ArrData )
                ExpectExceptionWhenValidate( data, CEvaluationException.ErrorTypeEnum.MismatchFieldAndFieldValue);
        }

        [Test]
        public void EvaluateOneCondTest( ) 
        {
            string exprData =  "3 > TradeCountX:Reviewed>=24";
            ConditionValidate( exprData); 
        }


        [Test]
        public void CorrectCondsTest() 
        {
            string []ArrData = 
            {
                "  DocType =  DocType_SISA_5  ",
                "  DocType <> DocType_SISA_5  ",
                "  ( DocType <> DocType_SISA_5 ) AND ( 1 = BOOL_YES ) ",
                "BOOL_YES",
                "BOOL_NO",
                "  ( ( STATE = STATE_CA ) OR ( STATE = STATE_FL ) ) AND ( CLTV > 80 )",

            };                                                                         

            foreach( string data in ArrData )
                ConditionValidate( data);
        }

        [Test]
        public void UnMatchingParenthesisTest() 
        {
            // http://opm/default.asp?3181
            string data_1 = @"( 
                                ( DocType = DocType_SISA_5 ) OR 
                                ( DocType = DocType_SIVA_3 ) 
                            ) AND 
                            ( LTV <= 80 ) AND 
                            ) 
                            ( TradesCountNonMajorDerog < 3 )";

            string []ArrData = 
            {
                data_1,
                " ( 1 = 2  ",
                " ( 1 = 2 ) ) ",
                " ( ( 1 = 2 ) AND ( 3 )  ",
            };                                                                         

            foreach( string data in ArrData )
                ExpectExceptionWhenValidate( data, CEvaluationException.ErrorTypeEnum.UnMatchingParenthesis);
        }

        [Test]
        public void EmptyParenthesesTest()
        {
            // OPM 60414
            string data_1 = @"( ( BkHas7BkFiledWithin:24 = Bool_Yes ) OR ( BkHasDisch7BkWithin:24 = Bool_Yes ) ) AND (  ) AND ( PurposeType <> PurposeType_FHASTreamlinedRefinance_6 )";

            string[] ArrData = 
            {
                data_1,
                " ( 1 = 2 ) OR () ",
                @" ( 1 = 2 ) OR (
                ) ",
                " ( 1 = 2 ) AND ( ( 1 = 2 ) OR ( ( 1 = 2 ) AND () ) ) ",
                " (      ) ",
            };

            foreach (string data in ArrData)
                ExpectExceptionWhenValidate(data, CEvaluationException.ErrorTypeEnum.EmptyParentheses);

        }

    }

    [TestFixture]
    public class EvaluateExprTest
    {

        public decimal Evaluate( string exprString )
        {
            XmlDocument xmlDocEval = new XmlDocument();

            CSymbolTable symbolTable = CRuleEvalTool.DefaultTable;

            string  xmlEval  = CRuleEvalTool.ReplaceForEval( exprString, symbolTable );
            XmlNode exprNode = CRuleEvalTool.CreateAnEvalNode( xmlDocEval, xmlEval );

            decimal rawResult  = CRuleEvalTool.ExpressionDispatch( exprNode, 0, symbolTable );
            return rawResult;
        }

        [Test]
        public void Test()
        {
            Assert.AreEqual(6,  Evaluate("1 + 5"));
            Assert.AreEqual(42, Evaluate( "( 1 + 5 ) * 7" ) );
            Assert.AreEqual(1, Evaluate( "( ( CertTime_DayOfWeek >= 1 ) and ( CertTime_DayOfWeek <= 7 ) )" ) );
        }

    }

    [TestFixture]
    public class PostfixTest
    {

        static CSymbolTable s_symbolTable = CRuleEvalTool.DefaultTable;
        static PostfixTest()
        {
        }

        static public string ToPostfix( string exprString )
        {
            return ToPostfix( exprString, /* verifyPostfixOnly = */ false );
        }

        static public string ToPostfix( string exprString, bool verifyPostfixOnly )
        {
            string  xmlEval = null;
            string infixString = null;
            XmlNode exprNode;
            try
            {
                PostfixExpression postfixExpr =  new PostfixExpression();
                XmlDocument xmlDocEval = new XmlDocument();


                CSymbolTable symbolTable = CRuleEvalTool.DefaultTable;
                try
                {
                    xmlEval  = CRuleEvalTool.ReplaceForEval( exprString, symbolTable );
                    exprNode = CRuleEvalTool.CreateAnEvalNode( xmlDocEval, xmlEval );
                    if( verifyPostfixOnly )
                        CRuleEvalTool.ExpressionDispatch( exprNode, 0, symbolTable, null );
                }
                catch
                {
                    if( verifyPostfixOnly )
                        return "";
                    throw;
                }

                decimal expect = CRuleEvalTool.ExpressionDispatch( exprNode, 0, s_symbolTable, postfixExpr );
                infixString = postfixExpr.ToString();

                decimal result = PostfixExpression.Evaluate( postfixExpr, s_symbolTable, new StringBuilder(), false );
                Assert.AreEqual(expect, result);

                return infixString;
            }
            catch
            {
                Console.WriteLine( "exprString = " + exprString + "\n" );
                if( xmlEval != null )
                    Console.WriteLine( "xmlEval = " + xmlEval + "\n" );
                if( infixString != null )
                    Console.WriteLine( "infixString = " + infixString + "\n" );

                throw;
            }
        }

    

        static void PostfixAssert(  string expectResult, string exprString  )
        {
            Assert.AreEqual( expectResult.ToLower(), ToPostfix( exprString ).ToLower() ) ;
        }


        
        // Array of { infix, expectPostfix }
        static void PostfixAssert(  string[ , ] testCases )
        {
            for( int i = 0; i < testCases.GetLength( 0 ); i++ )
            {
                string infix         = testCases[i, 0];
                string expectPostfix = testCases[i, 1];

                PostfixAssert( expectPostfix, infix);
            }
        }



        [Test]
        public void EmptyExpressionTest()
        {
            XmlNode exprNode = null;
            PostfixExpression postfixExpr  = (PostfixExpression)CRuleEvalTool.ParseToXmlNodeOrPostfixExpr( s_symbolTable, new XmlDocument(), "" /*condStr*/, ref exprNode );
            Assert.AreEqual(0, postfixExpr.Evaluate( s_symbolTable, new EvalStack(1), new StringBuilder(), false ) );

            Assert.AreEqual( 0, CRuleEvalTool.ExpressionDispatch( exprNode, 0, s_symbolTable )  );
        }


        [Test]
        public void OperatorsTest()
        {
            string [ , ] testCases =
            {
                // infix  -> postfix

                //empty
                {   ""          ,  ""      },

                // add
                {   " 1 + 2 "          ,  "1 2 +"      },
                {   " ( 1 + 2 ) "      ,  "1 2 +"      },
                {   " ( 1 + 2 ) + 3 "  ,  "1 2 + 3 +"  },

                // minus
                {   " 1 - 2 "          ,  "1 2 -"      },
                {   " ( 1 - 2 ) "      ,  "1 2 -"      },
                {   " ( 1 - 2 ) - 3 "  ,  "1 2 - 3 -"  },

                // multiply
                {   " 1 * 2 "          ,  "1 2 *"      },
                {   " ( 1 * 2 ) "      ,  "1 2 *"      },
                {   " ( 1 * 2 ) * 3 "  ,  "1 2 * 3 *"  },

                // div
                {   " 1 / 2 "          ,  "1 2 /"      },
                {   " ( 1 / 2 ) "      ,  "1 2 /"      },
                {   " ( 1 / 2 ) / 3 "  ,  "1 2 / 3 /"  },

                // operators : <, <=, >, >=, =, <> 
                {   " 1 < 2 "          ,  "1 2 <"      },
                {   " 1 <= 2 "         ,  "1 2 <="    },
                {   " 1 > 2 "          ,  "1 2 >"      },
                {   " 1 >= 2 "         ,  "1 2 >="    },
                {   " 1 = 2 "          ,  "1 2 ="      },
                {   " 1 <> 2 "         ,  "1 2 <>"    },

                // operators : or, and 
                {   " ( 1 < 2 ) and ( 4 > 3 ) "          ,  "1 2 < 4 3 > And"     },
                {   " ( 1 < 2 ) or  ( 4 > 3 ) "          ,  "1 2 < 4 3 > Or"      },

                {   " ( 1 < 2 ) and ( 4 > 3 ) and ( 5 > 6 )"          ,  "1 2 < 4 3 > And 5 6 > And"     },
                {   " ( 1 < 2 ) or  ( 4 > 3 ) or ( 5 > 6 )"          ,  "1 2 < 4 3 > Or 5 6 > Or"      },


                // mix

                {   " ( 1 - 2 ) + 3 "  ,  "1 2 - 3 +"  },
                {   " ( 1 + 2 ) * 3 "  ,  "1 2 + 3 *"  },
                {   " ( 1 * 2 ) + 3 "  ,  "1 2 * 3 +"  },
                {   " ( 1 * 2 ) / 3 "  ,  "1 2 * 3 /"  },
                {   " ( 1 + 2 ) / 3 "  ,  "1 2 + 3 /"  },

            };

            PostfixAssert(  testCases );

            //Console.WriteLine( ToPostfix( " ( 1 + 2 ) ") );
        }               


        [Test]
        public void FieldNameTest()
        {
            string [ , ] testCases =
            {
                //{ "( QLTV > 25 )"       , "QLTV 25 >" },
                { "( ResidualIncomeMonthly > 25 )"       , "ResidualIncomeMonthly 25 >" },
            };

            PostfixAssert( testCases );

            //string infix = "( QLTV > 25 )";
            //Console.WriteLine( ToPostfix( infix) );
        }


        [Test]
        public void FieldValueTest()
        {
            string [ , ] testCases =
            {
                { " DocType =  DocType_SISA_5 "       , "DocType DocType_SISA_5 =" },
                { " BOOL_YES "                        , "BOOL_YES" },
            };

            PostfixAssert( testCases );
        }

        [Test]
        public void FunctionTest()
        {
            string [ , ] testCases =
            {
                { " Bankruptcy:36 > 0 "               , "Bankruptcy(36) 0 >" },
                {  " 3 <= TradeCountX:Reviewed>=12 "  , "3 TradeCountX(reviewed>=12) <=" },
            };

            PostfixAssert( testCases );
        }



    }





}
