/// Author: David Dao

using System;
using NUnit.Framework;

using DataAccess;
namespace LendOSolnTest
{
    [TestFixture]
	public class AlwaysRoundupTest
	{
        [Test]
		public void RateRoundupTest()
		{
            decimal[,] rateTest = {
                                      {0, 0}
                                      , {10.0M, 10.000M}
                                      , {10.001M, 10.001M}
                                      , {10.0010002M, 10.002M}
                                      , {10.0019999M, 10.002M}
                                      , {10.0015M, 10.002M}
                                      , {-10.0M, -10.000M}
                                      , {-10.001M, -10.001M}
                                      , {-10.0010002M, -10.002M}
                                      , {-10.0019999M, -10.002M}
                                      , {-10.0015M, -10.002M}

                                  };
            for (int i = 0; i < rateTest.Length / 2; i++) 
            {
                Assert.AreEqual(rateTest[i, 1], Tools.AlwaysRoundUp(rateTest[i, 0], 3));
            }
		}

        [Test]
        public void MoneyRoundupTest() 
        {
            decimal[,] moneyTest = {
                                       {0, 0}
                                      , {10.0M, 10.00M}
                                      , {10.01M, 10.01M}
                                      , {10.010002M, 10.02M}
                                      , {10.019999M, 10.02M}
                                      , {10.015M, 10.02M}
                                       , {-10.0M, -10.00M}
                                       , {-10.01M, -10.01M}
                                       , {-10.010002M, -10.02M}
                                       , {-10.019999M, -10.02M}
                                       , {-10.015M, -10.02M}
                                  };
            for (int i = 0; i < moneyTest.Length / 2; i++) 
            {
                Assert.AreEqual(moneyTest[i, 1], Tools.AlwaysRoundUp(moneyTest[i, 0], 2));
            }

        }
	}
}
