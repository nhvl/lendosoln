﻿// <copyright file=""BasicClosingCostSetTest.cs"" company=""MeridianLink"">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: David Dao
//    Date:   12/9/2014 4:39:44 PM 
// </summary>
namespace LendOSolnTest.ClosingCostSetTest
{
    using System;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Constants;
    using LendOSolnTest.Common;
    using LendOSolnTest.Fakes;
    using NUnit.Framework;
    
    /// <summary>
    /// Test the basic CRUD for ClosingCostSet class.
    /// </summary>
    [TestFixture]
    public class BasicClosingCostSetTest
    {
        [TestFixtureSetUp]
        public void InitTest()
        {
            LoginTools.LoginAs(TestAccountType.Branch1_LenderAccountExec);;
        }

        [TearDown]
        public void Teardown()
        {
            FakePageDataUtilities.Instance.Reset();
        }

        [Test]
        public void TestExistingSerialization001()
        {

            string json = @"[{""aff"":false,""apr"":false,""bene"":0,""bene_id"":""00000000-0000-0000-0000-000000000000"",""can_shop"":false,""desc"":""Test"",""did_shop"":false,""disc_sect"":0,
                                ""f"":{""base"":""$1.00"",""p"":""1.000%"",""pb"":""$100,000.00"",""period"":""1"",""pt"":0,""t"":0},
                                ""fha"":false,""hudline"":""805"",""id"":""b9e53fdb-6a9b-4af6-b499-07b96530fe81"",""is_optional"":false,""is_system"":false,""is_title"":false,""legacy"":0,
                                ""mismo"":0,""pmts"":null,""prov"":0,""prov_choice"":0,""responsible"":0,""section"":12,""total"":""$1,001.00"",""tp"":false,
                                ""typeid"":""00000000-0000-0000-0000-000000000000"",""va"":false}]";

            BorrowerClosingCostSet closingCostSet = new BorrowerClosingCostSet(json);
            Func<BaseClosingCostFee, bool> setFilter = borrowerFee => ClosingCostSetUtils.ExOrigCompDiscountPrepsSecGMIFilter((BorrowerClosingCostFee)borrowerFee);

            // GF Disabled this because we add some fees at initialization.
            //Assert.AreEqual(1, closingCostSet.GetFees(setFilter).Count());

            BorrowerClosingCostFee fee = (BorrowerClosingCostFee)closingCostSet.GetFees(f => f.UniqueId == new Guid("b9e53fdb-6a9b-4af6-b499-07b96530fe81")).First();

            Assert.AreEqual("$1,001.00", fee.TotalAmount_rep);
            Assert.AreEqual(1001, fee.TotalAmount);

            fee.Percent = 2;

            Assert.AreEqual(2001, fee.TotalAmount);
        }

        [Test]
        public void TestEmptyClosingCost()
        {
            BorrowerClosingCostSet closingCostSet = new BorrowerClosingCostSet(string.Empty); // Create new object.
            Func<BaseClosingCostFee, bool> setFilter = borrowerFee => ClosingCostSetUtils.ExOrigCompDiscountPrepsSecGMIFilter((BorrowerClosingCostFee)borrowerFee);

            // Per-diem is added on initialization.
            Assert.AreEqual(1, closingCostSet.GetFees(setFilter).Count(), "Must have 1 count.");

            string json = closingCostSet.ToJson();

            // Recreate empty closing cost set from json.
            closingCostSet = new BorrowerClosingCostSet(json);

            Assert.AreEqual(1, closingCostSet.GetFees(setFilter).Count());
        }

        [Test]
        public void TestBasicClosingCost()
        {
            BorrowerClosingCostSet closingCostSet = new BorrowerClosingCostSet(string.Empty, null); // Create new object.

            Guid feeId = Guid.Empty;
            BorrowerClosingCostFee fee = new BorrowerClosingCostFee();

            fee.HudLine = 800;
            fee.Description = "800 Description";
            fee.GfeSectionT = E_GfeSectionT.B1;
            fee.IsApr = true;
            fee.IsFhaAllowable = false;
            fee.Beneficiary = E_AgentRoleT.Lender;
            fee.BeneficiaryAgentId = Guid.NewGuid();
            fee.BaseAmount = 10000;
            fee.Percent = 1.00M;
            fee.PercentBaseT = E_PercentBaseT.LoanAmount;
            fee.NumberOfPeriods = 1;
            fee.GfeResponsiblePartyT = E_GfeResponsiblePartyT.Buyer;

            Assert.AreEqual(1, fee.Payments.Count()); // There is always at least 1 payment.
            BorrowerClosingCostFeePayment payment = new BorrowerClosingCostFeePayment();
            payment.Amount = 300;
            payment.GfeClosingCostFeePaymentTimingT = E_GfeClosingCostFeePaymentTimingT.AtClosing;
            payment.PaymentDate = DateTime.Now;

            fee.AddPayment(payment);

            closingCostSet.AddOrUpdate(fee);
            Assert.AreNotEqual(Guid.Empty, fee.UniqueId, "After insert in closing cost set id cannot be guid.empty.");

            feeId = fee.UniqueId;

            Func<BaseClosingCostFee, bool> setFilter = borrowerFee => borrowerFee.UniqueId == fee.UniqueId;
            Assert.AreEqual(1, closingCostSet.GetFees(setFilter).Count());

            string json = closingCostSet.ToJson();

            // Recreate closing cost set from json.
            closingCostSet = new BorrowerClosingCostSet(json);

            Assert.AreEqual(1, closingCostSet.GetFees(setFilter).Count());

            fee = (BorrowerClosingCostFee)closingCostSet.GetFees(setFilter).First();

            Assert.AreEqual(feeId, fee.UniqueId);
            Assert.AreEqual(3, fee.Payments.Count());

        }

        [Test]
        public void TestSplitClosingCostPayment()
        {
            BorrowerClosingCostSet closingCostSet = new BorrowerClosingCostSet(string.Empty); // Create new object.

            Guid feeId = Guid.Empty;
            BorrowerClosingCostFee fee = new BorrowerClosingCostFee();

            fee.HudLine = 800;
            fee.Description = "800 Description";
            fee.GfeSectionT = E_GfeSectionT.B1;
            fee.IsApr = true;
            fee.IsFhaAllowable = false;
            fee.Beneficiary = E_AgentRoleT.Lender;
            fee.BeneficiaryAgentId = Guid.NewGuid();
            fee.BaseAmount = 1000;
            fee.Percent = 1.00M;
            fee.PercentBaseT = E_PercentBaseT.LoanAmount;
            fee.NumberOfPeriods = 1;
            fee.GfeResponsiblePartyT = E_GfeResponsiblePartyT.Buyer;

            EnsureFeeAndPaymentsValid(fee);
            Assert.AreEqual(1, fee.Payments.Count());

            Assert.AreEqual(false, fee.Payments.First().IsSystemGenerated, "First payment cannot be system generated.");

            BorrowerClosingCostFeePayment payment = new BorrowerClosingCostFeePayment();
            payment.Amount = 300;
            payment.GfeClosingCostFeePaymentTimingT = E_GfeClosingCostFeePaymentTimingT.AtClosing;
            payment.PaymentDate = DateTime.Now;

            fee.AddPayment(payment);

            closingCostSet.AddOrUpdate(fee);
            Assert.AreNotEqual(Guid.Empty, fee.UniqueId, "After insert in closing cost set id cannot be guid.empty.");

            Assert.AreEqual(3, fee.Payments.Count());

            EnsureFeeAndPaymentsValid(fee);

            // Update the total amount in fee. Expected the sume of payment list still match.
            fee.BaseAmount = 1100;


            Assert.AreEqual(1100, fee.TotalAmount);
            EnsureFeeAndPaymentsValid(fee);

            string json = closingCostSet.ToJson();

            closingCostSet = new BorrowerClosingCostSet(json, null);
            Func<BaseClosingCostFee, bool> setFilter = borrowerFee => borrowerFee.UniqueId == fee.UniqueId;

            fee = (BorrowerClosingCostFee)closingCostSet.GetFees(setFilter).First();
            EnsureFeeAndPaymentsValid(fee);

            Assert.AreEqual(3, fee.Payments.Count());
            fee.Payments.ElementAt(0).Amount = 600;
            fee.Payments.ElementAt(1).Amount = 500;

            EnsureFeeAndPaymentsValid(fee);
        }

        [Test]
        public void TestClosingCostFromDataLoan()
        {
            var dataLoan = FakePageData.Create();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            dataLoan.sLAmtCalc_rep = "$100,000";
            dataLoan.sLAmtLckd = true;

            BorrowerClosingCostSet closingCostSet = dataLoan.sClosingCostSet;

            // Don't assert count is 0, we add some fees at initialization.
            //Assert.AreEqual(0, closingCostSet.GetFees(setFilter).Count());

            BorrowerClosingCostFee fee = new BorrowerClosingCostFee();

            fee.HudLine = 805;
            fee.Description = "Test";

            fee.Percent = 1;
            fee.PercentBaseT = E_PercentBaseT.LoanAmount;
            fee.BaseAmount = 1;
            fee.NumberOfPeriods = 1;
            fee.GfeSectionT = E_GfeSectionT.NotApplicable;
            closingCostSet.AddOrUpdate(fee);

            Assert.AreEqual(1001, fee.TotalAmount);

            dataLoan.Save();

            dataLoan = FakePageData.Create();
            dataLoan.InitLoad();

            closingCostSet = dataLoan.sClosingCostSet;

            Func<BaseClosingCostFee, bool> setFilter = borrowerFee => borrowerFee.UniqueId == fee.UniqueId;

            Assert.AreEqual(1, closingCostSet.GetFees(setFilter).Count());

            fee = (BorrowerClosingCostFee)closingCostSet.GetFees(setFilter).First();

            Assert.AreEqual(1001, fee.TotalAmount);

            dataLoan.sLAmtCalc_rep = "$200,000";

            Assert.AreEqual(2001, fee.TotalAmount);

            // Disconnect Closing Cost Set from dataLoan.

            string json = closingCostSet.ToJson();

            closingCostSet = new BorrowerClosingCostSet(json);

            fee = (BorrowerClosingCostFee)closingCostSet.GetFees(setFilter).First();

            Assert.AreEqual(2001, fee.TotalAmount);

            dataLoan.sLAmtCalc_rep = "$100,00"; // Update loan amount should not affect the disconnected closing cost.

            Assert.AreEqual(2001, fee.TotalAmount);
        }

        [Test]
        public void TestGuidCombining()
        {
            Guid lastSaltedId1 = Guid.NewGuid();
            Guid lastSaltedId2 = Guid.NewGuid();
            Guid lastUniquerId = Guid.NewGuid();

            for (int i = 0; i < 1000; ++i)
            {
                Guid uniqueId = Guid.NewGuid();
                Guid saltedId1 = ClosingCostSetUtils.CombineGuids(uniqueId, ClosingCostSetUtils.SaltGuid1);
                Guid saltedId2 = ClosingCostSetUtils.CombineGuids(uniqueId, ClosingCostSetUtils.SaltGuid2);
                Guid uniquerId = ClosingCostSetUtils.CombineGuids(uniqueId, uniqueId);

                Assert.AreNotEqual(uniqueId, saltedId1);
                Assert.AreNotEqual(uniqueId, saltedId2);
                Assert.AreNotEqual(uniqueId, uniquerId);

                Assert.AreNotEqual(Guid.Empty, saltedId1);
                Assert.AreNotEqual(Guid.Empty, saltedId2);
                Assert.AreNotEqual(Guid.Empty, uniquerId);

                Assert.AreNotEqual(lastSaltedId1, saltedId1);
                Assert.AreNotEqual(lastSaltedId2, saltedId2);
                Assert.AreNotEqual(lastUniquerId, uniquerId);

                lastSaltedId1 = saltedId1;
                lastSaltedId2 = saltedId2;
                lastUniquerId = uniquerId;
            }
        }

        [Test]
        public void TestClosingCostSetJson_RoundtripsThroughLoan()
        {
            var dataLoan = FakePageData.Create();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            string originalJson = dataLoan.GetsClosingCostSetJson();

            var closingCostSet = dataLoan.sClosingCostSet;
            string newJson = closingCostSet.ToJson();

            Assert.AreEqual(originalJson, newJson);
        }

        [Test]
        public void TestClosingCostSetSystemGeneratedPaymentIds()
        {
            var dataLoan = FakePageData.Create();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            dataLoan.sClosingCostFeeVersionT = E_sClosingCostFeeVersionT.ClosingCostFee2015;
            dataLoan.sProcF = 100.00M;
            dataLoan.GetAppData(0).aCLastNm = "Test";

            var processingFee = (BorrowerClosingCostFee)dataLoan.sClosingCostSet.FindFeeByTypeId(DefaultSystemClosingCostFee.Hud800ProcessingFeeTypeId);
            var thisFeelsSoWrong = new BorrowerClosingCostFeePayment();
            thisFeelsSoWrong.IsSystemGenerated = true;
            processingFee.AddPayment(thisFeelsSoWrong);
            var processingFeePmt = processingFee.Payments.First();
            processingFeePmt.Amount = 42.00M;
            processingFeePmt.PaidByT = E_ClosingCostFeePaymentPaidByT.Seller;
            dataLoan.sClosingCostSet.AddOrUpdate(processingFee);

            dataLoan.Save();

            dataLoan = FakePageData.Create();

            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            string originalJson = dataLoan.GetsClosingCostSetJson();

            var closingCostSet = dataLoan.sClosingCostSet;
            string newJson = closingCostSet.ToJson();

            Assert.AreEqual(originalJson, newJson);

            var feeId0 = ((BorrowerClosingCostFee)dataLoan.sClosingCostSet.FindFeeByTypeId(DefaultSystemClosingCostFee.Hud800ProcessingFeeTypeId)).Payments.ElementAt(0).Id;
            var feeId1 = ((BorrowerClosingCostFee)dataLoan.sClosingCostSet.FindFeeByTypeId(DefaultSystemClosingCostFee.Hud800ProcessingFeeTypeId)).Payments.ElementAt(1).Id;

            Assert.AreNotEqual(feeId0, feeId1);
        }

        [Test]
        public void TestClosingCostSetPaymentsResetWithConsistentIds()
        {
            var dataLoan = FakePageData.Create();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            dataLoan.sClosingCostFeeVersionT = E_sClosingCostFeeVersionT.ClosingCostFee2015;
            dataLoan.sLT = E_sLT.VA;
            dataLoan.sLAmtCalc = 100000.00M;
            dataLoan.sHazInsRsrvMonLckd = true;
            dataLoan.sHazInsRsrvMon = 12;
            dataLoan.sHazInsPiaMon = 12;
            dataLoan.sProHazInsMb = 75.00M; // Need to change the value of the monthly hazard insurance to change the fee amounts and hit the ResetPayments() call in BorrowerClosingCostSet.ToJson.

            dataLoan.Save();

            dataLoan = FakePageData.Create();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            // Make sure these are not 0 so that when we set them to 0, they change.
            Assert.AreEqual(900.00M, dataLoan.sHazInsRsrv);
            Assert.AreEqual(900.00M, dataLoan.sHazInsPia);

            dataLoan.sLT = E_sLT.FHA;
            dataLoan.sFfUfMipIsBeingFinanced = true;
            dataLoan.sFfUfmip1003Lckd = true;

            dataLoan.sFfUfmip1003 = 100.46M;

            dataLoan.sProHazInsMb = 0.00M;

            // Make sure that these are now 0 to trigger the code paths for changing to 0
            // as well as the code paths for the value being 0.
            Assert.AreEqual(0, dataLoan.sHazInsRsrv);
            Assert.AreEqual(0, dataLoan.sHazInsPia);

            // Make sure that the code paths that set up the UFMIP payments gets hit.
            Assert.AreEqual(100.00M, dataLoan.sFfUfmipFinanced);
            Assert.AreEqual(0.46M, dataLoan.sUfCashPd);

            Guid[] oldIds = new Guid[]
            {
                ((BorrowerClosingCostFee)dataLoan.sClosingCostSet.FindFeeByTypeId(DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumUpfrontFeeTypeId)).Payments.First().Id,
                ((BorrowerClosingCostFee)dataLoan.sClosingCostSet.FindFeeByTypeId(DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumUpfrontFeeTypeId)).Payments.Skip(1).First().Id,
                ((BorrowerClosingCostFee)dataLoan.sClosingCostSet.FindFeeByTypeId(DefaultSystemClosingCostFee.Hud900HazardInsuranceFeeTypeId)).Payments.First().Id,
                ((BorrowerClosingCostFee)dataLoan.sClosingCostSet.FindFeeByTypeId(DefaultSystemClosingCostFee.Hud1000HazardInsuranceReserveFeeTypeId)).Payments.First().Id,
            };

            dataLoan.Save();

            dataLoan = FakePageData.Create();
            dataLoan.InitLoad();

            Guid[] newIds = new Guid[]
            {
                ((BorrowerClosingCostFee)dataLoan.sClosingCostSet.FindFeeByTypeId(DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumUpfrontFeeTypeId)).Payments.First().Id,
                ((BorrowerClosingCostFee)dataLoan.sClosingCostSet.FindFeeByTypeId(DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumUpfrontFeeTypeId)).Payments.Skip(1).First().Id,
                ((BorrowerClosingCostFee)dataLoan.sClosingCostSet.FindFeeByTypeId(DefaultSystemClosingCostFee.Hud900HazardInsuranceFeeTypeId)).Payments.First().Id,
                ((BorrowerClosingCostFee)dataLoan.sClosingCostSet.FindFeeByTypeId(DefaultSystemClosingCostFee.Hud1000HazardInsuranceReserveFeeTypeId)).Payments.First().Id,
            };

            // Main test: Do the corresponding IDs match after saving and loading?
            CollectionAssert.AreEqual(oldIds, newIds);

            // Main test: Do the non-corresponding IDs differ from each other?
            Assert.AreNotEqual(newIds[0], newIds[1]);
            Assert.AreNotEqual(newIds[1], newIds[2]);
            Assert.AreNotEqual(newIds[2], newIds[3]);
        }

        private void EnsureFeeAndPaymentsValid(BorrowerClosingCostFee fee)
        {
            // These properties must be valid ClosingCostFee.
            //    1) There must be at least one payment.
            //    2) Sum of payments MUST equal fee.TotalAmount
            //    3) There can be only at MOST 1 generate payment.
            //    4) Generate Payment CANNOT be zero. -- NO LONG APPLY. 3/24/2015
            //    5) Generate Payment MUST BE last in list.

            Assert.GreaterOrEqual(fee.Payments.Count(), 1, "There must be at least one payment.");

            decimal sumOfPayment = 0;
            int generatePaymentCount = 0;
            decimal sumOfGeneratePayment = 0;

            foreach (BorrowerClosingCostFeePayment o in fee.Payments)
            {
                sumOfPayment += o.Amount;

                if (o.IsSystemGenerated)
                {
                    generatePaymentCount++;
                    sumOfGeneratePayment += o.Amount;
                }
            }

            Assert.AreEqual(fee.TotalAmount, sumOfPayment, "fee.TotalAmount MUST EQUAL sum of payments.");

            Assert.LessOrEqual(generatePaymentCount, 1, "There can be only at MOST 1 generate payment.");

            if (generatePaymentCount > 0)
            {
                // Assert.AreNotEqual(0, sumOfGeneratePayment, "Generate payment cannot have $0 amount.");

                Assert.AreEqual(true, fee.Payments.Last().IsSystemGenerated, "Generate payment must be a last record");
            }
        }
    }
}