﻿namespace LendOSolnTest
{
    using System;
    using LendersOffice.ObjLib.Extensions;
    using NUnit.Framework;

    [TestFixture]
    public class LqbSingleThreadInitializeLazyTest
    {
        internal static int Counter = 0;

        [TearDown]
        public void MethodTeardown()
        {
            LqbSingleThreadInitializeLazyTest.Counter = 0;
        }

        [Test]
        public void InitializeLazy_CreatesValue()
        {
            var lazy = this.GetTestLazy();
            Assert.IsNotNull(lazy.Value);
            Assert.IsTrue(lazy.IsValueCreated);
        }

        [Test]
        public void MultipleThreads_InitializerOnlyRunsOnce()
        {
            var lazy = this.GetTestLazy();
            System.Threading.Tasks.Parallel.For(0, 100, (index) =>
            {
                Assert.IsNotNull(lazy.Value);
                Assert.AreEqual(1, LqbSingleThreadInitializeLazyTest.Counter);
            });
        }

        [Test]
        public void ThrowsException_AllowsRetry()
        {
            var throwException = true;
            var lazy = new LqbSingleThreadInitializeLazy<LazyTestMock>(() => LazyTestMock.CreateWithException(throwException));

            try
            {
                Assert.IsNull(lazy.Value);
            }
            catch (InvalidOperationException)
            {
                // Expected exception
            }

            throwException = false;
            Assert.IsNotNull(lazy.Value);
        }

        private LqbSingleThreadInitializeLazy<LazyTestMock> GetTestLazy()
        {
            return new LqbSingleThreadInitializeLazy<LazyTestMock>(() => new LazyTestMock());
        }

        private class LazyTestMock
        {
            public LazyTestMock()
            {
                LqbSingleThreadInitializeLazyTest.Counter++;
            }

            public static LazyTestMock CreateWithException(bool throwException)
            {
                if (throwException)
                {
                    throw new InvalidOperationException();
                }

                return new LazyTestMock();
            }
        }
    }
}
