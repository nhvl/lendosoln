///Author Francesc Calvo Serra
using System;
using DataAccess;
using NUnit.Framework;
using LendersOffice.Security;
using LendersOffice.Constants;
using LendOSolnTest.Common;
using PriceMyLoan.los.DataAccess;

namespace LendOSolnTest
{
    [TestFixture]
    public class PMLAddLiabilityTest
    {
        private Guid m_loanId = Guid.Empty;
        private BrokerUserPrincipal m_principal;

        private const E_DebtRegularT DEBT_TYPE = E_DebtRegularT.Open;
        private const E_LiaOwnerT OWNER_TYPE = E_LiaOwnerT.Borrower;

        #region Test dummy data
        private const string COM_NM = "CompanyName";
        private const string BAL = "$1,000.00";
        private const string PMT = "$450.00";
        private const string COM_ADDR = "123 Company Address";
        private const string COM_CITY = "Los Angeles";
        private const string COM_STATE = "CA";
        private const string COM_ZIP = "90210";
        private const string ACC_NM = "AccountHolder";
        private const string ACC_NUM = "123456789-0";
        private const bool RATIO = true;
        private const bool PAID = false;
        #endregion

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            LoginTools.LoginAs(TestAccountType.LoTest001);
            m_principal = BrokerUserPrincipal.CurrentPrincipal;

            CLoanFileCreator creator = CLoanFileCreator.GetCreator(m_principal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
            m_loanId = creator.CreateBlankLoanFile();
        }

        private CPageData CreateDataObject(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(PMLAddLiabilityTest));
        }


        [Test]
        public void LiabilityCountTest()
        {
            int count = 0;

            CPageData dataLoan = CreateDataObject(m_loanId);
            dataLoan.InitLoad();
            CAppData dataApp = dataLoan.GetAppData(0);
            ILiaCollection recordList = dataApp.aLiaCollection;

            PmlAddLiabilityData liability = new PmlAddLiabilityData(m_loanId, dataApp.aAppId, m_principal);
            for (int i = 0; i < 5; i++)
            {
                liability.AddEmpty(ConstAppDavid.SkipVersionCheck);
                count++;
            }

            dataLoan = CreateDataObject(m_loanId);
            dataLoan.InitLoad();
            dataApp = dataLoan.GetAppData(0);
            recordList = dataApp.aLiaCollection;

            Assert.AreEqual(count, recordList.CountRegular);

        }


        [Test]
        public void LiabilityDataCheck()
        {
            CPageData dataLoan = CreateDataObject(m_loanId);
            dataLoan.InitLoad();
            CAppData dataApp = dataLoan.GetAppData(0);
            ILiaCollection recordList = dataApp.aLiaCollection;

            PmlAddLiabilityData liability = new PmlAddLiabilityData(m_loanId, dataApp.aAppId, m_principal);
            Guid recordId = liability.Add(ConstAppDavid.SkipVersionCheck,
                  DEBT_TYPE
                , OWNER_TYPE
                , BAL
                , PMT
                , COM_NM
                , COM_ADDR
                , COM_CITY
                , COM_STATE
                , COM_ZIP
                , ACC_NM
                , ACC_NUM
                , RATIO
                , PAID
                );

            dataLoan = CreateDataObject(m_loanId);
            dataLoan.InitLoad();
            dataApp = dataLoan.GetAppData(0);
            recordList = dataApp.aLiaCollection;

            Assert.IsTrue(recordList.HasRecordOf(recordId));

            var record = recordList.GetRegRecordOf(recordId);
            ILiabilityRegular newLiability = record;

            Assert.AreEqual(newLiability.DebtT, DEBT_TYPE);
            Assert.AreEqual(newLiability.OwnerT, OWNER_TYPE);
            Assert.AreEqual(newLiability.Bal_rep, BAL);
            Assert.AreEqual(newLiability.Pmt_rep, PMT);
            Assert.AreEqual(newLiability.ComNm, COM_NM);
            Assert.AreEqual(newLiability.ComAddr, COM_ADDR);
            Assert.AreEqual(newLiability.ComCity, COM_CITY);
            Assert.AreEqual(newLiability.ComState, COM_STATE);
            Assert.AreEqual(newLiability.ComZip, COM_ZIP);
            Assert.AreEqual(newLiability.AccNm, ACC_NM);
            Assert.AreEqual(newLiability.AccNum.Value, ACC_NUM);
            Assert.AreEqual(newLiability.NotUsedInRatio, !RATIO);
            Assert.AreEqual(newLiability.WillBePdOff, PAID);
        }


        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            if (m_loanId != Guid.Empty)
            {
                Tools.DeclareLoanFileInvalid(BrokerUserPrincipal.CurrentPrincipal, m_loanId, false, false);
            }
        }
    }
}