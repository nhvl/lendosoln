﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using DataAccess;
using System.Reflection;
using LendersOffice.Security;
using LendOSolnTest.Common;
using LendersOffice.Constants;
using LendersOfficeApp.los.admin;
using LendersOffice.Audit;

namespace LendOSolnTest.GFEArchive
{
    [TestFixture]
    [Category(CategoryConstants.RunBeforePullRequest)]
    public class GFEArchiveTest
    {
        AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.LoTest001);
        private Guid loanID = Guid.Empty;


        /// <summary>
        /// Fields that the GFE page appear to depend on but aren't actually needed for getting the GFE. <para></para>
        /// DO NOT MODIFY at runtime.
        /// </summary>
        #region WhiteListedFieldsForGFEArchive
        private HashSet<string> WhiteListedFieldsForGFEArchive = new HashSet<string>(
        new string[]
        {
            "s800U1FSettlementSection"
            , "s800U2FSettlementSection"
            , "s800U3FSettlementSection"
            , "s800U4FSettlementSection"
            , "s800U5FSettlementSection"
            , "s900U1PiaSettlementSection"
            , "s904PiaSettlementSection"
            , "sAdjustmentList"
            , "sAdjustmentListJson"
            , "sAssetsAsAdjustmentsJson"
            , "sArmIndexNameVstr"
            , "sAssumeLT"
            , "sAttorneyFSettlementSection"
            , "sDocMagicClosingD"
            , "sDocMagicClosingDLckd"
            , "sDocMagicDisbursementD"
            , "sDocMagicDisbursementDLckd"
            , "sDocPrepFSettlementSection"
            , "sEscrowFSettlementSection"
            , "sGrossDueFromBorrPersonalProperty"
            , "sHasDemandFeature"
            , "sIsAllowExcludeToleranceCure"
            , "sLastDisclosedClosingDisclosureArchiveD"
            , "sLastDisclosedClosingDisclosureArchiveId"
            , "sLateChargeBaseDesc"
            , "sLateChargePc"
            , "sLateDays"
            , "sLDiscnt1003"
            , "sLDiscnt1003Lckd"
            , "sLenderCaseNum"
            , "sLenderCaseNumLckd"
            , "sMiCertId"
            , "sNonMIHousingExpensesNotEscrowedReasonT"
            , "sNotaryFSettlementSection"
            , "sOCredit1Amt"
            , "sOCredit1Desc"
            , "sOCredit1Lckd"
            , "sOCredit2Amt"
            , "sOCredit2Desc"
            , "sOCredit2Lckd"
            , "sOCredit3Amt"
            , "sOCredit3Desc"
            , "sOCredit4Amt"
            , "sOCredit4Desc"
            , "sONewFinCc"
            , "sPrepmtPeriodMonths"
            , "sProrationListJson"
            , "sReductionsDueToSellerExcessDeposit"
            , "sReductionsDueToSellerPayoffOf1stMtgLoan"
            , "sReductionsDueToSellerPayoffOf2ndMtgLoan"
            , "sRefPdOffAmt1003"
            , "sRefPdOffAmt1003Lckd"
            , "sReqPropIns"
            , "sSellerResponsibleClosingCostSetJsonContent"
            , "sSettlement1008RsrvMon"
            , "sSettlement1009RsrvMon"
            , "sSettlement800U1F"
            , "sSettlement800U1FDesc"
            , "sSettlement800U2F"
            , "sSettlement800U2FDesc"
            , "sSettlement800U3F"
            , "sSettlement800U3FDesc"
            , "sSettlement800U4F"
            , "sSettlement800U4FDesc"
            , "sSettlement800U5F"
            , "sSettlement800U5FDesc"
            , "sSettlement900U1Pia"
            , "sSettlement900U1PiaDesc"
            , "sSettlement904Pia"
            , "sSettlement904PiaDesc"
            , "sSettlementAggregateAdjRsrv"
            , "sSettlementAggregateAdjRsrvLckd"
            , "sSettlementApprF"
            , "sSettlementApprFPaid"
            , "sSettlementAttorneyF"
            , "sSettlementBrokerCredit"
            , "sSettlementCountyRtcBaseT"
            , "sSettlementCountyRtcMb"
            , "sSettlementCountyRtcPc"
            , "sSettlementCreditLenderPaidItemT"
            , "sSettlementCrF"
            , "sSettlementCrFPaid"
            , "sSettlementDocPrepF"
            , "sSettlementEscrowF"
            , "sSettlementFloodCertificationF"
            , "sSettlementFloodInsRsrvMon"
            , "sSettlementHazInsPiaMon"
            , "sSettlementHazInsRsrvMon"
            , "sSettlementInspectF"
            , "sSettlementIPerDay"
            , "sSettlementIPerDayLckd"
            , "sSettlementIPiaDy"
            , "sSettlementIPiaDyLckd"
            , "sSettlementLDiscntBaseT"
            , "sSettlementLDiscntFMb"
            , "sSettlementLDiscntPc"
            , "sSettlementLDiscntProps"
            , "sSettlementLOrigBrokerCreditFProps"
            , "sSettlementLOrigFMb"
            , "sSettlementLOrigFPc"
            , "sSettlementMBrokFBaseT"
            , "sSettlementMBrokFMb"
            , "sSettlementMBrokFPc"
            , "sSettlementMInsRsrvMon"
            , "sSettlementNotaryF"
            , "sSettlementOwnerTitleInsF"
            , "sSettlementPestInspectF"
            , "sSettlementProcF"
            , "sSettlementProcFPaid"
            , "sSettlementRealETxRsrvMon"
            , "sSettlementRecBaseT"
            , "sSettlementRecDeed"
            , "sSettlementRecFLckd"
            , "sSettlementRecFMb"
            , "sSettlementRecFPc"
            , "sSettlementRecMortgage"
            , "sSettlementRecRelease"
            , "sSettlementSchoolTxRsrvMon"
            , "sSettlementStateRtcBaseT"
            , "sSettlementStateRtcMb"
            , "sSettlementStateRtcPc"
            , "sSettlementTitleInsF"
            , "sSettlementTxServF"
            , "sSettlementU1GovRtcBaseT"
            , "sSettlementU1GovRtcDesc"
            , "sSettlementU1GovRtcMb"
            , "sSettlementU1GovRtcPc"
            , "sSettlementU1Sc"
            , "sSettlementU1ScDesc"
            , "sSettlementU1Tc"
            , "sSettlementU1TcDesc"
            , "sSettlementU2GovRtcBaseT"
            , "sSettlementU2GovRtcDesc"
            , "sSettlementU2GovRtcMb"
            , "sSettlementU2GovRtcPc"
            , "sSettlementU2Sc"
            , "sSettlementU2ScDesc"
            , "sSettlementU2Tc"
            , "sSettlementU2TcDesc"
            , "sSettlementU3GovRtcBaseT"
            , "sSettlementU3GovRtcDesc"
            , "sSettlementU3GovRtcMb"
            , "sSettlementU3GovRtcPc"
            , "sSettlementU3RsrvMon"
            , "sSettlementU3Sc"
            , "sSettlementU3ScDesc"
            , "sSettlementU3Tc"
            , "sSettlementU3TcDesc"
            , "sSettlementU4RsrvMon"
            , "sSettlementU4Sc"
            , "sSettlementU4ScDesc"
            , "sSettlementU4Tc"
            , "sSettlementU4TcDesc"
            , "sSettlementU5Sc"
            , "sSettlementU5ScDesc"
            , "sSettlementUwF"
            , "sSettlementWireF"
            , "sSoftPrepmtPeriodMonths"
            , "sTitleInsFSettlementSection"
            , "sToleranceCureCalculationT"
            , "sTotCcPbs"
            , "sTotCcPbsLocked"
            , "sTotEstPp1003"
            , "sTotEstPp1003Lckd"
            , "sTransNetCash"
            , "sTransNetCashLckd"
            , "sTRIDEscrowWaiverFee"
            , "sTRIDIsLoanEstimateRateLocked"
            , "sTRIDLoanEstimateNoteIRAvailTillD"
            , "sTRIDLoanEstimateNoteIRAvailTillDTimeZoneT"
            , "sTRIDLoanEstimateSetLockStatusMethodT"
            , "sU1GovRtcSettlementSection"
            , "sU1ScSettlementSection"
            , "sU1TcSettlementSection"
            , "sU2GovRtcSettlementSection"
            , "sU2ScSettlementSection"
            , "sU2TcSettlementSection"
            , "sU3GovRtcSettlementSection"
            , "sU3ScSettlementSection"
            , "sU3TcSettlementSection"
            , "sU4ScSettlementSection"
            , "sU4TcSettlementSection"
            , "sU5ScSettlementSection"
            , "sTRIDLoanEstimateCashToCloseCalcMethodT"
            , "sfGetGfeDescription"
            , "sfSetGfeDescription"
            , "sfGetGfe32BitPropsField"
            , "sfSetGfe32BitPropsField"
            , "sEstCcItems"
            , "sLDiscnt"
            , "sGfeOriginationF"
            , "sTestLoanFileEnvironmentId"
            , "sLoanFileT"
            , "sLoanEstScAvailTillD_Time"
            , "sONewFinNetProceeds"
            , "sONewFinNetProceedsLckd"
            , "sTRIDLoanEstimateLenderIntendsToServiceLoan"
            , "sSettlementAgentFileNum"
            , "sTRIDPartialPaymentAcceptanceT"
            , "sTRIDPropertyIsInNonRecourseLoanState"
            , "sInitialLoanEstimateCreatedD"
            , "sInitialLoanEstimateIssuedD"
            , "sLoanEstimateDatesInfo"
            , "sClosingDisclosureDatesInfo"
            , "sClosingCostAutomationUpdateT"
            , "sExcludeToleranceCureFromFinal1003LenderCredit"
            , "sWasCCAutomationEnabledOnLastRegistration"
            , "sIsRefinancing"
            , "sfCalculatePmlLoans"
            , "sLtvROtherFinPe"
            , "sNonZeroMinSalesPriceApprValPe"
            , "sHas2ndFinPe"
            , "sLtvROtherFin"
            , "sEmployeeLoanRepId"
            , "sEmployeeExternalSecondaryId"
            , "sfUpdateAffiliateOfficialContactValues"
            , "sfUpdatePmlBrokerId"
            , "sfComputeLpePriceGroupId"
            , "sfGetAgentRecordCount"
            , "sfGetAgentFields"
            , "sHouseVal"
            , "sApprValFor_LTV_CLTV_HCLTV"
            , "sPriorAppraisalUsed"
            , "PmlBrokerAffiliates"
            , "sfTransformPmlPropertyTypeToLendersOffice"
            , "sToleranceTenPercentCure"
            , "sDisclosuresDueD_Old"
            , "sBranchChannelT"
            , "sPropertyTransferD"
            , "sPropertyTransferDLckd"
            , "sClosingDisclosureDatesJSONContent"
            , "sfSumClosingCostFeeTotalsMatching"
            , "sTotLenderRsrv"
            , "sLOrigF"
            , "sCalculateInitialLoanEstimate"
            , "sCalculateInitialClosingDisclosure"
            , "sMBrokF"
            , "sRecF"
            , "sCountyRtc"
            , "sStateRtc"
            , "sU1GovRtc"
            , "sU2GovRtc"
            , "sU3GovRtc"
            , "sfGetGfeFeeBaseAmount"
            , "sfSetGfeFeeBaseAmount"
            , "sfGetGfePercent"
            , "sfSetGfePercent"
            , "sfGetGfePercentBaseT"
            , "sfSetGfePercentBaseT"
            , "sfGetGfeFeeTotalAmount"
            , "sfSetGfeFeeTotalAmount"
            , "sMBrokFBaseAmt"
            , "sfGetGfeSection"
            , "sfSetGfeSection"
            , "sfGetGfePaidTo"
            , "sfSetGfePaidTo"
            , "sAvailableSettlementServiceProvidersJson"
            , nameof(CPageBase.sFinalLAmt)
            , nameof(CPageBase.sHousingExpenses)
            , nameof(CPageBase.sLine1008Expense)
            , nameof(CPageBase.sLine1009Expense)
            , nameof(CPageBase.sLine1010Expense)
            , nameof(CPageBase.sLine1011Expense)
            , nameof(CPageBase.sFloodExpense)
            , nameof(CPageBase.sSchoolTaxExpense)
            , nameof(CPageBase.sHazardExpense)
            , nameof(CPageBase.sHOADuesExpense)
            , nameof(CPageBase.sRealEstateTaxExpense)
            , nameof(CPageBase.sIs8020PricingScenario)
            , nameof(CPageBase.sLTotI)
            , nameof(CPageBase.sMonthlyPmtQual)
            , nameof(CPageBase.sSpRentalCf)
            , "sEnableNewExpenseCalculations"
            , "sfGet_sProMIns_ByMonth"
            , "sfGetUsdaMonthlyMipList"
            , "sfGetUsdaAverageOutstandingBalanceList"
            , "sfGetFhaMonthlyMipList"
            , "sQualIRLckd"
            , "sUseQualRate"
            , "sQualRateCalculationT"
            , "sQualRateCalculationFieldT1"
            , "sQualRateCalculationFieldT2"
            , "sQualRateCalculationAdjustment1"
            , "sQualRateCalculationAdjustment2"
            , "sLenderFeeBuyoutRequestedT"
            , "sLeadSrcId"
            , "sIsNewConstruction"
            , "sHmdaActionTakenT"
            , "sCalculateInitAprAndLastDiscApr"
            , nameof(CPageBase.sIsStatusEventMigrated)
            , "sStatusEventMigrationVersion"
            , "sIsRequireAlternateCashToCloseForNonSellerTransactions"
            , "sLoanTransactionInvolvesSeller"
            , "sLoanTransactionInvolvesSellerLckd"
            , "sProOFinBalPe"
            , "sNonZeroMinSalesPriceApprVal"
            , "sCostConstrRepairsRehab"
            , "sArchitectEngineerFee"
            , "sConsultantFee"
            , "sRenovationConstrInspectFee"
            , "sTitleUpdateFee"
            , "sPermitFee"
            , "sFeasibilityStudyFee"
            , "sFHA203kType"
            , "sContingencyRsrvPcRenovationHardCost"
            , "sFinanceMortgagePmntRsrvTgtVal"
            , "sFinanceMortgagePmntRsrvTgtValLckd"
            , "sRenoMtgPmntRsrvMnth"
            , "sFinanceOriginationFeeTgtVal"
            , "sDiscntPntOnRepairCostFeeTgtVal"
            , "sfGetPropsUpdatedWithLinkedProps"
            , "sfGetLinkedPaidToThirdParty"
            , "sfGetLinkedThisPartyIsAffiliate"
            , "sfCalcNewAggregateAdj"
            , "sHazInsRsrvMonRecommended"
            , "sMInsRsrvMonRecommended"
            , "sRealETxRsrvMonRecommended"
            , "sSchoolTxRsrvMonRecommended"
            , "sFloodInsRsrvMonRecommended"
            , "s1006RsrvMonRecommended"
            , "s1007RsrvMonRecommended"
            , "sfIsIncludeInEscrowPerTable"
            , "sInitialEscrowAcc"
            , "sfIsIncludeInEscrow"
            , "sEscrowPmt"
            , "sAggrEscrowCushionRequired"
            , "sfCalculateRsrvMonRecommended"
            , "sLockPolicy"
            , "sPriceGroup"
            , "sLAmtCalc"
            , "sProThisMPmt"
            , "sProMIns2"
            , "sProRealETx"
            , "sProHazIns"
            , "sLDiscntBaseAmt"
            , "sVaFf"
            , "sHazInsPia"
            , "sMipPia"
            , "sRecurringMipPia"
            , "sUpfrontMipPia"
            , "sIPia"
            , "s1006Rsrv"
            , "s1007Rsrv"
            , "sFloodInsRsrv"
            , "sRealETxRsrv"
            , "sSchoolTxRsrv"
            , "sMInsRsrv"
            , "sHazInsRsrv"
            , "sfIsEscrowed"
            , "sfCalculateTotalAmount"
            , "sTLAmtUFMIPCalcTMaxTargetTLAmt"
            , "sfGetPaidToString"
            , "sPriceGroupLenderPaidOriginatorCompensationOptionT"
            , "sIsAutoCalculateLastDisclosedArchiveId"
            , "sOtherRenovationCost"
            , "sTotalRenovationCostsPeVal"
            , "sLtvRPe"
            , "sFeeNewLoan"
            , "sInducementPurchPrice"
            , "sFHAExistingMLien"
            , "sFHAIsExistingDebt"
            , "sMonthlyPmt"
            , "sAltCostLckd"
            , "sTotalRenovationCostsLckd"
            , "sTRIDClosingDisclosureAltCost"
            , "sTRIDClosingDisclosureAltCostLckd"
            , "sHelocCalculatePrepaidInterest"
            , "sTridTargetRegulationVersionT"
            , "sTridTargetRegulationVersionTLckd"
            , "sServicingPmtXmlContent"
            , "sServicingOtherPmts"
            , "sLienToPayoffTotDebt"
            , "sTridClosingDisclosureCalculateEscrowAccountFromT"
            , "sNonMIHousingExpensesEscrowedReasonT"
            , "sBorrowerApplicationCollectionT"
            , "Liabilities"
            , "RealProperties"
            , "Assets"
            , "ConsumerLiabilities"
            , "ConsumerRealProperties"
            , "ConsumerAssets"
            , "EmploymentRecords"
            , nameof(CPageBase.sQualTerm)
            , nameof(CPageBase.sQualTermCalculationType)
            , nameof(CPageBase.sLeadD)
            , nameof(CPageBase.sLeadDeclinedD)
            , nameof(CPageBase.sLeadCanceledD)
            , nameof(CPageBase.sLeadOtherD)
            , "sAlwaysRequireAllBorrowersToReceiveClosingDisclosure"
            , "sSyncUladAndLegacyApps"

            , nameof (CPageBase.sLotLien)
            , nameof (CPageBase.sConstructionPurposeT)
            , nameof (CPageData.sLotImprovC)
            , nameof(CPageBase.sConstructionLoanD)
            , nameof(CPageBase.sConstructionIntAmount)
            , nameof(CPageBase.sSubsequentlyPaidFinanceChargeAmt)
            , nameof(CPageBase.sConstructionPhaseIntAccrualT)
            , nameof(CPageBase.sConstructionIntAccrualD)
            , nameof(CPageBase.sConstructionDiscT)
            , nameof(CPageBase.sConstructionIntCalcT)
            , nameof(CPageBase.sIsIntReserveRequired)
            , nameof(CPageBase.sConstructionLoanDLckd)
            , nameof(CPageBase.sConstructionIntAccrualDLckd)
            , nameof(CPageBase.sIsUseManualApr)
            , nameof(CPageBase.HousingHistoryEntries)
            , nameof(CPageBase.UladApplications)
            , nameof(CPageBase.UladApplicationConsumers)
            , nameof(CPageBase.sIsIncomeCollectionEnabled)
            , nameof(CPageBase.ConsumerIncomeSources)
            , nameof(CPageBase.IncomeSources)
            , "sCanSyncUladAndLegacyApps"
            , "LegacyApplications"
            , "sPmlLastGenerateResultD"
            , "sVaProThisMPmt"
            , "sVaProRealETx"
            , "sVaProHazIns"
            , "sVaMaintainAssessPmt"
            , "sVaSpecialAssessPmt"
            , "sFHAPro1stMPmt"
            , "sFHAProMIns"
            , "sFHAProHoAssocDues"
            , "sFHAProGroundRent"
            , "sFHAPro2ndFinPmt"
            , "sFHAProHazIns"
            , "sFHAProRealETx"
            , "PublicRecords"
            , "sVaProMaintenancePmt"
            , "sVaProUtilityPmt"
            , "sVaProThisMPmtLckd"
            , "sVaProRealETxLckd"
            , "sVaProHazInsLckd"
            , "sVaMaintainAssessPmtLckd"
            , "sFHAProMInsLckd"
            , "sTotalRealtyTaxesPITI"
            , "sTotalPropertyInsurancePITI"
            , "sTotalOtherHousingExpensesPITI"
            , "sGroundRentExpense"
            , "sIsConstructionLoan"
            , "sPurchPriceWithLandCost"
            , "sRefPdOffAmt"
            , "sForceSinglePaymentMipFrequency"
            , "sFHAPurposeIsStreamlineRefi"
            , nameof(CPageBase.sLienToIncludeCashDepositInTridDisclosures)
            , nameof(CPageBase.RealPropertyLiabilities)
            , nameof(CPageBase.IncomeSourceEmploymentRecords)
            , nameof(CPageBase.ConsumerPublicRecords)
            , nameof(CPageBase.sConstructionPeriodEndD)
            , nameof(CPageBase.sConstructionInitialOddDays)
            , nameof(CPageBase.sIsConstructionRAdjFloorRReadOnly)
            , nameof(CPageBase.sConstructionRAdjFloorTotalR)
            , nameof(CPageBase.sConstructionRAdjFloorLifeCapR)
            , nameof(CPageBase.sIsAcquiringDwelling)
            , nameof(CPageBase.sConstructionAmortT)
            , nameof(CPageBase.sConstructionRAdj1stCapMon)
            , nameof(CPageBase.sConstructionRAdj1stCapR)
            , nameof(CPageBase.sConstructionRAdjCapMon)
            , nameof(CPageBase.sConstructionRAdjCapR)
            , nameof(CPageBase.sConstructionRAdjFloorAddR)
            , nameof(CPageBase.sConstructionRAdjFloorCalcT)
            , nameof(CPageBase.sConstructionRAdjFloorR)
            , nameof(CPageBase.sConstructionRAdjIndexR)
            , nameof(CPageBase.sConstructionRAdjLifeCapR)
            , nameof(CPageBase.sConstructionRAdjMarginR)
            , nameof(CPageBase.sConstructionRAdjRoundT)
            , nameof(CPageBase.sConstructionRAdjRoundToR)
            , nameof(CPageBase.sConstructionFirstPaymentD)
            , nameof(CPageBase.sConstructionFirstPaymentDLckd)
            , nameof(CPageBase.sLotOwnerT)
            , nameof(CPageBase.sConstructionPeriodEndD)
            , nameof(CPageBase.sIsAcquiringDwelling)
            , nameof(CPageBase.sLienToIncludeCashDepositInTridDisclosures)
            , nameof(CPageBase.sIsPurchasePriceIncludedInValuation)
            , nameof(CPageBase.sIntReserveAmt)
            , nameof(CPageBase.sFinCharge)
            , nameof(CPageBase.sIsTargeting2019Ulad)
            , nameof(CPageBase.sSellerResponsibleClosingCostSet)
            , nameof(CPageBase.sProMInsCancelAppraisalLtv)
        });
        #endregion

        [Test]
        public void TEMP_GFEArchiveFieldNames_HasAllNecessaryFields()
        {
            var gfeArchiveFields = InitializeGFEArchiveFields(new Type[]{
                    typeof(LendersOfficeApp.newlos.Forms.GoodFaithEstimate2010),
                    typeof(LendersOffice.Pdf.AbstractGoodFaithEstimate2010PDF)
                });
            var currentHardcodedFields = CPageBase.TEMP_GFEArchiveFieldNamesFrom2013Q3Release;
            var currentFieldsNeeded = from p in gfeArchiveFields
                                      where false == WhiteListedFieldsForGFEArchive.Contains(p.Name)
                                    select p.Name;

            bool needVerify = true;
            var missingFields = currentFieldsNeeded.Except(currentHardcodedFields);
            var missingFieldsForPrinting = missingFields.Select(f => "\""+f+"\"").ToArray();
            if (missingFields.Count() > 0)
            {
                var missingListAsString = string.Join(Environment.NewLine + ","
                    , missingFieldsForPrinting);
                Console.WriteLine(missingListAsString);

                string errMsg = "please consider adding to LFF.TEMP_GFEArchiveFieldNamesFrom2013Q3Release OR GFEArchiveTest.cs > WhiteListedFieldsForGFEArchive (if you're sure it won't be needed by the old archive) the fields: " + Environment.NewLine + missingListAsString;

                GenerateSameSelectSqlQueryAssert(currentHardcodedFields, currentFieldsNeeded, errMsg);
                needVerify = false;

                Tools.LogWarning(errMsg);
            }

            var extraFields = currentHardcodedFields.Except(currentFieldsNeeded);
            var extraFieldsForPrinting = extraFields.Select(f => "\"" + f + "\"").ToArray();
            if (extraFields.Count() > 0)
            {
                var extraListAsString = string.Join(Environment.NewLine + ","
                    , extraFieldsForPrinting);
                Console.WriteLine(extraListAsString);

                string errMsg = "please consider the effects of deletion in ApplyGFEArchive," +
                               " and delete from LFF.TEMP_GFEArchiveFieldNamesFrom2013Q3Release the fields: " + Environment.NewLine + extraListAsString;

                if (needVerify)
                {
                    GenerateSameSelectSqlQueryAssert(currentHardcodedFields, currentFieldsNeeded, errMsg);
                    needVerify = false; // Don't need it. But keep for easily maintain.
                }

                Tools.LogWarning(errMsg);
            }

        }

        /// <summary>
        /// Verify that we can perform a GFE Archive.
        /// </summary>
        [Test]
        public void ArchiveGFEAndCoC()
        {
            this.PrepareToArchive();

            var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                this.loanID,
                typeof(GFEArchiveTest));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);
            loan.sDisclosureRegulationTLckd = true;
            loan.sDisclosureRegulationT = E_sDisclosureRegulationT.GFE;
            loan.sClosingCostFeeVersionT = E_sClosingCostFeeVersionT.Legacy;

            loan.ArchiveGFE(E_GFEArchivedReasonT.ManuallyArchived);

            loan.Save();
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            loan.sCircumstanceChangeExplanation = "This is a test, yo.";
            loan.sCircumstanceChange1FeeId = "LQBGFE804";
            loan.sCircumstanceChange1Amount_rep = "1";

            loan.ArchiveCoC(false, false, false, false);
        }

        /// <summary>
        /// Creates a loan and enables the GFE Archive feature as needed.
        /// </summary>
        private void PrepareToArchive()
        {
            if (this.loanID == Guid.Empty)
            {
                this.CreateLoan();
            }

            if (!this.principal.BrokerDB.IsGFEandCoCVersioningEnabled)
            {
                this.principal.BrokerDB.TempOptionXmlContent = this.principal.BrokerDB.TempOptionXmlContent.Value +
                    "<option name=\"enablegfeandcocversioning\" value=\"true\" />";
            }
        }

        /// <summary>
        /// Creates a blank loan.
        /// </summary>
        private void CreateLoan()
        {
            var fileCreator = CLoanFileCreator.GetCreator(
                this.principal,
                E_LoanCreationSource.UserCreateFromBlank);

            this.loanID = fileCreator.CreateBlankLoanFile();
        }

        [TestFixtureTearDown]
        public void TearDown()
        {
            if (this.loanID != Guid.Empty)
            {
                bool sendNotifications = false;
                bool generateLinkLoanMsgEvent = false;

                Tools.DeclareLoanFileInvalid(
                    this.principal,
                    this.loanID,
                    sendNotifications,
                    generateLinkLoanMsgEvent);
            }
        }

        /// <summary>
        /// figure out the fields that the GFEArchive depends on, and cache them.
        /// </summary>
        private static IEnumerable<PropertyInfo> InitializeGFEArchiveFields(Type[] ts)
        {
            List<PropertyInfo> gfeArchiveFields = null;

            if (gfeArchiveFields == null || gfeArchiveFields.Count() == 0)
            {
                gfeArchiveFields = new List<PropertyInfo>();

                var fieldNames = CPageBase.GetPageBaseOrAppDBFieldsForTypes(ts);
                foreach (var fieldName in fieldNames)
                {
                    if (fieldName == "IgnoreThisField" || fieldName == "sfCreateLegacyFee" || fieldName == "sAggregateEscrowAccount" || fieldName == "sClosingCostSet")
                    {
                        continue;
                    }
                    var pi = CPageBase.GetPageBaseOrAppBasePropertyInfo(fieldName);
                    if (pi != null)
                    {
                        // include CPageBase props, ignore CAppBase props.
                        if (pi.DeclaringType == typeof(CPageBase))
                        {
                            gfeArchiveFields.Add(pi);
                        }
                    }
                    else
                    {
                        Tools.LogError(fieldName + " was missing from CPageBase/CAppBase");
                    }
                }
            }
            return gfeArchiveFields;
        }

        void GenerateSameSelectSqlQueryAssert(IEnumerable<string> expect, IEnumerable<string> actual, string errMsg)
        {
            var expectedStat = CSelectStatementProvider.GetProviderFor(E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, expect.ToList()).GenerateSelectStatement(null);
            var actualStat = CSelectStatementProvider.GetProviderFor(E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, actual.ToList()).GenerateSelectStatement(null);

            Func<string, string> toDisplayMsg = (string queryType) => errMsg + Environment.NewLine + $"Their {queryType} are different." + Environment.NewLine + 
                                                                     $"Note: ConstStage.UseUpdateGraphLegacy = {ConstStage.UseUpdateGraphLegacy}";

            Assert.AreEqual(expectedStat.LoanSelectFormat, actualStat.LoanSelectFormat, toDisplayMsg("LoanSelectFormat"));
            Assert.AreEqual(expectedStat.AppSelectFormat, actualStat.AppSelectFormat, toDisplayMsg("AppSelectFormat"));
            Assert.AreEqual(expectedStat.AssignedEmployeeSelectFormat, actualStat.AssignedEmployeeSelectFormat, toDisplayMsg("AssignedEmployeeSelectFormat"));
            Assert.AreEqual(expectedStat.AssignedTeamSelectFormat, actualStat.AssignedTeamSelectFormat, toDisplayMsg("AssignedTeamSelectFormat"));
            Assert.AreEqual(expectedStat.SubmissionSnapshotSelectFormat, actualStat.SubmissionSnapshotSelectFormat, toDisplayMsg("SubmissionSnapshotSelectFormat"));
            Assert.AreEqual(expectedStat.Query_LoanFileA, actualStat.Query_LoanFileA, toDisplayMsg("Query_LoanFileA"));
            Assert.AreEqual(expectedStat.Query_LoanFileB, actualStat.Query_LoanFileB, toDisplayMsg("Query_LoanFileB"));
            Assert.AreEqual(expectedStat.Query_LoanFileC, actualStat.Query_LoanFileC, toDisplayMsg("Query_LoanFileC"));
            Assert.AreEqual(expectedStat.Query_LoanFileD, actualStat.Query_LoanFileD, toDisplayMsg("Query_LoanFileD"));
            Assert.AreEqual(expectedStat.Query_LoanFileE, actualStat.Query_LoanFileE, toDisplayMsg("Query_LoanFileE"));
            Assert.AreEqual(expectedStat.Query_LoanFileF, actualStat.Query_LoanFileF, toDisplayMsg("Query_LoanFileF"));
            Assert.AreEqual(expectedStat.Query_Branch, actualStat.Query_Branch, toDisplayMsg("Query_Branch"));
        }
    }
}
