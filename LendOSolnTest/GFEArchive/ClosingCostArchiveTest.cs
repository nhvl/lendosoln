﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using DataAccess;
using System.Reflection;
using LendersOffice.Security;
using LendOSolnTest.Common;
using LendersOffice.Constants;
using LendersOfficeApp.los.admin;
using LendersOffice.Audit;
using DavidDao.Reflection;
using LendersOffice.Common.SerializationTypes;

namespace LendOSolnTest.ClosingCostArchiveTest
{
    [TestFixture]
    [Category(CategoryConstants.RunBeforePullRequest)]
    public class ClosingCostArchiveTest
    {
        /// <summary>
        /// Q: What does this test do?
        /// A: This test exists to make sure that the Loan Estimate Archive has ALL
        /// fields that are needed by the loan estimate page.  If any are missing,
        /// it is a data loss situation.

        /// Q: How do I fix it when this test is failing?
        /// A: The fields that are in the Loan Estimate but not in a Loan Estimate Archive probably
        /// need to be added to the Loan Estimate archive format.  May need to check with
        /// PDE how to handle old archives that are missing this new one.
        /// </summary>
        [Test]
        public void DoesLoanEstimatePageUseFieldsNotStoredInArchive()
        {
            List<string> ignoreListForLoanEstimate = new List<string>()
            {
                "sMinMaxInterestRate",
                "sLimitsOnInterestRateFirstChange",
                "sLimitsOnInterestRateSubsequentChanges",
                "sIsRequireFeesFromDropDown",
                "sClosingCostFeeVersionT",
                "sIPerDayLckd",
                "sIPiaDyLckd",
                "sLastDisclosedClosingCostArchive",
                "sHasLoanEstimateArchiveInUnknownStatus",
                "sLoanEstimateArchiveInUnknownStatus",
                "sLoanEstimateDatesInfo",
                "sHasLastDisclosedLoanEstimateArchive",
                "sHasLoanEstimateArchiveInPendingStatus",
                "sLoanEstimateArchiveInPendingStatus",
                "sTRIDLender",
                "sTRIDLoanOfficer",
                "sTRIDMortgageBroker",
                "sIsRequireAlternateCashToCloseForNonSellerTransactions",
                "sLoanTransactionInvolvesSeller",
                "sIsRenovationLoan", // These two renovation fields are used for UI only and don't appear in the archive
                "sTotalRenovationCosts",
                "UpdateLinkedServicingPayments",
                "sLienPosT",
                // OPM 467063. Only field in this method not in the archive is sConcurSubFin. 
                // According to Camron, sConcurSubFin does not need to be in the archive
                "ValidateAdjustmentsAndProrationsForTrid2"
            };



            HashSet<string> missingList;
            if (DoesPageUseFieldsNotStoredInArchive(ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                typeof(LendersOfficeApp.newlos.Disclosure.LoanEstimate),
                ignoreListForLoanEstimate,
                out missingList))
            {
                Assert.Fail("Loan Estimate Page uses the following fields that are not stored in a Loan Estimate Format Archive: " + Commafy(missingList) + ".");
            }
                
        }

        /// <summary>
        /// Q: What does this test do?
        /// A: This test exists to make sure that the 2015 GFE Archive has ALL
        /// fields that are needed by the 2015 GFE page.  If any are missing,
        /// it is a data loss situation.

        /// Q: How do I fix it when this test is failing?
        /// A: The fields that are in the 2015 GFE but not in a 2015 GFE Archive probably
        /// need to be added to the 2015 GFE archive format.  May need to check with
        /// PDE how to handle old archives that are missing this new one.
        /// </summary>
        [Test]
        public void DoesGFE2015PageUseFieldsNotStoredInArchive()
        {
            List<string> ignoreForGFE2015Page = new List<string> {
                // ejm OPM 215135 - These fields are on the Lender Credits tab on the Footer. They don't need to be added to the archive.
                "sBrokerLockOriginatorPriceBrokComp1PcPrice",
                "sBrokerLockOriginatorPriceBrokComp1PcAmt",
                "sLDiscntPc",
                "sLDiscntBaseT",
                "sLDiscntFMb",
                "sGfeCreditLenderPaidItemF",
                "sGfeLenderCreditF",
                "sLenderPaidItemNotIncludedInInitialDisclosureAmtAsChargeAmt",
                "sLenderCustomCredit1Description",
                "sLenderCustomCredit1Amount",
                "sLenderCustomCredit2Description",
                "sLenderCustomCredit2Amount",
                "sLenderActualTotalCreditAmt_Neg",
                "sLenderCreditAvailableAmt_Neg",
                "sLenderPaidFeeDiscloseLocationT",
                "sLenderPaidFeesAmt_Neg",
                "sLenderGeneralCreditDiscloseLocationT",
                "sLenderGeneralCreditAmt_Neg",
                "sLenderCustomCredit1DiscloseLocationT",
                "sLenderCustomCredit2DiscloseLocationT",
                "sLenderCustomCredit1AmountAsCharge",
                "sLenderCustomCredit2AmountAsCharge",
                "sIsRequireFeesFromDropDown",
                "sLenderCreditCalculationMethodT",
                "sClosingCostFeeVersionT",
                "sIPerDayLckd",
                "sIPiaDyLckd",
                // OPM 227090.  Fixing unit test found these ones also in the footer, not in archive.
                "sLAmtLckd",
                "sFfUfmip1003Lckd",
                "sGfeCreditLenderPaidItemT",
                
                // OPM 227987 - These fields show up in the 2015 GFE but not on the 2015 GFE archive page.
                "sClosingCostAutomationUpdateT",
                "sIsLoanProgramRegistered",
                "sFeeServiceApplicationHistoryXmlContent",

                // OPM 244236 - This field is only used for auditing and does not need to be archived
                "sSpState",

                // OPM 466386 - This field is used only for disabling a checkbox for the 2015 UI and does not appear
                // on the archive page.
                "sIsRenovationLoan",
                "sAltCostLckd",  // OPM 466388
                "UpdateLinkedServicingPayments",
                // OPM 467063. Only field in this method not in the archive is sConcurSubFin. 
                // According to Camron, sConcurSubFin does not need to be in the archive
                "ValidateAdjustmentsAndProrationsForTrid2",

                "sPurchasePrice1003",
                "sLandIfAcquiredSeparately1003"
            };

            HashSet<string> missingList;

            if (DoesPageUseFieldsNotStoredInArchive(ClosingCostArchive.E_ClosingCostArchiveType.Gfe2015,
                typeof(LendersOfficeApp.newlos.Test.RActiveGFE), 
                ignoreForGFE2015Page,
                out missingList))
            {
                Assert.Fail("2015 GFE Page uses the following fields that are not stored in a 2015 GFE Format Archive: " + Commafy(missingList) + ".");
            }
        }

        /// <summary>
        /// Q: What does this test do?
        /// A: This test exists to make sure that the Closing Disclosure Archive has ALL
        /// fields that are needed by the 2015 GFE page.  If any are missing,
        /// it is a data loss situation.

        /// Q: How do I fix it when this test is failing?
        /// A: The fields that are in the Closing Disclosure but not in a Closing Disclosure Archive probably
        /// need to be added to the Closing Disclosure archive format.  May need to check with
        /// PDE how to handle old archives that are missing this new one.
        /// </summary>
        [Test]
        public void DoesClosingDisclosurePageUseFieldsNotStoredInArchive()
        {
            List<string> ignoreListForClosingDisclosure = new List<string>()
            {
                "ValidateAdjustmentsAndProrationsForTrid2",
                "sIsRequireFeesFromDropDown",
                "sMinMaxInterestRate",
                "sLimitsOnInterestRateFirstChange",
                "sLimitsOnInterestRateSubsequentChanges",
                "sTRIDNegativeClosingDisclosureGeneralLenderCredits",
                "sClosingCostFeeVersionT",
                "sIPerDayLckd",
                "sIPiaDyLckd",
                "sSellerCollection",
                "sLastDisclosedClosingDisclosureArchive",
                "sTRIDLender",
                "sTRIDMortgageBroker",
                "sTRIDSettlementAgent",
                "sTRIDRealEstateBrokerBuyer",
                "sTRIDRealEstateBrokerSeller",
                "sIsRenovationLoan", // These two renovation fields are used for UI only and don't appear in the archive
                "sTotalRenovationCosts",
                "sIsRenovationLoan",
                "sAltCostLckd",
                "UpdateLinkedServicingPayments",
                "sTridEscrowAccountExists", // This is a calculated field that base its value from fields already in the archive.
                "sLienPosT",
                "sIsOFinNew"
            };

            HashSet<string> missingList;

            if (DoesPageUseFieldsNotStoredInArchive(ClosingCostArchive.E_ClosingCostArchiveType.ClosingDisclosure,
                typeof(LendersOfficeApp.newlos.Disclosure.ClosingDisclosure),
                ignoreListForClosingDisclosure,
                out missingList))
            {
                Assert.Fail("Closing Disclosure Page uses the following fields that are not stored in a Closing Disclosure Format Archive: " + Commafy(missingList) + ".");
            }
        }

        
        /// <summary>
        /// Q: What does this test do?
        /// A: This test exists to make sure that any field that an archive considers 
        /// to be "calculated", and therefore unsettable, accesses an archive in its getter.
        /// If it does not and just falls through to the datalayer, it is a data loss bug.

        /// Q: How do I fix it when this test is failing?
        /// A: If the fields truly belong in an archive, go ahead and add the bypass to
        /// their properties.
        /// </summary>
        [Test]
        public void AreAnyFieldsInArchiveMissingDataLayerBypass()
        {
            HashSet<string> missingList;
            if (AreAnyFieldsInArchiveMissingDataLayerBypass( out missingList))
            {
                Assert.Fail("The following fields are considered calculated (unsettable) by archives, but their data layer properties do not access an archive: " + Commafy(missingList) + ".");
            }
        }

        /// <summary>
        /// The dependency sfApplyClosingCostArchive should always contain the entire list of
        /// fields that an archive holds.  It is needed both when extracting an archive from a
        /// loan file and when reading from loanfile that has has an archive applied to it (to
        /// allow live-data fallthrough when needed.)
        /// </summary>
        [Test]
        public void AreAnyArchiveFieldsMissingFromDependency()
        {
            var archiveFields = ClosingCostArchive.GetCalculatedLoanPropertyList(ClosingCostArchive.E_ClosingCostArchiveType.Gfe2015)
                .Union(ClosingCostArchive.GetCalculatedLoanPropertyList(ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate))
                .Union(ClosingCostArchive.GetCalculatedLoanPropertyList(ClosingCostArchive.E_ClosingCostArchiveType.ClosingDisclosure));

            var missingFields = archiveFields.Except(GetDependsOnFields("sfApplyClosingCostArchive"));

            if (missingFields.Count() != 0)
            {
                Assert.Fail("The following fields are used by a Closing Cost archive, but not in the dependency list of sfApplyClosingCostArchive: " + Commafy(missingFields) + ".");
            }
        }

        [Test]
        public void AreAllGfeArchiveReasonsMappedToArchiveSource()
        {
            var reasons = Enum.GetValues(typeof(E_GFEArchivedReasonT))
                .Cast<E_GFEArchivedReasonT>();

            foreach (var reason in reasons)
            {
                var source = CPageBase.GetArchiveSourceFromReason(reason);
            }
        }

        private static bool HasDependsOnFields(string fieldId)
        {
            IEnumerable<PropertyInfo> properties = typeof(CPageBase).GetProperties(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance).Where(p => string.Equals(p.Name, fieldId, StringComparison.OrdinalIgnoreCase));

            if (properties != null && properties.Count() != 0)
            {
                var property = properties.First();
                var dependingAttr = property.GetCustomAttributes(typeof(DependsOnAttribute), false);
                if (dependingAttr != null)
                {
                    return true;
                }
            }
            else
            {
                return false; // no property
            }

            return false;
        }

        /// <summary>
        /// Check if all fields appearing in the specified page also appear in 
        /// the specified archive type's dataset.
        /// </summary>
        private bool DoesPageUseFieldsNotStoredInArchive(ClosingCostArchive.E_ClosingCostArchiveType archiveType, Type pageType, List<string> pageSpecificIgnoreList, out HashSet<string> missingFields)
        {
            // Field calculations coming OUT OF these are ok to not be present in their archive.
            // The fields going INTO the calculation of these DO need to present (we verify in unit test).
            // HINT/TIP: It would be VERY rare to add to this list.
            List<string> dependAllowedFields = new List<string>();
            if (archiveType == ClosingCostArchive.E_ClosingCostArchiveType.ClosingDisclosure
                || archiveType == ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate)
            {
                dependAllowedFields.Add("sAmortTable"); // Per PDE Douglas Telfer.
                dependAllowedFields.Add("sAmortTableExpand");
                dependAllowedFields.Add("sAmortTableWorstCaseExpand");
            }

            // Have store underlying string representation for these.
            // Fields going into the calculation of these must be present.
            List<string> specialFields = new List<string>(new[] { 
                    "sClosingCostSet", // sClosingCostSetJsonContent
                    "sHousingExpenses", // sHousingExpenseJsonContent
                    "sProrationList", // sProrationListJson
                    "sAdjustmentList", // sAdjustmentListJson
                    "sBorrowerAndSellerClosingCostSet", // SellerResponsibleClosingCostJsonContent
                    "sSellerResponsibleClosingCostSet" // SellerResponsibleClosingCostJsonContent,
                });

            // Can skip these
            List<string> ignoreFields = new List<string>(new[] { 
                    "sClosingCostArchive",
                    "sLastDisclosedGFEArchiveD",
                    "sLastDisclosedGFEArchiveId",
                    "sAgentDataSet", // Get this from sAgentXmlData
                    "sSchedPmtAndEscrowPmt",
                    "sAssetsAsAdjustments", // sAssetsAsAdjustmentsJson,
                    "sAdjustmentList" // sAdjustmentList and sAssetsAsAdjustments
            });
            if (pageSpecificIgnoreList != null)
            {
                // Add the page specific ignore list to this ignore list.
                ignoreFields.AddRange(pageSpecificIgnoreList);
            }

            IEnumerable<string> fieldsAppearingOnPage = GetFieldsFromPage(pageType);
            IEnumerable<string> fieldsCurrentlyInArchive = ClosingCostArchive.GetLoanPropertyList(archiveType);

            // Make sure all fields that are on the page are either in the archive
            // or allowed not to be there.
            missingFields = new HashSet<string>();
            foreach (var fieldId in fieldsAppearingOnPage)
            {
                if (!fieldsCurrentlyInArchive.Contains(fieldId))
                {
                    // Potential Missing Field!  This one appears on the page,
                    // but is not recorded in the page's archive format.

                    // Let's look at its dependencies and see if we can allow it to be missing.
                    if (!CanFieldBeCalculated(fieldId, dependAllowedFields.Union(fieldsCurrentlyInArchive)))
                    {
                        missingFields.Add(fieldId);
                    }
                }
            }

            // Also need to knwo if we have everythinkg needed for the specla ones:

            // We should also check that the fields that we consider we always
            // have do actually have all their components present.
            foreach (var special in dependAllowedFields.Union(specialFields))
            {
                foreach (var dependingOnField in GetDependsOnFields(special).Where(p => !p.StartsWith("sf")))
                {
                    if (!fieldsCurrentlyInArchive.Contains(dependingOnField))
                    {
                        // Log here that this field needs this one.
                        missingFields.Add(dependingOnField);
                    }
                }
            }

            missingFields.RemoveWhere(p => specialFields.Contains(p));
            missingFields.RemoveWhere(p => ignoreFields.Contains(p));
            missingFields.RemoveWhere(p => dependAllowedFields.Contains(p));

            return missingFields.Count() != 0;
        }

        private bool CanFieldBeCalculated(string fieldId, IEnumerable<string> knownFields)
        {
            var dependencies = GetDependsOnFields(fieldId);

            if (dependencies == null
                || dependencies.Count() == 0
                || (dependencies.Count() == 1 && dependencies.First() == fieldId))
            {
                return false;
            }

            foreach (var neededField in dependencies)
            {
                if ( knownFields.Contains(neededField))
                {
                    // Ok, as this is something we know for sure.
                    continue;
                }
                else
                {
                    // There is something needed that we do not have.
                    return false;
                }
            }

            // No dependencies missing
            return true;
        }

        /// <summary>
        /// Check if all fields that the archive treats as "calculated" (archive cannot directly set them),
        /// access the archive in their getter.
        /// </summary>
        private bool AreAnyFieldsInArchiveMissingDataLayerBypass(out HashSet<string> missingFields)
        {
            // Gather Fields the archives thinks are calcuated
            var calculatedFieldsAccordingToArchive = ClosingCostArchive.GetCalculatedLoanPropertyList(ClosingCostArchive.E_ClosingCostArchiveType.Gfe2015)
                .Union(ClosingCostArchive.GetCalculatedLoanPropertyList(ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate))
                .Union(ClosingCostArchive.GetCalculatedLoanPropertyList(ClosingCostArchive.E_ClosingCostArchiveType.ClosingDisclosure));

            // ...And their properties
            IEnumerable<PropertyInfo> properties = typeof(CPageBase).GetProperties(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance).Where(p => calculatedFieldsAccordingToArchive.Contains(p.Name));

            missingFields = new HashSet<string>();
            foreach (var prop in properties)
            {
                if (DoesAccessArchive(prop) == false)
                {
                    missingFields.Add(prop.Name);
                }
            }

            return missingFields.Count != 0;
        }

        private static bool DoesAccessArchive(PropertyInfo prop)
        {
            string[] accessMethods = new string[] { "GetValue", "GetBitValue", "GetCountValue", "GetMoneyValue", "GetRateValue", "GetCDateTimeValue", "GetDateTimeValue", "GetGuidValue" };

            var getter = prop.GetGetMethod(true);

            foreach (var methodBase in getter.GetDependsOnList())
            {
                if (methodBase.DeclaringType == typeof(LendersOffice.Common.SerializationTypes.ClosingCostArchive)
                    && accessMethods.Contains(methodBase.Name))
                {
                    // Access to a get method that pulls from an archive
                    // Assuming this means it should return the archive value.

                    return true;
                }
            }

            return false;  // Never found access
        }

        private static string FixFieldName(string fieldId)
        {
            string ret = fieldId;
            if (ret.StartsWith("get_") || ret.StartsWith("set_"))
            {
                ret = ret.Substring(4);
            }
            if (ret.EndsWith("_rep"))
            {
                ret = ret.Substring(0, ret.Length - 4);
            }

            return ret;

        }

        private static IEnumerable<string> GetDependsOnFields(string fieldId)
        {
            IEnumerable<PropertyInfo> properties = typeof(CPageBase).GetProperties(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance).Where(p => string.Equals(p.Name, fieldId, StringComparison.OrdinalIgnoreCase));

            if (properties != null && properties.Count() != 0)
            {
                var property = properties.First();
                var dependingAttr = property.GetCustomAttributes(typeof(DependsOnAttribute), false);
                if (dependingAttr != null && dependingAttr.Count() > 0)
                {
                    List<string> list = new List<string>();

                    foreach (var o in ((DependsOnAttribute)dependingAttr[0]).GetList())
                    {
                        list.Add(o.Name);
                    }
                    //return ((DependsOnAttribute)dependingAttr[0]).GetDependencies();
                    return list;
                }
            }

            return new HashSet<string>();
        }

        private static IEnumerable<string> GetFieldsFromPage(Type pageType)
        {
            HashSet<string> fieldList = new HashSet<string>();
            foreach (var methodBase in pageType.GetDependsOnList())
            {
                var name = FixFieldName(methodBase.Name);
                if ( !name.StartsWith("s") )
                {
                    continue;
                }

                if (IsBelongToCPageBaseOrCAppData(methodBase.DeclaringType))
                {
                    fieldList.Add(name);
                }
            }
            
            return fieldList;
        }

        private static bool IsBelongToCPageBaseOrCAppData(Type t)
        {
            return t.IsSubclassOf(typeof(DataAccess.CPageData)) || t.IsSubclassOf(typeof(DataAccess.CPageBase)) || t == typeof(DataAccess.CPageData) || t == typeof(DataAccess.CAppData) || t == typeof(DataAccess.CPageBase);
        }

        private static string Commafy(IEnumerable<string> list)
        {
            string ret = string.Empty;
            foreach (var strItem in list)
            {
                //ret += ( ret == string.Empty ?  "" : "," ) + strItem;
                ret += strItem + Environment.NewLine;
            }

            return ret;
        }
    }
}
