﻿namespace LendOSolnTest.GFEArchive
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Common.SerializationTypes;
    using LendersOffice.ObjLib.TRID2;
    using LendersOffice.Security;
    using LendOSolnTest.Common;
    using NUnit.Framework;

    [TestFixture]
    public class TemporaryArchiveTest
    {
        private const string PackageName = "TEST";

        private AbstractUserPrincipal principal;
        private Guid loanId;

        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            this.principal = LoginTools.LoginAs(TestAccountType.LoTest001);
            var creator = CLoanFileCreator.GetCreator(principal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
            this.loanId = creator.CreateNewTestFile(); // Use a test loan to get around broker temp option DisableTrid20ForNonTestLoans.
        }

        [TestFixtureTearDown]
        public void FixtureTeardown()
        {
            Tools.SystemDeclareLoanFileInvalid(this.principal, this.loanId, sendNotifications: false, generateLinkLoanMsgEvent: false);
        }

        [Test]
        public void CreateTempArchiveTrid1_LoanEstimatePackage_ContainsToleranceDataOnly()
        {
            var loan = this.GetTestLoan(TridTargetRegulationVersionT.TRID2015);
            var result = loan.CreateTempArchive(packageHasClosingDisclosure: false, tempArchiveSource: null, shouldSetTempArchive: true, principal: null);
            Assert.IsNotNull(result.ToleranceDataClosingCostArchive);
            Assert.IsNull(result.LiveDataClosingCostArchive);
        }

        [Test]
        public void CreateTempArchiveTrid1_ClosingDisclosurePackage_ContainsToleranceDataOnly()
        {
            var loan = this.GetTestLoan(TridTargetRegulationVersionT.TRID2015);
            var result = loan.CreateTempArchive(packageHasClosingDisclosure: true, tempArchiveSource: null, shouldSetTempArchive: true, principal: null);
            Assert.IsNotNull(result.ToleranceDataClosingCostArchive);
            Assert.IsNull(result.LiveDataClosingCostArchive);
        }

        [Test]
        public void CreateTempArchiveTrid1_NoLastDisclosedOrPending_CreatesNewArchive()
        {
            var loan = this.GetTestLoan(TridTargetRegulationVersionT.TRID2015);
            loan.CreateTempArchive(packageHasClosingDisclosure: false, tempArchiveSource: null, shouldSetTempArchive: true, principal: null);
        }

        [Test]
        public void CreateTempArchiveTrid1_LastDisclosedOrPendingUsed()
        {
            // Scenarios:
            // Last disclosed and pending: uses pending
            // Last disclosed, no pending: uses last disclosed
            // No last disclosed, has pending: uses pending

            var loan = this.GetTestLoan(TridTargetRegulationVersionT.TRID2015);

            var disclosedArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed,
                ClosingCostArchive.E_ClosingCostArchiveSource.DocumentGeneration);

            var pendingArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                ClosingCostArchive.E_ClosingCostArchiveStatus.PendingDocumentGeneration,
                ClosingCostArchive.E_ClosingCostArchiveSource.DocumentGeneration);

            loan.sClosingCostArchive.Add(disclosedArchive);
            loan.sClosingCostArchive.Add(pendingArchive);

            loan.CreateTempArchive(packageHasClosingDisclosure: false, tempArchiveSource: null, shouldSetTempArchive: true, principal: null);

            loan.sClosingCostArchive.Clear();
            loan.sClosingCostArchive.Add(disclosedArchive);

            loan.CreateTempArchive(packageHasClosingDisclosure: false, tempArchiveSource: null, shouldSetTempArchive: true, principal: null);

            loan.sClosingCostArchive.Clear();
            loan.sClosingCostArchive.Add(pendingArchive);

            loan.CreateTempArchive(
                packageHasClosingDisclosure: false, 
                tempArchiveSource: LendersOffice.ObjLib.DocumentGeneration.DocumentAuditor.TemporaryArchiveSource.PendingArchive,
                shouldSetTempArchive: true,
                principal: null);
        }

        [Test]
        public void CreateTempArchiveTrid2_LoanEstimatePackage_ContainsToleranceAndLiveData()
        {
            var loan = this.GetTestLoan(TridTargetRegulationVersionT.TRID2017);
            var result = loan.CreateTempArchive(packageHasClosingDisclosure: false, tempArchiveSource: null, shouldSetTempArchive: true, principal: null);
            Assert.IsNotNull(result.ToleranceDataClosingCostArchive);
            Assert.IsNotNull(result.LiveDataClosingCostArchive);
        }

        [Test]
        public void CreateTempArchiveTrid2_ClosingDisclosurePackage_ContainsToleranceDataOnly()
        {
            var loan = this.GetTestLoan(TridTargetRegulationVersionT.TRID2017);
            var result = loan.CreateTempArchive(packageHasClosingDisclosure: true, tempArchiveSource: null, shouldSetTempArchive: true, principal: null);
            Assert.IsNotNull(result.ToleranceDataClosingCostArchive);
            Assert.IsNull(result.LiveDataClosingCostArchive);
        }

        [Test]
        public void CreateTempArchiveTrid2_NoLastDisclosedOrPending_CreatesNewArchive()
        {
            var loan = this.GetTestLoan(TridTargetRegulationVersionT.TRID2017);
            loan.CreateTempArchive(packageHasClosingDisclosure: false, tempArchiveSource: null, shouldSetTempArchive: true, principal: null);
        }

        [Test]
        public void CreateTempArchiveTrid2_LastDisclosedOrPendingUsed()
        {
            // Scenarios:
            // Last disclosed and pending: uses pending
            // Last disclosed, no pending: uses last disclosed
            // No last disclosed, has pending: uses pending

            var loan = this.GetTestLoan(TridTargetRegulationVersionT.TRID2017);

            var disclosedArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed,
                ClosingCostArchive.E_ClosingCostArchiveSource.DocumentGeneration);

            var pendingArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                ClosingCostArchive.E_ClosingCostArchiveStatus.PendingDocumentGeneration,
                ClosingCostArchive.E_ClosingCostArchiveSource.DocumentGeneration);

            loan.sClosingCostArchive.Add(disclosedArchive);
            loan.sClosingCostArchive.Add(pendingArchive);

            loan.CreateTempArchive(packageHasClosingDisclosure: false, tempArchiveSource: null, shouldSetTempArchive: true, principal: null);

            loan.sClosingCostArchive.Clear();
            loan.sClosingCostArchive.Add(disclosedArchive);

            loan.CreateTempArchive(packageHasClosingDisclosure: false, tempArchiveSource: null, shouldSetTempArchive: true, principal: null);

            loan.sClosingCostArchive.Clear();
            loan.sClosingCostArchive.Add(pendingArchive);

            loan.CreateTempArchive(
                packageHasClosingDisclosure: false,
                tempArchiveSource: LendersOffice.ObjLib.DocumentGeneration.DocumentAuditor.TemporaryArchiveSource.PendingArchive,
                shouldSetTempArchive: true,
                principal: null);
        }

        [Test]
        public void SaveTempArchiveTrid1_NoOtherArchivesOnFile_InsertsOneArchive()
        {
            var loan = this.GetTestLoan(TridTargetRegulationVersionT.TRID2015);

            var tempArchive = this.GetTempArchive(loan);
            loan.SetTemporaryArchive(tempArchive, null);

            var toleranceArchive = loan.SaveTemporaryArchive(ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed, PackageName);
            Assert.AreEqual(1, loan.sClosingCostArchive.Count);
            Assert.AreEqual(tempArchive.ToleranceDataClosingCostArchive.Id, toleranceArchive.Id);
        }

        [Test]
        public void SaveTempArchiveTrid1_PendingLoanEstimateArchiveOnFile_ReplacesPendingArchive()
        {
            var loan = this.GetTestLoan(TridTargetRegulationVersionT.TRID2015);

            Assert.IsFalse(loan.sHasLoanEstimateArchiveInPendingStatus);

            var pendingArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                ClosingCostArchive.E_ClosingCostArchiveStatus.PendingDocumentGeneration,
                ClosingCostArchive.E_ClosingCostArchiveSource.DocumentGeneration);

            loan.sClosingCostArchive.Add(pendingArchive);

            Assert.IsTrue(loan.sHasLoanEstimateArchiveInPendingStatus);
            Assert.AreEqual(loan.sLoanEstimateArchiveInPendingStatus.Id, pendingArchive.Id);

            var tempArchive = this.GetTempArchive(loan);
            loan.SetTemporaryArchive(tempArchive, null);

            var toleranceArchive = loan.SaveTemporaryArchive(ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed, PackageName);
            Assert.AreEqual(1, loan.sClosingCostArchive.Count);
            Assert.AreEqual(pendingArchive.Id, toleranceArchive.Id);
            Assert.IsFalse(loan.sHasLoanEstimateArchiveInPendingStatus);
        }

        [Test]
        public void SaveTempArchiveTrid1_UpdatesTempArchiveStatus()
        {
            var loan = this.GetTestLoan(TridTargetRegulationVersionT.TRID2015);

            var tempArchive = this.GetTempArchive(loan);
            loan.SetTemporaryArchive(tempArchive, null);

            loan.SaveTemporaryArchive(ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed, PackageName);
            Assert.IsTrue(loan.sClosingCostArchive.All(archive => archive.Status == ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed));
        }

        [Test]
        public void SaveTempArchiveTrid1_NewArchiveIsNotDisclosed_ResetsInitialLoanEstimate()
        {
            var loan = this.GetTestLoan(TridTargetRegulationVersionT.TRID2015);
            var tempArchive = this.GetTempArchive(loan, ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed);

            var createdDate = DateTime.Now;

            loan.sLoanEstimateDatesInfo.AddDates(this.CreateLoanEstimateDates(
                createdDate,
                loan,
                tempArchive.ToleranceDataClosingCostArchive.Id,
                tempArchive.ToleranceDataClosingCostArchive.DateArchived));

            loan.sLoanEstimateDatesInfo.AddDates(this.CreateLoanEstimateDates(
                createdDate.AddDays(1),
                loan,
                Guid.NewGuid(),
                createdDate.AddDays(1).ToString()));

            loan.sLoanEstimateDatesInfo.AddDates(this.CreateLoanEstimateDates(
                createdDate.AddDays(2),
                loan,
                tempArchive.ToleranceDataClosingCostArchive.Id,
                tempArchive.ToleranceDataClosingCostArchive.DateArchived));

            loan.sLoanEstimateDatesInfo.CalculateInitialLoanEstimate(calculateInitialLoanEstimate: true, closingCostArchives: new[] { tempArchive.ToleranceDataClosingCostArchive });

            loan.SetTemporaryArchive(tempArchive, null);

            loan.SaveTemporaryArchive(ClosingCostArchive.E_ClosingCostArchiveStatus.SupersededCoC, PackageName);
            Assert.IsNull(loan.sLoanEstimateDatesInfo.InitialLoanEstimate);
        }

        [Test]
        public void SaveTempArchiveTrid1_AffectsToleranceCure_UpdatesToleranceArchiveId()
        {
            var loan = this.GetTestLoan(TridTargetRegulationVersionT.TRID2015);
            loan.sToleranceTenPercentCureLckd = true;
            loan.sPdByBorrowerU6F_rep = loan.m_convertLos.ToMoneyString(100M, FormatDirection.ToRep);

            var disclosedArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed,
                ClosingCostArchive.E_ClosingCostArchiveSource.DocumentGeneration);

            loan.sClosingCostArchive.Add(disclosedArchive);

            var tempArchive = this.GetTempArchive(loan);
            loan.SetTemporaryArchive(tempArchive, null);

            loan.SaveTemporaryArchive(ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed, PackageName);
            Assert.AreEqual(tempArchive.ToleranceDataClosingCostArchive.Id, loan.sTolerance10BasisLEArchiveId);
        }

        [Test]
        public void SaveTempArchiveTrid1_ExecutesPostDocGenAction_MarkInvalid()
        {
            var loan = this.GetTestLoan(TridTargetRegulationVersionT.TRID2015);

            Assert.IsFalse(loan.sHasLoanEstimateArchiveInPendingStatus);

            var invalidArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                ClosingCostArchive.E_ClosingCostArchiveStatus.Invalid,
                ClosingCostArchive.E_ClosingCostArchiveSource.DocumentGeneration);

            loan.sClosingCostArchive.Add(invalidArchive);

            var supercedeCoCArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                ClosingCostArchive.E_ClosingCostArchiveStatus.SupersededCoC,
                ClosingCostArchive.E_ClosingCostArchiveSource.DocumentGeneration);

            loan.sClosingCostArchive.Add(supercedeCoCArchive);

            var pendingArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                ClosingCostArchive.E_ClosingCostArchiveStatus.PendingDocumentGeneration,
                ClosingCostArchive.E_ClosingCostArchiveSource.DocumentGeneration);

            loan.sClosingCostArchive.Add(pendingArchive);

            var tempArchive = this.GetTempArchive(loan, action: TemporaryArchive.PostDocumentGenerationAction.MarkPendingLoanEstimateInvalid);
            loan.SetTemporaryArchive(tempArchive, null);

            Assert.IsTrue(loan.sHasLoanEstimateArchiveInPendingStatus);

            loan.SaveTemporaryArchive(ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed, PackageName);

            Assert.AreEqual(ClosingCostArchive.E_ClosingCostArchiveStatus.Invalid, supercedeCoCArchive.Status);
            Assert.IsFalse(loan.sHasLoanEstimateArchiveInPendingStatus);
        }

        [Test]
        public void SaveTempArchiveTrid1_ExecutesPostDocGenAction_IncludeInCd()
        {
            var loan = this.GetTestLoan(TridTargetRegulationVersionT.TRID2015);

            Assert.IsFalse(loan.sHasLoanEstimateArchiveInPendingStatus);

            var invalidArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                ClosingCostArchive.E_ClosingCostArchiveStatus.Invalid,
                ClosingCostArchive.E_ClosingCostArchiveSource.DocumentGeneration);

            loan.sClosingCostArchive.Add(invalidArchive);

            var supercedeCoCArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                ClosingCostArchive.E_ClosingCostArchiveStatus.SupersededCoC,
                ClosingCostArchive.E_ClosingCostArchiveSource.DocumentGeneration);

            loan.sClosingCostArchive.Add(supercedeCoCArchive);

            var pendingArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                ClosingCostArchive.E_ClosingCostArchiveStatus.PendingDocumentGeneration,
                ClosingCostArchive.E_ClosingCostArchiveSource.DocumentGeneration);

            loan.sClosingCostArchive.Add(pendingArchive);

            var tempArchive = this.GetTempArchive(loan, action: TemporaryArchive.PostDocumentGenerationAction.MarkPendingLoanEstimateIncludedInClosingDisclosure);
            loan.SetTemporaryArchive(tempArchive, null);

            Assert.IsTrue(loan.sHasLoanEstimateArchiveInPendingStatus);
            
            loan.SaveTemporaryArchive(ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed, PackageName);

            Assert.IsFalse(loan.sHasLoanEstimateArchiveInPendingStatus);
            Assert.IsTrue(loan.sHasLoanEstimateArchiveInIncludedInClosingDisclosureStatus);
        }

        [Test]
        public void SaveTempArchiveTrid2_NoOtherArchivesOnFile_InsertsTwoArchives()
        {
            var loan = this.GetTestLoan(TridTargetRegulationVersionT.TRID2017);

            var tempArchive = this.GetTempArchive(loan);
            loan.SetTemporaryArchive(tempArchive, null);

            var liveArchive = loan.SaveTemporaryArchive(ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed, PackageName);
            Assert.AreEqual(2, loan.sClosingCostArchive.Count);
            Assert.AreEqual(tempArchive.LiveDataClosingCostArchive.Id, liveArchive.Id);
        }

        [Test]
        public void SaveTempArchiveTrid2_PendingLoanEstimateArchiveOnFileNoLinkedTolerance_ReplacesPendingArchive()
        {
            var loan = this.GetTestLoan(TridTargetRegulationVersionT.TRID2017);

            Assert.IsFalse(loan.sHasLoanEstimateArchiveInPendingStatus);

            var pendingArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                ClosingCostArchive.E_ClosingCostArchiveStatus.PendingDocumentGeneration,
                ClosingCostArchive.E_ClosingCostArchiveSource.DocumentGeneration);

            loan.sClosingCostArchive.Add(pendingArchive);

            Assert.IsTrue(loan.sHasLoanEstimateArchiveInPendingStatus);
            Assert.AreEqual(loan.sLoanEstimateArchiveInPendingStatus.Id, pendingArchive.Id);

            var tempArchive = this.GetTempArchive(loan);
            loan.SetTemporaryArchive(tempArchive, null);
            
            var liveArchive = loan.SaveTemporaryArchive(ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed, PackageName);

            Assert.AreEqual(2, loan.sClosingCostArchive.Count);
            Assert.AreEqual(pendingArchive.Id, liveArchive.Id);
            Assert.IsNotNull(loan.sClosingCostArchive.Find(archive => archive.Id == tempArchive.ToleranceDataClosingCostArchive.Id));
            Assert.IsFalse(loan.sHasLoanEstimateArchiveInPendingStatus);
        }

        [Test]
        public void SaveTempArchiveTrid2_PendingLoanEstimateArchiveOnFileWithLinkedTolerance_ReplacesPendingArchive()
        {
            var loan = this.GetTestLoan(TridTargetRegulationVersionT.TRID2017);

            Assert.IsFalse(loan.sHasLoanEstimateArchiveInPendingStatus);

            var pendingArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                ClosingCostArchive.E_ClosingCostArchiveStatus.PendingDocumentGeneration,
                ClosingCostArchive.E_ClosingCostArchiveSource.DocumentGeneration);

            var linkedArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                ClosingCostArchive.E_ClosingCostArchiveStatus.PendingDocumentGeneration,
                ClosingCostArchive.E_ClosingCostArchiveSource.DocumentGeneration);

            pendingArchive.UsesLiveData = true;
            pendingArchive.LinkedArchiveId = linkedArchive.Id;

            linkedArchive.LinkedArchiveId = pendingArchive.Id;

            loan.sClosingCostArchive.Add(linkedArchive);
            loan.sClosingCostArchive.Add(pendingArchive);

            Assert.IsTrue(loan.sHasLoanEstimateArchiveInPendingStatus);
            Assert.AreEqual(loan.sLoanEstimateArchiveInPendingStatus.Id, pendingArchive.Id);

            var tempArchive = this.GetTempArchive(loan);
            loan.SetTemporaryArchive(tempArchive, null);
            
            var liveArchive = loan.SaveTemporaryArchive(ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed, PackageName);

            Assert.AreEqual(2, loan.sClosingCostArchive.Count);
            Assert.AreEqual(pendingArchive.Id, liveArchive.Id);
            Assert.IsNotNull(loan.sClosingCostArchive.Find(archive => archive.Id == tempArchive.ToleranceDataClosingCostArchive.Id));
            Assert.IsFalse(loan.sHasLoanEstimateArchiveInPendingStatus);
        }

        [Test]
        public void SaveTempArchiveTrid2_UpdatesTempArchiveStatus()
        {
            var loan = this.GetTestLoan(TridTargetRegulationVersionT.TRID2017);

            var tempArchive = this.GetTempArchive(loan);
            loan.SetTemporaryArchive(tempArchive, null);
            
            loan.SaveTemporaryArchive(ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed, PackageName);
            Assert.IsTrue(loan.sClosingCostArchive.All(archive => archive.Status == ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed));
        }

        [Test]
        public void SaveTempArchiveTrid2_NewArchiveIsNotDisclosed_ResetsInitialLoanEstimate()
        {
            var loan = this.GetTestLoan(TridTargetRegulationVersionT.TRID2017);
            var tempArchive = this.GetTempArchive(loan, ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed);

            var createdDate = DateTime.Now;

            loan.sLoanEstimateDatesInfo.AddDates(this.CreateLoanEstimateDates(
                createdDate, 
                loan,
                tempArchive.LiveDataClosingCostArchive.Id,
                tempArchive.LiveDataClosingCostArchive.DateArchived));

            loan.sLoanEstimateDatesInfo.AddDates(this.CreateLoanEstimateDates(
                createdDate.AddDays(1),
                loan,
                Guid.NewGuid(),
                createdDate.AddDays(1).ToString()));

            loan.sLoanEstimateDatesInfo.AddDates(this.CreateLoanEstimateDates(
                createdDate.AddDays(2),
                loan,
                tempArchive.LiveDataClosingCostArchive.Id,
                tempArchive.LiveDataClosingCostArchive.DateArchived));
            
            loan.sLoanEstimateDatesInfo.CalculateInitialLoanEstimate(calculateInitialLoanEstimate: true, closingCostArchives: new[] { tempArchive.LiveDataClosingCostArchive });

            loan.SetTemporaryArchive(tempArchive, null);

            loan.SaveTemporaryArchive(ClosingCostArchive.E_ClosingCostArchiveStatus.SupersededCoC, PackageName);
            Assert.IsNull(loan.sLoanEstimateDatesInfo.InitialLoanEstimate);
        }

        [Test]
        public void SaveTempArchiveTrid2_AffectsToleranceCure_UpdatesToleranceArchiveId()
        {
            var loan = this.GetTestLoan(TridTargetRegulationVersionT.TRID2017);
            loan.sToleranceTenPercentCureLckd = true;
            loan.sPdByBorrowerU6F_rep = loan.m_convertLos.ToMoneyString(100M, FormatDirection.ToRep);

            var disclosedArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed,
                ClosingCostArchive.E_ClosingCostArchiveSource.DocumentGeneration);

            var linkedToleranceArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed,
                ClosingCostArchive.E_ClosingCostArchiveSource.DocumentGeneration);

            disclosedArchive.UsesLiveData = true;
            disclosedArchive.LinkedArchiveId = linkedToleranceArchive.Id;

            linkedToleranceArchive.LinkedArchiveId = disclosedArchive.Id;

            loan.sClosingCostArchive.Add(disclosedArchive);
            loan.sClosingCostArchive.Add(linkedToleranceArchive);

            var tempArchive = this.GetTempArchive(loan);
            loan.SetTemporaryArchive(tempArchive, null);

            loan.SaveTemporaryArchive(ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed, PackageName);
            Assert.AreEqual(tempArchive.ToleranceDataClosingCostArchive.Id, loan.sTolerance10BasisLEArchiveId);
        }

        [Test]
        public void SaveTempArchiveTrid2_ExecutesPostDocGenAction_MarkInvalid()
        {
            var loan = this.GetTestLoan(TridTargetRegulationVersionT.TRID2017);

            Assert.IsFalse(loan.sHasLoanEstimateArchiveInPendingStatus);

            var invalidArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                ClosingCostArchive.E_ClosingCostArchiveStatus.Invalid,
                ClosingCostArchive.E_ClosingCostArchiveSource.DocumentGeneration);

            loan.sClosingCostArchive.Add(invalidArchive);

            var supercedeCoCArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                ClosingCostArchive.E_ClosingCostArchiveStatus.SupersededCoC,
                ClosingCostArchive.E_ClosingCostArchiveSource.DocumentGeneration);

            loan.sClosingCostArchive.Add(supercedeCoCArchive);

            var pendingArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                ClosingCostArchive.E_ClosingCostArchiveStatus.PendingDocumentGeneration,
                ClosingCostArchive.E_ClosingCostArchiveSource.DocumentGeneration);

            loan.sClosingCostArchive.Add(pendingArchive);

            var tempArchive = this.GetTempArchive(loan, action: TemporaryArchive.PostDocumentGenerationAction.MarkPendingLoanEstimateInvalid);
            loan.SetTemporaryArchive(tempArchive, null);

            Assert.IsTrue(loan.sHasLoanEstimateArchiveInPendingStatus);

            loan.SaveTemporaryArchive(ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed, PackageName);

            Assert.AreEqual(ClosingCostArchive.E_ClosingCostArchiveStatus.Invalid, supercedeCoCArchive.Status);
            Assert.IsFalse(loan.sHasLoanEstimateArchiveInPendingStatus);
        }

        [Test]
        public void SaveTempArchiveTrid2_ExecutesPostDocGenAction_IncludeInCd()
        {
            var loan = this.GetTestLoan(TridTargetRegulationVersionT.TRID2017);

            Assert.IsFalse(loan.sHasLoanEstimateArchiveInPendingStatus);

            var invalidArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                ClosingCostArchive.E_ClosingCostArchiveStatus.Invalid,
                ClosingCostArchive.E_ClosingCostArchiveSource.DocumentGeneration);

            loan.sClosingCostArchive.Add(invalidArchive);

            var supercedeCoCArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                ClosingCostArchive.E_ClosingCostArchiveStatus.SupersededCoC,
                ClosingCostArchive.E_ClosingCostArchiveSource.DocumentGeneration);

            loan.sClosingCostArchive.Add(supercedeCoCArchive);

            var pendingArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                ClosingCostArchive.E_ClosingCostArchiveStatus.PendingDocumentGeneration,
                ClosingCostArchive.E_ClosingCostArchiveSource.DocumentGeneration);

            loan.sClosingCostArchive.Add(pendingArchive);

            var tempArchive = this.GetTempArchive(loan, action: TemporaryArchive.PostDocumentGenerationAction.MarkPendingLoanEstimateIncludedInClosingDisclosure);
            loan.SetTemporaryArchive(tempArchive, null);

            Assert.IsTrue(loan.sHasLoanEstimateArchiveInPendingStatus);

            loan.SaveTemporaryArchive(ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed, PackageName);
            
            Assert.IsFalse(loan.sHasLoanEstimateArchiveInPendingStatus);
            Assert.IsTrue(loan.sHasLoanEstimateArchiveInIncludedInClosingDisclosureStatus);
        }

        private CPageData GetTestLoan(TridTargetRegulationVersionT tridTargetVersion)
        {
            var loan = CPageData.CreateUsingSmartDependency(this.loanId, typeof(TemporaryArchiveTest));
            loan.InitLoad();

            loan.sDisclosureRegulationTLckd = true;
            loan.sDisclosureRegulationT = E_sDisclosureRegulationT.TRID;

            loan.sTridTargetRegulationVersionTLckd = true;
            loan.sTridTargetRegulationVersionT = tridTargetVersion;

            loan.sClosingCostArchive.Clear();

            return loan;
        }

        private TemporaryArchive GetTempArchive(
            CPageData loan,
            ClosingCostArchive.E_ClosingCostArchiveStatus status = ClosingCostArchive.E_ClosingCostArchiveStatus.Unknown,
            TemporaryArchive.PostDocumentGenerationAction action = TemporaryArchive.PostDocumentGenerationAction.None)
        {
            var archiveDate = DateTime.Now.ToString("M/dd/yyyy hh:mm:ss tt");

            var toleranceArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                status,
                ClosingCostArchive.E_ClosingCostArchiveSource.Unknown);

            toleranceArchive.DateArchived = archiveDate;

            ClosingCostArchive liveDataArchive = null;
            if (loan.sTridTargetRegulationVersionT == TridTargetRegulationVersionT.TRID2017)
            {
                liveDataArchive = loan.ExtractClosingCostArchive(
                    ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                    status,
                    ClosingCostArchive.E_ClosingCostArchiveSource.Unknown);

                toleranceArchive.LinkedArchiveId = liveDataArchive.Id;

                liveDataArchive.DateArchived = archiveDate;
                liveDataArchive.LinkedArchiveId = toleranceArchive.Id;
                liveDataArchive.UsesLiveData = true;
            }

            return new TemporaryArchive(toleranceArchive, liveDataArchive, action);
        }

        private LoanEstimateDates CreateLoanEstimateDates(
            DateTime createdDate,
            CPageData loan,
            Guid associatedArchiveId,
            string archiveDate)
        {
            loan.BrokerDB.TempOptionXmlContent = loan.BrokerDB.TempOptionXmlContent.Value + "<option name=\"EnableBorrowerLevelDisclosureTracking\" value=\"true\" />";
            var metadata = new LoanEstimateDatesMetadata(loan.GetClosingCostArchiveById, loan.GetConsumerDisclosureMetadataByConsumerId(), loan.sLoanRescindableT);
            var dates = LoanEstimateDates.Create(metadata);

            dates.CreatedDate = createdDate;
            dates.IsManual = false;
            dates.TransactionId = Guid.NewGuid().ToString();
            dates.DeliveryMethod = E_DeliveryMethodT.Email;
            dates.DeliveryMethodLckd = true;

            dates.DisableManualArchiveAssociation = true;
            dates.ArchiveId = associatedArchiveId;
            dates.ArchiveDate = DateTime.Parse(archiveDate);

            return dates;
        }
    }
}
