﻿namespace LendOSolnTest.GFEArchive
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Common.SerializationTypes;
    using LendersOffice.ObjLib.TRID2;
    using LendersOffice.Security;
    using LendOSolnTest.Common;
    using NUnit.Framework;

    [TestFixture]
    public class LiveDataArchiveTest
    {
        private AbstractUserPrincipal principal;
        private Guid loanId;

        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            this.principal = LoginTools.LoginAs(TestAccountType.LoTest001);
            var creator = CLoanFileCreator.GetCreator(principal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
            this.loanId = creator.CreateNewTestFile();  // Use a test loan to get around broker temp option DisableTrid20ForNonTestLoans.
        }

        [TestFixtureTearDown]
        public void FixtureTeardown()
        {
            Tools.SystemDeclareLoanFileInvalid(this.principal, this.loanId, sendNotifications: false, generateLinkLoanMsgEvent: false);
        }

        [Test]
        public void ArchiveClosingCostCoC_Trid1()
        {
            var loan = this.GetTestLoan(TridTargetRegulationVersionT.TRID2015);

            var disclosedArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed,
                ClosingCostArchive.E_ClosingCostArchiveSource.DocumentGeneration);

            loan.sClosingCostArchive.Add(disclosedArchive);

            loan.ArchiveClosingCostCoC(
                new List<Guid>(),
                new List<Tuple<string, bool>>(),
                ComplianceEagle.CEagleChangedCircumstanceCategory.Blank);

            Assert.AreEqual(2, loan.sClosingCostArchive.Count);
        }

        [Test]
        public void PrepareUnknownLoanEstimateArchiveForResubmission_Trid1()
        {
            var loan = this.GetTestLoan(TridTargetRegulationVersionT.TRID2015);

            var unknownArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                ClosingCostArchive.E_ClosingCostArchiveStatus.Unknown,
                ClosingCostArchive.E_ClosingCostArchiveSource.DocumentGeneration);

            unknownArchive.WasSourcedFromPendingCoC = true;

            loan.sClosingCostArchive.Add(unknownArchive);

            Assert.IsTrue(loan.sHasLoanEstimateArchiveInUnknownStatus);

            loan.BrokerDB.TempOptionXmlContent = loan.BrokerDB.TempOptionXmlContent.Value + "<option name=\"EnableBorrowerLevelDisclosureTracking\" value=\"true\" />";
            var metadata = new LoanEstimateDatesMetadata(loan.GetClosingCostArchiveById, loan.GetConsumerDisclosureMetadataByConsumerId(), loan.sLoanRescindableT);
            
            var loanEstimateDates = LoanEstimateDates.Create(metadata);
            loanEstimateDates.DisableManualArchiveAssociation = true;
            loanEstimateDates.ArchiveId = unknownArchive.Id;

            loan.sLoanEstimateDatesInfo.AddDates(loanEstimateDates);

            loan.PrepareUnknownLoanEstimateArchiveForResubmission();

            Assert.AreEqual(ClosingCostArchive.E_ClosingCostArchiveStatus.PendingDocumentGeneration, unknownArchive.Status);
            Assert.AreEqual(2, loan.sClosingCostArchive.Count);

            // The invalid dupe should be after the unknown archive.
            var invalidArchive = loan.sClosingCostArchive[1];
            Assert.AreEqual(ClosingCostArchive.E_ClosingCostArchiveStatus.Invalid, invalidArchive.Status);
            Assert.AreEqual(invalidArchive.Id, loanEstimateDates.ArchiveId);
            Assert.IsFalse(invalidArchive.WasSourcedFromPendingCoC);
        }

        [Test]
        public void ArchiveClosingCostCoC_Trid2()
        {
            var loan = this.GetTestLoan(TridTargetRegulationVersionT.TRID2017);

            var disclosedArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed,
                ClosingCostArchive.E_ClosingCostArchiveSource.DocumentGeneration);

            var linkedToleranceArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed,
                ClosingCostArchive.E_ClosingCostArchiveSource.DocumentGeneration);

            disclosedArchive.UsesLiveData = true;
            disclosedArchive.LinkedArchiveId = linkedToleranceArchive.Id;

            linkedToleranceArchive.LinkedArchiveId = disclosedArchive.Id;

            loan.sClosingCostArchive.Add(disclosedArchive);
            loan.sClosingCostArchive.Add(linkedToleranceArchive);
            
            loan.ArchiveClosingCostCoC(
                new List<Guid>(),
                new List<Tuple<string, bool>>(),
                ComplianceEagle.CEagleChangedCircumstanceCategory.Blank);

            // Archiving closing costs should add a tolerance and live data archive.
            Assert.AreEqual(4, loan.sClosingCostArchive.Count);
        }

        [Test]
        public void PrepareUnknownLoanEstimateArchiveForResubmission_Trid2()
        {
            var loan = this.GetTestLoan(TridTargetRegulationVersionT.TRID2017);

            var unknownArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                ClosingCostArchive.E_ClosingCostArchiveStatus.Unknown,
                ClosingCostArchive.E_ClosingCostArchiveSource.DocumentGeneration);

            var linkedToleranceArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                ClosingCostArchive.E_ClosingCostArchiveStatus.Unknown,
                ClosingCostArchive.E_ClosingCostArchiveSource.DocumentGeneration);

            unknownArchive.UsesLiveData = true;
            unknownArchive.WasSourcedFromPendingCoC = true;
            unknownArchive.LinkedArchiveId = linkedToleranceArchive.Id;

            linkedToleranceArchive.LinkedArchiveId = unknownArchive.Id;

            loan.sClosingCostArchive.Add(linkedToleranceArchive);
            loan.sClosingCostArchive.Add(unknownArchive);

            loan.BrokerDB.TempOptionXmlContent = loan.BrokerDB.TempOptionXmlContent.Value + "<option name=\"EnableBorrowerLevelDisclosureTracking\" value=\"true\" />";
            var metadata = new LoanEstimateDatesMetadata(loan.GetClosingCostArchiveById, loan.GetConsumerDisclosureMetadataByConsumerId(), loan.sLoanRescindableT);

            var loanEstimateDates = LoanEstimateDates.Create(metadata);
            loanEstimateDates.DisableManualArchiveAssociation = true;
            loanEstimateDates.ArchiveId = unknownArchive.Id;

            loan.sLoanEstimateDatesInfo.AddDates(loanEstimateDates);

            Assert.IsTrue(loan.sHasLoanEstimateArchiveInUnknownStatus);

            loan.PrepareUnknownLoanEstimateArchiveForResubmission();

            Assert.AreEqual(ClosingCostArchive.E_ClosingCostArchiveStatus.PendingDocumentGeneration, unknownArchive.Status);
            Assert.AreEqual(ClosingCostArchive.E_ClosingCostArchiveStatus.PendingDocumentGeneration, linkedToleranceArchive.Status);
            Assert.AreEqual(4, loan.sClosingCostArchive.Count);

            // The invalid dupe should be after the unknown archive.
            var invalidToleranceArchive = loan.sClosingCostArchive[2];
            var invalidDisclosedArchive = loan.sClosingCostArchive[3];

            Assert.AreEqual(ClosingCostArchive.E_ClosingCostArchiveStatus.Invalid, invalidToleranceArchive.Status);
            Assert.AreEqual(ClosingCostArchive.E_ClosingCostArchiveStatus.Invalid, invalidDisclosedArchive.Status);

            Assert.AreEqual(invalidDisclosedArchive.Id, loanEstimateDates.ArchiveId);

            Assert.IsFalse(invalidToleranceArchive.WasSourcedFromPendingCoC);
            Assert.IsFalse(invalidDisclosedArchive.WasSourcedFromPendingCoC);

            Assert.AreEqual(invalidToleranceArchive.Id, invalidDisclosedArchive.LinkedArchiveId);
            Assert.AreEqual(invalidDisclosedArchive.Id, invalidToleranceArchive.LinkedArchiveId);

            Assert.IsFalse(invalidToleranceArchive.UsesLiveData);
            Assert.IsTrue(invalidDisclosedArchive.UsesLiveData);
        }

        [Test]
        [TestCase(ClosingCostArchive.E_ClosingCostArchiveStatus.PendingDocumentGeneration)]
        [TestCase(ClosingCostArchive.E_ClosingCostArchiveStatus.Unknown)]
        public void HasMoreThanOneTridArchiveInPendingUnknownStatus_2015_ReturnsCorrectValue(
            ClosingCostArchive.E_ClosingCostArchiveStatus status)
        {
            var loan = this.GetTestLoan(TridTargetRegulationVersionT.TRID2015);
            Func<CPageData, bool> methodToAssert = testLoan => status == ClosingCostArchive.E_ClosingCostArchiveStatus.PendingDocumentGeneration
                ? testLoan.HasMoreThanOneLoanEstimateArchivePendingDocumentGeneration()
                : testLoan.HasMoreThanOneTridArchiveInUnknownStatus();

            var trid2015Archive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                status,
                ClosingCostArchive.E_ClosingCostArchiveSource.Manual);

            loan.sClosingCostArchive.Add(trid2015Archive);

            Assert.IsFalse(methodToAssert(loan));

            loan.sTridTargetRegulationVersionT = TridTargetRegulationVersionT.TRID2017;

            Assert.IsFalse(methodToAssert(loan));

            loan.sTridTargetRegulationVersionT = TridTargetRegulationVersionT.TRID2015;

            trid2015Archive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                status,
                ClosingCostArchive.E_ClosingCostArchiveSource.Manual);

            loan.sClosingCostArchive.Add(trid2015Archive);

            Assert.IsTrue(methodToAssert(loan));

            loan.sTridTargetRegulationVersionT = TridTargetRegulationVersionT.TRID2017;

            Assert.IsTrue(methodToAssert(loan));

            loan.UpdateArchiveStatus(trid2015Archive.Id, ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed);

            Assert.IsFalse(methodToAssert(loan));

            loan.sTridTargetRegulationVersionT = TridTargetRegulationVersionT.TRID2017;

            Assert.IsFalse(methodToAssert(loan));
        }

        [Test]
        [TestCase(ClosingCostArchive.E_ClosingCostArchiveStatus.PendingDocumentGeneration)]
        [TestCase(ClosingCostArchive.E_ClosingCostArchiveStatus.Unknown)]
        public void HasMoreThanOneTridArchiveInPendingUnknownStatus_MixedArchives_ReturnsCorrectValue(
            ClosingCostArchive.E_ClosingCostArchiveStatus status)
        {
            var loan = this.GetTestLoan(TridTargetRegulationVersionT.TRID2015);
            Func<CPageData, bool> methodToAssert = testLoan => status == ClosingCostArchive.E_ClosingCostArchiveStatus.PendingDocumentGeneration
               ? testLoan.HasMoreThanOneLoanEstimateArchivePendingDocumentGeneration()
               : testLoan.HasMoreThanOneTridArchiveInUnknownStatus();

            var trid2015Archive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                status,
                ClosingCostArchive.E_ClosingCostArchiveSource.Manual);

            loan.sClosingCostArchive.Add(trid2015Archive);

            Assert.IsFalse(methodToAssert(loan));

            loan.sTridTargetRegulationVersionT = TridTargetRegulationVersionT.TRID2017;

            Assert.IsFalse(methodToAssert(loan));

            var toleranceArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                status,
                ClosingCostArchive.E_ClosingCostArchiveSource.Manual);

            var liveDataArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                status,
                ClosingCostArchive.E_ClosingCostArchiveSource.Manual);

            liveDataArchive.UsesLiveData = true;
            liveDataArchive.LinkedArchiveId = toleranceArchive.Id;

            toleranceArchive.LinkedArchiveId = liveDataArchive.Id;

            loan.sClosingCostArchive.Add(toleranceArchive);
            loan.sClosingCostArchive.Add(liveDataArchive);

            Assert.IsTrue(methodToAssert(loan));

            loan.sTridTargetRegulationVersionT = TridTargetRegulationVersionT.TRID2017;

            Assert.IsTrue(methodToAssert(loan));

            loan.UpdateArchiveStatus(trid2015Archive.Id, ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed);

            Assert.IsFalse(methodToAssert(loan));

            loan.sTridTargetRegulationVersionT = TridTargetRegulationVersionT.TRID2017;

            Assert.IsFalse(methodToAssert(loan));
        }

        [Test]
        [TestCase(ClosingCostArchive.E_ClosingCostArchiveStatus.PendingDocumentGeneration)]
        [TestCase(ClosingCostArchive.E_ClosingCostArchiveStatus.Unknown)]
        public void HasMoreThanOneTridArchiveInPendingUnknownStatus_2017_ReturnsCorrectValue(
            ClosingCostArchive.E_ClosingCostArchiveStatus status)
        {
            var loan = this.GetTestLoan(TridTargetRegulationVersionT.TRID2017);
            Func<CPageData, bool> methodToAssert = testLoan => status == ClosingCostArchive.E_ClosingCostArchiveStatus.PendingDocumentGeneration
               ? testLoan.HasMoreThanOneLoanEstimateArchivePendingDocumentGeneration()
               : testLoan.HasMoreThanOneTridArchiveInUnknownStatus();

            var toleranceArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                status,
                ClosingCostArchive.E_ClosingCostArchiveSource.Manual);

            var liveDataArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                status,
                ClosingCostArchive.E_ClosingCostArchiveSource.Manual);

            liveDataArchive.UsesLiveData = true;
            liveDataArchive.LinkedArchiveId = toleranceArchive.Id;

            toleranceArchive.LinkedArchiveId = liveDataArchive.Id;

            loan.sClosingCostArchive.Add(toleranceArchive);
            loan.sClosingCostArchive.Add(liveDataArchive);

            Assert.IsFalse(methodToAssert(loan));

            toleranceArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                status,
                ClosingCostArchive.E_ClosingCostArchiveSource.Manual);

            liveDataArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                status,
                ClosingCostArchive.E_ClosingCostArchiveSource.Manual);

            liveDataArchive.UsesLiveData = true;
            liveDataArchive.LinkedArchiveId = toleranceArchive.Id;

            toleranceArchive.LinkedArchiveId = liveDataArchive.Id;

            loan.sClosingCostArchive.Add(toleranceArchive);
            loan.sClosingCostArchive.Add(liveDataArchive);

            Assert.IsTrue(methodToAssert(loan));

            loan.UpdateArchiveStatus(liveDataArchive.Id, ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed);

            Assert.IsFalse(methodToAssert(loan));

            loan.sTridTargetRegulationVersionT = TridTargetRegulationVersionT.TRID2017;

            Assert.IsFalse(methodToAssert(loan));
        }

        [Test]
        public void UpdateArchiveStatusTrid2_UpdatesLinkedArchive()
        {
            var loan = this.GetTestLoan(TridTargetRegulationVersionT.TRID2017);

            var toleranceArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                ClosingCostArchive.E_ClosingCostArchiveStatus.Unknown,
                ClosingCostArchive.E_ClosingCostArchiveSource.DocumentGeneration);

            var liveDataArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                ClosingCostArchive.E_ClosingCostArchiveStatus.Unknown,
                ClosingCostArchive.E_ClosingCostArchiveSource.DocumentGeneration);

            liveDataArchive.UsesLiveData = true;
            liveDataArchive.LinkedArchiveId = toleranceArchive.Id;

            toleranceArchive.LinkedArchiveId = liveDataArchive.Id;

            loan.sClosingCostArchive.Add(toleranceArchive);
            loan.sClosingCostArchive.Add(liveDataArchive);

            loan.UpdateArchiveStatus(toleranceArchive.Id, ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed);

            Assert.AreEqual(ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed, liveDataArchive.Status);
        }

        [Test]
        public void SetArchiveAsPreviousBasisByDate_Trid1_SetsBasisArchive()
        {
            var loan = this.GetTestLoan(TridTargetRegulationVersionT.TRID2015);
            var archiveDate = DateTime.Now.Date.ToString();

            var archive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                ClosingCostArchive.E_ClosingCostArchiveStatus.PendingDocumentGeneration,
                ClosingCostArchive.E_ClosingCostArchiveSource.DocumentGeneration);

            archive.DateArchived = archiveDate;
            archive.WasBasisArchive = false;

            loan.sClosingCostArchive.Add(archive);

            loan.SetArchiveAsPreviousBasisByDate(archiveDate);

            Assert.IsTrue(archive.WasBasisArchive);
        }

        [Test]
        public void SetArchiveAsPreviousBasisByDate_Trid2_SetsBasisArchive()
        {
            var loan = this.GetTestLoan(TridTargetRegulationVersionT.TRID2017);
            var archiveDate = DateTime.Now.Date.ToString();

            var toleranceArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                ClosingCostArchive.E_ClosingCostArchiveStatus.Unknown,
                ClosingCostArchive.E_ClosingCostArchiveSource.DocumentGeneration);

            var liveDataArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                ClosingCostArchive.E_ClosingCostArchiveStatus.Unknown,
                ClosingCostArchive.E_ClosingCostArchiveSource.DocumentGeneration);

            liveDataArchive.UsesLiveData = true;
            liveDataArchive.WasBasisArchive = false;
            liveDataArchive.LinkedArchiveId = toleranceArchive.Id;
            liveDataArchive.DateArchived = archiveDate;

            toleranceArchive.LinkedArchiveId = liveDataArchive.Id;
            toleranceArchive.WasBasisArchive = false;
            toleranceArchive.DateArchived = archiveDate;

            loan.sClosingCostArchive.Add(toleranceArchive);
            loan.sClosingCostArchive.Add(liveDataArchive);

            loan.SetArchiveAsPreviousBasisByDate(archiveDate);

            Assert.IsFalse(liveDataArchive.WasBasisArchive);
            Assert.IsTrue(toleranceArchive.WasBasisArchive);
        }

        [Test]
        public void SetArchiveAsPreviousBasisById_Trid1_SetsBasisArchive()
        {
            var loan = this.GetTestLoan(TridTargetRegulationVersionT.TRID2015);

            var archive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                ClosingCostArchive.E_ClosingCostArchiveStatus.PendingDocumentGeneration,
                ClosingCostArchive.E_ClosingCostArchiveSource.DocumentGeneration);
            
            archive.WasBasisArchive = false;

            loan.sClosingCostArchive.Add(archive);

            loan.SetArchiveAsPreviousBasisById(archive.Id);

            Assert.IsTrue(archive.WasBasisArchive);
        }

        [Test]
        public void SetArchiveAsPreviousBasisById_Trid2DisclosedArchiveId_SetsToleranceBasisArchive()
        {
            var loan = this.GetTestLoan(TridTargetRegulationVersionT.TRID2017);

            var toleranceArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                ClosingCostArchive.E_ClosingCostArchiveStatus.Unknown,
                ClosingCostArchive.E_ClosingCostArchiveSource.DocumentGeneration);

            var liveDataArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                ClosingCostArchive.E_ClosingCostArchiveStatus.Unknown,
                ClosingCostArchive.E_ClosingCostArchiveSource.DocumentGeneration);

            liveDataArchive.UsesLiveData = true;
            liveDataArchive.WasBasisArchive = false;
            liveDataArchive.LinkedArchiveId = toleranceArchive.Id;

            toleranceArchive.LinkedArchiveId = liveDataArchive.Id;
            toleranceArchive.WasBasisArchive = false;

            loan.sClosingCostArchive.Add(toleranceArchive);
            loan.sClosingCostArchive.Add(liveDataArchive);

            loan.SetArchiveAsPreviousBasisById(liveDataArchive.Id);

            Assert.IsFalse(liveDataArchive.WasBasisArchive);
            Assert.IsTrue(toleranceArchive.WasBasisArchive);
        }

        [Test]
        public void SetArchiveAsPreviousBasisById_Trid2ToleranceArchiveId_SetsToleranceBasisArchive()
        {
            var loan = this.GetTestLoan(TridTargetRegulationVersionT.TRID2017);

            var toleranceArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                ClosingCostArchive.E_ClosingCostArchiveStatus.Unknown,
                ClosingCostArchive.E_ClosingCostArchiveSource.DocumentGeneration);

            var liveDataArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                ClosingCostArchive.E_ClosingCostArchiveStatus.Unknown,
                ClosingCostArchive.E_ClosingCostArchiveSource.DocumentGeneration);

            liveDataArchive.UsesLiveData = true;
            liveDataArchive.WasBasisArchive = false;
            liveDataArchive.LinkedArchiveId = toleranceArchive.Id;

            toleranceArchive.LinkedArchiveId = liveDataArchive.Id;
            toleranceArchive.WasBasisArchive = false;

            loan.sClosingCostArchive.Add(toleranceArchive);
            loan.sClosingCostArchive.Add(liveDataArchive);

            loan.SetArchiveAsPreviousBasisById(toleranceArchive.Id);

            Assert.IsFalse(liveDataArchive.WasBasisArchive);
            Assert.IsTrue(toleranceArchive.WasBasisArchive);
        }

        [Test]
        public void Trid2Archives_DistinctLinkedArchiveIds_DoesNotThrowOnFlush()
        {
            var loan = this.GetTestLoan(TridTargetRegulationVersionT.TRID2017);

            var toleranceArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed,
                ClosingCostArchive.E_ClosingCostArchiveSource.DocumentGeneration);

            var liveDataArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed,
                ClosingCostArchive.E_ClosingCostArchiveSource.DocumentGeneration);

            toleranceArchive.LinkedArchiveId = liveDataArchive.Id;

            liveDataArchive.LinkedArchiveId = toleranceArchive.Id;
            liveDataArchive.UsesLiveData = true;

            loan.sClosingCostArchive.Add(toleranceArchive);
            loan.sClosingCostArchive.Add(liveDataArchive);

            loan.FlushClosingCostArchives();
        }

        [Test, ExpectedException(typeof(LendersOffice.Common.GenericUserErrorMessageException))]
        public void Trid2Archives_MultipleArchivesWithSameLinkedId_ThrowsOnFlush()
        {
            var loan = this.GetTestLoan(TridTargetRegulationVersionT.TRID2017);

            var toleranceArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed,
                ClosingCostArchive.E_ClosingCostArchiveSource.DocumentGeneration);

            var liveDataArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed,
                ClosingCostArchive.E_ClosingCostArchiveSource.DocumentGeneration);

            var duplicateLiveDataArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed,
                ClosingCostArchive.E_ClosingCostArchiveSource.DocumentGeneration);

            toleranceArchive.LinkedArchiveId = liveDataArchive.Id;

            liveDataArchive.LinkedArchiveId = toleranceArchive.Id;
            liveDataArchive.UsesLiveData = true;

            duplicateLiveDataArchive.LinkedArchiveId = toleranceArchive.Id;
            duplicateLiveDataArchive.UsesLiveData = true;

            loan.sClosingCostArchive.Add(toleranceArchive);
            loan.sClosingCostArchive.Add(liveDataArchive);
            loan.sClosingCostArchive.Add(duplicateLiveDataArchive);
            
            loan.FlushClosingCostArchives();
            Assert.Fail("Expected exception due to duplicate linked IDs.");
        }

        [Test]
        public void Trid2Archives_DuplicatedOnLoanDuplication()
        {
            var creator = CLoanFileCreator.GetCreator(this.principal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
            var originalLoanId = creator.CreateNewTestFile();

            var loan = CPageData.CreateUsingSmartDependency(originalLoanId, typeof(LiveDataArchiveTest));
            loan.InitSave(LendersOffice.Constants.ConstAppDavid.SkipVersionCheck);

            loan.sDisclosureRegulationTLckd = true;
            loan.sDisclosureRegulationT = E_sDisclosureRegulationT.TRID;

            loan.sTridTargetRegulationVersionTLckd = true;
            loan.sTridTargetRegulationVersionT = TridTargetRegulationVersionT.TRID2017;

            var toleranceArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                ClosingCostArchive.E_ClosingCostArchiveStatus.Unknown,
                ClosingCostArchive.E_ClosingCostArchiveSource.DocumentGeneration);

            var liveDataArchive = loan.ExtractClosingCostArchive(
                ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                ClosingCostArchive.E_ClosingCostArchiveStatus.Unknown,
                ClosingCostArchive.E_ClosingCostArchiveSource.DocumentGeneration);

            liveDataArchive.UsesLiveData = true;
            liveDataArchive.WasBasisArchive = false;
            liveDataArchive.LinkedArchiveId = toleranceArchive.Id;

            toleranceArchive.LinkedArchiveId = liveDataArchive.Id;
            toleranceArchive.WasBasisArchive = false;

            loan.sClosingCostArchive.Add(toleranceArchive);
            loan.sClosingCostArchive.Add(liveDataArchive);

            loan.FlushClosingCostArchives();
            loan.Save();
            
            var duplicateLoanId = creator.DuplicateLoanFile(originalLoanId);
            loan = CPageData.CreateUsingSmartDependency(duplicateLoanId, typeof(LiveDataArchiveTest));
            loan.InitLoad();

            Assert.AreEqual(2, loan.sClosingCostArchive.Count);

            var newToleranceArchive = loan.sClosingCostArchive[0];
            var newDisclosedArchive = loan.sClosingCostArchive[1];

            Assert.IsTrue(newToleranceArchive.LinkedArchiveId.HasValue);
            Assert.IsTrue(newDisclosedArchive.LinkedArchiveId.HasValue);

            Assert.IsFalse(newToleranceArchive.UsesLiveData);
            Assert.IsTrue(newDisclosedArchive.UsesLiveData);

            Assert.AreEqual(newToleranceArchive.Id, newDisclosedArchive.LinkedArchiveId.Value);
            Assert.AreEqual(newDisclosedArchive.Id, newToleranceArchive.LinkedArchiveId.Value);

            Tools.SystemDeclareLoanFileInvalid(this.principal, originalLoanId, sendNotifications: false, generateLinkLoanMsgEvent: false);
            Tools.SystemDeclareLoanFileInvalid(this.principal, duplicateLoanId, sendNotifications: false, generateLinkLoanMsgEvent: false);
        }

        private CPageData GetTestLoan(TridTargetRegulationVersionT tridTargetVersion)
        {
            var loan = CPageData.CreateUsingSmartDependency(this.loanId, typeof(LiveDataArchiveTest));
            loan.InitLoad();

            loan.sDisclosureRegulationTLckd = true;
            loan.sDisclosureRegulationT = E_sDisclosureRegulationT.TRID;

            loan.sTridTargetRegulationVersionTLckd = true;
            loan.sTridTargetRegulationVersionT = tridTargetVersion;

            loan.sClosingCostArchive.Clear();

            return loan;
        }
    }
}
