﻿namespace LendOSolnTest.GFEArchive
{
    using System;
    using System.Linq;
    using System.Reflection;
    using DataAccess;
    using NUnit.Framework;

    [TestFixture]
    public class GfeAttributeFieldNameListTest
    {
        [Test]
        public void OptimizedList_HasAllReflectedFields()
        {
            var reflectedList = this.GetListFromReflection().StringCollection;
            var optimizedList = Tools.GetFieldNamesWithGfeAttribute().StringCollection;

            var missingFields = reflectedList.Except(optimizedList);
            Assert.AreEqual(
                0, 
                missingFields.Count(), 
                "The static GFE property list is missing the following field(s): " + string.Join(",", missingFields));
        }

        [Test]
        public void OptimizedList_HasNoExtraFields()
        {
            var reflectedList = this.GetListFromReflection().StringCollection;
            var optimizedList = Tools.GetFieldNamesWithGfeAttribute().StringCollection;

            var extraFields = optimizedList.Except(reflectedList);
            Assert.AreEqual(
                0,
                extraFields.Count(),
                "The static GFE property list has the following extra fields: " + string.Join(",", extraFields));
        }

        private StringList GetListFromReflection()
        {
            var list = new StringList();

            foreach (PropertyInfo propInfo in typeof(CPageBase).GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance))
            {
                foreach (Attribute attr in propInfo.GetCustomAttributes(typeof(GfeAttribute), false))
                {
                    if (attr is GfeAttribute) // redundant but to make compiler happy.
                    {
                        list.Add(propInfo.Name);
                    }
                }
            }

            return list;
        }
    }
}
