<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:param name="Param0"/>
  <xsl:param name="Param1"/>
  <xsl:output method="xml" version="1.0" encoding="UTF-16" indent="yes" />
 
  <xsl:template match="/">
    <xsl:apply-templates select="item"/>
  </xsl:template>
  <xsl:template match="item">
  Version_2::Param0:<xsl:value-of select="$Param0"/>;Param1:<xsl:value-of select="$Param1"/>;Attr_0:<xsl:value-of select="@attr0"/>
  </xsl:template>  
</xsl:stylesheet>

  