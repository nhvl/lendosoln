﻿// <copyright file="BasicHousingExpensesTest.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   01/20/2015
// </summary>
namespace LendOSolnTest.HousingExpenseTest
{
    using System;
    using DataAccess;
    using LendersOffice.Constants;
    using LendOSolnTest.Fakes;
    using Common;
    using NUnit.Framework;

    /// <summary>
    /// Class to test the basic functions of the new HousingExpenses.
    /// </summary>
    [TestFixture]
    public class BasicHousingExpensesTest
    {
        [TestFixtureSetUp]
        public void Setup()
        {
            LoginTools.LoginAs(TestAccountType.LoTest001);
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
            FakePageDataUtilities.Instance.Reset();
        }

        /// <summary>
        /// Tests creating, serializing, and deserializing an empty HousingExpenses object.
        /// </summary>
        [Test]
        public void TestEmptyExpenses()
        {
            HousingExpenses emptyExpense = new HousingExpenses("", null);
            Assert.AreEqual(emptyExpense.AssignedExpenses.Count, 0);
            Assert.AreEqual(emptyExpense.UnassignedExpenses.Count, 0);

            string emptyJson = emptyExpense.ToJson();

            HousingExpenses recreatedExpense = new HousingExpenses(emptyJson, null);
            Assert.AreEqual(recreatedExpense.AssignedExpenses.Count, 0);
            Assert.AreEqual(recreatedExpense.UnassignedExpenses.Count, 0);
        }

        /// <summary>
        /// Tests the serialization of the new HousingExpenses structure.
        /// </summary>
        [Test]
        public void TestNonCustomSerialization()
        {
            string json = @"{
                                ""aHExp"": [
                                    {
                                        ""Key"": 0,
                                        ""Value"": {
                                            ""__type"": ""HazardExpense:#DataAccess"",
                                            ""hAnnAmtCalcBaseT"": 2,
                                            ""hAnnAmtCalcP"": 10.2,
                                            ""hAnnAmtCalcT"": 0,
                                            ""hCusLineNum"": 0,
                                            ""hDisbList"": [],
                                            ""hDisbRepI"": 0,
                                            ""hEscClosing"": 1,
                                            ""hExpT"": 0,
                                            ""hMnthAmtF"": 102000,
                                            ""hPaidTo"": 0,
                                            ""hPolicyActD"": ""/Date(1349161200000-0700)/"",
                                            ""hPrePMnth"": 12,
                                            ""hResMnthLckd"": true,
                                            ""hResMon"": 12,
                                            ""hTaxT"": 0
                                        }
                                    }
                                ],
                                ""uHExp"": []
                            }";

            HousingExpenses expenses = new HousingExpenses(json, null);

            Assert.AreEqual(expenses.AssignedExpenses.Count, 1);
            Assert.AreEqual(expenses.UnassignedExpenses.Count, 0);

            Assert.AreEqual(expenses.DoesExpenseExist(E_HousingExpenseTypeT.HazardInsurance), true);
            BaseHousingExpense hazardExp = expenses.GetExpense(E_HousingExpenseTypeT.HazardInsurance);

            Assert.IsInstanceOf<HazardExpense>(hazardExp);
            Assert.AreEqual(hazardExp.HousingExpenseType, E_HousingExpenseTypeT.HazardInsurance);
            
            Assert.AreEqual(hazardExp.AnnualAmtCalcBasePerc, 10.2);
            Assert.AreEqual(hazardExp.MonthlyAmtFixedAmt, 102000);

            hazardExp.AnnualAmtCalcBasePerc = 12.3M;
            hazardExp.MonthlyAmtFixedAmt = 1234.56M;

            Assert.AreEqual(hazardExp.AnnualAmtCalcBasePerc, 12.3M);
            Assert.AreEqual(hazardExp.MonthlyAmtFixedAmt, 1234.56M);
        }

        /// <summary>
        /// Tests the serialization of custom expenses.
        /// </summary>
        [Test]
        public void TestSerializationCustom()
        { 
            string json = @"{
                                ""aHExp"": [
                                    {
                                        ""Key"": 2,
                                        ""Value"": {
                                            ""__type"": ""CustomExpense:#DataAccess"",
                                            ""hAnnAmtCalcBaseT"": 2,
                                            ""hAnnAmtCalcP"": 10.2,
                                            ""hAnnAmtCalcT"": 0,
                                            ""hCusLineNum"": 1,
                                            ""hDisbList"": [],
                                            ""hDisbRepI"": 0,
                                            ""hEscClosing"": 1,
                                            ""hExpT"": 2,
                                            ""hMnthAmtF"": 10200,
                                            ""hPaidTo"": 0,
                                            ""hPolicyActD"": ""/Date(1429254000000-0700)/"",
                                            ""hPrePMnth"": 0,
                                            ""hResMnthLckd"": true,
                                            ""hResMon"": 5,
                                            ""hTaxT"": 0
                                        }
                                    }
                                ],
                                ""uHExp"": [
                                    {
                                        ""Key"": 1,
                                        ""Value"": {
                                            ""__type"": ""CustomExpense:#DataAccess"",
                                            ""hAnnAmtCalcBaseT"": 0,
                                            ""hAnnAmtCalcP"": 0,
                                            ""hAnnAmtCalcT"": 0,
                                            ""hCusLineNum"": 1,
                                            ""hDisbList"": [],
                                            ""hDisbRepI"": 0,
                                            ""hEscClosing"": 1,
                                            ""hExpT"": 13,
                                            ""hMnthAmtF"": 0,
                                            ""hPaidTo"": 0,
                                            ""hPolicyActD"": ""/Date(-62135568000000-0800)/"",
                                            ""hPrePMnth"": 0,
                                            ""hResMnthLckd"": false,
                                            ""hResMon"": 13,
                                            ""hTaxT"": 0
                                        }
                                    }
                                ]
                            }";

            HousingExpenses expenses = new HousingExpenses(json, null);

            Assert.AreEqual(expenses.AssignedExpenses.Count, 1);
            Assert.AreEqual(expenses.UnassignedExpenses.Count, 1);

            // Make sure the individual expenses serializes properly.
            BaseHousingExpense windExp = expenses.GetExpense(E_HousingExpenseTypeT.WindstormInsurance);
            Assert.AreEqual(windExp.HousingExpenseType, E_HousingExpenseTypeT.WindstormInsurance);
            Assert.AreEqual(windExp.CustomExpenseLineNum, E_CustomExpenseLineNumberT.Line1008);
            Assert.AreEqual(windExp.MonthlyAmtFixedAmt, 10200);

            BaseHousingExpense unassignedExp = expenses.GetUnassignedExp(E_CustomExpenseLineNumberT.Line1008);
            Assert.AreEqual(unassignedExp.HousingExpenseType, E_HousingExpenseTypeT.Unassigned);
            Assert.AreEqual(unassignedExp.CustomExpenseLineNum, E_CustomExpenseLineNumberT.Line1008);
            Assert.AreEqual(unassignedExp.MonthlyAmtFixedAmt, 0);

            // Make sure the correct expense is used when getting by line number.
            BaseHousingExpense l1008Exp = expenses.GetExpenseByLineNum(E_CustomExpenseLineNumberT.Line1008);
            Assert.AreEqual(windExp.MonthlyAmtTotal, l1008Exp.MonthlyAmtTotal);
            Assert.AreEqual(windExp.IsEscrowedAtClosing, l1008Exp.IsEscrowedAtClosing);
        }

        /// <summary>
        /// Tests a non custom expense and its interactions with an associated data loan.
        /// </summary>
        [Test]
        public void TestNonCustomExpenses()
        {
            CPageData dataLoan = FakePageData.Create();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            // Ensure that the new structure is being used.
            dataLoan.sIsHousingExpenseMigrated = true;
            HousingExpenses expenses = dataLoan.sHousingExpenses;

            // Test if saving to loan file field affects the new structure.
            dataLoan.sProHazInsR = 10.0M;
            dataLoan.sProHazInsMb = 1000;

            BaseHousingExpense hazExp = expenses.GetExpense(E_HousingExpenseTypeT.HazardInsurance);
            Assert.IsNotNull(hazExp);
            Assert.AreEqual(hazExp.AnnualAmtCalcBasePerc, 10.0M);
            Assert.AreEqual(hazExp.MonthlyAmtFixedAmt, 1000);

            // Test if saving to new structure affects old file fields.
            hazExp.IsPrepaid = true;
            hazExp.PrepaidMonths = 10;
            hazExp.IsEscrowedAtClosing = E_TriState.No;
            Assert.AreEqual(dataLoan.sHazInsPiaMon, 10);
            Assert.AreEqual(dataLoan.sHazInsRsrvEscrowedTri, E_TriState.No);

            // Test if calculated fields are being properly calculated.
            dataLoan.sProHazInsT = E_PercentBaseT.SalesPrice;
            dataLoan.sPurchPrice = 100000;

            Assert.AreEqual(hazExp.AnnualAmtCalcBaseAmt, 100000);
            Assert.AreEqual(hazExp.AnnualAmtCalcBaseAmt, dataLoan.sProHazInsBaseAmt);

            // Test saving, serializing, deserializing.
            dataLoan.Save();

            dataLoan = FakePageData.Create();
            dataLoan.InitLoad();

            HousingExpenses remadeExpenses = dataLoan.sHousingExpenses;
            BaseHousingExpense rhazExp = remadeExpenses.GetExpense(E_HousingExpenseTypeT.HazardInsurance);
            Assert.IsNotNull(rhazExp);
            Assert.AreEqual(rhazExp.AnnualAmtCalcBasePerc, 10.0M);
            Assert.AreEqual(dataLoan.sProHazInsMb, 1000);
        }

        /// <summary>
        /// Test custom expenses. Tests for choosing the right expense and linking/unlinking.
        /// </summary>
        [Test]
        public void TestCustomExpenses()
        {
            LoginTools.LoginAs(TestAccountType.LoTest001);

            CPageData dataLoan = FakePageData.Create();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            // Ensure that the new structure is being used.
            dataLoan.sIsHousingExpenseMigrated = true;
            HousingExpenses expenses = dataLoan.sHousingExpenses;

            BaseHousingExpense windExp = dataLoan.sWindstormExpense;
            windExp.SetCustomLineNumber(E_CustomExpenseLineNumberT.Line1008);

            // Test if changing loan file fields changes expense.
            dataLoan.s1006ProHExp = 100M;
            dataLoan.s1006RsrvEscrowedTri = E_TriState.No;
            Assert.AreEqual(windExp.MonthlyAmtTotal, 100M);
            Assert.AreEqual(windExp.IsEscrowedAtClosing, E_TriState.No);

            // Test if changing expense changes loan file fields.
            windExp.ReserveMonthsLckd = true;
            windExp.ReserveMonths = 99;
            Assert.AreEqual(dataLoan.s1006RsrvMonLckd, true);
            Assert.AreEqual(dataLoan.s1006RsrvMon, 99);

            // Test unlinking expense from line number. Should not equal its previous values.
            windExp.SetCustomLineNumber(E_CustomExpenseLineNumberT.None);
            Assert.AreNotEqual(dataLoan.s1006ProHExp, 100M);
            Assert.AreNotEqual(dataLoan.s1006RsrvMon, 99);

            // Test linking back to line number. Assigned expense should replace unassigned expense.
            dataLoan.s1006ProHExp = 123.45M;
            dataLoan.s1006RsrvEscrowedTri = E_TriState.Yes;
            windExp.SetCustomLineNumber(E_CustomExpenseLineNumberT.Line1008);
            Assert.AreNotEqual(windExp.MonthlyAmtTotal, 123.45M);
            Assert.AreNotEqual(windExp.IsEscrowedAtClosing, E_TriState.Yes);
        }

        /// <summary>
        /// Tests basic functions of actual disbursements. 
        /// </summary>
        [Test]
        public void TestActualDisbursements()
        {
            LoginTools.LoginAs(TestAccountType.LoTest001);

            CPageData dataLoan = FakePageData.Create();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sSchedDueD1Lckd = true;
            dataLoan.sSchedDueD1_rep = "01/1/2015";

            // Ensure that the new structure is being used.
            dataLoan.sIsHousingExpenseMigrated = true;

            BaseHousingExpense hazExp = dataLoan.sHazardExpense;
            hazExp.AnnualAmtCalcType = E_AnnualAmtCalcTypeT.Disbursements;

            // No Actual Disbursements are in the expense just yet.
            Assert.AreEqual(hazExp.ActualDisbursementsList.Count, 0);

            SingleDisbursement newDisb = new SingleDisbursement();
            newDisb.DisbursementType = E_DisbursementTypeT.Actual;
            newDisb.DisbursementAmtLckd = true;
            newDisb.DisbursementAmt = 10000M;

            // Successful adding
            Assert.IsTrue(hazExp.AddDisbursement(newDisb));

            // Get the right disbursement based on due date.
            Assert.AreEqual(hazExp.ActualDisbursementsInOneYear.Count, 0);
            newDisb.DueDate_rep = "01/1/2015";
            Assert.AreEqual(hazExp.ActualDisbursementsInOneYear.Count, 1);

            // Annual amount based off of disbursement total.
            Assert.AreEqual(hazExp.AnnualAmt, 10000M);

            // Test removing disbursement.
            hazExp.RemoveDisbursement(newDisb.DisbId);
            Assert.AreEqual(hazExp.ActualDisbursementsList.Count, 0);
            Assert.AreEqual(hazExp.AnnualAmt, 0);
        }

        [Test]
        public void IsTax_HandlesAllEnumValues_NoException()
        {
            var allTypes = Enum.GetValues(typeof(E_HousingExpenseTypeT));
            foreach (E_HousingExpenseTypeT type in allTypes)
            {
                var expense = new CustomExpense(type);
                bool test = expense.IsTax;
            }
        }

        [Test]
        public void IsInsurance_HandlesAllEnumValues_NoException()
        {
            var allTypes = Enum.GetValues(typeof(E_HousingExpenseTypeT));
            foreach (E_HousingExpenseTypeT type in allTypes)
            {
                var expense = new CustomExpense(type);
                bool test = expense.IsTax;
            }
        }
    }
}
