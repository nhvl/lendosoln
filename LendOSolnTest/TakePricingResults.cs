using System;
using System.Xml;
using NUnit.Framework;
using LendersOffice.Security;
using LendOSolnTest.Common;

namespace LendOSolnTest
{
    [TestFixture]
    public class PriceTest
    {
        [Test]
        public void QbcExecutionTest()
        {
            // 04/15/09 mf - OPM 25872. QBC Test cases on localhost.
            // These ones have different app/borrower combinations,
            // and should result in loan programs that are attached
            // to policies with all three legal QBC rule consequences
            // (MaxDTI, Disqual, and Stip)

            Guid[] testLoans = new Guid[] {
                    new Guid("1a2358b3-3c00-4872-83c8-164da30c91f6")
                    , new Guid("f2719824-532a-4b66-9657-64c82c306b32")
                    , new Guid("dff1c6d4-97d2-4a2b-a550-cfb02229cf8f")
                    , new Guid("438bd2f0-279b-4b04-a15a-f19ccef38d63") };

            LoginTools.LoginAs(TestAccountType.Branch1_LoanOfficer);
            XmlDocument doc;
            
            try
            {
                doc = LendersOfficeApp.los.LoanFindService.TakeResultSnapshot(testLoans);
            }
            catch (LendersOffice.Common.LoanNotFoundException)
            {
                Console.Error.WriteLine("QBC Test loans do not exist.");
                return;
            }

            XmlNodeList nodeList = doc.SelectNodes("/Snapshot/Loan/Results");
            foreach (XmlNode results in nodeList)
            {
                if (results.ChildNodes.Count > 1)
                    continue;

                if (results.ChildNodes.Count <= 0)
                    throw new Exception("Take snapshot fail.");

                XmlNodeType nodeType = results.ChildNodes[0].NodeType;
                if (nodeType == XmlNodeType.Text)
                    throw new Exception(results.ChildNodes[0].InnerText);
            }

            // 06/02/09 mf. - Getting unusual results from comparison tool
            // For now, just run pricing with resulting QBC rules,
            // and forgo the comparison.

            /*
            // This test should eventually price with both options once
            // it is decided how we implement the QBC-option.
            string testFileLocation =
                //@"/QbcPricingResult.xml";
                @"/NonQbcPricingResult.xml";

            XmlDocument doc2 = new XmlDocument();
            doc2.Load(TestDataRootFolder.Location + testFileLocation);

            string report;
            bool result = LpeSnapshotCompareTools.Compare(doc, doc2, out report);

            if (result == false)
            {
                Console.Error.WriteLine(report);
                throw new ApplicationException("Results do not match.");
            }
             */
        }

    }



//#define ThienUse



#if DEBUG && ThienUse
    [TestFixture]
	public class TakePricingResults
	{
        bool   m_isOld = false;


        string GetFileName( string seed )
        {
            return m_isOld ? GetOldFileName( seed ) : GetNewFileName( seed );
        }

        string GetOldFileName( string seed )
        {
            return @"c:\Snapshot\" + seed + "-old.xml";
        }

        string GetNewFileName( string seed )
        {
            return @"c:\Snapshot\" + seed + "-new.xml";
        }

        [Test]
        public void Pml0001()
        {
            RunPricing("Pml0001", "AUTOMATED_TEST_DO_NOT_TOUCH" );
        }

        [Test]
        public void Pml0006()
        {
            RunPricing( "Pml0006" );
        }

        [Test]
        public void Pml0007()
        {
            RunPricing( "Pml0007" );
        }

        [Test]
        public void Pml0008()
        {
            RunPricing( "Pml0008" );
        }

        [Test]
        public void Pml0009()
        {
            RunPricing( "Pml0009" );
        }

        [Test]
        public void Pml0015()
        {
            RunPricing( "Pml0015" );
        }

        [Test]
        public void Pml0016()
        {
            RunPricing( "Pml0016" );
        }

        [Test]
        public void Pml0017()
        {
            RunPricing("Pml0017");
        }

        [Test]
        public void Pml0018()
        {
            RunPricing("Pml0018");
        }

        [Test]
        public void Pml0026()
        {
            RunPricing("Pml0026");
        }

        [Test]
        public void Pml0041()
        {
            RunPricing("Pml0041");
        }

        [Test]
        public void Pml0047()
        {
            RunPricing("Pml0047", new Guid("906b1549-cd66-4913-a71a-cfa8050ff138"), new Guid("73f38ea8-53b9-4597-8b57-458bcaa8ab9e"), new Guid("ce6e253f-60e4-4934-a4f5-b874c046d3d4") );
        }

        void RunPricing( string brokerLoginName, params Guid[] loanIds )
        {
            string saveResultFileName = GetFileName( brokerLoginName );

            LoginTools.SetBecomePlus( brokerLoginName );

            XmlDocument doc = LendersOfficeApp.los.LoanFindService.TakeResultSnapshot( loanIds );
            Assert.IsNotNull( doc );

            XmlNodeList nodeList = doc.SelectNodes("/Snapshot/Loan/Results");
            foreach( XmlNode results in nodeList )
            {
                if( results.ChildNodes.Count > 1 )
                    continue;

                if( results.ChildNodes.Count <= 0 )
                    throw new Exception("Take snapshot fail.");

                XmlNodeType nodeType = results.ChildNodes[0].NodeType;
                if( nodeType == XmlNodeType.Text )
                    throw new Exception(results.ChildNodes[0].InnerText);
            }
            doc.Save( saveResultFileName );
            doc = null;

            CompareTwoVersions( brokerLoginName );


        }

        //Guid loanId = LoanTools.CurBrokerGetLoanIdByName( "AUTOTEST" );

        void RunPricing( string brokerLoginName )
        {
            RunPricing( brokerLoginName, "AUTOMATE_TEST_DO_NOT_TOUCH" );
        }

        void RunPricing( string brokerLoginName, string loanName )
        {
            LoginTools.SetBecomePlus( brokerLoginName );
            Guid loanId = LoanTools.CurBrokerGetLoanIdByName( loanName );
            RunPricing( brokerLoginName, loanId );
        }

        void CompareTwoVersions( string brokerLoginName )
        {
            if( m_isOld == true )
                return;

            string newVersion = GetNewFileName( brokerLoginName );
            string oldVersion = GetOldFileName( brokerLoginName );

            string report;
            bool result = LendersOfficeApp.TestSuite.LpeSnapshotCompareTools.Compare(newVersion, oldVersion, out report);
            if( result == false )
                throw new Exception( report );

        }


	}
#endif
}

