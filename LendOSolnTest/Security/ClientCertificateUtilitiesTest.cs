﻿// <copyright file="ClientCertificateUtilitiesTest.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: David Dao
//    Date:   5/4/2014 3:23:59 PM 
// </summary>
namespace LendOSolnTest.Security
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using NUnit.Framework;
    using LendersOffice.Security;
    using LendersOffice.Constants;
    using LendOSolnTest.Common;

    /// <summary>
    /// Unit Test for Client Certificate.
    /// </summary>
    [TestFixture]
    [Category(CategoryConstants.NewBuildServerIgnore)]
    public class ClientCertificateUtilitiesTest
    {
        [Test]
        public void TestSerialNumberGeneration()
        {
            for (int i = 0; i < 100; i++)
            {
                Guid guid = Guid.NewGuid();

                string str = ClientCertificateUtilities.GenerateHexadecimalString(ClientCertificateUtilities.GenerateX509SerialNumber(guid));

                Guid guid2 = ClientCertificateUtilities.ConvertFromX509SerialString(str);

                Assert.AreEqual(guid, guid2, "Guid: [" + guid + "] failed round trip test. Hex=[" + str + "]. Guid2=[" + guid2 + "]");
            }
        }

        [Test]
        [Category(CategoryConstants.ExpectFailureOnLocalhost)]
        public void TestGenerateClientCertificate()
        {
            if (string.IsNullOrEmpty(ConstStage.IntermediateCertificateAuhorityThumbPrint))
            {
                // 5/11/2014 dd - Skip this test if const stage is not setup intermediate certificate thumb print.
                return;
            }
            byte[] bytes = ClientCertificateUtilities.GenerateClientCertificate(Guid.NewGuid(), "Password123", "JOHN DOE", ClientCertificateType.User);

            Assert.Greater(bytes.Length, 0);
        }

        [Test]
        public void TestGenerateClientCertificateForUser()
        {
            if (string.IsNullOrEmpty(ConstStage.IntermediateCertificateAuhorityThumbPrint))
            {
                // 5/11/2014 dd - Skip this test if const stage is not setup intermediate certificate thumb print.
                return;
            }
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.LoTest001);

            Guid certificateId = Guid.NewGuid();

            ClientCertificate.Create(principal, certificateId, principal.UserId, "Testing");

            bool found = false;
            foreach (var item in ClientCertificate.ListByUser(principal.BrokerId, principal.UserId))
            {
                if (item.CertificateId == certificateId)
                {
                    found = true;
                    break;
                }
            }
            Assert.AreEqual(true, found, "Could not find the new created certificate");

            // Cleanup
            foreach (var item in ClientCertificate.ListByUser(principal.BrokerId, principal.UserId))
            {
                ClientCertificate.Delete(principal.BrokerId, principal.UserId, item.CertificateId);
            }
        }
    }
}