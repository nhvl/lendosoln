﻿namespace LendOSolnTest.Security
{
    using Common;
    using DataAccess;
    using LendersOffice.Security;
    using NUnit.Framework;
    using System.IO;

    [TestFixture]
    public class AuthServiceHelperTest
    {
        [Test]
        public void RoundTripBasicTicket()
        {
            var principal = LoginTools.LoginAs(TestAccountType.LoTest001);
            var ticket = AuthServiceHelper.CreateAuthTicket("USER_TICKET", principal);
            var principal2 = AuthServiceHelper.GetPrincipalFromTicket(ticket, null);
            Assert.That(principal.BrokerId == principal2.BrokerId, "Principals broker ids does not match.");
            Assert.That(principal.UserId == principal2.UserId, "Principals broker ids does not match.");
            Assert.That(principal.Type == principal2.Type, "Types does not match between tickets.");
        }
    }
}
