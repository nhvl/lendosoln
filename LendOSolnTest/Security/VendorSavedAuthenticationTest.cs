﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using LendersOffice.Security;
using LendOSolnTest.Common;

namespace LendOSolnTest.Security
{
    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public class VendorSavedAuthenticationTest
    {
        [Test]
        public void DocMagicSavedAuthentication_SaveAndClearValue_ValueIsUpdated()
        {
            LoginTools.LoginAs(TestAccountType.LoanOfficer);

            DocMagicSavedAuthentication auth = DocMagicSavedAuthentication.Current;

            auth.CustomerId = "CustomerId";
            auth.UserName = "UserName";
            auth.Password = "Password";

            auth.Save();


            auth = DocMagicSavedAuthentication.Current;
            Assert.AreEqual("CustomerId", auth.CustomerId);
            Assert.AreEqual("UserName", auth.UserName);
            Assert.AreEqual("Password", auth.Password);

            auth.UserName = "";
            auth.Password = "";
            auth.CustomerId = "";
            auth.Save();

            auth = DocMagicSavedAuthentication.Current;
            Assert.AreEqual("", auth.CustomerId);
            Assert.AreEqual("", auth.UserName);
            Assert.AreEqual("", auth.Password);
        }

        [Test]
        public void DocuTechSavedAuthentication_SaveAndClearValue_ValueIsUpdated(
            [Values("UserName", "Use this!@ Name")]string userName,
            [Values("Password", "ya0%R:<R@_aw")]string password)
        {
            var principal = LoginTools.LoginAs(TestAccountType.LoanOfficer);
            {
                var credential = DocuTechSavedAuthentication.Retrieve(principal);
                credential.UserName = userName;
                credential.Password = password;
                credential.Save();
            }

            {
                var credential = DocuTechSavedAuthentication.Retrieve(principal);
                Assert.AreEqual(userName, credential.UserName, "UserName should be saved");
                Assert.AreEqual(password, credential.Password, "Password should be saved");
                credential.UserName = string.Empty;
                credential.Password = string.Empty;
                credential.Save();
            }

            {
                var credential = DocuTechSavedAuthentication.Retrieve(principal);
                Assert.AreEqual(string.Empty, credential.UserName, "UserName should be saved");
                Assert.AreEqual(string.Empty, credential.Password, "Password should be saved");
            }
        }

        [Test]
        public void ComplianceEaseSavedAuthentication_SaveAndClearValue_ValueIsUpdated(
            [Values("UserName", "Use this!@ Name")]string userName,
            [Values("Password", "ya0%R:<R@_aw")]string password)
        {
            var principal = LoginTools.LoginAs(TestAccountType.LoanOfficer);
            {
                var credential = ComplianceEaseSavedAuthentication.Retrieve(principal);
                credential.UserName = userName;
                credential.Password = password;
                credential.Save();
            }

            {
                var credential = ComplianceEaseSavedAuthentication.Retrieve(principal);
                Assert.AreEqual(userName, credential.UserName, "UserName should be saved");
                Assert.AreEqual(password, credential.Password, "Password should be saved");
                credential.UserName = string.Empty;
                credential.Password = string.Empty;
                credential.Save();
            }

            {
                var credential = ComplianceEaseSavedAuthentication.Retrieve(principal);
                Assert.AreEqual(string.Empty, credential.UserName, "UserName should be saved");
                Assert.AreEqual(string.Empty, credential.Password, "Password should be saved");
            }
        }

        [Test]
        public void ComplianceEagleSavedAuthentication_SaveAndClearValue_ValueIsUpdated(
            [Values("UserName", "Use this!@ Name")]string userName,
            [Values("Password", "ya0%R:<R@_aw")]string password)
        {
            var principal = LoginTools.LoginAs(TestAccountType.LoanOfficer);
            {
                var credential = ComplianceEagleUserCredentials.Retrieve(principal);
                credential.UserName = userName;
                credential.Password = password;
                credential.Save();
            }

            {
                var credential = ComplianceEagleUserCredentials.Retrieve(principal);
                Assert.AreEqual(userName, credential.UserName, "UserName should be saved");
                Assert.AreEqual(password, credential.Password, "Password should be saved");
                credential.UserName = string.Empty;
                credential.Password = string.Empty;
                credential.Save();
            }

            {
                var credential = ComplianceEagleUserCredentials.Retrieve(principal);
                Assert.AreEqual(string.Empty, credential.UserName, "UserName should be saved");
                Assert.AreEqual(string.Empty, credential.Password, "Password should be saved");
            }
        }

        [Test]
        public void DriveSavedAuthentication_SaveAndClearValue_ValueIsUpdated(
            [Values("UserName", "Use this!@ Name")]string userName,
            [Values("Password", "ya0%R:<R@_aw")]string password)
        {
            var principal = LoginTools.LoginAs(TestAccountType.LoanOfficer);
            {
                var credential = DriveSavedAuthentication.Retrieve(principal);
                credential.UserName = userName;
                credential.Password = password;
                credential.Save();
            }

            {
                var credential = DriveSavedAuthentication.Retrieve(principal);
                Assert.AreEqual(userName, credential.UserName, "UserName should be saved");
                Assert.AreEqual(password, credential.Password, "Password should be saved");
                credential.UserName = string.Empty;
                credential.Password = string.Empty;
                credential.Save();
            }

            {
                var credential = DriveSavedAuthentication.Retrieve(principal);
                Assert.AreEqual(string.Empty, credential.UserName, "UserName should be saved");
                Assert.AreEqual(string.Empty, credential.Password, "Password should be saved");
            }
        }

        [Test]
        public void FloodSavedAuthentication_SaveAndClearValue_ValueIsUpdated(
            [Values("UserName", "Use this!@ Name")]string userName,
            [Values("Password", "ya0%R:<R@_aw")]string password)
        {
            var principal = LoginTools.LoginAs(TestAccountType.LoanOfficer);
            {
                var credential = new FloodSavedAuthentication(principal.BrokerId, principal.UserId);
                credential.UserName = userName;
                credential.Password = password;
                credential.Save();
            }

            {
                var credential = new FloodSavedAuthentication(principal.BrokerId, principal.UserId);
                Assert.AreEqual(userName, credential.UserName, "UserName should be saved");
                Assert.AreEqual(password, credential.Password, "Password should be saved");
                credential.UserName = string.Empty;
                credential.Password = string.Empty;
                credential.Save();
            }

            {
                var credential = new FloodSavedAuthentication(principal.BrokerId, principal.UserId);
                Assert.AreEqual(string.Empty, credential.UserName, "UserName should be saved");
                Assert.AreEqual(string.Empty, credential.Password, "Password should be saved");
            }
        }
    }
}
