﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using LendersOffice.Security;
using LendOSolnTest.Common;
using LendersOffice.Common;

namespace LendOSolnTest.Security
{
    [TestFixture]
    public class BrokerGlobalIpWhiteListTest
    {
        [Test]
        public void CRUDTest()
        {
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.LoTest001);

            // 4/19/2014 dd - This test will perform basics operation of BrokerGlobalIpWhiteList class: List, Create, Edit, Retrieve, Delete

            IEnumerable<BrokerGlobalIpWhiteList> initialList = BrokerGlobalIpWhiteList.ListByBrokerId(principal.BrokerId);

            Assert.AreEqual(0, initialList.Count(), "There should be no whitelist in test suite");

            int id = -1;
            #region Create New
            BrokerGlobalIpWhiteList item = new BrokerGlobalIpWhiteList();
            item.IpAddress = "11.12.13.*";
            item.Description = "Test";
            item.Save(principal);
            
            id = item.Id;
            initialList = BrokerGlobalIpWhiteList.ListByBrokerId(principal.BrokerId);

            Assert.AreEqual(1, initialList.Count(), "Should be 1 entry after create new item");


            #endregion

            #region Edit using object create from new.
            item.IpAddress = "11.12.13.14";
            item.Description = "Test_Edit";
            item.Save(principal);

            initialList = BrokerGlobalIpWhiteList.ListByBrokerId(principal.BrokerId);

            Assert.AreEqual(1, initialList.Count());
            #endregion

            #region Retrieve Existing Item
            BrokerGlobalIpWhiteList item1 = BrokerGlobalIpWhiteList.RetrieveById(principal.BrokerId, id);

            Assert.AreEqual(item.IpAddress, item1.IpAddress);
            Assert.AreEqual(item.Description, item1.Description);
            Assert.AreEqual(principal.BrokerId, item1.BrokerId);
            Assert.AreEqual(principal.LoginNm, item1.LastModifiedLoginName);

            #endregion

            #region Edit Existing Item

            item1.Description = "Test_Edit_2";
            item1.Save(principal);

            initialList = BrokerGlobalIpWhiteList.ListByBrokerId(principal.BrokerId);

            Assert.AreEqual(1, initialList.Count());

            BrokerGlobalIpWhiteList item2 = initialList.ElementAt(0);
            Assert.AreEqual(item1.IpAddress, item2.IpAddress);
            Assert.AreEqual(item1.Description, item2.Description);
            #endregion

            #region Delete
            BrokerGlobalIpWhiteList.Delete(principal.BrokerId, id);
            initialList = BrokerGlobalIpWhiteList.ListByBrokerId(principal.BrokerId);

            Assert.AreEqual(0, initialList.Count());
            #endregion

            try
            {
                BrokerGlobalIpWhiteList.RetrieveById(principal.BrokerId, id);
                Assert.Fail("Should throw NotFoundException");
            }
            catch (NotFoundException)
            {
            }

        }
    }
}
