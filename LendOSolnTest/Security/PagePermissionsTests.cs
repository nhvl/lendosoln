﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using LendersOffice.Common;
using LendOSolnTest.Common;
using System.Threading;
using LendersOfficeApp.los.admin;

namespace LendOSolnTest.Security
{
    [TestFixture]
    public class PagePermissionsTests
    {
        private System.Security.Principal.IPrincipal m_oldPrincipal;
        [TestFixtureSetUp]
        public void Setup()
        {
            m_oldPrincipal = Thread.CurrentPrincipal;
            var brokerUser = LoginTools.LoginAs(TestAccountType.LoanOfficer);
        }

        [TestFixtureTearDown]
        public void TearDown()
        {
            Thread.CurrentPrincipal = m_oldPrincipal;
        }

        [Test]
        public void AllLosAdminAspxPagesRequireAPermissionOrRole()
        {
            bool hasError = false;
            string errorMessage = string.Empty;
            var brokerInfoPage = new BrokerInfo();
            var lendersOfficeAppAssembly = brokerInfoPage.GetType().Assembly;
            var losAdminTypes = lendersOfficeAppAssembly.GetTypes().Where(t => t != null && t.IsClass && t.Namespace != null && t.Namespace.StartsWith("LendersOfficeApp.los.Admin", StringComparison.OrdinalIgnoreCase)).ToArray();
            var losAdminPageTypes = losAdminTypes.Where(t => t.IsSubclassOf(typeof(System.Web.UI.Page))).ToArray();
            //var bindingFlags =  BindingFlags.Static | BindingFlags.Public | BindingFlags.FlattenHierarchy;

            foreach(var losAdminPage in losAdminPageTypes)
            {
                if(losAdminPage.IsSubclassOf(typeof(BasePage)))
                {
                    try
                    {
                        var pageInstance = (BasePage)Activator.CreateInstance(losAdminPage);

                        if (pageInstance.RequiredPermissionsForLoadingPage.Count() == 0
                        && pageInstance.RequiredRolesForLoadingPage.Count() == 0)
                        {
                            hasError = true;
                            errorMessage = $"{errorMessage} {losAdminPage} had no required permissions or roles l1,";
                        }
                    }
                    catch (Exception)
                    {
                        hasError = true;
                        errorMessage = $"{errorMessage} {losAdminPage} failed to initialize l2,";
                    }

                    // if using static and new (which didn't work to actually do the blocking):
                    //var requiredPermissions = (IEnumerable<Permission>)losAdminPage.GetProperty("RequiredPermissionsForLoadingPage", bindingFlags).GetValue(null, null);
                    //var requiredRoles = (IEnumerable<E_RoleT>)losAdminPage.GetProperty("RequiredRolesForLoadingPage", bindingFlags).GetValue(null, null);
                    //if(requiredPermissions.Count() == 0 && requiredRoles.Count() == 0)
                    //{
                    //    hasError = true;
                    //    Tools.LogError(losAdminPage + " had no required permissions or roles.");
                    //}
                }
                else if (losAdminPage.IsSubclassOf(typeof(BaseSimpleServiceXmlPage)))
                {
                    try
                    {
                        var pageInstance = (BaseSimpleServiceXmlPage)Activator.CreateInstance(losAdminPage);

                        if (pageInstance.RequiredPermissionsForLoadingPage.Count() == 0
                        && pageInstance.RequiredRolesForLoadingPage.Count() == 0)
                        {
                            hasError = true;
                            errorMessage = $"{errorMessage} {losAdminPage} had no required permissions or roles l3,";

                        }
                    }
                    catch (Exception)
                    {
                        hasError = true;
                        errorMessage = $"{errorMessage} {losAdminPage} failed to initialize l4,";

                    }

                    
                    // if using static and new (which didn't work to actually do the blocking):
                    //var requiredPermissions = (IEnumerable<Permission>)losAdminPage.GetProperty("RequiredPermissionsForLoadingPage", bindingFlags).GetValue(null, null);
                    //var requiredRoles = (IEnumerable<E_RoleT>)losAdminPage.GetProperty("RequiredRolesForLoadingPage", bindingFlags).GetValue(null, null);
                    //if (requiredPermissions.Count() == 0 && requiredRoles.Count() == 0)
                    //{
                    //    hasError = true;
                    //    Tools.LogError(losAdminPage + " had no required permissions or roles.");
                    //}
                }
                else
                {
                    hasError = true;
                    errorMessage = $"{errorMessage} {losAdminPage} is not a subclass of BasePage or BaseSimpleServiceXmlPage l5,";

                }
            }

            if(hasError)
            {
                Assert.Fail(errorMessage);
            }
        }
    }
}
