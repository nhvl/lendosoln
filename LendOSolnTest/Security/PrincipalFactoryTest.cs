/// Author: David Dao

using System;
using NUnit.Framework;
using LendersOffice.Security;
using LendersOffice.Constants;
using LendOSolnTest.Common;
using LqbGrammar.Drivers.SecurityEventLogging;

namespace LendOSolnTest.Security
{
    [TestFixture]
	public class PrincipalFactoryTest
	{
        [Test]
        public void LoginNormally() 
        {
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.LoTest001);

            Assert.IsNotNull(principal);
            Assert.AreEqual("LOTEST 001", principal.DisplayName, "Someone modify test account name.");
        }

        [Test]
        public void InvalidLogin() 
        {
            PrincipalFactory.E_LoginProblem loginProblem = PrincipalFactory.E_LoginProblem.None;
            AbstractUserPrincipal principal = PrincipalFactory.CreateWithFailureType(
                "B!#*(#", 
                "BAD_PASSWORD", 
                out loginProblem, 
                false /* allowDuplicateLogin */, 
                true /* isStoreToCookie */,
                LoginSource.Website);

            Assert.IsNull(principal);
            Assert.AreEqual(PrincipalFactory.E_LoginProblem.InvalidLoginPassword, loginProblem);

        }
	}
}
