﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using LendersOffice.Security;
using LendersOffice.Common;
using DataAccess;

namespace LendOSolnTest.Security
{
    [TestFixture]
    public class RoleTest
    {
        [Test]
        public void TestListAll()
        {
            foreach (var role in Role.AllRoles)
            {
                Assert.IsNotNull(role);
                Assert.IsNotNull(role.Desc);
                Assert.IsNotNull(role.ModifiableDesc);
            }
            
        }
        [Test]
        public void TestGetRoleByType()
        {
            // 8/27/2013 dd - Test to make sure Role.Get work for all enum type.

            E_RoleT[] roleTypeList = (E_RoleT[])Enum.GetValues(typeof(E_RoleT));

            foreach (E_RoleT roleType in roleTypeList)
            {
                if (roleType == E_RoleT.Pml_Administrator || roleType == E_RoleT.Pml_LoanOfficer)
                {
                    // 8/27/2013 dd - These two roles are special. They are not in ROLE database table.
                    continue;
                }
                Role role = Role.Get(roleType);
                Assert.IsNotNull(role);
            }
        }

        [Test]
        public void TestGetRoleByDesc()
        {
            string[] validDescriptionList = {
                                                "Underwriter",
                                                "DocDrawer",
                                                "Administrator",
                                                "LoanOpener",
                                                "CollateralAgent",
                                                "PostCloser",
                                                "LockDesk",
                                                "Shipper",
                                                "Insuring",
                                                "Telemarketer",
                                                "Closer",
                                                "Processor",
                                                "Accountant",
                                                "Manager",
                                                "Agent",
                                                "RealEstateAgent",
                                                "BrokerProcessor",
                                                "Consumer",
                                                "LenderAccountExec",
                                                "Funder",
                                                "CreditAuditor",
                                                "DisclosureDesk",
                                                "JuniorProcessor",
                                                "JuniorUnderwriter",
                                                "LegalAuditor",
                                                "LoanOfficerAssistant",
                                                "Purchaser",
                                                "QCCompliance",
                                                "Secondary",
                                                "Servicing",
                                                "ExternalSecondary",
                                                "ExternalPostCloser"
                                            };

            foreach (var desc in validDescriptionList)
            {
                var role = Role.Get(desc);
                Assert.IsNotNull(role);
            }

            string[] invalidDescriptionList = {
                                                  string.Empty,
                                                  null,
                                                  "UNDERWRITER", // Get is not case sensitive.
                                                  "Blahxxx"
                                              };
            foreach (var desc in invalidDescriptionList)
            {
                try
                {
                    var role = Role.Get(desc);
                    Assert.Fail("Should fail for role=[" + desc + "]");
                }
                catch (NotFoundException) { }
            }
        }

        [Test]
        public void TestGetRoleDescription()
        {
            foreach (E_RoleT roleType in Enum.GetValues(typeof(E_RoleT))) 
            {
                string desc = Role.GetRoleDescription(roleType);
                Assert.IsTrue(string.IsNullOrEmpty(desc) == false, roleType + " cannot not empty description");
            }
        }

        [Test]
        public void TestGetAgentRoleMapping()
        {
            foreach (E_RoleT roleType in Enum.GetValues(typeof(E_RoleT)))
            {
                E_AgentRoleT agentRoleT = Role.GetAgentRoleT(roleType);

            }
        }
        [Test]
        public void EnsureRoleIdAreConsistent()
        {
            // 8/27/2013 dd - Make sure role id and short desc are matches across Dev/Test/Production

            // When adding new role to the system. Just update this list.
            var list = new[] {
                           new { RoleT=E_RoleT.Underwriter, RoleId = new Guid("E0534F45-2AE1-4A8C-952E-00520F445819"), Desc="Underwriter"},
                           new { RoleT=E_RoleT.DocDrawer, RoleId = new Guid("74097291-502A-4F50-9018-11A6A302B12E"), Desc="DocDrawer"},
                           new { RoleT=E_RoleT.Administrator, RoleId = new Guid("86C86CF3-FEB6-400F-80DC-123363A79605"), Desc="Administrator"},
                           new { RoleT=E_RoleT.LoanOpener, RoleId = new Guid("1764DFFD-EBF9-4375-ACCD-1B822B39FA63"), Desc="LoanOpener"},
                           new { RoleT=E_RoleT.CollateralAgent, RoleId = new Guid("E34352C6-45A8-4239-B2D1-218748C8B120"), Desc="CollateralAgent"},
                           new { RoleT=E_RoleT.PostCloser, RoleId = new Guid("A24E962E-301E-4436-BCA1-37E5D2227A30"), Desc="PostCloser"},
                           new { RoleT=E_RoleT.LockDesk, RoleId = new Guid("9067077D-6BD3-4E70-99C2-4A4AAF7811D7"), Desc="LockDesk"},
                           new { RoleT=E_RoleT.Shipper, RoleId = new Guid("27424FDC-D6C8-432C-860A-4ECA064F924F"), Desc="Shipper"},
                           new { RoleT=E_RoleT.Insuring, RoleId = new Guid("D8DC85B9-629E-4488-95AB-5DEC71FD8018"), Desc="Insuring"},
                           new { RoleT=E_RoleT.CallCenterAgent, RoleId = new Guid("5B02D42C-1F89-4BEC-8A29-63872837FC1D"), Desc="Telemarketer"},
                           new { RoleT=E_RoleT.Closer, RoleId = new Guid("270A7689-EEED-49F9-B438-69AC322EA834"), Desc="Closer"},
                           new { RoleT=E_RoleT.Processor, RoleId = new Guid("89572CFA-A3EE-4513-9B25-71A212AD5D09"), Desc="Processor"},
                           new { RoleT=E_RoleT.Accountant, RoleId = new Guid("F98C17F5-27AD-4097-ACFB-88F6A754ACF3"), Desc="Accountant"},
                           new { RoleT=E_RoleT.Manager, RoleId = new Guid("F544DF6A-5C55-4568-9579-97D74ED0F752"), Desc="Manager"},
                           new { RoleT=E_RoleT.LoanOfficer, RoleId = new Guid("6DFC9B0E-11E3-48C1-B410-A1B0B5363B69"), Desc="Agent"},
                           new { RoleT=E_RoleT.RealEstateAgent, RoleId = new Guid("74ED15E7-C06D-4EAB-9CB0-A64AB412FBE5"), Desc="RealEstateAgent"},
                           new { RoleT=E_RoleT.Pml_BrokerProcessor, RoleId = new Guid("B3785253-D9E8-4A3F-B0EC-D02D6FCF8538"), Desc="BrokerProcessor"},
                           new { RoleT=E_RoleT.Consumer, RoleId = new Guid("552EBA07-EA55-404F-8C00-DC696EAA04BB"), Desc="Consumer"},
                           new { RoleT=E_RoleT.LenderAccountExecutive, RoleId = new Guid("4FAA1C9E-3188-4D82-9734-E4991F6AD50C"), Desc="LenderAccountExec"},
                           new { RoleT=E_RoleT.Funder, RoleId = new Guid("9658E876-A3EC-4B3E-B081-F0F955719820"), Desc="Funder"},
                           new { RoleT=E_RoleT.CreditAuditor, RoleId = new Guid("6C83465C-D3E9-4950-918B-331DE262B4A5"), Desc="CreditAuditor"},
                           new { RoleT=E_RoleT.DisclosureDesk, RoleId = new Guid("1386D205-F5FB-435E-AFA8-730CDDEB824E"), Desc="DisclosureDesk"},
                           new { RoleT=E_RoleT.JuniorProcessor, RoleId = new Guid("4A03CF0F-D2E8-4201-B161-A5BC9E87FDE3"), Desc="JuniorProcessor"},
                           new { RoleT=E_RoleT.JuniorUnderwriter, RoleId = new Guid("AD4661DF-4DCA-4FE4-A60D-85424FB937FE"), Desc="JuniorUnderwriter"},
                           new { RoleT=E_RoleT.LegalAuditor, RoleId = new Guid("A9604616-6C20-4D0F-BE97-89913CF11AE3"), Desc="LegalAuditor"},
                           new { RoleT=E_RoleT.LoanOfficerAssistant, RoleId = new Guid("E4949811-34DC-4050-ADF5-78A06990E24E"), Desc="LoanOfficerAssistant"},
                           new { RoleT=E_RoleT.Purchaser, RoleId = new Guid("763F27C8-EAC4-45FF-AD65-15E27BDC41F1"), Desc="Purchaser"},
                           new { RoleT=E_RoleT.QCCompliance, RoleId = new Guid("270A9A82-F572-4810-8B8D-3D8D0EEDC65F"), Desc="QCCompliance"},
                           new { RoleT=E_RoleT.Secondary, RoleId = new Guid("1F51B219-5F29-4729-BAD4-AB2ACC670F24"), Desc="Secondary"},
                           new { RoleT=E_RoleT.Servicing, RoleId = new Guid("C5058D4E-BBC2-425B-B4A3-73B8CD66D896"), Desc="Servicing"},
                           new { RoleT=E_RoleT.Pml_Secondary, RoleId = new Guid("274981E4-8C18-4204-B97B-2F537922CCD9"), Desc="ExternalSecondary"},
                           new { RoleT=E_RoleT.Pml_PostCloser, RoleId = new Guid("89811C63-6A28-429A-BB45-3152449D854E"), Desc="ExternalPostCloser"}
                       };

            foreach (var o in list)
            {
                var role = Role.Get(o.RoleT);
                Assert.AreEqual(o.RoleId, role.Id, "RoleT=" + o.RoleT + " does not have expected id.");
                Assert.AreEqual(o.Desc, role.Desc, "RoleT=" + o.RoleT + " does not have expected desc.");
            }
        }

    }
}
