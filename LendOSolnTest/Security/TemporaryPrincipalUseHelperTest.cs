﻿namespace LendOSolnTest.Security
{
    using System;
    using LendersOffice.Security;
    using NUnit.Framework;

    [TestFixture]
    [Ignore("These tests are dependent upon LoDev data.")]
    public class TemporaryPrincipalUseHelperTest
    {
        [Test]
        public void Dispose_NonNullNewPrincipal_RestoresOriginalPrincipal()
        {
            var brokerId = new Guid("B321687E-A1B3-4A4E-88E4-397A0CB831B6");

            var originalPrincipal = PrincipalFactory.Create(brokerId, new Guid("EA30E886-CEB4-4650-A688-7E8C8770E36C"), "B", allowDuplicateLogin: true, isStoreToCookie: false);
            var newPrincipal = PrincipalFactory.RetrievePrincipalForUser(brokerId, new Guid("7362A439-69BB-498F-85A8-23742E8425B4"), "B");

            using (var helper = new TemporaryPrincipalUseHelper(originalPrincipal, newPrincipal))
            {
                Assert.AreEqual(PrincipalFactory.CurrentPrincipal.LoginNm, newPrincipal.LoginNm);
            }

            Assert.AreEqual(PrincipalFactory.CurrentPrincipal.LoginNm, originalPrincipal.LoginNm);
        }

        [Test]
        public void Dispose_NullNewPrincipal_DoesNotAlterCurrentPrincipal()
        {
            var brokerId = new Guid("B321687E-A1B3-4A4E-88E4-397A0CB831B6");

            var originalPrincipal = PrincipalFactory.Create(brokerId, new Guid("EA30E886-CEB4-4650-A688-7E8C8770E36C"), "B", allowDuplicateLogin: true, isStoreToCookie: false);

            using (var helper = new TemporaryPrincipalUseHelper(originalPrincipal, temporaryPrincipal: null))
            {
                Assert.AreEqual(PrincipalFactory.CurrentPrincipal.LoginNm, originalPrincipal.LoginNm);
            }

            Assert.AreEqual(PrincipalFactory.CurrentPrincipal.LoginNm, originalPrincipal.LoginNm);
        }

        [Test]
        public void Dispose_NullOriginalPrincipal_RestoresNullOriginalPrincipal()
        {
            var brokerId = new Guid("B321687E-A1B3-4A4E-88E4-397A0CB831B6");

            var newPrincipal = PrincipalFactory.RetrievePrincipalForUser(brokerId, new Guid("7362A439-69BB-498F-85A8-23742E8425B4"), "B");

            using (var helper = new TemporaryPrincipalUseHelper(originalPrincipal: null, temporaryPrincipal: newPrincipal))
            {
                Assert.AreEqual(PrincipalFactory.CurrentPrincipal.LoginNm, newPrincipal.LoginNm);
            }

            Assert.IsNull(PrincipalFactory.CurrentPrincipal);
        }

        [Test]
        public void Dispose_ThrowInUsing_RestoresOriginalPrincipal()
        {
            var brokerId = new Guid("B321687E-A1B3-4A4E-88E4-397A0CB831B6");

            var originalPrincipal = PrincipalFactory.Create(brokerId, new Guid("EA30E886-CEB4-4650-A688-7E8C8770E36C"), "B", allowDuplicateLogin: true, isStoreToCookie: false);
            var newPrincipal = PrincipalFactory.RetrievePrincipalForUser(brokerId, new Guid("7362A439-69BB-498F-85A8-23742E8425B4"), "B");

            try
            {
                using (var helper = new TemporaryPrincipalUseHelper(originalPrincipal, newPrincipal))
                {
                    Assert.AreEqual(PrincipalFactory.CurrentPrincipal.LoginNm, newPrincipal.LoginNm);
                    throw new Exception();
                }
            }
            catch
            {
                Assert.AreEqual(PrincipalFactory.CurrentPrincipal.LoginNm, originalPrincipal.LoginNm);
            }
        }
    }
}
