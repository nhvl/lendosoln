﻿
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Constants;
using NUnit.Framework;

namespace LendOSolnTest.Security
{
    [TestFixture]
    public class PasswordTests
    {   
        [Test]
        public void CheckPasswordDoesntContainBadCharacters()
        {
            List<string> possiblePasswords = new List<string> { "antonio1", "antoniov1" };
            List<string> badPasswords = new List<string>() { "antonio1>", "ant1onio<", "antoni&o1", "anto>ads1", "\"asdnasd23", "anto1n\"ion", "ant'ansdi02", "an%anotiew32us", "%%%%%%%%%a01", "ant2onio;asd", "antonio()aodsasd92", "piod+dsa2", "pasdoads-asdasdj19" };

            EmployeeDB db = new EmployeeDB();
            db.LoginName = "somerandomloginname";

            possiblePasswords.ForEach(password => Assert.That(db.IsStrongPassword(password), Is.EqualTo(StrongPasswordStatus.OK)));
            badPasswords.ForEach(password => Assert.That(db.IsStrongPassword(password), Is.EqualTo(StrongPasswordStatus.PasswordHasUnverifiedCharacters),"Failing password is=" + password));


        }

    }
}
