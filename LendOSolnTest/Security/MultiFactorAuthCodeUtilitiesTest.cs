﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using LendersOffice.Security;
using LendOSolnTest.Common;

namespace LendOSolnTest.Security
{
    [TestFixture]
    public class MultiFactorAuthCodeUtilitiesTest
    {
        [Test]
        public void SuccessfulTest()
        {
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.LoTest001);

            Guid userId = principal.UserId;

            string authCode = MultiFactorAuthCodeUtilities.GenerateNewCode(principal.BrokerId, userId);

            Assert.AreEqual(true, MultiFactorAuthCodeUtilities.Verify(principal.BrokerId, userId, authCode));
        }

        [Test]
        public void Successful2Test()
        {
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.LoTest001);

            Guid userId = principal.UserId;

            string authCode = MultiFactorAuthCodeUtilities.GenerateNewCode(principal.BrokerId, userId);

            Assert.AreEqual(false, MultiFactorAuthCodeUtilities.Verify(principal.BrokerId, userId, "FAKE"));
            Assert.AreEqual(true, MultiFactorAuthCodeUtilities.Verify(principal.BrokerId, userId, authCode));
        }

        [Test]
        public void ExceedFailedAttemptsTest()
        {
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.LoTest001);

            Guid userId = principal.UserId;

            string authCode = MultiFactorAuthCodeUtilities.GenerateNewCode(principal.BrokerId, userId);

            for (int i = 0; i < MultiFactorAuthCodeUtilities.MaxAllowNumberOfFailedAttempts; i++)
            {
                Assert.AreEqual(false, MultiFactorAuthCodeUtilities.Verify(principal.BrokerId, userId, "FAKE_PIN"));
            }

            // 4/27/2014 dd - Account should be lock.
            Assert.AreEqual(false, MultiFactorAuthCodeUtilities.Verify(principal.BrokerId, userId, authCode));
        }
    }
}
