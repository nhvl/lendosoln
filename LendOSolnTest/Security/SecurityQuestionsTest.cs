﻿using System.Linq;
using NUnit.Framework;
using LendOSolnTest.Common;
using LendersOffice.Admin;
using PriceMyLoan.Security;

namespace LendOSolnTest.Security
{
    [TestFixture]
    public class SecurityQuestionsTest
    {
        PriceMyLoanPrincipal m_principal = null;
        int m_nQuestionId1, m_nQuestionId2, m_nQuestionId3;

        [TestFixtureSetUp]
        public void SetUp()
        {
            m_principal = (PriceMyLoanPrincipal)LoginTools.LoginAs(TestAccountType.Pml_User0);
            CreateSecurityQuestions();
        }

        [TearDown]
        public void FixtureTearDown()
        {
        }

        public void CreateSecurityQuestions()
        {
            //Instead of creating new questions, use the first 3 questions
            m_nQuestionId1 = 1;
            m_nQuestionId2 = 2;
            m_nQuestionId3 = 3;

            Assert.IsTrue(m_nQuestionId1 > 0 && m_nQuestionId2 > 0 && m_nQuestionId3 > 0);
        }

        [Test]
        public void FetchSecurityQuestions()
        {
            var list = SecurityQuestion.ListAllSecurityQuestions(true);

            Assert.IsTrue(list.Count() > 0);
        }

        [Test]
        public void SaveAndMatchSecurityQuestionsAnswers()
        {
            bool bRet = BrokerUserDB.SaveSecurityQuestionsAnswers(m_principal.BrokerId, m_principal.UserId, m_nQuestionId1, "a", m_nQuestionId2, "b", m_nQuestionId3, "c");
            Assert.IsTrue(bRet);

            int nAttempts;
            int nRet = BrokerUserDB.MatchSecurityQuestionsAnswers(m_principal.BrokerId, m_principal.UserId, "a", "b", "c", out nAttempts);
            Assert.IsTrue(nRet != -2);

            nRet = BrokerUserDB.MatchSecurityQuestionsAnswers(m_principal.BrokerId, m_principal.UserId, "a", "b", "cc", out nAttempts);
            Assert.IsFalse(nRet != -2);
        }

        [Test]
        public void TestMarkingQuestionsObsoleteAndActive()
        {
            //Test marking a question obsolete
            SecurityQuestion.MarkQuestionObsolete(m_nQuestionId1, true);
            SecurityQuestion.MarkQuestionObsolete(m_nQuestionId2, true);
            SecurityQuestion.MarkQuestionObsolete(m_nQuestionId3, true);

            foreach (SecurityQuestion question in SecurityQuestion.ListAllSecurityQuestions(false))
            {
                Assert.AreNotEqual(m_nQuestionId1, question.QuestionId);
                Assert.AreNotEqual(m_nQuestionId2, question.QuestionId);
                Assert.AreNotEqual(m_nQuestionId3, question.QuestionId);

            }

            SecurityQuestion.MarkQuestionObsolete(m_nQuestionId1, false);

            bool bTest = false;

            foreach (SecurityQuestion question in SecurityQuestion.ListAllSecurityQuestions(true))
            {
                if (question.QuestionId == m_nQuestionId1)
                {
                    bTest = true;
                }
            }

            Assert.IsTrue(bTest);
        }
    }
}
