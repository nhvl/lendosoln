﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using LendersOffice.AntiXss;
using LendersOffice.Constants;
using DataAccess;

namespace LendOSolnTest.Security
{
    [TestFixture]
    public class AspxToolsTest
    {
        [Test]
        public void TestJsString()
        {
            string[,] data = {
                                {null, "''"}
                                 , {"", "''"}
                                 , {" abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_", "' abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_'"}
                                 , {"~!@#%^&*()", "'\\x7E\\x21\\x40\\x23\\x25\\x5E\\x26\\x2A\\x28\\x29'"}

                                 , {"'\"", "'\\'\\x22'"}
                                 , {"ABC\nDEF", "'ABC\\x0ADEF'"}
                             };

            for (int i = 0; i < data.Length / 2; i++)
            {
                Assert.AreEqual(data[i, 1], AspxTools.JsString(data[i, 0]), data[i, 0]);
            }

        }

        [Test]
        public void TestHtmlString()
        {
            string[,] data = {
                                {null, ""}
                                 , {"", ""}
                                 , {" abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_", " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_"}
                                 , {"~!@#%^&*()", "&#x7E;&#x21;&#x40;&#x23;&#x25;&#x5E;&amp;&#x2A;&#x28;&#x29;"}

                                 , {"'\"", "&#x27;&#x22;"}
                                 , {"ABC\nDEF", "ABC&#x0A;DEF"}
                             };

            for (int i = 0; i < data.Length / 2; i++)
            {
                Assert.AreEqual(data[i, 1], AspxTools.HtmlString(data[i, 0]), data[i, 0]);
            }

        }
        [Test]
        public void TestHtmlAttribute()
        {
            string[,] data = {
                                {null, "\"\""}
                                 , {"", "\"\""}
                                 , {" abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_", "\" abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_\""}
                                 , {"~!@#%^&*()", "\"&#x7E;&#x21;&#x40;&#x23;&#x25;&#x5E;&amp;&#x2A;&#x28;&#x29;\""}

                                 , {"'\"", "\"&#x27;&#x22;\""}
                                 , {"ABC\nDEF", "\"ABC&#x0A;DEF\""}
                             };

            for (int i = 0; i < data.Length / 2; i++)
            {
                Assert.AreEqual(data[i, 1], AspxTools.HtmlAttribute(data[i, 0]), data[i, 0]);
            }

        }
        [Test]
        public void TestSafeUrl()
        {
            string[,] data = {
                                 {null, "\"\""}
                                 , {"http://www.yahoo.com", "\"\""}
                                 , {"TestFoo.aspx", "\"TestFoo.aspx\""}
                                 , {"Test/Blah_/Blah/Foo.aspx", "\"Test/Blah_/Blah/Foo.aspx\""}
                                 , {"TestFoo.aspx?foo=bar", "\"TestFoo.aspx?foo=bar\""}
                                 , {"TestFoo.aspx?foo=bar&foo0=bar0", "\"TestFoo.aspx?foo=bar&foo0=bar0\""}
                                 , {"TestFoo.aspx?foo=bar&foo0=%20bar0", "\"TestFoo.aspx?foo=bar&foo0=%20bar0\""}
                                 , {"TestFoo.aspx?foo=bar&foo0=%20bar0%32", "\"TestFoo.aspx?foo=bar&foo0=%20bar0%32\""}
                                 , {"TestFoo.aspx?foo=bar&foo0=%20bar0%3G", "\"TestFoo.aspx?foo=bar&foo0=%20bar0%253G\""}
                                 , {Tools.GetEDocsLink("hello.aspx"), "\"" + Tools.GetEDocsLink("hello.aspx") + "\""}
                                 , {Tools.GetEDocsLink("/test.aspx?foo=bar"), "\"" + Tools.GetEDocsLink("/test.aspx?foo=bar") + "\""}
                                 , {Tools.GetEDocsLink("/TestF//oo.aspx?foo=bar"), "\"\""}
                                 , {Tools.GetLOLink("Foo.aspx"), "\"" + Tools.GetLOLink("Foo.aspx") + "\""}
                                 , {"TestFoo.aspx?foo=\"bar", "\"TestFoo.aspx?foo=%22bar\""}
                                 , {"//www.google.com", "\"\""}
                                 , {"TestF//oo.aspx?foo=bar", "\"\""}
                             };

            for (int i = 0; i < data.Length / 2; i++)
            {
                Assert.AreEqual(data[i, 1], AspxTools.SafeUrl(data[i, 0]), data[i, 0]);
            }
        }

        [Test]
        public void TestJsArray()
        {
            List<string> list = new List<string>() { "Test", "Test0", "Test1" };

            string expectedResult = "['Test','Test0','Test1']";

            Assert.AreEqual(expectedResult, AspxTools.JsArray(list));

        }

        [Test]
        public void TestJsArray1()
        {
            List<object> list = new List<object>() { "Test", 1, 2 };

            string expectedResult = "['Test',1,2]";

            Assert.AreEqual(expectedResult, AspxTools.JsArray(list));

        }
        [Test]
        public void TestJsArray2()
        {
            List<object> list = new List<object>();

            List<object> item0 = new List<object>() { "Test", 1, 2 };
            List<object> item1 = new List<object>() { "Test2", 3 };

            list.Add(item0);
            list.Add(item1);

            string expectedResult = "[['Test',1,2],['Test2',3]]";

            Assert.AreEqual(expectedResult, AspxTools.JsArray(list));

        }

    }
}
