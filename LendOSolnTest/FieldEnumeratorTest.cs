﻿using System;
using DataAccess;
using NUnit.Framework;
using LUnit;
using LendersOfficeApp.newlos;
using LendersOffice.Security;
using LendersOffice.Constants;
using PriceMyLoan.los.DataAccess;
using System.Linq;

using LendOSolnTest.Common;
using PriceMyLoan.Security;
using LendersOffice.Common;
using System.Reflection;
using Org.BouncyCastle.Utilities.Collections;
using System.Collections.Generic;
using System.Text;
using LendersOfficeApp.LOAdmin.TaskBackendUtilities;

namespace LendOSolnTest
{
    [TestFixture]
    public class FieldEnumeratorTest
    {
        [Test]
        public void CheckUniquePropertyNames()
        {
            var allPropertyNames = typeof(CAppData).GetProperties
            (
                BindingFlags.Instance |
                BindingFlags.NonPublic |
                BindingFlags.Public
            )
            .Union
            (
                typeof(CPageData).GetProperties
                (
                    BindingFlags.Instance |
                    BindingFlags.NonPublic |
                    BindingFlags.Public
                )
            )
            .FilterTypes()
            .Select(a => a.Name);

            var uniqueNames = new HashSet<string>();
            bool noCollisions = true;
            StringBuilder collisions = new StringBuilder("The following properties exist in both CAppData and CPageData: ");
            collisions.AppendLine();
            foreach (var name in allPropertyNames)
            {
                if (!uniqueNames.Add(name))
                {
                    noCollisions = false;
                    collisions.AppendLine("\"" + name + "\",");
                }
            }
            collisions.AppendLine();
            collisions.AppendLine("Please add their names to IgnoredPropertyNames in FieldEnumerator.cs");

            Assert.That(noCollisions, collisions.ToString());
        }
    }
}
