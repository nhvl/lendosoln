﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using LendersOffice.Common;

namespace LendOSolnTest.CommonTools
{
    [TestFixture]
    public class LOCacheTest
    {
        [Test]
        public void TestNonExistItem()
        {
            object value = LOCache.Get(Guid.NewGuid().ToString());
            Assert.IsNull(value);
        }

        [Test]
        public void TestNonExpireItem()
        {
            string value = Guid.NewGuid().ToString();

            string key = "KEY_" + Guid.NewGuid().ToString();
            LOCache.Set(key, value, DateTime.Now.AddMinutes(10));

            string outputValue = (string) LOCache.Get(key);

            Assert.AreEqual(value, outputValue);
        }

        [Test]
        public void TestUpdateNonExpireItem()
        {
            string value = Guid.NewGuid().ToString();

            string key = "KEY_" + Guid.NewGuid().ToString();
            LOCache.Set(key, value, DateTime.Now.AddMinutes(10));

            string updatedValue = Guid.NewGuid().ToString();
            LOCache.Set(key, updatedValue, DateTime.Now.AddMinutes(10));

            string outputValue = (string)LOCache.Get(key);
            Assert.AreEqual(updatedValue, outputValue);
        }

        [Test]
        public void TestExpiredItem()
        {
            string value = Guid.NewGuid().ToString();
            string key = "KEY_" + Guid.NewGuid().ToString();
            LOCache.Set(key, value, DateTime.Now.AddMinutes(-10));

            string outputValue = (string)LOCache.Get(key);
            Assert.IsNull(outputValue);
        }
    }
}
