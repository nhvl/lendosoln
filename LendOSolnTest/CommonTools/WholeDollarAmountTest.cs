﻿namespace LendOSolnTest.CommonTools
{
    using System.Linq;
    using DataAccess;
    using NUnit.Framework;

    [TestFixture]
    public class WholeDollarAmountTest
    {
        [Test]
        public void Amounts_AlreadyWholeDollars_ReturnTrue()
        {
            Assert.IsTrue(Tools.IsWholeDollarAmount(250000.000000M));
            Assert.IsTrue(Tools.IsWholeDollarAmount(500000M));
            Assert.IsTrue(Tools.IsWholeDollarAmount(750000.00M));
        }

        [Test]
        public void Amounts_CalculatesToWholeDollars_ReturnTrue()
        {
            var valuesToHundredthsPlace = SumDecimalsWithoutRounding(250000.25M, 125.5M, 1275.75M, 1850.5M);
            Assert.IsTrue(Tools.IsWholeDollarAmount(valuesToHundredthsPlace));

            var valuesToSixthDecimalPlace = SumDecimalsWithoutRounding(
                250000.999915M,
                125.000005M,
                1275.000075M,
                1850.000005M);

            Assert.IsTrue(Tools.IsWholeDollarAmount(valuesToHundredthsPlace));

            var valuesToTwentiethDecimalPlace = SumDecimalsWithoutRounding(
                250000.99999999999999999915M,
                125.00000000000000000005M,
                1275.00000000000000000075M,
                1850.00000000000000000005M);

            Assert.IsTrue(Tools.IsWholeDollarAmount(valuesToTwentiethDecimalPlace));
        }

        [Test]
        public void Amounts_AlreadyWithDecimals_ReturnFalse()
        {
            Assert.IsFalse(Tools.IsWholeDollarAmount(250000.2500000M));
            Assert.IsFalse(Tools.IsWholeDollarAmount(500000.75M));
            Assert.IsFalse(Tools.IsWholeDollarAmount(750000.999999999999975M));
        }

        [Test]
        public void Amounts_CalculatesToFractionalDollars_ReturnFalse()
        {
            var valuesToHundredthsPlace = SumDecimalsWithoutRounding(250000.15M, 125.5M, 1275.65M, 1850.5M);
            Assert.IsFalse(Tools.IsWholeDollarAmount(valuesToHundredthsPlace));

            var valuesToSixthDecimalPlace = SumDecimalsWithoutRounding(
                250000.000025M,
                125.000005M,
                1275.000075M,
                1850.000005M);

            Assert.IsFalse(Tools.IsWholeDollarAmount(valuesToHundredthsPlace));

            var valuesToTwentiethDecimalPlace = SumDecimalsWithoutRounding(
                250000.00000000000000000025M,
                125.00000000000000000005M,
                1275.00000000000000000075M,
                1850.00000000000000000005M);

            Assert.IsFalse(Tools.IsWholeDollarAmount(valuesToTwentiethDecimalPlace));
        }

        private decimal SumDecimalsWithoutRounding(params decimal[] decimals) => decimals.Sum();
    }
}
