﻿


using DataAccess;
using LookUpTableLib;
using System.Collections.Generic;
using System;
namespace LendOSolnTest.CommonTools.TestLookupTable
{
    public enum E_ConvertsProdSptToNumberOfUnits_Action
    {
        Four,
        One,
        Three,
        Two,
    }

    /* Table's Schema 
    * ======================
    * sProdSpT(Enum:sProdSpT):SFR PUD Condo CoOp Manufactured Townhouse Commercial MixedUse TwoUnits ThreeUnits FourUnits Modular Rowhouse
    * sLT(Enum:sLT):Conventional FHA VA UsdaRural
    */

    public class ConvertsProdSptToNumberOfUnits : ComboTableLUBase
    {
        private static readonly ConvertsProdSptToNumberOfUnits Instance = new ConvertsProdSptToNumberOfUnits();
        private ConvertsProdSptToNumberOfUnits()
        {
            schema = new List<NewTableVariable>();
            var Vars = new[]
			{
				new { name = "sProdSpT", TypeName = "Enum:sProdSpT", values = new string[]{"SFR", "PUD", "Condo", "CoOp", "Manufactured", "Townhouse", "Commercial", "MixedUse", "TwoUnits", "ThreeUnits", "FourUnits", "Modular", "Rowhouse"}},
				new { name = "sLT", TypeName = "Enum:sLT", values = new string[]{"Conventional", "FHA", "VA", "UsdaRural"}}
			};

            foreach (var v in Vars)
            {
                schema.Add(new NewTableVariable(v.name, v.TypeName, v.values));
            }
            actionStrs = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase){
				{"sprodspt:sfr;slt:conventional;", "One"},
				{"sprodspt:sfr;slt:fha;", "One"},
				{"sprodspt:sfr;slt:va;", "One"},
				{"sprodspt:sfr;slt:usdarural;", "One"},
				{"sprodspt:pud;slt:conventional;", "One"},
				{"sprodspt:pud;slt:fha;", "One"},
				{"sprodspt:pud;slt:va;", "One"},
				{"sprodspt:pud;slt:usdarural;", "One"},
				{"sprodspt:condo;slt:conventional;", "One"},
				{"sprodspt:condo;slt:fha;", "One"},
				{"sprodspt:condo;slt:va;", "One"},
				{"sprodspt:condo;slt:usdarural;", "One"},
				{"sprodspt:coop;slt:conventional;", "One"},
				{"sprodspt:coop;slt:fha;", "One"},
				{"sprodspt:coop;slt:va;", "One"},
				{"sprodspt:coop;slt:usdarural;", "One"},
				{"sprodspt:manufactured;slt:conventional;", "One"},
				{"sprodspt:manufactured;slt:fha;", "One"},
				{"sprodspt:manufactured;slt:va;", "One"},
				{"sprodspt:manufactured;slt:usdarural;", "One"},
				{"sprodspt:townhouse;slt:conventional;", "One"},
				{"sprodspt:townhouse;slt:fha;", "One"},
				{"sprodspt:townhouse;slt:va;", "One"},
				{"sprodspt:townhouse;slt:usdarural;", "One"},
				{"sprodspt:commercial;slt:conventional;", "One"},
				{"sprodspt:commercial;slt:fha;", "One"},
				{"sprodspt:commercial;slt:va;", "One"},
				{"sprodspt:commercial;slt:usdarural;", "One"},
				{"sprodspt:mixeduse;slt:conventional;", "One"},
				{"sprodspt:mixeduse;slt:fha;", "One"},
				{"sprodspt:mixeduse;slt:va;", "One"},
				{"sprodspt:mixeduse;slt:usdarural;", "One"},
				{"sprodspt:twounits;slt:conventional;", "Two"},
				{"sprodspt:twounits;slt:fha;", "Two"},
				{"sprodspt:twounits;slt:va;", "Two"},
				{"sprodspt:twounits;slt:usdarural;", "Two"},
				{"sprodspt:threeunits;slt:conventional;", "Three"},
				{"sprodspt:threeunits;slt:fha;", "Three"},
				{"sprodspt:threeunits;slt:va;", "Three"},
				{"sprodspt:threeunits;slt:usdarural;", "Three"},
				{"sprodspt:fourunits;slt:conventional;", "Four"},
				{"sprodspt:fourunits;slt:fha;", "Four"},
				{"sprodspt:fourunits;slt:va;", "Four"},
				{"sprodspt:fourunits;slt:usdarural;", "Four"},
				{"sprodspt:modular;slt:conventional;", "One"},
				{"sprodspt:modular;slt:fha;", "One"},
				{"sprodspt:modular;slt:va;", "One"},
				{"sprodspt:modular;slt:usdarural;", "One"},
				{"sprodspt:rowhouse;slt:conventional;", "One"},
				{"sprodspt:rowhouse;slt:fha;", "One"},
				{"sprodspt:rowhouse;slt:va;", "One"},
				{"sprodspt:rowhouse;slt:usdarural;", "One"},
			};
        }

        private E_ConvertsProdSptToNumberOfUnits_Action LookUpNonStatic(E_sProdSpT @sProdSpT, E_sLT @sLT)
        {
            switch (@sProdSpT)
            {
                case E_sProdSpT.SFR:
                    break;
                case E_sProdSpT.PUD:
                    break;
                case E_sProdSpT.Condo:
                    break;
                case E_sProdSpT.CoOp:
                    break;
                case E_sProdSpT.Manufactured:
                    break;
                case E_sProdSpT.Townhouse:
                    break;
                case E_sProdSpT.Commercial:
                    break;
                case E_sProdSpT.MixedUse:
                    break;
                case E_sProdSpT.TwoUnits:
                    break;
                case E_sProdSpT.ThreeUnits:
                    break;
                case E_sProdSpT.FourUnits:
                    break;
                case E_sProdSpT.Modular:
                    break;
                case E_sProdSpT.Rowhouse:
                    break;
                default:
                    throw new Exception(@sProdSpT + " was not recognized as a E_sProdSpT.");
            }

            switch (@sLT)
            {
                case E_sLT.Conventional:
                    break;
                case E_sLT.FHA:
                    break;
                case E_sLT.VA:
                    break;
                case E_sLT.UsdaRural:
                    break;
                default:
                    throw new Exception(@sLT + " was not recognized as a E_sLT.");
            }


            List<string> varnames = new List<string>(new string[] { "sProdSpT", "sLT" });
            List<string> vals = new List<string>(new string[] { @sProdSpT.ToString(), @sLT.ToString() });
            string actionStr = pLookUp(varnames, vals);
            Type actionType = typeof(E_ConvertsProdSptToNumberOfUnits_Action);
            E_ConvertsProdSptToNumberOfUnits_Action action = (E_ConvertsProdSptToNumberOfUnits_Action)Enum.Parse(actionType, actionStr, true);
            return action;
        }
        public static E_ConvertsProdSptToNumberOfUnits_Action LookUp(E_sProdSpT @sProdSpT, E_sLT @sLT)
        {
            return Instance.LookUpNonStatic(@sProdSpT, @sLT);
        }
    }
}
#region <xml>
/*
<?xml version="1.0" encoding="utf-8"?>
<NewTableObj xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <head>
    <NewTableVariable>
      <name>sProdSpT</name>
      <currValueIndex>0</currValueIndex>
      <TypeName>Enum:sProdSpT</TypeName>
      <Name>sProdSpT</Name>
      <Values>
        <string>SFR</string>
        <string>PUD</string>
        <string>Condo</string>
        <string>CoOp</string>
        <string>Manufactured</string>
        <string>Townhouse</string>
        <string>Commercial</string>
        <string>MixedUse</string>
        <string>TwoUnits</string>
        <string>ThreeUnits</string>
        <string>FourUnits</string>
        <string>Modular</string>
        <string>Rowhouse</string>
      </Values>
      <CurrValueIndex>0</CurrValueIndex>
    </NewTableVariable>
    <NewTableVariable>
      <name>sLT</name>
      <currValueIndex>0</currValueIndex>
      <TypeName>Enum:sLT</TypeName>
      <Name>sLT</Name>
      <Values>
        <string>Conventional</string>
        <string>FHA</string>
        <string>VA</string>
        <string>UsdaRural</string>
      </Values>
      <CurrValueIndex>0</CurrValueIndex>
    </NewTableVariable>
  </head>
  <actions>
    <actionDic numItems="0" />
  </actions>
  <rules>
    <rules>
      <NewRule>
        <variables>
          <NewTableVariable>
            <name>sProdSpT</name>
            <currValueIndex>0</currValueIndex>
            <TypeName>Enum:sProdSpT</TypeName>
            <Name>sProdSpT</Name>
            <Values>
              <string>SFR</string>
              <string>PUD</string>
              <string>Condo</string>
              <string>CoOp</string>
              <string>Manufactured</string>
              <string>Townhouse</string>
              <string>Commercial</string>
              <string>MixedUse</string>
              <string>Modular</string>
              <string>Rowhouse</string>
            </Values>
            <CurrValueIndex>0</CurrValueIndex>
          </NewTableVariable>
        </variables>
        <actions>One</actions>
      </NewRule>
      <NewRule>
        <variables>
          <NewTableVariable>
            <name>sProdSpT</name>
            <currValueIndex>0</currValueIndex>
            <TypeName>Enum:sProdSpT</TypeName>
            <Name>sProdSpT</Name>
            <Values>
              <string>ThreeUnits</string>
            </Values>
            <CurrValueIndex>0</CurrValueIndex>
          </NewTableVariable>
        </variables>
        <actions>Three</actions>
      </NewRule>
      <NewRule>
        <variables>
          <NewTableVariable>
            <name>sProdSpt</name>
            <currValueIndex>0</currValueIndex>
            <TypeName>Enum:sProdSpT</TypeName>
            <Name>sProdSpt</Name>
            <Values>
              <string>FourUnits</string>
            </Values>
            <CurrValueIndex>0</CurrValueIndex>
          </NewTableVariable>
        </variables>
        <actions>Four</actions>
      </NewRule>
      <NewRule>
        <variables>
          <NewTableVariable>
            <name>sProdSpt</name>
            <currValueIndex>0</currValueIndex>
            <TypeName>Enum:sProdSpT</TypeName>
            <Name>sProdSpt</Name>
            <Values>
              <string>TwoUnits</string>
            </Values>
            <CurrValueIndex>0</CurrValueIndex>
          </NewTableVariable>
        </variables>
        <actions>Two</actions>
      </NewRule>
    </rules>
  </rules>
</NewTableObj>*/
#endregion
