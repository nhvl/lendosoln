﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using DataAccess;

namespace LendOSolnTest.CommonTools.TestLookupTable
{
    [TestFixture]
    public class LookupTableTest
    {
        [Test]
        public void TestTable()
        {
            Assert.That(ConvertsProdSptToNumberOfUnits.LookUp(E_sProdSpT.SFR, E_sLT.Conventional), Is.EqualTo(E_ConvertsProdSptToNumberOfUnits_Action.One));
        }
    }
}
