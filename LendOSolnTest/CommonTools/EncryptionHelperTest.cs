/// Author: David Dao

using System;
using NUnit.Framework;
using LendersOffice.Common;
using DataAccess;
using System.Web.Security;
using System.Text;
using System.Linq;

namespace LendOSolnTest.CommonTools
{
    [TestFixture]
	public class EncryptionHelperTest
	{



        [Test]
        public void VerifyGeneratePBKDF2HashIsNotChanged()
        {

                var passwordsToCheck = new[] {
                    new { Password = "_$Lf9|-Em&" , Salt = "3E8.b6xb7MxObV1KPu42R9C1IA==", PasswordHash = "C3r9e6zLyMDOzdERSXD2vKbGOQhdIiE5" },
                    new { Password = "(w7&HbK!ac" , Salt = "3E8.rL4RvMwOp0Rb9NvIKDHh+g==", PasswordHash = "pr5CseTmUrpsT7rtgguCf08vgoKC6c9Q" },
                    new { Password = "E;_%gjd>{t" , Salt = "3E8.OKMJINB+T0Z3aepxvVu7vw==", PasswordHash = "eq2KLmh5PlMRqP2XHDpAbd0+TruOM72E" },
                    new { Password = "nL&M8A$;n0" , Salt = "3E8.cMSR1dH/P1yJlWF5Y79Dcg==", PasswordHash = "0trlstUyPPbKJYQh5kr2TdYqdL+qK37H" },
                    new { Password = "d$3yZw$_3V" , Salt = "3E8.U9I+Q0hsQivaqLMezugE+g==", PasswordHash = "PbNWg95bCU9Xt9P7ksEbFBAcp5gNNgS1" },
                    new { Password = "l?qQ)Ipn^f" , Salt = "3E8.I8w4UTPtWEWFOJ5fHvkpwQ==", PasswordHash = "5W0TAtcbDoVA++Ueg0+Rft9R3M4E7r5/" },
                    new { Password = "#/oOa|ff26" , Salt = "3E8.Cq4r6wy3QfOxKJJH1L3COw==", PasswordHash = "o4S/h3LI3t5TOZFfpwHTvh6CLEyXQA9G" },
                    new { Password = "s.}IAkSL.1" , Salt = "3E8.Ps/GP2VD6e4bdKp5eJyURw==", PasswordHash = "wJIIQoioQjTaOzZF3k2KFUz4LZ8pFyU9" },
                    new { Password = "KA>!Je^6yV" , Salt = "3E8.798wfOuksJyhXNfWDIZokQ==", PasswordHash = "xUSLv0hS6/aGL/Yi1txBN9Nz0gjtksG5" },
                    new { Password = "krie+)).Ti" , Salt = "3E8.97K0GlwmPJ4MzLIGY4YKjw==", PasswordHash = "AovckZ1TaMVPFzngmsqBvPQzPhPaFRCY" },
                    new { Password = "kzb)|#YnUP" , Salt = "3E8.2Z+uttKbRipOsbUcrj4XnQ==", PasswordHash = "yrYRgnoLa5DzTGC01QfMt2mmQhEFN4Fe" },
                    new { Password = "c(5c+H21^m" , Salt = "3E8.FPzne7Ap3ixV0tUnSMKcvw==", PasswordHash = "Q4b9eUXZtvD1nswVAMBlZGyt/lrmOlqB" },
                    new { Password = "d!+oeDCg+S" , Salt = "3E8.J55MnrlCxsEwFK37FsDBfw==", PasswordHash = "wV16iw6dHs5V2O4DA+N8OyiH3v0WoLGG" },
                    new { Password = "&%9#!CKrJo" , Salt = "3E8.i7TKGUIR+KajJpx4aINuqQ==", PasswordHash = "t76Zm8U89a7lapT8Pj90VM2dWPIiWgoW" },
                    new { Password = "gSsy|!XNg>" , Salt = "3E8.e55c17uvSk7imAvrVEkptw==", PasswordHash = "AEK4dRQS6RnXNSRm+cUb8N1I+uhngzCJ" },
                    new { Password = "UkS+X%*W!@" , Salt = "3E8.E1v6wBNay8if2VQxzsN/+g==", PasswordHash = "iOT+46dCZG+0VYxXVfiIj5BWrVvXuoAK" },
                    new { Password = "G=i5&m;I]$" , Salt = "3E8.hF+AcNaajIQbubd6ktbv1A==", PasswordHash = "tF7KoT5gHDCkR8fxikUaYRU0x0N07BBN" },
                    new { Password = "@(JZ3hppq}" , Salt = "3E8.kl7USACIBRGUrD81yVVTLw==", PasswordHash = "JJGualyvhk/bQ+QXmO62EJFLW2ZkgYVg" },
                    new { Password = "ZF?A*hl{Xc" , Salt = "3E8.PucYQo4RC6cH2EA5XxdOhw==", PasswordHash = "pXd3sxcH7W2RDWriq8LSgEvRC1sU32X8" },
                    new { Password = "|O[cl[V-R!" , Salt = "3E8.5mYeB+blHh4F84zhFLZZjQ==", PasswordHash = "cr4H1pQJnLqqFroP/97WDr4KnskS3/jC" },
                    new { Password = ".+Q5ldpU]z" , Salt = "3E8.+xN96/ozpzjC+9+btG/5GA==", PasswordHash = "mP1zUgMwyODFi52y6244/2ADBrd2HbFP" },
                    new { Password = "u@y}5V5BD+" , Salt = "3E8.8Kfh9KN+wPEo1FVJr+MOpQ==", PasswordHash = "9LE0je6Gjvhd1cuMTc5zHkl4t2nTd/NB" },
                    new { Password = "]Pn6-Y]vhM" , Salt = "3E8.dBxVZ+XtS4zZJkko/8lFKA==", PasswordHash = "gUOMknIxAT+X+7skgHhBdLF0bYmkqxgx" },
                    new { Password = "VlN_gx?5-F" , Salt = "3E8.P3EZzTwRaOxKEXz/RGjH7Q==", PasswordHash = "ae1G5p1Xh5e7Tb9HROrDP7eDswmFnFRY" },
                    new { Password = "NHqzq&9;:X" , Salt = "3E8.+5Qcxfs7A6DkxoZ8HjYcyg==", PasswordHash = "Gm4WL7fy0FrHA+kzSBThac+tU9F4CEYS" },
                    new { Password = "&6+t&vUbk3" , Salt = "3E8.aPfxtIaB3nZBQgJgrmHUgQ==", PasswordHash = "trVNFAqSdS6ZpVnsovvpGKjo+FRUlADT" },
                    new { Password = "=)ttV2k{2m" , Salt = "3E8.R+yq66iSkGQfhcb1izSXlw==", PasswordHash = "BqqnZ5YWLNkIp19PCyZ1kPB7gZEana0G" },
                    new { Password = ";2UOr(&5Be" , Salt = "3E8.eF7O0Dq5CD/WZLr36cp6oQ==", PasswordHash = "h1OjaTzKd2iPqzbRvaWWEx9E1Ti6Rbfh" },
                    new { Password = "_jRhe:7>s!" , Salt = "3E8.z0J73EdhjuPxuhbBL94BYQ==", PasswordHash = "1NhybqSCdmvQhvrUfx6GkB73WgpRVp3X" },
                    new { Password = "8EfeU2+.y{" , Salt = "3E8.MIMLQjlgHyywDSuuVbVGiQ==", PasswordHash = "vetjBdzvzkk7rSjwaCea5ZoFZ/6zclw3" },
                    new { Password = "ifUk:N=dZ(" , Salt = "3E8.FfRdz/4//a32e0tgUmhPsQ==", PasswordHash = "FtWniWNyPvy3Dn+DqswlWKIU0zcV3L1t" },
                    new { Password = "Kh@EDQ:y?m" , Salt = "3E8.iIOwlpacggHUrVDhXGDDWQ==", PasswordHash = "LzmXzHnoyFoBI6hEd3W6v8dA8DPt2mzB" },
                    new { Password = "x-IcG}PDe:" , Salt = "3E8.uMO/W9vGALl7H5LJ/Ob5yQ==", PasswordHash = "pNPA1etXRNLiDof7QwIcJgRcrmTMYeh8" },
                    new { Password = "GCAy$[bB6|" , Salt = "3E8.Qi6hQQSf0LN0yRsKaFgCag==", PasswordHash = "n7Ejplk5i763JRJelZ2b7CZh7/JPeHV2" },
                    new { Password = "K]==nJNVbi" , Salt = "3E8.hVD6vZDd88tKSeQA3oy2kA==", PasswordHash = "A+KArXfKmsyPdE6K6jTaCcHeVyTM2m8W" },
                    new { Password = "l%wWb&&83i" , Salt = "3E8.OT/3/jn5z+oEP0JnozS2lg==", PasswordHash = "7OoUJBOOxF57jJ4DWclnBxLuf0V+Qfr9" },
                    new { Password = "/;I0Vhji$f" , Salt = "3E8.hf08vFpfcJ/TS7HlZ5gLBw==", PasswordHash = "akn06a8FDtW2g+naB+ei644AqEqudbwt" },
                    new { Password = "m=SI;;_PbX" , Salt = "3E8.Sbic5T8LqZT45BC5tgx5aw==", PasswordHash = "XqkTnuaE/PHMAH40syCLA5x4rJoHZhqZ" },
                    new { Password = "^D!W}orkOr" , Salt = "3E8.u2L7DcuC/471SDysqPrN+g==", PasswordHash = "51TIo+znLAamq44cEz87rtQcG6RPOEvp" },
                    new { Password = "_sh6n#O#LN" , Salt = "3E8.PgWKPR7OJUmh5DYKRlTaLQ==", PasswordHash = "WKyGG6D/cWHyG7ZaubI2mJjq0cEwwH+X" },
                    new { Password = "]W*73A>zP?" , Salt = "3E8.fdtMYaaYZkbjV5OZlVFfVg==", PasswordHash = "XjuTQiAFbMkviC7BLK9xKuZeVjICMwv2" },
                    new { Password = "MaC{0nf>k&" , Salt = "3E8.4ujZO5D7yXQ5WoyT1lFPHA==", PasswordHash = "ETuWiafLsvefHgCtzSmGSp1nFrGxAWyO" },
                    new { Password = "3Bb}xf^8K_" , Salt = "3E8.qf99aSMSB2XKh9KtBZLT+g==", PasswordHash = "7xqG1kIP0Kv60rHbXCBSvvwDK5mdvooN" },
                    new { Password = ";-:t=Ln=h@" , Salt = "3E8.ZzibzjNYpWeBnVtxPncBMA==", PasswordHash = "h0FRAv0t3qYvZaNjolVPe3+eQlrhrA8I" },
                    new { Password = "=$2{Q9onn8" , Salt = "3E8./OhC3xC0Gsy+gQpE8mtONw==", PasswordHash = "i1o1D6GEzNeN7egbmrfsH+lL4WcCJ5LN" },
                    new { Password = "Z!bWw%Hs|D" , Salt = "3E8.RZCZWZPQJUuo6TJlfbnYtA==", PasswordHash = "52z/hiyv82F1aTN8KCgC+67gPUkmcu47" },
                    new { Password = "sghau%!+yj" , Salt = "3E8.dY5RuXinsKdW/CL08apXCA==", PasswordHash = "8Lhuy+f5uXT3vdFxEX27QhTl88LqjsK4" },
                    new { Password = "cA^#(A5Ona" , Salt = "3E8.cICrueMG7dEiEuu32KEdLQ==", PasswordHash = "cuan8bTiluGVy6MhX5WmxdxV75+A9YjG" },
                    new { Password = "]C:drsp>Hr" , Salt = "3E8.FZa8waocRYCWKNkhi11a6w==", PasswordHash = "fI/rNmo7yfWqLDe0h0owk39BnEKb3rV4" },
                    new { Password = "3Mbh81:$D." , Salt = "3E8.ISa0yud1LEwl9RdLzMHraA==", PasswordHash = "fIAY/LQvLUKdocyf9uYIwXjrxlpL5jND" },
                    new { Password = "(D}}}U^!S_" , Salt = "3E8.JGVjtZp8tkheo4eEqDHG/w==", PasswordHash = "+Fjbzke0c0f9NL1fv397sS4A8ITaOklG" },
                    new { Password = "d@?@zTVeBG" , Salt = "3E8.v0CRMkq5+c7c0ycN9lUWHQ==", PasswordHash = "NIZTIea5pAsEw+WlFb4wTIO1CojpfH4B" },
                    new { Password = "{/0O_NO-Xk" , Salt = "3E8.TLnsmC6W9pjV9jCa7zB7bg==", PasswordHash = "rvduRF/X7D5HOmcylX0/SSI0hKWjGFI6" },
                    new { Password = ")5W#uDh8w=" , Salt = "3E8.jiHoLvt/lEAjCMKMvheh7w==", PasswordHash = "QKqGHLCe26esrhnXgTXCaJUAK2YBWBR8" },
                    new { Password = "-x:NZC?N)h" , Salt = "3E8.4/ZlnWAvqPqZ3D3dHtKvGw==", PasswordHash = "YoZjfnjIB0KWwB5kI5ALNdrMXKGbiC4Y" },
                    new { Password = "+ADx^uc@Sf" , Salt = "3E8.p/rNvIwbaWFEMHo6s5fIuA==", PasswordHash = "pgpGd/zPRxxnRRpfMd3upPKB7nekFti5" },
                    new { Password = "JqLO;Q*X-v" , Salt = "3E8.9JnlcQiEkFPjxV+5ZlibUg==", PasswordHash = "fM0my0IUZC8KNWZyiPuf+pHv6IJJbR0i" },
                    new { Password = "3z[A>1Fq%k" , Salt = "3E8.xgW0+nuu0R9ouOh1i7+GUw==", PasswordHash = "Ivlz2EDQvFWfqZSOGqx9HAXSGnzeDHLx" },
                    new { Password = "o=mfX$9f%e" , Salt = "3E8.MIAhYUQUL48AdYo7UV8GQw==", PasswordHash = "ANXFvoUdv2dPT5JMZSB7yk34KcSjJEGd" },
                    new { Password = ";ioYqP]^Et" , Salt = "3E8.Dr4aA1ZffkrrXg7zvAttSw==", PasswordHash = "pSH1kCM0ANtGDhqV5tHfzNiJqS/IEaou" },
                    new { Password = "_-A_oAXRah" , Salt = "3E8.G68Q3E6rN6vVA1lSVUD52g==", PasswordHash = "6j7B61jt4cDhBYRubOpedEHCv+/EduWo" },
                    new { Password = "V1XP@k:|FK" , Salt = "3E8.rE1QFIHkWTuxcAJBIF1YFA==", PasswordHash = "0yb03Ukwh39gOwyyN7gLnAqYcD/EdhfZ" },
                    new { Password = ";tAv+_HJC;" , Salt = "3E8.HNMW5ptuBrqtoBlsizbQTg==", PasswordHash = "YSu0a8jjOqS7uAcgNA99gILaSVM9iZWC" },
                    new { Password = "g3:ilVo%|p" , Salt = "3E8.XiG14Lq+1Oz24sVuZNE82g==", PasswordHash = "5SOR/HolkL2wYTJTvJJSe8J3QL68z7La" },
                    new { Password = "#pf:O-vH(V" , Salt = "3E8.ymscfI0Y0lSS7WBLzyU23g==", PasswordHash = "92jWQbrw0JMrqAZ6/OGcwwc/wp/rgEU/" },
                    new { Password = "yG/:D^2tdw" , Salt = "3E8.HWI5CbkW6MhP8CDnc+t4kQ==", PasswordHash = "CCqgfgEhKp1h8jLQg5F+Q4TC+sqVNSbf" },
                    new { Password = "gRM{[nsM:%" , Salt = "3E8.rA4DyKJPpIooKnjs5TWE/Q==", PasswordHash = "VSlzOJ5j50FtEWlQr9yN7HAH4alicPFX" },
                    new { Password = "k6^0yP.B3%" , Salt = "3E8.opWO4RoXdKrloNqsTFa3Rw==", PasswordHash = "6dIN7hRNWTaSUljnqWduaeYwb5SjlNCd" },
                    new { Password = "M3G;r;Q=x8" , Salt = "3E8.mV0bqJveSBVSnltizf/59A==", PasswordHash = "tq29n6XW86B6ZufhN7/3IQfO4J2YnDCh" },
                    new { Password = "Vc$4GxL4^$" , Salt = "3E8.5WuIDZcr1fwsenJAfPGr+g==", PasswordHash = "5F5WWbKGpCxVTPkyxlW8azbcoml7FP8p" },
                    new { Password = "s=eb((yYtG" , Salt = "3E8.Zd3dkp1sfU4yBoHggrFbzQ==", PasswordHash = "TsIo0UVHjBUrDJckcVqc0ql+mwEi/mXD" },
                    new { Password = "7=}j:DUNyX" , Salt = "3E8.iug/2Gr2TSpRr/ptNsK/gQ==", PasswordHash = "1EANhzvAXpX78c7+zSiHjAzloZDYXZ9s" },
                    new { Password = "U?s{qo$rKj" , Salt = "3E8.QVRMhP9myoqbfNGeQF0dYw==", PasswordHash = "klw3eXomiSFpV0dLPKBhTbouFa5SWvBC" },
                    new { Password = "aNRqh#(Gi|" , Salt = "3E8.hoxXRxoCQQ9r1jML0rf5Cg==", PasswordHash = "rSRVMDBh437EhZCU2BhqoS4QJAsaFDef" },
                    new { Password = "!A=5o|MORn" , Salt = "3E8.shvnp7VM0fVRa6N5F21LCA==", PasswordHash = "HeuE49MoW2xk9E+HT9LZ2wBtCcbb/LjF" },
                    new { Password = ")N(81511_C" , Salt = "3E8.F4/qWD4us9M122jokObtfg==", PasswordHash = "tjjpnFTvpiY1CNUACDhNYwAtgEwrcCQv" },
                    new { Password = "7^_jH8ME0]" , Salt = "3E8.KW/b2bAixaDYFuZ12upwyg==", PasswordHash = "VY3w2wcO1ayS6+IlEIIfxCvS8scGQPzA" },
                    new { Password = "#-ljTKJ(Xn" , Salt = "3E8.Ha6vsgNQalSBBBDO00CqQA==", PasswordHash = "dSPIym2Lb5VR4fiq5qrH7gEIGX74049Q" },
                    new { Password = "c|eqp-*Y*+" , Salt = "3E8.9yItQ0vztpeFuYVv4DfHow==", PasswordHash = "pNfjCkuZR5MtTHudws7uo2nBujAZDEHZ" },
                    new { Password = "]otKMPb.L+" , Salt = "3E8.JlrFQ5YZfgavQgfj9DsT5w==", PasswordHash = "z7226STPE+SQQ5fSfZaYrbNOqcKPvcJH" },
                    new { Password = "Pn]ZS-q@dx" , Salt = "3E8.YJMPkuPnhE/n0ffRPGCrSw==", PasswordHash = "4A3m97gVOzUzN7prVtmqYoEkO+3eWJ6/" },
                    new { Password = "G%BI$jBGx!" , Salt = "3E8.3EZCefUslHqVFLrLq4000A==", PasswordHash = "8zYz2Fd5mQtWpNIlW/uRvSEHfVAu/gj4" },
                    new { Password = ":/&TY=Gi-D" , Salt = "3E8.rkwG7P7z1/mEoPSTJ1M1Dg==", PasswordHash = "+rr+++2jI+UQaDuSZwgTEysWfuWqNEqI" },
                    new { Password = "gK([}&>?kk" , Salt = "3E8.4uRF4JJCaYswDDfZ07eemw==", PasswordHash = "iBLhPc01Hiumv/tnlQ4oRbcdmD4Voo9t" },
                    new { Password = "#MQrt>a*79" , Salt = "3E8.DWMH0NKBdJp7LMBKxuFthA==", PasswordHash = "LoKZeppR3KWBVJiouSv8JJKSmlP1kzfi" },
                    new { Password = "*@5^FZ0WaH" , Salt = "3E8.ubjuYm8o1bWCrlR7ntx8nA==", PasswordHash = "oxstLorouWjEvV5X2E7axS8I7k7s8KvU" },
                    new { Password = "y#N#B0d.Ck" , Salt = "3E8.mRxYnyb+QDHAHJ1F8qZhjA==", PasswordHash = "BuMTxxf5DzeFh3/bWfDvo/XUMvyOu8H6" },
                    new { Password = ":K3;y)L9ar" , Salt = "3E8.Dcu+Oc7IHX415nH0bATzqQ==", PasswordHash = "qB+zo2fBvZRg7FocxWaESP+w8FsFVGSy" },
                    new { Password = "vdRq!(z6y%" , Salt = "3E8.8QUAO8JoSAPx1gpnCIMJbw==", PasswordHash = "GAwwADgOpDAePoxZ70ojalPbrH8JsXFo" },
                    new { Password = "8;}nxLhK?s" , Salt = "3E8.capx3WFlw5J+pqUsRLaRdA==", PasswordHash = "VvTPOW3oRTMcL1ff6jPUnqy4k3DnssgP" },
                    new { Password = "l#*GM2Pu+z" , Salt = "3E8.uEMUXoS7qTpblhzFv1S0JA==", PasswordHash = "6srd0BOvScIv5pHEaz/6SBxqfnbD/Aj+" },
                    new { Password = "&B%LTJMvE(" , Salt = "3E8.6EGG+CUBD2kypxGMd2Bfww==", PasswordHash = "gD8br/AOUcekDI8uppuj3eg4UZ+zJUFl" },
                    new { Password = "Fh6En^{Q-V" , Salt = "3E8.f4JCvzZBZMqFxWCH+5SnCQ==", PasswordHash = "rSESi1Jd+6vFSEBhiHrPgvPeY2Rne2zP" },
                    new { Password = "u3%m_9)tyC" , Salt = "3E8.PqBcodsZK3hI+WJTmXWrqQ==", PasswordHash = "eyZeYYgn44kFshlfYqX1bm000rbm9kpe" },
                    new { Password = "#IJDn?{x[:" , Salt = "3E8.JRyv83+JMPbrR0Of8q5Cxw==", PasswordHash = "/gvcs+WjtktQ0Ifvkhwod0DrEs8/9DLc" },
                    new { Password = "s2]!V8_6gC" , Salt = "3E8.kbxD0uNuqFhEIg8zEFNUaw==", PasswordHash = "EF/qwTAB6IhGHO8VVnHasF2+QZxKk8yM" },
                    new { Password = "T*H;:6d5Nb" , Salt = "3E8.ea8jdXwCg+3W9Q9E5dmADw==", PasswordHash = "X4h4K4xGAnRtdo7Hrz5vmDwSj+ZptiBb" },
                    new { Password = "T!Mo:ZvB_P" , Salt = "3E8.2bMfxzHiwBbrggSLb4Bh+A==", PasswordHash = "j1WFljg6bpofn/EqHcA/JaATgAcS0hFd" },
                    new { Password = "I@)1Lha-Wp" , Salt = "3E8.m5nDALccDY0EsCqtaKRJpQ==", PasswordHash = "ADx9S8KdEaJqGMqTqfknfGfMEWQftz31" },
                    new { Password = "*rul:j>mHy" , Salt = "3E8.QmOCTXiAMF3X9ucEy2aESw==", PasswordHash = "G/6L3pA/2NJFLBlCqvUmA5u7deSFNf4b" },
                };

                foreach (var passwordToCheck in passwordsToCheck)
                {
                    Assert.That(EncryptionHelper.ValidatePBKDF2Hash(passwordToCheck.Password, passwordToCheck.PasswordHash, passwordToCheck.Salt));
                }

        }
        [Test]
        public void EncryptionAndDecryptionTest() 
        {

            Assert.AreEqual("", EncryptionHelper.Encrypt(null), "Encryption of null.");

            string[] data = {"The quick brown fox jumps over the lazy dog.", "1234567890!@#$%^&*()<>", ""};
 
            foreach (string s in data) 
            {
                string encrypt = EncryptionHelper.Encrypt(s);
                string decrypt = EncryptionHelper.Decrypt(encrypt);
                Assert.AreEqual(s, decrypt, s);
            }

        }

        [Test]
        public void EncryptionAndDecryptionCompatibilityTest() 
        {
            // 10/22/2008 dd - This test will make sure no change in the encrypt method that will
            // break the existing encrypted data.
            string[][] data = 
            {
                new string[] {"The quick brown fox jumps over the lazy dog.", "coFCQ4OjD6nfe8BnR8xpBz6cFii6Unv4TDWju1NNbbg0TMmJzR0bPGmC/p4MbP3w"},
                new string[] {"1234567890!@#$%^&*()<>", "+za1Y8KmK3oxn8+E0Xv7FsMsxHL7eioL4slgCuoMqdA="},
            };

            foreach (string[] s in data) 
            {
                Assert.AreEqual(s[1], EncryptionHelper.Encrypt(s[0]), s[0]);
            }
        }

        [Test]
        public void EncryptionAndDecryptionMeritMatrixTest() 
        {
            Assert.AreEqual("", EncryptionHelper.EncryptMeritMatrix(null), "Encryption of null.");

            string[] data = {"The quick brown fox jumps over the lazy dog.", "1234567890!@#$%^&*()<>", ""};
 
            foreach (string s in data) 
            {
                string encrypt = EncryptionHelper.EncryptMeritMatrix(s);
                string decrypt = EncryptionHelper.DecryptMeritMatrix(encrypt);

                Assert.AreEqual(s, decrypt, s);
            }
        }

        [Test]
        [Category(Common.CategoryConstants.IntegrationTest)]
        public void EncryptionAndDecryptionThirdPartyTest()
        {
            Assert.IsNull(EncryptionHelper.EncryptThirdParty(null), "Encryption of null.");

            string[] data = { "Time flies like an arrow, fruit flies like a banana.", "1234567890!@#$%^&*()<>" };

            foreach (string s in data)
            {
                var encrypt = EncryptionHelper.EncryptThirdParty(s);
                string decrypt = EncryptionHelper.DecryptThirdParty(encrypt);

                Assert.AreEqual(s, decrypt, s);
            }
        }

        [Test]
        [Category(Common.CategoryConstants.IntegrationTest)]
        public void Boundary_DocFramework_Null()
        {
            string data = null;

            var encryped = EncryptionHelper.EncryptDocumentFramework(data);
            var decrypted = EncryptionHelper.DecryptDocumentFramework(encryped);

            Assert.IsTrue(string.IsNullOrEmpty(decrypted));
        }

        [Test]
        [Category(Common.CategoryConstants.IntegrationTest)]
        public void Boundary_DocFramework_Empty()
        {
            string data = string.Empty;

            var encryped = EncryptionHelper.EncryptDocumentFramework(data);
            var decrypted = EncryptionHelper.DecryptDocumentFramework(encryped);

            Assert.IsTrue(string.IsNullOrEmpty(decrypted));
        }

        [Test]
        [Category(Common.CategoryConstants.IntegrationTest)]
        public void Boundary_ThirdParty_Null()
        {
            string data = null;

            var encryped = EncryptionHelper.EncryptThirdParty(data);
            var decrypted = EncryptionHelper.DecryptThirdParty(encryped);

            Assert.IsTrue(string.IsNullOrEmpty(decrypted));
        }

        [Test]
        [Category(Common.CategoryConstants.IntegrationTest)]
        public void Boundary_ThirdParty_Empty()
        {
            string data = string.Empty;

            var encryped = EncryptionHelper.EncryptThirdParty(data);
            var decrypted = EncryptionHelper.DecryptThirdParty(encryped);

            Assert.IsTrue(string.IsNullOrEmpty(decrypted));
        }

        [Test]
        [Category(Common.CategoryConstants.IntegrationTest)]
        public void Boundary_MeritMatrix_Null()
        {
            string data = null;

            var encryped = EncryptionHelper.EncryptMeritMatrix(data);
            var decrypted = EncryptionHelper.DecryptMeritMatrix(encryped);

            Assert.IsTrue(string.IsNullOrEmpty(decrypted));
        }

        [Test]
        [Category(Common.CategoryConstants.IntegrationTest)]
        public void Boundary_MeritMatrix_Empty()
        {
            string data = string.Empty;

            var encryped = EncryptionHelper.EncryptMeritMatrix(data);
            var decrypted = EncryptionHelper.DecryptMeritMatrix(encryped);

            Assert.IsTrue(string.IsNullOrEmpty(decrypted));
        }

        [Test]
        [Category(Common.CategoryConstants.IntegrationTest)]
        public void Boundary_MeritMatrix_BOM()
        {
            string data = AddBOM(" ");

            var encryped = EncryptionHelper.EncryptMeritMatrix(data);
            var decrypted = EncryptionHelper.DecryptMeritMatrix(encryped);

            Assert.IsTrue(string.IsNullOrEmpty(decrypted));
        }

        [Test]
        [Category(Common.CategoryConstants.IntegrationTest)]
        public void Boundary_ConnectionString_Null()
        {
            string data = null;

            var encryped = EncryptionHelper.EncryptConnectionString(data);
            var decrypted = EncryptionHelper.DecryptConnectionString(encryped);

            Assert.IsTrue(string.IsNullOrEmpty(decrypted));
        }

        [Test]
        [Category(Common.CategoryConstants.IntegrationTest)]
        public void Boundary_ConnectionString_Empty()
        {
            string data = string.Empty;

            var encryped = EncryptionHelper.EncryptConnectionString(data);
            var decrypted = EncryptionHelper.DecryptConnectionString(encryped);

            Assert.IsTrue(string.IsNullOrEmpty(decrypted));
        }

        private string AddBOM(string data)
        {
            char bom = Convert.ToChar(0xfeff);
            return bom + data;
        }

        [Test]
        public void MD5ChecksumTest()
        {
            var testCaseList = new[] {
                new { Data="The quick brown fox jumps over the lazy dog.", Checksum="e4d909c290d0fb1ca068ffaddf22cbd0"},
                new { Data="LendingQB - The Lean Lending Way", Checksum="6b151779741303740879923fbb3dcdcd"},
            };

            foreach (var o in testCaseList)
            {
                Assert.AreEqual(o.Checksum, EncryptionHelper.ComputeMD5Hash(o.Data), o.Data);
            }
        }


        [Test]
        public void MD5ChecksumTestList()
        {
            string a = "asd";
            string b = "NHqzq&9;:X";
            string c = "asjkdhasjkldh asda hslj90-9;l;k;kl;*rul:j>mHy@^#&&@^#81kdh ajksld ";

            Assert.AreEqual(EncryptionHelper.ComputeMD5Hash(a).ToUpper(), EncryptionHelper.ComputeMD5Hash(new string[] { a }, false), "1");
            Assert.AreEqual(EncryptionHelper.ComputeMD5Hash(a + b).ToUpper(), EncryptionHelper.ComputeMD5Hash(new string[] { a, b }, false), "2");
            Assert.AreEqual(EncryptionHelper.ComputeMD5Hash(a + c).ToUpper(), EncryptionHelper.ComputeMD5Hash(new string[] { a, c }, false),"3");
            Assert.AreEqual(EncryptionHelper.ComputeMD5Hash(a + b + c).ToUpper(), EncryptionHelper.ComputeMD5Hash(new string[] { a, b, c }, false), "4");


            Assert.AreEqual(EncryptionHelper.ComputeMD5Hash(a.ToLower()).ToUpper(), EncryptionHelper.ComputeMD5Hash(new string[] { a }, true), "5");
            Assert.AreEqual(EncryptionHelper.ComputeMD5Hash(a.ToLower() + b.ToLower()).ToUpper(), EncryptionHelper.ComputeMD5Hash(new string[] { a, b }, true), "6");
            Assert.AreEqual(EncryptionHelper.ComputeMD5Hash(a.ToLower() + c.ToLower()).ToUpper(), EncryptionHelper.ComputeMD5Hash(new string[] { a, c }, true), "7");
            Assert.AreEqual(EncryptionHelper.ComputeMD5Hash(a.ToLower() + b.ToLower() + c.ToLower()).ToUpper(), EncryptionHelper.ComputeMD5Hash(new string[] { a, b, c }, true), "8"); 
        }
	}
}
