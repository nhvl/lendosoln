﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using LendersOffice.Common;

namespace LendOSolnTest.CommonTools
{
    [TestFixture]
    public class CompressionHelperTest
    {
        [Test]
        public void Test()
        {
            Assert.IsNull(CompressionHelper.Compress(null));
            Assert.IsNull(CompressionHelper.Compress(string.Empty));


            string[] testCases = {
                                     "<xml><blah>TEST</blah></xml>",
                                     "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuv0123456789",
                                 };

            foreach (string testCase in testCases)
            {
                byte[] bytes = CompressionHelper.Compress(testCase);

                string decompressString = CompressionHelper.Decompress(bytes);

                Assert.AreEqual(testCase, decompressString);
            }
        }
    }
}
