/// Author: David Dao

using System;
using System.Collections;
using NUnit.Framework;

using DataAccess;

namespace LendOSolnTest.CommonTools
{
    [TestFixture]
	public class FriendlyFileIdGeneratorTest
	{
        [Test]
        public void UniquenessTest() 
        {
            // 10/22/2008 dd - Test to make sure it does not generate duplicate id.
            Hashtable hash = new Hashtable();

            CPmlFIdGenerator generator = new CPmlFIdGenerator();
            for (int i = 0; i < 1000; i++) 
            {
                // 10/22/2008 dd - Since it is not possible to test all the possibility, I will create
                // X random id and test for uniqueness.
                
                string id = generator.GenerateNewFriendlyId();
                if (hash.ContainsKey(id)) 
                {
                    Assert.Fail("Duplicate ID found.");
                } 
                hash.Add(id, id);

            }
        }
	}
}
