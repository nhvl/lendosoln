/// Author: David Dao

using System;
using System.IO;
using LendOSolnTest.Common;
using LendersOffice.Common;
using NUnit.Framework;

using System.Xml.Xsl;

namespace LendOSolnTest.CommonTools
{
    [TestFixture]
	public class XslTransformHelperTest
	{

        private static string XsltTestFile = TestDataRootFolder.Location + "xsl/XsltTest_1234567.xslt";

        [TestFixtureTearDown]
        public void TearDown() 
        {
            if (File.Exists(XsltTestFile))
                File.Delete(XsltTestFile);
        }
        [Test]
        public void Test0() 
        {
            CopyXsltToTestLocation("XsltTest001.xslt");

            string xml = "<item attr0=\"Hello\"></item>";
            string expectedOutputFormat = "<?xml version=\"1.0\" encoding=\"utf-16\"?>\r\n  {0}::Param0:{1};Param1:{2};Attr_0:Hello";

            XsltArgumentList args = new XsltArgumentList();
            args.AddParam("Param0", "", "Param0Value");
            args.AddParam("Param1", "", "Param1Value");

            string output = XslTransformHelper.Transform(XsltTestFile, xml, args);

            string expectedOutput = string.Format(expectedOutputFormat, "Version_1", "Param0Value", "Param1Value");

            Assert.AreEqual(expectedOutput, output);

            args = new XsltArgumentList();
            args.AddParam("Param0", "", "Param0Value_1");
            args.AddParam("Param1", "", "Param1Value_1");

            output = XslTransformHelper.Transform(XsltTestFile, xml, args);
            expectedOutput = string.Format(expectedOutputFormat, "Version_1", "Param0Value_1", "Param1Value_1");
            Assert.AreEqual(expectedOutput, output);

            // Now pretend Xslt file is update.
            CopyXsltToTestLocation("XsltTest002.xslt");
            args = new XsltArgumentList();
            args.AddParam("Param0", "", "Param0Value_2");
            args.AddParam("Param1", "", "Param1Value_2");

            output = XslTransformHelper.Transform(XsltTestFile, xml, args);
            expectedOutput = string.Format(expectedOutputFormat, "Version_2", "Param0Value_2", "Param1Value_2");
            Assert.AreEqual(expectedOutput, output);

        }

        private void CopyXsltToTestLocation(string xslFile) 
        {
            File.Copy(TestDataRootFolder.Location + "xsl/" + xslFile, XsltTestFile, true);
            File.SetLastWriteTime(XsltTestFile, DateTime.Now);
            System.Threading.Thread.Sleep(2000); // Sleep for 2 seconds to avoid 2 files has same timestamp.
        }
	}
}
