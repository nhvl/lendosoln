﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using LendersOffice.Common;
using DataAccess;

namespace LendOSolnTest.CommonTools
{
    [TestFixture]
    public class BankingHolidayTest
    {
        [Test]
        public void TestListHolidays2010()
        {
            var expectedDates = new[] {
                new { Date = new DateTime(2010, 1, 1), Type=HolidayType.NewYearDay}
                , new { Date = new DateTime(2010, 1, 18), Type= HolidayType.MartinLutherKingDay}
                , new { Date = new DateTime(2010, 2, 15), Type=HolidayType.PresidentsDay}
                , new { Date = new DateTime(2010, 5, 31), Type= HolidayType.MemorialDay}
                , new { Date = new DateTime(2010, 7, 4), Type = HolidayType.IndependenceDay}
                , new { Date = new DateTime(2010, 9, 6), Type = HolidayType.LaborDay}
                , new { Date = new DateTime(2010, 10, 11), Type = HolidayType.ColumbusDay}
                , new { Date = new DateTime(2010, 11, 11), Type = HolidayType.VeteransDay}
                , new { Date = new DateTime(2010, 11, 25), Type = HolidayType.ThanksgivingDay}
                , new { Date = new DateTime(2010, 12, 25), Type = HolidayType.ChristmasDay}
            };
            int i = 0;
            foreach (BankingHoliday holiday in BankingHoliday.GetHolidayDaysInYear(2010))
            {
                var expectedDate = expectedDates[i++];
                Assert.AreEqual(expectedDate.Date, holiday.Date, "Date");
                Assert.AreEqual(expectedDate.Type, holiday.Type, "Type");
            }
        }
        [Test]
        public void TestListHolidays2011()
        {
            var expectedDates = new[] {
                new { Date = new DateTime(2011, 1, 1), Type=HolidayType.NewYearDay}
                , new { Date = new DateTime(2011, 1, 17), Type= HolidayType.MartinLutherKingDay}
                , new { Date = new DateTime(2011, 2, 21), Type=HolidayType.PresidentsDay}
                , new { Date = new DateTime(2011, 5, 30), Type= HolidayType.MemorialDay}
                , new { Date = new DateTime(2011, 7, 4), Type = HolidayType.IndependenceDay}
                , new { Date = new DateTime(2011, 9, 5), Type = HolidayType.LaborDay}
                , new { Date = new DateTime(2011, 10, 10), Type = HolidayType.ColumbusDay}
                , new { Date = new DateTime(2011, 11, 11), Type = HolidayType.VeteransDay}
                , new { Date = new DateTime(2011, 11, 24), Type = HolidayType.ThanksgivingDay}
                , new { Date = new DateTime(2011, 12, 25), Type = HolidayType.ChristmasDay}
            };
            int i = 0;
            foreach (BankingHoliday holiday in BankingHoliday.GetHolidayDaysInYear(2011))
            {
                var expectedDate = expectedDates[i++];
                Assert.AreEqual(expectedDate.Date, holiday.Date, "Date");
                Assert.AreEqual(expectedDate.Type, holiday.Type, "Type");
            }
        }
        [Test]
        public void TestListHolidays2012()
        {
            var expectedDates = new[] {
                new { Date = new DateTime(2012, 1, 1), Type=HolidayType.NewYearDay}
                , new { Date = new DateTime(2012, 1, 16), Type= HolidayType.MartinLutherKingDay}
                , new { Date = new DateTime(2012, 2, 20), Type=HolidayType.PresidentsDay}
                , new { Date = new DateTime(2012, 5, 28), Type= HolidayType.MemorialDay}
                , new { Date = new DateTime(2012, 7, 4), Type = HolidayType.IndependenceDay}
                , new { Date = new DateTime(2012, 9, 3), Type = HolidayType.LaborDay}
                , new { Date = new DateTime(2012, 10, 8), Type = HolidayType.ColumbusDay}
                , new { Date = new DateTime(2012, 11, 11), Type = HolidayType.VeteransDay}
                , new { Date = new DateTime(2012, 11, 22), Type = HolidayType.ThanksgivingDay}
                , new { Date = new DateTime(2012, 12, 25), Type = HolidayType.ChristmasDay}
            };
            int i = 0;
            foreach (BankingHoliday holiday in BankingHoliday.GetHolidayDaysInYear(2012))
            {
                var expectedDate = expectedDates[i++];
                Assert.AreEqual(expectedDate.Date, holiday.Date, "Date");
                Assert.AreEqual(expectedDate.Type, holiday.Type, "Type");
            }
        }
        [Test]
        public void TestListHolidays2013()
        {
            var expectedDates = new[] {
                new { Date = new DateTime(2013, 1, 1), Type=HolidayType.NewYearDay}
                , new { Date = new DateTime(2013, 1, 21), Type= HolidayType.MartinLutherKingDay}
                , new { Date = new DateTime(2013, 2, 18), Type=HolidayType.PresidentsDay}
                , new { Date = new DateTime(2013, 5, 27), Type= HolidayType.MemorialDay}
                , new { Date = new DateTime(2013, 7, 4), Type = HolidayType.IndependenceDay}
                , new { Date = new DateTime(2013, 9, 2), Type = HolidayType.LaborDay}
                , new { Date = new DateTime(2013, 10, 14), Type = HolidayType.ColumbusDay}
                , new { Date = new DateTime(2013, 11, 11), Type = HolidayType.VeteransDay}
                , new { Date = new DateTime(2013, 11, 28), Type = HolidayType.ThanksgivingDay}
                , new { Date = new DateTime(2013, 12, 25), Type = HolidayType.ChristmasDay}
            };
            int i = 0;
            foreach (BankingHoliday holiday in BankingHoliday.GetHolidayDaysInYear(2013))
            {
                var expectedDate = expectedDates[i++];
                Assert.AreEqual(expectedDate.Date, holiday.Date, "Date");
                Assert.AreEqual(expectedDate.Type, holiday.Type, "Type");
            }
        }

        [Test]
        public void TestListHolidays2014()
        {
            var expectedDates = new[] {
                new { Date = new DateTime(2014, 1, 1), Type=HolidayType.NewYearDay}
                , new { Date = new DateTime(2014, 1, 20), Type= HolidayType.MartinLutherKingDay}
                , new { Date = new DateTime(2014, 2, 17), Type=HolidayType.PresidentsDay}
                , new { Date = new DateTime(2014, 5, 26), Type= HolidayType.MemorialDay}
                , new { Date = new DateTime(2014, 7, 4), Type = HolidayType.IndependenceDay}
                , new { Date = new DateTime(2014, 9, 1), Type = HolidayType.LaborDay}
                , new { Date = new DateTime(2014, 10, 13), Type = HolidayType.ColumbusDay}
                , new { Date = new DateTime(2014, 11, 11), Type = HolidayType.VeteransDay}
                , new { Date = new DateTime(2014, 11, 27), Type = HolidayType.ThanksgivingDay}
                , new { Date = new DateTime(2014, 12, 25), Type = HolidayType.ChristmasDay}
            };
            int i = 0;
            foreach (BankingHoliday holiday in BankingHoliday.GetHolidayDaysInYear(2014))
            {
                var expectedDate = expectedDates[i++];
                Assert.AreEqual(expectedDate.Date, holiday.Date, "Date");
                Assert.AreEqual(expectedDate.Type, holiday.Type, "Type");
            }
        }

        [Test]
        public void TestAddingNegativeBankingDays()
        {
            // 3/23/2010 dd - Not support negative banking days now.
            try
            {
                BankingHoliday.AddWorkingBankDays(new DateTime(1, 1, 2010), -2, false);
                Assert.Fail(); // 4/2/2010 dd - Expected Exception
            }
            catch
            {
            }
        }
        [Test]
        public void TestAddBankingDays()
        {
            var testCases = new[] {
                    new { StartedDate = "2/12/2010", NumDays = 4, ExpectedDate = "2/18/2010"}
                    , new { StartedDate = "12/29/2012", NumDays = 4, ExpectedDate = "1/4/2013"}
                    , new { StartedDate = "7/2/2010", NumDays = 4, ExpectedDate = "7/7/2010"}
                    , new { StartedDate = "3/31/2010", NumDays = 4, ExpectedDate = "4/5/2010"}
                    , new { StartedDate = "11/22/2010", NumDays = 3, ExpectedDate = "11/26/2010"}
                    , new { StartedDate = "11/21/2010", NumDays = 4, ExpectedDate = "11/26/2010"}
                    , new { StartedDate = "11/22/2010", NumDays = 0, ExpectedDate = "11/22/2010"}
            };
            foreach (var o in testCases)
            {
                DateTime dt = DateTime.Parse(o.StartedDate);

                DateTime expectedDate = DateTime.Parse(o.ExpectedDate);

                Assert.AreEqual(expectedDate, BankingHoliday.AddWorkingBankDays(dt, o.NumDays, false), o.ToString());
            }
        }

        [Test]
        public void TestPreviousBusinessDays()
        {
            var testCases = new[] {
                    new { StartedDate = "11/22/2010", NumDays = 0, ExpectedDate = "11/22/2010"}
                    , new { StartedDate = "11/17/2010", NumDays = 1, ExpectedDate = "11/16/2010"}
                    , new { StartedDate = "11/17/2010", NumDays = 2, ExpectedDate = "11/15/2010"}
                    , new { StartedDate = "11/17/2010", NumDays = 3, ExpectedDate = "11/12/2010"}
                    , new { StartedDate = "11/17/2010", NumDays = 4, ExpectedDate = "11/10/2010"} // 11/11 is veteran days.
            };
            foreach (var o in testCases)
            {
                DateTime dt = DateTime.Parse(o.StartedDate);

                DateTime expectedDate = DateTime.Parse(o.ExpectedDate);

                Assert.AreEqual(expectedDate, BankingHoliday.GetDateByPreviousBusinessDays(dt, o.NumDays, false, Guid.Empty), o.ToString());
            }

        }

    }
}
