﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using LendersOffice.Common;

namespace LendOSolnTest.CommonTools
{
    [TestFixture]
    public class EmailUtilitiesTest
    {
        [Test]
        public void TestIsErrorMessageLastSendExceed()
        {
            string errorMessage = "UNIT_TEST_ERR_MSG_" + Guid.NewGuid();
            int currentCount = 0;
            bool isNewError = false;
            DateTime lastEmailedToOpmDate = DateTime.MinValue;
            DateTime lastEmailedToCriticalDate = DateTime.MinValue;
            Assert.AreEqual(true, EmailUtilities.IsErrorMessageLastSendExceed(errorMessage, 10, out currentCount, out isNewError, out lastEmailedToOpmDate, out lastEmailedToCriticalDate), "Initial Insert of Error Message");
            Assert.AreEqual(1, currentCount);
            Assert.AreEqual(true, isNewError);
            for (int i = 0; i < 100; i++)
            {
                Assert.AreEqual(false, EmailUtilities.IsErrorMessageLastSendExceed(errorMessage, 10, out currentCount, out isNewError, out lastEmailedToOpmDate, out lastEmailedToCriticalDate), "Subsequence Check of error message");
                Assert.AreEqual(i+2, currentCount);
                Assert.AreEqual(false, isNewError);

            }
            
        }
    }
}
