﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using LendersOffice.Common;

namespace LendOSolnTest.CommonTools
{
    [TestFixture]
    public class DependencySetTest
    {
        [Test]
        public void TestDependOn()
        {
            DependencySet<string> set = new DependencySet<string>(StringComparer.OrdinalIgnoreCase);

            set.Add("Zero", new string[] { "0" });
            set.Add("Odd", new string[] { "1", "3", "5", "7", "9" });
            set.Add("Even", new string[] { "2", "4", "6", "8", "10" });
            set.Add("Multiple3", new string[] { "3", "6", "9" });
            set.Add("A", new string[] { "A0", "A1", "A2", "B" });
            set.Add("B", new string[] { "B0", "B1", "B2" });
            set.Add("C", new string[] { "A", "B" });
            set.Add("D", new string[] { "D", "A"});
            set.Add("E", new string[] { "E" });


            var testCaseList = new[] {
                new { Name = new string[] { "Zero"}, Expected = new string[] { "0"}},
                new { Name = new string[] { "Odd" }, Expected = new string[] { "1", "3", "5", "7", "9" }},
                new { Name = new string[] { "Unknown" }, Expected = new string[] {}},
                new { Name = new string[] { "Odd", "Even"}, Expected = new string[] { "1", "3", "5", "7", "9", "2", "4", "6", "8", "10"}},
                new { Name = new string[] {"Odd", "Multiple3"}, Expected = new string[] {"1", "3", "5", "7", "9", "6"}},
                
                new { Name = new string[] {"C"}, Expected = new string[] {"A0", "A1", "A2", "B", "B0", "B1", "B2", "A"}},
                new { Name= new string[] {"D"}, Expected = new string[] { "D", "A", "A0", "A1", "A2", "B", "B0", "B1", "B2"}},
                new { Name = new string[] {"C", "D"}, Expected = new string[] { "A0", "A1", "A2", "B", "B0", "B1", "B2", "A", "D"}}
            };

            foreach (var testCase in testCaseList)
            {
                var result = set.GetDependsOn(testCase.Name);
                Assert.That(result, Is.EquivalentTo(testCase.Expected), string.Join(", ", testCase.Name));
            }
        }


        [Test]
        public void TestUsedBy()
        {
            DependencySet<string> set = new DependencySet<string>(StringComparer.OrdinalIgnoreCase);

            set.Add("Zero", new string[] { "0" });
            set.Add("Odd", new string[] { "1", "3", "5", "7", "9" });
            set.Add("Even", new string[] { "2", "4", "6", "8", "10" });
            set.Add("Multiple3", new string[] { "3", "6", "9" });
            set.Add("A", new string[] { "A0", "A1", "A2", "B" });
            set.Add("B", new string[] { "B0", "B1", "B2" });
            set.Add("C", new string[] { "A", "B" });
            set.Add("D", new string[] { "D", "A" });
            set.Add("E", new string[] { "E" });


            var testCaseList = new[] {
                new { Name = new string[] { "0"}, Expected = new string[] { "Zero"}},
                new { Name = new string[] { "3" }, Expected = new string[] { "Odd", "Multiple3" }},
                new { Name = new string[] { "100" }, Expected = new string[] {}},
                new { Name = new string[] { "0", "1", "2", "3" }, Expected = new string[] {"Zero", "Odd", "Even", "Multiple3"}},
                
                new { Name = new string[] {"C"}, Expected = new string[] {}},
                new { Name= new string[] {"D"}, Expected = new string[] { "D"}},
                new { Name = new string[] {"B"}, Expected = new string[] { "C", "A", "D"}},
                new { Name = new string[] {"B2"}, Expected = new string[] {"B", "A", "C", "D"}}

            };

            foreach (var testCase in testCaseList)
            {
                var result = set.GetUsedBy(testCase.Name);
                Assert.That(result, Is.EquivalentTo(testCase.Expected), string.Join(", ", testCase.Name));
            }
        }

        [Test]
        public void TestTreeGetDescendants()
        {
            //                            Root
            // Level_1_A0                             Level_1_A1
            // Level_2_A0_B0 Level_2_A0_B1         Level_2_A1_B0  Level_2_A1_B1

            DependencySet<string> set = new DependencySet<string>(StringComparer.OrdinalIgnoreCase);

            set.Add("Root", new string[] { "Level_1_A0", "Level_1_A1" });
            set.Add("Level_1_A0", new string[] { "Level_2_A0_B0", "Level_2_A0_B1" });
            set.Add("Level_1_A1", new string[] { "Level_2_A1_B0", "Level_2_A1_B1" });


            var testCaseList = new[] {
                new { Node="Root", Expected= new string[] {"Level_1_A0", "Level_1_A1", "Level_2_A0_B0", "Level_2_A0_B1","Level_2_A1_B0", "Level_2_A1_B1"}},
                new { Node="Level_1_A0", Expected=new string[] {"Level_2_A0_B0", "Level_2_A0_B1"}},
                new { Node="Level_1_A1", Expected=new string[] {"Level_2_A1_B0", "Level_2_A1_B1"}},
                new { Node="Level_2_A0_B0", Expected=new string[] {}}
            };

            foreach (var testCase in testCaseList)
            {
                var result = set.GetDependsOn(new string[] { testCase.Node });
                Assert.That(result, Is.EquivalentTo(testCase.Expected), testCase.Node);
            }
        }
        [Test]
        public void TestTreeGetAncestors()
        {
            //                            Root
            // Level_1_A0                             Level_1_A1
            // Level_2_A0_B0 Level_2_A0_B1         Level_2_A1_B0  Level_2_A1_B1

            DependencySet<string> set = new DependencySet<string>(StringComparer.OrdinalIgnoreCase);

            set.Add("Root", new string[] { "Level_1_A0", "Level_1_A1" });
            set.Add("Level_1_A0", new string[] { "Level_2_A0_B0", "Level_2_A0_B1" });
            set.Add("Level_1_A1", new string[] { "Level_2_A1_B0", "Level_2_A1_B1" });


            var testCaseList = new[] {
                new { Node="Root", Expected= new string[] {}},
                new { Node="Level_1_A0", Expected=new string[] {"Root"}},
                new { Node="Level_1_A1", Expected=new string[] {"Root"}},
                new { Node="Level_2_A0_B0", Expected=new string[] {"Level_1_A0", "Root"}}
            };

            foreach (var testCase in testCaseList)
            {
                var result = set.GetUsedBy(new string[] { testCase.Node });
                Assert.That(result, Is.EquivalentTo(testCase.Expected), testCase.Node);
            }
        }

        [Test]
        public void TestTreeDuplication()
        {
            // A -> A0, A0, A1, A2
            // A -> A3, A2
            // B -> B0, B0
            DependencySet<string> set = new DependencySet<string>(StringComparer.OrdinalIgnoreCase);

            set.Add("A", new string[] { "A0", "a0", "A1", "A2" });
            set.Add("a", new string[] { "A3", "A2" });
            set.Add("B", new string[] { "B0", "B0" });

            var testCaseList = new[] {
                new { Node="A", Expected = new string[] {"A0", "A1", "A2", "A3"}},
                new { Node="B", Expected = new string[] {"B0"}}
            };

            foreach (var testCase in testCaseList)
            {
                var result = set.GetDependsOn(new string[] { testCase.Node });
                Assert.That(result, Is.EquivalentTo(testCase.Expected), testCase.Node);
            }
        }

        [Test]
        public void TestIntDependency()
        {
            // 1 --> 10, 11, 12
            // 2 -> 20, 21, 22
            // 20 --> 200, 201, 202
            // 20 --> 200, 204

            DependencySet<int> set = new DependencySet<int>();
            set.Add(1, new int[] { 10, 11, 12 });
            set.Add(2, new int[] { 20, 21, 22 });
            set.Add(20, new int[] { 200, 201, 202 });
            set.Add(20, new int[] { 200, 204 });

            var testCaseList = new[] {
                new { Node=1, Expected = new int[] { 10, 11, 12}},
                new { Node=2, Expected = new int[] { 20, 21, 22, 200, 201, 202, 204}}

            };
            foreach (var testCase in testCaseList)
            {
                var result = set.GetDependsOn(new int[] { testCase.Node });
                Assert.That(result, Is.EquivalentTo(testCase.Expected), testCase.Node.ToString());
            }
        }

    }
}
