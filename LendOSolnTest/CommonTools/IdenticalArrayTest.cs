﻿namespace LendOSolnTest.CommonTools
{
    using DataAccess;
    using NUnit.Framework;

    [TestFixture]
    public class IdenticalArrayTest
    {
        [Test]
        public void BothNull_ReturnsTrue()
        {
            byte[] first = null;
            byte[] second = null;

            var result = Tools.AreArraysIdentical<byte>(first, second);
            Assert.IsTrue(result);
        }

        [Test]
        public void OneNull_ReturnsFalse()
        {
            byte[] first = null;
            byte[] second = new byte[] { 1, 2, 3 };

            var result = Tools.AreArraysIdentical<byte>(first, second);
            Assert.IsFalse(result);

            first = new byte[] { 1, 2, 3 };
            second = null;

            result = Tools.AreArraysIdentical<byte>(first, second);
            Assert.IsFalse(result);
        }

        [Test]
        public void BothEmpty_ReturnsTrue()
        {
            byte[] first = new byte[0];
            byte[] second = new byte[0];

            var result = Tools.AreArraysIdentical<byte>(first, second);
            Assert.IsTrue(result);
        }

        [Test]
        public void DifferentLength_ReturnsFalse()
        {
            byte[] first = new byte[] { 1 };
            byte[] second = new byte[] { 2, 3 };

            var result = Tools.AreArraysIdentical<byte>(first, second);
            Assert.IsFalse(result);
        }

        [Test]
        public void DifferentValues_ReturnsFalse()
        {
            byte[] first = new byte[] { 4, 5 };
            byte[] second = new byte[] { 5, 6 };

            var result = Tools.AreArraysIdentical<byte>(first, second);
            Assert.IsFalse(result);
        }

        [Test]
        public void SameValues_ReturnsTrue()
        {
            byte[] first = new byte[] { 1, 2, 3, 4, 5 };
            byte[] second = new byte[] { 1, 2, 3, 4, 5 };

            var result = Tools.AreArraysIdentical<byte>(first, second);
            Assert.IsTrue(result);
        }
    }
}
