﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using LendersOffice.Common;
using DataAccess;


namespace LendOSolnTest.CommonTools
{
    
    public class SimplePerson
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
    [TestFixture]
    public class SerializationHelperTest
    {


        [Test]
        public void TestJsonSerializeNull()
        {
            string json = ObsoleteSerializationHelper.JsonSerialize<SimplePerson>(null);
            Assert.AreEqual(string.Empty, json);
        }

        [Test]
        public void TestJsonDeserializeNull()
        {
            SimplePerson person = ObsoleteSerializationHelper.JsonDeserialize<SimplePerson>(null);
            Assert.IsNull(person);

            person = ObsoleteSerializationHelper.JsonDeserialize<SimplePerson>(string.Empty);
            Assert.IsNull(person);
        }

        [Test]
        public void TestJsonSerializeAndDeserialize()
        {
            SimplePerson person = new SimplePerson() { ID = 5, FirstName = "David", LastName = "Dao" };

            string json = ObsoleteSerializationHelper.JsonSerialize(person);
            SimplePerson personNew = ObsoleteSerializationHelper.JsonDeserialize<SimplePerson>(json);

            Assert.AreEqual(person.ID, personNew.ID);
            Assert.AreEqual(person.FirstName, personNew.FirstName);
            Assert.AreEqual(person.LastName, personNew.LastName);

        }

        [Test]
        public void TestJsonSerializeAndDeserializeListOfObject()
        {
            List<SimplePerson> list = new List<SimplePerson>()
            {
                new SimplePerson() { ID=1, FirstName= "John", LastName="Doe"},
                new SimplePerson() { ID=2, FirstName="Mary", LastName="Doe"}
            };

            string json = ObsoleteSerializationHelper.JsonSerialize(list);

            List<SimplePerson> newList = ObsoleteSerializationHelper.JsonDeserialize<List<SimplePerson>>(json);

            Assert.AreEqual(list.Count, newList.Count);

            for (int i = 0; i < list.Count; i++)
            {
                var original = list[i];
                var newObject = newList[i];
                Assert.AreEqual(original.ID, newObject.ID);
                Assert.AreEqual(original.FirstName, newObject.FirstName);
                Assert.AreEqual(original.LastName, newObject.LastName);
            }
        }

        [Test]
        public void JsonSerializeAndSanitize_SimplePersonWithCleanData_SerializesWithoutIssue()
        {
            SimplePerson person = new SimplePerson() { ID = 5, FirstName = "David", LastName = "Dao" };
            string expectedJson = "{\"FirstName\":\"David\",\"ID\":5,\"LastName\":\"Dao\"}";

            string actualJson = ObsoleteSerializationHelper.JsonSerializeAndSanitize(person);

            Assert.AreEqual(expectedJson, actualJson);
        }

        [Test]
        public void JsonSerializeAndSanitize_SimplePersonWithInvalidData_RemovesInvalidCharacter()
        {
            SimplePerson person = new SimplePerson() { ID = 5, FirstName = "Dav\u0007id", LastName = "Dao" };
            string expectedJson = "{\"FirstName\":\"David\",\"ID\":5,\"LastName\":\"Dao\"}";

            string actualJson = ObsoleteSerializationHelper.JsonSerializeAndSanitize(person);

            Assert.AreEqual(expectedJson, actualJson);
        }

        [Test]
        public void JsonDeserializeAndSanitize_SimplePersonWithCleanData_DeserializesWithoutIssue()
        {
            string json = "{\"FirstName\":\"David\",\"ID\":5,\"LastName\":\"Dao\"}";

            SimplePerson person = ObsoleteSerializationHelper.JsonDeserializeAndSanitize<SimplePerson>(json);

            Assert.AreEqual(5, person.ID);
            Assert.AreEqual("David", person.FirstName);
            Assert.AreEqual("Dao", person.LastName);
        }

        [Test]
        public void JsonDeserializeAndSanitize_SimplePersonWithInvalidData_RemovesInvalidCharacter()
        {
            string json = "{\"FirstName\":\"Dav\u0007id\",\"ID\":5,\"LastName\":\"Dao\"}";

            SimplePerson person = ObsoleteSerializationHelper.JsonDeserializeAndSanitize<SimplePerson>(json);

            Assert.AreEqual(5, person.ID);
            Assert.AreEqual("David", person.FirstName);
            Assert.AreEqual("Dao", person.LastName);
        }

        [Test]
        public void SanitizeJsonString_CleanMinJson_IsUnchanged()
        {
            string json = "{\"FirstProp\":\"A prop value\",\"Second Prop\":5,\"children\":[]}";

            string sanitizedJson = SerializationHelper.SanitizeJsonString(json);

            Assert.AreEqual(json, sanitizedJson);
        }

        [Test]
        public void SanitizeJsonString_InvalidMinJson_RemovesInvalidCharacter()
        {
            string json = "{\"FirstProp\":\"A prop\u0007 value\",\"Second Prop\":5,\"children\":[]}";
            string expectJson = "{\"FirstProp\":\"A prop value\",\"Second Prop\":5,\"children\":[]}";

            string sanitizedJson = SerializationHelper.SanitizeJsonString(json);

            Assert.AreEqual(expectJson, sanitizedJson);
        }
    }
}
