﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using DataAccess;

namespace LendOSolnTest.CommonTools
{
    [TestFixture]
    public class ShortGuidTests
    {
        [Test]
        public void TestManyGuids()
        {
            for (int i = 0; i < 1000000; i++)
            {
                var g = Guid.NewGuid();
                string shortGuid = Tools.ShortenGuid( g );
                Guid decodedGuid = Tools.GetGuidFrom(shortGuid);
                Assert.That(g, Is.EqualTo(decodedGuid), shortGuid);
            }
        }
    }
}
