/// Author: David Dao

using System;
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using System.Linq;
using LendersOffice.Common;

namespace LendOSolnTest.CommonTools
{

    [TestFixture]
	public class MultiPartParserTest
	{
        [Test]
        public void Test1() 
        {
            string data = @"--ieoau._._+2_8_GoodLuck8.3-ds0d0J0S0Kl234324jfLdsjfdAuaoei--
Content-Disposition: form-data; name=""RI""; filename=""RI_File""

CONTENT 1

--ieoau._._+2_8_GoodLuck8.3-ds0d0J0S0Kl234324jfLdsjfdAuaoei--
Content-Disposition: form-data; name=""CI""; filename=""CI_File""

CONTENT 2

--ieoau._._+2_8_GoodLuck8.3-ds0d0J0S0Kl234324jfLdsjfdAuaoei----";


            var parts = MultiPartParser.Parse(data).ToList() ;

            Assert.AreEqual(2, parts.Count, "Number of Parts");

            MultiPartItem item1 = (MultiPartItem) parts[0];
            Assert.AreEqual("RI", item1.Name, "Item1 - Name");
            Assert.AreEqual("RI_File", item1.FileName, "Item1 - FileName");
            Assert.AreEqual("CONTENT 1", item1.Content, "Item1 - Content");

            MultiPartItem item2 = (MultiPartItem) parts[1];
            Assert.AreEqual("CI", item2.Name, "Item2 - Name");
            Assert.AreEqual("CI_File", item2.FileName, "Item2 - FileName");
            Assert.AreEqual("CONTENT 2", item2.Content, "Item2 - Content");
        }

        [Test]
        public void Test2() 
        {
            string data = @"--ieoau._._+2_8_GoodLuck8.3-ds0d0J0S0Kl234324jfLdsjfdAuaoei--
Content-type: text/plain; charset=UTF-8
Content-Disposition: form-data; name=""RI""; filename=""RI_File""

CONTENT 1

--ieoau._._+2_8_GoodLuck8.3-ds0d0J0S0Kl234324jfLdsjfdAuaoei--
Content-type: text/plain; charset=UTF-8
Content-Disposition: form-data; name=""CI""; filename=""CI_File""

CONTENT 2

--ieoau._._+2_8_GoodLuck8.3-ds0d0J0S0Kl234324jfLdsjfdAuaoei----";

            var parts = MultiPartParser.Parse(data).ToList();

            Assert.AreEqual(2, parts.Count, "Number of Parts");

            MultiPartItem item1 = (MultiPartItem) parts[0];
            Assert.AreEqual("RI", item1.Name, "Item1 - Name");
            Assert.AreEqual("RI_File", item1.FileName, "Item1 - FileName");
            Assert.AreEqual("CONTENT 1", item1.Content, "Item1 - Content");

            MultiPartItem item2 = (MultiPartItem) parts[1];
            Assert.AreEqual("CI", item2.Name, "Item2 - Name");
            Assert.AreEqual("CI_File", item2.FileName, "Item2 - FileName");
            Assert.AreEqual("CONTENT 2", item2.Content, "Item2 - Content");

        }

        [Test]
        public void Test3() 
        {
            string data = @"------------dodu-----0123456789-6acfb227-ba3f-4529-9939-43bb7fb5627a
Content-Disposition: form-data; name=""RI""; filename=""RI_File""

CONTENT 1

------------dodu-----0123456789-6acfb227-ba3f-4529-9939-43bb7fb5627a--";

            var parts = MultiPartParser.Parse(data).ToList();

            Assert.AreEqual(1, parts.Count, "Number of Parts");

            MultiPartItem item1 = (MultiPartItem) parts[0];
            Assert.AreEqual("RI", item1.Name, "Item1 - Name");
            Assert.AreEqual("RI_File", item1.FileName, "Item1 - FileName");
            Assert.AreEqual("CONTENT 1", item1.Content, "Item1 - Content");

        }

        [Test]
        public void Test4() 
        {
            var parts = MultiPartParser.Parse(null).ToList();

            Assert.AreEqual(0, parts.Count, "Parse NULL Content");

            parts = MultiPartParser.Parse("").ToList();
            Assert.AreEqual(0, parts.Count, "Parse EMPTY string");

            parts = MultiPartParser.Parse("BLAH Content-Disposition: form-data").ToList();
            Assert.AreEqual(0, parts.Count, "Parse Invalid string");

        }

        [Test]
        public void Test5()
        {
            string data = @"------------dodu-----0123456789-bb289a47-c6f8-4627-99ea-e71609c1f68c
Content-Disposition: form-data; name=""RO""

<RESPONSE_GROUP MISMOVersionID=""2.1""><RESPONSE LoginAccountIdentifier=""w2n7eonu"" LoginAccountPassword=""ML-Orig1"" ResponseDateTime=""1/27/2009 8:22:18 PM""><RESPONSE_DATA><FNM_PRODUCT _Name=""DWEB"" _FunctionName=""CasefileImport"" _VersionNumber=""2.0""><CONNECTION _ModeIdentifier=""Asynchronous"" /></FNM_PRODUCT></RESPONSE_DATA><STATUS _Name=""DWEB"" _Condition=""FAILURE"" _Description=""Request Failed"" _Code=""-1"" /></RESPONSE></RESPONSE_GROUP>

------------dodu-----0123456789-bb289a47-c6f8-4627-99ea-e71609c1f68c
Content-Disposition: form-data; name=""CO""

<DWEB_CASEFILE_IMPORT_RESPONSE MORNETPlusCasefileIdentifier=""""><STATUS _Name="""" _Condition=""failure"" _Description=""Exception of type 'FannieMae.DODU.Common.DODUException' was thrown."" _Code=""BackendMessage"" /><STATUS _Name="""" _Condition="""" _Description=""The following required fields are missing or invalid. Please check the loan file and try again."" _Code=""DI011"" /><CASEFILE_INSTITUTION LenderCaseIdentifier="""" InstitutionIdentifier=""500163"" InstitutionName="""" InstitutionType="""" /><DATA_FILE _Name=""VALIDATION_RESULTS"" _Type=""10"" _FormatType=""5"" _VersionNumber=""1.0"" /></DWEB_CASEFILE_IMPORT_RESPONSE>

------------dodu-----0123456789-bb289a47-c6f8-4627-99ea-e71609c1f68c
Content-Disposition: form-data; name=""VALIDATION_RESULTS""; filename=""cf1e8760-34e0-489d-8f63-bb6ac886bb9d.xml""

<VALIDATION_RESULTS>
  <RESULT_DETAILS _FieldId=""10R-020"" _Description=""Borrower Race SSN"" _Message=""Required Field Missing"" />
</VALIDATION_RESULTS>

------------dodu-----0123456789-bb289a47-c6f8-4627-99ea-e71609c1f68c--
";
            var parts = MultiPartParser.Parse(data).ToList();
            Assert.AreEqual(3, parts.Count, "Number of Parts");

            MultiPartItem item1 = (MultiPartItem)parts[0];
            Assert.AreEqual("RO", item1.Name, "Item1 - Name");

            MultiPartItem item2 = (MultiPartItem)parts[1];
            Assert.AreEqual("CO", item2.Name, "Item2 - Name");

            MultiPartItem item3 = (MultiPartItem)parts[2];
            Assert.AreEqual("VALIDATION_RESULTS", item3.Name, "Item3 - Name");
        }
	}
}
