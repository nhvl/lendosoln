﻿namespace LendOSolnTest.CommonTools
{
    using DataAccess;
    using LendersOffice.AntiXss;
    using LendersOffice.ConfigSystem;
    using LendersOffice.Constants;
    using LendersOffice.Lon;
    using LendersOffice.Reports;
    using LendersOffice.Security;
    using LendersOfficeApp.los.RatePrice;
    using NUnit.Framework;

    [TestFixture, Ignore("These have been verified locally")]
    public class StaticInitializerTest
    {
        [Test]
        public void Assert_CFieldDbInfoList()
        {
            var instance = CFieldDbInfoList.TheOnlyInstance;
            Assert.IsNotNull(instance);
        }

        [Test]
        public void Assert_DbConnectionStringManager()
        {
            Assert.IsNotNull(DbConnectionStringManager.GetConnectionString(DataSrc.LOShareROnly));
        }

        [Test]
        public void Assert_CSelectStatementProvider()
        {
            var cacheInstance = CSelectStatementProvider.GetProviderFor(E_ReadDBScenarioType.InputNothing_NeedAllCachedFields, null);
            var nonCacheInstance = CSelectStatementProvider.GetProviderFor(E_ReadDBScenarioType.InputNothing_NeedAllFields, null);

            Assert.IsNotNull(cacheInstance);
            Assert.IsNotNull(nonCacheInstance);
        }

        [Test]
        public void Assert_ParameterReporting()
        {
            var allParameters = ParameterReporting.RetrieveSystemParameters();
            Assert.IsNotNull(allParameters);
        }

        [Test]
        public void Assert_LoanReporting()
        {
            var reporting = new LoanReporting();
            Assert.IsNotNull(reporting);
        }

        [Test]
        public void Assert_CApplicantPrice()
        {
            var message = CApplicantPrice.GetUserMsg("Test");
            Assert.IsNotNull(message);
        }

        [Test]
        public void Assert_CPageBase()
        {
            var loan = Fakes.FakePageData.Create();
            Assert.AreEqual(0M, loan.sTotEstPPPoc);
        }

        [Test]
        public void Assert_LendingQBExecutingEngine()
        {
            var result = LendingQBExecutingEngine.GetDependencyFieldsByOperation(
                ConstAppDavid.SystemBrokerGuid, 
                LendersOffice.ConfigSystem.Operations.WorkflowOperations.ReadLoan);
            Assert.IsNotNull(result);
        }

        [Test]
        public void Assert_UpdateLONMainClass()
        {
            UpdateLONMainClass.ProcessMessage(Fakes.FakePageData.Create(), ConstStage.LON_USERNAME, ConstStage.LON_PASSWORD);
        }

        [Test]
        public void Assert_AspxTools()
        {
            var result = AspxTools.SafeUrl("Test");
            Assert.IsNotNull(result);
        }

        [Test]
        public void Assert_SystemUserPrincipal()
        {
            var principal = SystemUserPrincipal.AutomatedDisclosuresSystemUser(ConstStage.AutoDisclosureSystemUserId);
            Assert.IsNotNull(principal);
        }
    }
}
