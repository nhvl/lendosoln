﻿namespace LendOSolnTest.CommonTools
{
    using DataAccess.Utilities;
    using NUnit.Framework;

    [TestFixture]
    public class Mod97Radix10ChecksumGeneratorTest
    {
        private Mod97Radix10ChecksumGenerator Generator { get; } = new Mod97Radix10ChecksumGenerator();

        [Test]
        public void LargeChecksumDoesNotOverflowTest()
        {
            this.TestGenerator(new string('Z', 23), "71");
        }

        [Test]
        public void CfpbProvidedExampleTest()
        {
            this.TestGenerator("10Bx939c5543TqA1144M999143X", "38");
        }

        [Test]
        public void AllNumericNoRemainderTest()
        {
            this.TestGenerator("0600001234567", "58");
        }

        [Test]
        public void AllNumericRemainderTest()
        {
            this.TestGenerator("0600001234586", "98");
        }

        [Test]
        public void SingleDigitCheckSumAddsLeadingZeroTest()
        {
            this.TestGenerator("00U", "08");
            this.TestGenerator("1P9", "07");
        }

        [Test]
        [ExpectedException(typeof(System.ArgumentException))]
        public void InputSmallerThanTwoThrowsExceptionTest()
        {
            this.TestGenerator("1", "1");
            Assert.Fail("Expected exception due to input string length.");
        }

        private void TestGenerator(string input, string expectedValue)
        {
            var result = this.Generator.ComputeCheckDigits(input);
            Assert.AreEqual(expectedValue, result);
        }
    }
}
