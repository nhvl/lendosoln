/// Author: David Dao

using System;
using NUnit.Framework;

using LendersOffice.Common;
namespace LendOSolnTest.CommonTools
{
    [TestFixture]
	public class MersUtilitiesTest
	{
        [Test]
        public void GenerateMersNumberTest() 
        {
            object[][] testCases =
            {
                new object[] {"1234567", 1L, "123456700000000012"},
                new object[] {"1234567", 99L, "123456700000000996"},
                new object[] {"1234567", 56789L, "123456700000567895"},
            };

            foreach (object[] testCase in testCases) 
            {
                Assert.AreEqual((string) testCase[2], MersUtilities.GenerateMersNumber((string) testCase[0], (long) testCase[1]));
            }
        }

        [ExpectedException(typeof(ArgumentException))]
        [Test]
        public void GenerateMersNumberArgumentExceptionTest() 
        {
            MersUtilities.GenerateMersNumber("345", 99);
        }
        [ExpectedException(typeof(FormatException))]
        [Test]
        public void GenerateMersNumberFormatExceptionTest() 
        {
            MersUtilities.GenerateMersNumber("ABCDEFG", 99);
        }
	}
}
