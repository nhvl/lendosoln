﻿namespace LendOSolnTest.CommonTools
{
    using System;

    using global::CommonProjectLib.Caching;
    using Fakes;
    using NUnit.Framework;

    [TestFixture]
    public class MemoryCacheUtilitiesTest
    {
        private const string Key = nameof(MemoryCacheUtilitiesTest);
        private const int Version = 0;

        [Test]
        public void CacheVersionedItem_UpdatedVersion_CacheReturnsUpdatedItem()
        {
            var original = MemoryCacheUtilities.GetOrAddExistingWithVersioning(
                Key,
                () => Version,
                () => new FakeVersioned(),
                TimeSpan.FromMinutes(5));

            var noChangeOriginal = MemoryCacheUtilities.GetOrAddExistingWithVersioning(
                Key,
                () => Version,
                () => new FakeVersioned(),
                TimeSpan.FromMinutes(5));

            Assert.AreEqual(original.TestId, noChangeOriginal.TestId);

            var current = MemoryCacheUtilities.GetOrAddExistingWithVersioning(
                Key,
                () => Version + 1,
                () => new FakeVersioned(),
                TimeSpan.FromMinutes(5));
            
            Assert.AreNotEqual(original.TestId, current.TestId);
        }
    }
}
