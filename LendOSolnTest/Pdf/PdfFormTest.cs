﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.PdfForm;
using LendersOffice.PdfLayout;
using LendOSolnTest.Common;
using LendOSolnTest.Fakes;
using NUnit.Framework;

namespace LendOSolnTest.Pdf
{
    [TestFixture]
    public class PdfFormTest
    {
        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            LoginTools.LoginAs(TestAccountType.LoTest001);
        }

        [Test]
        public void TestRetrieveInvalid()
        {


            try
            {
                PdfForm.LoadById(Guid.NewGuid());
                Assert.Fail();
            }
            catch (NotFoundException)
            {

            }

            try
            {
                PdfForm.LoadById(Guid.Empty);
                Assert.Fail();
            }
            catch (NotFoundException)
            {
            }
        }


        [Test]
        public void TestCRUD()
        {
            PdfForm form = PdfForm.CreateNew();
            
            Assert.AreEqual(true, form.IsNew);

            string description = "LO_FORM_DESCRIPTION";
            string fileName = "LO_FORM_FILENAME";

            form.Description = description;
            form.FileName = fileName;


            form.Save();

            Guid formId = form.FormId;

            form = PdfForm.LoadById(formId);

            Assert.AreEqual(description, form.Description);
            Assert.AreEqual(fileName, form.FileName);
            Assert.AreEqual(false, form.IsSignable);

            form.Description = form.Description + "_EDIT";
            form.FileName = form.FileName + "_EDIT";


            form.Save();

            form = PdfForm.LoadById(formId);
            Assert.AreEqual(description + "_EDIT", form.Description);
            Assert.AreEqual(fileName + "_EDIT", form.FileName);


            PdfForm.DeleteById(formId);

            try
            {
                PdfForm.LoadById(formId);
                Assert.Fail();
            }
            catch (NotFoundException)
            {
            }

        }

        [Test]
        public void TestRetrieveAll()
        {
            PdfForm form = PdfForm.CreateNew();
            form.Description = "DESC_1";
            form.Save();

            form = PdfForm.CreateNew();
            form.Description = "DESC_2";
            form.Save();

            var list = PdfForm.RetrieveCustomFormsOfCurrentBroker();

            Assert.GreaterOrEqual(list.ToList().Count, 2);
        }

        [Test]
        public void TestSavingContent()
        {
            byte[] content = null;
            using (FileStream fileStream = new FileStream(TestDataRootFolder.Location + "Grid_Letter_Test.pdf", FileMode.Open, FileAccess.Read))
            {
                content = Tools.ReadFully(fileStream, 0);
            }

            PdfForm form = PdfForm.CreateNew();
            form.PdfContent = content;
            form.Description = "Test";
            form.Save();

            Guid formId = form.FormId;

            form = PdfForm.LoadById(formId);

            VerifyByteContent(content, form.PdfContent);
            
        }
        private void VerifyByteContent(byte[] expected, byte[] actual)
        {
            if (expected.Length != actual.Length)
            {
                Assert.Fail();
            }
            for (int i = 0; i < expected.Length; i++)
            {
                if (expected[i] != actual[i])
                {
                    Assert.Fail();
                }
            }
        }

        [Test]
        public void TestSavingLayout()
        {
            PdfFormLayout layout = new PdfFormLayout();

            layout.Add(new PdfField() { Name = "Field1", Type = PdfFieldType.Text });
            layout.Add(new PdfField() { Name = "Field2", Type = PdfFieldType.Checkbox });
            layout.Add(new PdfField() { Name = "aBSignature", Type = PdfFieldType.Signature });

            PdfForm form = PdfForm.CreateNew();
            form.Description = "Test";
            form.Layout = layout;
            form.Save();

            Guid formId = form.FormId;

            form = PdfForm.LoadById(formId);

            layout = form.Layout;

            Assert.AreEqual(true, form.IsSignable);

            foreach (PdfField field in layout.FieldList)
            {
                if (field.Name == "Field1" && field.Type == PdfFieldType.Text)
                {
                }
                else if (field.Name == "Field2" && field.Type == PdfFieldType.Checkbox)
                {
                }
                else if (field.Name == "aBSignature" && field.Type == PdfFieldType.Signature)
                {
                }
                else
                {
                    Assert.Fail();
                }
            }

        }

        [Test]
        public void TestPdfFieldJsonSerialization()
        {
            List<PdfField> fieldList = new List<PdfField>();

            fieldList.Add(new PdfField() 
            { 
                Name = "Field1", 
                Type = PdfFieldType.Text, 
                Align = PdfFieldAlignType.Left, 
                Rectangle = new iTextSharp.text.Rectangle(0, 1, 10, 11) 
            });

            fieldList.Add(new PdfField()
            {
                Name = "Field2",
                Type = PdfFieldType.Checkbox,
                Align = PdfFieldAlignType.Right,
                Rectangle = new iTextSharp.text.Rectangle(11, 12, 21, 22)
            });

            fieldList.Add(new PdfField()
            {
                Name = "aBSignature",
                Type = PdfFieldType.Signature,
                Align = PdfFieldAlignType.Center,
                Rectangle = new iTextSharp.text.Rectangle(33, 34, 55, 56)
            });

            string json = ObsoleteSerializationHelper.JsonSerialize(fieldList);

            List<PdfField> fieldList2 = ObsoleteSerializationHelper.JsonDeserialize<List<PdfField>>(json);

            for (int i = 0; i < fieldList.Count; i++)
            {
                PdfField x = fieldList[i];
                PdfField y = fieldList2[i];

                Assert.AreEqual(x.Name, y.Name);
                Assert.AreEqual(x.Type, y.Type);
                Assert.AreEqual(x.Align, y.Align);
                Assert.AreEqual(x.Rectangle.Left, y.Rectangle.Left);
                Assert.AreEqual(x.Rectangle.Right, y.Rectangle.Right);
                Assert.AreEqual(x.Rectangle.Top, y.Rectangle.Top);
                Assert.AreEqual(x.Rectangle.Bottom, y.Rectangle.Bottom);
            }
        }

        [Test]
        public void TestCustomFormGeneratePdf()
        {
            byte[] content = null;
            using (FileStream fileStream = new FileStream(TestDataRootFolder.Location + "Grid_Letter_Test.pdf", FileMode.Open, FileAccess.Read))
            {
                content = Tools.ReadFully(fileStream, 0);
            }

            PdfForm form = PdfForm.CreateNew();
            form.PdfContent = content;
            form.Description = "Test";

            PdfFormLayout layout = new PdfFormLayout();
            layout.Add(new PdfField() { Type = PdfFieldType.Text, Name = "aBNm", Rectangle = new iTextSharp.text.Rectangle(20, 20, 50, 30), PageNumber = 1 });
            form.Layout = layout;
            form.Save();


            Guid formId = form.FormId;
            form = PdfForm.LoadById(formId);
            Assert.AreEqual(false, form.IsStandardForm);

            var dataLoan = FakePageData.Create();

            using (FileStream outputStream = new FileStream(ConstApp.TempFolder + @"\PdfFormTestOutput.pdf", FileMode.Create))
            {
                //PdfFormLayout layout;
                byte[] outputContent = form.GeneratePrintPdfAndPdfLayout(dataLoan.sLId, dataLoan.GetAppData(0).aAppId, Guid.Empty, out layout);
                outputStream.Write(outputContent, 0, outputContent.Length);
            }


        }

        [Test]
        public void TestStandardFormGeneratePdf()
        {
            var dataLoan = FakePageData.Create();
            var dataApp = dataLoan.GetAppData(0);

            foreach (PdfForm form in PdfForm.RetrieveAllFormsOfCurrentBroker(dataLoan.sLId, dataApp.aAppId)) 
            {
                if (form.IsStandardForm)
                {
                    try
                    {
                        using (FileStream outputStream = new FileStream(ConstApp.TempFolder + @"\" + form.FileName, FileMode.Create))
                        {
                            PdfFormLayout layout;
                            byte[] outputContent = form.GeneratePrintPdfAndPdfLayout(dataLoan.sLId, dataApp.aAppId, Guid.Empty, out layout);
                            outputStream.Write(outputContent, 0, outputContent.Length);
                        }
                    }
                    catch (Exception exc)
                    {
                        Assert.Fail($"{form.FileName}::{form.Description} {exc.Message}");
                    }
                }
            }
        }
    }
}
