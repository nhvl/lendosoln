﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using LendOSolnTest.Common;
using LendersOffice.PdfLayout;
using LendersOffice.Security;
using LendersOffice.Common;

namespace LendOSolnTest.Pdf
{
    [TestFixture]
    public class PdfSignatureTemplateTest
    {
        private AbstractUserPrincipal m_principal;

        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            m_principal = LoginTools.LoginAs(TestAccountType.LoTest001);
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            foreach (var o in PdfSignatureTemplate.ListTemplatesByCurrentUser())
            {
                o.Delete();
            }
        }

        [Test]
        public void TestCrud()
        {
            PdfSignatureTemplate template = PdfSignatureTemplate.Create();
            template.Name = "TestName";
            template.FormLayout.Add(new PdfField() { Name = "Field1", Type = PdfFieldType.Text });

            template.Save();

            Guid templateId = template.TemplateId;

            template = PdfSignatureTemplate.RetrieveByTemplateId(templateId);

            Assert.IsNotNull(template);
            Assert.AreEqual(m_principal.BrokerId, template.BrokerId, "BrokerId");
            Assert.AreEqual(m_principal.UserId, template.OwnerUserId, "OwnerUserId");
            Assert.AreEqual("TestName", template.Name, "Name");

            foreach (var f in template.FormLayout.FieldList)
            {
                Assert.AreEqual("Field1", f.Name, "FieldName");
                Assert.AreEqual(PdfFieldType.Text, f.Type, "FieldType");
            }

            template.Name = "TestName_Edit";
            template.Save();

            template = PdfSignatureTemplate.RetrieveByTemplateId(templateId);
            Assert.AreEqual("TestName_Edit", template.Name, "Name");

            template.Delete();


            try
            {
                PdfSignatureTemplate.RetrieveByTemplateId(templateId);
                Assert.Fail();
            }
            catch (NotFoundException)
            {

            }
        }
        [Test]
        public void TestAddDuplicate()
        {
            PdfSignatureTemplate template = PdfSignatureTemplate.Create();
            template.Name = "TestNameDuplicate";
            template.FormLayout.Add(new PdfField() { Name = "Field1", Type = PdfFieldType.Text });

            template.Save();

            try
            {
                template = PdfSignatureTemplate.Create();
                template.Name = "TestNameDuplicate";
                template.FormLayout.Add(new PdfField() { Name = "Field1", Type = PdfFieldType.Text });

                template.Save();

                Assert.Fail();
            }
            catch (DuplicatePdfSignatureTemplateException)
            {
            }


        }
    }
}
