﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using System.Reflection;
using LendersOffice.Common;
using LendersOffice.Pdf;

namespace LendOSolnTest.Pdf
{
    [TestFixture]
    public class PdfPrintingTest
    {
        [Test]
        public void TestIPdfPrintItem_DependencyFields()
        {
            // 5/18/2010 dd - Verify that all the Pdf class that implement LendersOffice.Common.IPdfPrintItem and have default
            // constructor can invoke DependencyFields without exception.

            Assembly lendersOfficeAppAssembly = typeof(LendersOfficeApp.newlos.PrintList).Assembly;

            Type[] typeList = lendersOfficeAppAssembly.GetTypes();
            foreach (Type t in typeList)
            {
                
                if (!t.IsAbstract && typeof(LendersOffice.Pdf.IPDFPrintItem).IsAssignableFrom(t))
                {
                    if (t == typeof(LendersOffice.Pdf.CCustomPDF))
                    {
                        continue;
                    }
                    // 5/18/2010 dd - Make sure the class has default constructor.

                    ConstructorInfo constructor = t.GetConstructor(new Type[0]);
                    if (null != constructor)
                    {
                        IPDFPrintItem item = (IPDFPrintItem)constructor.Invoke(new object[0]);
                        Assert.IsNotNull(item.DependencyFields, t.FullName);
                    }
                }
            }
        }
    }
}
