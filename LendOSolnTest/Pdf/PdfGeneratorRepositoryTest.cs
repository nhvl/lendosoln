﻿namespace LendOSolnTest.Pdf
{
    using System;
    using System.Collections.Specialized;
    using System.Linq;
    using System.IO;
    using LendersOffice.Pdf;
    using LendOSolnTest.Common;
    using NUnit.Framework;

    [TestFixture]
    public class PdfGeneratorRepositoryTest
    {
        [Test]
        public void HasPdfGenerator_Always_ReturnsTrueForStandardForm()
        {
            var repository = new PdfGeneratorRepository();
            var testDataPath = TestUtilities.GetFilePath("StandardPdfFormNames.txt");

            foreach (var standardFormName in File.ReadAllLines(testDataPath))
            {
                Assert.IsTrue(repository.HasPdfGenerator(standardFormName));
            }
        }

        [Test]
        public void HasPdfGenerator_CustomPdf_ReturnsTrue()
        {
            var repository = new PdfGeneratorRepository();
            Assert.IsTrue(repository.HasPdfGenerator("custom"));
        }

        [Test]
        public void GetAllStandardFormNames_Always_ReturnsStandardFormNames()
        {
            var repository = new PdfGeneratorRepository();

            var testDataPath = TestUtilities.GetFilePath("StandardPdfFormNames.txt");
            var expectedStandardFormNames = File.ReadAllLines(testDataPath);

            foreach (var standardFormName in expectedStandardFormNames)
            {
                Assert.IsTrue(repository.HasPdfGenerator(standardFormName));
            }
        }

        [Test]
        public void GetAllStandardFormNames_Never_ReturnsCustomName()
        {
            var repository = new PdfGeneratorRepository();
            var returnedStandardFormNames = repository.GetAllStandardFormNames();

            CollectionAssert.DoesNotContain(returnedStandardFormNames, "custom");
        }

        [Test]
        public void CreatePdfInstanceNoArguments_Always_ReturnsGeneratorForStandardForm()
        {
            var repository = new PdfGeneratorRepository();
            var testDataPath = TestUtilities.GetFilePath("StandardPdfFormNames.txt");

            foreach (var standardFormName in File.ReadAllLines(testDataPath))
            {
                Assert.IsNotNull(repository.CreatePdfInstance(standardFormName));
            }
        }

        [Test]
        public void CreatePdfInstanceNoArguments_Custom_ReturnsGenerator()
        {
            var repository = new PdfGeneratorRepository();
            Assert.IsNotNull(repository.CreatePdfInstance("custom"));
        }

        [Test]
        public void CreatePdfInstanceWithArguments_Always_ReturnsGeneratorForStandardForm()
        {
            var repository = new PdfGeneratorRepository();
            var testDataPath = TestUtilities.GetFilePath("StandardPdfFormNames.txt");

            var arguments = new NameValueCollection();
            arguments.Add("applicationid", Guid.NewGuid().ToString());
            arguments.Add("passwordid", Guid.NewGuid().ToString());
            arguments.Add("recordid", Guid.NewGuid().ToString());
            arguments.Add("borrtype", "B");
            arguments.Add("isadduli", bool.TrueString);

            foreach (var standardFormName in File.ReadAllLines(testDataPath))
            {
                Assert.IsNotNull(repository.CreatePdfInstance(standardFormName, arguments));
            }
        }

        [Test]
        public void CreatePdfInstanceWithArguments_Custom_ReturnsGenerator()
        {
            var repository = new PdfGeneratorRepository();

            var arguments = new NameValueCollection();
            arguments.Add("applicationid", Guid.NewGuid().ToString());
            arguments.Add("passwordid", Guid.NewGuid().ToString());
            arguments.Add("recordid", Guid.NewGuid().ToString());
            arguments.Add("borrtype", "B");
            arguments.Add("isadduli", bool.TrueString);

            Assert.IsNotNull(repository.CreatePdfInstance("custom", arguments));
        }
    }
}
