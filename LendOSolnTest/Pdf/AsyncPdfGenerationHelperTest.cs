﻿namespace LendOSolnTest.Pdf
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Linq;
    using LendersOffice.Pdf;
    using LendersOffice.Pdf.Async;
    using LendersOffice.Security;
    using NUnit.Framework;

    // 2018-12 tj - Commenting out the test cases entirely so that the NUnit output file will not include ~8MB of ignored tests
    [TestFixture]
    [Ignore("This class generates a significant number (> 11k) tests due to the number of combinations. " +
        "While each individual test case is fast, the sheer number greatly increases the time spent running tests.")]
    public class AsyncPdfGenerationHelperTest
    {
        private static readonly PdfGeneratorRepository repository = new PdfGeneratorRepository();

        // These PDFs have been verified manually.
        private static readonly HashSet<string> ignoreList = new HashSet<string>()
        {
            "creditreport", // Requires an credit report PDF on file
            "lqicreditreport", // Requires an credit report PDF on file
            "lqbappraisalorder", // Requires an LQB appraisal order on file
            "appraisalorder", // Requires an appraisal order on file
            "loadmintasklist", // Requires LOAdmin user
            "pipelinetasks", // Requires cached task pipeline data
            "suspensenotice", // Requires a pre-filled form in FileDB
            "suspensenotice_1",
            "suspensenotice_2",
            "suspensenoticeobsolete",
            "pmlapproval",
            "ratelockconfirmation",
            "pmlsummary", // End requires pre-filled form
            "itemizationofamountfinance_old", // Form is deprecated and no longer selectable
            "worstcaseamortization", // Has an outstanding dependency error to be fixed outside of this case
            "bestcaseamortization", // Has an outstanding dependency error to be fixed outside of this case
            "vom", // Requires VOM records on file
            "vol", // Requires VOL records on file
            "vod", // Requires VOD records on file
            "verbalvoepdf", // Requires VOE records on file
        };

        private static readonly object[] standardPdfFormArguments = (
            from standardFormName in repository.GetAllStandardFormNames()
            from borrowerType in new[] { "B", "C" }
            from isAddUli in new[] { true, false }
            from printOptions in new[] { E_PdfPrintOptions.BlankForms, E_PdfPrintOptions.FormWithData }
            where !ignoreList.Contains(standardFormName)
            select new object[] { standardFormName, borrowerType, isAddUli, printOptions }
            ).ToArray();

        private static readonly object[] customPdfFormArguments = (
            from borrowerType in new[] { "B", "C" }
            from isAddUli in new[] { true, false }
            from printOptions in new[] { E_PdfPrintOptions.BlankForms, E_PdfPrintOptions.FormWithData }
            from pageSize in new[] { "0", "1" }
            select new object[] { borrowerType, isAddUli, printOptions, pageSize }
            ).ToArray();

        private static readonly object[] batchCustomPdfFormArguments = (
            from borrowerType in new[] { "B", "C" }
            from isAddUli in new[] { true, false }
            from printOptions in new[] { E_PdfPrintOptions.BlankForms, E_PdfPrintOptions.FormWithData }
            from pageSize in new[] { "0", "1" }
            select new object[] { borrowerType, isAddUli, printOptions, pageSize }
            ).ToArray();

        private static readonly object[] batchPdfSource = (
            from printOptions in new[] { E_PdfPrintOptions.BlankForms, E_PdfPrintOptions.FormWithData }
            from borrowerType in new[] { "B", "C" }
            from isAddUli in new[] { true, false }
            from pageSize in new[] { "0", "1" }
            from pageName in repository.GetAllStandardFormNames()
            where !ignoreList.Contains(pageName)
            select new object[] { printOptions, borrowerType, isAddUli, pageSize, pageName }
            ).ToArray();

        private readonly Guid loanId = new Guid("6C2C8092-0C45-4715-B6B7-A73500A5FEE5");

        private readonly AbstractUserPrincipal principal = PrincipalFactory.RetrievePrincipalForUser(
            new Guid("B321687E-A1B3-4A4E-88E4-397A0CB831B6"),
            new Guid("6F0B69D3-8F15-4BAE-A185-B3F2A0E6A2D4"),
            "B");

        ////[TestCaseSource(nameof(standardPdfFormArguments))]
        public void GenerateSinglePdf_Always_CreatesNewPdfInstanceForStandardForms(string standardFormName, string borrowerType, bool isAddUli, E_PdfPrintOptions printOptions)
        {
            using (var principalHelper = new TemporaryPrincipalUseHelper(System.Threading.Thread.CurrentPrincipal, this.principal))
            {
                var loan = DataAccess.CPageData.CreateUsingSmartDependency(this.loanId, typeof(AsyncPdfGenerationHelperTest));
                loan.InitLoad();

                var arguments = new NameValueCollection
                {
                    { "loanid", loan.sLId.ToString() },
                    { "applicationid", loan.GetAppData(0).aAppId.ToString() },
                    { "brokerid", loan.sBrokerId.ToString() },
                    { "IsAddUli", isAddUli.ToString() },
                    { "printoptions", printOptions.ToString() },
                    { "borrtype", borrowerType }
                };

                var pdfHelper = new AsyncPdfGenerationHelper();
                var generationResult = pdfHelper.GenerateSinglePdf(standardFormName, arguments, password: string.Empty);
                Assert.AreEqual(AsyncPdfGenerationStatus.Complete, generationResult.Status, generationResult.ErrorMessage);
            }
        }

        ////[TestCaseSource(nameof(customPdfFormArguments))]
        public void GenerateSinglePdf_Always_CreatesNewPdfInstanceForCustomForms(string borrowerType, bool isAddUli, E_PdfPrintOptions printOptions, string pageSize)
        {
            using (var principalHelper = new TemporaryPrincipalUseHelper(System.Threading.Thread.CurrentPrincipal, this.principal))
            {
                var loan = DataAccess.CPageData.CreateUsingSmartDependency(this.loanId, typeof(AsyncPdfGenerationHelperTest));
                loan.InitLoad();

                var arguments = new NameValueCollection
                {
                    { "loanid", loan.sLId.ToString() },
                    { "applicationid", loan.GetAppData(0).aAppId.ToString() },
                    { "brokerid", loan.sBrokerId.ToString() },
                    { "IsAddUli", isAddUli.ToString() },
                    { "printoptions", printOptions.ToString() },
                    { "borrtype", borrowerType },
                    { "custompdfid", "54CEC0D6-D813-48D8-903D-61CD372BF843" },
                    { "pagesize", pageSize }
                };

                var pdfHelper = new AsyncPdfGenerationHelper();
                var generationResult = pdfHelper.GenerateSinglePdf("custom", arguments, password: string.Empty);
                Assert.AreEqual(AsyncPdfGenerationStatus.Complete, generationResult.Status, generationResult.ErrorMessage);
            }
        }

        ////[TestCaseSource(nameof(batchPdfSource))]
        public void GenerateBatchPdf_StandardForms_CreatesNewBatchPdfInstance(E_PdfPrintOptions printOptions, string borrowerType, bool isAddUli, string pageSize, string formName)
        {
            using (var principalHelper = new TemporaryPrincipalUseHelper(System.Threading.Thread.CurrentPrincipal, this.principal))
            {
                var loan = DataAccess.CPageData.CreateUsingSmartDependency(this.loanId, typeof(AsyncPdfGenerationHelperTest));
                loan.InitLoad();

                var printList = new LendersOffice.Pdf.Async.PdfPrintList
                {
                    sLId = this.loanId,
                    PrintOptions = printOptions.ToString()
                };

                var id = repository.CreatePdfInstance(formName).GetType().Name;
                var item = new PdfPrintItem()
                {
                    ID = id,
                    PageSize = pageSize,
                    ApplicationID = loan.GetAppData(0).aAppId,
                    BorrType = borrowerType,
                    IsAddUli = isAddUli.ToString()
                };

                printList.ItemList.Add(item);

                var pdfHelper = new AsyncPdfGenerationHelper();
                var generationResult = pdfHelper.GenerateBatchPdf(printList, password: string.Empty);
                Assert.AreEqual(AsyncPdfGenerationStatus.Complete, generationResult.Status, generationResult.ErrorMessage);
            }
        }

        ////[TestCaseSource(nameof(batchCustomPdfFormArguments))]
        public void GenerateBatchPdf_CustomForm_CreatesNewBatchPdfInstance(string borrowerType, bool isAddUli, E_PdfPrintOptions printOptions, string pageSize)
        {
            using (var principalHelper = new TemporaryPrincipalUseHelper(System.Threading.Thread.CurrentPrincipal, this.principal))
            {
                var loan = DataAccess.CPageData.CreateUsingSmartDependency(this.loanId, typeof(AsyncPdfGenerationHelperTest));
                loan.InitLoad();

                var printList = new LendersOffice.Pdf.Async.PdfPrintList
                {
                    sLId = this.loanId,
                    PrintOptions = printOptions.ToString()
                };

                var item = new PdfPrintItem()
                {
                    ID = "54CEC0D6-D813-48D8-903D-61CD372BF843",
                    Type = "custompdf",
                    PageSize = pageSize,
                    ApplicationID = loan.GetAppData(0).aAppId,
                    BorrType = borrowerType,
                    IsAddUli = isAddUli.ToString()
                };

                printList.ItemList.Add(item);

                var pdfHelper = new AsyncPdfGenerationHelper();
                var generationResult = pdfHelper.GenerateBatchPdf(printList, password: string.Empty);
                Assert.AreEqual(AsyncPdfGenerationStatus.Complete, generationResult.Status, generationResult.ErrorMessage);
            }
        }
    }
}
