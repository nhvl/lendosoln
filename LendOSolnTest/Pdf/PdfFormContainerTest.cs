﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using LendOSolnTest.Common;
using LendersOffice.PdfLayout;
using LendersOffice.Constants;

namespace LendOSolnTest.Pdf
{
    [TestFixture]
    public class PdfFormContainerTest
    {
        [Test]
        public void MainTest()
        {

            using (FileStream os = new FileStream(ConstApp.TempFolder + @"\PdfFormContainerTestOutput.pdf", FileMode.Create))
            {
                Document document = new Document();

                PdfCopy writer = new PdfCopy(document, os);
                document.Open();

                PdfFormLayout layout = new PdfFormLayout();
                layout.Load(TestDataRootFolder.Location + "Grid_Letter_Test.pdf.xml.config");

                PdfFormContainer container = new PdfFormContainer(TestDataRootFolder.Location + "Grid_Letter_Test.pdf", layout);

                KeyValuePair<string, string>[] values = {
                                                                new KeyValuePair<string, string>("SingleLine_LeftAlign", "Test Line")
                                                                , new KeyValuePair<string, string>("SingleLine_RightAlign", "Test Line")
                                                                , new KeyValuePair<string, string>("SingleLine_CenterAlign", "Test Line")
                                                                , new KeyValuePair<string, string>("MultipleLine_LeftAlign", "Line 0" + Environment.NewLine + "Line 1" + Environment.NewLine + "Line 2")
                                                                , new KeyValuePair<string, string>("MultipleLine_RightAlign", "Line 0" + Environment.NewLine + "Line 1" + Environment.NewLine + "Line 2")
                                                                , new KeyValuePair<string, string>("MultipleLine_CenterAlign", "Line 0" + Environment.NewLine + "Line 1" + Environment.NewLine + "Line 2")
                                                                , new KeyValuePair<string, string>("cb_with_border", "Yes")
                                                                , new KeyValuePair<string, string>("cb_without_border", "Yes")

                                                                , new KeyValuePair<string, string>("SingleLine_LeftAlign_CenterVAlign", "Test Line (H-Align:Left V-Align: Center)")
                                                                , new KeyValuePair<string, string>("SingleLine_RightAlign_CenterVAlign", "Test Line (H-Align:Right V-Align: Center)")
                                                                , new KeyValuePair<string, string>("SingleLine_CenterAlign_CenterVAlign", "Test Line (H-Align:Center V-Align: Center)")

                                                                , new KeyValuePair<string, string>("SingleLine_LeftAlign_BottomVAlign", "Test Line (H-Align:Left V-Align: Bottom)")
                                                                , new KeyValuePair<string, string>("SingleLine_RightAlign_BottomVAlign", "Test Line (H-Align:Right V-Align: Bottom)")
                                                                , new KeyValuePair<string, string>("SingleLine_CenterAlign_BottomVAlign", "Test Line (H-Align:Center V-Align: Bottom)")

                                                                , new KeyValuePair<string, string>("MultipleLine_LeftAlign_CenterVAlign", "Line 0 (H-Align:Left V-Align: Center)" + Environment.NewLine + "Line 1" + Environment.NewLine + "Line 2")
                                                                , new KeyValuePair<string, string>("MultipleLine_RightAlign_CenterVAlign", "Line 0 (H-Align:Right V-Align: Center)" + Environment.NewLine + "Line 1" + Environment.NewLine + "Line 2")
                                                                , new KeyValuePair<string, string>("MultipleLine_CenterAlign_CenterVAlign", "Line 0 (H-Align:Center V-Align: Center)" + Environment.NewLine + "Line 1" + Environment.NewLine + "Line 2")

                                                                , new KeyValuePair<string, string>("MultipleLine_LeftAlign_BottomVAlign", "Line 0 (H-Align:Left V-Align: Bottom)" + Environment.NewLine + "Line 1" + Environment.NewLine + "Line 2")
                                                                , new KeyValuePair<string, string>("MultipleLine_RightAlign_BottomVAlign", "Line 0 (H-Align:Right V-Align: Bottom)" + Environment.NewLine + "Line 1" + Environment.NewLine + "Line 2")
                                                                , new KeyValuePair<string, string>("MultipleLine_CenterAlign_BottomVAlign", "Line 0 (H-Align:Center V-Align: Bottom)" + Environment.NewLine + "Line 1" + Environment.NewLine + "Line 2")

                                                            };
                foreach (var o in values)
                {
                    container.SetFieldValue(o.Key, o.Value);
                }
                container.SetFieldValue("aBSignature", new PdfSignatureValue() { Name = "David Dao", SignedDate = new DateTime(2010, 4, 24), TransactionNumber = "1234567890" });
                container.AppendTo(writer);
                document.Close();
            }
        }

        [Test]
        public void GenerateTestPdf()
        {
            using (FileStream os = new FileStream(ConstApp.TempFolder + @"\PdfFormContainerTestCompleteOutput.pdf", FileMode.Create))
            {
                Document document = new Document();

                PdfCopy writer = new PdfCopy(document, os);
                document.Open();

                PdfFormLayout layout = new PdfFormLayout();
                layout.Load(TestDataRootFolder.Location + "Grid_Letter_Test.pdf.xml.config");

                PdfFormContainer container = new PdfFormContainer(TestDataRootFolder.Location + "Grid_Letter_Test.pdf", layout);
                container.IsTestMode = true;
                container.AppendTo(writer);
                document.Close();
            }
        }
    }
}
