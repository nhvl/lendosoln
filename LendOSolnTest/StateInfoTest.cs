﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using DataAccess;
using System.Collections;

namespace LendOSolnTest
{
    [TestFixture]
    public class StateInfoTest
    {

        [Test]
        public void TestGetFirstCountyByStateAndZip()
        {
            Assert.That(StateInfo.Instance.GetFirstCountyByStateAndZip("CA", "92008").Equals("San Diego", StringComparison.OrdinalIgnoreCase), 
                Is.True, "Zipcode 92008 doesnt match san diego county");
        }

        [Test]
        public void TestCountyCaseInsentitive()
        {
            // 3/19/2009 dd - Relate to external OPM 28652.
            string[,] stateCounties = new string[,] {
                {"CA", "San Diego"}
                , {"Ca", "San Diego"}
                , {"ca", "san diego"}
                , {"ca", "SAN DIEGO"}
            };

            int fipsCode = -1;
            for (int i = 0; i < stateCounties.Length / 2; i++ )
            {

                if (i == 0)
                {
                    fipsCode = StateInfo.Instance.GetFipsCodeFor(stateCounties[i, 0], stateCounties[i, 1]);
                }
                else
                {
                    Assert.AreEqual(fipsCode, StateInfo.Instance.GetFipsCodeFor(stateCounties[i, 0], stateCounties[i, 1]), "FIPS does not match for " + stateCounties[i, 0] + ", " + stateCounties[i, 1]);
                }
            }
        }
        [Test]
        public void TestGetFipsCode()
        {
            Assert.That(StateInfo.Instance.GetFipsCodeFor("ca", "san diego"), Is.EqualTo(6073));
            Assert.That(StateInfo.Instance.GetFipsCodeFor("or", "Curry"), Is.EqualTo(41015));
            Assert.That(StateInfo.Instance.GetFipsCodeFor("", ""), Is.EqualTo(0));
            Assert.That(StateInfo.Instance.GetFipsCodeFor("CA", "new mexico"), Is.EqualTo(0));
        }
        [Test]
        public void TestGetCounties()
        {
            ArrayList counties = StateInfo.Instance.GetCountiesFor("CA");
            foreach (string county in counties)
            {
                if (county.ToUpper() == "LOS ANGELES")
                {
                    return;
                }
            }
            Assert.Fail();
        }
        [Test]
        public void TestGetCountiesIn()
        {
            IList<string> counties = StateInfo.Instance.GetCountiesIn("or");
            Assert.That(counties.Contains("Crook"), Is.True);
        }
        [Test]
        public void TestGetStateCode()
        {
            Assert.That(StateInfo.Instance.StateCode("ca"), Is.EqualTo(6));
            Assert.That(StateInfo.Instance.StateCode("adsasd"), Is.EqualTo(-1));
        }

        [Test]
        public void StringFiltering()
        {
            string[,] tests = new string[,]{
                //Make sure periods and apostrophes are removed
                {"N'Y.'.''.''.'", "S.'c'h'u.yl'..'e'.r", "NY", "Schuyler"},
                //Make sure dashes are turned into spaces
                {"LA", "Saint-Landry", "LA", "Saint Landry"},
                //These counties have dashes and apostrophes and things in them already
                //(use this query to get them: select distinct state, county, FipsCountyCode from CityCountyStateZip where county like '%[''-.]%')
                {"AK", "Yukon-Koyukuk", "AK", "Yukon Koyukuk"},
                {"FL", "Miami-dade", "FL", "Miami dade"},
                {"IN", "St. Joseph", "IN", "St Joseph"},
                {"MD", "Prince George's", "MD", "Prince Georges"}
            };
            /* indexes: 
             * 0 = messy state, 
             * 1 = messy county,
             * 2 = clean state,
             * 3 = clean county
             */
            StateInfo SI = StateInfo.Instance;

            for (int i = 0; i < tests.GetLength(0); i++)
            {
                //Make sure they all have the same FIPS codes
                Assert.AreEqual(SI.GetFipsCodeFor(tests[i, 2], tests[i, 3]), SI.GetFipsCodeFor(tests[i, 0], tests[i, 1]));

                //The states should also have the same FIPS code
                Assert.AreEqual(SI.StateCode(tests[i, 2]), SI.StateCode(tests[i, 0]));

                //And the messy state should contain the clean county
                Assert.That(SI.GetCountiesIn(tests[i, 0]).Contains(tests[i, 3]));
            }
        }
    }
}
