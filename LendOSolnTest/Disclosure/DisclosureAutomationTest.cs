﻿namespace LendOSolnTest.Disclosure
{
    using System;
    using System.Linq;
    using global::ConfigSystem.DataAccess;
    using global::ConfigSystem.Engine;
    using DataAccess;
    using LendersOffice.Audit;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem;
    using LendersOffice.Constants;
    using LendersOffice.Integration.DocumentVendor.LQBESignUpdate;
    using LendersOffice.Migration;
    using LendersOffice.ObjLib.DocMagicLib;
    using LendersOffice.Security;
    using LendOSolnTest.Common;
    using NUnit.Framework;

    [TestFixture]
    public class DisclosureAutomationTest
    {
        private Guid m_sLId = Guid.Empty;

        private AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.LoTest001);
        private string initialTempOptionXmlContent;

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            this.initialTempOptionXmlContent = this.principal.BrokerDB.TempOptionXmlContent.Value;
        }

        [SetUp]
        public void Setup()
        {
            var fileCreator = CLoanFileCreator.GetCreator(principal, E_LoanCreationSource.UserCreateFromBlank);
            m_sLId = fileCreator.CreateBlankLoanFile();
        }

        [TearDown]
        public void TearDown()
        {
            this.principal.BrokerDB.TempOptionXmlContent = this.initialTempOptionXmlContent;

            if (m_sLId != Guid.Empty)
            {
                Tools.DeclareLoanFileInvalid(principal, m_sLId, false, false);
            }
        }

        /// <summary>
        /// Ensure that a redisclosure trigger will not fire on a loan that needs
        /// initial disclosures.
        /// </summary>
        [Test]
        public void RediscShouldNotOverrideInitialDiscTest()
        {
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            
            // Set initial disclosure fields.
            dataLoan.sNeedInitialDisc = true;
            dataLoan.sDisclosureNeededT = E_sDisclosureNeededT.InitDisc_RESPAAppReceived;
            dataLoan.sDisclosuresDueD = CDateTime.Create(DateTime.Today);
            dataLoan.sTilGfeDueD = dataLoan.sDisclosuresDueD;

            // Set redisclosure trigger field.
            dataLoan.sLpTemplateNmSubmitted = "TEST NM";

            dataLoan.Save();

            Assert.AreEqual(true, dataLoan.sNeedInitialDisc, "Should still need initial disclosure.");
            Assert.AreEqual(false, dataLoan.sNeedRedisc, "Should not need redisclosure.");
            Assert.AreEqual(true, DateTime.Today.Equals(dataLoan.sDisclosuresDueD.DateTimeForComputation), 
                "Shouldn't update disclosure due date.");
            Assert.AreEqual(dataLoan.sDisclosuresDueD_rep, dataLoan.sTilGfeDueD_rep, 
                "Should not update sTilGfeDueD.");
            Assert.AreEqual(E_sDisclosureNeededT.InitDisc_RESPAAppReceived, dataLoan.sDisclosureNeededT, 
                "Should not change type of disclosure needed");
        }

        /// <summary>
        /// Ensure that changing one of the redisclosure trigger fields works
        /// as expected.
        /// </summary>
        [Test]
        public void RediscFieldChangeTest()
        {
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sLastDisclosedD = CDateTime.Create(DateTime.Now);
            dataLoan.sLpTemplateNmSubmitted = "TEST NM";
            dataLoan.Save();

            ValidateRedisclosureTrigger(dataLoan, E_sDisclosureNeededT.CC_ProgramChanged);
        }

        /// <summary>
        /// Ensure that an processing an APR trigger works as expected.
        /// </summary>
        [Test]
        public void APRDisclosureTest()
        {
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            // OPM 236812 - Redisclosure only needed for non-TRID loans.
            dataLoan.sDisclosureRegulationTLckd = true;
            dataLoan.sDisclosureRegulationT = E_sDisclosureRegulationT.GFE;

            dataLoan.sLastDisclosedD = CDateTime.Create(DateTime.Now);
            dataLoan.Save();

            Assert.That(!dataLoan.sNeedInitialDisc && !dataLoan.sNeedRedisc,
            "Shouldn't have worked because APR is invalid.");

            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sLAmtLckd = true;
            dataLoan.sLAmtCalc = 100.0m;
            dataLoan.Save();

            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sLastDiscAPR = 15.00m;
            dataLoan.sNoteIR = 5.0m;
            dataLoan.sTerm = 360;
            dataLoan.sDue = 360;
            dataLoan.sSchedDueD1 = CDateTime.Create(DateTime.Now);
            dataLoan.Save();

            ValidateRedisclosureTrigger(dataLoan, E_sDisclosureNeededT.APROutOfTolerance_RediscReqd);

            // Ensure that the dependencies are working.
            CreateDataObject();
            dataLoan = new CPageData(m_sLId, new string[] { "sLastDiscApr" });
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sLastDiscAPR = 5.0m;
            dataLoan.Save();
        }

        [Test]
        public void APRShouldNotTriggerForHelocTest()
        {
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sIsLineOfCredit = true;
            dataLoan.sLAmtLckd = true;
            dataLoan.sLAmtCalc = 100.0m;
            dataLoan.Save();

            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sLastDisclosedD = CDateTime.Create(DateTime.Now);
            dataLoan.sLastDiscAPR = 15.00m;
            dataLoan.Save();

            Assert.That(!dataLoan.sNeedInitialDisc && !dataLoan.sNeedRedisc,
                "Shouldn't have worked because APR is invalid.");

            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sNoteIR = 5.0m;
            dataLoan.sTerm = 360;
            dataLoan.sDue = 360;
            dataLoan.sSchedDueD1 = CDateTime.Create(DateTime.Now);
            dataLoan.Save();

            Assert.That(
                !dataLoan.sNeedInitialDisc && !dataLoan.sNeedRedisc,
                "Loan should not be marked as needing disclosure.");

            Assert.That(
                dataLoan.sDisclosureNeededT == E_sDisclosureNeededT.None,
                "Disclosure needed should be none.");

            Assert.That(
                !dataLoan.sDisclosuresDueD.IsValid,
                "Should not have a disclosures due date.");
        }

        /// <summary>
        /// Ensure that processing a rate lock trigger works as expected.
        /// </summary>
        [Test]
        public void RateLockTest()
        {
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sLastDisclosedD = CDateTime.Create(DateTime.Now);
            dataLoan.LockRate("MICKEYMOUSE", "YUP", false);
            dataLoan.Save();

            ValidateRedisclosureTrigger(dataLoan, E_sDisclosureNeededT.CC_LoanLocked);
        }

        /// <summary>
        /// Ensure that processing a lock extension trigger works as expected.
        /// </summary>
        [Test]
        public void LockExtendTest()
        {
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sLastDisclosedD = CDateTime.Create(DateTime.Now);
            dataLoan.LockRate("MICKEYMOUSE", "YUP", false);
            dataLoan.ExtendRateLock("MICKEYMOUSE", "YUP", 2, "1.000%");
            dataLoan.Save();

            ValidateRedisclosureTrigger(dataLoan, E_sDisclosureNeededT.CC_LockExtended);
        }

        /// <summary>
        /// Check that the loan needs re-disclosure, has the expected disclosures due date,
        /// that this date matches the TIL/GFE due date, and that the type of disclosure is
        /// correct.
        /// </summary>
        /// <param name="dataLoan">The loan to check.</param>
        /// <param name="disclosureT">The type of disclosure that is expected.</param>
        private void ValidateRedisclosureTrigger(CPageData dataLoan, E_sDisclosureNeededT disclosureT)
        {
            Assert.AreEqual(true, dataLoan.sNeedRedisc, "Should need redisclosure.");

            CDateTime discDueD = CDateTime.Create(DateTime.Today);

            if (disclosureT == E_sDisclosureNeededT.APROutOfTolerance_RediscReqd)
            {
                discDueD = discDueD.AddWorkingBankDays(3,
                    !LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V28_DontRollSundayHolidaysToMonday));
            }
            else
            {
                discDueD = discDueD.AddBusinessDays(3, dataLoan.BrokerDB);
            }

            Assert.AreEqual(true, discDueD.DateTimeForComputation.Equals(dataLoan.sDisclosuresDueD.DateTimeForComputation),
                "Disclosures should be due in 3 business days from current date.");
            Assert.AreEqual(dataLoan.sDisclosuresDueD_rep, dataLoan.sTilGfeDueD_rep,
                "sTilGfeDueD should be updated to match disclosure due date.");
            Assert.AreEqual(disclosureT, dataLoan.sDisclosureNeededT);
        }

        /// <summary>
        /// Ensure that processing an initial disclosures drawn (w/ e-disclosure)
        /// trigger works as expected.
        /// </summary>
        [Test]
        public void DrawInitialDisclosuresEDisclosedTest()
        {
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            // Set up as if it needed initial disclosure
            dataLoan.sNeedInitialDisc = true;
            dataLoan.sDisclosureNeededT = E_sDisclosureNeededT.InitDisc_RESPAAppReceived;
            dataLoan.sDisclosuresDueD = CDateTime.Create(DateTime.Today);

            // Act like the user pulled the initial disclosure docs with edisclosure.
            dataLoan.ProcessDocDisclosureTrigger(E_DisclosureT.InitialDisclosure, true /*isEDisclosed*/, false /*isEClosed*/, false /*isManualFulfillment*/, "PACKAGETYPE", LqbGrammar.DataTypes.DocumentIntegrationPackageName.Create("PACKAGE TYPE NAME").ForceValue());
            dataLoan.Save();

            Assert.AreEqual(false, dataLoan.sNeedInitialDisc, "Should not need initial disclosure");
            Assert.AreEqual(E_sDisclosureNeededT.AwaitingEConsent, dataLoan.sDisclosureNeededT, 
                "Should be awaiting E-Consent");
            Assert.AreEqual(true, dataLoan.sTilGfeOd.SameAs(CDateTime.Create(DateTime.Today)),
                "sTilGfeOd should be current date.");
        }

        /// <summary>
        /// Ensure that processing an initial disclosures draw (w/o e-disclosure)
        /// works as expected.
        /// </summary>
        [Test]
        public void DrawInitialDisclosuresNotEDisclosedTest()
        {
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            // Set up as if it needed initial disclosure
            dataLoan.sNeedInitialDisc = true;
            dataLoan.sDisclosureNeededT = E_sDisclosureNeededT.InitDisc_RESPAAppReceived;
            dataLoan.sDisclosuresDueD = CDateTime.Create(DateTime.Today);

            // Act like the user pulled the initial disclosure docs without edisclosure.
            dataLoan.ProcessDocDisclosureTrigger(E_DisclosureT.InitialDisclosure, false /*isEDisclosed*/, false /*isEClosed*/, true /* isManualFulfillment */, "PACKAGETYPE", LqbGrammar.DataTypes.DocumentIntegrationPackageName.Create("PACKAGE TYPE NAME").ForceValue());
            dataLoan.Save();

            Assert.AreEqual(false, dataLoan.sNeedInitialDisc, "Should not need initial disclosure");
            Assert.AreEqual(E_sDisclosureNeededT.None, dataLoan.sDisclosureNeededT, 
                "Should not have any disclosure needed.");
            Assert.AreEqual("", dataLoan.sDisclosuresDueD_rep, "Disclosure due date should be cleared.");
            Assert.AreEqual(true, dataLoan.sTilGfeOd.SameAs(CDateTime.Create(DateTime.Today)),
                "sTilGfeOd should be current date.");
        }

        /// <summary>
        /// Ensure that processing a redisclosures drawn trigger works
        /// as expected.
        /// </summary>
        [Test]
        public void DrawRedisclosuresTest()
        {
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            // Set up as if it needed re-disclosure
            dataLoan.sLastDisclosedD = CDateTime.Create(DateTime.Now);
            dataLoan.sNeedRedisc = true;
            dataLoan.sDisclosureNeededT = E_sDisclosureNeededT.CC_ProgramChanged;
            dataLoan.sDisclosuresDueD = CDateTime.Create(DateTime.Today);

            // Act like the user pulled the redisclosure docs.
            dataLoan.ProcessDocDisclosureTrigger(E_DisclosureT.Redisclosure, true /*isEDisclosed*/, false /*isEClosed*/, false /*isManualFulfillment*/, "PACKAGETYPE", LqbGrammar.DataTypes.DocumentIntegrationPackageName.Create("PACKAGE TYPE NAME").ForceValue());
            dataLoan.Save();

            Assert.AreEqual(false, dataLoan.sNeedRedisc, "Should not need redisclosure");
            Assert.AreEqual(E_sDisclosureNeededT.None, dataLoan.sDisclosureNeededT,
                "Should not have any disclosure needed.");
            Assert.AreEqual("", dataLoan.sDisclosuresDueD_rep, "Disclosure due date should be cleared.");
            Assert.AreEqual(true, dataLoan.sTilGfeOd.SameAs(CDateTime.Create(DateTime.Today)),
                "sTilGfeOd should be current date.");
        }


        /// <summary>
        /// Ensure that processing a manual disclosure event works as expected.
        /// </summary>
        [Test]
        public void ManualDisclosureTest()
        {
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            // Set all of the fields that this is supposed to reset.
            dataLoan.sNeedInitialDisc = true;
            dataLoan.sNeedRedisc = true;
            dataLoan.sDisclosureNeededT = E_sDisclosureNeededT.CC_ProgramChanged;
            dataLoan.sDisclosuresDueD = CDateTime.Create(DateTime.Today);

            dataLoan.ProcessManualDisclosureTrigger();
            dataLoan.Save();

            Assert.AreEqual(false, dataLoan.sNeedInitialDisc, "Should not need initial disclosure");
            Assert.AreEqual(false, dataLoan.sNeedRedisc, "Should not need redisclosure");
            Assert.AreEqual(E_sDisclosureNeededT.None, dataLoan.sDisclosureNeededT, 
                "Should not have any disclosure needed.");
            Assert.AreEqual("", dataLoan.sDisclosuresDueD_rep, "Disclosure due date should be cleared.");
        }

        /// <summary>
        /// Ensure that the disclosures due date cannot be extended once it has
        /// a valid value.
        /// </summary>
        [Test]
        public void DisclosureDueDateTest()
        {
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sDisclosuresDueD = CDateTime.Create(DateTime.Today);
            dataLoan.sDisclosuresDueD = dataLoan.sDisclosuresDueD.AddWorkingBankDays(2,
                !LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V28_DontRollSundayHolidaysToMonday));
            dataLoan.Save();

            Assert.AreEqual(true, dataLoan.sDisclosuresDueD.SameAs(CDateTime.Create(DateTime.Today)),
                "Once set to a valid value, the disclosure due date should not be extended.");
        }

        /// <summary>
        /// Ensure that processing a doc magic documents reviewed email notification
        /// trigger works as expected.
        /// </summary>
        [Test]
        public void DM_DocumentReviewCompletedTest()
        {
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            CAppData dataApp = dataLoan.GetAppData(0);
            FillOutBasicAppInfo(dataApp);
            
            var emailDetails = GetDocMagicEmailDetailsWithName("GEOFF FELTMAN");

            DateTime today = DateTime.Today;
            dataLoan.ProcessEDisclosureDisclosureTrigger(E_EDisclosureDisclosureEventT.DM_DocumentReviewCompleted,
                emailDetails, today);
            dataLoan.Save();

            Assert.AreEqual(true, today.Equals(dataLoan.sTilGfeRd.DateTimeForComputation),
                "sTilGfeRd should equal the date that the message was received.");
        }

        /// <summary>
        /// Ensure that processing an E-Consent Receieved email from DocMagic
        /// works as expected given a valid borrower name.
        /// </summary>
        [Test]
        public void DM_EConsentReceivedTest()
        {
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            CAppData dataApp = dataLoan.GetAppData(0);
            FillOutBasicAppInfo(dataApp);
            
            var emailDetails = GetDocMagicEmailDetailsWithName("GEOFF FELTMAN");

            DateTime now = DateTime.Now;
            
            dataLoan.ProcessEDisclosureDisclosureTrigger(E_EDisclosureDisclosureEventT.DM_EConsentReceived,
                emailDetails, now);
            dataLoan.Save();

            Assert.That(dataApp.aBEConsentReceivedD.IsValid,
                "aBEconsentReceivedD should be a valid date.");
            Assert.That(now.Date.Equals(dataApp.aBEConsentReceivedD.DateTimeForComputation),
                "E-Consent should be marked as received on correct date for the borrower.");
            Assert.That(now.Hour == dataApp.aBEConsentReceivedD.DateTimeForComputationWithTime.Hour,
                "E-Consent should be marked as received at correct hour for the borrower.");
            Assert.That(now.Minute == dataApp.aBEConsentReceivedD.DateTimeForComputationWithTime.Minute,
                "E-Consent should be marked as received at correct minute for the borrower.");
            Assert.That(now.Second == dataApp.aBEConsentReceivedD.DateTimeForComputationWithTime.Second,
                "E-Consent should be marked as received at correct second for the borrower.");
            Assert.AreEqual(false, dataLoan.sEConsentCompleted,
                "E-Consent should not be marked as received becuase it hasn't "
                + " been received for the co-borrower.");
        }

        /// <summary>
        /// Ensure that processing an LQBESignUpdate for E-Consent Received works
        /// as expected for a valid borrower name.
        /// </summary>
        [Test]
        public void ESignUpdateEConsentReceivedTest()
        {
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            
            CAppData dataApp = dataLoan.GetAppData(0);
            FillOutBasicAppInfo(dataApp);

            var eSignUpdate = GetLQBESignUpdateWithMessage("E-Consent Received for GEOFF FELTMAN");

            dataLoan.ProcessEDisclosureDisclosureTrigger(E_EDisclosureDisclosureEventT.EConsentReceived, 
                eSignUpdate);

            dataLoan.Save();

            Assert.AreEqual(true, dataApp.aBEConsentReceivedD.IsValid,
                "There should be a valid value in aBEConsentReceivedD");
        }

        [Test]
        public void ESignUpdateEConsentReceivedForAllBorrowersTest()
        {
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            CAppData dataApp = dataLoan.GetAppData(0);
            FillOutBasicAppInfo(dataApp);

            dataLoan.sNeedInitialDisc = true;
            dataLoan.sDisclosureNeededT = E_sDisclosureNeededT.InitDisc_RESPAAppReceived;
            dataLoan.sDisclosuresDueD = CDateTime.Create(DateTime.Today);

            // Act like the user pulled the initial disclosure docs with edisclosure.
            dataLoan.ProcessDocDisclosureTrigger(E_DisclosureT.InitialDisclosure, true /*isEDisclosed*/, false /*isEClosed*/, false /*isManualFulfillment*/, "PACKAGETYPE", LqbGrammar.DataTypes.DocumentIntegrationPackageName.Create("PACKAGE TYPE NAME").ForceValue());

            // Receiving first e-sign update.
            var eSignUpdate = GetLQBESignUpdateWithMessage("E-Consent Received for GEOFF FELTMAN");

            dataLoan.ProcessEDisclosureDisclosureTrigger(E_EDisclosureDisclosureEventT.EConsentReceived, 
                eSignUpdate);

            Assert.AreEqual(true, dataApp.aBEConsentReceivedD.IsValid);
            Assert.AreEqual(dataLoan.sEConsentCompleted, false);
            Assert.AreEqual(dataLoan.sNeedInitialDisc, false);
            Assert.AreEqual(dataLoan.sNeedRedisc, false);
            Assert.AreEqual(dataLoan.sDisclosureNeededT, E_sDisclosureNeededT.AwaitingEConsent);

            // Receiving e-consent for final borrower
            eSignUpdate = GetLQBESignUpdateWithMessage("E-Consent Received for OCTAVIUS CAESAR");

            dataLoan.ProcessEDisclosureDisclosureTrigger(E_EDisclosureDisclosureEventT.EConsentReceived, 
                eSignUpdate);

            Assert.AreEqual(true, dataApp.aCEConsentReceivedD.IsValid);
            Assert.AreEqual(true, dataLoan.sEConsentCompleted);
            Assert.AreEqual(false, dataLoan.sNeedInitialDisc);
            Assert.AreEqual(false, dataLoan.sNeedRedisc);
            Assert.AreEqual(E_sDisclosureNeededT.None, dataLoan.sDisclosureNeededT);
        }

        /// <summary>
        /// Ensure that the dates for sLoanEstimateDatesInfo and sClosingDisclosureDatesInfo get updated correctly.
        /// as expected for a valid borrower name.
        /// </summary>
        [Test]
        public void ESignDateReceivedTest()
        {
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            var eSignUpdate = GetLQBESignUpdateWithMessage("E-Consent Documents Reviewed for GEOFF FELTMAN");
            eSignUpdate.TransactionID = "e4420a7e-427f-4c30-873c-8a03058a8ad5";

            // The loan has no dates yet.  Check to make sure it doesn't crash when attempting to check the dates.
            dataLoan.ProcessEDisclosureDisclosureTrigger(E_EDisclosureDisclosureEventT.DocumentsReviewed,
                eSignUpdate);

            dataLoan.BrokerDB.TempOptionXmlContent = dataLoan.BrokerDB.TempOptionXmlContent.Value + "<option name=\"EnableBorrowerLevelDisclosureTracking\" value=\"true\" />";
            var metadata = new LoanEstimateDatesMetadata(dataLoan.GetClosingCostArchiveById, dataLoan.GetConsumerDisclosureMetadataByConsumerId(), dataLoan.sLoanRescindableT);
            
            LoanEstimateDates leDate = LoanEstimateDates.Create(metadata);
            leDate.TransactionId = "e4420a7e-427f-4c30-873c-8a03058a8ad5";
            dataLoan.sLoanEstimateDatesInfo.AddDates(leDate);
            dataLoan.sLoanEstimateDatesInfo.AddDates(LoanEstimateDates.Create(metadata));
            dataLoan.SaveLEandCDDates();

            dataLoan.ProcessEDisclosureDisclosureTrigger(E_EDisclosureDisclosureEventT.DocumentsReviewed,
                eSignUpdate);

            dataLoan.Save();

            Assert.AreEqual( dataLoan.sLoanEstimateDatesInfo.LoanEstimateDatesList[0].ReceivedDate, DateTime.Today);
            Assert.AreNotEqual(dataLoan.sLoanEstimateDatesInfo.LoanEstimateDatesList[1].ReceivedDate, DateTime.Today);
        }

        /// <summary>
        /// Ensure that processing an LQBESignUpdate for E-Consent Declined works
        /// as expected for a valid borrower name.
        /// </summary>
        [Test]
        public void ESignUpdateEConsentDeclinedTest()
        {
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            CAppData dataApp = dataLoan.GetAppData(0);
            FillOutBasicAppInfo(dataApp);

            var eSignUpdate = GetLQBESignUpdateWithMessage("E-Consent Declined by GEOFF FELTMAN");

            dataLoan.ProcessEDisclosureDisclosureTrigger(E_EDisclosureDisclosureEventT.EConsentDeclined,
                eSignUpdate);

            dataLoan.Save();

            Assert.AreEqual(true, dataLoan.sNeedInitialDisc,
                "Loan should need initial disclosure after econsent declined.");
            Assert.AreEqual(E_sDisclosureNeededT.EConsentDec_PaperDiscReqd, dataLoan.sDisclosureNeededT, 
                "Type of disclosure should indicate paper disclosure required.");
            Assert.AreEqual(true, dataApp.aBEConsentDeclinedD.IsValid,
                "There should be a valid date in ABEConsentDeclined");
        }

        [Test]
        public void ESignCompleted_NoLeCdDates_DoesNotGenerateError()
        {
            var esignUpdate = this.GetLQBESignUpdateWithMessage("E-SIGN COMPLETED");
            var loan = CreateDataObject();
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            loan.ProcessEDisclosureDisclosureTrigger(E_EDisclosureDisclosureEventT.ESignCompleted, esignUpdate);

            // Making it here without error => success.
        }

        [Test]
        public void ESignCompleted_NoMatchingLeDate_DoesNotGenerateError()
        {
            var esignUpdate = this.GetLQBESignUpdateWithMessage("E-SIGN COMPLETED");
            esignUpdate.TransactionID = new Guid("11111111-1111-1111-1111-111111111111").ToString();
            var loan = CreateDataObject();
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            loan.BrokerDB.TempOptionXmlContent = loan.BrokerDB.TempOptionXmlContent.Value + "<option name=\"EnableBorrowerLevelDisclosureTracking\" value=\"true\" />";
            var metadata = new LoanEstimateDatesMetadata(loan.GetClosingCostArchiveById, loan.GetConsumerDisclosureMetadataByConsumerId(), loan.sLoanRescindableT);
            var le = LoanEstimateDates.Create(metadata);
            le.TransactionId = Guid.Empty.ToString();
            loan.sLoanEstimateDatesInfo.AddDates(le);
            loan.SaveLEandCDDates();

            loan.ProcessEDisclosureDisclosureTrigger(E_EDisclosureDisclosureEventT.ESignCompleted, esignUpdate);

            // Making it here without error => success.
        }

        [Test]
        public void ESignCompleted_MatchingLeDateWithBlankSignedDate_PopulatesSignedDateToToday()
        {
            var transactionId = new Guid("00000000-0000-0000-0000-000000000001").ToString();
            var esignUpdate = this.GetLQBESignUpdateWithMessage("E-SIGN COMPLETED");
            esignUpdate.TransactionID = transactionId;
            var loan = CreateDataObject();
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            loan.BrokerDB.TempOptionXmlContent = loan.BrokerDB.TempOptionXmlContent.Value + "<option name=\"EnableBorrowerLevelDisclosureTracking\" value=\"true\" />";
            var metadata = new LoanEstimateDatesMetadata(loan.GetClosingCostArchiveById, loan.GetConsumerDisclosureMetadataByConsumerId(), loan.sLoanRescindableT);
            var le = LoanEstimateDates.Create(metadata);
            le.TransactionId = transactionId;
            loan.sLoanEstimateDatesInfo.AddDates(le);
            loan.SaveLEandCDDates();

            loan.ProcessEDisclosureDisclosureTrigger(E_EDisclosureDisclosureEventT.ESignCompleted, esignUpdate);

            var leFromCollection = loan.sLoanEstimateDatesInfo.LoanEstimateDatesList.Single(x => x.UniqueId == le.UniqueId);
            Assert.AreEqual(DateTime.Today, leFromCollection.SignedDate);
        }

        [Test]
        public void ESignCompleted_MatchingLeDateWithNonBlankLockedSignedDate_DoesNotOverwriteSignedDate()
        {
            var transactionId = new Guid("00000000-0000-0000-0000-000000000002").ToString();
            var esignUpdate = this.GetLQBESignUpdateWithMessage("E-SIGN COMPLETED");
            esignUpdate.TransactionID = transactionId;
            var loan = CreateDataObject();
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            loan.BrokerDB.TempOptionXmlContent = loan.BrokerDB.TempOptionXmlContent.Value + "<option name=\"EnableBorrowerLevelDisclosureTracking\" value=\"true\" />";
            var metadata = new LoanEstimateDatesMetadata(loan.GetClosingCostArchiveById, loan.GetConsumerDisclosureMetadataByConsumerId(), loan.sLoanRescindableT);
            var le = LoanEstimateDates.Create(metadata);
            le.TransactionId = transactionId;
            le.SignedDate = DateTime.Today.AddDays(-1);
            le.SignedDateLckd = true;
            loan.sLoanEstimateDatesInfo.AddDates(le);
            loan.SaveLEandCDDates();

            loan.ProcessEDisclosureDisclosureTrigger(E_EDisclosureDisclosureEventT.ESignCompleted, esignUpdate);

            var leFromCollection = loan.sLoanEstimateDatesInfo.LoanEstimateDatesList.Single(x => x.UniqueId == le.UniqueId);
            Assert.AreEqual(DateTime.Today.AddDays(-1), leFromCollection.SignedDate);
        }

        [Test]
        public void ESignCompleted_MatchingLeDateWithNonBlankBorrowerLevelSignedDate_DoesNotOverwriteSignedDate()
        {
            var transactionId = new Guid("00000000-0000-0000-0000-000000000002").ToString();
            var esignUpdate = this.GetLQBESignUpdateWithMessage("E-SIGN COMPLETED");
            esignUpdate.TransactionID = transactionId;
            var loan = CreateDataObject();
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            FillOutBasicAppInfo(loan.GetAppData(0));
            loan.BrokerDB.TempOptionXmlContent = loan.BrokerDB.TempOptionXmlContent.Value + "<option name=\"EnableBorrowerLevelDisclosureTracking\" value=\"true\" />";

            var metadata = new LoanEstimateDatesMetadata(loan.GetClosingCostArchiveById, loan.GetConsumerDisclosureMetadataByConsumerId(), loan.sLoanRescindableT);
            var le = LoanEstimateDates.Create(metadata);
            le.TransactionId = transactionId;
            le.DisclosureDatesByConsumerId.First().Value.SignedDate = DateTime.Today.AddDays(-1);
            loan.sLoanEstimateDatesInfo.AddDates(le);
            loan.SaveLEandCDDates();

            loan.ProcessEDisclosureDisclosureTrigger(E_EDisclosureDisclosureEventT.ESignCompleted, esignUpdate);

            var leFromCollection = loan.sLoanEstimateDatesInfo.LoanEstimateDatesList.Single(x => x.UniqueId == le.UniqueId);
            Assert.AreEqual(DateTime.Today.AddDays(-1), leFromCollection.SignedDate);
        }

        [Test]
        public void ESignCompleted_MatchingLeDateWithNonBlankLockedReceivedDate_DoesNotOverwriteSignedDate()
        {
            var transactionId = new Guid("00000000-0000-0000-0000-000000000002").ToString();
            var esignUpdate = this.GetLQBESignUpdateWithMessage("E-SIGN COMPLETED");
            esignUpdate.TransactionID = transactionId;
            var loan = CreateDataObject();
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            FillOutBasicAppInfo(loan.GetAppData(0));
            loan.BrokerDB.TempOptionXmlContent = loan.BrokerDB.TempOptionXmlContent.Value + "<option name=\"EnableBorrowerLevelDisclosureTracking\" value=\"true\" />";

            var metadata = new LoanEstimateDatesMetadata(loan.GetClosingCostArchiveById, loan.GetConsumerDisclosureMetadataByConsumerId(), loan.sLoanRescindableT);
            var le = LoanEstimateDates.Create(metadata);
            le.TransactionId = transactionId;
            le.ReceivedDate = DateTime.Today.AddDays(-1);
            le.ReceivedDateLckd = true;
            loan.sLoanEstimateDatesInfo.AddDates(le);
            loan.SaveLEandCDDates();

            loan.ProcessEDisclosureDisclosureTrigger(E_EDisclosureDisclosureEventT.ESignCompleted, esignUpdate);

            var leFromCollection = loan.sLoanEstimateDatesInfo.LoanEstimateDatesList.Single(x => x.UniqueId == le.UniqueId);
            Assert.AreEqual(DateTime.Today.AddDays(-1), leFromCollection.ReceivedDate);
        }

        [Test]
        public void ESignCompleted_MatchingLeDateWithNonBlankBorrowerLevelReceivedDate_DoesNotOverwriteSignedDate()
        {
            var transactionId = new Guid("00000000-0000-0000-0000-000000000002").ToString();
            var esignUpdate = this.GetLQBESignUpdateWithMessage("E-SIGN COMPLETED");
            esignUpdate.TransactionID = transactionId;
            var loan = CreateDataObject();
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            FillOutBasicAppInfo(loan.GetAppData(0));
            loan.BrokerDB.TempOptionXmlContent = loan.BrokerDB.TempOptionXmlContent.Value + "<option name=\"EnableBorrowerLevelDisclosureTracking\" value=\"true\" />";

            var metadata = new LoanEstimateDatesMetadata(loan.GetClosingCostArchiveById, loan.GetConsumerDisclosureMetadataByConsumerId(), loan.sLoanRescindableT);
            var le = LoanEstimateDates.Create(metadata);
            le.TransactionId = transactionId;
            le.DisclosureDatesByConsumerId.First().Value.ReceivedDate = DateTime.Today.AddDays(-1);
            loan.sLoanEstimateDatesInfo.AddDates(le);
            loan.SaveLEandCDDates();

            loan.ProcessEDisclosureDisclosureTrigger(E_EDisclosureDisclosureEventT.ESignCompleted, esignUpdate);

            var leFromCollection = loan.sLoanEstimateDatesInfo.LoanEstimateDatesList.Single(x => x.UniqueId == le.UniqueId);
            Assert.AreEqual(DateTime.Today.AddDays(-1), leFromCollection.ReceivedDate);
        }

        [Test]
        public void ESignCompleted_NoMatchingCdDate_DoesNotGenerateError()
        {
            var esignUpdate = this.GetLQBESignUpdateWithMessage("E-SIGN COMPLETED");
            esignUpdate.TransactionID = new Guid("11111111-1111-1111-1111-111111111111").ToString();
            var loan = CreateDataObject();
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            FillOutBasicAppInfo(loan.GetAppData(0));
            loan.BrokerDB.TempOptionXmlContent = loan.BrokerDB.TempOptionXmlContent.Value + "<option name=\"EnableBorrowerLevelDisclosureTracking\" value=\"true\" />";

            var metadata = new ClosingDisclosureDatesMetadata(
                loan.GetClosingCostArchiveById, 
                loan.GetConsumerDisclosureMetadataByConsumerId(), 
                loan.sLoanRescindableT, 
                loan.sAlwaysRequireAllBorrowersToReceiveClosingDisclosure);

            var cd = ClosingDisclosureDates.Create(metadata);
            cd.TransactionId = Guid.Empty.ToString();
            loan.sClosingDisclosureDatesInfo.AddDates(cd);
            loan.SaveLEandCDDates();

            loan.ProcessEDisclosureDisclosureTrigger(E_EDisclosureDisclosureEventT.ESignCompleted, esignUpdate);

            // Making it here without error => success.
        }

        [Test]
        public void ESignCompleted_MatchingCdDateWithBlankSignedDate_PopulatesSignedDateToToday()
        {
            var transactionId = new Guid("00000000-0000-0000-0000-000000000001").ToString();
            var esignUpdate = this.GetLQBESignUpdateWithMessage("E-SIGN COMPLETED");
            esignUpdate.TransactionID = transactionId;
            var loan = CreateDataObject();
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            FillOutBasicAppInfo(loan.GetAppData(0));
            loan.BrokerDB.TempOptionXmlContent = loan.BrokerDB.TempOptionXmlContent.Value + "<option name=\"EnableBorrowerLevelDisclosureTracking\" value=\"true\" />";

            var metadata = new ClosingDisclosureDatesMetadata(
                loan.GetClosingCostArchiveById,
                loan.GetConsumerDisclosureMetadataByConsumerId(),
                loan.sLoanRescindableT,
                loan.sAlwaysRequireAllBorrowersToReceiveClosingDisclosure);

            var cd = ClosingDisclosureDates.Create(metadata);
            cd.TransactionId = transactionId;
            loan.sClosingDisclosureDatesInfo.AddDates(cd);
            loan.SaveLEandCDDates();

            loan.ProcessEDisclosureDisclosureTrigger(E_EDisclosureDisclosureEventT.ESignCompleted, esignUpdate);

            var cdFromCollection = loan.sClosingDisclosureDatesInfo.ClosingDisclosureDatesList.Single(x => x.UniqueId == cd.UniqueId);
            Assert.AreEqual(DateTime.Today, cdFromCollection.SignedDate);
        }

        [Test]
        public void ESignCompleted_MatchingCdDateWithNonBlankLockedSignedDate_DoesNotOverwriteSignedDate()
        {
            var transactionId = new Guid("00000000-0000-0000-0000-000000000002").ToString();
            var esignUpdate = this.GetLQBESignUpdateWithMessage("E-SIGN COMPLETED");
            esignUpdate.TransactionID = transactionId;
            var loan = CreateDataObject();
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            FillOutBasicAppInfo(loan.GetAppData(0));
            loan.BrokerDB.TempOptionXmlContent = loan.BrokerDB.TempOptionXmlContent.Value + "<option name=\"EnableBorrowerLevelDisclosureTracking\" value=\"true\" />";

            var metadata = new ClosingDisclosureDatesMetadata(
                loan.GetClosingCostArchiveById,
                loan.GetConsumerDisclosureMetadataByConsumerId(),
                loan.sLoanRescindableT,
                loan.sAlwaysRequireAllBorrowersToReceiveClosingDisclosure);

            var cd = ClosingDisclosureDates.Create(metadata);
            cd.TransactionId = transactionId;
            cd.SignedDate = DateTime.Today.AddDays(-1);
            cd.SignedDateLckd = true;
            loan.sClosingDisclosureDatesInfo.AddDates(cd);
            loan.SaveLEandCDDates();

            loan.ProcessEDisclosureDisclosureTrigger(E_EDisclosureDisclosureEventT.ESignCompleted, esignUpdate);

            var cdFromCollection = loan.sClosingDisclosureDatesInfo.ClosingDisclosureDatesList.Single(x => x.UniqueId == cd.UniqueId);
            Assert.AreEqual(DateTime.Today.AddDays(-1), cdFromCollection.SignedDate);
        }

        [Test]
        public void ESignCompleted_MatchingCdDateWithNonBlankBorrowerLevelSignedDate_DoesNotOverwriteSignedDate()
        {
            var transactionId = new Guid("00000000-0000-0000-0000-000000000002").ToString();
            var esignUpdate = this.GetLQBESignUpdateWithMessage("E-SIGN COMPLETED");
            esignUpdate.TransactionID = transactionId;
            var loan = CreateDataObject();
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            FillOutBasicAppInfo(loan.GetAppData(0));
            loan.BrokerDB.TempOptionXmlContent = loan.BrokerDB.TempOptionXmlContent.Value + "<option name=\"EnableBorrowerLevelDisclosureTracking\" value=\"true\" />";

            var metadata = new ClosingDisclosureDatesMetadata(
                loan.GetClosingCostArchiveById,
                loan.GetConsumerDisclosureMetadataByConsumerId(),
                LoanRescindableT.NotRescindable,
                alwaysRequireAllBorrowersToReceiveClosingDisclosure: false);

            var cd = ClosingDisclosureDates.Create(metadata);
            cd.TransactionId = transactionId;
            cd.DisclosureDatesByConsumerId.First().Value.SignedDate = DateTime.Today.AddDays(-1);

            loan.sClosingDisclosureDatesInfo.AddDates(cd);
            loan.SaveLEandCDDates();

            loan.ProcessEDisclosureDisclosureTrigger(E_EDisclosureDisclosureEventT.ESignCompleted, esignUpdate);

            var cdFromCollection = loan.sClosingDisclosureDatesInfo.ClosingDisclosureDatesList.Single(x => x.UniqueId == cd.UniqueId);
            Assert.AreEqual(DateTime.Today.AddDays(-1), cdFromCollection.SignedDate);
        }

        [Test]
        public void ESignCompleted_MatchingCdDateWithNonBlankLockedReceiveDate_DoesNotOverwriteSignedDate()
        {
            var transactionId = new Guid("00000000-0000-0000-0000-000000000002").ToString();
            var esignUpdate = this.GetLQBESignUpdateWithMessage("E-SIGN COMPLETED");
            esignUpdate.TransactionID = transactionId;
            var loan = CreateDataObject();
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            FillOutBasicAppInfo(loan.GetAppData(0));
            loan.BrokerDB.TempOptionXmlContent = loan.BrokerDB.TempOptionXmlContent.Value + "<option name=\"EnableBorrowerLevelDisclosureTracking\" value=\"true\" />";

            var metadata = new ClosingDisclosureDatesMetadata(
                loan.GetClosingCostArchiveById,
                loan.GetConsumerDisclosureMetadataByConsumerId(),
                LoanRescindableT.NotRescindable,
                false);

            var cd = ClosingDisclosureDates.Create(metadata);
            cd.TransactionId = transactionId;
            cd.ReceivedDate = DateTime.Today.AddDays(-1);
            cd.ReceivedDateLckd = true;
            loan.sClosingDisclosureDatesInfo.AddDates(cd);
            loan.SaveLEandCDDates();

            loan.ProcessEDisclosureDisclosureTrigger(E_EDisclosureDisclosureEventT.ESignCompleted, esignUpdate);

            var cdFromCollection = loan.sClosingDisclosureDatesInfo.ClosingDisclosureDatesList.Single(x => x.UniqueId == cd.UniqueId);
            Assert.AreEqual(DateTime.Today.AddDays(-1), cdFromCollection.ReceivedDate);
        }

        [Test]
        public void ESignCompleted_MatchingCdDateWithNonBlankBorrowerLevelReceivedDate_DoesNotOverwriteSignedDate()
        {
            var transactionId = new Guid("00000000-0000-0000-0000-000000000002").ToString();
            var esignUpdate = this.GetLQBESignUpdateWithMessage("E-SIGN COMPLETED");
            esignUpdate.TransactionID = transactionId;
            var loan = CreateDataObject();
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            FillOutBasicAppInfo(loan.GetAppData(0));
            loan.BrokerDB.TempOptionXmlContent = loan.BrokerDB.TempOptionXmlContent.Value + "<option name=\"EnableBorrowerLevelDisclosureTracking\" value=\"true\" />";

            var metadata = new ClosingDisclosureDatesMetadata(
                loan.GetClosingCostArchiveById,
                loan.GetConsumerDisclosureMetadataByConsumerId(),
                LoanRescindableT.NotRescindable,
                false);

            var cd = ClosingDisclosureDates.Create(metadata);
            cd.TransactionId = transactionId;
            cd.DisclosureDatesByConsumerId.First().Value.ReceivedDate = DateTime.Today.AddDays(-1);

            loan.sClosingDisclosureDatesInfo.AddDates(cd);
            loan.SaveLEandCDDates();

            loan.ProcessEDisclosureDisclosureTrigger(E_EDisclosureDisclosureEventT.ESignCompleted, esignUpdate);

            var cdFromCollection = loan.sClosingDisclosureDatesInfo.ClosingDisclosureDatesList.Single(x => x.UniqueId == cd.UniqueId);
            Assert.AreEqual(DateTime.Today.AddDays(-1), cdFromCollection.ReceivedDate);
        }

        /// <summary>
        /// Ensure that we don't fail horribly when the given name is either
        /// of an unexpected format or doesn't match a borrower on the file.
        /// </summary>
        [Test]
        public void DontFailHorriblyWhenCantParseNameTest()
        {
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            CAppData dataApp = dataLoan.GetAppData(0);
            FillOutBasicAppInfo(dataApp);

            // Test name that isn't on file.
            var eSignUpdate = GetLQBESignUpdateWithMessage("E-Consent Declined by Tequila Joe.");
            dataLoan.ProcessEDisclosureDisclosureTrigger(E_EDisclosureDisclosureEventT.EConsentDeclined,
                eSignUpdate);

            // Test unexpected format.
            eSignUpdate.ESignNotification = "OH HAI THIS DOESN'T FOLLOW THE CORRECT FORMAT";
            dataLoan.ProcessEDisclosureDisclosureTrigger(E_EDisclosureDisclosureEventT.EConsentDeclined,
                eSignUpdate);

            // Test name not on file.
            var emailDetails = GetDocMagicEmailDetailsWithName("Tequila Joe");
            dataLoan.ProcessEDisclosureDisclosureTrigger(E_EDisclosureDisclosureEventT.DM_EConsentReceived,
                emailDetails, DateTime.Now);

            // Making it to the end is the victory.
        }

        /// <summary>
        /// Ensure that the currently hard-coded list of dependencies for the 
        /// RESPA triggers is indeed what is needed. 
        /// </summary>
        /// <remarks>
        /// This test exists becuase we need to make sure that the fields
        /// for disclosures (sNeedInitialDisc, sDisclosuresDueD, etc...) are
        /// loaded EVERY time that the workflow trigger is going to be evaluated.
        /// The field dependencies are taken care of in PageData.cs and 
        /// FieldInfo.cs
        /// </remarks>
        [Test]
        public void WorkflowTriggerDependenciesAreInSyncTest()
        {
            // Want to make sure that the field dependencies are in order.
            IConfigRepository configRepository = ConfigHandler.GetRepository(ConstAppDavid.SystemBrokerGuid);
            var sysEngine = ExecutingEngine.GetEngineByBrokerId(configRepository, ConstAppDavid.SystemBrokerGuid);

            Assert.That(sysEngine.HasCustomVariable(ConstStage.DisclosureWorkflowRESPATriggerName),
                "The RESPA trigger must be found in order for the workflow trigger to work!");

            var triggerDependencyList = sysEngine.GetDependencyVariableListForTrigger(
                ConstStage.DisclosureWorkflowRESPATriggerName);
            foreach (var field in triggerDependencyList)
            {
                Assert.That(ConstApp.DisclosureWorkflowFieldDependencies.Contains(field, StringComparer.OrdinalIgnoreCase), 
                    "The hard-coded dependency list must contain all of the fields dependent upon the RESPA trigger.");
            }

            // These are the actual fields that will need to be loaded in
            // case the trigger evaluates to true.
            var disclosureFields = new string[]
            {
                "sNeedInitialDisc",
                "sDisclosureNeededT",
                "sDisclosuresDueD",
                "sTilGfeDueD"
            };

            foreach (var field in disclosureFields)
            {
                Assert.That(ConstApp.DisclosureWorkflowFieldDependencies.Contains(field, StringComparer.OrdinalIgnoreCase),
                    "The hard-coded dependency list must contain all of the fields dependent upon the RESPA trigger.");
            }
        }

        /// <summary>
        /// Ensure that fulfulling one of the RESPA triggers causes the trigger
        /// to fire and that it is handled correctly.
        /// </summary>
        [Test]
        public void BasicWorkflowTriggerTest()
        {
            SetupLoanForWorkflowTriggerTest(false);
            CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(m_sLId,
                typeof(DisclosureAutomationTest));
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            ValidateWorkflowTrigger(dataLoan, false);

            dataLoan.sApprVal = 400000.00M;
            dataLoan.GetAppData(0).aBBaseI = 1.00M;
            dataLoan.Save();

            ValidateWorkflowTrigger(dataLoan, true);
        }

        /// <summary>
        /// Ensure that even when the user is not using smart dependencies
        /// that the required fields will be loaded.
        /// </summary>
        /// <remarks>
        /// This is probably tested elsewhere, but I just wanted to verify here
        /// for peace of mind.
        /// </remarks>
        [Test]
        public void NotSmartDependencyWorkflowTest()
        {
            SetupLoanForWorkflowTriggerTest(true);
            
            CPageData notSmartDependency = new CPageData(m_sLId, new string[] { "aBBaseI" });
            notSmartDependency.InitSave(ConstAppDavid.SkipVersionCheck);

            ValidateWorkflowTrigger(notSmartDependency, false);

            notSmartDependency.GetAppData(0).aBBaseI = 1.00M;
            notSmartDependency.Save();

            ValidateWorkflowTrigger(notSmartDependency, true);
        }

        /// <summary>
        /// Ensure that the workflow trigger will not fire if the loan has already
        /// been marked as disclosed. The workflow trigger is meant to indicate 
        /// when a file needs initial disclosure, which doesn't make sense if the
        /// loan has already been disclosed.
        /// </summary>
        [Test]
        public void InitialDisclosureAfterDisclosedTest()
        {
            SetupLoanForWorkflowTriggerTest(false);
            CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(m_sLId,
                typeof(DisclosureAutomationTest));
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sApprVal = 400000.00M;
            dataLoan.GetAppData(0).aBBaseI = 1.00M;
            dataLoan.sLastDisclosedD = CDateTime.Create(DateTime.Now);
            dataLoan.Save();

            ValidateWorkflowTrigger(dataLoan, false);
        }

        /// <summary>
        /// Validate that the workflow trigger set the disclosure fields correctly.
        /// </summary>
        /// <param name="dataLoan">The loan to test.</param>
        /// <param name="shouldHaveFired">
        /// If true, it expects the field changes to be in place. Else, it expects
        /// the disclosure fields to have their default values.
        /// </param>
        private void ValidateWorkflowTrigger(CPageData dataLoan, bool shouldHaveFired)
        {
            if (shouldHaveFired)
            {
                Assert.That(dataLoan.sNeedInitialDisc == true, "Loan should need initial disclosure.");
                Assert.That(dataLoan.sDisclosureNeededT == E_sDisclosureNeededT.InitDisc_RESPAAppReceived,
                    "Loan should indicate that RESPA application was received.");
                Assert.That(dataLoan.sDisclosuresDueD.IsValid && dataLoan.sDisclosuresDueD.SameAs(dataLoan.sTilGfeDueD),
                    "Loan should have valid disclosures due date, and it should matche sTilGfeDueD");
            }
            else
            {
                Assert.That(dataLoan.sNeedInitialDisc == false,
                "If the loan has been disclosed, initial disclosure triggers shouldn't fire.");
                Assert.That(dataLoan.sDisclosureNeededT == E_sDisclosureNeededT.None,
                    "If the loan has been disclosed, initial disclosure triggers shouldn't fire.");
                Assert.That(!dataLoan.sDisclosuresDueD.IsValid, "Shouldn't have valid disclosures due date.");
            }
        }

        /// <summary>
        /// Ensure that setting trigger fields doesn't mess with marking the loan
        /// as disclosed. Also ensure that the triggers can still fire after the
        /// save that involved marking the loan as disclosed.
        /// </summary>
        [Test]
        public void DontOverwriteDisclosureValuesWhenMarkingLoanDisclosed()
        {
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sLpTemplateNmSubmitted = "TEST NM";
            dataLoan.ProcessManualDisclosureTrigger();
            dataLoan.sLpTemplateNmSubmitted = "OH HAI AGAIN";
            dataLoan.Save();

            Assert.That(dataLoan.sNeedInitialDisc == false, "Should not need initial disclosure");
            Assert.That(dataLoan.sNeedRedisc == false, "Should not need redisclosure");
            Assert.That(!dataLoan.sDisclosuresDueD.IsValid, "Should not have disclosures due date.");
            Assert.That(dataLoan.sDisclosureNeededT == E_sDisclosureNeededT.None, 
                "Should not have disclosures due date.");

            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sLpTemplateNmSubmitted = "THIS ONE SHOULD WORK";
            dataLoan.Save();

            ValidateRedisclosureTrigger(dataLoan, E_sDisclosureNeededT.CC_ProgramChanged);
        }

        /// <summary>
        /// Verify field change initial disclosure trigger.
        /// </summary>
        [Test]
        public void TestWholesaleRegisteredTrigger()
        {
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            
            // Wholesale
            dataLoan.sIsBranchActAsLenderForFileTri = E_TriState.Yes;
            dataLoan.sIsBranchActAsOriginatorForFileTri = E_TriState.No;
            //dataLoan.sBranchChannelT = E_BranchChannelT.Wholesale;
            
            dataLoan.sStatusLckd = true;
            dataLoan.sStatusT = E_sStatusT.Loan_Registered;
            dataLoan.Save();

            Assert.That(dataLoan.sNeedInitialDisc == true, "Loan should need initial disclosure.");
            Assert.That(dataLoan.sDisclosureNeededT == E_sDisclosureNeededT.InitDisc_LoanRegistered,
                "Should need initials due to loan being registered.");
            Assert.That(dataLoan.sDisclosuresDueD.IsValid && dataLoan.sDisclosuresDueD.SameAs(dataLoan.sTilGfeDueD),
                "Loan should have valid disclosures due date, and it should matche sTilGfeDueD");
        }

        [Test]
        public void TestWholesaleRespaTrigger()
        {
            SetupLoanForWorkflowTriggerTest(false);
            CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(m_sLId,
                typeof(DisclosureAutomationTest));
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            // Wholesale
            dataLoan.sIsBranchActAsLenderForFileTri = E_TriState.Yes;
            dataLoan.sIsBranchActAsOriginatorForFileTri = E_TriState.No;

            ValidateWorkflowTrigger(dataLoan, false);

            dataLoan.sApprVal = 400000.00M;
            dataLoan.GetAppData(0).aBBaseI = 1.00M;
            dataLoan.Save();

            ValidateWorkflowTrigger(dataLoan, false);

            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sDocumentCheckD = CDateTime.Create(DateTime.Now);
            dataLoan.Save();

            ValidateWorkflowTrigger(dataLoan, false);

            dataLoan.BrokerDB.TempOptionXmlContent = dataLoan.BrokerDB.TempOptionXmlContent.Value + "<option name=\"UseRespaForNonRetailInitialDisclosures\" value=\"true\" />";
            dataLoan.BrokerDB.TempOptionXmlContent = dataLoan.BrokerDB.TempOptionXmlContent.Value + "<option name=\"ReqDocCheckDAndRespaForNonRetailInitialDisclosures\" value=\"true\" />";

            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sDocumentCheckD_rep = "";
            dataLoan.sStatusLckd = true;
            dataLoan.sStatusT = E_sStatusT.Loan_Registered;
            dataLoan.Save();

            Assert.That(!dataLoan.sNeedInitialDisc, "Should not trigger initials on registration.");

            ValidateWorkflowTrigger(dataLoan, false);

            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sDocumentCheckD = CDateTime.Create(DateTime.Now);
            dataLoan.Save();

            ValidateWorkflowTrigger(dataLoan, true);
        }

        [Test]
        public void TestWholesaleRespaTriggerAgain()
        {
            SetupLoanForWorkflowTriggerTest(false);
            CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(m_sLId,
                typeof(DisclosureAutomationTest));
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            // Wholesale
            dataLoan.sIsBranchActAsLenderForFileTri = E_TriState.Yes;
            dataLoan.sIsBranchActAsOriginatorForFileTri = E_TriState.No;
            dataLoan.BrokerDB.TempOptionXmlContent = "";

            ValidateWorkflowTrigger(dataLoan, false);

            dataLoan.sApprVal = 400000.00M;
            dataLoan.GetAppData(0).aBBaseI = 1.00M;
            dataLoan.Save();

            ValidateWorkflowTrigger(dataLoan, false);

            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sDocumentCheckD = CDateTime.Create(DateTime.Now);
            dataLoan.Save();

            ValidateWorkflowTrigger(dataLoan, false);

            dataLoan.BrokerDB.TempOptionXmlContent = dataLoan.BrokerDB.TempOptionXmlContent.Value + "<option name=\"UseRespaForNonRetailInitialDisclosures\" value=\"true\" />";

            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sDocumentCheckD_rep = "";
            dataLoan.Save();

            Assert.That(dataLoan.sNeedInitialDisc, "Should trigger initials on RESPA for wholesale.");

            ValidateWorkflowTrigger(dataLoan, true);
        }

        [Test]
        public void EConsentExpiredTest()
        {
            this.SetupLoanForWorkflowTriggerTest(false);

            var dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(m_sLId,
                typeof(DisclosureAutomationTest));
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            // Trigger RESPA
            ValidateWorkflowTrigger(dataLoan, false);
            dataLoan.sApprVal = 400000.00M;
            dataLoan.GetAppData(0).aBBaseI = 1.00M;
            dataLoan.Save();
            ValidateWorkflowTrigger(dataLoan, true);

            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            // Now draw e-disclosed docs
            dataLoan.ProcessDocDisclosureTrigger(
                E_DisclosureT.InitialDisclosure,
                true,
                false,
                false,
                "MEOW",
                LqbGrammar.DataTypes.DocumentIntegrationPackageName.Create("MEOWZA").ForceValue());
            Assert.That(!dataLoan.sNeedInitialDisc && !dataLoan.sNeedRedisc);

            // Now expire the e-consent.
            dataLoan.ProcessEDisclosureDisclosureTrigger(E_EDisclosureDisclosureEventT.EConsentExpired);
            Assert.That(dataLoan.sNeedInitialDisc, "E-consent expired should now be treated as an initial disclosure trigger.");

            var disclosuresDueD = CDateTime.Create(DateTime.Today.AddDays(-3));
            dataLoan.sDisclosuresDueD_rep = "";
            dataLoan.sDisclosuresDueD = disclosuresDueD;

            // Now trigger a field change changed circumstance.
            dataLoan.ProcessDelayedRedisclosureTrigger(E_sDisclosureNeededT.CC_LoanAmtChanged, Guid.Empty, "MEOW");
            Assert.That(dataLoan.sNeedInitialDisc && !dataLoan.sNeedRedisc, "Loan should still be marked as needing initials.");
            Assert.That(dataLoan.sDisclosureNeededT == E_sDisclosureNeededT.EConsentNotReceived_PaperDiscReqd,
                "The file should still be marked as e-consent expired.");

            // Trigger an APR re-disclosure change.
            dataLoan.sLastDiscAPR = 3;
            dataLoan.sNoteIR = 5.0m;
            dataLoan.sTerm = 360;
            dataLoan.sDue = 360;
            dataLoan.sSchedDueD1 = CDateTime.Create(DateTime.Now);
            dataLoan.Save();
            Assert.That(dataLoan.sNeedInitialDisc && !dataLoan.sNeedRedisc, "Loan should still be marked as needing initials.");
            Assert.That(dataLoan.sDisclosureNeededT == E_sDisclosureNeededT.EConsentNotReceived_PaperDiscReqd,
                "The file should still be marked as e-consent expired.");

            // Now trigger a rate lock changed circumstance.
            dataLoan.LockRate("MICKEYMOUSE", "YUP", false);
            Assert.That(dataLoan.sNeedInitialDisc && !dataLoan.sNeedRedisc, "Loan should still be marked as needing initials.");
            Assert.That(dataLoan.sDisclosureNeededT == E_sDisclosureNeededT.EConsentNotReceived_PaperDiscReqd,
                "The file should still be marked as e-consent expired.");

            Assert.That(dataLoan.sDisclosuresDueD.SameAs(disclosuresDueD), "The disclosures due date should not be changed.");
        }

        [Test]
        public void TestAprChangedCircumstanceInteraction()
        {
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            // OPM 236812 - Redisclosure only needed for non-TRID loans.
            dataLoan.sDisclosureRegulationTLckd = true;
            dataLoan.sDisclosureRegulationT = E_sDisclosureRegulationT.GFE;


            dataLoan.Save();

            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sLastDisclosedD = CDateTime.Create(DateTime.Now);
            dataLoan.Save();

            Assert.That(!dataLoan.sNeedInitialDisc && !dataLoan.sNeedRedisc,
                "Shouldn't have worked because APR is invalid.");

            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sLAmtLckd = true;
            dataLoan.sLAmtCalc = 100.0m;
            dataLoan.Save();

            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sLastDiscAPR = 15.00m;
            dataLoan.sNoteIR = 5.0m;
            dataLoan.sTerm = 360;
            dataLoan.sDue = 360;
            dataLoan.sSchedDueD1 = CDateTime.Create(DateTime.Now);
            dataLoan.Save();

            this.ValidateRedisclosureTrigger(dataLoan, E_sDisclosureNeededT.APROutOfTolerance_RediscReqd);

            var aprDueD = dataLoan.sDisclosuresDueD;

            // Now trigger changed circumstance.
            dataLoan.LockRate("MICKEYMOUSE", "YUP", false);
            this.ValidateRedisclosureTrigger(dataLoan, E_sDisclosureNeededT.CC_LoanLocked);
            var changedCircumstanceDueD = dataLoan.sDisclosuresDueD;

            // Log if the value was extended.
            Tools.LogInfo(string.Format("APR Due Date: {0}, Changed Circumstance Due Date: {1}", aprDueD, changedCircumstanceDueD));
        }

        [Test]
        public void AprTriggerShouldNotExtendCCDueDate()
        {
            var dataLoan = this.CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            dataLoan.sLastDisclosedD = CDateTime.Create(DateTime.Now);
            dataLoan.ProcessDelayedRedisclosureTrigger(E_sDisclosureNeededT.CC_LoanAmtChanged, Guid.Empty, "MEOW");
            this.ValidateRedisclosureTrigger(dataLoan, E_sDisclosureNeededT.CC_LoanAmtChanged);

            var disclosuresDueD = CDateTime.Create(DateTime.Today.AddDays(-3));
            dataLoan.sDisclosuresDueD_rep = "";
            dataLoan.sDisclosuresDueD = disclosuresDueD;

            // Trigger an APR re-disclosure
            dataLoan.sLastDiscAPR = 1.00m;
            dataLoan.Save();

            // Trigger
            Assert.That(dataLoan.sNeedRedisc);
            Assert.That(dataLoan.sDisclosuresDueD.SameAs(disclosuresDueD), "The due date should not have moved.");
        }

        /// <summary>
        /// Set up the base respa trigger on the loan. This requires that the 
        /// borrower name & ssn be non-blank, sSpAddr is valid, and sLAmtCalc 
        /// is non-zero.
        /// </summary>
        private void SetupLoanForWorkflowTriggerTest(bool setAppraisedValue)
        {   
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            FillOutBasicAppInfo(dataLoan.GetAppData(0));

            // Retail
            dataLoan.sIsBranchActAsLenderForFileTri = E_TriState.Yes;
            dataLoan.sIsBranchActAsOriginatorForFileTri = E_TriState.Yes;
            //dataLoan.sBranchChannelT = E_BranchChannelT.Retail;
            dataLoan.sSpAddr = "-";
            dataLoan.sLAmtLckd = true;
            dataLoan.sLAmtCalc = 500000.00M;
            
            if (setAppraisedValue)
            {
                dataLoan.sApprVal = 400000.00M;
            }

            dataLoan.Save();
        }

        private void FillOutBasicAppInfo(CAppData dataApp)
        {
            dataApp.aBFirstNm = "GEOFF";
            dataApp.aBLastNm = "FELTMAN";
            dataApp.aBSsn = "000-00-0000";

            dataApp.aCFirstNm = "OCTAVIUS";
            dataApp.aCLastNm = "CAESAR";
            dataApp.aCSsn = "111-11-1111";
        }

        private DocMagicEmailDetails GetDocMagicEmailDetailsWithName(string name)
        {
            return new DocMagicEmailDetails(
                "ACCOUNT NUMBER",
                "LOAN NUMBER",
                "WORKSHEET NUMBER",
                "PACKAGE TYPE",
                name);
        }

        private LQBESignUpdate GetLQBESignUpdateWithMessage(string message)
        {
            return new LQBESignUpdate
            {
                ESignNotification = message,
                TransactionID = ""
            };
        }

        public CPageData CreateDataObject()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(m_sLId,
                typeof(DisclosureAutomationTest));
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            // In the initial save, reset all of the non-disclosure fields.
            // We will need to reset all of the disclosure fields, and we 
            // don't want these changes to mess with that.
            CAppData dataApp = dataLoan.GetAppData(0);
            dataLoan.sLpTemplateNmSubmitted = ""; 
            dataLoan.sApprVal = 0.00M;
            dataLoan.sSpAddr = "";
            dataLoan.sLAmtLckd = true;
            dataLoan.sLAmtCalc = 0.00M;
            dataLoan.sLAmtLckd = false;
            dataApp.aBBaseI = 0.00M;
            dataLoan.sStatusLckd = true;
            dataLoan.sStatusT = E_sStatusT.Loan_Open;
            dataLoan.sNoteIR = 0;
            dataLoan.sLastDiscAPR = 0;
            dataLoan.sIsLineOfCredit = false;

            dataLoan.Save();
            
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            if (dataLoan.sIsRateLocked)
            {
                dataLoan.BreakRateLock("MICKEYMOUSE", "YUP");
            }
            dataLoan.sLastDisclosedD = CDateTime.InvalidWrapValue;
            dataLoan.sNeedInitialDisc = false;
            dataLoan.sNeedRedisc = false;
            dataLoan.sDisclosureNeededT = E_sDisclosureNeededT.None;
            dataLoan.sDisclosuresDueD_rep = "";
            dataLoan.sTilGfeOd_rep = "";
            dataLoan.sTilGfeRd_rep = "";
            

            dataApp = dataLoan.GetAppData(0);
            dataApp.aBEConsentReceivedD = CDateTime.InvalidWrapValue;
            dataApp.aCEConsentReceivedD = CDateTime.InvalidWrapValue;
            dataApp.aBEConsentDeclinedD = CDateTime.InvalidWrapValue;
            dataApp.aCEConsentDeclinedD = CDateTime.InvalidWrapValue;

            dataLoan.sLoanEstimateDatesInfo = LoanEstimateDatesInfo.Create();
            dataLoan.sClosingDisclosureDatesInfo = ClosingDisclosureDatesInfo.Create();
            dataLoan.SaveLEandCDDates();
            
            dataLoan.Save();
            
            return dataLoan;
        }
    }
}
