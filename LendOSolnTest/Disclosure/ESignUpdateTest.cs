﻿namespace LendOSolnTest.Disclosure
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using Fakes;
    using LendersOffice.Common.SerializationTypes;
    using LendersOffice.Constants;
    using LendersOffice.Integration.DocumentVendor.LQBESignUpdate;
    using LqbGrammar;
    using LqbGrammar.Queries;
    using NSubstitute;
    using NUnit.Framework;

    [TestFixture]
    public class ESignUpdateTest
    {
        [TestFixtureTearDown]
        public void TearDown()
        {
            FakePageDataUtilities.Instance.Reset();
        }

        private static BorrowerSet Elvis = new BorrowerSet(new[] { NewName("Elvis", "Aaron", "Presley"), null, });
        private static BorrowerSet ElvisAndWoman = new BorrowerSet(new[] { NewName("Elvis", "Aaron", "Presley"), NewName("A", "woman", "duh"), });
        private static BorrowerSet ElvisAndEgo = new BorrowerSet(new[] { NewName("Elvis", "Aaron", "Presley"), null, NewName("Elvis", "\"The King\" Aaron", "Presley"), null, });
        private static BorrowerSet ElvisWomanAndEgo = new BorrowerSet(new[] { NewName("Elvis", "Aaron", "Presley"), NewName("A", "woman", "duh"), NewName("Elvis", "\"The King\" Aaron", "Presley"), null, });
        private static object[][] ESignUpdateEConsentReceivedOrDeclinedRawCases =
        {
            new object[] { 0, "A man duh", Elvis, null, },
            new object[] { 1, "A man duh", ElvisAndWoman, null, },
            new object[] { 2, "A man duh", ElvisAndEgo, null, },
            new object[] { 3, "A man duh", ElvisWomanAndEgo, null, },
            new object[] { 4, "Elvis Presley", Elvis, null, },
            new object[] { 5, "Elvis Presley", ElvisAndWoman, null, },
            new object[] { 6, "Elvis Presley", ElvisAndEgo, null, },
            new object[] { 7, "Elvis Presley", ElvisWomanAndEgo, null, },
            new object[] { 8, "A woman duh", Elvis, null, },
            new object[] { 9, "A woman duh", ElvisAndWoman, 1, },
            new object[] { 10, "Awoman duh", ElvisAndEgo, null, },
            new object[] { 11, "AWOman duh", ElvisWomanAndEgo, 1, },
            new object[] { 12, "Elvis AARON Presley", Elvis, 0, },
            new object[] { 13, "Elvis aaron Presley", ElvisAndWoman, 0, },
            new object[] { 14, "Elvis aaron. Presley", ElvisAndEgo, 0, },
            new object[] { 15, "Elvis AA-Ron Presley", ElvisWomanAndEgo, 0, },
            new object[] { 16, "Elvis \"The King\" AARON Presley", Elvis, null, },
            new object[] { 17, "Elvis The-King-Aaron Presley", ElvisAndWoman, null, },
            new object[] { 18, "Elvis \"The King\" aaron Presley.", ElvisAndEgo, 2, },
            new object[] { 19, "ElvisTheKing AA-Ron Presley", ElvisWomanAndEgo, 2, },
        };

        private static object[][] ProcessEDisclosureDisclosureTrigger_OriginalDisclosureStateValuesSource =
            (from needInitialDisclosuresValue in new[] { true, false }
             from needRedisclosuresValue in new[] { true, false }
             from disclosureNeededType in Enum.GetValues(typeof(E_sDisclosureNeededT)).Cast<E_sDisclosureNeededT>()
             where (needInitialDisclosuresValue && !needRedisclosuresValue) || (needRedisclosuresValue && !needInitialDisclosuresValue)
             select new object[] { needInitialDisclosuresValue, needRedisclosuresValue, disclosureNeededType }).ToArray();

        private static object[][] ESignUpdateEConsentReceivedOrDeclinedCases =
            ESignUpdateEConsentReceivedOrDeclinedRawCases.Select(test => InsertItem(test, 1, E_EDisclosureDisclosureEventT.EConsentReceived))
            .Concat(ESignUpdateEConsentReceivedOrDeclinedRawCases.Select(test => InsertItem(test, 1, E_EDisclosureDisclosureEventT.EConsentDeclined)))
            .ToArray(); // using techniques from https://bgrva.github.io/blog/2015/05/25/nunit-tricks-for-test-params/ to try to improve readability

        [Test, TestCaseSource(nameof(ESignUpdateEConsentReceivedOrDeclinedCases))]
        public void ESignUpdateEConsentReceivedOrDeclined_ParameterBorrower_UpdatesCorrectly(int testId, E_EDisclosureDisclosureEventT consentReceivedOrDeclined, string econsentFor, BorrowerSet borrowers, int? borrowerMatchIndex)
        {
            // Arguments
            if (consentReceivedOrDeclined != E_EDisclosureDisclosureEventT.EConsentReceived && consentReceivedOrDeclined != E_EDisclosureDisclosureEventT.EConsentDeclined)
            {
                throw new InvalidOperationException("Bad value for " + nameof(consentReceivedOrDeclined) + "; received " + consentReceivedOrDeclined);
            }

            // Arrange
            NameComponents[] borrowersOnFile = borrowers.Borrowers;
            int appCount = borrowersOnFile.Length / 2;
            CPageData loan = FakePageData.Create(appCount);
            loan.InitSave(LendersOffice.Constants.ConstAppDavid.SkipVersionCheck);
            for (int i = 0; i < appCount; ++i)
            {
                CAppData dataApp = loan.GetAppData(i);
                ApplyToBorrower(dataApp, borrowersOnFile[(2 * i)]);
                ApplyToCoBorrower(dataApp, borrowersOnFile[(2 * i) + 1]);
            }

            Guid transactionId = Guid.NewGuid();
            AddLoanEstimateDates(loan, Guid.NewGuid());
            AddLoanEstimateDates(loan, transactionId);
            AddLoanEstimateDates(loan, Guid.NewGuid());
            AddClosingDisclosureDates(loan, Guid.NewGuid());
            AddClosingDisclosureDates(loan, transactionId);
            AddClosingDisclosureDates(loan, Guid.NewGuid());
            loan.SaveLEandCDDates();
            string eventDescriptionPrefix = consentReceivedOrDeclined == E_EDisclosureDisclosureEventT.EConsentReceived ? "E-Consent Received for " : "E-Consent Declined by ";
            var eSignUpdate = GetLQBESignUpdateWithMessage(eventDescriptionPrefix + econsentFor, transactionId.ToString());

            // Act
            loan.ProcessEDisclosureDisclosureTrigger(consentReceivedOrDeclined, eSignUpdate);

            // Assert
            if (borrowerMatchIndex.HasValue)
            {
                bool isBorrowerMatch = borrowerMatchIndex.Value % 2 == 0;
                CAppData app = loan.GetAppData(borrowerMatchIndex.Value / 2);
                if (consentReceivedOrDeclined == E_EDisclosureDisclosureEventT.EConsentReceived)
                {
                    Assert.AreEqual(true, isBorrowerMatch ? app.aBEConsentReceivedD.IsValid : app.aCEConsentReceivedD.IsValid, "There should be a valid consent received date");

                    Assert.AreNotEqual(DateTime.Today, loan.sLoanEstimateDatesInfo.LoanEstimateDatesList[0].ReceivedDate);
                    Assert.AreEqual(DateTime.Today, loan.sLoanEstimateDatesInfo.LoanEstimateDatesList[1].ReceivedDate);
                    Assert.AreNotEqual(DateTime.Today, loan.sLoanEstimateDatesInfo.LoanEstimateDatesList[2].ReceivedDate);
                    Assert.AreNotEqual(DateTime.Today, loan.sClosingDisclosureDatesInfo.ClosingDisclosureDatesList[0].ReceivedDate);
                    Assert.AreNotEqual(DateTime.Today, loan.sClosingDisclosureDatesInfo.ClosingDisclosureDatesList[1].ReceivedDate); // Record is only updated if BrokerDB.RequireAllBorrowersToReceiveCD
                    Assert.AreNotEqual(DateTime.Today, loan.sClosingDisclosureDatesInfo.ClosingDisclosureDatesList[2].ReceivedDate);
                }
                else
                {
                    Assert.AreEqual(true, isBorrowerMatch ? app.aBEConsentDeclinedD.IsValid : app.aCEConsentDeclinedD.IsValid, "There should be a valid consent declined date");
                }
            }
            else
            {
                foreach (CAppData app in loan.Apps)
                {
                    Assert.AreEqual(false, app.aBEConsentReceivedD.IsValid, "There should not be a valid value in aBEConsentReceivedD");
                    Assert.AreEqual(false, app.aCEConsentReceivedD.IsValid, "There should not be a valid value in aCEConsentReceivedD");
                    Assert.AreEqual(false, app.aBEConsentDeclinedD.IsValid, "There should not be a valid value in aBEConsentDeclinedD");
                    Assert.AreEqual(false, app.aCEConsentDeclinedD.IsValid, "There should not be a valid value in aCEConsentDeclinedD");
                }

                foreach (var leDate in loan.sLoanEstimateDatesInfo.LoanEstimateDatesList)
                {
                    Assert.AreNotEqual(DateTime.Today, leDate.ReceivedDate, "The loan estimate record should not have a received date of today");
                }

                foreach (var row in loan.sClosingDisclosureDatesInfo.ClosingDisclosureDatesList)
                {
                    Assert.AreNotEqual(DateTime.Today, row.ReceivedDate, "The closing disclosure record should not have a received date of today");
                }
            }
        }

        /// <remarks>
        /// This test is here to document some funky behavior I noticed.  Namely, the doc vendor will not always pass us a
        /// good value for the event description with a borrower name we expect, but we will still fire the parsing code
        /// on the value we got.  Worse still, we'll try to parse the event information (e.g. "E-Consent Received") as a
        /// borrower name since our removal code failed.
        /// </remarks>
        [TestCase(new object[] { E_EDisclosureDisclosureEventT.EConsentReceived, })]
        [TestCase(new object[] { E_EDisclosureDisclosureEventT.EConsentDeclined, })]
        [Test]
        public void ESignUpdateEConsentEvent_NoForPortion_NoUpdates(E_EDisclosureDisclosureEventT eventType)
        {
            // Arrange
            CPageData loan = FakePageData.Create();
            loan.InitSave(LendersOffice.Constants.ConstAppDavid.SkipVersionCheck);
            CAppData dataApp = loan.GetAppData(0);
            ApplyToBorrower(dataApp, NewName("Elvis", "Aaron", "Presley"));
            ApplyToCoBorrower(dataApp, null);

            Guid transactionId = Guid.NewGuid();
            AddLoanEstimateDates(loan, Guid.NewGuid());
            AddLoanEstimateDates(loan, transactionId);
            AddLoanEstimateDates(loan, Guid.NewGuid());
            AddClosingDisclosureDates(loan, Guid.NewGuid());
            AddClosingDisclosureDates(loan, transactionId);
            AddClosingDisclosureDates(loan, Guid.NewGuid());
            loan.SaveLEandCDDates();
            string eventDescription = eventType == E_EDisclosureDisclosureEventT.EConsentReceived ? "E-Consent Received" : "E-Consent Declined";
            var eSignUpdate = GetLQBESignUpdateWithMessage(eventDescription, transactionId.ToString()); // Normally this would be ...for BorrowerName

            // Act
            loan.ProcessEDisclosureDisclosureTrigger(eventType, eSignUpdate);

            // Assert
            foreach (CAppData app in loan.Apps)
            {
                Assert.AreEqual(false, app.aBEConsentReceivedD.IsValid, "There should not be a valid value in aBEConsentReceivedD");
                Assert.AreEqual(false, app.aCEConsentReceivedD.IsValid, "There should not be a valid value in aCEConsentReceivedD");
                Assert.AreEqual(false, app.aBEConsentDeclinedD.IsValid, "There should not be a valid value in aBEConsentDeclinedD");
                Assert.AreEqual(false, app.aCEConsentDeclinedD.IsValid, "There should not be a valid value in aCEConsentDeclinedD");
            }

            foreach (var leDate in loan.sLoanEstimateDatesInfo.LoanEstimateDatesList)
            {
                Assert.AreNotEqual(DateTime.Today, leDate.ReceivedDate, "The loan estimate record should not have a received date of today");
            }

            foreach (var row in loan.sClosingDisclosureDatesInfo.ClosingDisclosureDatesList)
            {
                Assert.AreNotEqual(DateTime.Today, row.ReceivedDate, "The closing disclosure record should not have a received date of today");
            }
        }

        [Test]
        public void ESignUpdate_DocumentsSignedForNoOneInParticular_LESignedDateDoesNotUpdate()
        {
            // Arrange
            CPageData loan = FakePageData.Create(2);
            loan.InitSave(LendersOffice.Constants.ConstAppDavid.SkipVersionCheck);
            var app = loan.GetAppData(0);
            app.aBFirstNm = "Pat";
            app.aBLastNm = "Metheny";
            app = loan.GetAppData(1);
            app.aBFirstNm = "Chuck";
            app.aBLastNm = "Mangione";
            app.aCFirstNm = "Chuck";
            app.aCLastNm = "Testa";
            Guid transactionId = Guid.NewGuid();
            AddLoanEstimateDates(loan, Guid.NewGuid());
            AddLoanEstimateDates(loan, Guid.NewGuid());
            AddLoanEstimateDates(loan, transactionId);
            AddLoanEstimateDates(loan, Guid.NewGuid());
            loan.SaveLEandCDDates();

            var eSignUpdate = GetLQBESignUpdateWithMessage("Documents Signed", transactionId.ToString()); // Normally this would be ...by BorrowerName, but I've seen a prod Doc Vendor send this instead.

            // Act
            loan.ProcessEDisclosureDisclosureTrigger(E_EDisclosureDisclosureEventT.DocumentsSigned, eSignUpdate);

            // Assert
            foreach (var leDate in loan.sLoanEstimateDatesInfo.LoanEstimateDatesList)
            {
                Assert.AreNotEqual(DateTime.Today, leDate.SignedDate);
            }
        }

        [Test]
        public void ESignUpdate_DocumentsSignedForBorrower_LESignedDateUpdates()
        {
            // Arrange
            CPageData loan = FakePageData.Create(2);
            loan.InitSave(LendersOffice.Constants.ConstAppDavid.SkipVersionCheck);
            var app = loan.GetAppData(0);
            app.aBFirstNm = "Pat";
            app.aBLastNm = "Metheny";
            app = loan.GetAppData(1);
            app.aBFirstNm = "Chuck";
            app.aBLastNm = "Mangione";
            app.aCFirstNm = "Chuck";
            app.aCLastNm = "Testa";
            Guid transactionId = Guid.NewGuid();
            AddLoanEstimateDates(loan, Guid.NewGuid());
            AddLoanEstimateDates(loan, Guid.NewGuid());
            AddLoanEstimateDates(loan, transactionId);
            AddLoanEstimateDates(loan, Guid.NewGuid());
            loan.SaveLEandCDDates();

            var eSignUpdate = GetLQBESignUpdateWithMessage("Documents Signed for Chuck Mangione", transactionId.ToString());

            // Act
            loan.ProcessEDisclosureDisclosureTrigger(E_EDisclosureDisclosureEventT.DocumentsSigned, eSignUpdate);

            // Assert
            Assert.AreNotEqual(DateTime.Today, loan.sLoanEstimateDatesInfo.LoanEstimateDatesList[2].SignedDate); // Updating this would require BrokerDB.AutoSaveLeSignedDateWhenAnyBorrowerSigns
        }

        [Test, TestCase(true, false), TestCase(false, true)]
        public void ProcessEDisclosureDisclosureTrigger_EConsentDeclinedAutomationEnabled_SetsDisclosureStateChangeFields(bool needInitialDisclosure, bool needRedisclosure)
        {
            var loan = FakePageData.Create();
            loan.InitLoad();

            loan.sNeedInitialDisc = needInitialDisclosure;
            loan.sNeedRedisc = needRedisclosure;

            var app = loan.GetAppData(0);
            app.aBFirstNm = "Pat";
            app.aBLastNm = "Metheny";

            var eSignUpdate = GetLQBESignUpdateWithMessage("E-CONSENT DECLINED BY Pat Metheny");

            loan.ProcessEDisclosureDisclosureTrigger(E_EDisclosureDisclosureEventT.EConsentDeclined, eSignUpdate);

            Assert.AreEqual(DateTime.Today, app.aBEConsentDeclinedD.DateTimeForComputation.Date);
            Assert.IsTrue(loan.sNeedInitialDisc);
            Assert.IsFalse(loan.sNeedRedisc);
            Assert.AreEqual(E_sDisclosureNeededT.EConsentDec_PaperDiscReqd, loan.sDisclosureNeededT);
        }

        [Test, TestCaseSource(nameof(ProcessEDisclosureDisclosureTrigger_OriginalDisclosureStateValuesSource))]
        public void ProcessEDisclosureDisclosureTrigger_EConsentDeclinedAutomationDisabled_DoesNotSetDisclosureStateChangeFields(
            bool originalNeedInitialDisclosure,
            bool originalNeedRedisclosure,
            E_sDisclosureNeededT originalDisclosureNeededType)
        {
            var loan = FakePageData.Create();
            loan.InitLoad();

            loan.sNeedInitialDisc = originalNeedInitialDisclosure;
            loan.sNeedRedisc = originalNeedRedisclosure;
            loan.sDisclosureNeededT = originalDisclosureNeededType;

            var app = loan.GetAppData(0);
            app.aBFirstNm = "Pat";
            app.aBLastNm = "Metheny";

            var eSignUpdate = GetLQBESignUpdateWithMessage("E-CONSENT DECLINED BY Pat Metheny");

            loan.DisableEConsentMonitoringAutomation();
            loan.ProcessEDisclosureDisclosureTrigger(E_EDisclosureDisclosureEventT.EConsentDeclined, eSignUpdate);

            Assert.AreEqual(originalNeedInitialDisclosure, loan.sNeedInitialDisc);
            Assert.AreEqual(originalNeedRedisclosure, loan.sNeedRedisc);
            Assert.AreEqual(originalDisclosureNeededType, loan.sDisclosureNeededT);
        }

        [Test, TestCase(true, false), TestCase(false, true)]
        public void ProcessEDisclosureDisclosureTrigger_EConsentExpiredAutomationEnabled_SetsDisclosureStateChangeFields(bool needInitialDisclosure, bool needRedisclosure)
        {
            var loan = FakePageData.Create();
            loan.InitLoad();

            loan.sNeedInitialDisc = needInitialDisclosure;
            loan.sNeedRedisc = needRedisclosure;

            loan.ProcessEDisclosureDisclosureTrigger(E_EDisclosureDisclosureEventT.EConsentExpired);

            Assert.IsTrue(loan.sNeedInitialDisc);
            Assert.IsFalse(loan.sNeedRedisc);
            Assert.AreEqual(E_sDisclosureNeededT.EConsentNotReceived_PaperDiscReqd, loan.sDisclosureNeededT);
        }

        [Test, TestCaseSource(nameof(ProcessEDisclosureDisclosureTrigger_OriginalDisclosureStateValuesSource))]
        public void ProcessEDisclosureDisclosureTrigger_EConsentExpiredAutomationEnabled_SetsDisclosureStateChangeFields(bool originalNeedInitialDisclosure,
            bool originalNeedRedisclosure,
            E_sDisclosureNeededT originalDisclosureNeededType)
        {
            var loan = FakePageData.Create();
            loan.InitLoad();

            loan.sNeedInitialDisc = originalNeedInitialDisclosure;
            loan.sNeedRedisc = originalNeedRedisclosure;
            loan.sDisclosureNeededT = originalDisclosureNeededType;

            loan.DisableEConsentMonitoringAutomation();
            loan.ProcessEDisclosureDisclosureTrigger(E_EDisclosureDisclosureEventT.EConsentExpired);

            Assert.AreEqual(originalNeedInitialDisclosure, loan.sNeedInitialDisc);
            Assert.AreEqual(originalNeedRedisclosure, loan.sNeedRedisc);
            Assert.AreEqual(originalDisclosureNeededType, loan.sDisclosureNeededT);
        }

        private static void AddLoanEstimateDates(CPageData loan, Guid transactionId)
        {
            loan.BrokerDB.TempOptionXmlContent = loan.BrokerDB.TempOptionXmlContent.Value + "<option name=\"EnableBorrowerLevelDisclosureTracking\" value=\"true\" />";
            var metadata = new LoanEstimateDatesMetadata(loan.GetClosingCostArchiveById, loan.GetConsumerDisclosureMetadataByConsumerId(), loan.sLoanRescindableT);
            
            LoanEstimateDates disclosureDates = LoanEstimateDates.Create(metadata);
            disclosureDates.TransactionId = transactionId.ToString();
            loan.sLoanEstimateDatesInfo.AddDates(disclosureDates);
        }

        private static void AddClosingDisclosureDates(CPageData loan, Guid transactionId)
        {
            loan.BrokerDB.TempOptionXmlContent = loan.BrokerDB.TempOptionXmlContent.Value + "<option name=\"EnableBorrowerLevelDisclosureTracking\" value=\"true\" />";
            var metadata = new ClosingDisclosureDatesMetadata(
                loan.GetClosingCostArchiveById,
                loan.GetConsumerDisclosureMetadataByConsumerId(),
                loan.sLoanRescindableT,
                loan.sAlwaysRequireAllBorrowersToReceiveClosingDisclosure);
            
            ClosingDisclosureDates disclosureDates = ClosingDisclosureDates.Create(metadata);
            disclosureDates.TransactionId = transactionId.ToString();
            loan.sClosingDisclosureDatesInfo.AddDates(disclosureDates);
        }

        private static LQBESignUpdate GetLQBESignUpdateWithMessage(string message, string transactionId = "")
        {
            return new LQBESignUpdate
            {
                ESignNotification = message,
                TransactionID = transactionId,
            };
        }

        static int borrowerCounter = 0;

        private static void ApplyToBorrower(CAppData app, NameComponents name)
        {
            if (name == null)
            {
                return;
            }

            app.aBFirstNm = name.First;
            app.aBMidNm = name.Middle;
            app.aBLastNm = name.Last;
            app.aBSuffix = name.Suffix;
            app.aBSsn = System.Threading.Interlocked.Increment(ref borrowerCounter).ToString("D9").Insert(3, "-").Insert(6, "-");
        }

        private static void ApplyToCoBorrower(CAppData app, NameComponents name)
        {
            if (name == null)
            {
                return;
            }

            app.aCFirstNm = name.First;
            app.aCMidNm = name.Middle;
            app.aCLastNm = name.Last;
            app.aCSuffix = name.Suffix;
            app.aCSsn = System.Threading.Interlocked.Increment(ref borrowerCounter).ToString("D9").Insert(3, "-").Insert(6, "-");
        }

        private static T[] InsertItem<T>(T[] array, int index, T item)
        {
            index = Math.Max(index, 0);
            index = Math.Min(index, array.Length);

            T[] newArray = new T[array.Length + 1];
            if (index > 0)
            {
                Array.Copy(array, 0, newArray, 0, index);
            }

            newArray[index] = item;
            if (index < array.Length)
            {
                Array.Copy(array, index, newArray, index + 1, array.Length - index);
            }

            return newArray;
        }

        private static NameComponents NewName(string first = null, string middle = null, string last = null, string suffix = null)
        {
            return new NameComponents()
            {
                First = first,
                Middle = middle,
                Last = last,
                Suffix = suffix,
            };
        }

        public class BorrowerSet
        {
            public BorrowerSet(NameComponents[] borrowers)
            {
                if (borrowers == null || borrowers.Length < 2 || borrowers.Length % 2 != 0)
                {
                    throw new InvalidOperationException();
                }

                this.Borrowers = borrowers;
            }

            public NameComponents[] Borrowers { get; }

            public override string ToString()
            {
                return string.Join("; ", this.Borrowers.Select((name, i) => name == null ? null : ((i % 2 == 0 ? "B" : "C") + ":" + name)).Where(n => n != null));
            }
        }

        public class NameComponents
        {
            public string First { get; set; }
            public string Middle { get; set; }
            public string Last { get; set; }
            public string Suffix { get; set; }

            public override string ToString()
            {
                return "Name(" + string.Join(", ", (new[] { this.First, this.Middle, this.Last, this.Suffix }).Select(s => s == null ? "null" : ("\"" + s.Replace("\"", "\"\"") + "\""))) + ")";
            }
        }
    }
}
