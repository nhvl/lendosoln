﻿namespace LendOSolnTest.Disclosure
{
    using global::DataAccess;
    using LendersOffice.Common.SerializationTypes;
    using LendersOffice.Constants;
    using LendersOffice.Integration.DocumentVendor.LQBESignUpdate;
    using LendOSolnTest.Common;
    using LqbGrammar;
    using LqbGrammar.Queries;
    using NSubstitute;
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    [TestFixture]
    public class DelayedESignNotificationTest
    {
        [Test]
        public void ESignUpdate_DelayedESignNotification_ClosingDisclosureTest()
        {
            if (!ConstStage.EnableDelayedESignNotificationLeCdRowUpdates)
            {
                return;
            }

            using (var testLoan = LoanForIntegrationTest.Create(TestAccountType.LoTest001, false))
            {
                // Arrange
                var loan = testLoan.CreateNewPageDataWithBypass();
                loan.InitSave(ConstAppDavid.SkipVersionCheck);

                loan.CreateTempArchive(packageHasClosingDisclosure: true, tempArchiveSource: null, shouldSetTempArchive: true, principal: null);
                var testArchive = loan.SaveTemporaryArchive(ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed, "TEST");

                Guid transactionId = Guid.NewGuid();
                Guid consumerId = Guid.NewGuid();

                var eSignNotification = new LQBESignUpdate()
                {
                    TransactionID = transactionId.ToString(),
                    ResponseSource = "TESTT",
                    LoanID = loan.sLId.ToString(),
                    BorrLastName = "TESTER",
                    BorrFirstName = "TESTERTEST",
                    BorrMiddleName = "TESTINNG",
                    BorrSuffix = "SR",
                    BorrFullName = "TESTERTEST TESTINNG TESTER SR",
                    BorrEmail = "TEST@TEST.COM",
                    ConsumerID = consumerId.ToString(),
                    ESignNotification = "Documents Signed for TESTERTEST TESTINNG",
                    Documents = "IDK",
                    OtherInserts = new string[] { "ho", "tho", "co" },
                };

                var issuedDate = DateTime.Now.Subtract(TimeSpan.FromDays(2)).ToString("d");
                var deliveryMethod = E_DeliveryMethodT.Email;
                var receivedDate = DateTime.Now.ToString("d");
                var isInitial = true;
                var isPreview = false;
                var lastDisclosedTRIDLoanProductDescription = "TEST TESTTT";
                var aPR = "4.235%";
                var signedDate = DateTime.Now.Subtract(TimeSpan.FromDays(3)).ToString("d");
                var docCode = "IDK";
                var isDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower = true;
                var isDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller = false;
                var isDisclosurePostClosingDueToNonNumericalClericalError = false;
                var isDisclosurePostClosingDueToCureForToleranceViolation = false;
                var postConsummationRedisclosureReasonDate = DateTime.Now.Subtract(TimeSpan.FromDays(5)).ToString("d");
                var postConsummationKnowledgeOfEventDate = DateTime.Now.Subtract(TimeSpan.FromDays(7)).ToString("d");
                var borrowerDisclosureMetaData = new LendersOffice.Integration.DocumentVendor.LQBESignUpdate.BorrowerDisclosureMetadata()
                {
                    IssuedDate = issuedDate,
                    ReceivedDate = receivedDate,
                    SignedDate = signedDate,
                    DeliveryMethod = deliveryMethod.ToString()
                };

                eSignNotification.DisclosureMetadata = new DisclosureMetadata()
                {
                    Item = new ClosingDisclosureMetadata()
                    {
                        IssuedDate = issuedDate,
                        DeliveryMethod = deliveryMethod.ToString(),
                        ReceivedDate = receivedDate,
                        IsInitial = isInitial.ToString(),
                        IsPreview = isPreview.ToString(),
                        LastDisclosedTRIDLoanProductDescription = lastDisclosedTRIDLoanProductDescription,
                        APR = aPR,
                        SignedDate = signedDate,
                        DocCode = docCode,
                        IsDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower = isDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower.ToString(),
                        IsDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller = isDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller.ToString(),
                        IsDisclosurePostClosingDueToNonNumericalClericalError = isDisclosurePostClosingDueToNonNumericalClericalError.ToString(),
                        IsDisclosurePostClosingDueToCureForToleranceViolation = isDisclosurePostClosingDueToCureForToleranceViolation.ToString(),
                        PostConsummationRedisclosureReasonDate = postConsummationRedisclosureReasonDate,
                        PostConsummationKnowledgeOfEventDate = postConsummationKnowledgeOfEventDate,
                        BorrowerDisclosureMetadata = borrowerDisclosureMetaData
                    }
                };

                // Act
                loan.ProcessEDisclosureDisclosureTrigger(E_EDisclosureDisclosureEventT.DocumentsSigned, eSignNotification);
                loan.UpdateTridDisclosureDatesForDocGeneration(testArchive, true, false, false, transactionId.ToString(), null, null, null, false);

                // Assert
                var closingDisclosure = loan.sClosingDisclosureDatesInfo.ClosingDisclosureDatesList
                        .SingleOrDefault(d => d.TransactionId.Equals(transactionId.ToString()));
                Assert.AreEqual(closingDisclosure.IssuedDate.ToString("d"), issuedDate);
                Assert.AreEqual(closingDisclosure.DeliveryMethod, deliveryMethod);
                Assert.AreEqual(closingDisclosure.ReceivedDate.ToString("d"), receivedDate);
                Assert.AreEqual(closingDisclosure.IsInitial, isInitial);
                Assert.AreEqual(closingDisclosure.IsPreview, isPreview);
                Assert.AreEqual(closingDisclosure.LastDisclosedTRIDLoanProductDescription, lastDisclosedTRIDLoanProductDescription);
                Assert.AreEqual(closingDisclosure.DocVendorApr_rep, aPR);
                Assert.AreEqual(closingDisclosure.SignedDate.ToString("d"), signedDate);
                Assert.AreEqual(closingDisclosure.DocCode, docCode);
                Assert.AreEqual(closingDisclosure.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower, isDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower);
                Assert.AreEqual(closingDisclosure.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller, isDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller);
                Assert.AreEqual(closingDisclosure.IsDisclosurePostClosingDueToNonNumericalClericalError, isDisclosurePostClosingDueToNonNumericalClericalError);
                Assert.AreEqual(closingDisclosure.IsDisclosurePostClosingDueToCureForToleranceViolation, isDisclosurePostClosingDueToCureForToleranceViolation);
                Assert.AreEqual(closingDisclosure.PostConsummationRedisclosureReasonDate.ToString("d"), postConsummationRedisclosureReasonDate);
                Assert.AreEqual(closingDisclosure.PostConsummationKnowledgeOfEventDate.ToString("d"), postConsummationKnowledgeOfEventDate);
            }            
        }

        [Test]
        public void ESignUpdate_DelayedESignNotification_LoanEstimateTest()
        {
            if (!ConstStage.EnableDelayedESignNotificationLeCdRowUpdates)
            {
                return;
            }

            using (var testLoan = LoanForIntegrationTest.Create(TestAccountType.LoTest001, false))
            {
                // Arrange
                var loan = testLoan.CreateNewPageDataWithBypass();
                loan.InitSave(ConstAppDavid.SkipVersionCheck);

                loan.CreateTempArchive(packageHasClosingDisclosure: false, tempArchiveSource: null, shouldSetTempArchive: true, principal: null);
                var testArchive = loan.SaveTemporaryArchive(ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed, "TEST");

                Guid transactionId = Guid.NewGuid();
                Guid consumerId = Guid.NewGuid();

                var eSignNotification = new LQBESignUpdate()
                {
                    TransactionID = transactionId.ToString(),
                    ResponseSource = "TESTT",
                    LoanID = loan.sLId.ToString(),
                    BorrLastName = "TESTER",
                    BorrFirstName = "TESTERTEST",
                    BorrMiddleName = "TESTINNG",
                    BorrSuffix = "SR",
                    BorrFullName = "TESTERTEST TESTINNG TESTER SR",
                    BorrEmail = "TEST@TEST.COM",
                    ConsumerID = consumerId.ToString(),
                    ESignNotification = "Documents Signed for TESTERTEST TESTINNG",
                    Documents = "IDK",
                    OtherInserts = new string[] { "ho", "tho", "co" },
                };

                var issuedDate = DateTime.Now.Subtract(TimeSpan.FromDays(2)).ToString("d");
                var deliveryMethod = E_DeliveryMethodT.Email;
                var receivedDate = DateTime.Now.ToString("d");
                var isInitial = true;
                var lastDisclosedTRIDLoanProductDescription = "TEST TESTTT";
                var aPR = "4.235%";
                var signedDate = DateTime.Now.Subtract(TimeSpan.FromDays(3)).ToString("d");
                var postConsummationRedisclosureReasonDate = DateTime.Now.Subtract(TimeSpan.FromDays(5)).ToString("d");
                var postConsummationKnowledgeOfEventDate = DateTime.Now.Subtract(TimeSpan.FromDays(7)).ToString("d");
                var borrowerDisclosureMetaData = new LendersOffice.Integration.DocumentVendor.LQBESignUpdate.BorrowerDisclosureMetadata()
                {
                    IssuedDate = issuedDate,
                    ReceivedDate = receivedDate,
                    SignedDate = signedDate,
                    DeliveryMethod = deliveryMethod.ToString()
                };

                eSignNotification.DisclosureMetadata = new DisclosureMetadata()
                {
                    Item = new LoanEstimateMetadata()
                    {
                        IssuedDate = issuedDate,
                        DeliveryMethod = deliveryMethod.ToString(),
                        ReceivedDate = receivedDate,
                        IsInitial = isInitial.ToString(),
                        LastDisclosedTRIDLoanProductDescription = lastDisclosedTRIDLoanProductDescription,
                        APR = aPR,
                        SignedDate = signedDate,
                        BorrowerDisclosureMetadata = borrowerDisclosureMetaData
                    }
                };

                // Act
                loan.ProcessEDisclosureDisclosureTrigger(E_EDisclosureDisclosureEventT.DocumentsSigned, eSignNotification);
                loan.UpdateTridDisclosureDatesForDocGeneration(testArchive, true, false, false, transactionId.ToString(), null, null, null, false);

                // Assert
                var loanEstimate = loan.sLoanEstimateDatesInfo.LoanEstimateDatesList
                        .SingleOrDefault(d => d.TransactionId.Equals(transactionId.ToString()));
                var closingDisclosure = loan.sClosingDisclosureDatesInfo.ClosingDisclosureDatesList
                        .SingleOrDefault(d => d.TransactionId.Equals(transactionId.ToString()));
                Assert.AreEqual(loanEstimate.IssuedDate.ToString("d"), issuedDate);
                Assert.AreEqual(loanEstimate.DeliveryMethod, deliveryMethod);
                Assert.AreEqual(loanEstimate.ReceivedDate.ToString("d"), receivedDate);
                Assert.AreEqual(loanEstimate.IsInitial, isInitial);
                Assert.AreEqual(loanEstimate.LastDisclosedTRIDLoanProductDescription, lastDisclosedTRIDLoanProductDescription);
                Assert.AreEqual(loanEstimate.DocVendorApr_rep, aPR);
                Assert.AreEqual(loanEstimate.SignedDate.ToString("d"), signedDate);
            }            
        }

        [Test]
        public void ESignUpdate_DelayedESignNotification_MultipleESignNotifications_ClosingDisclosureTest()
        {
            if (!ConstStage.EnableDelayedESignNotificationLeCdRowUpdates)
            {
                return;
            }

            using (var testLoan = LoanForIntegrationTest.Create(TestAccountType.LoTest001, false))
            {
                // Arrange
                var loan = testLoan.CreateNewPageDataWithBypass();
                loan.InitSave(ConstAppDavid.SkipVersionCheck);

                loan.CreateTempArchive(packageHasClosingDisclosure: true, tempArchiveSource: null, shouldSetTempArchive: true, principal: null);
                var testArchive = loan.SaveTemporaryArchive(ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed, "TEST");

                Guid transactionId = Guid.NewGuid();
                Guid consumerId = Guid.NewGuid();

                var eSignNotification = new LQBESignUpdate()
                {
                    TransactionID = transactionId.ToString(),
                    ResponseSource = "TESTT",
                    LoanID = loan.sLId.ToString(),
                    BorrLastName = "TESTER",
                    BorrFirstName = "TESTERTEST",
                    BorrMiddleName = "TESTINNG",
                    BorrSuffix = "SR",
                    BorrFullName = "TESTERTEST TESTINNG TESTER SR",
                    BorrEmail = "TEST@TEST.COM",
                    ConsumerID = consumerId.ToString(),
                    ESignNotification = "Documents Signed for TESTERTEST TESTINNG",
                    Documents = "IDK",
                    OtherInserts = new string[] { "ho", "tho", "co" },
                };

                var issuedDate = DateTime.Now.Subtract(TimeSpan.FromDays(2)).ToString("d");
                var deliveryMethod = E_DeliveryMethodT.Email;
                var receivedDate = DateTime.Now.ToString("d");
                var isInitial = true;
                var isPreview = false;
                var lastDisclosedTRIDLoanProductDescription = "TEST TESTTT";
                var aPR = "4.235%";
                var signedDate = DateTime.Now.Subtract(TimeSpan.FromDays(3)).ToString("d");
                var docCode = "IDK";
                var isDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower = true;
                var isDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller = false;
                var isDisclosurePostClosingDueToNonNumericalClericalError = false;
                var isDisclosurePostClosingDueToCureForToleranceViolation = false;
                var postConsummationRedisclosureReasonDate = DateTime.Now.Subtract(TimeSpan.FromDays(5)).ToString("d");
                var postConsummationKnowledgeOfEventDate = DateTime.Now.Subtract(TimeSpan.FromDays(7)).ToString("d");
                var borrowerDisclosureMetaData = new LendersOffice.Integration.DocumentVendor.LQBESignUpdate.BorrowerDisclosureMetadata()
                {
                    IssuedDate = issuedDate,
                    ReceivedDate = receivedDate,
                    SignedDate = signedDate,
                    DeliveryMethod = deliveryMethod.ToString()
                };

                eSignNotification.DisclosureMetadata = new DisclosureMetadata()
                {
                    Item = new ClosingDisclosureMetadata()
                    {
                        IssuedDate = issuedDate,
                        DeliveryMethod = deliveryMethod.ToString(),
                        ReceivedDate = receivedDate,
                        IsInitial = isInitial.ToString(),
                        IsPreview = isPreview.ToString(),
                        LastDisclosedTRIDLoanProductDescription = lastDisclosedTRIDLoanProductDescription,
                        APR = aPR,
                        SignedDate = signedDate,
                        DocCode = docCode,
                        IsDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower = isDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower.ToString(),
                        IsDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller = isDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller.ToString(),
                        IsDisclosurePostClosingDueToNonNumericalClericalError = isDisclosurePostClosingDueToNonNumericalClericalError.ToString(),
                        IsDisclosurePostClosingDueToCureForToleranceViolation = isDisclosurePostClosingDueToCureForToleranceViolation.ToString(),
                        PostConsummationRedisclosureReasonDate = postConsummationRedisclosureReasonDate,
                        PostConsummationKnowledgeOfEventDate = postConsummationKnowledgeOfEventDate,
                        BorrowerDisclosureMetadata = borrowerDisclosureMetaData
                    }
                };

                var eSignNotification2 = new LQBESignUpdate()
                {
                    TransactionID = transactionId.ToString(),
                    ResponseSource = "TESTT2",
                    LoanID = loan.sLId.ToString(),
                    BorrLastName = "TESTER2",
                    BorrFirstName = "TESTERTEST2",
                    BorrMiddleName = "TESTINNG2",
                    BorrSuffix = "SR2",
                    BorrFullName = "TESTERTEST TESTINNG TESTER SR2",
                    BorrEmail = "TEST@TEST.COM2",
                    ConsumerID = consumerId.ToString(),
                    ESignNotification = "Documents Signed for TESTERTEST TESTINNG2",
                    Documents = "IDK2",
                    OtherInserts = new string[] { "ho2", "tho2", "co2" },
                };

                var issuedDate2 = DateTime.Now.Subtract(TimeSpan.FromDays(22)).ToString("d");
                var deliveryMethod2 = E_DeliveryMethodT.Fax;
                var receivedDate2 = DateTime.Now.ToString("d");
                var isInitial2 = true;
                var isPreview2 = false;
                var lastDisclosedTRIDLoanProductDescription2 = "TEST TESTTT";
                var aPR2 = "4.235%";
                var signedDate2 = DateTime.Now.Subtract(TimeSpan.FromDays(32)).ToString("d");
                var docCode2 = "IDK";
                var isDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower2 = true;
                var isDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller2 = false;
                var isDisclosurePostClosingDueToNonNumericalClericalError2 = false;
                var isDisclosurePostClosingDueToCureForToleranceViolation2 = false;
                var postConsummationRedisclosureReasonDate2 = DateTime.Now.Subtract(TimeSpan.FromDays(52)).ToString("d");
                var postConsummationKnowledgeOfEventDate2 = DateTime.Now.Subtract(TimeSpan.FromDays(72)).ToString("d");
                var borrowerDisclosureMetaData2 = new LendersOffice.Integration.DocumentVendor.LQBESignUpdate.BorrowerDisclosureMetadata()
                {
                    IssuedDate = issuedDate2,
                    ReceivedDate = receivedDate2,
                    SignedDate = signedDate2,
                    DeliveryMethod = deliveryMethod2.ToString()
                };

                eSignNotification2.DisclosureMetadata = new DisclosureMetadata()
                {
                    Item = new ClosingDisclosureMetadata()
                    {
                        IssuedDate = issuedDate2,
                        DeliveryMethod = deliveryMethod2.ToString(),
                        ReceivedDate = receivedDate2,
                        IsInitial = isInitial2.ToString(),
                        IsPreview = isPreview2.ToString(),
                        LastDisclosedTRIDLoanProductDescription = lastDisclosedTRIDLoanProductDescription2,
                        APR = aPR2,
                        SignedDate = signedDate2,
                        DocCode = docCode2,
                        IsDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower = isDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower2.ToString(),
                        IsDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller = isDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller2.ToString(),
                        IsDisclosurePostClosingDueToNonNumericalClericalError = isDisclosurePostClosingDueToNonNumericalClericalError2.ToString(),
                        IsDisclosurePostClosingDueToCureForToleranceViolation = isDisclosurePostClosingDueToCureForToleranceViolation2.ToString(),
                        PostConsummationRedisclosureReasonDate = postConsummationRedisclosureReasonDate2,
                        PostConsummationKnowledgeOfEventDate = postConsummationKnowledgeOfEventDate2,
                        BorrowerDisclosureMetadata = borrowerDisclosureMetaData2
                    }
                };

                // Act
                loan.ProcessEDisclosureDisclosureTrigger(E_EDisclosureDisclosureEventT.DocumentsSigned, eSignNotification);
                loan.ProcessEDisclosureDisclosureTrigger(E_EDisclosureDisclosureEventT.EConsentReceived, eSignNotification2);
                loan.UpdateTridDisclosureDatesForDocGeneration(testArchive, true, false, false, transactionId.ToString(), null, null, null, false);

                // Assert
                var closingDisclosure = loan.sClosingDisclosureDatesInfo.ClosingDisclosureDatesList
                        .SingleOrDefault(d => d.TransactionId.Equals(transactionId.ToString()));
                Assert.AreEqual(closingDisclosure.IssuedDate.ToString("d"), issuedDate2);
                Assert.AreEqual(closingDisclosure.DeliveryMethod, deliveryMethod2);
                Assert.AreEqual(closingDisclosure.ReceivedDate.ToString("d"), receivedDate2);
                Assert.AreEqual(closingDisclosure.IsInitial, isInitial2);
                Assert.AreEqual(closingDisclosure.IsPreview, isPreview2);
                Assert.AreEqual(closingDisclosure.LastDisclosedTRIDLoanProductDescription, lastDisclosedTRIDLoanProductDescription2);
                Assert.AreEqual(closingDisclosure.DocVendorApr_rep, aPR2);
                Assert.AreEqual(closingDisclosure.SignedDate.ToString("d"), signedDate2);
                Assert.AreEqual(closingDisclosure.DocCode, docCode2);
                Assert.AreEqual(closingDisclosure.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower, isDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower2);
                Assert.AreEqual(closingDisclosure.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller, isDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller2);
                Assert.AreEqual(closingDisclosure.IsDisclosurePostClosingDueToNonNumericalClericalError, isDisclosurePostClosingDueToNonNumericalClericalError2);
                Assert.AreEqual(closingDisclosure.IsDisclosurePostClosingDueToCureForToleranceViolation, isDisclosurePostClosingDueToCureForToleranceViolation2);
                Assert.AreEqual(closingDisclosure.PostConsummationRedisclosureReasonDate.ToString("d"), postConsummationRedisclosureReasonDate2);
                Assert.AreEqual(closingDisclosure.PostConsummationKnowledgeOfEventDate.ToString("d"), postConsummationKnowledgeOfEventDate2);
            }
        }

        [Test]
        public void ESignUpdate_DelayedESignNotification_MultipleESignNotifications_LoanEstimateTest()
        {
            if (!ConstStage.EnableDelayedESignNotificationLeCdRowUpdates)
            {
                return;
            }

            using (var testLoan = LoanForIntegrationTest.Create(TestAccountType.LoTest001, false))
            {                
                // Arrange
                var loan = testLoan.CreateNewPageDataWithBypass();
                loan.InitSave(ConstAppDavid.SkipVersionCheck);

                loan.CreateTempArchive(packageHasClosingDisclosure: false, tempArchiveSource: null, shouldSetTempArchive: true, principal: null);
                var testArchive = loan.SaveTemporaryArchive(ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed, "TEST");

                Guid transactionId = Guid.NewGuid();
                Guid consumerId = Guid.NewGuid();

                var eSignNotification = new LQBESignUpdate()
                {
                    TransactionID = transactionId.ToString(),
                    ResponseSource = "TESTT",
                    LoanID = loan.sLId.ToString(),
                    BorrLastName = "TESTER",
                    BorrFirstName = "TESTERTEST",
                    BorrMiddleName = "TESTINNG",
                    BorrSuffix = "SR",
                    BorrFullName = "TESTERTEST TESTINNG TESTER SR",
                    BorrEmail = "TEST@TEST.COM",
                    ConsumerID = consumerId.ToString(),
                    ESignNotification = "Documents Signed for TESTERTEST TESTINNG",
                    Documents = "IDK",
                    OtherInserts = new string[] { "ho", "tho", "co" },
                };

                var issuedDate = DateTime.Now.Subtract(TimeSpan.FromDays(2)).ToString("d");
                var deliveryMethod = E_DeliveryMethodT.Email;
                var receivedDate = DateTime.Now.ToString("d");
                var isInitial = true;
                var lastDisclosedTRIDLoanProductDescription = "TEST TESTTT";
                var aPR = "4.235%";
                var signedDate = DateTime.Now.Subtract(TimeSpan.FromDays(3)).ToString("d");
                var postConsummationRedisclosureReasonDate = DateTime.Now.Subtract(TimeSpan.FromDays(5)).ToString("d");
                var postConsummationKnowledgeOfEventDate = DateTime.Now.Subtract(TimeSpan.FromDays(7)).ToString("d");
                var borrowerDisclosureMetaData = new LendersOffice.Integration.DocumentVendor.LQBESignUpdate.BorrowerDisclosureMetadata()
                {
                    IssuedDate = issuedDate,
                    ReceivedDate = receivedDate,
                    SignedDate = signedDate,
                    DeliveryMethod = deliveryMethod.ToString()
                };

                eSignNotification.DisclosureMetadata = new DisclosureMetadata()
                {
                    Item = new LoanEstimateMetadata()
                    {
                        IssuedDate = issuedDate,
                        DeliveryMethod = deliveryMethod.ToString(),
                        ReceivedDate = receivedDate,
                        IsInitial = isInitial.ToString(),
                        LastDisclosedTRIDLoanProductDescription = lastDisclosedTRIDLoanProductDescription,
                        APR = aPR,
                        SignedDate = signedDate,
                        BorrowerDisclosureMetadata = borrowerDisclosureMetaData
                    }
                };

                var eSignNotification2 = new LQBESignUpdate()
                {
                    TransactionID = transactionId.ToString(),
                    ResponseSource = "TESTT2",
                    LoanID = loan.sLId.ToString(),
                    BorrLastName = "TESTER2",
                    BorrFirstName = "TESTERTEST2",
                    BorrMiddleName = "TESTINNG2",
                    BorrSuffix = "SR2",
                    BorrFullName = "TESTERTEST TESTINNG TESTER SR2",
                    BorrEmail = "TEST@TEST.COM2",
                    ConsumerID = consumerId.ToString(),
                    ESignNotification = "Documents Signed for TESTERTEST TESTINNG2",
                    Documents = "IDK2",
                    OtherInserts = new string[] { "ho2", "tho2", "co2" },
                };

                var issuedDate2 = DateTime.Now.Subtract(TimeSpan.FromDays(22)).ToString("d");
                var deliveryMethod2 = E_DeliveryMethodT.Fax;
                var receivedDate2 = DateTime.Now.ToString("d");
                var isInitial2 = true;
                var lastDisclosedTRIDLoanProductDescription2 = "TEST TESTTT";
                var aPR2 = "4.235%";
                var signedDate2 = DateTime.Now.Subtract(TimeSpan.FromDays(32)).ToString("d");
                var postConsummationRedisclosureReasonDate2 = DateTime.Now.Subtract(TimeSpan.FromDays(52)).ToString("d");
                var postConsummationKnowledgeOfEventDate2 = DateTime.Now.Subtract(TimeSpan.FromDays(72)).ToString("d");
                var borrowerDisclosureMetaData2 = new LendersOffice.Integration.DocumentVendor.LQBESignUpdate.BorrowerDisclosureMetadata()
                {
                    IssuedDate = issuedDate2,
                    ReceivedDate = receivedDate2,
                    SignedDate = signedDate2,
                    DeliveryMethod = deliveryMethod2.ToString()
                };

                eSignNotification2.DisclosureMetadata = new DisclosureMetadata()
                {
                    Item = new LoanEstimateMetadata()
                    {
                        IssuedDate = issuedDate2,
                        DeliveryMethod = deliveryMethod2.ToString(),
                        ReceivedDate = receivedDate2,
                        IsInitial = isInitial2.ToString(),
                        LastDisclosedTRIDLoanProductDescription = lastDisclosedTRIDLoanProductDescription2,
                        APR = aPR2,
                        SignedDate = signedDate2,
                        BorrowerDisclosureMetadata = borrowerDisclosureMetaData2
                    }
                };

                // Act
                loan.ProcessEDisclosureDisclosureTrigger(E_EDisclosureDisclosureEventT.DocumentsSigned, eSignNotification);
                loan.ProcessEDisclosureDisclosureTrigger(E_EDisclosureDisclosureEventT.EConsentReceived, eSignNotification2);
                loan.UpdateTridDisclosureDatesForDocGeneration(testArchive, true, false, false, transactionId.ToString(), null, null, null, false);

                // Assert
                var loanEstimate = loan.sLoanEstimateDatesInfo.LoanEstimateDatesList
                        .SingleOrDefault(d => d.TransactionId.Equals(transactionId.ToString()));
                var closingDisclosure = loan.sClosingDisclosureDatesInfo.ClosingDisclosureDatesList
                        .SingleOrDefault(d => d.TransactionId.Equals(transactionId.ToString()));
                Assert.AreEqual(loanEstimate.IssuedDate.ToString("d"), issuedDate2);
                Assert.AreEqual(loanEstimate.DeliveryMethod, deliveryMethod2);
                Assert.AreEqual(loanEstimate.ReceivedDate.ToString("d"), receivedDate2);
                Assert.AreEqual(loanEstimate.IsInitial, isInitial2);
                Assert.AreEqual(loanEstimate.LastDisclosedTRIDLoanProductDescription, lastDisclosedTRIDLoanProductDescription2);
                Assert.AreEqual(loanEstimate.DocVendorApr_rep, aPR2);
                Assert.AreEqual(loanEstimate.SignedDate.ToString("d"), signedDate2);
            }
        }

        [Test]
        public void ESignUpdate_DelayedESignNotification_ClosingDisclosureTest_DocGeneratedFirst()
        {
            if (!ConstStage.EnableDelayedESignNotificationLeCdRowUpdates)
            {
                return;
            }

            using (var testLoan = LoanForIntegrationTest.Create(TestAccountType.LoTest001, false))
            {
                // Arrange
                var loan = testLoan.CreateNewPageDataWithBypass();
                loan.InitSave(ConstAppDavid.SkipVersionCheck);                

                loan.CreateTempArchive(packageHasClosingDisclosure: true, tempArchiveSource: null, shouldSetTempArchive: true, principal: null);
                var testArchive = loan.SaveTemporaryArchive(ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed, "TEST");

                Guid transactionId = Guid.NewGuid();
                Guid consumerId = Guid.NewGuid();

                var eSignNotification = new LQBESignUpdate()
                {
                    TransactionID = transactionId.ToString(),
                    ResponseSource = "TESTT",
                    LoanID = loan.sLId.ToString(),
                    BorrLastName = "TESTER",
                    BorrFirstName = "TESTERTEST",
                    BorrMiddleName = "TESTINNG",
                    BorrSuffix = "SR",
                    BorrFullName = "TESTERTEST TESTINNG TESTER SR",
                    BorrEmail = "TEST@TEST.COM",
                    ConsumerID = consumerId.ToString(),
                    ESignNotification = "Documents Signed for TESTERTEST TESTINNG",
                    Documents = "IDK",
                    OtherInserts = new string[] { "ho", "tho", "co" },
                };

                var issuedDate = DateTime.Now.Subtract(TimeSpan.FromDays(2)).ToString("d");
                var deliveryMethod = E_DeliveryMethodT.Email;
                var receivedDate = DateTime.Now.ToString("d");
                var isInitial = true;
                var isPreview = false;
                var lastDisclosedTRIDLoanProductDescription = "TEST TESTTT";
                var aPR = "4.235%";
                var signedDate = DateTime.Now.Subtract(TimeSpan.FromDays(3)).ToString("d");
                var docCode = "IDK";
                var isDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower = true;
                var isDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller = false;
                var isDisclosurePostClosingDueToNonNumericalClericalError = false;
                var isDisclosurePostClosingDueToCureForToleranceViolation = false;
                var postConsummationRedisclosureReasonDate = DateTime.Now.Subtract(TimeSpan.FromDays(5)).ToString("d");
                var postConsummationKnowledgeOfEventDate = DateTime.Now.Subtract(TimeSpan.FromDays(7)).ToString("d");
                var borrowerDisclosureMetaData = new LendersOffice.Integration.DocumentVendor.LQBESignUpdate.BorrowerDisclosureMetadata()
                {
                    IssuedDate = issuedDate,
                    ReceivedDate = receivedDate,
                    SignedDate = signedDate,
                    DeliveryMethod = deliveryMethod.ToString()
                };

                eSignNotification.DisclosureMetadata = new DisclosureMetadata()
                {
                    Item = new ClosingDisclosureMetadata()
                    {
                        IssuedDate = issuedDate,
                        DeliveryMethod = deliveryMethod.ToString(),
                        ReceivedDate = receivedDate,
                        IsInitial = isInitial.ToString(),
                        IsPreview = isPreview.ToString(),
                        LastDisclosedTRIDLoanProductDescription = lastDisclosedTRIDLoanProductDescription,
                        APR = aPR,
                        SignedDate = signedDate,
                        DocCode = docCode,
                        IsDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower = isDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower.ToString(),
                        IsDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller = isDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller.ToString(),
                        IsDisclosurePostClosingDueToNonNumericalClericalError = isDisclosurePostClosingDueToNonNumericalClericalError.ToString(),
                        IsDisclosurePostClosingDueToCureForToleranceViolation = isDisclosurePostClosingDueToCureForToleranceViolation.ToString(),
                        PostConsummationRedisclosureReasonDate = postConsummationRedisclosureReasonDate,
                        PostConsummationKnowledgeOfEventDate = postConsummationKnowledgeOfEventDate,
                        BorrowerDisclosureMetadata = borrowerDisclosureMetaData
                    }
                };

                // Act
                loan.UpdateTridDisclosureDatesForDocGeneration(testArchive, true, false, false, transactionId.ToString(), null, null, null, false);
                loan.ProcessEDisclosureDisclosureTrigger(E_EDisclosureDisclosureEventT.DocumentsSigned, eSignNotification);

                // Assert
                var closingDisclosure = loan.sClosingDisclosureDatesInfo.ClosingDisclosureDatesList
                        .SingleOrDefault(d => d.TransactionId.Equals(transactionId.ToString()));
                Assert.AreEqual(closingDisclosure.IssuedDate.ToString("d"), issuedDate);
                Assert.AreEqual(closingDisclosure.DeliveryMethod, deliveryMethod);
                Assert.AreEqual(closingDisclosure.ReceivedDate.ToString("d"), receivedDate);
                Assert.AreEqual(closingDisclosure.IsInitial, isInitial);
                Assert.AreEqual(closingDisclosure.IsPreview, isPreview);
                Assert.AreEqual(closingDisclosure.LastDisclosedTRIDLoanProductDescription, lastDisclosedTRIDLoanProductDescription);
                Assert.AreEqual(closingDisclosure.DocVendorApr_rep, aPR);
                Assert.AreEqual(closingDisclosure.SignedDate.ToString("d"), signedDate);
                Assert.AreEqual(closingDisclosure.DocCode, docCode);
                Assert.AreEqual(closingDisclosure.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower, isDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower);
                Assert.AreEqual(closingDisclosure.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller, isDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller);
                Assert.AreEqual(closingDisclosure.IsDisclosurePostClosingDueToNonNumericalClericalError, isDisclosurePostClosingDueToNonNumericalClericalError);
                Assert.AreEqual(closingDisclosure.IsDisclosurePostClosingDueToCureForToleranceViolation, isDisclosurePostClosingDueToCureForToleranceViolation);
                Assert.AreEqual(closingDisclosure.PostConsummationRedisclosureReasonDate.ToString("d"), postConsummationRedisclosureReasonDate);
                Assert.AreEqual(closingDisclosure.PostConsummationKnowledgeOfEventDate.ToString("d"), postConsummationKnowledgeOfEventDate);
            }
        }

        [Test]
        public void ESignUpdate_DelayedESignNotification_LoanEstimateTest_DocGeneratedFirst()
        {
            if (!ConstStage.EnableDelayedESignNotificationLeCdRowUpdates)
            {
                return;
            }

            using (var testLoan = LoanForIntegrationTest.Create(TestAccountType.LoTest001, false))
            {
                // Arrange
                var loan = testLoan.CreateNewPageDataWithBypass();
                loan.InitSave(ConstAppDavid.SkipVersionCheck);

                loan.CreateTempArchive(packageHasClosingDisclosure: false, tempArchiveSource: null, shouldSetTempArchive: true, principal: null);
                var testArchive = loan.SaveTemporaryArchive(ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed, "TEST");

                Guid transactionId = Guid.NewGuid();
                Guid consumerId = Guid.NewGuid();

                var eSignNotification = new LQBESignUpdate()
                {
                    TransactionID = transactionId.ToString(),
                    ResponseSource = "TESTT",
                    LoanID = loan.sLId.ToString(),
                    BorrLastName = "TESTER",
                    BorrFirstName = "TESTERTEST",
                    BorrMiddleName = "TESTINNG",
                    BorrSuffix = "SR",
                    BorrFullName = "TESTERTEST TESTINNG TESTER SR",
                    BorrEmail = "TEST@TEST.COM",
                    ConsumerID = consumerId.ToString(),
                    ESignNotification = "Documents Signed for TESTERTEST TESTINNG",
                    Documents = "IDK",
                    OtherInserts = new string[] { "ho", "tho", "co" },
                };

                var issuedDate = DateTime.Now.Subtract(TimeSpan.FromDays(2)).ToString("d");
                var deliveryMethod = E_DeliveryMethodT.Email;
                var receivedDate = DateTime.Now.ToString("d");
                var isInitial = true;
                var lastDisclosedTRIDLoanProductDescription = "TEST TESTTT";
                var aPR = "4.235%";
                var signedDate = DateTime.Now.Subtract(TimeSpan.FromDays(3)).ToString("d");
                var postConsummationRedisclosureReasonDate = DateTime.Now.Subtract(TimeSpan.FromDays(5)).ToString("d");
                var postConsummationKnowledgeOfEventDate = DateTime.Now.Subtract(TimeSpan.FromDays(7)).ToString("d");
                var borrowerDisclosureMetaData = new LendersOffice.Integration.DocumentVendor.LQBESignUpdate.BorrowerDisclosureMetadata()
                {
                    IssuedDate = issuedDate,
                    ReceivedDate = receivedDate,
                    SignedDate = signedDate,
                    DeliveryMethod = deliveryMethod.ToString()
                };

                eSignNotification.DisclosureMetadata = new DisclosureMetadata()
                {
                    Item = new LoanEstimateMetadata()
                    {
                        IssuedDate = issuedDate,
                        DeliveryMethod = deliveryMethod.ToString(),
                        ReceivedDate = receivedDate,
                        IsInitial = isInitial.ToString(),
                        LastDisclosedTRIDLoanProductDescription = lastDisclosedTRIDLoanProductDescription,
                        APR = aPR,
                        SignedDate = signedDate,
                        BorrowerDisclosureMetadata = borrowerDisclosureMetaData
                    }
                };

                // Act
                loan.UpdateTridDisclosureDatesForDocGeneration(testArchive, true, false, false, transactionId.ToString(), null, null, null, false);
                loan.ProcessEDisclosureDisclosureTrigger(E_EDisclosureDisclosureEventT.DocumentsSigned, eSignNotification);

                // Assert
                var loanEstimate = loan.sLoanEstimateDatesInfo.LoanEstimateDatesList
                        .SingleOrDefault(d => d.TransactionId.Equals(transactionId.ToString()));
                var closingDisclosure = loan.sClosingDisclosureDatesInfo.ClosingDisclosureDatesList
                        .SingleOrDefault(d => d.TransactionId.Equals(transactionId.ToString()));
                Assert.AreEqual(loanEstimate.IssuedDate.ToString("d"), issuedDate);
                Assert.AreEqual(loanEstimate.DeliveryMethod, deliveryMethod);
                Assert.AreEqual(loanEstimate.ReceivedDate.ToString("d"), receivedDate);
                Assert.AreEqual(loanEstimate.IsInitial, isInitial);
                Assert.AreEqual(loanEstimate.LastDisclosedTRIDLoanProductDescription, lastDisclosedTRIDLoanProductDescription);
                Assert.AreEqual(loanEstimate.DocVendorApr_rep, aPR);
                Assert.AreEqual(loanEstimate.SignedDate.ToString("d"), signedDate);
            }
        }

        [Test]
        public void ESignUpdate_DelayedESignNotification_MultipleESignNotifications_ClosingDisclosureTest_DocGeneratedFirst()
        {
            if (!ConstStage.EnableDelayedESignNotificationLeCdRowUpdates)
            {
                return;
            }

            using (var testLoan = LoanForIntegrationTest.Create(TestAccountType.LoTest001, false))
            {
                // Arrange
                var loan = testLoan.CreateNewPageDataWithBypass();
                loan.InitSave(ConstAppDavid.SkipVersionCheck);

                loan.CreateTempArchive(packageHasClosingDisclosure: true, tempArchiveSource: null, shouldSetTempArchive: true, principal: null);
                var testArchive = loan.SaveTemporaryArchive(ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed, "TEST");

                Guid transactionId = Guid.NewGuid();
                Guid consumerId = Guid.NewGuid();

                var eSignNotification = new LQBESignUpdate()
                {
                    TransactionID = transactionId.ToString(),
                    ResponseSource = "TESTT",
                    LoanID = loan.sLId.ToString(),
                    BorrLastName = "TESTER",
                    BorrFirstName = "TESTERTEST",
                    BorrMiddleName = "TESTINNG",
                    BorrSuffix = "SR",
                    BorrFullName = "TESTERTEST TESTINNG TESTER SR",
                    BorrEmail = "TEST@TEST.COM",
                    ConsumerID = consumerId.ToString(),
                    ESignNotification = "Documents Signed for TESTERTEST TESTINNG",
                    Documents = "IDK",
                    OtherInserts = new string[] { "ho", "tho", "co" },
                };

                var issuedDate = DateTime.Now.Subtract(TimeSpan.FromDays(2)).ToString("d");
                var deliveryMethod = E_DeliveryMethodT.Email;
                var receivedDate = DateTime.Now.ToString("d");
                var isInitial = true;
                var isPreview = false;
                var lastDisclosedTRIDLoanProductDescription = "TEST TESTTT";
                var aPR = "4.235%";
                var signedDate = DateTime.Now.Subtract(TimeSpan.FromDays(3)).ToString("d");
                var docCode = "IDK";
                var isDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower = true;
                var isDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller = false;
                var isDisclosurePostClosingDueToNonNumericalClericalError = false;
                var isDisclosurePostClosingDueToCureForToleranceViolation = false;
                var postConsummationRedisclosureReasonDate = DateTime.Now.Subtract(TimeSpan.FromDays(5)).ToString("d");
                var postConsummationKnowledgeOfEventDate = DateTime.Now.Subtract(TimeSpan.FromDays(7)).ToString("d");
                var borrowerDisclosureMetaData = new LendersOffice.Integration.DocumentVendor.LQBESignUpdate.BorrowerDisclosureMetadata()
                {
                    IssuedDate = issuedDate,
                    ReceivedDate = receivedDate,
                    SignedDate = signedDate,
                    DeliveryMethod = deliveryMethod.ToString()
                };

                eSignNotification.DisclosureMetadata = new DisclosureMetadata()
                {
                    Item = new ClosingDisclosureMetadata()
                    {
                        IssuedDate = issuedDate,
                        DeliveryMethod = deliveryMethod.ToString(),
                        ReceivedDate = receivedDate,
                        IsInitial = isInitial.ToString(),
                        IsPreview = isPreview.ToString(),
                        LastDisclosedTRIDLoanProductDescription = lastDisclosedTRIDLoanProductDescription,
                        APR = aPR,
                        SignedDate = signedDate,
                        DocCode = docCode,
                        IsDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower = isDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower.ToString(),
                        IsDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller = isDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller.ToString(),
                        IsDisclosurePostClosingDueToNonNumericalClericalError = isDisclosurePostClosingDueToNonNumericalClericalError.ToString(),
                        IsDisclosurePostClosingDueToCureForToleranceViolation = isDisclosurePostClosingDueToCureForToleranceViolation.ToString(),
                        PostConsummationRedisclosureReasonDate = postConsummationRedisclosureReasonDate,
                        PostConsummationKnowledgeOfEventDate = postConsummationKnowledgeOfEventDate,
                        BorrowerDisclosureMetadata = borrowerDisclosureMetaData
                    }
                };

                var eSignNotification2 = new LQBESignUpdate()
                {
                    TransactionID = transactionId.ToString(),
                    ResponseSource = "TESTT2",
                    LoanID = loan.sLId.ToString(),
                    BorrLastName = "TESTER2",
                    BorrFirstName = "TESTERTEST2",
                    BorrMiddleName = "TESTINNG2",
                    BorrSuffix = "SR2",
                    BorrFullName = "TESTERTEST TESTINNG TESTER SR2",
                    BorrEmail = "TEST@TEST.COM2",
                    ConsumerID = consumerId.ToString(),
                    ESignNotification = "Documents Signed for TESTERTEST TESTINNG2",
                    Documents = "IDK2",
                    OtherInserts = new string[] { "ho2", "tho2", "co2" },
                };

                var issuedDate2 = DateTime.Now.Subtract(TimeSpan.FromDays(22)).ToString("d");
                var deliveryMethod2 = E_DeliveryMethodT.Fax;
                var receivedDate2 = DateTime.Now.ToString("d");
                var isInitial2 = true;
                var isPreview2 = false;
                var lastDisclosedTRIDLoanProductDescription2 = "TEST TESTTT";
                var aPR2 = "4.235%";
                var signedDate2 = DateTime.Now.Subtract(TimeSpan.FromDays(32)).ToString("d");
                var docCode2 = "IDK";
                var isDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower2 = true;
                var isDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller2 = false;
                var isDisclosurePostClosingDueToNonNumericalClericalError2 = false;
                var isDisclosurePostClosingDueToCureForToleranceViolation2 = false;
                var postConsummationRedisclosureReasonDate2 = DateTime.Now.Subtract(TimeSpan.FromDays(52)).ToString("d");
                var postConsummationKnowledgeOfEventDate2 = DateTime.Now.Subtract(TimeSpan.FromDays(72)).ToString("d");
                var borrowerDisclosureMetaData2 = new LendersOffice.Integration.DocumentVendor.LQBESignUpdate.BorrowerDisclosureMetadata()
                {
                    IssuedDate = issuedDate2,
                    ReceivedDate = receivedDate2,
                    SignedDate = signedDate2,
                    DeliveryMethod = deliveryMethod2.ToString()
                };

                eSignNotification2.DisclosureMetadata = new DisclosureMetadata()
                {
                    Item = new ClosingDisclosureMetadata()
                    {
                        IssuedDate = issuedDate2,
                        DeliveryMethod = deliveryMethod2.ToString(),
                        ReceivedDate = receivedDate2,
                        IsInitial = isInitial2.ToString(),
                        IsPreview = isPreview2.ToString(),
                        LastDisclosedTRIDLoanProductDescription = lastDisclosedTRIDLoanProductDescription2,
                        APR = aPR2,
                        SignedDate = signedDate2,
                        DocCode = docCode2,
                        IsDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower = isDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower2.ToString(),
                        IsDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller = isDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller2.ToString(),
                        IsDisclosurePostClosingDueToNonNumericalClericalError = isDisclosurePostClosingDueToNonNumericalClericalError2.ToString(),
                        IsDisclosurePostClosingDueToCureForToleranceViolation = isDisclosurePostClosingDueToCureForToleranceViolation2.ToString(),
                        PostConsummationRedisclosureReasonDate = postConsummationRedisclosureReasonDate2,
                        PostConsummationKnowledgeOfEventDate = postConsummationKnowledgeOfEventDate2,
                        BorrowerDisclosureMetadata = borrowerDisclosureMetaData2
                    }
                };

                // Act
                loan.UpdateTridDisclosureDatesForDocGeneration(testArchive, true, false, false, transactionId.ToString(), null, null, null, false);
                loan.ProcessEDisclosureDisclosureTrigger(E_EDisclosureDisclosureEventT.DocumentsSigned, eSignNotification);
                loan.ProcessEDisclosureDisclosureTrigger(E_EDisclosureDisclosureEventT.EConsentReceived, eSignNotification2);

                // Assert
                var closingDisclosure = loan.sClosingDisclosureDatesInfo.ClosingDisclosureDatesList
                        .SingleOrDefault(d => d.TransactionId.Equals(transactionId.ToString()));
                Assert.AreEqual(closingDisclosure.IssuedDate.ToString("d"), issuedDate2);
                Assert.AreEqual(closingDisclosure.DeliveryMethod, deliveryMethod2);
                Assert.AreEqual(closingDisclosure.ReceivedDate.ToString("d"), receivedDate2);
                Assert.AreEqual(closingDisclosure.IsInitial, isInitial2);
                Assert.AreEqual(closingDisclosure.IsPreview, isPreview2);
                Assert.AreEqual(closingDisclosure.LastDisclosedTRIDLoanProductDescription, lastDisclosedTRIDLoanProductDescription2);
                Assert.AreEqual(closingDisclosure.DocVendorApr_rep, aPR2);
                Assert.AreEqual(closingDisclosure.SignedDate.ToString("d"), signedDate2);
                Assert.AreEqual(closingDisclosure.DocCode, docCode2);
                Assert.AreEqual(closingDisclosure.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower, isDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower2);
                Assert.AreEqual(closingDisclosure.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller, isDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller2);
                Assert.AreEqual(closingDisclosure.IsDisclosurePostClosingDueToNonNumericalClericalError, isDisclosurePostClosingDueToNonNumericalClericalError2);
                Assert.AreEqual(closingDisclosure.IsDisclosurePostClosingDueToCureForToleranceViolation, isDisclosurePostClosingDueToCureForToleranceViolation2);
                Assert.AreEqual(closingDisclosure.PostConsummationRedisclosureReasonDate.ToString("d"), postConsummationRedisclosureReasonDate2);
                Assert.AreEqual(closingDisclosure.PostConsummationKnowledgeOfEventDate.ToString("d"), postConsummationKnowledgeOfEventDate2);
            }
        }

        [Test]
        public void ESignUpdate_DelayedESignNotification_MultipleESignNotifications_LoanEstimateTest_DocGeneratedFirst()
        {
            if (!ConstStage.EnableDelayedESignNotificationLeCdRowUpdates)
            {
                return;
            }

            using (var testLoan = LoanForIntegrationTest.Create(TestAccountType.LoTest001, false))
            {
                // Arrange
                var loan = testLoan.CreateNewPageDataWithBypass();
                loan.InitSave(ConstAppDavid.SkipVersionCheck);

                loan.CreateTempArchive(packageHasClosingDisclosure: false, tempArchiveSource: null, shouldSetTempArchive: true, principal: null);
                var testArchive = loan.SaveTemporaryArchive(ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed, "TEST");

                Guid transactionId = Guid.NewGuid();
                Guid consumerId = Guid.NewGuid();

                var eSignNotification = new LQBESignUpdate()
                {
                    TransactionID = transactionId.ToString(),
                    ResponseSource = "TESTT",
                    LoanID = loan.sLId.ToString(),
                    BorrLastName = "TESTER",
                    BorrFirstName = "TESTERTEST",
                    BorrMiddleName = "TESTINNG",
                    BorrSuffix = "SR",
                    BorrFullName = "TESTERTEST TESTINNG TESTER SR",
                    BorrEmail = "TEST@TEST.COM",
                    ConsumerID = consumerId.ToString(),
                    ESignNotification = "Documents Signed for TESTERTEST TESTINNG",
                    Documents = "IDK",
                    OtherInserts = new string[] { "ho", "tho", "co" },
                };

                var issuedDate = DateTime.Now.Subtract(TimeSpan.FromDays(2)).ToString("d");
                var deliveryMethod = E_DeliveryMethodT.Email;
                var receivedDate = DateTime.Now.ToString("d");
                var isInitial = true;
                var lastDisclosedTRIDLoanProductDescription = "TEST TESTTT";
                var aPR = "4.235%";
                var signedDate = DateTime.Now.Subtract(TimeSpan.FromDays(3)).ToString("d");
                var postConsummationRedisclosureReasonDate = DateTime.Now.Subtract(TimeSpan.FromDays(5)).ToString("d");
                var postConsummationKnowledgeOfEventDate = DateTime.Now.Subtract(TimeSpan.FromDays(7)).ToString("d");
                var borrowerDisclosureMetaData = new LendersOffice.Integration.DocumentVendor.LQBESignUpdate.BorrowerDisclosureMetadata()
                {
                    IssuedDate = issuedDate,
                    ReceivedDate = receivedDate,
                    SignedDate = signedDate,
                    DeliveryMethod = deliveryMethod.ToString()
                };

                eSignNotification.DisclosureMetadata = new DisclosureMetadata()
                {
                    Item = new LoanEstimateMetadata()
                    {
                        IssuedDate = issuedDate,
                        DeliveryMethod = deliveryMethod.ToString(),
                        ReceivedDate = receivedDate,
                        IsInitial = isInitial.ToString(),
                        LastDisclosedTRIDLoanProductDescription = lastDisclosedTRIDLoanProductDescription,
                        APR = aPR,
                        SignedDate = signedDate,
                        BorrowerDisclosureMetadata = borrowerDisclosureMetaData
                    }
                };

                var eSignNotification2 = new LQBESignUpdate()
                {
                    TransactionID = transactionId.ToString(),
                    ResponseSource = "TESTT2",
                    LoanID = loan.sLId.ToString(),
                    BorrLastName = "TESTER2",
                    BorrFirstName = "TESTERTEST2",
                    BorrMiddleName = "TESTINNG2",
                    BorrSuffix = "SR2",
                    BorrFullName = "TESTERTEST TESTINNG TESTER SR2",
                    BorrEmail = "TEST@TEST.COM2",
                    ConsumerID = consumerId.ToString(),
                    ESignNotification = "Documents Signed for TESTERTEST TESTINNG2",
                    Documents = "IDK2",
                    OtherInserts = new string[] { "ho2", "tho2", "co2" },
                };

                var issuedDate2 = DateTime.Now.Subtract(TimeSpan.FromDays(22)).ToString("d");
                var deliveryMethod2 = E_DeliveryMethodT.Fax;
                var receivedDate2 = DateTime.Now.ToString("d");
                var isInitial2 = true;
                var lastDisclosedTRIDLoanProductDescription2 = "TEST TESTTT";
                var aPR2 = "4.235%";
                var signedDate2 = DateTime.Now.Subtract(TimeSpan.FromDays(32)).ToString("d");
                var postConsummationRedisclosureReasonDate2 = DateTime.Now.Subtract(TimeSpan.FromDays(52)).ToString("d");
                var postConsummationKnowledgeOfEventDate2 = DateTime.Now.Subtract(TimeSpan.FromDays(72)).ToString("d");
                var borrowerDisclosureMetaData2 = new LendersOffice.Integration.DocumentVendor.LQBESignUpdate.BorrowerDisclosureMetadata()
                {
                    IssuedDate = issuedDate2,
                    ReceivedDate = receivedDate2,
                    SignedDate = signedDate2,
                    DeliveryMethod = deliveryMethod2.ToString()
                };

                eSignNotification2.DisclosureMetadata = new DisclosureMetadata()
                {
                    Item = new LoanEstimateMetadata()
                    {
                        IssuedDate = issuedDate2,
                        DeliveryMethod = deliveryMethod2.ToString(),
                        ReceivedDate = receivedDate2,
                        IsInitial = isInitial2.ToString(),
                        LastDisclosedTRIDLoanProductDescription = lastDisclosedTRIDLoanProductDescription2,
                        APR = aPR2,
                        SignedDate = signedDate2,
                        BorrowerDisclosureMetadata = borrowerDisclosureMetaData2
                    }
                };

                // Act
                loan.UpdateTridDisclosureDatesForDocGeneration(testArchive, true, false, false, transactionId.ToString(), null, null, null, false);
                loan.ProcessEDisclosureDisclosureTrigger(E_EDisclosureDisclosureEventT.DocumentsSigned, eSignNotification);
                loan.ProcessEDisclosureDisclosureTrigger(E_EDisclosureDisclosureEventT.EConsentReceived, eSignNotification2);

                // Assert
                var loanEstimate = loan.sLoanEstimateDatesInfo.LoanEstimateDatesList
                        .SingleOrDefault(d => d.TransactionId.Equals(transactionId.ToString()));
                var closingDisclosure = loan.sClosingDisclosureDatesInfo.ClosingDisclosureDatesList
                        .SingleOrDefault(d => d.TransactionId.Equals(transactionId.ToString()));
                Assert.AreEqual(loanEstimate.IssuedDate.ToString("d"), issuedDate2);
                Assert.AreEqual(loanEstimate.DeliveryMethod, deliveryMethod2);
                Assert.AreEqual(loanEstimate.ReceivedDate.ToString("d"), receivedDate2);
                Assert.AreEqual(loanEstimate.IsInitial, isInitial2);
                Assert.AreEqual(loanEstimate.LastDisclosedTRIDLoanProductDescription, lastDisclosedTRIDLoanProductDescription2);
                Assert.AreEqual(loanEstimate.DocVendorApr_rep, aPR2);
                Assert.AreEqual(loanEstimate.SignedDate.ToString("d"), signedDate2);
            }
        }
    }
}
