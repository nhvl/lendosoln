﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using DataAccess;
using System.Threading;

namespace LendOSolnTest.Common
{
    public static class TestUtilities
    {

        /// <summary>
        /// Opens the file at  TestDataRootFolder.Location + endingFileName  and returns the filestream. 
        /// Be sure to dispose. 
        /// </summary>
        /// <param name="endingFileName"></param>
        /// <returns></returns>
        public static FileStream GetFileStream(string endingFileName)
        {
            return File.OpenRead(TestDataRootFolder.Location + endingFileName);
        }

        public static string GetFilePath(string fileName)
        {
            return string.Concat(TestDataRootFolder.Location, fileName);
        }

        /// <summary>
        /// Reads the file at   TestDataRootFolder.Location + endingFileName and returns a string. 
        /// </summary>
        /// <param name="endingFileName"></param>
        /// <returns></returns>
        public static string ReadFile(string endingFileName)
        {
            using (FileStream fs = GetFileStream(endingFileName) )
            {
                using (StreamReader reader = new StreamReader(fs))
                {
                    return reader.ReadToEnd();
                }
            }
        }

        public static byte[] ReadBinaryFile(string endingFileName)
        {
            using (FileStream fs = GetFileStream(endingFileName))
            {
                return Tools.ReadFully(fs, -1);
            }
        }

        public static void MultiThreadTest(Action actionForAuxiliaryThreads, uint numAuxThreads)
        {
            var threads = new List<Thread>();
            for (int i = 0; i < numAuxThreads; i++)
            {
                ThreadStart ts = new ThreadStart(actionForAuxiliaryThreads);
                threads.Add(new Thread(ts));
            }
            foreach (var thread in threads)
            {
                thread.Start();
            }
            foreach (var thread in threads)
            {
                thread.Join();
            }
        }

        /// <summary>
        /// Generates a string representation of an IDictionary that, when printed, can be
        /// pasted back to a .cs file as a collection initializer.
        /// </summary>
        /// <typeparam name="TKey">The type of the key.</typeparam>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="dict">The dictionary to literalize.</param>
        /// <param name="indentLevel">The number of spaces to indent the literal.</param>
        /// <returns>Literally the string representation of the dictionary.</returns>
        private static string ToLiteral(this IDictionary dict, int indentLevel)
        {
            var str = new StringBuilder();
            str.Append("\r\n");
            str.Append(' ', indentLevel);
            str.Append("{");

            foreach (DictionaryEntry pair in dict)
            {
                str.Append("\r\n");
                str.Append(' ', indentLevel + 4);
                str.AppendFormat("{{ {0}, {1} }}, ", pair.Key.ToLiteral(indentLevel + 8), pair.Value.ToLiteral(indentLevel + 8));
            }
            str.Append("}");

            return str.ToString();
        }

        /// <summary>
        /// Fixes up the output of some common c# data types to match their literal
        /// representation. Falls back to .ToString() if there isn't a special case.
        /// Add more if you run into a type that doesn't work.
        /// The intended use case is to be used against values that were extracted
        /// from a Dictionary<string, object>, so using the overloads for each type
        /// is out.
        /// </summary>
        /// <param name="o">The object to literalize</param>
        /// <returns>Literally the string representation of the object.</returns>
        public static string ToLiteral(this object o)
        {
            return ToLiteral(o, 0);
        }
         
        /// <summary>
        /// Private method to track the indent level for collection types.
        /// </summary>
        /// <param name="o">The object to literalize.</param>
        /// <param name="indentLevel">The level of indentation to use.</param>
        /// <returns>Literally the string representation of the object.</returns>
        private static string ToLiteral(this object o, int indentLevel)
        {
            using (var writer = new StringWriter())
            {
                using (var provider = System.CodeDom.Compiler.CodeDomProvider.CreateProvider("CSharp"))
                {
                    Type t = o.GetType();
                    if (t.IsValueType || t == typeof(string))
                    {
                        provider.GenerateCodeFromExpression(new System.CodeDom.CodePrimitiveExpression(Convert.ChangeType(o, t)), writer, null);
                        return writer.ToString();
                    }
                    else
                    {
                        Type[] interfaces = t.GetInterfaces();

                        if (interfaces.Contains(typeof(System.Collections.IDictionary)))
                        {
                            return ToLiteral((IDictionary)o, indentLevel);
                        }
                        return o.ToString();
                    }
                }
            }
        }

        /// <summary>
        /// Given a dictionary and a set of keys, returns a new Dictionary that contains only the
        /// key-value pairs for the provided set of keys.
        /// Keys in the key set that are missing from the input dictionary will be silently ignored.
        /// </summary>
        /// <typeparam name="TKey">The type of the keys.</typeparam>
        /// <typeparam name="TValue">The type of the values.</typeparam>
        /// <param name="dict">The dictionary to filter.</param>
        /// <param name="keys">The keys whose values we want to extract.</param>
        /// <returns>A new Dictionary with only the desired key-value pairs.</returns>
        public static Dictionary<TKey, TValue> FilterByKeys<TKey, TValue>(IDictionary<TKey,TValue> dict, IEnumerable<TKey> keys)
        {
            if (dict == null)
            {
                return null;
            }

            Dictionary<TKey, TValue> retDict = new Dictionary<TKey, TValue>();

            foreach(TKey key in keys)
            {
                if (dict.ContainsKey(key))
                {
                    retDict.Add(key, dict[key]);
                }
            }

            return retDict;
        }

        /// <summary>
        /// Returns the indicated properties and their values as keys and values in a dictionary.
        /// </summary>
        /// <param name="obj">The object whose properties we are extracting.</param>
        /// <param name="keys">The list of properties we want to extract.</param>
        /// <returns>
        /// A dictionary with the extracted valuse as key-value pairs.
        /// Keys prepended with @ indicate that the actual value could not be found, so the
        /// returned value should not be taken literally.
        /// </returns>
        public static Dictionary<string, object> ExtractPropertiesAsDict(object obj, IEnumerable<string> keys)
        {
            if (obj == null)
            {
                return null;
            }

            Dictionary<string, object> retDict = new Dictionary<string, object>();
            Type t = obj.GetType();

            foreach (string key in keys)
            {
                System.Reflection.PropertyInfo prop = t.GetProperty(key);

                if (prop != null)
                {
                    retDict.Add(key, prop.GetValue(obj, null));
                }
                else
                {
                    retDict.Add("@" + key, "missing property");
                }
            }

            return retDict;
        }

        /// <summary>
        /// Compares the actual values of the properties of an object against the expected
        /// values of those properties, which are stored in a Dictionary. Only checks those
        /// properties whose keys are in the Dictionary, and uses Equals to determine equality.
        /// </summary>
        /// <typeparam name="T">The type of the actual object.</typeparam>
        /// <param name="expectedProps">The expected values as a dictionary where the keys
        /// are the names of the properties that we want to test, and the values are the
        /// expected values.</param>
        /// <param name="actual">The actual value whose properties we are testing.</param>
        /// <returns>
        /// A dictionary of the actual values of the properties that are unequal.
        /// Keys prepended with @ indicate that the actual value could not be found, so the
        /// returned value should not be taken literally.
        /// Keys prepended with @@ indicate problems with the parameters, and do not correspond
        /// to properties.
        /// </returns>
        public static Dictionary<string, object> FindUnequalProperties(IDictionary<string, object> expectedProps, object actual)
        {
            Dictionary<string, object> retDict = new Dictionary<string, object>();

            if (actual == null)
            {
                retDict.Add("@@Actual", "null");
            }

            if (expectedProps == null)
            {
                retDict.Add("@@Expected", "null");
            }

            if (retDict.Count > 0)
            {
                return retDict;
            }

            Type actualType = actual.GetType();

            foreach (string key in expectedProps.Keys)
            {
                System.Reflection.PropertyInfo prop = actualType.GetProperty(key);
                if (prop != null)
                {
                    var actualVal = prop.GetValue(actual, null);

                    if (!expectedProps[key].Equals(actualVal))
                    {
                        retDict.Add(key, actualVal);
                    }
                }
                else
                {
                    retDict.Add("@" + key, "missing property");
                }
            }

            return retDict;
        }
    }
}
