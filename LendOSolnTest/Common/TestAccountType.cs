/// Author: David Dao

using System;

namespace LendOSolnTest.Common
{
	public enum TestAccountType
	{
        LoTest001,
        Corporate_Underwriter,
        Corporate_LockDesk,
        Corporate_Processor,
        Corporate_LoanOfficer,
        Corporate_LenderAccountExec,
        Corporate_Manager,
        Branch1_Underwriter,
        Branch1_LockDesk,
        Branch1_Processor,
        Branch1_LoanOfficer,
        Branch1_LenderAccountExec,
        Branch1_Manager,
        MainBranch_Underwriter,
        MainBranch_LockDesk,
        MainBranch_Processor,
        MainBranch_LoanOfficer,
        MainBranch_LenderAccountExec,
        MainBranch_Manager,
        Underwriter,
        LockDesk,
        Processor,
        LoanOfficer,
        LenderAccountExec,
        Manager,
        Pml_Supervisor,
        Pml_User0,
        Pml_User1,
        Pml_User2,
        Pml_BP1,
        Pml_LO1,
        Corporate_InternalAdmin,
        LockDesk_AllPerFieldPerm,
        LockDesk_LockDeskPerFieldPerm,
        LockDesk_CloserPerFieldPerm,
        LockDesk_AccountantPerFieldPerm
	}
}
