﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using LendersOffice.Common;
using System.Net;

namespace LendOSolnTest.Common
{
    [TestFixture]
    public class IPAddressRangeTests
    {
        #region ( Members )

        private static readonly IList<string> X_VALID_IPV6_STRINGS_ORDERED = new List<string>()
        {
                "::"
            ,   "2002:b0c:d82::b0c:d82"
            ,   "ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff"
        };
        private static readonly IList<string> X_VALID_IPV4_STRINGS_ORDERED = new List<string>()
        {
                "0.0.0.0"
            ,   "91.209.196.0"
            ,   "208.116.56.43"
            ,   "255.255.255.255"
        };
        private static readonly IList<string> X_INVALID_IP_STRINGS = new List<string>()
        {
                "256.255.255.255"   // TOO BIG
            ,   "-1.0.0.0"          // TOO SMALL
            ,   "ASDF"              // WHAT?
            ,   null
            ,   "f::f::f"           // Can't have more than one '::'
        };

        #endregion

        [Test]
        public void ConstructorTest()
        {
            #region ( valid ipv4 constructors test )
            for (var i = 0; i < X_VALID_IPV4_STRINGS_ORDERED.Count(); i++)
            {
                var firstIPString = X_VALID_IPV4_STRINGS_ORDERED[i];
                var firstIP = IPAddress.Parse(firstIPString);
                for (var j = i; j < X_VALID_IPV4_STRINGS_ORDERED.Count(); j++)
                {
                    var secondIPString = X_VALID_IPV4_STRINGS_ORDERED[j];
                    var secondIP = IPAddress.Parse(secondIPString);

                    // test that no errors are thrown.
                    {
                        var rangeWithConstructor1 = new IPAddressRange(firstIPString, secondIPString);
                        var rangeWithConstructor2 = new IPAddressRange(firstIP, secondIP);
                    }
                }
            }
            #endregion

            #region ( valid ipv6 constructors test )
            for (var i = 0; i < X_VALID_IPV6_STRINGS_ORDERED.Count(); i++)
            {
                var firstIPString = X_VALID_IPV6_STRINGS_ORDERED[i];
                var firstIP = IPAddress.Parse(firstIPString);
                for (var j = i; j < X_VALID_IPV6_STRINGS_ORDERED.Count(); j++)
                {
                    var secondIPString = X_VALID_IPV6_STRINGS_ORDERED[j];
                    var secondIP = IPAddress.Parse(secondIPString);

                    // test that no errors are thrown.
                    {
                        var rangeWithConstructor1 = new IPAddressRange(firstIPString, secondIPString);
                        var rangeWithConstructor2 = new IPAddressRange(firstIP, secondIP);
                    }
                }
            }
            #endregion

            #region ( invalid ipstrings test )
            for (var i = 0; i < X_INVALID_IP_STRINGS.Count(); i++)
            {
                var ipString = X_INVALID_IP_STRINGS[i];

                try
                {
                    var ip = new IPAddressRange(ipString, ipString);
                    Assert.Fail("IPAddresRange constructor should not accept " + ipString);
                }
                catch (ArgumentException)
                {
                }
                catch (FormatException)
                {
                }
            }
            #endregion

            #region ( invalid ranges because of mixed families )
            foreach (var ipv4AddressString in X_VALID_IPV4_STRINGS_ORDERED)
            {
                foreach (var ipv6AddressString in X_VALID_IPV6_STRINGS_ORDERED)
                {
                    try
                    {
                        var ip = new IPAddressRange(ipv4AddressString, ipv6AddressString);
                        Assert.Fail("Shouldn't be able to mix and match ip families: " + ipv4AddressString + " " + ipv6AddressString);
                    }
                    catch (ArgumentException)
                    {
                    }

                    try
                    {
                        var ip = new IPAddressRange(ipv6AddressString, ipv4AddressString);
                        Assert.Fail("Shouldn't be able to mix and match ip families: " + ipv6AddressString + " " + ipv4AddressString);
                    }
                    catch (ArgumentException)
                    {
                    }
                }
            }
            #endregion
        }
        [Test]
        public void ContainsTest()
        {
            var validIPV4s = (from ip in X_VALID_IPV4_STRINGS_ORDERED
                             select IPAddress.Parse(ip)).ToList();

            var validIPV6s = (from ip in X_VALID_IPV6_STRINGS_ORDERED
                              select IPAddress.Parse(ip)).ToList();

            var validIPV4Ranges = new List<IPAddressRange>();
            var validIPV6Ranges = new List<IPAddressRange>();
            
            #region ( create IPV4 Ranges, test all ipv4s for membership ) 
            for (int i = 0; i < validIPV4s.Count(); i++)
            {
                var startIP = validIPV4s[i];

                for (int k = i; k < validIPV4s.Count(); k++)
                {
                    var endIP = validIPV4s[k];
                    var range = new IPAddressRange(startIP, endIP);

                    for (int j = 0; j < validIPV4s.Count(); j++)
                    {
                        var midIP = validIPV4s[j];
                        
                        if (i <= j && j <= k)
                        {
                            Assert.That(range.Contains(midIP));
                        }
                        else
                        {
                            Assert.That(false == range.Contains(midIP));
                        }
                    }
                    validIPV4Ranges.Add(range);
                }
            }
            #endregion
            #region ( create IPV6 Ranges, test all ipv6s for membership )
            for (int i = 0; i < validIPV6s.Count(); i++)
            {
                var startIP = validIPV6s[i];

                for (int k = i; k < validIPV6s.Count(); k++)
                {
                    var endIP = validIPV6s[k];
                    var range = new IPAddressRange(startIP, endIP);

                    for (int j = 0; j < validIPV6s.Count(); j++)
                    {
                        var midIP = validIPV6s[j];

                        if (i <= j && j <= k)
                        {
                            Assert.That(range.Contains(midIP));
                        }
                        else
                        {
                            Assert.That(false == range.Contains(midIP));
                        }
                    }
                    validIPV6Ranges.Add(range);
                }
            }
            #endregion

            #region ( make sure ipv4 ranges don't contain ipv6s )

            foreach (var ipv4Range in validIPV4Ranges)
            {
                foreach (var ipv6 in validIPV6s)
                {
                    Assert.That(ipv4Range.Contains(ipv6) == false, "ipv4Range " + ipv4Range + " should not contain ipv6 address " + ipv6);
                }
            }
            #endregion
            #region ( make sure ipv6 ranges don't contain ipv4s )

            foreach (var ipv6Range in validIPV6Ranges)
            {
                foreach (var ipv4 in validIPV4s)
                {
                    Assert.That(ipv6Range.Contains(ipv4) == false, "ipv6Range " + ipv6Range + " should not contain ipv4 address " + ipv4);
                }
            }
            #endregion
        }
    }
}
