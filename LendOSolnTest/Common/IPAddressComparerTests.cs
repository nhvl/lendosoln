﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using LendersOffice.Common;
using System.Net;

namespace LendOSolnTest.Common
{
    [TestFixture]
    public class IPAddressComparerTests
    {
        #region ( Members )

        private static readonly IList<string> X_VALID_IPV6_STRINGS_ORDERED = new List<string>()
        {
                "::"
            ,   "2002:b0c:d82::b0c:d82"
            ,   "ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff"
        };
        private static readonly IList<string> X_VALID_IPV4_STRINGS_ORDERED = new List<string>()
        {
                "0.0.0.0"
            ,   "91.209.196.0"
            ,   "208.116.56.43"
            ,   "255.255.255.255"
        };

        #endregion

        
        [Test]
        public void CompareTest()
        {
            CompareTestImpl(IPAddressComparer.Compare);
        }
        [Test]
        public void CompareToTest()
        {
            CompareTestImpl(IPAddressComparer.CompareTo);
        }

        /// <summary>
        /// asserts that (for ipv4 and ipv6 families)
        /// - can only compare withing families
        /// - can't compare to null
        /// </summary>
        private void CompareTestImpl(Func<IPAddress, IPAddress, int> comparer)
        {
            var validIPV4s = (from ip in X_VALID_IPV4_STRINGS_ORDERED
                              select IPAddress.Parse(ip)).ToList();

            var validIPV6s = (from ip in X_VALID_IPV6_STRINGS_ORDERED
                              select IPAddress.Parse(ip)).ToList();


            CompareWithinFamilyTest(validIPV4s, comparer);
            CompareWithinFamilyTest(validIPV6s, comparer);

            CompareAcrossFamiliesTest(validIPV4s, validIPV6s, comparer);

            NullCompareTest(validIPV4s[0], comparer);
        }


        /// <summary>
        /// asserts that (for ipv4 and ipv6 families):
        /// - can't compare ips from different families.
        /// </summary>
        private void CompareAcrossFamiliesTest(IEnumerable<IPAddress> ipsFromOneFamily, IEnumerable<IPAddress> ipsFromOtherFamily, Func<IPAddress, IPAddress, int> compare)
        {
            foreach (var ipF1 in ipsFromOneFamily)
            {
                foreach (var ipF2 in ipsFromOtherFamily)
                {
                    try
                    {
                        var comparison = compare(ipF1, ipF2);

                        Assert.Fail(
                            string.Format(
                                "Should not be allowed to compare ips across families. ip1 is {0}, ip2 is {1}"
                                , ipF1
                                , ipF2
                            )
                        );
                    }
                    catch (ArgumentException)
                    {
                    }
                }
            }
        }

        /// <summary>
        /// ensures comparing ips in the same family yields:
        /// - (-1) if the first is bigger
        /// - 0 if they are equal
        /// - 1 if the second is bigger.
        /// </summary>
        private void CompareWithinFamilyTest(IList<IPAddress> sortedIPsWithinSameFamily, Func<IPAddress, IPAddress, int> compare)
        {
            for (int i = 0; i < sortedIPsWithinSameFamily.Count(); i++)
            {
                var firstIP = sortedIPsWithinSameFamily[i];

                for (var j = 0; j < sortedIPsWithinSameFamily.Count(); j++)
                {
                    var secondIP = sortedIPsWithinSameFamily[j];

                    if (i < j)
                    {
                        Assert.That(compare(firstIP, secondIP) != -1, firstIP + " should not be bigger than " + secondIP);
                    }
                    else if (j < i)
                    {
                        Assert.That(compare(firstIP, secondIP) != 1, firstIP + " should not be smaller than " + secondIP);
                    }
                    else // if j == i
                    {
                        Assert.That(compare(firstIP, secondIP) == 0, firstIP + " should be equal to " + secondIP);
                    }
                }
            }
        }

        /// <summary>
        /// asserts that comparing to null barks an error.
        /// </summary>
        private void NullCompareTest(IPAddress ip, Func<IPAddress, IPAddress, int> compare)
        {
            IPAddress nullIP = null;
            try
            {
                var comparison = compare(nullIP, ip);
                Assert.Fail("Can't compare null to an IP");
            }
            catch (NullReferenceException)
            {
            }
            try
            {
                var comparison = compare(ip, nullIP);
                Assert.Fail("Can't compare an IP to null");
            }
            catch (NullReferenceException)
            {
            }
        }
    }
}
