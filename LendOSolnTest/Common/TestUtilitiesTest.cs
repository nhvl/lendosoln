﻿// <copyright file="TestUtilitiesTest.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Douglas Telfer
//    Date:   6/24/2015
// </summary>
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace LendOSolnTest.Common
{
    /// <summary>
    /// And I am a snake head eating
    /// the head on the opposite side.
    /// </summary>
    [TestFixture]
    public class TestUtilitiesTest
    {
        /// <summary>
        /// Tests to ensure that nested dictionaries are handled correctly.
        /// </summary>
        [Test]
        public void NestedLiteralDict()
        {
            Dictionary<string, object> yoyoDawgDict =
                new Dictionary<string, object>
                {
                    {"intro", "Yo dawg"},
                    {"body", "I heard you like dictionaries"}
                };

            Dictionary<object, object> yoDawgDict =
                new Dictionary<object, object>
                {
                    {0.00M, true},
                    {"intro", "Yo dawg"},
                    {"body", "I heard you like dictionaries"},
                    {"result", "So I put a dictionary in your dictionary"},
                    {"yoyodawg", yoyoDawgDict},
                    {"punchline", "So you can look up while you look up."}
                };

            string expected =
"\r\n" +
"{\r\n" +
"    { 0.00m, true }, \r\n" +
"    { \"intro\", \"Yo dawg\" }, \r\n" +
"    { \"body\", \"I heard you like dictionaries\" }, \r\n" +
"    { \"result\", \"So I put a dictionary in your dictionary\" }, \r\n" +
"    { \"yoyodawg\", \r\n" +
"        {\r\n" +
"            { \"intro\", \"Yo dawg\" }, \r\n" +
"            { \"body\", \"I heard you like dictionaries\" }, } }, \r\n" +
"    { \"punchline\", \"So you can look up while you look up.\" }, }";

            Assert.AreEqual(expected, yoDawgDict.ToLiteral());
        }

        /// <summary>
        /// Tests that FilterByKeys returns the expected keys and values.
        /// </summary>
        [Test]
        public void FilterByKeysTest1()
        {
            Dictionary<object, object> sourceDict =
                new Dictionary<object, object>
                {
                    { "$100.00", 100.00M },
                    { 100.00M, 1.0e2 },
                    { 1.0e2, 100 },
                    { 100, "$100.00" },
                    { "$200.00", 200.00M },
                    { 200.00M, 2.0e2 },
                    { 2.0e2, 200 },
                    { 200, "$200.00" }
                };

            List<object> filterKeys =
                new List<object> { "$100.00", 1.0e2, 200.00M, 200 };

            Dictionary<object, object> actual = TestUtilities.FilterByKeys(sourceDict, filterKeys);
            Dictionary<object, object> expected =
                new Dictionary<object, object>
                {
                    { "$100.00", 100.00M },
                    { 1.0e2, 100 },
                    { 200.00M, 2.0e2 },
                    { 200, "$200.00" }
                };

            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// Tests that ExtractPropertiesAsDict returns the expected Dictionary.
        /// </summary>
        [Test]
        public void ExtractPropertiesAsDictTest1()
        {
            Type type = typeof(Type);
            string[] props = {"FullName", "IsValueType", "IsClass"};

            Dictionary<string, object> actual =
                TestUtilities.ExtractPropertiesAsDict(type, props);

            Dictionary<string, object> expected =
                new Dictionary<string,object>
                {
                    {"FullName", "System.Type"},
                    {"IsValueType", false},
                    {"IsClass", true}
                };

            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// Tests that FindUnequalProperties returns the expected Dictionary.
        /// </summary>
        [Test]
        public void FindUnequalPropertiesTest()
        {
            Type type = typeof(Type);
            string[] props = { "FullName", "IsValueType", "IsClass" };
            Dictionary<string, object> expectedProps =
                new Dictionary<string,object>
                {
                    {"FullName", "System.Typo"},
                    {"IsValueType", false},
                    {"IsClass", false}
                };

            Dictionary<string, object> actual =
                TestUtilities.FindUnequalProperties(expectedProps, type);

            Dictionary<string, object> expected =
                new Dictionary<string, object>
                {
                    {"FullName", "System.Type"},
                    {"IsClass", true}
                };

            Assert.AreEqual(expected, actual);
        }
    }
}
