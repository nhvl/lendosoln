﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using LendersOffice.Common;
using LendersOffice.Constants;
using NUnit.Framework;

namespace LendOSolnTest.Common
{
    [TestFixture, Ignore("Broken on Teamcity")]
    class MultiPartParsing
    {
        [Test]
        public void MultiPartEquifaxAttachment()
        {
            string fileContents = File.ReadAllText(Path.Combine(ConstSite.TestDataFolder, @"Equifax4506_T\Sample Form Attachment- Request.txt"));
            MimeMultipartContent multiPartParts = MimeMultipartContent.Parse(fileContents, "--=C642062B70FB498299F0F50D2A190B3C");

            Assert.AreEqual("--=C642062B70FB498299F0F50D2A190B3C", multiPartParts.Boundary);
            Assert.AreEqual(2, multiPartParts.Parts.Count);

            Assert.AreEqual("application/x-ofx", multiPartParts.Parts[0].Headers["Content-Type"]);

            // Could compare strings directly or compute a hash to be even more sure of the contents, but length and number of lines should be sufficient for now.
            Assert.AreEqual(1166, multiPartParts.Parts[0].Content.Length);
            int firstPartNumLines = multiPartParts.Parts[0].Content.Length - multiPartParts.Parts[0].Content.Replace("\n", string.Empty).Length + 1;
            Assert.AreEqual(34, firstPartNumLines);

            Assert.AreEqual("application/pdf", multiPartParts.Parts[1].Headers["Content-Type"]);
            Assert.AreEqual("base64", multiPartParts.Parts[1].Headers["Content-Transfer-Encoding"]);
            Assert.AreEqual("SignedAuthForm.pdf", multiPartParts.Parts[1].Headers["Content-Location"]);
            Assert.AreEqual(80045, multiPartParts.Parts[1].Content.Length);
        }

        [Test]
        public void MultiPartEquifaxRetrieveResponse()
        {
            string fileContents = File.ReadAllText(Path.Combine(ConstSite.TestDataFolder, @"Equifax4506_T\Sample Retrieve Order- Response.txt"));
            MimeMultipartContent multiPartParts = MimeMultipartContent.Parse(fileContents, "--=C642062B70FB498299F0F50D2A190B3C");

            Assert.AreEqual(2, multiPartParts.Parts.Count);

            Assert.AreEqual(962740, multiPartParts.Parts[0].Content.Length);
            int firstPartNumLines = multiPartParts.Parts[0].Content.Length - multiPartParts.Parts[0].Content.Replace("\n", string.Empty).Length + 1;
            Assert.AreEqual(21093, firstPartNumLines);

            Assert.AreEqual(80045, multiPartParts.Parts[1].Content.Length);

            Assert.AreEqual("application/pdf", multiPartParts.Parts[1].Headers["Content-Type"]);
            Assert.AreEqual("base64", multiPartParts.Parts[1].Headers["Content-Transfer-Encoding"]);
            Assert.AreEqual("2134545-response.pdf", multiPartParts.Parts[1].Headers["Content-Location"]);
        }

        [Test]
        public void MultiPartDuFile()
        {
            string fileContents = File.ReadAllText(Path.Combine(ConstSite.TestDataFolder, @"DU\XIS_CaseFileExportResponse_OPM_31159.txt"));
            MimeMultipartContent multiPartParts = MimeMultipartContent.Parse(fileContents, "--------ieoau._._+2_8_GoodLuck8.3-ds0d0J0S0Kl234324jfLdsjfdAuaoei-----");

            Assert.AreEqual(8, multiPartParts.Parts.Count);
            Assert.AreEqual(1000, multiPartParts.Parts[1].Content.Length);
        }
    }
}
