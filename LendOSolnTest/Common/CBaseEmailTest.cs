﻿namespace LendOSolnTest.Common
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Email;
    using NUnit.Framework;

    [TestFixture]
    public class CBaseEmailTest
    {

        #region ( Test Methods )

        [Test]
        public void AttachmentLoopThroughTest()
        {
            // Check that the attachments added are the same as what the CBaseEmail stores.

            CBaseEmail cbe = standardCBE();
            int NUMATTACHMENTS = 3;

            string[] attachmentNames = new string[NUMATTACHMENTS];
            attachmentNames[0] = "first.txt";
            attachmentNames[1] = "second.txt";
            attachmentNames[2] = "third.txt";

            string[] attachmentDatas = new string[NUMATTACHMENTS];
            attachmentDatas[0] = "the data for the first";
            attachmentDatas[1] = "second's data.  not much of a body.";
            attachmentDatas[2] = "last and least.";

            for (int i = 0; i < NUMATTACHMENTS; i++)
            {
                cbe.AddAttachment(attachmentNames[i], attachmentDatas[i]);
            }

            Attachment[] Attachments = cbe.Attachments;
            Assert.That(Attachments.Length == NUMATTACHMENTS);
            //foreach (Attachment attachment in Attachments)
            //{
            //    Console.WriteLine("attachment name = " + attachment.Name);
            //    Console.WriteLine("attachment data = " + attachment.Data);
            //}
            for (int i = 0; i < NUMATTACHMENTS; i++)
            {
                Assert.That(attachmentNames[i] == Attachments[i].Name, "attachment " + i + "'s name should have matched");
                Assert.That(attachmentDatas[i] == Attachments[i].Data, "attachment " + i + "'s data should have matched");
            }
        }

        [Test]
        public void AttachmentTestsManyTypes()
        {
            //Console only test.  adds different attachment types.
            CBaseEmail cbe = new CBaseEmail(ConstAppDavid.SystemBrokerGuid)
            {
                Subject = "Subject1",
                From = "from@from.com",
                Bcc = "",
                To = "to@to.com",
                CCRecipient = "cc@cc.com",
                Message = "message aka body."
            };
            //! get rid of these silly attachments when done testing.
            cbe.AddAttachment("a_txtfile.txt", "here is the attachment!");

            string fileToRead = "Grid_Letter_Test.pdf";
            byte[] fileAsByteArray = LendOSolnTest.Common.TestUtilities.ReadBinaryFile(fileToRead);
            cbe.AddAttachment("a_pdf_file.pdf", fileAsByteArray);
            //if (cbe.Attachments == null || cbe.Attachments.Length == 0)
            //{
            //    Console.Write("would send first email w/o attachments");
            //}
            //Console.Write(cbe);

            //! get rid of this no attachment test.
            CBaseEmail cbe2 = new CBaseEmail(ConstAppDavid.SystemBrokerGuid)
            {
                Subject = "Subject1",
                From = "from@from.com",
                Bcc = "",
                To = "to@to.com",
                CCRecipient = "cc@cc.com",
                Message = "message aka body."
            };
            //if (cbe2.Attachments == null || cbe2.Attachments.Length == 0)
            //{
            //    Console.Write("would send second email w/o attachments");
            //}

            //! get rid of this bad attachment test
            CBaseEmail cbe3 = new CBaseEmail(ConstAppDavid.SystemBrokerGuid)
            {
                Subject = "Subject1",
                From = "from@from.com",
                Bcc = "",
                To = "to@to.com",
                CCRecipient = "cc@cc.com",
                Message = "message aka body."
            };
            cbe3.AddAttachment("", "");
            cbe3.AddAttachment("", "lol");
            cbe3.AddAttachment(".txt", "");
            cbe3.AddAttachment(".txt", "lol");
            //if (cbe3.Attachments == null || cbe3.Attachments.Length == 0)
            //{
            //    Console.Write("would send third email w/o attachments");
            //}
        }

        [Test]
        public void FileReadAndConvertTest()
        {
            //Reads a file into byte array (using a TestUtilities function),
            //converts it to a string, converts it back to byte array,
            //and checks that both byte arrays are identical.

            string fileToRead = "Grid_Letter_Test.pdf";            

            //Readfile into byte[]
            byte[] fileAsByteArray = LendOSolnTest.Common.TestUtilities.ReadBinaryFile(fileToRead);
            
            //Convert it to base 64 string
            string fileAsString64 = Convert.ToBase64String(fileAsByteArray);

            //Convert it back to a byte[]
            byte[] fileReByteized = Convert.FromBase64String(fileAsString64);

            //Check that all bytes match b/t the two byte arrays.
            int i = 0;
            bool fileMismatched = false;
            while (i < fileReByteized.Length)
            {
                if (fileAsByteArray[i] != fileReByteized[i])
                {
                    fileMismatched = true;
                    break;
                }
                ++i;
            }
            Assert.That(!fileMismatched);

        }

        [Test]
        public void FiveParameterConstructorTest()
        {
            //This method creates a CBaseEmail object with a five parameter constructor
            //and checks that its (more than 5) fields are set to the appropriate defaults.
            String subject = "Subject";
            String from = "from@from.com";
            String bcc = "bcc@bcc.com";
            String to = "to@to.com";
            String body = "This is the body.";
            CBaseEmail cbe = new CBaseEmail(ConstAppDavid.SystemBrokerGuid)
            {
                Subject = subject,
                From = from,
                Bcc = bcc,
                To = to,
                Message = body
            };
            Assert.That(cbe.IsHtmlEmailSpecified, Is.EqualTo(true));
            Assert.That(!cbe.IsHtmlEmail);
            Assert.That(cbe.Subject, Is.EqualTo(subject));
            Assert.That(cbe.From, Is.EqualTo(from));
            Assert.That(cbe.Message, Is.EqualTo(body));
            Assert.That(cbe.DisclaimerType, Is.EqualTo(E_DisclaimerType.NORMAL));
            Assert.That(cbe.Bcc, Is.EqualTo(bcc));
            Assert.That(cbe.To, Is.EqualTo(to));            
        }

        [Test]
        public void FunnyCharactersInRecipientListTest()
        {
            String subject = " Subject ";
            String subjectOut = subject.Trim();
            String from = " from@from.com ";
            String fromOut = from.Trim();
            String bcc = " bcc@bcc.com ";
            String bccOut = bcc.Trim();
            String to = " to@t'\"o.com ";
            //System.Console.WriteLine(to);
            String toOut = to.Trim();
            String cc = "cc@c¿.com   ";
            String ccOut = cc.Trim();
            String body = " This is the body. ";
            String bodyOut = body;

            bccOut = bccOut.Replace("\'", "").Replace("\"", "");
            toOut = toOut.Replace("\'", "").Replace("\"", "");
            ccOut = ccOut.Replace("\'", "").Replace("\"", "");
            CBaseEmail cbe = new CBaseEmail(ConstAppDavid.SystemBrokerGuid)
            {
                Subject = subject,
                From = from,
                Bcc = bcc,
                To = to,
                CCRecipient = cc,
                Message = body
            };

            String MessageInXml = cbe.ToString();
            //System.Console.Write(MessageInXml);

            Assert.That(cbe.IsHtmlEmailSpecified, Is.EqualTo(true));
            Assert.That(!cbe.IsHtmlEmail);
            Assert.That(cbe.Subject, Is.EqualTo(subjectOut));
            Assert.That(cbe.From, Is.EqualTo(fromOut));
            Assert.That(cbe.Message, Is.EqualTo(bodyOut));
            Assert.That(cbe.DisclaimerType, Is.EqualTo(E_DisclaimerType.NORMAL));
            Assert.That(cbe.Bcc, Is.EqualTo(bccOut));
            Assert.That(cbe.To, Is.EqualTo(toOut));
            Assert.That(cbe.CCRecipient, Is.EqualTo(ccOut));
        }
        
        [Test]
        public void MultipleAttachmentTest()
        {
            // Only shows the xml version of the CBaseEmail in the Console
            String sSubject = "Subj";
            String sFrom = "from@from.com";
            String sBcc = "bcc@bcc.com";
            String sTo = "to@to.com";
            String sCC = "cc@cc.com";
            String sMessage = "email message";
            Boolean sIsHtmlEmail = true;
            CBaseEmail cbe = new CBaseEmail(ConstAppDavid.SystemBrokerGuid)
            {
                Subject = sSubject,
                From = sFrom,
                Bcc = sBcc,
                To = sTo,
                CCRecipient = sCC,
                Message = sMessage,
                IsHtmlEmail = sIsHtmlEmail
            };
            cbe.AddAttachment("first", "these characters are the string representing the first attachement");
            cbe.AddAttachment("first", "dang it... so close.  Next time I'll get first.");
            cbe.AddAttachment("third", "The guys above me aren't attachments.");
            string MessageInXml = cbe.ToString();
            //System.Console.Write(MessageInXml);
        }

        [Test]
        public void NoParameterConstructorTest()
        {
            // Checks that a parameterless constructed CBaseEmail
            // has the default fields we expect.

            CBaseEmail cbe = new CBaseEmail(ConstAppDavid.SystemBrokerGuid);
            String MessageInXml = cbe.ToString();
            //System.Console.Write(MessageInXml);
            Assert.That(cbe.IsHtmlEmailSpecified, Is.EqualTo(true));
            Assert.That(!cbe.IsHtmlEmail);
            Assert.That(cbe.Subject, Is.EqualTo(""));
            Assert.That(cbe.From, Is.EqualTo(""));
            Assert.That(cbe.Message, Is.EqualTo(""));
            Assert.That(cbe.DisclaimerType, Is.EqualTo(E_DisclaimerType.NORMAL));
            Assert.That(cbe.Bcc, Is.EqualTo(""));
            Assert.That(cbe.To, Is.EqualTo(""));
            Assert.That(cbe.CCRecipient, Is.EqualTo(""));
        }

        [Test]
        public void SerDesNoAttachmentsTest()
        {
            // Creates CBaseEmail w/o Attachments
            // Serializes it, deserializes it to a new CBaseEmail, and reserializes that
            // checks that both strings are equal.
            // also checks that the AttachmentArray of the second email has length 0.

            CBaseEmail cbe = standardCBE();
            string messageInXml = cbe.ToString();
            //Console.Write(messageInXml);

            byte[] ba = Encoding.ASCII.GetBytes(messageInXml);
            MemoryStream ms = new MemoryStream(ba);
            CBaseEmail cbePostSerialize = CBaseEmail.ToObject(ms);
            string messageInXml2 = cbePostSerialize.ToString();
            //Console.Write(messageInXml2);

            Assert.That(messageInXml == messageInXml2,
                "The ser-deser-serialized version of the orig CBaseEmail should be the same as the serialized version");

            Attachment[] attachmentArray = cbePostSerialize.Attachments;
            //Console.WriteLine(attachmentArray);

            //Console.WriteLine("the number of attachments is:\t" + attachmentArray.Length);
            Assert.That(attachmentArray.Length == 0, "the attachment array length should be 0.");
        }

        [Test]
        public void SerDesWithAttachmentsTest()
        {
            // Creates CBaseEmail w 3 Attachments
            // Serializes it, deserializes it to a new CBaseEmail, and reserializes that
            // checks that both strings are equal.
            // also checks that the AttachmentArray of the second email has length 3.
            // lastly checks that the binary attachment byte array is the same as what we started with.

            CBaseEmail cbe = standardCBE();

            string fileToRead = "Grid_Letter_Test.pdf";
            byte[] fileAsByteArray = LendOSolnTest.Common.TestUtilities.ReadBinaryFile(fileToRead);
            string fileAsString64 = Convert.ToBase64String(fileAsByteArray);

            cbe.AddAttachment("first", fileAsString64);
            cbe.AddAttachment("second", fileAsString64);
            cbe.AddAttachment("last", "yo");
            string messageInXml = cbe.ToString();
            //Console.Write(messageInXml);


            byte[] ba = Encoding.ASCII.GetBytes(messageInXml);
            MemoryStream ms = new MemoryStream(ba);
            CBaseEmail cbePostSerialize = CBaseEmail.ToObject(ms);
            string messageInXml2 = cbePostSerialize.ToString();
            //Console.Write(messageInXml2);

            Assert.That(messageInXml == messageInXml2,
                            "The ser-deser-serialized version of the orig CBaseEmail should be the same as the serialized version");

            Attachment[] attachmentArray = cbePostSerialize.Attachments;
            //Console.WriteLine(attachmentArray);

            //Console.WriteLine("the number of attachments is:\t" + attachmentArray.Length);
            Assert.That(attachmentArray.Length == 3, "the attachment array length should be 3.");

            Attachment oneAttachment = attachmentArray[1];
            //Console.Write(oneAttachment);
            string filePostSerializeString64 = oneAttachment.Data;
            //Console.Write(filePostSerializeString64);
            byte[] filePostSerialize = Convert.FromBase64String(filePostSerializeString64);

            int i = 0;
            bool fileMismatched = false;
            while (i < filePostSerialize.Length)
            {
                if (fileAsByteArray[i] != filePostSerialize[i])
                {
                    fileMismatched = true;
                    break;
                }
                ++i;
            }
            Assert.That(!fileMismatched, "the byte array of the ser-deserialized version of the attachment should match the original");

        }

        [Test]
        public void SerializeAndDeserializeTest()
        {
            //Creates a CBaseEmail 
            //serializes it with an XmlSerializer, Deserializes it, and checks that all
            //fields of the object are the same as they started + the defaults.
            String subject = "Subject";
            String from = "from@from.com";
            String bcc = "bcc@bcc.com";
            String to = "to@to.com";
            String cc = "cc@cc.com";
            String body = "This is the body.";
            CBaseEmail cbe = new CBaseEmail(ConstAppDavid.SystemBrokerGuid)
            {
                Subject = subject,
                From = from,
                Bcc = bcc,
                To = to,
                CCRecipient = cc,
                Message = body
            };
            String MessageInXml = cbe.ToString();
            //System.Console.Write(MessageInXml);

            byte[] ba = Encoding.ASCII.GetBytes(MessageInXml);
            MemoryStream ms = new MemoryStream(ba);


            CBaseEmail cbePostSerialize = CBaseEmail.ToObject(ms);
            Assert.That(cbePostSerialize.IsHtmlEmailSpecified, Is.EqualTo(true));
            Assert.That(!cbePostSerialize.IsHtmlEmail);
            Assert.That(cbePostSerialize.Subject, Is.EqualTo(subject));
            Assert.That(cbePostSerialize.From, Is.EqualTo(from));
            Assert.That(cbePostSerialize.Message, Is.EqualTo(body));
            Assert.That(cbePostSerialize.DisclaimerType, Is.EqualTo(E_DisclaimerType.NORMAL));
            Assert.That(cbePostSerialize.Bcc, Is.EqualTo(bcc));
            Assert.That(cbePostSerialize.To, Is.EqualTo(to));
            Assert.That(cbePostSerialize.CCRecipient, Is.EqualTo(cc));
        }

        [Test]
        public void SimpleStringSplitTest()
        {
            //Only testing the functionality of the string.Split function.

            String toParse = "lol This Is A Bad Address, scottk @meridianlink .com";
            //foreach (String s in toParse.Split(',',';'))
            //{
            //    Console.Write(s.Trim()+'\n');
            //}
            string[] parsed = toParse.Split(',', ';');
            Assert.That(parsed.Length == 2, "should have to strings in parsed array");
            Assert.That(parsed[0], Is.EqualTo("lol This Is A Bad Address"));
            Assert.That(parsed[1], Is.EqualTo(" scottk @meridianlink .com"));
        }

        [Test]
        public void testGenerics()
        {
            // Only tested that generics are indeed type checked.
            List<int> lint = new List<int>();
            lint.Add(1);
            try
            {
                lint.Add(2);
                lint.Add(1);
                lint.Add(Convert.ToInt32("lol"));
                //foreach (var i in lint)
                //    Console.WriteLine(i);
            }
            catch (Exception e)
            {
                //Console.WriteLine("inside catch block.");
                Assert.That(e.GetType().ToString() == "System.FormatException");
            }
            finally
            {

                Assert.That(lint.ElementAt<int>(0) == 1);
                Assert.That(lint.ElementAt(1) == 2);
            }
        }

        [Test]
        public void tryCatch()
        {
            // A basis of comparison to see if tryCatchWithFunction will properly catch the error like this one.
            try
            {
                CBaseEmail cbe = new CBaseEmail(ConstAppDavid.SystemBrokerGuid);
                //Console.Write(cbe.Attachments[0]);
            }
            catch (Exception e)
            {
                //Console.Write("inside catch block");
                Assert.That(e.GetType().ToString() == "System.IndexOutOfRangeException");
            }
        }

        [Test]
        public void tryCatchWithFunction()
        {
            // Just testing to see that the causeError function doesn't cause an uncaught error at runtime.
            // essentially this shows that try catch blocks catch all errors from functions within them.
            try
            {
                causeError();
            }
            catch (Exception e)
            {
                //Console.Write("inside catch block");
                Assert.That(e.GetType().ToString() == "System.IndexOutOfRangeException");
            }
        }
        
        #endregion

        #region ( Helper Functions )
        public void causeError()
        {
            // Causes an error since it attempts to reference a null element.
            CBaseEmail cbe = new CBaseEmail(ConstAppDavid.SystemBrokerGuid);
            //Console.Write(cbe.Attachments[0]);
        }

        public CBaseEmail standardCBE()
        {
            // Creates a simple CBaseEmail w/o attachments
            String sSubject = "Subj";
            String sFrom = "from@from.com";
            String sBcc = "bcc@bcc.com";
            String sTo = "to@to.com";
            String sCc = "cc@cc.com";
            String sMessage = "email message";
            Boolean sIsHtmlEmail = true;
            CBaseEmail cbe = new CBaseEmail(ConstAppDavid.SystemBrokerGuid)
            {
                Subject = sSubject,
                From = sFrom,
                Bcc = sBcc,
                To = sTo,
                CCRecipient = sCc,
                Message = sMessage,
                IsHtmlEmail = sIsHtmlEmail
            };
            return cbe;
        }

        #endregion


    }
}