﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using LendersOffice.Common;

namespace LendOSolnTest.Common
{
    [TestFixture]
    public class IPAddressUtilitiesTest
    {
        [Test]
        public void IsMatchIndividualTest()
        {

            try
            {
                IPAddressUtilities.IsMatch("12.13.1.10", string.Empty);
                Assert.Fail("Should throw ArgumentException when actual IP address is empty");
            }
            catch (ArgumentException)
            {
            }

            try
            {
                IPAddressUtilities.IsMatch("12.13.1.10", null);
                Assert.Fail("Should throw ArgumentException when actual IP address is null");
            }
            catch (ArgumentException)
            {
            }
            var testCaseList = new [] {
                  new { IPAddressRange = (string)null, IPAddress = "12.13.1.*", Result = true}
                , new { IPAddressRange = ""          , IPAddress = "12.13.1.10", Result = true}
                , new { IPAddressRange = "*.*.*.*", IPAddress = "12.13.1.10", Result = true}

                , new { IPAddressRange = "12.13.1.10", IPAddress = "12.13.1.10", Result = true}
                , new { IPAddressRange = "12.13.1.10", IPAddress = "12.13.1.11", Result = false}
                , new { IPAddressRange = "12.13.1.10", IPAddress = "12.13.1.*", Result = false}

                , new { IPAddressRange = "12.13.1.*", IPAddress = "12.13.1.11", Result = true}
                , new { IPAddressRange = "12.13.1.*", IPAddress = "12.13.1.12", Result = true}

                // 4/18/2014 dd - Currently does not validate for valid IP format
                , new { IPAddressRange = "12.13.1.*", IPAddress = "12.13.1.foo", Result = true}

                , new { IPAddressRange = "12.13.1.*", IPAddress = "12.13.1.*", Result = true}

                , new { IPAddressRange = "12.13.1.*", IPAddress = "12.13.1", Result = false}

                , new { IPAddressRange = "12.13.1.*", IPAddress = "12.13.2.10", Result = false}

                , new { IPAddressRange = "12.13.*.*", IPAddress = "12.13.2.10", Result = true}
                , new { IPAddressRange = "12.13.*.*", IPAddress = "12.14.2.10", Result = false}
                               };

            foreach (var testCase in testCaseList)
            {
                Assert.AreEqual(testCase.Result, IPAddressUtilities.IsMatch(testCase.IPAddressRange, testCase.IPAddress), testCase.ToString());
            }
        }

        [Test]
        public void IsMatchListTest()
        {
            string[] ipAddressRangeList = { "10.11.12.13", "12.14.16.*", "14.16.*.*" };

            var testCaseList = new[] {
                new { IPAddress = "10.11.12.13", Result = true}
                , new { IPAddress = "10.11.12.14", Result = false}
                , new { IPAddress = "10.11.12.*", Result = false}

                , new { IPAddress = "12.14.16.13", Result = true}
                , new { IPAddress = "12.14.14.13", Result = false}

                , new { IPAddress = "14.16.12.13", Result = true}
            };

            foreach (var testCase in testCaseList)
            {
                Assert.AreEqual(testCase.Result, IPAddressUtilities.IsMatch(ipAddressRangeList, testCase.IPAddress), testCase.ToString());
            }
        }

        [Test]
        public void IsMatchSpecialIpListTest()
        {
            string[] ipAddressRangeList = { "10.11.12.13", "12.14.16.*", "14.16.*.*" };

            foreach (var ipAddr in IPAddressUtilities.SpecialInternalIpList)
            {
                // 4/18/2014 dd - Each special internal IP should match regardless of the input range list.
                Assert.AreEqual(true, IPAddressUtilities.IsMatch(ipAddressRangeList, ipAddr), ipAddr);
            }
        }

        [Test]
        public void MatchNullListTest()
        {
            string[] ipAddressRangeList = null;
            Assert.AreEqual(true, IPAddressUtilities.IsMatch(ipAddressRangeList, "12.13.14.15"));

            List<string> emptyList = new List<string>();
            Assert.AreEqual(true, IPAddressUtilities.IsMatch(emptyList, "12.13.14.15"));
        }
    }
}
