﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using LendersOffice.Common;

namespace LendOSolnTest.Common
{
    [TestFixture]
    public class CurrentRequestOrThreadCacheTest
    {
        private class TestCacheKey : GeneratedKeyForRequestOrThreadCache
        {
            public TestCacheKey(string Name) : base(Name) { }
            public static TestCacheKey ThreadCacheTest { get { return new TestCacheKey("lolname"); } }
            public static TestCacheKey NoneAdded { get { return new TestCacheKey("none_added"); } }
        }
        [Test]
        public void ThreadCachingTest() // since it's not ASP.NET, it should use the threading portion of the object
        {
            double x = 5;
            var key = TestCacheKey.ThreadCacheTest;
            double x2;

            // cache and un-cache
            CurrentRequestOrThreadCache.SetRequestOrThreadObjectByName(key , x);
            x2 = (double)CurrentRequestOrThreadCache.GetRequestOrThreadObjectByName(key);

            // should be the same after caching and uncaching.
            Assert.That(x == x2);
        }
        [Test]
        public void NUnitThreadTest() // shows that nunit tests are run on the same thread... but this should run before NUnitThreadTestPart2
        {
            CurrentRequestOrThreadCache.SetRequestOrThreadObjectByName(TestCacheKey.ThreadCacheTest, 10);
        }
        [Test]
        public void NUnitThreadTestPart2() // fails if run individually, succeeds if run by running all on the class.
        // shows that nunit tests are run on the same thread...
        {
            if (System.Web.HttpContext.Current == null)
            {
                Console.WriteLine("fails if run individually, succeeds if run by running all on the class.");
                var x = (int)CurrentRequestOrThreadCache.GetRequestOrThreadObjectByName(TestCacheKey.ThreadCacheTest);
                Assert.That(x == 10); // using same thread for all methods.
            }
        }
        [Test]
        public void NullObjectTest()
        {
            var obj = CurrentRequestOrThreadCache.GetRequestOrThreadObjectByName(TestCacheKey.NoneAdded);
            Assert.That(obj == null);
        }
    
    }

    [TestFixture]
    public class ExpiringCurrentRequestOrThreadCacheTest
    {
        private class TestCacheKey : GeneratedKeyForRequestOrThreadCache
        {
            public TestCacheKey(string Name) : base(Name) { }
            public static TestCacheKey ExpiringTest { get { return new TestCacheKey("exp_lol"); } }
        }
        [Test]
        public void ExpirationTests()
        {
            int x = 10;
            var key = TestCacheKey.ExpiringTest;
            
            // non expiring test.
            ExpiringCurrentRequestOrThreadCache.SetRequestOrThreadObjectByName(key, x, TimeSpan.FromHours(1));
            Assert.That(10 == (int)ExpiringCurrentRequestOrThreadCache.GetRequestOrThreadObjectByName(key));

            // expiring test.
            ExpiringCurrentRequestOrThreadCache.SetRequestOrThreadObjectByName(key, x, TimeSpan.FromHours(-1));
            Assert.That(null == ExpiringCurrentRequestOrThreadCache.GetRequestOrThreadObjectByName(key));
        }
    }
}
