﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit;
using NUnit.Framework;
using System.IO;
using DataAccess;

namespace LendOSolnTest.Common
{
    [TestFixture]
    public class FileDBToolsTest
    {
        [Test]
        public void TestAddingFileFromByteAndReadingFromByte()
        {
            Guid fileKey = Guid.NewGuid();

            byte[] binary = TestUtilities.ReadBinaryFile("Grid_Letter_Test.pdf");
            FileDBTools.WriteData(E_FileDB.EDMS, fileKey.ToString(), binary);

            try
            {
                byte[] binary2 = FileDBTools.ReadData(E_FileDB.EDMS, fileKey.ToString());
                Assert.AreEqual(binary, binary2);
            }
            finally
            {
                FileDBTools.Delete(E_FileDB.EDMS, fileKey.ToString());
            }
        }

        [Test]
        public void WriteUsingFileStream()
        {
            Guid fileKey = Guid.NewGuid();
            byte[] binary = TestUtilities.ReadBinaryFile("Grid_Letter_Test.pdf");

            Action<Stream> fileWriter = delegate(Stream outStream)
            {
                outStream.Write(binary, 0, binary.Length);
            };

            FileDBTools.WriteData(E_FileDB.EDMS, fileKey.ToString(), fileWriter);

            try
            {
                byte[] buffer = FileDBTools.ReadData(E_FileDB.EDMS, fileKey.ToString());
                Assert.AreEqual(binary, buffer);
            }

            finally
            {
                FileDBTools.Delete(E_FileDB.EDMS, fileKey.ToString());
            }
        }

        [Test]
        [ExpectedException(typeof(FileNotFoundException))]
        public void ReadFakeFile()
        {
            FileDBTools.ReadData(E_FileDB.EDMS, Guid.NewGuid().ToString());
        }

        [Test]
        public void TestReadingFromFileStream()
        {
            Guid fileKey = Guid.NewGuid();
            byte[] binary = TestUtilities.ReadBinaryFile("Grid_Letter_Test.pdf");
            FileDBTools.WriteData(E_FileDB.EDMS, fileKey.ToString(), binary);

            try
            {
                byte[] buffer = null;

                Action<Stream> fs = delegate(Stream fileStream)
                {
                    buffer = Tools.ReadFully(fileStream,32768 );
                };

                FileDBTools.ReadData(E_FileDB.EDMS, fileKey.ToString(), fs);
                Assert.AreEqual(binary, buffer);
                
            }

            finally
            {
                FileDBTools.Delete(E_FileDB.EDMS, fileKey.ToString());
            }
        }


        [Test]
        public void CreateBackUpFileTest()
        {
            Guid fileKey = Guid.NewGuid();
            byte[] binary = TestUtilities.ReadBinaryFile("Grid_Letter_Test.pdf");
            FileDBTools.WriteData(E_FileDB.EDMS, fileKey.ToString(), binary);
               string tempFile = null;
            try
            {
                tempFile = FileDBTools.CreateCopy(E_FileDB.EDMS, fileKey.ToString());
                Assert.That(string.IsNullOrEmpty(tempFile), Is.False);
                Assert.That(tempFile, Is.Not.EqualTo(fileKey.ToString()));
                byte[] buffer = File.ReadAllBytes(tempFile);
                Assert.AreEqual(binary, buffer);
            }

            finally
            {
                FileDBTools.Delete(E_FileDB.EDMS, fileKey.ToString());
                if (false == string.IsNullOrEmpty(tempFile))
                {
                    File.Delete(tempFile);
                }
            }
        }

        [Test]
        public void RestoreBackupFile()
        {
            Guid fileKey = Guid.NewGuid();
            byte[] binary = TestUtilities.ReadBinaryFile("Grid_Letter_Test.pdf");
            FileDBTools.WriteData(E_FileDB.EDMS, fileKey.ToString(), binary);
            string tempFile = null;
            try
            {
                tempFile = FileDBTools.CreateCopy(E_FileDB.EDMS, fileKey.ToString());
                Assert.That(string.IsNullOrEmpty(tempFile), Is.False);
                Assert.That(tempFile, Is.Not.EqualTo(fileKey.ToString()));
                byte[] buffer = File.ReadAllBytes(tempFile);
                Assert.AreEqual(binary, buffer);
                FileDBTools.WriteData(E_FileDB.EDMS, fileKey.ToString(), "Cleaning");
                buffer = FileDBTools.ReadData(E_FileDB.EDMS, fileKey.ToString());
                Assert.AreNotEqual(buffer, binary);

                FileDBTools.WriteFile(E_FileDB.EDMS, fileKey.ToString(), tempFile);
                buffer  = FileDBTools.ReadData(E_FileDB.EDMS, fileKey.ToString());
                Assert.AreEqual(binary, buffer);
            }

            finally
            {
                FileDBTools.Delete(E_FileDB.EDMS, fileKey.ToString());
                if (false == string.IsNullOrEmpty(tempFile))
                {
                    if (File.Exists(tempFile))
                    {
                        File.Delete(tempFile);
                    }
                }
            }
        }

        [Test]
        public void DeleteNonExistantFileFromFileDB()
        {
            FileDBTools.Delete(E_FileDB.EDMS, Guid.NewGuid().ToString());
        }

        [Test]
        public void ReadJSonData()
        {
            string key = Guid.NewGuid().ToString() + "unittest";

            try
            {
                int size = 10000;
                List<int> randomObject = new List<int>(size);
                Random r = new Random();

                for (int i = 0; i < size; i++)
                {
                    randomObject.Add(r.Next(int.MaxValue));
                }

                FileDBTools.WriteJsonNetObject(E_FileDB.Temp, key, randomObject);

                var rereadObject = FileDBTools.ReadJsonNetObject<List<int>>(E_FileDB.Temp, key);

                Assert.That(rereadObject, Is.EquivalentTo(randomObject));
            }
            finally
            {
                try
                {
                    FileDBTools.Delete(E_FileDB.Temp, key);
                }
                catch (FileNotFoundException) { }
            }

        }

        [Test]
        public void ReadStringData()
        {
            Guid fileKey = Guid.NewGuid();
            string data = "☼ÿçÑ¼}";
            FileDBTools.WriteData(E_FileDB.EDMS, fileKey.ToString(), data);

            string text = FileDBTools.ReadDataText(E_FileDB.EDMS, fileKey.ToString());
            Assert.That(data, Is.EqualTo(text));
        }

        [Test]
        public void TestCopy()
        {
            string srcFileKey = Guid.NewGuid().ToString();
            string destFileKey = Guid.NewGuid().ToString();

            try
            {
                string data = "HELLO WORLD!!!";

                FileDBTools.WriteData(E_FileDB.Temp, srcFileKey, data);

                FileDBTools.Copy(E_FileDB.Temp, srcFileKey, destFileKey);

                string destData = FileDBTools.ReadDataText(E_FileDB.Temp, destFileKey);

                Assert.That(data, Is.EqualTo(destData));
            }
            finally
            {
                FileDBTools.Delete(E_FileDB.Temp, srcFileKey);
                FileDBTools.Delete(E_FileDB.Temp, destFileKey);
            }
        }
    }
}