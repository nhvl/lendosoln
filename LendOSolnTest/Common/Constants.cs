/// Author: David Dao

using System;

namespace LendOSolnTest.Common
{
    public static class CategoryConstants
    {
        public const string ConnectionDependent = "Connection Dependent"; // if this value is changed then please change the value in LUnit as well.
        public const string NewBuildServerIgnore = "BuildServerTempIgnore";
        public const string RunBeforePullRequest = "Run Before Pull Request";

        /// <summary>
        /// Indicates that we don't expect the test to pass when running the suite on our local machines.
        /// </summary>
        public const string ExpectFailureOnLocalhost = "Expect Failure on Localhost";

        /// <summary>
        /// If a test method uses mocks registered with LqbApplication.Current, please mark it with this category.
        /// </summary>
        public const string LqbMocks = "LQB.Mocks"; // if this value is changed then please change the value in LUnit as well.

        /// <summary>
        /// If a test has no side-effects and is designed to test a small bit of code, please mark it with this category.
        /// </summary>
        public const string UnitTest = "UNIT";

        /// <summary>
        /// If a test is designed to test against an external component or system, please mark it with this category.
        /// </summary>
        public const string IntegrationTest = "INTEGRATION";
    }
}
