﻿// <copyright file="AssignTest.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: geoffreyf
//    Date:   9/11/2014 11:10:44 AM 
// </summary>
namespace LendOSolnTest.MortgagePools
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Audit;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.BatchOperationError;
    using LendersOffice.ObjLib.MortgagePool;
    using LendersOffice.Security;
    using LendOSolnTest.Common;
    using NUnit.Framework;

    /// <summary>
    /// Tests the operations that can occur to a loan at pool assignment.
    /// </summary>
    [TestFixture]
    public class AssignTest
    {
        private AbstractUserPrincipal principal;
        private MortgagePool pool;
        private Guid loanID;

        [TestFixtureSetUp]
        public void SetUp()
        {
            this.principal = LoginTools.LoginAs(TestAccountType.LoTest001);

            this.CreateMortgagePool();

            this.CreateLoan();
        }

        [TestFixtureTearDown]
        public void TearDown()
        {
            bool sendNotifications = false;
            bool generateLinkLoanMsgEvent = false;

            Tools.DeclareLoanFileInvalid(
                this.principal,
                this.loanID,
                sendNotifications,
                generateLinkLoanMsgEvent);

            MortgagePool.DeletePool(this.principal.BrokerId, this.pool.PoolId);
        }

        private void CreateMortgagePool()
        {
            var poolID = MortgagePool.CreatePool(Guid.NewGuid().ToString());
            this.pool = new MortgagePool(poolID);
        }

        private void CreateLoan()
        {
            var fileCreator = CLoanFileCreator.GetCreator(
                this.principal,
                E_LoanCreationSource.UserCreateFromBlank);

            this.loanID = fileCreator.CreateBlankLoanFile();
        }

        [Test]
        public void SetBackEndBasePriceWithoutRateInPriceTable()
        {
            this.pool.BasePricingCalcType = E_BasePricingCalcType.Table;
            this.pool.SaveData();

            var settings = new LoanAssignmentSettings()
            {
                IsSetBackEndBasePrice = true
            };

            BatchOperationError<LoanError> errors;

            this.pool.AssignLoans(
                new Guid[] { this.loanID },
                settings,
                out errors);

            Assert.That(
                errors.Errors.Count() == 1,
                "There should be an error because there are no entries in the table.");

            var convert = new LosConvert(); 

            var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                this.loanID,
                typeof(AssignTest));
            loan.InitLoad();

            Assert.That(
                convert.ToRate(loan.sInvestorLockBaseBrokComp1PcPrice) != this.pool.BasePrice,
                "The loan file back-end base price should not match the pool base price.");

            this.pool.RemoveLoans(this.loanID);
        }

        [Test]
        public void SetBackEndBasePrice()
        {
            this.pool.BasePricingCalcType = E_BasePricingCalcType.PoolLevel;
            this.pool.BasePricingPoolLevelLckd = true;
            this.pool.BasePricingPoolLevelManual = 97.978m;
            this.pool.SaveData();

            var settings = new LoanAssignmentSettings()
            {
                IsSetBackEndBasePrice = true
            };

            BatchOperationError<LoanError> errors;

            this.pool.AssignLoans(
                new Guid[] { this.loanID },
                settings,
                out errors);

            Assert.That(
                !errors.Errors.Any(),
                "There should be not be any errors.");

            var convert = new LosConvert(); 

            var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                this.loanID,
                typeof(AssignTest));
            loan.InitLoad();

            Assert.That(
                convert.ToRate(loan.sInvestorLockBaseBrokComp1PcPrice) == this.pool.BasePrice,
                "The loan file back-end base price should match the pool base price.");

            this.pool.RemoveLoans(this.loanID);
        }

        [Test]
        public void TestSetDeliveryFee()
        {
            var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                this.loanID,
                typeof(AssignTest));
            loan.InitLoad();

            loan.sInvestorLockAdjustments = new List<PricingAdjustment>();

            var settings = new LoanAssignmentSettings()
            {
                IsSetDeliveryFee = true,
                DeliveryFee = 5
            };

            BatchOperationError<LoanError> errors;

            this.pool.AssignLoans(
                new Guid[] { this.loanID },
                settings,
                out errors);

            Assert.That(
                !errors.Errors.Any(),
                "There should be not be any errors.");

            loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                this.loanID,
                typeof(AssignTest));
            loan.InitLoad();

            var adjustmentsAfterAssigning = loan.sInvestorLockAdjustments;

            Assert.That(
                adjustmentsAfterAssigning.Count == 1,
                "The loan should have a single adjustment added from pool assignment");

            var adjustment = adjustmentsAfterAssigning.First();

            var convert = new LosConvert();

            Assert.That(
                convert.ToRate(adjustment.Fee) == settings.DeliveryFee,
                "The fee should equal what was set in the assignment settings.");

            Assert.That(
                adjustment.Description == MortgagePool.PoolDeliveryAdjustmentDescription,
                "The adjustment description should match what was dictated in the spec.");

            loan.InitSave(ConstAppDavid.SkipVersionCheck);
            loan.sInvestorLockAdjustments = new List<PricingAdjustment>();
            loan.Save();

            this.pool.RemoveLoans(this.loanID);
        }

        [Test]
        public void TestSetDeliveryFeeWithExisting()
        {
            var adjustment = new PricingAdjustment();
            adjustment.Fee = "99.999";
            adjustment.Description = MortgagePool.PoolDeliveryAdjustmentDescription;

            var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                this.loanID,
                typeof(AssignTest));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            loan.sInvestorLockAdjustments = new List<PricingAdjustment>() 
            { 
                adjustment 
            };
            loan.Save(); 

            var settings = new LoanAssignmentSettings()
            {
                IsSetDeliveryFee = true,
                DeliveryFee = 5
            };

            BatchOperationError<LoanError> errors;

            this.pool.AssignLoans(
                new Guid[] { this.loanID },
                settings,
                out errors);

            Assert.That(
                !errors.Errors.Any(),
                "There should be not be any errors.");

            loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                this.loanID,
                typeof(AssignTest));
            loan.InitLoad();

            var adjustmentsAfterAssigning = loan.sInvestorLockAdjustments;

            Assert.That(
                adjustmentsAfterAssigning.Count == 1,
                "The loan should have a single adjustment added from pool assignment");

            adjustment = adjustmentsAfterAssigning.First();

            var convert = new LosConvert();

            Assert.That(
                convert.ToRate(adjustment.Fee) == settings.DeliveryFee,
                "The fee should equal what was set in the assignment settings.");

            Assert.That(
                adjustment.Description == MortgagePool.PoolDeliveryAdjustmentDescription,
                "The adjustment description should match what was dictated in the spec.");

            loan.InitSave(ConstAppDavid.SkipVersionCheck);
            loan.sInvestorLockAdjustments = new List<PricingAdjustment>();
            loan.Save();

            this.pool.RemoveLoans(this.loanID);

        }

        [Test]
        public void TestSetBackEndLock()
        {
            this.pool.BasePricingCalcType = E_BasePricingCalcType.PoolLevel;
            this.pool.BasePricingPoolLevelLckd = true;
            this.pool.BasePricingPoolLevelManual = 95.555m;
            this.pool.CommitmentExpires = CDateTime.Create(DateTime.Today.AddDays(7));
            this.pool.SaveData();

            var settings = new LoanAssignmentSettings()
            {
                IsSetBackEndRateLock = true
            };

            BatchOperationError<LoanError> errors;

            this.pool.AssignLoans(
                new Guid[] { this.loanID },
                settings,
                out errors);

            Assert.That(
                !errors.Errors.Any(),
                "There should be not be any errors.");

            var convert = new LosConvert(); 

            var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                this.loanID,
                typeof(AssignTest));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            Assert.That(
                loan.sInvestorLockRateLockStatusT == E_sInvestorLockRateLockStatusT.Locked,
                "The back-end rate lock status should be locked.");

            Assert.That(
                loan.sInvestorLockRLckExpiredD.SameAs(pool.CommitmentExpires),
                "The back-end rate lock expiration date should be the same as the commitment expires date.");

            var expectedLockPeriod = this.pool.CommitmentExpires
                .DateTimeForComputation
                .Subtract(DateTime.Today)
                .Days;

            Assert.That(
                loan.sInvestorLockRLckdDays == expectedLockPeriod,
                "The investor lock period should be the commitment expires date minus today.");

            loan.BreakInvestorRateLock("Test user", "Test reason");
            loan.Save();

            this.pool.RemoveLoans(this.loanID);
        }

        [Test]
        public void TestAllOptions()
        {
            this.pool.BasePricingCalcType = E_BasePricingCalcType.PoolLevel;
            this.pool.BasePricingPoolLevelLckd = true;
            this.pool.BasePricingPoolLevelManual = 44.444m;
            this.pool.CommitmentExpires = CDateTime.Create(DateTime.Today.AddDays(10));
            this.pool.SaveData();

            var settings = new LoanAssignmentSettings()
            {
                IsSetBackEndBasePrice = true,
                IsSetDeliveryFee = true,
                DeliveryFee = 15,
                IsSetBackEndRateLock = true
            };

            BatchOperationError<LoanError> errors;

            this.pool.AssignLoans(
                new Guid[] { this.loanID },
                settings,
                out errors);

            Assert.That(
                !errors.Errors.Any(),
                "There should be not be any errors.");

            var convert = new LosConvert(); 

            var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                this.loanID,
                typeof(AssignTest));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            Assert.That(
                convert.ToRate(loan.sInvestorLockBaseBrokComp1PcPrice) == this.pool.BasePrice,
                "The loan file back-end base price should match the pool base price.");

            var adjustment = loan.sInvestorLockAdjustments
                .Where(a => a.Description == MortgagePool.PoolDeliveryAdjustmentDescription)
                .FirstOrDefault();

            Assert.That(
                adjustment != null,
                "The pool assignment adjusmtment should be there.");

            Assert.That(
                convert.ToRate(adjustment.Fee) == settings.DeliveryFee,
                "The fee should equal what was set in the assignment settings.");

            Assert.That(
                loan.sInvestorLockRateLockStatusT == E_sInvestorLockRateLockStatusT.Locked,
                "The back-end rate lock status should be locked.");

            Assert.That(
                loan.sInvestorLockRLckExpiredD.SameAs(pool.CommitmentExpires),
                "The back-end rate lock expiration date should be the same as the commitment expires date.");

            var expectedLockPeriod = this.pool.CommitmentExpires
                .DateTimeForComputation
                .Subtract(DateTime.Today)
                .Days;

            Assert.That(
                loan.sInvestorLockRLckdDays == expectedLockPeriod,
                "The investor lock period should be the commitment expires date minus today.");

            loan.BreakInvestorRateLock("Test user", "Test reason");
            loan.Save();

            this.pool.RemoveLoans(this.loanID);
        }
    }
}
