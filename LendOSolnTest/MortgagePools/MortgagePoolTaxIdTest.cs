﻿namespace LendOSolnTest.MortgagePools
{
    using System;
    using LendersOffice.Security;
    using LendersOffice.ObjLib.MortgagePool;
    using NUnit.Framework;
    using Common;

    [TestFixture]
    public class MortgagePoolTaxIdTest
    {
        private const string TaxId1 = "123456789";
        private const string TaxId2 = "987654321";

        private AbstractUserPrincipal principal;
        private long? testPoolId;

        [TestFixtureSetUp]
        public void Setup()
        {
            this.principal = LoginTools.LoginAs(TestAccountType.LoTest001);
            this.testPoolId = MortgagePool.CreatePool(Guid.NewGuid().ToString());
        }

        [TestFixtureTearDown]
        public void TearDown()
        {
            if (this.testPoolId.HasValue)
            {
                MortgagePool.DeletePool(this.principal.BrokerId, this.testPoolId.Value);
            }
        }

        [Test]
        public void CreateNewPool_CreatesPool()
        {
            Assert.IsNotNull(this.testPoolId);
        }

        [Test]
        public void LoadPool_LoadsPool()
        {
            Assert.IsNotNull(this.testPoolId);
            var pool = new MortgagePool(this.testPoolId.Value, this.principal.BrokerId);
            Assert.IsNotNull(pool);
        }

        [Test]
        public void UpdatePool_TaxId_UpdatesTaxId()
        {
            Assert.IsNotNull(this.testPoolId);
            var pool = new MortgagePool(this.testPoolId.Value, this.principal.BrokerId);

            pool.TaxID = TaxId1;

            Assert.IsTrue(pool.SaveData());

            pool = new MortgagePool(this.testPoolId.Value, this.principal.BrokerId);
            Assert.AreEqual(TaxId1, pool.TaxID);

            pool.TaxID = TaxId2;

            Assert.IsTrue(pool.SaveData());

            pool = new MortgagePool(this.testPoolId.Value, this.principal.BrokerId);
            Assert.AreEqual(TaxId2, pool.TaxID);
        }
    }
}
