﻿using System;
using LendOSolnTest.Common;
using NUnit.Framework;
using RatesheetMap;
using System.IO;
using System.Collections;
using DataAccess;

namespace LendOSolnTest.RatesheetMapProcessorTests
{
    [TestFixture]
    public class ProcessingSuccessValuesTest : RatesheetMapProcessorTestBase
    {
        RatesheetMapProcessor m_processor = null;
        string m_resultInfo = "";

        public ProcessingSuccessValuesTest()
		{
            if (m_isExcelInstalled == false)
                return;
            
            string ratesheetLocation = RatesheetMapTests.RatesheetMapTestFileLocation + "CHASE_PML0022.XLS";
			m_processor = new RatesheetMapProcessor(true);
			m_resultInfo = m_processor.Process(ratesheetLocation);
            m_createdFiles = m_processor.RateOptionOutputFilenames;
		}

        [Test]
        public void TestIfProcessedUnSuccessfully()
        {
            if (m_processor == null)
                return;
            
            // db - I have intentionally created an error in this map
            Assert.AreEqual(false, m_processor.IsCompletelySuccessful);
        }

        [Test]
        public void TestIndividualOutputFileSuccessValues()
        {
            if (m_processor == null)
                return;
            
            ArrayList outputFilenames = m_processor.RateOptionOutputFilenames;
            string fileNameNoPath = "";
            bool outputSuccess = false;

            foreach (string rofilename in m_createdFiles)
            {
                fileNameNoPath = System.IO.Path.GetFileName(rofilename);
                outputSuccess = !m_processor.DoesOutputFileHaveError(rofilename);
                if (fileNameNoPath.Equals("CHASE_PML0022_CHASE_CONF_RATE_MAP_m-conventional fixed_Output.csv"))
                    Assert.AreEqual(false, outputSuccess, string.Format("Test TestIndividualOutputFileSuccessValues fails because the output file {0} should fail but did not.", fileNameNoPath));
                else
                    Assert.AreEqual(true, outputSuccess, string.Format("Test TestIndividualOutputFileSuccessValues fails because the output file {0} should pass but did not.", fileNameNoPath));
            }
        }

        [Test]
        public void TestIfOutputFilesAreEqual()
        {
            if (m_processor == null)
                return;
            
            bool areEqual = false;
            ArrayList outputFilenames = m_processor.RateOptionOutputFilenames;
            string fileNameNoPath = "";
            
            foreach (string rofilename in m_createdFiles)
            {
                fileNameNoPath = System.IO.Path.GetFileName(rofilename);
                areEqual = IsFileIdentical(rofilename, RatesheetMapTestFileLocation + fileNameNoPath);
                Assert.AreEqual(true, areEqual, string.Format("Test TestIfOutputFilesAreEqual fails because the output file {0} does not match the expected output.", fileNameNoPath));
            }
        }

        [TearDown]
        public void TearDown()
        {
            DeleteGeneratedTestOutput();
        }
    }
}
