﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using RatesheetMap.Extensions; 

namespace LendOSolnTest.RatesheetMapProcessorTests
{
    [TestFixture] 
    public class ExtensionTest
    {
        [Test]
        public void ConvertPercentTest()
        {
            Assert.That("".ConvertPercent(), Is.EqualTo(""));
            Assert.That("%".ConvertPercent(), Is.EqualTo("%"));
            Assert.That("1%".ConvertPercent(), Is.EqualTo("0.01"));
            Assert.That("Asdasd".ConvertPercent(), Is.EqualTo("Asdasd"));
            Assert.That("   adsasd   ".ConvertPercent(), Is.EqualTo("   adsasd   "));
            Assert.That("1%%".ConvertPercent(), Is.EqualTo("1%%"));
            Assert.That(" 1%% ".ConvertPercent(), Is.EqualTo(" 1%% "));
            Assert.That("20%".ConvertPercent(), Is.EqualTo("0.2"));
            Assert.That("20.53".ConvertPercent(), Is.EqualTo("20.53"));
            Assert.That("20.53%".ConvertPercent(), Is.EqualTo("0.2053"));   
            Assert.That("20.523123123123%".ConvertPercent(), Is.EqualTo("0.20523123123123"));
            Assert.That(" 20.53 %".ConvertPercent(), Is.EqualTo("0.2053"));
            Assert.That(" 101% ".ConvertPercent(), Is.EqualTo("1.01"));
            Assert.That("100.4%".ConvertPercent(), Is.EqualTo("1.004"));
            Assert.That("asdasd%".ConvertPercent(), Is.EqualTo("asdasd%"));
            Assert.That(" sadasd% ".ConvertPercent(), Is.EqualTo(" sadasd% ")); 
        }
    }
}
