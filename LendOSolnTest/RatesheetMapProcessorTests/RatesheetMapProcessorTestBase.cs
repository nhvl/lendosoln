﻿using System;
using LendOSolnTest.Common;
using NUnit.Framework;
using RatesheetMap;
using System.IO;
using System.Collections;

namespace LendOSolnTest.RatesheetMapProcessorTests
{
    public class RatesheetMapProcessorTestBase
    {
        #region Variables
        protected ArrayList m_createdFiles = null;
        protected bool m_isExcelInstalled = true;
        #endregion

        public RatesheetMapProcessorTestBase()
        {
            m_isExcelInstalled = IsExcelInstalled();
            m_createdFiles = new ArrayList();
        }

        private bool IsExcelInstalled()
        {
            return false;
            //try
            //{
            //    ExcelApp_RS excel = new ExcelApp_RS();
            //}
            //catch(Exception e)
            //{
            //    DataAccess.Tools.LogWarning("Unable to create excel object for RatesheetMapProcessor NUnit tests - this likely indicates that Excel is not installed.  Therefore, these tests will not be executed.  Error details: " + e.ToString());
            //    return false;
            //}

            //return true;
        }
        
        static protected string RatesheetMapTestFileLocation
        {
            get
            {
                return LendersOffice.Constants.ConstSite.TestRsMapDataFolder;
            }
        }

        public void DeleteGeneratedTestOutput()
        {
            // db - remove this deletion for now so I can have access to the output files that were generated
            // and see why they do not match
            /*try
            {
                foreach (string generatedFile in m_createdFiles)
                {
                    if (File.Exists(generatedFile))
                        File.Delete(generatedFile);
                }
            }
            catch { }*/
        }

        /// <summary>
        /// Do a binary comparison between 2 files
        /// </summary>
        /// <returns>True if the files are identical</returns>
        protected static bool IsFileIdentical(string sFile1, string sFile2)
        {
            if (!File.Exists(sFile1) || !File.Exists(sFile2)) return false;

            const int BUFSIZE = 1024;
            byte[] buf1 = new byte[BUFSIZE];
            byte[] buf2 = new byte[BUFSIZE];

            using (FileStream fs1 = new FileStream(sFile1, FileMode.Open))
            {
                using (FileStream fs2 = new FileStream(sFile2, FileMode.Open))
                {
                    while (true)
                    {
                        int n1 = fs1.Read(buf1, 0, BUFSIZE);
                        int n2 = fs2.Read(buf2, 0, BUFSIZE);

                        if (n1 != n2) return false;
                        if (n1 == 0) return true;

                        for (int i = 0; i < n1; i++)
                            if (buf1[i] != buf2[i])
                                return false;
                    }
                }
            }
        }

    }
}
