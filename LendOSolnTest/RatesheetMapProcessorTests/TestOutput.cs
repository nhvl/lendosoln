using System;
using LendOSolnTest.Common;
using NUnit.Framework;
using RatesheetMap;
using System.IO;
using System.Collections;

namespace LendOSolnTest.RatesheetMapProcessorTests
{
	[TestFixture]
    public class RatesheetMapTests : RatesheetMapProcessorTestBase
	{
		RatesheetMapProcessor m_processor = null;
		string m_resultInfo = "";

		public RatesheetMapTests()
		{
            if (m_isExcelInstalled == false)
                return;

            string ratesheetLocation = RatesheetMapTestFileLocation + "CitiMortgageRs_POINTER.xls";
			m_processor = new RatesheetMapProcessor(true );
			m_resultInfo = m_processor.Process(ratesheetLocation);
            m_createdFiles = m_processor.RateOptionOutputFilenames;
		}

        [TearDown]
        public void TearDown()
        {
            DeleteGeneratedTestOutput();
        }

		[Test]
		public void TestIfMapExists() 
		{
            if (m_processor == null)
                return;

            Assert.AreEqual(true, m_processor.MapExists);
		}

		[Test]
		public void TestIfProcessedSuccessfully() 
		{
            if (m_processor == null)
                return;

            Assert.AreEqual(true, m_processor.IsCompletelySuccessful);
		}

		[Test]
		public void TestIf__CitiMortgageRs_POINTER_CITI_CONF_POINTER_MAP__IsEqual()
		{
            if (m_processor == null)
                return;
            
            bool areEqual = false;
			bool tested = false;
			ArrayList outputFilenames = m_processor.RateOptionOutputFilenames;
			string fileNameNoPath = "CitiMortgageRs_POINTER_CITI_CONF_POINTER_MAP_m-sheet1_Output.csv";
			
			foreach(string rofilename in m_createdFiles)
			{
				if(rofilename.IndexOf(fileNameNoPath) >= 0)
				{
					tested = true;
                    areEqual = IsFileIdentical(rofilename, RatesheetMapTestFileLocation + fileNameNoPath);
					Assert.AreEqual(true, areEqual);
				}
			}

			if(tested == false)
			{
				throw new Exception("Test TestIf__CitiMortgageRs_POINTER_CITI_CONF_POINTER_MAP__IsEqual fails because no filenames matched to test.");
			}
		}
	}
}