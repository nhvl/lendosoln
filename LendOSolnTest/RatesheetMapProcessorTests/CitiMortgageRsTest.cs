﻿using System;
using LendOSolnTest.Common;
using NUnit.Framework;
using RatesheetMap;
using System.IO;
using System.Collections;

namespace LendOSolnTest.RatesheetMapProcessorTests
{
    [TestFixture]
    public class CitiMortgageRsTest : RatesheetMapProcessorTestBase
    {
        RatesheetMapProcessor m_processor = null;
        string m_resultInfo = "";

        public CitiMortgageRsTest()
        {
            if (m_isExcelInstalled == false)
                return;
            
            string ratesheetLocation = RatesheetMapTestFileLocation + "CitiMortgageRs.xls";
            m_processor = new RatesheetMapProcessor(true);
            m_resultInfo = m_processor.Process(ratesheetLocation);
            m_createdFiles = m_processor.RateOptionOutputFilenames;
        }

        [TearDown]
        public void TearDown()
        {
            DeleteGeneratedTestOutput();
        }

        [Test]
        public void TestIfMapExists()
        {
            if (m_processor == null)
                return;
            
            Assert.AreEqual(true, m_processor.MapExists);
        }

        [Test]
        public void TestIfProcessedSuccessfully()
        {
            if (m_processor == null)
                return;
            
            Assert.AreEqual(true, m_processor.IsCompletelySuccessful);
        }

        [Test]
        public void TestIf_CitiMortgageRs_IsEqual()
        {
            if (m_processor == null)
                return;
            
            bool areEqual = false;
            ArrayList outputFilenames = m_processor.RateOptionOutputFilenames;
            string fileNameNoPath = "";

            foreach (string rofilename in m_createdFiles)
            {
                fileNameNoPath = System.IO.Path.GetFileName(rofilename);
                areEqual = IsFileIdentical(rofilename, RatesheetMapTestFileLocation + fileNameNoPath);
                Assert.AreEqual(true, areEqual);
            }
        }
    }
}