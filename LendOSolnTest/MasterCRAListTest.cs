/// Author: David Dao

using System;
using System.Collections;

using NUnit.Framework;

using LendersOffice.CreditReport;
using LendersOffice.Security;
using LendOSolnTest.Common;
namespace LendOSolnTest
{
    [TestFixture]
	public class MasterCRAListTest
	{

        [Test]
        public void ListAvailableCras() 
        {
            // 10/29/2008 dd - Test to make sure RetrieveAvailableCras DOES NOT return CRA with Is Internal bit turn on.

            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.LoTest001);
            var list = MasterCRAList.RetrieveAvailableCras(principal.BrokerId);
            foreach (CRA cra in list) 
            {
                // cra.IsInternalOnly can only be FALSE.
                Assert.IsFalse(cra.IsInternalOnly, cra.VendorName + " has IsInternalOnly=true");
            }
        }

	}
}
