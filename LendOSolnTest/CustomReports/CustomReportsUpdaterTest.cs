﻿using System;
using System.Collections.Generic;
using System.Data;
using NUnit.Framework;

using System.Data.SqlClient;
using System.Linq;
using System.Data.Common;
using LendersOffice.Email;
using LendersOffice.QueryProcessor;

namespace LendOSolnTest.CustomReportsUpdaterTest
{
    [TestFixture]
    public class CustomReportsUpdater
    {
        /// <summary>
        /// SQLException is sealed and has no public constructors, so we'll use DbException (its parent) instead
        /// and throw this whenever we need to
        /// </summary>
        private class FakeSQLException : DbException
        {

        }


        /// <summary>
        /// Holds all of the configuration for a given run of the tester.
        /// </summary>
        private class NextRunConfig
        {
            /// <summary>
            /// The last run before this current run
            /// </summary>
            public DateTime LastRun;
            public DateTime CurrentRun;
            public DateTime EndDate;
            public DateTime EffectiveDate;
            public DateTime RunTime;
            public DateTime RetryUntil;
            public int ReportId;

            /// <summary>
            /// This is really an interval, but it's called frequency internally :(
            /// For weekly, it happens once every frequency weeks
            /// For daily, it happens once every frequency days
            /// For months it doesn't exist...
            ///   and DayOfTheMonthOrWeekNo is used instead to the same effect :|
            /// </summary>
            public int? Frequency;
            public bool? IncludeSatSun;
            public DayOfWeek? Weekday;
            public string Month;
            public int? DayOfTheMonthOrWeekNo;
            public string Description;


            /// <summary>
            /// This is used by the verifier to make sure that the reported next run time is as expected
            /// </summary>
            public DateTime ExpectedNextRun;


            public NextRunConfig() : this(new DateTime(), new DateTime()){}

            public NextRunConfig(DateTime curr, DateTime exp)
            {
                LastRun = new DateTime(1900, 1, 1);
                EndDate = new DateTime(9999, 1, 1);
                EffectiveDate = new DateTime(1900, 1, 1);
                CurrentRun = curr;
                ExpectedNextRun = exp;
                RunTime = new DateTime(1900, 1, 1, 23, 0, 0);//Default RunTime hour: 23:00
                RetryUntil = new DateTime(1900, 1, 1, 1, 0, 0);//Default RetryUntil hour: 1:00
                Weekday = null;
                Month = null;
                Description = null;
                DayOfTheMonthOrWeekNo = null;
            }

            public object[] ToRow()
            {
                List<object> row = new List<object>();
                //The order of all these adds matters unfortunately
                row.Add(ReportId);
                row.Add(EffectiveDate);
                row.Add(LastRun);
                row.Add(CurrentRun);
                row.Add(EndDate);
                row.Add(RunTime);
                row.Add(RetryUntil);
                //These next ones may or may not exist, and doing them in this order ensures that the 
                //row will be compatible with any kind of column set
                if (Frequency != null) row.Add(Frequency);
                if (IncludeSatSun != null) row.Add(IncludeSatSun);
                if (Weekday != null)
                {
                    row.Add(Enum.GetName(typeof(DayOfWeek), Weekday));
                }
                else if (!string.IsNullOrEmpty(Month))
                {
                    //Need to add a blank entry for weekday if it doesn't exist and month does
                    row.Add("");
                }
                if(!string.IsNullOrEmpty(Month)) row.Add(Month);
                if(DayOfTheMonthOrWeekNo != null) row.Add(DayOfTheMonthOrWeekNo);

                return row.ToArray();
            }
        }

        private static readonly List<string> Months = new List<string>(new string[] { "Blank", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" });
        private string[] DailyColumns = { "ReportId", "EffectiveDate", "LastRun", "NextRun", "EndDate", "RunTime", "RetryUntil", "Frequency", "IncludeSatSun" };
        private string[] WeeklyColumns = { "ReportId", "EffectiveDate", "LastRun", "NextRun", "EndDate", "RunTime", "RetryUntil", "Frequency", "Weekday" };
        //Both monthly kinds share the same column structure, weekday is not null in monthly1 and null in monthly2
        private string[] MonthlyColumns = { "ReportId", "EffectiveDate", "LastRun", "NextRun", "EndDate", "RunTime", "RetryUntil", "Weekday", "Month", "DateOfTheMonthOrWeekNo"};

        //The report run stuff all uses the same columns, so we'll make just one ds for it
        private DataSet ReportRunData = new DataSet();

        private string[] ReportColumns = { "BrokerId", "QueryName", "UserId", "ReportId", "Email", "CcIds", "ScheduleName", "QueryId", "ReportType", "IsActive" };
        private Type[] ReportColType = { typeof(Guid), typeof(String), typeof(Guid), typeof(Int32), typeof(String), typeof(String), typeof(String), typeof(Guid), typeof(String), typeof(Boolean) };

        //This guy maps ReportId => run config
        private Dictionary<int, NextRunConfig> NextRunReference = new Dictionary<int, NextRunConfig>();

        //This one keeps track of which reports have successfully had their next runs calculated
        private List<int> SuccessfulNextRunUpdates = new List<int>();

        //And this one keeps track of which reports have actually been run
        private List<Guid> SuccessfulReportRuns = new List<Guid>();

        //And here we keep track of which reports have failed
        private List<int> FailedReports = new List<int>();

        private void AddReportUserIdToSuccessList(Guid userId)
        {
            SuccessfulReportRuns.Add(userId);
        }

        private ScheduleCustomReport.CsvResult FailReportBasedOnUserId(Guid userId, IEnumerable<Guid> failList)
        {
            Assert.IsFalse(failList.Contains(userId));
            return new ScheduleCustomReport.CsvResult("Ran!", 1);
        }

        private ScheduleCustomReport.CsvResult ThrowReportBasedOnUserId(Guid userId, IEnumerable<Guid> failList, Exception toThrow)
        {
            if (failList.Contains(userId))
            {
                throw toThrow;
            }

            return new ScheduleCustomReport.CsvResult("Ran!", 1);
        }

        private ScheduleCustomReport.CsvResult RunReport(Guid brokerId, Guid queryId, Guid userId)
        {
            return new ScheduleCustomReport.CsvResult("Ran!", 1);
        }

        private int currentReportId = 0;
        private DataRow MakeNewReport(NextRunConfig config)
        {
           return MakeNewReport(config, GetNextReportId()); 
        }

        private DataRow MakeNewReport(NextRunConfig config, int reportId)
        {
            config.ReportId = reportId;

            NextRunReference.Add(reportId, config);
            return ReportRunData.Tables[0].Rows.Add(Guid.Empty /*BrokerId */, "Query: " + reportId, new Guid(reportId.ToString().PadLeft(32, 'F')), reportId, reportId + "@example.com", "ccs@example.com", "Schedule: " + reportId, new Guid(), "Testing", 1);
        }

        private int GetNextReportId()
        {
            return currentReportId++;
        }

        private void AddReportItem(NextRunConfig config)
        {
            if (!NextRunReference.ContainsKey(config.ReportId))
            {
                MakeNewReport(config, config.ReportId);
                return;
            }
            
            NextRunConfig compareTo = NextRunReference[config.ReportId];
            Assert.AreEqual(compareTo.ExpectedNextRun, config.ExpectedNextRun, "Error setting up, tried to add a config with the same id without the same expected next run");
        }

        private DataTable GetWeeklyTable()
        {
            DataTable Weekly = new DataTable();
            foreach (string col in WeeklyColumns)
            {
                Weekly.Columns.Add(col);
            }
            return Weekly;
        }

        private DataTable GetDailyTable()
        {
            DataTable Daily = new DataTable();
            foreach (string col in DailyColumns)
            {
                Daily.Columns.Add(col);
            }
            return Daily;
        }

        private DataTable GetMonthlyTable()
        {
            DataTable Monthly = new DataTable();
            foreach (string col in MonthlyColumns)
            {
                Monthly.Columns.Add(col);
            }
            return Monthly;
        }

        private void MergeDataSetBasedOnSproc(DataSet mergeIn, string sproc, DataSet ReportRun, DataTable NextRun)
        {
            //All of the sprocs that get report run info have "Detail" in them
            if (sproc.Contains("Detail"))
            {
                mergeIn.Merge(ReportRun);
            }
            else
            {
                //Anything else is going to be a next run sproc
                mergeIn.Merge(NextRun);
            }
        }

        private void VerifyExpectedDate(IEnumerable<SqlParameter> actual)
        {
            int id = -1;
            DateTime CalcNextRun = new DateTime();

            //It's easier to just foreach over this array, it's only ever got three entries anyway.
            foreach (SqlParameter p in actual)
            {
                if(p.ParameterName.Equals("@ReportId", StringComparison.InvariantCultureIgnoreCase))
                {
                    id = int.Parse(p.Value.ToString());
                }

                else if(p.ParameterName.Equals("@NextRun", StringComparison.InvariantCultureIgnoreCase))
                {
                    CalcNextRun = DateTime.Parse(p.Value.ToString());
                }
            }
            NextRunConfig n = NextRunReference[id];

            //Run configs that don't have an expected next date should not get here
            Assert.IsNotNull(n.ExpectedNextRun, "VerifyExpectedDate was called on something that should have failed!");

            //And of course the expected next run must be the calculated next run
            Assert.AreEqual(n.ExpectedNextRun.Date, CalcNextRun.Date, n.Description + " did not have the expected value");
            //Otherwise it was successful, so add it to the list
            SuccessfulNextRunUpdates.Add(id);
        }

        private void ThrowOnTriggerLastRunDate(IEnumerable<SqlParameter> parms, DateTime trigger, Exception toThrow)
        {
            foreach(SqlParameter p in parms)
            {
                if (p.ParameterName.Equals("@LastRun", StringComparison.InvariantCultureIgnoreCase))
                {
                    if (DateTime.Parse(p.Value.ToString()).Equals(trigger))
                        throw toThrow;
                }
            }
        }

        private void ThrowOnTriggerReportId(IEnumerable<SqlParameter> parms, IEnumerable<int> FailIds, Exception toThrow)
        {
            foreach (SqlParameter p in parms)
            {
                if (p.ParameterName.Equals("@ReportId", StringComparison.InvariantCultureIgnoreCase))
                {
                    if(FailIds.Contains(int.Parse(p.Value.ToString()))) throw toThrow;
                }
            }
        }

        private void RunTest(ScheduleCustomReport.ReportType type, DataTable data)
        {
            ScheduleCustomReport.TestReportRun
            (
              type,
              (ds, dsrc, sproc, pars) => MergeDataSetBasedOnSproc(ds, sproc, ReportRunData, data),
              (email) => { return; },
              (connInfo, name, retries, sqlparams) => { VerifyExpectedDate(sqlparams); return 0; },
              RunReport,
              (msg, exc) => { throw exc; },
              new DateTime(1900, 1, 1)
            );  
        }


        private void RunTest(ScheduleCustomReport.ReportType type, DataTable data, DateTime today)
        {
            ScheduleCustomReport.TestReportRun
            (
              type,
              (ds, dsrc, sproc, pars) => MergeDataSetBasedOnSproc(ds, sproc, ReportRunData, data),
              (email) => 
              { 
                  return; 
              },
              (connInfo, name, retries, sqlparams) => { VerifyExpectedDate(sqlparams); return 0; },
              RunReport, 
              (msg, e) => {}, 
              today
            );
        }

        private void RunExceptionTest(ScheduleCustomReport.ReportType type, DataTable data, DateTime setDate)
        {
            ScheduleCustomReport.TestReportRun
            (
              type,
              (ds, dsrc, sproc, pars) => MergeDataSetBasedOnSproc(ds, sproc, ReportRunData, data),
              (email) => { return; },
              (connInfo, name, retries, sqlparams) => { VerifyExpectedDate(sqlparams); return 0; },
              RunReport,
              //The messages are in the format <stuff> report id: <report id>, so we just have to extract the report id
              (msg, e) => { FailedReports.Add(int.Parse(msg.Split(':')[1])); },
              setDate
            );
        }

        [TestFixtureSetUp]
        public void Setup()
        {
            DataTable reports = new DataTable();
            for(int i = 0; i < ReportColumns.Length; i++)
            {
                reports.Columns.Add(ReportColumns[i], ReportColType[i]);
            }
            ReportRunData.Tables.Add(reports);
        }

        [TearDown]        
        public void ClearData()
        {
            //Make sure to clear report run data between tests
            ReportRunData.Clear();

            //And successful / not reports
            SuccessfulNextRunUpdates.Clear();
            FailedReports.Clear();
        }

        [Test]
        public void InactiveUsersCantRunReports()
        {
            DataTable Test = GetDailyTable();
            List<Guid> failUserIds = new List<Guid>();
            DateTime start = new DateTime(2011, 9, 5);
            List<Guid> ExpectedSuccessful = new List<Guid>();

            //Active users
            for (int i = 1; i <= 7; i++)
            {
                NextRunConfig c = new NextRunConfig()
                {
                    Frequency = i,
                    IncludeSatSun = true,
                    CurrentRun = start,
                    ExpectedNextRun = start.AddDays(i),
                    Description = "Active users should run"
                };

                DataRow r = MakeNewReport(c);
                Test.Rows.Add(c.ToRow());
                ExpectedSuccessful.Add(new Guid(r["UserId"].ToString()));
            }


            //Make a report with an inactive user
            NextRunConfig inactive = new NextRunConfig() 
            { 
                Frequency = 1, 
                IncludeSatSun = true, 
                Description = "Inactive users shouldn't be able to run reports" 
            };

            //More active users
            for (int i = 1; i <= 7; i++)
            {
                NextRunConfig c = new NextRunConfig()
                {
                    Frequency = i,
                    IncludeSatSun = true,
                    CurrentRun = start,
                    ExpectedNextRun = start.AddDays(i),
                    Description = "Active users should run"
                };

                DataRow r = MakeNewReport(c);
                Test.Rows.Add(c.ToRow());
                ExpectedSuccessful.Add(new Guid(r["UserId"].ToString()));
            }

            DataRow ReportRow = MakeNewReport(inactive);
            ReportRow["IsActive"] = 0;
            failUserIds.Add(new Guid(ReportRow["UserId"].ToString()));

            Test.Rows.Add(inactive.ToRow());

            ScheduleCustomReport.TestReportRun(ScheduleCustomReport.ReportType.Daily,
                      (ds, dsrc, sproc, pars) => MergeDataSetBasedOnSproc(ds, sproc, ReportRunData, Test),
                      (email) => { return; },
                      (connInfo, name, retries, sqlparams) => { return 0; },
                      (brokerId, queryId, userId) => {AddReportUserIdToSuccessList(userId); return FailReportBasedOnUserId(userId, failUserIds);}, //Should fail if it tries to run the report
                                    (msg, exc) => { throw exc; },
              new DateTime(1900, 1, 1)
                      );      
            //Make sure that everything that was supposed to run did
            ExpectedSuccessful.Sort();
            SuccessfulReportRuns.Sort();

            Assert.That(ExpectedSuccessful.SequenceEqual(SuccessfulReportRuns));
        }

        [Test]
        public void DontSendErrorEmailOnSuccess()
        {
            DataTable Daily = GetDailyTable();
            DateTime start = new DateTime(2011, 9, 5); //this should be a Monday

            //Simple successful run
            for (int i = 1; i <= 7; i++)
            {
                NextRunConfig c = new NextRunConfig()
                {
                    Frequency = i,
                    IncludeSatSun = true,
                    CurrentRun = start,
                    ExpectedNextRun = start.AddDays(i),
                    Description = "Daily reports, no skipping sat/sun"
                };

                MakeNewReport(c);
                Daily.Rows.Add(c.ToRow());
            }


            ScheduleCustomReport.TestReportRun
            (
              ScheduleCustomReport.ReportType.Daily,
              (ds, dsrc, sproc, pars) => MergeDataSetBasedOnSproc(ds, sproc, ReportRunData, Daily),
              (email) => { Assert.That(!email.Subject.Contains("Error"), "Error e-mails should not be sent on successful runs"); },
              (connInfo, name, retries, sqlparams) => { VerifyExpectedDate(sqlparams); return 0; },
              RunReport,
                            (msg, exc) => { throw exc; },
              new DateTime(1900, 1, 1)
            );
        }

        [Test]
        public void DailyReports()
        {
            DataTable Daily = GetDailyTable();
            DateTime start = new DateTime(2011, 9, 5); //this should be a Monday
            DateTime Today = new DateTime(2011, 9, 5);

            //Simple - no skipping sat/sun, start from some day and increase "frequency" up to a week
            for (int i = 1; i <= 7; i++)
            {
                NextRunConfig c = new NextRunConfig() { 
                    Frequency = i, 
                    IncludeSatSun = true,  
                    CurrentRun = start,
                    ExpectedNextRun = start.AddDays(i),
                    Description = "Daily reports, no skipping sat/sun"
                };

                MakeNewReport(c);
                Daily.Rows.Add(c.ToRow());
            }

            //Less simple - skip sat/sun. For this test we're making the frequency always be 5 (so it's always a week away)
            //and increasing the start date so that we cover starting on Monday through Sunday
            //Since Sat and Sun are special cases, we do them later
            for (int i = 0; i < 5; i++)
            {
                NextRunConfig c = new NextRunConfig()
                {
                    Frequency = 5,
                    IncludeSatSun = false,
                    CurrentRun = start.AddDays(i),
                    ExpectedNextRun = start.AddDays(i).AddDays(7), // we expect it to end up a week away
                    Description = "Daily reports, skipping sat/sun"
                };

                MakeNewReport(c);
                Daily.Rows.Add(c.ToRow());
            }
            //And here's sat/sun
            for (int i = 5; i < 7; i++)
            {
                NextRunConfig c = new NextRunConfig()
                {
                    Frequency = 5,
                    IncludeSatSun = false,
                    CurrentRun = start.AddDays(i),
                    //We expect both of them to end up on the 16th, 
                    //since it's supposed to treat sat and sun as if they were the previous Friday
                    ExpectedNextRun = start.AddDays(11),
                    Description = "Daily reports, skipping sat/sun, starting on " + start.AddDays(i).ToShortDateString()
                };

                MakeNewReport(c);
                Daily.Rows.Add(c.ToRow()); 
            }

            RunTest(ScheduleCustomReport.ReportType.Daily, Daily, Today);
        }

        [Test]
        public void WeeklyReports()
        {
            DataTable Weekly = GetWeeklyTable();

            DateTime start = new DateTime(2011, 9, 5);

            //Simple test - a task that starts on Monday and can run on any day of the week,
            //every 1..3 weeks.
            //They should all run on the next Tuesday
            for(int i = 1; i <= 3; i++)
            {
                //We have to set this manually, because of the way the queries work
                //Extra information (like multiple weekdays) is gathered by grouping 
                //reportId's together.
                int reportId = GetNextReportId();
                foreach(DayOfWeek day in Enum.GetValues(typeof(DayOfWeek)))
                {
                    NextRunConfig c = new NextRunConfig()
                    {
                        ReportId = reportId,
                        CurrentRun = start,
                        ExpectedNextRun = start.AddDays(1),
                        Frequency = i,
                        Weekday = day,
                        Description = "Weekly tasks, any day of the week starting Monday, should run on Tuesday"
                    };
                    AddReportItem(c);
                    Weekly.Rows.Add(c.ToRow());
                }
            }

            //a little less simple - a task that starts on Monday, can only run on Monday and Friday,
            //every 1..3 weeks
            //They should all run on the next Friday
            for (int i = 1; i <= 3; i++)
            {
                int reportId = GetNextReportId();
                foreach(DayOfWeek day in new DayOfWeek[] {DayOfWeek.Monday, DayOfWeek.Friday})
                {
                    NextRunConfig c = new NextRunConfig()
                    {
                        ReportId = reportId,
                        CurrentRun = start,
                        ExpectedNextRun = start.AddDays(4),
                        Frequency = i,
                        Weekday = day,
                        Description = "Weekly tasks, Monday and Friday only, should go to Friday"
                    };

                    AddReportItem(c);
                    Weekly.Rows.Add(c.ToRow());
                }
            }

            //Now we test the week skipping - starts on Mon, only runs on Mon
            for (int i = 1; i <= 10; i++)
            {
                NextRunConfig c = new NextRunConfig()
                {
                    CurrentRun = start,
                    ExpectedNextRun = start.AddDays(i * 7),
                    Frequency = i,
                    Weekday = DayOfWeek.Monday,
                    Description = "Weekly tasks, Monday only, should go to the Monday of the next allowable week"
                };

                MakeNewReport(c);
                Weekly.Rows.Add(c.ToRow());
            }

            //Now tests that can only run on Sunday (hey you never know), starting from Monday - Sunday, skip 2 weeks
            for (int i = -2; i < 5; i++)
            {
                NextRunConfig c = new NextRunConfig()
                {
                    CurrentRun = start.AddDays(i),
                    Frequency = 2,
                    Weekday = DayOfWeek.Sunday,
                    ExpectedNextRun = new DateTime(2011, 9, 18),
                    Description = "Weekly tasks, Sunday only, index " + i + ", with starting date " + start.AddDays(i).ToString()
                };

                MakeNewReport(c);
                Weekly.Rows.Add(c.ToRow());
            }

            //Test Saturday weekly reports since they're a special case.
            NextRunConfig config = new NextRunConfig()
            {
                CurrentRun = new DateTime(2015, 7, 4),
                Frequency = 1,
                Weekday = DayOfWeek.Saturday,
                ExpectedNextRun = new DateTime(2015, 7, 11),
                Description = "Weekly tasks, Saturday only, with starting date 7/4/2015"
            };

            MakeNewReport(config);
            Weekly.Rows.Add(config.ToRow());

            RunTest(ScheduleCustomReport.ReportType.Weekly, Weekly);
        }

        [Test]
        public void Monthly1Reports()
        {
            DataTable Monthly = GetMonthlyTable();

            DateTime start = new DateTime(2011, 9, 1);

            //Test every day of the month, report runs on the second Monday of October (which is the next month)
            for (int i = 0; i < 30; i++)
            {
                NextRunConfig c = new NextRunConfig()
                {
                    CurrentRun = start.AddDays(i),
                    Month = "October",
                    DayOfTheMonthOrWeekNo = 2,
                    Weekday = DayOfWeek.Monday,
                    ExpectedNextRun = new DateTime(2011, 10, 10),
                    Description = "Every day of one month, end in next month"
                };

                MakeNewReport(c);
                Monthly.Rows.Add(c.ToRow());
            }

            //Same as the first test, but during February of a leap year
            DateTime feb = new DateTime(2012, 2, 1);

            for (int i = 0; i < 29; i++)
            {
                NextRunConfig c = new NextRunConfig()
                {
                    CurrentRun = feb.AddDays(i),
                    Month = "March",
                    DayOfTheMonthOrWeekNo = 2,
                    Weekday = DayOfWeek.Monday,
                    ExpectedNextRun = new DateTime(2012, 3, 12),
                    Description = "Every day of Feb during leap, end in March"
                };

                MakeNewReport(c);
                Monthly.Rows.Add(c.ToRow());
            }

            //Same as the first test, but into the last Wednesday (leap day) of February of a leap year
            DateTime Jan = new DateTime(2012, 1, 1);

            for (int i = 0; i < 29; i++)
            {
                NextRunConfig c = new NextRunConfig()
                {
                    CurrentRun = Jan.AddDays(i),
                    Month = "February",
                    DayOfTheMonthOrWeekNo = 5,
                    Weekday = DayOfWeek.Wednesday,
                    ExpectedNextRun = new DateTime(2012, 2, 29),
                    Description = "Every day of Jan during leap year, end on leap day"
                };

                MakeNewReport(c);
                Monthly.Rows.Add(c.ToRow());
            }

            //Same as the first test, but over New Year's
            DateTime newYears = new DateTime(2011, 12, 1);

            for (int i = 0; i < 31; i++)
            {
                NextRunConfig c = new NextRunConfig()
                {
                    CurrentRun = newYears.AddDays(i),
                    Month = "January",
                    DayOfTheMonthOrWeekNo = 2,
                    Weekday = DayOfWeek.Monday,
                    ExpectedNextRun = new DateTime(2012, 1, 9),
                    Description = "Every day of Dec, end in Jan"
                };

                MakeNewReport(c);
                Monthly.Rows.Add(c.ToRow());
            }

            //Constant start date, day of the week, and week number, reports that run on a given month
            //Skip January itself, it's a special case

            //I had to compile this list by hand, since there doesn't seem to be an easy way of getting DateTime to do that calculation for you.
            //(which is why it has to be tested, of course!)
            DateTime[] SecondMondays = { new DateTime(2013, 1, 14), new DateTime(2012, 2, 13), new DateTime(2012, 3, 12), new DateTime(2012, 4, 9), new DateTime(2012, 5, 14), new DateTime(2012, 6, 11), new DateTime(2012, 7, 9), new DateTime(2012, 8, 13), new DateTime(2012, 9, 10), new DateTime(2012, 10, 8), new DateTime(2012, 11, 12), new DateTime(2012, 12, 10) };

            for (int i = 1; i < 12; i++)
            {
                NextRunConfig c = new NextRunConfig()
                {
                    CurrentRun = Jan,
                    Month = Months[i + Jan.Month],
                    DayOfTheMonthOrWeekNo = 2,
                    Weekday = DayOfWeek.Monday,
                    ExpectedNextRun = SecondMondays[i],
                    Description = "Every month"
                };

                MakeNewReport(c);
                Monthly.Rows.Add(c.ToRow());
            }

            NextRunConfig JanuaryLooparound = new NextRunConfig()
            {
                CurrentRun = Jan,
                Month = "January",
                DayOfTheMonthOrWeekNo = 2,
                Weekday = DayOfWeek.Monday,
                ExpectedNextRun = SecondMondays[0],
                Description = "January, should loop around"
            };
            MakeNewReport(JanuaryLooparound);
            Monthly.Rows.Add(JanuaryLooparound.ToRow());

            RunTest(ScheduleCustomReport.ReportType.MonthlyByDayOfWeek, Monthly);
        }

        [Test]
        public void Monthly2Reports()
        {
            DataTable Monthly = GetMonthlyTable();
            DateTime start = new DateTime(2011, 9, 1);

            //Starting from every day in the month, only run on the first day of October
            for (int i = 0; i < 30; i++)
            {
                NextRunConfig c = new NextRunConfig()
                {
                    CurrentRun = start.AddDays(i),
                    Month = "October",
                    DayOfTheMonthOrWeekNo = 1,
                    ExpectedNextRun = new DateTime(2011, 10, 1),
                    Description = "Every day, runs on first of the month"
                };

                MakeNewReport(c);
                Monthly.Rows.Add(c.ToRow());
            }

            //Starting from every day in the month save the last, only run on the last day of the month
            for (int i = 0; i < 29; i++)
            {
                NextRunConfig c = new NextRunConfig()
                {
                    CurrentRun = start.AddDays(i),
                    Month = "September",
                    DayOfTheMonthOrWeekNo = 30,
                    ExpectedNextRun = new DateTime(2011, 9, 30),
                    Description = "Every day, runs on last of the month"
                };
            }

            //Starting from every day in the month, only run on the first day of the month next year
            for (int i = 0; i < 30; i++)
            {
                NextRunConfig c = new NextRunConfig()
                {
                    CurrentRun = start.AddDays(i),
                    Month = "September",
                    DayOfTheMonthOrWeekNo = 1,
                    ExpectedNextRun = new DateTime(2012, 9, 1),
                    Description = "Every day, runs on first of the month next year"
                };

                MakeNewReport(c);
                Monthly.Rows.Add(c.ToRow());
            }

            //Runs only on leap day
            for (int i = 0; i < 30; i++)
            {
                NextRunConfig c = new NextRunConfig()
                {
                    CurrentRun = start.AddDays(i),
                    Month = "February",
                    DayOfTheMonthOrWeekNo = 29,
                    ExpectedNextRun = new DateTime(2012, 2, 29),
                    Description = "Only leap day!"
                };

                MakeNewReport(c);
                Monthly.Rows.Add(c.ToRow());
            }

            RunTest(ScheduleCustomReport.ReportType.MonthlyByDayOfMonth, Monthly);
        }

        /// <summary>
        /// Tests to make sure that impossible-to-calculate dates cause an exception, while other report runs still go through.
        /// Since the only run config I can think of that fails is having MonthlyByDayOfMonth try to run only on a month which doesn't
        /// have a particular day, that's what I use.
        /// </summary>
        [Test]
        public void CantCalcDateException()
        {
            DataTable Monthly = GetMonthlyTable();
            DateTime start = new DateTime(2011, 9, 1);
            List<int> ExpectedSuccessful = new List<int>();
            //Make some stuff that will work

            for (int i = 0; i < 30; i++)
            {
                NextRunConfig c = new NextRunConfig()
                {
                    CurrentRun = start.AddDays(i),
                    Month = "October",
                    DayOfTheMonthOrWeekNo = 1,
                    ExpectedNextRun = new DateTime(2011, 10, 1),
                    Description = "Every day, runs on first of the month"
                };

                MakeNewReport(c);
                Monthly.Rows.Add(c.ToRow());
                ExpectedSuccessful.Add(c.ReportId);
            }

            //This guy will fail
            NextRunConfig AlwaysFail = new NextRunConfig()
            {
                CurrentRun = start,
                Month = "February",
                DayOfTheMonthOrWeekNo = 30, //doesn't exist except in 1700s Sweden
                Description = "Impossible date"
            };

            MakeNewReport(AlwaysFail);
            Monthly.Rows.Add(AlwaysFail.ToRow());


            //Make some more stuff that will work
            for (int i = 0; i < 30; i++)
            {
                NextRunConfig c = new NextRunConfig()
                {
                    CurrentRun = start.AddDays(i),
                    Month = "October",
                    DayOfTheMonthOrWeekNo = 1,
                    ExpectedNextRun = new DateTime(2011, 10, 1),
                    Description = "Every day, runs on first of the month"
                };

                MakeNewReport(c);
                Monthly.Rows.Add(c.ToRow());
                ExpectedSuccessful.Add(c.ReportId);
            }

            RunExceptionTest(ScheduleCustomReport.ReportType.MonthlyByDayOfMonth, Monthly, new DateTime(1900, 1, 1));
            //Now, we need to make sure that the stuff that should have worked worked

            ExpectedSuccessful.Sort();
            SuccessfulNextRunUpdates.Sort();

            //Everything that was supposed to succeed did
            Assert.That(ExpectedSuccessful.SequenceEqual(SuccessfulNextRunUpdates));
            //Everything that was supposed to fail did
            Assert.That(FailedReports.Contains(AlwaysFail.ReportId));
            //And only things that were supposed to fail did
            Assert.That(FailedReports.Count == 1);
        }

        /// <summary>
        /// Test that it handles SQL Exceptions properly
        /// </summary>
        [Test]
        public void SQLException()
        {
            DataTable Daily = GetDailyTable();
            DateTime start = new DateTime(2011, 9, 5); //this should be a Monday
            List<int> ExpectedSuccessfulNextRunUpdates = new List<int>();
            List<int> ExpectedFail = new List<int>();
            List<int> SaveFail = new List<int>();
            List<Guid> FailGuids = new List<Guid>();
            List<CBaseEmail> Emails = new List<CBaseEmail>();

            //From daily - these should succeed
            for (int i = 1; i <= 7; i++)
            {
                NextRunConfig c = new NextRunConfig()
                {
                    Frequency = i,
                    IncludeSatSun = true,
                    CurrentRun = start,
                    ExpectedNextRun = start.AddDays(i),
                    Description = "Daily reports, no skipping sat/sun"
                };

                MakeNewReport(c);
                Daily.Rows.Add(c.ToRow());
                ExpectedSuccessfulNextRunUpdates.Add(c.ReportId);
            }

            //These should fail when saving next run info to the db
            for (int i = 1; i <= 7; i++)
            {
                NextRunConfig c = new NextRunConfig()
                {
                    Frequency = i,
                    IncludeSatSun = true,
                    ExpectedNextRun = start.AddDays(i).AddDays(1),
                    Description = "Daily reports, no skipping sat/sun"
                };

                MakeNewReport(c);
                Daily.Rows.Add(c.ToRow());
                ExpectedFail.Add(c.ReportId);
                SaveFail.Add(c.ReportId);
            }

            //These should also fail when running the reports
            for (int i = 1; i <= 7; i++)
            {
                NextRunConfig c = new NextRunConfig()
                {
                    Frequency = i,
                    IncludeSatSun = true,
                    CurrentRun = start,
                    ExpectedNextRun = start.AddDays(i),
                    Description = "Daily reports, no skipping sat/sun"
                };

                DataRow dr = MakeNewReport(c);
                Daily.Rows.Add(c.ToRow());
                ExpectedFail.Add(c.ReportId);
                //These will fail when running the report, but succeed when updating next run
                ExpectedSuccessfulNextRunUpdates.Add(c.ReportId);
                FailGuids.Add(new Guid(dr["UserId"].ToString()));
            }

            //Back to success
            for (int i = 1; i <= 7; i++)
            {
                NextRunConfig c = new NextRunConfig()
                {
                    Frequency = i,
                    IncludeSatSun = true,
                    CurrentRun = start,
                    ExpectedNextRun = start.AddDays(i),
                    Description = "Daily reports, no skipping sat/sun"
                };

                MakeNewReport(c);
                Daily.Rows.Add(c.ToRow());
                ExpectedSuccessfulNextRunUpdates.Add(c.ReportId);
            }

            ScheduleCustomReport.TestReportRun
            (
              ScheduleCustomReport.ReportType.Daily,
              (ds, dsrc, sproc, pars) => MergeDataSetBasedOnSproc(ds, sproc, ReportRunData, Daily),
              (email) => { Emails.Add(email); },
              (connInfo, name, retries, sqlparams) => {
                  ThrowOnTriggerReportId(sqlparams, SaveFail, new FakeSQLException());  
                  VerifyExpectedDate(sqlparams); 
                  return 0; 
              },
              (brokerId, queryId, userId) => ThrowReportBasedOnUserId(userId, FailGuids, new FakeSQLException()),
                //The messages are in the format <stuff> report id: <report id>, so we just have to extract the report id
              (msg, e) => { FailedReports.Add(int.Parse(msg.Split(':').Last().Trim())); },
              new DateTime(1900, 1, 1)
            );

            //Now, we need to make sure that the stuff that should have worked worked
            ExpectedSuccessfulNextRunUpdates.Sort();
            SuccessfulNextRunUpdates.Sort();
            //Everything that was supposed to succeed did
            Assert.That(ExpectedSuccessfulNextRunUpdates.SequenceEqual(SuccessfulNextRunUpdates), "Expected successful reports does not match observed successes");

            //And the things that shouldn't have didn't
            
            //Retry 4 times (23:00 to 1:00)
            var ExpectedFail4Times = new List<int>();
            
            ExpectedFail4Times.AddRange(ExpectedFail);
            ExpectedFail4Times.AddRange(ExpectedFail);
            ExpectedFail4Times.AddRange(ExpectedFail);
            ExpectedFail4Times.AddRange(ExpectedFail);
            ExpectedFail4Times.Sort();
            FailedReports.Sort();
            //Everything that was supposed to fail did
            Assert.That(ExpectedFail4Times.SequenceEqual(FailedReports), "Expected failed reports does not match observed failures");
        }

        /// <summary>
        /// Test that it handles failing reports properly
        /// </summary>
        [Test]
        public void ReportFailure()
        {
            DataTable Daily = GetDailyTable();
            DateTime start = new DateTime(2011, 9, 5); //this should be a Monday
            List<int> ExpectedSuccessfulNextRunUpdates = new List<int>();
            List<int> ExpectedFail = new List<int>();
            List<Guid> FailGuids = new List<Guid>();
            List<CBaseEmail> Emails = new List<CBaseEmail>();

            //From daily - these should succeed
            for (int i = 1; i <= 7; i++)
            {
                NextRunConfig c = new NextRunConfig()
                {
                    Frequency = i,
                    IncludeSatSun = true,
                    CurrentRun = start,
                    ExpectedNextRun = start.AddDays(i),
                    Description = "Daily reports, no skipping sat/sun"
                };

                MakeNewReport(c);
                Daily.Rows.Add(c.ToRow());
                ExpectedSuccessfulNextRunUpdates.Add(c.ReportId);
            }

            //These should fail when running the reports
            for (int i = 1; i <= 7; i++)
            {
                NextRunConfig c = new NextRunConfig()
                {
                    Frequency = i,
                    IncludeSatSun = true,
                    CurrentRun = start,
                    ExpectedNextRun = start.AddDays(i),
                    Description = "Daily reports, no skipping sat/sun"
                };

                DataRow dr = MakeNewReport(c);
                Daily.Rows.Add(c.ToRow());
                ExpectedFail.Add(c.ReportId);
                //These will fail when running the report, but succeed when updating next run
                ExpectedSuccessfulNextRunUpdates.Add(c.ReportId);
                FailGuids.Add(new Guid(dr["UserId"].ToString()));
            }

            //Back to success
            for (int i = 1; i <= 7; i++)
            {
                NextRunConfig c = new NextRunConfig()
                {
                    Frequency = i,
                    IncludeSatSun = true,
                    CurrentRun = start,
                    ExpectedNextRun = start.AddDays(i),
                    Description = "Daily reports, no skipping sat/sun"
                };

                MakeNewReport(c);
                Daily.Rows.Add(c.ToRow());
                ExpectedSuccessfulNextRunUpdates.Add(c.ReportId);
            }

            ScheduleCustomReport.TestReportRun
            (
              ScheduleCustomReport.ReportType.Daily,
              (ds, dsrc, sproc, pars) => MergeDataSetBasedOnSproc(ds, sproc, ReportRunData, Daily),
              (email) => { Emails.Add(email); },
              (connInfo, name, retries, sqlparams) =>
              {
                  ThrowOnTriggerLastRunDate(sqlparams, start.AddDays(1), new NoNullAllowedException());
                  VerifyExpectedDate(sqlparams);
                  return 0;
              },
              (brokerId, queryId, userId) => ThrowReportBasedOnUserId(userId, FailGuids, new FakeSQLException()),
                //The messages are in the format <stuff> report id: <report id>, so we just have to extract the report id
              (msg, e) => { FailedReports.Add(int.Parse(msg.Split(':').Last().Trim())); },
              new DateTime(1900, 1, 1)
            );

            //Now, we need to make sure that the stuff that should have worked worked
            ExpectedSuccessfulNextRunUpdates.Sort();
            SuccessfulNextRunUpdates.Sort();
            //Everything that was supposed to succeed did
            Assert.That(ExpectedSuccessfulNextRunUpdates.SequenceEqual(SuccessfulNextRunUpdates), "Not all reports that were supposed to succeed did!");

            //And the things that shouldn't have didn't

            //Retry 4 times (23:00 to 1:00)
            var ExpectedFail4Times = new List<int>();

            ExpectedFail4Times.AddRange(ExpectedFail);
            ExpectedFail4Times.AddRange(ExpectedFail);
            ExpectedFail4Times.AddRange(ExpectedFail);
            ExpectedFail4Times.AddRange(ExpectedFail);
            ExpectedFail4Times.Sort();
            FailedReports.Sort();
            //Everything that was supposed to fail did
            Assert.That(ExpectedFail4Times.SequenceEqual(FailedReports), "Not all reports that were supposed to fail did!");
        }
    }

 
}
