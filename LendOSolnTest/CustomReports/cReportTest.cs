﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using DataAccess;
using LendersOffice.Security;
using LendOSolnTest.Common;
using LendersOffice.Audit;
using System.Data.SqlClient;
using System.Data;
using LendersOffice.QueryProcessor;
using LendersOffice.Reports;

namespace LendOSolnTest.CustomReports
{
    class CField
    {
        public string Name { get; private set; }
        public string DataType { get; private set; }
        public string Table { get; private set; }

        public CField(string _name, string _type, string _table)
        {
            Name = _name;
            DataType = _type;
            Table = _table;
        }
    }

    [TestFixture]
    public class cReportTest
    {
        //Regular simple Custom Report with 2 columns and no conditions
        private const string CUSTOM_QUERY1 = "<?xml version=\"1.0\"?><Query xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><Label><Title>Test Report</Title><SavedBy>Test Report</SavedBy><SavedOn>2009-07-16T16:37:34.3002950-07:00</SavedOn><SavedId>531346ad-aaaa-4d62-b442-b1608530cc3b</SavedId><Version>4</Version></Label><Columns><Items><Column Type=\"Str\" Kind=\"Txt\" Show=\"true\"><Format /><Id>sLNm</Id><Name>Loan Number</Name><Functions><Items /></Functions></Column><Column Type=\"Dec\" Kind=\"Csh\" Show=\"true\"><Format>C</Format><Id>sLAmtCalc</Id><Name>Loan Amount</Name><Functions><Items /></Functions></Column></Items></Columns><Sorting><Items /></Sorting><GroupBy><Items /></GroupBy><Relates Type=\"An\"><Items /></Relates><Filters Type=\"An\"><Items><Conditions Type=\"An\"><Items><Conditions Type=\"Or\"><Items><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>0</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>1</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>2</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>3</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>4</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>5</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>6</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>7</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>13</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>19</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>8</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>10</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>9</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>11</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>12</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>15</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>16</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>18</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>17</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>20</Value></Argument></Condition></Items></Conditions></Items></Conditions></Items></Filters><Options><Items /></Options><Details>Test Report</Details><Id>745e3176-d09e-aaaa-bf76-b2981f0da47d</Id></Query>";
        
        //Custom report with 2 columns and 2 conditions that require sBranchName and sBranchCode
        private const string CUSTOM_QUERY2 = "<?xml version=\"1.0\"?><Query xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><Label><Title>Test Report 2</Title><SavedBy>Test Report</SavedBy><SavedOn>2009-07-16T16:37:34.3002950-07:00</SavedOn><SavedId>531346ad-aaaa-4d62-b442-b1608758cc3b</SavedId><Version>4</Version></Label><Columns><Items><Column Type=\"Str\" Kind=\"Txt\" Show=\"true\"><Format /><Id>sLNm</Id><Name>Loan Number</Name><Functions><Items /></Functions></Column><Column Type=\"Dec\" Kind=\"Csh\" Show=\"true\"><Format>C</Format><Id>sLAmtCalc</Id><Name>Loan Amount</Name><Functions><Items /></Functions></Column></Items></Columns><Sorting><Items /></Sorting><GroupBy><Items /></GroupBy><Relates Type=\"An\"><Items><Conditions Type=\"An\"><Items><Condition Type=\"Ne\" Id=\"sBranchNm\"><Argument Type=\"Const\"><Value>alfabeta</Value></Argument></Condition><Condition Type=\"Ne\" Id=\"sBranchCode\"><Argument Type=\"Const\"><Value>alfabeta</Value></Argument></Condition></Items></Conditions></Items></Relates><Filters Type=\"An\"><Items><Conditions Type=\"An\"><Items><Conditions Type=\"Or\"><Items><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>0</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>1</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>2</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>3</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>4</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>5</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>6</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>7</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>13</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>19</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>8</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>10</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>9</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>11</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>12</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>15</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>16</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>18</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>17</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>20</Value></Argument></Condition></Items></Conditions></Items></Conditions></Items></Filters><Options><Items /></Options><Details>Test Report</Details><Id>745e3176-d09e-aaaa-bf76-b2981f0da47d</Id></Query>";

        //Custom report with 2 columns and 2 conditions that require a field to be different to the value of sBranchName and sBranchCode
        private const string CUSTOM_QUERY3 = "<?xml version=\"1.0\"?><Query xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><Label><Title>Test Report 2</Title><SavedBy>Test Report</SavedBy><SavedOn>2009-07-16T16:37:34.3002950-07:00</SavedOn><SavedId>531346ad-aaaa-4d62-b442-b1668158318e</SavedId><Version>4</Version></Label><Columns><Items><Column Type=\"Str\" Kind=\"Txt\" Show=\"true\"><Format /><Id>sLNm</Id><Name>Loan Number</Name><Functions><Items /></Functions></Column><Column Type=\"Dec\" Kind=\"Csh\" Show=\"true\"><Format>C</Format><Id>sLAmtCalc</Id><Name>Loan Amount</Name><Functions><Items /></Functions></Column></Items></Columns><Sorting><Items /></Sorting><GroupBy><Items /></GroupBy><Relates Type=\"An\"><Items><Conditions Type=\"An\"><Items><Condition Type=\"Ne\" Id=\"sLpTemplateNm\"><Argument Type=\"Field\"><Value>sBranchNm</Value></Argument></Condition><Condition Type=\"Ne\" Id=\"sLpTemplateNm\"><Argument Type=\"Field\"><Value>sBranchCode</Value></Argument></Condition></Items></Conditions></Items></Relates><Filters Type=\"An\"><Items><Conditions Type=\"An\"><Items><Conditions Type=\"Or\"><Items><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>0</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>1</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>2</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>3</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>4</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>5</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>6</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>7</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>13</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>19</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>8</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>10</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>9</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>11</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>12</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>15</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>16</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>18</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>17</Value></Argument></Condition><Condition Type=\"Eq\" Id=\"sStatusT\"><Argument Type=\"Const\"><Value>20</Value></Argument></Condition></Items></Conditions></Items></Conditions></Items></Filters><Options><Items /></Options><Details>Test Report</Details><Id>745e3176-d09e-aaaa-bf76-b2981f0da47d</Id></Query>";

        private Dictionary<string, CField> LoanCache1 = new Dictionary<string, CField>();
        private Dictionary<string, CField> LoanCache2 = new Dictionary<string, CField>();
        private Dictionary<string, CField> ViewLoanCache = new Dictionary<string, CField>();
        private Dictionary<string, CField> ViewLoanCacheBranch = new Dictionary<string, CField>();
        private AbstractUserPrincipal m_principal;
        
        [TestFixtureSetUp]
        public void Setup()
        {
            m_principal = LoginTools.LoginAs(TestAccountType.LockDesk_AllPerFieldPerm);
            Assert.IsNotNull(m_principal);
        }


        //av opm 61259 Add unit test to determine if enum custom report values are missing
        [Test]
        public void ValidateCustomReportFields()
        {
            System.Reflection.Assembly a = typeof(LoanReporting).Assembly;

            var skipList = new[] 
            {
                new { Name = "DataAccess.E_sStatusT", Entries = new[] { "14" } }
            };

            LoanReporting lR = new LoanReporting();
            foreach (Field field in lR.Fields.GetAllFields())
            {
                if (field.Type != Field.E_FieldType.Enu)
                {
                    continue;
                }

                       
                var enumType = a.GetType(field.EnumName);
                Assert.That(enumType, Is.Not.Null);

                Array realValues = Enum.GetValues(enumType);
                List<string> fieldValues = new List<string>(field.Mapping.Count);

                foreach( string value in field.Mapping )
                {
                    fieldValues.Add(field.Mapping.ByVal[value]);
                }

                foreach (int value in realValues  )
                {
                    if (skipList.Any(p => p.Name == field.EnumName && p.Entries.Any(y => y == value.ToString())))
                    {
                        continue;
                    }
                    CollectionAssert.Contains(fieldValues, value.ToString(), "A Enum Value is missing from Custom Reports : " +  field.Name + "/" + field.EnumName);
                }


                
            }
        }
    }

 
}
