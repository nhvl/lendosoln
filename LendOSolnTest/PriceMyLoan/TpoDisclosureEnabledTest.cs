﻿namespace LendOSolnTest.PriceMyLoan
{
    using System;
    using System.Collections.Generic;
    using DataAccess;
    using LendersOffice.Admin;
    using LendOSolnTest.Admin;
    using NUnit.Framework;

    [TestFixture]
    public class TpoDisclosureEnabledTest
    {
        private readonly MockBrokerDb broker = new MockBrokerDb();
        private const string FirstGroupName = "Test 1";
        private const string SecondGroupName = "Test 2";
        private readonly Guid FirstGroupId = new Guid("11111111-1111-1111-1111-111111111111");
        private readonly Guid SecondGroupId = new Guid("22222222-2222-2222-2222-222222222222");

        [TearDown]
        public void ResetBroker()
        {
            this.broker.TpoRedisclosureAllowedGroupType = NewTpoFeatureOcGroupAccess.None;
            this.broker.TpoInitialClosingDisclosureAllowedGroupType = NewTpoFeatureOcGroupAccess.None;
            this.broker.TpoNewFeatureAlphaTestingGroup = null;
            this.broker.TpoNewFeatureBetaTestingGroup = null;
        }
        
        [Test]
        public void InitialDisclosure_AlwaysAllowed()
        {
            foreach (NewTpoFeatureOcGroupAccess groupAccess in Enum.GetValues(typeof(NewTpoFeatureOcGroupAccess)))
            {
                this.broker.TpoRedisclosureAllowedGroupType = groupAccess;
                this.broker.TpoInitialClosingDisclosureAllowedGroupType = groupAccess;
                Assert.IsTrue(Tools.IsTpoDisclosureEnabled(this.broker, membershipGroups: null, type: TpoDisclosureType.InitialDisclosure));
            }
        }

        [Test]
        public void Redisclosure_NoGroupAccess_IsNotAllowed()
        {
            this.broker.TpoRedisclosureAllowedGroupType = NewTpoFeatureOcGroupAccess.None;
            Assert.IsFalse(Tools.IsTpoDisclosureEnabled(this.broker, this.GetDefaultGroups(inFirstGroup: true), TpoDisclosureType.Redisclosure));
            Assert.IsFalse(Tools.IsTpoDisclosureEnabled(this.broker, this.GetDefaultGroups(inFirstGroup: false), TpoDisclosureType.Redisclosure));
        }

        [Test]
        public void Redisclosure_AlphaGroupAccess_IsAllowedForAlphaGroup()
        {
            this.broker.TpoRedisclosureAllowedGroupType = NewTpoFeatureOcGroupAccess.Alpha;
            this.broker.TpoNewFeatureAlphaTestingGroup = this.FirstGroupId;
            this.broker.TpoNewFeatureBetaTestingGroup = this.SecondGroupId;

            Assert.IsTrue(Tools.IsTpoDisclosureEnabled(this.broker, this.GetDefaultGroups(inFirstGroup: true), TpoDisclosureType.Redisclosure, FirstGroupName, SecondGroupName));
            Assert.IsFalse(Tools.IsTpoDisclosureEnabled(this.broker, this.GetDefaultGroups(inFirstGroup: false), TpoDisclosureType.Redisclosure, FirstGroupName, SecondGroupName));
        }

        [Test]
        public void Redisclosure_AlphaAndBetaGroupAccess_IsAllowedForAlphaAndBetaGroup()
        {
            this.broker.TpoRedisclosureAllowedGroupType = NewTpoFeatureOcGroupAccess.AlphaAndBeta;
            this.broker.TpoNewFeatureAlphaTestingGroup = this.FirstGroupId;
            this.broker.TpoNewFeatureBetaTestingGroup = this.SecondGroupId;

            Assert.IsTrue(Tools.IsTpoDisclosureEnabled(this.broker, this.GetDefaultGroups(inFirstGroup: true), TpoDisclosureType.Redisclosure, FirstGroupName, SecondGroupName));
            Assert.IsTrue(Tools.IsTpoDisclosureEnabled(this.broker, this.GetDefaultGroups(inFirstGroup: false), TpoDisclosureType.Redisclosure, FirstGroupName, SecondGroupName));
        }

        [Test]
        public void Redisclosure_AllGroupAccess_IsAllowed()
        {
            this.broker.TpoRedisclosureAllowedGroupType = NewTpoFeatureOcGroupAccess.All;
            Assert.IsTrue(Tools.IsTpoDisclosureEnabled(this.broker, this.GetDefaultGroups(inFirstGroup: true), TpoDisclosureType.Redisclosure));
            Assert.IsTrue(Tools.IsTpoDisclosureEnabled(this.broker, this.GetDefaultGroups(inFirstGroup: false), TpoDisclosureType.Redisclosure));
        }

        [Test]
        public void InitialClosingDisclosure_NoGroupAccess_IsNotAllowed()
        {
            this.broker.TpoInitialClosingDisclosureAllowedGroupType = NewTpoFeatureOcGroupAccess.None;
            Assert.IsFalse(Tools.IsTpoDisclosureEnabled(this.broker, this.GetDefaultGroups(inFirstGroup: true), TpoDisclosureType.InitialClosingDisclosure));
            Assert.IsFalse(Tools.IsTpoDisclosureEnabled(this.broker, this.GetDefaultGroups(inFirstGroup: false), TpoDisclosureType.InitialClosingDisclosure));
        }

        [Test]
        public void InitialClosingDisclosure_AlphaGroupAccess_IsAllowedForAlphaGroup()
        {
            this.broker.TpoInitialClosingDisclosureAllowedGroupType = NewTpoFeatureOcGroupAccess.Alpha;
            this.broker.TpoNewFeatureAlphaTestingGroup = this.FirstGroupId;
            this.broker.TpoNewFeatureBetaTestingGroup = this.SecondGroupId;

            Assert.IsTrue(Tools.IsTpoDisclosureEnabled(this.broker, this.GetDefaultGroups(inFirstGroup: true), TpoDisclosureType.InitialClosingDisclosure, FirstGroupName, SecondGroupName));
            Assert.IsFalse(Tools.IsTpoDisclosureEnabled(this.broker, this.GetDefaultGroups(inFirstGroup: false), TpoDisclosureType.InitialClosingDisclosure, FirstGroupName, SecondGroupName));
        }

        [Test]
        public void InitialClosingDisclosure_AlphaAndBetaGroupAccess_IsAllowedForAlphaAndBetaGroup()
        {
            this.broker.TpoInitialClosingDisclosureAllowedGroupType = NewTpoFeatureOcGroupAccess.AlphaAndBeta;
            this.broker.TpoNewFeatureAlphaTestingGroup = this.FirstGroupId;
            this.broker.TpoNewFeatureBetaTestingGroup = this.SecondGroupId;

            Assert.IsTrue(Tools.IsTpoDisclosureEnabled(this.broker, this.GetDefaultGroups(inFirstGroup: true), TpoDisclosureType.InitialClosingDisclosure, FirstGroupName, SecondGroupName));
            Assert.IsTrue(Tools.IsTpoDisclosureEnabled(this.broker, this.GetDefaultGroups(inFirstGroup: false), TpoDisclosureType.InitialClosingDisclosure, FirstGroupName, SecondGroupName));
        }

        [Test]
        public void InitialClosingDisclosure_AllGroupAccess_IsAllowed()
        {
            this.broker.TpoInitialClosingDisclosureAllowedGroupType = NewTpoFeatureOcGroupAccess.All;
            Assert.IsTrue(Tools.IsTpoDisclosureEnabled(this.broker, this.GetDefaultGroups(inFirstGroup: true), TpoDisclosureType.InitialClosingDisclosure));
            Assert.IsTrue(Tools.IsTpoDisclosureEnabled(this.broker, this.GetDefaultGroups(inFirstGroup: false), TpoDisclosureType.InitialClosingDisclosure));
        }

        private IEnumerable<MembershipGroup> GetDefaultGroups(bool inFirstGroup)
        {
            return new[]
            {
                new MembershipGroup() { GroupId = FirstGroupId, GroupName = FirstGroupName, IsInGroup = inFirstGroup ? 1 : 0 },
                new MembershipGroup() { GroupId = SecondGroupId, GroupName = SecondGroupName, IsInGroup = inFirstGroup ? 0 : 1 }
            };
        }
    }
}
