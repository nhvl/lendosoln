﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using DataAccess;
using LendersOffice.PriceMyLoan;

namespace LendOSolnTest.PriceMyLoan
{
    [TestFixture]
    public class ResultActionLinkTest
    {
        [Test]
        public void Test()
        {
            #region Test data
            var testcases = new[]
            {

                new { Has2ndLoan = true, CurrentLienQualifyModeT = E_sLienQualifyModeT.ThisLoan, IsPmlSubmissionAllowed=true, 
                    IsEncompassIntegration=true, LpeRunModeT = E_LpeRunModeT.Full, IsRateLockedAtSubmission=true,
                    Expected_NormalTitle = ResultActionLink.ResultActionLinkTitle_GoTo2ndLien,
                    Expected_ExpiredTitle = ResultActionLink.ResultActionLinkTitle_GoTo2ndLien,
                },
                new { Has2ndLoan = true, CurrentLienQualifyModeT = E_sLienQualifyModeT.ThisLoan, IsPmlSubmissionAllowed=true, 
                    IsEncompassIntegration=true, LpeRunModeT = E_LpeRunModeT.Full, IsRateLockedAtSubmission=false,
                    Expected_NormalTitle = ResultActionLink.ResultActionLinkTitle_GoTo2ndLien,
                    Expected_ExpiredTitle = ResultActionLink.ResultActionLinkTitle_GoTo2ndLien,
                },

                new { Has2ndLoan = true, CurrentLienQualifyModeT = E_sLienQualifyModeT.ThisLoan, IsPmlSubmissionAllowed=true, 
                    IsEncompassIntegration=true, LpeRunModeT = E_LpeRunModeT.OriginalProductOnly_Direct, IsRateLockedAtSubmission=true,
                    Expected_NormalTitle = ResultActionLink.ResultActionLinkTitle_GoTo2ndLien,
                    Expected_ExpiredTitle = ResultActionLink.ResultActionLinkTitle_GoTo2ndLien,
                },
                new { Has2ndLoan = true, CurrentLienQualifyModeT = E_sLienQualifyModeT.ThisLoan, IsPmlSubmissionAllowed=true, 
                    IsEncompassIntegration=true, LpeRunModeT = E_LpeRunModeT.OriginalProductOnly_Direct, IsRateLockedAtSubmission=false,
                    Expected_NormalTitle = ResultActionLink.ResultActionLinkTitle_GoTo2ndLien,
                    Expected_ExpiredTitle = ResultActionLink.ResultActionLinkTitle_GoTo2ndLien,
                },

                new { Has2ndLoan = true, CurrentLienQualifyModeT = E_sLienQualifyModeT.ThisLoan, IsPmlSubmissionAllowed=false, 
                    IsEncompassIntegration=true, LpeRunModeT = E_LpeRunModeT.Full, IsRateLockedAtSubmission=true,
                    Expected_NormalTitle = ResultActionLink.ResultActionLinkTitle_GoTo2ndLien,
                    Expected_ExpiredTitle = ResultActionLink.ResultActionLinkTitle_GoTo2ndLien,
                },
                new { Has2ndLoan = true, CurrentLienQualifyModeT = E_sLienQualifyModeT.ThisLoan, IsPmlSubmissionAllowed=false, 
                    IsEncompassIntegration=true, LpeRunModeT = E_LpeRunModeT.Full, IsRateLockedAtSubmission=false,
                    Expected_NormalTitle = ResultActionLink.ResultActionLinkTitle_GoTo2ndLien,
                    Expected_ExpiredTitle = ResultActionLink.ResultActionLinkTitle_GoTo2ndLien,
                },

                new { Has2ndLoan = true, CurrentLienQualifyModeT = E_sLienQualifyModeT.ThisLoan, IsPmlSubmissionAllowed=false, 
                    IsEncompassIntegration=true, LpeRunModeT = E_LpeRunModeT.OriginalProductOnly_Direct, IsRateLockedAtSubmission=true,
                    Expected_NormalTitle = ResultActionLink.ResultActionLinkTitle_GoTo2ndLien,
                    Expected_ExpiredTitle = ResultActionLink.ResultActionLinkTitle_GoTo2ndLien,
                },
                new { Has2ndLoan = true, CurrentLienQualifyModeT = E_sLienQualifyModeT.ThisLoan, IsPmlSubmissionAllowed=false, 
                    IsEncompassIntegration=true, LpeRunModeT = E_LpeRunModeT.OriginalProductOnly_Direct, IsRateLockedAtSubmission=false,
                    Expected_NormalTitle = ResultActionLink.ResultActionLinkTitle_GoTo2ndLien,
                    Expected_ExpiredTitle = ResultActionLink.ResultActionLinkTitle_GoTo2ndLien,
                },


                new { Has2ndLoan = true, CurrentLienQualifyModeT = E_sLienQualifyModeT.OtherLoan, IsPmlSubmissionAllowed=true, 
                    IsEncompassIntegration=true, LpeRunModeT = E_LpeRunModeT.Full, IsRateLockedAtSubmission=true,
                    Expected_NormalTitle = ResultActionLink.ResultActionLinkTitle_RegisterLock,
                    Expected_ExpiredTitle = ResultActionLink.ResultActionLinkTitle_RegisterOnly,
                },
                new { Has2ndLoan = true, CurrentLienQualifyModeT = E_sLienQualifyModeT.OtherLoan, IsPmlSubmissionAllowed=true, 
                    IsEncompassIntegration=true, LpeRunModeT = E_LpeRunModeT.Full, IsRateLockedAtSubmission=false,
                    Expected_NormalTitle = ResultActionLink.ResultActionLinkTitle_RegisterOnly,
                    Expected_ExpiredTitle = ResultActionLink.ResultActionLinkTitle_RegisterOnly,
                },

                new { Has2ndLoan = true, CurrentLienQualifyModeT = E_sLienQualifyModeT.OtherLoan, IsPmlSubmissionAllowed=true, 
                    IsEncompassIntegration=true, LpeRunModeT = E_LpeRunModeT.OriginalProductOnly_Direct, IsRateLockedAtSubmission=true,
                    Expected_NormalTitle = ResultActionLink.ResultActionLinkTitle_LockOnly,
                    Expected_ExpiredTitle = ResultActionLink.ResultActionLinkTitle_LockUnavailable,
                },
                new { Has2ndLoan = true, CurrentLienQualifyModeT = E_sLienQualifyModeT.OtherLoan, IsPmlSubmissionAllowed=true, 
                    IsEncompassIntegration=true, LpeRunModeT = E_LpeRunModeT.OriginalProductOnly_Direct, IsRateLockedAtSubmission=false,
                    Expected_NormalTitle = string.Empty,
                    Expected_ExpiredTitle = string.Empty,
                },

                new { Has2ndLoan = true, CurrentLienQualifyModeT = E_sLienQualifyModeT.OtherLoan, IsPmlSubmissionAllowed=false, 
                    IsEncompassIntegration=true, LpeRunModeT = E_LpeRunModeT.Full, IsRateLockedAtSubmission=true,
                    Expected_NormalTitle = ResultActionLink.ResultActionLinkTitle_RegisterLock,
                    Expected_ExpiredTitle = ResultActionLink.ResultActionLinkTitle_RegisterOnly,
                },
                new { Has2ndLoan = true, CurrentLienQualifyModeT = E_sLienQualifyModeT.OtherLoan, IsPmlSubmissionAllowed=false, 
                    IsEncompassIntegration=true, LpeRunModeT = E_LpeRunModeT.Full, IsRateLockedAtSubmission=false,
                    Expected_NormalTitle = ResultActionLink.ResultActionLinkTitle_RegisterOnly,
                    Expected_ExpiredTitle = ResultActionLink.ResultActionLinkTitle_RegisterOnly,
                },

                new { Has2ndLoan = true, CurrentLienQualifyModeT = E_sLienQualifyModeT.OtherLoan, IsPmlSubmissionAllowed=false, 
                    IsEncompassIntegration=true, LpeRunModeT = E_LpeRunModeT.OriginalProductOnly_Direct, IsRateLockedAtSubmission=true,
                    Expected_NormalTitle = ResultActionLink.ResultActionLinkTitle_LockOnly,
                    Expected_ExpiredTitle = ResultActionLink.ResultActionLinkTitle_LockUnavailable
                },
                new { Has2ndLoan = true, CurrentLienQualifyModeT = E_sLienQualifyModeT.OtherLoan, IsPmlSubmissionAllowed=false, 
                    IsEncompassIntegration=true, LpeRunModeT = E_LpeRunModeT.OriginalProductOnly_Direct, IsRateLockedAtSubmission=false,
                    Expected_NormalTitle = string.Empty,
                    Expected_ExpiredTitle = string.Empty,
                },

                new { Has2ndLoan = false, CurrentLienQualifyModeT = E_sLienQualifyModeT.ThisLoan, IsPmlSubmissionAllowed=true, 
                    IsEncompassIntegration=true, LpeRunModeT = E_LpeRunModeT.Full, IsRateLockedAtSubmission=true,
                    Expected_NormalTitle = ResultActionLink.ResultActionLinkTitle_RegisterLock,
                    Expected_ExpiredTitle = ResultActionLink.ResultActionLinkTitle_RegisterOnly,
                },
                new { Has2ndLoan = false, CurrentLienQualifyModeT = E_sLienQualifyModeT.ThisLoan, IsPmlSubmissionAllowed=true, 
                    IsEncompassIntegration=true, LpeRunModeT = E_LpeRunModeT.Full, IsRateLockedAtSubmission=false,
                    Expected_NormalTitle = ResultActionLink.ResultActionLinkTitle_RegisterOnly,
                    Expected_ExpiredTitle = ResultActionLink.ResultActionLinkTitle_RegisterOnly,
                },

                new { Has2ndLoan = false, CurrentLienQualifyModeT = E_sLienQualifyModeT.ThisLoan, IsPmlSubmissionAllowed=true, 
                    IsEncompassIntegration=true, LpeRunModeT = E_LpeRunModeT.OriginalProductOnly_Direct, IsRateLockedAtSubmission=true,
                    Expected_NormalTitle = ResultActionLink.ResultActionLinkTitle_LockOnly,
                    Expected_ExpiredTitle = ResultActionLink.ResultActionLinkTitle_LockUnavailable,
                },
                new { Has2ndLoan = false, CurrentLienQualifyModeT = E_sLienQualifyModeT.ThisLoan, IsPmlSubmissionAllowed=true, 
                    IsEncompassIntegration=true, LpeRunModeT = E_LpeRunModeT.OriginalProductOnly_Direct, IsRateLockedAtSubmission=false,
                    Expected_NormalTitle = string.Empty,
                    Expected_ExpiredTitle = string.Empty,
                },

                new { Has2ndLoan = false, CurrentLienQualifyModeT = E_sLienQualifyModeT.ThisLoan, IsPmlSubmissionAllowed=false, 
                    IsEncompassIntegration=true, LpeRunModeT = E_LpeRunModeT.Full, IsRateLockedAtSubmission=true,
                    Expected_NormalTitle = ResultActionLink.ResultActionLinkTitle_RegisterLock,
                    Expected_ExpiredTitle = ResultActionLink.ResultActionLinkTitle_RegisterOnly,
                },
                new { Has2ndLoan = false, CurrentLienQualifyModeT = E_sLienQualifyModeT.ThisLoan, IsPmlSubmissionAllowed=false, 
                    IsEncompassIntegration=true, LpeRunModeT = E_LpeRunModeT.Full, IsRateLockedAtSubmission=false,
                    Expected_NormalTitle = ResultActionLink.ResultActionLinkTitle_RegisterOnly,
                    Expected_ExpiredTitle = ResultActionLink.ResultActionLinkTitle_RegisterOnly,
                },

                new { Has2ndLoan = false, CurrentLienQualifyModeT = E_sLienQualifyModeT.ThisLoan, IsPmlSubmissionAllowed=false, 
                    IsEncompassIntegration=true, LpeRunModeT = E_LpeRunModeT.OriginalProductOnly_Direct, IsRateLockedAtSubmission=true,
                    Expected_NormalTitle = ResultActionLink.ResultActionLinkTitle_LockOnly,
                    Expected_ExpiredTitle = ResultActionLink.ResultActionLinkTitle_LockUnavailable,
                },
                new { Has2ndLoan = false, CurrentLienQualifyModeT = E_sLienQualifyModeT.ThisLoan, IsPmlSubmissionAllowed=false, 
                    IsEncompassIntegration=true, LpeRunModeT = E_LpeRunModeT.OriginalProductOnly_Direct, IsRateLockedAtSubmission=false,
                    Expected_NormalTitle = string.Empty,
                    Expected_ExpiredTitle = string.Empty,
                },

                new { Has2ndLoan = false, CurrentLienQualifyModeT = E_sLienQualifyModeT.ThisLoan, IsPmlSubmissionAllowed=false, 
                    IsEncompassIntegration=false, LpeRunModeT = E_LpeRunModeT.Full, IsRateLockedAtSubmission=false,
                    Expected_NormalTitle = ResultActionLink.ResultActionLinkTitle_Select,
                    Expected_ExpiredTitle = ResultActionLink.ResultActionLinkTitle_Select,
                },
                new { Has2ndLoan = false, CurrentLienQualifyModeT = E_sLienQualifyModeT.ThisLoan, IsPmlSubmissionAllowed=false, 
                    IsEncompassIntegration=false, LpeRunModeT = E_LpeRunModeT.Full, IsRateLockedAtSubmission=false,
                    Expected_NormalTitle = ResultActionLink.ResultActionLinkTitle_Select,
                    Expected_ExpiredTitle = ResultActionLink.ResultActionLinkTitle_Select,
                },

            };
            #endregion

            foreach (var testcase in testcases)
            {
                List<string> titles = ResultActionLink.GetLinkTitles(testcase.Has2ndLoan, testcase.CurrentLienQualifyModeT,
                    testcase.IsPmlSubmissionAllowed, testcase.IsEncompassIntegration, testcase.LpeRunModeT,testcase.IsRateLockedAtSubmission,
                    true, (testcase.LpeRunModeT == E_LpeRunModeT.OriginalProductOnly_Direct), true
                    );

                Assert.AreEqual(2, titles.Count);

                Assert.AreEqual(testcase.Expected_NormalTitle, titles[0], "NormalTitle. " + testcase);
                Assert.AreEqual(testcase.Expected_ExpiredTitle, titles[1], "ExpiredTitle. " + testcase);
            }
        }
    }
}
