﻿namespace LendOSolnTest.TRID
{
    using Common;
    using DataAccess;
    using Fakes;
    using LendersOffice.ObjLib.TRID2;
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Unit tests for calculating Escrow Accounts under TRID
    /// </summary>
    [TestFixture]
    public class TRIDEscrowAccountTest
    {
        private CPageData dataLoan = null;
        private string errMsg;

        private static CPageData CreateDataObject()
        {
            return FakePageData.Create();
        }

        private void InitData()
        {
            if (this.dataLoan == null)
            {
                this.dataLoan = CreateDataObject();
                this.dataLoan.InitLoad();

                // Use a test loan to get around broker temp option DisableTrid20ForNonTestLoans.
                this.dataLoan.sLoanFileT = E_sLoanFileT.Test;

                this.dataLoan.sDocMagicClosingDLckd = true;
                this.dataLoan.sDocMagicClosingD = CDateTime.Create(new DateTime(2018, 6, 16));

                this.dataLoan.sSchedDueD1Lckd = true;
                this.dataLoan.sSchedDueD1_rep = "8/1/2018";

                this.dataLoan.sTridTargetRegulationVersionTLckd = true;

                this.dataLoan.sLAmtLckd = true;
                this.dataLoan.sProMInsLckd = true;
            }            
        }

        private void UpdateErrMsg()
        {
            if (dataLoan.sTridTargetRegulationVersionT == TridTargetRegulationVersionT.TRID2017 && dataLoan.sTridClosingDisclosureCalculateEscrowAccountFromT == TridClosingDisclosureCalculateEscrowAccountFromT.FirstPaymentDate)
            {
                this.errMsg = "Settings: TRID 2017, First Payment Date. Failed for: ";
            }
            else if (dataLoan.sTridTargetRegulationVersionT != TridTargetRegulationVersionT.TRID2017 && dataLoan.sTridClosingDisclosureCalculateEscrowAccountFromT == TridClosingDisclosureCalculateEscrowAccountFromT.FirstPaymentDate)
            {
                this.errMsg = "Settings: TRID 2015, First Payment Date. Failed for: ";
            }
            else if (dataLoan.sTridTargetRegulationVersionT != TridTargetRegulationVersionT.TRID2017 && dataLoan.sTridClosingDisclosureCalculateEscrowAccountFromT == TridClosingDisclosureCalculateEscrowAccountFromT.Consummation)
            {
                this.errMsg = "Settings: TRID 2015, Consummation Date. Failed for: ";
            }
            else
            {
                this.errMsg = "Settings: TRID 2017, Consummation Date. Failed for: ";
            }
        }

        [Test]
        public void AllEscrowShortYearTest()
        {
            InitData();
            this.dataLoan.sLAmtCalc = 382500M;
            this.dataLoan.sProMIns = 159.38M;

            // gotten from Dev loan Case467072-AllEscrowShortYear on KevinTest
            string housingExpenseJson = TestUtilities.ReadFile("TRID/Case467072-AllEscrowShortYear-HousingExpense.json");
            this.dataLoan.CopysHousingExpenseJson(housingExpenseJson);

            // TRID 2017, 1 year from first payment date test
            this.dataLoan.sTridTargetRegulationVersionT = TridTargetRegulationVersionT.TRID2017;
            this.dataLoan.sTridClosingDisclosureCalculateEscrowAccountFromT = TridClosingDisclosureCalculateEscrowAccountFromT.FirstPaymentDate;
            UpdateErrMsg();

            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(10012.56M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(0M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureNonEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureNonEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(834.38M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureMonthlyEscrowPayment_rep, errMsg + "sClosingDisclosureMonthlyEscrowPayment");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(0M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosurePropertyCostsFirstYear_rep, errMsg + "sClosingDisclosurePropertyCostsFirstYear");

            // TRID 2015, 1 year from first payment date test
            this.dataLoan.sTridTargetRegulationVersionT = TridTargetRegulationVersionT.TRID2015;
            UpdateErrMsg();

            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(8100M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(0M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureNonEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureNonEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(675M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureMonthlyEscrowPayment_rep, errMsg + "sClosingDisclosureMonthlyEscrowPayment");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(0M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosurePropertyCostsFirstYear_rep, errMsg + "sClosingDisclosurePropertyCostsFirstYear");

            // TRID 2015, 1 year from consummation date test
            this.dataLoan.sTridClosingDisclosureCalculateEscrowAccountFromT = TridClosingDisclosureCalculateEscrowAccountFromT.Consummation;
            UpdateErrMsg();        
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(7425M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(0M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureNonEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureNonEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(675M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureMonthlyEscrowPayment_rep, errMsg + "sClosingDisclosureMonthlyEscrowPayment");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(0M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosurePropertyCostsFirstYear_rep, errMsg + "sClosingDisclosurePropertyCostsFirstYear");

            // TRID 2017, 1 year from consummation date test
            this.dataLoan.sTridTargetRegulationVersionT = TridTargetRegulationVersionT.TRID2017;
            UpdateErrMsg();

            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(9178.18M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(0M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureNonEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureNonEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(834.38M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureMonthlyEscrowPayment_rep, errMsg + "sClosingDisclosureMonthlyEscrowPayment");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(0M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosurePropertyCostsFirstYear_rep, errMsg + "sClosingDisclosurePropertyCostsFirstYear");
        }

        [Test]
        public void EscrowShortYearNoEscHOATest()
        {
            InitData();
            this.dataLoan.sLAmtCalc = 382500M;
            this.dataLoan.sProMIns = 159.38M;

            // gotten from Dev loan Case467072-EscrowShortYearNoEscHOA on KevinTest
            string housingExpenseJson = TestUtilities.ReadFile("TRID/Case467072-EscrowShortYearNoEscHOA-HousingExpense.json");
            this.dataLoan.CopysHousingExpenseJson(housingExpenseJson);

            // TRID 2017, 1 year from first payment date test
            this.dataLoan.sTridTargetRegulationVersionT = TridTargetRegulationVersionT.TRID2017;
            this.dataLoan.sTridClosingDisclosureCalculateEscrowAccountFromT = TridClosingDisclosureCalculateEscrowAccountFromT.FirstPaymentDate;
            UpdateErrMsg();

            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(7012.56M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(3000M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureNonEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureNonEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(584.38M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureMonthlyEscrowPayment_rep, errMsg + "sClosingDisclosureMonthlyEscrowPayment");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(0M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosurePropertyCostsFirstYear_rep, errMsg + "sClosingDisclosurePropertyCostsFirstYear");

            // TRID 2015, 1 year from first payment date test
            this.dataLoan.sTridTargetRegulationVersionT = TridTargetRegulationVersionT.TRID2015;
            UpdateErrMsg();

            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(5100M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(3000M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureNonEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureNonEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(425M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureMonthlyEscrowPayment_rep, errMsg + "sClosingDisclosureMonthlyEscrowPayment");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(0M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosurePropertyCostsFirstYear_rep, errMsg + "sClosingDisclosurePropertyCostsFirstYear");

            // TRID 2015, 1 year from consummation date test
            this.dataLoan.sTridClosingDisclosureCalculateEscrowAccountFromT = TridClosingDisclosureCalculateEscrowAccountFromT.Consummation;
            UpdateErrMsg();

            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(4675M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(2750M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureNonEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureNonEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(425M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureMonthlyEscrowPayment_rep, errMsg + "sClosingDisclosureMonthlyEscrowPayment");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(0M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosurePropertyCostsFirstYear_rep, errMsg + "sClosingDisclosurePropertyCostsFirstYear");

            // TRID 2017, 1 year from consummation date test
            this.dataLoan.sTridTargetRegulationVersionT = TridTargetRegulationVersionT.TRID2017;
            UpdateErrMsg();

            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(6428.18M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(2750M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureNonEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureNonEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(584.38M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureMonthlyEscrowPayment_rep, errMsg + "sClosingDisclosureMonthlyEscrowPayment");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(0M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosurePropertyCostsFirstYear_rep, errMsg + "sClosingDisclosurePropertyCostsFirstYear");
        }

        [Test]
        public void MIWithNoEscrowShortYearTest()
        {
            InitData();
            this.dataLoan.sLAmtCalc = 382500M;
            this.dataLoan.sProMIns = 159.38M;

            // gotten from Dev loan Case467072-MIWithNoEscrowShortYear on KevinTest
            string housingExpenseJson = TestUtilities.ReadFile("TRID/Case467072-MIWithNoEscrowShortYear-HousingExpense.json");
            this.dataLoan.CopysHousingExpenseJson(housingExpenseJson);

            // TRID 2017, 1 year from first payment date test
            this.dataLoan.sTridTargetRegulationVersionT = TridTargetRegulationVersionT.TRID2017;
            this.dataLoan.sTridClosingDisclosureCalculateEscrowAccountFromT = TridClosingDisclosureCalculateEscrowAccountFromT.FirstPaymentDate;
            UpdateErrMsg();

            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(1912.56M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(8100M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureNonEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureNonEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(159.38M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureMonthlyEscrowPayment_rep, errMsg + "sClosingDisclosureMonthlyEscrowPayment");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(0M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosurePropertyCostsFirstYear_rep, errMsg + "sClosingDisclosurePropertyCostsFirstYear");

            // TRID 2015, 1 year from first payment date test
            this.dataLoan.sTridTargetRegulationVersionT = TridTargetRegulationVersionT.TRID2015;
            UpdateErrMsg();

            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(0M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(0M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureNonEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureNonEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(0M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureMonthlyEscrowPayment_rep, errMsg + "sClosingDisclosureMonthlyEscrowPayment");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(8100M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosurePropertyCostsFirstYear_rep, errMsg + "sClosingDisclosurePropertyCostsFirstYear");

            // TRID 2015, 1 year from consummation date test
            this.dataLoan.sTridClosingDisclosureCalculateEscrowAccountFromT = TridClosingDisclosureCalculateEscrowAccountFromT.Consummation;
            UpdateErrMsg();

            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(0M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(0M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureNonEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureNonEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(0M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureMonthlyEscrowPayment_rep, errMsg + "sClosingDisclosureMonthlyEscrowPayment");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(7425M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosurePropertyCostsFirstYear_rep, errMsg + "sClosingDisclosurePropertyCostsFirstYear");

            // TRID 2017, 1 year from consummation date test
            this.dataLoan.sTridTargetRegulationVersionT = TridTargetRegulationVersionT.TRID2017;
            UpdateErrMsg();

            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(1753.18M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(7850M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureNonEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureNonEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(159.38M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureMonthlyEscrowPayment_rep, errMsg + "sClosingDisclosureMonthlyEscrowPayment");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(0M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosurePropertyCostsFirstYear_rep, errMsg + "sClosingDisclosurePropertyCostsFirstYear");
        }

        [Test]
        public void Disbursements1Test()
        {
            InitData();
            this.dataLoan.sLAmtCalc = 360000M;
            this.dataLoan.sProMIns = 0M;

            // gotten from Dev loan Case467072-Disbursements1 on KevinTest
            string housingExpenseJson = TestUtilities.ReadFile("TRID/Case467072-Disbursements1-HousingExpense.json");
            this.dataLoan.CopysHousingExpenseJson(housingExpenseJson);

            // TRID 2017, 1 year from first payment date test
            this.dataLoan.sTridTargetRegulationVersionT = TridTargetRegulationVersionT.TRID2017;
            this.dataLoan.sTridClosingDisclosureCalculateEscrowAccountFromT = TridClosingDisclosureCalculateEscrowAccountFromT.FirstPaymentDate;
            UpdateErrMsg();

            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(0M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(0M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureNonEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureNonEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(0M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureMonthlyEscrowPayment_rep, errMsg + "sClosingDisclosureMonthlyEscrowPayment");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(8100M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosurePropertyCostsFirstYear_rep, errMsg + "sClosingDisclosurePropertyCostsFirstYear");

            // TRID 2015, 1 year from first payment date test
            this.dataLoan.sTridTargetRegulationVersionT = TridTargetRegulationVersionT.TRID2015;
            UpdateErrMsg();

            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(0M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(0M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureNonEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureNonEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(0M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureMonthlyEscrowPayment_rep, errMsg + "sClosingDisclosureMonthlyEscrowPayment");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(8100M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosurePropertyCostsFirstYear_rep, errMsg + "sClosingDisclosurePropertyCostsFirstYear");

            // TRID 2015, 1 year from consummation date test
            this.dataLoan.sTridClosingDisclosureCalculateEscrowAccountFromT = TridClosingDisclosureCalculateEscrowAccountFromT.Consummation;
            UpdateErrMsg();

            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(0M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(0M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureNonEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureNonEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(0M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureMonthlyEscrowPayment_rep, errMsg + "sClosingDisclosureMonthlyEscrowPayment");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(7425M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosurePropertyCostsFirstYear_rep, errMsg + "sClosingDisclosurePropertyCostsFirstYear");

            // TRID 2017, 1 year from consummation date test
            this.dataLoan.sTridTargetRegulationVersionT = TridTargetRegulationVersionT.TRID2017;
            UpdateErrMsg();

            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(0M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(0M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureNonEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureNonEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(0M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureMonthlyEscrowPayment_rep, errMsg + "sClosingDisclosureMonthlyEscrowPayment");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(7400M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosurePropertyCostsFirstYear_rep, errMsg + "sClosingDisclosurePropertyCostsFirstYear");
        }

        [Test]
        public void Disbursements2MITest()
        {
            InitData();
            this.dataLoan.sLAmtCalc = 360000M;
            this.dataLoan.sProMIns = 100M;

            // gotten from Dev loan Case467072-Disbursements2MI on KevinTest
            string housingExpenseJson = TestUtilities.ReadFile("TRID/Case467072-Disbursements2MI-HousingExpense.json");
            this.dataLoan.CopysHousingExpenseJson(housingExpenseJson);

            // TRID 2017, 1 year from first payment date test
            this.dataLoan.sTridTargetRegulationVersionT = TridTargetRegulationVersionT.TRID2017;
            this.dataLoan.sTridClosingDisclosureCalculateEscrowAccountFromT = TridClosingDisclosureCalculateEscrowAccountFromT.FirstPaymentDate;
            UpdateErrMsg();

            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(1200M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(8100M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureNonEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureNonEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(100M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureMonthlyEscrowPayment_rep, errMsg + "sClosingDisclosureMonthlyEscrowPayment");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(0M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosurePropertyCostsFirstYear_rep, errMsg + "sClosingDisclosurePropertyCostsFirstYear");

            // TRID 2015, 1 year from first payment date test
            this.dataLoan.sTridTargetRegulationVersionT = TridTargetRegulationVersionT.TRID2015;
            UpdateErrMsg();

            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(0M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(0M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureNonEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureNonEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(0M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureMonthlyEscrowPayment_rep, errMsg + "sClosingDisclosureMonthlyEscrowPayment");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(8100M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosurePropertyCostsFirstYear_rep, errMsg + "sClosingDisclosurePropertyCostsFirstYear");

            // TRID 2015, 1 year from consummation date test
            this.dataLoan.sTridClosingDisclosureCalculateEscrowAccountFromT = TridClosingDisclosureCalculateEscrowAccountFromT.Consummation;
            UpdateErrMsg();

            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(0M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(0M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureNonEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureNonEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(0M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureMonthlyEscrowPayment_rep, errMsg + "sClosingDisclosureMonthlyEscrowPayment");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(7425M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosurePropertyCostsFirstYear_rep, errMsg + "sClosingDisclosurePropertyCostsFirstYear");

            // TRID 2017, 1 year from consummation date test
            this.dataLoan.sTridTargetRegulationVersionT = TridTargetRegulationVersionT.TRID2017;
            UpdateErrMsg();

            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(1100M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(7400M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureNonEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureNonEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(100M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureMonthlyEscrowPayment_rep, errMsg + "sClosingDisclosureMonthlyEscrowPayment");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(0M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosurePropertyCostsFirstYear_rep, errMsg + "sClosingDisclosurePropertyCostsFirstYear");
        }

        [Test]
        public void EscrowShortYearNoEscHOA_V2Test()
        {
            InitData();
            this.dataLoan.sLAmtCalc = 382500M;
            this.dataLoan.sProMIns = 159.38M;

            // gotten from Dev loan Case467072-EscrowShortYearNoEs_V2 on KevinTest
            string housingExpenseJson = TestUtilities.ReadFile("TRID/Case467072-EscrowShortYearNoEs_V2-HousingExpense.json");
            this.dataLoan.CopysHousingExpenseJson(housingExpenseJson);

            // TRID 2017, 1 year from first payment date test
            this.dataLoan.sTridTargetRegulationVersionT = TridTargetRegulationVersionT.TRID2017;
            this.dataLoan.sTridClosingDisclosureCalculateEscrowAccountFromT = TridClosingDisclosureCalculateEscrowAccountFromT.FirstPaymentDate;
            UpdateErrMsg();

            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(7012.56M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(4200M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureNonEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureNonEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(584.38M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureMonthlyEscrowPayment_rep, errMsg + "sClosingDisclosureMonthlyEscrowPayment");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(0M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosurePropertyCostsFirstYear_rep, errMsg + "sClosingDisclosurePropertyCostsFirstYear");

            // TRID 2015, 1 year from first payment date test
            this.dataLoan.sTridTargetRegulationVersionT = TridTargetRegulationVersionT.TRID2015;
            UpdateErrMsg();

            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(5100M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(4200M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureNonEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureNonEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(425M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureMonthlyEscrowPayment_rep, errMsg + "sClosingDisclosureMonthlyEscrowPayment");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(0M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosurePropertyCostsFirstYear_rep, errMsg + "sClosingDisclosurePropertyCostsFirstYear");

            // TRID 2015, 1 year from consummation date test
            this.dataLoan.sTridClosingDisclosureCalculateEscrowAccountFromT = TridClosingDisclosureCalculateEscrowAccountFromT.Consummation;
            UpdateErrMsg();

            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(4675M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(3850M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureNonEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureNonEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(425M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureMonthlyEscrowPayment_rep, errMsg + "sClosingDisclosureMonthlyEscrowPayment");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(0M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosurePropertyCostsFirstYear_rep, errMsg + "sClosingDisclosurePropertyCostsFirstYear");

            // TRID 2017, 1 year from consummation date test
            this.dataLoan.sTridTargetRegulationVersionT = TridTargetRegulationVersionT.TRID2017;
            UpdateErrMsg();

            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(6428.18M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(3950M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureNonEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureNonEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(584.38M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureMonthlyEscrowPayment_rep, errMsg + "sClosingDisclosureMonthlyEscrowPayment");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(0M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosurePropertyCostsFirstYear_rep, errMsg + "sClosingDisclosurePropertyCostsFirstYear");
        }

        [Test]
        public void EscrowShortYearNoEsc_V3Test()
        {
            InitData();
            this.dataLoan.sLAmtCalc = 382500M;
            this.dataLoan.sProMIns = 159.38M;

            // gotten from Dev loan Case467072-EscrowShortYearNoEs_V3 on KevinTest
            string housingExpenseJson = TestUtilities.ReadFile("TRID/Case467072-EscrowShortYearNoEs_V3-HousingExpense.json");
            this.dataLoan.CopysHousingExpenseJson(housingExpenseJson);

            // TRID 2017, 1 year from first payment date test
            this.dataLoan.sTridTargetRegulationVersionT = TridTargetRegulationVersionT.TRID2017;
            this.dataLoan.sTridClosingDisclosureCalculateEscrowAccountFromT = TridClosingDisclosureCalculateEscrowAccountFromT.FirstPaymentDate;
            UpdateErrMsg();

            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(7012.56M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(4200M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureNonEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureNonEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(584.38M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureMonthlyEscrowPayment_rep, errMsg + "sClosingDisclosureMonthlyEscrowPayment");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(0M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosurePropertyCostsFirstYear_rep, errMsg + "sClosingDisclosurePropertyCostsFirstYear");

            // TRID 2015, 1 year from first payment date test
            this.dataLoan.sTridTargetRegulationVersionT = TridTargetRegulationVersionT.TRID2015;
            UpdateErrMsg();

            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(5100M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(4200M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureNonEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureNonEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(425M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureMonthlyEscrowPayment_rep, errMsg + "sClosingDisclosureMonthlyEscrowPayment");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(0M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosurePropertyCostsFirstYear_rep, errMsg + "sClosingDisclosurePropertyCostsFirstYear");

            // TRID 2015, 1 year from consummation date test
            this.dataLoan.sTridClosingDisclosureCalculateEscrowAccountFromT = TridClosingDisclosureCalculateEscrowAccountFromT.Consummation;
            UpdateErrMsg();

            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(4675M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(3850M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureNonEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureNonEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(425M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureMonthlyEscrowPayment_rep, errMsg + "sClosingDisclosureMonthlyEscrowPayment");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(0M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosurePropertyCostsFirstYear_rep, errMsg + "sClosingDisclosurePropertyCostsFirstYear");

            // TRID 2017, 1 year from consummation date test
            this.dataLoan.sTridTargetRegulationVersionT = TridTargetRegulationVersionT.TRID2017;
            UpdateErrMsg();

            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(6428.18M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(3950M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureNonEscrowedPropertyCostsFirstYear_rep, errMsg + "sClosingDisclosureNonEscrowedPropertyCostsFirstYear");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(584.38M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosureMonthlyEscrowPayment_rep, errMsg + "sClosingDisclosureMonthlyEscrowPayment");
            Assert.AreEqual(this.dataLoan.m_convertLos.ToMoneyString(0M, FormatDirection.ToRep), this.dataLoan.sClosingDisclosurePropertyCostsFirstYear_rep, errMsg + "sClosingDisclosurePropertyCostsFirstYear");
        }
    }
}
