﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using DataAccess;
using LendersOffice.Security;
using LendOSolnTest.Common;
using LendersOffice.Reports;
using LendersOffice.QueryProcessor;
using ConfigSystem;

namespace LendOSolnTest.ConfigSystem
{
    [TestFixture]
    public class ParameterAccessTest
    {
        private AbstractUserPrincipal m_userPrinicpal = LoginTools.LoginAs(TestAccountType.Corporate_LoanOfficer);

        [TestFixtureSetUp]
        public void Setup()
        {
        }

        [Test]
        public void CustomVariableAddTest()
        {
            ParameterReporting reporting = ParameterReporting.RetrieveByBrokerId(m_userPrinicpal.BrokerId);
            // Clear custom variables
            reporting.UpdateCustomList(new CustomVariable[]{});

            // Take initial count
            int firstCount = reporting.Parameters.Count();
            
            // Add three custom variables
            reporting.UpdateCustomList(new CustomVariable[] {
                new CustomVariable("1003 Complete", CustomVariable.E_Scope.Lender),
                new CustomVariable("1008 Complete", CustomVariable.E_Scope.Lender),
                new CustomVariable("HUD-1 Complete", CustomVariable.E_Scope.Lender)});

            // Take new count
            int secondCount = reporting.Parameters.Count();
            
            // Count custom variables
            int customVarCount = reporting.Parameters.Count(p => p.Type == SecurityParameter.SecurityParameterType.Custom);

            // Clear custom variables
            reporting.UpdateCustomList(new CustomVariable[] { });


            // Final total count
            int finalCount = reporting.Parameters.Count();

            //removing support for deleting custom variables;
            //Assert.That(firstCount, Is.EqualTo(finalCount),      "Adding then removing custom variables changed parameter count.");
            Assert.That(secondCount, Is.EqualTo(firstCount + 3), "Failed to add custom variables to parameter list.");
            Assert.That(customVarCount, Is.EqualTo(3),           "Custom variable count not equal to custom variables added.");
            //Assert.That(clearCustomVarCount, Is.EqualTo(0),      "Clearing custom variable list did not remove all custom variables.");
        }
        //[Test] // 12/01/10 mf. Disable test pending further implementation of OPM 57805
        public void EnumChangeTest()
        {
            // OPM 57805. mf 11/17/10.  This test is meant to alert SDE/DS when either:
            //      A) A new value has been added to an enum field in the custom reporting schema
            //      B) A new enum field has been added to the custom reporting schema
            //
            // In both cases SDE shoudl update the list below to pass the test.
            // HOWEVER, if case A) occurs, you must NOTIFY THE WORKFLOW DEPLOYMENT SPECIALISTS!!!
            // They have to know that a new potential value is possible for a field at least 3 days 
            // before the change is released to production.  We do this so the DS can review
            // applicable rules to make sure they take the new enum value into account.

            #region Known Enum Parameter List - Master list.
            var knownEnumParameters = new[]
            {
             new { ParamId = "ActAsRateLocked", EnumValues = new string[]{ "NotAffect", "AllowRead", "AllowWrite" } }
            , new { ParamId = "Lien", EnumValues = new string[]{ "First", "Second", "Other" } }
            , new { ParamId = "Scope", EnumValues = new string[]{ "In Scope", "Non-Duty" } }
            , new { ParamId = "aBGender", EnumValues = new string[]{ "Male", "Female", "Not Applicable", "Unfurnished", "Left Blank" } }
            , new { ParamId = "aBHispanicT", EnumValues = new string[]{ "Hispanic", "Not Hispanic", "Left Blank" } }
            , new { ParamId = "aBRaceT", EnumValues = new string[]{ "American Indian", "Asian", "Black", "Hispanic", "White", "Other", "Not Furnished", "Not Applicable", "Left Blank" } }
            , new { ParamId = "aCGender", EnumValues = new string[]{ "Male", "Female", "Not Applicable", "Unfurnished", "Left Blank" } }
            , new { ParamId = "aCHispanicT", EnumValues = new string[]{ "Hispanic", "Not Hispanic", "Left Blank" } }
            , new { ParamId = "aCRaceT", EnumValues = new string[]{ "American Indian", "Asian", "Black", "Hispanic", "White", "Other", "Not Furnished", "Not Applicable", "Left Blank" } }
            , new { ParamId = "sHmdaPropT", EnumValues = new string[]{ "1 - 4 Family", "Manufactured Housing", "Multi-Family", "Left Blank" } }
            , new { ParamId = "aIntrvwrMethodT", EnumValues = new string[]{ "Face To Face", "By Mail", "By Telephone", "Internet", "Left Blank" } }
            , new { ParamId = "sHmdaLienT", EnumValues = new string[]{ "Left Blank", "First Lien", "Subordinate Lien", "Not Secured By Lien", "Not Applicable" } }
            , new { ParamId = "sHmdaPreapprovalT", EnumValues = new string[]{ "Left Blank", "Preapproval Requested", "Preapproval Not Requested", "Not Applicable" } }
            , new { ParamId = "sHmdaPurchaser2004T", EnumValues = new string[]{ "Loan Was Not Sold", "FNMA", "GNMA", "FHLMC", "FarmerMac", "Private Securitization", "Commercial Bank", "Life Insurance Co., Finance Co., etc.", "Affiliate Institution", "Other", "Left Blank" } }
            , new { ParamId = "sInvestorLockCommitmentT", EnumValues = new string[]{ "Best Effort", "Mandatory", "Hedged", "Securitized" } }
            , new { ParamId = "sInvestorLockRateLockStatusT", EnumValues = new string[]{ "Not Locked", "Locked" } }
            , new { ParamId = "sFinMethT", EnumValues = new string[]{ "ARM", "Fixed", "Graduated Payment" } }
            , new { ParamId = "sFannieDocT", EnumValues = new string[]{ "Alternative", "Full", "Leave Blank", "No Documentation", "Reduced", "Streamlined Refinanced", "No Ratio", "Limited Documentation", "No Income, No Employment and No Assets on 1003", "No Income and No Assets on 1003", "No Assets on 1003", "No Income and No Employment on 1003", "No Income on 1003", "No Verification of Stated Income, Employment or Assets", "No Verification of Stated Income or Assets", "No Verification of Stated Assets", "No Verification of Stated Income or Employment", "No Verification of Stated Income", "Verbal Verification of Employment (VVOE)", "One paystub", "One paystub and VVOE", "One paystub and one W-2 and VVOE or one yr 1040" } }
            , new { ParamId = "sArmIndexT", EnumValues = new string[]{ "", "Weekly Average CMT", "Monthly Average CMT", "Weekly Average TAAI", "Weekly Average TAABD", "Weekly Average SMTI", "Daily CD Rate", "Weekly Average CD Rate", "Weekly Average Prime Rate", "T-Bill Daily Value", "11th District COF", "National Monthly Median Cost of Funds", "Wall Street Journal LIBOR", "Fannie Mae LIBOR" } }
            , new { ParamId = "sFreddieArmIndexT", EnumValues = new string[]{ "", "One Year Treasury", "Three Year Treasury", "Six Month Treasury", "11th District Cost Of Funds", "National Monthly Median Cost Of Funds", "LIBOR", "Other" } }
            , new { ParamId = "sLienPosT", EnumValues = new string[]{ "First", "Second" } }
            , new { ParamId = "sLPurposeT", EnumValues = new string[]{ "Construction", "Construction Permanent", "Other", "Purchase", "Refinance", "Refinance Cash-out", "FHA Streamlined Refinance", "VA IRRRL" } }
            , new { ParamId = "sLT", EnumValues = new string[]{ "Conventional", "FHA", "Other", "USDA Rural", "VA" } }
            , new { ParamId = "sLoanOfficerRateLockStatusT", EnumValues = new string[]{ "Not Selected", "Broken", "Locked" } }
            , new { ParamId = "sRateLockStatusT", EnumValues = new string[]{ "Locked", "Not Locked", "Lock Requested" } }
            , new { ParamId = "sStatusLoanCategoryT", EnumValues = new string[]{ "Active Loan", "Inactive Loan", "Other Loan" } }
            , new { ParamId = "sStatusProgressT", EnumValues = new string[]{ "Lead New", "Loan Open", "Pre-qual", "Pre-approved", "Registered", "Approved", "Loan Submitted", "In Underwriting", "Docs Out", "Loan Shipped", "Funded", "Recorded", "Loan On-hold", "Loan Other", "Lead Other", "Loan Closed", "Loan Suspended", "Loan Canceled", "Loan Denied", "Lead Declined", "Lead Canceled", "Clear to Close", "Processing", "Final Underwriting", "Docs Back", "Funding Conditions", "Final Docs", "Loan Sold" } }
            , new { ParamId = "sStatusT", EnumValues = new string[]{ "Lead Canceled", "Lead Declined", "Lead New", "Lead Other", "Approved", "Loan Canceled", "Loan Closed", "Docs Out", "Funded", "Loan On-hold", "Loan Open", "Loan Other", "Pre-approved", "Pre-qual", "Recorded", "Loan Denied", "Registered", "Loan Suspended", "Loan Submitted", "In Underwriting", "Loan Shipped", "Clear to Close", "Loan Web Consumer", "Processing", "Final Underwriting", "Docs Back", "Funding Conditions", "Final Docs", "Loan Sold" } }
            , new { ParamId = "sSecondStatus", EnumValues = new string[]{ "None", "Investor Funds Disbursed", "Loan Funded", "Loan Sold", "Loan Reconciled" } }
            , new { ParamId = "sProd3rdPartyUwResultT", EnumValues = new string[]{ "None/Not Submitted", "DU Approve/Eligible", "DU Approve/Ineligible", "DU Refer/Eligible", "DU Refer/Ineligible", "DU Refer with Caution/Eligible", "DU Refer with Caution/Ineligible", "DU EA I/Eligible", "DU EA II/Eligible", "DU EA III/Eligible", "LP Accept/Eligible", "LP Accept/Ineligible", "LP Caution/Eligible", "LP Caution/Ineligible", "LP A- Level 1", "LP A- Level 2", "LP A- Level 3", "LP A- Level 4", "LP A- Level 5", "LP Refer", "Out of Scope", "TOTAL Approve/Eligible", "TOTAL Approve/Ineligible", "TOTAL Refer/Eligible", "TOTAL Refer/Ineligible" } }
            , new { ParamId = "aProdBCitizenT", EnumValues = new string[]{ "US Citizen", "Permanent Resident", "Non-permanent Resident", "Non-Resident Alien (Foreign National)" } }
            , new { ParamId = "sProdDocT", EnumValues = new string[]{ "Full Document", "Alt", "Lite", "NINA", "No Ratio", "NISA", "NIV (SISA)", "NIV (SIVA)", "VISA", "No Doc", "No Doc Verif Assets", "VINA", "Streamline" } }
            , new { ParamId = "sProdSpStructureT", EnumValues = new string[]{ "Attached", "Detached" } }
            , new { ParamId = "sProdSpT", EnumValues = new string[]{ "SFR", "2 Units", "3 Units", "4 Units", "PUD", "Commercial", "Condo", "Co-Op", "Manufactured", "Mixed Use", "Attached PUD", "Modular", "Rowhouse" } }
            , new { ParamId = "Role", EnumValues = new string[]{ "Accountant", "Administrator", "Call Center Agent", "Closer", "Consumer", "Funder", "Lender Account Executive", "Loan Officer", "Loan Opener", "Lock Desk", "Manager", "Processor", "Real Estate Agent", "Shipper", "Underwriter", "Loan Officer (PML)", "Broker Processor (PML)", "Administrator (PML)" } }
            , new { ParamId = "aOccT", EnumValues = new string[]{ "Investment", "Primary Residence", "Secondary Residence" } }
            };
            #endregion

            ParameterReporting reporting = ParameterReporting.RetrieveByBrokerId(m_userPrinicpal.BrokerId);

            foreach (SecurityParameter.EnumeratedParmeter param in reporting.Parameters.Where(p => p is SecurityParameter.EnumeratedParmeter))
            {
                
                if ( param.CategoryName.Equals("Groups", StringComparison.OrdinalIgnoreCase) )
                    continue; // Group enum values vary by lender.

                SecurityParameter.EnumeratedParmeter enumParam = param as SecurityParameter.EnumeratedParmeter;

                bool paramExists = false;
                foreach (var knownParam in knownEnumParameters)
                {
                    if (knownParam.ParamId.Equals(param.Id,StringComparison.OrdinalIgnoreCase))
                    {
                        paramExists = true;
                        foreach (SecurityParameter.EnumMapping val in param.EnumMapping)
                        {
                            if (knownParam.EnumValues.Contains(val.FriendlyValue, StringComparer.OrdinalIgnoreCase) == false)
                            {
                                Assert.Fail(string.Format("{0} ({1}) might have a new potential value: \"{2}\". Add it to the list in EnumChangeTest and LET THE DEPLOYMENT SPECIALISTS KNOW that this new potential value exists! (see case 57805)", param.Id, param.FriendlyName, val.FriendlyValue));
                            }
                        }
                        
                        break;
                    }
                }
                if (paramExists == false)
                {
                    Assert.Fail(string.Format("{0} ({1}) does not exist in known list of enum config parameters in EnumChangeTest.  Please add so we can track its enum values.", param.Id, param.FriendlyName));
                }
            }
        }
    }
}
