﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConfigSystem.DataAccess;
using ConfigSystem;

namespace LendOSolnTest.ConfigSystem
{
    internal class NoOpMockConfigSystemRepository : IConfigRepository
    {

        #region IConfigRepository Members

        public BasicConfigData LoadDraft(Guid organizationId)
        {
            return new BasicConfigData();
        }

        public FilterSet LoadFilters(Guid organizationId)
        {
            return new FilterSet();
        }

        public ReleaseConfigData LoadActiveRelease(Guid organizationId)
        {
            return new ReleaseConfigData();
        }

        public long SaveReleaseConfig(Guid organizationId, SystemConfig config, Guid userId)
        {
            return 0L;
        }

        public void SaveFilters(Guid organizationId, FilterSet filters)
        {
            //no op
        }

        public void SaveDraftConfig(Guid organizationId, SystemConfig config)
        {
            //no op
        }


        public Nullable<DateTime> GetLastModifiedDateForActiveRelease(Guid organizationId)
        {
            return null;
        }


        public IEnumerable<ReleaseConfigData> GetActiveReleases()
        {
            return new List<ReleaseConfigData>();
        }

        public IEnumerable<BasicConfigData> GetDrafts()
        {
            return new List<BasicConfigData>();
        }

        public IEnumerable<ReleaseConfigData> GetAllReleasesFor(Guid orgId)
        {
            return new List<ReleaseConfigData>();
        }

        public long GetConfigVersion(Guid organizationId)
        {
            return -1L;
        }

        public long? GetActiveReleaseId(Guid brokerId)
        {
            return -1L;
        }

        #endregion
    }
}
