﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using ConfigSystem.DataAccess;
using LendersOffice.ConfigSystem;
using ConfigSystem;
using System.Xml.Linq;
using LendOSolnTest.Common;
using System.Xml;
using System.IO;
using DataAccess;
using CommonProjectLib.Logging;
using LendersOffice.Constants;

namespace LendOSolnTest.ConfigSystem
{
    [TestFixture]
    public class ValidationConfigRepositoryTest
    {
        [TestFixtureSetUp]
        public void Setup()
        {
        }

        private class TestCase
        {
            public SystemConfig config;
            public string Result;
            public string Id;
            public int ConditionErrorCount = -1;
            public int ConstraintErrorCount = -1;

            public TestCase(string xml, string result, string id, int count, int constraintErrorCount)
            {
                config = new SystemConfig(xml);
                Result = result;
                Id = id;
                ConditionErrorCount = count;
                ConstraintErrorCount = constraintErrorCount;
                
            }

        }

        [Test]
        public void TestConfigXml()
        {
            foreach (TestCase testCase in GetTestCases())
            {
                SystemConfigValidator v = new SystemConfigValidator(Guid.Empty, testCase.config);
                if (testCase.Result == "Fail")
                {

                    Assert.That(v.IsValid(), Is.False, "Config ID = " + testCase.Id + String.Join(",", v.ConditionErrors.Union(v.ConstraintErrors).Union(v.CustomVariableErrors).ToArray()));

                    if (testCase.ConditionErrorCount != -1)
                    {
                        Assert.That(v.ConditionErrorCount, Is.EqualTo(testCase.ConditionErrorCount), "Config ID = " + testCase.Id + String.Join(",", v.ConditionErrors.Union(v.ConstraintErrors).Union(v.CustomVariableErrors).ToArray()));
                    }

                    if (testCase.ConstraintErrorCount != -1)
                    {
                        Assert.That(v.ConstraintErrorCount, Is.EqualTo(testCase.ConstraintErrorCount), "Config ID = " + testCase.Id + String.Join(",", v.ConditionErrors.Union(v.ConstraintErrors).Union(v.CustomVariableErrors).ToArray()));
                    }
                }
                else if (testCase.Result == "Pass")
                {
                    Assert.That(v.IsValid(), Is.True, "Config ID = " + testCase.Id + String.Join(",", v.ConditionErrors.Union(v.ConstraintErrors).Union(v.CustomVariableErrors).ToArray()));
                }
                else
                {
                    Assert.Fail(testCase.Result + " not found");
                }
            }
        }

        //[Test]
        //public void ValidateSystemRules()
        //{
        //    System.Reflection.Assembly assembly = typeof(SystemConfigValidator).Assembly;

        //    using (Stream s = assembly.GetManifestResourceStream("LendersOffice.ConfigSystem.GlobalSystemConfig.xml"))
        //    using (XmlReader stream = XmlReader.Create(s))
        //    {
        //        XDocument doc = XDocument.Load(stream, LoadOptions.None);
        //        SystemConfig config = new SystemConfig(doc.ToString());

        //        SystemConfigValidator v = new SystemConfigValidator(Guid.Empty, config);

        //        if (v.IsValid() == false)
        //        {
        //            PrintErrors(v);
        //        }
        //        Assert.That(v.IsValid(), Is.True);      
        //    }
        //}


        private void PrintErrors(SystemConfigValidator v)
        {
            if (v.IsValid())
            {
                return;
            }
            StringBuilder b = new StringBuilder();
            b.AppendLine("Condition Error Count " + v.ConditionErrorCount);
            int count = 0;
            foreach (string error in v.ConditionErrors)
            {
                b.AppendLine("\t" + count + ". " + error);
                count++;
            }
            b.AppendLine("Constraint Error Count " + v.ConstraintErrorCount);
            count = 0;
            foreach (string error in v.ConstraintErrors)
            {
                b.AppendLine("\t" + count + ". " + error);
                count++;
            }
            count = 0;
            b.AppendLine("Custom Variable Error Count " + v.CustomVariableErrorCount);
            foreach (string error in v.CustomVariableErrors)
            {
                b.AppendLine("\t" + count + ". " + error);
                count++;

            }

            Tools.LogError(b.ToString());
        }
        //[Test]
        //public void LoadSystemConfig()
        //{
        //    WorkflowSystemConfigModel c = new WorkflowSystemConfigModel(Guid.Empty, true);
        //    Assert.That(c.CanSave, Is.False);
        //}
        /// <summary>
        /// Loads configurations from  TestData/ConfigSystem/ValidationTestData.xml 
        /// The Xml is composed of multiple configurations 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private IEnumerable<TestCase> GetTestCases()
        {
            using (FileStream fs = TestUtilities.GetFileStream("ConfigSystem/ValidationTestData.xml"))

            using (XmlReader reader = XmlReader.Create(fs))
            {
                XDocument doc = XDocument.Load(reader);

                IEnumerable<XElement> y = from el in doc.Element("SystemConfigs").Elements("Config")
                                          select el;


                foreach (XElement configElement in y)
                {
                    string result = configElement.Element("Result").Value;
                    string config = configElement.Element("SystemConfig").ToString();
                    string configid = configElement.Attribute("id").Value;
                    XElement e = configElement.Element("ConditionErrorCount");
                    XElement p = configElement.Element("ConstraintErrorCount");
                    int ccCount = -1;
                    int cCount = -1;
                    if (e != null)
                    {
                        cCount = int.Parse(e.Value);
                    }
                    if (p != null)
                    {
                        ccCount = int.Parse(p.Value);
                    }
                    yield return new TestCase(config, result, configid, cCount, ccCount);
                }
            }
        }


    }
}
