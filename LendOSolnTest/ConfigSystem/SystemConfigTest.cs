﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using CommonProjectLib.Logging;
using ConfigSystem;
using System.Xml.Linq;
using System.Xml;

namespace LendOSolnTest.ConfigSystem
{
    [TestFixture]
    public class SystemConfigTest
    {
        private string TestXML1
        {
            get
            {
                string xmlQuery = @"<?xml version='1.0' encoding='utf-8' standalone='yes'?>
                <SystemConfig>
					<ConditionSet>
					    <Condition id='aa187c95-f1f9-4310-b931-c66b0b37e33f'>
			                <ParameterSet>
                                <Parameter name='sLAmt' function='Between' type='Float'>
					                <Value>100,000</Value>
					                <Value>200,000</Value>
				                </Parameter>
                                <Parameter name='sStatusT' function='In' type='Float'>
					                <Value desc='closed'>3</Value>
					                <Value desc='deleted'>4</Value>
				                </Parameter>
				                <Parameter name='BranchGroup' sensitive='true' function='In' type='String' >
					                <Value> AwesomeBranch </Value> 
				                </Parameter>
                            </ParameterSet>
                            <ApplicableSystemOperationSet>
				                <SystemOperation>ReadLoan</SystemOperation>
				                <SystemOperation>RunPmlToStep3</SystemOperation>
			                </ApplicableSystemOperationSet>
                            <Notes>
				                <![CDATA[XML validator is forcing me to add this against my will]]>
			                </Notes>
                        </Condition>
		                <Condition id='08ff920d-8013-4f25-8307-6eb5555d1c42'>
                            <ParameterSet>
				                <Parameter name='Role' function='In' type='Int'>
					                <Value desc='LoanRep(p)'>12</Value>
					                <Value desc='BrokerProcessor'>10</Value>
					                <Value desc='Admin(P)'>20</Value>
				                </Parameter>
				                <Parameter name='sLAmt' function='Between' type='Float'>
					                <Value>100,000</Value>
					                <Value>200,000</Value>
				                </Parameter>
				                <Parameter name='sStatusT' function='In' type='Int'>
					                <Value desc='Opened'>5</Value>
					                <Value desc='PreAprroved'>6</Value>
				                </Parameter>
				                <Parameter name='UserGroup' sensitive='true' function='In' type='String' >
					                <Value>Retail</Value>
				                </Parameter>
				                <Parameter name='Is1003Complete' function='In' type='Bool'>
					                <Value>True</Value>
				                </Parameter>
			                </ParameterSet>
			                <ApplicableSystemOperationSet>
				                <SystemOperation>ReadLoan</SystemOperation>
				                <SystemOperation>RunPmlToStep3</SystemOperation>
			                </ApplicableSystemOperationSet>
			                <Notes>
				                <![CDATA[Lender asked for it on 8/23/2010]]>
			                </Notes>
		                </Condition>
                        <Condition id='1a3f545c-9211-4a1c-beb5-8e663c02547c'>
                            <ParameterSet>
                                <Parameter name='Role' function='In' type='Int'>
                                    <Value desc='Administrator'>1</Value>
                                </Parameter>
                            </ParameterSet>
                            <ApplicableSystemOperationSet>
                                <SystemOperation>ReadLoan</SystemOperation>
                                <SystemOperation>WriteLoan</SystemOperation>
                                <SystemOperation>
                                    WriteField
                                    <FieldSet>
                                        <Field friendlyName='ActAsRateLocked' id='ActAsRateLocked' category='' clearable='true' updateable='false' />
                                        <Field friendlyName='AlwaysTrue' id='AlwaysTrue' category='' clearable='true' updateable='false' />
                                        <Field friendlyName='IsBranchAllowLOEditLoanUntilUW' id='IsBranchAllowLOEditLoanUntilUW' category='' clearable='true' updateable='false' />
                                        <Field friendlyName='AccessClosedLoan' id='AccessClosedLoan' category='' clearable='true' updateable='false' />
                                    </FieldSet>
                                </SystemOperation>
                            </ApplicableSystemOperationSet>
                            <ConditionFailureMessage><![CDATA[denial message]]></ConditionFailureMessage>
                            <Notes><![CDATA[heahaheah]]></Notes>
                        </Condition>
                    </ConditionSet>	
                    <ConstraintSet>
	                    <Constraint id='bb257744-4f39-4992-b2ef-4c4991bd0add'>
		                    <ParameterSet>
			                    <Parameter name='sLAmt' function='Between' type='Float'>
				                    <Value>300,000</Value>
				                    <Value>400,000</Value>
			                    </Parameter>
			                    <Parameter name='sStatusT' function='In' type='Int'>
				                    <Value desc='closed'>5</Value>
				                    <Value desc='deleted'>6</Value>
			                    </Parameter>
			                    <Parameter name='BranchGroup' sensitive='true' function='In' type='String' >
				                    <Value> NotAwesomeBranch </Value> 
			                    </Parameter>
		                    </ParameterSet>
		                    <ApplicableSystemOperationSet>
			                    <SystemOperation>ReadLoan</SystemOperation>
			                    <SystemOperation>RunPmlToStep3</SystemOperation>
		                    </ApplicableSystemOperationSet>
                            <Notes>
				                <![CDATA[XML validator is forcing me to add this against my will]]>
			                </Notes>
	                    </Constraint>
	                </ConstraintSet>
                    <CustomVariableSet>
		                <CustomVariable id='a6723f1a-f397-464f-b980-8aa8f31d0427' name='Is1003Complete' scope='Lender'>
			                <ParameterSet>
				               
				                <Parameter name='sFinalLoanAmt' function='IsGreaterThan' type='Float'>	
					                <Value>120,000</Value>
				                </Parameter>
				                <Parameter name='sFinalLoanAmt' function='IsGreaterThanVar' type='Float'>
					                <Value>sLAmt</Value>
				                </Parameter>
			                </ParameterSet>
		                </CustomVariable>
	                </CustomVariableSet>
			    </SystemConfig>";

                return xmlQuery;
            }
        }
        
        [TestFixtureSetUp]
        public void Setup()
        {
            LogManager.ClearAll();
        }

        [Test]
        public void TestEmptyConfig()
        {
            SystemConfig config = new SystemConfig();

            Assert.That(config.Conditions, Is.Not.Null);
            Assert.That(config.Constraints, Is.Not.Null);
            Assert.That(config.CustomVariables, Is.Not.Null);


            XDocument configXml = new XDocument(
                new XDeclaration("1.0", "utf-16", "yes"),
                new XElement("SystemConfig",
                new XElement("ConditionSet"),
                new XElement("ConstraintSet"),
                new XElement("CustomVariableSet"))
            );


            LogManager.Info(config.ToString());
            XDocument doc = XDocument.Parse(config.ToString());
            Assert.That(XDocument.DeepEquals(doc, configXml), Is.True);
        }


        [Test]
        [ExpectedException(typeof(XmlException))]
        public void TestBadXml()
        {
            XDocument configXml = new XDocument(
                 new XDeclaration("1.0", "utf-16", "yes"),
                 new XElement("asd",
                 new XElement("ss"),
                 new XElement("dsa"),
                 new XElement("a"))
            );

            SystemConfig config = new SystemConfig(configXml.ToString());
            
        }

        [Test]
        public void TestEmptyConfigRoundTest()
        {

            XDocument configXml = new XDocument(
                new XDeclaration("1.0", "utf-16", "yes"),
                new XElement("SystemConfig",
                new XElement("ConditionSet"),
                new XElement("ConstraintSet"),
                new XElement("CustomVariableSet"))
            );

            SystemConfig config = new SystemConfig(configXml.ToString());

            Assert.That(config.Conditions, Is.Not.Null);
            Assert.That(config.Constraints, Is.Not.Null);
            Assert.That(config.Restraints, Is.Not.Null);
            Assert.That(config.CustomVariables, Is.Not.Null);

            XDocument doc = XDocument.Parse(config.ToString());
            Assert.That(XDocument.DeepEquals(doc, configXml), Is.True);
        }

        [Test]
        public void TestRoundTripWithData()
        {
            SystemConfig sysConfig = new SystemConfig(TestXML1);
            XDocument oldDoc = XDocument.Parse(TestXML1);
            XDocument newDoc = XDocument.Parse(sysConfig.ToString());
            Assert.That(XDocument.DeepEquals(oldDoc, newDoc), Is.True);
        }

   

    }
}
