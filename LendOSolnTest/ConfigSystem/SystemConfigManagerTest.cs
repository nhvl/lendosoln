﻿namespace LendOSolnTest.ConfigSystem
{
    using System;
    using System.Linq;

    using global::ConfigSystem;
    using global::ConfigSystem.DataAccess;
    using DataAccess;
    using LendersOffice.ConfigSystem;
    using LendersOffice.Security;
    using LendOSolnTest.Common;
    using NUnit.Framework;

    [TestFixture]
    public class DBConfigRepositoryTest
    {
        AbstractUserPrincipal x_Principal = LoginTools.LoginAs(TestAccountType.LoanOfficer);
        private IConfigRepository repo;
        private ReleaseConfigData m_cachedConfig;

        [TestFixtureSetUp]                  
        public void Setup()
        {

            repo = ConfigHandler.GetUncachedRepository(x_Principal.BrokerId);
            
            m_cachedConfig = repo.LoadActiveRelease(x_Principal.BrokerId);

        }


        [Test]
        public void SaveEmptyDraftConfig()
        {
            repo.SaveDraftConfig(x_Principal.BrokerId, new SystemConfig());
            BasicConfigData data = repo.LoadDraft(x_Principal.BrokerId);
            Assert.That( data.Configuration.Conditions.Count(), Is.EqualTo(0));
            Assert.That( data.Configuration.CustomVariables.Count(), Is.EqualTo(0));
            Assert.That( data.Configuration.Constraints.Count(), Is.EqualTo(0));
        }

        [Test]
        public void LoadEmptyDraftConfig()
        {
            Guid nonExistantBrokerId = Guid.NewGuid(); 
            BasicConfigData data = repo.LoadDraft(nonExistantBrokerId);
            Assert.That(data.Configuration.Conditions.Count(), Is.EqualTo(0));
            Assert.That(data.OrganizationId, Is.EqualTo(nonExistantBrokerId));
        }



        [Test]
        public void LoadLatestReleasetest()
        {
            repo.SaveReleaseConfig(x_Principal.BrokerId, new SystemConfig(), x_Principal.UserId);
            //make sure one exist first

            ReleaseConfigData data = repo.LoadActiveRelease(x_Principal.BrokerId);

            System.Threading.Thread.Sleep(TimeSpan.FromSeconds(1));
            repo.SaveReleaseConfig(x_Principal.BrokerId, new SystemConfig(), x_Principal.UserId);

            ReleaseConfigData data2 = repo.LoadActiveRelease(x_Principal.BrokerId);
            Assert.That(data.ReleaseDate.HasValue);
            Assert.That(data2.ReleaseDate.HasValue);
            Assert.That(data.ReleaseDate, Is.LessThan(data2.ReleaseDate));
        }

        [Test]
        public void EnsureNewReleaseDateIsBeingReturnedAfterASave()
        {
            repo.SaveReleaseConfig(x_Principal.BrokerId, new SystemConfig(), x_Principal.UserId);

            DateTime? initialReleaseDate = repo.GetLastModifiedDateForActiveRelease(x_Principal.BrokerId);

            Assert.That(initialReleaseDate, Is.Not.Null, "Last MOdified dATE CANNOT be null we just added it!");

            System.Threading.Thread.Sleep(TimeSpan.FromSeconds(5)); //mAKE SURE the times are not equal for some reason
            repo.SaveReleaseConfig(x_Principal.BrokerId, new SystemConfig(), x_Principal.UserId);

            DateTime? newReleaseDate = repo.GetLastModifiedDateForActiveRelease(x_Principal.BrokerId);

            Assert.That(newReleaseDate, Is.Not.Null);
            Assert.That(newReleaseDate, Is.GreaterThan(initialReleaseDate));
        }

        [Test]

        public void EnsureDateisNullForGuidEmpty()
        {
            DateTime? d = repo.GetLastModifiedDateForActiveRelease(Guid.Empty);

            Assert.That(d, Is.Null);
        }


        [TestFixtureTearDown]
        public void Remove()
        {
            if (m_cachedConfig.Configuration != null)
            {
                repo.SaveReleaseConfig(x_Principal.BrokerId, m_cachedConfig.Configuration, x_Principal.UserId);
            }
        }



    }
}
