﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using ConfigSystem;

namespace LendOSolnTest.ConfigSystem
{



    internal class SetGenerator
    {
        private readonly char[] DelimiterChars = { ',' };

        private ConditionSet m_conditions = new ConditionSet();
        private ConstraintSet m_constraints = new ConstraintSet();
        private SystemOperationSet m_operationSet = new SystemOperationSet();

        private Condition m_currentCondition = new Condition();
        private Constraint m_currentConstraint = new Constraint();

        private bool m_composingCondition = true;


        /// <summary>
        /// Declare operations to be shared for the next condition and constraint
        /// </summary>
        /// <param name="operations">comma separated list</param>
        internal void Operations( string operations )
        {
            m_operationSet = new SystemOperationSet();

            string[] parsed = operations.Split(DelimiterChars);
            for (int i = 0; i < parsed.Length; ++i)
                m_operationSet.Add(new SystemOperation(parsed[i].Trim()));

        }

        /// <summary>
        /// Open a new condition and ready for new parameters to be added
        /// </summary>
        /// <param name="operations">comma seperated list</param>
        internal void Condition(string operations)
        {
            Operations(operations);
            Condition();
        }

        /// <summary>
        /// Open a new condition and ready for new parameters to be added
        /// </summary>
        internal void Condition()
        {
            m_currentCondition = new Condition();
            m_currentCondition.SysOpSet = m_operationSet;
            m_currentCondition.ParameterSet = new ParameterSet();
            m_conditions.AddCondition(m_currentCondition);

            m_composingCondition = true;
        }

        /// <summary>
        /// Open a new constraint and ready for new parameters to be added
        /// </summary>
        internal void Constraint(string operations)
        {
            Operations(operations);
            Constraint();
        }

        /// <summary>
        /// Open a new constraint and ready for new parameters to be added
        /// </summary>
        internal void Constraint()
        {
            m_currentConstraint = new Constraint();
            m_currentConstraint.SysOpSet = m_operationSet;
            m_currentConstraint.ParameterSet = new ParameterSet();
            m_constraints.AddConstraint(m_currentConstraint);

            m_composingCondition = false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="varNm"></param>
        /// <param name="funcT"></param>
        /// <param name="valT"></param>
        /// <param name="vals">comma separated list</param>
        /// <param name="operations">comma seperated list</param>
        internal void Param(string varNm, E_FunctionT funcT, E_ParameterValueType valT, string vals)
        {
            Parameter p = new Parameter(varNm, funcT, valT, false, false);

            string[] parsed = vals.Split(DelimiterChars);
            for (int i = 0; i < parsed.Length; ++i)
                p.AddValue(parsed[i].Trim(), "");

            if (m_composingCondition)
                m_currentCondition.ParameterSet.AddParameter(p);
            else
                m_currentConstraint.ParameterSet.AddParameter(p);
        }

        internal void ClearConditions()
        {
            m_conditions = new ConditionSet();
        }

        internal void ClearConstraints()
        {
            m_constraints = new ConstraintSet();
        }

        /// <summary>
        /// The conditions will be removed from this object after this function returns;
        /// </summary>
        /// <returns></returns>
        internal ConditionSet ConditionSet
        {
            get { return m_conditions; }
        }

        /// <summary>
        /// The constraints will be removed from this object after this function returns;
        /// </summary>
        /// <returns></returns>
        internal ConstraintSet ConstraintSet
        {
            get { return m_constraints; }
        }
    }

    [TestFixture]
    public class ConflictDetectionTest
    {
        
        [Test]
        public void TestSimple()
        {
#region CREATE_CONDITIONSET

            Parameter p = new Parameter("sLAmt", E_FunctionT.IsGreaterThanOrEqualTo, E_ParameterValueType.Float, false, false);
            p.AddValue("1000000", "1 million");

            ParameterSet ps = new ParameterSet();
            ps.AddParameter(p);

            SystemOperationSet operations = new SystemOperationSet();
            operations.Add( new SystemOperation( "Operation A Test" ) );
            operations.Add( new SystemOperation( "Operation B Test" ) );
            operations.Add( new SystemOperation( "Operation C Test" ) );

            Condition cond = new Condition();
            cond.SysOpSet = operations;
            cond.ParameterSet = ps;
            

            ConditionSet cs = new ConditionSet();
            cs.AddCondition(cond);
#endregion // CREATE_CONDITION_SET

            Parameter p2 = new Parameter("sLAmt", E_FunctionT.IsLessThan, E_ParameterValueType.Float, false, false);
            p2.AddValue("1000000", "1 million");

            ParameterSet ps2 = new ParameterSet();
            ps2.AddParameter(p2);

            Constraint constraint1 = new Constraint();
            constraint1.SysOpSet = operations;
            constraint1.ParameterSet = ps2;

            ConstraintSet constraintS = new ConstraintSet();
            constraintS.AddConstraint(constraint1);

            ConflictDetector detector = new ConflictDetector();
            List<Condition> conflictConditions = detector.DetectConflictConditions(cs, constraint1, null);
            Assert.That(conflictConditions.Count == 0);

            Parameter p3 = new Parameter("sLAmt", E_FunctionT.IsLessThanOrEqualTo, E_ParameterValueType.Float, false, false);
            p3.AddValue("1000000", "1 million");

            ParameterSet ps3 = new ParameterSet();
            ps2.AddParameter(p3);

            Constraint constraint2 = new Constraint();
            constraint2.SysOpSet = operations;
            constraint2.ParameterSet = ps3;

            List<Condition> conflictConditions2 = detector.DetectConflictConditions(cs, constraint2, null);
            Assert.That(conflictConditions2.Count == 1);

            
        }

        [Test]
        public void TestDisjointOperations()
        {
            SetGenerator gen = new SetGenerator();

            // DISJOINT OPERATIONS
            gen.Condition( "Operation A" );
            gen.Param( "X", E_FunctionT.In, E_ParameterValueType.Int, "1,3,5" );

            gen.Constraint("Operation B");
            gen.Param( "X", E_FunctionT.In, E_ParameterValueType.Int, "1,3,5" );

            ConflictDetector detector = new ConflictDetector();
            Assert.That( !detector.HasConflict( gen.ConditionSet, gen.ConstraintSet ) );

            // JOINT OPERATIONS
            gen.Condition( "Operation A, Operation B" );
            gen.Param("X", E_FunctionT.In, E_ParameterValueType.Int, "1,3,5");

            gen.Constraint( "Operation C, Operation A" );
            gen.Param("X", E_FunctionT.In, E_ParameterValueType.Int, "1,3,5" );

            Assert.That( detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));
        }

        [Test]
        public void TestMultipleConditions()
        {
            SetGenerator gen = new SetGenerator();
            ConflictDetector detector = new ConflictDetector();

            gen.Operations( "Operation A" );

            gen.Condition();
            gen.Param("XStr", E_FunctionT.In, E_ParameterValueType.String, "abc,def,ghi");
            gen.Param("YIn", E_FunctionT.In, E_ParameterValueType.Int, "2,4,6");

            gen.Condition();
            gen.Param("XStr", E_FunctionT.In, E_ParameterValueType.String, "xyz,MNO,rts");
            gen.Param("YIn", E_FunctionT.In, E_ParameterValueType.Int, "8,9,10" );

            gen.Constraint();
            gen.Param("XStr", E_FunctionT.In, E_ParameterValueType.String, "foo,MNO,blah");
            gen.Param("YIn", E_FunctionT.In, E_ParameterValueType.Int, "9");

            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("XStr", E_FunctionT.In, E_ParameterValueType.String, "foo,MNO,blah");
            gen.Param("YIn", E_FunctionT.In, E_ParameterValueType.Int, "11");

            Assert.That(!detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));
        }

        [Test]
        public void TestMultipleConstraints()
        {
            SetGenerator gen = new SetGenerator();
            ConflictDetector detector = new ConflictDetector();

            gen.Operations("Operation A");

            gen.Condition();
            gen.Param("XStr", E_FunctionT.In, E_ParameterValueType.String, "abc,MATCH,ghi");
            gen.Param("YIn", E_FunctionT.In, E_ParameterValueType.Int, "2,4,6");

            gen.Constraint();
            gen.Param("XStr", E_FunctionT.In, E_ParameterValueType.String, "foo,car,blah");
            gen.Param("YIn", E_FunctionT.In, E_ParameterValueType.Int, "2");

            gen.Constraint();
            gen.Param("XStr", E_FunctionT.In, E_ParameterValueType.String, "foo,MATCH,blah");
            gen.Param("YIn", E_FunctionT.In, E_ParameterValueType.Int, "4");

            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("XStr", E_FunctionT.In, E_ParameterValueType.String, "foo,car,blah");
            gen.Param("YIn", E_FunctionT.In, E_ParameterValueType.Int, "2");

            gen.Constraint();
            gen.Param("XStr", E_FunctionT.In, E_ParameterValueType.String, "foo,MATCH,blah");
            gen.Param("YIn", E_FunctionT.In, E_ParameterValueType.Int, "100");
            
            Assert.That(!detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));
        }

        [Test]
        public void TestMultipleConditionsMultipleConstraints()
        {
            SetGenerator gen = new SetGenerator();
            ConflictDetector detector = new ConflictDetector();

            gen.Operations("Operation A");

            gen.Condition();
            gen.Param("XStr", E_FunctionT.In, E_ParameterValueType.Int, "abc,MATCH,ghi");
            gen.Param("YIn", E_FunctionT.In, E_ParameterValueType.Int, "2,4,6");

            gen.Condition();
            gen.Param("XStr", E_FunctionT.In, E_ParameterValueType.Int, "xyz,MATCH,mno");
            gen.Param("YIn", E_FunctionT.In, E_ParameterValueType.Int, "10,11,12");

            gen.Constraint();
            gen.Param("XStr", E_FunctionT.In, E_ParameterValueType.Int, "foo,car,blah");
            gen.Param("YIn", E_FunctionT.In, E_ParameterValueType.Int, "20,21,22");

            gen.Constraint();
            gen.Param("XStr", E_FunctionT.In, E_ParameterValueType.Int, "foo,MATCH,blah");
            gen.Param("YIn", E_FunctionT.In, E_ParameterValueType.Int, "11");

            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("XStr", E_FunctionT.In, E_ParameterValueType.Int, "foo,MATCH,blah");
            gen.Param("YIn", E_FunctionT.In, E_ParameterValueType.Int, "20,21,22");

            gen.Param("XStr", E_FunctionT.In, E_ParameterValueType.Int, "foo,MATCH,blah");
            gen.Param("YIn", E_FunctionT.In, E_ParameterValueType.Int, "21");

            Assert.That(!detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));
        }


        [Test]
        public void TestAllFunctionTypes()
        {
           
            SetGenerator gen = new SetGenerator();
            ConflictDetector detector = new ConflictDetector();

            gen.Operations("Operation A");

            // In
            gen.ClearConditions();
            gen.Condition();
            gen.Param("XIn", E_FunctionT.In, E_ParameterValueType.String, "abc,MATCH,ghi");

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("XIn", E_FunctionT.In, E_ParameterValueType.String, "fooo,MATCH,blah");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("XIn", E_FunctionT.In, E_ParameterValueType.String, "fooo,blah");
            Assert.That(!detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            // Between
            gen.ClearConditions();
            gen.Condition();
            gen.Param("X", E_FunctionT.Between, E_ParameterValueType.Float, "100.55,200.99");

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.Between, E_ParameterValueType.Float, "199.11,400.52");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.Between, E_ParameterValueType.Float, "201.00,300.22");
            Assert.That(!detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.Between, E_ParameterValueType.Float, "-500.12,99.23");
            Assert.That(!detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.Between, E_ParameterValueType.Float, "-500.12,99.23");
            Assert.That(!detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.IsGreaterThan, E_ParameterValueType.Float, "50.22");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.IsGreaterThan, E_ParameterValueType.Float, "200.99");
            Assert.That(!detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.IsGreaterThan, E_ParameterValueType.Float, "-1000.11");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.IsLessThan, E_ParameterValueType.Float, "300.12");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.IsLessThan, E_ParameterValueType.Float, "150.22");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.IsLessThan, E_ParameterValueType.Float, "100.55");
            Assert.That(!detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.IsLessThan, E_ParameterValueType.Float, "100.22");
            Assert.That(!detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.IsLessThanOrEqualTo, E_ParameterValueType.Float, "100.55");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.IsLessThanOrEqualTo, E_ParameterValueType.Float, "100.54");
            Assert.That(!detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            // Equal
            gen.ClearConditions();
            gen.Condition();
            gen.Param("XEqual", E_FunctionT.Equal, E_ParameterValueType.Bool, "True");

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("XEqual", E_FunctionT.Equal, E_ParameterValueType.Bool, "True");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("XEqual", E_FunctionT.Equal, E_ParameterValueType.Bool, "False");
            Assert.That(!detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            // IsNonblankStr
            gen.ClearConditions();
            gen.Condition();
            gen.Param("XIsNonblankStr", E_FunctionT.IsNonblankStr, E_ParameterValueType.String, "True");

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("XIsNonblankStr", E_FunctionT.IsNonblankStr, E_ParameterValueType.String, "True");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("XIsNonblankStr", E_FunctionT.IsNonblankStr, E_ParameterValueType.String, "False");
            Assert.That(!detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            // IsGreaterThan
            gen.ClearConditions();
            gen.Condition();
            gen.Param("X", E_FunctionT.IsGreaterThan, E_ParameterValueType.Float, "1000000");

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.IsGreaterThan, E_ParameterValueType.Float, "500000");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.IsGreaterThanOrEqualTo, E_ParameterValueType.Float, "500000");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.IsLessThanOrEqualTo, E_ParameterValueType.Float, "1000000");
            Assert.That(!detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.IsLessThan, E_ParameterValueType.Float, "1500000");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.IsLessThan, E_ParameterValueType.Float, "1000000");
            Assert.That(!detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.IsLessThan, E_ParameterValueType.Float, "500000");
            Assert.That(!detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            // IsLessThan
            gen.ClearConditions();
            gen.Condition();
            gen.Param("X", E_FunctionT.IsLessThan, E_ParameterValueType.Float, "1000000");

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.IsGreaterThan, E_ParameterValueType.Float, "500000");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.IsGreaterThanOrEqualTo, E_ParameterValueType.Float, "500000");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.IsGreaterThanOrEqualTo, E_ParameterValueType.Float, "1500000");
            Assert.That(!detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.IsLessThanOrEqualTo, E_ParameterValueType.Float, "1000000");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.IsLessThan, E_ParameterValueType.Float, "1500000");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.IsLessThan, E_ParameterValueType.Float, "1000000");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.IsGreaterThan, E_ParameterValueType.Float, "500000");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));


            // IsGreaterThanOrEqualTo
            gen.ClearConditions();
            gen.Condition();
            gen.Param("X", E_FunctionT.IsGreaterThanOrEqualTo, E_ParameterValueType.Float, "100");
            
            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.IsGreaterThanOrEqualTo, E_ParameterValueType.Float, "5");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.IsGreaterThan, E_ParameterValueType.Float, "5");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.IsLessThanOrEqualTo, E_ParameterValueType.Float, "100");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.IsLessThan, E_ParameterValueType.Float, "100");
            Assert.That(!detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));
            
            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.IsLessThanOrEqualTo, E_ParameterValueType.Float, "99");
            Assert.That(!detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            // IsLessThanOrEqualTo
            gen.ClearConditions();
            gen.Condition();
            gen.Param("X", E_FunctionT.IsLessThanOrEqualTo, E_ParameterValueType.Float, "100");

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.IsGreaterThanOrEqualTo, E_ParameterValueType.Float, "5");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.IsGreaterThan, E_ParameterValueType.Float, "5");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.IsLessThanOrEqualTo, E_ParameterValueType.Float, "50");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.IsLessThan, E_ParameterValueType.Float, "50");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.IsGreaterThan, E_ParameterValueType.Float, "100");
            Assert.That(!detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.IsGreaterThanOrEqualTo, E_ParameterValueType.Float, "100");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            // IsNonzeroValue
            gen.ClearConditions();
            gen.Condition();
            gen.Param("X", E_FunctionT.IsNonzeroValue, E_ParameterValueType.Float, "True");

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.IsNonzeroValue, E_ParameterValueType.Float, "True");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.IsNonzeroValue, E_ParameterValueType.Float, "False");
            Assert.That(!detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));
             
            
            // IsAnyValue
            gen.ClearConditions();
            gen.Condition();
            gen.Param("X", E_FunctionT.IsAnyValue, E_ParameterValueType.DateTime, "True");

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.IsAnyValue, E_ParameterValueType.DateTime, "True");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.IsAnyValue, E_ParameterValueType.DateTime, "False");
            Assert.That(!detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));


            gen.ClearConditions();
            gen.Condition();
            gen.Param("X", E_FunctionT.IsAnyValue, E_ParameterValueType.Float, "True");

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.IsAnyValue, E_ParameterValueType.Float, "True");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.IsAnyValue, E_ParameterValueType.Float, "False");
            Assert.That(!detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            // These are the future ones, they are not being tested here.
            // IsGreaterThanVar,
            // IsLessThanVar,
            // EqualVar,
            // IsGreaterThanOrEqualToVar,
            // IsLessThanOrEqualToVar,

        }


        [Test]
        public void TestAllValueTypes()
        {
            SetGenerator gen = new SetGenerator();
            ConflictDetector detector = new ConflictDetector();
            gen.Operations("Operation A");

            // Int
            gen.ClearConditions();
            gen.Condition();
            gen.Param("X", E_FunctionT.In, E_ParameterValueType.Int, "1,3,5");

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.In, E_ParameterValueType.Int, "3,100");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.In, E_ParameterValueType.Int, "0,100");
            Assert.That(!detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            // Float
            gen.ClearConditions();
            gen.Condition();
            gen.Param("X", E_FunctionT.Between, E_ParameterValueType.Float, "20.45,99.88");

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.Between, E_ParameterValueType.Float, "50.22,300.23");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.Between, E_ParameterValueType.Float, "9.22,300.23");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.Between, E_ParameterValueType.Float, "9.22,50.23");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.Between, E_ParameterValueType.Float, "9.22,13.23");
            Assert.That(!detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            // String
            gen.ClearConditions();
            gen.Condition();
            gen.Param("X", E_FunctionT.In, E_ParameterValueType.String, "abc,MATCH");

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.In, E_ParameterValueType.String, "caar,fooo");
            Assert.That(!detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.In, E_ParameterValueType.String, "caar,MATCH");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            // Datetime
            gen.ClearConditions();
            gen.Condition();
            gen.Param("X", E_FunctionT.Between, E_ParameterValueType.DateTime, "2/1/2010,3/1/2010");

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.Between, E_ParameterValueType.DateTime, "2/15/2010,3/15/2010");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.Between, E_ParameterValueType.DateTime, "3/1/2010,3/15/2010");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.Between, E_ParameterValueType.DateTime, "3/2/2010,3/15/2010");
            Assert.That(!detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.Between, E_ParameterValueType.DateTime, "1/1/2010,2/1/2010");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.Between, E_ParameterValueType.DateTime, "1/1/2010,1/15/2010");
            Assert.That(!detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            // Bool
            gen.ClearConditions();
            gen.Condition();
            gen.Param("X", E_FunctionT.Equal, E_ParameterValueType.Bool, "True");

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.Equal, E_ParameterValueType.Bool, "True");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.Equal, E_ParameterValueType.Bool, "False");
            Assert.That(!detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            // CustomVariable
            gen.ClearConditions();
            gen.Condition();
            gen.Param("X", E_FunctionT.Equal, E_ParameterValueType.CustomVariable, "True");

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.Equal, E_ParameterValueType.CustomVariable, "True");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.Equal, E_ParameterValueType.CustomVariable, "False");
            Assert.That(!detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

        }

        [Test]
        public void TestDontCares()
        {
            SetGenerator gen = new SetGenerator();
            ConflictDetector detector = new ConflictDetector();

            gen.Condition("Operation A");
            gen.Param("First", E_FunctionT.IsGreaterThan, E_ParameterValueType.Float, "0");
            gen.Param("Second", E_FunctionT.In, E_ParameterValueType.String, "abc,MATCH");

            gen.Constraint();
            gen.Param("First", E_FunctionT.IsGreaterThan, E_ParameterValueType.Float, "100");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("Second", E_FunctionT.In, E_ParameterValueType.String, "MATCH,fooo");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));


            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("First", E_FunctionT.Between, E_ParameterValueType.Float, "-100,100");
            gen.Param("Second", E_FunctionT.In, E_ParameterValueType.String, "MATCH,fooo");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("First", E_FunctionT.Between, E_ParameterValueType.Float, "-100,100");
            gen.Param("Second", E_FunctionT.In, E_ParameterValueType.String, "MATCH,fooo");
            gen.Param("Third", E_FunctionT.In, E_ParameterValueType.String, "caar,baar");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("First", E_FunctionT.Between, E_ParameterValueType.Float, "-100,100");
            gen.Param("Second", E_FunctionT.In, E_ParameterValueType.String, "caar,fooo");
            Assert.That(!detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("Second", E_FunctionT.In, E_ParameterValueType.String, "MATCH,fooo");
            gen.Param("First", E_FunctionT.Between, E_ParameterValueType.Float, "-100,100");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));
        }

        [Test]
        public void TestCaseSenstivity()
        {
            
            SetGenerator gen = new SetGenerator();
            ConflictDetector detector = new ConflictDetector();
            gen.Operations("Operation A");

            // variable name

            gen.ClearConditions();
            gen.Condition();
            gen.Param("A", E_FunctionT.In, E_ParameterValueType.Int, "1,3,5");

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("a", E_FunctionT.In, E_ParameterValueType.Int, "3,100");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConditions();
            gen.Condition();
            gen.Param("A", E_FunctionT.In, E_ParameterValueType.Int, "1,3,5");

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("A", E_FunctionT.In, E_ParameterValueType.Int, "3,100");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));


            gen.ClearConditions();
            gen.Condition();
            gen.Param("A", E_FunctionT.In, E_ParameterValueType.Int, "1,3,5");

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("a", E_FunctionT.In, E_ParameterValueType.Int, "99,100");
            Assert.That(!detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConditions();
            gen.Condition();
            gen.Param("A", E_FunctionT.In, E_ParameterValueType.Int, "1,3,5");

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("A", E_FunctionT.In, E_ParameterValueType.Int, "99,100");
            Assert.That(!detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            // string values

            gen.ClearConditions();
            gen.Condition();
            gen.Param("X", E_FunctionT.In, E_ParameterValueType.String, "abc,MaTch");

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.In, E_ParameterValueType.String, "mAtCh,fooo");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConditions();
            gen.Condition();
            gen.Param("X", E_FunctionT.In, E_ParameterValueType.String, "abc,match");

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.In, E_ParameterValueType.String, "MATCH,fooo");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConditions();
            gen.Condition();
            gen.Param("X", E_FunctionT.In, E_ParameterValueType.String, "abc,Match");

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.In, E_ParameterValueType.String, "mATCH,fooo");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

            gen.ClearConditions();
            gen.Condition();
            gen.Param("X", E_FunctionT.In, E_ParameterValueType.String, "abc,def");

            gen.ClearConstraints();
            gen.Constraint();
            gen.Param("X", E_FunctionT.In, E_ParameterValueType.String, "caar,fooo");
            Assert.That(!detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));


            // Operation Names

            gen.ClearConditions();
            gen.Condition("OperaTion A");
            gen.Param("X", E_FunctionT.In, E_ParameterValueType.Int, "1,3,5");

            gen.ClearConstraints();
            gen.Constraint("Operation a");
            gen.Param("X", E_FunctionT.In, E_ParameterValueType.Int, "1,3,5");
            Assert.That(detector.HasConflict(gen.ConditionSet, gen.ConstraintSet));

        }
    }


}
