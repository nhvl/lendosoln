﻿namespace LendOSolnTest.ConfigSystem
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using global::ConfigSystem.Engine;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem;
    using LendersOffice.ConfigSystem.Operations;
    using NUnit.Framework;
    using OperationResultDictionary = System.Collections.Generic.Dictionary<string, global::ConfigSystem.Engine.ExecutingEngineResultType>;

    [TestFixture]
    public class WorkflowOperationTest
    {
        [Test]
        [Category(Common.CategoryConstants.RunBeforePullRequest)]
        [Category(Common.CategoryConstants.IntegrationTest)]
        public void Operations_All_DefinedInSystemConfig()
        {
            HashSet<WorkflowOperation> ignoredOperations = new HashSet<WorkflowOperation>
            {
                // 07/05/2017 tj - These operations currently fail the test on dev, so we'll just ignore them for now.
                WorkflowOperations.PreSaveValidation,
                WorkflowOperations.RequestRateLockCausesLock,
                WorkflowOperations.ReadLoanOrTemplate,
                WorkflowOperations.WriteLoanOrTemplate,
                WorkflowOperations.LoanStatusChange,
                WorkflowOperations.LoanStatusChangeToConditionReview,
                WorkflowOperations.LoanStatusChangeToDocumentCheck
            };

            var systemConfig = new WorkflowSystemConfigModel(ExecutingEngine.SystemOrganizationId, loadDraft: false);
            var operationSet = new HashSet<string>(systemConfig.GetFullConditionSet().SelectMany(conditionGroup => conditionGroup.SysOpSet.Select(op => op.Name)));
            var missingOperations = new List<WorkflowOperation>();
            foreach (WorkflowOperation operation in WorkflowOperations.GetAll())
            {
                if (!operationSet.Contains(operation.Id) && !ignoredOperations.Contains(operation))
                {
                    missingOperations.Add(operation);
                }
            }

            Assert.AreEqual(0, missingOperations.Count, $"Unable to find the following operations in the system config for the current environment: {string.Join(", ", missingOperations)}");
        }

        [Test]
        public void DependentOperations_ForAllOperations_MatchOldCode()
        {
            DependencySet<string> oldOperationDependencies = GetOperationDependencySet_Old();
            DependencySet<WorkflowOperation> newOperationDependencies = LendingQBExecutingEngine.GetOperationDependencySet();

            foreach (var operation in WorkflowOperations.GetAll())
            {
                // Skip operations added since workflow refactor.
                if (!friendlyOpNames.ContainsKey(operation.Id))
                {
                    continue;
                }

                var id = new[] { operation.Id };
                CollectionAssert.AreEquivalent(
                    oldOperationDependencies.GetDependsOn(id),
                    newOperationDependencies.GetDependsOn(id.Select(i => WorkflowOperations.Get(i))).Select(o => o.Id));
            }
        }

        [Test]
        public void Operations_All_HaveIdsThatMatchFormerConstAppVersions()
        {
            Assert.AreEqual(OPERATION_READ_TEMPLATE, WorkflowOperations.ReadTemplate.Id);
            Assert.AreEqual(OPERATION_WRITE_TEMPLATE, WorkflowOperations.WriteTemplate.Id);
            Assert.AreEqual(OPERATION_WRITE_LOAN, WorkflowOperations.WriteLoan.Id);
            Assert.AreEqual(OPERATION_READ_LOAN, WorkflowOperations.ReadLoan.Id);
            Assert.AreEqual(OPERATION_READ_LOAN_OR_TEMPLATE, WorkflowOperations.ReadLoanOrTemplate.Id);
            Assert.AreEqual(OPERATION_WRITE_LOAN_OR_TEMPLATE, WorkflowOperations.WriteLoanOrTemplate.Id);
            Assert.AreEqual(OPERATION_RUN_PML_ALL_LOANS, WorkflowOperations.RunPmlForAllLoans.Id);
            Assert.AreEqual(OPERATION_RUN_PML_REGISTERED_LOANS, WorkflowOperations.RunPmlForRegisteredLoans.Id);
            Assert.AreEqual(OPERATION_RUN_PML_TO_STEP_3, WorkflowOperations.RunPmlToStep3.Id);
            Assert.AreEqual(OPERATION_REQUEST_RATE_LOCK_AUTO_RATE_LOCK, WorkflowOperations.RequestRateLockCausesLock.Id);
            Assert.AreEqual(OPERATION_REQUEST_RATE_LOCK_MANUAL_RATE_LOCK_BY_LOCK_DESK, WorkflowOperations.RequestRateLockRequiresManualLock.Id);
            Assert.AreEqual(OPERATION_USE_OLD_SECURITY_MODEL_PML, WorkflowOperations.UseOldSecurityModelForPml.Id);
            Assert.AreEqual(OPERATION_REGISTER_LOAN, WorkflowOperations.RegisterLoan.Id);
            Assert.AreEqual(OPERATION_DELETE_LOAN, WorkflowOperations.DeleteLoan.Id);
            Assert.AreEqual(OPERATION_PROTECT_FIELD, WorkflowOperations.ProtectField.Id);
            Assert.AreEqual(OPERATION_WRITE_FIELD, WorkflowOperations.WriteField.Id);
            Assert.AreEqual(OPERATION_DOC_MAGIC_GENERATE_DOCS, WorkflowOperations.GenerateDocs.Id);
            Assert.AreEqual(OPERATION_EXTEND_LOCK, WorkflowOperations.ExtendLock.Id);
            Assert.AreEqual(OPERATION_FLOAT_DOWN_LOCK, WorkflowOperations.FloatDownLock.Id);
            Assert.AreEqual(OPERATION_RATE_RE_LOCK, WorkflowOperations.RateReLock.Id);
            Assert.AreEqual(OPERATION_DELETE_APP, WorkflowOperations.DeleteApp.Id);
            Assert.AreEqual(OPERATION_ORDER_APPRAISAL, WorkflowOperations.OrderAppraisal.Id);
            Assert.AreEqual(OPERATION_PRESAVE_VALIDATION, WorkflowOperations.PreSaveValidation.Id);
            Assert.AreEqual(OPERATION_IMPORT_INTO_EXISTING_FILE, WorkflowOperations.ImportIntoExistingFile.Id);
            Assert.AreEqual(OPERATION_MANUALLY_OVERRIDE_QM_SETTINGS, WorkflowOperations.ManuallyOverrideQMSettings.Id);
            Assert.AreEqual(OPERATION_LOAN_STATUS_CHANGE_DOCUMENT_CHECK, WorkflowOperations.LoanStatusChangeToDocumentCheck.Id);
            Assert.AreEqual(OPERATION_LOAN_STATUS_CHANGE_CONDITION_REVIEW, WorkflowOperations.LoanStatusChangeToConditionReview.Id);
        }

        [Test]
        public void Operations_All_HaveFriendlyDescriptionThatMatchesFormerConstApp()
        {
            foreach (var operation in WorkflowOperations.GetAll())
            {
                // Skip operations added since workflow refactor.
                if (!friendlyOpNames.ContainsKey(operation.Id))
                {
                    continue;
                }

                Assert.AreEqual(GetFriendlyOpName(operation.Id), operation.FriendlyDescription);
            }
        }

        [Test]
        public void NewOperationEvaluation_Always_MatchesOldEvaluation()
        {
            var set = LendingQBExecutingEngine.GetOperationDependencySet();

            foreach (var operation in WorkflowOperations.GetAll())
            {
                // Skip operations added since workflow refactor.
                if (!friendlyOpNames.ContainsKey(operation.Id))
                {
                    continue;
                }

                var dependencies = set.GetDependsOn(new[] { operation });
                var allResultCombinations = GeneratePermutations(dependencies);

                foreach (var operationResults in allResultCombinations)
                {
                    var oldResult = GetResult_Old(operation.Id, operationResults.ToDictionary(kvp => kvp.Key.Id, kvp => kvp.Value));
                    var newResult = LendingQBExecutingEngine.GetResult(operation, operationResults);
                    Assert.AreEqual(oldResult, newResult, $"{operation.Id} produced unexpected result.");
                }
            }
        }

        [Test]
        public void NewGetFriendlyOperationNames_VariedInput_MatchesOldOutput()
        {
            var ids = new[] { WorkflowOperations.ReadLoan.Id, WorkflowOperations.PreSaveValidation.Id };
            var oldOut = GetFriendlyOperationNames_Old(ids);
            var newOut = WorkflowSystemConfigModel.GetFriendlyOperationNames(ids);
            CollectionAssert.AreEquivalent(oldOut, newOut);

            ids = new[] { WorkflowOperations.ReadLoanOrTemplate.Id, WorkflowOperations.ProtectField.Id };
            oldOut = GetFriendlyOperationNames_Old(ids);
            newOut = WorkflowSystemConfigModel.GetFriendlyOperationNames(ids);
            CollectionAssert.AreEquivalent(oldOut, newOut);

            ids = WorkflowOperations.GetAll().Select(op => op.Id).Where(id => friendlyOpNames.ContainsKey(id)).ToArray();
            oldOut = GetFriendlyOperationNames_Old(ids);
            newOut = WorkflowSystemConfigModel.GetFriendlyOperationNames(ids);
            CollectionAssert.AreEquivalent(oldOut, newOut);
        }

        [Test]
        public void Equals_SameReference_ReturnsTrue()
        {
            var a = WorkflowOperations.DeleteApp;
            var b = WorkflowOperations.DeleteApp;

            Assert.That(a.Equals(b));
            Assert.That(b.Equals(a));
            Assert.That(a == b);
            Assert.That(b == a);
        }

        [Test]
        public void Equals_SameOperationSeparateReferences_ReturnsTrue()
        {
            var a = new DeleteApplication();
            var b = new DeleteApplication();

            Assert.That(a.Equals(b));
            Assert.That(b.Equals(a));
            Assert.That(a == b);
            Assert.That(b == a);
        }

        [Test]
        public void NotEquals_DifferentOperations_ReturnsTrue()
        {
            var a = WorkflowOperations.PreSaveValidation;
            var b = WorkflowOperations.ManuallyOverrideQMSettings;

            Assert.That(a != b);
        }

        private IEnumerable<Dictionary<WorkflowOperation, ExecutingEngineResultType>> GeneratePermutations(IEnumerable<WorkflowOperation> dependencies)
        {
            // Each dependency will either be true or false, we want all the possible combinations.
            int numDependencies = dependencies.Count();

            if (numDependencies < 1)
            {
                throw new Exception();
            }
            else if (numDependencies == 1)
            {
                var id = WorkflowOperations.Get(dependencies.First().Id);
                return new List<Dictionary<WorkflowOperation, ExecutingEngineResultType>>()
                {
                    new Dictionary<WorkflowOperation, ExecutingEngineResultType>()
                    {
                        [id] = ExecutingEngineResultType.Allow
                    },
                    new Dictionary<WorkflowOperation, ExecutingEngineResultType>()
                    {
                        [id] = ExecutingEngineResultType.NotAllow
                    },
                    new Dictionary<WorkflowOperation, ExecutingEngineResultType>()
                    {
                        [id] = ExecutingEngineResultType.OperationNotFound
                    }
                };
            }

            var permutations = GeneratePermutations(dependencies.Take(numDependencies - 1));
            var resultingPerms = new List<Dictionary<WorkflowOperation, ExecutingEngineResultType>>();
            var currentDep = dependencies.Last();

            foreach (var perm in permutations)
            {
                var allow = new Dictionary<WorkflowOperation, ExecutingEngineResultType>(perm);
                allow.Add(currentDep, ExecutingEngineResultType.Allow);

                var notAllow = new Dictionary<WorkflowOperation, ExecutingEngineResultType>(perm);
                notAllow.Add(currentDep, ExecutingEngineResultType.NotAllow);

                var notFound = new Dictionary<WorkflowOperation, ExecutingEngineResultType>(perm);
                notFound.Add(currentDep, ExecutingEngineResultType.OperationNotFound);

                resultingPerms.Add(allow);
                resultingPerms.Add(notAllow);
                resultingPerms.Add(notFound);
            }

            return resultingPerms;
        }

        #region Extracted Constants / Methods to Verify Legacy Behavior
        // This was pulled out of LendingQbExecutingEngine.
        public static DependencySet<string> GetOperationDependencySet_Old()
        {
            var set = new DependencySet<string>(StringComparer.OrdinalIgnoreCase);
            set.Add(OPERATION_READ_TEMPLATE, new string[] { OPERATION_READ_TEMPLATE });
            set.Add(OPERATION_WRITE_TEMPLATE, new string[] { OPERATION_WRITE_TEMPLATE, OPERATION_READ_TEMPLATE });

            set.Add(OPERATION_WRITE_LOAN, new string[] { OPERATION_WRITE_LOAN, OPERATION_READ_LOAN });
            set.Add(OPERATION_READ_LOAN, new string[] { OPERATION_READ_LOAN });

            set.Add(OPERATION_READ_LOAN_OR_TEMPLATE, new string[] { OPERATION_READ_LOAN, OPERATION_READ_TEMPLATE });

            set.Add(OPERATION_WRITE_LOAN_OR_TEMPLATE, new string[] { OPERATION_WRITE_LOAN, OPERATION_WRITE_TEMPLATE });

            set.Add(OPERATION_RUN_PML_ALL_LOANS, new string[] { OPERATION_RUN_PML_ALL_LOANS, OPERATION_READ_LOAN });
            set.Add(OPERATION_RUN_PML_REGISTERED_LOANS, new string[] { OPERATION_RUN_PML_REGISTERED_LOANS, OPERATION_READ_LOAN });

            set.Add(OPERATION_RUN_PML_TO_STEP_3, new string[] { OPERATION_RUN_PML_TO_STEP_3, OPERATION_READ_LOAN });
            set.Add(OPERATION_REQUEST_RATE_LOCK_AUTO_RATE_LOCK, new string[] { OPERATION_REQUEST_RATE_LOCK_AUTO_RATE_LOCK, OPERATION_READ_LOAN });
            set.Add(OPERATION_REQUEST_RATE_LOCK_MANUAL_RATE_LOCK_BY_LOCK_DESK, new string[] { OPERATION_REQUEST_RATE_LOCK_MANUAL_RATE_LOCK_BY_LOCK_DESK, OPERATION_READ_LOAN });
            set.Add(OPERATION_USE_OLD_SECURITY_MODEL_PML, new string[] { OPERATION_USE_OLD_SECURITY_MODEL_PML });
            set.Add(OPERATION_REGISTER_LOAN, new string[] { OPERATION_REGISTER_LOAN });

            set.Add(OPERATION_DELETE_LOAN, new string[] { OPERATION_DELETE_LOAN, OPERATION_WRITE_LOAN_OR_TEMPLATE });
            set.Add(OPERATION_PROTECT_FIELD, new string[] { OPERATION_PROTECT_FIELD });
            set.Add(OPERATION_WRITE_FIELD, new string[] { OPERATION_WRITE_FIELD });

            set.Add(OPERATION_DOC_MAGIC_GENERATE_DOCS, new string[] { OPERATION_DOC_MAGIC_GENERATE_DOCS });

            set.Add(OPERATION_EXTEND_LOCK, new string[] { OPERATION_EXTEND_LOCK });
            set.Add(OPERATION_FLOAT_DOWN_LOCK, new string[] { OPERATION_FLOAT_DOWN_LOCK });
            set.Add(OPERATION_RATE_RE_LOCK, new string[] { OPERATION_RATE_RE_LOCK });

            set.Add(OPERATION_DELETE_APP, new string[] { OPERATION_DELETE_APP, OPERATION_WRITE_LOAN_OR_TEMPLATE });

            set.Add(OPERATION_ORDER_APPRAISAL, new string[] { OPERATION_ORDER_APPRAISAL, OPERATION_READ_LOAN });

            set.Add(OPERATION_PRESAVE_VALIDATION, new string[] { OPERATION_PRESAVE_VALIDATION });

            set.Add(OPERATION_IMPORT_INTO_EXISTING_FILE, new string[] { OPERATION_IMPORT_INTO_EXISTING_FILE, OPERATION_WRITE_LOAN, OPERATION_READ_LOAN });

            set.Add(OPERATION_MANUALLY_OVERRIDE_QM_SETTINGS, new string[] { OPERATION_MANUALLY_OVERRIDE_QM_SETTINGS, OPERATION_WRITE_LOAN, OPERATION_READ_LOAN });

            set.Add(OPERATION_LOAN_STATUS_CHANGE_DOCUMENT_CHECK, new string[] { OPERATION_LOAN_STATUS_CHANGE_DOCUMENT_CHECK, OPERATION_WRITE_LOAN, OPERATION_READ_LOAN });

            set.Add(OPERATION_LOAN_STATUS_CHANGE_CONDITION_REVIEW, new string[] { OPERATION_LOAN_STATUS_CHANGE_CONDITION_REVIEW, OPERATION_WRITE_LOAN, OPERATION_READ_LOAN });
            return set;
        }

        // This was pulled out of LendingQbExecutingEngine.
        public static bool GetResult_Old(string operation, OperationResultDictionary dictionary)
        {
            bool result = false;

            if (operation.Equals(OPERATION_READ_LOAN_OR_TEMPLATE, StringComparison.OrdinalIgnoreCase))
            {
                // 10/2/2010 dd - READ_LOAN_OR_TEMPLATE = READ_LOAN OR READ_TEMPLATE
                result = dictionary.IsAllow(OPERATION_READ_LOAN) || dictionary.IsAllow(OPERATION_READ_TEMPLATE);
            }
            else if (operation.Equals(OPERATION_WRITE_LOAN, StringComparison.OrdinalIgnoreCase))
            {
                // 10/2/2010 dd - WRITE_LOAN = WRITE_LOAN AND READ_LOAN
                result = dictionary.IsAllow(OPERATION_WRITE_LOAN) && dictionary.IsAllow(OPERATION_READ_LOAN);

            }
            else if (operation.Equals(OPERATION_WRITE_LOAN_OR_TEMPLATE, StringComparison.OrdinalIgnoreCase))
            {
                // 10/2/2010 dd - WRITE_LOAN_OR_TEMPLATE = (WRITE_LOAN AND READ_LOAN) OR (WRITE_TEMPLATE AND READ_TEMPLATE)
                result = (
                        dictionary.IsAllow(OPERATION_WRITE_LOAN) && dictionary.IsAllow(OPERATION_READ_LOAN)
                    ) || (
                        dictionary.IsAllow(OPERATION_WRITE_TEMPLATE) && dictionary.IsAllow(OPERATION_READ_TEMPLATE)
                    );
            }
            else if (operation.Equals(OPERATION_RUN_PML_ALL_LOANS, StringComparison.OrdinalIgnoreCase))
            {
                // 10/2/2010 dd - RUN_PML_ALL = RUN_PML_ALL AND READ_LOAN
                result = dictionary.IsAllow(OPERATION_RUN_PML_ALL_LOANS) &&
                         dictionary.IsAllow(OPERATION_READ_LOAN);
            }
            else if (operation.Equals(OPERATION_RUN_PML_REGISTERED_LOANS, StringComparison.OrdinalIgnoreCase))
            {
                // 10/2/2010 dd - RUN_PML_REGISTERED = RUN_PML_REGISTERED AND READ_LOAN
                result = dictionary.IsAllow(OPERATION_RUN_PML_REGISTERED_LOANS) &&
                         dictionary.IsAllow(OPERATION_READ_LOAN);

            }
            else if (operation.Equals(OPERATION_RUN_PML_TO_STEP_3, StringComparison.OrdinalIgnoreCase))
            {
                result = dictionary.IsAllow(OPERATION_RUN_PML_TO_STEP_3) &&
                    dictionary.IsAllow(OPERATION_READ_LOAN);
            }
            else if (operation.Equals(OPERATION_REQUEST_RATE_LOCK_AUTO_RATE_LOCK, StringComparison.OrdinalIgnoreCase))
            {
                //If we have this permission, check it and return its value
                if (dictionary.ContainsKey(OPERATION_REQUEST_RATE_LOCK_AUTO_RATE_LOCK))
                {
                    result = dictionary.IsAllow(OPERATION_REQUEST_RATE_LOCK_AUTO_RATE_LOCK) &&
                        dictionary.IsAllow(OPERATION_READ_LOAN);
                }
                else
                {
                    //Otherwise, since there were issues with this, return false.
                    result = false;
                }
            }
            else if (operation.Equals(OPERATION_REQUEST_RATE_LOCK_MANUAL_RATE_LOCK_BY_LOCK_DESK, StringComparison.OrdinalIgnoreCase))
            {
                result = dictionary.IsAllow(OPERATION_REQUEST_RATE_LOCK_MANUAL_RATE_LOCK_BY_LOCK_DESK) &&
                    dictionary.IsAllow(OPERATION_READ_LOAN);
            }
            else if (operation.Equals(OPERATION_DELETE_LOAN, StringComparison.OrdinalIgnoreCase))
            {
                // 11/2/2010 dd - DELETE_LOAN = DELETE_LOAN AND ( (WRITE_LOAN AND READ_LOAN) OR (WRITE_TEMPLATE AND READ_TEMPLATE) )
                result = dictionary.IsAllow(OPERATION_DELETE_LOAN) &&
                    ((dictionary.IsAllow(OPERATION_WRITE_LOAN) && dictionary.IsAllow(OPERATION_READ_LOAN)) ||
                    (dictionary.IsAllow(OPERATION_WRITE_TEMPLATE) && dictionary.IsAllow(OPERATION_READ_TEMPLATE)));
            }
            else if (operation.Equals(OPERATION_DELETE_APP, StringComparison.OrdinalIgnoreCase))
            {
                // 7/17/2013 BB - DELETE_APP = DELETE_APP AND ( (WRITE_LOAN AND READ_LOAN) OR (WRITE_TEMPLATE AND READ_TEMPLATE) )
                result = dictionary.IsAllow(OPERATION_DELETE_APP) &&
                    ((dictionary.IsAllow(OPERATION_WRITE_LOAN) && dictionary.IsAllow(OPERATION_READ_LOAN)) ||
                    (dictionary.IsAllow(OPERATION_WRITE_TEMPLATE) && dictionary.IsAllow(OPERATION_READ_TEMPLATE)));
            }
            else if (operation.Equals(OPERATION_ORDER_APPRAISAL, StringComparison.OrdinalIgnoreCase))
            {
                result = dictionary.IsAllow(OPERATION_ORDER_APPRAISAL) && dictionary.IsAllow(OPERATION_READ_LOAN);
            }
            else if (operation.Equals(OPERATION_IMPORT_INTO_EXISTING_FILE, StringComparison.OrdinalIgnoreCase))
            {
                result = dictionary.IsAllow(OPERATION_IMPORT_INTO_EXISTING_FILE) 
                    && dictionary.IsAllow(OPERATION_WRITE_LOAN)
                    && dictionary.IsAllow(OPERATION_READ_LOAN);
            }
            else if (operation.Equals(OPERATION_MANUALLY_OVERRIDE_QM_SETTINGS, StringComparison.OrdinalIgnoreCase))
            {
                result = dictionary.IsAllow(OPERATION_MANUALLY_OVERRIDE_QM_SETTINGS)
                    && dictionary.IsAllow(OPERATION_WRITE_LOAN)
                    && dictionary.IsAllow(OPERATION_READ_LOAN);
            }
            else if (operation.Equals(OPERATION_LOAN_STATUS_CHANGE_DOCUMENT_CHECK, StringComparison.OrdinalIgnoreCase))
            {
                result = dictionary.IsAllow(OPERATION_LOAN_STATUS_CHANGE_DOCUMENT_CHECK)
                    && dictionary.IsAllow(OPERATION_READ_LOAN);
            }
            else if (operation.Equals(OPERATION_LOAN_STATUS_CHANGE_CONDITION_REVIEW, StringComparison.OrdinalIgnoreCase))
            {
                result = dictionary.IsAllow(OPERATION_LOAN_STATUS_CHANGE_CONDITION_REVIEW)
                    && dictionary.IsAllow(OPERATION_READ_LOAN);
            }
            else
            {
                result = dictionary.IsAllow(operation);
            }

            return result;
        }

        // This was pulled out of WorkflowSystemConfigModel.
        public static IEnumerable<string> GetFriendlyOperationNames_Old(IEnumerable<string> opList)
        {
            List<string> friendlyNames = new List<string>();
            foreach (string op in opList)
            {
                friendlyNames.Add(GetFriendlyOpName(op));
            }

            return friendlyNames;
        }

        // These were pulled out of ConstApp.
        public const string OPERATION_READ_LOAN_OR_TEMPLATE = "ReadLoanOrTemplate";
        public const string OPERATION_WRITE_LOAN_OR_TEMPLATE = "WriteLoanOrTemplate";
        public const string OPERATION_READ_LOAN = "ReadLoan";
        public const string OPERATION_WRITE_LOAN = "WriteLoan";
        public const string OPERATION_REGISTER_LOAN = "RegisterLoan";
        public const string OPERATION_WRITE_FIELD = "WriteField"; // Field-level write permission, Task Backend Business Rules, OPM 73366
        public const string OPERATION_PROTECT_FIELD = "ProtectField";
        public const string OPERATION_READ_TEMPLATE = "ReadTemplate";
        public const string OPERATION_WRITE_TEMPLATE = "WriteTemplate";
        public const string OPERATION_RUN_PML_REGISTERED_LOANS = "RunPmlForRegisteredLoans";
        public const string OPERATION_RUN_PML_ALL_LOANS = "RunPmlForAllLoans";
        public const string OPERATION_RUN_PML_TO_STEP_3 = "RunPmlToStep3";
        public const string OPERATION_REQUEST_RATE_LOCK_MANUAL_RATE_LOCK_BY_LOCK_DESK = "RequestRateLockRequiresManualLock";
        public const string OPERATION_REQUEST_RATE_LOCK_AUTO_RATE_LOCK = "RequestRateLockCausesLock";
        public const string OPERATION_DELETE_LOAN = "DeleteLoan";
        public const string OPERATION_USE_OLD_SECURITY_MODEL_PML = "UseOldSecurityModelForPML";
        public const string OPERATION_DOC_MAGIC_GENERATE_DOCS = "DocMagicGenerateDocs";
        public const string OPERATION_EXTEND_LOCK = "ExtendLock";
        public const string OPERATION_FLOAT_DOWN_LOCK = "FloatDownLoack";
        public const string OPERATION_RATE_RE_LOCK = "RateReLock";
        public const string OPERATION_DELETE_APP = "DeleteApplication";
        public const string OPERATION_ORDER_APPRAISAL = "OrderAppraisal";
        public const string OPERATION_PRESAVE_VALIDATION = "PreSaveValidation"; // 3/18/2014 dd - OPM 144112
        public const string OPERATION_IMPORT_INTO_EXISTING_FILE = "ImportIntoExistingFile";
        public const string OPERATION_MANUALLY_OVERRIDE_QM_SETTINGS = "ManuallyOverrideQMSettings";
        public const string OPERATION_LOAN_STATUS_CHANGE_DOCUMENT_CHECK = "LoanStatusChangeToDocumentCheck";
        public const string OPERATION_LOAN_STATUS_CHANGE_CONDITION_REVIEW = "LoanStatusChangeToConditionReview";

        // These were pulled out of ConstApp.
        private static Dictionary<string, string> friendlyOpNames = new Dictionary<string, string>()
        {
            {OPERATION_READ_LOAN, "Read Loan"},
            {OPERATION_WRITE_LOAN, "Write Loan"},
            {OPERATION_WRITE_FIELD, "Write: Field Level"},
            {OPERATION_PROTECT_FIELD, "Protection: Field Level"},
            {OPERATION_RUN_PML_REGISTERED_LOANS, "Run PML (registered program only)"},
            {OPERATION_RUN_PML_ALL_LOANS, "Run PML (all programs)"},
            {OPERATION_RUN_PML_TO_STEP_3, "Run PML (to step 3)"},
            {OPERATION_REQUEST_RATE_LOCK_MANUAL_RATE_LOCK_BY_LOCK_DESK, "Request Rate Lock (manual lock)"},
            {OPERATION_REQUEST_RATE_LOCK_AUTO_RATE_LOCK, "Request Rate Lock (auto lock)"},
            {OPERATION_DELETE_LOAN, "Delete Loan"},
            {OPERATION_REGISTER_LOAN, "Register Loan"},
            {OPERATION_USE_OLD_SECURITY_MODEL_PML, "Use Old Security Model For PML"},
            {OPERATION_DOC_MAGIC_GENERATE_DOCS, "Generate Docs" },
            {OPERATION_EXTEND_LOCK , "Extend Lock"},
            {OPERATION_FLOAT_DOWN_LOCK, "Float Down Lock"},
            {OPERATION_RATE_RE_LOCK, "Rate Re-Lock"},
            {OPERATION_DELETE_APP, "Delete Application"},
            {OPERATION_ORDER_APPRAISAL, "Order Appraisal"},
            {OPERATION_PRESAVE_VALIDATION, "Pre-Saved Validation"},
            {OPERATION_IMPORT_INTO_EXISTING_FILE, "Import FNM 3.2 Into Existing File"},
            {OPERATION_MANUALLY_OVERRIDE_QM_SETTINGS, "Manually override QM third party and affiliate settings"}
        };

        // This was pulled out of ConstApp.
        public static string GetFriendlyOpName(string op)
        {
            try
            {
                return friendlyOpNames[op];
            }
            catch (KeyNotFoundException)
            {
                return op;
            }
        }
        #endregion
    }
}
