﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using LendersOffice.Security;
using LendOSolnTest.Common;
using LendersOffice.LoanSearch;
using System.Data;
using DataAccess;


namespace LendOSolnTest.LoanSearch
{
    [TestFixture]
    public class LendingQBSearchTest
    {
        [Test]
        public void TestSimpleSearch()
        {
            LendingQBSearchFilter searchFilter = new LendingQBSearchFilter();
            searchFilter.aBLastNm = "Testcase";

            TestAccountType[] accountTypeList = { /*TestAccountType.Corporate_LoanOfficer, */TestAccountType.Branch1_LenderAccountExec, TestAccountType.Manager };

            foreach (TestAccountType account in accountTypeList)
            {
                AbstractUserPrincipal principal = LoginTools.LoginAs(account);
                int totalLoanCount;
                DataSet ds = LendingQBSearch.Search(principal, searchFilter, "aBLastNm ASC", 0, 50, out totalLoanCount);
            }

        }

        [Test]
        public void TestMyLead()
        {
            TestAccountType[] accountTypeList = { /*TestAccountType.Corporate_LoanOfficer, */ TestAccountType.Branch1_LenderAccountExec, TestAccountType.Manager };

            foreach (TestAccountType account in accountTypeList)
            {
                AbstractUserPrincipal principal = LoginTools.LoginAs(account);
                DataSet ds = LendingQBSearch.ListMyLeads(principal);

            }

        }

        [Test]
        public void TestUnassignedLoans()
        {
            TestAccountType[] accountTypeList = { /*TestAccountType.Corporate_LoanOfficer, */ TestAccountType.Branch1_LenderAccountExec, TestAccountType.Manager };

            foreach (TestAccountType account in accountTypeList)
            {
                AbstractUserPrincipal principal = LoginTools.LoginAs(account);
                DataSet ds = LendingQBSearch.ListUnassignedLoans(principal);

            }

        }

        [Test]
        public void TestUnssiangedLeads()
        {
            TestAccountType[] accountTypeList = { /*TestAccountType.Corporate_LoanOfficer, */ TestAccountType.Branch1_LenderAccountExec, TestAccountType.Manager };

            foreach (TestAccountType account in accountTypeList)
            {
                AbstractUserPrincipal principal = LoginTools.LoginAs(account);
                DataSet ds = LendingQBSearch.ListUnassignedLeads(principal);

            }

        }

        //[Test]
        //opm
        public void TestListRecentLoansForMobileInXmlFormat()
        {
            TestAccountType[] accountTypeList = { TestAccountType.Branch1_LenderAccountExec, TestAccountType.Manager };

            foreach (TestAccountType account in accountTypeList)
            {
                AbstractUserPrincipal principal = LoginTools.LoginAs(account);
                string xml = LendingQBSearch.ListRecentLoansForMobileInXmlFormat(principal);
                Tools.LogInfo(xml);
            }

        }
    }
}
