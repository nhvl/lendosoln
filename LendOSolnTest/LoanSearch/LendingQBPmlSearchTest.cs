﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using LendersOffice.LoanSearch;
using System.Data;
using LendersOffice.Security;
using LendOSolnTest.Common;

namespace LendOSolnTest.LoanSearch
{
    [TestFixture]
    public class LendingQBPmlSearchTest
    {
        [Test]
        public void TestPmlSearch()
        {
            LendingQBSearchFilter searchFilter = new LendingQBSearchFilter();
            searchFilter.sStatusDateT = -1;

            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.Pml_User0);
            DataSet ds = LendingQBPmlSearch.Search(principal, searchFilter, 50);
        }

        [Test]
        public void TestPmlSearchWithBrokerProcessor()
        {
            LendingQBSearchFilter searchFilter = new LendingQBSearchFilter();
            searchFilter.sStatusDateT = -1;

            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.Pml_BP1);
            DataSet ds = LendingQBPmlSearch.Search(principal, searchFilter, 50);
        }

        [Test]
        public void TestPmlSearchWithSupervisor()
        {
            LendingQBSearchFilter searchFilter = new LendingQBSearchFilter();
            searchFilter.sStatusDateT = -1;

            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.Pml_Supervisor);
            DataSet ds = LendingQBPmlSearch.Search(principal, searchFilter, 50);
        }

    }
}
