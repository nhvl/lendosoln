/// <copyright file="FakeFannieMae32Importer.cs" company="MeridianLink">
///     Copyright (c) MeridianLink. All rights reserved.
/// </copyright>
/// <summary>
/// Author: Michael Leinweaver
/// Date:   11/17/2016
/// </summary>
namespace LendOSolnTest.Fakes
{
    using LendersOffice.Constants;
    using LendersOffice.Conversions;

    /// <summary>
    /// Provides a means for faking the loan creation within the
    /// Fannie Mae 3.2 importer.
    /// </summary>
    internal class FakeFannieMae32Importer : FannieMae32Importer
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FakeFannieMae32Importer"/> class.
        /// </summary>
        /// <param name="source">
        /// The import source for the fake.
        /// </param>
        public FakeFannieMae32Importer(FannieMaeImportSource source) : base(source)
        {
        }

        /// <summary>
        /// Initializes the loan for the fake.
        /// </summary>
        protected override void Initialize()
        {
            this.m_dataLoan = FakePageData.Create();
            this.m_dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            this.m_dataLoan.SetFormatTarget(DataAccess.FormatTarget.FannieMaeMornetPlus);
        }
    }
}