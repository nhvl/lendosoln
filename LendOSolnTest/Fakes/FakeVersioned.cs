﻿namespace LendOSolnTest.Fakes
{
    using System;
    using global::CommonProjectLib.Caching;

    public class FakeVersioned : IVersionable
    {
        public Guid TestId { get; set; } = Guid.NewGuid();

        public long Version { get; set; }
    }
}
