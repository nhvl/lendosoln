﻿/// <copyright file="FakeMismo33RequestProvider.cs" company="MeridianLink">
///     Copyright (c) MeridianLink. All rights reserved.
/// </copyright>
/// <summary>
/// Author: Michael Leinweaver
/// Date:   11/23/2016
/// </summary>
namespace LendOSolnTest.Fakes
{
    using System;
    using DataAccess;
    using LendersOffice.Common.SerializationTypes;
    using LendersOffice.Conversions.Mismo3.Version3;

    /// <summary>
    /// Provides a means for faking the MISMO 3.3 request provider.
    /// </summary>
    internal class FakeMismo33RequestProvider : Mismo33RequestProvider
    {
        /// <summary>
        /// Prevents a default instance of the <see cref="FakeMismo33RequestProvider"/>
        /// class from being created.
        /// </summary>
        private FakeMismo33RequestProvider() : base(
            loanIdentifier: Guid.Empty,
            vendorAccountID: null,
            transactionID: Guid.NewGuid().ToString(),
            docPackage: null,
            includeIntegratedDisclosureExtension: false,
            useTemporaryArchive: false,
            principal: null)
        {
        }

        /// <summary>
        /// Serializes the message for the fake request.
        /// </summary>
        /// <returns>
        /// The serialized fake request.
        /// </returns>
        public static string SerializeMessage()
        {
            return Mismo33RequestProvider.SerializeMessage(new FakeMismo33RequestProvider());
        }

        /// <summary>
        /// Initializes the fake loan for the provider.
        /// </summary>
        /// <param name="loanIdentifier">
        /// The parameter is not used.
        /// </param>
        /// <returns>
        /// A test loan populated with initial data.
        /// </returns>
        protected override CPageData InitPageData(Guid loanIdentifier)
        {
            var loanData = FakePageData.Create();
            loanData.InitLoad();

            loanData.SetFormatTarget(FormatTarget.MismoClosing);
            loanData.ByPassFieldSecurityCheck = true;

            // Populate the test loan with dummy data necessary for a DOWN_PAYMENT container.
            loanData.sPurchPrice = 100000.0M;
            loanData.sEquityCalc = 20.0M;
            loanData.sDwnPmtSrc = "Trust Funds";
            loanData.sDwnPmtSrcExplain = "Down payment from a trust.";

            return loanData;
        }
    }
}
