﻿/// <copyright file="FakePageDataUtilities.cs" company="MeridianLink">
///     Copyright (c) MeridianLink. All rights reserved.
/// </copyright>
/// <summary>
/// Author: Michael Leinweaver
/// Date:   8/12/2016
/// </summary>
namespace LendOSolnTest.Fakes
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Reflection;
    using DataAccess;
    using LendOSolnTest.Common;

    /// <summary>
    /// Provides a set of utilities for faking loans under test.
    /// </summary>
    internal class FakePageDataUtilities
    {
        /// <summary>
        /// Represents the number of applications that should be added to
        /// the in-memory loan to be extracted.
        /// </summary>
        public const int FakeApplicationCount = 5;

        /// <summary>
        /// Represents the <see cref="IDataContainer"/> for the original loan values
        /// of the in-memory loan.
        /// </summary>
        private readonly DataRowContainer originalLoanDataContainer;

        /// <summary>
        /// Represents the list of <see cref="IDataContainer"/> instances for the
        /// original application values of the in-memory loan.
        /// </summary>
        private readonly List<DataRowContainer> originalAppDataContainer = new List<DataRowContainer>(FakeApplicationCount);

        /// <summary>
        /// Represents the mapping between field name and value for the original FileDB
        /// field content.
        /// </summary>
        private readonly Dictionary<FakedFileDbField, string> originalFileDbFieldData = new Dictionary<FakedFileDbField, string>();

        /// <summary>
        /// Represents the current loan data for a fake loan under test.
        /// </summary>
        private DataRowContainer currentLoanDataContainer;

        /// <summary>
        /// Represents the current application data for a fake loan under test.
        /// </summary>
        private List<DataRowContainer> currentAppDataContainer = new List<DataRowContainer>(FakeApplicationCount);

        /// <summary>
        /// Represents the mapping between field name and value for the current FileDB
        /// field content.
        /// </summary>
        private Dictionary<FakedFileDbField, string> currentFileDbFieldData = new Dictionary<FakedFileDbField, string>();

        /// <summary>
        /// Initializes static members of the <see cref="FakePageDataUtilities"/> class.
        /// </summary>
        static FakePageDataUtilities()
        {
            Instance = new FakePageDataUtilities();
        }

        /// <summary>
        /// Prevents a default instance of the <see cref="FakePageDataUtilities"/> class
        /// from being created.
        /// </summary>
        private FakePageDataUtilities()
        {
            var creator = CLoanFileCreator.GetCreator(
                LoginTools.LoginAs(TestAccountType.LoTest001),
                LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);

            var loanId = creator.CreateBlankLoanFile();

            var dataLoan = new CPageBaseWrapped(
                loanId,
                nameof(FakePageDataUtilities),
                CSelectStatementProvider.GetProviderFor(E_ReadDBScenarioType.InputNothing_NeedAllFields, inputFields: null));

            for (int i = 0; i < FakeApplicationCount; i++)
            {
                dataLoan.AddNewApp();
            }

            dataLoan.InitSave(LendersOffice.Constants.ConstAppDavid.SkipVersionCheck);

            var bindingFlags = BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance;
            var loanDataContainer = dataLoan.GetType().GetField("m_rowLoan", bindingFlags).GetValue(dataLoan) as DataRowContainer;
            this.originalLoanDataContainer = new DataRowContainer(this.CloneDataRow(loanDataContainer.DataRow));

            for (int i = 0; i < FakeApplicationCount; i++)
            {
                var appData = dataLoan.GetAppData(i);
                var appDataContainer = appData.GetType().GetField("m_rowApp", bindingFlags).GetValue(appData) as DataRowContainer;
                this.originalAppDataContainer.Add(new DataRowContainer(this.CloneDataRow(appDataContainer.DataRow)));
            }

            foreach (FakedFileDbField field in Enum.GetValues(typeof(FakedFileDbField)))
            {
                this.originalFileDbFieldData[field] = this.GetOriginalFileDbField(field, dataLoan);
            }

            this.Reset();

            // Save an indicator in custom field 60 so this loan can be cleaned
            // up using the loan deletion tool.
            dataLoan.sCustomField60Notes = nameof(FakePageData);
            dataLoan.sCustomField60Bit = true;
            dataLoan.Save();

            dataLoan = null;
        }

        /// <summary>
        /// Represents the set of FileDB fields that are faked.
        /// </summary>
        /// <remarks>
        /// To fake a new FileDB field, add a new constant with the name of the
        /// field to this enumeration and then modify the switch statement in 
        /// <see cref="GetOriginalFileDbField(FakedFileDbField, CPageBase)"/> 
        /// to return the value of the field from the in-memory loan.
        /// </remarks>
        private enum FakedFileDbField
        {
            /// <summary>
            /// The closing cost JSON content.
            /// </summary>
            sClosingCostSetJsonContent,

            /// <summary>
            /// The PML certificate XML content.
            /// </summary>
            sPmlCertXmlContent,

            /// <summary>
            /// The agent data set XML content.
            /// </summary>
            sAgentXmlContent,

            /// <summary>
            /// The application liability XML content.
            /// </summary>
            aLiaXmlContent,

            /// <summary>
            /// The application asset XML content.
            /// </summary>
            aAssetXmlContent,

            /// <summary>
            /// The condition data set XML content.
            /// </summary>
            sCondXmlContent,

            /// <summary>
            /// The DU Third-Party Service providers JSON content.
            /// </summary>
            sDuThirdPartyProviders,

            /// <summary>
            /// The result XML of an FHA case query.
            /// </summary>
            sFHACaseQueryResultXmlContent,

            /// <summary>
            /// The result XML of an FHA case number query.
            /// </summary>
            sFHACaseNumberResultXmlContent,

            /// <summary>
            /// The result XML of an FHA CAVIRS query.
            /// </summary>
            sFHACAVIRSResultXmlContent,

            /// <summary>
            /// The XML content for the last PML data snapshot.
            /// </summary>
            sProdPmlDataUsedForLastPricingXmlContent,

            /// <summary>
            /// The TOTAL Scorecard certificate XML content.
            /// </summary>
            sTotalScoreCertificateXmlContent,

            /// <summary>
            /// The TOTAL Scorecard temporary certificate XML content.
            /// </summary>
            sTotalScoreTempCertificateXmlContent,

            /// <summary>
            /// The preparer data set XML content.
            /// </summary>
            sPreparerXmlContent
        }

        /// <summary>
        /// Gets the instance of the singleton.
        /// </summary>
        /// <value>
        /// The singleton instance of the <see cref="FakePageDataUtilities"/> class.
        /// </value>
        public static FakePageDataUtilities Instance { get; }

        /// <summary>
        /// Gets the value for the list of fields where edits are tracked before a save.
        /// </summary>
        /// <value>
        /// The <see cref="IEnumerable{T}"/> containing the tracked fields.
        /// </value>
        /// <remarks>
        /// <see cref="LendersOffice.Audit.TrackedField.Retrieve()"/> does not rely on a
        /// static constructor and instead uses lazy loading. To prevent an expensive reload
        /// of the tracked fields in case the list of fields is reclaimed by the garbage collector
        /// in-between tests, this utilities class will call the method once and keep the list of
        /// fields stored in this property.
        /// </remarks>
        public IEnumerable<string> TrackedFields { get; private set; } = LendersOffice.Audit.TrackedField.Retrieve().Keys;

        /// <summary>
        /// Gets a loan data copy for a fake.
        /// </summary>
        /// <returns>
        /// A <see cref="DataRow"/> copy of the values for the in-memory loan.
        /// </returns>
        public DataRow GetLoanDataContainerCopy()
        {
            return this.CloneDataRow(this.currentLoanDataContainer.DataRow);
        }

        /// <summary>
        /// Gets an application data copy for a fake. A parameter controls
        /// the application selected.
        /// </summary>
        /// <param name="index">
        /// The index of the application to obtain data.
        /// </param>
        /// <returns>
        /// A <see cref="DataRow"/> copy of the values for the application.
        /// </returns>
        public DataRow GetAppContainerCopy(int index)
        {
            if (index < 0 || index > this.currentAppDataContainer.Count - 1)
            {
                throw new ArgumentException($"Application for index {index} does not exist.", nameof(index));
            }

            return this.CloneDataRow(this.currentAppDataContainer[index].DataRow);
        }

        /// <summary>
        /// Gets the FileDB content for the specified <paramref name="fieldName"/>.
        /// </summary>
        /// <param name="fieldName">
        /// The name of the FileDB field.
        /// </param>
        /// <returns>
        /// The content for the specified field.
        /// </returns>
        public string GetFileDBField(string fieldName)
        {
            FakedFileDbField fakedFieldId;
            string fieldValue;
            if (!Enum.TryParse(fieldName, ignoreCase: true, result: out fakedFieldId) ||
                !this.currentFileDbFieldData.TryGetValue(fakedFieldId, out fieldValue))
            {
                throw new ArgumentException($"Could not find unknown field '{fieldName}'.", nameof(fieldName));
            }

            return string.Copy(fieldValue);
        }

        /// <summary>
        /// Sets the FileDB content for the specified <paramref name="fieldName"/>.
        /// </summary>
        /// <param name="fieldName">
        /// The name of the FileDB field.
        /// </param>
        /// <param name="newValue">
        /// The new value for the field.
        /// </param>
        public void SetFileDBField(string fieldName, string newValue)
        {
            FakedFileDbField fakedFieldId;
            if (!Enum.TryParse(fieldName, ignoreCase: true, result: out fakedFieldId))
            {
                throw new ArgumentException($"Could not set value for unknown field '{fieldName}'.", nameof(fieldName));
            }

            this.currentFileDbFieldData[fakedFieldId] = newValue;
        }

        /// <summary>
        /// Sets the current data for a fake loan.
        /// </summary>
        /// <param name="currentLoanRow">
        /// The <see cref="DataRow"/> containing the current values for the fake loan.
        /// </param>
        public void SetLoanData(DataRow currentLoanRow)
        {
            this.currentLoanDataContainer = new DataRowContainer(this.CloneDataRow(currentLoanRow));
        }

        /// <summary>
        /// Sets the current data for a fake application. A parameter specifies the application
        /// to set the data.
        /// </summary>
        /// <param name="index">
        /// The index of the application to set data.
        /// </param>
        /// <param name="currentAppRow">
        /// The <see cref="DataRow"/> containing the current values for the fake application.
        /// </param>
        public void SetAppData(int index, DataRow currentAppRow)
        {
            if (index < 0 || index > this.currentAppDataContainer.Count - 1)
            {
                throw new ArgumentException($"Application for index {index} does not exist.", nameof(index));
            }

            this.currentAppDataContainer[index] = new DataRowContainer(this.CloneDataRow(currentAppRow));
        }

        /// <summary>
        /// Resets the current loan data to the original values obtained from the in-memory loan.
        /// </summary>
        /// <remarks>
        /// This method should be called in the teardown for any test fixture where 
        /// <see cref="FakePageData.Save()"/> was called to prevent changes from one fixture
        /// impacting the results of other fixtures.
        /// </remarks>
        public void Reset()
        {
            this.currentAppDataContainer = new List<DataRowContainer>(this.originalAppDataContainer);
            this.SetLoanData(this.originalLoanDataContainer.DataRow);

            foreach (FakedFileDbField fieldId in Enum.GetValues(typeof(FakedFileDbField)))
            {
                this.currentFileDbFieldData[fieldId] = this.originalFileDbFieldData[fieldId];
            }
        }

        /// <summary>
        /// Obtains the original value for the specified <see cref="field"/>.
        /// </summary>
        /// <param name="field">
        /// The <see cref="FakedFileDbField"/> to obtain a value.
        /// </param>
        /// <param name="dataLoan">
        /// The in-memory loan containing the original value.
        /// </param>
        /// <returns>
        /// The FileDB content for the field.
        /// </returns>
        private string GetOriginalFileDbField(FakedFileDbField field, CPageBase dataLoan)
        {
            switch (field)
            {
                case FakedFileDbField.sClosingCostSetJsonContent:
                    return dataLoan.sClosingCostSet.ToJson(includeOrigCompFee: true);
                case FakedFileDbField.sPmlCertXmlContent:
                    return dataLoan.sPmlCertXmlContent.Value;
                case FakedFileDbField.sAgentXmlContent:
                    return dataLoan.sAgentDataSet.GetXml();
                case FakedFileDbField.aLiaXmlContent:
                    return dataLoan.GetAppData(0).aLiaXmlContent.Value;
                case FakedFileDbField.aAssetXmlContent:
                    return dataLoan.GetAppData(0).aAssetXmlContent.Value;
                case FakedFileDbField.sCondXmlContent:
                    return dataLoan.sCondDataSet.Value.GetXml();
                case FakedFileDbField.sDuThirdPartyProviders:
                    return dataLoan.sDuThirdPartyProviders.SerializeJson();
                case FakedFileDbField.sFHACaseQueryResultXmlContent:
                    return dataLoan.sFHACaseQueryResultXmlContent;
                case FakedFileDbField.sFHACaseNumberResultXmlContent:
                    return dataLoan.sFHACaseNumberResultXmlContent;
                case FakedFileDbField.sFHACAVIRSResultXmlContent:
                    return dataLoan.sFHACAVIRSResultXmlContent;
                case FakedFileDbField.sProdPmlDataUsedForLastPricingXmlContent:
                    // This property is private on the loan file. To prevent exposing
                    // this field and because the new loan we created for the fake 
                    // should not have data for any prior pricing runs stored, return 
                    // the empty string.
                    return string.Empty;
                case FakedFileDbField.sTotalScoreCertificateXmlContent:
                    return dataLoan.sTotalScoreCertificateXmlContent.Value;
                case FakedFileDbField.sTotalScoreTempCertificateXmlContent:
                    return dataLoan.sTotalScoreTempCertificateXmlContent.Value;
                case FakedFileDbField.sPreparerXmlContent:
                    return dataLoan.sPreparerDataSet.GetXml();
                default:
                    throw new UnhandledEnumException(field);
            }
        }

        /// <summary>
        /// Clones the specified <see cref="source"/>.
        /// </summary>
        /// <param name="source">
        /// The <see cref="DataRow"/> to clone.
        /// </param>
        /// <returns>
        /// A copy of the specified source.
        /// </returns>
        private DataRow CloneDataRow(DataRow source)
        {
            var dataTable = source.Table.Clone();
            dataTable.ImportRow(source);
            return dataTable.Rows[0];
        }
    }
}
