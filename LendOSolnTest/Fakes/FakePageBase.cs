﻿/// <copyright file="FakeAppBase.cs" company="MeridianLink">
///     Copyright (c) MeridianLink. All rights reserved.
/// </copyright>
/// <summary>
/// Author: Michael Leinweaver
/// Date:   8/12/2016
/// </summary>
namespace LendOSolnTest.Fakes
{
    using System;
    using System.Data;
    using DataAccess;
    using LendersOffice.Integration.UcdDelivery;

    /// <summary>
    /// Provides a fake for the internal implementation of a loan.
    /// </summary>
    internal class FakePageBase : CPageBaseWrapped
    {
        /// <summary>
        /// Fake UCD delivery collection.
        /// </summary>
        /// <remarks>
        /// TODO: LOAD THIS CORRECTLY.
        /// </remarks>
        private UcdDeliveryCollection fakeUcdDeliveryCollection;

        /// <summary>
        /// Determines whether e-consent monitoring is enabled for the fake.
        /// </summary>
        private bool enableEConsentMonitoringAutomation = true;

        /// <summary>
        /// Initializes a new instance of the <see cref="FakePageBase"/> class.
        /// </summary>
        /// <param name="loanValues">
        /// The <see cref="DataRow"/> containing the values for the fake.
        /// </param>
        /// <param name="appCount">
        /// The number of applications that the fake should have.
        /// </param>
        public FakePageBase(DataRow loanValues, int appCount) : base(loanValues, nameof(FakePageBase), new FakeSelectStatementProvider())
        {
            this.m_dataApps = new FakeAppBase[appCount];

            for (var i = 0; i < appCount; i++)
            {
                this.m_dataApps[i] = new FakeAppBase(this, new DataRowContainer(FakePageDataUtilities.Instance.GetAppContainerCopy(i)));
            }

            this.fakeUcdDeliveryCollection = UcdDeliveryCollection.CreateEmpty(this.sLId, this.sBrokerId);
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="Assets"/> collection or any of its related associations have tracked changes, which is always false, since <see cref="FakePageBase"/> does not have a collection container.
        /// </summary>
        public override bool IsAssetsCollectionModified => false;

        /// <summary>
        /// Gets a value indicating whether the <see cref="EmploymentRecords"/> collection or any of its related associations have tracked changes, which is always false, since <see cref="FakePageBase"/> does not have a collection container.
        /// </summary>
        public override bool IsEmploymentRecordsCollectionModified => false;

        /// <summary>
        /// Gets a value indicating whether the <see cref="Liabilities"/> collection or any of its related associations have tracked changes, which is always false, since <see cref="FakePageBase"/> does not have a collection container.
        /// </summary>
        public override bool IsLiabilitiesCollectionModified => false;

        /// <summary>
        /// Gets a value indicating whether the <see cref="RealProperties"/> collection or any of its related associations have tracked changes, which is always false, since <see cref="FakePageBase"/> does not have a collection container.
        /// </summary>
        public override bool IsRealPropertiesCollectionModified => false;

        /// <summary>
        /// Gets the FileDB content for the specified <paramref name="fieldName"/>.
        /// </summary>
        /// <param name="fieldName">
        /// The name of the FileDB field.
        /// </param>
        /// <returns>
        /// The content for the specified field.
        /// </returns>
        public override string GetFileDBField(string fieldName)
        {
            return FakePageDataUtilities.Instance.GetFileDBField(fieldName);
        }

        /// <summary>
        /// Sets the FileDB content for the specified <paramref name="fieldName"/>.
        /// </summary>
        /// <param name="fieldName">
        /// The name of the FileDB field.
        /// </param>
        /// <param name="newValue">
        /// The new value for the field.
        /// </param>
        public override void SetFileDBField(string fieldName, string newValue)
        {
            FakePageDataUtilities.Instance.SetFileDBField(fieldName, newValue);
        }

        /// <summary>
        /// Adds a new application to the fake.
        /// </summary>
        /// <returns>
        /// The index in the array of applications for the new application.
        /// </returns>
        public override int AddNewApp()
        {
            var newApps = new CAppBase[this.nApps + 1];
            this.InitializeLoanLqbCollectionContainer();

            Array.Copy(this.m_dataApps, newApps, this.nApps);

            newApps[this.nApps] = new FakeAppBase(this, new DataRowContainer(FakePageDataUtilities.Instance.GetAppContainerCopy(0)));

            this.m_dataApps = newApps;

            return newApps.Length - 1;
        }

        /// <summary>
        /// Deletes the application at the specified <paramref name="indexToDelete"/>.
        /// </summary>
        /// <param name="indexToDelete">
        /// The index of the application to delete.
        /// </param>
        /// <returns>
        /// True if the deletion was successful, false otherwise.
        /// </returns>
        public override bool DelApp(int indexToDelete)
        {
            if (indexToDelete < 0 || indexToDelete > (this.nApps - 1))
            {
                return false;
            }

            var newApps = new FakeAppBase[this.nApps - 1];

            var appIndex = 0;

            for (int i = 0; i < this.nApps; i++)
            {
                if (appIndex == indexToDelete)
                {
                    continue;
                }

                newApps[appIndex] = (FakeAppBase)this.m_dataApps[i];

                ++appIndex;
            }

            this.m_dataApps = newApps;

            return true;
        }

        /// <summary>
        /// Initializes loan data for the fake.
        /// </summary>
        public override void InitLoad()
        {
            this.m_flush = false;
            this.InitializeLoanLqbCollectionContainer();

            this.DataState = E_DataState.InitLoad;

            this.SetEncryptionData();

            (this.GetDataRow() as DataRowContainer).DataRow.BeginEdit();

            for (var i = 0; i < this.nApps; i++)
            {
                (((FakeAppBase)this.m_dataApps[i]).AppRow as DataRowContainer).DataRow.BeginEdit();
            }
        }

        /// <summary>
        /// Prepares the loan for saving.
        /// </summary>
        /// <param name="expectedVersion">
        /// The expected version of the loan to save.
        /// </param>
        public override void InitSave(int expectedVersion)
        {
            this.SetEncryptionData();
            this.InitializeLoanLqbCollectionContainer();

            this.AddFieldsToKeepTrackOfSnapshot(FakePageDataUtilities.Instance.TrackedFields);

            this.TakeSnapshotOfTrackingFields(TrackFieldStatusT.Current);

            this.m_flush = false;

            this.DataState = E_DataState.InitSave;

            (this.GetDataRow() as DataRowContainer).DataRow.BeginEdit();

            for (var i = 0; i < this.nApps; i++)
            {
                (((FakeAppBase)this.m_dataApps[i]).AppRow as DataRowContainer).DataRow.BeginEdit();
            }
        }

        /// <summary>
        /// Completes the initialization for a loan load.
        /// </summary>
        public override void DoneLoad()
        {
            this.DataState = E_DataState.Normal;
        }

        /// <summary>
        /// Saves the loan data.
        /// </summary>
        public override void Save()
        {
            if (this.CalcModeT == E_CalcModeT.PriceMyLoan &&
                this.IsPmlLoansModified)
            {
                this.CalculatePmlLoans();
            }

            this.Flush();

            if (this.updatePmlBrokerId)
            {
                var newPmlBrokerId = this.UpdatePmlBrokerId();

                this.updatePmlBrokerId = false;

                if (newPmlBrokerId != null)
                {
                    this.UpdatePmlBrokerIdWithSetterBypass(newPmlBrokerId.Value);
                }
            }

            var loanDataRow = (this.GetDataRow() as DataRowContainer).DataRow;

            loanDataRow.AcceptChanges();

            FakePageDataUtilities.Instance.SetLoanData(loanDataRow);

            for (var i = 0; i < this.nApps; i++)
            {
                var appDataRow = (((FakeAppBase)this.m_dataApps[i]).AppRow as DataRowContainer).DataRow;

                appDataRow.AcceptChanges();

                FakePageDataUtilities.Instance.SetAppData(i, appDataRow);
            }

            this.DataState = E_DataState.Normal;
        }

        /// <summary>
        /// Disables e-consent monitoring automation for the file.
        /// </summary>
        public void DisableEConsentMonitoringAutomation()
        {
            this.enableEConsentMonitoringAutomation = false;
        }

        /// <summary>
        /// Gets the FileDB content for the specified <paramref name="fieldName"/>. 
        /// A parameter specifies whether an on-demand migration should be performed
        /// to move the field from the database to FileDB.
        /// </summary>
        /// <param name="fieldName">
        /// The name of the FileDB field.
        /// </param>
        /// <param name="requireOnDemandMigration">
        /// Whether an on-demand migration should be performed. This parameter is not used.
        /// </param>
        /// <returns>
        /// The content for the specified field.
        /// </returns>
        protected override string GetFileDBTextField(string fieldName, bool requireOnDemandMigration = false)
        {
            return FakePageDataUtilities.Instance.GetFileDBField(fieldName);
        }

        /// <summary>
        /// Sets the FileDB content for the specified <paramref name="fieldName"/>.
        /// </summary>
        /// <param name="fieldName">
        /// The name of the FileDB field.
        /// </param>
        /// <param name="newValue">
        /// The new value for the field.
        /// </param>
        protected override void SetFileDBTextField(string fieldName, string newValue)
        {
            FakePageDataUtilities.Instance.SetFileDBField(fieldName, newValue);
        }

        /// <summary>
        /// Gets the ucd delivery collection. 
        /// </summary>
        /// <value>The UCD delivery collection.</value>
        public override UcdDeliveryCollection sUcdDeliveryCollection
        {
            get
            {
                return this.fakeUcdDeliveryCollection;
            }
        }

        /// <summary>
        /// Gets or Sets the loan file type.
        /// </summary>
        /// <remarks>
        /// Eliminates security checks that only allowed loan file type to be set under certain conditions.
        /// </remarks>
        public override E_sLoanFileT sLoanFileT
        {
            get { return (E_sLoanFileT)GetTypeIndexField("sLoanFileT"); }
            set { SetTypeIndexField("sLoanFileT", (int)value); }
        }

        /// <summary>
        /// Gets a value indicating whether e-consent monitoring automation
        /// is enabled.
        /// </summary>
        protected override bool sIsEnableEConsentMonitoringAutomation => this.enableEConsentMonitoringAutomation;
    }
}
