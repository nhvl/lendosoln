﻿/// <copyright file="FakeAppBase.cs" company="MeridianLink">
///     Copyright (c) MeridianLink. All rights reserved.
/// </copyright>
/// <summary>
/// Author: Michael Leinweaver
/// Date:   8/12/2016
/// </summary>
namespace LendOSolnTest.Fakes
{
    using System;
    using DataAccess;
    using LendersOffice.CreditReport;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Provides a fake for the internal implementation of an application.
    /// </summary>
    internal class FakeAppBase : CAppBase
    {
        /// <summary>
        /// Provides a means for retrieving the fake credit report data.
        /// </summary>
        private readonly Lazy<Sensitive<ICreditReport>> creditReport;

        /// <summary>
        /// Initializes a new instance of the <see cref="FakeAppBase"/> class.
        /// </summary>
        /// <param name="parent">
        /// The parent for the fake application.
        /// </param>
        /// <param name="dataRowContainer">
        /// The wrapper for the <see cref="DataRow"/> containing values for the application.
        /// </param>
        public FakeAppBase(CPageBase parent, DataRowContainer dataRowContainer)
            : base(
                  parent, 
                  new Guid(dataRowContainer["aAppId"].ToString()), 
                  dataRowContainer, 
                  nameof(FakeAppBase))
        {
            this.creditReport = new Lazy<Sensitive<ICreditReport>>(
                this.RetrieveFakeCreditReport, 
                System.Threading.LazyThreadSafetyMode.PublicationOnly);
        }

        /// <summary>
        /// Gets the value for the backing data container of the fake application.
        /// </summary>
        /// <value>
        /// The <see cref="IDataContainer"/> for the fake application.
        /// </value>
        public IDataContainer AppRow
        {
            get { return this.GetDataRow(); }
        }

        /// <summary>
        /// Gets the value for the credit report data for the application.
        /// </summary>
        /// <value>
        /// The <see cref="Sensitive{T}"/> containing the credit report data.
        /// </value>
        public override Sensitive<ICreditReport> CreditReportData
        {
            get
            {
                if (this.m_parent.IsRunningPricingEngine)
                {
                    return this.creditReport.Value;
                }

                return null;
            }
        }

        /// <summary>
        /// Gets the FileDB content for the specified <paramref name="fieldName"/>.
        /// </summary>
        /// <param name="fieldName">
        /// The name of the FileDB field.
        /// </param>
        /// <returns>
        /// The content for the specified field.
        /// </returns>
        public override string GetFileDBField(string fieldName)
        {
            return FakePageDataUtilities.Instance.GetFileDBField(fieldName);
        }

        /// <summary>
        /// Sets the FileDB content for the specified <paramref name="fieldName"/>.
        /// </summary>
        /// <param name="fieldName">
        /// The name of the FileDB field.
        /// </param>
        /// <param name="newValue">
        /// The new value for the field.
        /// </param>
        public override void SetFileDBField(string fieldName, string newValue)
        {
            FakePageDataUtilities.Instance.SetFileDBField(fieldName, newValue);
        }
        
        /// <summary>
        /// Gets the FileDB content for the specified <paramref name="fieldName"/>. 
        /// A parameter specifies whether an on-demand migration should be performed
        /// to move the field from the database to FileDB.
        /// </summary>
        /// <param name="fieldName">
        /// The name of the FileDB field.
        /// </param>
        /// <param name="requireOnDemandMigration">
        /// Whether an on-demand migration should be performed. This parameter is not used.
        /// </param>
        /// <returns>
        /// The content for the specified field.
        /// </returns>
        protected override string GetFileDBTextField(string fieldName, bool requireOnDemandMigration = false)
        {
            return FakePageDataUtilities.Instance.GetFileDBField(fieldName);
        }

        /// <summary>
        /// Sets the FileDB content for the specified <paramref name="fieldName"/>.
        /// </summary>
        /// <param name="fieldName">
        /// The name of the FileDB field.
        /// </param>
        /// <param name="newValue">
        /// The new value for the field.
        /// </param>
        protected override void SetFileDBTextField(string fieldName, string newValue)
        {
            FakePageDataUtilities.Instance.SetFileDBField(fieldName, newValue);
        }

        /// <summary>
        /// Obtains the credit report used by the fake.
        /// </summary>
        /// <returns>
        /// The fake credit report.
        /// </returns>
        private Sensitive<ICreditReport> RetrieveFakeCreditReport()
        {
            var loginNm = GetNameFromCurrentPrincipal("N/A");
            var debugInfo = new CreditReportDebugInfo(this.m_parent.sLId, loginNm);
            return CreditReportFactory.ConstructOptimisticCreditReport(this, debugInfo);
        }
    }
}
