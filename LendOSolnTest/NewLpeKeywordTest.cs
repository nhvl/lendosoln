using System;
using System.Collections;
using NUnit.Framework;
using LendersOfficeApp.los.RatePrice;
using System.Xml;

namespace LendOSolnTest
{
    [TestFixture]
    public class LpeNewKeywordUtilityTest
    {
        void AssertInvalidNewKeyword( string keyword )
        {
            AssertInvalidNewKeyword( keyword, "1" );
        }

        void AssertInvalidNewKeyword( string keyword, string defValue )
        {
            AssertInvalidNewKeyword( keyword, defValue,  LpeNewKeywordUtility.FieldType);
            AssertInvalidNewKeyword( keyword, defValue,  LpeNewKeywordUtility.FunctionIntType);
            AssertInvalidNewKeyword( keyword, defValue,  LpeNewKeywordUtility.FunctionStringType);
        }

        void AssertInvalidNewKeyword( string keyword, string defValue, string category)
        {
            //string errMsg;
            //LpeNewKeywordUtility.IsValidNewKeyword( keyword, out errMsg );
            //Assert.AreNotEqual( "", errMsg, "Invalid keyowrd " + keyword );

            try
            {
                LpeNewKeywordUtility.CreateSymbolEntry( keyword, defValue, category);
            }
            catch
            {                
                return;
            }

            Assert.Fail( "Expect : CreateSymbolEntry( '" + keyword + "' ) throws exception");

        }

        [Test]
        public void NewKeywordMustContainAtleast2CharsTest()
        {
            AssertInvalidNewKeyword( null ); 
            AssertInvalidNewKeyword( "" ); 
            for( int c = 1; c < 255; c++)
                AssertInvalidNewKeyword( new string( (char)c, 1 ) ); 
        }

        [Test]
        public void NewKeywordMustContainAtLeatOneLetterTest()
        {
            AssertInvalidNewKeyword( "__" );
            AssertInvalidNewKeyword( "_1" );
            AssertInvalidNewKeyword( "2_1" );
        }

        [Test]
        public void NumberIsInvalidNewKeywordTest()
        {
            AssertInvalidNewKeyword( "123" );
            AssertInvalidNewKeyword( "123.12" );
        }

        [Test]
        public void NewKeywordContainsOnlyLetterDigitUnderlineCharsTest()
        {
            AssertInvalidNewKeyword( "the=" );
            AssertInvalidNewKeyword( "the:" );
        }

        [Test]
        public void ValidNewKeywordsTest()
        {
            string []keywords = { "a12", "a1_12", "A12", "A12_","2nd", "_2nd" };
            foreach( string keyword in keywords )
            {
                LpeNewKeywordUtility.ExceptionIfInvalidNewKeyword( keyword );
                LpeNewKeywordUtility.CreateSymbolEntry( keyword, "1", LpeNewKeywordUtility.FieldType);
            }

        }

        [Test]
        public void FirstCharCanBeDigitOrUnderlineChar()
        {
            string []keywords = { "2nd", "_2nd" };
            foreach( string keyword in keywords )
            {
                LpeNewKeywordUtility.ExceptionIfInvalidNewKeyword( keyword );
                LpeNewKeywordUtility.CreateSymbolEntry( keyword, "1", LpeNewKeywordUtility.FieldType);
                LpeNewKeywordUtility.CreateSymbolEntry( keyword, "1", LpeNewKeywordUtility.FunctionIntType);
                LpeNewKeywordUtility.CreateSymbolEntry( keyword, "1", LpeNewKeywordUtility.FunctionIntType);
            }

        }


        [Test]
        public void NewKeywordCannotBeReserveWord()
        {
            CSymbolTable tableWithoutNewKeywords = CSymbolTable.CreateSymbolTableWithoutNewKeywords();
            foreach (string key in tableWithoutNewKeywords.Entries.Keys)
            {
                string coreKeyword = key.Split(':')[0];
                AssertInvalidNewKeyword(coreKeyword);
                AssertInvalidNewKeyword(coreKeyword.ToLower());
                AssertInvalidNewKeyword(coreKeyword.ToUpper());

            }

        }

        [Test]
        public void DefValueOfNewKeywordMustBeANumberTest()
        {
            LpeNewKeywordUtility.CreateSymbolEntry( "SomeKeyword", "1", LpeNewKeywordUtility.FieldType);
            AssertInvalidNewKeyword( "SomeKeyword", "a" );
        }

    }

}
