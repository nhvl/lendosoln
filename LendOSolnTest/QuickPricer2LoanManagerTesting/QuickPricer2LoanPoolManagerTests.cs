﻿using System;
using NUnit.Framework;
using LendersOffice.ObjLib.QuickPricer;
using LendersOffice.Security;
using LendOSolnTest.Common;
using DataAccess;
using System.Linq;
using System.Threading;
using System.Security.Principal;
using System.Net.Sockets;
using System.Collections.Generic;
using LendersOffice.Common;
using LendersOffice.Constants;

namespace LendOSolnTest.QuickPricer2LoanPoolTesting
{
    [TestFixture]
    public class QuickPricer2LoanPoolManagerTests
    {
        private IPrincipal OldThreadPrincipal;

        [TestFixtureSetUp]
        public void Setup()
        {
            this.OldThreadPrincipal = Thread.CurrentPrincipal;
            Thread.CurrentPrincipal = LoginTools.LoginAs(TestAccountType.LoanOfficer);
        }
        [TestFixtureTearDown]
        public void TearDown()
        {
            Thread.CurrentPrincipal = this.OldThreadPrincipal;
        }

        [Test]
        public void CreateSandboxedUnusedLoan()
        {
            var oldPrincipal = Thread.CurrentPrincipal;
            Thread.CurrentPrincipal = null;
            var principal = LoginTools.LoginAs(TestAccountType.LoanOfficer) as AbstractUserPrincipal;

            var brokerID = principal.BrokerId;
            var userID = principal.UserId;
            var broker = principal.BrokerDB;
            var loanID = QuickPricer2LoanPoolManager.CreateSandboxedUnusedLoan(principal);

            var dataLoan = new CFullAccessPageData(loanID, new string[] { "sLoanFileT", "IsValid" });
            dataLoan.ByPassFieldSecurityCheck = true;
            dataLoan.AllowLoadWhileQP2Sandboxed = true;
            dataLoan.InitLoad();

            var qpDataLoan = new CFullAccessPageData(broker.QuickPricerTemplateId, new string[] { });
            qpDataLoan.ByPassFieldSecurityCheck = true;
            qpDataLoan.InitLoad();

            Assert.AreEqual(dataLoan.sLoanFileT, E_sLoanFileT.QuickPricer2Sandboxed);
            //Assert.AreEqual(dataLoan.sFileVersion, qpDataLoan.sFileVersion);
            Assert.That(dataLoan.IsValid);
            MLAssert.NotThrowsSpecifically<FormatException>(() => new Guid(dataLoan.sLNm));
            Guid slnmID = new Guid(dataLoan.sLNm);
            Assert.AreEqual(slnmID, dataLoan.sLId);

            Thread.CurrentPrincipal = oldPrincipal;
        }

        [Test]
        public void CreateSandboxedInUseLoan()
        {
            var oldPrincipal = Thread.CurrentPrincipal;
            Thread.CurrentPrincipal = LoginTools.LoginAs(TestAccountType.LoanOfficer);

            var loanID = QuickPricer2LoanPoolManager.CreateSandboxedInUseLoan();

            var dataLoan = new CFullAccessPageData(loanID, new string[] { "sLoanFileT", "IsValid" });
            dataLoan.ByPassFieldSecurityCheck = true;
            dataLoan.AllowLoadWhileQP2Sandboxed = true;
            dataLoan.InitLoad();

            var broker = dataLoan.BrokerDB;

            var qpDataLoan = new CFullAccessPageData(broker.QuickPricerTemplateId, new string[] { });
            qpDataLoan.ByPassFieldSecurityCheck = true;
            qpDataLoan.InitLoad();

            Assert.AreEqual(dataLoan.sLoanFileT, E_sLoanFileT.QuickPricer2Sandboxed);
            //Assert.AreEqual(dataLoan.sFileVersion, qpDataLoan.sFileVersion);
            Assert.That(dataLoan.IsValid);

            Thread.CurrentPrincipal = oldPrincipal;
        }
        /*
        I'm commenting this test out because it is using DbAccessUtils.ExecuteReaderDangerously which I'm replacing
        with code that conforms to the new architecture.  It isn't clear what should happen in unit tests, either
        to read as verification of some activity or to use mocks.  
                [Test]
                public void GetSandboxedUnusedLoanAndMarkAsInUse()
                {
                    var oldPrincipal = Thread.CurrentPrincipal;
                    Thread.CurrentPrincipal = LoginTools.LoginAs(TestAccountType.LoanOfficer);

                    var loanID = QuickPricer2LoanPoolManager.GetSandboxedUnusedLoanAndMarkAsInUse();

                    var dataLoan = new CFullAccessPageData(loanID, new string[] { "sLoanFileT", "IsValid" });
                    dataLoan.ByPassFieldSecurityCheck = true;
                    dataLoan.AllowLoadWhileQP2Sandboxed = true;
                    dataLoan.InitLoad();

                    var broker = dataLoan.BrokerDB;

                    var qpDataLoan = new CFullAccessPageData(broker.QuickPricerTemplateId, new string[] { });
                    qpDataLoan.ByPassFieldSecurityCheck = true;
                    qpDataLoan.InitLoad();

                    Assert.AreEqual(dataLoan.sLoanFileT, E_sLoanFileT.QuickPricer2Sandboxed);
                    //Assert.AreEqual(dataLoan.sFileVersion, qpDataLoan.sFileVersion);
                    Assert.That(dataLoan.IsValid);

                    var query = "select * from QuickPricer_2_LOAN_USAGE WHERE LoanID = '" + loanID + "'";
                    using (var reader = DbAccessUtils.ExecuteReaderDangerously(broker.BrokerID, query, null))
                    {
                        if (reader.Read())
                        {
                            Assert.That((bool)reader["IsInUse"]);
                            Assert.That((int)reader["QuickPricerTemplateFileVersion"] == qpDataLoan.sFileVersion);

                            if (reader.Read())
                            {
                                var msg = "More than one loan was in the QuickPricer_2_LOAN_USAGE table with LoanID " + loanID;
                                throw new CBaseException(msg, msg);
                            }
                        }
                        else
                        {
                            var msg = "Not even one loan was in the QuickPricer_2_LOAN_USAGE table with LoanID " + loanID;
                            throw new CBaseException(msg, msg);
                        }
                    }
                    Thread.CurrentPrincipal = oldPrincipal;
                }
        */
        [Test]
        public void InUseLoanCreationFailsIfNoPrincipal()
        {
            var oldPrincipal = Thread.CurrentPrincipal;
            Thread.CurrentPrincipal = null;

            MLAssert.ThrowsSpecifically<NullReferenceException>(() => QuickPricer2LoanPoolManager.CreateSandboxedInUseLoan());

            Thread.CurrentPrincipal = oldPrincipal;
        }

        private class LoanDeletionInfo
        {
            public Guid LoanID { get; set;}
            public bool CanDelete { get; set; }
            public string ReasonCantDelete { get; set;}
            public override string  ToString()
            {
 	            return "slid: " + LoanID + " IsDeletable: " + CanDelete + " b/c: " + ReasonCantDelete;
            }
        }
        //[Test] // Ignoring for now since it takes a long time and since the deletion process is running.
        public void FindLoansForDeletion()
        {
            var broker = PrincipalFactory.CurrentPrincipal.BrokerDB;
            var oldLoans = QuickPricer2LoanPoolManager.FindOldInUseLoansAtBroker(broker);
            var badLoans = QuickPricer2LoanPoolManager.FindUnusedLoansNotMatchingQPTemplateAtBroker(broker);
            var loans = oldLoans.Union(badLoans);
            var oldPrincipal = Thread.CurrentPrincipal;
            Thread.CurrentPrincipal = SystemUserPrincipal.QP2SystemUser;
            var loanDeletionInfos = new List<LoanDeletionInfo>();
            foreach (var loanID in loans)
            {
                string reasonCantDelete = "bad socket";
                var canDeleteLoan = false;
                IEnumerable<FileDBEntryInfo> fileDBEntryInfos;
                try
                {
                    canDeleteLoan = QuickPricer2LoanPoolManager.CanDeleteLoan(
                        broker,
                        loanID,
                        true,
                        true,
                        true,
                        out reasonCantDelete,
                        out fileDBEntryInfos);
                }
                catch (SocketException se)
                {
                    Console.WriteLine(ErrorMessages.SocketExceptionWhenUsingFileDB3_ConfigurationFix);
                    Tools.LogError(ErrorMessages.SocketExceptionWhenUsingFileDB3_ConfigurationFix, se);
                    throw;
                }
                
                var loanDeletionInfo = new LoanDeletionInfo(){LoanID = loanID, CanDelete = canDeleteLoan, ReasonCantDelete = reasonCantDelete}; 
                loanDeletionInfos.Add(loanDeletionInfo);
                Console.WriteLine(loanDeletionInfo);
            }
            Thread.CurrentPrincipal = oldPrincipal;
            foreach (var loanID in loans)
            {
                string reasonCantDelete;
                IEnumerable<FileDBEntryInfo> fileDBEntryInfos;
                Assert.IsFalse(
                    QuickPricer2LoanPoolManager.CanDeleteLoan(
                    broker,
                    loanID,
                    true,
                    true,
                    true,
                    out reasonCantDelete,
                    out fileDBEntryInfos));
                break;
            }
            if(loanDeletionInfos.Any((l) => false == l.CanDelete))
            {
                Tools.LogInfo("UNDELETABLE_LOANS" 
                    + Environment.NewLine 
                    + string.Join(
                        Environment.NewLine,
                        loanDeletionInfos.Where((l)=>false == l.CanDelete).Select((l)=> l.ToString()).ToArray()));
                Assert.Fail("There is at least one loan from the test account that we can't delete.");
            }
        }

        [Test]
        public void TryBestEffortDeleteTest()
        {
            var broker = PrincipalFactory.CurrentPrincipal.BrokerDB;
            var oldPrincipal = Thread.CurrentPrincipal;
            
            // create the loan to be deleted.
            var principal = LoginTools.LoginAs(TestAccountType.LoanOfficer) as AbstractUserPrincipal;
            var loanID = QuickPricer2LoanPoolManager.CreateSandboxedUnusedLoan(principal);
            
            // update the loan so it can be deleted (remove it from pool, set sCreatedD to earlier.)
            QuickPricer2LoanPoolManager.RemoveFromPool(principal, loanID);
            var dataLoan = new CFullAccessPageData(loanID, new string[] { "sCreatedD" });
            dataLoan.ByPassFieldSecurityCheck = true;
            dataLoan.AllowSaveWhileQP2Sandboxed = true;
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sCreatedD = CDateTime.Create(DateTime.Now.AddDays(-(ConstStage.QuickPricer2InUseLoanExpirationDays+1)));
            dataLoan.Save();

            // check that we can delete the loan.
            Thread.CurrentPrincipal = SystemUserPrincipal.QP2SystemUser;
            string reasonCantDelete = "bad socket";
            var canDeleteLoan = false;
            IEnumerable<FileDBEntryInfo> fileDBEntryInfos;
            try
            {
                canDeleteLoan = QuickPricer2LoanPoolManager.CanDeleteLoan(
                    broker,
                    loanID,
                    true,
                    true,
                    true,
                    out reasonCantDelete,
                    out fileDBEntryInfos);
            }
            catch (SocketException se)
            {
                Console.WriteLine(ErrorMessages.SocketExceptionWhenUsingFileDB3_ConfigurationFix);
                Tools.LogError(ErrorMessages.SocketExceptionWhenUsingFileDB3_ConfigurationFix, se);
                throw;
            }
            var loanDeletionInfo = new LoanDeletionInfo() { LoanID = loanID, CanDelete = canDeleteLoan, ReasonCantDelete = reasonCantDelete };
            Console.WriteLine(loanDeletionInfo);
            if (false == canDeleteLoan)
            {
                Assert.Fail("Created a loan but can't delete it: " + loanDeletionInfo);
            }

            // delete the loan.
            string reasonDenied;
            var deleted = QuickPricer2LoanPoolManager.TryBestEffortDeleteLoan(broker, loanID, out reasonDenied);
            if (false == deleted)
            {
                Assert.Fail("Deletion failed for loan " + loanID + " because " + reasonDenied);
            }

            string placesWhereItsPresent = string.Empty;
            bool isPresent = QuickPricer2LoanPoolManager.LoanHasAnyPresence(broker, loanID, out placesWhereItsPresent);

            if (isPresent)
            {
                var msg = string.Format(
                           "LoanID: '{0}' at BrokerID: '{1}' ({2}, {3}) was present in: {4}",
                           loanID, // {0}
                           broker.BrokerID, // {1}
                           broker.CustomerCode, // {2}
                           broker.Name, // {3}
                           placesWhereItsPresent); // {4}

                // Essentially this only happens if the loan was recently processed.
                // This is very unlikely to happen for real loans being deleted.
                // unless memory is holding onto the loan for several days before putting it in the queue.
                if (placesWhereItsPresent == ", LOAN_CACHE_UPDATE_REQUEST")
                {
                    Assert.Ignore(msg);
                }
                else
                {
                    Assert.Fail(msg);
                }
            }
                
            // restore order.
            Thread.CurrentPrincipal = oldPrincipal;
        }
    }
}
