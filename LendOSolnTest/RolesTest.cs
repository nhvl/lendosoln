﻿using System;
using DataAccess;
using NUnit.Framework;
using LendersOfficeApp.newlos;
using LendersOffice.Security;
using LendersOffice.Constants;
using LendOSolnTest.Common;
using EDocs;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data;

namespace LendOSolnTest
{
    [TestFixture]
    public class RolesTest
    {
        private List<EDocRoles> m_currentFolderRoleIds = new List<EDocRoles>();
        private bool m_needToUpdateRoles = false;
        
        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            LoginTools.LoginAs(TestAccountType.LoTest001);

            BrokerUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;
            int unclassifiedFolderId = -1;

            if (EDocumentFolder.BrokerHasUnclassifiedFolder(principal.BrokerId) == false)
            {
                unclassifiedFolderId = EDocumentFolder.CreateUnclassifiedFolderAndSave(principal.BrokerId);
            }
            else
            {
                unclassifiedFolderId = EDocumentFolder.GetUnclassifiedFolderId(principal.BrokerId);
            }

            Assert.AreNotEqual(-1, unclassifiedFolderId);

            EDocumentFolder eFolder = new EDocumentFolder(principal.BrokerId, unclassifiedFolderId);

            IList<EDocRoles> folderRoleIds = eFolder.AssociatedRoleIds;
            
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "INTERNAL_USE_ONLY_ListRoles"))
            {
                while (reader.Read())
                {
                    Guid roleId = (Guid)reader["RoleId"];
                    bool isPmlUser = roleId == Role.Get(E_RoleT.Pml_BrokerProcessor).Id
                        || roleId == Role.Get(E_RoleT.Pml_Secondary).Id
                        || roleId == Role.Get(E_RoleT.Pml_PostCloser).Id;
                    m_currentFolderRoleIds.Add(new EDocRoles() { RoleId = roleId, IsPmlUser = isPmlUser });
                }
            }

            m_currentFolderRoleIds.Add(new EDocRoles() { RoleId = Role.Get(E_RoleT.LoanOfficer).Id, IsPmlUser = true } );
            m_currentFolderRoleIds.Add(new EDocRoles() { RoleId = Role.Get(E_RoleT.Administrator).Id, IsPmlUser = true });


            m_needToUpdateRoles = VerifyPreviousRoleListMatchesCurrent(folderRoleIds) == false;

            bool success = true;

            if (m_needToUpdateRoles)
            {
                using (CStoredProcedureExec exce = new CStoredProcedureExec(principal.BrokerId))
                {
                    exce.BeginTransactionForWrite();
                    try
                    {
                        int folderId = EDocumentFolder.GetUnclassifiedFolderId(principal.BrokerId);
                        EDocumentFolder.SetRoleListForFolderAndSave(m_currentFolderRoleIds, folderId, exce);
                        UpdateExistingUnclassifiedFolders(principal.BrokerId, exce);
                        Tools.LogInfo("Just updated unclassified folders.");
                    }
                    catch (Exception e)
                    {
                        success = false;
                        exce.RollbackTransaction();
                        Tools.LogError("Unable to update unclassified folders with new roles.", e);
                    }

                    Assert.AreEqual(true, success);

                    if (success)
                    {
                        exce.CommitTransaction();
                    }
                }
            }
        }

        // We don't care if the previous role list has extra roles at this point, we just want to make sure it has at
        // least the same roles as the current role list.  Also, we can't just verify that the count matches,
        // because it's possible a role was removed and a new one added, making the same count but a different set
        // (this would be extremely rare)
        private bool VerifyPreviousRoleListMatchesCurrent(IList<EDocRoles> previousRoleList)
        {
            foreach (EDocRoles roleId in m_currentFolderRoleIds)
            {

                if (previousRoleList.Contains(roleId) == false)
                {
                    return false;
                }
            }

            return true;
        }

        private void UpdateExistingUnclassifiedFolders(Guid brokerId, CStoredProcedureExec exce)
        {
            if (m_currentFolderRoleIds == null)
            {
                throw new CBaseException("Current role list was not instantiated - cannot update existing unclassified folders.", "Current role list was not instantiated - cannot update existing unclassified folders.");
            }
            
            List<Guid> brokerIds = new List<Guid>();
            SqlParameter[] parameters = {
                                            new SqlParameter("@FolderName", EDocumentFolder.UNCLASSIFIED_FOLDERNAME)
                                        };
            using (IDataReader sR = StoredProcedureHelper.ExecuteReader(brokerId, "EDOCS_ListBrokerIdsThatHaveFolderName", parameters))
            {
                while (sR.Read())
                {
                    int folderId = (int)sR["FolderId"];

                    EDocumentFolder.SetRoleListForFolderAndSave(m_currentFolderRoleIds, folderId, exce);
                }
            }
        }

        [Test]
        public void VerifyAllBrokersWithEdocsHaveCurrentRoleList()
        {
            BrokerUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;
            
            List<Guid> brokerIds = new List<Guid>();
            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                using (IDataReader sR = StoredProcedureHelper.ExecuteReader(connInfo, "INTERNAL_USE_ONLY_ListBrokerIdsThatHaveOrHadEDocsOn", null))
                {
                    while (sR.Read())
                    {
                        brokerIds.Add((Guid)sR["BrokerId"]);
                    }
                }
            }

            foreach (Guid brokerId in brokerIds)
            {
                int unclassifiedFolderId = EDocumentFolder.GetUnclassifiedFolderId(principal.BrokerId);
                Assert.AreNotEqual(-1, unclassifiedFolderId);
                
                EDocumentFolder eFolder = new EDocumentFolder(principal.BrokerId, unclassifiedFolderId);
                EDocumentFolder.SetRoleListForFolderAndSave(principal.BrokerId, m_currentFolderRoleIds, unclassifiedFolderId);
                bool match = VerifyPreviousRoleListMatchesCurrent(eFolder.AssociatedRoleIds);
                Assert.AreEqual(true, match, "Role list didn't match current db role list for unclassified folder in brokerId: " + brokerId + ", unclassified folder Id: " + unclassifiedFolderId);
            }
        }
    }
}
