// Author: Peter Anargirou

using System;
using System.Collections.Generic;
using NUnit.Framework;
using LendersOffice.Security;
using LendOSolnTest.Common;
using DataAccess;
using LendersOffice.Audit;
using System.IO;
using LendersOffice.Conversions;
using LendersOffice.Common;
using LendersOffice.Constants;

namespace LendOSolnTest.Import
{
    [TestFixture]
    public class ImportTest
    {
        Guid m_blankTemplateId = Guid.Empty;
        Guid m_populatedTemplateId = Guid.Empty;
        Guid m_blankLoanId = Guid.Empty;
        Guid m_populatedLoanId = Guid.Empty;

        AbstractUserPrincipal x_principal = null;
        private AbstractUserPrincipal m_currentPrincipal
        {
            get
            {
                if (null == x_principal)
                {
                    x_principal = LoginTools.LoginAs(TestAccountType.LoTest001);
                }

                return x_principal;
            }
        }

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            CreateLoanTemplates();
            CreateLoanFiles();
        }

        private CPageData CreatePageData(Guid templateId)
        {
            return CPageData.CreateUsingSmartDependency(templateId, typeof(ImportTest));
        }

        private void CreateLoanTemplates()
        {
            #region blank template

            CLoanFileCreator fileCreator = CLoanFileCreator.GetCreator(m_currentPrincipal, E_LoanCreationSource.UserCreateFromBlank);
            m_blankTemplateId = fileCreator.CreateBlankLoanTemplate();
            CPageData dataLoan = CreatePageData(m_blankTemplateId);

            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            CAppData dataAppForTemplate = dataLoan.GetAppData(0);
            //dataAppForTemplate.aBMaritalStatT = E_aBMaritalStatT.Married; // explicitly state this because the default value, LeaveBlank, is 3 and not 0

            dataLoan.Save();

            #endregion

            #region populated template

            #region initialize data loan
            CLoanFileCreator fileCreator2 = CLoanFileCreator.GetCreator(m_currentPrincipal, E_LoanCreationSource.UserCreateFromBlank);
            m_populatedTemplateId = fileCreator2.CreateBlankLoanTemplate();
            CPageData dataLoan2 = CreatePageData(m_populatedTemplateId);
            dataLoan2.InitSave(ConstAppDavid.SkipVersionCheck);
            #endregion

            #region Set fields
            dataLoan2.sDuCaseId = "test1";
            dataLoan2.sLT = E_sLT.FHA;
            dataLoan2.sNoteIR = 45.7m;
            dataLoan2.sTerm = 4;
            #endregion

            #region Set data app fields
            CAppData dataApp = dataLoan2.GetAppData(0);
            dataApp.aBDob_rep = "10/11/2000";
            dataApp.aBSsn = "123-45-6789"; // needed to set DOB
            dataApp.aBIsAsian = true;
            dataApp.aBMaritalStatT = E_aBMaritalStatT.NotMarried;
            dataApp.aOccT = E_aOccT.Investment;
            dataApp.aBBaseI = 1234.56m;
            #endregion

            dataLoan2.Save();

            #endregion
        }

        private void CreateLoanFiles()
        {
            #region blank loan file

            CLoanFileCreator fileCreator = CLoanFileCreator.GetCreator(m_currentPrincipal, E_LoanCreationSource.UserCreateFromBlank);
            m_blankLoanId = fileCreator.CreateBlankLoanFile();
            CPageData dataLoan = CreatePageData(m_blankLoanId);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            CAppData dataAppForTemplate = dataLoan.GetAppData(0);
            dataAppForTemplate.aBMaritalStatT = E_aBMaritalStatT.Married; // Explicitly set because the default, LeaveBlank, has a value of 3, not 0.

            dataLoan.Save();

            #endregion

            #region populated loan file

            #region initialize data loan
            CLoanFileCreator fileCreator2 = CLoanFileCreator.GetCreator(m_currentPrincipal, E_LoanCreationSource.UserCreateFromBlank);
            m_populatedLoanId = fileCreator2.CreateBlankLoanFile();
            CPageData dataLoan2 = CreatePageData(m_populatedLoanId);
            dataLoan2.InitSave(ConstAppDavid.SkipVersionCheck);
            #endregion

            #region set fields
            dataLoan2.sDuCaseId = "fooo";
            dataLoan2.sLT = E_sLT.Other;
            dataLoan2.sNoteIR = 64.3m;
            dataLoan2.sTerm = 7;
            #endregion

            #region set data app fields
            CAppData dataApp = dataLoan2.GetAppData(0);
            dataApp.aBDob_rep = "10/12/2000";
            dataApp.aBSsn = "111-22-3333"; // needed so DOB will set
            dataApp.aBIsAsian = true;
            dataApp.aBMaritalStatT = E_aBMaritalStatT.Separated;
            dataApp.aOccT = E_aOccT.SecondaryResidence;
            dataApp.aBBaseI = 1337.90m;
            dataApp.aBAddr = "111 Present Address";
            dataApp.aBCity = "City";
            dataApp.aBAddrT = E_aBAddrT.Own;
            #endregion

            #region set employment collection.
            IPrimaryEmploymentRecord rec = dataApp.aBEmpCollection.GetPrimaryEmp(true);
            rec.EmplrNm = "EmplrNm";
            rec.EmplrAddr = "EmplrAddr";
            rec.Update();

            IRegularEmploymentRecord regRec = dataApp.aBEmpCollection.AddRegularRecord();
            regRec.EmplrNm = "EmplrNm";
            regRec.EmplrAddr = "EmplrAddr";
            regRec.Update();
            #endregion
            dataLoan2.Save();

            #endregion
        }


        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            Tools.DeclareLoanFileInvalid(m_currentPrincipal, m_blankTemplateId, false /* sendNotifications*/, false /* generateLinkLoanMsgEvent */);
            Tools.DeclareLoanFileInvalid(m_currentPrincipal, m_populatedTemplateId, false /* sendNotifications*/, false /* generateLinkLoanMsgEvent */);
            Tools.DeclareLoanFileInvalid(m_currentPrincipal, m_blankLoanId, false /* sendNotifications*/, false /* generateLinkLoanMsgEvent */);
            Tools.DeclareLoanFileInvalid(m_currentPrincipal, m_populatedLoanId, false /* sendNotifications*/, false /* generateLinkLoanMsgEvent */);
        }

        [Test]
        public void FannieMaeBlankTemplateBlankLoan()
        {
            CPageData importedLoan = ExportAndImportFannieMae(m_blankTemplateId, m_blankLoanId);
            importedLoan.InitLoad();
            CAppData dataAppForImportedLoan = importedLoan.GetAppData(0);

            CPageData template = CreatePageData(m_blankTemplateId);
            template.InitLoad();
            CAppData dataAppForTemplate = template.GetAppData(0);

            /* This can be used to check data
			CPageData originalLoan = CreatePageData( m_blankLoanId );
			originalLoan.InitLoad();
			CAppData dataAppForOriginalLoan = originalLoan.GetAppData(0);
			*/

            try
            {
                Assert.AreEqual(template.sDuCaseId, importedLoan.sDuCaseId, "sDuCaseId");
                Assert.AreEqual(template.sLT, importedLoan.sLT, "sLT");
                Assert.AreEqual(dataAppForTemplate.aBMaritalStatT, dataAppForImportedLoan.aBMaritalStatT, "aBMaritalStatusT");
                Assert.AreEqual(dataAppForTemplate.aBDob_rep, dataAppForImportedLoan.aBDob_rep, "aBDob");
                Assert.AreEqual(dataAppForTemplate.aBBaseI, dataAppForImportedLoan.aBBaseI, "aBBaseI");
                Assert.AreEqual(template.sNoteIR, importedLoan.sNoteIR, "sNoteIR");
                Assert.AreEqual(dataAppForTemplate.aBIsAsian, dataAppForImportedLoan.aBIsAsian, "aBIsAsian");
                Assert.AreEqual(dataAppForTemplate.aOccT, dataAppForImportedLoan.aOccT, "aOccT");
            }
            finally
            {
                Tools.DeclareLoanFileInvalid(m_currentPrincipal, importedLoan.sLId, false /* sendNotifications*/, false /* generateLinkLoanMsgEvent */);
            }
        }

        [Test]
        public void FannieMaeBlankTemplatePopulatedLoan()
        {
            CPageData importedLoan = ExportAndImportFannieMae(m_blankTemplateId, m_populatedLoanId);
            importedLoan.InitLoad();
            CAppData dataAppForImportedLoan = importedLoan.GetAppData(0);

            Assert.Greater(dataAppForImportedLoan.aBEmpCollection.CountRegular, 0, "aBEmpCollection.CountRegular");

            CPageData originalLoan = CreatePageData(m_populatedLoanId);
            originalLoan.InitLoad();
            CAppData dataAppForOriginalLoan = originalLoan.GetAppData(0);

            /* This can be used to check data
			CPageData template = CreatePageData( m_blankTemplateId );
			template.InitLoad();
			CAppData dataAppForTemplate = template.GetAppData(0);
			*/

            try
            {
                Assert.AreEqual(originalLoan.sDuCaseId, importedLoan.sDuCaseId, "sDuCaseId");
                Assert.AreEqual(originalLoan.sLT, importedLoan.sLT, "sLT");
                Assert.AreEqual(dataAppForOriginalLoan.aBBaseI, dataAppForImportedLoan.aBBaseI, "aBBaseI");
                Assert.AreEqual(originalLoan.sNoteIR, importedLoan.sNoteIR, "sNoteIR");
                Assert.AreEqual(dataAppForOriginalLoan.aBDob_rep, dataAppForImportedLoan.aBDob_rep, "aBDob");
                Assert.AreEqual(dataAppForOriginalLoan.aBIsAsian, dataAppForImportedLoan.aBIsAsian, "aBIsAsian");
                Assert.AreEqual(dataAppForOriginalLoan.aBMaritalStatT, dataAppForImportedLoan.aBMaritalStatT, "aBMaritalStatusT");
                Assert.AreEqual(dataAppForOriginalLoan.aOccT, dataAppForImportedLoan.aOccT, "aOccT");
                Assert.AreEqual(dataAppForOriginalLoan.aBAddrT, dataAppForImportedLoan.aBAddrT, "aBAddrT");
            }
            finally
            {
                Tools.DeclareLoanFileInvalid(m_currentPrincipal, importedLoan.sLId, false /* sendNotifications*/, false /* generateLinkLoanMsgEvent */);
            }
        }

        [Test]
        public void FannieMaePopulatedTemplateBlankLoan()
        {
            CPageData importedLoan = ExportAndImportFannieMae(m_populatedTemplateId, m_blankLoanId);
            importedLoan.InitLoad();
            CAppData dataAppForImportedLoan = importedLoan.GetAppData(0);

            CPageData template = CreatePageData(m_populatedTemplateId);
            template.InitLoad();
            CAppData dataAppForTemplate = template.GetAppData(0);

            /* This can be used to check data
			CPageData originalLoan = CreatePageData( m_blankLoanId );
			originalLoan.InitLoad();
			CAppData dataAppForOriginalLoan = originalLoan.GetAppData(0);
			*/

            try
            {
                Assert.AreEqual(template.sDuCaseId, importedLoan.sDuCaseId, "sDuCaseId");
                Assert.AreEqual(template.sLT, importedLoan.sLT, "sLT");
                Assert.AreEqual(dataAppForTemplate.aBMaritalStatT, dataAppForImportedLoan.aBMaritalStatT, "aBMaritalStatusT");
                Assert.AreEqual(dataAppForTemplate.aBDob_rep, dataAppForImportedLoan.aBDob_rep, "aBDob");
                Assert.AreEqual(dataAppForTemplate.aBBaseI, dataAppForImportedLoan.aBBaseI, "aBBaseI");
                Assert.AreEqual(template.sNoteIR, importedLoan.sNoteIR, "sNoteIR");
                Assert.AreEqual(dataAppForTemplate.aOccT, dataAppForImportedLoan.aOccT, "aOccT");
            }
            finally
            {
                Tools.DeclareLoanFileInvalid(m_currentPrincipal, importedLoan.sLId, false /* sendNotifications*/, false /* generateLinkLoanMsgEvent */);
            }
        }

        [Test]
        public void FannieMaePopulatedTemplatePopulatedLoan()
        {
            CPageData importedLoan = ExportAndImportFannieMae(m_populatedTemplateId, m_populatedLoanId);
            importedLoan.InitLoad();
            CAppData dataAppForImportedLoan = importedLoan.GetAppData(0);

            CPageData template = CreatePageData(m_populatedTemplateId);
            template.InitLoad();
            CAppData dataAppForTemplate = template.GetAppData(0);

            /* This can be used to check data
			CPageData originalLoan = CreatePageData( m_populatedLoanId );
			originalLoan.InitLoad();
			CAppData dataAppForOriginalLoan = originalLoan.GetAppData(0);
			*/

            try
            {
                Assert.AreEqual(dataAppForTemplate.aBDob_rep, dataAppForImportedLoan.aBDob_rep, "aBDob");
                Assert.AreEqual(template.sDuCaseId, importedLoan.sDuCaseId, "sDuCaseId");
                Assert.AreEqual(template.sLT, importedLoan.sLT, "sLT");
                Assert.AreEqual(template.sNoteIR, importedLoan.sNoteIR, "sNoteIR");
                Assert.AreEqual(dataAppForTemplate.aBBaseI, dataAppForImportedLoan.aBBaseI, "aBBaseI");
                Assert.AreEqual(dataAppForTemplate.aBMaritalStatT, dataAppForImportedLoan.aBMaritalStatT, "aBMaritalStatusT");
                Assert.AreEqual(dataAppForTemplate.aOccT, dataAppForImportedLoan.aOccT, "aOccT");
            }
            finally
            {
                Tools.DeclareLoanFileInvalid(m_currentPrincipal, importedLoan.sLId, false /* sendNotifications*/, false /* generateLinkLoanMsgEvent */);
            }
        }

        private CPageData ExportAndImportFannieMae(Guid loanTemplateId, Guid loanId)
        {
            FannieMae32Exporter fannieMaeExporter = new FannieMae32Exporter(loanId);
            byte[] fannieMae = fannieMaeExporter.Export();

            CPageData pageBase;
            using (Stream fannieMaeStream = new MemoryStream(fannieMae))
            {
                pageBase = FannieMaeImporter.Import(m_currentPrincipal, fannieMaeStream, FannieMaeImportSource.LendersOffice, false /*addEmployeeAsOfficialAgent*/, loanTemplateId, true);
            }
            return CreatePageData(pageBase.sLId);
        }

        [Test]
        public void TestFannieMae32ImporterIsMatch()
        {
            List<ApplicantInfo> oldList = new List<ApplicantInfo>()
            {
                new ApplicantInfo() { aBNm = "John Doe", aBSsn = "111-11-1111"},
                new ApplicantInfo() { aBNm = "John Homeowner", aBSsn = "222-22-2222", aCNm="Mary Homeowner", aCSsn="333-33-3333"},
                new ApplicantInfo() { aBNm = "Sister Doe", aBSsn="444-44-4444"},
            };

            List<ApplicantInfo> newList = new List<ApplicantInfo>()
            {
                new ApplicantInfo() { aBNm = "John doe", aBSsn="111-11-1111"},
                new ApplicantInfo() { aBNm = "Sister Doe", aBSsn="444-44-4444"},

                new ApplicantInfo() { aBNm="Mary Homeowner", aBSsn="333-33-3333", aCNm="John Homeowner", aCSsn="222-22-2222"},
            };
            List<E_ComparisionResult> expectedResult = new List<E_ComparisionResult>() {
                E_ComparisionResult.Matched,
                E_ComparisionResult.Deleted,
                E_ComparisionResult.Matched,
                E_ComparisionResult.Added
            };
            FannieMae32Importer importer = new FannieMae32Importer();

            List<ApplicantInfoCompareResult> resultList;

            bool isMatch = importer.IsMatch(oldList, oldList, out resultList);
            Assert.AreEqual(true, isMatch, "Matching itself");

            isMatch = importer.IsMatch(oldList, newList, out resultList);
            Assert.AreEqual(false, isMatch);

            Assert.AreEqual(expectedResult.Count, resultList.Count);
            for (int i = 0; i < expectedResult.Count; i++)
            {
                Assert.AreEqual(expectedResult[i], resultList[i].Result);
            }
        }

    }
}