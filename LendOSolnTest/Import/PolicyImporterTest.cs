﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using LendersOffice.LoanPrograms;
using LendersOffice.Security;
using LendersOfficeApp.los.RatePrice;
using LendOSolnTest.Common;
using NUnit.Framework;

namespace LendOSolnTest.Import
{
    [TestFixture]
    public class PolicyImporterTest
    {
        private Guid policyGuid = new Guid("11112222-3333-4444-5555-666677778888");
        CPricePolicy m_pricePolicy = null;

        private FileStream[] m_streams;
        AbstractUserPrincipal x_principal = null;

        private AbstractUserPrincipal m_currentPrincipal
        {
            get
            {
                if (null == x_principal)
                {
                    x_principal = LoginTools.LoginAs(TestAccountType.LoTest001);
                }

                return x_principal;
            }
        }

        static private string TestFileLocation
        {
            get
            {
                return LendersOffice.Constants.ConstSite.TestDataFolder + "PolicyImporterFiles\\";
            }
        }

        static private string[] TestFileName
        {
            get
            {
                return new string[]{
                                     TestFileLocation + "TestDetectOldFormat.csv"
                                    ,TestFileLocation + "TestDetectNewFormat.csv"
                                    ,TestFileLocation + "TestImportNotExistentPolicy.csv"
                                    ,TestFileLocation + "TestRuleSyntaxError.csv"
                                    ,TestFileLocation + "TestValidFile.csv"
                };
            }
        }


        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            List<FileStream> streams = new List<FileStream>();
            for (int i = 0; i < TestFileName.Length; i++)
            {
                if (File.Exists(TestFileName[i]))
                    streams.Add(File.OpenRead(TestFileName[i]));
            }

            m_streams = streams.ToArray();
                        
            //Remove existing rules from the policy
            m_pricePolicy = CPricePolicy.Retrieve(policyGuid);
            m_pricePolicy.DeleteAllRules();
            m_pricePolicy.Save();
            
        }

        [Test]
        public void FileTypeTest()
        {
            bool IsOldFileType = RateAdjustImport.CheckFileFormat(m_streams[0]);
            bool IsNewFileType = !RateAdjustImport.CheckFileFormat(m_streams[1]);

            Assert.AreEqual(true, IsOldFileType);
            Assert.AreEqual(true, IsNewFileType);
        }

        [Test]
        public void ImportNonExistentPolicy()
        {

            RateAdjustImport import = new RateAdjustImport();
            IEnumerable<string> errorList = null;
            Assert.AreEqual(import.Import(m_streams[2], out errorList), false);
            Assert.AreEqual(4, errorList.Count()); //There are 4 non existent policies in the file.
            Assert.That((errorList.ElementAt(0)).Contains("11112222-3333-8888-5555-666677778888"));
        }

        [Test]
        public void ImportInvalidRule()
        {
            IEnumerable<string> errorList = null;
            RateAdjustImport import = new RateAdjustImport();

            bool IsImportOk = import.Import(m_streams[3], out errorList);
            Assert.That(!IsImportOk);
            Assert.AreEqual(3, errorList.Count()); //There are 3 invalud rules in the file.
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            foreach (Stream str in m_streams)
            {
                if (str != null)
                    str.Close();
            }

            //Remove existing rules (if any) from the policy
            m_pricePolicy = CPricePolicy.Retrieve(policyGuid);
            m_pricePolicy.DeleteAllRules();
            m_pricePolicy.Save();

        }
    }
}