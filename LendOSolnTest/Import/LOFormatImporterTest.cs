﻿using DataAccess;
using LendersOffice.Security;
using LendOSolnTest.Common;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LendOSolnTest.Import
{
    [TestFixture]
    public class LOFormatImporterTest
    {
        private Guid loanId;
        private AbstractUserPrincipal principal;

        [SetUp]
        public void Setup()
        {

            this.principal = LoginTools.LoginAs(TestAccountType.LoanOfficer);
            CLoanFileCreator creator = CLoanFileCreator.GetCreator(principal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);

            this.loanId = creator.CreateBlankLoanFile();
        }

        [TearDown]
        public void Teardown()
        {
            Tools.DeclareLoanFileInvalid(this.principal, this.loanId, false, false);
        }

        public bool ToBool(string val)
        {
            if ("Yes".Equals(val, StringComparison.OrdinalIgnoreCase))
            {
                return true;
            }

            return false;
        }

        public string ToYesNo(bool bal)
        {
            return bal ? "Yes" : "No";
        }

        [Test]
        public void EnsureFieldsAreSettable()
        {
            var test = new[]
            {
new { FieldName = "slnm", Setter = new Action<string, CPageData>((s, loan) => { loan.sLNm = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sLNm.ToString();})},
new { FieldName = "spurchprice", Setter = new Action<string, CPageData>((s, loan) => { loan.sPurchPrice_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sPurchPrice_rep.ToString();})},
new { FieldName = "sapprval", Setter = new Action<string, CPageData>((s, loan) => { loan.sApprVal_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sApprVal_rep.ToString();})},
new { FieldName = "sprodincludenormalproc", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdIncludeNormalProc = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdIncludeNormalProc.ToString();})},
new { FieldName = "sprodincludemycommunityproc", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdIncludeMyCommunityProc = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdIncludeMyCommunityProc.ToString();})},
new { FieldName = "sprodincludehomepossibleproc", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdIncludeHomePossibleProc = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdIncludeHomePossibleProc.ToString();})},
new { FieldName = "sprodincludefhatotalproc", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdIncludeFHATotalProc = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdIncludeFHATotalProc.ToString();})},
new { FieldName = "sprodincludevaproc", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdIncludeVAProc = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdIncludeVAProc.ToString();})},
new { FieldName = "sfinmethdesc", Setter = new Action<string, CPageData>((s, loan) => { loan.sFinMethDesc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sFinMethDesc.ToString();})},
new { FieldName = "sequitycalc", Setter = new Action<string, CPageData>((s, loan) => { loan.sEquityCalc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sEquityCalc_rep.ToString();})},
new { FieldName = "slamtlckd", Setter = new Action<string, CPageData>((s, loan) => { loan.sLAmtLckd = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sLAmtLckd.ToString();})},
new { FieldName = "snoteir", Setter = new Action<string, CPageData>((s, loan) => { loan.sNoteIR_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sNoteIR_rep.ToString();})},
new { FieldName = "sterm", Setter = new Action<string, CPageData>((s, loan) => { loan.sTerm_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sTerm_rep.ToString();})},
new { FieldName = "sdue", Setter = new Action<string, CPageData>((s, loan) => { loan.sDue_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sDue_rep.ToString();})},
new { FieldName = "squalir", Setter = new Action<string, CPageData>((s, loan) => { loan.sQualIR_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sQualIR_rep.ToString();})},
new { FieldName = "slptemplatenm", Setter = new Action<string, CPageData>((s, loan) => { loan.sLpTemplateNm = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sLpTemplateNm.ToString();})},
new { FieldName = "scctemplatenm", Setter = new Action<string, CPageData>((s, loan) => { loan.sCcTemplateNm = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCcTemplateNm.ToString();})},
new { FieldName = "sprohazinsr", Setter = new Action<string, CPageData>((s, loan) => { loan.sProHazInsR_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sProHazInsR_rep.ToString();})},
new { FieldName = "sprohazinsmb", Setter = new Action<string, CPageData>((s, loan) => { loan.sProHazInsMb_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sProHazInsMb_rep.ToString();})},
new { FieldName = "sprorealetxr", Setter = new Action<string, CPageData>((s, loan) => { loan.sProRealETxR_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sProRealETxR_rep.ToString();})},
new { FieldName = "sprorealetxmb", Setter = new Action<string, CPageData>((s, loan) => { loan.sProRealETxMb_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sProRealETxMb_rep.ToString();})},
new { FieldName = "sprominsr", Setter = new Action<string, CPageData>((s, loan) => { loan.sProMInsR_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sProMInsR_rep.ToString();})},
new { FieldName = "sprominsmb", Setter = new Action<string, CPageData>((s, loan) => { loan.sProMInsMb_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sProMInsMb_rep.ToString();})},
new { FieldName = "sprominslckd", Setter = new Action<string, CPageData>((s, loan) => { loan.sProMInsLckd = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sProMInsLckd.ToString();})},
new { FieldName = "spromins", Setter = new Action<string, CPageData>((s, loan) => { loan.sProMIns_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sProMIns_rep.ToString();})},
new { FieldName = "sprohoassocdues", Setter = new Action<string, CPageData>((s, loan) => { loan.sProHoAssocDues_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sProHoAssocDues_rep.ToString();})},
new { FieldName = "ssubfin", Setter = new Action<string, CPageData>((s, loan) => { loan.sSubFin_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sSubFin_rep.ToString();})},
new { FieldName = "sconcursubfin", Setter = new Action<string, CPageData>((s, loan) => { loan.sConcurSubFin_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sConcurSubFin_rep.ToString();})},
new { FieldName = "ssubfinir", Setter = new Action<string, CPageData>((s, loan) => { loan.sSubFinIR_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sSubFinIR_rep.ToString();})},
new { FieldName = "ssubfinterm", Setter = new Action<string, CPageData>((s, loan) => { loan.sSubFinTerm_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sSubFinTerm_rep.ToString();})},
new { FieldName = "ssubfinmb", Setter = new Action<string, CPageData>((s, loan) => { loan.sSubFinMb_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sSubFinMb_rep.ToString();})},
new { FieldName = "sisionlyforsubfin", Setter = new Action<string, CPageData>((s, loan) => { loan.sIsIOnlyForSubFin = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sIsIOnlyForSubFin.ToString();})},
new { FieldName = "sisofinnew", Setter = new Action<string, CPageData>((s, loan) => { loan.sIsOFinNew = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sIsOFinNew.ToString();})},
new { FieldName = "sisofincreditlineindrawperiod", Setter = new Action<string, CPageData>((s, loan) => { loan.sIsOFinCreditLineInDrawPeriod = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sIsOFinCreditLineInDrawPeriod.ToString();})},
new { FieldName = "ssubfinpmt", Setter = new Action<string, CPageData>((s, loan) => { loan.sSubFinPmt_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sSubFinPmt_rep.ToString();})},
new { FieldName = "ssubfinpmtlckd", Setter = new Action<string, CPageData>((s, loan) => { loan.sSubFinPmtLckd = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sSubFinPmtLckd.ToString();})},
new { FieldName = "slayeredtotalforgivablebalance", Setter = new Action<string, CPageData>((s, loan) => { loan.sLayeredTotalForgivableBalance_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sLayeredTotalForgivableBalance_rep.ToString();})},
new { FieldName = "srlckdd", Setter = new Action<string, CPageData>((s, loan) => { loan.sRLckdD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sRLckdD_rep.ToString();})},
new { FieldName = "srlckdn", Setter = new Action<string, CPageData>((s, loan) => { loan.sRLckdN = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sRLckdN.ToString();})},
new { FieldName = "srlckdexpiredn", Setter = new Action<string, CPageData>((s, loan) => { loan.sRLckdExpiredN = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sRLckdExpiredN.ToString();})},
new { FieldName = "sleadd", Setter = new Action<string, CPageData>((s, loan) => { loan.sLeadD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sLeadD_rep.ToString();})},
new { FieldName = "sleadn", Setter = new Action<string, CPageData>((s, loan) => { loan.sLeadN = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sLeadN.ToString();})},
new { FieldName = "sopenedd", Setter = new Action<string, CPageData>((s, loan) => { loan.sOpenedD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sOpenedD_rep.ToString();})},
new { FieldName = "sopenedn", Setter = new Action<string, CPageData>((s, loan) => { loan.sOpenedN = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sOpenedN.ToString();})},
new { FieldName = "sprequald", Setter = new Action<string, CPageData>((s, loan) => { loan.sPreQualD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sPreQualD_rep.ToString();})},
new { FieldName = "sprequaln", Setter = new Action<string, CPageData>((s, loan) => { loan.sPreQualN = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sPreQualN.ToString();})},
new { FieldName = "spreapprovd", Setter = new Action<string, CPageData>((s, loan) => { loan.sPreApprovD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sPreApprovD_rep.ToString();})},
new { FieldName = "spreapprovn", Setter = new Action<string, CPageData>((s, loan) => { loan.sPreApprovN = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sPreApprovN.ToString();})},
new { FieldName = "ssubmitd", Setter = new Action<string, CPageData>((s, loan) => { loan.sSubmitD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sSubmitD_rep.ToString();})},
new { FieldName = "ssubmitn", Setter = new Action<string, CPageData>((s, loan) => { loan.sSubmitN = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sSubmitN.ToString();})},
new { FieldName = "sapprovd", Setter = new Action<string, CPageData>((s, loan) => { loan.sApprovD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sApprovD_rep.ToString();})},
new { FieldName = "sapprovn", Setter = new Action<string, CPageData>((s, loan) => { loan.sApprovN = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sApprovN.ToString();})},
new { FieldName = "sestclosen", Setter = new Action<string, CPageData>((s, loan) => { loan.sEstCloseN = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sEstCloseN.ToString();})},
new { FieldName = "sunderwritingd", Setter = new Action<string, CPageData>((s, loan) => { loan.sUnderwritingD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sUnderwritingD_rep.ToString();})},
new { FieldName = "sunderwritingn", Setter = new Action<string, CPageData>((s, loan) => { loan.sUnderwritingN = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sUnderwritingN.ToString();})},
new { FieldName = "sdocsd", Setter = new Action<string, CPageData>((s, loan) => { loan.sDocsD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sDocsD_rep.ToString();})},
new { FieldName = "sdocsn", Setter = new Action<string, CPageData>((s, loan) => { loan.sDocsN = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sDocsN.ToString();})},
new { FieldName = "sschedfundd", Setter = new Action<string, CPageData>((s, loan) => { loan.sSchedFundD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sSchedFundD_rep.ToString();})},
new { FieldName = "sschedfundn", Setter = new Action<string, CPageData>((s, loan) => { loan.sSchedFundN = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sSchedFundN.ToString();})},
new { FieldName = "sfundd", Setter = new Action<string, CPageData>((s, loan) => { loan.sFundD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sFundD_rep.ToString();})},
new { FieldName = "sfundn", Setter = new Action<string, CPageData>((s, loan) => { loan.sFundN = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sFundN.ToString();})},
new { FieldName = "srecordedd", Setter = new Action<string, CPageData>((s, loan) => { loan.sRecordedD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sRecordedD_rep.ToString();})},
new { FieldName = "srecordedn", Setter = new Action<string, CPageData>((s, loan) => { loan.sRecordedN = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sRecordedN.ToString();})},
new { FieldName = "sclosedd", Setter = new Action<string, CPageData>((s, loan) => { loan.sClosedD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sClosedD_rep.ToString();})},
new { FieldName = "sclosedn", Setter = new Action<string, CPageData>((s, loan) => { loan.sClosedN = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sClosedN.ToString();})},
new { FieldName = "sonholdd", Setter = new Action<string, CPageData>((s, loan) => { loan.sOnHoldD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sOnHoldD_rep.ToString();})},
new { FieldName = "sonholdn", Setter = new Action<string, CPageData>((s, loan) => { loan.sOnHoldN = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sOnHoldN.ToString();})},
new { FieldName = "scanceledd", Setter = new Action<string, CPageData>((s, loan) => { loan.sCanceledD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCanceledD_rep.ToString();})},
new { FieldName = "scanceledn", Setter = new Action<string, CPageData>((s, loan) => { loan.sCanceledN = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCanceledN.ToString();})},
new { FieldName = "srejectd", Setter = new Action<string, CPageData>((s, loan) => { loan.sRejectD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sRejectD_rep.ToString();})},
new { FieldName = "srejectn", Setter = new Action<string, CPageData>((s, loan) => { loan.sRejectN = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sRejectN.ToString();})},
new { FieldName = "ssuspendedd", Setter = new Action<string, CPageData>((s, loan) => { loan.sSuspendedD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sSuspendedD_rep.ToString();})},
new { FieldName = "ssuspendedn", Setter = new Action<string, CPageData>((s, loan) => { loan.sSuspendedN = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sSuspendedN.ToString();})},
new { FieldName = "scleartoclosed", Setter = new Action<string, CPageData>((s, loan) => { loan.sClearToCloseD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sClearToCloseD_rep.ToString();})},
new { FieldName = "sinvestorlockloannum", Setter = new Action<string, CPageData>((s, loan) => { loan.sInvestorLockLoanNum = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sInvestorLockLoanNum.ToString();})},
new { FieldName = "sinvestorlockconfnum", Setter = new Action<string, CPageData>((s, loan) => { loan.sInvestorLockConfNum = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sInvestorLockConfNum.ToString();})},
new { FieldName = "sinvestorlockrlckdd", Setter = new Action<string, CPageData>((s, loan) => { loan.sInvestorLockRLckdD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sInvestorLockRLckdD_rep.ToString();})},
new { FieldName = "sshippedtoinvestord", Setter = new Action<string, CPageData>((s, loan) => { loan.sShippedToInvestorD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sShippedToInvestorD_rep.ToString();})},
new { FieldName = "slpurchased", Setter = new Action<string, CPageData>((s, loan) => { loan.sLPurchaseD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sLPurchaseD_rep.ToString();})},
new { FieldName = "sgoodbyletterd", Setter = new Action<string, CPageData>((s, loan) => { loan.sGoodByLetterD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sGoodByLetterD_rep.ToString();})},
new { FieldName = "slendercasenum", Setter = new Action<string, CPageData>((s, loan) => { loan.sLenderCaseNum = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sLenderCaseNum.ToString();})},
new { FieldName = "sagencycasenum", Setter = new Action<string, CPageData>((s, loan) => { loan.sAgencyCaseNum = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sAgencyCaseNum.ToString();})},
new { FieldName = "su1lstatdesc", Setter = new Action<string, CPageData>((s, loan) => { loan.sU1LStatDesc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sU1LStatDesc.ToString();})},
new { FieldName = "su1lstatd", Setter = new Action<string, CPageData>((s, loan) => { loan.sU1LStatD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sU1LStatD_rep.ToString();})},
new { FieldName = "su1lstatn", Setter = new Action<string, CPageData>((s, loan) => { loan.sU1LStatN = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sU1LStatN.ToString();})},
new { FieldName = "su2lstatdesc", Setter = new Action<string, CPageData>((s, loan) => { loan.sU2LStatDesc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sU2LStatDesc.ToString();})},
new { FieldName = "su2lstatd", Setter = new Action<string, CPageData>((s, loan) => { loan.sU2LStatD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sU2LStatD_rep.ToString();})},
new { FieldName = "su2lstatn", Setter = new Action<string, CPageData>((s, loan) => { loan.sU2LStatN = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sU2LStatN.ToString();})},
new { FieldName = "su3lstatdesc", Setter = new Action<string, CPageData>((s, loan) => { loan.sU3LStatDesc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sU3LStatDesc.ToString();})},
new { FieldName = "su3lstatd", Setter = new Action<string, CPageData>((s, loan) => { loan.sU3LStatD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sU3LStatD_rep.ToString();})},
new { FieldName = "su3lstatn", Setter = new Action<string, CPageData>((s, loan) => { loan.sU3LStatN = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sU3LStatN.ToString();})},
new { FieldName = "su4lstatdesc", Setter = new Action<string, CPageData>((s, loan) => { loan.sU4LStatDesc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sU4LStatDesc.ToString();})},
new { FieldName = "su4lstatd", Setter = new Action<string, CPageData>((s, loan) => { loan.sU4LStatD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sU4LStatD_rep.ToString();})},
new { FieldName = "su4lstatn", Setter = new Action<string, CPageData>((s, loan) => { loan.sU4LStatN = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sU4LStatN.ToString();})},
new { FieldName = "sprelimrprtod", Setter = new Action<string, CPageData>((s, loan) => { loan.sPrelimRprtOd_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sPrelimRprtOd_rep.ToString();})},
new { FieldName = "sprelimrprtdued", Setter = new Action<string, CPageData>((s, loan) => { loan.sPrelimRprtDueD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sPrelimRprtDueD_rep.ToString();})},
new { FieldName = "sprelimrprtrd", Setter = new Action<string, CPageData>((s, loan) => { loan.sPrelimRprtRd_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sPrelimRprtRd_rep.ToString();})},
new { FieldName = "sprelimrprtn", Setter = new Action<string, CPageData>((s, loan) => { loan.sPrelimRprtN = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sPrelimRprtN.ToString();})},
new { FieldName = "sapprrprtod", Setter = new Action<string, CPageData>((s, loan) => { loan.sApprRprtOd_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sApprRprtOd_rep.ToString();})},
new { FieldName = "sappsubmittedd", Setter = new Action<string, CPageData>((s, loan) => { loan.sAppSubmittedD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sAppSubmittedD_rep.ToString();})},
new { FieldName = "sapprrprtdued", Setter = new Action<string, CPageData>((s, loan) => { loan.sApprRprtDueD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sApprRprtDueD_rep.ToString();})},
new { FieldName = "sapprrprtrd", Setter = new Action<string, CPageData>((s, loan) => { loan.sApprRprtRd_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sApprRprtRd_rep.ToString();})},
new { FieldName = "sapprrprtn", Setter = new Action<string, CPageData>((s, loan) => { loan.sApprRprtN = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sApprRprtN.ToString();})},
new { FieldName = "stilgfeod", Setter = new Action<string, CPageData>((s, loan) => { loan.sTilGfeOd_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sTilGfeOd_rep.ToString();})},
new { FieldName = "stilgfedued", Setter = new Action<string, CPageData>((s, loan) => { loan.sTilGfeDueD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sTilGfeDueD_rep.ToString();})},
new { FieldName = "stilgferd", Setter = new Action<string, CPageData>((s, loan) => { loan.sTilGfeRd_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sTilGfeRd_rep.ToString();})},
new { FieldName = "stilgfen", Setter = new Action<string, CPageData>((s, loan) => { loan.sTilGfeN = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sTilGfeN.ToString();})},
new { FieldName = "su1docstatdesc", Setter = new Action<string, CPageData>((s, loan) => { loan.sU1DocStatDesc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sU1DocStatDesc.ToString();})},
new { FieldName = "su1docstatod", Setter = new Action<string, CPageData>((s, loan) => { loan.sU1DocStatOd_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sU1DocStatOd_rep.ToString();})},
new { FieldName = "su1docstatdued", Setter = new Action<string, CPageData>((s, loan) => { loan.sU1DocStatDueD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sU1DocStatDueD_rep.ToString();})},
new { FieldName = "su1docstatrd", Setter = new Action<string, CPageData>((s, loan) => { loan.sU1DocStatRd_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sU1DocStatRd_rep.ToString();})},
new { FieldName = "su1docstatn", Setter = new Action<string, CPageData>((s, loan) => { loan.sU1DocStatN = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sU1DocStatN.ToString();})},
new { FieldName = "su2docstatdesc", Setter = new Action<string, CPageData>((s, loan) => { loan.sU2DocStatDesc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sU2DocStatDesc.ToString();})},
new { FieldName = "su2docstatod", Setter = new Action<string, CPageData>((s, loan) => { loan.sU2DocStatOd_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sU2DocStatOd_rep.ToString();})},
new { FieldName = "su2docstatdued", Setter = new Action<string, CPageData>((s, loan) => { loan.sU2DocStatDueD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sU2DocStatDueD_rep.ToString();})},
new { FieldName = "su2docstatrd", Setter = new Action<string, CPageData>((s, loan) => { loan.sU2DocStatRd_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sU2DocStatRd_rep.ToString();})},
new { FieldName = "su2docstatn", Setter = new Action<string, CPageData>((s, loan) => { loan.sU2DocStatN = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sU2DocStatN.ToString();})},
new { FieldName = "su3docstatdesc", Setter = new Action<string, CPageData>((s, loan) => { loan.sU3DocStatDesc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sU3DocStatDesc.ToString();})},
new { FieldName = "su3docstatod", Setter = new Action<string, CPageData>((s, loan) => { loan.sU3DocStatOd_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sU3DocStatOd_rep.ToString();})},
new { FieldName = "su3docstatdued", Setter = new Action<string, CPageData>((s, loan) => { loan.sU3DocStatDueD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sU3DocStatDueD_rep.ToString();})},
new { FieldName = "su3docstatrd", Setter = new Action<string, CPageData>((s, loan) => { loan.sU3DocStatRd_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sU3DocStatRd_rep.ToString();})},
new { FieldName = "su3docstatn", Setter = new Action<string, CPageData>((s, loan) => { loan.sU3DocStatN = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sU3DocStatN.ToString();})},
new { FieldName = "su4docstatdesc", Setter = new Action<string, CPageData>((s, loan) => { loan.sU4DocStatDesc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sU4DocStatDesc.ToString();})},
new { FieldName = "su4docstatod", Setter = new Action<string, CPageData>((s, loan) => { loan.sU4DocStatOd_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sU4DocStatOd_rep.ToString();})},
new { FieldName = "su4docstatdued", Setter = new Action<string, CPageData>((s, loan) => { loan.sU4DocStatDueD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sU4DocStatDueD_rep.ToString();})},
new { FieldName = "su4docstatrd", Setter = new Action<string, CPageData>((s, loan) => { loan.sU4DocStatRd_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sU4DocStatRd_rep.ToString();})},
new { FieldName = "su4docstatn", Setter = new Action<string, CPageData>((s, loan) => { loan.sU4DocStatN = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sU4DocStatN.ToString();})},
new { FieldName = "su5docstatdesc", Setter = new Action<string, CPageData>((s, loan) => { loan.sU5DocStatDesc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sU5DocStatDesc.ToString();})},
new { FieldName = "su5docstatod", Setter = new Action<string, CPageData>((s, loan) => { loan.sU5DocStatOd_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sU5DocStatOd_rep.ToString();})},
new { FieldName = "su5docstatdued", Setter = new Action<string, CPageData>((s, loan) => { loan.sU5DocStatDueD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sU5DocStatDueD_rep.ToString();})},
new { FieldName = "su5docstatrd", Setter = new Action<string, CPageData>((s, loan) => { loan.sU5DocStatRd_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sU5DocStatRd_rep.ToString();})},
new { FieldName = "su5docstatn", Setter = new Action<string, CPageData>((s, loan) => { loan.sU5DocStatN = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sU5DocStatN.ToString();})},
new { FieldName = "sleadsrcdesc", Setter = new Action<string, CPageData>((s, loan) => { loan.sLeadSrcDesc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sLeadSrcDesc.ToString();})},
new { FieldName = "sspaddr", Setter = new Action<string, CPageData>((s, loan) => { loan.sSpAddr = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sSpAddr.ToString();})},
new { FieldName = "sspcity", Setter = new Action<string, CPageData>((s, loan) => { loan.sSpCity = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sSpCity.ToString();})},
new { FieldName = "sspstate", Setter = new Action<string, CPageData>((s, loan) => { loan.sSpState = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sSpState.ToString();})},
new { FieldName = "sspcounty", Setter = new Action<string, CPageData>((s, loan) => { loan.sSpCounty = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sSpCounty.ToString();})},
new { FieldName = "sunitsnum", Setter = new Action<string, CPageData>((s, loan) => { loan.sUnitsNum_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sUnitsNum_rep.ToString();})},
new { FieldName = "syrbuilt", Setter = new Action<string, CPageData>((s, loan) => { loan.sYrBuilt = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sYrBuilt.ToString();})},
new { FieldName = "ssplegaldesc", Setter = new Action<string, CPageData>((s, loan) => { loan.sSpLegalDesc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sSpLegalDesc.ToString();})},
new { FieldName = "sprodcondostories", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdCondoStories_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdCondoStories_rep.ToString();})},
new { FieldName = "sprodisspinruralarea", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdIsSpInRuralArea = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdIsSpInRuralArea.ToString();})},
new { FieldName = "sprodiscondotel", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdIsCondotel = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdIsCondotel.ToString();})},
new { FieldName = "sprodisnonwarrantableproj", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdIsNonwarrantableProj = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdIsNonwarrantableProj.ToString();})},
new { FieldName = "sprodppmtpenaltymon", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdPpmtPenaltyMon_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdPpmtPenaltyMon_rep.ToString();})},
new { FieldName = "sprodrlckddays", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdRLckdDays_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdRLckdDays_rep.ToString();})},
new { FieldName = "sprodavailreservemonths", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdAvailReserveMonths_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdAvailReserveMonths_rep.ToString();})},
new { FieldName = "sprodppmtpenaltymon2ndlien", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdPpmtPenaltyMon2ndLien_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdPpmtPenaltyMon2ndLien_rep.ToString();})},
new { FieldName = "sionlymon", Setter = new Action<string, CPageData>((s, loan) => { loan.sIOnlyMon_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sIOnlyMon_rep.ToString();})},
new { FieldName = "sprodimpound", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdImpound = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdImpound.ToString();})},
new { FieldName = "sstatuslckd", Setter = new Action<string, CPageData>((s, loan) => { loan.sStatusLckd = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sStatusLckd.ToString();})},
new { FieldName = "sprofitgrossborpnt", Setter = new Action<string, CPageData>((s, loan) => { loan.sProfitGrossBorPnt_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sProfitGrossBorPnt_rep.ToString();})},
new { FieldName = "sprofitgrossbormb", Setter = new Action<string, CPageData>((s, loan) => { loan.sProfitGrossBorMb_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sProfitGrossBorMb_rep.ToString();})},
new { FieldName = "sprofitrebatepnt", Setter = new Action<string, CPageData>((s, loan) => { loan.sProfitRebatePnt_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sProfitRebatePnt_rep.ToString();})},
new { FieldName = "sprofitrebatemb", Setter = new Action<string, CPageData>((s, loan) => { loan.sProfitRebateMb_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sProfitRebateMb_rep.ToString();})},
new { FieldName = "sprofitancillaryfee", Setter = new Action<string, CPageData>((s, loan) => { loan.sProfitAncillaryFee_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sProfitAncillaryFee_rep.ToString();})},
new { FieldName = "sprofitancillaryfeelckd", Setter = new Action<string, CPageData>((s, loan) => { loan.sProfitAncillaryFeeLckd = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sProfitAncillaryFeeLckd.ToString();})},
new { FieldName = "sprofitexternalfee", Setter = new Action<string, CPageData>((s, loan) => { loan.sProfitExternalFee_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sProfitExternalFee_rep.ToString();})},
new { FieldName = "sbrokcomp1pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sBrokComp1Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sBrokComp1Pc_rep.ToString();})},
new { FieldName = "sradjmarginr", Setter = new Action<string, CPageData>((s, loan) => { loan.sRAdjMarginR_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sRAdjMarginR_rep.ToString();})},
new { FieldName = "srlckddays", Setter = new Action<string, CPageData>((s, loan) => { loan.sRLckdDays_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sRLckdDays_rep.ToString();})},
new { FieldName = "sppmtpenaltymon", Setter = new Action<string, CPageData>((s, loan) => { loan.sPpmtPenaltyMon_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sPpmtPenaltyMon_rep.ToString();})},
new { FieldName = "sradj1stcapr", Setter = new Action<string, CPageData>((s, loan) => { loan.sRAdj1stCapR_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sRAdj1stCapR_rep.ToString();})},
new { FieldName = "sradj1stcapmon", Setter = new Action<string, CPageData>((s, loan) => { loan.sRAdj1stCapMon_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sRAdj1stCapMon_rep.ToString();})},
new { FieldName = "sradjcapr", Setter = new Action<string, CPageData>((s, loan) => { loan.sRAdjCapR_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sRAdjCapR_rep.ToString();})},
new { FieldName = "sradjcapmon", Setter = new Action<string, CPageData>((s, loan) => { loan.sRAdjCapMon_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sRAdjCapMon_rep.ToString();})},
new { FieldName = "sradjlifecapr", Setter = new Action<string, CPageData>((s, loan) => { loan.sRAdjLifeCapR_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sRAdjLifeCapR_rep.ToString();})},
new { FieldName = "sradjindexr", Setter = new Action<string, CPageData>((s, loan) => { loan.sRAdjIndexR_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sRAdjIndexR_rep.ToString();})},
new { FieldName = "sradjfloorr", Setter = new Action<string, CPageData>((s, loan) => { loan.sRAdjFloorR_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sRAdjFloorR_rep.ToString();})},
new { FieldName = "sradjroundtor", Setter = new Action<string, CPageData>((s, loan) => { loan.sRAdjRoundToR_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sRAdjRoundToR_rep.ToString();})},
new { FieldName = "spmtadjcapr", Setter = new Action<string, CPageData>((s, loan) => { loan.sPmtAdjCapR_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sPmtAdjCapR_rep.ToString();})},
new { FieldName = "spmtadjcapmon", Setter = new Action<string, CPageData>((s, loan) => { loan.sPmtAdjCapMon_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sPmtAdjCapMon_rep.ToString();})},
new { FieldName = "spmtadjrecastperiodmon", Setter = new Action<string, CPageData>((s, loan) => { loan.sPmtAdjRecastPeriodMon_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sPmtAdjRecastPeriodMon_rep.ToString();})},
new { FieldName = "spmtadjrecaststop", Setter = new Action<string, CPageData>((s, loan) => { loan.sPmtAdjRecastStop_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sPmtAdjRecastStop_rep.ToString();})},
new { FieldName = "spmtadjmaxbalpc", Setter = new Action<string, CPageData>((s, loan) => { loan.sPmtAdjMaxBalPc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sPmtAdjMaxBalPc_rep.ToString();})},
new { FieldName = "sisselfemployed", Setter = new Action<string, CPageData>((s, loan) => { loan.sIsSelfEmployed = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sIsSelfEmployed.ToString();})},
new { FieldName = "sprodcrmanual120mortlatecount", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdCrManual120MortLateCount_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdCrManual120MortLateCount_rep.ToString();})},
new { FieldName = "sprodcrmanual150mortlatecount", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdCrManual150MortLateCount_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdCrManual150MortLateCount_rep.ToString();})},
new { FieldName = "sprodcrmanual30mortlatecount", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdCrManual30MortLateCount_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdCrManual30MortLateCount_rep.ToString();})},
new { FieldName = "sprodcrmanual60mortlatecount", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdCrManual60MortLateCount_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdCrManual60MortLateCount_rep.ToString();})},
new { FieldName = "sprodcrmanual90mortlatecount", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdCrManual90MortLateCount_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdCrManual90MortLateCount_rep.ToString();})},
new { FieldName = "sprodcrmanualbk13has", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdCrManualBk13Has = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdCrManualBk13Has.ToString();})},
new { FieldName = "sprodcrmanualbk13recentfilemon", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdCrManualBk13RecentFileMon_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdCrManualBk13RecentFileMon_rep.ToString();})},
new { FieldName = "sprodcrmanualbk13recentfileyr", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdCrManualBk13RecentFileYr_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdCrManualBk13RecentFileYr_rep.ToString();})},
new { FieldName = "sprodcrmanualbk13recentsatisfiedmon", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdCrManualBk13RecentSatisfiedMon_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdCrManualBk13RecentSatisfiedMon_rep.ToString();})},
new { FieldName = "sprodcrmanualbk13recentsatisfiedyr", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdCrManualBk13RecentSatisfiedYr_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdCrManualBk13RecentSatisfiedYr_rep.ToString();})},
new { FieldName = "sprodcrmanualbk7has", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdCrManualBk7Has = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdCrManualBk7Has.ToString();})},
new { FieldName = "sprodcrmanualbk7recentfilemon", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdCrManualBk7RecentFileMon_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdCrManualBk7RecentFileMon_rep.ToString();})},
new { FieldName = "sprodcrmanualbk7recentfileyr", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdCrManualBk7RecentFileYr_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdCrManualBk7RecentFileYr_rep.ToString();})},
new { FieldName = "sprodcrmanualbk7recentsatisfiedmon", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdCrManualBk7RecentSatisfiedMon_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdCrManualBk7RecentSatisfiedMon_rep.ToString();})},
new { FieldName = "sprodcrmanualbk7recentsatisfiedyr", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdCrManualBk7RecentSatisfiedYr_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdCrManualBk7RecentSatisfiedYr_rep.ToString();})},
new { FieldName = "sprodcrmanualforeclosurehas", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdCrManualForeclosureHas = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdCrManualForeclosureHas.ToString();})},
new { FieldName = "sprodcrmanualforeclosurerecentfilemon", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdCrManualForeclosureRecentFileMon_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdCrManualForeclosureRecentFileMon_rep.ToString();})},
new { FieldName = "sprodcrmanualforeclosurerecentfileyr", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdCrManualForeclosureRecentFileYr_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdCrManualForeclosureRecentFileYr_rep.ToString();})},
new { FieldName = "sprodcrmanualforeclosurerecentsatisfiedmon", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdCrManualForeclosureRecentSatisfiedMon_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdCrManualForeclosureRecentSatisfiedMon_rep.ToString();})},
new { FieldName = "sprodcrmanualforeclosurerecentsatisfiedyr", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdCrManualForeclosureRecentSatisfiedYr_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdCrManualForeclosureRecentSatisfiedYr_rep.ToString();})},
new { FieldName = "sprodcrmanualnonrolling30mortlatecount", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdCrManualNonRolling30MortLateCount_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdCrManualNonRolling30MortLateCount_rep.ToString();})},
new { FieldName = "sprodcrmanualrolling60mortlatecount", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdCrManualRolling60MortLateCount_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdCrManualRolling60MortLateCount_rep.ToString();})},
new { FieldName = "sprodcrmanualrolling90mortlatecount", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdCrManualRolling90MortLateCount_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdCrManualRolling90MortLateCount_rep.ToString();})},
new { FieldName = "soptionarmteaserr", Setter = new Action<string, CPageData>((s, loan) => { loan.sOptionArmTeaserR_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sOptionArmTeaserR_rep.ToString();})},
new { FieldName = "sisoptionarm", Setter = new Action<string, CPageData>((s, loan) => { loan.sIsOptionArm = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sIsOptionArm.ToString();})},
new { FieldName = "soptionarmteaserrsubmitted", Setter = new Action<string, CPageData>((s, loan) => { loan.sOptionArmTeaserRSubmitted_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sOptionArmTeaserRSubmitted_rep.ToString();})},
new { FieldName = "sisoptionarmsubmitted", Setter = new Action<string, CPageData>((s, loan) => { loan.sIsOptionArmSubmitted = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sIsOptionArmSubmitted.ToString();})},
new { FieldName = "soptionarmminpayperiod", Setter = new Action<string, CPageData>((s, loan) => { loan.sOptionArmMinPayPeriod_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sOptionArmMinPayPeriod_rep.ToString();})},
new { FieldName = "soptionarmintroductoryperiod", Setter = new Action<string, CPageData>((s, loan) => { loan.sOptionArmIntroductoryPeriod_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sOptionArmIntroductoryPeriod_rep.ToString();})},
new { FieldName = "soptionarminitialfixminpmtperiod", Setter = new Action<string, CPageData>((s, loan) => { loan.sOptionArmInitialFixMinPmtPeriod_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sOptionArmInitialFixMinPmtPeriod_rep.ToString();})},
new { FieldName = "soptionarmminpayisioonly", Setter = new Action<string, CPageData>((s, loan) => { loan.sOptionArmMinPayIsIOOnly = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sOptionArmMinPayIsIOOnly.ToString();})},
new { FieldName = "sisfullamortafterrecast", Setter = new Action<string, CPageData>((s, loan) => { loan.sIsFullAmortAfterRecast = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sIsFullAmortAfterRecast.ToString();})},
new { FieldName = "soptionarmpmtdiscount", Setter = new Action<string, CPageData>((s, loan) => { loan.sOptionArmPmtDiscount_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sOptionArmPmtDiscount_rep.ToString();})},
new { FieldName = "soptionarmnoteirdiscount", Setter = new Action<string, CPageData>((s, loan) => { loan.sOptionArmNoteIRDiscount_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sOptionArmNoteIRDiscount_rep.ToString();})},
new { FieldName = "sprodestimatedresiduali", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdEstimatedResidualI_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdEstimatedResidualI_rep.ToString();})},

//Query 2

new { FieldName = "sprodfilterfinmethfixed", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdFilterFinMethFixed = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdFilterFinMethFixed.ToString();})},
new { FieldName = "sprodfilterfinmeth3yrsarm", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdFilterFinMeth3YrsArm = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdFilterFinMeth3YrsArm.ToString();})},
new { FieldName = "sprodfilterfinmeth5yrsarm", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdFilterFinMeth5YrsArm = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdFilterFinMeth5YrsArm.ToString();})},
new { FieldName = "sprodfilterfinmeth7yrsarm", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdFilterFinMeth7YrsArm = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdFilterFinMeth7YrsArm.ToString();})},
new { FieldName = "sprodfilterfinmeth10yrsarm", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdFilterFinMeth10YrsArm = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdFilterFinMeth10YrsArm.ToString();})},
new { FieldName = "sprodfilterfinmethother", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdFilterFinMethOther = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdFilterFinMethOther.ToString();})},
new { FieldName = "sspacqyr", Setter = new Action<string, CPageData>((s, loan) => { loan.sSpAcqYr = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sSpAcqYr.ToString();})},
new { FieldName = "ssporigc", Setter = new Action<string, CPageData>((s, loan) => { loan.sSpOrigC_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sSpOrigC_rep.ToString();})},
new { FieldName = "ssplien", Setter = new Action<string, CPageData>((s, loan) => { loan.sSpLien_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sSpLien_rep.ToString();})},
new { FieldName = "srefpurpose", Setter = new Action<string, CPageData>((s, loan) => { loan.sRefPurpose = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sRefPurpose.ToString();})},
new { FieldName = "sspimprovdesc", Setter = new Action<string, CPageData>((s, loan) => { loan.sSpImprovDesc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sSpImprovDesc.ToString();})},
new { FieldName = "sspimprovc", Setter = new Action<string, CPageData>((s, loan) => { loan.sSpImprovC_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sSpImprovC_rep.ToString();})},
new { FieldName = "scustomfield1desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField1Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField1Desc.ToString();})},
new { FieldName = "scustomfield1d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField1D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField1D_rep.ToString();})},
new { FieldName = "scustomfield1money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField1Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField1Money_rep.ToString();})},
new { FieldName = "scustomfield1pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField1Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField1Pc_rep.ToString();})},
new { FieldName = "scustomfield1bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField1Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField1Bit.ToString();})},
new { FieldName = "scustomfield1notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField1Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField1Notes.ToString();})},
new { FieldName = "scustomfield2desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField2Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField2Desc.ToString();})},
new { FieldName = "scustomfield2d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField2D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField2D_rep.ToString();})},
new { FieldName = "scustomfield2money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField2Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField2Money_rep.ToString();})},
new { FieldName = "scustomfield2pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField2Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField2Pc_rep.ToString();})},
new { FieldName = "scustomfield2bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField2Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField2Bit.ToString();})},
new { FieldName = "scustomfield2notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField2Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField2Notes.ToString();})},
new { FieldName = "scustomfield3desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField3Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField3Desc.ToString();})},
new { FieldName = "scustomfield3d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField3D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField3D_rep.ToString();})},
new { FieldName = "scustomfield3money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField3Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField3Money_rep.ToString();})},
new { FieldName = "scustomfield3pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField3Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField3Pc_rep.ToString();})},
new { FieldName = "scustomfield3bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField3Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField3Bit.ToString();})},
new { FieldName = "scustomfield3notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField3Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField3Notes.ToString();})},
new { FieldName = "scustomfield4desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField4Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField4Desc.ToString();})},
new { FieldName = "scustomfield4d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField4D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField4D_rep.ToString();})},
new { FieldName = "scustomfield4money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField4Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField4Money_rep.ToString();})},
new { FieldName = "scustomfield4pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField4Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField4Pc_rep.ToString();})},
new { FieldName = "scustomfield4bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField4Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField4Bit.ToString();})},
new { FieldName = "scustomfield4notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField4Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField4Notes.ToString();})},
new { FieldName = "scustomfield5desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField5Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField5Desc.ToString();})},
new { FieldName = "scustomfield5d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField5D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField5D_rep.ToString();})},
new { FieldName = "scustomfield5money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField5Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField5Money_rep.ToString();})},
new { FieldName = "scustomfield5pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField5Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField5Pc_rep.ToString();})},
new { FieldName = "scustomfield5bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField5Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField5Bit.ToString();})},
new { FieldName = "scustomfield5notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField5Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField5Notes.ToString();})},
new { FieldName = "scustomfield6desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField6Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField6Desc.ToString();})},
new { FieldName = "scustomfield6d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField6D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField6D_rep.ToString();})},
new { FieldName = "scustomfield6money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField6Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField6Money_rep.ToString();})},
new { FieldName = "scustomfield6pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField6Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField6Pc_rep.ToString();})},
new { FieldName = "scustomfield6bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField6Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField6Bit.ToString();})},
new { FieldName = "scustomfield6notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField6Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField6Notes.ToString();})},
new { FieldName = "scustomfield7desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField7Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField7Desc.ToString();})},
new { FieldName = "scustomfield7d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField7D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField7D_rep.ToString();})},
new { FieldName = "scustomfield7money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField7Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField7Money_rep.ToString();})},
new { FieldName = "scustomfield7pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField7Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField7Pc_rep.ToString();})},
new { FieldName = "scustomfield7bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField7Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField7Bit.ToString();})},
new { FieldName = "scustomfield7notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField7Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField7Notes.ToString();})},
new { FieldName = "scustomfield8desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField8Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField8Desc.ToString();})},
new { FieldName = "scustomfield8d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField8D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField8D_rep.ToString();})},
new { FieldName = "scustomfield8money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField8Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField8Money_rep.ToString();})},
new { FieldName = "scustomfield8pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField8Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField8Pc_rep.ToString();})},
new { FieldName = "scustomfield8bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField8Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField8Bit.ToString();})},
new { FieldName = "scustomfield8notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField8Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField8Notes.ToString();})},
new { FieldName = "scustomfield9desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField9Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField9Desc.ToString();})},
new { FieldName = "scustomfield9d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField9D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField9D_rep.ToString();})},
new { FieldName = "scustomfield9money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField9Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField9Money_rep.ToString();})},
new { FieldName = "scustomfield9pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField9Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField9Pc_rep.ToString();})},
new { FieldName = "scustomfield9bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField9Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField9Bit.ToString();})},
new { FieldName = "scustomfield9notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField9Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField9Notes.ToString();})},
new { FieldName = "scustomfield10desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField10Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField10Desc.ToString();})},
new { FieldName = "scustomfield10d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField10D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField10D_rep.ToString();})},
new { FieldName = "scustomfield10money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField10Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField10Money_rep.ToString();})},
new { FieldName = "scustomfield10pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField10Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField10Pc_rep.ToString();})},
new { FieldName = "scustomfield10bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField10Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField10Bit.ToString();})},
new { FieldName = "scustomfield10notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField10Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField10Notes.ToString();})},
new { FieldName = "scustomfield11desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField11Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField11Desc.ToString();})},
new { FieldName = "scustomfield11d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField11D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField11D_rep.ToString();})},
new { FieldName = "scustomfield11money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField11Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField11Money_rep.ToString();})},
new { FieldName = "scustomfield11pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField11Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField11Pc_rep.ToString();})},
new { FieldName = "scustomfield11bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField11Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField11Bit.ToString();})},
new { FieldName = "scustomfield11notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField11Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField11Notes.ToString();})},
new { FieldName = "scustomfield12desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField12Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField12Desc.ToString();})},
new { FieldName = "scustomfield12d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField12D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField12D_rep.ToString();})},
new { FieldName = "scustomfield12money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField12Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField12Money_rep.ToString();})},
new { FieldName = "scustomfield12pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField12Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField12Pc_rep.ToString();})},
new { FieldName = "scustomfield12bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField12Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField12Bit.ToString();})},
new { FieldName = "scustomfield12notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField12Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField12Notes.ToString();})},
new { FieldName = "scustomfield13desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField13Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField13Desc.ToString();})},
new { FieldName = "scustomfield13d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField13D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField13D_rep.ToString();})},
new { FieldName = "scustomfield13money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField13Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField13Money_rep.ToString();})},
new { FieldName = "scustomfield13pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField13Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField13Pc_rep.ToString();})},
new { FieldName = "scustomfield13bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField13Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField13Bit.ToString();})},
new { FieldName = "scustomfield13notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField13Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField13Notes.ToString();})},
new { FieldName = "scustomfield14desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField14Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField14Desc.ToString();})},
new { FieldName = "scustomfield14d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField14D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField14D_rep.ToString();})},
new { FieldName = "scustomfield14money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField14Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField14Money_rep.ToString();})},
new { FieldName = "scustomfield14pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField14Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField14Pc_rep.ToString();})},
new { FieldName = "scustomfield14bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField14Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField14Bit.ToString();})},
new { FieldName = "scustomfield14notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField14Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField14Notes.ToString();})},
new { FieldName = "scustomfield15desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField15Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField15Desc.ToString();})},
new { FieldName = "scustomfield15d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField15D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField15D_rep.ToString();})},
new { FieldName = "scustomfield15money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField15Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField15Money_rep.ToString();})},
new { FieldName = "scustomfield15pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField15Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField15Pc_rep.ToString();})},
new { FieldName = "scustomfield15bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField15Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField15Bit.ToString();})},
new { FieldName = "scustomfield15notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField15Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField15Notes.ToString();})},
new { FieldName = "scustomfield16desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField16Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField16Desc.ToString();})},
new { FieldName = "scustomfield16d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField16D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField16D_rep.ToString();})},
new { FieldName = "scustomfield16money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField16Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField16Money_rep.ToString();})},
new { FieldName = "scustomfield16pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField16Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField16Pc_rep.ToString();})},
new { FieldName = "scustomfield16bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField16Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField16Bit.ToString();})},
new { FieldName = "scustomfield16notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField16Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField16Notes.ToString();})},
new { FieldName = "scustomfield17desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField17Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField17Desc.ToString();})},
new { FieldName = "scustomfield17d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField17D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField17D_rep.ToString();})},
new { FieldName = "scustomfield17money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField17Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField17Money_rep.ToString();})},
new { FieldName = "scustomfield17pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField17Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField17Pc_rep.ToString();})},
new { FieldName = "scustomfield17bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField17Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField17Bit.ToString();})},
new { FieldName = "scustomfield17notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField17Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField17Notes.ToString();})},
new { FieldName = "scustomfield18desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField18Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField18Desc.ToString();})},
new { FieldName = "scustomfield18d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField18D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField18D_rep.ToString();})},
new { FieldName = "scustomfield18money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField18Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField18Money_rep.ToString();})},
new { FieldName = "scustomfield18pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField18Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField18Pc_rep.ToString();})},
new { FieldName = "scustomfield18bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField18Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField18Bit.ToString();})},
new { FieldName = "scustomfield18notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField18Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField18Notes.ToString();})},
new { FieldName = "scustomfield19desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField19Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField19Desc.ToString();})},
new { FieldName = "scustomfield19d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField19D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField19D_rep.ToString();})},
new { FieldName = "scustomfield19money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField19Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField19Money_rep.ToString();})},
new { FieldName = "scustomfield19pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField19Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField19Pc_rep.ToString();})},
new { FieldName = "scustomfield19bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField19Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField19Bit.ToString();})},
new { FieldName = "scustomfield19notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField19Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField19Notes.ToString();})},
new { FieldName = "scustomfield20desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField20Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField20Desc.ToString();})},
new { FieldName = "scustomfield20d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField20D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField20D_rep.ToString();})},
new { FieldName = "scustomfield20money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField20Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField20Money_rep.ToString();})},
new { FieldName = "scustomfield20pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField20Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField20Pc_rep.ToString();})},
new { FieldName = "scustomfield20bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField20Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField20Bit.ToString();})},
new { FieldName = "scustomfield20notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField20Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField20Notes.ToString();})},
new { FieldName = "scustomfield21desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField21Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField21Desc.ToString();})},
new { FieldName = "scustomfield21d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField21D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField21D_rep.ToString();})},
new { FieldName = "scustomfield21money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField21Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField21Money_rep.ToString();})},
new { FieldName = "scustomfield21pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField21Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField21Pc_rep.ToString();})},
new { FieldName = "scustomfield21bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField21Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField21Bit.ToString();})},
new { FieldName = "scustomfield21notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField21Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField21Notes.ToString();})},
new { FieldName = "scustomfield22desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField22Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField22Desc.ToString();})},
new { FieldName = "scustomfield22d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField22D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField22D_rep.ToString();})},
new { FieldName = "scustomfield22money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField22Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField22Money_rep.ToString();})},
new { FieldName = "scustomfield22pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField22Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField22Pc_rep.ToString();})},
new { FieldName = "scustomfield22bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField22Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField22Bit.ToString();})},
new { FieldName = "scustomfield22notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField22Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField22Notes.ToString();})},
new { FieldName = "scustomfield23desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField23Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField23Desc.ToString();})},
new { FieldName = "scustomfield23d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField23D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField23D_rep.ToString();})},
new { FieldName = "scustomfield23money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField23Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField23Money_rep.ToString();})},
new { FieldName = "scustomfield23pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField23Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField23Pc_rep.ToString();})},
new { FieldName = "scustomfield23bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField23Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField23Bit.ToString();})},
new { FieldName = "scustomfield23notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField23Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField23Notes.ToString();})},
new { FieldName = "scustomfield24desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField24Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField24Desc.ToString();})},
new { FieldName = "scustomfield24d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField24D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField24D_rep.ToString();})},
new { FieldName = "scustomfield24money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField24Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField24Money_rep.ToString();})},
new { FieldName = "scustomfield24pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField24Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField24Pc_rep.ToString();})},
new { FieldName = "scustomfield24bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField24Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField24Bit.ToString();})},
new { FieldName = "scustomfield24notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField24Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField24Notes.ToString();})},
new { FieldName = "scustomfield25desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField25Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField25Desc.ToString();})},
new { FieldName = "scustomfield25d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField25D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField25D_rep.ToString();})},
new { FieldName = "scustomfield25money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField25Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField25Money_rep.ToString();})},
new { FieldName = "scustomfield25pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField25Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField25Pc_rep.ToString();})},
new { FieldName = "scustomfield25bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField25Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField25Bit.ToString();})},
new { FieldName = "scustomfield25notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField25Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField25Notes.ToString();})},
new { FieldName = "scustomfield26desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField26Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField26Desc.ToString();})},
new { FieldName = "scustomfield26d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField26D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField26D_rep.ToString();})},
new { FieldName = "scustomfield26money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField26Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField26Money_rep.ToString();})},
new { FieldName = "scustomfield26pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField26Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField26Pc_rep.ToString();})},
new { FieldName = "scustomfield26bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField26Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField26Bit.ToString();})},
new { FieldName = "scustomfield26notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField26Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField26Notes.ToString();})},
new { FieldName = "scustomfield27desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField27Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField27Desc.ToString();})},
new { FieldName = "scustomfield27d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField27D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField27D_rep.ToString();})},
new { FieldName = "scustomfield27money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField27Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField27Money_rep.ToString();})},
new { FieldName = "scustomfield27pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField27Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField27Pc_rep.ToString();})},
new { FieldName = "scustomfield27bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField27Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField27Bit.ToString();})},
new { FieldName = "scustomfield27notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField27Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField27Notes.ToString();})},
new { FieldName = "scustomfield28desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField28Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField28Desc.ToString();})},
new { FieldName = "scustomfield28d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField28D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField28D_rep.ToString();})},
new { FieldName = "scustomfield28money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField28Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField28Money_rep.ToString();})},
new { FieldName = "scustomfield28pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField28Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField28Pc_rep.ToString();})},
new { FieldName = "scustomfield28bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField28Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField28Bit.ToString();})},
new { FieldName = "scustomfield28notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField28Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField28Notes.ToString();})},
new { FieldName = "scustomfield29desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField29Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField29Desc.ToString();})},
new { FieldName = "scustomfield29d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField29D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField29D_rep.ToString();})},
new { FieldName = "scustomfield29money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField29Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField29Money_rep.ToString();})},
new { FieldName = "scustomfield29pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField29Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField29Pc_rep.ToString();})},
new { FieldName = "scustomfield29bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField29Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField29Bit.ToString();})},
new { FieldName = "scustomfield29notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField29Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField29Notes.ToString();})},
new { FieldName = "scustomfield30desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField30Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField30Desc.ToString();})},
new { FieldName = "scustomfield30d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField30D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField30D_rep.ToString();})},
new { FieldName = "scustomfield30money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField30Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField30Money_rep.ToString();})},
new { FieldName = "scustomfield30pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField30Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField30Pc_rep.ToString();})},
new { FieldName = "scustomfield30bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField30Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField30Bit.ToString();})},
new { FieldName = "scustomfield30notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField30Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField30Notes.ToString();})},
new { FieldName = "scustomfield31desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField31Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField31Desc.ToString();})},
new { FieldName = "scustomfield31d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField31D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField31D_rep.ToString();})},
new { FieldName = "scustomfield31money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField31Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField31Money_rep.ToString();})},
new { FieldName = "scustomfield31pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField31Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField31Pc_rep.ToString();})},
new { FieldName = "scustomfield31bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField31Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField31Bit.ToString();})},
new { FieldName = "scustomfield31notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField31Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField31Notes.ToString();})},
new { FieldName = "scustomfield32desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField32Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField32Desc.ToString();})},
new { FieldName = "scustomfield32d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField32D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField32D_rep.ToString();})},
new { FieldName = "scustomfield32money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField32Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField32Money_rep.ToString();})},
new { FieldName = "scustomfield32pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField32Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField32Pc_rep.ToString();})},
new { FieldName = "scustomfield32bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField32Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField32Bit.ToString();})},
new { FieldName = "scustomfield32notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField32Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField32Notes.ToString();})},
new { FieldName = "scustomfield33desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField33Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField33Desc.ToString();})},
new { FieldName = "scustomfield33d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField33D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField33D_rep.ToString();})},
new { FieldName = "scustomfield33money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField33Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField33Money_rep.ToString();})},
new { FieldName = "scustomfield33pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField33Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField33Pc_rep.ToString();})},
new { FieldName = "scustomfield33bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField33Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField33Bit.ToString();})},
new { FieldName = "scustomfield33notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField33Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField33Notes.ToString();})},
new { FieldName = "scustomfield34desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField34Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField34Desc.ToString();})},
new { FieldName = "scustomfield34d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField34D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField34D_rep.ToString();})},
new { FieldName = "scustomfield34money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField34Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField34Money_rep.ToString();})},
new { FieldName = "scustomfield34pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField34Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField34Pc_rep.ToString();})},
new { FieldName = "scustomfield34bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField34Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField34Bit.ToString();})},
new { FieldName = "scustomfield34notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField34Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField34Notes.ToString();})},
new { FieldName = "scustomfield35desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField35Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField35Desc.ToString();})},
new { FieldName = "scustomfield35d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField35D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField35D_rep.ToString();})},
new { FieldName = "scustomfield35money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField35Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField35Money_rep.ToString();})},
new { FieldName = "scustomfield35pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField35Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField35Pc_rep.ToString();})},
new { FieldName = "scustomfield35bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField35Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField35Bit.ToString();})},
new { FieldName = "scustomfield35notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField35Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField35Notes.ToString();})},
new { FieldName = "scustomfield36desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField36Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField36Desc.ToString();})},
new { FieldName = "scustomfield36d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField36D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField36D_rep.ToString();})},
new { FieldName = "scustomfield36money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField36Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField36Money_rep.ToString();})},
new { FieldName = "scustomfield36pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField36Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField36Pc_rep.ToString();})},
new { FieldName = "scustomfield36bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField36Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField36Bit.ToString();})},
new { FieldName = "scustomfield36notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField36Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField36Notes.ToString();})},
new { FieldName = "scustomfield37desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField37Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField37Desc.ToString();})},
new { FieldName = "scustomfield37d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField37D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField37D_rep.ToString();})},
new { FieldName = "scustomfield37money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField37Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField37Money_rep.ToString();})},
new { FieldName = "scustomfield37pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField37Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField37Pc_rep.ToString();})},
new { FieldName = "scustomfield37bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField37Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField37Bit.ToString();})},
new { FieldName = "scustomfield37notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField37Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField37Notes.ToString();})},
new { FieldName = "scustomfield38desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField38Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField38Desc.ToString();})},
new { FieldName = "scustomfield38d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField38D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField38D_rep.ToString();})},
new { FieldName = "scustomfield38money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField38Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField38Money_rep.ToString();})},
new { FieldName = "scustomfield38pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField38Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField38Pc_rep.ToString();})},
new { FieldName = "scustomfield38bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField38Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField38Bit.ToString();})},
new { FieldName = "scustomfield38notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField38Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField38Notes.ToString();})},
new { FieldName = "scustomfield39desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField39Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField39Desc.ToString();})},
new { FieldName = "scustomfield39d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField39D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField39D_rep.ToString();})},
new { FieldName = "scustomfield39money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField39Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField39Money_rep.ToString();})},
new { FieldName = "scustomfield39pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField39Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField39Pc_rep.ToString();})},
new { FieldName = "scustomfield39bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField39Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField39Bit.ToString();})},
new { FieldName = "scustomfield39notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField39Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField39Notes.ToString();})},
new { FieldName = "scustomfield40desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField40Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField40Desc.ToString();})},
new { FieldName = "scustomfield40d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField40D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField40D_rep.ToString();})},
new { FieldName = "scustomfield40money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField40Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField40Money_rep.ToString();})},
new { FieldName = "scustomfield40pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField40Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField40Pc_rep.ToString();})},
new { FieldName = "scustomfield40bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField40Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField40Bit.ToString();})},
new { FieldName = "scustomfield40notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField40Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField40Notes.ToString();})},
new { FieldName = "scustomfield41desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField41Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField41Desc.ToString();})},
new { FieldName = "scustomfield41d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField41D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField41D_rep.ToString();})},
new { FieldName = "scustomfield41money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField41Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField41Money_rep.ToString();})},
new { FieldName = "scustomfield41pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField41Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField41Pc_rep.ToString();})},
new { FieldName = "scustomfield41bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField41Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField41Bit.ToString();})},
new { FieldName = "scustomfield41notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField41Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField41Notes.ToString();})},
new { FieldName = "scustomfield42desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField42Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField42Desc.ToString();})},
new { FieldName = "scustomfield42d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField42D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField42D_rep.ToString();})},
new { FieldName = "scustomfield42money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField42Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField42Money_rep.ToString();})},
new { FieldName = "scustomfield42pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField42Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField42Pc_rep.ToString();})},
new { FieldName = "scustomfield42bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField42Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField42Bit.ToString();})},
new { FieldName = "scustomfield42notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField42Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField42Notes.ToString();})},
new { FieldName = "scustomfield43desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField43Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField43Desc.ToString();})},
new { FieldName = "scustomfield43d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField43D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField43D_rep.ToString();})},
new { FieldName = "scustomfield43money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField43Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField43Money_rep.ToString();})},
new { FieldName = "scustomfield43pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField43Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField43Pc_rep.ToString();})},
new { FieldName = "scustomfield43bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField43Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField43Bit.ToString();})},
new { FieldName = "scustomfield43notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField43Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField43Notes.ToString();})},
new { FieldName = "scustomfield44desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField44Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField44Desc.ToString();})},
new { FieldName = "scustomfield44d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField44D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField44D_rep.ToString();})},
new { FieldName = "scustomfield44money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField44Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField44Money_rep.ToString();})},
new { FieldName = "scustomfield44pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField44Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField44Pc_rep.ToString();})},
new { FieldName = "scustomfield44bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField44Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField44Bit.ToString();})},
new { FieldName = "scustomfield44notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField44Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField44Notes.ToString();})},
new { FieldName = "scustomfield45desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField45Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField45Desc.ToString();})},
new { FieldName = "scustomfield45d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField45D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField45D_rep.ToString();})},
new { FieldName = "scustomfield45money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField45Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField45Money_rep.ToString();})},
new { FieldName = "scustomfield45pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField45Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField45Pc_rep.ToString();})},
new { FieldName = "scustomfield45bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField45Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField45Bit.ToString();})},
new { FieldName = "scustomfield45notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField45Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField45Notes.ToString();})},
new { FieldName = "scustomfield46desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField46Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField46Desc.ToString();})},
new { FieldName = "scustomfield46d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField46D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField46D_rep.ToString();})},
new { FieldName = "scustomfield46money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField46Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField46Money_rep.ToString();})},
new { FieldName = "scustomfield46pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField46Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField46Pc_rep.ToString();})},
new { FieldName = "scustomfield46bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField46Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField46Bit.ToString();})},
new { FieldName = "scustomfield46notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField46Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField46Notes.ToString();})},
new { FieldName = "scustomfield47desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField47Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField47Desc.ToString();})},
new { FieldName = "scustomfield47d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField47D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField47D_rep.ToString();})},
new { FieldName = "scustomfield47money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField47Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField47Money_rep.ToString();})},
new { FieldName = "scustomfield47pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField47Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField47Pc_rep.ToString();})},
new { FieldName = "scustomfield47bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField47Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField47Bit.ToString();})},
new { FieldName = "scustomfield47notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField47Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField47Notes.ToString();})},
new { FieldName = "scustomfield48desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField48Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField48Desc.ToString();})},
new { FieldName = "scustomfield48d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField48D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField48D_rep.ToString();})},
new { FieldName = "scustomfield48money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField48Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField48Money_rep.ToString();})},
new { FieldName = "scustomfield48pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField48Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField48Pc_rep.ToString();})},
new { FieldName = "scustomfield48bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField48Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField48Bit.ToString();})},
new { FieldName = "scustomfield48notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField48Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField48Notes.ToString();})},
new { FieldName = "scustomfield49desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField49Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField49Desc.ToString();})},
new { FieldName = "scustomfield49d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField49D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField49D_rep.ToString();})},
new { FieldName = "scustomfield49money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField49Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField49Money_rep.ToString();})},
new { FieldName = "scustomfield49pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField49Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField49Pc_rep.ToString();})},
new { FieldName = "scustomfield49bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField49Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField49Bit.ToString();})},
new { FieldName = "scustomfield49notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField49Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField49Notes.ToString();})},
new { FieldName = "scustomfield50desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField50Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField50Desc.ToString();})},
new { FieldName = "scustomfield50d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField50D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField50D_rep.ToString();})},
new { FieldName = "scustomfield50money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField50Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField50Money_rep.ToString();})},
new { FieldName = "scustomfield50pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField50Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField50Pc_rep.ToString();})},
new { FieldName = "scustomfield50bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField50Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField50Bit.ToString();})},
new { FieldName = "scustomfield50notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField50Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField50Notes.ToString();})},
new { FieldName = "scustomfield51desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField51Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField51Desc.ToString();})},
new { FieldName = "scustomfield51d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField51D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField51D_rep.ToString();})},
new { FieldName = "scustomfield51money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField51Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField51Money_rep.ToString();})},
new { FieldName = "scustomfield51pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField51Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField51Pc_rep.ToString();})},
new { FieldName = "scustomfield51bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField51Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField51Bit.ToString();})},
new { FieldName = "scustomfield51notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField51Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField51Notes.ToString();})},
new { FieldName = "scustomfield52desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField52Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField52Desc.ToString();})},
new { FieldName = "scustomfield52d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField52D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField52D_rep.ToString();})},
new { FieldName = "scustomfield52money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField52Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField52Money_rep.ToString();})},
new { FieldName = "scustomfield52pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField52Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField52Pc_rep.ToString();})},
new { FieldName = "scustomfield52bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField52Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField52Bit.ToString();})},
new { FieldName = "scustomfield52notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField52Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField52Notes.ToString();})},
new { FieldName = "scustomfield53desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField53Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField53Desc.ToString();})},
new { FieldName = "scustomfield53d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField53D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField53D_rep.ToString();})},
new { FieldName = "scustomfield53money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField53Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField53Money_rep.ToString();})},
new { FieldName = "scustomfield53pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField53Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField53Pc_rep.ToString();})},
new { FieldName = "scustomfield53bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField53Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField53Bit.ToString();})},
new { FieldName = "scustomfield53notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField53Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField53Notes.ToString();})},
new { FieldName = "scustomfield54desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField54Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField54Desc.ToString();})},
new { FieldName = "scustomfield54d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField54D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField54D_rep.ToString();})},
new { FieldName = "scustomfield54money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField54Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField54Money_rep.ToString();})},
new { FieldName = "scustomfield54pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField54Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField54Pc_rep.ToString();})},
new { FieldName = "scustomfield54bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField54Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField54Bit.ToString();})},
new { FieldName = "scustomfield54notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField54Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField54Notes.ToString();})},
new { FieldName = "scustomfield55desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField55Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField55Desc.ToString();})},
new { FieldName = "scustomfield55d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField55D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField55D_rep.ToString();})},
new { FieldName = "scustomfield55money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField55Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField55Money_rep.ToString();})},
new { FieldName = "scustomfield55pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField55Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField55Pc_rep.ToString();})},
new { FieldName = "scustomfield55bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField55Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField55Bit.ToString();})},
new { FieldName = "scustomfield55notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField55Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField55Notes.ToString();})},
new { FieldName = "scustomfield56desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField56Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField56Desc.ToString();})},
new { FieldName = "scustomfield56d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField56D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField56D_rep.ToString();})},
new { FieldName = "scustomfield56money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField56Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField56Money_rep.ToString();})},
new { FieldName = "scustomfield56pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField56Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField56Pc_rep.ToString();})},
new { FieldName = "scustomfield56bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField56Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField56Bit.ToString();})},
new { FieldName = "scustomfield56notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField56Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField56Notes.ToString();})},
new { FieldName = "scustomfield57desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField57Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField57Desc.ToString();})},
new { FieldName = "scustomfield57d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField57D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField57D_rep.ToString();})},
new { FieldName = "scustomfield57money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField57Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField57Money_rep.ToString();})},
new { FieldName = "scustomfield57pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField57Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField57Pc_rep.ToString();})},
new { FieldName = "scustomfield57bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField57Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField57Bit.ToString();})},
new { FieldName = "scustomfield57notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField57Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField57Notes.ToString();})},
new { FieldName = "scustomfield58desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField58Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField58Desc.ToString();})},
new { FieldName = "scustomfield58d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField58D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField58D_rep.ToString();})},
new { FieldName = "scustomfield58money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField58Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField58Money_rep.ToString();})},
new { FieldName = "scustomfield58pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField58Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField58Pc_rep.ToString();})},
new { FieldName = "scustomfield58bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField58Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField58Bit.ToString();})},
new { FieldName = "scustomfield58notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField58Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField58Notes.ToString();})},
new { FieldName = "scustomfield59desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField59Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField59Desc.ToString();})},
new { FieldName = "scustomfield59d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField59D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField59D_rep.ToString();})},
new { FieldName = "scustomfield59money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField59Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField59Money_rep.ToString();})},
new { FieldName = "scustomfield59pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField59Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField59Pc_rep.ToString();})},
new { FieldName = "scustomfield59bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField59Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField59Bit.ToString();})},
new { FieldName = "scustomfield59notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField59Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField59Notes.ToString();})},
new { FieldName = "scustomfield60desc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField60Desc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField60Desc.ToString();})},
new { FieldName = "scustomfield60d", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField60D_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField60D_rep.ToString();})},
new { FieldName = "scustomfield60money", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField60Money_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField60Money_rep.ToString();})},
new { FieldName = "scustomfield60pc", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField60Pc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField60Pc_rep.ToString();})},
new { FieldName = "scustomfield60bit", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField60Bit = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField60Bit.ToString();})},
new { FieldName = "scustomfield60notes", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomField60Notes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomField60Notes.ToString();})},
new { FieldName = "strnotes", Setter = new Action<string, CPageData>((s, loan) => { loan.sTrNotes = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sTrNotes.ToString();})},
new { FieldName = "sfhalenderidcode", Setter = new Action<string, CPageData>((s, loan) => { loan.sFHALenderIdCode = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sFHALenderIdCode.ToString();})},
new { FieldName = "sfhasponsoragentidcode", Setter = new Action<string, CPageData>((s, loan) => { loan.sFHASponsorAgentIdCode = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sFHASponsorAgentIdCode.ToString();})},
new { FieldName = "sfhasponsoredoriginatorein", Setter = new Action<string, CPageData>((s, loan) => { loan.sFHASponsoredOriginatorEIN = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sFHASponsoredOriginatorEIN.ToString();})},
new { FieldName = "soriginatorcompensationborrpaidpc", Setter = new Action<string, CPageData>((s, loan) => { loan.sOriginatorCompensationBorrPaidPc_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sOriginatorCompensationBorrPaidPc_rep.ToString();})},
new { FieldName = "soriginatorcompensationborrpaidmb", Setter = new Action<string, CPageData>((s, loan) => { loan.sOriginatorCompensationBorrPaidMb_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sOriginatorCompensationBorrPaidMb_rep.ToString();})},
new { FieldName = "sprocessingd", Setter = new Action<string, CPageData>((s, loan) => { loan.sProcessingD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sProcessingD_rep.ToString();})},
new { FieldName = "sfinalunderwritingd", Setter = new Action<string, CPageData>((s, loan) => { loan.sFinalUnderwritingD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sFinalUnderwritingD_rep.ToString();})},
new { FieldName = "sdocsbackd", Setter = new Action<string, CPageData>((s, loan) => { loan.sDocsBackD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sDocsBackD_rep.ToString();})},
new { FieldName = "sfundingconditionsd", Setter = new Action<string, CPageData>((s, loan) => { loan.sFundingConditionsD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sFundingConditionsD_rep.ToString();})},
new { FieldName = "sfinaldocsd", Setter = new Action<string, CPageData>((s, loan) => { loan.sFinalDocsD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sFinalDocsD_rep.ToString();})},
new { FieldName = "sffufmipr", Setter = new Action<string, CPageData>((s, loan) => { loan.sFfUfmipR_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sFfUfmipR_rep.ToString();})},
new { FieldName = "sprodisdurefiplus", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdIsDuRefiPlus = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdIsDuRefiPlus.ToString();})},
new { FieldName = "sffufmipisbeingfinanced", Setter = new Action<string, CPageData>((s, loan) => { loan.sFfUfMipIsBeingFinanced = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sFfUfMipIsBeingFinanced.ToString();})},
new { FieldName = "sprodisvafundingfinanced", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdIsVaFundingFinanced = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdIsVaFundingFinanced.ToString();})},
new { FieldName = "sprodvafundingfee", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdVaFundingFee_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdVaFundingFee_rep.ToString();})},
new { FieldName = "sprodisusdaruralhousingfeefinanced", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdIsUsdaRuralHousingFeeFinanced = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdIsUsdaRuralHousingFeeFinanced.ToString();})},
new { FieldName = "shmdamsanum", Setter = new Action<string, CPageData>((s, loan) => { loan.sHmdaMsaNum = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sHmdaMsaNum.ToString();})},
new { FieldName = "shmdacountycode", Setter = new Action<string, CPageData>((s, loan) => { loan.sHmdaCountyCode = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sHmdaCountyCode.ToString();})},
new { FieldName = "shmdastatecode", Setter = new Action<string, CPageData>((s, loan) => { loan.sHmdaStateCode = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sHmdaStateCode.ToString();})},
new { FieldName = "shmdacensustract", Setter = new Action<string, CPageData>((s, loan) => { loan.sHmdaCensusTract = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sHmdaCensusTract.ToString();})},
new { FieldName = "sfloodcertificationisinspecialarea", Setter = new Action<string, CPageData>((s, loan) => { loan.sFloodCertificationIsInSpecialArea = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sFloodCertificationIsInSpecialArea.ToString();})},
new { FieldName = "sfloodcertificationmapnum", Setter = new Action<string, CPageData>((s, loan) => { loan.sFloodCertificationMapNum = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sFloodCertificationMapNum.ToString();})},
new { FieldName = "sfloodcertificationpanelnums", Setter = new Action<string, CPageData>((s, loan) => { loan.sFloodCertificationPanelNums = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sFloodCertificationPanelNums.ToString();})},
new { FieldName = "sfloodcertificationpanelsuffix", Setter = new Action<string, CPageData>((s, loan) => { loan.sFloodCertificationPanelSuffix = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sFloodCertificationPanelSuffix.ToString();})},
new { FieldName = "sfloodcertificationcommunitynum", Setter = new Action<string, CPageData>((s, loan) => { loan.sFloodCertificationCommunityNum = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sFloodCertificationCommunityNum.ToString();})},
new { FieldName = "snfipfloodzoneid", Setter = new Action<string, CPageData>((s, loan) => { loan.sNfipFloodZoneId = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sNfipFloodZoneId.ToString();})},
new { FieldName = "sfloodcertificationdeterminationd", Setter = new Action<string, CPageData>((s, loan) => { loan.sFloodCertificationDeterminationD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sFloodCertificationDeterminationD_rep.ToString();})},
new { FieldName = "sfloodcertid", Setter = new Action<string, CPageData>((s, loan) => { loan.sFloodCertId = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sFloodCertId.ToString();})},
new { FieldName = "sfloodhazardcommunitydesc", Setter = new Action<string, CPageData>((s, loan) => { loan.sFloodHazardCommunityDesc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sFloodHazardCommunityDesc.ToString();})},
new { FieldName = "sfloodcertificationparticipationstatus", Setter = new Action<string, CPageData>((s, loan) => { loan.sFloodCertificationParticipationStatus = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sFloodCertificationParticipationStatus.ToString();})},
new { FieldName = "sfloodcertificationiscbraoropa", Setter = new Action<string, CPageData>((s, loan) => { loan.sFloodCertificationIsCBRAorOPA = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sFloodCertificationIsCBRAorOPA.ToString();})},
new { FieldName = "sfloodcertificationdesignationd", Setter = new Action<string, CPageData>((s, loan) => { loan.sFloodCertificationDesignationD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sFloodCertificationDesignationD_rep.ToString();})},
new { FieldName = "sfloodcertificationislolupgraded", Setter = new Action<string, CPageData>((s, loan) => { loan.sFloodCertificationIsLOLUpgraded = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sFloodCertificationIsLOLUpgraded.ToString();})},
new { FieldName = "sfloodcertificationlolupgraded", Setter = new Action<string, CPageData>((s, loan) => { loan.sFloodCertificationLOLUpgradeD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sFloodCertificationLOLUpgradeD_rep.ToString();})},
new { FieldName = "sfloodcertificationlomchangedd", Setter = new Action<string, CPageData>((s, loan) => { loan.sFloodCertificationLOMChangedD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sFloodCertificationLOMChangedD_rep.ToString();})},
new { FieldName = "sfloodcertificationmapd", Setter = new Action<string, CPageData>((s, loan) => { loan.sFloodCertificationMapD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sFloodCertificationMapD_rep.ToString();})},
new { FieldName = "scustompmlfield1", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomPMLField1_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomPMLField1_rep.ToString();})},
new { FieldName = "scustompmlfield2", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomPMLField2_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomPMLField2_rep.ToString();})},
new { FieldName = "scustompmlfield3", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomPMLField3_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomPMLField3_rep.ToString();})},
new { FieldName = "scustompmlfield4", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomPMLField4_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomPMLField4_rep.ToString();})},
new { FieldName = "scustompmlfield5", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomPMLField5_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomPMLField5_rep.ToString();})},
new { FieldName = "scustompmlfield6", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomPMLField6_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomPMLField6_rep.ToString();})},
new { FieldName = "scustompmlfield7", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomPMLField7_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomPMLField7_rep.ToString();})},
new { FieldName = "scustompmlfield8", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomPMLField8_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomPMLField8_rep.ToString();})},
new { FieldName = "scustompmlfield9", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomPMLField9_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomPMLField9_rep.ToString();})},
new { FieldName = "scustompmlfield10", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomPMLField10_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomPMLField10_rep.ToString();})},
new { FieldName = "scustompmlfield11", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomPMLField11_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomPMLField11_rep.ToString();})},
new { FieldName = "scustompmlfield12", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomPMLField12_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomPMLField12_rep.ToString();})},
new { FieldName = "scustompmlfield13", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomPMLField13_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomPMLField13_rep.ToString();})},
new { FieldName = "scustompmlfield14", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomPMLField14_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomPMLField14_rep.ToString();})},
new { FieldName = "scustompmlfield15", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomPMLField15_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomPMLField15_rep.ToString();})},
new { FieldName = "scustompmlfield16", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomPMLField16_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomPMLField16_rep.ToString();})},
new { FieldName = "scustompmlfield17", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomPMLField17_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomPMLField17_rep.ToString();})},
new { FieldName = "scustompmlfield18", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomPMLField18_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomPMLField18_rep.ToString();})},
new { FieldName = "scustompmlfield19", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomPMLField19_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomPMLField19_rep.ToString();})},
new { FieldName = "scustompmlfield20", Setter = new Action<string, CPageData>((s, loan) => { loan.sCustomPMLField20_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sCustomPMLField20_rep.ToString();})},
new { FieldName = "sspvaluationeffectived", Setter = new Action<string, CPageData>((s, loan) => { loan.sSpValuationEffectiveD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sSpValuationEffectiveD_rep.ToString();})},
new { FieldName = "sspunit1bedroomscount", Setter = new Action<string, CPageData>((s, loan) => { loan.sSpUnit1BedroomsCount_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sSpUnit1BedroomsCount_rep.ToString();})},
new { FieldName = "sspunit2bedroomscount", Setter = new Action<string, CPageData>((s, loan) => { loan.sSpUnit2BedroomsCount_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sSpUnit2BedroomsCount_rep.ToString();})},
new { FieldName = "sspunit3bedroomscount", Setter = new Action<string, CPageData>((s, loan) => { loan.sSpUnit3BedroomsCount_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sSpUnit3BedroomsCount_rep.ToString();})},
new { FieldName = "sspunit4bedroomscount", Setter = new Action<string, CPageData>((s, loan) => { loan.sSpUnit4BedroomsCount_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sSpUnit4BedroomsCount_rep.ToString();})},
new { FieldName = "sspappraisalid", Setter = new Action<string, CPageData>((s, loan) => { loan.sSpAppraisalId = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sSpAppraisalId.ToString();})},
new { FieldName = "sspsubmittedtoucdpd", Setter = new Action<string, CPageData>((s, loan) => { loan.sSpSubmittedToUcdpD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sSpSubmittedToUcdpD_rep.ToString();})},
new { FieldName = "stitleinspolicyorderedd", Setter = new Action<string, CPageData>((s, loan) => { loan.sTitleInsPolicyOrderedD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sTitleInsPolicyOrderedD_rep.ToString();})},
new { FieldName = "stitleinspolicydocumentd", Setter = new Action<string, CPageData>((s, loan) => { loan.sTitleInsPolicyDocumentD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sTitleInsPolicyDocumentD_rep.ToString();})},
new { FieldName = "stitleinspolicyreceivedd", Setter = new Action<string, CPageData>((s, loan) => { loan.sTitleInsPolicyReceivedD_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sTitleInsPolicyReceivedD_rep.ToString();})},
new { FieldName = "sassessorsparcelid", Setter = new Action<string, CPageData>((s, loan) => { loan.sAssessorsParcelId = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sAssessorsParcelId.ToString();})},
new { FieldName = "srequiredendorsements", Setter = new Action<string, CPageData>((s, loan) => { loan.sRequiredEndorsements = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sRequiredEndorsements.ToString();})},
new { FieldName = "stitlereportitemsdescription", Setter = new Action<string, CPageData>((s, loan) => { loan.sTitleReportItemsDescription = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sTitleReportItemsDescription.ToString();})},
new { FieldName = "svestingtoread", Setter = new Action<string, CPageData>((s, loan) => { loan.sVestingToRead = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.sVestingToRead.ToString();})},
new { FieldName = "svestingtoreadlckd", Setter = new Action<string, CPageData>((s, loan) => { loan.sVestingToReadLckd = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sVestingToReadLckd.ToString();})},



new { FieldName = "slt", Setter = new Action<string, CPageData>((s, loan) => { loan.sLT = (E_sLT)Enum.Parse(typeof(E_sLT), s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sLT.ToString("D");})},
new { FieldName = "sfinmetht", Setter = new Action<string, CPageData>((s, loan) => { loan.sFinMethT = (E_sFinMethT)Enum.Parse(typeof(E_sFinMethT), s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sFinMethT.ToString("D");})},
new { FieldName = "sprohazinst", Setter = new Action<string, CPageData>((s, loan) => { loan.sProHazInsT = (E_PercentBaseT)Enum.Parse(typeof(E_PercentBaseT), s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sProHazInsT.ToString("D");})},
new { FieldName = "sprorealetxt", Setter = new Action<string, CPageData>((s, loan) => { loan.sProRealETxT = (E_PercentBaseT)Enum.Parse(typeof(E_PercentBaseT), s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sProRealETxT.ToString("D");})},
new { FieldName = "sprominst", Setter = new Action<string, CPageData>((s, loan) => { loan.sProMInsT = (E_PercentBaseT)Enum.Parse(typeof(E_PercentBaseT), s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sProMInsT.ToString("D");})},
new { FieldName = "sinvestorlockcommitmentt", Setter = new Action<string, CPageData>((s, loan) => { loan.sInvestorLockCommitmentT = (E_sInvestorLockCommitmentT)Enum.Parse(typeof(E_sInvestorLockCommitmentT), s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sInvestorLockCommitmentT.ToString("D");})},
new { FieldName = "sproddoct", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdDocT = (E_sProdDocT)Enum.Parse(typeof(E_sProdDocT), s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdDocT.ToString("D");})},
new { FieldName = "sprodfinmethfiltert", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdFinMethFilterT = (E_sProdFinMethFilterT)Enum.Parse(typeof(E_sProdFinMethFilterT), s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdFinMethFilterT.ToString("D");})},
new { FieldName = "sprodspstructuret", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdSpStructureT = (E_sProdSpStructureT)Enum.Parse(typeof(E_sProdSpStructureT), s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdSpStructureT.ToString("D");})},
new { FieldName = "sradjroundt", Setter = new Action<string, CPageData>((s, loan) => { loan.sRAdjRoundT = (E_sRAdjRoundT)Enum.Parse(typeof(E_sRAdjRoundT), s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sRAdjRoundT.ToString("D");})},
new { FieldName = "sprodcrmanualbk13recentstatust", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdCrManualBk13RecentStatusT = (E_sProdCrManualDerogRecentStatusT)Enum.Parse(typeof(E_sProdCrManualDerogRecentStatusT), s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdCrManualBk13RecentStatusT.ToString("D");})},
new { FieldName = "sprodcrmanualbk7recentstatust", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdCrManualBk7RecentStatusT = (E_sProdCrManualDerogRecentStatusT)Enum.Parse(typeof(E_sProdCrManualDerogRecentStatusT), s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdCrManualBk7RecentStatusT.ToString("D");})},
new { FieldName = "sprodcrmanualforeclosurerecentstatust", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdCrManualForeclosureRecentStatusT = (E_sProdCrManualDerogRecentStatusT)Enum.Parse(typeof(E_sProdCrManualDerogRecentStatusT), s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdCrManualForeclosureRecentStatusT.ToString("D");})},
new { FieldName = "sprodmioptiont", Setter = new Action<string, CPageData>((s, loan) => { loan.sProdMIOptionT = (E_sProdMIOptionT)Enum.Parse(typeof(E_sProdMIOptionT), s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sProdMIOptionT.ToString("D");})},

new { FieldName = "slpurposet", Setter = new Action<string, CPageData>((s, loan) => { loan.sLPurposeT = (E_sLPurposeT)Enum.Parse(typeof(E_sLPurposeT), s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sLPurposeT.ToString("D");})},
new { FieldName = "slienpost", Setter = new Action<string, CPageData>((s, loan) => { loan.sLienPosT = (E_sLienPosT)Enum.Parse(typeof(E_sLienPosT), s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sLienPosT.ToString("D");})},
new { FieldName = "soptionarmminpayoptiont", Setter = new Action<string, CPageData>((s, loan) => { loan.sOptionArmMinPayOptionT = (E_sOptionArmMinPayOptionT)Enum.Parse(typeof(E_sOptionArmMinPayOptionT), s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sOptionArmMinPayOptionT.ToString("D");})},
new { FieldName = "sspimprovtimeframet", Setter = new Action<string, CPageData>((s, loan) => { loan.sSpImprovTimeFrameT = (E_sSpImprovTimeFrameT)Enum.Parse(typeof(E_sSpImprovTimeFrameT), s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sSpImprovTimeFrameT.ToString("D");})},
new { FieldName = "stotalscorefhaproductt", Setter = new Action<string, CPageData>((s, loan) => { loan.sTotalScoreFhaProductT = (E_sTotalScoreFhaProductT)Enum.Parse(typeof(E_sTotalScoreFhaProductT), s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sTotalScoreFhaProductT.ToString("D");})},
new { FieldName = "soriginatorcompensationborrpaidbaset", Setter = new Action<string, CPageData>((s, loan) => { loan.sOriginatorCompensationBorrPaidBaseT = (E_PercentBaseT)Enum.Parse(typeof(E_PercentBaseT), s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sOriginatorCompensationBorrPaidBaseT.ToString("D");})},
new { FieldName = "sfhacondoapprovalstatust", Setter = new Action<string, CPageData>((s, loan) => { loan.sFhaCondoApprovalStatusT = (E_sFhaCondoApprovalStatusT)Enum.Parse(typeof(E_sFhaCondoApprovalStatusT), s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sFhaCondoApprovalStatusT.ToString("D");})},
new { FieldName = "sspvaluationmethodt", Setter = new Action<string, CPageData>((s, loan) => { loan.sSpValuationMethodT = (E_sSpValuationMethodT)Enum.Parse(typeof(E_sSpValuationMethodT), s);}), Getter = new Func<CPageData, string>((loan) => {return loan.sSpValuationMethodT.ToString("D");})},

new { FieldName = "abfirstnm", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBFirstNm = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBFirstNm.ToString();})},
new { FieldName = "abmidnm", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBMidNm = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBMidNm.ToString();})},
new { FieldName = "ablastnm", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBLastNm = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBLastNm.ToString();})},
new { FieldName = "absuffix", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBSuffix = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBSuffix.ToString();})},
new { FieldName = "abssn", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBSsn = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBSsn.ToString();})},
new { FieldName = "abdob", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBDob_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBDob_rep.ToString();})},
new { FieldName = "abage", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBAge_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBAge_rep.ToString();})},
new { FieldName = "abschoolyrs", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBSchoolYrs_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBSchoolYrs_rep.ToString();})},
new { FieldName = "abdependnum", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBDependNum_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBDependNum_rep.ToString();})},
new { FieldName = "abdependages", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBDependAges = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBDependAges.ToString();})},
new { FieldName = "abhphone", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBHPhone = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBHPhone.ToString();})},
new { FieldName = "abcellphone", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBCellPhone = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBCellPhone.ToString();})},
new { FieldName = "abbusphone", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBBusPhone = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBBusPhone.ToString();})},
new { FieldName = "abfax", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBFax = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBFax.ToString();})},
new { FieldName = "abemail", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBEmail = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBEmail.ToString();})},
new { FieldName = "abaddr", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBAddr = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBAddr.ToString();})},
new { FieldName = "abcity", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBCity = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBCity.ToString();})},
new { FieldName = "abstate", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBState = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBState.ToString();})},
new { FieldName = "abzip", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBZip = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBZip.ToString();})},
new { FieldName = "abaddryrs", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBAddrYrs = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBAddrYrs.ToString();})},
new { FieldName = "abaddrmailusepresentaddr", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBAddrMailUsePresentAddr = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBAddrMailUsePresentAddr.ToString();})},
new { FieldName = "abaddrmail", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBAddrMail = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBAddrMail.ToString();})},
new { FieldName = "abcitymail", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBCityMail = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBCityMail.ToString();})},
new { FieldName = "abstatemail", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBStateMail = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBStateMail.ToString();})},
new { FieldName = "abzipmail", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBZipMail = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBZipMail.ToString();})},
new { FieldName = "abprev1addr", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBPrev1Addr = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBPrev1Addr.ToString();})},
new { FieldName = "abprev1city", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBPrev1City = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBPrev1City.ToString();})},
new { FieldName = "abprev1state", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBPrev1State = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBPrev1State.ToString();})},
new { FieldName = "abprev1addryrs", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBPrev1AddrYrs = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBPrev1AddrYrs.ToString();})},
new { FieldName = "abprev2addr", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBPrev2Addr = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBPrev2Addr.ToString();})},
new { FieldName = "abprev2city", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBPrev2City = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBPrev2City.ToString();})},
new { FieldName = "abprev2state", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBPrev2State = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBPrev2State.ToString();})},
new { FieldName = "abprev2addryrs", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBPrev2AddrYrs = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBPrev2AddrYrs.ToString();})},
new { FieldName = "acfirstnm", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCFirstNm = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCFirstNm.ToString();})},
new { FieldName = "acmidnm", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCMidNm = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCMidNm.ToString();})},
new { FieldName = "aclastnm", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCLastNm = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCLastNm.ToString();})},
new { FieldName = "acsuffix", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCSuffix = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCSuffix.ToString();})},
new { FieldName = "acssn", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCSsn = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCSsn.ToString();})},
new { FieldName = "acdob", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCDob_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCDob_rep.ToString();})},
new { FieldName = "acage", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCAge_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCAge_rep.ToString();})},
new { FieldName = "acschoolyrs", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCSchoolYrs_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCSchoolYrs_rep.ToString();})},
new { FieldName = "acdependnum", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCDependNum_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCDependNum_rep.ToString();})},
new { FieldName = "acdependages", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCDependAges = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCDependAges.ToString();})},
new { FieldName = "achphone", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCHPhone = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCHPhone.ToString();})},
new { FieldName = "accellphone", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCCellPhone = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCCellPhone.ToString();})},
new { FieldName = "acbusphone", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCBusPhone = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCBusPhone.ToString();})},
new { FieldName = "acfax", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCFax = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCFax.ToString();})},
new { FieldName = "acemail", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCEmail = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCEmail.ToString();})},
new { FieldName = "acaddr", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCAddr = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCAddr.ToString();})},
new { FieldName = "accity", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCCity = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCCity.ToString();})},
new { FieldName = "acstate", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCState = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCState.ToString();})},
new { FieldName = "aczip", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCZip = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCZip.ToString();})},
new { FieldName = "acaddryrs", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCAddrYrs = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCAddrYrs.ToString();})},
new { FieldName = "acaddrmailusepresentaddr", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCAddrMailUsePresentAddr = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCAddrMailUsePresentAddr.ToString();})},
new { FieldName = "acaddrmail", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCAddrMail = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCAddrMail.ToString();})},
new { FieldName = "accitymail", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCCityMail = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCCityMail.ToString();})},
new { FieldName = "acstatemail", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCStateMail = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCStateMail.ToString();})},
new { FieldName = "aczipmail", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCZipMail = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCZipMail.ToString();})},
new { FieldName = "acprev1addr", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCPrev1Addr = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCPrev1Addr.ToString();})},
new { FieldName = "acprev1city", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCPrev1City = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCPrev1City.ToString();})},
new { FieldName = "acprev1state", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCPrev1State = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCPrev1State.ToString();})},
new { FieldName = "acprev1addryrs", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCPrev1AddrYrs = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCPrev1AddrYrs.ToString();})},
new { FieldName = "acprev2addr", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCPrev2Addr = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCPrev2Addr.ToString();})},
new { FieldName = "acprev2city", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCPrev2City = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCPrev2City.ToString();})},
new { FieldName = "acprev2state", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCPrev2State = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCPrev2State.ToString();})},
new { FieldName = "acprev2addryrs", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCPrev2AddrYrs = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCPrev2AddrYrs.ToString();})},
new { FieldName = "abbasei", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBBaseI_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBBaseI_rep.ToString();})},
new { FieldName = "acbasei", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCBaseI_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCBaseI_rep.ToString();})},
new { FieldName = "abovertimei", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBOvertimeI_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBOvertimeI_rep.ToString();})},
new { FieldName = "acovertimei", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCOvertimeI_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCOvertimeI_rep.ToString();})},
new { FieldName = "abbonusesi", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBBonusesI_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBBonusesI_rep.ToString();})},
new { FieldName = "acbonusesi", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCBonusesI_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCBonusesI_rep.ToString();})},
new { FieldName = "abcommisioni", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBCommisionI_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBCommisionI_rep.ToString();})},
new { FieldName = "accommisioni", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCCommisionI_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCCommisionI_rep.ToString();})},
new { FieldName = "abdividendi", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBDividendI_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBDividendI_rep.ToString();})},
new { FieldName = "acdividendi", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCDividendI_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCDividendI_rep.ToString();})},
new { FieldName = "anetrenti1003lckd", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aNetRentI1003Lckd = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aNetRentI1003Lckd.ToString();})},
new { FieldName = "abnetrenti1003", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBNetRentI1003_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBNetRentI1003_rep.ToString();})},
new { FieldName = "acnetrenti1003", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCNetRentI1003_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCNetRentI1003_rep.ToString();})},
new { FieldName = "abdecjudgment", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBDecJudgment = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBDecJudgment.ToString();})},
new { FieldName = "abdecbankrupt", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBDecBankrupt = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBDecBankrupt.ToString();})},
new { FieldName = "abdecforeclosure", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBDecForeclosure = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBDecForeclosure.ToString();})},
new { FieldName = "abdeclawsuit", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBDecLawsuit = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBDecLawsuit.ToString();})},
new { FieldName = "abdecobligated", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBDecObligated = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBDecObligated.ToString();})},
new { FieldName = "abdecdelinquent", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBDecDelinquent = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBDecDelinquent.ToString();})},
new { FieldName = "abdecalimony", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBDecAlimony = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBDecAlimony.ToString();})},
new { FieldName = "abdecborrowing", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBDecBorrowing = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBDecBorrowing.ToString();})},
new { FieldName = "abdecendorser", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBDecEndorser = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBDecEndorser.ToString();})},
new { FieldName = "abdeccitizen", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBDecCitizen = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBDecCitizen.ToString();})},
new { FieldName = "abdecresidency", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBDecResidency = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBDecResidency.ToString();})},
new { FieldName = "abdecocc", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBDecOcc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBDecOcc.ToString();})},
new { FieldName = "abdecpastownership", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBDecPastOwnership = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBDecPastOwnership.ToString();})},
new { FieldName = "acdecjudgment", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCDecJudgment = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCDecJudgment.ToString();})},
new { FieldName = "acdecbankrupt", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCDecBankrupt = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCDecBankrupt.ToString();})},
new { FieldName = "acdecforeclosure", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCDecForeclosure = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCDecForeclosure.ToString();})},
new { FieldName = "acdeclawsuit", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCDecLawsuit = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCDecLawsuit.ToString();})},
new { FieldName = "acdecobligated", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCDecObligated = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCDecObligated.ToString();})},
new { FieldName = "acdecdelinquent", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCDecDelinquent = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCDecDelinquent.ToString();})},
new { FieldName = "acdecalimony", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCDecAlimony = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCDecAlimony.ToString();})},
new { FieldName = "acdecborrowing", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCDecBorrowing = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCDecBorrowing.ToString();})},
new { FieldName = "acdecendorser", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCDecEndorser = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCDecEndorser.ToString();})},
new { FieldName = "acdeccitizen", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCDecCitizen = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCDecCitizen.ToString();})},
new { FieldName = "acdecresidency", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCDecResidency = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCDecResidency.ToString();})},
new { FieldName = "acdecocc", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCDecOcc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCDecOcc.ToString();})},
new { FieldName = "acdecpastownership", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCDecPastOwnership = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCDecPastOwnership.ToString();})},
new { FieldName = "abnofurnish", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBNoFurnish = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBNoFurnish.ToString();})},
new { FieldName = "acnofurnish", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCNoFurnish = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCNoFurnish.ToString();})},
new { FieldName = "abisamericanindian", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBIsAmericanIndian = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBIsAmericanIndian.ToString();})},
new { FieldName = "acisamericanindian", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCIsAmericanIndian = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCIsAmericanIndian.ToString();})},
new { FieldName = "abisasian", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBIsAsian = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBIsAsian.ToString();})},
new { FieldName = "acisasian", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCIsAsian = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCIsAsian.ToString();})},
new { FieldName = "abisblack", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBIsBlack = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBIsBlack.ToString();})},
new { FieldName = "acisblack", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCIsBlack = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCIsBlack.ToString();})},
new { FieldName = "abispacificislander", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBIsPacificIslander = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBIsPacificIslander.ToString();})},
new { FieldName = "acispacificislander", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCIsPacificIslander = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCIsPacificIslander.ToString();})},
new { FieldName = "abiswhite", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBIsWhite = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBIsWhite.ToString();})},
new { FieldName = "aciswhite", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCIsWhite = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCIsWhite.ToString();})},
new { FieldName = "abexperianscore", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBExperianScore_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBExperianScore_rep.ToString();})},
new { FieldName = "abtransunionscore", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBTransUnionScore_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBTransUnionScore_rep.ToString();})},
new { FieldName = "abequifaxscore", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBEquifaxScore_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBEquifaxScore_rep.ToString();})},
new { FieldName = "acexperianscore", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCExperianScore_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCExperianScore_rep.ToString();})},
new { FieldName = "actransunionscore", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCTransUnionScore_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCTransUnionScore_rep.ToString();})},
new { FieldName = "acequifaxscore", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCEquifaxScore_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCEquifaxScore_rep.ToString();})},
new { FieldName = "aisborrspouseprimarywageearner", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aIsBorrSpousePrimaryWageEarner = ToBool(s);}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aIsBorrSpousePrimaryWageEarner.ToString();})},


new { FieldName = "abmaritalstatt", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBMaritalStatT = (E_aBMaritalStatT)Enum.Parse(typeof(E_aBMaritalStatT), s);}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBMaritalStatT.ToString("D");})},
new { FieldName = "abaddrmailsourcet", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBAddrMailSourceT = (E_aAddrMailSourceT)Enum.Parse(typeof(E_aAddrMailSourceT), s);}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBAddrMailSourceT.ToString("D");})},
new { FieldName = "acmaritalstatt", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCMaritalStatT = (E_aCMaritalStatT)Enum.Parse(typeof(E_aCMaritalStatT), s);}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCMaritalStatT.ToString("D");})},
new { FieldName = "acaddrmailsourcet", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCAddrMailSourceT = (E_aAddrMailSourceT)Enum.Parse(typeof(E_aAddrMailSourceT), s);}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCAddrMailSourceT.ToString("D");})},
new { FieldName = "abdecpastownedpropt", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBDecPastOwnedPropT = (E_aBDecPastOwnedPropT)Enum.Parse(typeof(E_aBDecPastOwnedPropT), s);}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBDecPastOwnedPropT.ToString("D");})},
new { FieldName = "abdecpastownedproptitlet", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBDecPastOwnedPropTitleT = (E_aBDecPastOwnedPropTitleT)Enum.Parse(typeof(E_aBDecPastOwnedPropTitleT), s);}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBDecPastOwnedPropTitleT.ToString("D");})},
new { FieldName = "acdecpastownedpropt", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCDecPastOwnedPropT = (E_aCDecPastOwnedPropT)Enum.Parse(typeof(E_aCDecPastOwnedPropT), s);}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCDecPastOwnedPropT.ToString("D");})},
new { FieldName = "acdecpastownedproptitlet", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCDecPastOwnedPropTitleT = (E_aCDecPastOwnedPropTitleT)Enum.Parse(typeof(E_aCDecPastOwnedPropTitleT), s);}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCDecPastOwnedPropTitleT.ToString("D");})},
new { FieldName = "abhispanict", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBHispanicT = (E_aHispanicT)Enum.Parse(typeof(E_aHispanicT), s);}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBHispanicT.ToString("D");})},
new { FieldName = "achispanict", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCHispanicT = (E_aHispanicT)Enum.Parse(typeof(E_aHispanicT), s);}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCHispanicT.ToString("D");})},
new { FieldName = "abgender", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBGender = (E_GenderT)Enum.Parse(typeof(E_GenderT), s);}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBGender.ToString("D");})},
new { FieldName = "acgender", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCGender = (E_GenderT)Enum.Parse(typeof(E_GenderT), s);}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCGender.ToString("D");})},
new { FieldName = "aocct", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aOccT = (E_aOccT)Enum.Parse(typeof(E_aOccT), s);}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aOccT.ToString("D");})},

new { FieldName = "abtotalscorefhtbcounselingt", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBTotalScoreFhtbCounselingT = (E_aTotalScoreFhtbCounselingT)Enum.Parse(typeof(E_aTotalScoreFhtbCounselingT), s);}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBTotalScoreFhtbCounselingT.ToString("D");})},
new { FieldName = "actotalscorefhtbcounselingt", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCTotalScoreFhtbCounselingT = (E_aTotalScoreFhtbCounselingT)Enum.Parse(typeof(E_aTotalScoreFhtbCounselingT), s);}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCTotalScoreFhtbCounselingT.ToString("D");})},
new { FieldName = "abrelationshiptitlet", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBRelationshipTitleT = (E_aRelationshipTitleT)Enum.Parse(typeof(E_aRelationshipTitleT), s);}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBRelationshipTitleT.ToString("D");})},
new { FieldName = "acrelationshiptitlet", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCRelationshipTitleT = (E_aRelationshipTitleT)Enum.Parse(typeof(E_aRelationshipTitleT), s);}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCRelationshipTitleT.ToString("D");})},

new { FieldName = "apresrent", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aPresRent_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aPresRent_rep.ToString();})},
new { FieldName = "apres1stm", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aPres1stM_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aPres1stM_rep.ToString();})},
new { FieldName = "apresofin", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aPresOFin_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aPresOFin_rep.ToString();})},
new { FieldName = "apreshoassocdues", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aPresHoAssocDues_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aPresHoAssocDues_rep.ToString();})},
new { FieldName = "apreshazins", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aPresHazIns_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aPresHazIns_rep.ToString();})},
new { FieldName = "apresmins", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aPresMIns_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aPresMIns_rep.ToString();})},
new { FieldName = "apresrealetx", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aPresRealETx_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aPresRealETx_rep.ToString();})},
new { FieldName = "apresohexp", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aPresOHExp_rep = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aPresOHExp_rep.ToString();})},
new { FieldName = "amanner", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aManner = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aManner.ToString();})},
new { FieldName = "abrelationshiptitleotherdesc", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aBRelationshipTitleOtherDesc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aBRelationshipTitleOtherDesc.ToString();})},
new { FieldName = "acrelationshiptitleotherdesc", Setter = new Action<string, CPageData>((s, loan) => { loan.GetAppData(0).aCRelationshipTitleOtherDesc = s;}), Getter = new Func<CPageData, string>((loan) => {return loan.GetAppData(0).aCRelationshipTitleOtherDesc.ToString();})},
new { FieldName = "sprod3rdpartyuwresultt", Setter = new Action<string, CPageData>((s, loan) => { loan.sProd3rdPartyUwResultT = (E_sProd3rdPartyUwResultT)Enum.Parse(typeof(E_sProd3rdPartyUwResultT), s);  }), Getter = new Func<CPageData, string>((loan) => {return loan.sProd3rdPartyUwResultT.ToString("D");})},

                new { FieldName = "GetPreparerOfForm[App1003Interviewer].IsLocked",
                      Setter = new Action<string, CPageData>((s, loan) =>
                      {
                          var preparer = loan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.CreateNew);
                          preparer.IsLocked = ToBool(s);
                          preparer.Update();
                      }),
                    Getter = new Func<CPageData, string>((loan) =>
                    {
                        return loan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.CreateNew).IsLocked.ToString();
                    })},
                new { FieldName = "GetPreparerOfForm[App1003Interviewer].AgentRoleT",
                      Setter = new Action<string, CPageData>((s, loan) =>
                      {
                          var preparer = loan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.CreateNew);
                          preparer.IsLocked = true;
                          preparer.AgentRoleT = (E_AgentRoleT)Enum.Parse(typeof(E_AgentRoleT), s);
                          preparer.Update();
                      }),
                    Getter = new Func<CPageData, string>((loan) =>
                    {
                        return loan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.CreateNew).AgentRoleT.ToString("D");
                    })},
                new { FieldName = "GetPreparerOfForm[App1003Interviewer].PreparerName",
                      Setter = new Action<string, CPageData>((s, loan) =>
                      {
                          var preparer = loan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.CreateNew);
                          preparer.IsLocked = true;
                          preparer.PreparerName = s;
                          preparer.Update();
                      }),
                    Getter = new Func<CPageData, string>((loan) =>
                    {
                        return loan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.CreateNew).PreparerName;
                    })},
                new { FieldName = "GetPreparerOfForm[App1003Interviewer].Title",
                      Setter = new Action<string, CPageData>((s, loan) =>
                      {
                          var preparer = loan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.CreateNew);
                          preparer.IsLocked = true;
                          preparer.Title = s;
                          preparer.Update();
                      }),
                    Getter = new Func<CPageData, string>((loan) =>
                    {
                        return loan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.CreateNew).Title;
                    })},
                new { FieldName = "GetAgentOfRole[Servicing].AgentName",
                      Setter = new Action<string, CPageData>((s, loan) =>
                      {
                          var agent = loan.GetAgentOfRole(E_AgentRoleT.Servicing, E_ReturnOptionIfNotExist.CreateNew);
                          agent.AgentName = s;
                          agent.Update();
                      }),
                    Getter = new Func<CPageData, string>((loan) =>
                    {
                        return loan.GetAgentOfRole(E_AgentRoleT.Servicing, E_ReturnOptionIfNotExist.CreateNew).AgentName;
                    })},
            };
            //string data = "";
           //Regex r = new Regex("^                    case \"([^ \"]+)\":\\s + dataLoan[.](\\S +)\\s *=\\s * ([^;]+); break; (?=\\r ?$)", RegexOptions. );

           // var x = r.Matches("asd")[0];


            this.principal = LoginTools.LoginAs(TestAccountType.LoanOfficer);
            List<Tuple<string, string, string, string>> checks = new List<Tuple<string, string, string, string>>();
            CLoanFileCreator creator = CLoanFileCreator.GetCreator(principal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);

            var loanId = creator.CreateBlankLoanFile();

            CPageData dataLoanExplicit = new CFullAccessPageData(loanId, test.Select(p => p.FieldName));
            dataLoanExplicit.InitLoad();
            dataLoanExplicit.ByPassFieldSecurityCheck = true;
            dataLoanExplicit.SetFormatTarget(FormatTarget.ReportResult);
            
            CPageData dataLoanImplicit = new CFullAccessPageData(loanId, test.Select(p => p.FieldName));
            dataLoanImplicit.InitLoad();
            dataLoanImplicit.ByPassFieldSecurityCheck = true;
            dataLoanImplicit.SetFormatTarget(FormatTarget.ReportResult);
            var formatter = dataLoanImplicit.m_convertLos;

            foreach (var entry in test)
            {
                E_PageDataFieldType fieldType;
                if (!PageDataUtilities.GetFieldType(entry.FieldName, out fieldType))
                {
                    Assert.Fail("Field not found " + entry.FieldName);
                }

                if (PageDataUtilities.ContainsEnumField(entry.FieldName))
                {
                    fieldType = E_PageDataFieldType.Enum;
                }

                string originalFieldValueImplicit = PageDataUtilities.GetValue(dataLoanImplicit, dataLoanImplicit.GetAppData(0), entry.FieldName);
                string originalFieldValueExplicit = entry.Getter(dataLoanExplicit);

                if (PageDataUtilities.ContainsBooleanField(entry.FieldName))
                {
                    fieldType = E_PageDataFieldType.Bool;
                    originalFieldValueExplicit = ToYesNo(bool.Parse(originalFieldValueExplicit));
                }

                Assert.That(originalFieldValueExplicit, Is.EqualTo(originalFieldValueImplicit), "Values do not match between same loans. originalFieldValueExplicit: {0} originalFieldValueImplicit :{1} FieldId {2}", originalFieldValueExplicit, originalFieldValueImplicit, entry.FieldName);

                string newFieldValueSet = string.Empty;
                switch (fieldType)
                {
                    case E_PageDataFieldType.Unknown:
                    case E_PageDataFieldType.String:
                        if (entry.FieldName == "sfhasponsoredoriginatorein")
                        {
                            newFieldValueSet = "12-3456789";
                        }
                        else
                        {
                            newFieldValueSet = entry.FieldName;
                        }
                        break;
                    case E_PageDataFieldType.Money:
                        newFieldValueSet = "10000";
                        break;
                    case E_PageDataFieldType.Percent:
                        newFieldValueSet = "1.100%";
                        break;

                    case E_PageDataFieldType.Ssn:
                        newFieldValueSet = "111-11-1123";
                        break;
                    case E_PageDataFieldType.Phone:
                        newFieldValueSet = "213-322-3333";
                        break;
                    case E_PageDataFieldType.Integer:
                        newFieldValueSet = "2";
                        if (entry.FieldName == "sdue")
                        {
                            newFieldValueSet = "1";
                        }
                        break;
                    case E_PageDataFieldType.Bool:
                        if (originalFieldValueImplicit.Equals("Yes", StringComparison.OrdinalIgnoreCase))
                        {
                            newFieldValueSet = "No";
                        }
                        else
                        {
                            newFieldValueSet = "Yes";
                        }
                        break;
                    case E_PageDataFieldType.Enum:
                        newFieldValueSet = "1";
                        break;
                    case E_PageDataFieldType.DateTime:
                        newFieldValueSet = "1/1/1970";
                        break;
                    case E_PageDataFieldType.Decimal:
                        newFieldValueSet = "100.10";
                        break;
                    case E_PageDataFieldType.Array:
                    case E_PageDataFieldType.Guid:
                    default:
                        Assert.Fail("Failed " + entry.FieldName);
                        break;
                }

                Assert.That(originalFieldValueImplicit, Is.Not.EqualTo(newFieldValueSet), "Field ID {0} Values match {1}", entry.FieldName, newFieldValueSet);

                PageDataUtilities.SetValue(dataLoanImplicit, dataLoanImplicit.GetAppData(0), entry.FieldName, newFieldValueSet);
                entry.Setter(newFieldValueSet, dataLoanExplicit);

                string afterNewFieldValueSetImplicit = PageDataUtilities.GetValue(dataLoanImplicit, dataLoanImplicit.GetAppData(0), entry.FieldName);
                string afterNewFieldValueSetExplicit = entry.Getter(dataLoanExplicit);

                if (PageDataUtilities.ContainsBooleanField(entry.FieldName))
                {
                    afterNewFieldValueSetExplicit = ToYesNo(bool.Parse(afterNewFieldValueSetExplicit));
                }
           
                if (!afterNewFieldValueSetImplicit.Equals(afterNewFieldValueSetExplicit))
                {
                    checks.Add(Tuple.Create(entry.FieldName, newFieldValueSet, afterNewFieldValueSetImplicit, afterNewFieldValueSetExplicit));
                }
            }

            foreach (var entry in checks)
            {
                Console.WriteLine("Field {0} newFieldValueSet: {1} afterNewFieldValueSetImplicit: {2} afterNewFieldValueSetExplicit: {3}", entry.Item1, entry.Item2, entry.Item3, entry.Item4);
            }

            Assert.That(checks, Is.Empty);
        }

        [Test]
        public void SavingAgent_IsOriginatorAffiliate_UpdatesAsExpected()
        {
            string loxml = @"<LOXmlFormat version=""1.0"">
  <loan>
    <collection id=""sAgentDataSet"">
      <record action=""0"">
        <field id=""AgentRoleT"">18</field>
        <field id=""AgentName"" />
        <field id=""CompanyName"">ABC Surveyors</field>
        <field id=""StreetAddr"">123 Not here</field>
        <field id=""City"">Novi</field>
        <field id=""State"">MI</field>
        <field id=""Zip"">48167</field>
        <field id=""Phone"">2485557788</field>
        <field id=""EmailAddr"" />
        <field id=""IsOriginatorAffiliate"">True</field>
      </record>
    </collection>
  </loan>
</LOXmlFormat>";

            var result = LendersOffice.Conversions.LOFormatImporter.Import(this.loanId, this.principal, loxml, bypassPermissions: true);

            Assert.AreEqual(LendersOffice.Conversions.LoFormatImporterResultStatus.Ok, result.ResultStatus, "LOFormatImporter returned the following Errors:\r\n" + result.ErrorMessages);

            CPageData loanAfterImport = CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.loanId, typeof(LOFormatImporterTest));
            loanAfterImport.InitLoad();
            var agent = loanAfterImport.GetAgentOfRole(E_AgentRoleT.Surveyor, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            Assert.IsTrue(agent.IsOriginatorAffiliate);
        }
    }
}
