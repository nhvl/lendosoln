﻿using LendersOffice.ObjLib.LoXml;
using LendOSolnTest.Common;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;

namespace LendOSolnTest.Import
{
    [TestFixture]
    public class LOXmlParserTest
    {
        [Test]
        public void ValidateNormalFile()
        {
            string file = Path.Combine(TestDataRootFolder.Location, "LOXml/LOXmlQuery_Blank.xml");
            string data = File.ReadAllText(file);
            LoXmlInputParser parser = new LoXmlInputParser(data);
            var doc = parser.GetValidatedDocument();

            Assert.That(doc.SelectNodes("//LOXmlFormat/loan/field").Count, Is.EqualTo(9));
            Assert.That(doc.SelectNodes("//LOXmlFormat/loan/applicant/field").Count, Is.EqualTo(18));
            Assert.That(doc.SelectNodes("//LOXmlFormat/loan/applicant/collection").Count, Is.EqualTo(1));
            Assert.That(doc.SelectNodes("//LOXmlFormat/loan/collection").Count, Is.EqualTo(1));
        }

        [Test]
        public void ValidateQPFile()
        {
            string file = Path.Combine(TestDataRootFolder.Location, "LOXml/LOXmlQuery_OrderNewCredit.xml");
            string data = File.ReadAllText(file);

            LoXmlInputParser parser = new QuickPricerLoXmlInputParser(
                data,
                new QuickPricerLoXmlInputParser.OriginatorCompensationSettings(true, E_sOriginatorCompensationPaymentSourceT.SourceNotSpecified));
            var doc = parser.GetValidatedDocument();

            Assert.That(parser.GetErrors().Count(), Is.EqualTo(1));
            Assert.That(doc.SelectNodes("//LOXmlFormat/loan/applicant/credit").Count, Is.EqualTo(0));
        }

        [Test]
        public void Qp2File_CompSourceToggleDisabled_ReturnsErrorWhenSettingCompSource()
        {
            string loXml =
                @"<?xml version=""1.0"" encoding=""utf-8"" ?>
                  <LOXmlFormat version=""1.0"">
                    <loan>
                      <field id=""sOriginatorCompensationPaymentSourceT"">1</field>
                    </loan>
                  </LOXmlFormat>";


            var parser = new QuickPricerLoXmlInputParser(
                loXml,
                new QuickPricerLoXmlInputParser.OriginatorCompensationSettings(false, E_sOriginatorCompensationPaymentSourceT.LenderPaid));
            var doc = parser.GetValidatedDocument();

            Assert.That(parser.GetErrors().Count(), Is.EqualTo(1));
        }

        [Test]
        public void Qp2File_CompSourceToggleEnabled_AllowsSettingCompSource()
        {
            string loXml =
                @"<?xml version=""1.0"" encoding=""utf-8"" ?>
                  <LOXmlFormat version=""1.0"">
                    <loan>
                      <field id=""sOriginatorCompensationPaymentSourceT"">1</field>
                    </loan>
                  </LOXmlFormat>";


            var parser = new QuickPricerLoXmlInputParser(
                loXml,
                new QuickPricerLoXmlInputParser.OriginatorCompensationSettings(true, E_sOriginatorCompensationPaymentSourceT.LenderPaid));
            var doc = parser.GetValidatedDocument();

            Assert.That(parser.GetErrors().Count(), Is.EqualTo(0));
        }

        [Test]
        public void Qp2File_CompSourceToggleDisabledBorrPaid_AllowsSettingBorrPaidFields()
        {
            string loXml =
                @"<?xml version=""1.0"" encoding=""utf-8"" ?>
                  <LOXmlFormat version=""1.0"">
                    <loan>
                      <field id=""sOriginatorCompensationBorrPaidPc"">1</field>
                      <field id=""sOriginatorCompensationBorrPaidBaseT"">4</field>
                      <field id=""sOriginatorCompensationBorrPaidMb"">5</field>
                    </loan>
                  </LOXmlFormat>";


            var parser = new QuickPricerLoXmlInputParser(
                loXml,
                new QuickPricerLoXmlInputParser.OriginatorCompensationSettings(false, E_sOriginatorCompensationPaymentSourceT.BorrowerPaid));
            var doc = parser.GetValidatedDocument();

            Assert.That(parser.GetErrors().Count(), Is.EqualTo(0));
        }

        [Test]
        public void Qp2File_CompSourceToggleDisabledLenderPaid_ForbidsSettingBorrPaidFields()
        {
            string loXml =
                @"<?xml version=""1.0"" encoding=""utf-8"" ?>
                  <LOXmlFormat version=""1.0"">
                    <loan>
                      <field id=""sOriginatorCompensationBorrPaidPc"">1</field>
                      <field id=""sOriginatorCompensationBorrPaidBaseT"">4</field>
                      <field id=""sOriginatorCompensationBorrPaidMb"">5</field>
                    </loan>
                  </LOXmlFormat>";


            var parser = new QuickPricerLoXmlInputParser(
                loXml,
                new QuickPricerLoXmlInputParser.OriginatorCompensationSettings(false, E_sOriginatorCompensationPaymentSourceT.LenderPaid)); 
            var doc = parser.GetValidatedDocument();

            Assert.That(parser.GetErrors().Count(), Is.EqualTo(3));
        }

        [Test]
        public void Qp2File_AssignMclInstantViewId_ResultsInOneError()
        {
            var loXml =
                @"<?xml version=""1.0"" encoding=""utf-8""?>
                    <LOXmlFormat version=""1.0"">
                      <loan>
                        <applicant id=""000000000"" mcl_instant_view_id=""blah"">
                        </applicant>
                      </loan>
                    </LOXmlFormat>";

            var parser = new QuickPricerLoXmlInputParser(
                loXml,
                new QuickPricerLoXmlInputParser.OriginatorCompensationSettings(false, E_sOriginatorCompensationPaymentSourceT.BorrowerPaid));

            var doc = parser.GetValidatedDocument();

            Assert.AreEqual(1, parser.GetErrors().Count());
        }
    }
}
