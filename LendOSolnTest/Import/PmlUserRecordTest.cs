﻿// <copyright file="PmlUserRecordTest.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: geoffreyf
//    Date:   10/10/2014 4:55:48 PM 
// </summary>
namespace LendOSolnTest.Import
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using LendersOffice.Admin;
    using LendersOffice.Security;
    using LendersOfficeApp.WebService;
    using NUnit.Framework;
    using LendOSolnTest.Common;
    using LendOSolnTest.Admin;
    using NameId = System.Tuple<string, System.Guid>;
    using System.Collections;
    using DataAccess;

    /// <summary>
    /// Test the PmlUserRecord class.
    /// </summary>
    [TestFixture]
    public class PmlUserRecordTest
    {
        #region Guids Galore
        private static readonly Guid TestManagerEmployeeID = new Guid("0E52BFC7-8E7F-4BB9-AF2B-D5988345101A");
        private static readonly Guid TestProcessorEmployeeID = new Guid("672D93F8-C8AE-4BA3-9924-6BBA5AD3C842");
        private static readonly Guid TestJuniorProcessorEmployeeID = new Guid("7BA57148-C710-4B1A-A5B1-3DF22408615C");
        private static readonly Guid TestLenderAcctExecEmployeeID = new Guid("FF5E0BF8-2ACF-4620-9079-D4AA99BC65FF");
        private static readonly Guid TestUnderwriterEmployeeID = new Guid("55F4F675-769F-4180-996A-486C0B77C24D");
        private static readonly Guid TestJuniorUnderwriterEmployeeID = new Guid("A53A17F9-436E-46A2-B905-81FDEF58A93A");
        private static readonly Guid TestLockDeskEmployeeID = new Guid("6D2DCADD-113C-4AF8-8128-70799CA90CB9");
        private static readonly Guid TestMiniCorrManagerEmployeeId = new Guid("B8484217-7C8F-41D9-A0DD-F2AD1887DDF6");
        private static readonly Guid TestMiniCorrProcessorEmployeeId = new Guid("7677BEB6-DCC6-4C74-8A3B-9590E1581F71");
        private static readonly Guid TestMiniCorrJuniorProcessorEmployeeId = new Guid("3BEEE7F7-CC5E-4F76-BCD2-48963C327DC9");
        private static readonly Guid TestMiniCorrLenderAccExecEmployeeId = new Guid("7F95173C-EDEA-4B55-993B-F1DFF686B814");
        private static readonly Guid TestMiniCorrExternalPostCloserEmployeeId = new Guid("8F89F919-F779-43C8-9A61-6729036907C5");
        private static readonly Guid TestMiniCorrUnderwriterEmployeeId = new Guid("878F9022-4E32-444B-B413-AAAB0C1DDDE6");
        private static readonly Guid TestMiniCorrJuniorUnderwriterEmployeeId = new Guid("0239BB5F-6BF9-4E58-9BA0-0F21F79D960A");
        private static readonly Guid TestMiniCorrCreditAuditorEmployeeId = new Guid("5D4772F2-7DF3-4B77-9CC2-C79648EAC488");
        private static readonly Guid TestMiniCorrLegalAuditorEmployeeId = new Guid("5F7D0632-B9C3-41C2-A113-8926D6AB3350");
        private static readonly Guid TestMiniCorrLockDeskEmployeeId = new Guid("FA9114F1-E317-45DB-813A-3D943FA79028");
        private static readonly Guid TestMiniCorrPurchaserEmployeeId = new Guid("C6BF101D-9645-42D6-8488-125B46C2B205");
        private static readonly Guid TestMiniCorrSecondaryEmployeeId = new Guid("0B497FA4-9A1A-4D60-85A7-7B64C2337515");
        private static readonly Guid TestCorrManagerEmployeeId = new Guid("CBFD710E-66ED-4B83-A2C1-E89653F28A78");
        private static readonly Guid TestCorrProcessorEmployeeId = new Guid("CCF93D5E-A8A0-4A13-A054-BEC07A8C91EA");
        private static readonly Guid TestCorrJuniorProcessorEmployeeId = new Guid("9F687BBC-5326-456D-8FE3-FD0FF3F66414");
        private static readonly Guid TestCorrLenderAccExecEmployeeId = new Guid("E8B4F128-033A-4002-BAC2-BEA7F77A78B4");
        private static readonly Guid TestCorrExternalPostCloserEmployeeId = new Guid("35B096BC-8BE2-4916-AE1D-755104307EB1");
        private static readonly Guid TestCorrUnderwriterEmployeeId = new Guid("FAC7C70A-4093-4FFC-ACE2-FAA9A181F7A0");
        private static readonly Guid TestCorrJuniorUnderwriterEmployeeId = new Guid("256C3074-1865-4585-AD25-77F1BA2894F8");
        private static readonly Guid TestCorrCreditAuditorEmployeeId = new Guid("DF64992A-AD6F-4A78-88D5-E44D0B098839");
        private static readonly Guid TestCorrLegalAuditorEmployeeId = new Guid("5E09AE48-3F2D-4AA3-A4C2-31964E72813C");
        private static readonly Guid TestCorrLockDeskEmployeeId = new Guid("5DB9D900-48F1-4B77-9AD7-8F43D9602CC4");
        private static readonly Guid TestCorrPurchaserEmployeeId = new Guid("FE48DCEF-9FBF-403C-A8C3-18D9F2AD146E");
        private static readonly Guid TestCorrSecondaryEmployeeId = new Guid("7237E540-CDAB-4340-B77F-C9105E5DC553");
        #endregion

        [SetUp]
        public void SetUp()
        {
            LoginTools.LoginAs(TestAccountType.LoanOfficer);
        }

        #region Broker Relationships
        #region Branch
        [Test]
        public void TestExistingUserBranch()
        {
            var pmlUserRecord = new PmlUserRecordFake();
            pmlUserRecord.BranchName = PmlUserRecordFake.Branch1Name;
            pmlUserRecord.Update("");

            var employee = pmlUserRecord.GetEmployeeDB();
            Assert.That(
                employee.BranchID == PmlUserRecordFake.Branch1Id,
                "Employee should have correct branch ID assigned.");
        }

        [Test]
        public void TestNewUserBranch()
        {
            var pmlUserRecord = new PmlUserRecordFake();
            pmlUserRecord.BranchName = PmlUserRecordFake.Branch1Name;
            pmlUserRecord.Create();

            var employee = pmlUserRecord.GetEmployeeDesc();
            Assert.That(
                employee.BranchId == PmlUserRecordFake.Branch1Id,
                "Employee should have correct branch ID assigned.");
        }

        [Test]
        public void TestExistingUserClearBranch()
        {
            var pmlUserRecord = new PmlUserRecordFake();
            pmlUserRecord.BranchName = "";
            pmlUserRecord.Update("");

            var employee = pmlUserRecord.GetEmployeeDB();
            Assert.That(
                employee.BranchID == PrincipalFactory.CurrentPrincipal.BranchId,
                "Employee should have correct branch ID assigned.");
        }

        [Test]
        public void TestNewUserClearBranch()
        {
            var pmlUserRecord = new PmlUserRecordFake();
            pmlUserRecord.BranchName = "";
            pmlUserRecord.Create();

            var employee = pmlUserRecord.GetEmployeeDesc();
            Assert.That(
                employee.BranchId == PrincipalFactory.CurrentPrincipal.BranchId,
                "Employee should have correct branch ID assigned.");
        }
        #endregion

        #region Price Group
        [Test]
        public void TestExistingUserPriceGroup()
        {
            var pmlUserRecord = new PmlUserRecordFake();
            pmlUserRecord.PriceGroup = PmlUserRecordFake.PriceGroup3Name;
            pmlUserRecord.Update("");

            var employee = pmlUserRecord.GetEmployeeDB();
            Assert.That(
                employee.LpePriceGroupID == PmlUserRecordFake.PriceGroup3Id,
                "Employee should have correct price group ID assigned.");
        }

        [Test]
        public void TestNewUserPriceGroup()
        {
            var pmlUserRecord = new PmlUserRecordFake();
            pmlUserRecord.PriceGroup = PmlUserRecordFake.PriceGroup2Name;
            pmlUserRecord.Create();

            var employee = pmlUserRecord.GetEmployeeDesc();
            Assert.That(
                employee.PriceGroup == PmlUserRecordFake.PriceGroup2Id,
                "Employee should have correct price group ID assigned.");
        }

        [Test]
        public void TestExistingUserClearPriceGroup()
        {
            var pmlUserRecord = new PmlUserRecordFake();
            pmlUserRecord.PriceGroup = "";
            pmlUserRecord.Update("");

            var employee = pmlUserRecord.GetEmployeeDB();
            Assert.That(
                employee.LpePriceGroupID == Guid.Empty,
                "Employee should have correct price group ID assigned.");
        }

        [Test]
        public void TestNewUserClearPriceGroup()
        {
            var pmlUserRecord = new PmlUserRecordFake();
            pmlUserRecord.PriceGroup = "";
            pmlUserRecord.Create();

            var employee = pmlUserRecord.GetEmployeeDesc();
            Assert.That(
                employee.PriceGroup == Guid.Empty,
                "Employee should have correct price group ID assigned.");
        }
        #endregion

        #region Employees
        [Test]
        public void TestExistingUserBrokerManagerRelationship()
        {
            var pmlUserRecord = new PmlUserRecordFake();

            pmlUserRecord.PopulateBrokerRelationshipsT = EmployeePopulationMethodT.IndividualUser.ToString("G");

            pmlUserRecord.Manager = BrokerLoanAssignmentTableStub.ManagerName;
            pmlUserRecord.Create();

            var employee = pmlUserRecord.GetEmployeeDesc();

            Assert.That(
                employee.ManagerId == BrokerLoanAssignmentTableStub.ManagerId,
                "The employee ID for the relationship should be what was assigned.");
        }

        [Test]
        public void TestNewUserBrokerManagerRelationship()
        {
            var pmlUserRecord = new PmlUserRecordFake();

            pmlUserRecord.PopulateBrokerRelationshipsT = EmployeePopulationMethodT.IndividualUser.ToString("G");

            pmlUserRecord.Manager = BrokerLoanAssignmentTableStub.ManagerName;
            pmlUserRecord.Create();

            var employee = pmlUserRecord.GetEmployeeDesc();

            Assert.That(
                employee.ManagerId == BrokerLoanAssignmentTableStub.ManagerId,
                "The employee ID for the relationship should be what was assigned.");
        }

        [Test]
        public void TestExistingUserBrokerProcessorRelationship()
        {
            var pmlUserRecord = new PmlUserRecordFake();

            pmlUserRecord.PopulateBrokerRelationshipsT = EmployeePopulationMethodT.IndividualUser.ToString("G");

            pmlUserRecord.Processor = BrokerLoanAssignmentTableStub.ProcessorName;
            pmlUserRecord.Create();

            var employee = pmlUserRecord.GetEmployeeDesc();

            Assert.That(
                employee.ProcessorId == BrokerLoanAssignmentTableStub.ProcessorId,
                "The employee ID for the relationship should be what was assigned.");
        }

        [Test]
        public void TestNewUserBrokerProcessorRelationship()
        {
            var pmlUserRecord = new PmlUserRecordFake();

            pmlUserRecord.PopulateBrokerRelationshipsT = EmployeePopulationMethodT.IndividualUser.ToString("G");

            pmlUserRecord.Processor = BrokerLoanAssignmentTableStub.ProcessorName;
            pmlUserRecord.Create();

            var employee = pmlUserRecord.GetEmployeeDesc();

            Assert.That(
                employee.ProcessorId == BrokerLoanAssignmentTableStub.ProcessorId,
                "The employee ID for the relationship should be what was assigned.");
        }

        [Test]
        public void TestExistingUserBrokerJuniorProcessorRelationship()
        {
            var pmlUserRecord = new PmlUserRecordFake();

            pmlUserRecord.PopulateBrokerRelationshipsT = EmployeePopulationMethodT.IndividualUser.ToString("G");

            pmlUserRecord.JuniorProcessor = BrokerLoanAssignmentTableStub.JuniorProcessorName;
            pmlUserRecord.Create();

            var employee = pmlUserRecord.GetEmployeeDesc();

            Assert.That(
                employee.JuniorProcessorId == BrokerLoanAssignmentTableStub.JuniorProcessorId,
                "The employee ID for the relationship should be what was assigned.");
        }

        [Test]
        public void TestNewUserBrokerJuniorProcessorRelationship()
        {
            var pmlUserRecord = new PmlUserRecordFake();

            pmlUserRecord.PopulateBrokerRelationshipsT = EmployeePopulationMethodT.IndividualUser.ToString("G");

            pmlUserRecord.JuniorProcessor = BrokerLoanAssignmentTableStub.JuniorProcessorName;
            pmlUserRecord.Create();

            var employee = pmlUserRecord.GetEmployeeDesc();

            Assert.That(
                employee.JuniorProcessorId == BrokerLoanAssignmentTableStub.JuniorProcessorId,
                "The employee ID for the relationship should be what was assigned.");
        }

        [Test]
        public void TestExistingUserBrokerAccountExecRelationship()
        {
            var pmlUserRecord = new PmlUserRecordFake();

            pmlUserRecord.PopulateBrokerRelationshipsT = EmployeePopulationMethodT.IndividualUser.ToString("G");

            pmlUserRecord.AccountExecutive = BrokerLoanAssignmentTableStub.LenderAccountExecutiveName;
            pmlUserRecord.Create();

            var employee = pmlUserRecord.GetEmployeeDesc();

            Assert.That(
                employee.LenderAccountExecId == BrokerLoanAssignmentTableStub.LenderAccountExecutiveId,
                "The employee ID for the relationship should be what was assigned.");
        }

        [Test]
        public void TestNewUserBrokerAccountExecRelationship()
        {
            var pmlUserRecord = new PmlUserRecordFake();

            pmlUserRecord.PopulateBrokerRelationshipsT = EmployeePopulationMethodT.IndividualUser.ToString("G");

            pmlUserRecord.AccountExecutive = BrokerLoanAssignmentTableStub.LenderAccountExecutiveName;
            pmlUserRecord.Create();

            var employee = pmlUserRecord.GetEmployeeDesc();

            Assert.That(
                employee.LenderAccountExecId == BrokerLoanAssignmentTableStub.LenderAccountExecutiveId,
                "The employee ID for the relationship should be what was assigned.");
        }

        [Test]
        public void TestExistingUserBrokerBrokerProcessorRelationship()
        {
            var pmlUserRecord = new PmlUserRecordFake();
            pmlUserRecord.BrokerProcessor = BrokerLoanAssignmentTableStub.Pml_BrokerProcessorName;
            pmlUserRecord.Create();

            var employee = pmlUserRecord.GetEmployeeDesc();

            Assert.That(
                employee.BrokerProcessorId == BrokerLoanAssignmentTableStub.Pml_BrokerProcessorId,
                "The employee ID for the relationship should be what was assigned.");
        }

        [Test]
        public void TestNewUserBrokerBrokerProcessorRelationship()
        {
            var pmlUserRecord = new PmlUserRecordFake();
            pmlUserRecord.BrokerProcessor = BrokerLoanAssignmentTableStub.Pml_BrokerProcessorName;
            pmlUserRecord.Create();

            var employee = pmlUserRecord.GetEmployeeDesc();

            Assert.That(
                employee.BrokerProcessorId == BrokerLoanAssignmentTableStub.Pml_BrokerProcessorId,
                "The employee ID for the relationship should be what was assigned.");
        }

        [Test]
        public void TestExistingUserBrokerUnderwriterRelationship()
        {
            var pmlUserRecord = new PmlUserRecordFake();

            pmlUserRecord.PopulateBrokerRelationshipsT = EmployeePopulationMethodT.IndividualUser.ToString("G");

            pmlUserRecord.Underwriter = BrokerLoanAssignmentTableStub.UnderwriterName;
            pmlUserRecord.Create();

            var employee = pmlUserRecord.GetEmployeeDesc();

            Assert.That(
                employee.UnderwriterId == BrokerLoanAssignmentTableStub.UnderwriterId,
                "The employee ID for the relationship should be what was assigned.");
        }

        [Test]
        public void TestNewUserBrokerUnderwriterRelationship()
        {
            var pmlUserRecord = new PmlUserRecordFake();

            pmlUserRecord.PopulateBrokerRelationshipsT = EmployeePopulationMethodT.IndividualUser.ToString("G");

            pmlUserRecord.Underwriter = BrokerLoanAssignmentTableStub.UnderwriterName;
            pmlUserRecord.Create();

            var employee = pmlUserRecord.GetEmployeeDesc();

            Assert.That(
                employee.UnderwriterId == BrokerLoanAssignmentTableStub.UnderwriterId,
                "The employee ID for the relationship should be what was assigned.");
        }

        [Test]
        public void TestExistingUserBrokerJuniorUnderwriterRelationship()
        {
            var pmlUserRecord = new PmlUserRecordFake();

            pmlUserRecord.PopulateBrokerRelationshipsT = EmployeePopulationMethodT.IndividualUser.ToString("G");

            pmlUserRecord.JuniorUnderwriter = BrokerLoanAssignmentTableStub.JuniorUnderwriterName;
            pmlUserRecord.Create();

            var employee = pmlUserRecord.GetEmployeeDesc();

            Assert.That(
                employee.JuniorUnderwriterId == BrokerLoanAssignmentTableStub.JuniorUnderwriterId,
                "The employee ID for the relationship should be what was assigned.");
        }

        [Test]
        public void TestNewUserBrokerJuniorUnderwriterRelationship()
        {
            var pmlUserRecord = new PmlUserRecordFake();

            pmlUserRecord.PopulateBrokerRelationshipsT = EmployeePopulationMethodT.IndividualUser.ToString("G");

            pmlUserRecord.JuniorUnderwriter = BrokerLoanAssignmentTableStub.JuniorUnderwriterName;
            pmlUserRecord.Create();

            var employee = pmlUserRecord.GetEmployeeDesc();

            Assert.That(
                employee.JuniorUnderwriterId == BrokerLoanAssignmentTableStub.JuniorUnderwriterId,
                "The employee ID for the relationship should be what was assigned.");
        }

        [Test]
        public void TestExistingUserBrokerLockDeskRelationship()
        {
            var pmlUserRecord = new PmlUserRecordFake();

            pmlUserRecord.PopulateBrokerRelationshipsT = EmployeePopulationMethodT.IndividualUser.ToString("G");

            pmlUserRecord.LockDesk = BrokerLoanAssignmentTableStub.LockDeskName;
            pmlUserRecord.Create();

            var employee = pmlUserRecord.GetEmployeeDesc();

            Assert.That(
                employee.LockDeskId == BrokerLoanAssignmentTableStub.LockDeskId,
                "The employee ID for the relationship should be what was assigned.");
        }

        [Test]
        public void TestNewUserBrokerLockDeskRelationship()
        {
            var pmlUserRecord = new PmlUserRecordFake();

            pmlUserRecord.PopulateBrokerRelationshipsT = EmployeePopulationMethodT.IndividualUser.ToString("G");

            pmlUserRecord.LockDesk = BrokerLoanAssignmentTableStub.LockDeskName;
            pmlUserRecord.Create();

            var employee = pmlUserRecord.GetEmployeeDesc();

            Assert.That(
                employee.LockDeskId == BrokerLoanAssignmentTableStub.LockDeskId,
                "The employee ID for the relationship should be what was assigned.");
        }
        #endregion
        #endregion

        #region Mini-Correspondent Relationships
        #region Branch
        [Test]
        public void TestExistingUserMiniCorrespondentBranch()
        {
            var pmlUserRecord = new PmlUserRecordFake();
            pmlUserRecord.MiniCorrespondentBranchName = PmlUserRecordFake.Branch4Name;
            pmlUserRecord.Update("");

            var employee = pmlUserRecord.GetEmployeeDB();
            Assert.That(
                employee.MiniCorrespondentBranchID == PmlUserRecordFake.Branch4Id,
                "Employee should have correct branch ID assigned.");
        }

        [Test]
        public void TestNewUserMiniCorrespondentBranch()
        {
            var pmlUserRecord = new PmlUserRecordFake();
            pmlUserRecord.MiniCorrespondentBranchName = PmlUserRecordFake.Branch4Name;
            pmlUserRecord.Create();

            var employee = pmlUserRecord.GetEmployeeDesc();
            Assert.That(
                employee.MiniCorrespondentBranchId == PmlUserRecordFake.Branch4Id,
                "Employee should have correct branch ID assigned.");
        }

        [Test]
        public void TestExistingUserClearMiniCorrespondentBranch()
        {
            var pmlUserRecord = new PmlUserRecordFake();
            pmlUserRecord.MiniCorrespondentBranchName = "";
            pmlUserRecord.Update("");

            var employee = pmlUserRecord.GetEmployeeDB();

            Assert.That(
                employee.UseOriginatingCompanyMiniCorrBranchId,
                "Should use OC mini-correspondent branch by default.");

            Assert.That(
                employee.MiniCorrespondentBranchID == MockEmployeeDB.OCMiniCorrespondentBranchID,
                "Employee should have correct branch ID assigned.");
        }

        [Test]
        public void TestNewUserClearMiniCorrespondentBranch()
        {
            var pmlUserRecord = new PmlUserRecordFake();
            pmlUserRecord.MiniCorrespondentBranchName = "";
            pmlUserRecord.Create();

            var employee = pmlUserRecord.GetEmployeeDesc();

            Assert.That(
                employee.UseOriginatingCompanyMiniCorrBranchId,
                "Should use OC mini-correspondent price group by default.");
        }
        #endregion

        #region PriceGroup
        [Test]
        public void TestExistingUserMiniCorrespondentPriceGroup()
        {
            var pmlUserRecord = new PmlUserRecordFake();
            pmlUserRecord.MiniCorrespondentPriceGroup = PmlUserRecordFake.PriceGroup2Name;
            pmlUserRecord.Update("");

            var employee = pmlUserRecord.GetEmployeeDB();
            Assert.That(
                employee.MiniCorrespondentPriceGroupID == PmlUserRecordFake.PriceGroup2Id,
                "Employee should have correct price group ID assigned.");
        }

        [Test]
        public void TestNewUserMiniCorrespondentPriceGroup()
        {
            var pmlUserRecord = new PmlUserRecordFake();
            pmlUserRecord.MiniCorrespondentPriceGroup = PmlUserRecordFake.PriceGroup2Name;
            pmlUserRecord.Create();

            var employee = pmlUserRecord.GetEmployeeDesc();
            Assert.That(
                employee.MiniCorrespondentPriceGroupId == PmlUserRecordFake.PriceGroup2Id,
                "Employee should have correct price group ID assigned.");
        }

        [Test]
        public void TestExistingUserClearMiniCorrespondentPriceGroup()
        {
            var pmlUserRecord = new PmlUserRecordFake();
            pmlUserRecord.MiniCorrespondentPriceGroup = "";
            pmlUserRecord.Update("");

            var employee = pmlUserRecord.GetEmployeeDB();

            Assert.That(
                employee.UseOriginatingCompanyMiniCorrPriceGroupId,
                "Should use OC mini-correspondent price group by default.");

            Assert.That(
                employee.MiniCorrespondentPriceGroupID == MockEmployeeDB.OCMiniCorrespondentPriceGroupID,
                "Employee should have mini-correspondent price group assigned from originating company.");
        }

        [Test]
        public void TestNewUserClearMiniCorrespondentPriceGroup()
        {
            var pmlUserRecord = new PmlUserRecordFake();
            pmlUserRecord.MiniCorrespondentPriceGroup = "";
            pmlUserRecord.Create();

            var employee = pmlUserRecord.GetEmployeeDesc();

            Assert.That(
                employee.UseOriginatingCompanyMiniCorrPriceGroupId,
                "Should use OC mini-correspondent price group by default.");
        }
        #endregion

        #region Employees
        [Test]
        public void TestExistingUserAllMiniCorrespondentEmployeeRelationships()
        {
            var pmlUserRecord = new PmlUserRecordFake();

            pmlUserRecord.PopulateMiniCorrRelationshipsT = EmployeePopulationMethodT.IndividualUser.ToString("G");

            pmlUserRecord.MiniCorrespondentManager = BrokerLoanAssignmentTableStub.ManagerName;
            pmlUserRecord.MiniCorrespondentProcessor = BrokerLoanAssignmentTableStub.ProcessorName;
            pmlUserRecord.MiniCorrespondentJuniorProcessor = BrokerLoanAssignmentTableStub.JuniorProcessorName;
            pmlUserRecord.MiniCorrespondentLenderAccExec = BrokerLoanAssignmentTableStub.LenderAccountExecutiveName;
            pmlUserRecord.MiniCorrespondentExternalPostCloser = BrokerLoanAssignmentTableStub.ExternalPostCloserName;
            pmlUserRecord.MiniCorrespondentUnderwriter = BrokerLoanAssignmentTableStub.UnderwriterName;
            pmlUserRecord.MiniCorrespondentJuniorUnderwriter = BrokerLoanAssignmentTableStub.JuniorUnderwriterName;
            pmlUserRecord.MiniCorrespondentCreditAuditor = BrokerLoanAssignmentTableStub.CreditAuditorName;
            pmlUserRecord.MiniCorrespondentLegalAuditor = BrokerLoanAssignmentTableStub.LegalAuditorName;
            pmlUserRecord.MiniCorrespondentLockDesk = BrokerLoanAssignmentTableStub.LockDeskName;
            pmlUserRecord.MiniCorrespondentPurchaser = BrokerLoanAssignmentTableStub.PurchaserName;
            pmlUserRecord.MiniCorrespondentSecondary = BrokerLoanAssignmentTableStub.SecondaryName;

            pmlUserRecord.Update("");

            var employee = pmlUserRecord.GetEmployeeDB();

            employee.PopulateMiniCorrRelationshipsT = EmployeePopulationMethodT.IndividualUser;

            Assert.That(
                employee.MiniCorrManagerEmployeeId == BrokerLoanAssignmentTableStub.ManagerId,
                "The employee ID for the Manager relationship should be what was assigned");

            Assert.That(
                employee.MiniCorrProcessorEmployeeId == BrokerLoanAssignmentTableStub.ProcessorId,
                "The employee ID for the Processor relationship should be what was assigned");

            Assert.That(
                employee.MiniCorrJuniorProcessorEmployeeId == BrokerLoanAssignmentTableStub.JuniorProcessorId,
                "The employee ID for the JuniorProcessor relationship should be what was assigned");

            Assert.That(
                employee.MiniCorrLenderAccExecEmployeeId == BrokerLoanAssignmentTableStub.LenderAccountExecutiveId,
                "The employee ID for the LenderAccountExecutive relationship should be what was assigned");

            Assert.That(
                employee.MiniCorrExternalPostCloserEmployeeId == BrokerLoanAssignmentTableStub.ExternalPostCloserId,
                "The employee ID for the ExternalPostCloser relationship should be what was assigned");

            Assert.That(
                employee.MiniCorrUnderwriterEmployeeId == BrokerLoanAssignmentTableStub.UnderwriterId,
                "The employee ID for the Underwriter relationship should be what was assigned");

            Assert.That(
                employee.MiniCorrJuniorUnderwriterEmployeeId == BrokerLoanAssignmentTableStub.JuniorUnderwriterId,
                "The employee ID for the JuniorUnderwriter relationship should be what was assigned");

            Assert.That(
                employee.MiniCorrCreditAuditorEmployeeId == BrokerLoanAssignmentTableStub.CreditAuditorId,
                "The employee ID for the CreditAuditor relationship should be what was assigned");

            Assert.That(
                employee.MiniCorrLegalAuditorEmployeeId == BrokerLoanAssignmentTableStub.LegalAuditorId,
                "The employee ID for the LegalAuditor relationship should be what was assigned");

            Assert.That(
                employee.MiniCorrLockDeskEmployeeId == BrokerLoanAssignmentTableStub.LockDeskId,
                "The employee ID for the LockDesk relationship should be what was assigned");

            Assert.That(
                employee.MiniCorrPurchaserEmployeeId == BrokerLoanAssignmentTableStub.PurchaserId,
                "The employee ID for the Purchaser relationship should be what was assigned");

            Assert.That(
                employee.MiniCorrSecondaryEmployeeId == BrokerLoanAssignmentTableStub.SecondaryId,
                "The employee ID for the Secondary relationship should be what was assigned");
        }

        [Test]
        public void TestNewUserAllMiniCorrespondentEmployeeRelationships()
        {
            var pmlUserRecord = new PmlUserRecordFake();

            pmlUserRecord.PopulateMiniCorrRelationshipsT = EmployeePopulationMethodT.IndividualUser.ToString("G");

            pmlUserRecord.MiniCorrespondentManager = BrokerLoanAssignmentTableStub.ManagerName;
            pmlUserRecord.MiniCorrespondentProcessor = BrokerLoanAssignmentTableStub.ProcessorName;
            pmlUserRecord.MiniCorrespondentJuniorProcessor = BrokerLoanAssignmentTableStub.JuniorProcessorName;
            pmlUserRecord.MiniCorrespondentLenderAccExec = BrokerLoanAssignmentTableStub.LenderAccountExecutiveName;
            pmlUserRecord.MiniCorrespondentExternalPostCloser = BrokerLoanAssignmentTableStub.ExternalPostCloserName;
            pmlUserRecord.MiniCorrespondentUnderwriter = BrokerLoanAssignmentTableStub.UnderwriterName;
            pmlUserRecord.MiniCorrespondentJuniorUnderwriter = BrokerLoanAssignmentTableStub.JuniorUnderwriterName;
            pmlUserRecord.MiniCorrespondentCreditAuditor = BrokerLoanAssignmentTableStub.CreditAuditorName;
            pmlUserRecord.MiniCorrespondentLegalAuditor = BrokerLoanAssignmentTableStub.LegalAuditorName;
            pmlUserRecord.MiniCorrespondentLockDesk = BrokerLoanAssignmentTableStub.LockDeskName;
            pmlUserRecord.MiniCorrespondentPurchaser = BrokerLoanAssignmentTableStub.PurchaserName;
            pmlUserRecord.MiniCorrespondentSecondary = BrokerLoanAssignmentTableStub.SecondaryName;

            pmlUserRecord.Create();

            var employee = pmlUserRecord.GetEmployeeDesc();

            Assert.That(
                employee.MiniCorrespondentManagerId == BrokerLoanAssignmentTableStub.ManagerId,
                "The employee ID for the Manager relationship should be what was assigned");

            Assert.That(
                employee.MiniCorrespondentProcessorId == BrokerLoanAssignmentTableStub.ProcessorId,
                "The employee ID for the Processor relationship should be what was assigned");

            Assert.That(
                employee.MiniCorrespondentJuniorProcessorId == BrokerLoanAssignmentTableStub.JuniorProcessorId,
                "The employee ID for the JuniorProcessor relationship should be what was assigned");

            Assert.That(
                employee.MiniCorrespondentLenderAccExecId == BrokerLoanAssignmentTableStub.LenderAccountExecutiveId,
                "The employee ID for the LenderAccountExecutive relationship should be what was assigned");

            Assert.That(
                employee.MiniCorrespondentExternalPostCloserId == BrokerLoanAssignmentTableStub.ExternalPostCloserId,
                "The employee ID for the ExternalPostCloser relationship should be what was assigned");

            Assert.That(
                employee.MiniCorrespondentUnderwriterId == BrokerLoanAssignmentTableStub.UnderwriterId,
                "The employee ID for the Underwriter relationship should be what was assigned");

            Assert.That(
                employee.MiniCorrespondentJuniorUnderwriterId == BrokerLoanAssignmentTableStub.JuniorUnderwriterId,
                "The employee ID for the JuniorUnderwriter relationship should be what was assigned");

            Assert.That(
                employee.MiniCorrespondentCreditAuditorId == BrokerLoanAssignmentTableStub.CreditAuditorId,
                "The employee ID for the CreditAuditor relationship should be what was assigned");

            Assert.That(
                employee.MiniCorrespondentLegalAuditorId == BrokerLoanAssignmentTableStub.LegalAuditorId,
                "The employee ID for the LegalAuditor relationship should be what was assigned");

            Assert.That(
                employee.MiniCorrespondentLockDeskId == BrokerLoanAssignmentTableStub.LockDeskId,
                "The employee ID for the LockDesk relationship should be what was assigned");

            Assert.That(
                employee.MiniCorrespondentPurchaserId == BrokerLoanAssignmentTableStub.PurchaserId,
                "The employee ID for the Purchaser relationship should be what was assigned");

            Assert.That(
                employee.MiniCorrespondentSecondaryId == BrokerLoanAssignmentTableStub.SecondaryId,
                "The employee ID for the Secondary relationship should be what was assigned");
        }
        #endregion
        #endregion

        #region Correspondent Relationships
        #region Branch
        [Test]
        public void TestExistingUserCorrespondentBranch()
        {
            var pmlUserRecord = new PmlUserRecordFake();
            pmlUserRecord.CorrespondentBranchName = PmlUserRecordFake.Branch5Name;
            pmlUserRecord.Update("");

            var employee = pmlUserRecord.GetEmployeeDB();
            Assert.That(
                employee.CorrespondentBranchID == PmlUserRecordFake.Branch5Id,
                "Employee should have correct branch ID assigned.");
        }

        [Test]
        public void TestNewUserCorrespondentBranch()
        {
            var pmlUserRecord = new PmlUserRecordFake();
            pmlUserRecord.CorrespondentBranchName = PmlUserRecordFake.Branch5Name;
            pmlUserRecord.Create();

            var employee = pmlUserRecord.GetEmployeeDesc();
            Assert.That(
                employee.CorrespondentBranchId == PmlUserRecordFake.Branch5Id,
                "Employee should have correct branch ID assigned.");
        }

        [Test]
        public void TestExistingUserClearCorrespondentBranch()
        {
            var pmlUserRecord = new PmlUserRecordFake();
            pmlUserRecord.CorrespondentBranchName = "";
            pmlUserRecord.Update("");

            var employee = pmlUserRecord.GetEmployeeDB();

            Assert.That(
                employee.UseOriginatingCompanyCorrBranchId,
                "Should use OC correspondent branch by default.");

            Assert.That(
                employee.CorrespondentBranchID == MockEmployeeDB.OCCorrespondentBranchID,
                "Employee should have correct branch ID assigned.");
        }

        [Test]
        public void TestNewUserClearCorrespondentBranch()
        {
            var pmlUserRecord = new PmlUserRecordFake();
            pmlUserRecord.CorrespondentBranchName = "";
            pmlUserRecord.Create();

            var employee = pmlUserRecord.GetEmployeeDesc();

            Assert.That(
                employee.UseOriginatingCompanyCorrBranchId,
                "Should use OC correspondent price group by default.");
        }
        #endregion

        #region Price Group
        [Test]
        public void TestExistingUserCorrespondentPriceGroup()
        {
            var pmlUserRecord = new PmlUserRecordFake();
            pmlUserRecord.CorrespondentPriceGroup = PmlUserRecordFake.PriceGroup1Name;
            pmlUserRecord.Update("");

            var employee = pmlUserRecord.GetEmployeeDB();
            Assert.That(
                employee.CorrespondentPriceGroupID == PmlUserRecordFake.PriceGroup1Id,
                "Employee should have correct price group ID assigned.");
        }

        [Test]
        public void TestNewUserCorrespondentPriceGroup()
        {
            var pmlUserRecord = new PmlUserRecordFake();
            pmlUserRecord.CorrespondentPriceGroup = PmlUserRecordFake.PriceGroup1Name;
            pmlUserRecord.Create();

            var employee = pmlUserRecord.GetEmployeeDesc();
            Assert.That(
                employee.CorrespondentPriceGroupId == PmlUserRecordFake.PriceGroup1Id,
                "Employee should have correct price group ID assigned.");
        }

        [Test]
        public void TestExistingUserClearCorrespondentPriceGroup()
        {
            var pmlUserRecord = new PmlUserRecordFake();
            pmlUserRecord.CorrespondentPriceGroup = "";
            pmlUserRecord.Update("");

            var employee = pmlUserRecord.GetEmployeeDB();

            Assert.That(
                employee.UseOriginatingCompanyCorrPriceGroupId,
                "Should use OC correspondent price group by default.");

            Assert.That(
                employee.CorrespondentPriceGroupID == MockEmployeeDB.OCCorrespondentPriceGroupID,
                "Employee should have correspondent price group assigned from originating company.");
        }

        [Test]
        public void TestNewUserClearCorrespondentPriceGroup()
        {
            var pmlUserRecord = new PmlUserRecordFake();
            pmlUserRecord.CorrespondentPriceGroup = "";
            pmlUserRecord.Create();

            var employee = pmlUserRecord.GetEmployeeDesc();

            Assert.That(
                employee.UseOriginatingCompanyCorrPriceGroupId,
                "Should use OC correspondent price group by default.");
        }
        #endregion

        #region Employees
        [Test]
        public void TestExistingUserAllCorrespondentEmployeeRelationships()
        {
            var pmlUserRecord = new PmlUserRecordFake();

            pmlUserRecord.PopulateCorrRelationshipsT = EmployeePopulationMethodT.IndividualUser.ToString("G");

            pmlUserRecord.CorrespondentManager = BrokerLoanAssignmentTableStub.ManagerName;
            pmlUserRecord.CorrespondentProcessor = BrokerLoanAssignmentTableStub.ProcessorName;
            pmlUserRecord.CorrespondentJuniorProcessor = BrokerLoanAssignmentTableStub.JuniorProcessorName;
            pmlUserRecord.CorrespondentLenderAccExec = BrokerLoanAssignmentTableStub.LenderAccountExecutiveName;
            pmlUserRecord.CorrespondentExternalPostCloser = BrokerLoanAssignmentTableStub.ExternalPostCloserName;
            pmlUserRecord.CorrespondentUnderwriter = BrokerLoanAssignmentTableStub.UnderwriterName;
            pmlUserRecord.CorrespondentJuniorUnderwriter = BrokerLoanAssignmentTableStub.JuniorUnderwriterName;
            pmlUserRecord.CorrespondentCreditAuditor = BrokerLoanAssignmentTableStub.CreditAuditorName;
            pmlUserRecord.CorrespondentLegalAuditor = BrokerLoanAssignmentTableStub.LegalAuditorName;
            pmlUserRecord.CorrespondentLockDesk = BrokerLoanAssignmentTableStub.LockDeskName;
            pmlUserRecord.CorrespondentPurchaser = BrokerLoanAssignmentTableStub.PurchaserName;
            pmlUserRecord.CorrespondentSecondary = BrokerLoanAssignmentTableStub.SecondaryName;

            pmlUserRecord.Update("");

            var employee = pmlUserRecord.GetEmployeeDB();
            
            employee.PopulateCorrRelationshipsT = EmployeePopulationMethodT.IndividualUser;

            Assert.That(
                employee.CorrManagerEmployeeId == BrokerLoanAssignmentTableStub.ManagerId,
                "The employee ID for the Manager relationship should be what was assigned");

            Assert.That(
                employee.CorrProcessorEmployeeId == BrokerLoanAssignmentTableStub.ProcessorId,
                "The employee ID for the Processor relationship should be what was assigned");

            Assert.That(
                employee.CorrJuniorProcessorEmployeeId == BrokerLoanAssignmentTableStub.JuniorProcessorId,
                "The employee ID for the JuniorProcessor relationship should be what was assigned");

            Assert.That(
                employee.CorrLenderAccExecEmployeeId == BrokerLoanAssignmentTableStub.LenderAccountExecutiveId,
                "The employee ID for the LenderAccountExecutive relationship should be what was assigned");

            Assert.That(
                employee.CorrExternalPostCloserEmployeeId == BrokerLoanAssignmentTableStub.ExternalPostCloserId,
                "The employee ID for the ExternalPostCloser relationship should be what was assigned");

            Assert.That(
                employee.CorrUnderwriterEmployeeId == BrokerLoanAssignmentTableStub.UnderwriterId,
                "The employee ID for the Underwriter relationship should be what was assigned");

            Assert.That(
                employee.CorrJuniorUnderwriterEmployeeId == BrokerLoanAssignmentTableStub.JuniorUnderwriterId,
                "The employee ID for the JuniorUnderwriter relationship should be what was assigned");

            Assert.That(
                employee.CorrCreditAuditorEmployeeId == BrokerLoanAssignmentTableStub.CreditAuditorId,
                "The employee ID for the CreditAuditor relationship should be what was assigned");

            Assert.That(
                employee.CorrLegalAuditorEmployeeId == BrokerLoanAssignmentTableStub.LegalAuditorId,
                "The employee ID for the LegalAuditor relationship should be what was assigned");

            Assert.That(
                employee.CorrLockDeskEmployeeId == BrokerLoanAssignmentTableStub.LockDeskId,
                "The employee ID for the LockDesk relationship should be what was assigned");

            Assert.That(
                employee.CorrPurchaserEmployeeId == BrokerLoanAssignmentTableStub.PurchaserId,
                "The employee ID for the Purchaser relationship should be what was assigned");

            Assert.That(
                employee.CorrSecondaryEmployeeId == BrokerLoanAssignmentTableStub.SecondaryId,
                "The employee ID for the Secondary relationship should be what was assigned");
        }

        [Test]
        public void TestNewUserAllCorrespondentEmployeeRelationships()
        {
            var pmlUserRecord = new PmlUserRecordFake();

            pmlUserRecord.PopulateCorrRelationshipsT = EmployeePopulationMethodT.IndividualUser.ToString("G");

            pmlUserRecord.CorrespondentManager = BrokerLoanAssignmentTableStub.ManagerName;
            pmlUserRecord.CorrespondentProcessor = BrokerLoanAssignmentTableStub.ProcessorName;
            pmlUserRecord.CorrespondentJuniorProcessor = BrokerLoanAssignmentTableStub.JuniorProcessorName;
            pmlUserRecord.CorrespondentLenderAccExec = BrokerLoanAssignmentTableStub.LenderAccountExecutiveName;
            pmlUserRecord.CorrespondentExternalPostCloser = BrokerLoanAssignmentTableStub.ExternalPostCloserName;
            pmlUserRecord.CorrespondentUnderwriter = BrokerLoanAssignmentTableStub.UnderwriterName;
            pmlUserRecord.CorrespondentJuniorUnderwriter = BrokerLoanAssignmentTableStub.JuniorUnderwriterName;
            pmlUserRecord.CorrespondentCreditAuditor = BrokerLoanAssignmentTableStub.CreditAuditorName;
            pmlUserRecord.CorrespondentLegalAuditor = BrokerLoanAssignmentTableStub.LegalAuditorName;
            pmlUserRecord.CorrespondentLockDesk = BrokerLoanAssignmentTableStub.LockDeskName;
            pmlUserRecord.CorrespondentPurchaser = BrokerLoanAssignmentTableStub.PurchaserName;
            pmlUserRecord.CorrespondentSecondary = BrokerLoanAssignmentTableStub.SecondaryName;

            pmlUserRecord.Create();

            var employee = pmlUserRecord.GetEmployeeDesc();

            Assert.That(
                employee.CorrespondentManagerId == BrokerLoanAssignmentTableStub.ManagerId,
                "The employee ID for the Manager relationship should be what was assigned");

            Assert.That(
                employee.CorrespondentProcessorId == BrokerLoanAssignmentTableStub.ProcessorId,
                "The employee ID for the Processor relationship should be what was assigned");

            Assert.That(
                employee.CorrespondentJuniorProcessorId == BrokerLoanAssignmentTableStub.JuniorProcessorId,
                "The employee ID for the JuniorProcessor relationship should be what was assigned");

            Assert.That(
                employee.CorrespondentLenderAccExecId == BrokerLoanAssignmentTableStub.LenderAccountExecutiveId,
                "The employee ID for the LenderAccountExecutive relationship should be what was assigned");

            Assert.That(
                employee.CorrespondentExternalPostCloserId == BrokerLoanAssignmentTableStub.ExternalPostCloserId,
                "The employee ID for the ExternalPostCloser relationship should be what was assigned");

            Assert.That(
                employee.CorrespondentUnderwriterId == BrokerLoanAssignmentTableStub.UnderwriterId,
                "The employee ID for the Underwriter relationship should be what was assigned");

            Assert.That(
                employee.CorrespondentJuniorUnderwriterId == BrokerLoanAssignmentTableStub.JuniorUnderwriterId,
                "The employee ID for the JuniorUnderwriter relationship should be what was assigned");

            Assert.That(
                employee.CorrespondentCreditAuditorId == BrokerLoanAssignmentTableStub.CreditAuditorId,
                "The employee ID for the CreditAuditor relationship should be what was assigned");

            Assert.That(
                employee.CorrespondentLegalAuditorId == BrokerLoanAssignmentTableStub.LegalAuditorId,
                "The employee ID for the LegalAuditor relationship should be what was assigned");

            Assert.That(
                employee.CorrespondentLockDeskId == BrokerLoanAssignmentTableStub.LockDeskId,
                "The employee ID for the LockDesk relationship should be what was assigned");

            Assert.That(
                employee.CorrespondentPurchaserId == BrokerLoanAssignmentTableStub.PurchaserId,
                "The employee ID for the Purchaser relationship should be what was assigned");

            Assert.That(
                employee.CorrespondentSecondaryId == BrokerLoanAssignmentTableStub.SecondaryId,
                "The employee ID for the Secondary relationship should be what was assigned");
        }
        #endregion
        #endregion

        [Test]
        public void TestClearEmployeeRelationships()
        {
            var employee = new MockEmployeeDB();

            employee.UserType = 'P';

            employee.PopulateBrokerRelationshipsT = EmployeePopulationMethodT.IndividualUser;
            employee.PopulateMiniCorrRelationshipsT = EmployeePopulationMethodT.IndividualUser;
            employee.PopulateCorrRelationshipsT = EmployeePopulationMethodT.IndividualUser;

            employee.ManagerEmployeeID = TestManagerEmployeeID;
            employee.ProcessorEmployeeID = TestProcessorEmployeeID;
            employee.JuniorProcessorEmployeeID = TestJuniorProcessorEmployeeID;
            employee.LenderAcctExecEmployeeID = TestLenderAcctExecEmployeeID;
            employee.UnderwriterEmployeeID = TestUnderwriterEmployeeID;
            employee.JuniorUnderwriterEmployeeID = TestJuniorUnderwriterEmployeeID;
            employee.LockDeskEmployeeID = TestLockDeskEmployeeID;
            employee.MiniCorrManagerEmployeeId = TestMiniCorrManagerEmployeeId;
            employee.MiniCorrProcessorEmployeeId = TestMiniCorrProcessorEmployeeId;
            employee.MiniCorrJuniorProcessorEmployeeId = TestMiniCorrJuniorProcessorEmployeeId;
            employee.MiniCorrLenderAccExecEmployeeId = TestMiniCorrLenderAccExecEmployeeId;
            employee.MiniCorrExternalPostCloserEmployeeId = TestMiniCorrExternalPostCloserEmployeeId;
            employee.MiniCorrUnderwriterEmployeeId = TestMiniCorrUnderwriterEmployeeId;
            employee.MiniCorrJuniorUnderwriterEmployeeId = TestMiniCorrJuniorUnderwriterEmployeeId;
            employee.MiniCorrCreditAuditorEmployeeId = TestMiniCorrCreditAuditorEmployeeId;
            employee.MiniCorrLegalAuditorEmployeeId = TestMiniCorrLegalAuditorEmployeeId;
            employee.MiniCorrLockDeskEmployeeId = TestMiniCorrLockDeskEmployeeId;
            employee.MiniCorrPurchaserEmployeeId = TestMiniCorrPurchaserEmployeeId;
            employee.MiniCorrSecondaryEmployeeId = TestMiniCorrSecondaryEmployeeId;
            employee.CorrManagerEmployeeId = TestCorrManagerEmployeeId;
            employee.CorrProcessorEmployeeId = TestCorrProcessorEmployeeId;
            employee.CorrJuniorProcessorEmployeeId = TestCorrJuniorProcessorEmployeeId;
            employee.CorrLenderAccExecEmployeeId = TestCorrLenderAccExecEmployeeId;
            employee.CorrExternalPostCloserEmployeeId = TestCorrExternalPostCloserEmployeeId;
            employee.CorrUnderwriterEmployeeId = TestCorrUnderwriterEmployeeId;
            employee.CorrJuniorUnderwriterEmployeeId = TestCorrJuniorUnderwriterEmployeeId;
            employee.CorrCreditAuditorEmployeeId = TestCorrCreditAuditorEmployeeId;
            employee.CorrLegalAuditorEmployeeId = TestCorrLegalAuditorEmployeeId;
            employee.CorrLockDeskEmployeeId = TestCorrLockDeskEmployeeId;
            employee.CorrPurchaserEmployeeId = TestCorrPurchaserEmployeeId;
            employee.CorrSecondaryEmployeeId = TestCorrSecondaryEmployeeId;

            var pmlUserRecord = new PmlUserRecordFake(employee);

            pmlUserRecord.PopulateBrokerRelationshipsT = EmployeePopulationMethodT.IndividualUser.ToString("G");
            pmlUserRecord.PopulateMiniCorrRelationshipsT = EmployeePopulationMethodT.IndividualUser.ToString("G");
            pmlUserRecord.PopulateCorrRelationshipsT = EmployeePopulationMethodT.IndividualUser.ToString("G");

            pmlUserRecord.Manager = "";
            pmlUserRecord.Processor = "";
            pmlUserRecord.JuniorProcessor = "";
            pmlUserRecord.AccountExecutive = "";
            pmlUserRecord.Underwriter = "";
            pmlUserRecord.JuniorUnderwriter = "";
            pmlUserRecord.LockDesk = "";

            pmlUserRecord.MiniCorrespondentManager = "";
            pmlUserRecord.MiniCorrespondentProcessor = "";
            pmlUserRecord.MiniCorrespondentJuniorProcessor = "";
            pmlUserRecord.MiniCorrespondentLenderAccExec = "";
            pmlUserRecord.MiniCorrespondentExternalPostCloser = "";
            pmlUserRecord.MiniCorrespondentUnderwriter = "";
            pmlUserRecord.MiniCorrespondentJuniorUnderwriter = "";
            pmlUserRecord.MiniCorrespondentCreditAuditor = "";
            pmlUserRecord.MiniCorrespondentLegalAuditor = "";
            pmlUserRecord.MiniCorrespondentLockDesk = "";
            pmlUserRecord.MiniCorrespondentPurchaser = "";
            pmlUserRecord.MiniCorrespondentSecondary = "";

            pmlUserRecord.CorrespondentManager = "";
            pmlUserRecord.CorrespondentProcessor = "";
            pmlUserRecord.CorrespondentJuniorProcessor = "";
            pmlUserRecord.CorrespondentLenderAccExec = "";
            pmlUserRecord.CorrespondentExternalPostCloser = "";
            pmlUserRecord.CorrespondentUnderwriter = "";
            pmlUserRecord.CorrespondentJuniorUnderwriter = "";
            pmlUserRecord.CorrespondentCreditAuditor = "";
            pmlUserRecord.CorrespondentLegalAuditor = "";
            pmlUserRecord.CorrespondentLockDesk = "";
            pmlUserRecord.CorrespondentPurchaser = "";
            pmlUserRecord.CorrespondentSecondary = "";

            pmlUserRecord.Update("");

            var updatedEmployee = pmlUserRecord.GetEmployeeDB();

            Assert.That(
                updatedEmployee.ManagerEmployeeID == Guid.Empty,
                "The Manager relationship should be cleared.");

            Assert.That(
                updatedEmployee.ProcessorEmployeeID == Guid.Empty,
                "The Processor relationship should be cleared.");

            Assert.That(
                updatedEmployee.JuniorProcessorEmployeeID == Guid.Empty,
                "The JuniorProcessor relationship should be cleared.");

            Assert.That(
                updatedEmployee.LenderAcctExecEmployeeID == Guid.Empty,
                "The LenderAcctExec relationship should be cleared.");

            Assert.That(
                updatedEmployee.UnderwriterEmployeeID == Guid.Empty,
                "The Underwriter relationship should be cleared.");

            Assert.That(
                updatedEmployee.JuniorUnderwriterEmployeeID == Guid.Empty,
                "The JuniorUnderwriter relationship should be cleared.");

            Assert.That(
                updatedEmployee.LockDeskEmployeeID == Guid.Empty,
                "The LockDesk relationship should be cleared.");

            Assert.That(
                updatedEmployee.MiniCorrManagerEmployeeId == Guid.Empty,
                "The MiniCorrManager relationship should be cleared.");

            Assert.That(
                updatedEmployee.MiniCorrProcessorEmployeeId == Guid.Empty,
                "The MiniCorrProcessor relationship should be cleared.");

            Assert.That(
                updatedEmployee.MiniCorrJuniorProcessorEmployeeId == Guid.Empty,
                "The MiniCorrJuniorProcessor relationship should be cleared.");

            Assert.That(
                updatedEmployee.MiniCorrLenderAccExecEmployeeId == Guid.Empty,
                "The MiniCorrLenderAccExec relationship should be cleared.");

            Assert.That(
                updatedEmployee.MiniCorrExternalPostCloserEmployeeId == Guid.Empty,
                "The MiniCorrCorrProcessor relationship should be cleared.");

            Assert.That(
                updatedEmployee.MiniCorrUnderwriterEmployeeId == Guid.Empty,
                "The MiniCorrUnderwriter relationship should be cleared.");

            Assert.That(
                updatedEmployee.MiniCorrJuniorUnderwriterEmployeeId == Guid.Empty,
                "The MiniCorrJuniorUnderwriter relationship should be cleared.");

            Assert.That(
                updatedEmployee.MiniCorrCreditAuditorEmployeeId == Guid.Empty,
                "The MiniCorrCreditAuditor relationship should be cleared.");

            Assert.That(
                updatedEmployee.MiniCorrLegalAuditorEmployeeId == Guid.Empty,
                "The MiniCorrLegalAuditor relationship should be cleared.");

            Assert.That(
                updatedEmployee.MiniCorrLockDeskEmployeeId == Guid.Empty,
                "The MiniCorrLockDesk relationship should be cleared.");

            Assert.That(
                updatedEmployee.MiniCorrPurchaserEmployeeId == Guid.Empty,
                "The MiniCorrPurchaser relationship should be cleared.");

            Assert.That(
                updatedEmployee.MiniCorrSecondaryEmployeeId == Guid.Empty,
                "The MiniCorrSecondary relationship should be cleared.");

            Assert.That(
                updatedEmployee.CorrManagerEmployeeId == Guid.Empty,
                "The CorrManager relationship should be cleared.");

            Assert.That(
                updatedEmployee.CorrProcessorEmployeeId == Guid.Empty,
                "The CorrProcessor relationship should be cleared.");

            Assert.That(
                updatedEmployee.CorrJuniorProcessorEmployeeId == Guid.Empty,
                "The CorrJuniorProcessor relationship should be cleared.");

            Assert.That(
                updatedEmployee.CorrLenderAccExecEmployeeId == Guid.Empty,
                "The CorrLenderAccExec relationship should be cleared.");

            Assert.That(
                updatedEmployee.CorrExternalPostCloserEmployeeId == Guid.Empty,
                "The CorrCorrProcessor relationship should be cleared.");

            Assert.That(
                updatedEmployee.CorrUnderwriterEmployeeId == Guid.Empty,
                "The CorrUnderwriter relationship should be cleared.");

            Assert.That(
                updatedEmployee.CorrJuniorUnderwriterEmployeeId == Guid.Empty,
                "The CorrJuniorUnderwriter relationship should be cleared.");

            Assert.That(
                updatedEmployee.CorrCreditAuditorEmployeeId == Guid.Empty,
                "The CorrCreditAuditor relationship should be cleared.");

            Assert.That(
                updatedEmployee.CorrLegalAuditorEmployeeId == Guid.Empty,
                "The CorrLegalAuditor relationship should be cleared.");

            Assert.That(
                updatedEmployee.CorrLockDeskEmployeeId == Guid.Empty,
                "The CorrLockDesk relationship should be cleared.");

            Assert.That(
                updatedEmployee.CorrPurchaserEmployeeId == Guid.Empty,
                "The CorrPurchaser relationship should be cleared.");

            Assert.That(
                updatedEmployee.CorrSecondaryEmployeeId == Guid.Empty,
                "The CorrSecondary relationship should be cleared.");
        }

        [Test]
        public void TestMaintainAllEmployeeRelationships()
        {
            var employee = new MockEmployeeDB();

            employee.UserType = 'P';

            employee.PopulateBrokerRelationshipsT = EmployeePopulationMethodT.IndividualUser;
            employee.PopulateMiniCorrRelationshipsT = EmployeePopulationMethodT.IndividualUser;
            employee.PopulateCorrRelationshipsT = EmployeePopulationMethodT.IndividualUser;

            employee.ManagerEmployeeID = TestManagerEmployeeID;
            employee.ProcessorEmployeeID = TestProcessorEmployeeID;
            employee.JuniorProcessorEmployeeID = TestJuniorProcessorEmployeeID;
            employee.LenderAcctExecEmployeeID = TestLenderAcctExecEmployeeID;
            employee.UnderwriterEmployeeID = TestUnderwriterEmployeeID;
            employee.JuniorUnderwriterEmployeeID = TestJuniorUnderwriterEmployeeID;
            employee.LockDeskEmployeeID = TestLockDeskEmployeeID;
            employee.MiniCorrManagerEmployeeId = TestMiniCorrManagerEmployeeId;
            employee.MiniCorrProcessorEmployeeId = TestMiniCorrProcessorEmployeeId;
            employee.MiniCorrJuniorProcessorEmployeeId = TestMiniCorrJuniorProcessorEmployeeId;
            employee.MiniCorrLenderAccExecEmployeeId = TestMiniCorrLenderAccExecEmployeeId;
            employee.MiniCorrExternalPostCloserEmployeeId = TestMiniCorrExternalPostCloserEmployeeId;
            employee.MiniCorrUnderwriterEmployeeId = TestMiniCorrUnderwriterEmployeeId;
            employee.MiniCorrJuniorUnderwriterEmployeeId = TestMiniCorrJuniorUnderwriterEmployeeId;
            employee.MiniCorrCreditAuditorEmployeeId = TestMiniCorrCreditAuditorEmployeeId;
            employee.MiniCorrLegalAuditorEmployeeId = TestMiniCorrLegalAuditorEmployeeId;
            employee.MiniCorrLockDeskEmployeeId = TestMiniCorrLockDeskEmployeeId;
            employee.MiniCorrPurchaserEmployeeId = TestMiniCorrPurchaserEmployeeId;
            employee.MiniCorrSecondaryEmployeeId = TestMiniCorrSecondaryEmployeeId;
            employee.CorrManagerEmployeeId = TestCorrManagerEmployeeId;
            employee.CorrProcessorEmployeeId = TestCorrProcessorEmployeeId;
            employee.CorrJuniorProcessorEmployeeId = TestCorrJuniorProcessorEmployeeId;
            employee.CorrLenderAccExecEmployeeId = TestCorrLenderAccExecEmployeeId;
            employee.CorrExternalPostCloserEmployeeId = TestCorrExternalPostCloserEmployeeId;
            employee.CorrUnderwriterEmployeeId = TestCorrUnderwriterEmployeeId;
            employee.CorrJuniorUnderwriterEmployeeId = TestCorrJuniorUnderwriterEmployeeId;
            employee.CorrCreditAuditorEmployeeId = TestCorrCreditAuditorEmployeeId;
            employee.CorrLegalAuditorEmployeeId = TestCorrLegalAuditorEmployeeId;
            employee.CorrLockDeskEmployeeId = TestCorrLockDeskEmployeeId;
            employee.CorrPurchaserEmployeeId = TestCorrPurchaserEmployeeId;
            employee.CorrSecondaryEmployeeId = TestCorrSecondaryEmployeeId;

            var pmlUserRecord = new PmlUserRecordFake(employee);

            pmlUserRecord.Update("");

            var updatedEmployee = pmlUserRecord.GetEmployeeDB();

            Assert.That(
                updatedEmployee.ManagerEmployeeID == TestManagerEmployeeID,
                "The Manager relationship should be cleared.");

            Assert.That(
                updatedEmployee.ProcessorEmployeeID == TestProcessorEmployeeID,
                "The Processor relationship should be cleared.");

            Assert.That(
                updatedEmployee.JuniorProcessorEmployeeID == TestJuniorProcessorEmployeeID,
                "The JuniorProcessor relationship should be cleared.");

            Assert.That(
                updatedEmployee.LenderAcctExecEmployeeID == TestLenderAcctExecEmployeeID,
                "The LenderAcctExec relationship should be cleared.");

            Assert.That(
                updatedEmployee.UnderwriterEmployeeID == TestUnderwriterEmployeeID,
                "The Underwriter relationship should be cleared.");

            Assert.That(
                updatedEmployee.JuniorUnderwriterEmployeeID == TestJuniorUnderwriterEmployeeID,
                "The JuniorUnderwriter relationship should be cleared.");

            Assert.That(
                updatedEmployee.LockDeskEmployeeID == TestLockDeskEmployeeID,
                "The LockDesk relationship should be cleared.");

            Assert.That(
                updatedEmployee.MiniCorrManagerEmployeeId == TestMiniCorrManagerEmployeeId,
                "The MiniCorrManager relationship should be cleared.");

            Assert.That(
                updatedEmployee.MiniCorrProcessorEmployeeId == TestMiniCorrProcessorEmployeeId,
                "The MiniCorrProcessor relationship should be cleared.");

            Assert.That(
                updatedEmployee.MiniCorrJuniorProcessorEmployeeId == TestMiniCorrJuniorProcessorEmployeeId,
                "The MiniCorrJuniorProcessor relationship should be cleared.");

            Assert.That(
                updatedEmployee.MiniCorrLenderAccExecEmployeeId == TestMiniCorrLenderAccExecEmployeeId,
                "The MiniCorrLenderAccExec relationship should be cleared.");

            Assert.That(
                updatedEmployee.MiniCorrExternalPostCloserEmployeeId == TestMiniCorrExternalPostCloserEmployeeId,
                "The MiniCorrCorrProcessor relationship should be cleared.");

            Assert.That(
                updatedEmployee.MiniCorrUnderwriterEmployeeId == TestMiniCorrUnderwriterEmployeeId,
                "The MiniCorrUnderwriter relationship should be cleared.");

            Assert.That(
                updatedEmployee.MiniCorrJuniorUnderwriterEmployeeId == TestMiniCorrJuniorUnderwriterEmployeeId,
                "The MiniCorrJuniorUnderwriter relationship should be cleared.");

            Assert.That(
                updatedEmployee.MiniCorrCreditAuditorEmployeeId == TestMiniCorrCreditAuditorEmployeeId,
                "The MiniCorrCreditAuditor relationship should be cleared.");

            Assert.That(
                updatedEmployee.MiniCorrLegalAuditorEmployeeId == TestMiniCorrLegalAuditorEmployeeId,
                "The MiniCorrLegalAuditor relationship should be cleared.");

            Assert.That(
                updatedEmployee.MiniCorrLockDeskEmployeeId == TestMiniCorrLockDeskEmployeeId,
                "The MiniCorrLockDesk relationship should be cleared.");

            Assert.That(
                updatedEmployee.MiniCorrPurchaserEmployeeId == TestMiniCorrPurchaserEmployeeId,
                "The MiniCorrPurchaser relationship should be cleared.");

            Assert.That(
                updatedEmployee.MiniCorrSecondaryEmployeeId == TestMiniCorrSecondaryEmployeeId,
                "The MiniCorrSecondary relationship should be cleared.");

            Assert.That(
                updatedEmployee.CorrManagerEmployeeId == TestCorrManagerEmployeeId,
                "The CorrManager relationship should be cleared.");

            Assert.That(
                updatedEmployee.CorrProcessorEmployeeId == TestCorrProcessorEmployeeId,
                "The CorrProcessor relationship should be cleared.");

            Assert.That(
                updatedEmployee.CorrJuniorProcessorEmployeeId == TestCorrJuniorProcessorEmployeeId,
                "The CorrJuniorProcessor relationship should be cleared.");

            Assert.That(
                updatedEmployee.CorrLenderAccExecEmployeeId == TestCorrLenderAccExecEmployeeId,
                "The CorrLenderAccExec relationship should be cleared.");

            Assert.That(
                updatedEmployee.CorrExternalPostCloserEmployeeId == TestCorrExternalPostCloserEmployeeId,
                "The CorrCorrProcessor relationship should be cleared.");

            Assert.That(
                updatedEmployee.CorrUnderwriterEmployeeId == TestCorrUnderwriterEmployeeId,
                "The CorrUnderwriter relationship should be cleared.");

            Assert.That(
                updatedEmployee.CorrJuniorUnderwriterEmployeeId == TestCorrJuniorUnderwriterEmployeeId,
                "The CorrJuniorUnderwriter relationship should be cleared.");

            Assert.That(
                updatedEmployee.CorrCreditAuditorEmployeeId == TestCorrCreditAuditorEmployeeId,
                "The CorrCreditAuditor relationship should be cleared.");

            Assert.That(
                updatedEmployee.CorrLegalAuditorEmployeeId == TestCorrLegalAuditorEmployeeId,
                "The CorrLegalAuditor relationship should be cleared.");

            Assert.That(
                updatedEmployee.CorrLockDeskEmployeeId == TestCorrLockDeskEmployeeId,
                "The CorrLockDesk relationship should be cleared.");

            Assert.That(
                updatedEmployee.CorrPurchaserEmployeeId == TestCorrPurchaserEmployeeId,
                "The CorrPurchaser relationship should be cleared.");

            Assert.That(
                updatedEmployee.CorrSecondaryEmployeeId == TestCorrSecondaryEmployeeId,
                "The CorrSecondary relationship should be cleared.");
        }

        [Test]
        public void TestExistingUserLandingPages()
        {
            var pmlUserRecord = new PmlUserRecordFake();
            pmlUserRecord.TPOLandingPage = PmlUserRecordFake.LandingPage1Name;
            pmlUserRecord.MiniCorrespondentLandingPage = PmlUserRecordFake.LandingPage4Name;
            pmlUserRecord.CorrespondentLandingPage = PmlUserRecordFake.LandingPage2Name;

            pmlUserRecord.Update("");

            var employee = pmlUserRecord.GetEmployeeDB();

            Assert.That(
                employee.TPOLandingPageID == PmlUserRecordFake.LandingPage1Id,
                "The broker landing page should match what was assigned.");

            Assert.That(
                employee.MiniCorrespondentLandingPageID == PmlUserRecordFake.LandingPage4Id,
                "The mini-correspondent landing page should match what was assigned.");

            Assert.That(
                employee.CorrespondentLandingPageID == PmlUserRecordFake.LandingPage2Id,
                "The correspondent landing page should match what was assigned.");
        }

        [Test]
        public void TestNewUserLandingPages()
        {
            var pmlUserRecord = new PmlUserRecordFake();
            pmlUserRecord.TPOLandingPage = PmlUserRecordFake.LandingPage1Name;
            pmlUserRecord.MiniCorrespondentLandingPage = PmlUserRecordFake.LandingPage4Name;
            pmlUserRecord.CorrespondentLandingPage = PmlUserRecordFake.LandingPage2Name;

            pmlUserRecord.Create();

            var employee = pmlUserRecord.GetEmployeeDesc();

            Assert.That(
                employee.TPOLandingPageID == PmlUserRecordFake.LandingPage1Id,
                "The broker landing page should match what was assigned.");

            Assert.That(
                employee.MiniCorrespondentLandingPageID == PmlUserRecordFake.LandingPage4Id,
                "The mini-correspondent landing page should match what was assigned.");

            Assert.That(
                employee.CorrespondentLandingPageID == PmlUserRecordFake.LandingPage2Id,
                "The correspondent landing page should match what was assigned.");
        }

        [Test]
        public void TestAssignNewUserExternalSecondaryRole()
        {
            var pmlUserRecord = new PmlUserRecordFake();
            pmlUserRecord.IsExternalSecondary = "T";

            pmlUserRecord.Create();

            var employee = pmlUserRecord.GetEmployeeDesc();

            Assert.That(
                employee.RoleId.Equals(CEmployeeFields.s_ExternalSecondaryId.ToString(), StringComparison.InvariantCultureIgnoreCase),
                "The new user should only have the external secondary role.");
        }

        [Test]
        public void TestAssignExistingUserExternalSecondaryRole()
        {
            var pmlUserRecord = new PmlUserRecordFake();
            pmlUserRecord.IsExternalSecondary = "T";

            pmlUserRecord.Update("");

            var employee = pmlUserRecord.GetEmployeeDB();

            Assert.That(
                employee.GetRoles().Equals(CEmployeeFields.s_ExternalSecondaryId.ToString(), StringComparison.InvariantCultureIgnoreCase),
                "The new user should only have the external secondary role.");
        }

        [Test]
        public void TestAssignNewUserExternalPostCloserRole()
        {
            var pmlUserRecord = new PmlUserRecordFake();
            pmlUserRecord.IsExternalPostCloser = "T";

            pmlUserRecord.Create();

            var employee = pmlUserRecord.GetEmployeeDesc();

            Assert.That(
                employee.RoleId.Equals(CEmployeeFields.s_ExternalPostCloserId.ToString(), StringComparison.InvariantCultureIgnoreCase),
                "The new user should only have the external post-closer role.");
        }

        [Test]
        public void TestAssignExistingUserExternalPostCloserRole()
        {
            var pmlUserRecord = new PmlUserRecordFake();
            pmlUserRecord.IsExternalPostCloser = "T";

            pmlUserRecord.Update("");

            var employee = pmlUserRecord.GetEmployeeDB();

            Assert.That(
                employee.GetRoles().Equals(CEmployeeFields.s_ExternalPostCloserId.ToString(), StringComparison.InvariantCultureIgnoreCase),
                "The new user should only have the external post-closer role.");
        }

        [Test]
        public void TestAssignRelationshipsFromSupervisor()
        {
            var supervisor = new EmployeeDB();
            supervisor.MiniCorrManagerEmployeeId = TestMiniCorrManagerEmployeeId;
            supervisor.MiniCorrProcessorEmployeeId = TestMiniCorrProcessorEmployeeId;
            supervisor.MiniCorrJuniorProcessorEmployeeId = TestMiniCorrJuniorProcessorEmployeeId;
            supervisor.MiniCorrLenderAccExecEmployeeId = TestMiniCorrLenderAccExecEmployeeId;
            supervisor.MiniCorrExternalPostCloserEmployeeId = TestMiniCorrExternalPostCloserEmployeeId;
            supervisor.MiniCorrUnderwriterEmployeeId = TestMiniCorrUnderwriterEmployeeId;
            supervisor.MiniCorrJuniorUnderwriterEmployeeId = TestMiniCorrJuniorUnderwriterEmployeeId;
            supervisor.MiniCorrCreditAuditorEmployeeId = TestMiniCorrCreditAuditorEmployeeId;
            supervisor.MiniCorrLegalAuditorEmployeeId = TestMiniCorrLegalAuditorEmployeeId;
            supervisor.MiniCorrLockDeskEmployeeId = TestMiniCorrLockDeskEmployeeId;
            supervisor.MiniCorrPurchaserEmployeeId = TestMiniCorrPurchaserEmployeeId;
            supervisor.MiniCorrSecondaryEmployeeId = TestMiniCorrSecondaryEmployeeId;
            supervisor.CorrManagerEmployeeId = TestCorrManagerEmployeeId;
            supervisor.CorrProcessorEmployeeId = TestCorrProcessorEmployeeId;
            supervisor.CorrJuniorProcessorEmployeeId = TestCorrJuniorProcessorEmployeeId;
            supervisor.CorrLenderAccExecEmployeeId = TestCorrLenderAccExecEmployeeId;
            supervisor.CorrExternalPostCloserEmployeeId = TestCorrExternalPostCloserEmployeeId;
            supervisor.CorrUnderwriterEmployeeId = TestCorrUnderwriterEmployeeId;
            supervisor.CorrJuniorUnderwriterEmployeeId = TestCorrJuniorUnderwriterEmployeeId;
            supervisor.CorrCreditAuditorEmployeeId = TestCorrCreditAuditorEmployeeId;
            supervisor.CorrLegalAuditorEmployeeId = TestCorrLegalAuditorEmployeeId;
            supervisor.CorrLockDeskEmployeeId = TestCorrLockDeskEmployeeId;
            supervisor.CorrPurchaserEmployeeId = TestCorrPurchaserEmployeeId;
            supervisor.CorrSecondaryEmployeeId = TestCorrSecondaryEmployeeId;

            var pmlUserRecord = new PmlUserRecordFake();
            pmlUserRecord.CreateByPMLSupervisor(supervisor);

            var employeeDesc = pmlUserRecord.GetEmployeeDesc();

            Assert.That(employeeDesc.MiniCorrespondentManagerId == TestMiniCorrManagerEmployeeId);
            Assert.That(employeeDesc.MiniCorrespondentProcessorId == TestMiniCorrProcessorEmployeeId);
            Assert.That(employeeDesc.MiniCorrespondentJuniorProcessorId == TestMiniCorrJuniorProcessorEmployeeId);
            Assert.That(employeeDesc.MiniCorrespondentLenderAccExecId == TestMiniCorrLenderAccExecEmployeeId);
            Assert.That(employeeDesc.MiniCorrespondentExternalPostCloserId == Guid.Empty);
            Assert.That(employeeDesc.MiniCorrespondentUnderwriterId == TestMiniCorrUnderwriterEmployeeId);
            Assert.That(employeeDesc.MiniCorrespondentJuniorUnderwriterId == TestMiniCorrJuniorUnderwriterEmployeeId);
            Assert.That(employeeDesc.MiniCorrespondentCreditAuditorId == TestMiniCorrCreditAuditorEmployeeId);
            Assert.That(employeeDesc.MiniCorrespondentLegalAuditorId == TestMiniCorrLegalAuditorEmployeeId);
            Assert.That(employeeDesc.MiniCorrespondentLockDeskId == TestMiniCorrLockDeskEmployeeId);
            Assert.That(employeeDesc.MiniCorrespondentPurchaserId == TestMiniCorrPurchaserEmployeeId);
            Assert.That(employeeDesc.MiniCorrespondentSecondaryId == TestMiniCorrSecondaryEmployeeId);
            Assert.That(employeeDesc.CorrespondentManagerId == TestCorrManagerEmployeeId);
            Assert.That(employeeDesc.CorrespondentProcessorId == TestCorrProcessorEmployeeId);
            Assert.That(employeeDesc.CorrespondentJuniorProcessorId == TestCorrJuniorProcessorEmployeeId);
            Assert.That(employeeDesc.CorrespondentLenderAccExecId == TestCorrLenderAccExecEmployeeId);
            Assert.That(employeeDesc.CorrespondentExternalPostCloserId == Guid.Empty);
            Assert.That(employeeDesc.CorrespondentUnderwriterId == TestCorrUnderwriterEmployeeId);
            Assert.That(employeeDesc.CorrespondentJuniorUnderwriterId == TestCorrJuniorUnderwriterEmployeeId);
            Assert.That(employeeDesc.CorrespondentCreditAuditorId == TestCorrCreditAuditorEmployeeId);
            Assert.That(employeeDesc.CorrespondentLegalAuditorId == TestCorrLegalAuditorEmployeeId);
            Assert.That(employeeDesc.CorrespondentLockDeskId == TestCorrLockDeskEmployeeId);
            Assert.That(employeeDesc.CorrespondentPurchaserId == TestCorrPurchaserEmployeeId);
            Assert.That(employeeDesc.CorrespondentSecondaryId == TestCorrSecondaryEmployeeId);
        }
    }

    public class PmlUserRecordFake : PmlUserRecord
    {
        public const string Branch1Name = "Branch1";
        public const string Branch2Name = "Branch2";
        public const string Branch3Name = "Branch3";
        public const string Branch4Name = "Branch4";
        public const string Branch5Name = "Branch5";
        public static readonly Guid Branch1Id = new Guid("6A86E148-66B9-46A5-A815-392B383D11E0");
        public static readonly Guid Branch2Id = new Guid("3094FC38-D31C-4989-997E-A98FF30B824F");
        public static readonly Guid Branch3Id = new Guid("066526B0-46F9-4E6C-B7D7-BA71F634273C");
        public static readonly Guid Branch4Id = new Guid("A54A7B58-0D80-44A5-AD9E-9E2C3410B6C9");
        public static readonly Guid Branch5Id = new Guid("73DB7DC5-6C4D-47FD-8EFC-42468E43EBBE");

        public const string PriceGroup1Name = "PriceGroup1";
        public const string PriceGroup2Name = "PriceGroup2";
        public const string PriceGroup3Name = "PriceGroup3";
        public const string PriceGroup4Name = "PriceGroup4";
        public const string PriceGroup5Name = "PriceGroup5";
        public static readonly Guid PriceGroup1Id = new Guid("4569370D-3BF8-4B0B-BFFF-8C4CA2F4DA32");
        public static readonly Guid PriceGroup2Id = new Guid("4FB8D15C-11D4-4F43-9EE4-15B886F488E7");
        public static readonly Guid PriceGroup3Id = new Guid("EEC0EBA0-359B-404C-A646-048E4B112968");
        public static readonly Guid PriceGroup4Id = new Guid("94AD8511-5535-4A24-95C1-4FEDC0163D62");
        public static readonly Guid PriceGroup5Id = new Guid("FCCA82BF-66A4-4BA4-B862-B95FC72E88F9");

        public const string LandingPage1Name = "LandingPage1";
        public const string LandingPage2Name = "LandingPage2";
        public const string LandingPage3Name = "LandingPage3";
        public const string LandingPage4Name = "LandingPage4";
        public const string LandingPage5Name = "LandingPage5";
        public static readonly Guid LandingPage1Id = new Guid("4569370D-3BF8-4B0B-BFFF-8C4CA2F4DA32");
        public static readonly Guid LandingPage2Id = new Guid("4FB8D15C-11D4-4F43-9EE4-15B886F488E7");
        public static readonly Guid LandingPage3Id = new Guid("EEC0EBA0-359B-404C-A646-048E4B112968");
        public static readonly Guid LandingPage4Id = new Guid("94AD8511-5535-4A24-95C1-4FEDC0163D62");
        public static readonly Guid LandingPage5Id = new Guid("FCCA82BF-66A4-4BA4-B862-B95FC72E88F9");

        protected override Dictionary<string, Guid> branchIDByName
        {
            get
            {
                return new Dictionary<string, Guid>(StringComparer.OrdinalIgnoreCase)
                {
                    { Branch1Name, Branch1Id },
                    { Branch2Name, Branch2Id },
                    { Branch3Name, Branch3Id },
                    { Branch4Name, Branch4Id },
                    { Branch5Name, Branch5Id }
                };
            }
        }

        protected override Dictionary<string, Guid> priceGroupIDByName
        {
            get
            {
                return new Dictionary<string, Guid>(StringComparer.OrdinalIgnoreCase)
                {
                    { PriceGroup1Name, PriceGroup1Id },
                    { PriceGroup2Name, PriceGroup2Id },
                    { PriceGroup3Name, PriceGroup3Id },
                    { PriceGroup4Name, PriceGroup4Id },
                    { PriceGroup5Name, PriceGroup5Id }
                };
            }
        }

        protected override Dictionary<string, Guid> landingPages
        {
            get
            {
                return new Dictionary<string, Guid>(StringComparer.OrdinalIgnoreCase)
                {
                    { LandingPage1Name, LandingPage1Id },
                    { LandingPage2Name, LandingPage2Id },
                    { LandingPage3Name, LandingPage3Id },
                    { LandingPage4Name, LandingPage4Id },
                    { LandingPage5Name, LandingPage5Id }
                };
            }
        }

        private BrokerLoanAssignmentTableStub cachedBrokerLoanAssignmentTable = null;
        protected override BrokerLoanAssignmentTable brokerLoanAssignmentTable
        {
            get
            {
                if (this.cachedBrokerLoanAssignmentTable == null)
                {
                    this.cachedBrokerLoanAssignmentTable = new BrokerLoanAssignmentTableStub();
                }

                return this.cachedBrokerLoanAssignmentTable;
            }
        }

        public PmlUserRecordFake()
        {
        }

        public PmlUserRecordFake(MockEmployeeDB employee)
        {
            this.employeeEditing = employee;

            this.employeeEditing.UserType = 'P';
        }

        public override string Create()
        {
            this.IsCreating = true;
            this.employee = new EmployeeImportSet.Desc();

            var result = new ArrayList();

            result.AddRange(base.SetBranches());
            result.AddRange(base.SetPriceGroups());
            result.AddRange(base.SetRelationships());
            result.AddRange(base.SetLandingPages());
            result.Add(base.SetRoles());

            var list = new List<string>(result.Cast<string>());

            return string.Join(", ", list.Where(e => !string.IsNullOrEmpty(e)).ToArray());
        }

        public string CreateByPMLSupervisor(EmployeeDB supervisor)
        {
            this.IsCreating = true;
            this.employee = new EmployeeImportSet.Desc();

            var result = new ArrayList();

            result.AddRange(base.SetRelationshipsForPmlSupervisor());
            result.Add(base.SetRoles());

            this.SetMiniCorrespondentRelationships(employee, supervisor);

            this.SetCorrespondentRelationships(employee, supervisor);

            var list = new List<string>(result.Cast<string>());

            return string.Join(", ", list.Where(e => !string.IsNullOrEmpty(e)).ToArray());
        }

        public override string Update(string userName)
        {
            this.IsCreating = false;

            if (this.employeeEditing == null)
            {
                this.employeeEditing = new MockEmployeeDB();

                this.employeeEditing.UserType = 'P';
            }

            var result = new ArrayList();

            result.AddRange(base.SetBranches());
            result.AddRange(base.SetPriceGroups());
            result.AddRange(base.SetRelationships());
            result.AddRange(base.SetLandingPages());
            result.Add(base.SetRoles());

            var list = new List<string>(result.Cast<string>());

            return string.Join(", ", list.Where(e => !string.IsNullOrEmpty(e)).ToArray());
        }

        public override string UpdateByPMLSupervisor(string userName)
        {
            this.IsCreating = false;

            if (this.employeeEditing == null)
            {
                this.employeeEditing = new MockEmployeeDB();

                this.employeeEditing.UserType = 'P';
            }

            var result = new ArrayList();

            result.AddRange(base.SetRelationshipsForPmlSupervisor());
            result.Add(base.SetRoles());

            var list = new List<string>(result.Cast<string>());

            return string.Join(", ", list.Where(e => !string.IsNullOrEmpty(e)).ToArray());
        }

        public MockEmployeeDB GetEmployeeDB()
        {
            return (MockEmployeeDB)this.employeeEditing;
        }

        public EmployeeImportSet.Desc GetEmployeeDesc()
        {
            return this.employee;
        }
    }

    public class BrokerLoanAssignmentTableStub : BrokerLoanAssignmentTable
    {
        public const string ManagerName = "Manager";
        public const string ProcessorName = "Processor";
        public const string JuniorProcessorName = "JuniorProcessor";
        public const string LenderAccountExecutiveName = "LenderAccountExecutive";
        public const string Pml_BrokerProcessorName = "Pml_BrokerProcessor";
        public const string UnderwriterName = "Underwriter";
        public const string JuniorUnderwriterName = "JuniorUnderwriter";
        public const string LockDeskName = "LockDesk";
        public const string CreditAuditorName = "CreditAuditor";
        public const string LegalAuditorName = "LegalAuditor";
        public const string PurchaserName = "Purchaser";
        public const string SecondaryName = "Secondary";
        public const string ExternalPostCloserName = "ExternalPostCloser";

        public static readonly Guid ManagerId = new Guid("480E6728-972D-44AA-A2A9-97C982E389A3");
        public static readonly Guid ProcessorId = new Guid("55DAA5A9-B2E8-4142-81F2-1A7CF00E77C6");
        public static readonly Guid JuniorProcessorId = new Guid("99F5AE86-9CD0-45A7-B3F6-CA0FC519D4FB");
        public static readonly Guid LenderAccountExecutiveId = new Guid("431265D4-3EA7-4CC3-9781-B877807E6858");
        public static readonly Guid Pml_BrokerProcessorId = new Guid("8A5A705E-3591-4CA3-A503-A076E1D21379");
        public static readonly Guid UnderwriterId = new Guid("6883672F-73EE-4C90-99AE-6A3BAA1524DF");
        public static readonly Guid JuniorUnderwriterId = new Guid("5A80427E-DBE1-46AC-86EA-1A841771ED89");
        public static readonly Guid LockDeskId = new Guid("0E9626A1-B49C-4F93-972C-42303A4F1D70");
        public static readonly Guid CreditAuditorId = new Guid("1CE2191E-C018-4577-8B80-B8345D351B4A");
        public static readonly Guid LegalAuditorId = new Guid("F4A4AAE3-7031-4D30-BB93-FA8AF8B48D2A");
        public static readonly Guid PurchaserId = new Guid("866BAC2B-2EC2-4117-8919-A41618486BA3");
        public static readonly Guid SecondaryId = new Guid("6F2C80B3-2141-4E37-82D1-F9A17C02A9B1");
        public static readonly Guid ExternalPostCloserId = new Guid("19D13B49-F8E8-4E33-BE67-C429BCFC43A9");

        private List<Spec> genericEmployeeList = new List<Spec>()
        {
            new Spec() { EmployeeId = new Guid("11111111-1111-1111-1111-111111111111"), FirstName = "Employee", LastName = "Test1" },
            new Spec() { EmployeeId = new Guid("22222222-2222-2222-2222-222222222222"), FirstName = "Employee", LastName = "Test2" },
            new Spec() { EmployeeId = new Guid("33333333-3333-3333-3333-333333333333"), FirstName = "Employee", LastName = "Test3" },
            new Spec() { EmployeeId = new Guid("44444444-4444-4444-4444-444444444444"), FirstName = "Employee", LastName = "Test4" },
            new Spec() { EmployeeId = new Guid("55555555-5555-5555-5555-555555555555"), FirstName = "Employee", LastName = "Test5" },
            new Spec() { EmployeeId = new Guid("66666666-6666-6666-6666-666666666666"), FirstName = "Employee", LastName = "Test6" },
            new Spec() { EmployeeId = new Guid("77777777-7777-7777-7777-777777777777"), FirstName = "Employee", LastName = "Test7" },
            new Spec() { EmployeeId = new Guid("88888888-8888-8888-8888-888888888888"), FirstName = "Employee", LastName = "Test8" },
            new Spec() { EmployeeId = new Guid("99999999-9999-9999-9999-999999999999"), FirstName = "Employee", LastName = "Test9" },
            new Spec() { EmployeeId = new Guid("01010101-0101-0101-0101-010101010101"), FirstName = "Employee", LastName = "Test9" }
        };

        private Dictionary<E_RoleT, NameId> employeeNameByRole =
            new Dictionary<E_RoleT, NameId>()
        {
            { E_RoleT.Manager, new NameId(ManagerName, ManagerId) },
            { E_RoleT.Processor, new NameId(ProcessorName, ProcessorId) },
            { E_RoleT.JuniorProcessor, new NameId(JuniorProcessorName, JuniorProcessorId) },
            { E_RoleT.LenderAccountExecutive, new NameId(LenderAccountExecutiveName, LenderAccountExecutiveId) },
            { E_RoleT.Pml_BrokerProcessor, new NameId(Pml_BrokerProcessorName, Pml_BrokerProcessorId) },
            { E_RoleT.Underwriter, new NameId(UnderwriterName, UnderwriterId) },
            { E_RoleT.JuniorUnderwriter, new NameId(JuniorUnderwriterName, JuniorUnderwriterId) },
            { E_RoleT.LockDesk, new NameId(LockDeskName, LockDeskId) },
            { E_RoleT.CreditAuditor, new NameId(CreditAuditorName, CreditAuditorId) },
            { E_RoleT.LegalAuditor, new NameId(LegalAuditorName, LegalAuditorId) },
            { E_RoleT.Purchaser, new NameId(PurchaserName, PurchaserId) },
            { E_RoleT.Secondary, new NameId(SecondaryName, SecondaryId) },
            { E_RoleT.Pml_PostCloser, new NameId(ExternalPostCloserName, ExternalPostCloserId) }
        };

        private Dictionary<E_RoleT, IEnumerable<Spec>> availableEmployeesByRole =
            new Dictionary<E_RoleT, IEnumerable<Spec>>();

        public override void Retrieve(Guid brokerId, E_RoleT roleType)
        {
            if (!this.availableEmployeesByRole.ContainsKey(roleType))
            {
                var spec = new Spec() 
                { 
                    EmployeeId = this.employeeNameByRole[roleType].Item2,
                    FirstName = this.employeeNameByRole[roleType].Item1
                };

                this.availableEmployeesByRole.Add(
                    roleType,
                    this.genericEmployeeList.Concat(new Spec[] { spec }));
            }
        }

        public override IEnumerable<Spec> this[E_RoleT roleType]
        {
            get
            {
                if (this.availableEmployeesByRole.ContainsKey(roleType))
                {
                    return this.availableEmployeesByRole[roleType];
                }
                else
                {
                    return null;
                }
            }
        }
    }
}
