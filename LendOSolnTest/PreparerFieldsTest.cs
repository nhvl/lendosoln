/// Author: David Dao

using DataAccess;
using LendersOffice.Constants;
using LendOSolnTest.Common;
using LendOSolnTest.Fakes;
using NUnit.Framework;
namespace LendOSolnTest
{
    [TestFixture]
	public class PreparerFieldsTest
	{
        [TestFixtureSetUp]
        public void FixtureSetUp() 
        {
            LoginTools.LoginAs(TestAccountType.LoTest001);
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
            FakePageDataUtilities.Instance.Reset();
        }

        private CPageData CreateDataObject() 
        {
            return FakePageData.Create();
        }
        
        [Test]
        public void VerifyEmptyPreparerField()
        {
            CPreparerFields preparer = CPreparerFields.Empty;
            Assert.AreEqual(false, preparer.IsValid, "IsValid");
            Assert.AreEqual(true, preparer.IsLocked, "IsManualOverride");
            Assert.AreEqual(true, preparer.IsEmpty, "IsEmpty");
            Assert.AreEqual(string.Empty, preparer.CaseNum, "CaseNum");
            Assert.AreEqual(string.Empty, preparer.CellPhone, "CellPhone");
            Assert.AreEqual(string.Empty, preparer.City, "City");
            Assert.AreEqual(string.Empty, preparer.CityStateZip, "CityStateZip");
            Assert.AreEqual(string.Empty, preparer.CompanyName, "CompanyName");
            Assert.AreEqual(string.Empty, preparer.DepartmentName, "DepartmentName");
            Assert.AreEqual(string.Empty, preparer.EmailAddr, "EmailAddr");
            Assert.AreEqual(string.Empty, preparer.FaxNum, "FaxNum");
            Assert.AreEqual(string.Empty, preparer.FaxOfCompany, "FaxOfCompany");
            Assert.AreEqual(string.Empty, preparer.LicenseNumOfAgent, "LicenseNumOfAgent");
            Assert.AreEqual(string.Empty, preparer.LicenseNumOfCompany, "LicenseNumOfCompany");
            Assert.AreEqual(string.Empty, preparer.PagerNum, "PagerNum");
            Assert.AreEqual(string.Empty, preparer.Phone, "Phone");
            Assert.AreEqual(string.Empty, preparer.PhoneOfCompany, "PhoneOfCompany");
            Assert.AreEqual(string.Empty, preparer.PrepareDate_rep, "PrepareDate");
            Assert.AreEqual(string.Empty, preparer.PreparerName, "PreparerName");
            Assert.AreEqual(string.Empty, preparer.State, "State");
            Assert.AreEqual(string.Empty, preparer.StreetAddr, "StreetAddr");
            Assert.AreEqual(string.Empty, preparer.StreetAddrMultiLines, "StreetAddrMultilines");
            Assert.AreEqual(string.Empty, preparer.Title, "Title");
            Assert.AreEqual(string.Empty, preparer.Zip, "Zip");
            Assert.AreEqual(string.Empty, preparer.TaxId, "TaxId");
        }
        [Test]
        public void TestRetrieveEmptyPreparer()
        {
            E_PreparerFormT preparerFormT = E_PreparerFormT.CreditExperian; // Each test method should use unique preparer.

            CPageData dataLoan = CreateDataObject();
            dataLoan.InitLoad();

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(preparerFormT, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            Assert.AreEqual(true, preparer.IsEmpty, "IsEmpty");
            Assert.AreEqual(false, preparer.IsValid, "IsValid");
            Assert.AreEqual(false, preparer.IsValid, "IsValid");
            Assert.AreEqual(true, preparer.IsLocked, "IsManualOverride");
            Assert.AreEqual(true, preparer.IsEmpty, "IsEmpty");
            Assert.AreEqual(string.Empty, preparer.CaseNum, "CaseNum");
            Assert.AreEqual(string.Empty, preparer.CellPhone, "CellPhone");
            Assert.AreEqual(string.Empty, preparer.City, "City");
            Assert.AreEqual(string.Empty, preparer.CityStateZip, "CityStateZip");
            Assert.AreEqual(string.Empty, preparer.CompanyName, "CompanyName");
            Assert.AreEqual(string.Empty, preparer.DepartmentName, "DepartmentName");
            Assert.AreEqual(string.Empty, preparer.EmailAddr, "EmailAddr");
            Assert.AreEqual(string.Empty, preparer.FaxNum, "FaxNum");
            Assert.AreEqual(string.Empty, preparer.FaxOfCompany, "FaxOfCompany");
            Assert.AreEqual(string.Empty, preparer.LicenseNumOfAgent, "LicenseNumOfAgent");
            Assert.AreEqual(string.Empty, preparer.LicenseNumOfCompany, "LicenseNumOfCompany");
            Assert.AreEqual(string.Empty, preparer.PagerNum, "PagerNum");
            Assert.AreEqual(string.Empty, preparer.Phone, "Phone");
            Assert.AreEqual(string.Empty, preparer.PhoneOfCompany, "PhoneOfCompany");
            Assert.AreEqual(string.Empty, preparer.PrepareDate_rep, "PrepareDate");
            Assert.AreEqual(string.Empty, preparer.PreparerName, "PreparerName");
            Assert.AreEqual(string.Empty, preparer.State, "State");
            Assert.AreEqual(string.Empty, preparer.StreetAddr, "StreetAddr");
            Assert.AreEqual(string.Empty, preparer.StreetAddrMultiLines, "StreetAddrMultilines");
            Assert.AreEqual(string.Empty, preparer.Title, "Title");
            Assert.AreEqual(string.Empty, preparer.Zip, "Zip");

        }
        [Test]
        public void SimpleTest() 
        {
            E_PreparerFormT preparerFormT = E_PreparerFormT.CreditEquifax; // Each test method should use unique preparer.

            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(preparerFormT, E_ReturnOptionIfNotExist.CreateNew);

            preparer.CaseNum = "CaseNum";
            preparer.CellPhone = "714-111-1111";
            preparer.StreetAddr = null;
            preparer.City = "Anaheim";
            preparer.State = " CA ";
            preparer.Zip = " 12345 ";
            preparer.PrepareDate_rep = "1/1/2008";
            preparer.PreparerName = "Joe Smith";
            preparer.TaxId = "12-3456789";
            preparer.Update();

            dataLoan.Save();

            dataLoan = CreateDataObject();
            dataLoan.InitLoad();

            preparer = dataLoan.GetPreparerOfForm(preparerFormT, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            Assert.AreEqual(true, preparer.IsLocked, "IsManualOverride");
            Assert.AreEqual("CaseNum", preparer.CaseNum, "CaseNum");
            Assert.AreEqual("(714) 111-1111", preparer.CellPhone, "CellPhone");
            Assert.AreEqual("", preparer.StreetAddr, "StreetAddr");
            Assert.AreEqual("Anaheim", preparer.City, "City");
            Assert.AreEqual("CA", preparer.State, "State");
            Assert.AreEqual("12345", preparer.Zip, "Zip");
            Assert.AreEqual("1/1/2008", preparer.PrepareDate_rep, "PrepareDate");
            Assert.AreEqual("Joe Smith", preparer.PreparerName, "PreparerName");
            Assert.AreEqual("12-3456789", preparer.TaxId, "TaxId");
        }

        [Test]
        public void GettingInformationFromOfficialContactTest()
        {
            E_PreparerFormT preparerFormT = E_PreparerFormT.CreditDenialStatement; // Each test method should use unique preparer.
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            CAgentFields agent = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.CreateNew);
            agent.AgentName = "AgentName";
            agent.CompanyName = "AgentCompanyName";
            agent.LicenseNumOfAgent = "AgentLicenseNumber";
            agent.LicenseNumOfCompany = "AgentCompanyLicenseNumber";
            agent.CaseNum = "AgentCaseNum";
            agent.CellPhone = "(111) 111-1111";
            agent.City = "AgentCity";
            agent.DepartmentName = "AgentDepartmentName";
            agent.EmailAddr = "AgentEmailAddr";
            agent.FaxNum = "(222) 222-2222";
            agent.FaxOfCompany = "(333) 333-3333";
            agent.PagerNum = "(666) 666-6666";
            agent.Phone = "(444) 444-4444";
            agent.PhoneOfCompany = "(555) 555-5555";
            agent.State = "AgentState";
            agent.StreetAddr = "AgentStreetAddr";
            agent.Zip = "11111";
            agent.TaxId = "11-1111111";
            agent.Update();

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(preparerFormT, E_ReturnOptionIfNotExist.CreateNew);

            preparer.PreparerName = "PreparerName";
            preparer.CompanyName = "PreparerCompanyName";
            preparer.LicenseNumOfAgent = "PreparerLicenseNumOfAgent";
            preparer.LicenseNumOfCompany = "PreparerLicenseNumOfCompany";
            preparer.CaseNum = "PreparerCaseNum";
            preparer.CellPhone = "(111) 111-1234";
            preparer.City = "PreparerCity";
            preparer.DepartmentName = "PreparerDepartmentName";
            preparer.EmailAddr = "PreparerEmailAddr";
            preparer.FaxNum = "(222) 222-1234";
            preparer.FaxOfCompany = "(333) 333-1234";
            preparer.PagerNum = "(666) 666-1234";
            preparer.Phone = "(444) 444-1234";
            preparer.PhoneOfCompany = "(555) 555-5555";
            preparer.State = "PreparerState";
            preparer.StreetAddr = "PreparerStreetAddr";
            preparer.Title = "PreparerTitle";
            preparer.Zip = "22222";
            preparer.PrepareDate_rep = "1/1/2008";
            preparer.TaxId = "22-2222222";

            preparer.AgentRoleT = E_AgentRoleT.LoanOfficer;
            preparer.IsLocked = false;
            preparer.Update();

            dataLoan.Save();

            dataLoan = CreateDataObject();
            dataLoan.InitLoad();
            preparer = dataLoan.GetPreparerOfForm(preparerFormT, E_ReturnOptionIfNotExist.CreateNew);
            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.CreateNew);

            Assert.AreEqual("AgentName", agent.AgentName, "AgentName");
            Assert.AreEqual("AgentCompanyName", agent.CompanyName, "AgentCompanyName");
            
            Assert.AreEqual(preparerFormT, preparer.PreparerFormT, "PreparerFormT");

            Assert.AreEqual(agent.AgentName, preparer.PreparerName, "PreparerName");
            Assert.AreEqual(agent.CompanyName, preparer.CompanyName, "CompanyName");
            Assert.AreEqual(agent.LicenseNumOfAgent, preparer.LicenseNumOfAgent, "LicenseNumOfAgent");
            Assert.AreEqual(agent.LicenseNumOfCompany, preparer.LicenseNumOfCompany, "LicenseNumOfCompany");
            Assert.AreEqual(agent.CaseNum, preparer.CaseNum, "CaseNum");
            Assert.AreEqual(agent.CellPhone, preparer.CellPhone, "CellPhone");
            Assert.AreEqual(agent.City, preparer.City, "City");
            Assert.AreEqual(agent.CityStateZip, preparer.CityStateZip, "CityStateZip");
            Assert.AreEqual(agent.DepartmentName, preparer.DepartmentName, "DepartmentName");
            Assert.AreEqual(agent.EmailAddr, preparer.EmailAddr, "EmailAddr");
            Assert.AreEqual(agent.FaxNum, preparer.FaxNum, "FaxNum");
            Assert.AreEqual(agent.FaxOfCompany, preparer.FaxOfCompany, "FaxOfCompany");
            Assert.AreEqual(agent.PagerNum, preparer.PagerNum, "PagerNum");
            Assert.AreEqual(agent.Phone, preparer.Phone, "Phone");
            Assert.AreEqual(agent.PhoneOfCompany, preparer.PhoneOfCompany, "PHoneOfCompany");
            Assert.AreEqual(agent.State, preparer.State, "State");
            Assert.AreEqual(agent.StreetAddr, preparer.StreetAddr, "StreetAddr");
            Assert.AreEqual(agent.Zip, preparer.Zip, "Zip");
            Assert.AreEqual(agent.AgentRoleDescription, preparer.Title, "Title");
            Assert.AreEqual("1/1/2008", preparer.PrepareDate_rep, "PrepareDate");
            Assert.AreEqual(agent.TaxId, preparer.TaxId, "TaxId");

            preparer.IsLocked = true;

            // Verify that we retrieve data set in preparer.
            Assert.AreEqual("PreparerName", preparer.PreparerName, "PreparerName");
            Assert.AreEqual("PreparerCompanyName", preparer.CompanyName, "CompanyName");
            Assert.AreEqual("PreparerLicenseNumOfAgent", preparer.LicenseNumOfAgent, "LicenseNumOfAgent");
            Assert.AreEqual("PreparerLicenseNumOfCompany", preparer.LicenseNumOfCompany, "LicenseNumOfCompany");
            Assert.AreEqual("PreparerCaseNum", preparer.CaseNum, "CaseNum");
            Assert.AreEqual("(111) 111-1234", preparer.CellPhone, "CellPhone");
            Assert.AreEqual("PreparerCity", preparer.City, "City");
            Assert.AreEqual("PreparerDepartmentName", preparer.DepartmentName, "DepartmentName");
            Assert.AreEqual("PreparerEmailAddr", preparer.EmailAddr, "EmailAddr");
            Assert.AreEqual("(222) 222-1234", preparer.FaxNum, "FaxNum");
            Assert.AreEqual("(333) 333-1234", preparer.FaxOfCompany, "FaxOfCompany");
            Assert.AreEqual("(666) 666-1234", preparer.PagerNum, "PagerNum");
            Assert.AreEqual("(444) 444-1234", preparer.Phone, "Phone");
            Assert.AreEqual("(555) 555-5555", preparer.PhoneOfCompany, "PHoneOfCompany");
            Assert.AreEqual("PreparerState", preparer.State, "State");
            Assert.AreEqual("PreparerStreetAddr", preparer.StreetAddr, "StreetAddr");
            Assert.AreEqual("22222", preparer.Zip, "Zip");
            Assert.AreEqual("PreparerTitle", preparer.Title, "Title");
            Assert.AreEqual("22-2222222", preparer.TaxId, "TaxId");
        }

        private void SetUnderwriterAgentTestData(CPageData dataLoan)
        {
            var uw = dataLoan.GetAgentOfRole(E_AgentRoleT.Underwriter, E_ReturnOptionIfNotExist.CreateNew);
            uw.AgentName = "UW Agent Name";
            uw.ChumsId = "12345";
            uw.Update();
        }

        private void SetLenderAgentTestData(CPageData dataLoan)
        {
            var lender = dataLoan.GetAgentOfRole(E_AgentRoleT.Lender, E_ReturnOptionIfNotExist.CreateNew);
            lender.AgentName = "Lender Agent Name";
            lender.Update();
        }

        private void ClearOverrides(IPreparerFields preparer, bool callUpdate)
        {
            preparer.overrideNameLocked(false);
            preparer.overrideLicenseLocked(false);
            if (callUpdate)
            {
                preparer.Update();
            }
        }

        [Test]
        public void Test203kUnderwriterFormPreparer()
        {
            E_PreparerFormT preparerFormT = E_PreparerFormT.FHA203kWorksheetUnderwriter; // Each test method should use unique preparer.

            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            SetUnderwriterAgentTestData(dataLoan);
            SetLenderAgentTestData(dataLoan);

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(preparerFormT, E_ReturnOptionIfNotExist.CreateNew);
            ClearOverrides(preparer, false);

            Assert.AreEqual(E_AgentRoleT.Underwriter, preparer.AgentRoleT);
            Assert.AreEqual(false, preparer.NameLocked);
            Assert.AreEqual(false, preparer.ChumsIdLocked);
            Assert.AreEqual("UW Agent Name", preparer.PreparerName);
            Assert.AreEqual("12345", preparer.ChumsId);

            preparer.NameLocked = true;
            preparer.PreparerName = "203k Preparer Name";
            Assert.AreEqual("203k Preparer Name", preparer.PreparerName);
            Assert.AreEqual("12345", preparer.ChumsId);

            preparer.ChumsIdLocked = true;
            preparer.ChumsId = "67890";
            Assert.AreEqual("203k Preparer Name", preparer.PreparerName);
            Assert.AreEqual("67890", preparer.ChumsId);

            // Can't unlock...this needs to change at some point.
            preparer.NameLocked = false;
            preparer.ChumsIdLocked = false;
            Assert.AreEqual("203k Preparer Name", preparer.PreparerName);
            Assert.AreEqual("67890", preparer.ChumsId);

            preparer.Update();

            dataLoan.Save();

            dataLoan = CreateDataObject();
            dataLoan.InitLoad();

            preparer = dataLoan.GetPreparerOfForm(preparerFormT, E_ReturnOptionIfNotExist.ReturnEmptyObject);

            Assert.AreEqual(E_AgentRoleT.Underwriter, preparer.AgentRoleT);
            Assert.AreEqual("203k Preparer Name", preparer.PreparerName);
            Assert.AreEqual("67890", preparer.ChumsId);
        }

        [Test]
        public void TestFhaAppraisalAnalysisUnderwriterFormPreparer()
        {
            E_PreparerFormT preparerFormT = E_PreparerFormT.FHAAnalysisAppraisalUnderwriter; // Each test method should use unique preparer.

            CPageData dataLoan = CreateDataObject();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            SetUnderwriterAgentTestData(dataLoan);
            SetLenderAgentTestData(dataLoan);

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(preparerFormT, E_ReturnOptionIfNotExist.CreateNew);
            ClearOverrides(preparer, false);

            Assert.AreEqual(E_AgentRoleT.Underwriter, preparer.AgentRoleT);
            Assert.AreEqual(false, preparer.NameLocked);
            Assert.AreEqual(false, preparer.ChumsIdLocked);
            Assert.AreEqual("UW Agent Name", preparer.PreparerName);
            Assert.AreEqual("12345", preparer.ChumsId);

            preparer.ChumsIdLocked = true;
            preparer.ChumsId = "67890";
            Assert.AreEqual("UW Agent Name", preparer.PreparerName);
            Assert.AreEqual("67890", preparer.ChumsId);

            preparer.Update();

            dataLoan.Save();

            dataLoan = CreateDataObject();
            dataLoan.InitLoad();

            preparer = dataLoan.GetPreparerOfForm(preparerFormT, E_ReturnOptionIfNotExist.ReturnEmptyObject);

            Assert.AreEqual(E_AgentRoleT.Underwriter, preparer.AgentRoleT);
            Assert.AreEqual("UW Agent Name", preparer.PreparerName);
            Assert.AreEqual("67890", preparer.ChumsId);
        }

        [Test]
        public void TestFhaPreparerBasedLoanFileFields()
        {
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitLoad();

            SetUnderwriterAgentTestData(dataLoan);
            SetLenderAgentTestData(dataLoan);

            ClearOverrides(dataLoan.GetPreparerOfForm(E_PreparerFormT.FHAAddendumUnderwriter, E_ReturnOptionIfNotExist.CreateNew), true);
            ClearOverrides(dataLoan.GetPreparerOfForm(E_PreparerFormT.FHAAddendumMortgagee, E_ReturnOptionIfNotExist.CreateNew), true);

            dataLoan.sFHARatedReferByTotalScorecard = true;
            dataLoan.sFHARatedAcceptedByTotalScorecard = false;
            Assert.AreEqual("UW Agent Name", dataLoan.sFHAAddendumUnderwriterName);

            dataLoan.sFHARatedReferByTotalScorecard = false;
            dataLoan.sFHARatedAcceptedByTotalScorecard = true;
            Assert.AreEqual(string.Empty, dataLoan.sFHAAddendumUnderwriterName);

            dataLoan.sFHARatedReferByTotalScorecard = true;
            dataLoan.sFHARatedAcceptedByTotalScorecard = true;
            Assert.AreEqual(string.Empty, dataLoan.sFHAAddendumUnderwriterName);

            dataLoan.sFHAAddendumUnderwriterNameLckd = true;
            dataLoan.sFHAAddendumUnderwriterName = "LFF UW Name";
            Assert.AreEqual("LFF UW Name", dataLoan.sFHAAddendumUnderwriterName);

            // Can't unlock...this needs to change at some point.
            dataLoan.sFHAAddendumUnderwriterNameLckd = false;
            Assert.IsTrue(dataLoan.sFHAAddendumUnderwriterNameLckd);
            Assert.AreEqual("LFF UW Name", dataLoan.sFHAAddendumUnderwriterName);

            dataLoan.sFHARatedReferByTotalScorecard = true;
            dataLoan.sFHARatedAcceptedByTotalScorecard = false;
            Assert.AreEqual("12345", dataLoan.sFHAAddendumUnderwriterChumsId);

            dataLoan.sFHARatedReferByTotalScorecard = false;
            dataLoan.sFHARatedAcceptedByTotalScorecard = true;
            Assert.AreEqual(string.Empty, dataLoan.sFHAAddendumUnderwriterChumsId);

            dataLoan.sFHAAddendumUnderwriterChumsIdLckd = true;
            dataLoan.sFHAAddendumUnderwriterChumsId = "67890";
            Assert.AreEqual("67890", dataLoan.sFHAAddendumUnderwriterChumsId);

            // Can't unlock...this needs to change at some point.
            dataLoan.sFHAAddendumUnderwriterChumsIdLckd = false;
            Assert.IsTrue(dataLoan.sFHAAddendumUnderwriterChumsIdLckd);
            Assert.AreEqual("67890", dataLoan.sFHAAddendumUnderwriterChumsId);

            dataLoan.sFHARiskClassAA = true;
            dataLoan.sFHARiskClassRefer = false;
            Assert.AreEqual("ZFHA", dataLoan.sFHA92900LtUnderwriterChumsId);

            dataLoan.sFHARiskClassAA = false;
            dataLoan.sFHARiskClassRefer = false;
            Assert.AreEqual("12345", dataLoan.sFHA92900LtUnderwriterChumsId);

            dataLoan.sFHARiskClassAA = true;
            dataLoan.sFHARiskClassRefer = true;
            Assert.AreEqual("12345", dataLoan.sFHA92900LtUnderwriterChumsId);

            dataLoan.sFHA92900LtUnderwriterChumsIdLckd = true;
            dataLoan.sFHA92900LtUnderwriterChumsId = "67890";
            Assert.AreEqual("67890", dataLoan.sFHA92900LtUnderwriterChumsId);

            // Can't unlock...this needs to change at some point.
            dataLoan.sFHA92900LtUnderwriterChumsIdLckd = false;
            Assert.IsTrue(dataLoan.sFHA92900LtUnderwriterChumsIdLckd);
            Assert.AreEqual("67890", dataLoan.sFHA92900LtUnderwriterChumsId);

            dataLoan.sFHARatedReferByTotalScorecard = false;
            // We never actually associate any contact records with this preparer.
            Assert.AreEqual(string.Empty, dataLoan.sFHAAddendumMortgageeName);
            dataLoan.sFHAAddendumMortgageeNameLckd = true;
            dataLoan.sFHAAddendumMortgageeName = "LFF Mortgagee Name";
            Assert.AreEqual("LFF Mortgagee Name", dataLoan.sFHAAddendumMortgageeName);

            // Can't unlock...this needs to change at some point.
            dataLoan.sFHAAddendumMortgageeNameLckd = false;
            Assert.IsTrue(dataLoan.sFHAAddendumMortgageeNameLckd);
            Assert.AreEqual("LFF Mortgagee Name", dataLoan.sFHAAddendumMortgageeName);

            Assert.AreEqual(string.Empty, dataLoan.sFHAAddendumMortgageeTitle);
            dataLoan.sFHAAddendumMortgageeTitle = "Supreme Overlord";
            Assert.AreEqual("Supreme Overlord", dataLoan.sFHAAddendumMortgageeTitle);
        }

        [Test]
        public void TestFha203kUnderwriterLoanFileFields()
        {
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitLoad();

            SetUnderwriterAgentTestData(dataLoan);
            SetLenderAgentTestData(dataLoan);

            ClearOverrides(dataLoan.GetPreparerOfForm(E_PreparerFormT.FHA203kWorksheetUnderwriter, E_ReturnOptionIfNotExist.CreateNew), true);

            Assert.AreEqual(false, dataLoan.sFHA203kUnderwriterNameLckd);
            Assert.AreEqual("UW Agent Name", dataLoan.sFHA203kUnderwriterName);

            dataLoan.sFHA203kUnderwriterNameLckd = true;
            dataLoan.sFHA203kUnderwriterName = "LFF UW Name";
            Assert.AreEqual("LFF UW Name", dataLoan.sFHA203kUnderwriterName);

            // Can't unlock...this needs to change at some point.
            dataLoan.sFHA203kUnderwriterNameLckd = false;
            Assert.IsTrue(dataLoan.sFHA203kUnderwriterNameLckd);
            Assert.AreEqual("LFF UW Name", dataLoan.sFHA203kUnderwriterName);

            Assert.AreEqual("12345", dataLoan.sFHA203kUnderwriterChumsId);

            dataLoan.sFHA203kUnderwriterChumsIdLckd = true;
            dataLoan.sFHA203kUnderwriterChumsId = "67890";
            Assert.AreEqual("67890", dataLoan.sFHA203kUnderwriterChumsId);

            // Can't unlock...this needs to change at some point.
            dataLoan.sFHA203kUnderwriterChumsIdLckd = false;
            Assert.IsTrue(dataLoan.sFHA203kUnderwriterChumsIdLckd);
            Assert.AreEqual("67890", dataLoan.sFHA203kUnderwriterChumsId);
        }

        [Test]
        public void TestFhaAnalysisOfAppraisalUnderwriterLoanFileFields()
        {
            CPageData dataLoan = CreateDataObject();
            dataLoan.InitLoad();

            SetUnderwriterAgentTestData(dataLoan);
            SetLenderAgentTestData(dataLoan);

            ClearOverrides(dataLoan.GetPreparerOfForm(E_PreparerFormT.FHAAnalysisAppraisalUnderwriter, E_ReturnOptionIfNotExist.CreateNew), true);

            Assert.AreEqual(false, dataLoan.sFHAApprUnderwriterNameLckd);
            Assert.AreEqual("UW Agent Name", dataLoan.sFHAApprUnderwriterName);

            dataLoan.sFHAApprUnderwriterNameLckd = true;
            dataLoan.sFHAApprUnderwriterName = "LFF UW Name";
            Assert.AreEqual("LFF UW Name", dataLoan.sFHAApprUnderwriterName);

            // Can't unlock...this needs to change at some point.
            dataLoan.sFHAApprUnderwriterNameLckd = false;
            Assert.IsTrue(dataLoan.sFHAApprUnderwriterNameLckd);
            Assert.AreEqual("LFF UW Name", dataLoan.sFHAApprUnderwriterName);

            Assert.AreEqual("12345", dataLoan.sFHAApprUnderwriterChumsId);

            dataLoan.sFHAApprUnderwriterChumsIdLckd = true;
            dataLoan.sFHAApprUnderwriterChumsId = "67890";
            Assert.AreEqual("67890", dataLoan.sFHAApprUnderwriterChumsId);

            // Can't unlock...this needs to change at some point.
            dataLoan.sFHAApprUnderwriterChumsIdLckd = false;
            Assert.IsTrue(dataLoan.sFHAApprUnderwriterChumsIdLckd);
            Assert.AreEqual("67890", dataLoan.sFHAApprUnderwriterChumsId);
        }
    }
}
