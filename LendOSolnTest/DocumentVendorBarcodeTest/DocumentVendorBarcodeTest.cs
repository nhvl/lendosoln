﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Constants;
using LendersOffice.Conversions;
using LendersOffice.Edocs.DocMagic;
using LendersOffice.Edocs.DocuTech;
using LendersOffice.Integration.DocumentVendor;
using LendersOffice.ObjLib.Edocs;
using LendersOffice.ObjLib.Edocs.IDS;
using LendersOffice.Security;
using LendOSolnTest.Common;
using NUnit.Framework;
using LendersOffice.ObjLib.Edocs.DocuTech;
using LqbGrammar.DataTypes;
using LendersOffice.Common;

namespace LendOSolnTest.DocumentVendorBarcodeTest
{
    [TestFixture]
    public class DocumentVendorBarcodeTest
    {

        private IEnumerable<IDocVendorBarcode> ListBarCodesFromFileDB(E_FileDB fileDB, string key)
        {
            string filedbname = E_FileDB.Temp.GetDatabaseName();

            string url = string.Format("{0}?DBName={1}&dbfilekey={2}", ConstStage.BarcodeReaderUrl, filedbname, key);
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            webRequest.Timeout = 600000; //10 minutes

            List<LendersOffice.ObjLib.Edocs.Barcode> pdfPagesWithBarcode = new List<LendersOffice.ObjLib.Edocs.Barcode>();

            using (WebResponse response = webRequest.GetResponse())
            {
                using (Stream sr = response.GetResponseStream())
                {
                    using (XmlReader xmlReader = XmlReader.Create(sr))
                    {
                        XDocument doc = XDocument.Load(xmlReader);

                        if (doc.Root.Attribute("status").Value == "Error")
                        {
                            throw CBaseException.GenericException(doc.Root.Attribute("error_message").Value);
                        }


                        foreach (XElement page in doc.Root.Elements("page"))
                        {
                            int number = int.Parse(page.Attribute("number").Value);

                            foreach (XElement barcode in page.Elements("barcode"))
                            {
                                string value = barcode.Attribute("value").Value;
                                LendersOffice.ObjLib.Edocs.Barcode b = new LendersOffice.ObjLib.Edocs.Barcode(number, value);
                                pdfPagesWithBarcode.Add(b);
                            }
                        }

                    }
                }
            }
            E_DocBarcodeFormatT barcodeFormatT = AutoDetectBarcode(pdfPagesWithBarcode);

            List<IDocVendorBarcode> barcodeList = new List<IDocVendorBarcode>();

            foreach (var o in pdfPagesWithBarcode)
            {
                IDocVendorBarcode vendorBarcode = null;

                switch (barcodeFormatT)
                {
                    case E_DocBarcodeFormatT.DocMagic:
                        vendorBarcode = new DocMagicBarcode(o);
                        break;
                    case E_DocBarcodeFormatT.DocuTech:
                        vendorBarcode = new DocuTechBarcode(o);
                        break;
                    case E_DocBarcodeFormatT.IDS:
                        vendorBarcode = new IDSBarcode(o);
                        break;
                    default:
                        throw new UnhandledEnumException(barcodeFormatT);
                }

                barcodeList.Add(vendorBarcode);
            }
            return barcodeList;
        }


        private static E_DocBarcodeFormatT AutoDetectBarcode(IEnumerable<LendersOffice.ObjLib.Edocs.Barcode> barcodes)
        {
            foreach (var bc in barcodes)
            {
                if (DocuTechBarcode.MatchesFormat(bc)) return E_DocBarcodeFormatT.DocuTech;
                else if (DocMagicBarcode.MatchesFormat(bc)) return E_DocBarcodeFormatT.DocMagic;
                else if (IDSBarcode.MatchesFormat(bc)) return E_DocBarcodeFormatT.IDS;
            }

            //This'll error out anyway, since none of them look like a supported barcode.
            throw new CBaseException("Unhandle Barcode Format", "Unhandle Barcode Format");
        }

        private Dictionary<string, string> GetDocuTechBarcodeClassification()
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            var list = DocuTechDocType.ListAll();

            foreach (var docuTechDocType in list)
            {
                try
                {
                    dictionary.Add(docuTechDocType.BarcodeClassification, docuTechDocType.Description);
                }
                catch (ArgumentException exc)
                {
                    throw new ArgumentException(docuTechDocType.BarcodeClassification, exc);
                }
            }

            return dictionary;
        }
        private void ApplyLoxmlToAutoTestAccount()
        {
            string path = @"C:\LOTemp\01_Conf_Fixed.xml";
            string sLNm = "01_Conf_Fixed";

            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.LoTest001);
            
            Guid sLId = Tools.GetLoanIdByLoanName(principal.BrokerId, sLNm);

            string xml = File.ReadAllText(path);

            string resultXml;
            LoFormatImporterResult result = LOFormatImporter.Import(sLId, principal, xml);
            resultXml = result.ErrorMessages;

            File.WriteAllText(@"C:\LoTemp\out.txt", resultXml);

        }
        /// <summary>
        /// This method will grab loan from AutoTest account and generate
        ///     1. Fnma
        ///     2. LOXmlFormat
        ///     3. Expected Barcode List
        ///     
        ///  After generation, copy output files in C:\LOTemp and move to \TestData\DocumentVendors\DocuTech folder.
        /// </summary>
        /// <param name="sLNm"></param>
        private void GenerateTestFiles(string outputFilePrefix, string sLNm)
        {
            string path = @"C:\LOTemp\";

            //AbstractUserPrincipal principal = LoginTools.Login("david_acc2", "lodblodb2");
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.LoTest001);


            BrokerDB brokerDB = BrokerDB.RetrieveById(principal.BrokerId);

            Guid sLId = Tools.GetLoanIdByLoanName(principal.BrokerId, sLNm);

            #region 01 - Generate FNMA File
            FannieMae32Exporter fnmaExporter = new FannieMae32Exporter(sLId);
            string fnmaContent = fnmaExporter.ExportToString();

            File.WriteAllText(path + outputFilePrefix + ".fnm", fnmaContent);
            #endregion

            #region 02 - Generate LOXml Format
            string requestDataXml = TestUtilities.ReadFile("/DocumentVendors/DocuTech/00_SampleLoad.xml");

            string loxml = LOFormatExporter.Export(sLId, principal, requestDataXml);
            XDocument xdoc = XDocument.Parse(loxml);
            xdoc.Save(path + outputFilePrefix + ".xml");
            #endregion

            #region 03 - Generate Expected Barcode list
            VendorConfig smokeTestVendorConfig = GetDocuTechVendorConfig(principal.BrokerId);

            GenerateExpectedBarCodeList(sLId, DocuTechDisclosurePackageId, DocuTechDisclosurePackageName, principal, outputFilePrefix);
            GenerateExpectedBarCodeList(sLId, DocuTechClosingPackageId, DocuTechClosingPackageName, principal, outputFilePrefix);

            #endregion
        }

        private void GenerateExpectedBarCodeList(Guid sLId, string packageType, LqbGrammar.DataTypes.DocumentIntegrationPackageName packageName, AbstractUserPrincipal principal, string outputFilePrefix)
        {
            string path = @"C:\LOTemp\";
            VendorConfig smokeTestVendorConfig = GetDocuTechVendorConfig(principal.BrokerId);
            
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(DocumentVendorBarcodeTest));
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            if (DocuTechDisclosurePackageId == packageType)
            {
                dataLoan.sSettlementChargesExportSource = E_SettlementChargesExportSource.GFE;
            }
            else
            {
                dataLoan.sSettlementChargesExportSource = E_SettlementChargesExportSource.SETTLEMENT;
            }
            dataLoan.Save();


            ConfiguredDocumentVendor configuredDocumentVendor = new ConfiguredDocumentVendor(smokeTestVendorConfig, principal.BrokerId);
            IDocumentGenerationRequest request = configuredDocumentVendor.GetDocumentGenerationRequest(dataLoan.GetAppData(0).aAppId, sLId);

            request.PackageType = packageType;
            request.PackageName = packageName;
            request.LoanProgramId = dataLoan.sLpTemplateNm;

            var documentRequestResult = request.RequestDocuments(isPreviewOrder: false, principal: principal);

            if (documentRequestResult.HasError)
            {
                Assert.Fail("PackageType=[" + DocuTechDisclosurePackageId + "] IsClosingPackage=" +
                    smokeTestVendorConfig.IsClosingPackage(DocuTechDisclosurePackageId) + " Failed. " +
                    documentRequestResult.ErrorMessage);
            }

            ConfiguredDocumentGenerationResult result = documentRequestResult.Result as ConfiguredDocumentGenerationResult;

            if (result == null)
            {
                Assert.Fail("Expected result derived from ConfiguredDocumentGenerationResult");
                return;
            }
            var barcodeList = ListBarCodesFromFileDB(E_FileDB.Temp, result.PdfFileDbKey);

            Dictionary<string, string> barcodeClassificationDictionary = GetDocuTechBarcodeClassification();
            Dictionary<string, string> documentClassSet = new Dictionary<string, string>();

            XDocument xdoc = new XDocument();
            XElement rootElement = new XElement("root");
            xdoc.Add(rootElement);

            foreach (var barCode in barcodeList.OrderBy(o => o.PackagePageNumber))
            {
                rootElement.Add(new XElement("barcode", new XAttribute("pg", barCode.PackagePageNumber.ToString()),
                    new XAttribute("id", barCode.DocumentClass),
                    new XAttribute("desc", barcodeClassificationDictionary[barCode.DocumentClass])));

            }
            xdoc.Save(path + outputFilePrefix + "." + request.PackageType + ".barcode.xml");
            byte[] bytes = FileDBTools.ReadData(E_FileDB.Temp, result.PdfFileDbKey);
            File.WriteAllBytes(path + outputFilePrefix + "." + request.PackageType + ".pdf", bytes);
        }
        private void Generate()
        {
            GenerateTestFiles("01_Conf_Fixed", "01_Conf_Fixed");
        }
        private const string DocuTechDisclosurePackageId = "203";
        private readonly DocumentIntegrationPackageName DocuTechDisclosurePackageName = DocumentIntegrationPackageName.Create("Initial Disclosure").ForceValue();
        private const string DocuTechClosingPackageId = "202";
        private readonly DocumentIntegrationPackageName DocuTechClosingPackageName = DocumentIntegrationPackageName.Create("Closing").ForceValue();
        private VendorConfig GetDocuTechVendorConfig(Guid brokerId)
        {
            BrokerDB brokerDB = BrokerDB.RetrieveById(brokerId);

            VendorConfig smokeTestVendorConfig = null;
            foreach (var documentVendor in brokerDB.AllActiveDocumentVendors)
            {
                VendorConfig vendorConfig = VendorConfig.Retrieve(documentVendor.VendorId);

                if (vendorConfig.VendorName.Equals("SMOKETEST (DocuTech)", StringComparison.OrdinalIgnoreCase))
                {
                    smokeTestVendorConfig = vendorConfig;
                    break;
                }

            }

            if (smokeTestVendorConfig == null)
            {
                throw new CBaseException("AutoTest suit does not setup 'SMOKETEST (DocuTech)' document vendor integration",
                    "AutoTest suit does not setup 'SMOKETEST (DocuTech)' document vendor integration");
            }

            return smokeTestVendorConfig;
        }
        [Test]
        public void TestDocuTech()
        {
            //Generate();
            //ApplyLoxmlToAutoTestAccount();
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.LoTest001);


            BrokerDB brokerDB = BrokerDB.RetrieveById(principal.BrokerId);

            if (brokerDB.DocumentValidationOptionT == E_BrokerDocumentValidationOptionT.NoValidation)
            {
                // 4/22/2014 dd - To avoid smoke test fail on local each time DocuTech update and break our test.
                return;
            }
            VendorConfig smokeTestVendorConfig = null;
            foreach (var documentVendor in brokerDB.AllActiveDocumentVendors)
            {
                VendorConfig vendorConfig = VendorConfig.Retrieve(documentVendor.VendorId);

                if (vendorConfig.VendorName.Equals("SMOKETEST (DocuTech)", StringComparison.OrdinalIgnoreCase)) 
                {
                    smokeTestVendorConfig = vendorConfig;
                    break;
                }

            }

            if (smokeTestVendorConfig == null)
            {
                throw new CBaseException("AutoTest suit does not setup 'SMOKETEST (DocuTech)' document vendor integration",
                    "AutoTest suit does not setup 'SMOKETEST (DocuTech)' document vendor integration");
            }

            ConfiguredDocumentVendor configuredDocumentVendor = new ConfiguredDocumentVendor(smokeTestVendorConfig, principal.BrokerId);

            string[] testCaseList = {
                                        "01_Conf_Fixed"
                                    };
            foreach (string testCase in testCaseList)
            {
                Guid sLId = Guid.Empty;
                Guid aAppId = Guid.Empty;
                try
                {
                    LoadTestLoanData(principal, testCase, out sLId, out aAppId);



                    IDocumentGenerationRequest request = configuredDocumentVendor.GetDocumentGenerationRequest(aAppId, sLId);

                    foreach (Tuple<string, DocumentIntegrationPackageName> package in new[] { Tuple.Create(DocuTechDisclosurePackageId, DocuTechDisclosurePackageName), Tuple.Create(DocuTechClosingPackageId, DocuTechClosingPackageName) })
                    {
                        string packageType = package.Item1;
                        bool isClosingPackage = smokeTestVendorConfig.IsClosingPackage(packageType);
                        CPageData dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(DocumentVendorBarcodeTest));
                        dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                        dataLoan.sSettlementChargesExportSource = isClosingPackage ? E_SettlementChargesExportSource.SETTLEMENT : E_SettlementChargesExportSource.GFE;
                        dataLoan.Save();
                        string messagePrefix = "Package=[" + packageType + "]. IsClosingPackage=" + isClosingPackage + " TestCase=[" + testCase + "] - ";
                        request.PackageType = packageType;
                        request.PackageName = package.Item2;
                        request.LoanProgramId = dataLoan.sLpTemplateNm;


                        var documentRequestResult = request.RequestDocuments(isPreviewOrder: false, principal: principal);

                        Assert.AreEqual(false, documentRequestResult.HasError, messagePrefix + documentRequestResult.ErrorMessage);

                        ConfiguredDocumentGenerationResult result = documentRequestResult.Result as ConfiguredDocumentGenerationResult;

                        if (result == null)
                        {
                            Assert.Fail("Expected result derived from ConfiguredDocumentGenerationResult");
                            return;
                        }
                        var barcodeList = ListBarCodesFromFileDB(E_FileDB.Temp, result.PdfFileDbKey);
                        Dictionary<string, string> documentClassSet = new Dictionary<string, string>();


                        foreach (var barCode in barcodeList.OrderBy(o => o.PackagePageNumber))
                        {
                            documentClassSet.Add(barCode.PackagePageNumber.ToString(), barCode.DocumentClass);
                        }
                        XDocument xdoc = XDocument.Parse(TestUtilities.ReadFile("DocumentVendors/DocuTech/" + testCase + "." + packageType + ".barcode.xml"));

                        Dictionary<string, string> expectedDocumentClassSet = new Dictionary<string, string>();
                        foreach (XElement el in xdoc.Root.Elements("barcode"))
                        {
                            expectedDocumentClassSet.Add(el.Attribute("pg").Value, el.Attribute("id").Value);
                        }
                        
                        // 4/24/2014 dd - Uncomment the bellow line to generate a new base line of test case.
                        //GenerateExpectedBarCodeList(dataLoan.sLId, packageType, principal, "01_Conf_Fixed");
                        
                        AssertSameHashset(expectedDocumentClassSet, documentClassSet, messagePrefix);
                    }


                }
                finally
                {
                    if (sLId != Guid.Empty)
                    {
                        Tools.DeclareLoanFileInvalid(principal, sLId, false, false);
                    }
                }
            }

              
        }
        private void AssertSameHashset(Dictionary<string, string> expectedSet, Dictionary<string, string> actualSet, string message)
        {
            Dictionary<string, string> barcodeClassificationDictionary = GetDocuTechBarcodeClassification();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine(message);
            sb.AppendLine();
            bool isSame = true;

            SortedList<int, string> messageList = new SortedList<int, string>();
            foreach (var obj in actualSet)
            {
                if (expectedSet.ContainsKey(obj.Key) == false)
                {
                    isSame = false;
                    messageList.Add(int.Parse(obj.Key), "Pg=" + obj.Key + " NEW. New Barcode type=[" + obj.Value + "/" + barcodeClassificationDictionary[obj.Value] + "].");
                }
                else
                {
                    if (obj.Value != expectedSet[obj.Key])
                    {
                        isSame = false;
                        messageList.Add(int.Parse(obj.Key), "Pg=" + obj.Key + " MISMATCH. Expected=[" + expectedSet[obj.Key] + "/" + barcodeClassificationDictionary[expectedSet[obj.Key]] + "]. Actual=[" + obj.Value + "/" + barcodeClassificationDictionary[obj.Value] + "]");
                    }
                    expectedSet.Remove(obj.Key);
                }
            }
            foreach (var obj in expectedSet)
            {
                isSame = false;
                messageList.Add(int.Parse(obj.Key), "Pg=" + obj.Key + " MISSING. Old Barcode Type=[" + obj.Value + "/" + barcodeClassificationDictionary[obj.Value] + "].");
            }
            foreach (var s in messageList)
            {
                sb.AppendLine(s.Value);
            }
            Assert.AreEqual(true, isSame, sb.ToString());
        }
        private bool LoadTestLoanData(AbstractUserPrincipal principal, string testcaseName, out Guid sLId, out Guid aAppId)
        {

            CPageData dataLoan = null;
            using (Stream stream = TestUtilities.GetFileStream("/DocumentVendors/DocuTech/" + testcaseName + ".fnm"))
            {
                dataLoan = FannieMaeImporter.Import(principal, stream, true, Guid.Empty);
            }
            sLId = dataLoan.sLId;
            aAppId = dataLoan.GetAppData(0).aAppId;

            string xml = TestUtilities.ReadFile("/DocumentVendors/DocuTech/" + testcaseName + ".xml");
            LOFormatImporter.Import(sLId, principal, xml);
            return true;
        }
    }
}
