﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using LendOSolnTest.Common;
using LendersOffice.Security;
using DataAccess;
using LendersOffice.ObjLib.Task;
using System.Threading;

namespace LendOSolnTest.TaskTest
{
    //[TestFixture]
    public class TaskPerformanceTest
    {
        private const int NumberOfLoans = 10;
        private const int NumberOfTasksPerLoan = 20;

        private List<Guid> m_loanIdList = new List<Guid>();

        //[TestFixtureSetUp]
        public void FixtureSetUp()
        {
            LoginTools.LoginAs(TestAccountType.LoTest001);
            BrokerUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;

            for (int i = 0; i < NumberOfLoans; i++)
            {
                CLoanFileCreator creator = CLoanFileCreator.GetCreator(principal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
                Guid sLId = creator.CreateBlankLoanFile();
                m_loanIdList.Add(sLId);
            }
        }
        //[TestFixtureTearDown]
        public void FixtureTearDown()
        {
            foreach (Guid sLId in m_loanIdList)
            {
                Tools.DeclareLoanFileInvalid(BrokerUserPrincipal.CurrentPrincipal, sLId, false, false);
            }
        }
        //[Test]
        public void PerformanceTest()
        {
            AbstractUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;
            Guid brokerId = principal.BrokerId;
            List<Thread> threadList = new List<Thread>();
            bool hasException = false;
            foreach (Guid sLId in m_loanIdList)
            {
                Thread thread = new Thread(
                    delegate()
                    {
                        for (int j = 0; j < NumberOfTasksPerLoan; j++)
                        {
                            try
                            {
                                CreateTask(brokerId, sLId);
                            }
                            catch
                            {
                                hasException = true;
                                throw;
                            }
                        }
                    });
                thread.Start();
            }
            foreach (var thread in threadList)
            {
                thread.Join();

            }
            Assert.AreEqual(false, hasException);
        }
        private void CreateTask(Guid brokerId, Guid sLId)
        {
            AbstractUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;
            ConditionCategory conditionCategory = ConditionCategory.GetCategories(brokerId).First();

            Task task = Task.Create(sLId, brokerId);
            task.TaskDueDateCalcField = "sEstClosedD";
            task.TaskSubject = "TEST TASK CONDITION";
            task.TaskStatus = E_TaskStatus.Active;
            task.TaskPermissionLevelId = conditionCategory.DefaultTaskPermissionLevelId;
            task.TaskAssignedUserId = principal.UserId;
            task.TaskOwnerUserId = principal.UserId;
            task.Save(true);

        }
    }
}
