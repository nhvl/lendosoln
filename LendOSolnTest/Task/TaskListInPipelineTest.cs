﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using LendOSolnTest.Common;
using LendersOffice.Security;
using LendersOffice.ObjLib.Task;
using System.Data.SqlClient;

namespace LendOSolnTest.TaskTest
{
    [TestFixture]
    public class TaskListInPipelineTest
    {
        [Test]
        [Ignore]
        public void TestListTaskInLendingQB()
        {
            var accountTypeList = new TestAccountType[] { TestAccountType.LoTest001, 
                TestAccountType.Branch1_LenderAccountExec, TestAccountType.Corporate_Underwriter,
                TestAccountType.Pml_Supervisor, TestAccountType.Pml_BP1, TestAccountType.Pml_LO1
            };
            foreach (TestAccountType account in accountTypeList)
            {
                AbstractUserPrincipal principal = LoginTools.LoginAs(account);
                foreach (Guid assignedUserIdFilter in new Guid[] { Guid.Empty, principal.UserId}) {
                    foreach (E_TaskStatus filterTaskStatus in new E_TaskStatus[] { E_TaskStatus.Active, E_TaskStatus.Resolved })
                    {
                        foreach (string sortField in new string[] { "TaskDueDate", "TaskFollowUpDate", "TaskLastModifiedDate"}) {
                            foreach (SortOrder sortOrder in new SortOrder[] { SortOrder.Ascending, SortOrder.Descending })
                            {
                                TaskUtilities.GetTasksInPipeline(principal, assignedUserIdFilter, filterTaskStatus, sortField, sortOrder);
                            }
                        }
                    }
                }
            }

        }
    }
}
