﻿using NUnit.Framework;
using DataAccess;
using System;
using LendersOffice.Security;
using LendOSolnTest.Common;
using LendersOffice.Audit;
using LendersOffice.Reminders;
using LendersOffice.Common;
using LendersOffice.ObjLib.Task;
using System.Xml.Linq;
using System.Collections.Generic;

namespace LendOSolnTest.TaskTest
{
    public static class Extensions
    {
        /// <summary>
        /// Get the array slice between the two indexes.
        /// ... Inclusive for start index, exclusive for end index.
        /// </summary>
        public static List<T> Slice<T>(this List<T> source, int start, int end)
        {
            int len = end - start;

            // Return new array.
            List<T> res = new List<T>(len);
            for (int i = 0; i < len; i++)
            {
                res.Add(source[i + start]);
            }
            return res;
        }
    }

    /// <summary>
    /// NOTE: This does NOT test
    ///     Task:EnqueueTaskNotification,
    ///  or TaskHistory:GetHtmlContentForNewEntries
    ///  
    /// Also, hardcoded magic numbers are bad. Do not imitate.
    /// </summary>
    [TestFixture]
    public class TaskNotificationTest
    {
        System.Security.Principal.IPrincipal OldPrincipal;

        private List<XDocument> XDocs = new List<XDocument>();
        
        /// <summary>
        /// Contains the expected results for XDocument parsing for each of the XDocs
        /// 0: TaskId
        /// 1: TaskDueDate
        /// 2: LoanNumCached
        /// 3: BorrowerNmCached
        /// 4: TaskSubject
        /// 5: BrokerId
        /// 6: UserId
        /// 7: Body
        /// </summary>
        private List<List<string>> XDocAnswers = new List<List<string>>();

        private List<TaskNotification> Notifications = new List<TaskNotification>();

        private string[] Tasks = new string[] { "TASK0", "TASK1", "TASK2", "TASK3" };
        private string[] Loans = new string[] { "LOAN0", "LOAN1", "LOAN2", "LOAN3" };
        private List<Guid> Recipients = new List<Guid>() {
                Guid.NewGuid(), // Larry Loanofficer: collects rare gel pens
                Guid.NewGuid(), // Ulrich Underwriter: in an experimental punk-folk band
                Guid.NewGuid(), // Carey Closer: owns too many chopsticks
                Guid.NewGuid(), // Patty Processor: was literally born yesterday
            };

        private Guid BrokerId = Guid.NewGuid();
        private Guid PreviousAssignee = Guid.NewGuid(); // Some guy who quit
        private Guid CurrentAssignee = Guid.NewGuid(); // Ashley Agent: can beat all o' ya'll in a rap battle

        private TaskNotification CreateNotification(
            string emailedFrom, Guid previousAssignee, Guid currentAssignee,
            string taskId, string taskDueDate, string loanNumCached, string borrowerNmCached,
            string taskSubject, Guid brokerId, List<Guid> recipients, string body)
        {
            var recipientsCopy = new List<Guid>(recipients);
            recipientsCopy.Add(previousAssignee);
            recipientsCopy.Remove(currentAssignee);
            recipientsCopy.Remove(Guid.Empty);
            return new TaskNotification()
            {
                EmailedFrom = emailedFrom,
                PreviousAssignedUserId = previousAssignee,
                CurrentAssignedUserId = currentAssignee,

                // Header
                TaskId = taskId,
                TaskDueDate = taskDueDate,
                LoanNumCached = loanNumCached,
                BorrowerNmCached = borrowerNmCached,
                TaskSubject = taskSubject,
                BrokerId = brokerId,

                // Recipients
                Recipients = recipientsCopy,

                // Body
                Body = body,
            };
        }

        

        [TestFixtureSetUp]
        public void SetUp()
        {
            OldPrincipal = System.Threading.Thread.CurrentPrincipal;
            System.Threading.Thread.CurrentPrincipal = SystemUserPrincipal.TaskSystemUser; // Current user

            XDocAnswers.Add(new List<string>()
                {
                    "CD9LWX",
                    "2/2/2012",
                    "Task Notifications Q1 2012",
                    "a b",
                    "where did you get this",
                    "839127ef-72d1-4873-bfdf-18a23416b146",
                    "188236ad-b3dc-4dd6-b342-957d137d576a",
                    @"<HTML>
                      <HEAD>
                        <META http-equiv='Content-Type' content='text/html; charset=utf-8'>
                        <style type='text/css'>
                              table.mail
                              {
                              border-style:solid;
                              border-color:darkgray;
                              border-width:5px;
                              border-collapse:collapse;          
                              }
                              thead
                              {
                              background:darkgray;
                              }
                            </style>
                      </HEAD>
                      <body><strong>Opened and assigned to </strong><i>Matthew2 Pham2</i><strong> by </strong><i>Matthew Pham</i> 2/2/2012 3:04 PM PT<br><text>  </text>when i was younger,
                    <br />i lived in fear
                    <br />that incarceration
                    <br />of some kind was near<br><br></body>
                    </HTML>"
                }
            );
            XDocs.Add(XDocument.Parse(
                    string.Format(@"
                        <Root>
                          <Header>
                            <TaskId>{0}</TaskId>
                            <TaskDueDate>{1}</TaskDueDate>
                            <LoanNumCached>{2}</LoanNumCached>
                            <BorrowerNmCached>{3}</BorrowerNmCached>
                            <TaskSubject>{4}</TaskSubject>
                            <BrokerId>{5}</BrokerId>
                          </Header>
                          <Recipients>
                            <Recipient userId='{6}' />
                          </Recipients>
                          <Body><![CDATA[{7}]]></Body>
                        </Root>",
                        XDocAnswers[0][0],
                        XDocAnswers[0][1],
                        XDocAnswers[0][2],
                        XDocAnswers[0][3],
                        XDocAnswers[0][4],
                        XDocAnswers[0][5],
                        XDocAnswers[0][6],
                        XDocAnswers[0][7]
                    )
                )
            );
            

            Random rng = new Random(0);

            for (int i = 0; i < 10; i++) // Add regular notifications, task 0
            {
                Notifications.Add(
                    CreateNotification(
                        null,
                        Guid.Empty,
                        CurrentAssignee,
                        Tasks[0],
                        DateTime.Now.AddDays(rng.Next(100) - 50).ToShortDateString(), // within 50 days of today
                        Loans[0],
                        "Andy Andanowitz",
                        "Test0",
                        BrokerId,
                        Recipients,
                        "NEXT STOP: Placeholdernistan " + i
                    )
                );
            }
            // Add regular notifications... without Patty please, task 0
            for (int i = 0; i < 5; i++)
            {
                Notifications.Add(
                    CreateNotification(
                        null,
                        Guid.Empty,
                        CurrentAssignee,
                        Tasks[0],
                        DateTime.Now.AddDays(rng.Next(100) - 50).ToShortDateString(), // within 50 days of today
                        Loans[0],
                        "Andy Andanowitz",
                        "Test0",
                        BrokerId,
                        Recipients.Slice(0, Recipients.Count-1),
                        "I hope Patty can't see this " + i
                    )
                );
            }

            // Add regular notifications, task 1
            for (int i = 0; i < 5; i++)
            {
                Notifications.Add(
                    CreateNotification(
                        null,
                        Guid.Empty,
                        CurrentAssignee,
                        Tasks[1],
                        DateTime.Now.AddDays(rng.Next(100) - 50).ToShortDateString(), // within 50 days of today
                        Loans[1],
                        "Barry Bostonian",
                        "Test1",
                        BrokerId,
                        Recipients,
                        "I'm serious about that banana. " + i
                    )
                );
            }
            // Add regular/emailed notifications, task 2
            for (int i = 0; i < 3; i++)
            {
                Notifications.Add(
                    CreateNotification(
                        null,
                        Guid.Empty,
                        CurrentAssignee,
                        Tasks[2],
                        DateTime.Now.AddDays(rng.Next(100) - 50).ToShortDateString(), // within 50 days of today
                        Loans[2],
                        "Barry Bostonian",
                        "Test2",
                        BrokerId,
                        Recipients,
                        "I get the feeling that an email will be coming " + i
                    )
                );
            }
            Notifications.Add(
                CreateNotification(
                    "kittens@cats.com",
                    Guid.Empty,
                    CurrentAssignee,
                    Tasks[2],
                    DateTime.Now.AddDays(rng.Next(100) - 50).ToShortDateString(), // within 50 days of today
                    Loans[2],
                    "Barry Bostonian",
                    "Test2",
                    BrokerId,
                    Recipients,
                    "EMAIL! HERE!"
                )
            );
            // Add emailed/regular notifications, task 3
            Notifications.Add(
                CreateNotification(
                    "kottins@cots.com",
                    Guid.Empty,
                    CurrentAssignee,
                    Tasks[3],
                    DateTime.Now.AddDays(rng.Next(100) - 50).ToShortDateString(), // within 50 days of today
                    Loans[3],
                    "Charlie Channis",
                    "Test3",
                    BrokerId,
                    Recipients,
                    "Oy've got an EMAIL for some of those cots"
                )
            );
            for (int i = 0; i < 3; i++)
            {
                Notifications.Add(
                    CreateNotification(
                        null,
                        Guid.Empty,
                        PreviousAssignee,
                        Tasks[3],
                        DateTime.Now.AddDays(rng.Next(100) - 50).ToShortDateString(), // within 50 days of today
                        Loans[3],
                        "Charlie Channis",
                        "Test3",
                        BrokerId,
                        Recipients,
                        "Whew, was that an email? " + i
                    )
                );
            }
            // Add notifications with previous assignee, task 3
            for (int i = 0; i < 3; i++)
            {
                Notifications.Add(
                    CreateNotification(
                        null,
                        PreviousAssignee,
                        CurrentAssignee,
                        Tasks[3],
                        DateTime.Now.AddDays(rng.Next(100) - 50).ToShortDateString(), // within 50 days of today
                        Loans[3],
                        "Charlie Channis",
                        "Test3",
                        BrokerId,
                        Recipients,
                        "Assign it to someone else " + i
                    )
                );
            }
        }

        /// <summary>
        /// Test extracting data from an XDocument
        /// </summary>
        [Test]
        public void TaskNotificationCreationTest()
        {
            for (int i = 0; i < XDocs.Count; i++)
            {
                // 0: TaskId
                // 1: TaskDueDate
                // 2: LoanNumCached
                // 3: BorrowerNmCached
                // 4: TaskSubject
                // 5: BrokerId
                // 6: UserId
                // 7: Body
                TaskNotification tn = new TaskNotification(XDocs[i]);
                Assert.That(tn.TaskId == XDocAnswers[i][0]);
                Assert.That(tn.TaskDueDate == XDocAnswers[i][1]);
                Assert.That(tn.LoanNumCached == XDocAnswers[i][2]);
                Assert.That(tn.BorrowerNmCached == XDocAnswers[i][3]);
                Assert.That(tn.TaskSubject == XDocAnswers[i][4]);
                Assert.That(tn.BrokerId == new Guid(XDocAnswers[i][5]));
                Assert.That(tn.Recipients[0] == new Guid(XDocAnswers[i][6]));

                var parsedBody = tn.Body.Replace("\r\n", "\n"); // These are lost in translation
                var origBody = tn.Body.Replace("\r\n", "\n");
                Assert.That(parsedBody == origBody);

            }
            
            // TODO: Test the Task constructor
            // Hard to do since the Task has all of these permissions checks

        }

        /// <summary>
        /// Converting notifications to XML and back
        /// </summary>
        [Test]
        public void MarshalUnmarshal()
        {
            foreach (var notification in Notifications)
            {
                var xDoc = notification.ToXml();
                var cloneNotification = new TaskNotification(xDoc);
                Assert.That(notification.EmailedFrom == cloneNotification.EmailedFrom);
                Assert.That(notification.PreviousAssignedUserId == cloneNotification.PreviousAssignedUserId);
                // Assert.That(notification.CurrentAssignedUserId == cloneNotification.CurrentAssignedUserId); // Not set since it's only needed for recipients

                Assert.That(notification.TaskId == cloneNotification.TaskId);
                Assert.That(notification.TaskDueDate == cloneNotification.TaskDueDate);
                Assert.That(notification.LoanNumCached == cloneNotification.LoanNumCached);
                Assert.That(notification.BorrowerNmCached == cloneNotification.BorrowerNmCached);
                Assert.That(notification.TaskSubject == cloneNotification.TaskSubject);
                Assert.That(notification.BrokerId == cloneNotification.BrokerId);

                CollectionAssert.AreEqual(notification.Recipients, cloneNotification.Recipients);
                
                Assert.That(notification.Body == cloneNotification.Body);
                
                var cloneXDoc = cloneNotification.ToXml();
                Assert.That(XNode.DeepEquals(xDoc, cloneXDoc));
            }
        }

        [TestFixtureTearDown]
        public void TearDown()
        {
            System.Threading.Thread.CurrentPrincipal = OldPrincipal;
        }
    }
}