﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using LendersOffice.Security;
using LendOSolnTest.Common;
using LendersOffice.Audit;
using LendersOffice.Constants;
using LendersOffice.ObjLib.Task;
using NUnit.Framework;
using LendersOffice.Admin;
using System.Reflection;
using LendersOffice.Email;
using System.Data.SqlClient;
using System.Data;
using LendersOffice.ConfigSystem;
using LendersOffice.ConfigSystem.Operations;
using LqbGrammar.Drivers.SecurityEventLogging;
using LendersOfficeApp.newlos.Tasks;

namespace LendOSolnTest.TaskTest
{
    [TestFixture]
    public class TaskStateTest
    {
        #region Member Variables
        private AbstractUserPrincipal m_loUserPrincipal = LoginTools.LoginAs(TestAccountType.LoTest001);
        private string taskID;// = "C9WMKG";
        private string condTaskID;// = "C9WP9H";
        private string closedTaskID;// = "C9WP9J";
        private string pmlClosedTaskID;// = "CCCXDL";
        private string pmlTaskID;// = "CCCXNP";
        private Guid m_loanID;
        private Guid m_pmlLoanID;
        private Guid m_brokerID;// = new Guid("d87663fc-abcb-4420-8e84-a76f81a93708");
        #endregion

        #region Setup Stuff
        [TestFixtureSetUp]
        public void Setup()
        {
            
            
            m_brokerID = m_loUserPrincipal.BrokerId;
            //m_loanID = new Guid("5dee7bcc-0bed-45ff-bc5d-ad88746775d4");
            CreateLoanFile();

            //m_pmlLoanID = new Guid("60234d44-1637-4fdd-b425-7fc9d917dc36");
            CreatePMLLoanFile();

            //pmlClosedTaskID = "CCCXDL";
            //pmlTaskID = "CCCXNP";
            CreatePMLClosedTask();
            CreatePMLTask();
            CreatePMLTask();


            //taskID = "CCCR7M";
            //condTaskID = "CCCR7N";
            //closedTaskID = "CCCWCH";
            
            CreateTask();
            CreateCondition();
            CreateClosedTask();



        }
        #region Loan Creation
        private void CreatePMLLoanFile()
        {
            var pmlUserP = LoginTools.LoginAs(TestAccountType.Pml_User0);
            CPageData dataLoan;

            CLoanFileCreator fileCreator = CLoanFileCreator.GetCreator(pmlUserP, E_LoanCreationSource.UserCreateFromBlank);

            m_pmlLoanID = fileCreator.CreateBlankLoanFile();
            dataLoan = CPageData.CreateUsingSmartDependency(m_pmlLoanID, typeof(TaskStateTest));
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck); // this must come before attempting to get dataApp.
            CAppData dataApp = dataLoan.GetAppData(0);
            if (dataApp != null)
            {
                dataApp.aBFirstNm = "Mistuh";
                dataApp.aBLastNm = "PML";
            }


            dataLoan.Save();
            LoginTools.LoginAs(TestAccountType.LoTest001); // just in case this has side effects.
        }
        private void CreateLoanFile()
        {
            CPageData dataLoan;

            CLoanFileCreator fileCreator = CLoanFileCreator.GetCreator(m_loUserPrincipal, E_LoanCreationSource.UserCreateFromBlank);

            m_loanID = fileCreator.CreateBlankLoanFile();
            dataLoan = CPageData.CreateUsingSmartDependency(m_loanID, typeof(TaskStateTest));
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck); // this must come before attempting to get dataApp.
            CAppData dataApp = dataLoan.GetAppData(0);
            if (dataApp != null)
            {
                dataApp.aBFirstNm = "My name is ";
                dataApp.aBLastNm = "Jonas";
            }

           dataLoan.Save();
        }
        #endregion

        #region Task Creation
        private void CreateTask()
        {
            var brokerID = m_loUserPrincipal.BrokerId;
            Task task = Task.Create(m_loanID, brokerID);
            task.TaskSubject = "non empty subject.";
            task.TaskDueDate = DateTime.Now.AddDays(5);
            task.Save(false);
            taskID = task.TaskId;
        }
        private void CreateCondition()
        {
            var brokerID = m_loUserPrincipal.BrokerId;
            
            Task task = Task.Create(m_loanID, brokerID);
            task.TaskSubject = "non empty subject.";
            task.TaskDueDate = DateTime.Now.AddDays(5);
            task.TaskIsCondition = true;

            var categories = ConditionCategory.GetCategories(brokerID);
            var catID = categories.First().Id;
            task.CondCategoryId = catID;

            var permissionLevels = GetPermissionLevels(brokerID, true);
            task.TaskPermissionLevelId = permissionLevels.First().Id;
            task.Save(false);
            condTaskID = task.TaskId;

        }
        private void CreateClosedTask()
        {
            var brokerID = m_loUserPrincipal.BrokerId;
            
            Task task = Task.Create(m_loanID, brokerID);
            task.TaskSubject = "non empty subject.";
            task.TaskDueDate = DateTime.Now.AddDays(5);
            task.Save(false);
            closedTaskID = task.TaskId;
            task = Task.Retrieve(brokerID, closedTaskID);
            task.Close();

        }
        private void CreatePMLClosedTask()
        {
            var pmlUserP = LoginTools.LoginAs(TestAccountType.Pml_User0);
            var brokerID = pmlUserP.BrokerId;
            
            Task task = Task.Create(m_pmlLoanID, brokerID);
            task.TaskSubject = "non empty subject.";
            task.TaskDueDate = DateTime.Now.AddDays(5);
            task.Save(false);
            pmlClosedTaskID = task.TaskId;
            task = Task.Retrieve(brokerID, pmlClosedTaskID);
            task.Close();
            LoginTools.LoginAs(TestAccountType.LoTest001);
        }
        private void CreatePMLTask()
        {
            var pmlUserP = LoginTools.LoginAs(TestAccountType.Pml_User0);
            var brokerID = pmlUserP.BrokerId;

            Task task = Task.Create(m_pmlLoanID, brokerID);
            task.TaskSubject = "non empty subject.";
            task.TaskDueDate = DateTime.Now.AddDays(5);
            task.Save(false);
            pmlTaskID = task.TaskId;

            LoginTools.LoginAs(TestAccountType.LoTest001);
        }
        #endregion
        [Test]
        public void TestSetup()
        {
            // retrieving a loan file's app data requires calling InitSave.
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(m_loanID, typeof(TaskStateTest));
            //dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.InitLoad(m_loUserPrincipal);
            CAppData dataApp = dataLoan.GetAppData(0);
            Assert.AreEqual("My name is", dataApp.aFirstNm);
            Assert.AreEqual("Jonas", dataApp.aLastNm);
            
            var brokerID = m_loUserPrincipal.BrokerId;
            Assert.That(TaskExists(brokerID, taskID));
            Assert.That(TaskExists(brokerID, condTaskID));
            Assert.That(TaskExists(brokerID, closedTaskID));
        }
        #endregion

        #region maybe tests?
        //! [Test]
        public void MiscFromSpec()
        {
            //! implement
            throw new NotImplementedException();
            //{
            //    var brokerID = m_loUserPrincipal.BrokerId;
            //    var task = Task.Retrieve(brokerID, condTaskID);


            //    task.TaskPermissionLevelId = ; //?
            //    task.TaskAssignedUserId = ; //! make sure user is either 
            //        //!     type B and both active and allow login
            //        //! or  type P and status is active.
            //        //! they must have read access to the loan file - lots of factors (2.1.1, page 8)
            //    task.TaskToBeAssignedRoleId = ; // make sure it matches the assigned user.
            //    task.TaskToBeAssignedRoleId = ; // corporate users who aren't assigned should have this blank.

            //    task.TaskDueDateCalcField = ; // if not empty, this should be a field per 2.1.2
            //    task.TaskDueDateCalcField = ; //? also, this must be today or in the future, or the due date must be (not sure about this)
            //    task.TaskDueDateLocked = ; //? See bottom of page 9.

            //    //! a newly created Task should have status active, and task owner = task creator.
        }
        //! [Test]
        public void NotInSpecButSeemReasonable()
        {

            throw new NotImplementedException();
            //! implement

            //var brokerID = m_loUserPrincipal.BrokerId;
            //var task = Task.Retrieve(brokerID, condTaskID);

            //task.BrokerId = new Guid(); //? this shouldn't be allowed.
            //task.LoanId = new Guid();   //? this shouldn't be allowed.

            //task.isCalculating = false; //? what is this for

            //task.TaskCreatedDate = ; //? why modifiable
            //task.TaskCreatedByUserId = ; //? why modifiable
            //task.AssignedUserFullName = ; //? could be different from
            //task.BorrowerNmCached = ; //? what's this for?
            //task.ClosingUserFullName = ; //? should match closer id?
            //task.CondDeletedByUserNm = ; //? what's it mean?
            //task.CondDeletedDate = ;
            //task.CondIsDeleted = ;
            //task.CondRank = ; //?
            //task.DefaultPermissionForCondition = ;//?

            //task.EnforcePermissions = ;
            //task.HasEmailEntry = ;
            //task.IsTemplate = ;
            //task.LoanNumCached = ;
            //task.MostRecentEmailContent = ;
            //task.OwnerFullName = ;
            //task.PermissionLevelAppliesToConditions = ;
            //task.PermissionLevelDescription = ;
            //task.PermissionLevelName = ;
            //task.PermissionProcessor = ;

            //task.UserPermissionLevel = ;

            //task.TaskAssignedUserId = ;
            //task.TaskClosedDate = ;
            //task.TaskClosedUserId = ;
            //task.TaskCreatedByUserId = ;
            //task.TaskCreatedDate = ;
            //task.TaskDueDate = ;
            //task.TaskDueDateCalcDays = ;
            //task.TaskDueDateCalcField = ;
            //task.TaskDueDateLocked = ;
            //task.TaskEmailSubject = ;
            //task.TaskFollowUpDate = ;
            //task.TaskFromAddress = ;
            //task.TaskHistoryHtml = ;
            //task.TaskId = ;
            //task.TaskIsCondition = ;
            //task.TaskLastModifiedDate = ;
            //task.TaskOwnerUserId = ;
            //task.TaskOwnerUserIdForPermissionCheck = ;
            //task.TaskPermissionLevelId = ;
            //task.TaskResolvedUserId = ;
            //task.TaskRowVersion = ;
            //task.TaskSingleDueDate = ;  //? related to Due Date how?
            //task.TaskStatus = ;
            //task.TaskSubject = ;
            //task.TaskToBeAssignedRoleId = ;
            //task.TaskToBeOwnerRoleId = ;
            //task.TempIsMigration = ;
        }

        //? [Test]
        public void CreateTaskWithBadFields()
        {
            var brokerID = m_loUserPrincipal.BrokerId;


            Task task = Task.Create(m_loanID, brokerID);
            task.TaskSubject = "non empty subject.";
            task.TaskDueDate = DateTime.Now.AddDays(5);

            //? newly created tasks should have status = active.
            task.TaskStatus = E_TaskStatus.Resolved;
            AssertCBaseException(task);
            task.TaskStatus = E_TaskStatus.Active;

            // newly created tasks should have creator = owner
            //! update.
            //task.TaskCreatedByUserId == task.TaskOwnerUserId;

        }
        //[Test]
        public void AssignedUserIsAllowed()
        {
            //! update.
            // green light.
            var brokerID = m_loUserPrincipal.BrokerId;
            var task = Task.Retrieve(brokerID, taskID);

            // red light.

        }
        #endregion

        #region TestsToBeDeleted
        [Test]
        public void GetAssignableUsersList()
        {
            // copied much from RoleAndUserPicker.aspx.cs
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add("UserId", typeof(string));
            dataTable.Columns.Add("FullName", typeof(string));
            dataTable.Columns.Add("Type", typeof(string));
            dataTable.Columns.Add("Roles", typeof(string));
            var brokerID = m_loUserPrincipal.BrokerId;
            var user = m_loUserPrincipal;
            var LoanID = m_loanID;
            var IsPipelineQuery = true;
            var NameFilter = "";
            var NoPML = false;
            var LastFilter = "";
            var SkipSecurityCheck = true;
            var IsTemplate = false;
            var PermissionLevelID = -1; // may want to change b/t -1, 1, and 2.

            CPageData loanData = null;
            LoanValueEvaluator valueEvaluator = null;
            List<SqlParameter> pA = new List<SqlParameter>();
            if (!IsPipelineQuery)
            {
                valueEvaluator = new LoanValueEvaluator(brokerID, LoanID, WorkflowOperations.ReadLoanOrTemplate, WorkflowOperations.WriteLoanOrTemplate);
                loanData = CPageData.CreateUsingSmartDependency(LoanID, typeof(RoleAndUserPicker));
                loanData.InitLoad(); //! use this instead of InitSave (in some places.)
                pA.Add(new SqlParameter("@BranchId", loanData.sBranchId));
                pA.Add(new SqlParameter("@LoanId", loanData.sLId));
            }

            pA.Add(new SqlParameter("@BrokerId", brokerID));
            pA.Add(new SqlParameter("@NameFilter", NameFilter));
            pA.Add(new SqlParameter("@NoPML", NoPML));
            pA.Add(new SqlParameter("@IsPipelineQuery", IsPipelineQuery));
            if (!String.IsNullOrEmpty(LastFilter))
            {
                pA.Add(new SqlParameter("@LastFilter", LastFilter));
            }

            using (IDataReader dr = StoredProcedureHelper.ExecuteReader(brokerID, "TASK_GetUsersForAssignment", pA))
            {
                while (dr.Read())
                {
                    Guid employeeId = new Guid(dr["EmployeeId"].ToString());
                    Guid userId = new Guid(dr["UserId"].ToString());
                    string userName = dr["FullName"].ToString();
                    string type = dr["Type"].ToString();

                    Guid branchId = (Guid)dr["BranchId"];
                    Guid pmlBrokerId = (Guid)dr["PmlBrokerId"];
                    E_PmlLoanLevelAccess pmlLevelAccess = (E_PmlLoanLevelAccess)dr["PmlLevelAccess"];
                    string permissions = (string)dr["Permissions"];
                    E_ApplicationT applicationType = type == "B" ? E_ApplicationT.LendersOffice : E_ApplicationT.PriceMyLoan;

                    ExecutingEnginePrincipal principal = ExecutingEnginePrincipal.CreateFrom(brokerID, branchId, pmlBrokerId, employeeId, userId, pmlLevelAccess, applicationType, permissions, this.m_loUserPrincipal.Type);
                    //AbstractUserPrincipal principal = PrincipalFactory.RetrievePrincipalForUser(userId, type) as AbstractUserPrincipal;

                    if (!SkipSecurityCheck)
                    {
                        if (principal == null)
                        {
                            continue;
                        }

                        // Workflow config check

                        valueEvaluator.SetEvaluatingPrincipal(principal);
                        if (!LendingQBExecutingEngine.CanPerform(WorkflowOperations.ReadLoanOrTemplate, valueEvaluator))
                        {
                            continue;
                        }

                        // Permission level check
                        if (PermissionLevelID != -1)
                        {
                            PermissionLevel selectedPermission = PermissionLevel.Retrieve(user.BrokerId, PermissionLevelID);
                            List<Guid> UserGroups = new List<Guid>();
                            foreach (Group ugroup in GroupDB.ListInclusiveGroupForEmployee(principal.BrokerId, principal.EmployeeId))
                            {
                                UserGroups.Add(ugroup.Id);
                            }
                            List<E_RoleT> Roles = new List<E_RoleT>();
                            foreach (E_RoleT role in principal.GetRoles())
                            {
                                Roles.Add(role);
                            }
                            if (!selectedPermission.IsRoleOrGroupInLevel(E_PermissionLevel.WorkOn, Roles, UserGroups))
                            {
                                continue;
                            }
                        }
                    }

                    DataRow row = dataTable.NewRow();
                    row["UserId"] = userId;
                    row["FullName"] = userName;
                    row["Type"] = String.Compare(type, "B", true) == 0 ? "Employee" : "PML";

                    if (!IsPipelineQuery)
                    {
                        string roles = "";
                        if (IsTemplate)
                        {
                            StringBuilder roleList = new StringBuilder();
                            foreach (var role in principal.GetRoles())
                            {
                                roleList.Append(Role.GetRoleDescription(role));
                                roleList.Append(",");
                            }
                            roles = roleList.ToString();
                        }
                        else
                        {
                            // Appending loan assignments to users who passed the checks            
                            if (employeeId == loanData.sEmployeeProcessorId)
                            {
                                roles += "Processor,";
                            }
                            if (employeeId == loanData.sEmployeeBrokerProcessorId)
                            {
                                roles += "Broker Processor,";
                            }
                            if (employeeId == loanData.sEmployeeCallCenterAgentId)
                            {
                                roles += "Call Center Agent,";
                            }
                            if (employeeId == loanData.sEmployeeCloserId)
                            {
                                roles += "Closer,";
                            }
                            if (employeeId == loanData.sEmployeeLenderAccExecId)
                            {
                                roles += "Lender Account Executive,";
                            }
                            if (employeeId == loanData.sEmployeeLoanOpenerId)
                            {
                                roles += "Loan Opener,";
                            }
                            if (employeeId == loanData.sEmployeeLoanRepId)
                            {
                                roles += "Loan Officer,";
                            }
                            if (employeeId == loanData.sEmployeeLockDeskId)
                            {
                                roles += "Lock Desk,";
                            }
                            if (employeeId == loanData.sEmployeeManagerId)
                            {
                                roles += "Manager,";
                            }
                            if (employeeId == loanData.sEmployeeRealEstateAgentId)
                            {
                                roles += "Real Estate Agent,";
                            }
                            if (employeeId == loanData.sEmployeeUnderwriterId)
                            {
                                roles += "Underwriter,";
                            }
                        }

                        if (roles.Length > 0)
                        {
                            roles = Tools.SortCommaDelimitedString(roles.Substring(0, roles.Length - 1));
                        }
                        row["Roles"] = roles;
                    }

                    dataTable.Rows.Add(row);
                }
            }
            //foreach (DataRow row in dataTable.Rows)
            //{
            //    Console.WriteLine(row["UserId"] as string + "  " + (row["Type"] as string)[0] + "  " + row["FullName"]);
            //}
        }
        #endregion

        #region Tests
        [Test]
        public void SubjectEmpty()
        {
            var task = Task.Retrieve(m_loUserPrincipal.BrokerId, taskID);
            task.TaskSubject = "";
            MLAssert.Throws<CBaseException>(new Action(() => task.Save(false)));
        }
        [Test]
        public void DueDateEmpty()
        {
            var task = Task.Retrieve(m_loUserPrincipal.BrokerId, taskID);
            task.TaskDueDate = null;
            task.TaskDueDateCalcField = "";
            MLAssert.Throws<CBaseException>(new Action(() => task.Save(false)));
        }
        [Test]
        public void BorrowerNameFromLoanFile()
        {
            var brokerID = m_loUserPrincipal.BrokerId;

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(m_loanID, typeof(TaskStateTest));

            //dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.InitLoad(m_loUserPrincipal);
            Task task = Task.Retrieve(brokerID, taskID);

            var dataApp = dataLoan.GetAppData(0);
            Assert.AreEqual(task.BorrowerNmCached, dataApp.aBNm);
        }
        [Test]
        public void LoanNumberFromLoanFile()
        {
            var brokerID = m_loUserPrincipal.BrokerId;

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(m_loanID, typeof(TaskStateTest));

            dataLoan.InitLoad(m_loUserPrincipal);
            
            Task task = Task.Retrieve(brokerID, taskID);
            Assert.That(m_loanID.Equals(task.LoanId));
        }
        //[Test] //! finish this.
        public void DueDateFromLoanFileWhenCalculated()
        {
            var brokerID = m_loUserPrincipal.BrokerId;
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(m_loanID, typeof(TaskStateTest));
            //dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.InitLoad(m_loUserPrincipal);
            Task task = Task.Retrieve(brokerID, taskID); //! this should be a calculated date task.
            //! add something here to compare due date form loan and calculated duedate.
            throw new NotImplementedException();
        }
        //[Test]
        public void AuditEventCreatedForChanges()
        {
            //! unfortunately this will randomly fail or succeed, and thus is somewhat of a pointless test.
            //? perhaps instead loop through the fields that need to cause changes when changed.
            // (instead of randomly changing them.)
            var task = Task.Retrieve(m_loUserPrincipal.BrokerId, taskID);
            var PIs = task.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
            var randomGenerator = new Random();
            var taskDelta = (TaskChangeProfile) task.GetType().GetField("m_taskDelta", 
                BindingFlags.NonPublic | BindingFlags.Instance).GetValue(task);
            Assert.AreEqual(0, taskDelta.ModifiedFields.Count);
            while (true)
            {
                try
                {
                    uint randPIind = (uint)(randomGenerator.NextDouble() * PIs.Length);
                    var randPI = PIs[randPIind];
                    var oldValue = randPI.GetValue(task, null);
                    if (oldValue == null)
                    {
                        continue;
                    }
                    var newValue = Activator.CreateInstance(oldValue.GetType());
                    if (oldValue.Equals(newValue))
                    {
                        continue;
                    }
                    randPI.SetValue(task, newValue, null);
                }
                catch (Exception)
                {
                    continue;
                }
                Assert.AreNotEqual(0, taskDelta.ModifiedFields.Count);
                break;
            }
        }
        //[Test] //! can't test as only have one test broker account.
        //public void AssignedUserShouldAtSameBroker()
        //{
        //    var brokerID = m_loUserPrincipal.BrokerId;
        //    Console.WriteLine(brokerID);
        //    var task = Task.Retrieve(brokerID, taskID);
        //    var oldAssignedUserID = task.TaskAssignedUserId;
        //    // Find Another Broker
        //    var otherBrokerID = new Guid("839127ef-72d1-4873-bfdf-18a23416b146"); // ThinhTrial
        //    var otherUserID = new Guid(
        //    // Find an active user within that broker
        //    //Guid newUserID;
        //    throw new NotImplementedException();
        //    //task.TaskAssignedUserId = newUserID;
        //    //MLAssert.Throws<CBaseException>(() => task.Save(false));
        //    //task.TaskAssignedUserId = oldAssignedUserID;
        //    //task.Save(false);
        //}

        //[Test]
        public void DissallowAssignmentOfDisabledPMLOrInactiveLOUsers()
        {
            var brokerID = m_loUserPrincipal.BrokerId;
            var task = Task.Retrieve(brokerID, taskID);
            var oldTaskID = task.TaskAssignedUserId;
            throw new NotImplementedException();
            //task.TaskAssignedUserId = new Guid("89ee8bd8-5578-41b8-90f9-3f1b046bf9d6"); // inactive employee user id.
            //MLAssert.Throws<Exception>(() => task.Save(false)); 
            //task.TaskAssignedUserId = oldTaskID;
            //task.Save(false);

        }
        //[Test]
        public void DueDateCalculationSkipsWeekendsAndHolidays()
        {
            throw new NotImplementedException();
        }
        //[Test]
        public void OwnerAndOrAssignedNameShouldBeSetWhenAssigned()
        {
            throw new NotImplementedException();
        }
        [Test]
        public void ResolvingClosingOrReactivatingShouldSetStatusAppropriately()
        {
            var brokerID = m_loUserPrincipal.BrokerId;
            
            var task = Task.Retrieve(brokerID, taskID);
            task.Reactivate(); //? for some reason this is resolved to begin with.
            Assert.AreEqual(E_TaskStatus.Active, task.TaskStatus);

            task = Task.Retrieve(brokerID, taskID); // don't need these if reset the m_taskDelta on save.
            task.Resolve();
            Assert.AreEqual(E_TaskStatus.Resolved, task.TaskStatus);

            task = Task.Retrieve(brokerID, taskID);
            task.Close();
            Assert.AreEqual(E_TaskStatus.Closed, task.TaskStatus);

            task = Task.Retrieve(brokerID, taskID);
            task.Reactivate();
            Assert.AreEqual(E_TaskStatus.Active, task.TaskStatus);


            task = Task.Retrieve(brokerID, taskID);
            task.ResolveAndClose();
            Assert.AreEqual(E_TaskStatus.Closed, task.TaskStatus);


            task = Task.Retrieve(brokerID, taskID);
            task.Reactivate();
            Assert.AreEqual(E_TaskStatus.Active, task.TaskStatus);
        }
       //[Test]
        public void OwnershipCannotChangeForAClosedTask()
        {
            throw new NotImplementedException();
        //    var brokerID = m_loUserPrincipal.BrokerId;
        //    var task = Task.Retrieve(brokerID, closedTaskID);
        //    task.TaskOwnerUserId = m_loUserPrincipal.
        //    MLAssert.Throws<CBaseException>(() => task.Save(false));
        }
        //[Test]
        public void ResolvingShouldAssignOwner()
        {
            throw new NotImplementedException();
        }
        //[Test]
        public void ResolvingUserShouldGetTaskWhenReactivated()
        {
            throw new NotImplementedException();
        }
        //[Test]
        public void WhenPMLUserAccessTaskDisallowInternalSectionViewing()
        {
            throw new NotImplementedException();
        }
        //[Test]
        public void WhenReactivatingResolvingUserShouldGetTheTask()
        {
            throw new NotImplementedException();
        }
        //[Test]
        public void IsConditionCannotGoFromTrueToFalse()
        {
            var brokerID = m_loUserPrincipal.BrokerId;
            var task = Task.Retrieve(brokerID, condTaskID);

            // once a condition, always a condition.
            task.TaskIsCondition = false;
            MLAssert.Throws<CBaseException>(() => task.Save(false));
        }        
        [Test]
        public void PMLUserCannotSetIsCondition()
        {
            var pmlUserP = LoginTools.LoginAs(TestAccountType.Pml_User0);
            var brokerID = pmlUserP.BrokerId;
            Task task = Task.Retrieve(brokerID, pmlTaskID);

            task.TaskIsCondition = true; // was false previously.
            MLAssert.Throws<TaskPermissionException>(() => task.Save(false));
            LoginTools.LoginAs(TestAccountType.LoTest001);
        }
        //[Test]
        public void HiddenConditionCannotBeAssignedToPmlUser()
        {
            throw new NotImplementedException();
        }
        //? [Test]
        public void CalculatedDateShouldBeTodayOrLater()
        {
            throw new NotImplementedException();
        }
        //? [Test]
        public void CanMultipleDueDatePropertiesContradict()
        {
            throw new NotImplementedException();
        }
        //[Test]
        public void TheDateFieldCanOnlyBeOneOfThoseSpecd()
        {
            throw new NotImplementedException();
        }
        //[Test]
        public void MakeSureTaskIDIsBase36()
        {
            throw new NotImplementedException();
        }
        //[Test]
        public void UpdateBorrowerNameOrLoanNumberAnytimeEitherIsModified()
        {
            throw new NotImplementedException();
        }
        //[Test]
        public void DontAllowAssignmentIfStatusIsAndWasResolvedOrClosed()
        {
            throw new NotImplementedException();
        }
        //[Test]
        public void LikeAboveButAlsoDisallowAssignmentProgramaticallyOnStatusChangeIfDoesntAssignToRightPersonOrRole()
        {
            throw new NotImplementedException();
        }

        [Test]
        public void DontAllowEmailingUnlessStatusIsActive()
        {
            var brokerID = m_loUserPrincipal.BrokerId;
            var task = Task.Retrieve(brokerID, taskID);
            CBaseEmail email = new CBaseEmail(ConstAppDavid.SystemBrokerGuid);
            email.From = task.TaskFromAddress;
            email.To = "scottk@meridianlink.com";
            email.CCRecipient = "";
            email.Subject = "subj";
            email.Message = "message body";
            MLAssert.NotThrows<Exception>(() => task.SaveAndSendEmail(task.TaskRowVersion, email));
            task.Resolve();
            task = Task.Retrieve(brokerID, taskID);
            MLAssert.Throws<Exception>(() => task.SaveAndSendEmail(task.TaskRowVersion, email));
            task.Reactivate();
            
        }
        //[Test]
        public void DontAllowResolvingUnlessStatusIsActive()
        {
            throw new NotImplementedException();
        }
        //[Test]
        public void DontAllowClosingUnlessUserHasPermission()
        {
            throw new NotImplementedException();
        }
        //?[Test]
        public void DontAllowClosingUnlessStatusWasResolved()
        {
            throw new NotImplementedException();
        }
        //[Test]
        public void DontAllowTakingOwnershipUnlessUserHasPermissionToDoSo()
        {
            throw new NotImplementedException();
        }
        //[Test]
        public void DontAllowClosingUnlessUserCanClose()
        {
            throw new NotImplementedException();
        }
        [Test]
        public void DontAllowIsConditionToBeEditedIfResolved()
        {
            var brokerID = m_loUserPrincipal.BrokerId;
            var task = Task.Retrieve(brokerID, taskID);

            task.Resolve();
            task = Task.Retrieve(brokerID, taskID);

            task.TaskIsCondition = true;
            MLAssert.Throws<Exception>(() => task.Save(false));
            task.TaskIsCondition = false;
            
            task.Reactivate();
            task = Task.Retrieve(brokerID, condTaskID);
            
            task.Resolve();
            task = Task.Retrieve(brokerID, condTaskID);

            task.TaskIsCondition = false;
            MLAssert.Throws<Exception>(() => task.Save(false)); // throws exception but for other reasons.
            task.TaskIsCondition = true;
            task.Reactivate();
        }
        [Test]
        public void ConditionCategoryIDMustBeValidForConditions()
        {
            var brokerID = m_loUserPrincipal.BrokerId;
            var task = Task.Retrieve(brokerID, condTaskID);
            var categories = ConditionCategory.GetCategories(brokerID);
            
            MLAssert.Throws<NullReferenceException>(()=>task.CondCategoryId = -1);
        }
        [Test]
        public void ConditionCategoryMustNotExistForNonConditions()
        {
            var brokerID = m_loUserPrincipal.BrokerId;
            var task = Task.Retrieve(brokerID, taskID);
            var categories = ConditionCategory.GetCategories(brokerID);
            var catID = categories.First().Id;

            task.TaskIsCondition = false;
            
            task.CondCategoryId = categories.First().Id;
            MLAssert.Throws<CBaseException>(new Action(() => task.Save(false)));

            MLAssert.Throws<NullReferenceException>(new Action(() => task.CondCategoryId = -1));
        }
        [Test]
        public void ConditionCategoryMustExistForConditions()
        {
            var brokerID = m_loUserPrincipal.BrokerId;
            var task = Task.Retrieve(brokerID, condTaskID);
            var categories = ConditionCategory.GetCategories(brokerID);
            var linq = from c in categories
                       where string.Compare(c.Category, task.CondCategoryId_rep, true) == 0
                       select c;
            Assert.That(linq.Count() == 1);
        }
        [Test]
        public void TaskMayNotBeHiddenConditionUnlessItsACondition()
        {
            var brokerID = m_loUserPrincipal.BrokerId;
            var task = Task.Retrieve(brokerID, taskID);

            // red light.
            task.TaskIsCondition = false;
            task.CondIsHidden = true;
            MLAssert.Throws<CBaseException>(new Action(() => task.Save(false)));
            task.CondIsHidden = false;

            // green light.
            task = Task.Retrieve(brokerID, condTaskID);
            var wasHidden = task.CondIsHidden;
            task.CondIsHidden = true;
            MLAssert.NotThrows<Exception>(()=>task.Save(false));
            task.CondIsHidden = wasHidden;
            MLAssert.NotThrows<Exception>(()=>task.Save(false));
        }
        [Test]
        public void TaskMayNotHaveInternalNotesUnlessItsACondition()
        {
            var brokerID = m_loUserPrincipal.BrokerId;
            var task = Task.Retrieve(brokerID, taskID);

            // red light.
            task.TaskIsCondition = false;
            task.CondInternalNotes = " internal notes blah blah blah";
            MLAssert.Throws<CBaseException>(new Action(() => task.Save(false)));
            task.CondInternalNotes = "";

            // green light.
            task = Task.Retrieve(brokerID, condTaskID);
            var oldIntNotes = task.CondInternalNotes;
            task.CondInternalNotes = " internal notes blah blah blah";
            MLAssert.NotThrows<Exception>(() => task.Save(false));
            task.CondInternalNotes = oldIntNotes;
            MLAssert.NotThrows<Exception>(() => task.Save(false));
        }
        //[Test]
        public void CertainUsersMayNotSetATaskToBeACondition()
        {
            throw new NotImplementedException();
        }
        //[Test] //? What is the "date done" field
        public void SetTheDateDoneToBeTheDateThatTheLoanWasClosed()
        {
            throw new NotImplementedException();
        }
        //[Test] //? What is the "cleared by" field.
        public void PopulateTheClearedByFieldWithTheNameOfTheUserWhoClosedIt()
        {
            throw new NotImplementedException();
        }
        //[Test]
        public void DontEvenAllowPMLUsersToEditInternalNotesForConditions()
        {
            throw new NotImplementedException();
        }
        //[Test]
        public void DontAllowViewingOfInternalNotesForPMLUsersAndConditions()
        {
            throw new NotImplementedException();
        }
        //[Test]
        public void ErrorOnLoanSaveIfConditionAndNoCondDescField()
        {
            throw new NotImplementedException();
        }
        //[Test]
        public void ErrorOnLoanSaveIfConditionAndNoCategory()
        {
            throw new NotImplementedException();
        }
        //[Test]
        public void OnLoanSaveIfConditionAndDateDoneIsPastClosedDateShouldBeDateDone()
        {
            throw new NotImplementedException();
        }
        //[Test]
        public void OnLoanSaveIfConditionAndDateDoneNotPastStatusShouldBeActive()
        {
            throw new NotImplementedException();
        }
        //[Test]
        public void OnLoanSaveREquiredAssignedToIsRoleOrUsername()
        {
            throw new NotImplementedException();
        }
        //?[Test]
        public void ClosedTasksCannotBeEdited()
        {
            var brokerID = m_loUserPrincipal.BrokerId;
            var task = Task.Retrieve(brokerID, closedTaskID);
            task.TaskSubject = "here's a new subject for you...";
            MLAssert.Throws<CBaseException>(new Action(() => task.Save(false)));
            //? add more possible edits?  need to systematically make all possible edits?
        }
        //[Test]
        public void ResolvedTasksMustHaveAssignedUserIsOwner()
        {
            throw new NotImplementedException();
        }
        //[Test] //?
        public void NewlyCreatedTasksShouldHaveActiveStatus()
        {
            throw new NotImplementedException();
        }
        //[Test] //?
        public void TaskDueDateAndDueDateCalcFieldCantBothBeFilledOut()
        {
            var task = Task.Retrieve(m_loUserPrincipal.BrokerId, taskID);
            task.TaskDueDate = DateTime.Now.AddDays(5);
            task.TaskDueDateCalcField = Task.ListValidDateFields().First();
            MLAssert.Throws<CBaseException>(new Action(() => task.Save(false)));
        }
        //[Test]
        public void TaksDueDateOrCalcFieldMustBeFilledOut()
        {
            throw new NotImplementedException();
        }
        //[Test]
        public void TaskDueDateCalcFieldMustBeFilledOutValidlyIfFilledOut()
        {
            var task = Task.Retrieve(m_loUserPrincipal.BrokerId, taskID);
            task.TaskDueDateCalcField = "this clearly can't be a field value.";
            MLAssert.Throws<CBaseException>(new Action(() => task.Save(false)));
        }
        #endregion

        #region Helper Fxns
        private bool TaskExists(Guid brokerID, string taskID)
        {
            try
            {
                Task.Retrieve(brokerID, taskID);
                return true;
            }
            catch (TaskNotFoundException)
            {
                return false;
            }
        }
        
        private void AssertCBaseException(Task task)
        {
            MLAssert.Throws<CBaseException>(new Action(() => task.Save(false)));
        }

        private IEnumerable<PermissionLevel> GetPermissionLevels(Guid brokerID, bool forceConditions)
        {
            // Taken from TaskPermissionPicker.
            List<Guid> UserGroups = new List<Guid>();
            foreach (Group ugroup in GroupDB.ListInclusiveGroupForEmployee(m_loUserPrincipal.BrokerId, m_loUserPrincipal.EmployeeId))
            {
                UserGroups.Add(ugroup.Id);
            }
            List<E_RoleT> Roles = new List<E_RoleT>();
            foreach (E_RoleT role in m_loUserPrincipal.GetRoles())
            {
                Roles.Add(role);
            }

            var permissionLevels = PermissionLevel.RetrieveManageLevelsForUser(brokerID, Roles, UserGroups);
            if (forceConditions)
            {
                permissionLevels = permissionLevels.Where(p => p.IsAppliesToConditions);
            }
            return permissionLevels;
        }
        #endregion

    }

}
