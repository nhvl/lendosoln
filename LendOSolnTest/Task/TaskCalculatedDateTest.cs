﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using DataAccess;
using LendOSolnTest.Common;

namespace LendOSolnTest.TaskTest
{
    [TestFixture]
    [Category(CategoryConstants.RunBeforePullRequest)]
    public class TaskCalculatedDateTest
    {
        [Test]
        public void TestAllDatesAreValid()
        {
            // 6/17/2011 dd - This test will makes sure all the date available in Due Date calculation are valid.
            List<string> fieldIdErrorList = new List<string>();

            foreach (var field in LendersOffice.ObjLib.Task.Task.ListValidDateFields())
            {
                E_PageDataFieldType fieldType;
                if (PageDataUtilities.GetFieldType(field, out fieldType) == true)
                {
                    if (fieldType != E_PageDataFieldType.DateTime)
                    {
                        fieldIdErrorList.Add(field + " is not a date type");
                    }
                }
                else
                {
                    fieldIdErrorList.Add(field + " is missing from DataLayer");
                }
            }
            Assert.AreEqual(0, fieldIdErrorList.Count, "Error List: " + string.Join(Environment.NewLine, fieldIdErrorList.ToArray()));
        }
    }
}
