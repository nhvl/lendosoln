﻿namespace LendOSolnTest.TaskTest
{
    using System;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Audit;
    using LendersOffice.ObjLib.Task;
    using LendersOffice.Security;
    using LendOSolnTest.Common;
    using NUnit.Framework;
    using System.Data.SqlClient;
    using LendersOffice.Constants;
    using System.Collections.Generic;

    [TestFixture]
    [Category(CategoryConstants.RunBeforePullRequest)]
    public class TaskResolutionSettingsTest
    {
        private AbstractUserPrincipal cachedPrincipal;

        private Guid loanId = Guid.Empty;

        private string taskId = "";

        private AbstractUserPrincipal principal
        {
            get
            {
                if (this.cachedPrincipal == null)
                {
                    this.cachedPrincipal = LoginTools.LoginAs(TestAccountType.LoTest001);
                }
                return this.cachedPrincipal;
            }
        }

        [TestFixtureSetUp]
        public void Setup()
        {
            var fileCreator = CLoanFileCreator.GetCreator(this.principal, E_LoanCreationSource.UserCreateFromBlank);
            this.loanId = fileCreator.CreateBlankLoanFile();

            // 6/4/2014 gf - Set to retail to avoid disclosure trigger that will mess with 
            // the dates.
            var dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                this.loanId,
                typeof(TaskResolutionSettingsTest));
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sIsBranchActAsOriginatorForFileTri = E_TriState.Yes;
            dataLoan.sIsBranchActAsLenderForFileTri = E_TriState.Yes;
            dataLoan.Save();

            var brokerID = this.principal.BrokerId;
            var task = Task.Create(this.loanId, brokerID);
            task.TaskSubject = "Task";
            task.TaskDueDate = DateTime.Now.AddDays(2);
            task.TaskToBeAssignedRoleId = CEmployeeFields.s_LoanRepRoleId;
            task.TaskToBeOwnerRoleId = CEmployeeFields.s_LoanRepRoleId;
            task.Save(false);
            this.taskId = task.TaskId;
        }

        [TestFixtureTearDown]
        public void TearDown()
        {
            if (this.loanId != Guid.Empty)
            {
                Tools.DeclareLoanFileInvalid(this.principal, this.loanId, false, false);
                this.loanId = Guid.Empty;
            }
        }

        private static HashSet<string> nonNullDateFields = new HashSet<string>()
        {
            "sCreatedD",
            "sOpenedD"
        };

        /// <summary>
        /// Tests that all fields returned by the Task class to be used for the 
        /// resolution date setter will set the loan field to the current date 
        /// on task resolution.
        /// </summary>
        [Test]
        public void TestResolutionDateSetter()
        {
            var possibleDates = Task.ListValidResolutionDateSetterParameters(false);
            var baseErrorMessage = "If this field has been removed from the data " +
                "layer or changed to a calculated field, please add it to the " +
                "blacklist at ConstApp.TaskResolutionDateSetterFieldIdBlacklist.";

            var resolutionDateSetter = new ResolutionDateSetter(this.loanId, this.principal);

            var task = Task.Retrieve(this.principal.BrokerId, this.taskId);
            task.EnforcePermissions = false;
            task.ResolutionDateSetter = resolutionDateSetter;

            foreach (var dateParameter in possibleDates)
            {
                task.ResolutionDateSetterFieldId = dateParameter.Id;
                task.SetResolutionDateField();
            }

            // Set the fields and re-load the loan.
            resolutionDateSetter.SetFieldsToCurrentDateAndTime();

            var dataLoan = new CFullAccessPageData(this.loanId, possibleDates.Select(p => p.Id));
            dataLoan.InitLoad();

            // 6/3/2014 gf - There are some fields which will always return null if this setting isn't enabled.
            dataLoan.BrokerDB.TempOptionXmlContent += "<option name=\"EnableAdditionalSection1000CustomFees\" value=\"true\" />";

            var now = DateTime.Now;
            var invalidFieldIds = new List<string>();

            foreach (var dateParameter in possibleDates)
            {
                var stringValue = PageDataUtilities.GetValue(dataLoan, dataLoan.GetAppData(0), dateParameter.Id);
                var dateTimeWrapper = CDateTime.Create(stringValue, dataLoan.m_convertLos);

                if (!dateTimeWrapper.IsValid)
                {
                    invalidFieldIds.Add(dateParameter.Id);
                    continue;
                }

                var dateTime = dateTimeWrapper.DateTimeForComputation;

                bool sameDay = dateTime.Day == now.Day;
                bool sameMonth = dateTime.Month == now.Month;
                bool sameYear = dateTime.Year == now.Year;

                if (!sameDay || !sameMonth || !sameYear)
                {
                    invalidFieldIds.Add(dateParameter.Id);
                }
            }

            var errorMessage = string.Format(
                "The day, month, and year should be today for field(s): {0}. {1}",
                string.Join(", ", invalidFieldIds.ToArray()),
                baseErrorMessage);

            Assert.That(invalidFieldIds.Count == 0, errorMessage);
        }
    }
}
