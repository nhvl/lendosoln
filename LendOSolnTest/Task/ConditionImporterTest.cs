﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using LendersOffice.Security;
using LendOSolnTest.Common;
using LendersOffice.Audit;
using LendersOffice.Constants;
using LendersOffice.ObjLib.Task;
using LendersOffice.Reminders;
using NUnit.Framework;

namespace LendOSolnTest.TaskTest
{
    [TestFixture]
    public class ConditionImporterTest
    {
        private AbstractUserPrincipal m_currentPrincipal = LoginTools.LoginAs(TestAccountType.LoTest001);

        [Test]
        public void TestConditionImporter()
        {
            CLoanFileCreator fileCreator = CLoanFileCreator.GetCreator(m_currentPrincipal, E_LoanCreationSource.UserCreateFromBlank);
            Guid m_blankLoanId = fileCreator.CreateBlankLoanFile();
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(m_blankLoanId, typeof(ConditionImporterTest));

            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            CAppData dataAppForTemplate = dataLoan.GetAppData(0);
            dataAppForTemplate.aBFirstNm = "Bob";
            dataAppForTemplate.aBLastNm = "Testcase";
            
dataAppForTemplate.aBMaritalStatT = E_aBMaritalStatT.Married; // Explicitly set because the default, LeaveBlank, has a value of 3, not 0.
                 
            IConditionImporter importer = dataLoan.GetConditionImporter("Testing");

            Tools.LogInfo(dataLoan.sPrimaryNm);
            string desc = Guid.NewGuid().ToString();
            string cat = "TEST";
            ConditionDesc regularCondition = new ConditionDesc();
            regularCondition.Category = cat;
            regularCondition.Description = desc;
            regularCondition.IsDone = false;
            Tools.LogError(dataLoan.sLNm);

            importer.MergeIn(regularCondition);
            dataLoan.Save();

            List<Task> tasks = Task.GetAllConditionsByLoanId(m_currentPrincipal.BrokerId, m_blankLoanId);
            Assert.That(tasks.Count, Is.EqualTo(1));

            Task task = tasks[0];

            Assert.That(task.TaskSubject, Is.EqualTo(desc));
            Assert.That(task.CondCategoryId_rep, Is.EqualTo(cat));
            Assert.That(task.TaskDueDateLocked, Is.False);
            Assert.That(task.TaskDueDateCalcField, Is.EqualTo("sEstCloseD"));
            Assert.That(task.TaskAssignedUserId, Is.EqualTo(m_currentPrincipal.UserId));

            dataLoan = CPageData.CreateUsingSmartDependency(m_blankLoanId, typeof(ConditionImporterTest));
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            importer = dataLoan.GetConditionImporter("Testing");

            regularCondition.IsDone = true;
            regularCondition.DateDone = DateTime.Today.AddDays(-1).ToShortDateString();
            regularCondition.CondCompletedByUserNm = "Antonios TEST (TEST)";
            importer.MergeIn(regularCondition);
            dataLoan.Save();


            tasks = Task.GetAllConditionsByLoanId(m_currentPrincipal.BrokerId, m_blankLoanId);
            Assert.That(tasks.Count, Is.EqualTo(1));

            task = tasks[0];


            Assert.That(task.TaskSubject, Is.EqualTo(desc));
            Assert.That(task.CondCategoryId_rep, Is.EqualTo(cat));
            Assert.That(task.TaskDueDateLocked, Is.False);
            Assert.That(task.TaskDueDateCalcField, Is.EqualTo("sEstCloseD"));
            Assert.That(task.TaskAssignedUserId, Is.EqualTo(m_currentPrincipal.UserId));
            Assert.That(task.TaskStatus, Is.EqualTo(E_TaskStatus.Closed));
            Assert.That(task.ClosingUserFullName, Is.EqualTo(regularCondition.CondCompletedByUserNm));
            Assert.That(task.TaskClosedDate.Value.ToShortDateString(), Is.EqualTo(regularCondition.DateDone));

        }
    }
}
