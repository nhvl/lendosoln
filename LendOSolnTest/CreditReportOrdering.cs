// Antonio Valencia
using System;
using NUnit.Framework;
using LendersOffice.CreditReport;
using LendersOffice.CreditReport.Mcl;
using LendersOffice.Constants; 
using CommonLib; 
using System.Xml; 
using DataAccess;
using LendOSolnTest.Common;


namespace LendOSolnTest.Credit 
{

	public class CreditTestingUtils  
	{
        public static ICreditReport LoadReportFromTestData(string name)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(TestDataRootFolder.Location + name);
            Assert.That(doc, Is.Not.Null);
            ICreditReport report = CreditReportUtilities.GetReportFrom(doc);
            //Make sure the report has been stipped of sensitive data. This is minimal, reports from test credit page should be okay. 
            Assert.That(report.BorrowerInfo.FirstName, Is.EqualTo("JOHN"));
            Assert.That(report.BorrowerInfo.LastName, Is.EqualTo("DOE"));
            Assert.That(report.BorrowerInfo.Ssn.Replace("-", ""), Is.EqualTo("100000000")); //mcl loads ssn as XXX-XX-XXXX while Mismo is X{9}
            return report;
        }

		public static CreditRequestData CreateRequestData(string url, CreditReportProtocol creditProtocol, 
			string accountIdentifier, string userName, string password,
			string borrowerFirstName, string borrowerLastName, string borrowerSsn,
			string coborrowerFirstName, string coborrowerLastName, string coborrowerSsn,
			string presentStreetAddress, string presentCity, string presentState, string presentZipcode) 

		{
			CreditRequestData data = CreditRequestData.CreateForTesting();

			data.RequestingPartyRequestedByName = "David Dao";
			data.Url = url;
			data.CreditProtocol = creditProtocol;
			data.LoginInfo.AccountIdentifier = accountIdentifier;
			data.LoginInfo.UserName = userName;
			data.LoginInfo.Password = password;

			data.IncludeEquifax = true;
			if (creditProtocol == CreditReportProtocol.InfoNetwork) 
			{
				// 2/6/2006 dd - Kassa tell me not to test with Experian server for now.
				data.IncludeExperian = false;
			} 
			else 
			{
				data.IncludeExperian = true;
			}
			data.IncludeTransUnion = true;

			data.Borrower.FirstName = borrowerFirstName;
			data.Borrower.LastName = borrowerLastName;
			data.Borrower.Ssn = borrowerSsn;
			data.Coborrower.FirstName = coborrowerFirstName;
			data.Coborrower.LastName = coborrowerLastName;
			data.Coborrower.Ssn = coborrowerSsn;

			Address address = new Address();
			address.ParseStreetAddress(presentStreetAddress);
			address.City = presentCity;
			address.State = presentState;
			address.Zipcode = presentZipcode;

			data.Borrower.CurrentAddress = address;

			return data;

		}
		public static ICreditReportResponse TestCreditResponse( CreditRequestData data ) 
		{
            // Tests probably don't need to go through the BJP.
            CreditReportRequestHandler requestHandler = new CreditReportRequestHandler(data, principal: null);
            
            // Tests don't need to go through the asynchronous processor.
            var result = requestHandler.SubmitSynchronously();

            ICreditReportResponse response = result.CreditReportResponse;
			string errorMessage = response.ErrorMessage == null  ? string.Empty : response.ErrorMessage; 
			Assert.That(errorMessage, Is.Empty ); 
			Assert.That(response.HasError, Is.False ); 
			Assert.That(response.IsReady, Is.True ); 
			return response;
		}
	}


	[TestFixture]
    [Category(CategoryConstants.ExpectFailureOnLocalhost)]
	public class CreditReportRetrieveTests 
	{
        [Ignore]
		[Test (Description ="Kroll Factual Data") ]
        [Category(CategoryConstants.ConnectionDependent)]
		public void OrderNewKrollCreditReport() 
		{
			CreditRequestData data = CreditTestingUtils.CreateRequestData("", CreditReportProtocol.KrollFactualData,
				"0603INLEND", "foo", ConstStage.CreditTestPasswordForKroll,
				"Sara", "Pratt", "000-00-0029",
				"George", "Pratt", "000-00-0030",
				"1496 WILLOW PARK", "TOONTOWN", "IL", "60144");
			ICreditReportResponse response = CreditTestingUtils.TestCreditResponse( data ) ;
		}
        [Category(CategoryConstants.ConnectionDependent)]
		[Test (Description = "LandSafe Credit Report Beta Server ( possible timeout )" ) ]
		public void OrderNewLandSafeCreditReport() 
		{
			CreditRequestData data = CreditTestingUtils.CreateRequestData("", CreditReportProtocol.Landsafe,
				"", "LINNCOOK", ConstStage.CreditTestPasswordForLandSafe,
				"Carmen", "ACACOMMON", "066-62-2527",
				"", "", "",
				"2 CENTURY PLZ", "TOMMORROW", "IL", "60750");

			ICreditReportResponse response = CreditTestingUtils.TestCreditResponse( data ) ;	
		}
        [Category(CategoryConstants.ConnectionDependent)]
		[Test (Description = "Credco Credit Report" ) ]
		public void OrderNewCredcoCreditReport() 
		{
			CreditRequestData data = CreditTestingUtils.CreateRequestData("", CreditReportProtocol.Credco,
				"", "3232757", ConstStage.CreditTestPasswordForCredco,
				"Ken", "Customer", "500-50-7000",
				"", "", "",
				"10655 BIRCH STREET", "BURBANK", "CA", "91502");

			ICreditReportResponse response = CreditTestingUtils.TestCreditResponse( data ) ;	
           
		}
        [Category(CategoryConstants.ConnectionDependent)]
		[Test(Description="Universal Credits" )]
		public void OrderNewUniversalCreditReport() 
		{
			CreditRequestData data = CreditTestingUtils.CreateRequestData("", CreditReportProtocol.UniversalCredit,
				"", "TestPriceMyLoan", ConstStage.CreditTestPasswordForUniversal, 
				"Michael", "Abacommon", "076-84-9627",
				"", "", "",
				"1 Compliant Dr. Apt 14", "Tommorrow", "IL", "60750");
		
			ICreditReportResponse response = CreditTestingUtils.TestCreditResponse( data ) ;	
		}
        [Category(CategoryConstants.ConnectionDependent)]
		[Test(Description="Fiserv / Chase Credit" )]
		public void OrderNewFiservCreditReport() 
		{
			CreditRequestData data = CreditTestingUtils.CreateRequestData("", CreditReportProtocol.Fiserv,
				"", "meridian_test", ConstStage.CreditTestPasswordForFiserv,  
				"Wil", "TestCase", "101-01-0001",
				"", "", "",
				"9000 Pillow Bridge Rd", "Glen Arrow", "VA", "23000");

			ICreditReportResponse response = CreditTestingUtils.TestCreditResponse( data ) ;	
		}
        [Category(CategoryConstants.ConnectionDependent)]
		[Test (Description = "Order new McL Credit Report")]
		public void OrderMclReport() 
		{

			CreditRequestData data  = CreditTestingUtils.CreateRequestData("", 
				CreditReportProtocol.Mcl, "", "pml_test", ConstStage.CreditTestPasswordForMcl, "Marisol", "Testcase", "000-00-0001", "","","","123 W. 23th St", "Irvine", "CA", "92612");
			data.ProviderId = "CC"; 
			data.ReportID = "";
            data.MclRequestType = MclRequestType.New; 
			ICreditReportResponse response = CreditTestingUtils.TestCreditResponse( data ) ;
			Assert.IsInstanceOf<MclCreditReportResponse>(response, "Was expecting MCL Credit Report Response" );
			XmlDocument doc = Tools.CreateXmlDoc( response.RawXmlResponse);
            XmlNode node = doc.SelectSingleNode("OUTPUT/RESPONSE/OUTPUT_FORMAT[@format_type='MISMO2_3_1']"); 
			Assert.That(node, Is.Not.Null );
            doc = Tools.CreateXmlDoc( node.InnerText ); 
			Assert.That(doc, Is.Not.Null ); 


		}	
		private void OrderMCLInstantViewCreditReport(string reportId, string password) 
		{
			CreditRequestData data = CreditRequestData.CreateForTesting(); 
			data.CreditProtocol = CreditReportProtocol.Mcl; 
			//data.ReportID = "35888"; 
			//data.InstantViewID = "CC-30F1D463"; 
			data.ReportID = reportId;
            data.MclRequestType = MclRequestType.Get; 
			data.InstantViewID = password; 
			ICreditReportResponse response = CreditTestingUtils.TestCreditResponse( data ) ;
			Assert.IsInstanceOf<MclCreditReportResponse>(response, "Was expecting MCL Credit Report Response" );
		}

        [Category(CategoryConstants.ConnectionDependent)]
		[Test (Description = "Reissue Report ") ]
		public void ReissueMCLCreditReport() 
		{
			CreditRequestData data = CreditRequestData.CreateForTesting(); 
			data.CreditProtocol = CreditReportProtocol.Mcl;
            data.ReportID = "146161"; 
			data.LoginInfo.UserName = "pml_test";
            data.MclRequestType = MclRequestType.Get; 
			data.LoginInfo.Password = ConstStage.CreditTestPasswordForMcl; 
			data.ProviderId = "CC"; 
			ICreditReportResponse response = CreditTestingUtils.TestCreditResponse( data ) ;
			Assert.IsInstanceOf<MclCreditReportResponse>(response, "Was expecting MCL Credit Report Response" );
		}

        [Category(CategoryConstants.ConnectionDependent)]
		//this CRA will not exist in production so these test will fail. 
		[Test (Description = "Reissue Report  using Lender Mapping") ]
		public void ReissueMCLCreditReportUsingLenderMapping() 
		{
			CreditRequestData data = CreditRequestData.CreateForTesting(); 
			data.CreditProtocol = CreditReportProtocol.LenderMappingCRA;
			data.ActualCraID = new Guid("6AACB207-799A-4F85-BAF7-AD62ED19E905");
            data.MclRequestType = MclRequestType.Get;
            data.ReportID = "146161"; 
			data.LoginInfo.UserName = "pml_test"; 
			data.LoginInfo.Password = ConstStage.CreditTestPasswordForMcl; 
			ICreditReportResponse response = CreditTestingUtils.TestCreditResponse( data ) ;
			Assert.IsInstanceOf<MclCreditReportResponse>(response, "Was expecting MCL Credit Report Response" );
		}
        [Category(CategoryConstants.ConnectionDependent)]
		[Test (Description = "Order Report using Lender Mapping") ]
		public void OrderNewMCLUsingLenderMapping() 
		{
			CreditRequestData data  = CreditTestingUtils.CreateRequestData("", 
				CreditReportProtocol.Mcl, "", "pml_test", ConstStage.CreditTestPasswordForMcl , "Marisol", "Testcase", "000-00-0001", "","","","123 W. 23th St", "Irvine", "CA", "92612");
			data.CreditProtocol = CreditReportProtocol.LenderMappingCRA; 
			data.ActualCraID = new Guid("6AACB207-799A-4F85-BAF7-AD62ED19E905");
            data.MclRequestType = MclRequestType.New; 
			ICreditReportResponse response = CreditTestingUtils.TestCreditResponse( data ) ;
			Assert.IsInstanceOf<MclCreditReportResponse>(response, "Was expecting MCL Credit Report Response" );
		}
        [Category(CategoryConstants.ConnectionDependent)]
		[Test(Description="FannieMae" )]
		public void OrderNewFannieMaeCreditReport() 
		{
			CreditRequestData data = CreditTestingUtils.CreateRequestData("", CreditReportProtocol.FannieMae, "", "1234567", ConstStage.CreditTestPasswordForFannieMae,
				"Patrick", "Purchaser", "999-12-1234",
				"Lorraine", "Purchaser", "888-56-5678",
				"1234 Main Street", "Baltimore", "MD", "20600");

			data.LoginInfo.FannieMaeCreditProvider = "200";
			data.LoginInfo.FannieMaeMORNETUserID = ConstAppDavid.FannieMae_TestDoUserId;
			data.LoginInfo.FannieMaeMORNETPassword = ConstAppDavid.FannieMae_TestDoPassword;

			CreditTestingUtils.TestCreditResponse(data);

		}

        [Category(CategoryConstants.ConnectionDependent)]
		[Test(Description="SharperLending" )]
		[Ignore("COuld not connect to SharperLending on Dev5.")]
		public void OrderNewSharperLendingCreditReport() 
		{
			CreditRequestData data = CreditTestingUtils.CreateRequestData("", CreditReportProtocol.SharperLending, "ILS", "test", ConstStage.CreditTestPasswordForSharperLending,
				"Charles", "DTestfile", "777-77-7777",
				"Annette", "DTEstfile", "677-77-7777",
				"4444 W Main", "LTestcity", "CA", "99000");

			CreditTestingUtils.TestCreditResponse(data);

		}

        [Category(CategoryConstants.ConnectionDependent)]
		[Test(Description="Credit Quick Services")]
		public void OrderNewCreditQuciServicesReport() 
		{
			string url = "https://www.creditquickservices.com/losrequest/weblosrequest.aspx?Client=PriceMyLoan";

			CreditRequestData data = CreditTestingUtils.CreateRequestData(url, CreditReportProtocol.CreditInterlink, "", "pmlcqs", ConstStage.CreditTestPasswordForCQS, 
				"Betty", "Oldcredit", "707-07-0707", 
				"", "", "", 
				"425 E. McFetridge Drive", "Chicago", "IL", "60605");

			CreditTestingUtils.TestCreditResponse(data);
		}

        [Category(CategoryConstants.ConnectionDependent)]
		[Test(Description="Order Republic Services")]
		public void OrderNewORSCreditReport()
		{
			string url = "https://www.orcredit.com/losrequest/weblosrequest.aspx?Client=PriceMyLoan";

			CreditRequestData data = CreditTestingUtils.CreateRequestData(url, CreditReportProtocol.CreditInterlink, "", "pmlorcs", ConstStage.CreditTestPasswordForORCS, 
				"Joe", "Hicredit", "767-67-6767", 
				"", "", "", 
				"777 Red Dog Road", "Green Bay", "WI", "54303");

			CreditTestingUtils.TestCreditResponse(data);
		}

        [Category(CategoryConstants.ConnectionDependent)]
		[Test(Description="CSC")]
		public void OrderNewCSCCreditReport()
		{
			string url = "https://emsws.equifax.com/emsws/services/post/MergeCreditWWW";

			CreditRequestData data = CreditTestingUtils.CreateRequestData(url, CreditReportProtocol.CSC, "999PM08167", "999PM08167", ConstStage.CreditTestPasswordForCSC, 
				"Kitty", "Cat", "400-41-4001", 
				"", "", "", 
				"2525 Litter Lane", "Atlanta", "GA", "30022");


			CreditTestingUtils.TestCreditResponse(data);
		}

        [Ignore]
		[Test(Description="CSD")]
        [Category(CategoryConstants.ConnectionDependent)]
		public void OrderNewCSDCreditReport()
		{
			string url = "https://www.ultraamps.com/uaweb/mismo";

			CreditRequestData data = CreditTestingUtils.CreateRequestData(url, CreditReportProtocol.CSD, "", "ils", ConstStage.CreditTestPasswordForCSD, 
				"MICHAEL", "ABACOMMON", "076-84-9627", 
				"", "", "", 
				"1 COMPLIANT", "Fantasy Island", "IL", "60750");


			CreditTestingUtils.TestCreditResponse(data);
		}

		[Test(Description="CBC")]
        [Category(CategoryConstants.ConnectionDependent)]
		public void OrderNewCBCCreditReport()
		{
			string url = "https://www.creditbureaureports.com/servlet/gnbank?logid=lendoff&command=apiordretpost&options=ORD%3DIN+PA%3DXM+TEXT%3DN+PS%3DH+REVL%3DY+REVF%3DX4";

			CreditRequestData data = CreditTestingUtils.CreateRequestData(url, CreditReportProtocol.CBC, "", "pr99903b",ConstStage.CreditTestPasswordForCBC, 
				"RONAL", "BOGUS", "005-16-0001", 
				"", "", "", 
				"9806 CAPITALISTIC DR #306", "ALLISON PARK", "PA", "15101");


			CreditTestingUtils.TestCreditResponse(data);
		}
	}
}