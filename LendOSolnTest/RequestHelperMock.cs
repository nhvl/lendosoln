using System;
using CBaseException = DataAccess.CBaseException;
using System.Collections.Specialized;


namespace LendOSolnTest
{
	
	public class RequestHelperMock : LendersOffice.Common.IQueryArgs
	{
		NameValueCollection  m_queryString = new NameValueCollection();

		public NameValueCollection QueryString
		{
			get { return m_queryString; }
		}
	
		#region Implementation of IRequestHelper
		public Guid GetGuid(string key) 
		{
			Guid value = Guid.Empty;
			try 
			{
				if( QueryString[key] != "") 
				{
					value = new Guid(QueryString[key]);
				}

			} 
			catch {}
			if (value == Guid.Empty) 
			{
				new Exception(key + " is not a valid Guid format.");
			}
			return value;
		}
		/// <summary>
		/// Return Guid from Request.QueryString. Return default value if
		/// QueryString value does not exist.
		/// </summary>
		/// <param name="key"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		public Guid GetGuid(string key, Guid defaultValue) 
		{
			Guid value = defaultValue;
			try 
			{
				string val = QueryString[key];
				if( val != "" && val != null ) 
				{
					value = new Guid(val);
				}

			} 
			catch {}

			return value;
		}

		/// <summary>
		/// Return loanid value in Request.QueryString.
		/// </summary>
		public Guid LoanID 
		{
			get 
			{
				/*
				string key = "rLoanID";
				if (HttpContext.Current.Items[key] == null || HttpContext.Current.Items[key].GetType() != typeof(Guid)) 
				{
					Guid value = GetGuid("loanid");
					HttpContext.Current.Items[key] = value;
				}
				return (Guid) HttpContext.Current.Items[key];
				*/
				return GetGuid("loanid");

			}
		}

		/// <summary>
		/// Return bool value for query string variable. Return true if value is 'T', 't', '1', 'true', else return false.
		/// </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		public  bool GetBool(string key) 
		{
			bool value = false;
			string str = QueryString[key];

			if (str != null && (str == "T" || str == "t" || str == "1" || str.ToLower() == "true")) 
			{
				value = true;
			}
			return value;
		}

		/// <summary>
		/// Return int from Request.QueryString. Redirect user to error page
		/// if key is not found or invalid number.
		/// </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		public int GetInt(string key) 
		{
			if (QueryString[key] != "") 
			{
				try 
				{
					return int.Parse(QueryString[key]);
				} 
				catch {}
			}
			throw new Exception(key + " is not a valid number format.");
		}

		/// <summary>
		/// Return int from Request.QueryString. Return default value if key is not found or invalid number.
		/// </summary>
		/// <param name="key"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		public  int GetInt(string key, int defaultValue) 
		{
			int value = defaultValue;
			if (QueryString[key] != "") 
			{
				try 
				{
					value = int.Parse(QueryString[key]);
				} 
				catch {}
			}
			return value;
		}

		public void DisplayErrorPage(Exception exc, bool isSendEmail, Guid brokerID, Guid employeeID)
		{
			throw new Exception( "BrokerID = " + brokerID.ToString() + " *** employeeID = " + employeeID.ToString(), exc);
		}

		public  void DisplayErrorPage(CBaseException exc, bool isSendEmail, Guid brokerID, Guid employeeID)
		{
			throw new Exception( "BrokerID = " + brokerID.ToString() + " *** employeeID = " + employeeID.ToString(), exc);
		}


		#endregion
	}

}
