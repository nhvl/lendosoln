﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using iTextSharp.text.pdf;
using System.IO;
using LendersOffice.Constants;
using iTextSharp.text;
using LendOSolnTest.Common;


namespace LendOSolnTest
{
    [TestFixture]
    public class iTextSharpTest
    {
        string fileWSignedSigFieldName;
        string createdFileWBlankSigFieldName;
        string createdBasicFileName;

        string sigFieldName = "mySigField";
        string textFieldName = "some_text";

        [TestFixtureSetUp]
        public void Setup()
        {   
            fileWSignedSigFieldName = Path.Combine(TestDataRootFolder.Location, "SampleSignedPDFDocument.pdf");
            createdBasicFileName = Path.Combine(TestDataRootFolder.Location, "basicFile.pdf");
            createdFileWBlankSigFieldName = Path.Combine(TestDataRootFolder.Location, "blankSigField.pdf");
            if (!File.Exists(createdBasicFileName))
            {
                CreateBasicPdf();
            }
            if (!File.Exists(createdFileWBlankSigFieldName))
            {
                CreatePdfWithBlankSignatureField();
            }
        }
        [TestFixtureTearDown]
        public void TearDown()
        {
        }

        [Test]
        public void TestPdfReaderByPassOwnerPassword()
        {
            // 6/3/2010 dd - Test to make sure PdfReader from iTextSharp can get page count from pdf document with only owner password protection.

            PdfReader reader = new PdfReader(TestDataRootFolder.Location + "TestWithOwnerPassword.pdf");

            Assert.AreEqual(true, reader.IsOpenedWithFullPermissions); // 12/14/2010 dd - We modify iTextSharp so it ignore owner password
            Assert.AreEqual(4, reader.NumberOfPages);

            // 12/14/2010 dd - Allow to copy the pdf with owner password set.
            Document document = new Document();
            using (MemoryStream stream = new MemoryStream())
            {
                PdfCopy writer = new PdfCopy(document, stream);
                document.Open();

                for (int i = 1; i <= reader.NumberOfPages; i++)
                {
                    PdfImportedPage p = writer.GetImportedPage(reader, i);
                    writer.AddPage(p);
                }
                document.Close();
            }

        }

        [Test]
        public void TestSignatures()
        {
            Assert.IsTrue(!HasSignatureName(createdBasicFileName));
            Assert.IsTrue(HasSignatureName(fileWSignedSigFieldName));
            Assert.IsTrue(!HasSignatureName(createdFileWBlankSigFieldName));
            Assert.IsTrue(HasSignatureField(createdFileWBlankSigFieldName));
            Assert.IsTrue(!HasSignatureField(createdBasicFileName));
            Assert.IsTrue(HasSignatureField(fileWSignedSigFieldName));
        }
        [Test]
        public void IsBlankSignatureTest()
        {
            PdfReader reader = new PdfReader(createdFileWBlankSigFieldName);
            Assert.IsTrue(IsBlankSignature(reader.AcroFields, sigFieldName));
            MLAssert.Throws<ArgumentException>(()=>IsBlankSignature(reader.AcroFields, textFieldName));
            reader.Close();

            reader = new PdfReader(fileWSignedSigFieldName);
            Assert.IsTrue(false == IsBlankSignature(reader.AcroFields, "Signature2"));
            MLAssert.Throws<ArgumentException>(()=>IsBlankSignature(reader.AcroFields, "what?"));
        }

        #region Non-Tests
        
        #region File Creation
        private void CreatePdfWithBlankSignatureField()
        {
            if (!File.Exists(createdBasicFileName))
            {
                CreateBasicPdf();
            }
            PdfReader reader = new PdfReader(createdBasicFileName);
            FileStream fs = new FileStream(createdFileWBlankSigFieldName, FileMode.Create, FileAccess.Write);
            PdfStamper stamp = new PdfStamper(reader, fs, '\0', true);
            TextField field = new TextField(stamp.Writer, new iTextSharp.text.Rectangle(40, 500, 360, 530), textFieldName);
            stamp.AddAnnotation(field.GetTextField(), 1);
            stamp.FormFlattening = false; // lock fields and prevent further edits.

            PdfFormField sigField = PdfFormField.CreateSignature(stamp.Writer);
            sigField.SetWidget(new Rectangle(40, 700, 360, 730), PdfAnnotation.HIGHLIGHT_INVERT);
            sigField.FieldName = sigFieldName;
            sigField.SetFieldFlags(PdfAnnotation.FLAGS_PRINT);
            stamp.AddAnnotation(sigField, 1);

            stamp.Close();
            
        }
        private void CreateBasicPdf()
        {
            Document doc = new Document(PageSize.A4);
            var writer = PdfWriter.GetInstance(doc, new FileStream(createdBasicFileName, FileMode.Create));
            doc.Open();
            doc.Add(new Paragraph("First paragraph"));
            doc.NewPage();
            doc.NewPage();
            doc.Add(new iTextSharp.text.Paragraph("Second paragraph"));
            doc.Close();
        }
        #endregion

        private bool HasSignatureName(string fileName)
        {
            PdfReader reader = new PdfReader(fileName);

            if (reader.AcroFields.Fields.Count == 0)
            {
                Console.WriteLine("no fields.");
            }

            return reader.AcroFields.GetSignatureNames().Count > 0;
        }
        private bool HasSignatureField(string fileName)
        {
            PdfReader reader = new PdfReader(fileName);

            foreach (var fieldKVP in reader.AcroFields.Fields)
            {
                if (AcroFields.FIELD_TYPE_SIGNATURE == reader.AcroFields.GetFieldType(fieldKVP.Key))
                {
                    return true;
                }
            }
            return false;
        }

        #region Blank Signature Helpers
        private static bool IsBlankSignature(AcroFields af, string name)
        {
            Dictionary<string, int[]> sigNames;
            FindSignatureNames(af, out sigNames);
            if (!af.Fields.ContainsKey(name))
                throw new ArgumentException("fields does not contain field name: " + name);

            AcroFields.Item item = af.GetFieldItem(name);
            PdfDictionary merged = item.GetMerged(0);
            if (!PdfName.SIG.Equals(merged.GetAsName(PdfName.FT)))
                throw new ArgumentException("field named "+ name + " is not a signature field.");
            if (sigNames.ContainsKey(name))
                return false;
            return true;
        }
        private static List<string> GetBlankSignatureNames(AcroFields af, PdfReader reader)
        {
            Dictionary<string, int[]> sigNames;
            FindSignatureNames(af, out sigNames);
            List<String> sigs = new List<String>();
            foreach (KeyValuePair<string, AcroFields.Item> entry in af.Fields)
            {
                AcroFields.Item item = entry.Value;
                PdfDictionary merged = item.GetMerged(0);
                if (!PdfName.SIG.Equals(merged.GetAsName(PdfName.FT)))
                    continue;
                if (sigNames.ContainsKey(entry.Key))
                    continue;
                sigs.Add(entry.Key);
            }
            return sigs;
        }
        private static void FindSignatureNames(AcroFields af, out Dictionary<string, int[]> sigNames)
        {
            var fields = af.Fields;
            sigNames = new Dictionary<string, int[]>();
            List<object[]> sorter = new List<object[]>();
            foreach (KeyValuePair<string, AcroFields.Item> entry in fields)
            {
                AcroFields.Item item = entry.Value;
                PdfDictionary merged = item.GetMerged(0);
                if (!PdfName.SIG.Equals(merged.Get(PdfName.FT)))
                    continue;
                PdfDictionary v = merged.GetAsDict(PdfName.V);
                if (v == null)
                    continue;
                PdfString contents = v.GetAsString(PdfName.CONTENTS);
                if (contents == null)
                    continue;
                PdfArray ro = v.GetAsArray(PdfName.BYTERANGE);
                if (ro == null)
                    continue;
                int rangeSize = ro.Size;
                if (rangeSize < 2)
                    continue;
                int length = ro.GetAsNumber(rangeSize - 1).IntValue + ro.GetAsNumber(rangeSize - 2).IntValue;
                sorter.Add(new Object[] { entry.Key, new int[] { length, 0 } });
            }
            sorter.Sort(new AcroFields_ISorterComparator());
            if (sorter.Count > 0)
            {
                //if (((int[])sorter[sorter.Count - 1][1])[0] == reader.FileLength)
                //    totalRevisions = sorter.Count;
                //else
                //    totalRevisions = sorter.Count + 1;
                for (int k = 0; k < sorter.Count; ++k)
                {
                    Object[] objs = sorter[k];
                    String name = (String)objs[0];
                    int[] p = (int[])objs[1];
                    p[1] = k + 1;
                    sigNames[name] = p;
                }
            }
        }
        private class AcroFields_ISorterComparator : IComparer<Object[]>
        {
            public int Compare(Object[] o1, Object[] o2)
            {
                int n1 = ((int[])o1[1])[0];
                int n2 = ((int[])o2[1])[0];
                return n1 - n2;
            }
        }
        #endregion
        #endregion
    }
    
}
