﻿using System;
using DataAccess;
using LendersOffice.Constants;
using LendOSolnTest.Common;
using LendOSolnTest.Fakes;
using NUnit.Framework;

namespace LendOSolnTest.Conversions
{
    [TestFixture]
    public class FannieMaePropertyTypeTest
    {
        [TestFixtureSetUp]
        public void Setup()
        {
            LoginTools.LoginAs(TestAccountType.LoanOfficer);
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
            FakePageDataUtilities.Instance.Reset();
        }

        private CPageData CreatePageData()
        {
            return FakePageData.Create();
        }

        [Test]
        public void Test_sProdSpT_AfterTransformation()
        {
            
            // 7/15/2009 dd - In this test, I want to make sure sProdSpT and sProdSpStructureT are still the same after transformation from LO to PML.
            // This is to simulate the following scenario. User choose sProdSpT and sProdSpStructureT, run pricing and then get out of PML. When they
            // edit the loan in PML, the two values should be the same.
            foreach (E_sProdSpT sProdSpT in Enum.GetValues(typeof(E_sProdSpT)))
            {
                if (sProdSpT == E_sProdSpT.MixedUse || sProdSpT == E_sProdSpT.Townhouse || sProdSpT == E_sProdSpT.Commercial)
                {
                    continue; // 7/16/2009 dd - We currently do not use these three property type.
                }
                foreach (E_sProdSpStructureT sProdSpStructureT in Enum.GetValues(typeof(E_sProdSpStructureT)))
                {
                    CPageData dataLoan = CreatePageData();
                    dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                    dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
                    dataLoan.sProdSpT = sProdSpT;
                    dataLoan.sProdSpStructureT = sProdSpStructureT;
                    dataLoan.Save();

                    dataLoan = CreatePageData();
                    dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                    dataLoan.TransformDataToPml(E_TransformToPmlT.FromScratch);
                    dataLoan.Save();

                    dataLoan = CreatePageData();
                    dataLoan.InitLoad();
                    dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
                    Assert.AreEqual(sProdSpT, dataLoan.sProdSpT, "sProdSpT=" + sProdSpT + ", sProdSpStructureT=" + sProdSpStructureT);
                    Assert.AreEqual(sProdSpStructureT, dataLoan.sProdSpStructureT, "sProdSpT=" + sProdSpT + ", sProdSpStructureT=" + sProdSpStructureT);
                }
            }
            // Assert
        }
        [Test]
        public void TestPropertyTypeMappings()
        {
            var Tests = new[] 
            {
                new { sFannieSpT = E_sFannieSpT.Condo,                      sGseSpT = E_sGseSpT.Condominium,                 sProdSpT = E_sProdSpT.Condo,         sProdSpStructureT  = E_sProdSpStructureT.Attached},
                new { sFannieSpT = E_sFannieSpT.Detached,                   sGseSpT = E_sGseSpT.Detached,                    sProdSpT = E_sProdSpT.SFR,           sProdSpStructureT  = E_sProdSpStructureT.Detached},
                new { sFannieSpT = E_sFannieSpT.PUD,                        sGseSpT = E_sGseSpT.PUD,                         sProdSpT = E_sProdSpT.PUD,           sProdSpStructureT  = E_sProdSpStructureT.Detached},
                new { sFannieSpT = E_sFannieSpT.Attached,                   sGseSpT = E_sGseSpT.Attached,                    sProdSpT = E_sProdSpT.SFR,           sProdSpStructureT  = E_sProdSpStructureT.Attached},
                new { sFannieSpT = E_sFannieSpT.PUD,                        sGseSpT = E_sGseSpT.PUD,                         sProdSpT = E_sProdSpT.PUD,           sProdSpStructureT  = E_sProdSpStructureT.Detached},
                new { sFannieSpT = E_sFannieSpT.CoOp,                       sGseSpT = E_sGseSpT.Cooperative,                 sProdSpT = E_sProdSpT.CoOp,          sProdSpStructureT  = E_sProdSpStructureT.Attached},
                new { sFannieSpT = E_sFannieSpT.HighRiseCondo,              sGseSpT = E_sGseSpT.HighRiseCondominium,         sProdSpT = E_sProdSpT.Condo,         sProdSpStructureT  = E_sProdSpStructureT.Attached},
                new { sFannieSpT = E_sFannieSpT.Manufactured,               sGseSpT = E_sGseSpT.ManufacturedHousing,         sProdSpT = E_sProdSpT.Manufactured,  sProdSpStructureT  = E_sProdSpStructureT.Detached},
                new { sFannieSpT = E_sFannieSpT.DetachedCondo,              sGseSpT = E_sGseSpT.DetachedCondominium,         sProdSpT = E_sProdSpT.Condo,         sProdSpStructureT  = E_sProdSpStructureT.Detached},
                new { sFannieSpT = E_sFannieSpT.ManufacturedCondoPudCoop,   sGseSpT = E_sGseSpT.ManufacturedHomeCondominium, sProdSpT = E_sProdSpT.Manufactured,  sProdSpStructureT  = E_sProdSpStructureT.Detached},
            };

            CPageData data = CreatePageData();
            data.InitLoad();

            foreach (var test in Tests)
            {

                data.CalcModeT = E_CalcModeT.LendersOfficePageEditing;
                data.sFannieSpT = test.sFannieSpT;

                Assert.That(data.sGseSpT, Is.EqualTo(test.sGseSpT), "sFannieSpT:" + test.sFannieSpT + " Expected sGseSpT:" + test.sGseSpT);

                data.TransformDataToPml(E_TransformToPmlT.FromScratch);
                data.CalcModeT = E_CalcModeT.PriceMyLoan;

                Assert.That(data.sProdSpT, Is.EqualTo(test.sProdSpT), "sFannieSpT:" + test.sFannieSpT + " Expected sProdSpT:" + test.sProdSpT);
                Assert.That(data.sProdSpStructureT, Is.EqualTo(test.sProdSpStructureT), "sFannieSpT:" + test.sFannieSpT + " Expected sProdSpStructureT:" + test.sProdSpStructureT);

            }


        }

        [Test]
        public void TestMap_sGseSpT_To_sProdSpT_sProdSpStructureT()
        {
            var gseTests = new[] 
            {
                new { sGseSpT = E_sGseSpT.Condominium,                     sProdSpT = E_sProdSpT.Condo,         sProdSpStructureT  = E_sProdSpStructureT.Attached},
                new { sGseSpT = E_sGseSpT.Detached,                        sProdSpT = E_sProdSpT.SFR,           sProdSpStructureT  = E_sProdSpStructureT.Detached},
                new { sGseSpT = E_sGseSpT.Attached,                        sProdSpT = E_sProdSpT.SFR,           sProdSpStructureT  = E_sProdSpStructureT.Attached},
                new { sGseSpT = E_sGseSpT.PUD,                             sProdSpT = E_sProdSpT.PUD,           sProdSpStructureT  = E_sProdSpStructureT.Detached},
                new { sGseSpT = E_sGseSpT.Detached,                        sProdSpT = E_sProdSpT.SFR,           sProdSpStructureT  = E_sProdSpStructureT.Detached},
                new { sGseSpT = E_sGseSpT.PUD,                             sProdSpT = E_sProdSpT.PUD,           sProdSpStructureT  = E_sProdSpStructureT.Detached},
                new { sGseSpT = E_sGseSpT.Cooperative,                     sProdSpT = E_sProdSpT.CoOp,          sProdSpStructureT  = E_sProdSpStructureT.Attached},
                new { sGseSpT = E_sGseSpT.HighRiseCondominium,             sProdSpT = E_sProdSpT.Condo,         sProdSpStructureT  = E_sProdSpStructureT.Attached},
                new { sGseSpT = E_sGseSpT.ManufacturedHousing,             sProdSpT = E_sProdSpT.Manufactured,  sProdSpStructureT  = E_sProdSpStructureT.Detached},
                new { sGseSpT = E_sGseSpT.DetachedCondominium,             sProdSpT = E_sProdSpT.Condo,         sProdSpStructureT  = E_sProdSpStructureT.Detached},
                new { sGseSpT = E_sGseSpT.ManufacturedHomeCondominium,     sProdSpT = E_sProdSpT.Manufactured,  sProdSpStructureT  = E_sProdSpStructureT.Detached},
                new { sGseSpT = E_sGseSpT.ManufacturedHomeMultiwide,       sProdSpT = E_sProdSpT.Manufactured,  sProdSpStructureT  = E_sProdSpStructureT.Detached},
                new { sGseSpT = E_sGseSpT.ManufacturedHousingSingleWide,   sProdSpT = E_sProdSpT.Manufactured,  sProdSpStructureT  = E_sProdSpStructureT.Detached},
                new { sGseSpT = E_sGseSpT.ManufacturedHomeMultiwide,       sProdSpT = E_sProdSpT.Manufactured,  sProdSpStructureT  = E_sProdSpStructureT.Detached},
                new { sGseSpT = E_sGseSpT.Modular,                         sProdSpT = E_sProdSpT.Modular,       sProdSpStructureT  = E_sProdSpStructureT.Detached},
            };

            CPageData data = CreatePageData();
            data.InitLoad();

            foreach (var test in gseTests)
            {

                data.CalcModeT = E_CalcModeT.LendersOfficePageEditing;
                data.sGseSpT = test.sGseSpT;

                data.TransformDataToPml(E_TransformToPmlT.FromScratch);
                data.CalcModeT = E_CalcModeT.PriceMyLoan;

                Assert.That(data.sProdSpT, Is.EqualTo(test.sProdSpT), "sGseSpT:" + test.sGseSpT + " Expected sProdSpT:" + test.sProdSpT);
                Assert.That(data.sProdSpStructureT, Is.EqualTo(test.sProdSpStructureT), "sGseSpT:" + test.sGseSpT + " Expected sProdSpStructureT:" + test.sProdSpStructureT);

            }

        }

        [Test]
        public void TestMap_sProdSpT_sProdSpStructureT_To_sGseSpT()
        {
            var testCases = new[] {
                new { sProdSpT = E_sProdSpT.SFR, sProdSpStructureT = E_sProdSpStructureT.Detached, sProdCondoStories_rep = "1", sGseSpT = E_sGseSpT.Detached, sUnitsNum_rep = "1"}
                ,new { sProdSpT = E_sProdSpT.SFR, sProdSpStructureT = E_sProdSpStructureT.Attached, sProdCondoStories_rep = "1", sGseSpT = E_sGseSpT.Attached, sUnitsNum_rep = "1"}
                ,new { sProdSpT = E_sProdSpT.TwoUnits, sProdSpStructureT = E_sProdSpStructureT.Detached, sProdCondoStories_rep = "1", sGseSpT = E_sGseSpT.LeaveBlank, sUnitsNum_rep = "2"}
                ,new { sProdSpT = E_sProdSpT.TwoUnits, sProdSpStructureT = E_sProdSpStructureT.Attached, sProdCondoStories_rep = "1", sGseSpT = E_sGseSpT.LeaveBlank, sUnitsNum_rep = "2"}

                ,new { sProdSpT = E_sProdSpT.ThreeUnits, sProdSpStructureT = E_sProdSpStructureT.Detached, sProdCondoStories_rep = "1", sGseSpT = E_sGseSpT.LeaveBlank, sUnitsNum_rep = "3"}
                ,new { sProdSpT = E_sProdSpT.ThreeUnits, sProdSpStructureT = E_sProdSpStructureT.Attached, sProdCondoStories_rep = "1", sGseSpT = E_sGseSpT.LeaveBlank, sUnitsNum_rep = "3"}

                ,new { sProdSpT = E_sProdSpT.FourUnits, sProdSpStructureT = E_sProdSpStructureT.Detached, sProdCondoStories_rep = "1", sGseSpT = E_sGseSpT.LeaveBlank, sUnitsNum_rep = "4"}
                ,new { sProdSpT = E_sProdSpT.FourUnits, sProdSpStructureT = E_sProdSpStructureT.Attached, sProdCondoStories_rep = "1", sGseSpT = E_sGseSpT.LeaveBlank, sUnitsNum_rep = "4"}

                ,new { sProdSpT = E_sProdSpT.PUD, sProdSpStructureT = E_sProdSpStructureT.Detached, sProdCondoStories_rep = "1", sGseSpT = E_sGseSpT.PUD, sUnitsNum_rep = "1"}
                ,new { sProdSpT = E_sProdSpT.PUD, sProdSpStructureT = E_sProdSpStructureT.Attached, sProdCondoStories_rep = "1", sGseSpT = E_sGseSpT.PUD, sUnitsNum_rep = "1"}

                ,new { sProdSpT = E_sProdSpT.Condo, sProdSpStructureT = E_sProdSpStructureT.Detached, sProdCondoStories_rep = "1", sGseSpT = E_sGseSpT.DetachedCondominium, sUnitsNum_rep = "1"}
                ,new { sProdSpT = E_sProdSpT.Condo, sProdSpStructureT = E_sProdSpStructureT.Attached, sProdCondoStories_rep = "1", sGseSpT = E_sGseSpT.Condominium, sUnitsNum_rep = "1"}

                ,new { sProdSpT = E_sProdSpT.Condo, sProdSpStructureT = E_sProdSpStructureT.Detached, sProdCondoStories_rep = "4", sGseSpT = E_sGseSpT.DetachedCondominium, sUnitsNum_rep = "1"}
                ,new { sProdSpT = E_sProdSpT.Condo, sProdSpStructureT = E_sProdSpStructureT.Attached, sProdCondoStories_rep = "4", sGseSpT = E_sGseSpT.Condominium, sUnitsNum_rep = "1"}

                ,new { sProdSpT = E_sProdSpT.Condo, sProdSpStructureT = E_sProdSpStructureT.Detached, sProdCondoStories_rep = "5", sGseSpT = E_sGseSpT.HighRiseCondominium, sUnitsNum_rep = "1"}
                ,new { sProdSpT = E_sProdSpT.Condo, sProdSpStructureT = E_sProdSpStructureT.Attached, sProdCondoStories_rep = "5", sGseSpT = E_sGseSpT.HighRiseCondominium, sUnitsNum_rep = "1"}

                ,new { sProdSpT = E_sProdSpT.CoOp, sProdSpStructureT = E_sProdSpStructureT.Detached, sProdCondoStories_rep = "1", sGseSpT = E_sGseSpT.Cooperative, sUnitsNum_rep = "1"}
                ,new { sProdSpT = E_sProdSpT.CoOp, sProdSpStructureT = E_sProdSpStructureT.Attached, sProdCondoStories_rep = "1", sGseSpT = E_sGseSpT.Cooperative, sUnitsNum_rep = "1"}

                ,new { sProdSpT = E_sProdSpT.Manufactured, sProdSpStructureT = E_sProdSpStructureT.Detached, sProdCondoStories_rep = "1", sGseSpT = E_sGseSpT.ManufacturedHousing, sUnitsNum_rep = "1"}
                ,new { sProdSpT = E_sProdSpT.Manufactured, sProdSpStructureT = E_sProdSpStructureT.Attached, sProdCondoStories_rep = "1", sGseSpT = E_sGseSpT.ManufacturedHousing, sUnitsNum_rep = "1"}

                ,new { sProdSpT = E_sProdSpT.Rowhouse, sProdSpStructureT = E_sProdSpStructureT.Detached, sProdCondoStories_rep = "1", sGseSpT = E_sGseSpT.Attached, sUnitsNum_rep = "1"}
                ,new { sProdSpT = E_sProdSpT.Rowhouse, sProdSpStructureT = E_sProdSpStructureT.Attached, sProdCondoStories_rep = "1", sGseSpT = E_sGseSpT.Attached, sUnitsNum_rep = "1"}

                ,new { sProdSpT = E_sProdSpT.Modular, sProdSpStructureT = E_sProdSpStructureT.Detached, sProdCondoStories_rep = "1", sGseSpT = E_sGseSpT.Modular, sUnitsNum_rep = "1"}
                ,new { sProdSpT = E_sProdSpT.Modular, sProdSpStructureT = E_sProdSpStructureT.Attached, sProdCondoStories_rep = "1", sGseSpT = E_sGseSpT.Modular, sUnitsNum_rep = "1"}

            };

            CPageData dataLoan = CreatePageData();
            dataLoan.InitLoad();

            foreach (var test in testCases)
            {
                dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
                dataLoan.sProdSpT = test.sProdSpT;
                dataLoan.sProdSpStructureT = test.sProdSpStructureT;
                dataLoan.sProdCondoStories_rep = test.sProdCondoStories_rep;

                dataLoan.CalcModeT = E_CalcModeT.LendersOfficePageEditing;
                
                if (test.sGseSpT != E_sGseSpT.LeaveBlank)
                {
                    Assert.AreEqual(test.sGseSpT, dataLoan.sGseSpT, test.ToString());
                }
                Assert.AreEqual(test.sUnitsNum_rep, dataLoan.sUnitsNum_rep);
            }
        }
    }
}
