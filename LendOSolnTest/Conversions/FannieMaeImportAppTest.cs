﻿namespace LendOSolnTest.Conversions
{
    using System;
    using LendersOffice.Conversions;
    using LendersOffice.Security;
    using LendOSolnTest.Common;
    using LendOSolnTest.Fakes;
    using NUnit.Framework;

    [TestFixture]
    public class FannieMaeImportAppTest
    {
        private readonly AbstractUserPrincipal _principal = LoginTools.LoginAs(TestAccountType.LoTest001);

        [TestFixtureTearDown]
        public void TearDown()
        {
            FakePageDataUtilities.Instance.Reset();
        }

        [Test]
        public void ImportAppTest()
        {
            var testSettings = new[] 
            { 
                    new { Source = FannieMaeImportSource.LendersOffice, ImportApps = true,  Result = 2, Msg = "Importing All Apps LO" },
                    new { Source = FannieMaeImportSource.LendersOffice, ImportApps = false, Result = 1, Msg = "Importing 1 Apps LO"},
                    new { Source = FannieMaeImportSource.PriceMyLoan, ImportApps = true,  Result = 2, Msg = "Importing All Apps Pml"},
                    new { Source = FannieMaeImportSource.PriceMyLoan, ImportApps = false, Result = 1, Msg = "Importing 1 Apps PML"} 
            };
            
            foreach (var test in testSettings)
            {
                var importer = new FakeFannieMae32Importer(test.Source)
                {
                    AddEmployeeAsOfficialAgent = true,
                    ImportMultipleApps = test.ImportApps
                };

                var data = importer.Import(TestUtilities.GetFilePath("FannieMaeFiles/ALICE_FIRSTIMER.fnm"), _principal, Guid.Empty);
                Assert.AreEqual(data.nApps, test.Result, test.Msg);
            }
        }
    }
}
