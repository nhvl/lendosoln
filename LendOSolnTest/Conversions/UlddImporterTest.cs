﻿namespace LendOSolnTest.Conversions
{
    using System.Linq;
    using LendersOffice.Common;
    using LendersOffice.Conversions;
    using LendOSolnTest.Common;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    public class UlddImporterTest
    {
        [Test]
        public void ImportIntoExisting_FileUsingLegacyIncomeFields_CorrectlySetsBaseIncome()
        {
            using (var testLoan = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: false))
            {
                var xml = TestUtilities.ReadFile("ULDD_IMPORTER_TEST.xml");

                var importer = new UlddImporter();
                importer.ImportIntoExisting(testLoan.Id, xml);

                var loan = testLoan.CreateNewPageDataWithBypass();
                loan.InitLoad();
                Assert.AreEqual(3000, loan.GetAppData(0).aBBaseI);
                Assert.AreEqual(1500, loan.GetAppData(0).aCBaseI);
            }
        }

        [Test]
        public void ImportIntoExisting_FileUsingIncomeSourceCollectionWithNoExistingRecord_AddsBaseIncome()
        {
            using (var testLoan = LoanForIntegrationTest.Create(TestFileType.TestFile))
            {
                var loanBefore = testLoan.CreateNewPageDataWithBypass();
                loanBefore.InitSave();
                loanBefore.MigrateToUseLqbCollections();
                loanBefore.MigrateToIncomeSourceCollection();
                var appBefore = loanBefore.GetAppData(0);
                loanBefore.AddCoborrowerToLegacyApplication(loanBefore.GetAppData(0).aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                loanBefore.Save();
                Assert.IsTrue(loanBefore.sIsIncomeCollectionEnabled);
                Assert.AreEqual(0, loanBefore.IncomeSources.Count);
                var xml = TestUtilities.ReadFile("ULDD_IMPORTER_TEST.xml");

                var importer = new UlddImporter();
                importer.ImportIntoExisting(testLoan.Id, xml);

                var loanAfter = testLoan.CreateNewPageDataWithBypass();
                loanAfter.InitLoad();
                Assert.AreEqual(true, loanAfter.sIsIncomeCollectionEnabled);
                Assert.AreEqual(2, loanAfter.IncomeSources.Count);
                Assert.AreEqual(3000, loanAfter.GetAppData(0).aBBaseI);
                Assert.AreEqual(1500, loanAfter.GetAppData(0).aCBaseI);
            }
        }

        [Test]
        public void ImportIntoExisting_FileUsingIncomeSourceCollectionWithExistingRecords_CorrectlyOverwritesBaseIncome()
        {
            using (var testLoan = LoanForIntegrationTest.Create(TestFileType.TestFile))
            {
                var loanBefore = testLoan.CreateNewPageDataWithBypass();
                loanBefore.InitSave();
                loanBefore.MigrateToUseLqbCollections();
                loanBefore.MigrateToIncomeSourceCollection();
                Assert.IsTrue(loanBefore.sIsIncomeCollectionEnabled);
                var appBefore = loanBefore.GetAppData(0);
                loanBefore.AddCoborrowerToLegacyApplication(appBefore.aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                loanBefore.AddIncomeSource(
                    appBefore.aBConsumerId.ToIdentifier<DataObjectKind.Consumer>(),
                    new LendingQB.Core.Data.IncomeSource
                    {
                        IncomeType = IncomeType.BaseIncome,
                        MonthlyAmountData = Money.Create(1)
                    });
                loanBefore.AddIncomeSource(
                    appBefore.aBConsumerId.ToIdentifier<DataObjectKind.Consumer>(),
                    new LendingQB.Core.Data.IncomeSource
                    {
                        IncomeType = IncomeType.BaseIncome,
                        MonthlyAmountData = Money.Create(1)
                    });
                loanBefore.AddIncomeSource(
                    appBefore.aCConsumerId.ToIdentifier<DataObjectKind.Consumer>(),
                    new LendingQB.Core.Data.IncomeSource
                    {
                        IncomeType = IncomeType.BaseIncome,
                        MonthlyAmountData = Money.Create(2)
                    });
                loanBefore.AddIncomeSource(
                    appBefore.aCConsumerId.ToIdentifier<DataObjectKind.Consumer>(),
                    new LendingQB.Core.Data.IncomeSource
                    {
                        IncomeType = IncomeType.BaseIncome,
                        MonthlyAmountData = Money.Create(2)
                    });
                Assert.AreEqual(4, loanBefore.IncomeSources.Count);
                Assert.AreEqual(2, appBefore.aBBaseI);
                Assert.AreEqual(4, appBefore.aCBaseI);
                loanBefore.Save();
                var xml = TestUtilities.ReadFile("ULDD_IMPORTER_TEST.xml");

                var importer = new UlddImporter();
                importer.ImportIntoExisting(testLoan.Id, xml);

                var loanAfter = testLoan.CreateNewPageDataWithBypass();
                loanAfter.InitLoad();
                Assert.AreEqual(true, loanAfter.sIsIncomeCollectionEnabled);
                Assert.AreEqual(2, loanAfter.IncomeSources.Count);
                Assert.AreEqual(3000, loanAfter.GetAppData(0).aBBaseI);
                Assert.AreEqual(1500, loanAfter.GetAppData(0).aCBaseI);
            }
        }
    }
}
