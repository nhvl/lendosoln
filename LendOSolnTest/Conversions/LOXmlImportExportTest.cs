﻿using System;
using DataAccess;
using LendersOffice.Conversions;
using LendersOffice.Security;
using LendOSolnTest.Common;
using NUnit.Framework;
using LendersOffice.Constants;

namespace LendOSolnTest.Conversions
{
    [TestFixture]
    public class LOXmlImportExportTest
    {
        AbstractUserPrincipal x_principal = null;

        private AbstractUserPrincipal m_currentPrincipal
        {
            get
            {
                if (null == x_principal)
                {
                    x_principal = LoginTools.LoginAs(TestAccountType.LoTest001);
                }

                return x_principal;
            }

        }

        [SetUp]
        public void Setup()
        {
            System.Threading.Thread.CurrentPrincipal = m_currentPrincipal;
        }

        [Test]
        public void TestCountyRemapOnImport()
        {
            string xml = TestUtilities.ReadFile(@"LOXml\LOXmlQuery_CountyRemapTest.xml");
                        CLoanFileCreator fileCreator = CLoanFileCreator.GetCreator(m_currentPrincipal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
            Guid sLId = fileCreator.CreateBlankLoanFile();
            LOFormatImporter.Import(sLId, m_currentPrincipal, xml);

                        CPageData dataLoan = CreatePageData(sLId);
            dataLoan.InitLoad();

            Assert.That(dataLoan.sSpCounty.Equals("Lane"), "County was not remapped");

            Tools.DeclareLoanFileInvalid(m_currentPrincipal, sLId, false, false);
        }

        [Test]
        public void TestImportExport()
        {
            #region Create new loan file and set some data
            CLoanFileCreator fileCreator = CLoanFileCreator.GetCreator(m_currentPrincipal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
            Guid sLId = fileCreator.CreateBlankLoanFile();

            CPageData dataLoan = CreatePageData(sLId);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            dataLoan.sSpAddr = "sSpAddr";
            dataLoan.sSpCity = "sSpCity";
            dataLoan.sSpState = "CA";
            dataLoan.sSpZip = "92805";
            dataLoan.sTerm_rep = "360";
            dataLoan.sDue_rep = "180";
            dataLoan.sLPurposeT = E_sLPurposeT.Refin;

            CAppData dataApp = dataLoan.GetAppData(0);
            dataApp.aBFirstNm = "aBFirstNm";
            dataApp.aBLastNm = "aBLastNm";
            dataApp.aBMidNm = "aBMidNm";
            dataApp.aBSsn = "111-11-1111";
            dataApp.aBAddr = "aBAddr";
            dataApp.aBCity = "aBCity";
            dataApp.aBState = "CA";

            dataApp.aCFirstNm = "aCFirstNm";
            dataApp.aCLastNm = "aCLastNm";
            dataApp.aCMidNm = "aCMidNm";
            dataApp.aCSsn = "222-22-2222";
            dataApp.aCAddr = "aCAddr";
            dataApp.aCCity = "aCCity";
            dataApp.aCState = "CA";

            ILiabilityRegular lia = dataApp.aLiaCollection.AddRegularRecord();
            lia.OwnerT = E_LiaOwnerT.Borrower;
            lia.AccNm = "AccNm01";
            lia.AccNum = "AccNum01";
            lia.ComNm = "ComNm01";
            lia.Bal_rep = "$1,000.00";
            lia.Update();

            lia = dataApp.aLiaCollection.AddRegularRecord();
            lia.OwnerT = E_LiaOwnerT.CoBorrower;
            lia.AccNm = "AccNm02";
            lia.AccNum = "AccNum02";
            lia.ComNm = "ComNm02";
            lia.Bal_rep = "$1,000.00";
            lia.Update();

            lia = dataApp.aLiaCollection.AddRegularRecord();
            lia.OwnerT = E_LiaOwnerT.Joint;
            lia.AccNm = "AccNm03";
            lia.AccNum = "AccNum03";
            lia.ComNm = "ComNm03";
            lia.Bal_rep = "$1,000.00";
            lia.Update();

            dataLoan.Save();

            #endregion

            #region Export Data to LOXml format
            string requestXml = TestUtilities.ReadFile(@"LOXml\LOXmlQuery_Blank.xml");

            string dataXml = LOFormatExporter.Export(sLId, m_currentPrincipal, requestXml);
            #endregion

            #region Import Data from LOXmlFormat.
            fileCreator = CLoanFileCreator.GetCreator(m_currentPrincipal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
            Guid import_sLId = fileCreator.CreateBlankLoanFile();

            LoFormatImporterResult importResult = LOFormatImporter.Import(import_sLId, m_currentPrincipal, dataXml);
            string importXml = importResult.ErrorMessages;
            Assert.AreEqual(string.Empty, importXml.Trim(), "Import Xml");

            #endregion

            #region Re-export data.
            string newDataXml = LOFormatExporter.Export(import_sLId, m_currentPrincipal, requestXml);
            #endregion

            #region Verify the xml
            Assert.AreEqual(dataXml, newDataXml);

            #endregion

            #region Delete loan file
            Tools.DeclareLoanFileInvalid(m_currentPrincipal, sLId, false, false);
            Tools.DeclareLoanFileInvalid(m_currentPrincipal, import_sLId, false, false);
            #endregion
        }

        [Test]
        [Category(CategoryConstants.ConnectionDependent)]
        [Category(CategoryConstants.ExpectFailureOnLocalhost)]
        public void TestReissueInstantViewCreditReport()
        {
            #region Create new loan file and set some data
            CLoanFileCreator fileCreator = CLoanFileCreator.GetCreator(m_currentPrincipal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
            Guid sLId = fileCreator.CreateBlankLoanFile();

            CPageData dataLoan = CreatePageData(sLId);
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(0);
            Assert.AreEqual(0, dataApp.aLiaCollection.CountRegular);

            #endregion

            string xmlData = TestUtilities.ReadFile(@"LOXml\LOXmlQuery_ReissueInstantView.xml");
            LoFormatImporterResult importResult = LOFormatImporter.Import(sLId, m_currentPrincipal, xmlData);
            string response = importResult.ErrorMessages;

            Assert.AreEqual(string.Empty, response.Trim());

            dataLoan = CreatePageData(sLId);
            dataLoan.InitLoad();

            dataApp = dataLoan.GetAppData(0);
            Assert.That(dataApp.aLiaCollection.CountRegular > 0);

            #region Delete loan file
            Tools.DeclareLoanFileInvalid(m_currentPrincipal, sLId, false, false);
            #endregion
        }

        [Test]
        [Category(CategoryConstants.ConnectionDependent)]
        [Category(CategoryConstants.ExpectFailureOnLocalhost)]
        public void TestOrderNewCreditReport()
        {
            #region Create new loan file and set some data
            CLoanFileCreator fileCreator = CLoanFileCreator.GetCreator(m_currentPrincipal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
            Guid sLId = fileCreator.CreateBlankLoanFile();

            CPageData dataLoan = CreatePageData(sLId);
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(0);
            Assert.AreEqual(0, dataApp.aLiaCollection.CountRegular);

            #endregion

            string xmlData = TestUtilities.ReadFile(@"LOXml\LOXmlQuery_OrderNewCredit.xml");
            LoFormatImporterResult importResult = LOFormatImporter.Import(sLId, m_currentPrincipal, xmlData);
            string response = importResult.ErrorMessages;
            Assert.AreEqual(string.Empty, response.Trim());

            dataLoan = CreatePageData(sLId);
            dataLoan.InitLoad();

            dataApp = dataLoan.GetAppData(0);
            Assert.That(dataApp.aLiaCollection.CountRegular > 0);

            #region Delete loan file
            Tools.DeclareLoanFileInvalid(m_currentPrincipal, sLId, false, false);
            #endregion
        }

        private CPageData CreatePageData(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(LOXmlImportExportTest));
        }
    }
}
