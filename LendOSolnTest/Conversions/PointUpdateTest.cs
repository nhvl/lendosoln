﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using LendOSolnTest.Common;
using System.IO;
using LendersOfficeApp.LegacyLink.CalyxPoint;
using System.Collections.Specialized;

using PointInteropLib;
using LendersOffice.Constants;
using System.Linq;
using LendersOffice.Common;
using LqbGrammar.DataTypes;
using DataAccess;

namespace LendOSolnTest.Conversions
{
    [TestFixture]
    public class PointUpdateTest
    {
        [Test]
        public void ImportUpdateTest()
        {
            using (var testLoan = LoanForIntegrationTest.Create(TestAccountType.Corporate_LoanOfficer, useUladDataLayer: false))
            {
                var loanData = new CImportPointData(testLoan.Id);

                loanData.InitSave(ConstAppDavid.SkipVersionCheck);

                PointDataConversion pointConversion = new PointDataConversion(testLoan.Principal.BrokerId, testLoan.Id);
                pointConversion.UseLoanNumberFromPointFile = false;

                DataFile df = new DataFile();
                NameValueCollection colInput;
                using (Stream file = TestUtilities.GetFileStream("Filled-Out Loan.BRW"))
                {
                    colInput = df.ReadStream(file);
                }

                pointConversion.TransferDataFromPoint(loanData, new NameValueCollectionWrapper(colInput), 0);       //first import
                loanData.Save();

                loanData = new CImportPointData(testLoan.Id);
                loanData.InitLoad();

                var y = new NameValueCollection();
                NameValueCollectionWrapper wr = new NameValueCollectionWrapper(y);
                pointConversion.TransferDataToPoint(loanData, wr, 0);            //export out 1 

                loanData = new CImportPointData(testLoan.Id);

                using (Stream file = TestUtilities.GetFileStream("Filled-Out Loan.BRW"))
                {
                    colInput = df.ReadStream(file);
                }

                loanData.InitSave(ConstAppDavid.SkipVersionCheck);
                pointConversion.TransferDataFromPoint(loanData, new NameValueCollectionWrapper(colInput), 0, true);      //import in 1
                loanData.Save();

                loanData = new CImportPointData(testLoan.Id);
                loanData.InitLoad();
                var z = new NameValueCollection();

                pointConversion.TransferDataToPoint(loanData, new NameValueCollectionWrapper(z), 0);     //export out 2

                var fields = new SortedDictionary<short, string>();
                var fields2 = new SortedDictionary<short, string>();
                for (int i = 0; i < y.Count; i++)
                {
                    fields.Add(Convert.ToInt16(y.GetKey(i)), y[i]);
                }

                for (int i = 0; i < z.Count; i++)
                {
                    fields2.Add(Convert.ToInt16(z.GetKey(i)), z[i]);
                }

                Assert.That(fields.Count, Is.EqualTo(fields2.Count));

                foreach (var x in fields)
                {
                    Assert.That(fields2.ContainsKey(x.Key), x.Key.ToString());
                    Assert.That(fields2[x.Key], Is.EqualTo(x.Value), x.Key.ToString() + " val fail ");
                }
            }
        }

        [Test]
        public void TransferDataFromPoint_ConsumerIncomeFieldsOnOldIncomeFields_SetsFieldsAsExpected()
        {
            using (var testLoan = LoanForIntegrationTest.Create(TestAccountType.Corporate_LoanOfficer, useUladDataLayer: false))
            {
                var loanBefore = testLoan.CreateNewPageDataWithBypass();
                loanBefore.InitSave(ConstAppDavid.SkipVersionCheck);
                var appBefore = loanBefore.GetAppData(0);
                // Add other income items to ensure they are cleared out on import.
                var otherIncome = new List<DataAccess.OtherIncome>();
                for (var i = 0; i < 4; ++i)
                {
                    otherIncome.Add(new DataAccess.OtherIncome
                    {
                        Desc = $"Other {i + 1}",
                        Amount = i + 1,
                        IsForCoBorrower = i > 2
                    });
                }
                appBefore.aOtherIncomeList = otherIncome;
                Assert.AreEqual(4, loanBefore.GetAppData(0).aOtherIncomeList.Count);
                loanBefore.Save();
                var loanForImport = new CImportPointData(testLoan.Id);
                loanForImport.InitSave();
                var df = new DataFile();
                NameValueCollection colInput;
                using (Stream file = TestUtilities.GetFileStream("POINT_IMPORT_INCOME_TEST.BRW"))
                {
                    colInput = df.ReadStream(file);
                }

                var pointConversion = new PointDataConversion(testLoan.Principal.BrokerId, testLoan.Id);
                pointConversion.UseLoanNumberFromPointFile = false;
                pointConversion.TransferDataFromPoint(loanForImport, new NameValueCollectionWrapper(colInput), 0);
                loanForImport.Save();

                var loanAfter = new CImportPointData(testLoan.Id);
                loanAfter.InitLoad();
                var appAfter = loanAfter.GetAppData(0);
                AssertLegacyIncomeFieldsPopulated(appAfter, usingIncomeSourceCollection: false);
            }
        }

        [Test]
        public void TransferDataFromPoint_ConsumerIncomeFieldsOnIncomeSourceCollection_SetsExpectedValues()
        {
            using (var testLoan = LoanForIntegrationTest.Create(TestFileType.TestFile))
            {
                CPageData loanBefore = testLoan.CreateNewPageDataWithBypass();
                loanBefore.InitSave();
                loanBefore.MigrateToUseLqbCollections();
                loanBefore.MigrateToIncomeSourceCollection();
                Assert.IsTrue(loanBefore.sIsIncomeCollectionEnabled);
                var appBefore = loanBefore.GetAppData(0);
                loanBefore.AddCoborrowerToLegacyApplication(appBefore.aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());

                // Add other income items to ensure they are cleared out on import.
                for (var i = 0; i < 4; ++i)
                {
                    var consumerId = i < 2
                        ? appBefore.aBConsumerId.ToIdentifier<DataObjectKind.Consumer>()
                        : appBefore.aCConsumerId.ToIdentifier<DataObjectKind.Consumer>();

                    loanBefore.AddIncomeSource(
                        consumerId,
                        new LendingQB.Core.Data.IncomeSource
                        {
                            IncomeType = (IncomeType)i,
                            Description = DescriptionField.Create(i.ToString()),
                            MonthlyAmountData = Money.Create(i)
                        });
                }
                loanBefore.Save();
                var loan = new CImportPointData(testLoan.Id);
                loan.InitSave();
                Assert.AreEqual(4, loan.IncomeSources.Count);
                var df = new DataFile();
                NameValueCollection colInput;
                using (Stream file = TestUtilities.GetFileStream("POINT_IMPORT_INCOME_TEST.BRW"))
                {
                    colInput = df.ReadStream(file);
                }

                var pointConversion = new PointDataConversion(testLoan.Principal.BrokerId, testLoan.Id);
                pointConversion.UseLoanNumberFromPointFile = false;
                pointConversion.TransferDataFromPoint(loan, new NameValueCollectionWrapper(colInput), 0);
                loan.Save();

                var loanAfter = new CImportPointData(testLoan.Id);
                loanAfter.InitLoad();
                var appAfter = loanAfter.GetAppData(0);
                AssertLegacyIncomeFieldsPopulated(appAfter, usingIncomeSourceCollection: true);
                Assert.AreEqual(13, loanAfter.IncomeSources.Count);
                // Want to make sure we're creating the "other" incomes with the correct type.
                Assert.AreEqual(1, loanAfter.IncomeSources.Values.Count(i => i.IncomeType == LqbGrammar.DataTypes.IncomeType.AccessoryUnitIncome));
                Assert.AreEqual(1, loanAfter.IncomeSources.Values.Count(i => i.IncomeType == LqbGrammar.DataTypes.IncomeType.MilitaryBasePay));
                Assert.AreEqual(1, loanAfter.IncomeSources.Values.Count(i => i.IncomeType == LqbGrammar.DataTypes.IncomeType.PublicAssistance));
            }
        }

        [Test]
        public void TransferDataFromPoint_SubjectPropertyNetRentUsingIncomeSourceCollection_SetsGrossRent()
        {
            using (var testLoan = LoanForIntegrationTest.Create(TestFileType.TestFile))
            {
                CPageData loanBefore = testLoan.CreateNewPageDataWithBypass();
                loanBefore.InitSave(ConstAppDavid.SkipVersionCheck);
                loanBefore.MigrateToUseLqbCollections();
                loanBefore.MigrateToIncomeSourceCollection();
                Assert.IsTrue(loanBefore.sIsIncomeCollectionEnabled);
                var appBefore = loanBefore.GetAppData(0);
                loanBefore.AddCoborrowerToLegacyApplication(appBefore.aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                loanBefore.Save();
                var loan = new CImportPointData(testLoan.Id);
                loan.InitSave();
                var df = new DataFile();
                NameValueCollection colInput;
                using (Stream file = TestUtilities.GetFileStream("POINT_SUBJ_PROP_NET_RENT.BRW"))
                {
                    colInput = df.ReadStream(file);
                }

                var pointConversion = new PointDataConversion(testLoan.Principal.BrokerId, testLoan.Id);
                pointConversion.UseLoanNumberFromPointFile = false;
                pointConversion.TransferDataFromPoint(loan, new NameValueCollectionWrapper(colInput), 0);
                loan.Save();

                var loanAfter = new CImportPointData(testLoan.Id);
                loanAfter.InitLoad();
                Assert.AreEqual(3, loanAfter.sSpGrossRent);
            }
        }

        [Test]
        public void TransferDataFromPoint_BlankCurrentEmployment_DoesNotCreatePrimaryEmploymentRecords()
        {
            using (var testLoan = LoanForIntegrationTest.Create(TestFileType.RegularLoan))
            {
                CPageData loanBefore = testLoan.CreateNewPageDataWithBypass();
                loanBefore.InitLoad();
                Assert.AreEqual(1, loanBefore.nApps);
                Assert.IsNull(loanBefore.GetAppData(0).aBEmpCollection.GetPrimaryEmp(forceCreate: false));
                Assert.IsNull(loanBefore.GetAppData(0).aCEmpCollection.GetPrimaryEmp(forceCreate: false));

                var loan = new CImportPointData(testLoan.Id);
                loan.InitSave();
                var df = new DataFile();
                NameValueCollection colInput;
                using (Stream file = TestUtilities.GetFileStream("POINT_IMPORT_INCOME_TEST.BRW"))
                {
                    colInput = df.ReadStream(file);
                }

                var pointConversion = new PointDataConversion(testLoan.Principal.BrokerId, testLoan.Id);
                pointConversion.UseLoanNumberFromPointFile = false;
                pointConversion.TransferDataFromPoint(loan, new NameValueCollectionWrapper(colInput), 0);
                loan.Save();

                var loanAfter = testLoan.CreateNewPageDataWithBypass();
                loanAfter.InitLoad();
                Assert.AreEqual(1, loanAfter.nApps);
                Assert.IsNull(loanAfter.GetAppData(0).aBEmpCollection.GetPrimaryEmp(forceCreate: false));
                Assert.IsNull(loanAfter.GetAppData(0).aCEmpCollection.GetPrimaryEmp(forceCreate: false));
            }
        }

        [Test]
        public void TransferDataFromPoint_FilledOutCurrentEmployment_CreatesAndSetsPrimaryEmploymentRecords()
        {
            using (var testLoan = LoanForIntegrationTest.Create(TestFileType.RegularLoan))
            {
                CPageData loanBefore = testLoan.CreateNewPageDataWithBypass();
                loanBefore.InitLoad();
                Assert.AreEqual(1, loanBefore.nApps);
                Assert.IsNull(loanBefore.GetAppData(0).aBEmpCollection.GetPrimaryEmp(forceCreate: false));
                Assert.IsNull(loanBefore.GetAppData(0).aCEmpCollection.GetPrimaryEmp(forceCreate: false));

                var loan = new CImportPointData(testLoan.Id);
                loan.InitSave();
                var df = new DataFile();
                NameValueCollection colInput;
                using (Stream file = TestUtilities.GetFileStream("Filled-Out Loan.BRW"))
                {
                    colInput = df.ReadStream(file);
                }

                var pointConversion = new PointDataConversion(testLoan.Principal.BrokerId, testLoan.Id);
                pointConversion.UseLoanNumberFromPointFile = false;
                pointConversion.TransferDataFromPoint(loan, new NameValueCollectionWrapper(colInput), 0);
                loan.Save();

                var loanAfter = testLoan.CreateNewPageDataWithBypass();
                loanAfter.InitLoad();
                Assert.AreEqual(1, loanAfter.nApps);
                IPrimaryEmploymentRecord aBPrimaryEmp = loanAfter.GetAppData(0).aBEmpCollection.GetPrimaryEmp(forceCreate: false);
                IPrimaryEmploymentRecord aCPrimaryEmp = loanAfter.GetAppData(0).aCEmpCollection.GetPrimaryEmp(forceCreate: false);
                Assert.IsNotNull(aBPrimaryEmp);
                Assert.IsNotNull(aCPrimaryEmp);
                Assert.AreEqual("THE MAN", aBPrimaryEmp.JobTitle);
                Assert.AreEqual("THE LADY", aCPrimaryEmp.JobTitle);
                Assert.AreEqual("MLINK", aBPrimaryEmp.EmplrNm);
                Assert.AreEqual("EMPLOYER2", aCPrimaryEmp.EmplrNm);
            }
        }

        [Test]
        public void TransferDataFromPoint_CalledWithLoanNotMatchingConstructor_ThrowsException()
        {
            using (var testLoan = LoanForIntegrationTest.Create(TestAccountType.Corporate_LoanOfficer, useUladDataLayer: false))
            {
                var loanBefore = new CImportPointData(testLoan.Id);

                var pointConversion = new PointDataConversion(testLoan.Principal.BrokerId, Guid.NewGuid());

                // Verify both import methods.
                Assert.Throws<CBaseException>(() => pointConversion.TransferDataFromPoint(loanBefore, null, 0));
                Assert.Throws<CBaseException>(() => pointConversion.TransferDataFromPoint(loanBefore, null, 0, true));
            }
        }

        [Test]
        public void TransferDataToPoint_CalledWithLoanNotMatchingConstructor_ThrowsException()
        {
            using (var testLoan = LoanForIntegrationTest.Create(TestAccountType.Corporate_LoanOfficer, useUladDataLayer: false))
            {
                var loanBefore = new CImportPointData(testLoan.Id);

                var pointConversion = new PointDataConversion(testLoan.Principal.BrokerId, Guid.NewGuid());

                Assert.Throws<CBaseException>(() => pointConversion.TransferDataToPoint(loanBefore, null, 0));
            }
        }

        private static void AssertLegacyIncomeFieldsPopulated(DataAccess.CAppData appAfter, bool usingIncomeSourceCollection)
        {
            Assert.AreEqual(1, appAfter.aBBaseI);
            Assert.AreEqual(2, appAfter.aBOvertimeI);
            Assert.AreEqual(3, appAfter.aBBonusesI);
            Assert.AreEqual(4, appAfter.aBCommisionI);
            Assert.AreEqual(5, appAfter.aBDividendI);
            Assert.AreEqual(6, appAfter.aCBaseI);
            Assert.AreEqual(7, appAfter.aCOvertimeI);
            Assert.AreEqual(8, appAfter.aCBonusesI);
            Assert.AreEqual(9, appAfter.aCCommisionI);
            Assert.AreEqual(10, appAfter.aCDividendI);

            if (usingIncomeSourceCollection)
            {
                var populatedOtherIncomes = appAfter.aOtherIncomeList.Where(i => i.Amount > 0);
                Assert.AreEqual(3, populatedOtherIncomes.Count());

                var borrAccessoryUnitIncome = populatedOtherIncomes.Single(i => !i.IsForCoBorrower && i.Desc.Equals("ACCESSORY UNIT INCOME", StringComparison.OrdinalIgnoreCase));
                Assert.AreEqual(20, borrAccessoryUnitIncome.Amount);

                var borrMilitaryBasePay = populatedOtherIncomes.Single(i => !i.IsForCoBorrower && i.Desc.Equals("MILITARY BASE PAY", StringComparison.OrdinalIgnoreCase));
                Assert.AreEqual(30, borrMilitaryBasePay.Amount);

                var coborrOtherIncome = populatedOtherIncomes.Single(i => i.IsForCoBorrower);
                StringAssert.AreEqualIgnoringCase("PUBLIC ASSISTANCE", coborrOtherIncome.Desc);
                Assert.AreEqual(40, coborrOtherIncome.Amount);
            }
            else
            {
                var populatedOtherIncomes = appAfter.aOtherIncomeList.Where(i => i.Amount > 0);
                Assert.AreEqual(2, populatedOtherIncomes.Count());

                var borrOtherIncome = populatedOtherIncomes.Single(i => !i.IsForCoBorrower);
                StringAssert.AreEqualIgnoringCase("ACCESSORY UNIT INCOME; MILITARY BASE PAY", borrOtherIncome.Desc);
                Assert.AreEqual(50, borrOtherIncome.Amount);

                var coborrOtherIncome = populatedOtherIncomes.Single(i => i.IsForCoBorrower);
                StringAssert.AreEqualIgnoringCase("PUBLIC ASSISTANCE", coborrOtherIncome.Desc);
                Assert.AreEqual(40, coborrOtherIncome.Amount);
            }
        }

    }
}
