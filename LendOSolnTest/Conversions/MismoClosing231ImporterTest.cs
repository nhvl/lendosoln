﻿namespace LendOSolnTest.Conversions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Conversions.MismoClosing231;
    using LendOSolnTest.Common;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.IntegrationTest)]
    public class MismoClosing231ImporterTest
    {
        [Test]
        public void Import_UsingLegacyIncomeFields_SetsFieldsAsExpected()
        {
            using (var testLoan = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: false))
            {
                var loanBefore = testLoan.CreateNewPageDataWithBypass();
                loanBefore.InitSave();
                var appBefore = loanBefore.GetAppData(0);
                appBefore.aBSsn = "111111111";
                appBefore.aCSsn = "222222222";
                loanBefore.Save();
                Assert.AreEqual(false, loanBefore.sIsIncomeCollectionEnabled);
                var xml = TestUtilities.ReadFile("MISMO231_INCOME_IMPORT_TEST.xml");

                var importer = new MismoClosing231Importer();
                importer.Import(testLoan.Id, xml);

                var loanAfter = testLoan.CreateNewPageDataWithBypass();
                loanAfter.InitLoad();
                AssertLegacyFieldsPopulated(loanAfter.GetAppData(0));
            }
        }

        [Test]
        public void Import_UsingLegacyIncomeFieldsWithExistingData_OverwritesFields()
        {
            using (var testLoan = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: false))
            {
                var loanBefore = testLoan.CreateNewPageDataWithBypass();
                loanBefore.InitSave();
                var appBefore = loanBefore.GetAppData(0);
                appBefore.aBSsn = "111111111";
                appBefore.aCSsn = "222222222";
                PopulateIncomeFields(appBefore);
                loanBefore.Save();
                Assert.AreEqual(false, loanBefore.sIsIncomeCollectionEnabled);
                Assert.Greater(appBefore.aBTotI, 0);
                Assert.Greater(appBefore.aCTotI, 0);
                var xml = TestUtilities.ReadFile("MISMO231_INCOME_IMPORT_TEST.xml");

                var importer = new MismoClosing231Importer();
                importer.Import(testLoan.Id, xml);

                var loanAfter = testLoan.CreateNewPageDataWithBypass();
                loanAfter.InitLoad();
                AssertLegacyFieldsPopulated(loanAfter.GetAppData(0));
            }
        }

        [Test]
        public void Import_UsingIncomeSourceCollection_SetsFieldsAsExpected()
        {
            using (var testLoan = LoanForIntegrationTest.Create(TestFileType.TestFile))
            {
                var loanBefore = testLoan.CreateNewPageDataWithBypass();
                loanBefore.InitSave();
                loanBefore.MigrateToUseLqbCollections();
                loanBefore.MigrateToIncomeSourceCollection();
                Assert.IsTrue(loanBefore.sIsIncomeCollectionEnabled);
                var appBefore = loanBefore.GetAppData(0);
                appBefore.aBSsn = "111111111";
                appBefore.aCSsn = "222222222";
                loanBefore.AddCoborrowerToLegacyApplication(appBefore.aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                loanBefore.Save();
                var xml = TestUtilities.ReadFile("MISMO231_INCOME_IMPORT_TEST.xml");

                var importer = new MismoClosing231Importer();
                importer.Import(testLoan.Id, xml);

                var loanAfter = testLoan.CreateNewPageDataWithBypass();
                loanAfter.InitLoad();
                AssertLegacyFieldsPopulated(loanAfter.GetAppData(0));
                Assert.AreEqual(12, loanAfter.IncomeSources.Count);
                Assert.AreEqual(6, loanAfter.ConsumerIncomeSources.Values.Count(a => a.ConsumerId.Value == loanAfter.GetAppData(0).aBConsumerId));
                Assert.AreEqual(6, loanAfter.ConsumerIncomeSources.Values.Count(a => a.ConsumerId.Value == loanAfter.GetAppData(0).aCConsumerId));
                // Make sure the other incomes came over as expected.
                Assert.AreEqual(2, loanAfter.IncomeSources.Values.Count(i => i.IncomeType == LqbGrammar.DataTypes.IncomeType.Other && i.Description.ToString().Equals("Other Income", StringComparison.OrdinalIgnoreCase)));
            }
        }

        [Test]
        public void Import_UsingIncomeSourceCollectionWithExistingRecords_OverwritesExistingRecords()
        {
            using (var testLoan = LoanForIntegrationTest.Create(TestFileType.TestFile))
            {
                var loanBefore = testLoan.CreateNewPageDataWithBypass();
                loanBefore.InitSave();
                Assert.IsFalse(loanBefore.sIsIncomeCollectionEnabled);
                var appBefore = loanBefore.GetAppData(0);
                appBefore.aBSsn = "111111111";
                appBefore.aCSsn = "222222222";
                PopulateIncomeFields(appBefore);
                loanBefore.AddCoborrowerToLegacyApplication(appBefore.aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                loanBefore.MigrateToUseLqbCollections();
                loanBefore.MigrateToIncomeSourceCollection();
                Assert.IsTrue(loanBefore.sIsIncomeCollectionEnabled);
                loanBefore.Save();
                Assert.Greater(appBefore.aBTotI, 0);
                Assert.Greater(appBefore.aCTotI, 0);
                var xml = TestUtilities.ReadFile("MISMO231_INCOME_IMPORT_TEST.xml");

                var importer = new MismoClosing231Importer();
                importer.Import(testLoan.Id, xml);

                var loanAfter = testLoan.CreateNewPageDataWithBypass();
                loanAfter.InitLoad();
                AssertLegacyFieldsPopulated(loanAfter.GetAppData(0));
                Assert.AreEqual(12, loanAfter.IncomeSources.Count);
                Assert.AreEqual(6, loanAfter.ConsumerIncomeSources.Values.Count(a => a.ConsumerId.Value == loanAfter.GetAppData(0).aBConsumerId));
                Assert.AreEqual(6, loanAfter.ConsumerIncomeSources.Values.Count(a => a.ConsumerId.Value == loanAfter.GetAppData(0).aCConsumerId));
                // Make sure the other incomes came over as expected.
                Assert.AreEqual(2, loanAfter.IncomeSources.Values.Count(i => i.IncomeType == LqbGrammar.DataTypes.IncomeType.Other && i.Description.ToString().Equals("Other Income", StringComparison.OrdinalIgnoreCase)));
            }
        }

        [Test]
        public void Import_SubjectPropertyNetRentUsingIncomeSourceCollection_SetsSpGrossRent()
        {
            using (var testLoan = LoanForIntegrationTest.Create(TestFileType.TestFile))
            {
                var loanBefore = testLoan.CreateNewPageDataWithBypass();
                loanBefore.InitSave();
                loanBefore.MigrateToUseLqbCollections();
                loanBefore.MigrateToIncomeSourceCollection();
                Assert.IsTrue(loanBefore.sIsIncomeCollectionEnabled);
                var appBefore = loanBefore.GetAppData(0);
                appBefore.aBSsn = "111111111";
                appBefore.aCSsn = "222222222";
                loanBefore.AddCoborrowerToLegacyApplication(appBefore.aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                loanBefore.Save();
                Assert.AreEqual(0, appBefore.aBTotI);
                Assert.AreEqual(0, appBefore.aCTotI);
                var xml = TestUtilities.ReadFile("MISMO231_SUBJ_PROP_NET_RENT.xml");

                var importer = new MismoClosing231Importer();
                importer.Import(testLoan.Id, xml);

                var loanAfter = testLoan.CreateNewPageDataWithBypass();
                loanAfter.InitLoad();
                Assert.AreEqual(3, loanAfter.sSpGrossRent);
            }
        }
        private static void PopulateIncomeFields(CAppData appBefore)
        {
            appBefore.aBBaseI = 101;
            appBefore.aBOvertimeI = 103;
            appBefore.aBBonusesI = 105;
            appBefore.aBCommisionI = 107;
            appBefore.aBDividendI = 109;
            appBefore.aCBaseI = 102;
            appBefore.aCOvertimeI = 104;
            appBefore.aCBonusesI = 106;
            appBefore.aCCommisionI = 108;
            appBefore.aCDividendI = 110;

            var otherIncomes = new List<OtherIncome>
            {
                new OtherIncome
                {
                    Desc = "Test 1",
                    Amount = 1,
                    IsForCoBorrower = true
                },
                new OtherIncome
                {
                    Desc = "Test 2",
                    Amount = 2,
                    IsForCoBorrower = false
                },
                new OtherIncome
                {
                    Desc = "Test 3",
                    Amount = 3,
                    IsForCoBorrower = true
                },
            };

            appBefore.aOtherIncomeList = otherIncomes;
        }

        private static void AssertLegacyFieldsPopulated(DataAccess.CAppData appAfter)
        {
            Assert.AreEqual(1, appAfter.aBBaseI);
            Assert.AreEqual(3, appAfter.aBOvertimeI);
            Assert.AreEqual(5, appAfter.aBBonusesI);
            Assert.AreEqual(7, appAfter.aBCommisionI);
            Assert.AreEqual(9, appAfter.aBDividendI);
            Assert.AreEqual(2, appAfter.aCBaseI);
            Assert.AreEqual(4, appAfter.aCOvertimeI);
            Assert.AreEqual(6, appAfter.aCBonusesI);
            Assert.AreEqual(8, appAfter.aCCommisionI);
            Assert.AreEqual(10, appAfter.aCDividendI);

            Assert.AreEqual(2, appAfter.aOtherIncomeList.Count(i => i.Amount > 0));

            var borrOtherIncome = appAfter.aOtherIncomeList.Single(i => !i.IsForCoBorrower && i.Amount > 0);
            Assert.AreEqual("Other Income", borrOtherIncome.Desc);
            Assert.AreEqual(24, borrOtherIncome.Amount);

            var coborrOtherIncome = appAfter.aOtherIncomeList.Single(i => i.IsForCoBorrower);
            StringAssert.AreEqualIgnoringCase("Other Income", coborrOtherIncome.Desc);
            Assert.AreEqual(12, coborrOtherIncome.Amount);
        }

    }
}
