﻿namespace LendOSolnTest.Conversions
{
    using System;
    using System.Linq;
    using LendersOffice.ObjLib.Conversions.LoanFileImportExport;
    using NUnit.Framework;

    [TestFixture]
    public class ValidatedImplicitIdentityKeyReferenceTest
    {
        [Test]
        public void ValidTest()
        {
            var validInstance = new UnvalidatedImplicitIdentityKeyReference()
            {
                ReferencingDataSource = "LOShare",
                ReferencingTableName = "FEE_SERVICE_REVISION",
                ReferencingColumnName = "FeeTemplateXmlContent",
                IdentityKeyDataSource = "LOShare",
                IdentityKeyTableName = "Region_Set",
                IdentityKeyColumName = "RegionId",
                Type = "XPathAndAttribute",
                XPath = @"/ArrayOfFeeServiceTemplate/FeeServiceTemplate/FeeServiceConditionSet/Condition[@FieldId=""csRegion""]/Condition[@FieldId=""csRegion""]",
                AttributeName = "key"
            };

            EnsureValid(validInstance);
        }

        [Test]
        public void InValidTest()
        {
            var validInstance = new UnvalidatedImplicitIdentityKeyReference()
            {
                ReferencingDataSource = "LOShare",
                ReferencingTableName = "FEE_SERVICE_REVISION",
                ReferencingColumnName = "FeeTemplateXmlContent",
                IdentityKeyDataSource = "LOShare",
                IdentityKeyTableName = "Region_Set",
                IdentityKeyColumName = "RegionId",
                Type = "XPathAndAttribute",
                XPath = @"/ArrayOfFeeServiceTemplate/FeeServiceTemplate/FeeServiceConditionSet/Condition[@FieldId=""csRegion""]/Condition[@FieldId=""csRegion""]",
                AttributeName = "key"
            };

            TestInvalidation(validInstance, "asdfg", u => u.ReferencingDataSource, (u, s) => u.ReferencingDataSource = s);
            TestInvalidation(validInstance, "as df", u => u.ReferencingTableName, (u, s) => u.ReferencingTableName = s);
            TestInvalidation(validInstance, "as fg", u => u.ReferencingColumnName, (u, s) => u.ReferencingColumnName = s);
            TestInvalidation(validInstance, "asdfg", u => u.IdentityKeyDataSource, (u, s) => u.IdentityKeyDataSource = s);
            TestInvalidation(validInstance, "as fg", u => u.IdentityKeyTableName, (u, s) => u.IdentityKeyTableName = s);
            TestInvalidation(validInstance, "as fg", u => u.IdentityKeyColumName, (u, s) => u.IdentityKeyColumName = s);
            TestInvalidation(validInstance, "asdfg", u => u.Type, (u, s) => u.Type = s);
            TestInvalidation(validInstance, "as[fg", u => u.XPath, (u, s) => u.XPath = s);
            TestInvalidation(validInstance, "as df", u => u.AttributeName, (u, s) => u.AttributeName = s);
        }

        private void TestInvalidation(
            UnvalidatedImplicitIdentityKeyReference validInstance, 
            string invalidString,
            Func<UnvalidatedImplicitIdentityKeyReference, string> getter, 
            Action<UnvalidatedImplicitIdentityKeyReference, string> setter)
        {
            string old = getter(validInstance);
            setter(validInstance, invalidString);
            EnsureInvalid(validInstance);
            setter(validInstance, old);
            EnsureValid(validInstance);
        }

        private void EnsureValid(UnvalidatedImplicitIdentityKeyReference validInstance)
        {
            ValidatedImplicitIdentityKeyReference v;
            var reasonsInvalid = ValidatedImplicitIdentityKeyReference.GetValidInstanceOrReasonsInvalid(validInstance, out v);
            Assert.That(!reasonsInvalid.Any());
            Assert.That(v != null);
            Assert.That(v.ReferencingDataSource == DataAccess.DataSrc.LOShare);
            Assert.That(v.IdentityKeyDataSource == DataAccess.DataSrc.LOShare);
            Assert.That(v.ReferencingTableName == validInstance.ReferencingTableName);
            Assert.That(v.IdentityKeyTableName == validInstance.IdentityKeyTableName);
            Assert.That(v.ReferencingColumnName == validInstance.ReferencingColumnName);
            Assert.That(v.IdentityKeyColumName == validInstance.IdentityKeyColumName);
            Assert.That(v.AttributeName == validInstance.AttributeName);
            Assert.That(v.XPath != null);
            Assert.That(v.XPath.Expression == validInstance.XPath);
        }

        private void EnsureInvalid(UnvalidatedImplicitIdentityKeyReference u)
        {
            ValidatedImplicitIdentityKeyReference v;
            var reasonsInvalid = ValidatedImplicitIdentityKeyReference.GetValidInstanceOrReasonsInvalid(u, out v);
            Assert.That(reasonsInvalid.Any());
            Assert.That(v == null);
        }

    }
}
