﻿namespace LendOSolnTest.Conversions
{
    using System;
    using System.Collections.Generic;
    using System.Xml.XPath;
    using LendersOffice.ObjLib.Conversions.LoanFileImportExport;
    using NUnit.Framework;
    using global::CommonProjectLib.Common;
    using System.Xml.Linq;

    [TestFixture]
    public class XmlReplacerTest
    {
        private static Dictionary<string, string> DestValueBySourceValue = new Dictionary<string, string>()
        {
            {"replaceMe", "gotHim"},
            {"", "replacedEmptyString"}
        };

        private static Dictionary<string, string> DestAttrValueBySourceAttrValue = new Dictionary<string, string>()
        {
            {"X", "bX"},
        };

        [Test]
        public void ReplaceXmlTextValues_GoodValuesTest()
        {
            string origXml = "<a><b id='X'><c>replaceMe</c><c></c><c><d>replaceMe</d>replaceMe</c></b><b><c>replaceMe</c></b></a>";

            // this xpath finds all a/b/c elements where c has text as it's child and b has an id attribute.
            string xPathString = "a/b[@id]/c[text()]"; 

            // note that it does not replace the empty string.  Also the attribute single quotes '' go to double quotes "".
            string expectedXml = @"<a><b id=""X""><c>gotHim</c><c></c><c><d>replaceMe</d>gotHim</c></b><b><c>replaceMe</c></b></a>";
            XDocument document = XDocument.Parse(origXml);
            Action<string> throwError = (s) => { throw new CBaseException($"no replacement found for {s}.", $"no replacement found for {s}."); };

            XPathExpression compiledXPath = XPathExpression.Compile(xPathString);
            XmlReplacer.ReplaceXmlTextValues(document, compiledXPath, DestValueBySourceValue, throwError);
            string resultXml = document.ToString(SaveOptions.DisableFormatting);
            Assert.AreEqual(expectedXml, resultXml, "Did not get expected xml after replacement.");
        }

        [Test]
        public void ReplaceXmlTextValues_BadValuesTest()
        {
            string origXml = "<a><b id='X'><c>replaceMe</c><c/><c>replaceMe<d>replaceMe</d></c></b><b><c>replaceMe</c></b></a>";

            // this xpath finds all a/b/c elements where c has text as it's child and b has an id attribute.
            string xPathString = "a/b[@id]/c[text()]";

            // note that it skips the c that is empty.  Also the attribute single quotes '' go to double quotes "".
            //string expectedXml = @"<a><b id=""X""><c>gotHim</c><c></c><c>gotHim<d>replaceMe</d></c></b><b><c>replaceMe</c></b></a>";
            XDocument document = XDocument.Parse(origXml);
            Action<string> throwError = (s) => { throw new CBaseException($"no replacement found for {s}.", $"no replacement found for {s}."); };

            XPathExpression compiledXPath = XPathExpression.Compile(xPathString);
            
            MLAssert.Throws<ArgumentNullException>(() =>  XmlReplacer.ReplaceXmlTextValues(null, compiledXPath, DestValueBySourceValue, throwError));
            MLAssert.Throws<ArgumentNullException>(() => XmlReplacer.ReplaceXmlTextValues(document, null, DestValueBySourceValue, throwError));
            MLAssert.Throws<ArgumentNullException>(() => XmlReplacer.ReplaceXmlTextValues(document, compiledXPath, null, throwError));
            MLAssert.Throws<CBaseException>(() => XmlReplacer.ReplaceXmlTextValues(document, compiledXPath, DestAttrValueBySourceAttrValue, throwError));
        }


        [Test]
        public void ReplaceXmlAttributeValues_GoodValuesTest()
        {
            string origXml = "<a><b id='X'><c/></b><b></b></a>";

            // this xpath finds all a/b where b has an id attribute.
            string xPathString = "a/b[@id]";
            string attributeName = "id";
            // note that it does not replace the empty string.  Also the attribute single quotes '' go to double quotes "".
            string expectedXml = @"<a><b id=""bX""><c /></b><b></b></a>";
            XDocument document = XDocument.Parse(origXml);
            Action<string> throwError = (s) => { throw new CBaseException($"no replacement found for {s}.", $"no replacement found for {s}."); };

            XPathExpression compiledXPath = XPathExpression.Compile(xPathString);
            XmlReplacer.ReplaceXmlAttributeValues(document, compiledXPath, attributeName, DestAttrValueBySourceAttrValue, throwError);
            string resultXml = document.ToString(SaveOptions.DisableFormatting);
            Assert.AreEqual(expectedXml, resultXml, "Did not get expected xml after replacement.");
        }

        [Test]
        public void ReplaceXmlAttributeValues_BadValuesTest()
        {
            string origXml = "<a><b id='X'><c/></b><b></b></a>";

            // this xpath finds all a/b/c elements where c has text as it's child and b has an id attribute.
            string xPathString = "a/b[@id]";
            string attributeName = "id";
            string badAttributeName = "ID";
            string badXPathString = "a/b";
            // note that it does not replace the empty string.  Also the attribute single quotes '' go to double quotes "".
            //string expectedXml = @"<a><b id=""X""><c/></b><b></b></a>";
            XDocument document = XDocument.Parse(origXml);
            Action<string> throwError = (s) => { throw new CBaseException($"no replacement found for {s}.", $"no replacement found for {s}."); };

            XPathExpression compiledXPath = XPathExpression.Compile(xPathString);
            XPathExpression badCompiledXPath = XPathExpression.Compile(badXPathString);

            MLAssert.Throws<ArgumentNullException>(() => XmlReplacer.ReplaceXmlAttributeValues(null, compiledXPath, attributeName, DestAttrValueBySourceAttrValue, throwError));
            MLAssert.Throws<ArgumentNullException>(() => XmlReplacer.ReplaceXmlAttributeValues(document, null, attributeName, DestAttrValueBySourceAttrValue, throwError));
            MLAssert.Throws<ArgumentNullException>(() => XmlReplacer.ReplaceXmlAttributeValues(document, compiledXPath, null, DestAttrValueBySourceAttrValue, throwError));
            MLAssert.Throws<ArgumentNullException>(() => XmlReplacer.ReplaceXmlAttributeValues(document, compiledXPath, attributeName, null, throwError));
            MLAssert.Throws<ArgumentException>(() => XmlReplacer.ReplaceXmlAttributeValues(document, compiledXPath, badAttributeName, DestAttrValueBySourceAttrValue, throwError));
            MLAssert.Throws<ArgumentException>(() => XmlReplacer.ReplaceXmlAttributeValues(document, badCompiledXPath, attributeName, DestAttrValueBySourceAttrValue, throwError));
            MLAssert.Throws<CBaseException>(() => XmlReplacer.ReplaceXmlAttributeValues(document, badCompiledXPath, attributeName, DestValueBySourceValue, throwError));
        }

        [Test]
        public void GetXmlAttributeValues_GoodTest()
        {
            string origXml = "<a><b id='X'><c/></b><b></b><b id='Y'/></a>";

            // this xpath finds all a/b elements where b has an id attribute.
            string xPathString = "a/b[@id]";
            string attributeName = "id";
            XDocument document = XDocument.Parse(origXml);

            var expectedSetOfValues = new HashSet<string>(new string[2] { "X", "Y" });

            XPathExpression compiledXPath = XPathExpression.Compile(xPathString);
            var attributeValues = XmlReplacer.GetXmlAttributeValues(document, compiledXPath, attributeName);

            Assert.That(expectedSetOfValues.SetEquals(attributeValues), "Did not get expected set of attribute values.");
        }

        [Test]
        public void GetXmlTextValues_GoodTest()
        {
            string origXml = "<a><b id='X'><c>one</c><c/><c>two<d>three</d>four</c></b><b><c>five</c></b></a>";

            // this xpath finds all a/b/c elements where b has an id attribute.
            string xPathString = "a/b[@id]/c";
            XDocument document = XDocument.Parse(origXml);

            var expectedSetOfValues = new HashSet<string>(new string[3] { "one", "two", "four"});

            XPathExpression compiledXPath = XPathExpression.Compile(xPathString);
            var textValues = XmlReplacer.GetXmlTextValues(document, compiledXPath);

            Assert.That(expectedSetOfValues.SetEquals(textValues), "Did not get expected set of text values.");
        }
    }
}
