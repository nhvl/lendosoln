/// Author: David Dao

using System;

using NUnit.Framework;

using DataAccess;
using LendersOffice.Audit;
using LendersOffice.Conversions;
using LendersOffice.Security;
using System.IO;
using LendOSolnTest.Common;
using LendersOffice.Constants;

namespace LendOSolnTest.Conversions
{
    [TestFixture]
	public class FannieMaeImportExportTest
	{
        AbstractUserPrincipal x_principal = null;

        private AbstractUserPrincipal m_currentPrincipal 
        {
            get 
            {
                if (null == x_principal) 
                {
                    x_principal = LoginTools.LoginAs(TestAccountType.LoTest001);
                }

                return x_principal;
            }

        }

        [TestFixtureSetUp]
        public void Setup()
        {
            
            
            
        }


        [Test]
        public void ImportExport() 
        {
            // This test will perform the following.
            // - Create new loan file.
            // - Set some data to loan file.
            // - Save loan file.
            // - Export to FNM format. 
            // - Save to temp file
            // - Import temp file into system.
            // - Run comparision between original and new file.

            #region Create new loan file and set some data to it.
            CLoanFileCreator fileCreator = CLoanFileCreator.GetCreator(m_currentPrincipal, E_LoanCreationSource.UserCreateFromBlank);
            Guid src_sLId = fileCreator.CreateBlankLoanFile();

            CPageData dataLoan = CreateFnmaData(src_sLId);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sDuCaseId = "sDuCaseId";
            dataLoan.sNoteIR_rep = "7.875%";
            dataLoan.sTerm_rep = "360";
            dataLoan.sDue_rep = "180";
            dataLoan.sSpAddr = "sSpAddr";
            dataLoan.sSpCity = "sSpCity";
            dataLoan.sSpState = "CA";
            dataLoan.sSpZip = "92805";

            CAppData dataApp = dataLoan.GetAppData(0);
            dataApp.aBFirstNm = "aBFirstNm";
            dataApp.aBLastNm = "aBLastNm";
            dataApp.aBMidNm = "aBMidNm";
            dataApp.aBSsn = "111-11-1111";

            dataApp.aCFirstNm = "aCFirstNm";
            dataApp.aCLastNm = "aCLastNm";
            dataApp.aCMidNm = "aCMidNm";
            dataApp.aCSsn = "222-22-2222";

            dataLoan.Save();
            #endregion

            #region Export data to loan file.
            FannieMae32Exporter exporter = new FannieMae32Exporter(src_sLId);
            byte[] buffer = exporter.Export();
            #endregion

            #region Import data from buffer.
            Stream stream = new MemoryStream(buffer);
            dataLoan = FannieMaeImporter.Import(m_currentPrincipal, stream, FannieMaeImportSource.LendersOffice, false /*addEmployeeAsOfficialAgent*/, Guid.Empty /* templateId */, true /*import multiple apps*/);
            Guid dest_sLId = dataLoan.sLId;

            #endregion

            #region Load both src and dest files and compare value.
            CPageData srcDataLoan = CreateFnmaData(src_sLId);
            srcDataLoan.InitLoad();
            CAppData srcDataApp = srcDataLoan.GetAppData(0);

            CPageData destDataLoan = CreateFnmaData(dest_sLId);
            destDataLoan.InitLoad();
            CAppData destDataApp = destDataLoan.GetAppData(0);

            Assert.AreEqual(srcDataLoan.sDuCaseId, destDataLoan.sDuCaseId, "sDuCaseId");
            Assert.AreEqual(srcDataLoan.sNoteIR_rep, destDataLoan.sNoteIR_rep, "sNoteIR_rep");
            Assert.AreEqual(srcDataLoan.sTerm_rep, destDataLoan.sTerm_rep, "sTerm_rep");
            if (srcDataLoan.sBalloonPmt)
            {
                Assert.AreEqual(srcDataLoan.sDue_rep, destDataLoan.sDue_rep, "sDue_rep");
            }
            Assert.AreEqual(srcDataLoan.sSpAddr, destDataLoan.sSpAddr, "sSpAddr");
            Assert.AreEqual(srcDataLoan.sSpCity, destDataLoan.sSpCity, "sSpCity");
            Assert.AreEqual(srcDataLoan.sSpState, destDataLoan.sSpState, "sSpState");
                            
            Assert.AreEqual(srcDataApp.aBFirstNm, destDataApp.aBFirstNm, "aBFirstNm");
            Assert.AreEqual(srcDataApp.aBLastNm, destDataApp.aBLastNm, "aBLastNm");
            Assert.AreEqual(srcDataApp.aBMidNm, destDataApp.aBMidNm, "aBMidNm");
            Assert.AreEqual(srcDataApp.aBSsn, destDataApp.aBSsn, "aBSsn");
            Assert.AreEqual(srcDataApp.aCFirstNm, destDataApp.aCFirstNm, "aCFirstNm");
            Assert.AreEqual(srcDataApp.aCLastNm, destDataApp.aCLastNm, "aCLastNm");
            Assert.AreEqual(srcDataApp.aCMidNm, destDataApp.aCMidNm, "aCMidNm");
            Assert.AreEqual(srcDataApp.aCSsn, destDataApp.aCSsn, "aCSsn");

            #endregion

            #region Delete two loan files.
            Tools.DeclareLoanFileInvalid(m_currentPrincipal, src_sLId, false /* sendNotifications*/, false /* generateLinkLoanMsgEvent */);
            Tools.DeclareLoanFileInvalid(m_currentPrincipal, dest_sLId, false /* sendNotifications*/, false /* generateLinkLoanMsgEvent */);
            #endregion


        }

        private CPageData CreateFnmaData(Guid sLId) 
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(FannieMaeImportExportTest));
        }
	}
}
