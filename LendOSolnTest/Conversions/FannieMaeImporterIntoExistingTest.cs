﻿namespace LendOSolnTest.Conversions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Audit;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Conversions;
    using LendersOffice.Security;
    using LendOSolnTest.Common;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    public class FannieMaeImporterIntoExistingTest
    {
        private const string Borr1_aBFirstNm = "B1_aBFirstNm";
        private const string Borr1_aBLastNm = "B1_aBLastNm";
        private const string Borr1_aBSsn = "111-11-1111";

        private const string Borr1_aCFirstNm = "B1_aCFirstNm";
        private const string Borr1_aCLastNm = "B1_aCLastNm";
        private const string Borr1_aCSsn = "222-22-2222";

        private const string Borr2_aBFirstNm = "B2_aBFirstNm";
        private const string Borr2_aBLastNm = "B2_aBLastNm";
        private const string Borr2_aBSsn = "333-33-3333";

        private const string Borr2_aCFirstNm = "B2_aCFirstNm";
        private const string Borr2_aCLastNm = "B2_aCLastNm";
        private const string Borr2_aCSsn = "444-44-4444";

        private const string sTerm = "360";
        private const string sDue = "360";
        private const string sNoteIR = "7.875%";

        private AbstractUserPrincipal x_principal = null;

        [SetUp]
        public void Setup()
        {
            System.Threading.Thread.CurrentPrincipal = m_currentPrincipal;
        }

        
        private AbstractUserPrincipal m_currentPrincipal
        {
            get
            {
                if (null == x_principal)
                {
                    x_principal = LoginTools.LoginAs(TestAccountType.LoTest001);
                }
                return x_principal;
            }
        }

        private CPageData CreatePageData(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(FannieMaeImporterIntoExistingTest));
        }
        [Test]
        public void ImportSingleBorrowerWithSsnModifyTest()
        {
            #region Create new loan file
            CLoanFileCreator fileCreator = CLoanFileCreator.GetCreator(m_currentPrincipal, E_LoanCreationSource.UserCreateFromBlank);
            Guid sLId = fileCreator.CreateBlankLoanFile();
            #endregion

            #region Set some loan data
            CPageData dataLoan = CreatePageData(sLId);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sTerm_rep = sTerm;
            dataLoan.sDue_rep = sDue;
            dataLoan.sNoteIR_rep = sNoteIR;

            CAppData dataApp = dataLoan.GetAppData(0);
            dataApp.aBFirstNm = Borr1_aBFirstNm;
            dataApp.aBLastNm = Borr1_aBLastNm;
            dataApp.aBSsn = Borr1_aBSsn;

            ILiaCollection liaCollection = dataApp.aLiaCollection;
            ILiabilityRegular liaRegular = liaCollection.AddRegularRecord();
            liaRegular.ComNm = "BOR_1_ComNm_1";
            liaRegular.DebtT = E_DebtRegularT.Revolving;
            liaRegular.Bal_rep = "$1000";
            liaRegular.Update();

            liaRegular = liaCollection.AddRegularRecord();
            liaRegular.ComNm = "BOR_1_ComNm_2";
            liaRegular.DebtT = E_DebtRegularT.Revolving;
            liaRegular.Bal_rep = "$2000";
            liaRegular.Update();
            dataLoan.Save();
            #endregion

            #region Export data to FNMA
            FannieMae32Exporter exporter = new FannieMae32Exporter(sLId);
            string fnmaContent = exporter.ExportToString();
            #endregion

            #region Modify the borrower SSN
            dataLoan = CreatePageData(sLId);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sTerm_rep = "180";
            dataLoan.sDue_rep = "180";
            dataLoan.sNoteIR_rep = "1.000%";
            dataApp = dataLoan.GetAppData(0);
            dataApp.aBFirstNm = Borr1_aBFirstNm + "_EDIT";
            dataApp.aBLastNm = Borr1_aBLastNm + "_EDIT";
            dataApp.aBSsn = "999-99-9999";
            dataLoan.Save();

            #endregion

            #region Import FNMA data
            FannieMae32Importer importer = new FannieMae32Importer();

            importer.ImportIntoExisting(fnmaContent, sLId);
            #endregion

            #region Verify
            dataLoan = CreatePageData(sLId);
            dataLoan.InitLoad();
            Assert.AreEqual(1, dataLoan.nApps);

            Assert.AreEqual(sTerm, dataLoan.sTerm_rep, "sTerm");
            Assert.AreEqual(sDue, dataLoan.sDue_rep, "sDue");
            Assert.AreEqual(sNoteIR, dataLoan.sNoteIR_rep, "sNoteIR");

            dataApp = dataLoan.GetAppData(0);

            Assert.AreEqual(Borr1_aBFirstNm, dataApp.aBFirstNm, "aBFirstNm");
            Assert.AreEqual(Borr1_aBLastNm, dataApp.aBLastNm, "aBLastNm");
            Assert.AreEqual(Borr1_aBSsn, dataApp.aBSsn, "aBSsn");

            #endregion

            Tools.DeclareLoanFileInvalid(m_currentPrincipal, sLId, false, false);
        }

        [Test]
        public void ImportSingleBorrowerWithSpouseSsnModifyTest()
        {
            #region Create new loan file
            CLoanFileCreator fileCreator = CLoanFileCreator.GetCreator(m_currentPrincipal, E_LoanCreationSource.UserCreateFromBlank);
            Guid sLId = fileCreator.CreateBlankLoanFile();
            #endregion

            #region Set some loan data
            CPageData dataLoan = CreatePageData(sLId);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sTerm_rep = sTerm;
            dataLoan.sDue_rep = sDue;
            dataLoan.sNoteIR_rep = sNoteIR;

            CAppData dataApp = dataLoan.GetAppData(0);
            dataApp.aBFirstNm = Borr1_aBFirstNm;
            dataApp.aBLastNm = Borr1_aBLastNm;
            dataApp.aBSsn = Borr1_aBSsn;

            dataApp.aCFirstNm = Borr1_aCFirstNm;
            dataApp.aCLastNm = Borr1_aCLastNm;
            dataApp.aCSsn = Borr1_aCSsn;

            ILiaCollection liaCollection = dataApp.aLiaCollection;
            ILiabilityRegular liaRegular = liaCollection.AddRegularRecord();
            liaRegular.ComNm = "BOR_1_ComNm_1";
            liaRegular.DebtT = E_DebtRegularT.Revolving;
            liaRegular.Bal_rep = "$1000";
            liaRegular.Update();

            liaRegular = liaCollection.AddRegularRecord();
            liaRegular.ComNm = "BOR_1_ComNm_2";
            liaRegular.DebtT = E_DebtRegularT.Revolving;
            liaRegular.Bal_rep = "$2000";
            liaRegular.Update();
            dataLoan.Save();
            #endregion

            #region Export data to FNMA
            FannieMae32Exporter exporter = new FannieMae32Exporter(sLId);
            string fnmaContent = exporter.ExportToString();
            #endregion

            #region Modify the borrower SSN
            dataLoan = CreatePageData(sLId);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sTerm_rep = "180";
            dataLoan.sDue_rep = "180";
            dataLoan.sNoteIR_rep = "1.000%";
            dataApp = dataLoan.GetAppData(0);
            dataApp.aCFirstNm = Borr1_aBFirstNm + "_EDIT";
            dataApp.aCLastNm = Borr1_aBLastNm + "_EDIT";
            dataApp.aCSsn = "999-99-9999";
            dataLoan.Save();

            #endregion

            #region Import FNMA data
            FannieMae32Importer importer = new FannieMae32Importer();

            importer.ImportIntoExisting(fnmaContent, sLId);
            #endregion

            #region Verify
            dataLoan = CreatePageData(sLId);
            dataLoan.InitLoad();
            Assert.AreEqual(1, dataLoan.nApps);

            Assert.AreEqual(sTerm, dataLoan.sTerm_rep, "sTerm");
            Assert.AreEqual(sDue, dataLoan.sDue_rep, "sDue");
            Assert.AreEqual(sNoteIR, dataLoan.sNoteIR_rep, "sNoteIR");

            dataApp = dataLoan.GetAppData(0);

            Assert.AreEqual(Borr1_aBFirstNm, dataApp.aBFirstNm, "aBFirstNm");
            Assert.AreEqual(Borr1_aBLastNm, dataApp.aBLastNm, "aBLastNm");
            Assert.AreEqual(Borr1_aBSsn, dataApp.aBSsn, "aBSsn");

            Assert.AreEqual(Borr1_aCFirstNm, dataApp.aCFirstNm, "aCFirstNm");
            Assert.AreEqual(Borr1_aCLastNm, dataApp.aCLastNm, "aCLastNm");
            Assert.AreEqual(Borr1_aCSsn, dataApp.aCSsn, "aCSsn");
            #endregion

            Tools.DeclareLoanFileInvalid(m_currentPrincipal, sLId, false, false);
        }

        [Test]
        public void ImportMultipleBorrowersWithSecondBorrowerSsnModifyTest()
        {
            // 8/19/2009 dd - This test case is response to eOPM 174965

            // This test will perform the following
            // Create a new file
            // Set some data to loan file.
            // Add two borrowers.
            // Add Liabilities / Assets for each borrower.
            // Export to FNM format.
            // Save to temp file.
            // Edit the SSN of the second borrower in loan file.
            // Save to DB.
            // Import the FNM from temp file.
            // There should be 3 borrowers in the loan file (because we could not match one of the ssn)

            #region Create new loan file
            CLoanFileCreator fileCreator = CLoanFileCreator.GetCreator(m_currentPrincipal, E_LoanCreationSource.UserCreateFromBlank);
            Guid sLId = fileCreator.CreateBlankLoanFile();
            #endregion

            #region Create two borrowers set some loan data
            CPageData dataLoan = CreatePageData(sLId);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sTerm_rep = sTerm;
            dataLoan.sDue_rep = sDue;
            dataLoan.sNoteIR_rep = sNoteIR;

            CAppData dataApp = dataLoan.GetAppData(0);
            dataApp.aBFirstNm = Borr1_aBFirstNm;
            dataApp.aBLastNm = Borr1_aBLastNm;
            dataApp.aBSsn = Borr1_aBSsn;

            ILiaCollection liaCollection = dataApp.aLiaCollection;
            ILiabilityRegular liaRegular = liaCollection.AddRegularRecord();
            liaRegular.ComNm = "BOR_1_ComNm_1";
            liaRegular.DebtT = E_DebtRegularT.Revolving;
            liaRegular.Bal_rep = "$1000";
            liaRegular.Update();

            liaRegular = liaCollection.AddRegularRecord();
            liaRegular.ComNm = "BOR_1_ComNm_2";
            liaRegular.DebtT = E_DebtRegularT.Revolving;
            liaRegular.Bal_rep = "$2000";
            liaRegular.Update();
            dataLoan.Save();

            int iAppIndex = dataLoan.AddNewApp();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            Assert.Greater(iAppIndex, 0); // iAppIndex > 0
            dataApp = dataLoan.GetAppData(iAppIndex);
            dataApp.aBFirstNm = Borr2_aBFirstNm;
            dataApp.aBLastNm = Borr2_aBLastNm;
            dataApp.aBSsn = Borr2_aBSsn;

            liaCollection = dataApp.aLiaCollection;
            liaRegular = liaCollection.AddRegularRecord();
            liaRegular.ComNm = "BOR_2_ComNm_1";
            liaRegular.DebtT = E_DebtRegularT.Revolving;
            liaRegular.Bal_rep = "$1001";
            liaRegular.Update();

            liaRegular = liaCollection.AddRegularRecord();
            liaRegular.ComNm = "BOR_2_ComNm_2";
            liaRegular.DebtT = E_DebtRegularT.Revolving;
            liaRegular.Bal_rep = "$2001";
            liaRegular.Update();
            dataLoan.Save();

            #endregion

            #region Export data to FNMA
            FannieMae32Exporter exporter = new FannieMae32Exporter(sLId);
            string fnmaContent = exporter.ExportToString();
            #endregion

            #region Modify the second borrower SSN
            dataLoan = CreatePageData(sLId);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            dataApp = dataLoan.GetAppData(1);
            Assert.AreEqual(Borr2_aBFirstNm, dataApp.aBFirstNm);
            Assert.AreEqual(Borr2_aBLastNm, dataApp.aBLastNm);

            dataApp.aBSsn = "999-99-9999";
            dataLoan.Save();

            #endregion

            #region Import FNMA data
            FannieMae32Importer importer = new FannieMae32Importer();

            importer.ImportIntoExisting(fnmaContent, sLId);
            #endregion

            #region Verify
            dataLoan = CreatePageData(sLId);
            dataLoan.InitLoad();

            Assert.AreEqual(3, dataLoan.nApps);

            var expectedResults = new[] {
                                      new { aBFirstNm = Borr1_aBFirstNm, aBLastNm = Borr1_aBLastNm, aBSsn = Borr1_aBSsn},
                                      new { aBFirstNm = Borr2_aBFirstNm, aBLastNm = Borr2_aBLastNm, aBSsn = "999-99-9999"},
                                      new { aBFirstNm = Borr2_aBFirstNm, aBLastNm = Borr2_aBLastNm, aBSsn = Borr2_aBSsn}


                                  };
            for (int i = 0; i < dataLoan.nApps; i++)
            {
                dataApp = dataLoan.GetAppData(i);

                Assert.AreEqual(expectedResults[i].aBFirstNm, dataApp.aBFirstNm, i + " - aBFirstNm");
                Assert.AreEqual(expectedResults[i].aBLastNm, dataApp.aBLastNm, i + " - aBLastNm");
                Assert.AreEqual(expectedResults[i].aBSsn, dataApp.aBSsn, i + " - aBSsn");
            }
            #endregion

            Tools.DeclareLoanFileInvalid(m_currentPrincipal, sLId, false, false);
        }

        [Test]
        public void ImportMultipleBorrowersWithSecondBorrowerSpouseSsnModifyTest()
        {
            // 8/19/2009 dd - This test case is response to eOPM 174965

            // This test will perform the following
            // Create a new file
            // Set some data to loan file.
            // Add two borrowers.
            // Add Liabilities / Assets for each borrower.
            // Export to FNM format.
            // Save to temp file.
            // Edit the SSN of the second borrower in loan file.
            // Save to DB.
            // Import the FNM from temp file.
            // There should be 3 borrowers in the loan file (because we could not match one of the ssn)

            #region Create new loan file
            CLoanFileCreator fileCreator = CLoanFileCreator.GetCreator(m_currentPrincipal, E_LoanCreationSource.UserCreateFromBlank);
            Guid sLId = fileCreator.CreateBlankLoanFile();
            #endregion

            #region Create two borrowers set some loan data
            CPageData dataLoan = CreatePageData(sLId);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sTerm_rep = sTerm;
            dataLoan.sDue_rep = sDue;
            dataLoan.sNoteIR_rep = sNoteIR;

            CAppData dataApp = dataLoan.GetAppData(0);
            dataApp.aBFirstNm = Borr1_aBFirstNm;
            dataApp.aBLastNm = Borr1_aBLastNm;
            dataApp.aBSsn = Borr1_aBSsn;

            dataApp.aCFirstNm = Borr1_aCFirstNm;
            dataApp.aCLastNm = Borr1_aCLastNm;
            dataApp.aCSsn = Borr1_aCSsn;

            ILiaCollection liaCollection = dataApp.aLiaCollection;
            ILiabilityRegular liaRegular = liaCollection.AddRegularRecord();
            liaRegular.ComNm = "BOR_1_ComNm_1";
            liaRegular.DebtT = E_DebtRegularT.Revolving;
            liaRegular.Bal_rep = "$1000";
            liaRegular.Update();

            liaRegular = liaCollection.AddRegularRecord();
            liaRegular.ComNm = "BOR_1_ComNm_2";
            liaRegular.DebtT = E_DebtRegularT.Revolving;
            liaRegular.Bal_rep = "$2000";
            liaRegular.Update();
            dataLoan.Save();

            int iAppIndex = dataLoan.AddNewApp();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            Assert.Greater(iAppIndex, 0); // iAppIndex > 0
            dataApp = dataLoan.GetAppData(iAppIndex);
            dataApp.aBFirstNm = Borr2_aBFirstNm;
            dataApp.aBLastNm = Borr2_aBLastNm;
            dataApp.aBSsn = Borr2_aBSsn;

            dataApp.aCFirstNm = Borr2_aCFirstNm;
            dataApp.aCLastNm = Borr2_aCLastNm;
            dataApp.aCSsn = Borr2_aCSsn;

            liaCollection = dataApp.aLiaCollection;
            liaRegular = liaCollection.AddRegularRecord();
            liaRegular.ComNm = "BOR_2_ComNm_1";
            liaRegular.DebtT = E_DebtRegularT.Revolving;
            liaRegular.Bal_rep = "$1001";
            liaRegular.Update();

            liaRegular = liaCollection.AddRegularRecord();
            liaRegular.ComNm = "BOR_2_ComNm_2";
            liaRegular.DebtT = E_DebtRegularT.Revolving;
            liaRegular.Bal_rep = "$2001";
            liaRegular.Update();
            dataLoan.Save();

            #endregion

            #region Export data to FNMA
            FannieMae32Exporter exporter = new FannieMae32Exporter(sLId);
            string fnmaContent = exporter.ExportToString();
            #endregion

            #region Modify the second borrower SSN
            dataLoan = CreatePageData(sLId);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            dataApp = dataLoan.GetAppData(1);
            Assert.AreEqual(Borr2_aCFirstNm, dataApp.aCFirstNm);
            Assert.AreEqual(Borr2_aCLastNm, dataApp.aCLastNm);

            dataApp.aCSsn = "999-99-9999";
            dataLoan.Save();

            #endregion

            #region Import FNMA data
            FannieMae32Importer importer = new FannieMae32Importer();

            importer.ImportIntoExisting(fnmaContent, sLId);
            #endregion

            #region Verify
            dataLoan = CreatePageData(sLId);
            dataLoan.InitLoad();

            Assert.AreEqual(2, dataLoan.nApps);

            var expectedResults = new[] {
                                      new { aBFirstNm = Borr1_aBFirstNm, aBLastNm = Borr1_aBLastNm, aBSsn = Borr1_aBSsn,aCFirstNm = Borr1_aCFirstNm, aCLastNm = Borr1_aCLastNm, aCSsn = Borr1_aCSsn},
                                      new { aBFirstNm = Borr2_aBFirstNm, aBLastNm = Borr2_aBLastNm, aBSsn = Borr2_aBSsn,aCFirstNm = Borr2_aCFirstNm, aCLastNm = Borr2_aCLastNm, aCSsn = Borr2_aCSsn}

                                  };
            for (int i = 0; i < dataLoan.nApps; i++)
            {
                dataApp = dataLoan.GetAppData(i);

                Assert.AreEqual(expectedResults[i].aBFirstNm, dataApp.aBFirstNm, i + " - aBFirstNm");
                Assert.AreEqual(expectedResults[i].aBLastNm, dataApp.aBLastNm, i + " - aBLastNm");
                Assert.AreEqual(expectedResults[i].aBSsn, dataApp.aBSsn, i + " - aBSsn");

                Assert.AreEqual(expectedResults[i].aCFirstNm, dataApp.aCFirstNm, i + " - aBFirstNm");
                Assert.AreEqual(expectedResults[i].aCLastNm, dataApp.aCLastNm, i + " - aBLastNm");
                Assert.AreEqual(expectedResults[i].aCSsn, dataApp.aCSsn, i + " - aBSsn");

            }
            #endregion

            Tools.DeclareLoanFileInvalid(m_currentPrincipal, sLId, false, false);
        }

        [Test]
        public void ImportIntoExisting_FileUsingLegacyIncomeFieldsNoExistingValues_SetsIncomeFieldsAsExpected()
        {
            using (var testLoan = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: false))
            {
                var loanBefore = testLoan.CreateNewPageDataWithBypass();
                loanBefore.InitLoad();
                Assert.AreEqual(false, loanBefore.sIsIncomeCollectionEnabled);
                var fnmaContent = TestUtilities.ReadFile("FANNIE_INCOME_IMPORT_TEST.fnm");

                var importer = new FannieMae32Importer();
                importer.ImportIntoExisting(fnmaContent, testLoan.Id);

                var loanAfter = testLoan.CreateNewPageDataWithBypass();
                loanAfter.InitLoad();
                AssertLegacyFieldsPopulated(loanAfter.GetAppData(0));
            }
        }

        [Test]
        public void ImportIntoExisting_FileUsingLegacyIncomeFieldsWithExistingValues_OverwritesIncomeFieldsAsExpected()
        {
            using (var testLoan = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: false))
            {
                var loanBefore = testLoan.CreateNewPageDataWithBypass();
                loanBefore.InitSave();
                PopulateIncomeFields(loanBefore.GetAppData(0));
                loanBefore.Save();
                Assert.AreEqual(false, loanBefore.sIsIncomeCollectionEnabled);
                var fnmaContent = TestUtilities.ReadFile("FANNIE_INCOME_IMPORT_TEST.fnm");

                var importer = new FannieMae32Importer();
                importer.ImportIntoExisting(fnmaContent, testLoan.Id);

                var loanAfter = testLoan.CreateNewPageDataWithBypass();
                loanAfter.InitLoad();
                AssertLegacyFieldsPopulated(loanAfter.GetAppData(0));
            }
        }

        [Test]
        public void ImportIntoExisting_FileUsingIncomeSourceCollectionNoExistingValues_AddsIncomeSourcesAsExpected()
        {
            using (var testLoan = LoanForIntegrationTest.Create(TestFileType.TestFile))
            {
                var loanBefore = testLoan.CreateNewPageDataWithBypass();
                loanBefore.InitSave();
                loanBefore.MigrateToUseLqbCollections();
                loanBefore.MigrateToIncomeSourceCollection();
                Assert.IsTrue(loanBefore.sIsIncomeCollectionEnabled);
                loanBefore.AddCoborrowerToLegacyApplication(loanBefore.GetAppData(0).aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                loanBefore.Save();
                var fnmaContent = TestUtilities.ReadFile("FANNIE_INCOME_IMPORT_TEST.fnm");

                var importer = new FannieMae32Importer();
                importer.ImportIntoExisting(fnmaContent, testLoan.Id);

                var loanAfter = testLoan.CreateNewPageDataWithBypass();
                loanAfter.InitLoad();
                var appAfter = loanAfter.GetAppData(0);
                var borrowerId = appAfter.aBConsumerId;
                var coborrowerId = appAfter.aCConsumerId;
                AssertLegacyFieldsPopulated(appAfter);
                // The test file is multi-app, which leads to the higher total.
                Assert.AreEqual(25, loanAfter.IncomeSources.Count);
                Assert.AreEqual(7, loanAfter.ConsumerIncomeSources.Values.Count(a => a.ConsumerId.Value == borrowerId));
                Assert.AreEqual(6, loanAfter.ConsumerIncomeSources.Values.Count(a => a.ConsumerId.Value == coborrowerId));
                // Make sure the other income types came over as expected.
                Assert.AreEqual(1, loanAfter.IncomeSources.Values.Count(i => i.IncomeType == LqbGrammar.DataTypes.IncomeType.BoarderIncome));
                // There are three because of the values from the second app.
                Assert.AreEqual(3, loanAfter.IncomeSources.Values.Count(i => i.IncomeType == LqbGrammar.DataTypes.IncomeType.Other));
                Assert.AreEqual(1, loanAfter.IncomeSources.Values.Count(i => i.IncomeType == LqbGrammar.DataTypes.IncomeType.MilitaryBasePay));
            }
        }

        [Test]
        public void ImportIntoExisting_FileUsingIncomeSourceCollectionWithExistingValues_OverwritesOldIncomeSourcesAsExpected()
        {
            using (var testLoan = LoanForIntegrationTest.Create(TestFileType.TestFile))
            {
                var loanBefore = testLoan.CreateNewPageDataWithBypass();
                loanBefore.InitSave();
                Assert.IsFalse(loanBefore.sIsIncomeCollectionEnabled);
                var appBefore = loanBefore.GetAppData(0);
                PopulateIncomeFields(appBefore);
                loanBefore.AddCoborrowerToLegacyApplication(appBefore.aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                loanBefore.MigrateToUseLqbCollections();
                loanBefore.MigrateToIncomeSourceCollection();
                Assert.IsTrue(loanBefore.sIsIncomeCollectionEnabled);
                loanBefore.Save();
                var fnmaContent = TestUtilities.ReadFile("FANNIE_INCOME_IMPORT_TEST.fnm");

                var importer = new FannieMae32Importer();
                importer.ImportIntoExisting(fnmaContent, testLoan.Id);

                var loanAfter = testLoan.CreateNewPageDataWithBypass();
                loanAfter.InitLoad();
                var appAfter = loanAfter.GetAppData(0);
                var borrowerId = appAfter.aBConsumerId;
                var coborrowerId = appAfter.aCConsumerId;
                AssertLegacyFieldsPopulated(appAfter);
                // The test file is multi-app, which leads to the higher total.
                Assert.AreEqual(25, loanAfter.IncomeSources.Count);
                Assert.AreEqual(7, loanAfter.ConsumerIncomeSources.Values.Count(a => a.ConsumerId.Value == borrowerId));
                Assert.AreEqual(6, loanAfter.ConsumerIncomeSources.Values.Count(a => a.ConsumerId.Value == coborrowerId));
                // Make sure the other income types came over as expected.
                Assert.AreEqual(1, loanAfter.IncomeSources.Values.Count(i => i.IncomeType == LqbGrammar.DataTypes.IncomeType.BoarderIncome));
                // There are three because of the values from the second app.
                Assert.AreEqual(3, loanAfter.IncomeSources.Values.Count(i => i.IncomeType == LqbGrammar.DataTypes.IncomeType.Other));
                Assert.AreEqual(1, loanAfter.IncomeSources.Values.Count(i => i.IncomeType == LqbGrammar.DataTypes.IncomeType.MilitaryBasePay));
            }
        }

        [TestCase(false)]
        [TestCase(true)]
        public void ImportIntoExisting_SubjectPropertyNetRentSpecified_SetsSpGrossRentToExpectedValue(bool useMigratedIncome)
        {
            using (var testLoan = LoanForIntegrationTest.Create(TestFileType.TestFile))
            {
                var fnmaContent = TestUtilities.ReadFile("FANNIE_SUBJ_PROP_NET_RENT.fnm");
                var loanBefore = testLoan.CreateNewPageDataWithBypass();
                loanBefore.InitSave();
                if (useMigratedIncome)
                {
                    loanBefore.MigrateToUseLqbCollections();
                    loanBefore.MigrateToIncomeSourceCollection();
                    loanBefore.Save();
                }

                Assert.AreEqual(useMigratedIncome, loanBefore.sIsIncomeCollectionEnabled);

                var importer = new FannieMae32Importer();
                importer.ImportIntoExisting(fnmaContent, testLoan.Id);

                var loanAfter = testLoan.CreateNewPageDataWithBypass();
                loanAfter.InitLoad();
                Assert.AreEqual(3, loanAfter.sSpGrossRent);
            }
        }

        private static void PopulateIncomeFields(CAppData appBefore)
        {
            appBefore.aBBaseI = 101;
            appBefore.aBOvertimeI = 103;
            appBefore.aBBonusesI = 105;
            appBefore.aBCommisionI = 107;
            appBefore.aBDividendI = 109;
            appBefore.aCBaseI = 102;
            appBefore.aCOvertimeI = 104;
            appBefore.aCBonusesI = 106;
            appBefore.aCCommisionI = 108;
            appBefore.aCDividendI = 110;

            var otherIncomes = new List<OtherIncome>
            {
                new OtherIncome
                {
                    Desc = "Test 1",
                    Amount = 1,
                    IsForCoBorrower = true
                },
                new OtherIncome
                {
                    Desc = "Test 2",
                    Amount = 2,
                    IsForCoBorrower = false
                },
                new OtherIncome
                {
                    Desc = "Test 3",
                    Amount = 3,
                    IsForCoBorrower = true
                },
            };

            appBefore.aOtherIncomeList = otherIncomes;
        }

        private static void AssertLegacyFieldsPopulated(DataAccess.CAppData appAfter)
        {
            Assert.AreEqual(1, appAfter.aBBaseI);
            Assert.AreEqual(3, appAfter.aBOvertimeI);
            Assert.AreEqual(5, appAfter.aBBonusesI);
            Assert.AreEqual(7, appAfter.aBCommisionI);
            Assert.AreEqual(9, appAfter.aBDividendI);
            Assert.AreEqual(2, appAfter.aCBaseI);
            Assert.AreEqual(4, appAfter.aCOvertimeI);
            Assert.AreEqual(6, appAfter.aCBonusesI);
            Assert.AreEqual(8, appAfter.aCCommisionI);
            Assert.AreEqual(10, appAfter.aCDividendI);

            Assert.AreEqual(3, appAfter.aOtherIncomeList.Count);

            var borrOtherIncomes = appAfter.aOtherIncomeList.Where(i => !i.IsForCoBorrower);
            Assert.AreEqual(1, borrOtherIncomes.Count(i => i.Amount == 11 && i.Desc.Equals("Boarder Income", StringComparison.OrdinalIgnoreCase)));
            Assert.AreEqual(1, borrOtherIncomes.Count(i => i.Amount == 13 && i.Desc.Equals("Other Income", StringComparison.OrdinalIgnoreCase)));

            var coborrOtherIncome = appAfter.aOtherIncomeList.Single(i => i.IsForCoBorrower);
            StringAssert.AreEqualIgnoringCase("Military Base Pay", coborrOtherIncome.Desc);
            Assert.AreEqual(12, coborrOtherIncome.Amount);
        }
    }
}
