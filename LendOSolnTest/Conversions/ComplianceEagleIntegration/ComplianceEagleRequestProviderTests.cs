﻿namespace LendOSolnTest.Conversions.ComplianceEagleIntegration
{
    using System.Collections.Generic;
    using System.Linq;
    using Common;
    using Fakes;
    using LendersOffice.Conversions.ComplianceEagleIntegration;
    using NUnit.Framework;

    /// <summary>
    /// Tests behaviors of <see cref="ComplianceEagleRequestProvider"/>.
    /// </summary>
    [TestFixture]
    public class ComplianceEagleRequestProviderTests
    {
        /// <summary>
        /// Performs test setup.
        /// </summary>
        [TestFixtureSetUp]
        public void Setup() => LoginTools.LoginAs(TestAccountType.LoTest001);

        /// <summary>
        /// Performs test tear down.
        /// </summary>
        [TestFixtureTearDown]
        public void Teardown() => FakePageDataUtilities.Instance.Reset();

        /// <summary>
        /// Tests the main wrapper for the Compliance Eagle export.  Does not include MISMO export because that loads a separate loan instance.
        /// </summary>
        [Test]
        public void TestDataWrapperExport()
        {
            var loan = FakePageData.Create();
            loan.InitLoad();
            loan.SetFormatTarget(DataAccess.FormatTarget.MismoClosing);
            loan.GetAppData(0).aBSsn = "000-00-0000"; // necessary to trick the exporter into thinking this is a non-blank borrower.
            var requestProvider = new ComplianceEagleRequestProvider("username", "password", "customerID", loan);

            var complianceEagleRequest = requestProvider.CreateQSComplianceReportRequest();
            
            // Note that the export for dynamic fields will throw for bad entries, which is also being implicitly tested here
            Assert.AreEqual(1, complianceEagleRequest?.LoanData?.BorrowerList?.Count ?? -1, "Borrower count does not match");
        }
    }
}
