﻿// <copyright file="Mismo33ExportTest.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Justin Lara
//  Date:   January 22, 2014
// </summary>
namespace LendOSolnTest.Conversions
{
    using LendOSolnTest.Common;
    using LendOSolnTest.Fakes;
    using NUnit.Framework;

    /// <summary>
    /// Tests to validate the serialization of the MISMO 3.3 export.
    /// </summary>
    [TestFixture]
    public class Mismo33ExportTest
    {
        /// <summary>
        /// Performs test setup.
        /// </summary>
        [TestFixtureSetUp]
        public void Setup() => LoginTools.LoginAs(TestAccountType.LoTest001);

        /// <summary>
        /// Performs test teardown.
        /// </summary>
        [TestFixtureTearDown]
        public void Teardown() => FakePageDataUtilities.Instance.Reset();

        /// <summary>
        /// A simple test to verify whether serialization is working correctly, using a test loan
        /// populated with some down payment dummy data on a down payment.
        /// </summary>
        [Test]
        public void SerializationWithDownPaymentTest()
        {
            var mismoRequest = FakeMismo33RequestProvider.SerializeMessage();

            Assert.IsTrue(System.Text.RegularExpressions.Regex.IsMatch(
                mismoRequest, 
                "<MESSAGE[^>]*><DEAL_SETS><DEAL_SET SequenceNumber=\"1\"><DEALS><DEAL SequenceNumber=\"1\">.*</DEAL></DEALS></DEAL_SET></DEAL_SETS></MESSAGE>",
                System.Text.RegularExpressions.RegexOptions.Singleline));
        }
    }
}