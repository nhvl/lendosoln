﻿namespace LendOSolnTest.Conversions
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Conversions.LoanProspector;
    using LendersOffice.Security;
    using LendOSolnTest.Common;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    public class LoanProspectorImporterTest
    {
        AbstractUserPrincipal x_principal = null;

        private AbstractUserPrincipal m_currentPrincipal
        {
            get
            {
                if (null == x_principal)
                {
                    x_principal = LoginTools.LoginAs(TestAccountType.LoTest001);
                }

                return x_principal;
            }

        }
        private Guid m_sLId = Guid.Empty;

        [Test]
        public void ImportCase6Response()
        {
            AbstractUserPrincipal principal = m_currentPrincipal;
            m_sLId = CreateLoan();

            string xml = File.ReadAllText(TestDataRootFolder.Location + "LP_TestCase_6_Response.xml");
            var parsedServiceOrderResponse = LoanProspectorResponseParser.Parse(xml);
            var loanIdentifierData = parsedServiceOrderResponse.ValueOrDefault?.LoanApplication?.ExtensionLoanApplication?.LoanIdentifierData;
            if (loanIdentifierData != null)
            {
                loanIdentifierData.GloballyUniqueIdentifier = this.m_sLId.ToString(); // the importer reads the loan id from the object
            }

            LoanProspectorImporter importer = LoanProspectorImporter.CreateImporterForNonseamless(parsedServiceOrderResponse, isImport1003: true, isImportFeedback: true, isImportCreditReport: false, loanId: this.m_sLId);
            importer.Import();

            CPageData dataLoan = CreatePageData(m_sLId);
            dataLoan.InitLoad();
            Assert.AreEqual(E_sProd3rdPartyUwResultT.Lp_Refer, dataLoan.sProd3rdPartyUwResultT, "sProd3rdPartyUwResultT");
        }

        [Test]
        public void Import_FileUsingLegacyIncomeFields_SetsIncomeFieldsAsExpected()
        {
            using (var testLoan = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: false))
            {
                var loanBefore = testLoan.CreateNewPageDataWithBypass();
                loanBefore.InitSave();
                loanBefore.GetAppData(0).aBSsn = "005260001";
                loanBefore.GetAppData(0).aCSsn = "006260001";
                loanBefore.Save();
                Assert.AreEqual(false, loanBefore.sIsIncomeCollectionEnabled);
                var xml = TestUtilities.ReadFile("LOAN_PROSPECTOR_INCOME_IMPORT_TEST.xml");
                var parsedServiceOrderResponse = LoanProspectorResponseParser.Parse(xml);

                var importer = LoanProspectorImporter.CreateImporterForNonseamless(
                    parsedServiceOrderResponse,
                    isImport1003: true,
                    isImportFeedback: true,
                    isImportCreditReport: false,
                    loanId: testLoan.Id);
                importer.Import();

                var loanAfter = testLoan.CreateNewPageDataWithBypass();
                loanAfter.InitLoad();
                AssertLegacyFieldsPopulated(loanAfter.GetAppData(0));
            }
        }

        [Test]
        public void Import_FileUsingLegacyIncomeFieldsWithExistingData_OverwritesExistingIncomeFields()
        {
            using (var testLoan = LoanForIntegrationTest.Create(TestAccountType.LoTest001, useUladDataLayer: false))
            {
                var loanBefore = testLoan.CreateNewPageDataWithBypass();
                loanBefore.InitSave();
                loanBefore.GetAppData(0).aBSsn = "005260001";
                loanBefore.GetAppData(0).aCSsn = "006260001";
                PopulateIncomeFields(loanBefore.GetAppData(0));
                loanBefore.Save();
                Assert.AreEqual(false, loanBefore.sIsIncomeCollectionEnabled);
                Assert.Greater(loanBefore.GetAppData(0).aBTotI, 0);
                Assert.Greater(loanBefore.GetAppData(0).aCTotI, 0);
                var xml = TestUtilities.ReadFile("LOAN_PROSPECTOR_INCOME_IMPORT_TEST.xml");
                var parsedServiceOrderResponse = LoanProspectorResponseParser.Parse(xml);

                var importer = LoanProspectorImporter.CreateImporterForNonseamless(
                    parsedServiceOrderResponse,
                    isImport1003: true,
                    isImportFeedback: true,
                    isImportCreditReport: false,
                    loanId: testLoan.Id);
                importer.Import();

                var loanAfter = testLoan.CreateNewPageDataWithBypass();
                loanAfter.InitLoad();
                AssertLegacyFieldsPopulated(loanAfter.GetAppData(0));
            }
        }

        [Test]
        public void Import_FileUsingIncomeSourceCollection_AddsExpectedIncomeSources()
        {
            using (var testLoan = LoanForIntegrationTest.Create(TestFileType.TestFile))
            {
                var loanBefore = testLoan.CreateNewPageDataWithBypass();
                loanBefore.InitSave();
                loanBefore.MigrateToUseLqbCollections();
                loanBefore.MigrateToIncomeSourceCollection();
                Assert.IsTrue(loanBefore.sIsIncomeCollectionEnabled);
                loanBefore.GetAppData(0).aBSsn = "005260001";
                loanBefore.GetAppData(0).aCSsn = "006260001";
                loanBefore.AddCoborrowerToLegacyApplication(loanBefore.GetAppData(0).aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                loanBefore.Save();
                var xml = TestUtilities.ReadFile("LOAN_PROSPECTOR_INCOME_IMPORT_TEST.xml");
                var parsedServiceOrderResponse = LoanProspectorResponseParser.Parse(xml);

                var importer = LoanProspectorImporter.CreateImporterForNonseamless(
                    parsedServiceOrderResponse,
                    isImport1003: true,
                    isImportFeedback: true,
                    isImportCreditReport: false,
                    loanId: testLoan.Id);
                importer.Import();

                var loanAfter = testLoan.CreateNewPageDataWithBypass();
                loanAfter.InitLoad();
                AssertLegacyFieldsPopulated(loanAfter.GetAppData(0));
                Assert.AreEqual(16, loanAfter.IncomeSources.Count);
                Assert.AreEqual(8, loanAfter.ConsumerIncomeSources.Values.Count(a => a.ConsumerId.Value == loanAfter.GetAppData(0).aBConsumerId));
                Assert.AreEqual(8, loanAfter.ConsumerIncomeSources.Values.Count(a => a.ConsumerId.Value == loanAfter.GetAppData(0).aCConsumerId));
                // Make sure the other incomes came over as expected.
                Assert.AreEqual(2, loanAfter.IncomeSources.Values.Count(i => i.IncomeType == LqbGrammar.DataTypes.IncomeType.AlimonyChildSupport));
                Assert.AreEqual(2, loanAfter.IncomeSources.Values.Count(i => i.IncomeType == LqbGrammar.DataTypes.IncomeType.PensionRetirement));
                Assert.AreEqual(2, loanAfter.IncomeSources.Values.Count(i => i.IncomeType == LqbGrammar.DataTypes.IncomeType.UnemploymentWelfare));
            }
        }

        [Test]
        public void Import_SubjPropNetRentOnFileUsingIncomeSourceCollection_SetsSpGrossRent()
        {
            using (var testLoan = LoanForIntegrationTest.Create(TestFileType.TestFile))
            {
                var loanBefore = testLoan.CreateNewPageDataWithBypass();
                loanBefore.InitSave();
                loanBefore.MigrateToUseLqbCollections();
                loanBefore.MigrateToIncomeSourceCollection();
                Assert.IsTrue(loanBefore.sIsIncomeCollectionEnabled);
                loanBefore.GetAppData(0).aBSsn = "005260001";
                loanBefore.GetAppData(0).aCSsn = "006260001";
                loanBefore.AddCoborrowerToLegacyApplication(loanBefore.GetAppData(0).aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                loanBefore.Save();
                var xml = TestUtilities.ReadFile("LOAN_PROSPECTOR_SUBJ_PROP_NET_RENT.xml");
                var parsedServiceOrderResponse = LoanProspectorResponseParser.Parse(xml);

                var importer = LoanProspectorImporter.CreateImporterForNonseamless(
                    parsedServiceOrderResponse,
                    isImport1003: true,
                    isImportFeedback: true,
                    isImportCreditReport: false,
                    loanId: testLoan.Id);
                importer.Import();

                var loanAfter = testLoan.CreateNewPageDataWithBypass();
                loanAfter.InitLoad();
                Assert.AreEqual(3, loanAfter.sSpGrossRent);
            }
        }

        [Test]
        public void Import_FileUsingIncomeSourceCollectionWithExistingRecords_OverwritesExistingIncomeSources()
        {
            using (var testLoan = LoanForIntegrationTest.Create(TestFileType.TestFile))
            {
                var loanBefore = testLoan.CreateNewPageDataWithBypass();
                loanBefore.InitSave();
                Assert.IsFalse(loanBefore.sIsIncomeCollectionEnabled);
                loanBefore.GetAppData(0).aBSsn = "005260001";
                loanBefore.GetAppData(0).aCSsn = "006260001";
                loanBefore.AddCoborrowerToLegacyApplication(loanBefore.GetAppData(0).aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                PopulateIncomeFields(loanBefore.GetAppData(0));
                loanBefore.MigrateToUseLqbCollections();
                loanBefore.MigrateToIncomeSourceCollection();
                Assert.IsTrue(loanBefore.sIsIncomeCollectionEnabled);
                loanBefore.Save();
                Assert.AreEqual(true, loanBefore.sIsIncomeCollectionEnabled);
                Assert.Greater(loanBefore.GetAppData(0).aBTotI, 0);
                Assert.Greater(loanBefore.GetAppData(0).aCTotI, 0);
                var xml = TestUtilities.ReadFile("LOAN_PROSPECTOR_INCOME_IMPORT_TEST.xml");
                var parsedServiceOrderResponse = LoanProspectorResponseParser.Parse(xml);

                var importer = LoanProspectorImporter.CreateImporterForNonseamless(
                    parsedServiceOrderResponse,
                    isImport1003: true,
                    isImportFeedback: true,
                    isImportCreditReport: false,
                    loanId: testLoan.Id);
                importer.Import();

                var loanAfter = testLoan.CreateNewPageDataWithBypass();
                loanAfter.InitLoad();
                AssertLegacyFieldsPopulated(loanAfter.GetAppData(0));
                Assert.AreEqual(16, loanAfter.IncomeSources.Count);
                Assert.AreEqual(8, loanAfter.ConsumerIncomeSources.Values.Count(a => a.ConsumerId.Value == loanAfter.GetAppData(0).aBConsumerId));
                Assert.AreEqual(8, loanAfter.ConsumerIncomeSources.Values.Count(a => a.ConsumerId.Value == loanAfter.GetAppData(0).aCConsumerId));
                // Make sure the other incomes came over as expected.
                Assert.AreEqual(2, loanAfter.IncomeSources.Values.Count(i => i.IncomeType == LqbGrammar.DataTypes.IncomeType.AlimonyChildSupport));
                Assert.AreEqual(2, loanAfter.IncomeSources.Values.Count(i => i.IncomeType == LqbGrammar.DataTypes.IncomeType.PensionRetirement));
                Assert.AreEqual(2, loanAfter.IncomeSources.Values.Count(i => i.IncomeType == LqbGrammar.DataTypes.IncomeType.UnemploymentWelfare));
            }
        }

        private static void PopulateIncomeFields(CAppData appBefore)
        {
            appBefore.aBBaseI = 101;
            appBefore.aBOvertimeI = 103;
            appBefore.aBBonusesI = 105;
            appBefore.aBCommisionI = 107;
            appBefore.aBDividendI = 109;
            appBefore.aCBaseI = 102;
            appBefore.aCOvertimeI = 104;
            appBefore.aCBonusesI = 106;
            appBefore.aCCommisionI = 108;
            appBefore.aCDividendI = 110;

            var otherIncomes = new List<OtherIncome>
            {
                new OtherIncome
                {
                    Desc = "Test 1",
                    Amount = 1,
                    IsForCoBorrower = true
                },
                new OtherIncome
                {
                    Desc = "Test 2",
                    Amount = 2,
                    IsForCoBorrower = false
                },
                new OtherIncome
                {
                    Desc = "Test 3",
                    Amount = 3,
                    IsForCoBorrower = true
                },
            };

            appBefore.aOtherIncomeList = otherIncomes;
        }

        private static void AssertLegacyFieldsPopulated(DataAccess.CAppData appAfter)
        {
            Assert.AreEqual(1, appAfter.aBBaseI);
            Assert.AreEqual(2, appAfter.aBOvertimeI);
            Assert.AreEqual(3, appAfter.aBBonusesI);
            Assert.AreEqual(4, appAfter.aBCommisionI);
            Assert.AreEqual(5, appAfter.aBDividendI);
            Assert.AreEqual(51, appAfter.aCBaseI);
            Assert.AreEqual(52, appAfter.aCOvertimeI);
            Assert.AreEqual(53, appAfter.aCBonusesI);
            Assert.AreEqual(54, appAfter.aCCommisionI);
            Assert.AreEqual(55, appAfter.aCDividendI);

            // The files on the old income infrastructure append the other income to the existing empty list,
            // so we check here for populated other income.
            Assert.AreEqual(6, appAfter.aOtherIncomeList.Count(i => i.Amount != 0));

            var borrOtherIncomes = appAfter.aOtherIncomeList.Where(i => !i.IsForCoBorrower && i.Amount != 0);
            Assert.AreEqual(1, borrOtherIncomes.Count(i => i.Amount == 6 && i.Desc.Equals("Unemployment/Welfare Income", StringComparison.OrdinalIgnoreCase)));
            Assert.AreEqual(1, borrOtherIncomes.Count(i => i.Amount == -250 && i.Desc.Equals("Alimony/Child Support Income", StringComparison.OrdinalIgnoreCase)));
            Assert.AreEqual(1, borrOtherIncomes.Count(i => i.Amount == 1000 && i.Desc.Equals("Pension/Retirement Income", StringComparison.OrdinalIgnoreCase)));

            var coborrOtherIncomes = appAfter.aOtherIncomeList.Where(i => i.IsForCoBorrower && i.Amount != 0);
            Assert.AreEqual(1, coborrOtherIncomes.Count(i => i.Amount == 56 && i.Desc.Equals("Unemployment/Welfare Income", StringComparison.OrdinalIgnoreCase)));
            Assert.AreEqual(1, coborrOtherIncomes.Count(i => i.Amount == -200 && i.Desc.Equals("Alimony/Child Support Income", StringComparison.OrdinalIgnoreCase)));
            Assert.AreEqual(1, coborrOtherIncomes.Count(i => i.Amount == 1050 && i.Desc.Equals("Pension/Retirement Income", StringComparison.OrdinalIgnoreCase)));
        }

        [TearDown]
        public void Cleanup()
        {
            if (m_sLId != Guid.Empty)
            {
                Tools.DeclareLoanFileInvalid(m_currentPrincipal, m_sLId, false, false); 
            }
        }

        private static Guid CreateLoan()
        {
            AbstractUserPrincipal brokerUser = BrokerUserPrincipal.CurrentPrincipal;
            CLoanFileCreator creator = CLoanFileCreator.GetCreator(brokerUser, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
            Guid sLId = creator.CreateBlankLoanFile();
            return sLId;
        }

        private CPageData CreatePageData(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(LoanProspectorImporterTest));
        }

        [TestFixtureSetUp]
        public void setup()
        {
            
            
        }

    }
}
