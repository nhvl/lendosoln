﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace LendOSolnTest.LendingQBInfrequentChangeLibTest.CommonLibTest
{
    [TestFixture]
    public class AddressTest
    {
        [Test]
        public void AreEquivalentTest()
        {
            var addresses = new string[]{
                "1234 somewhere st N",                  // 0
                " 1234  someWhere  STREET  NoRth ",     // 1
                "TBD",                                  // 2
                "123 Bourbon St.",                      // 3 -- original
                "123 Bourbon Street",                   // 4
                "123 Whiskey St.",                      // 5
                "124 Bourbon St.",                      // 6
                "123 Bourbon Ave.",                     // 7
                "123 Bourbon St. Unit B",               // 8
                };

            //matches:  
            // 0,1    
            // 3,4

            var matches = new Dictionary<int, int>();  //! make an "equivalence class" object.
            matches.Add(0, 1);
            matches.Add(3, 4 );
            for (int i = 0; i < addresses.Length; i++)
            {
                for (int j = i + 1; j < addresses.Length; j++)
                {
                    if (matches.ContainsKey(i) && j == matches[i])
                    {
                        Assert.That(CommonLib.Address.AreEquivalent(addresses[i], addresses[j]));
                    }
                    else
                    {
                        Assert.That(false == CommonLib.Address.AreEquivalent(addresses[i], addresses[j]));
                    }
                }
            }

            var address1 = "1234 somewhere st N";
            var address2 = " 1234  someWhere  STREET  NoRth ";
            var address3 = "TBD";
            
            Assert.That(CommonLib.Address.AreEquivalent(address1, address2));
            Assert.That(CommonLib.Address.AreEquivalent(address1, address3) == false);
            Assert.That(address2 == " 1234  someWhere  STREET  NoRth ");  // note no side effects... but would that not be the case for non-string objects?
        }
        [Test]
        public void AreEquivalentByComparatorTest()
        {
            var addresses = new string[]{
                "1234 somewhere st N",                  // 0
                " 1234  someWhere  STREET  NoRth ",     // 1
                "TBD",                                  // 2
                "123 Bourbon St.",                      // 3 -- original
                "123 Bourbon Street",                   // 4
                "123 Whiskey St.",                      // 5
                "124 Bourbon St.",                      // 6
                "123 Bourbon Ave.",                     // 7
                "123 Bourbon St. Unit B",               // 8
                };

            //matches:  
            // 0,1    
            // 3,4,5,7,8, not 6.
            var matches = new Dictionary<int, HashSet<int>>();  //! make an "equivalence class" object.
            var hs1 = new HashSet<int>() { 0, 1 };
            var hs2 = new HashSet<int>() { 3, 4, 5, 7, 8 };
            foreach (var i in hs1)
            {
                matches.Add(i, hs1);
            }
            foreach (var i in hs2)
            {
                matches.Add(i, hs2);
            }

            for (int i = 0; i < addresses.Length; i++)
            {
                for (int j = i + 1; j < addresses.Length; j++)
                {
                    if (matches.ContainsKey(i) && matches[i].Contains(j))
                    {
                        Assert.That(CommonLib.Address.AreEquivalentByComparator(addresses[i], addresses[j], StreetsMatch));
                    }
                    else
                    {
                        Assert.That(false == CommonLib.Address.AreEquivalentByComparator(addresses[i], addresses[j], StreetsMatch));
                    }
                }
            }
        }
        private bool StreetsMatch(CommonLib.Address addr1, CommonLib.Address addr2)
        {
            return string.Compare(addr1.StreetNumber, addr2.StreetNumber, true) == 0;
        }
    }
}
