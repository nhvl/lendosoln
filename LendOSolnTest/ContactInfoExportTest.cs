﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

using DataAccess;
using LendOSolnTest.Common;
using LendersOffice.Security;
using LendersOfficeApp.los.admin;
using LendersOfficeApp.los.BrokerAdmin;
using NUnit.Framework;
using PriceMyLoan.Security;
using LendersOffice.Admin;
using LendersOffice.ObjLib.Security;

namespace LendOSolnTest
{
    [TestFixture]
    public class ContactInfoExportTest
    {
        private string getCellValue(PmlBroker.RoleFixup oRF, string sKey, Object oValue)
        {
            string s = oRF.BindCell(sKey, oValue.ToString().Replace(Environment.NewLine, " "));

            // Escape Commas
            if (s.IndexOf(',') != -1)
                return "\"" + s + "\"";

            return s;
        }

        [Test]
        public void CreateContactInfoCSVTest()
        {
            // Login
            LoginTools.LoginAs(TestAccountType.LoTest001);
            AbstractUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;

            Boolean isAdmin = principal.HasRole(E_RoleT.Administrator);
            
            PmlBroker.RoleFixup roleFixup = new PmlBroker.RoleFixup();
            roleFixup.Retrieve(principal.BrokerId);

            LosConvert losConvert = new LosConvert();

            // Retrieve all client certificates and registered IPs manually to 
            // prevent holding the data reader open while they are retrieved.
            var clientCertificates = new ClientCertificates(principal.BrokerId);
            clientCertificates.Retrieve();
            var registeredIpAddresses = new UserRegisteredIpAddresses(principal.BrokerId);
            registeredIpAddresses.Retrieve();

            // Build CSV using DB info
            var header = new List<string>();
            var row = new List<string>();
            string loginName = null;
            BrokerUserPermissions p = new BrokerUserPermissions();

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", principal.BrokerId)
                                        };

            // Get Employee from DB
            using (DbDataReader sR = StoredProcedureHelper.ExecuteReader(principal.BrokerId, "ListEmployeeContactInfoByBrokerId", parameters))
            {
                // Get First Employee
                EmployeeDB emp = new EmployeeDB();
                if (!sR.Read())
                {
                    Assert.Ignore("Cannot run test. No Employees found.");
                }

                emp.Retrieve(sR);

                emp.BrokerID = principal.BrokerId;

                loginName = emp.LoginName;
                p.Retrieve(principal.BrokerId, emp.ID, sR, null);

                header.Add("Login");
                row.Add(getCellValue(roleFixup, "LoginNm", emp.LoginName));

                header.Add("First Name");
                row.Add(getCellValue(roleFixup, "UserFirstNm", emp.FirstName));

                header.Add("Last Name");
                row.Add(getCellValue(roleFixup, "UserLastNm", emp.LastName));

                header.Add("Company Name");
                row.Add(getCellValue(roleFixup, "PmlBrokerName", emp.PmlBrokerName));

                header.Add("Address");
                row.Add(getCellValue(roleFixup, "Addr", emp.Address.StreetAddress));

                header.Add("City");
                row.Add(getCellValue(roleFixup, "City", emp.Address.City));

                header.Add("State");
                row.Add(getCellValue(roleFixup, "State", emp.Address.State));

                header.Add("Zipcode");
                row.Add(getCellValue(roleFixup, "Zip", emp.Address.Zipcode));

                header.Add("Phone");
                row.Add(getCellValue(roleFixup, "Phone", emp.Phone));

                header.Add("Fax");
                row.Add(getCellValue(roleFixup, "Fax", emp.Fax));
                
                header.Add("Cell Phone");
                row.Add(getCellValue(roleFixup, "CellPhone", emp.CellPhone));

                header.Add("Pager");
                row.Add(getCellValue(roleFixup, "Pager", emp.Pager));

                header.Add("Email");
                row.Add(getCellValue(roleFixup, "Email", emp.Email));

                header.Add("Is Loan Officer");
                row.Add(getCellValue(roleFixup, "IsLoanOfficer", emp.IsLoanOfficer));

                header.Add("Is Processor (External)");
                row.Add(getCellValue(roleFixup, "IsBrokerProcessor", emp.IsBrokerProcessor));

                header.Add("Branch");
                row.Add(getCellValue(roleFixup, "BranchId", emp.BranchID));

                header.Add("Supervisor");
                row.Add(getCellValue(roleFixup, "PmlExternalManagerEmployeeId", emp.PmlExternalManagerEmployeeId));

                header.Add("IsSupervisor");
                row.Add(getCellValue(roleFixup, "IsPmlManager", emp.IsPmlManager));

                header.Add("Access Level");
                row.Add(getCellValue(roleFixup, "PmlLevelAccessDesc", emp.PmlLevelAccessDesc));

                header.Add("Password Expiration Option");
                row.Add(getCellValue(roleFixup, "PasswordExpirationOptionDesc", emp.PasswordExpirationOptionDesc));

                header.Add("Password Expiration Date");
                row.Add(getCellValue(roleFixup, "PasswordExpirationD", emp.PasswordExpirationD));

                header.Add("Password Expiration Period");
                row.Add(getCellValue(roleFixup, "PasswordExpirationPeriod", emp.PasswordExpirationPeriod));

                header.Add("Lender Account Exec");
                row.Add(getCellValue(roleFixup, "LenderAccExecEmployeeId", emp.LenderAcctExecEmployeeID));

                header.Add("Lender Account Exec Phone");
                row.Add(getCellValue(roleFixup, "LenderAccExecPhone", ""));

                header.Add("Lender Account Exec Email");
                row.Add(getCellValue(roleFixup, "LenderAccExecEmail", ""));
                
                if (isAdmin)
                {
                    header.Add("Lender Account Exec Login");
                    row.Add(getCellValue(roleFixup, "LenderAccExecLoginNm", ""));
                }

                header.Add("Lock Desk");
                row.Add(getCellValue(roleFixup, "LockDeskEmployeeId", emp.LockDeskEmployeeID));

                header.Add("Lock Desk Phone");
                row.Add(getCellValue(roleFixup, "LockDeskPhone", ""));

                header.Add("Lock Desk Email");
                row.Add(getCellValue(roleFixup, "LockDeskEmail", ""));

                if (isAdmin)
                {
                    header.Add("Lock Desk Login");
                    row.Add(getCellValue(roleFixup, "LockDeskLoginNm", ""));
                }

                header.Add("Manager");
                row.Add(getCellValue(roleFixup, "ManagerEmployeeId", emp.ManagerEmployeeID));

                header.Add("Manager Phone");
                row.Add(getCellValue(roleFixup, "ManagerPhone", ""));

                header.Add("Manager Email");
                row.Add(getCellValue(roleFixup, "ManagerEmail", ""));

                if (isAdmin)
                {
                    header.Add("Manager Login");
                    row.Add(getCellValue(roleFixup, "ManagerLoginNm", ""));
                }

                header.Add("Underwriter");
                row.Add(getCellValue(roleFixup, "UnderwriterEmployeeId", emp.UnderwriterEmployeeID));

                header.Add("Underwriter Phone");
                row.Add(getCellValue(roleFixup, "UnderwriterPhone", ""));

                header.Add("Underwriter Email");
                row.Add(getCellValue(roleFixup, "UnderwriterEmail", ""));

                if (isAdmin)
                {
                    header.Add("Underwriter Login");
                    row.Add(getCellValue(roleFixup, "UnderwriterLoginNm", ""));
                }

                header.Add("Processor");
                row.Add(getCellValue(roleFixup, "ProcessorEmployeeId", emp.ProcessorEmployeeID));

                header.Add("Processor Phone");
                row.Add(getCellValue(roleFixup, "ProcessorPhone", ""));

                header.Add("Processor Email");
                row.Add(getCellValue(roleFixup, "ProcessorEmail", ""));

                if (isAdmin)
                {
                    header.Add("Processor Login");
                    row.Add(getCellValue(roleFixup, "ProcessorLoginNm", ""));
                }

                header.Add("Price Group");
                row.Add(getCellValue(roleFixup, "LpePriceGroupId", emp.LpePriceGroupID));

                header.Add("IsActive");
                row.Add(getCellValue(roleFixup, "IsActive", emp.IsActive));

                header.Add("Last Login");
                row.Add(getCellValue(roleFixup, "RecentLoginD", emp.RecentLoginD));

                header.Add("Lending Licenses");
                row.Add(getCellValue(roleFixup, "LicenseXmlContent", emp.LicenseXmlContent));

                header.Add("Notes");
                row.Add(getCellValue(roleFixup, "NotesByEmployer", emp.NotesByEmployer));

                header.Add("Created");
                row.Add(getCellValue(roleFixup, "StartD", emp.StartD));

                header.Add("Company Id");
                row.Add(getCellValue(roleFixup, "PmlCompanyId", emp.PmlCompanyId));

                header.Add("Portal Mode");
                row.Add(getCellValue(roleFixup, "PortalMode", emp.PortalMode));

                string levelT = string.Empty;
                switch(emp.OriginatorCompensationSetLevelT)
                {
                    case E_OrigiantorCompenationLevelT.IndividualUser:
                        levelT = "Individual User";
                        break;
                    case E_OrigiantorCompenationLevelT.OriginatingCompany:
                        levelT = "Originating Company";
                        break;
                }
                header.Add("Originator Compensation Plan Level");
                row.Add(levelT);

                header.Add("Originator Compensation Plan Effective Date");
                row.Add((emp.OriginatorCompensationLastModifiedDate.HasValue ? losConvert.ToDateTimeString(emp.OriginatorCompensationLastModifiedDate.Value) : ""));

                header.Add("Originator Compensation Only Paid for 1st Lien of a Combo");
                row.Add(emp.OriginatorCompensationIsOnlyPaidForFirstLienOfCombo ? "Yes" : "No");

                header.Add("Originator Compensation %");
                row.Add(losConvert.ToRateString(emp.OriginatorCompensationPercent).Replace(",", ""));

                string baseT = string.Empty;
                switch(emp.OriginatorCompensationBaseT)
                {
                    case E_PercentBaseT.LoanAmount:
                        baseT = "Loan Amount";
                        break;
                    case E_PercentBaseT.TotalLoanAmount:
                        baseT = "Total Loan Amount";
                        break;
                }
                header.Add("Originator Compensation Basis");
                row.Add(baseT);

                header.Add("Originator Compensation Minimum");
                row.Add(losConvert.ToMoneyString(emp.OriginatorCompensationMinAmount, FormatDirection.ToRep).Replace(",", ""));

                header.Add("Originator Compensation Maximum");
                row.Add(losConvert.ToMoneyString(emp.OriginatorCompensationMaxAmount, FormatDirection.ToRep).Replace(",", ""));

                header.Add("Originator Compensation Fixed Amount");
                row.Add(losConvert.ToMoneyString(emp.OriginatorCompensationFixedAmount, FormatDirection.ToRep).Replace(",", ""));

                string notes = emp.OriginatorCompensationNotes.Replace(",", "").Replace(Environment.NewLine, " ");
                header.Add("Originator Compensation Notes");
                row.Add(notes);

                header.Add("Junior Underwriter");
                row.Add(getCellValue(roleFixup, "JuniorUnderwriterEmployeeId", emp.JuniorUnderwriterEmployeeID));

                header.Add("Junior Underwriter Phone");
                row.Add(getCellValue(roleFixup, "JuniorUnderwriterPhone", ""));

                header.Add("Junior Underwriter Email");
                row.Add(getCellValue(roleFixup, "Junior UnderwriterEmail", ""));

                if (isAdmin)
                {
                    header.Add("Junior Underwriter Login");
                    row.Add(getCellValue(roleFixup, "JuniorUnderwriterLoginNm", ""));
                }

                header.Add("Junior Processor");
                row.Add(getCellValue(roleFixup, "JuniorProcessorEmployeeId", emp.JuniorProcessorEmployeeID));

                header.Add("Junior Processor Phone");
                row.Add(getCellValue(roleFixup, "JuniorProcessorPhone", ""));

                header.Add("Junior Processor Email");
                row.Add(getCellValue(roleFixup, "Junior ProcessorEmail", ""));

                if (isAdmin)
                {
                    header.Add("Junior Processor Login");
                    row.Add(getCellValue(roleFixup, "JuniorProcessorLoginNm", ""));
                }

                header.Add("Loan Officer Assistant");
                row.Add(getCellValue(roleFixup, "LoanOfficerAssistantEmployeeId", emp.LoanOfficerAssistantEmployeeID));

                header.Add("Loan Officer Assistant Phone");
                row.Add(getCellValue(roleFixup, "LoanOfficerAssistantPhone", ""));

                header.Add("Loan Officer Assistant Email");
                row.Add(getCellValue(roleFixup, "Loan Officer AssistantEmail", ""));

                if (isAdmin)
                {
                    header.Add("Loan Officer Assistant Login");
                    row.Add(getCellValue(roleFixup, "LoanOfficerAssistantLoginNm", ""));
                }

                header.Add("Is Secondary (External)");
                row.Add(getCellValue(roleFixup, "IsExternalSecondary", emp.IsExternalSecondary));

                header.Add("Is Post-Closer (External)");
                row.Add(getCellValue(roleFixup, "IsExternalPostCloser", emp.IsExternalPostCloser));

                #region Mini-Correspondent Relationships
                header.Add("Mini-Correspondent Branch");
                row.Add(getCellValue(roleFixup, "MiniCorrespondentBranchId", emp.MiniCorrespondentBranchID));

                header.Add("Mini-Correspondent Price Group");
                row.Add(getCellValue(roleFixup, "MiniCorrespondentPriceGroupId", emp.MiniCorrespondentPriceGroupID));

                header.Add("Mini-Correspondent Manager");
                row.Add(getCellValue(roleFixup, "MiniCorrManagerEmployeeId", emp.MiniCorrManagerEmployeeId));

                header.Add("Mini-Correspondent Manager Phone");
                row.Add(getCellValue(roleFixup, "MiniCorrManagerPhone", ""));

                header.Add("Mini-Correspondent Manager Email");
                row.Add(getCellValue(roleFixup, "MiniCorrManagerEmail", ""));

                if (isAdmin)
                {
                    header.Add("Mini-Correspondent Manager Login");
                    row.Add(getCellValue(roleFixup, "MiniCorrManagerLoginNm", ""));
                }

                header.Add("Mini-Correspondent Processor");
                row.Add(getCellValue(roleFixup, "MiniCorrProcessorEmployeeId", emp.MiniCorrProcessorEmployeeId));

                header.Add("Mini-Correspondent Processor Phone");
                row.Add(getCellValue(roleFixup, "MiniCorrProcessorPhone", ""));

                header.Add("Mini-Correspondent Processor Email");
                row.Add(getCellValue(roleFixup, "MiniCorrProcessorEmail", ""));

                if (isAdmin)
                {
                    header.Add("Mini-Correspondent Processor Login");
                    row.Add(getCellValue(roleFixup, "MiniCorrProcessorLoginNm", ""));
                }

                header.Add("Mini-Correspondent Junior Processor");
                row.Add(getCellValue(roleFixup, "MiniCorrJuniorProcessorEmployeeId", emp.MiniCorrJuniorProcessorEmployeeId));

                header.Add("Mini-Correspondent Junior Processor Phone");
                row.Add(getCellValue(roleFixup, "MiniCorrJuniorProcessorPhone", ""));

                header.Add("Mini-Correspondent Junior Processor Email");
                row.Add(getCellValue(roleFixup, "MiniCorrJuniorProcessorEmail", ""));

                if (isAdmin)
                {
                    header.Add("Mini-Correspondent Junior Processor Login");
                    row.Add(getCellValue(roleFixup, "MiniCorrJuniorProcessorLoginNm", ""));
                }

                header.Add("Mini-Correspondent Lender Account Exec");
                row.Add(getCellValue(roleFixup, "MiniCorrLenderAccExecEmployeeId", emp.MiniCorrLenderAccExecEmployeeId));

                header.Add("Mini-Correspondent Lender Account Exec Phone");
                row.Add(getCellValue(roleFixup, "MiniCorrLenderAccExecPhone", ""));

                header.Add("Mini-Correspondent Lender Account Exec Email");
                row.Add(getCellValue(roleFixup, "MiniCorrLenderAccExecEmail", ""));

                if (isAdmin)
                {
                    header.Add("Mini-Correspondent Lender Account Exec Login");
                    row.Add(getCellValue(roleFixup, "MiniCorrLenderAccExecLoginNm", ""));
                }

                header.Add("Mini-Correspondent Post-Closer (External)");
                row.Add(getCellValue(roleFixup, "MiniCorrExternalPostCloserEmployeeId", emp.MiniCorrExternalPostCloserEmployeeId));

                header.Add("Mini-Correspondent Post-Closer (External) Phone");
                row.Add(getCellValue(roleFixup, "MiniCorrExternalPostCloserPhone", ""));

                header.Add("Mini-Correspondent Post-Closer (External) Email");
                row.Add(getCellValue(roleFixup, "MiniCorrExternalPostCloserEmail", ""));

                if (isAdmin)
                {
                    header.Add("Mini-Correspondent Post-Closer (External) Login");
                    row.Add(getCellValue(roleFixup, "MiniCorrExternalPostCloserLoginNm", ""));
                }

                header.Add("Mini-Correspondent Underwriter");
                row.Add(getCellValue(roleFixup, "MiniCorrUnderwriterEmployeeId", emp.MiniCorrUnderwriterEmployeeId));

                header.Add("Mini-Correspondent Underwriter Phone");
                row.Add(getCellValue(roleFixup, "MiniCorrUnderwriterPhone", ""));

                header.Add("Mini-Correspondent Underwriter Email");
                row.Add(getCellValue(roleFixup, "MiniCorrUnderwriterEmail", ""));

                if (isAdmin)
                {
                    header.Add("Mini-Correspondent Underwriter Login");
                    row.Add(getCellValue(roleFixup, "MiniCorrUnderwriterLoginNm", ""));
                }

                header.Add("Mini-Correspondent Junior Underwriter");
                row.Add(getCellValue(roleFixup, "MiniCorrJuniorUnderwriterEmployeeId", emp.MiniCorrJuniorUnderwriterEmployeeId));

                header.Add("Mini-Correspondent Junior Underwriter Phone");
                row.Add(getCellValue(roleFixup, "MiniCorrJuniorUnderwriterPhone", ""));

                header.Add("Mini-Correspondent Junior Underwriter Email");
                row.Add(getCellValue(roleFixup, "MiniCorrJuniorUnderwriterEmail", ""));

                if (isAdmin)
                {
                    header.Add("Mini-Correspondent Junior Underwriter Login");
                    row.Add(getCellValue(roleFixup, "MiniCorrJuniorUnderwriterLoginNm", ""));
                }

                header.Add("Mini-Correspondent Credit Auditor");
                row.Add(getCellValue(roleFixup, "MiniCorrCreditAuditorEmployeeId", emp.MiniCorrCreditAuditorEmployeeId));

                header.Add("Mini-Correspondent Credit Auditor Phone");
                row.Add(getCellValue(roleFixup, "MiniCorrCreditAuditorPhone", ""));

                header.Add("Mini-Correspondent Credit Auditor Email");
                row.Add(getCellValue(roleFixup, "MiniCorrCreditAuditorEmail", ""));

                if (isAdmin)
                {
                    header.Add("Mini-Correspondent Credit Auditor Login");
                    row.Add(getCellValue(roleFixup, "MiniCorrCreditAuditorLoginNm", ""));
                }

                header.Add("Mini-Correspondent Legal Auditor");
                row.Add(getCellValue(roleFixup, "MiniCorrLegalAuditorEmployeeId", emp.MiniCorrLegalAuditorEmployeeId));

                header.Add("Mini-Correspondent Legal Auditor Phone");
                row.Add(getCellValue(roleFixup, "MiniCorrLegalAuditorPhone", ""));

                header.Add("Mini-Correspondent Legal Auditor Email");
                row.Add(getCellValue(roleFixup, "MiniCorrLegalAuditorEmail", ""));

                if (isAdmin)
                {
                    header.Add("Mini-Correspondent Legal Auditor Login");
                    row.Add(getCellValue(roleFixup, "MiniCorrLegalAuditorLoginNm", ""));
                }

                header.Add("Mini-Correspondent Lock Desk");
                row.Add(getCellValue(roleFixup, "MiniCorrLockDeskEmployeeId", emp.MiniCorrLockDeskEmployeeId));

                header.Add("Mini-Correspondent Lock Desk Phone");
                row.Add(getCellValue(roleFixup, "MiniCorrLockDeskPhone", ""));

                header.Add("Mini-Correspondent Lock Desk Email");
                row.Add(getCellValue(roleFixup, "MiniCorrLockDeskEmail", ""));

                if (isAdmin)
                {
                    header.Add("Mini-Correspondent Lock Desk Login");
                    row.Add(getCellValue(roleFixup, "MiniCorrLockDeskLoginNm", ""));
                }

                header.Add("Mini-Correspondent Purchaser");
                row.Add(getCellValue(roleFixup, "MiniCorrPurchaserEmployeeId", emp.MiniCorrPurchaserEmployeeId));

                header.Add("Mini-Correspondent Purchaser Phone");
                row.Add(getCellValue(roleFixup, "MiniCorrPurchaserPhone", ""));

                header.Add("Mini-Correspondent Purchaser Email");
                row.Add(getCellValue(roleFixup, "MiniCorrPurchaserEmail", ""));

                if (isAdmin)
                {
                    header.Add("Mini-Correspondent Purchaser Login");
                    row.Add(getCellValue(roleFixup, "MiniCorrPurchaserLoginNm", ""));
                }

                header.Add("Mini-Correspondent Secondary");
                row.Add(getCellValue(roleFixup, "MiniCorrSecondaryEmployeeId", emp.MiniCorrSecondaryEmployeeId));

                header.Add("Mini-Correspondent Secondary Phone");
                row.Add(getCellValue(roleFixup, "MiniCorrSecondaryPhone", ""));

                header.Add("Mini-Correspondent Secondary Email");
                row.Add(getCellValue(roleFixup, "MiniCorrSecondaryEmail", ""));

                if (isAdmin)
                {
                    header.Add("Mini-Correspondent Secondary Login");
                    row.Add(getCellValue(roleFixup, "MiniCorrSecondaryLoginNm", ""));
                }
                #endregion

                #region Correspondent Relationships
                header.Add("Correspondent Branch");
                row.Add(getCellValue(roleFixup, "CorrespondentBranchId", emp.CorrespondentBranchID));

                header.Add("Correspondent Price Group");
                row.Add(getCellValue(roleFixup, "CorrespondentPriceGroupId", emp.CorrespondentPriceGroupID));

                header.Add("Correspondent Manager");
                row.Add(getCellValue(roleFixup, "CorrManagerEmployeeId", emp.CorrManagerEmployeeId));

                header.Add("Correspondent Manager Phone");
                row.Add(getCellValue(roleFixup, "CorrManagerPhone", ""));

                header.Add("Correspondent Manager Email");
                row.Add(getCellValue(roleFixup, "CorrManagerEmail", ""));

                if (isAdmin)
                {
                    header.Add("Correspondent Manager Login");
                    row.Add(getCellValue(roleFixup, "CorrManagerLoginNm", ""));
                }

                header.Add("Correspondent Processor");
                row.Add(getCellValue(roleFixup, "CorrProcessorEmployeeId", emp.CorrProcessorEmployeeId));

                header.Add("Correspondent Processor Phone");
                row.Add(getCellValue(roleFixup, "CorrProcessorPhone", ""));

                header.Add("Correspondent Processor Email");
                row.Add(getCellValue(roleFixup, "CorrProcessorEmail", ""));

                if (isAdmin)
                {
                    header.Add("Correspondent Processor Login");
                    row.Add(getCellValue(roleFixup, "CorrProcessorLoginNm", ""));
                }

                header.Add("Correspondent Junior Processor");
                row.Add(getCellValue(roleFixup, "CorrJuniorProcessorEmployeeId", emp.CorrJuniorProcessorEmployeeId));

                header.Add("Correspondent Junior Processor Phone");
                row.Add(getCellValue(roleFixup, "CorrJuniorProcessorPhone", ""));

                header.Add("Correspondent Junior Processor Email");
                row.Add(getCellValue(roleFixup, "CorrJuniorProcessorEmail", ""));

                if (isAdmin)
                {
                    header.Add("Correspondent Junior Processor Login");
                    row.Add(getCellValue(roleFixup, "CorrJuniorProcessorLoginNm", ""));
                }

                header.Add("Correspondent Lender Account Exec");
                row.Add(getCellValue(roleFixup, "CorrLenderAccExecEmployeeId", emp.CorrLenderAccExecEmployeeId));

                header.Add("Correspondent Lender Account Exec Phone");
                row.Add(getCellValue(roleFixup, "CorrLenderAccExecPhone", ""));

                header.Add("Correspondent Lender Account Exec Email");
                row.Add(getCellValue(roleFixup, "CorrLenderAccExecEmail", ""));

                if (isAdmin)
                {
                    header.Add("Correspondent Lender Account Exec Login");
                    row.Add(getCellValue(roleFixup, "CorrLenderAccExecLoginNm", ""));
                }

                header.Add("Correspondent Post-Closer (External)");
                row.Add(getCellValue(roleFixup, "CorrExternalPostCloserEmployeeId", emp.CorrExternalPostCloserEmployeeId));

                header.Add("Correspondent Post-Closer (External) Phone");
                row.Add(getCellValue(roleFixup, "CorrExternalPostCloserPhone", ""));

                header.Add("Correspondent Post-Closer (External) Email");
                row.Add(getCellValue(roleFixup, "CorrExternalPostCloserEmail", ""));

                if (isAdmin)
                {
                    header.Add("Correspondent Post-Closer (External) Login");
                    row.Add(getCellValue(roleFixup, "CorrExternalPostCloserLoginNm", ""));
                }

                header.Add("Correspondent Underwriter");
                row.Add(getCellValue(roleFixup, "CorrUnderwriterEmployeeId", emp.CorrUnderwriterEmployeeId));

                header.Add("Correspondent Underwriter Phone");
                row.Add(getCellValue(roleFixup, "CorrUnderwriterPhone", ""));

                header.Add("Correspondent Underwriter Email");
                row.Add(getCellValue(roleFixup, "CorrUnderwriterEmail", ""));

                if (isAdmin)
                {
                    header.Add("Correspondent Underwriter Login");
                    row.Add(getCellValue(roleFixup, "CorrUnderwriterLoginNm", ""));
                }

                header.Add("Correspondent Junior Underwriter");
                row.Add(getCellValue(roleFixup, "CorrJuniorUnderwriterEmployeeId", emp.CorrJuniorUnderwriterEmployeeId));

                header.Add("Correspondent Junior Underwriter Phone");
                row.Add(getCellValue(roleFixup, "CorrJuniorUnderwriterPhone", ""));

                header.Add("Correspondent Junior Underwriter Email");
                row.Add(getCellValue(roleFixup, "CorrJuniorUnderwriterEmail", ""));

                if (isAdmin)
                {
                    header.Add("Correspondent Junior Underwriter Login");
                    row.Add(getCellValue(roleFixup, "CorrJuniorUnderwriterLoginNm", ""));
                }

                header.Add("Correspondent Credit Auditor");
                row.Add(getCellValue(roleFixup, "CorrCreditAuditorEmployeeId", emp.CorrCreditAuditorEmployeeId));

                header.Add("Correspondent Credit Auditor Phone");
                row.Add(getCellValue(roleFixup, "CorrCreditAuditorPhone", ""));

                header.Add("Correspondent Credit Auditor Email");
                row.Add(getCellValue(roleFixup, "CorrCreditAuditorEmail", ""));

                if (isAdmin)
                {
                    header.Add("Correspondent Credit Auditor Login");
                    row.Add(getCellValue(roleFixup, "CorrCreditAuditorLoginNm", ""));
                }

                header.Add("Correspondent Legal Auditor");
                row.Add(getCellValue(roleFixup, "CorrLegalAuditorEmployeeId", emp.CorrLegalAuditorEmployeeId));

                header.Add("Correspondent Legal Auditor Phone");
                row.Add(getCellValue(roleFixup, "CorrLegalAuditorPhone", ""));

                header.Add("Correspondent Legal Auditor Email");
                row.Add(getCellValue(roleFixup, "CorrLegalAuditorEmail", ""));

                if (isAdmin)
                {
                    header.Add("Correspondent Legal Auditor Login");
                    row.Add(getCellValue(roleFixup, "CorrLegalAuditorLoginNm", ""));
                }

                header.Add("Correspondent Lock Desk");
                row.Add(getCellValue(roleFixup, "CorrLockDeskEmployeeId", emp.CorrLockDeskEmployeeId));

                header.Add("Correspondent Lock Desk Phone");
                row.Add(getCellValue(roleFixup, "CorrLockDeskPhone", ""));

                header.Add("Correspondent Lock Desk Email");
                row.Add(getCellValue(roleFixup, "CorrLockDeskEmail", ""));

                if (isAdmin)
                {
                    header.Add("Correspondent Lock Desk Login");
                    row.Add(getCellValue(roleFixup, "CorrLockDeskLoginNm", ""));
                }

                header.Add("Correspondent Purchaser");
                row.Add(getCellValue(roleFixup, "CorrPurchaserEmployeeId", emp.CorrPurchaserEmployeeId));

                header.Add("Correspondent Purchaser Phone");
                row.Add(getCellValue(roleFixup, "CorrPurchaserPhone", ""));

                header.Add("Correspondent Purchaser Email");
                row.Add(getCellValue(roleFixup, "CorrPurchaserEmail", ""));

                if (isAdmin)
                {
                    header.Add("Correspondent Purchaser Login");
                    row.Add(getCellValue(roleFixup, "CorrPurchaserLoginNm", ""));
                }

                header.Add("Correspondent Secondary");
                row.Add(getCellValue(roleFixup, "CorrSecondaryEmployeeId", emp.CorrSecondaryEmployeeId));

                header.Add("Correspondent Secondary Phone");
                row.Add(getCellValue(roleFixup, "CorrSecondaryPhone", ""));

                header.Add("Correspondent Secondary Email");
                row.Add(getCellValue(roleFixup, "CorrSecondaryEmail", ""));

                if (isAdmin)
                {
                    header.Add("Correspondent Secondary Login");
                    row.Add(getCellValue(roleFixup, "CorrSecondaryLoginNm", ""));
                }
                #endregion

                var broker = BrokerUserPrincipal.CurrentPrincipal.BrokerDB;
                if (E_PermissionType.HardDisable != broker.ActualNewLoanEditorUIPermissionType)
                {
                    header.Add("Opts to use new loan editor UI");
                    row.Add(getCellValue(roleFixup, "OptsToUseNewLoanEditorUI", ""));
                }

                header.Add("Populate Broker Relationships");

                if (emp.PopulateBrokerRelationshipsT == EmployeePopulationMethodT.IndividualUser)
                {
                    row.Add("Individual User");
                }
                else
                {
                    row.Add("Originating Company");
                }

                header.Add("Populate Mini-Correspondent Relationships");

                if (emp.PopulateMiniCorrRelationshipsT == EmployeePopulationMethodT.IndividualUser)
                {
                    row.Add("Individual User");
                }
                else
                {
                    row.Add("Originating Company");
                }

                header.Add("Populate Correspondent Relationships");

                if (emp.PopulateCorrRelationshipsT == EmployeePopulationMethodT.IndividualUser)
                {
                    row.Add("Individual User");
                }
                else
                {
                    row.Add("Originating Company");
                }

                header.Add("Populate PML Permissions");

                if (emp.PopulatePMLPermissionsT == EmployeePopulationMethodT.IndividualUser)
                {
                    row.Add("Individual User");
                }
                else
                {
                    row.Add("Originating Company");
                }

                header.Add("Can Apply For Ineligible Loan Programs");
                row.Add(p.HasPermission(Permission.CanApplyForIneligibleLoanPrograms).ToString());

                header.Add("Middle Name");
                row.Add(getCellValue(roleFixup, "MiddleName", emp.MiddleName));

                header.Add("IP Address Restriction");
                row.Add(emp.EnabledIpRestriction ? "Restricted" : "Unrestricted");
                header.Add("IPs on Global Whitelist");
                row.Add(emp.EnabledIpRestriction ? "Yes" : string.Empty);
                header.Add("Devices with a Certificate");
                row.Add(emp.EnabledIpRestriction ? "Yes" : string.Empty);
                header.Add("Whitelisted IPs");
                row.Add(((Func<EmployeeDB, string>)(e =>
                {
                    if (string.IsNullOrEmpty(e.MustLogInFromTheseIpAddresses))
                    {
                        return string.Empty;
                    }

                    return string.Join("+", e.MustLogInFromTheseIpAddresses.Split(';').Where(s => !string.IsNullOrEmpty(s)));
                }))(emp));
                header.Add("Multi-Factor Authentication");
                row.Add(emp.EnabledMultiFactorAuthentication ? "Yes" : "No");
                header.Add("Allow Installing Digital Certificates");
                row.Add(emp.EnabledClientDigitalCertificateInstall ? "Yes" : "No");
                header.Add("List of Digital Certificates");
                row.Add(((Func<EmployeeDB, ClientCertificates, string>)((e, certs) =>
                {
                    var certificates = certs.GetCertificatesForUser(emp.UserID)
                        .OrderByDescending(c => c.CreatedDate)
                        .Select(c => c.Description + "::" + c.CreatedDate);

                    return string.Join("+", certificates);
                }))(emp, clientCertificates));
                header.Add("IP Access");
                row.Add(((Func<EmployeeDB, UserRegisteredIpAddresses, string>)((e, ips) =>
                {
                    var ipAddrs = ips.GetRegisteredIpsForUser(emp.UserID)
                        .Select(ip => ip.IpAddress);
                    return string.Join("+", ipAddrs);
                }))(emp, registeredIpAddresses));
                header.Add("Enable Auth Code via SMS");
                row.Add(emp.EnableAuthCodeViaSms ? "Yes" : "No");
                header.Add("Has Cell Phone Number");
                row.Add(emp.HasCellPhoneNumber ? "Yes" : "No");
                header.Add("NMLS ID");
                row.Add(getCellValue(roleFixup, "NmlsId", emp.LosIdentifier));
            }

            // Get CSV from Exporter
            string sCSV = PmlBroker.CreateContactInfoCSV(principal, "", loginName, Guid.Empty);

            Assert.IsNotEmpty(sCSV);

            // Compare expected CSV and CSV from Exporter output
            string sExpected = $"{string.Join(",", header)}{Environment.NewLine}{string.Join(",", row)}{Environment.NewLine}";
            Assert.AreEqual(sExpected, sCSV);
        }

        [Test]
        public void CreateContactInfoCSVForPmlSupervisorTest()
        {
            
            AbstractUserPrincipal pmlPrincipal = LoginTools.LoginAs(TestAccountType.Pml_Supervisor);
            Guid SupervisorEmployeeId = pmlPrincipal.EmployeeId;

            Assert.AreEqual(pmlPrincipal.Type, "P");

            // Login
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.LoTest001);
            Assert.AreEqual(pmlPrincipal.BrokerId, principal.BrokerId);
            
            PmlBroker.RoleFixup roleFixup = new PmlBroker.RoleFixup();
            roleFixup.Retrieve(principal.BrokerId);

            LosConvert losConvert = new LosConvert();

            // Retrieve all client certificates and registered IPs manually to 
            // prevent holding the data reader open while they are retrieved.
            var clientCertificates = new ClientCertificates(principal.BrokerId);
            clientCertificates.Retrieve();
            var registeredIpAddresses = new UserRegisteredIpAddresses(principal.BrokerId);
            registeredIpAddresses.Retrieve();

             // Get Employee from DB
            List<SqlParameter> aP = new List<SqlParameter>();
            aP.Add(new SqlParameter("@BrokerId", principal.BrokerId));
            aP.Add(new SqlParameter("@Type", "P"));
            aP.Add(new SqlParameter("@SupervisorEmployeeId", SupervisorEmployeeId));
            BrokerUserPermissions p;
            using (DbDataReader sR = StoredProcedureHelper.ExecuteReader(principal.BrokerId, "ListEmployeeContactInfoByBrokerIdForPmlSupervisor", aP))
            {
                // Get First Employee
                EmployeeDB emp = new EmployeeDB();
                if (!sR.Read())
                {
                    Assert.Ignore("Cannot run test. No Employees found.");
                }



                emp.Retrieve(sR);
                emp.BrokerID = principal.BrokerId;

                p = new BrokerUserPermissions();
                p.Retrieve(principal.BrokerId, emp.ID, sR, null);

                string loginName = emp.LoginName;

                // Build CSV using DB info
                var header = new List<string>();
                var row = new List<string>();

                header.Add("Login");
                row.Add(getCellValue(roleFixup, "LoginNm", emp.LoginName));

                header.Add("First Name");
                row.Add(getCellValue(roleFixup, "UserFirstNm", emp.FirstName));

                header.Add("Last Name");
                row.Add(getCellValue(roleFixup, "UserLastNm", emp.LastName));

                header.Add("Company Name");
                row.Add(getCellValue(roleFixup, "PmlBrokerName", emp.PmlBrokerName));

                header.Add("Phone");
                row.Add(getCellValue(roleFixup, "Phone", emp.Phone));

                header.Add("Cell Phone");
                row.Add(getCellValue(roleFixup, "CellPhone", emp.CellPhone));

                header.Add("Pager");
                row.Add(getCellValue(roleFixup, "Pager", emp.Pager));

                header.Add("Fax");
                row.Add(getCellValue(roleFixup, "Fax", emp.Fax));

                header.Add("Email");
                row.Add(getCellValue(roleFixup, "Email", emp.Email));

                header.Add("IsActive");
                row.Add(getCellValue(roleFixup, "IsActive", emp.IsActive));

                header.Add("Is Loan Officer");
                row.Add(getCellValue(roleFixup, "IsLoanOfficer", emp.IsLoanOfficer));

                header.Add("Is Processor (External)");
                row.Add(getCellValue(roleFixup, "IsBrokerProcessor", emp.IsBrokerProcessor));

                header.Add("Manager");
                row.Add(getCellValue(roleFixup, "ManagerEmployeeId", emp.ManagerEmployeeID));

                header.Add("Lock Desk");
                row.Add(getCellValue(roleFixup, "LockDeskEmployeeId", emp.LockDeskEmployeeID));

                header.Add("Lender Account Exec");
                row.Add(getCellValue(roleFixup, "LenderAccExecEmployeeId", emp.LenderAcctExecEmployeeID));

                header.Add("Underwriter");
                row.Add(getCellValue(roleFixup, "UnderwriterEmployeeId", emp.UnderwriterEmployeeID));

                header.Add("Processor");
                row.Add(getCellValue(roleFixup, "ProcessorEmployeeId", emp.ProcessorEmployeeID));

                header.Add("Access Level");
                row.Add(getCellValue(roleFixup, "PmlLevelAccessDesc", emp.PmlLevelAccessDesc));

                header.Add("Password Expiration Option");
                row.Add(getCellValue(roleFixup, "PasswordExpirationOptionDesc", emp.PasswordExpirationOptionDesc));

                header.Add("Password Expiration Date");
                row.Add(getCellValue(roleFixup, "PasswordExpirationD", emp.PasswordExpirationD));

                header.Add("Password Expiration Period");
                row.Add(getCellValue(roleFixup, "PasswordExpirationPeriod", emp.PasswordExpirationPeriod));

                header.Add("Company Id");
                row.Add(getCellValue(roleFixup, "PmlCompanyId", emp.PmlCompanyId));

                header.Add("Portal Mode");
                row.Add(getCellValue(roleFixup, "PortalMode", emp.PortalMode));

                string levelT = string.Empty;
                switch (emp.OriginatorCompensationSetLevelT)
                {
                    case E_OrigiantorCompenationLevelT.IndividualUser:
                        levelT = "Individual User";
                        break;
                    case E_OrigiantorCompenationLevelT.OriginatingCompany:
                        levelT = "Originating Company";
                        break;
                }
                header.Add("Originator Compensation Plan Level");
                row.Add(levelT);

                header.Add("Originator Compensation Plan Effective Date");
                row.Add((emp.OriginatorCompensationLastModifiedDate.HasValue ? losConvert.ToDateTimeString(emp.OriginatorCompensationLastModifiedDate.Value) : ""));

                header.Add("Originator Compensation Only Paid for 1st Lien of a Combo");
                row.Add(emp.OriginatorCompensationIsOnlyPaidForFirstLienOfCombo ? "Yes" : "No");

                header.Add("Originator Compensation %");
                row.Add(losConvert.ToRateString(emp.OriginatorCompensationPercent).Replace(",", ""));

                string baseT = string.Empty;
                switch (emp.OriginatorCompensationBaseT)
                {
                    case E_PercentBaseT.LoanAmount:
                        baseT = "Loan Amount";
                        break;
                    case E_PercentBaseT.TotalLoanAmount:
                        baseT = "Total Loan Amount";
                        break;
                }
                header.Add("Originator Compensation Basis");
                row.Add(baseT);

                header.Add("Originator Compensation Minimum");
                row.Add(losConvert.ToMoneyString(emp.OriginatorCompensationMinAmount, FormatDirection.ToRep).Replace(",", ""));

                header.Add("Originator Compensation Maximum");
                row.Add(losConvert.ToMoneyString(emp.OriginatorCompensationMaxAmount, FormatDirection.ToRep).Replace(",", ""));

                header.Add("Originator Compensation Fixed Amount");
                row.Add(losConvert.ToMoneyString(emp.OriginatorCompensationFixedAmount, FormatDirection.ToRep).Replace(",", ""));

                string notes = emp.OriginatorCompensationNotes.Replace(",", "").Replace(Environment.NewLine, " ");
                header.Add("Originator Compensation Notes");
                row.Add(notes);

                header.Add("Junior Underwriter");
                row.Add(getCellValue(roleFixup, "JuniorUnderwriterEmployeeId", emp.JuniorUnderwriterEmployeeID));

                header.Add("Junior Processor");
                row.Add(getCellValue(roleFixup, "JuniorProcessorEmployeeId", emp.JuniorProcessorEmployeeID));

                header.Add("Is Secondary (External)");
                row.Add(getCellValue(roleFixup, "IsExternalSecondary", emp.IsExternalSecondary));

                header.Add("Is Post-Closer (External)");
                row.Add(getCellValue(roleFixup, "IsExternalPostCloser", emp.IsExternalPostCloser));

                #region Mini-Correspondent Relationships
                header.Add("Mini-Correspondent Manager");
                row.Add(getCellValue(roleFixup, "MiniCorrManagerEmployeeId", emp.MiniCorrManagerEmployeeId));

                header.Add("Mini-Correspondent Processor");
                row.Add(getCellValue(roleFixup, "MiniCorrProcessorEmployeeId", emp.MiniCorrProcessorEmployeeId));

                header.Add("Mini-Correspondent Junior Processor");
                row.Add(getCellValue(roleFixup, "MiniCorrJuniorProcessorEmployeeId", emp.MiniCorrJuniorProcessorEmployeeId));

                header.Add("Mini-Correspondent Lender Account Exec");
                row.Add(getCellValue(roleFixup, "MiniCorrLenderAccExecEmployeeId", emp.MiniCorrLenderAccExecEmployeeId));

                header.Add("Mini-Correspondent Post-Closer (External)");
                row.Add(getCellValue(roleFixup, "MiniCorrExternalPostCloserEmployeeId", emp.MiniCorrExternalPostCloserEmployeeId));

                header.Add("Mini-Correspondent Underwriter");
                row.Add(getCellValue(roleFixup, "MiniCorrUnderwriterEmployeeId", emp.MiniCorrUnderwriterEmployeeId));

                header.Add("Mini-Correspondent Junior Underwriter");
                row.Add(getCellValue(roleFixup, "MiniCorrJuniorUnderwriterEmployeeId", emp.MiniCorrJuniorUnderwriterEmployeeId));

                header.Add("Mini-Correspondent Credit Auditor");
                row.Add(getCellValue(roleFixup, "MiniCorrCreditAuditorEmployeeId", emp.MiniCorrCreditAuditorEmployeeId));

                header.Add("Mini-Correspondent Legal Auditor");
                row.Add(getCellValue(roleFixup, "MiniCorrLegalAuditorEmployeeId", emp.MiniCorrLegalAuditorEmployeeId));

                header.Add("Mini-Correspondent Lock Desk");
                row.Add(getCellValue(roleFixup, "MiniCorrLockDeskEmployeeId", emp.MiniCorrLockDeskEmployeeId));

                header.Add("Mini-Correspondent Purchaser");
                row.Add(getCellValue(roleFixup, "MiniCorrPurchaserEmployeeId", emp.MiniCorrPurchaserEmployeeId));

                header.Add("Mini-Correspondent Secondary");
                row.Add(getCellValue(roleFixup, "MiniCorrSecondaryEmployeeId", emp.MiniCorrSecondaryEmployeeId));
                #endregion

                #region Correspondent Relationships
                header.Add("Correspondent Manager");
                row.Add(getCellValue(roleFixup, "CorrManagerEmployeeId", emp.CorrManagerEmployeeId));

                header.Add("Correspondent Processor");
                row.Add(getCellValue(roleFixup, "CorrProcessorEmployeeId", emp.CorrProcessorEmployeeId));

                header.Add("Correspondent Junior Processor");
                row.Add(getCellValue(roleFixup, "CorrJuniorProcessorEmployeeId", emp.CorrJuniorProcessorEmployeeId));

                header.Add("Correspondent Lender Account Exec");
                row.Add(getCellValue(roleFixup, "CorrLenderAccExecEmployeeId", emp.CorrLenderAccExecEmployeeId));

                header.Add("Correspondent Post-Closer (External)");
                row.Add(getCellValue(roleFixup, "CorrExternalPostCloserEmployeeId", emp.CorrExternalPostCloserEmployeeId));

                header.Add("Correspondent Underwriter");
                row.Add(getCellValue(roleFixup, "CorrUnderwriterEmployeeId", emp.CorrUnderwriterEmployeeId));

                header.Add("Correspondent Junior Underwriter");
                row.Add(getCellValue(roleFixup, "CorrJuniorUnderwriterEmployeeId", emp.CorrJuniorUnderwriterEmployeeId));

                header.Add("Correspondent Credit Auditor");
                row.Add(getCellValue(roleFixup, "CorrCreditAuditorEmployeeId", emp.CorrCreditAuditorEmployeeId));

                header.Add("Correspondent Legal Auditor");
                row.Add(getCellValue(roleFixup, "CorrLegalAuditorEmployeeId", emp.CorrLegalAuditorEmployeeId));

                header.Add("Correspondent Lock Desk");
                row.Add(getCellValue(roleFixup, "CorrLockDeskEmployeeId", emp.CorrLockDeskEmployeeId));

                header.Add("Correspondent Purchaser");
                row.Add(getCellValue(roleFixup, "CorrPurchaserEmployeeId", emp.CorrPurchaserEmployeeId));

                header.Add("Correspondent Secondary");
                row.Add(getCellValue(roleFixup, "CorrSecondaryEmployeeId", emp.CorrSecondaryEmployeeId));
                #endregion

                header.Add("Populate Broker Relationships");

                if (emp.PopulateBrokerRelationshipsT == EmployeePopulationMethodT.IndividualUser)
                {
                    row.Add("Individual User");
                }
                else
                {
                    row.Add("Originating Company");
                }

                header.Add("Populate Mini-Correspondent Relationships");

                if (emp.PopulateMiniCorrRelationshipsT == EmployeePopulationMethodT.IndividualUser)
                {
                    row.Add("Individual User");
                }
                else
                {
                    row.Add("Originating Company");
                }

                header.Add("Populate Correspondent Relationships");

                if (emp.PopulateCorrRelationshipsT == EmployeePopulationMethodT.IndividualUser)
                {
                    row.Add("Individual User");
                }
                else
                {
                    row.Add("Originating Company");
                }

                header.Add("Populate PML Permissions");

                if (emp.PopulatePMLPermissionsT == EmployeePopulationMethodT.IndividualUser)
                {
                    row.Add("Individual User");
                }
                else
                {
                    row.Add("Originating Company");
                }

                header.Add("Can Apply For Ineligible Loan Programs");
                row.Add(p.HasPermission(Permission.CanApplyForIneligibleLoanPrograms).ToString());

                header.Add("Middle Name");
                row.Add(getCellValue(roleFixup, "MiddleName", emp.MiddleName));

                header.Add("IP Address Restriction");
                row.Add(emp.EnabledIpRestriction ? "Restricted" : "Unrestricted");
                header.Add("IPs on Global Whitelist");
                row.Add(emp.EnabledIpRestriction ? "Yes" : string.Empty);
                header.Add("Devices with a Certificate");
                row.Add(emp.EnabledIpRestriction ? "Yes" : string.Empty);
                header.Add("Whitelisted IPs");
                row.Add(((Func<EmployeeDB, string>)(e =>
                {
                    if (string.IsNullOrEmpty(e.MustLogInFromTheseIpAddresses))
                    {
                        return string.Empty;
                    }

                    return string.Join("+", e.MustLogInFromTheseIpAddresses.Split(';').Where(s => !string.IsNullOrEmpty(s)));
                }))(emp));
                header.Add("Multi-Factor Authentication");
                row.Add(emp.EnabledMultiFactorAuthentication ? "Yes" : "No");
                header.Add("Allow Installing Digital Certificates");
                row.Add(emp.EnabledClientDigitalCertificateInstall ? "Yes" : "No");
                header.Add("List of Digital Certificates");
                row.Add(((Func<EmployeeDB, ClientCertificates, string>)((e, certs) =>
                {
                    var certificates = certs.GetCertificatesForUser(emp.UserID)
                        .OrderByDescending(c => c.CreatedDate)
                        .Select(c => c.Description + "::" + c.CreatedDate);

                    return string.Join("+", certificates);
                }))(emp, clientCertificates));
                header.Add("IP Access");
                row.Add(((Func<EmployeeDB, UserRegisteredIpAddresses, string>)((e, ips) =>
                {
                    var ipAddrs = ips.GetRegisteredIpsForUser(emp.UserID)
                        .Select(ip => ip.IpAddress);
                    return string.Join("+", ipAddrs);
                }))(emp, registeredIpAddresses));
                header.Add("Enable Auth Code via SMS");
                row.Add(emp.EnableAuthCodeViaSms ? "Yes" : "No");
                header.Add("Has Cell Phone Number");
                row.Add(emp.HasCellPhoneNumber ? "Yes" : "No");
                header.Add("NMLS ID");
                row.Add(getCellValue(roleFixup, "NmlsId", emp.LosIdentifier));

                // Get CSV from Exporter
                string sCSV = PmlBroker.CreateContactInfoCSVForPmlSupervisor(principal.BrokerId, SupervisorEmployeeId, loginName, Guid.Empty);

                Assert.IsNotEmpty(sCSV);

                // Compare expected CSV and CSV from Exporter output
                string sExpected = $"{string.Join(",", header)}{Environment.NewLine}{string.Join(",", row)}{Environment.NewLine}";
                Assert.AreEqual(sExpected, sCSV);
            }
        }
    }
}