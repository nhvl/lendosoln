﻿//Antonio Valencia
//Due to lack of time these test will need some work in the future. 
//I copied paste the top 16 lines of the fha file into our test data. 
//And just ran some checks to see if it works. 
//Currently there is a gap of testing. The code that actually makes sure the stuff is filtered out correctly and put into the database. 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOfficeApp.ObjLib.LoanLimitsData;
using System.IO;
using NUnit.Framework;
using System.Collections; 
using LendOSolnTest.Common;

namespace LendOSolnTest.LoanLimits
{
    [TestFixture]
    public class LoanLimitParserTest
    {
        [Test]
        public void ParsingTest()
        {
            string data =
@"                                                            203B S       0271050034700004194000521250                                                      20080305 
                                                            ZZ203S       0729750093420011292501403400                                                      20081202 
9999900000NON-METRO                                         203B H00880000271050034700004194000521250IA135IOWA                      MONROE         2008030520080305 
1446014484BOSTON-QUINCY, MA METROPOLITAN DIVISION           203B H04190000523750067050008104501007200MA023MASSACHUSETTS             PLYMOUTH       2004111320080305 
4790047894WASHINGTON-ARLINGTON-ALEXANDRIA, DC-VA-MD-WV METRO203B H06700000729750093420011292501403400VA107VIRGINIA                  LOUDOUN        2004111320080305 
2634000000HOUGHTON, MI (MICRO)                              203B H01170000271050034700004194000521250MI083MICHIGAN                  KEWEENAW       2004111320080305 
9999900000NON-METRO                                         203B H00780000271050034700004194000521250AR003ARKANSAS                  ASHLEY         2008030520080305 
9999900000NON-METRO                                         203B H00910000271050034700004194000521250MT091MONTANA                   SHERIDAN       2008030520080305 
2486000000GREENVILLE, SC (MSA)                              203B H02360000295000037765004565000567300SC045SOUTH CAROLINA            GREENVILLE     2004111320080305 
9999900000NON-METRO                                         203B H01190000271050034700004194000521250SC027SOUTH CAROLINA            CLARENDON      2008030520080305 
3942000000PULLMAN, WA (MICRO)                               203B H02110000271050034700004194000521250WA075WASHINGTON                WHITMAN        2004111320080305 
1206000000ATLANTA-SANDY SPRINGS-MARIETTA, GA (MSA)          203B H02770000346250044325005358000665850GA247GEORGIA                   ROCKDALE       2004111320080305 
1630000000CEDAR RAPIDS, IA (MSA)                            203B H01280000271050034700004194000521250IA113IOWA                      LINN           2004111320080305 
9999900000NON-METRO                                         203B H00620000271050034700004194000521250IL193ILLINOIS                  WHITE          2008030520080305 
9999900000NON-METRO                                         203B H01080000271050034700004194000521250MI127MICHIGAN                  OCEANA         2008030520080305 
1146000000ANN ARBOR, MI (MSA)                               203B H02760000345000044165005338500663450MI161MICHIGAN                  WASHTENAW      2004111320080305 ";

            LoanLimitParser parser = new FixedLengthLoanLimitParser(data);
            IEnumerable<LoanLimitEntry> entries = parser.GetLoanLimitEntries();
            Assert.That(entries.Count(), Is.EqualTo(16)); 

            //lets check some random ones. 
            List<LoanLimitEntry> entryList = new List<LoanLimitEntry>(entries);
            Assert.That(entryList[0].State, Is.EqualTo(""));  //standard entry 
            Assert.That(entryList[0].SoaCode, Is.EqualTo("203B"));
            Assert.That(entryList[0].FipsCode, Is.EqualTo(0));
            Assert.That(entryList[0].Limit1Units, Is.EqualTo("0271050"));
            Assert.That(entryList[0].Limit2Units, Is.EqualTo("0347000"));
            Assert.That(entryList[0].Limit3Units, Is.EqualTo("0419400"));
            Assert.That(entryList[0].Limit4Units, Is.EqualTo("0521250"));


            Assert.That(entryList[1].State, Is.EqualTo(""));
            Assert.That(entryList[1].SoaCode, Is.EqualTo("ZZ203"));
            Assert.That(entryList[1].FipsCode, Is.EqualTo(0));
            Assert.That(entryList[1].Limit1Units, Is.EqualTo("0729750"));
            Assert.That(entryList[1].Limit2Units, Is.EqualTo("0934200"));
            Assert.That(entryList[1].Limit3Units, Is.EqualTo("1129250"));
            Assert.That(entryList[1].Limit4Units, Is.EqualTo("1403400"));


            Assert.That(entryList[6].State, Is.EqualTo("AR"));
            Assert.That(entryList[6].SoaCode, Is.EqualTo("203B"));
            Assert.That(entryList[6].FipsCode, Is.EqualTo(5003)); //arkansas ashley
            Assert.That(entryList[6].Limit1Units, Is.EqualTo("0271050"));
            Assert.That(entryList[6].Limit2Units, Is.EqualTo("0347000"));
            Assert.That(entryList[6].Limit3Units, Is.EqualTo("0419400"));
            Assert.That(entryList[6].Limit4Units, Is.EqualTo("0521250"));

            Assert.That(entryList[11].State, Is.EqualTo("GA"));
            Assert.That(entryList[11].SoaCode, Is.EqualTo("203B"));
            Assert.That(entryList[11].FipsCode, Is.EqualTo(13247));
            Assert.That(entryList[11].Limit1Units, Is.EqualTo("0346250"));
            Assert.That(entryList[11].Limit2Units, Is.EqualTo("0443250"));
            Assert.That(entryList[11].Limit3Units, Is.EqualTo("0535800"));
            Assert.That(entryList[11].Limit4Units, Is.EqualTo("0665850"));
        }

        [Test]
        public void TestRetrievingLoanLimitsFHA()
        {
            DateTime year2016 = new DateTime(2016, 1, 1);
            DateTime year2017 = new DateTime(2017, 1, 1);
            DateTime year2018 = new DateTime(2018, 1, 1);
            DateTime year2019 = new DateTime(2019, 1, 1);
            DateTime latestYear = year2019;

            // Test county: Ashley County, Arkansas: FIPS 05003
            LoanLimit ashleyCountyArkansas2016 = LoanLimit.GetLoanLimitFor(E_LoanLimitType.FHA, 5003, year2016);
            Assert.That(ashleyCountyArkansas2016, Is.Not.Null);
            Assert.That(ashleyCountyArkansas2016.FipsCode, Is.EqualTo(5003));
            Assert.That(ashleyCountyArkansas2016.Limit1Units, Is.EqualTo(0271050));
            Assert.That(ashleyCountyArkansas2016.Limit2Units, Is.EqualTo(0347000));
            Assert.That(ashleyCountyArkansas2016.Limit3Units, Is.EqualTo(0419425));
            Assert.That(ashleyCountyArkansas2016.Limit4Units, Is.EqualTo(0521250));

            LoanLimit ashleyCountyArkansas2017 = LoanLimit.GetLoanLimitFor(E_LoanLimitType.FHA, 5003, year2017);
            Assert.That(ashleyCountyArkansas2017, Is.Not.Null);
            Assert.That(ashleyCountyArkansas2017.FipsCode, Is.EqualTo(5003));
            Assert.That(ashleyCountyArkansas2017.Limit1Units, Is.EqualTo(0275665));
            Assert.That(ashleyCountyArkansas2017.Limit2Units, Is.EqualTo(0352950));
            Assert.That(ashleyCountyArkansas2017.Limit3Units, Is.EqualTo(0426625));
            Assert.That(ashleyCountyArkansas2017.Limit4Units, Is.EqualTo(0530150));

            LoanLimit ashleyCountyArkansas2018 = LoanLimit.GetLoanLimitFor(E_LoanLimitType.FHA, 5003, year2018);
            Assert.That(ashleyCountyArkansas2018, Is.Not.Null);
            Assert.That(ashleyCountyArkansas2018.FipsCode, Is.EqualTo(5003));
            Assert.That(ashleyCountyArkansas2018.Limit1Units, Is.EqualTo(0294515));
            Assert.That(ashleyCountyArkansas2018.Limit2Units, Is.EqualTo(0377075));
            Assert.That(ashleyCountyArkansas2018.Limit3Units, Is.EqualTo(0455800));
            Assert.That(ashleyCountyArkansas2018.Limit4Units, Is.EqualTo(0566425));

            LoanLimit ashleyCountyArkansas2019 = LoanLimit.GetLoanLimitFor(E_LoanLimitType.FHA, 5003, year2019);
            Assert.That(ashleyCountyArkansas2019, Is.Not.Null);
            Assert.That(ashleyCountyArkansas2019.FipsCode, Is.EqualTo(5003));
            Assert.That(ashleyCountyArkansas2019.Limit1Units, Is.EqualTo(0314827));
            Assert.That(ashleyCountyArkansas2019.Limit2Units, Is.EqualTo(0403125));
            Assert.That(ashleyCountyArkansas2019.Limit3Units, Is.EqualTo(0487250));
            Assert.That(ashleyCountyArkansas2019.Limit4Units, Is.EqualTo(0605525));

            LoanLimit standard = LoanLimit.GetLoanLimitFor(E_LoanLimitType.FHA, 9999, year2018);
            Assert.That(standard, Is.Not.Null);
            Assert.That(standard.FipsCode, Is.EqualTo(0));
            Assert.That(standard.Limit1Units, Is.EqualTo(0271050));
            Assert.That(standard.Limit2Units, Is.EqualTo(0347000));
            Assert.That(standard.Limit3Units, Is.EqualTo(0419425));
            Assert.That(standard.Limit4Units, Is.EqualTo(0521250));

            standard = LoanLimit.GetLoanLimitFor(E_LoanLimitType.FHA, 9999, year2019);
            Assert.That(standard, Is.Not.Null);
            Assert.That(standard.FipsCode, Is.EqualTo(0));
            Assert.That(standard.Limit1Units, Is.EqualTo(0271050));
            Assert.That(standard.Limit2Units, Is.EqualTo(0347000));
            Assert.That(standard.Limit3Units, Is.EqualTo(0419425));
            Assert.That(standard.Limit4Units, Is.EqualTo(0521250));

            LoanLimit selectedS = LoanLimit.GetLoanLimitFor(E_LoanLimitType.FHA, 0, year2016);
            Assert.That(selectedS, Is.Not.Null);
            Assert.That(selectedS.FipsCode, Is.EqualTo(0));
            Assert.That(selectedS.Limit1Units, Is.EqualTo(0271050));
            Assert.That(selectedS.Limit2Units, Is.EqualTo(0347000));
            Assert.That(selectedS.Limit3Units, Is.EqualTo(0419425));
            Assert.That(selectedS.Limit4Units, Is.EqualTo(0521250));

            selectedS = LoanLimit.GetLoanLimitFor(E_LoanLimitType.FHA, 0, year2017);
            Assert.That(selectedS, Is.Not.Null);
            Assert.That(selectedS.FipsCode, Is.EqualTo(0));
            Assert.That(selectedS.Limit1Units, Is.EqualTo(0275665));
            Assert.That(selectedS.Limit2Units, Is.EqualTo(0352950));
            Assert.That(selectedS.Limit3Units, Is.EqualTo(0426625));
            Assert.That(selectedS.Limit4Units, Is.EqualTo(0530150));

            selectedS = LoanLimit.GetLoanLimitFor(E_LoanLimitType.FHA, 0, year2018);
            Assert.That(selectedS, Is.Not.Null);
            Assert.That(selectedS.FipsCode, Is.EqualTo(0));
            Assert.That(selectedS.Limit1Units, Is.EqualTo(0294515));
            Assert.That(selectedS.Limit2Units, Is.EqualTo(0377075));
            Assert.That(selectedS.Limit3Units, Is.EqualTo(0455800));
            Assert.That(selectedS.Limit4Units, Is.EqualTo(0566425));

            selectedS = LoanLimit.GetLoanLimitFor(E_LoanLimitType.FHA, 0, year2019);
            Assert.That(selectedS, Is.Not.Null);
            Assert.That(selectedS.FipsCode, Is.EqualTo(0));
            Assert.That(selectedS.Limit1Units, Is.EqualTo(0314827));
            Assert.That(selectedS.Limit2Units, Is.EqualTo(0403125));
            Assert.That(selectedS.Limit3Units, Is.EqualTo(0487250));
            Assert.That(selectedS.Limit4Units, Is.EqualTo(0605525));

            // Rockdale County Georgia: FIPS 13247
            selectedS = LoanLimit.GetLoanLimitFor(E_LoanLimitType.FHA, 13247, year2017);
            Assert.That(selectedS.FipsCode, Is.EqualTo(13247));
            Assert.That(selectedS.Limit1Units, Is.EqualTo(0358800));
            Assert.That(selectedS.Limit2Units, Is.EqualTo(0459300));
            Assert.That(selectedS.Limit3Units, Is.EqualTo(0555200));
            Assert.That(selectedS.Limit4Units, Is.EqualTo(0690000));

            selectedS = LoanLimit.GetLoanLimitFor(E_LoanLimitType.FHA, 13247, year2018);
            Assert.That(selectedS.FipsCode, Is.EqualTo(13247));
            Assert.That(selectedS.Limit1Units, Is.EqualTo(0359950));
            Assert.That(selectedS.Limit2Units, Is.EqualTo(0460800));
            Assert.That(selectedS.Limit3Units, Is.EqualTo(0557000));
            Assert.That(selectedS.Limit4Units, Is.EqualTo(0692200));

            selectedS = LoanLimit.GetLoanLimitFor(E_LoanLimitType.FHA, 13247, year2019);
            Assert.That(selectedS.FipsCode, Is.EqualTo(13247));
            Assert.That(selectedS.Limit1Units, Is.EqualTo(0379500));
            Assert.That(selectedS.Limit2Units, Is.EqualTo(0485800));
            Assert.That(selectedS.Limit3Units, Is.EqualTo(0587250));
            Assert.That(selectedS.Limit4Units, Is.EqualTo(0729800));

            DateTime now = DateTime.Now;
            Assert.That(now.Year, Is.EqualTo(latestYear.Year), "Test must be updated to handle limits for " + now.Year + ".");
        }

        [Test]
        public void TestRetrievingLoanLimitsGSE()
        {
            DateTime year2016 = new DateTime(2016, 1, 1);
            DateTime year2017 = new DateTime(2017, 1, 1);
            DateTime year2018 = new DateTime(2018, 1, 1);
            DateTime year2019 = new DateTime(2019, 1, 1);
            DateTime latestYear = year2019;

            // Test county: Henry County, Kentucky: FIPS 21103
            LoanLimit henryCountyKentucky2016 = LoanLimit.GetLoanLimitFor(E_LoanLimitType.GSE, 21103, year2016);
            Assert.That(henryCountyKentucky2016, Is.Not.Null);
            Assert.That(henryCountyKentucky2016.FipsCode, Is.EqualTo(21103));
            Assert.That(henryCountyKentucky2016.Limit1Units, Is.EqualTo(0417000));
            Assert.That(henryCountyKentucky2016.Limit2Units, Is.EqualTo(0533850));
            Assert.That(henryCountyKentucky2016.Limit3Units, Is.EqualTo(0645300));
            Assert.That(henryCountyKentucky2016.Limit4Units, Is.EqualTo(0801950));

            LoanLimit henryCountyKentucky2017 = LoanLimit.GetLoanLimitFor(E_LoanLimitType.GSE, 21103, year2017);
            Assert.That(henryCountyKentucky2017, Is.Not.Null);
            Assert.That(henryCountyKentucky2017.FipsCode, Is.EqualTo(21103));
            Assert.That(henryCountyKentucky2017.Limit1Units, Is.EqualTo(0424100));
            Assert.That(henryCountyKentucky2017.Limit2Units, Is.EqualTo(0543000));
            Assert.That(henryCountyKentucky2017.Limit3Units, Is.EqualTo(0656350));
            Assert.That(henryCountyKentucky2017.Limit4Units, Is.EqualTo(0815650));

            LoanLimit henryCountyKentucky2018 = LoanLimit.GetLoanLimitFor(E_LoanLimitType.GSE, 21103, year2018);
            Assert.That(henryCountyKentucky2018, Is.Not.Null);
            Assert.That(henryCountyKentucky2018.FipsCode, Is.EqualTo(21103));
            Assert.That(henryCountyKentucky2018.Limit1Units, Is.EqualTo(0453100));
            Assert.That(henryCountyKentucky2018.Limit2Units, Is.EqualTo(0580150));
            Assert.That(henryCountyKentucky2018.Limit3Units, Is.EqualTo(0701250));
            Assert.That(henryCountyKentucky2018.Limit4Units, Is.EqualTo(0871450));

            LoanLimit henryCountyKentucky2019 = LoanLimit.GetLoanLimitFor(E_LoanLimitType.GSE, 21103, year2019);
            Assert.That(henryCountyKentucky2019, Is.Not.Null);
            Assert.That(henryCountyKentucky2019.FipsCode, Is.EqualTo(21103));
            Assert.That(henryCountyKentucky2019.Limit1Units, Is.EqualTo(0484350));
            Assert.That(henryCountyKentucky2019.Limit2Units, Is.EqualTo(0620200));
            Assert.That(henryCountyKentucky2019.Limit3Units, Is.EqualTo(0749650));
            Assert.That(henryCountyKentucky2019.Limit4Units, Is.EqualTo(0931600));

            LoanLimit standard = LoanLimit.GetLoanLimitFor(E_LoanLimitType.GSE, 9999, year2018);
            Assert.That(standard, Is.Not.Null);
            Assert.That(standard.FipsCode, Is.EqualTo(0));
            Assert.That(standard.Limit1Units, Is.EqualTo(0417000));
            Assert.That(standard.Limit2Units, Is.EqualTo(0533850));
            Assert.That(standard.Limit3Units, Is.EqualTo(0645300));
            Assert.That(standard.Limit4Units, Is.EqualTo(0801950));

            standard = LoanLimit.GetLoanLimitFor(E_LoanLimitType.GSE, 9999, year2019);
            Assert.That(standard, Is.Not.Null);
            Assert.That(standard.FipsCode, Is.EqualTo(0));
            Assert.That(standard.Limit1Units, Is.EqualTo(0417000));
            Assert.That(standard.Limit2Units, Is.EqualTo(0533850));
            Assert.That(standard.Limit3Units, Is.EqualTo(0645300));
            Assert.That(standard.Limit4Units, Is.EqualTo(0801950));

            standard = LoanLimit.GetLoanLimitFor(E_LoanLimitType.GSE, 0, year2016);
            Assert.That(standard, Is.Not.Null);
            Assert.That(standard.FipsCode, Is.EqualTo(0));
            Assert.That(standard.Limit1Units, Is.EqualTo(0417000));
            Assert.That(standard.Limit2Units, Is.EqualTo(0533850));
            Assert.That(standard.Limit3Units, Is.EqualTo(0645300));
            Assert.That(standard.Limit4Units, Is.EqualTo(0801950));

            standard = LoanLimit.GetLoanLimitFor(E_LoanLimitType.GSE, 0, year2017);
            Assert.That(standard, Is.Not.Null);
            Assert.That(standard.FipsCode, Is.EqualTo(0));
            Assert.That(standard.Limit1Units, Is.EqualTo(0424100));
            Assert.That(standard.Limit2Units, Is.EqualTo(0543000));
            Assert.That(standard.Limit3Units, Is.EqualTo(0656350));
            Assert.That(standard.Limit4Units, Is.EqualTo(0815650));

            standard = LoanLimit.GetLoanLimitFor(E_LoanLimitType.GSE, 0, year2018);
            Assert.That(standard, Is.Not.Null);
            Assert.That(standard.FipsCode, Is.EqualTo(0));
            Assert.That(standard.Limit1Units, Is.EqualTo(0453100));
            Assert.That(standard.Limit2Units, Is.EqualTo(0580150));
            Assert.That(standard.Limit3Units, Is.EqualTo(0701250));
            Assert.That(standard.Limit4Units, Is.EqualTo(0871450));

            standard = LoanLimit.GetLoanLimitFor(E_LoanLimitType.GSE, 0, year2019);
            Assert.That(standard, Is.Not.Null);
            Assert.That(standard.FipsCode, Is.EqualTo(0));
            Assert.That(standard.Limit1Units, Is.EqualTo(0484350));
            Assert.That(standard.Limit2Units, Is.EqualTo(0620200));
            Assert.That(standard.Limit3Units, Is.EqualTo(0749650));
            Assert.That(standard.Limit4Units, Is.EqualTo(0931600));

            DateTime now = DateTime.Now;
            Assert.That(now.Year, Is.EqualTo(latestYear.Year), "Test must be updated to handle limits for " + now.Year + ".");
        }
    }
}
