﻿// Author: Peter Anargirou
// 12/23/08

using NUnit.Framework;
using DataAccess;
using System;
using LendersOffice.Security;
using LendOSolnTest.Common;
using LendersOffice.Audit;
using LendersOffice.Reminders;
using LendersOffice.Common;
using LendersOfficeApp.los.admin;

namespace LendOSolnTest.TaskConditions
{
    [TestFixture]
    public class TaskConditionsTest
    {
        #region member variables
        private Guid m_loanId = Guid.Empty;

        private int x_conditionNumber = 0;
        private int m_conditionNumber
        {
            get
            {
                return x_conditionNumber;
            }
        }

        private int m_previousConditionNumber
        {
            get
            {
                return m_conditionNumber - 1;
            }
        }

        private AbstractUserPrincipal x_principal = null;
        private AbstractUserPrincipal m_currentPrincipal
        {
            get
            {
                if (null == x_principal)
                {
                    x_principal = LoginTools.LoginAs(TestAccountType.LoTest001);
                }

                return x_principal;
            }
        }

        private BrokerUserPrincipal m_brokerUser
        {
            get
            {
                BrokerUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;
                if (null == principal)
                    throw new ApplicationException("The CurrentPrincipal is null");

                return principal;
            }
        }

        #region Constants
        private const string sTestData1 = "my description";
        private const string sTestData2 = "test";
        private const string sTestData3 = "my notes";
        private const string sTestData4 = "Hello, world!";
        private const string sTestData5 = "foo";
        private const string sTestData6 = "Good bye, world!";
        private const string sTestData7 = "bar";
        #endregion

        #endregion

        private void IncrementConditionNumber()
        {
            x_conditionNumber++;
        }

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            CLoanFileCreator fileCreator
                = CLoanFileCreator.GetCreator(m_currentPrincipal, E_LoanCreationSource.UserCreateFromBlank);
            m_loanId = fileCreator.CreateBlankLoanFile();
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            Tools.DeclareLoanFileInvalid(m_currentPrincipal, m_loanId, false /* sendNotifications*/, false /* generateLinkLoanMsgEvent */);
        }

        private void AssertConditionsAreEqual(CLoanConditionObsolete expected, CLoanConditionObsolete actual)
        {
            AssertConditionsAreEqual(expected, actual, "No note");
        }

        private void AssertConditionsAreEqual(CLoanConditionObsolete expected, CLoanConditionObsolete actual, string note)
        {
            Assert.AreEqual(expected.LoanId, actual.LoanId, note + " - LoanId");
            Assert.AreEqual(DateTime.Now.Date, actual.CreatedD.Date, note + " - CreatedD");
            Assert.AreEqual(expected.CondCategoryDesc, actual.CondCategoryDesc, note + " - CondCategoryDesc");
            Assert.AreEqual(expected.CondDesc, actual.CondDesc, note + " - CondDesc");
            Assert.AreEqual(expected.CondIsHidden, actual.CondIsHidden, note + " - CondIsHidden");
            Assert.AreEqual(expected.CondIsValid, actual.CondIsValid, note + " - CondIsValid");
            Assert.AreEqual(expected.CondNotes, actual.CondNotes, note + " - CondNotes");
            Assert.AreEqual(expected.CondStatus, actual.CondStatus, note + " - CondStatus");
            Assert.AreEqual(expected.CondCompletedByUserNm, actual.CondCompletedByUserNm, note + " - CondCompletedByUserNm");

            if (expected.CondStatusDate == DateTime.MinValue)
                Assert.AreEqual(DateTime.Today.Date, actual.CondStatusDate.Date, note + " - CondStatusDate");
            else
                Assert.AreEqual(expected.CondStatusDate, actual.CondStatusDate, note + " - CondStatusDate");
        }

        [Test]
        public void AddConditionTest()
        {
            LoanConditionSetObsolete conditionSet = RetrieveConditionsList(false /* getDeleted */);
            int initialNumberOfConditions = conditionSet.Count;

            // Create a condition and populate its fields
            CLoanConditionObsolete condition = AddCondition();
            conditionSet = RetrieveConditionsList(false /* getDeleted */);

            // Retrieve list of conditions to make sure new one was added
            Assert.AreEqual(initialNumberOfConditions + 1, conditionSet.Count, "There should be one condition more than the initial amount.");

            // Add second one

            CLoanConditionObsolete condition2 = new CLoanConditionObsolete(m_currentPrincipal.BrokerId);

            condition2.CondCategoryDesc = sTestData1;
            condition2.CondDesc = sTestData2 + m_conditionNumber;
            condition2.CondNotes = sTestData3;
            condition2.CondIsHidden = true;
            condition2.CondIsValid = true;
            condition2.LoanId = m_loanId;
            condition2.CondStatus = LendersOffice.Reminders.E_CondStatus.Done;
            condition2.CondCompletedByUserNm = m_brokerUser.DisplayName;
            condition2.CondStatusDate = DateTime.Now.Date.AddDays(2.0);

            IncrementConditionNumber();

            using (CStoredProcedureExec spExec = new CStoredProcedureExec(this.m_currentPrincipal.BrokerId))
            {
                spExec.BeginTransactionForWrite();
                SaveCondition(condition2, spExec);
                spExec.CommitTransaction();
            }

            // Compare all values in new conditions
            conditionSet = RetrieveConditionsList(false /* getDeleted */);
            Assert.AreEqual(initialNumberOfConditions + 2, conditionSet.Count, "There should be two conditions.");
            CLoanConditionObsolete retrievedCondition1 = conditionSet.GetMatch(condition.CondCategoryDesc, condition.CondDesc);
            CLoanConditionObsolete retrievedCondition2 = conditionSet.GetMatch(condition2.CondCategoryDesc, condition2.CondDesc);

            AssertConditionsAreEqual(condition, retrievedCondition1, "First condition");
            AssertConditionsAreEqual(condition2, retrievedCondition2, "Second condition");
        }

        [Test]
        public void EditConditionTest()
        {
            CLoanConditionObsolete condition = AddCondition();

            using (CStoredProcedureExec spExec = new CStoredProcedureExec(this.m_currentPrincipal.BrokerId))
            {
                spExec.BeginTransactionForWrite();

                // Edit every value of the condition
                condition.CondCategoryDesc = sTestData6;
                condition.CondDesc = sTestData2 + m_conditionNumber;
                IncrementConditionNumber();
                condition.CondIsHidden = true;
                condition.CondNotes = sTestData7;
                condition.CondOrderedPosition = 1;
                condition.CondStatus = E_CondStatus.Done;
                condition.CondCompletedByUserNm = m_brokerUser.DisplayName;
                condition.CondStatusDate = DateTime.Today.AddDays(1);

                SaveCondition(condition, spExec);

                spExec.CommitTransaction();
            }

            LoanConditionSetObsolete conditionSet = RetrieveConditionsList(false /* getDeleted */);
            CLoanConditionObsolete retrievedCondition = conditionSet.GetMatch(sTestData6, sTestData2 + m_previousConditionNumber);

            AssertConditionsAreEqual(condition, retrievedCondition);
        }

        [Test]
        public void DeleteConditionTest()
        {
            #region Initial

            // Gets the number of valid conditions initially
            LoanConditionSetObsolete conditionSet = RetrieveConditionsList(false /* getDeleted */);
            int numberOfNondeletedConditionsInitially = conditionSet.Count;

            // Gets the number of deleted conditions initially
            conditionSet = RetrieveConditionsList(true /* getDeleted */);
            int numberOfDeletedConditionsInitially = conditionSet.Count;

            #endregion

            #region Add Condition

            CLoanConditionObsolete condition = AddCondition();
            string conditionDescription = condition.CondDesc;

            // Gets the number of valid conditions after adding
            conditionSet = RetrieveConditionsList(false /*getDeleted */);
            int numberOfNondeletedConditionsAfterAdding = conditionSet.Count;

            Assert.AreEqual(numberOfNondeletedConditionsInitially + 1, numberOfNondeletedConditionsAfterAdding, "Number of non-deleted conditions after adding");

            // Gets the number of deleted conditions after adding
            conditionSet = RetrieveConditionsList(true /* getDeleted */);
            int numberOfDeletedConditionsAfterAdding = conditionSet.Count;

            Assert.AreEqual(numberOfDeletedConditionsInitially, numberOfDeletedConditionsAfterAdding, "Number of deleted conditions after adding");

            #endregion

            #region Delete Condition
            Guid conditionId = condition.CondId;

            condition = new CLoanConditionObsolete(m_brokerUser.BrokerId, conditionId);

            DeleteCondition(condition);

            // Gets the number of valid conditions after deleting
            conditionSet = RetrieveConditionsList(false /* getDeleted */);
            int numberOfNondeletedConditionsAfterDeleting = conditionSet.Count;

            Assert.AreEqual(numberOfNondeletedConditionsAfterAdding - 1, numberOfNondeletedConditionsAfterDeleting, "Number of non-deleted conditions after deleting");

            // Gets the number of deleted conditions after deleting
            conditionSet = RetrieveConditionsList(true /* getDeleted */);
            int numberOfDeletedConditionsAfterDeleting = conditionSet.Count;

            Assert.AreEqual(numberOfDeletedConditionsAfterAdding + 1, numberOfDeletedConditionsAfterDeleting, "Number of deletedconditions after deleting");

            #endregion

            #region Make sure condition is invalid

            condition = conditionSet.GetMatch(sTestData4, conditionDescription);

            Assert.AreEqual(false, condition.CondIsValid, "CondIsValid");

            #endregion
        }

        [Test]
        public void RestoreDeletedConditionTest()
        {
            LoanConditionSetObsolete conditionSet;
            CLoanConditionObsolete condition = AddCondition();
            string conditionDescription = condition.CondDesc;

            Guid conditionId = condition.CondId;
            condition = new CLoanConditionObsolete(m_brokerUser.BrokerId, conditionId);

            DeleteCondition(condition);

            // Gets number of invalid conditions after deleting
            conditionSet = RetrieveConditionsList(true /* getDeleted */);
            int numberOfDeletedConditionsAfterDeleting = conditionSet.Count;

            // Retrieves the condition that was just deleted
            CLoanConditionObsolete conditionThatWasDeleted = conditionSet.GetMatch(sTestData4, conditionDescription);
            Assert.AreEqual(false, conditionThatWasDeleted.CondIsValid, "Should be deleted");

            // Gets number of valid conditions after deleting
            conditionSet = RetrieveConditionsList(false /* getDeleted */);
            int numberOfNondeletedConditionsAfterDeleting = conditionSet.Count;

            RestoreDeletedCondition(condition);

            // Gets number of invalid conditions after restoring
            conditionSet = RetrieveConditionsList(true /* getDeleted */);
            int numberOfDeletedConditionsAfterRestoring = conditionSet.Count;

            Assert.AreEqual(numberOfDeletedConditionsAfterDeleting - 1, numberOfDeletedConditionsAfterRestoring, "Number of deleted conditions after restoring");

            // Gets number of valid conditions after restoring
            conditionSet = RetrieveConditionsList(false /* getDeleted */);
            int numberOfNondeletedConditionsAfterRestoring = conditionSet.Count;

            Assert.AreEqual(numberOfNondeletedConditionsAfterDeleting + 1, numberOfNondeletedConditionsAfterRestoring, "Number of non-deleted conditions after restoring");

            // Retrieves the condition that was restored
            CLoanConditionObsolete conditionThatWasRestored = conditionSet.GetMatch(sTestData4, conditionDescription);
            Assert.AreEqual(true, conditionThatWasRestored.CondIsValid, "Should be valid");
        }

        private void RestoreDeletedCondition(CLoanConditionObsolete condition)
        {
            using (CStoredProcedureExec spExec = new CStoredProcedureExec(m_currentPrincipal.BrokerId))
            {
                spExec.BeginTransactionForWrite();

                condition.CondIsValid = true;

                SaveCondition(condition, spExec);

                spExec.CommitTransaction();
            }
        }

        private void DeleteCondition(CLoanConditionObsolete condition)
        {
            using (CStoredProcedureExec spExec = new CStoredProcedureExec(m_currentPrincipal.BrokerId))
            {
                spExec.BeginTransactionForWrite();

                condition.Retrieve(spExec);

                if (!condition.IsRetrieved)
                {
                    spExec.RollbackTransaction();
                    throw new ApplicationException("Failed to retrieve task condition");
                }

                condition.CondIsValid = false;

                SaveCondition(condition, spExec);

                spExec.CommitTransaction();
            }
        }

        private void SaveCondition(CLoanConditionObsolete condition, CStoredProcedureExec spExec)
        {
            try
            {
                condition.Save(spExec);
            }
            catch (Exception e)
            {
                spExec.RollbackTransaction();
                throw e;
            }
        }

        private CLoanConditionObsolete AddCondition()
        {
            CLoanConditionObsolete condition = new CLoanConditionObsolete(m_currentPrincipal.BrokerId);

            condition.UseNotes = true;

            condition.CondCategoryDesc = sTestData4;
            condition.CondDesc = sTestData2 + m_conditionNumber;
            condition.CondNotes = sTestData5;
            condition.CondIsHidden = false;
            condition.CondIsValid = true;
            condition.LoanId = m_loanId;
            condition.CondStatus = LendersOffice.Reminders.E_CondStatus.Active;

            IncrementConditionNumber();

            using (CStoredProcedureExec spExec = new CStoredProcedureExec(m_currentPrincipal.BrokerId))
            {
                spExec.BeginTransactionForWrite();
                SaveCondition(condition, spExec);
                spExec.CommitTransaction();
            }

            return condition;
        }

        private LoanConditionSetObsolete RetrieveConditionsList(bool getDeleted)
        {
            LoanConditionSetObsolete conditionSet = new LoanConditionSetObsolete();
            conditionSet.Retrieve(m_loanId, true, true, getDeleted);

            return conditionSet;
        }

        /// <summary>
        /// Logs the data regarding a condition to PaulBunyan using a default note of "None"
        /// </summary>
        /// <param name="condition">Condition to log</param>
        private void LogDebugInformation(CLoanConditionObsolete condition)
        {
            LogDebugInformation(condition, "None");
        }

        /// <summary>
        /// Logs the data regarding a condition to PaulBunyan using a custom note
        /// </summary>
        /// <param name="condition">The condition to log</param>
        /// <param name="note">Note to log with condition</param>
        private void LogDebugInformation(CLoanConditionObsolete condition, string note)
        {
            Tools.LogInfo("Note:					" + note);
            Tools.LogInfo("CondCategoryDesc:		" + condition.CondCategoryDesc);
            Tools.LogInfo("CondDesc:				" + condition.CondDesc);
            Tools.LogInfo("CondId:				" + condition.CondId);
            Tools.LogInfo("CondIsHidden:			" + condition.CondIsHidden);
            Tools.LogInfo("CondIsValid:			" + condition.CondIsValid);
            Tools.LogInfo("CondNotes:			" + condition.CondNotes);
            Tools.LogInfo("CondOrderedPosition:	" + condition.CondOrderedPosition);
            Tools.LogInfo("CondStatus:			" + condition.CondStatus);
            Tools.LogInfo("CondStatusDate:		" + condition.CondStatusDate);
            Tools.LogInfo("CondCompletedByUserNm:" + condition.CondCompletedByUserNm);
            Tools.LogInfo("CreatedD:				" + condition.CreatedD);
            Tools.LogInfo("IsLoanStillValid:		" + condition.IsLoanStillValid);
            Tools.LogInfo("IsNew:				" + condition.IsNew);
            Tools.LogInfo("IsRetrieved:			" + condition.IsRetrieved);
            Tools.LogInfo("LoanId:				" + condition.LoanId);

        }
    }
}