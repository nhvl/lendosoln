﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using LendOSolnTest.Common;
using LendersOffice.Security;
using DataAccess;
using LendersOffice.Conversions.LoanProspector;
using System.IO;

namespace LendOSolnTest.LP
{
    [TestFixture]
    public class LPImportResponseTest
    {
        private Guid m_loanId = Guid.Empty;

        private Guid CreateLoan()
        {
            AbstractUserPrincipal brokerUser = BrokerUserPrincipal.CurrentPrincipal;
            CLoanFileCreator creator = CLoanFileCreator.GetCreator(brokerUser, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
            Guid sLId = creator.CreateBlankLoanFile();
            return sLId;
        }

        private CPageData CreateDataObject(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(LPImportResponseTest));
        }
        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            LoginTools.LoginAs(TestAccountType.LoTest001);

        }
        [Test]
        public void ImportWithPublicRecordTest()
        {
            Guid? sLId = null;
            try
            {
                sLId = CreateLoan();
                string xml = File.ReadAllText(TestDataRootFolder.Location + "/LP/LP_ImportResponse_MergeCredit_WithPublicRecord.txt");
                var parsedServiceOrderResponse = LoanProspectorResponseParser.Parse(xml);
                var loanIdentifierData = parsedServiceOrderResponse.ValueOrDefault?.LoanApplication?.ExtensionLoanApplication?.LoanIdentifierData;
                if (loanIdentifierData != null)
                {
                    loanIdentifierData.GloballyUniqueIdentifier = sLId.ToString(); // the importer reads the loan id from the object
                }

                LoanProspectorImporter importer = LoanProspectorImporter.CreateImporterForNonseamless(parsedServiceOrderResponse, isImport1003: true, isImportFeedback: true, isImportCreditReport: true, loanId: sLId.Value);
                importer.Import();

                CPageData dataLoan = CreateDataObject(sLId.Value);
                dataLoan.InitLoad();

                CAppData dataApp = dataLoan.GetAppData(0);
                Assert.AreEqual(4, dataApp.aPublicRecordCollection.CountRegular, "Public Record Count");
                Assert.AreEqual("600", dataApp.aBExperianScore_rep, "aBExperianScore");
                Assert.AreEqual("545", dataApp.aBTransUnionScore_rep, "aBTransUnionScore");
                Assert.AreEqual("597", dataApp.aBEquifaxScore_rep, "aBEquifaxScore");
                Assert.AreEqual("584", dataApp.aCExperianScore_rep, "aCExperianScore");
                Assert.AreEqual("553", dataApp.aCTransUnionScore_rep, "aCTransUnionScore");
                Assert.AreEqual("560", dataApp.aCEquifaxScore_rep, "aCEquifaxScore");
            }
            finally
            {
                if (sLId.HasValue)
                {
                    Tools.DeclareLoanFileInvalid(BrokerUserPrincipal.CurrentPrincipal, sLId.Value, false, false);
                }
            }

        }
    }
}
