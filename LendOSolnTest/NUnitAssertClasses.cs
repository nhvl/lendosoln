﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace LendOSolnTest
{
    public static class MLAssert
    {
        public static void Throws<T>(Action action) where T : Exception
        {
            Assert.IsTrue(action.Throws<T>());
        }
        public static void ThrowsSpecifically<T>(Action action) where T : Exception
        {
            Assert.IsTrue(action.ThrowsSpecifically<T>());
        }
        public static void NotThrows<T>(Action action) where T : Exception
        {
            Assert.IsFalse(action.Throws<T>());
        }
        public static void NotThrowsSpecifically<T>(Action action) where T : Exception
        {
            Assert.IsFalse(action.ThrowsSpecifically<T>());
        }
    }
    [TestFixture]
    public class MLAssertTests
    {
        private void Throw<T>(T exc) where T : Exception
        {
            throw exc;
        }
        [Test]
        public void ThrowsTest()
        {
            MLAssert.Throws<ArgumentException>(() => Throw(new ArgumentException()));
            MLAssert.ThrowsSpecifically<ArgumentException>(() => Throw(new ArgumentException()));

            MLAssert.NotThrows<ArgumentException>(()=>Throw(new Exception()));
            MLAssert.NotThrowsSpecifically<ArgumentException>(() => Throw(new Exception()));

            MLAssert.Throws<Exception>(() => Throw(new ArgumentException()));
            MLAssert.NotThrowsSpecifically<Exception>(() => Throw(new ArgumentException()));

            Action a = () => Throw(new ArgumentException());
            Assert.IsTrue(a.Throws<ArgumentException>());
            Assert.IsTrue(a.Throws<Exception>());
            Assert.IsFalse(a.ThrowsSpecifically<Exception>());

            a = () => Throw(new Exception());
            Assert.IsFalse(a.Throws<ArgumentException>());
            Assert.IsFalse(a.ThrowsSpecifically<ArgumentException>());
        }
       
        
    }
    public static partial class ExtensionMethods
    {
        public static bool Throws<T>(this Action action) where T : Exception
        {
            try
            {
                action.Invoke();
            }
            catch (T)
            {
                return true;
            }
            catch (Exception)
            {
                return false;
            }
            return false;
        }
    }
    public static partial class ExtensionMethods
    {
        public static bool ThrowsSpecifically<T>(this Action action) where T : Exception
        {
            try
            {
                action.Invoke();
            }
            catch (T e)
            {
                return e.GetType().IsAssignableFrom(typeof(T));
            }
            catch (Exception)
            {
                return false;
            }
            return false;
        }
    
    }
}
