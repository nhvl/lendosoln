﻿using System;
using System.Collections;
using NUnit.Framework;
using LendersOfficeApp.los.RatePrice;
using System.Xml;

namespace LendOSolnTest
{
    [TestFixture]
    public class CSymbolTableTest
    {

        [Test]
        public void NewKeywordsAreCaseInsensitiveTest()
        {
            IgnoreCase4KeywordTest(new CSymbolTable(null));
        }

        [Test]
        public void ReadNewKeywordsFromDBTest()
        {
            LpeNewKeywordUtility.ReadNewKeywordsFromDB();
        }

        [Test]
        public void InvalidKeywordTest()
        {
            CSymbolTable symbolTable = new CSymbolTable(null);
            string condStr = "NeverHavingThisKeyword > 1";

            ExpectExceptionWhenValidate(symbolTable, condStr, CEvaluationException.ErrorTypeEnum.InvalidToken);
            //ConditionValidate( condStr, sym );
        }

        [Test]
        public void RecognizeNewKeywords()
        {
            Hashtable newKeywords = CreateMockSymbols("ThienField12", "ThienFunctionInt_FunctionInt", "ThienFunctionString_FunctionString");
            Assert.AreEqual(3, newKeywords.Count);
            RecognizeNewKeywords(new CSymbolTable(null, newKeywords), newKeywords);
        }

        [Test]
        public void RecognizeNewKeywordsInDBTest()
        {
            string errMsg;
            Hashtable newKeywords = LpeNewKeywordUtility.ReadNewKeywordsFromDB(out errMsg);
            RecognizeNewKeywords(new CSymbolTable(null), newKeywords);
        }

        void RecognizeNewKeywords(CSymbolTable symbolTableContainsNewKeyword, Hashtable newKeywords)
        {
            CSymbolTable symbolTable = CSymbolTable.CreateSymbolTableWithoutNewKeywords();
            string[] conds = CreateConditionsFromMockSymbolEntries(newKeywords);

            foreach (string condStr in conds)
                ExpectExceptionWhenValidate(symbolTable, condStr, CEvaluationException.ErrorTypeEnum.InvalidToken);

            foreach (string condStr in conds)
                ConditionValidate(symbolTableContainsNewKeyword, condStr);
        }


        [Test]
        public void ReevaluateWithRemovedKeywordTest()
        {
            // We successly evaluate CCondition, RtCondition objects those contain new keywords.
            // After removing new keywords, we re-evaluate those objects with expection : CEvaluationException.ErrorTypeEnum.InvalidToken

            Hashtable newKeywords = CreateMockSymbols("ThienField12", "ThienFunctionInt_FunctionInt", "ThienFunctionString_FunctionString");
            CSymbolTable symbolTable = new CSymbolTable(null, newKeywords);
            CSymbolTable tableWithoutNewKeywords = CSymbolTable.CreateSymbolTableWithoutNewKeywords();

            string[] conds = CreateConditionsFromMockSymbolEntries(newKeywords);

            CCondition condition;
            RtCondition rtCondition;

            foreach (string condStr in conds)
            {
                ConditionValidate(symbolTable, condStr, out condition, out rtCondition);
                ExpectExceptionWhenValidate(tableWithoutNewKeywords, condition, CEvaluationException.ErrorTypeEnum.InvalidToken);
                ExpectExceptionWhenValidate(tableWithoutNewKeywords, rtCondition, CEvaluationException.ErrorTypeEnum.InvalidToken);
            }

        }

        [Test]
        public void ReevaluateWithNewKeywordTest()
        {
            // Assume, RtCondition obj contains new keywords but they are not release.
            // step 1. expect CEvaluationException.ErrorTypeEnum.InvalidToken when evaluate it 
            // step 2. new keywords are available => successly evaluate

            Hashtable newKeywords = CreateMockSymbols("ThienField12", "ThienFunctionInt_FunctionInt", "ThienFunctionString_FunctionString");
            CSymbolTable symbolTable = new CSymbolTable(null, newKeywords);
            CSymbolTable tableWithoutNewKeywords = CSymbolTable.CreateSymbolTableWithoutNewKeywords();

            string[] conds = CreateConditionsFromMockSymbolEntries(newKeywords);

            foreach (string condStr in conds)
            {
                RtCondition rtCondition = new RtCondition(condStr);
                ExpectExceptionWhenValidate(tableWithoutNewKeywords, rtCondition, CEvaluationException.ErrorTypeEnum.InvalidToken);
                rtCondition.Evaluate(new XmlDocument(), symbolTable, new EvalStack(2000), new System.Text.StringBuilder(), false);
            }

        }

        #region helper methods
        private static bool ConditionValidate(CSymbolTable sym, string condStr)
        {
            CCondition condition;
            RtCondition rtCondition;
            return ConditionValidate(sym, condStr, out condition, out rtCondition);
        }
        private static bool ConditionValidate(CSymbolTable sym, string condStr, out CCondition condition)
        {
            RtCondition rtCondition;
            return ConditionValidate(sym, condStr, out condition, out rtCondition);
        }

        private static bool ConditionValidate(CSymbolTable sym, string condStr, out CCondition condition, out RtCondition rtCondition)
        {
            condition = CCondition.CreateTestCondition(condStr);

            XmlDocument xmlDocEval = new XmlDocument();
            bool result = condition.Evaluate(xmlDocEval, sym);

            rtCondition = new RtCondition(condStr);
            Assert.AreEqual(result, rtCondition.Evaluate(xmlDocEval, sym, new EvalStack(2000), new System.Text.StringBuilder(), false));
            return result;
        }

        static void ExpectExceptionWhenValidate(CSymbolTable sym, string condStr, CEvaluationException.ErrorTypeEnum expectErrType)
        {
            try
            {
                ConditionValidate(sym, condStr);
            }
            catch (CConditionException exc)
            {
                Assert.AreEqual(expectErrType, exc.ErrorType);
                return;
            }
            catch (CEvaluationException exc)
            {
                Assert.AreEqual(expectErrType, exc.ErrorType, condStr);
                return;
            }

            Assert.Fail("The CConditionException ( " + expectErrType + " ) was expected for expression : " + condStr);
        }

        static void ExpectExceptionWhenValidate(CSymbolTable sym, CCondition cond, CEvaluationException.ErrorTypeEnum expectErrType)
        {
            string exprStr = cond.UserExpression;
            try
            {
                cond.Evaluate(new XmlDocument(), sym);
            }
            catch (CConditionException exc)
            {
                Assert.AreEqual(expectErrType, exc.ErrorType, exprStr);
                return;
            }
            catch (CEvaluationException exc)
            {
                Assert.AreEqual(expectErrType, exc.ErrorType, exprStr);
                return;
            }

            Assert.Fail("The CConditionException ( " + expectErrType + " ) was expected for expression : " + exprStr);
        }

        static void ExpectExceptionWhenValidate(CSymbolTable sym, RtCondition cond, CEvaluationException.ErrorTypeEnum expectErrType)
        {
            string expr = cond.RecoverExpr();
            try
            {
                cond.Evaluate(new XmlDocument(), sym, new EvalStack(2000), new System.Text.StringBuilder(), false);
            }
            catch (CConditionException exc)
            {
                Assert.AreEqual(expectErrType, exc.ErrorType, expr);
                return;
            }
            catch (CEvaluationException exc)
            {
                Assert.AreEqual(expectErrType, exc.ErrorType, expr);
                return;
            }

            Assert.Fail("The CConditionException ( " + expectErrType + " ) was expected for expression : " + expr);
        }

        public void IgnoreCase4KeywordTest(CSymbolTable table)
        {

        }

        public Hashtable CreateMockSymbols(params string[] names)
        {
            Hashtable table = new Hashtable();
            foreach (string entry in names)
            {
                string name = entry;

                const string FunctionString = "_FunctionString";
                const string FunctionInt = "_FunctionInt";

                if (name.EndsWith(FunctionString) == true)
                {
                    name = name.Substring(0, name.Length - FunctionString.Length);
                    table[name] = LpeNewKeywordUtility.CreateSymbolEntry(name, "1", LpeNewKeywordUtility.FunctionStringType);
                }
                else if (name.EndsWith(FunctionInt) == true)
                {
                    name = name.Substring(0, name.Length - FunctionInt.Length);
                    table[name] = LpeNewKeywordUtility.CreateSymbolEntry(name, "10", LpeNewKeywordUtility.FunctionIntType);
                }
                else
                    table[name] = LpeNewKeywordUtility.CreateSymbolEntry(name, "100", LpeNewKeywordUtility.FieldType);
            }

            return table;

        }

        public void EvaluateAndGetInvalidToken(CSymbolTable symbolTable, params string[] conditions)
        {
            foreach (string condStr in conditions)
                ExpectExceptionWhenValidate(symbolTable, condStr, CEvaluationException.ErrorTypeEnum.InvalidToken);

        }

        static string[] CreateConditionsFromMockSymbolEntries(Hashtable newLpeKeywords)
        {
            ArrayList arr = new ArrayList();
            foreach (CSymbolEntry entry in newLpeKeywords.Values)
            {
                string condStr = entry.InputKeyword;
                int idx = condStr.IndexOf(":");
                if (idx > 0)
                {
                    if (entry is CSymbolFunction)
                        condStr = condStr.Substring(0, idx) + ":0 > 1";
                    else if (entry is CSymbolFunctionStrParams)
                        condStr += "=10 > 1";
                }
                else
                    condStr = entry.InputKeyword + " > 0 ";
                arr.Add(condStr);
            }

            return (string[])arr.ToArray(typeof(string));
        }

        #endregion
    }
}
