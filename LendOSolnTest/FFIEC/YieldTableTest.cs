﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using LendersOffice.FFIEC;
using DataAccess;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using LendOSolnTest.Common;

namespace LendOSolnTest.FFIEC
{
    [TestFixture]
    [Category(CategoryConstants.NewBuildServerIgnore)]
    public class YieldTableTest
    {
        [Test]
        public void TestLookupWithInvalidValue()
        {
            var testCaseList = new[] {
                new { sFinMethT = E_sFinMethT.Graduated, sRLckD = string.Empty, sDue=string.Empty, sRAdj1stCapMon = string.Empty},
                new { sFinMethT = E_sFinMethT.Fixed, sRLckD = string.Empty, sDue = string.Empty, sRAdj1stCapMon = string.Empty},
                new { sFinMethT = E_sFinMethT.Fixed, sRLckD = "09/01/2013", sDue = string.Empty, sRAdj1stCapMon = string.Empty},
                new { sFinMethT = E_sFinMethT.Fixed, sRLckD = "09/01/2013", sDue = string.Empty, sRAdj1stCapMon = "6"},
                new { sFinMethT = E_sFinMethT.ARM, sRLckD = "09/01/2013", sDue = string.Empty, sRAdj1stCapMon = string.Empty},
                new { sFinMethT = E_sFinMethT.Fixed, sRLckD = "09/01/2013", sDue = "612", sRAdj1stCapMon = string.Empty}, // 51 yrs
                new { sFinMethT = E_sFinMethT.Fixed, sRLckD = "09/01/2013", sDue = string.Empty, sRAdj1stCapMon = "6"},
                new { sFinMethT = E_sFinMethT.Fixed, sRLckD = DateTime.Today.AddDays(14).ToString("MM/dd/yyyy"), sDue = "360", sRAdj1stCapMon = string.Empty},

                               };


            foreach (var testCase in testCaseList)
            {
                Assert.AreEqual(decimal.MinValue, YieldTable.LookUp(testCase.sFinMethT, testCase.sRLckD, testCase.sDue, testCase.sRAdj1stCapMon, CDateTime.Create(testCase.sRLckD, null)), testCase.ToString());
            }
        }

        [Test]
        public void TestLookupWithValidValue()
        {
            var testCaseList = new[] {
                new { ExpectedValue = 3.39M, sFinMethT = E_sFinMethT.Fixed, sRLckD = "9/2/2013", sDue="12", sRAdj1stCapMon = string.Empty},
                new { ExpectedValue = 3.08M, sFinMethT = E_sFinMethT.Fixed, sRLckD = "9/2/2013", sDue="24", sRAdj1stCapMon = string.Empty},
                new { ExpectedValue = 4.57M, sFinMethT = E_sFinMethT.Fixed, sRLckD = "9/2/2013", sDue="360", sRAdj1stCapMon = string.Empty},
                new { ExpectedValue = 3.39M, sFinMethT = E_sFinMethT.Fixed, sRLckD = "9/4/2013", sDue="12", sRAdj1stCapMon = string.Empty},
                new { ExpectedValue = 3.08M, sFinMethT = E_sFinMethT.Fixed, sRLckD = "9/4/2013", sDue="24", sRAdj1stCapMon = string.Empty},
                new { ExpectedValue = 4.57M, sFinMethT = E_sFinMethT.Fixed, sRLckD = "9/4/2013", sDue="360", sRAdj1stCapMon = string.Empty},
                new { ExpectedValue = 3.39M, sFinMethT = E_sFinMethT.Fixed, sRLckD = "9/8/2013", sDue="12", sRAdj1stCapMon = string.Empty},
                new { ExpectedValue = 3.08M, sFinMethT = E_sFinMethT.Fixed, sRLckD = "9/8/2013", sDue="24", sRAdj1stCapMon = string.Empty},
                new { ExpectedValue = 4.57M, sFinMethT = E_sFinMethT.Fixed, sRLckD = "9/8/2013", sDue="360", sRAdj1stCapMon = string.Empty},


                new { ExpectedValue = 2.90M, sFinMethT = E_sFinMethT.ARM, sRLckD = "9/2/2013", sDue="360", sRAdj1stCapMon = "12"},
                new { ExpectedValue = 2.89M, sFinMethT = E_sFinMethT.ARM, sRLckD = "9/2/2013", sDue="360", sRAdj1stCapMon = "24"},
                new { ExpectedValue = 2.90M, sFinMethT = E_sFinMethT.ARM, sRLckD = "9/4/2013", sDue="360", sRAdj1stCapMon = "12"},
                new { ExpectedValue = 2.89M, sFinMethT = E_sFinMethT.ARM, sRLckD = "9/4/2013", sDue="360", sRAdj1stCapMon = "24"},
                new { ExpectedValue = 2.90M, sFinMethT = E_sFinMethT.ARM, sRLckD = "9/8/2013", sDue="360", sRAdj1stCapMon = "12"},
                new { ExpectedValue = 2.89M, sFinMethT = E_sFinMethT.ARM, sRLckD = "9/8/2013", sDue="360", sRAdj1stCapMon = "24"},
                        };

            foreach (var testCase in testCaseList)
            {
                Assert.AreEqual(testCase.ExpectedValue,
                    YieldTable.LookUp(testCase.sFinMethT, testCase.sRLckD, testCase.sDue, testCase.sRAdj1stCapMon, CDateTime.Create(testCase.sRLckD, null)),
                    testCase.ToString());
            }
        }
    }
}
