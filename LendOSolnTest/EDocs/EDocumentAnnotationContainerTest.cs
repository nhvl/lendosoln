﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EDocs;
using NUnit.Framework;
using iTextSharp.text;

namespace LendOSolnTest.EDocsTest
{
    [TestFixture]
    public class EDocumentAnnotationContainerTest
    {
        [Test]
        public void CreateEmptyContainerTest()
        {
            EDocumentAnnotationContainer container = new EDocumentAnnotationContainer();

            string xml = container.GetXmlContent();

            EDocumentAnnotationContainer newContainer = new EDocumentAnnotationContainer();
            newContainer.LoadContent(xml);

            Assert.AreEqual(container.Version, newContainer.Version);
            Assert.AreEqual(0, newContainer.AnnotationCount);
        }

        [Test]
        public void EDocumentAnnotationItem_NotesTest()
        {
            string createdBy = "CreatedBy";
            DateTime createdDate = new DateTime(2010, 1, 1);
            string lastModifiedBy = "LastModifiedBy";
            DateTime lastModifiedDate = new DateTime(2010, 10, 1, 11, 30, 30);
            int pageNumber = 10;
            string text = "THIS IS A NOTE TEST";
            Rectangle rectangle = new Rectangle(10, 20, 30, 40);

            EDocumentAnnotationContainer container = new EDocumentAnnotationContainer();

            EDocumentAnnotationItem item = new EDocumentAnnotationItem();
            item.CreatedBy = createdBy;
            item.CreatedDate = createdDate;
            item.LastModifiedBy = lastModifiedBy;
            item.LastModifiedDate = lastModifiedDate;
            item.PageNumber = pageNumber;
            item.Text = text;
            item.Rectangle = rectangle;
            item.ItemType = E_EDocumentAnnotationItemType.Notes;
            item.NoteStateType = E_DocumentAnnotationNoteStateType.Maximized;
            container.Add(item);

            string xml = container.GetXmlContent();
            EDocumentAnnotationContainer newContainer = new EDocumentAnnotationContainer();
            newContainer.LoadContent(xml);

            Assert.AreEqual(1, newContainer.AnnotationCount);

            foreach (var o in newContainer.AnnotationList)
            {
                Assert.AreEqual(createdBy, o.CreatedBy);
                Assert.AreEqual(createdDate, o.CreatedDate);
                Assert.AreEqual(lastModifiedBy, o.LastModifiedBy);
                Assert.AreEqual(lastModifiedDate, o.LastModifiedDate);
                Assert.AreEqual(pageNumber, o.PageNumber);
                Assert.AreEqual(text, o.Text);
                Assert.AreEqual(rectangle.Left, o.Rectangle.Left);
                Assert.AreEqual(rectangle.Right, o.Rectangle.Right);
                Assert.AreEqual(rectangle.Top, o.Rectangle.Top);
                Assert.AreEqual(rectangle.Bottom, o.Rectangle.Bottom);
                Assert.AreEqual(E_EDocumentAnnotationItemType.Notes, o.ItemType);
                Assert.AreEqual(E_DocumentAnnotationNoteStateType.Maximized, o.NoteStateType);
            }
        }

        [Test]
        public void EDocumentAnnotationItem_HighlightTest()
        {
            string createdBy = "CreatedBy";
            DateTime createdDate = new DateTime(2010, 1, 1);
            string lastModifiedBy = "LastModifiedBy";
            DateTime lastModifiedDate = new DateTime(2010, 10, 1, 11, 30, 30);
            int pageNumber = 10;
            Rectangle rectangle = new Rectangle(10, 20, 30, 40);

            EDocumentAnnotationContainer container = new EDocumentAnnotationContainer();

            EDocumentAnnotationItem item = new EDocumentAnnotationItem();
            item.CreatedBy = createdBy;
            item.CreatedDate = createdDate;
            item.LastModifiedBy = lastModifiedBy;
            item.LastModifiedDate = lastModifiedDate;
            item.PageNumber = pageNumber;
            item.Rectangle = rectangle;
            item.ItemType = E_EDocumentAnnotationItemType.Highlight;
            container.Add(item);

            string xml = container.GetXmlContent();
            EDocumentAnnotationContainer newContainer = new EDocumentAnnotationContainer();
            newContainer.LoadContent(xml);

            int count = 0;
            foreach (var o in newContainer.AnnotationList)
            {
                count++;

                Assert.AreEqual(createdBy, o.CreatedBy);
                Assert.AreEqual(createdDate, o.CreatedDate);
                Assert.AreEqual(lastModifiedBy, o.LastModifiedBy);
                Assert.AreEqual(lastModifiedDate, o.LastModifiedDate);
                Assert.AreEqual(pageNumber, o.PageNumber);
                Assert.AreEqual(rectangle.Left, o.Rectangle.Left);
                Assert.AreEqual(rectangle.Right, o.Rectangle.Right);
                Assert.AreEqual(rectangle.Top, o.Rectangle.Top);
                Assert.AreEqual(rectangle.Bottom, o.Rectangle.Bottom);
                Assert.AreEqual(E_EDocumentAnnotationItemType.Highlight, o.ItemType);
            }

            Assert.AreEqual(1, count);
        }

        [Test]
        public void TestAddingMultiples()
        {
            string createdBy = "CreatedBy";
            DateTime createdDate = new DateTime(2010, 1, 1);
            string lastModifiedBy = "LastModifiedBy";
            DateTime lastModifiedDate = new DateTime(2010, 10, 1, 11, 30, 30);
            int pageNumber = 10;
            Rectangle rectangle = new Rectangle(10, 20, 30, 40);

            EDocumentAnnotationContainer container = new EDocumentAnnotationContainer();

            EDocumentAnnotationItem item = new EDocumentAnnotationItem();
            item.CreatedBy = createdBy;
            item.CreatedDate = createdDate;
            item.LastModifiedBy = lastModifiedBy;
            item.LastModifiedDate = lastModifiedDate;
            item.PageNumber = pageNumber;
            item.Rectangle = rectangle;
            item.ItemType = E_EDocumentAnnotationItemType.Highlight;

            for (int i = 0; i < 20; i++)
            {
                container.Add(item);
            }

            string xml = container.GetXmlContent();
            EDocumentAnnotationContainer newContainer = new EDocumentAnnotationContainer();
            newContainer.LoadContent(xml);
            Assert.AreEqual(container.AnnotationCount, newContainer.AnnotationCount);
        }

    }
}
