﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EDocs;
using NUnit.Framework;
using LendOSolnTest.Common;
using System.IO;
using DataAccess;
using Org.BouncyCastle.Utilities.Collections;

namespace LendOSolnTest.EDocsTest
{
    [TestFixture]
    public class PDFPageIDManagerTest
    {
        [Test]
        public void TestNewClass()
        {
            PDFPageIDManager idManager = new PDFPageIDManager(TestDataRootFolder.Location + "400.pdf");
            for (int page = 1; page <= 400; page++)
            {
                Assert.That(idManager.GetPageId(page), Is.Null);
            }

        }

        [Test]
        public void TestUpdatePdf()
        {
            string path = Path.GetTempFileName();

            using (PDFPageIDManager idManager = new PDFPageIDManager(TestDataRootFolder.Location + "400.pdf"))
            {
                Dictionary<int, string> newIds = new Dictionary<int, string>();
                foreach (int page in idManager.PagesWithNoAnnotations)
                {
                    string key = newIds[page] = Guid.NewGuid().ToString();
                    idManager.Update(page, key);
                }


                idManager.Save(path);
                //Console.Out.WriteLine(path);

                PDFPageIDManager manager2;
                manager2 = new PDFPageIDManager(path);

                foreach (var id in newIds)
                {
                    Assert.That(manager2.GetPageId(id.Key), Is.EqualTo(id.Value));
                }
                manager2.Dispose();
            }

            
            //clean up
            File.Delete(path);
        }

        [Test]
        public void DeleteAllTest()
        {
            
            PDFPageIDManager idManager = new PDFPageIDManager(TestDataRootFolder.Location + "400withnotes.pdf");
            idManager.DeletePageIds();
            string path = Path.GetTempFileName();
            try
            {

                idManager.Save(path);
                idManager.Dispose();
                //System.Console.WriteLine(path);
                idManager = new PDFPageIDManager(path);

                HashSet<int> pages = new HashSet<int>();
                foreach (int page in idManager.PagesWithNoAnnotations)
                {
                    pages.Add(page);
                }

                for (int i = 1; i <= 400; i++)
                {
                    Assert.That(pages.Contains(i));
                }
                idManager.Dispose();
            }
            finally
            {
                File.Delete(path);
            }

        }

        [Test]
        public void DeleteFirst200()
        {

            PDFPageIDManager idManager = new PDFPageIDManager(TestDataRootFolder.Location + "400withnotes.pdf");
            List<int> idsToDelete = new List<int>();

            for (int i = 1; i <= 200; i++)
            {
                idManager.DeletePageIdOn(i); 
            }


            Dictionary<int, string> newKeys = new Dictionary<int, string>();
            for (int i = 201; i <= 400; i++)
            {
                string key = newKeys[i] = Guid.NewGuid().ToString() ;
                idManager.Update(i, key);
            }


            string path = Path.GetTempFileName();
            try
            {

                idManager.Save(path);
                idManager.Dispose();
                idManager = new PDFPageIDManager(path);

                HashSet<int> pages = new HashSet<int>();
                foreach (int page in idManager.PagesWithNoAnnotations)
                {
                    pages.Add(page);
                }

                for (int i = 1; i <= 200; i++)
                {
                    Assert.That(pages.Contains(i));
                }

                for (int i = 201; i <= 400; i++)
                {
                    Assert.That(newKeys[i], Is.EqualTo(idManager.GetPageId(i)));
                }
                idManager.Dispose();
            }
            finally
            {
                File.Delete(path);
            }

        }
    }
}
