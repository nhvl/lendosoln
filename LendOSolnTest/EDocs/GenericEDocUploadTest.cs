﻿namespace LendOSolnTest.EDocsTest
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Xml;
    using Common;
    using DataAccess;
    using EDocs;
    using EDocs.Contents;
    using Fakes;
    using LendersOffice.Common;
    using LendersOffice.Security;
    using LqbGrammar.Exceptions;
    using NUnit.Framework;

    /// <summary>
    /// Tests uploading and deleting different types of generic EDocs.
    /// </summary>
    [TestFixture]
    public class GenericEDocUploadTest
    {
        /// <summary>
        /// The name of the Appraisal test file.
        /// </summary>
        private const string AppraisalFileName = "AppraisalFile.xml";

        /// <summary>
        /// The name of the UCD test file.
        /// </summary>
        private const string UcdFileName = "UcdFile.xml";

        /// <summary>
        /// The name of the MS Excel test file.
        /// </summary>
        private const string SpreadsheetFileName = "SpreadsheetFile.xlsx";

        /// <summary>
        /// The name of a file which contains XML that is not of a format 
        /// </summary>
        private const string UnsupportedXmlFileName = "UnsupportedXmlFile.xml";

        /// <summary>
        /// The name of a file that does not contain parsable XML.
        /// </summary>
        private const string InvalidXmlFileName = "InvalidXmlFile.xml";

        /// <summary>
        /// The test user principal.
        /// </summary>
        private AbstractUserPrincipal principal;

        /// <summary>
        /// The document repository.
        /// </summary>
        private EDocumentRepository repository;

        /// <summary>
        /// The mock loan file.
        /// </summary>
        private CPageData loan;

        /// <summary>
        /// Gets the test user principal.
        /// </summary>
        /// <value>The test user principal.</value>
        protected AbstractUserPrincipal Principal
        {
            get
            {
                if (this.principal == null)
                {
                    this.principal = LoginTools.LoginAs(TestAccountType.Corporate_Processor);
                    this.principal.BrokerDB.TempOptionXmlContent += "<option name=\"AllowAnyXmlToValidateAsUcd\" value=\"true\" />";
                }

                return this.principal;
            }
        }

        /// <summary>
        /// Gets the document repository.
        /// </summary>
        /// <value>The document repository.</value>
        protected EDocumentRepository Repository
        {
            get
            {
                if (this.repository == null)
                {
                    this.repository = EDocumentRepository.GetSystemRepository(this.Principal.BrokerId);
                }

                return this.repository;
            }
        }

        /// <summary>
        /// Gets the mock loan file.
        /// </summary>
        /// <value>The mock loan file.</value>
        protected CPageData Loan
        {
            get
            {
                if (this.loan == null)
                {
                    this.loan = FakePageData.Create();
                }

                return this.loan;
            }
        }

        /// <summary>
        /// Tests uploading an Appraisal generic EDoc.
        /// </summary>
        [Test]
        public void AppraisalEdocFileUploadTest()
        {
            this.TestGenericEdocUpload(E_FileType.AppraisalXml, AppraisalFileName);
        }

        /// <summary>
        /// Tests uploading a UCD generic EDoc.
        /// </summary>
        [Test]
        public void UcdEdocFileUploadTest()
        {
            this.TestGenericEdocUpload(E_FileType.UniformClosingDataset, UcdFileName);
        }

        /// <summary>
        /// Tests uploading an MS Excel generic EDoc.
        /// </summary>
        [Test]
        public void SpreadsheetEdocFileUploadTest()
        {
            this.TestGenericEdocUpload(E_FileType.MicrosoftSpreadsheet, SpreadsheetFileName);
        }

        /// <summary>
        /// Tests uploading a file with valid XML that is not recognized as the other XML file types.
        /// </summary>
        [Test]
        public void UnsupportedXmlFileUploadTest()
        {
            this.TestGenericEdocUpload(E_FileType.UnspecifiedXml, UnsupportedXmlFileName);
        }

        /// <summary>
        /// Tests uploading a file that does not contain parsable XML.
        /// </summary>
        [Test]
        public void InvalidXmlFileUploadTest()
        {
            this.TestGenericEdocUpload(E_FileType.UniformClosingDataset, InvalidXmlFileName);
        }

        /// <summary>
        /// Tests uploading a generic EDoc. Validates that the contents are correct and that
        /// it gets assigned the correct file type.
        /// </summary>
        /// <param name="expectedType">The expected file type.</param>
        /// <param name="fileName">The test file name.</param>
        private void TestGenericEdocUpload(E_FileType expectedType, string fileName)
        {
            Thread.CurrentPrincipal = this.Principal;
            string filePath = TestUtilities.GetFilePath($"GenericEDocs/{fileName}");

            Guid docId = Guid.Empty;
            try
            {
                docId = this.UploadGenericEdoc(filePath, fileName);
            }
            catch (Exception e) when (e is CBaseException || e is DeveloperException || e is XmlException)
            {
                string failedUploadError = $"Generic EDoc test upload of file {fileName} failed.";

                if (e is XmlException)
                {
                    // Only the file with unparseable XML is allowed to throw an XML exception.
                    Assert.AreEqual(fileName, InvalidXmlFileName, failedUploadError);
                }

                return;
            }

            Assert.AreNotEqual(Guid.Empty, docId, $"An EDoc ID was not correctly assigned to the {expectedType} file.");

            // Unsupported or invalid files should never make it to this point.
            string falsePositiveUploadError = $"The file {fileName} was successfully uploaded, despite not being supported.";
            Assert.AreNotEqual(fileName, InvalidXmlFileName, falsePositiveUploadError);

            var edoc = this.RetrieveGenericEdoc(docId);

            Assert.IsNotNull(edoc, $"Could not retrieve test Generic EDoc with ID {docId}.");
            Assert.IsInstanceOf<GenericEDocument>(edoc);
            Assert.That(expectedType, Is.EqualTo(edoc.FileType));

            string expectedContents = File.ReadAllText(filePath);
            var edocContentPath = edoc.GetContentPath();
            string edocContents = File.ReadAllText(edocContentPath);

            Assert.AreEqual(expectedContents, edocContents);

            this.Repository.DeleteGenericDoc(docId, this.Principal.UserId);

            var deletedEdoc = this.RetrieveGenericEdoc(docId);
            Assert.IsNull(deletedEdoc, $"Generic Edoc with ID {docId} was not correctly deleted.");
        }

        /// <summary>
        /// Retrieves a generic EDoc from the repository.
        /// </summary>
        /// <param name="docId">The document ID.</param>
        /// <returns>A generic EDoc.</returns>
        private GenericEDocument RetrieveGenericEdoc(Guid docId)
        {
            try
            {
                return this.Repository.GetGenericDocumentById(docId);
            }
            catch (Exception e) when (e is CBaseException || e is AccessDenied)
            {
                return null;
            }
        }

        /// <summary>
        /// Uploads a generic EDoc.
        /// </summary>
        /// <param name="filePath">The path containing the file to upload.</param>
        /// <param name="fileName">The name of the file.</param>
        /// <returns>A GUID referencing the uploaded generic EDoc.</returns>
        /// <remarks>Note that it is crucial to use a valid loan and application ID.
        /// The sproc does a table join on the application so it will fail otherwise.</remarks>
        private Guid UploadGenericEdoc(string filePath, string fileName)
        {
            string contents = File.ReadAllText(filePath);
            var fileData = new FileData(fileName, contents);
            var doctypes = EDocumentDocType.GetDocTypesByBroker(this.Principal.BrokerId, E_EnforceFolderPermissions.True).ToList();

            return EDocsUpload.UploadDoc(
                this.Principal,
                fileData,
                loanId: this.Loan.sLId,
                appId: this.Loan.GetAppData(0).aAppId,
                docTypeId: int.Parse(doctypes.First().DocTypeId),
                internalComments: "A file comment.",
                description: $"This is {fileName}.");
        }
    }
}
