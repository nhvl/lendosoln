﻿using System.Linq;
using DataAccess;
using EDocs;
using LendersOffice.Constants;
using LendersOffice.Security;
using LendOSolnTest.Common;
using LendOSolnTest.Fakes;
using NUnit.Framework;

namespace LendOSolnTest.EDocsTest
{
    [TestFixture]
    public class EDocCreationTest
    {
        private AbstractUserPrincipal m_principal;

        [Test]
        public void TestPmlCreationAndEditing()
        {
            m_principal = LoginTools.LoginAs(TestAccountType.Pml_LO1);
            CreateEDocTest();
        }

        [Test]
        public void TestLoCreationAndEditing()
        {
            m_principal = LoginTools.LoginAs(TestAccountType.Corporate_LoanOfficer);
            CreateEDocTest();
        }

        [Test]
        [Category(CategoryConstants.ExpectFailureOnLocalhost)]
        public void ScannerTest()
        {
            if (ConstStage.AntiVirusScannerIPs.Count() == 0)
            {
                return; 
            }

            string path = TestUtilities.GetFilePath("Grid_Letter_Test.pdf");
            var result = AntiVirusScanner.Scan(path);

            Assert.That(result, Is.EqualTo(E_AntiVirusResult.NoVirus));
        }

        private void CreateEDocTest()
        {
            bool isPml = m_principal.Type == "P";
            
            string originalPath = TestDataRootFolder.Location + "\\Grid_Letter_Test.pdf";

            var data = FakePageData.Create();

            EDocumentRepository repo = EDocumentRepository.GetSystemRepository(m_principal.BrokerId);
            EDocument doc = repo.CreateDocument(E_EDocumentSource.UserUpload);

            var a = EDocumentDocType.GetDocTypesByBroker(m_principal.BrokerId, E_EnforceFolderPermissions.True).ToList();
            doc.DocumentTypeId = int.Parse(a.First().DocTypeId);
            doc.LoanId = data.sLId;
            doc.AppId = data.GetAppData(0).aAppId;
            doc.IsUploadedByPmlUser = isPml;
            doc.PublicDescription = "Description";
            doc.UpdatePDFContentOnSave(originalPath);
            doc.EDocOrigin = E_EDocOrigin.LO;
            repo.Save(doc);
            
            var newDoc = repo.GetDocumentById(doc.DocumentId);

            Assert.That(newDoc.AppId, Is.EqualTo(doc.AppId));
            Assert.That(newDoc.InternalDescription, Is.EqualTo(doc.InternalDescription));
            Assert.That(newDoc.IsUploadedByPmlUser, Is.EqualTo(doc.IsUploadedByPmlUser));
            Assert.That(newDoc.PublicDescription, Is.EqualTo(doc.PublicDescription));
            Assert.That(newDoc.PageCount, Is.EqualTo(4));
            
            newDoc.PublicDescription = "Updating the notes.";
            repo.Save(newDoc);
            
            newDoc = repo.GetDocumentById(doc.DocumentId);
            Assert.That(newDoc.PublicDescription, Is.EqualTo("Updating the notes."));

            PDFPageIDManager originalIdManager = new PDFPageIDManager(originalPath);
            PDFPageIDManager newIdManager = new PDFPageIDManager(newDoc.GetPDFTempFile_Current());
            
            Assert.That(originalIdManager.PageCount, Is.EqualTo(newIdManager.PageCount));
        }
    }
}
