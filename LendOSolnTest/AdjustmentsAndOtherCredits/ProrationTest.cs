﻿namespace LendOSolnTest.AdjustmentsAndOtherCredits
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using NUnit.Framework;
    using LendersOffice.ObjLib.AdjustmentsAndOtherCredits;
    using DataAccess;
    using LendersOffice.Common;

    [TestFixture]
    public class ProrationTest
    {
        public readonly DateTime ClosingDate = new DateTime(2015, 4, 15);
        public readonly LosConvert losConvert = new LosConvert();

        [Test]
        public void PaymentSource_PaidThroughDayBeforeClosing_IsBlank()
        {
            var proration = new Proration();

            var closingDate = CDateTime.Create(this.ClosingDate);
            var paidThroughDate = CDateTime.Create(closingDate.DateTimeForComputation.AddDays(-1));
            proration.ClosingDate = closingDate;
            proration.PaidThroughDate = paidThroughDate;

            Assert.AreEqual(E_PartyT.Blank, proration.PaidFromParty);
        }

        [Test]
        public void Id_BeforeAndAfterSave_IsTheSame()
        {
            var a1 = new Proration();
            a1.GenerateIdOnSerialization = true;
            var s = SerializationHelper.JsonNetSerialize(a1);
            var a2 = SerializationHelper.JsonNetDeserialize<Proration>(s);

            Assert.That(a1.Id.HasValue);
            Assert.AreEqual(a1.Id, a2.Id, "id differs before and after ser, deser.");
        }

        [Test]
        public void PaymentRecipient_PaidThroughDayBeforeClosing_IsBlank()
        {
            var proration = new Proration();

            var closingDate = CDateTime.Create(this.ClosingDate);
            var paidThroughDate = CDateTime.Create(closingDate.DateTimeForComputation.AddDays(-1));
            proration.ClosingDate = closingDate;
            proration.PaidThroughDate = paidThroughDate;

            Assert.AreEqual(E_PartyT.Blank, proration.PaidToParty);
        }

        [Test]
        public void PaidFromDate_PaidThroughDayBeforeClosing_IsBlank()
        {
            var proration = new Proration();

            var closingDate = CDateTime.Create(this.ClosingDate);
            var paidThroughDate = CDateTime.Create(closingDate.DateTimeForComputation.AddDays(-1));
            proration.ClosingDate = closingDate;
            proration.PaidThroughDate = paidThroughDate;

            Assert.AreEqual(CDateTime.InvalidWrapValue, proration.PaidFromDate);
        }

        [Test]
        public void PaidToDate_PaidThroughDayBeforeClosing_IsBlank()
        {
            var proration = new Proration();

            var closingDate = CDateTime.Create(this.ClosingDate);
            var paidThroughDate = CDateTime.Create(closingDate.DateTimeForComputation.AddDays(-1));
            proration.ClosingDate = closingDate;
            proration.PaidThroughDate = paidThroughDate;

            Assert.AreEqual(CDateTime.InvalidWrapValue, proration.PaidToDate);
        }

        [Test]
        public void PaymentSource_PaidThroughAfterClosing_IsBorrower()
        {
            var proration = new Proration();

            var closingDate = CDateTime.Create(this.ClosingDate);
            var paidThrough = CDateTime.Create(closingDate.DateTimeForComputation.AddMonths(1));
            proration.ClosingDate = closingDate;
            proration.PaidThroughDate = paidThrough;

            Assert.AreEqual(E_PartyT.Borrower, proration.PaidFromParty);
        }

        [Test]
        public void PaymentRecipient_PaidThroughAfterClosing_IsSeller()
        {
            var proration = new Proration();

            var closingDate = CDateTime.Create(this.ClosingDate);
            var paidThrough = CDateTime.Create(closingDate.DateTimeForComputation.AddMonths(1));
            proration.ClosingDate = closingDate;
            proration.PaidThroughDate = paidThrough;

            Assert.AreEqual(E_PartyT.Seller, proration.PaidToParty);
        }

        [Test]
        public void PaidFromDate_PaidThroughAfterClosing_IsClosingDate()
        {
            var proration = new Proration();

            var closingDate = CDateTime.Create(this.ClosingDate);
            var paidThrough = CDateTime.Create(closingDate.DateTimeForComputation.AddMonths(1));
            proration.ClosingDate = closingDate;
            proration.PaidThroughDate = paidThrough;

            Assert.AreEqual(closingDate.ToString(losConvert), proration.PaidFromDate.ToString(losConvert));
        }

        [Test]
        public void PaidToDate_PaidThroughAfterClosing_IsPaidThroughDate()
        {
            var proration = new Proration();

            var closingDate = CDateTime.Create(this.ClosingDate);
            var paidThrough = CDateTime.Create(closingDate.DateTimeForComputation.AddMonths(1));
            proration.ClosingDate = closingDate;
            proration.PaidThroughDate = paidThrough;

            Assert.AreEqual(paidThrough.ToString(losConvert), proration.PaidToDate.ToString(losConvert));
        }

        [Test]
        public void PaymentSource_NotPaidThroughDayBeforeClosing_IsSeller()
        {
            var proration = new Proration();

            var closingDate = CDateTime.Create(this.ClosingDate);
            var paidThrough = CDateTime.Create(closingDate.DateTimeForComputation.AddMonths(-1));
            proration.ClosingDate = closingDate;
            proration.PaidThroughDate = paidThrough;

            Assert.AreEqual(E_PartyT.Seller, proration.PaidFromParty);
        }

        [Test]
        public void PaymentRecipient_NotPaidThroughDayBeforeClosing_IsBorrower()
        {
            var proration = new Proration();

            var closingDate = CDateTime.Create(this.ClosingDate);
            var paidThrough = CDateTime.Create(closingDate.DateTimeForComputation.AddMonths(-1));
            proration.ClosingDate = closingDate;
            proration.PaidThroughDate = paidThrough;

            Assert.AreEqual(E_PartyT.Borrower, proration.PaidToParty);
        }

        [Test]
        public void PaidFromDate_NotPaidThroughDayBeforeClosing_IsDayAfterPaidThrough()
        {
            var proration = new Proration();

            var closingDate = CDateTime.Create(this.ClosingDate);
            var paidThrough = CDateTime.Create(closingDate.DateTimeForComputation.AddMonths(-1));
            proration.ClosingDate = closingDate;
            proration.PaidThroughDate = paidThrough;

            var dayAfterPaidThrough = CDateTime.Create(paidThrough.DateTimeForComputation.AddDays(1));
            Assert.AreEqual(dayAfterPaidThrough.ToString(losConvert), proration.PaidFromDate.ToString(losConvert));
        }

        [Test]
        public void PaidToDate_NotPaidThroughDayBeforeClosing_IsDayBeforeClosing()
        {
            var proration = new Proration();

            var closingDate = CDateTime.Create(this.ClosingDate);
            var paidThrough = CDateTime.Create(closingDate.DateTimeForComputation.AddMonths(-1));
            proration.ClosingDate = closingDate;
            proration.PaidThroughDate = paidThrough;

            var dayBeforeClosing = CDateTime.Create(closingDate.DateTimeForComputation.AddDays(-1));
            Assert.AreEqual(dayBeforeClosing.ToString(losConvert), proration.PaidToDate.ToString(losConvert));
        }

        [Test]
        public void PaidFromDate_SetPaidThroughValueBeforeSettingClosingValue_IsClosingDate()
        {
            var proration = new Proration();

            var closingDate = CDateTime.Create(this.ClosingDate);
            var paidThrough = CDateTime.Create(closingDate.DateTimeForComputation.AddMonths(1));
            proration.PaidThroughDate = paidThrough;
            proration.ClosingDate = closingDate;

            Assert.AreEqual(closingDate.ToString(losConvert), proration.PaidFromDate.ToString(losConvert));
        }

        [Test]
        public void Description_CityTaxSystemType_IsExpectedValue()
        {
            var type = E_ProrationT.CityTownTaxes;
            var proration = new Proration();
            proration.ProrationType = type;

            Assert.AreEqual("City/Town Taxes", proration.Description);
        }

        [Test]
        public void Description_CountyTaxSystemType_IsExpectedValue()
        {
            var type = E_ProrationT.CountyTaxes;
            var proration = new Proration();
            proration.ProrationType = type;

            Assert.AreEqual("County Taxes", proration.Description);
        }

        [Test]
        public void Description_AssessmentsSystemType_IsExpectedValue()
        {
            var type = E_ProrationT.Assessments;
            var proration = new Proration();
            proration.ProrationType = type;

            Assert.AreEqual("Assessments", proration.Description);
        }

        [Test]
        public void Description_CustomType_IsManualDescription()
        {
            var proration = new Proration();
            var manualDescription = "Manual Description";
            proration.Description = manualDescription;

            Assert.AreEqual(manualDescription, proration.Description);
        }

        [Test]
        public void Description_InvokeGetterForAllProrationTypes_DoesNotThrowException()
        {
            var proration = new Proration();
            foreach (E_ProrationT type in Enum.GetValues(typeof(E_ProrationT)))
            {
                proration.ProrationType = type;
                var desc = proration.Description;
            }
        }

        [Test]
        public void Equals_TwoDefaultInstances_IsTrue()
        {
            var p1 = new Proration();
            var p2 = new Proration();

            bool areEqual = p1.Equals(p2);

            Assert.AreEqual(true, areEqual);
        }

        [Test]
        public void Equals_DifferentType_IsFalse()
        {
            var p1 = new Proration(E_ProrationT.CityTownTaxes);
            var p2 = new Proration(E_ProrationT.CountyTaxes);

            bool areEqual = p1.Equals(p2);

            Assert.AreEqual(false, areEqual);
        }

        [Test]
        public void Equals_DifferentDescriptions_IsFalse()
        {
            var p1 = new Proration();
            var p2 = new Proration();

            p1.Description = "desc1";
            p2.Description = "desc2";
            bool areEqual = p1.Equals(p2);

            Assert.AreEqual(false, areEqual);
        }

        [Test]
        public void Equals_DifferentPaidThroughDates_IsFalse()
        {
            var p1 = new Proration();
            var p2 = new Proration();

            var dateString1 = "04/15/2015";
            var dateString2 = "04/17/2015";
            p1.PaidThroughDate = CDateTime.Create(dateString1, null);
            p2.PaidThroughDate = CDateTime.Create(dateString2, null);
            bool areEqual = p1.Equals(p2);

            Assert.AreEqual(false, areEqual);
        }

        [Test]
        public void Equals_DifferentAmounts_IsFalse()
        {
            var p1 = new Proration();
            var p2 = new Proration();

            p1.Amount = 10;
            p2.Amount = 15;
            bool areEqual = p1.Equals(p2);

            Assert.AreEqual(false, areEqual);
        }

        [Test]
        public void Equals_DifferentClosingDates_IsFalse()
        {
            var closingDate1= CDateTime.Create("04/15/15", null);
            var closingDate2= CDateTime.Create(closingDate1.DateTimeForComputation.AddDays(1));
            var p1 = new Proration();
            p1.ClosingDate = closingDate1;
            var p2 = new Proration();
            p2.ClosingDate = closingDate2;

            bool areEqual = p1.Equals(p2);

            Assert.AreEqual(false, areEqual);
        }

        [Test]
        public void Equals_CallerWithNullPaidThroughDate_IsFalse()
        {
            var p1 = new Proration();
            p1.PaidThroughDate = null;
            var p2 = new Proration();

            bool areEqual = p1.Equals(p2);

            Assert.AreEqual(false, areEqual);
        }

        [Test]
        public void Equals_ParamWithNullPaidThroughDate_IsFalse()
        {
            var p1 = new Proration();
            var p2 = new Proration();
            p2.PaidThroughDate = null;

            bool areEqual = p1.Equals(p2);

            Assert.AreEqual(false, areEqual);
        }

        [Test]
        public void Equals_BothNullPaidThroughDate_IsTrue()
        {
            var p1 = new Proration();
            p1.PaidThroughDate = null;
            var p2 = new Proration();
            p2.PaidThroughDate = null;

            bool areEqual = p1.Equals(p2);

            Assert.AreEqual(true, areEqual);
        }

        [Test]
        public void Equals_CallerWithNullClosingDate_IsFalse()
        {
            var p1 = new Proration();
            p1.ClosingDate = null;
            var p2 = new Proration();

            bool areEqual = p1.Equals(p2);

            Assert.AreEqual(false, areEqual);
        }

        [Test]
        public void Equals_ParamWithNullClosingDate_IsFalse()
        {
            var p1 = new Proration();
            var p2 = new Proration();
            p2.ClosingDate = null;

            bool areEqual = p1.Equals(p2);

            Assert.AreEqual(false, areEqual);
        }

        [Test]
        public void Equals_BothNullClosingDate_IsTrue()
        {
            var p1 = new Proration();
            p1.ClosingDate = null;
            var p2 = new Proration();
            p2.ClosingDate = null;

            bool areEqual = p1.Equals(p2);

            Assert.AreEqual(true, areEqual);
        }
    }
}
