﻿namespace LendOSolnTest.AdjustmentsAndOtherCredits
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using NUnit.Framework;
    using LendersOffice.ObjLib.AdjustmentsAndOtherCredits;
    using LendersOffice.Common;

    [TestFixture]
    public class AdjustmentTest
    {
        public readonly decimal DefaultAmount = 3000;

        [Test]
        public void BorrowerAmount_ToBorrowerAndNotFromSeller_IsAdjustmentAmount()
        {
            var adjustment = new Adjustment();

            adjustment.Amount = this.DefaultAmount;
            adjustment.PaidFromParty = E_PartyT.BorrowersEmployer;
            adjustment.PaidToParty = E_PartyT.Borrower;

            Assert.AreEqual(adjustment.Amount, adjustment.BorrowerAmount);
        }

        [Test]
        public void Id_BeforeAndAfterSave_IsTheSame()
        {
            var a1 = new Adjustment();
            a1.GenerateIdOnSerialization = true;
            var s = SerializationHelper.JsonNetSerialize(a1);
            var a2 = SerializationHelper.JsonNetDeserialize<Adjustment>(s);

            Assert.That(a1.Id.HasValue);
            Assert.AreEqual(a1.Id, a2.Id, "id differs before and after ser, deser.");
        }

        [Test]
        public void SellerAmount_ToBorrowerAndNotFromSeller_IsZero()
        {
            var adjustment = new Adjustment();

            adjustment.Amount = this.DefaultAmount;
            adjustment.PaidFromParty = E_PartyT.BorrowersEmployer;
            adjustment.PaidToParty = E_PartyT.Borrower;

            Assert.AreEqual(0, adjustment.SellerAmount);
        }

        [Test]
        public void BorrowerAmount_PaidFromSellerToBorrower_IsAdjustmentAmount()
        {
            var adjustment = new Adjustment();

            adjustment.Amount = this.DefaultAmount;
            adjustment.PaidFromParty = E_PartyT.Seller;
            adjustment.PaidToParty = E_PartyT.Borrower;

            Assert.AreEqual(adjustment.Amount, adjustment.BorrowerAmount);
        }

        [Test]
        public void SellerAmount_PaidFromSellerToBorrower_IsAdjustmentAmount()
        {
            var adjustment = new Adjustment();

            adjustment.Amount = this.DefaultAmount;
            adjustment.PaidFromParty = E_PartyT.Seller;
            adjustment.PaidToParty = E_PartyT.Borrower;

            Assert.AreEqual(adjustment.Amount, adjustment.SellerAmount);
        }

        [Test]
        public void BorrowerAmount_PaidFromSellerNotToBorrower_IsZero()
        {
            var adjustment = new Adjustment();

            adjustment.Amount = this.DefaultAmount;
            adjustment.PaidFromParty = E_PartyT.Seller;
            adjustment.PaidToParty = E_PartyT.Other;

            Assert.AreEqual(0, adjustment.BorrowerAmount);
        }

        [Test]
        public void SellerAmount_PaidFromSellerNotToBorrower_IsAdjustmentAmount()
        {
            var adjustment = new Adjustment();

            adjustment.Amount = this.DefaultAmount;
            adjustment.PaidFromParty = E_PartyT.Seller;
            adjustment.PaidToParty = E_PartyT.Other;

            Assert.AreEqual(adjustment.Amount, adjustment.SellerAmount);
        }

        [Test]
        public void BorrowerAmount_PaidFromBorrowerNotToSeller_IsAdjustmentAmount()
        {
            var adjustment = new Adjustment();

            adjustment.Amount = this.DefaultAmount;
            adjustment.PaidFromParty = E_PartyT.Borrower;
            adjustment.PaidToParty = E_PartyT.RealEstateAgent;

            Assert.AreEqual(adjustment.Amount, adjustment.BorrowerAmount);
        }

        [Test]
        public void SellerAmount_PaidFromBorrowerNotToSeller_IsZero()
        {
            var adjustment = new Adjustment();

            adjustment.Amount = this.DefaultAmount;
            adjustment.PaidFromParty = E_PartyT.Borrower;
            adjustment.PaidToParty = E_PartyT.RealEstateAgent;

            Assert.AreEqual(0, adjustment.SellerAmount);
        }

        [Test]
        public void AdjustmentType_DescriptionEmployerAssistedHousing_IsEmployerAssistedHousing()
        {
            var adjustment = new Adjustment();

            adjustment.Description = "Employer Assisted Housing";

            Assert.AreEqual(E_AdjustmentT.EmployerAssistedHousing, adjustment.AdjustmentType);
        }

        [Test]
        public void AdjustmentType_DescriptionLeasePurchaseFund_IsLeasePurchaseFund()
        {
            var adjustment = new Adjustment();

            adjustment.Description = "Lease Purchase Fund";

            Assert.AreEqual(E_AdjustmentT.LeasePurchaseFund, adjustment.AdjustmentType);
        }

        [Test]
        public void AdjustmentType_DescriptionRelocationFunds_IsRelocationFunds()
        {
            var adjustment = new Adjustment();

            adjustment.Description = "Relocation Funds";

            Assert.AreEqual(E_AdjustmentT.RelocationFunds, adjustment.AdjustmentType);
        }

        [Test]
        public void AdjustmentType_DescriptionSellerCredit_IsSellerCredit()
        {
            var adjustment = new Adjustment();

            adjustment.Description = "Seller Credit";

            Assert.AreEqual(E_AdjustmentT.SellerCredit, adjustment.AdjustmentType);
        }

        [Test]
        public void AdjustmentType_DescriptionPrincipalReductionReduceCashToBorrower_IsPrincipalReductionReduceCashToBorrower()
        {
            var adjustment = new Adjustment();

            adjustment.Description = "Principal Reduction to Reduce Cash to Borrower";

            Assert.AreEqual(E_AdjustmentT.PrincipalReductionToReduceCashToBorrower, adjustment.AdjustmentType);
            Assert.AreEqual(E_AdjustmentT.PrincipalReductionToReduceCashToBorrower, AdjustmentsAndOtherCreditsData.PredefinedAdjustmentsStringMap["Principal Reduction to Reduce Cash to Borrower"].Item1);
        }

        [Test]
        public void AdjustmentType_DescriptionPrincipalReductionCureToleranceViolation_IsPrincipalReductionCureToleranceViolation()
        {
            var adjustment = new Adjustment();

            adjustment.Description = "Principal Reduction to Cure Tolerance Violation";

            Assert.AreEqual(E_AdjustmentT.PrincipalReductionToCureToleranceViolation, adjustment.AdjustmentType);
            Assert.AreEqual(E_AdjustmentT.PrincipalReductionToCureToleranceViolation, AdjustmentsAndOtherCreditsData.PredefinedAdjustmentsStringMap["Principal Reduction to Cure Tolerance Violation"].Item1);
        }

        [Test]
        public void AdjustmentType_DescriptionCustom_IsOther()
        {
            var adjustment = new Adjustment();

            adjustment.Description = "Custom Adjustment";

            Assert.AreEqual(E_AdjustmentT.Other, adjustment.AdjustmentType);
        }

        [Test]
        public void Equals_TwoDefaultInstances_AreEqual()
        {
            var a1 = new Adjustment();
            var a2 = new Adjustment();

            bool areEqual = a1.Equals(a2);

            Assert.AreEqual(true, areEqual);
        }

        [Test]
        public void Equals_TwoNonDefaultInstances_AreEqual()
        {
            var a1 = new Adjustment();
            var a2 = new Adjustment();

            a1.Description = a2.Description = "desc";
            a1.PaidFromParty = a2.PaidFromParty = E_PartyT.Borrower;
            a1.PaidToParty = a2.PaidToParty = E_PartyT.Lender;
            a1.Amount = a2.Amount = 5;

            bool areEqual = a1.Equals(a2);

            Assert.AreEqual(true, areEqual);
        }

        [Test]
        public void Equals_DifferentDescriptions_AreNotEqual()
        {
            var a1 = new Adjustment();
            var a2 = new Adjustment();

            a1.Description = "blah";
            a2.Description = "desc";
            a1.PaidFromParty = a2.PaidFromParty = E_PartyT.Borrower;
            a1.PaidToParty = a2.PaidToParty = E_PartyT.Lender;
            a1.Amount = a2.Amount = 5;

            bool areEqual = a1.Equals(a2);

            Assert.AreEqual(false, areEqual);
        }

        [Test]
        public void Equals_DifferentPaidFromParties_AreNotEqual()
        {
            var a1 = new Adjustment();
            var a2 = new Adjustment();

            a1.Description = a2.Description = "desc";
            a1.PaidFromParty = E_PartyT.Blank;
            a2.PaidFromParty = E_PartyT.Borrower;
            a1.PaidToParty = a2.PaidToParty = E_PartyT.Lender;
            a1.Amount = a2.Amount = 5;

            bool areEqual = a1.Equals(a2);

            Assert.AreEqual(false, areEqual);
        }

        [Test]
        public void Equals_DifferentPaidToParties_AreNotEqual()
        {
            var a1 = new Adjustment();
            var a2 = new Adjustment();

            a1.Description = a2.Description = "desc";
            a1.PaidFromParty = a2.PaidFromParty = E_PartyT.Borrower;
            a1.PaidToParty = E_PartyT.Blank;
            a2.PaidToParty = E_PartyT.Lender;
            a1.Amount = a2.Amount = 5;

            bool areEqual = a1.Equals(a2);

            Assert.AreEqual(false, areEqual);
        }

        [Test]
        public void Equals_NullAdjustmentAsParameter_AreNotEqual()
        {
            var a1 = new Adjustment();
            Adjustment a2 = null;

            bool areEqual = a1.Equals(a2);

            Assert.AreEqual(false, areEqual);
        }

        [Test]
        public void Equals_NullObjectAsParameter_AreNotEqual()
        {
            var a1 = new Adjustment();
            object a2 = null;

            bool areEqual = a1.Equals(a2);

            Assert.AreEqual(false, areEqual);
        }

        [Test]
        public void Equals_NullDescriptionOnSourceObject_AreNotEqual()
        {
            var a1 = new Adjustment();
            a1.Description = null;

            var a2 = new Adjustment();
            a2.Description = "not null";

            bool areEqual = a1.Equals(a2);

            Assert.AreEqual(false, areEqual);
        }

        [Test]
        public void Dflp_DefaultValueForExistingRecords_IsFalse()
        {
            var json =
                @"{
                    ""Description"": ""Relocation Funds"",
                    ""PaidFromParty"": 4,
                    ""PaidFromParty_rep"": ""Borrower's Employer"",
                    ""IsPaidFromBorrower"": false,
                    ""IsPaidFromSeller"": false,
                    ""PaidToParty"": 1,
                    ""IsPaidToBorrower"": true,
                    ""IsPaidToSeller"": false,
                    ""Amount"": ""$20.00"",
                    ""BorrowerAmount"": ""$20.00"",
                    ""SellerAmount"": ""$0.00"",
                    ""PopulateTo"": 1,
                    ""IsPopulateToLineLOn1003"": true,
                    ""POC"": false
                }";

            var a = SerializationHelper.JsonNetDeserialize<Adjustment>(json);

            Assert.AreEqual(false, a.DFLP);
        }

        [Test]
        public void Dflp_SetToTrueWhenPaidToLender_IsTrue()
        {
            var a = new Adjustment();
            a.PaidToParty = E_PartyT.Lender;

            a.SetPocDflp(PocDflp.Dflp);

            Assert.AreEqual(true, a.DFLP);
        }

        [Test]
        public void Dflp_SetPocToTrue_IsFalse()
        {
            var a = new Adjustment();
            a.PaidToParty = E_PartyT.Lender;
            a.SetPocDflp(PocDflp.Dflp);

            a.SetPocDflp(PocDflp.Poc);

            Assert.AreEqual(false, a.DFLP);
        }

        [Test]
        public void PaidFrom_PrincipalReduction_IsBorrower()
        {
            Assert.AreEqual(E_PartyT.Borrower, Adjustment.DefaultPaidFromPartyTForAdjustmentT(E_AdjustmentT.PrincipalReductionToReduceCashToBorrower));
            Assert.AreEqual(E_PartyT.Borrower, Adjustment.DefaultPaidFromPartyTForAdjustmentT(E_AdjustmentT.PrincipalReductionToCureToleranceViolation));
            Assert.AreEqual(E_PartyT.Borrower, AdjustmentsAndOtherCreditsData.PredefinedAdjustmentTypeMap[E_AdjustmentT.PrincipalReductionToReduceCashToBorrower].PaidFromParty);
            Assert.AreEqual(E_PartyT.Borrower, AdjustmentsAndOtherCreditsData.PredefinedAdjustmentTypeMap[E_AdjustmentT.PrincipalReductionToCureToleranceViolation].PaidFromParty);
        }

        [Test]
        public void PaidTo_PrincipalReduction_IsLender()
        {
            Assert.AreEqual(E_PartyT.Lender, Adjustment.DefaultPaidToPartyTForAdjustmentT(E_AdjustmentT.PrincipalReductionToReduceCashToBorrower));
            Assert.AreEqual(E_PartyT.Lender, Adjustment.DefaultPaidToPartyTForAdjustmentT(E_AdjustmentT.PrincipalReductionToCureToleranceViolation));
            Assert.AreEqual(E_PartyT.Lender, AdjustmentsAndOtherCreditsData.PredefinedAdjustmentTypeMap[E_AdjustmentT.PrincipalReductionToReduceCashToBorrower].PaidToParty);
            Assert.AreEqual(E_PartyT.Lender, AdjustmentsAndOtherCreditsData.PredefinedAdjustmentTypeMap[E_AdjustmentT.PrincipalReductionToCureToleranceViolation].PaidToParty);
        }

        [Test]
        public void Poc_SetDflpToTrue_IsFalse()
        {
            var a = new Adjustment();
            a.PaidToParty = E_PartyT.Lender;
            a.SetPocDflp(PocDflp.Poc);

            a.SetPocDflp(PocDflp.Dflp);

            Assert.AreEqual(false, a.POC);
        }

        [Test]
        [ExpectedException]
        public void Serialization_BothPocDflpTrue_Throws()
        {
            var json =
                @"{
                    ""Description"": ""Relocation Funds"",
                    ""PaidFromParty"": 4,
                    ""PaidFromParty_rep"": ""Borrower's Employer"",
                    ""IsPaidFromBorrower"": false,
                    ""IsPaidFromSeller"": false,
                    ""PaidToParty"": 3,
                    ""IsPaidToBorrower"": true,
                    ""IsPaidToSeller"": false,
                    ""Amount"": ""$20.00"",
                    ""BorrowerAmount"": ""$20.00"",
                    ""SellerAmount"": ""$0.00"",
                    ""PopulateTo"": 1,
                    ""IsPopulateToLineLOn1003"": false,
                    ""POC"": true,
                    ""DFLP"": true
                }";

            var a = SerializationHelper.JsonNetDeserialize<Adjustment>(json);

            SerializationHelper.JsonNetSerialize(a);
        }
    }
}
