﻿namespace LendOSolnTest.AdjustmentsAndOtherCredits
{
    using System;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.AdjustmentsAndOtherCredits;
    using LendersOffice.Security;
    using LendOSolnTest.Common;
    using NUnit.Framework;

    [TestFixture]
    public class AdjustmentServicingPaymentTest
    {
        private static readonly DateTime DisbursementDate = DateTime.Parse("5/1/2018").Date;
        private static readonly string ServicingPaymentXml = TestUtilities.ReadFile("ServicingPayments.xml");
        private const string LinkedPaymentDefaultTransactionType = "Principal Reduction";

        private AbstractUserPrincipal principal;
        private Guid loanId;

        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            this.principal = LoginTools.LoginAs(TestAccountType.LoTest001);

            var creator = CLoanFileCreator.GetCreator(this.principal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
            this.loanId = creator.CreateBlankLoanFile();
        }

        [TestFixtureTearDown]
        public void FixtureTeardown()
        {
            Tools.SystemDeclareLoanFileInvalid(this.principal, this.loanId, sendNotifications: false, generateLinkLoanMsgEvent: false);
        }
        
        [Test]
        public void AdjustmentWithoutPaymentLink_DoesNotAffectPayments()
        {
            var loan = this.GetTestLoan(clearAdjustmentsAndPayments: true);

            var adjustmentList = new AdjustmentList();

            var newAdjustment = this.GetAdjustmentFromPredefinedAdjustmentList(E_AdjustmentT.BorrowerPaidFees);
            newAdjustment.Amount = 50M;
            adjustmentList.Add(newAdjustment);

            loan.sAdjustmentList = adjustmentList;
            loan.Save();

            loan = this.GetTestLoan();
            Assert.AreEqual(0, loan.sServicingPayments.Count);
        }

        [Test]
        [TestCase(E_AdjustmentT.PrincipalReductionToReduceCashToBorrower)]
        [TestCase(E_AdjustmentT.PrincipalReductionToCureToleranceViolation)]
        public void AddAdjustmentWithPaymentLink_NoExistingPayments_AddsOnePayment(E_AdjustmentT adjustmentType)
        {
            var loan = this.GetTestLoan(clearAdjustmentsAndPayments: true);

            var adjustmentList = new AdjustmentList();

            var newAdjustment = this.GetAdjustmentFromPredefinedAdjustmentList(adjustmentType);
            newAdjustment.Amount = 50M;
            adjustmentList.Add(newAdjustment);

            loan.sAdjustmentList = adjustmentList;
            loan.Save();

            loan = this.GetTestLoan();
            Assert.AreEqual(1, loan.sServicingPayments.Count);
        }

        [Test, Combinatorial]
        public void AddAdjustmentWithPaymentLink_NoExistingPayments_SetPaymentData(
            [Values(E_AdjustmentT.PrincipalReductionToCureToleranceViolation, E_AdjustmentT.PrincipalReductionToReduceCashToBorrower)] E_AdjustmentT adjustmentType,
            [Values(150, 1500)] decimal amount,
            [Values("Test1", "Test2")] string description,
            [Values(true, false)] bool poc,
            [Values(true, false)] bool dflp)
        {
            var loan = this.GetTestLoan(clearAdjustmentsAndPayments: true);

            var adjustmentList = new AdjustmentList();

            var newAdjustment = this.GenerateNewAdjustment(
                adjustmentType,
                description,
                amount,
                E_PartyT.Borrower,
                E_PartyT.Lender,
                E_PopulateToT.DoNotPopulate,
                poc,
                dflp);

            adjustmentList.Add(newAdjustment);

            loan.sAdjustmentList = adjustmentList;
            loan.Save();

            loan = this.GetTestLoan();
            Assert.AreEqual(1, loan.sServicingPayments.Count);

            var linkedPayment = loan.sServicingPayments[0];
            this.AssertPaymentValues(amount, linkedPayment);
            Assert.AreEqual(LinkedPaymentDefaultTransactionType, linkedPayment.ServicingTransactionT);

            var now = DateTime.Now.Date;
            DateTime compareTime;
            if (poc)
            {
                compareTime = DisbursementDate > now ? DisbursementDate : now;
            }
            else
            {
                compareTime = DisbursementDate;
            }

            Assert.AreEqual(compareTime, linkedPayment.DueD.Date);
            Assert.AreEqual(compareTime, linkedPayment.PaymentD.Date);
            Assert.AreEqual(compareTime, linkedPayment.PaymentReceivedD.Date);
            Assert.AreEqual(compareTime, linkedPayment.TransactionD.Date);
        }

        [Test]
        [TestCase(E_AdjustmentT.PrincipalReductionToReduceCashToBorrower)]
        [TestCase(E_AdjustmentT.PrincipalReductionToCureToleranceViolation)]
        public void AddAdjustmentWithPaymentLink_ExistingPayments_AddsPayment(E_AdjustmentT adjustmentType)
        {
            var loan = this.GetTestLoan(clearAdjustmentsAndPayments: true);

            loan.sServicingPmtXmlContent = ServicingPaymentXml;

            var adjustmentList = new AdjustmentList();

            var newAdjustment = this.GetAdjustmentFromPredefinedAdjustmentList(adjustmentType);
            newAdjustment.Amount = 50M;
            adjustmentList.Add(newAdjustment);

            loan.sAdjustmentList = adjustmentList;
            loan.Save();

            loan = this.GetTestLoan();
            Assert.AreEqual(3, loan.sServicingPayments.Count);
        }

        [Test, Combinatorial]
        public void AddAdjustmentWithPaymentLink_ExistingPayments_SetPaymentData(
            [Values(E_AdjustmentT.PrincipalReductionToCureToleranceViolation, E_AdjustmentT.PrincipalReductionToReduceCashToBorrower)] E_AdjustmentT adjustmentType,
            [Values(150, 1500)] decimal amount,
            [Values("Test1", "Test2")] string description,
            [Values(true, false)] bool poc,
            [Values(true, false)] bool dflp)
        {
            var loan = this.GetTestLoan(clearAdjustmentsAndPayments: true);

            loan.sServicingPmtXmlContent = ServicingPaymentXml;

            var adjustmentList = new AdjustmentList();

            Adjustment newAdjustment = this.GenerateNewAdjustment(
                adjustmentType,
                description,
                amount,
                E_PartyT.Borrower,
                E_PartyT.Lender,
                E_PopulateToT.DoNotPopulate,
                poc,
                dflp);

            adjustmentList.Add(newAdjustment);

            loan.sAdjustmentList = adjustmentList;
            loan.Save();

            loan = this.GetTestLoan();
            Assert.AreEqual(3, loan.sServicingPayments.Count);

            newAdjustment = loan.sAdjustmentList.Last();
            Assert.IsTrue(newAdjustment.LinkedServicingPaymentId.HasValue);

            var linkedPayment = loan.sServicingPayments.FirstOrDefault(payment => payment.Id == newAdjustment.LinkedServicingPaymentId.Value);
            AssertPaymentValues(amount, linkedPayment);
            Assert.AreEqual(LinkedPaymentDefaultTransactionType, linkedPayment.ServicingTransactionT);

            var now = DateTime.Now.Date;
            DateTime compareTime;
            if (poc)
            {
                compareTime = DisbursementDate > now ? DisbursementDate : now;
            }
            else
            {
                compareTime = DisbursementDate;
            }

            Assert.AreEqual(compareTime, linkedPayment.DueD.Date);
            Assert.AreEqual(compareTime, linkedPayment.PaymentD.Date);
            Assert.AreEqual(compareTime, linkedPayment.PaymentReceivedD.Date);
            Assert.AreEqual(compareTime, linkedPayment.TransactionD.Date);
        }

        [Test]
        [TestCase(E_AdjustmentT.PrincipalReductionToCureToleranceViolation)]
        [TestCase(E_AdjustmentT.PrincipalReductionToReduceCashToBorrower)]
        public void AddAdjustmentWithPaymentLink_ExistingNonLinkedAdjustment_AddsPayment(E_AdjustmentT newAdjustmentType)
        {
            var loan = this.GetTestLoan(clearAdjustmentsAndPayments: true);

            loan.sServicingPmtXmlContent = ServicingPaymentXml;

            var adjustmentList = new AdjustmentList();
            var existingAdjustment = this.GetAdjustmentFromPredefinedAdjustmentList(E_AdjustmentT.FuelCosts);
            existingAdjustment.Amount = 50M;

            adjustmentList.Add(existingAdjustment);
            loan.sAdjustmentList = adjustmentList;
            loan.Save();

            loan = this.GetTestLoan();
            
            Assert.AreEqual(2, loan.sServicingPayments.Count);

            adjustmentList = new AdjustmentList();
            var newAdjustment = this.GetAdjustmentFromPredefinedAdjustmentList(newAdjustmentType);
            newAdjustment.Amount = 150M;

            adjustmentList.Add(existingAdjustment);
            adjustmentList.Add(newAdjustment);

            loan.sAdjustmentList = adjustmentList;
            loan.Save();

            loan = this.GetTestLoan();
            Assert.AreEqual(3, loan.sServicingPayments.Count);
        }

        [Test]
        public void AddAdjustmentWithPaymentLink_ExistingLinkedAdjustment_AddsPayment()
        {
            var loan = this.GetTestLoan(clearAdjustmentsAndPayments: true);

            loan.sServicingPmtXmlContent = ServicingPaymentXml;

            var adjustmentList = new AdjustmentList();
            var existingAdjustment = this.GetAdjustmentFromPredefinedAdjustmentList(E_AdjustmentT.PrincipalReductionToCureToleranceViolation);
            existingAdjustment.Amount = 50M;

            adjustmentList.Add(existingAdjustment);
            loan.sAdjustmentList = adjustmentList;
            loan.Save();

            loan = this.GetTestLoan();
            Assert.AreEqual(3, loan.sServicingPayments.Count);

            adjustmentList = new AdjustmentList();
            var newAdjustment = this.GetAdjustmentFromPredefinedAdjustmentList(E_AdjustmentT.PrincipalReductionToReduceCashToBorrower);
            newAdjustment.Amount = 150M;

            adjustmentList.Add(existingAdjustment);
            adjustmentList.Add(newAdjustment);

            loan.sAdjustmentList = adjustmentList;
            loan.Save();

            loan = this.GetTestLoan();
            Assert.AreEqual(4, loan.sServicingPayments.Count);
        }

        [Test, Combinatorial]
        public void AddAdjustmentWithPaymentLink_ExistingNonLinkedAdjustments_SetPaymentData(
            [Values(E_AdjustmentT.PrincipalReductionToCureToleranceViolation, E_AdjustmentT.PrincipalReductionToReduceCashToBorrower)] E_AdjustmentT adjustmentType,
            [Values(150, 1500)] decimal amount,
            [Values("Test1", "Test2")] string description,
            [Values(true, false)] bool poc,
            [Values(true, false)] bool dflp)
        {
            var loan = this.GetTestLoan(clearAdjustmentsAndPayments: true);

            loan.sServicingPmtXmlContent = ServicingPaymentXml;

            var adjustmentList = new AdjustmentList();
            var existingAdjustment = this.GetAdjustmentFromPredefinedAdjustmentList(E_AdjustmentT.FuelCosts);
            existingAdjustment.Amount = 50M;

            adjustmentList.Add(existingAdjustment);
            loan.sAdjustmentList = adjustmentList;
            loan.Save();

            loan = this.GetTestLoan();
            adjustmentList = new AdjustmentList();

            Adjustment newAdjustment = this.GenerateNewAdjustment(
                adjustmentType,
                description,
                amount,
                E_PartyT.Borrower,
                E_PartyT.Lender,
                E_PopulateToT.DoNotPopulate,
                poc,
                dflp);

            adjustmentList.Add(existingAdjustment);
            adjustmentList.Add(newAdjustment);

            loan.sAdjustmentList = adjustmentList;
            loan.Save();

            loan = this.GetTestLoan();
            Assert.AreEqual(3, loan.sServicingPayments.Count);

            newAdjustment = loan.sAdjustmentList.Last();
            Assert.IsTrue(newAdjustment.LinkedServicingPaymentId.HasValue);

            var linkedPayment = loan.sServicingPayments.FirstOrDefault(payment => payment.Id == newAdjustment.LinkedServicingPaymentId.Value);
            AssertPaymentValues(amount, linkedPayment);
            Assert.AreEqual(LinkedPaymentDefaultTransactionType, linkedPayment.ServicingTransactionT);

            var now = DateTime.Now.Date;
            DateTime compareTime;
            if (poc)
            {
                compareTime = DisbursementDate > now ? DisbursementDate : now;
            }
            else
            {
                compareTime = DisbursementDate;
            }

            Assert.AreEqual(compareTime, linkedPayment.DueD.Date);
            Assert.AreEqual(compareTime, linkedPayment.PaymentD.Date);
            Assert.AreEqual(compareTime, linkedPayment.PaymentReceivedD.Date);
            Assert.AreEqual(compareTime, linkedPayment.TransactionD.Date);
        }

        [Test, Combinatorial]
        public void AddAdjustmentWithPaymentLink_ExistingLinkedAdjustments_SetPaymentData(
            [Values(150, 1500)] decimal amount,
            [Values("Test1", "Test2")] string description,
            [Values(true, false)] bool poc,
            [Values(true, false)] bool dflp)
        {
            var loan = this.GetTestLoan(clearAdjustmentsAndPayments: true);

            loan.sServicingPmtXmlContent = ServicingPaymentXml;

            var adjustmentList = new AdjustmentList();
            var existingAdjustment = this.GetAdjustmentFromPredefinedAdjustmentList(E_AdjustmentT.PrincipalReductionToCureToleranceViolation);
            existingAdjustment.Amount = 50M;

            adjustmentList.Add(existingAdjustment);
            loan.sAdjustmentList = adjustmentList;
            loan.Save();

            loan = this.GetTestLoan();
            adjustmentList = new AdjustmentList();

            Adjustment newAdjustment = this.GenerateNewAdjustment(
                E_AdjustmentT.PrincipalReductionToReduceCashToBorrower,
                description,
                amount,
                E_PartyT.Borrower,
                E_PartyT.Lender,
                E_PopulateToT.DoNotPopulate,
                poc,
                dflp);

            adjustmentList.Add(existingAdjustment);
            adjustmentList.Add(newAdjustment);

            loan.sAdjustmentList = adjustmentList;
            loan.Save();

            loan = this.GetTestLoan();
            Assert.AreEqual(4, loan.sServicingPayments.Count);

            newAdjustment = loan.sAdjustmentList.Last();
            Assert.IsTrue(newAdjustment.LinkedServicingPaymentId.HasValue);

            var linkedPayment = loan.sServicingPayments.FirstOrDefault(payment => payment.Id == newAdjustment.LinkedServicingPaymentId.Value);
            AssertPaymentValues(amount, linkedPayment);
            Assert.AreEqual(LinkedPaymentDefaultTransactionType, linkedPayment.ServicingTransactionT);

            var now = DateTime.Now.Date;
            DateTime compareTime;
            if (poc)
            {
                compareTime = DisbursementDate > now ? DisbursementDate : now;
            }
            else
            {
                compareTime = DisbursementDate;
            }

            Assert.AreEqual(compareTime, linkedPayment.DueD.Date);
            Assert.AreEqual(compareTime, linkedPayment.PaymentD.Date);
            Assert.AreEqual(compareTime, linkedPayment.PaymentReceivedD.Date);
            Assert.AreEqual(compareTime, linkedPayment.TransactionD.Date);
        }

        [Test]
        [TestCase(E_AdjustmentT.PrincipalReductionToCureToleranceViolation)]
        [TestCase(E_AdjustmentT.PrincipalReductionToReduceCashToBorrower)]
        public void AddAdjustmentWithPaymentLink_NoConsummationDate_UsesTodaysDate(E_AdjustmentT type)
        {
            var loan = this.GetTestLoan(clearAdjustmentsAndPayments: true, useTestDisbursementDate: false);

            loan.sConsummationDLckd = true;
            loan.sConsummationD_rep = string.Empty;

            var adjustmentList = new AdjustmentList();

            var newAdjustment = this.GetAdjustmentFromPredefinedAdjustmentList(type);
            newAdjustment.Amount = 5M;
            adjustmentList.Add(newAdjustment);

            loan.sAdjustmentList = adjustmentList;
            loan.Save();

            loan = this.GetTestLoan();
            Assert.AreEqual(1, loan.sServicingPayments.Count);

            var linkedPayment = loan.sServicingPayments[0];
            Assert.AreEqual(DateTime.Now.Date, linkedPayment.DueD.Date);
            Assert.AreEqual(DateTime.Now.Date, linkedPayment.PaymentD.Date);
            Assert.AreEqual(DateTime.Now.Date, linkedPayment.PaymentReceivedD.Date);
            Assert.AreEqual(DateTime.Now.Date, linkedPayment.TransactionD.Date);
        }

        [Test, Combinatorial]
        public void EditAdjustmentWithPaymentLink_SyncsChanges(
            [Values(E_AdjustmentT.PrincipalReductionToCureToleranceViolation, E_AdjustmentT.PrincipalReductionToReduceCashToBorrower)] E_AdjustmentT type, 
            [Values(150, 1500)] decimal amount)
        {
            var loan = this.GetTestLoan(clearAdjustmentsAndPayments: true);

            loan.sServicingPmtXmlContent = ServicingPaymentXml;

            var adjustmentList = new AdjustmentList();

            Adjustment newAdjustment = this.GetAdjustmentFromPredefinedAdjustmentList(type);
            newAdjustment.Amount = 5M;
            adjustmentList.Add(newAdjustment);

            loan.sAdjustmentList = adjustmentList;
            loan.Save();

            loan = this.GetTestLoan();
            Assert.AreEqual(3, loan.sServicingPayments.Count);

            adjustmentList = new AdjustmentList();

            newAdjustment = loan.sAdjustmentList.Last();
            newAdjustment.Amount = amount;

            adjustmentList.Add(newAdjustment);

            loan.sAdjustmentList = adjustmentList;
            loan.Save();

            loan = this.GetTestLoan();
            Assert.AreEqual(3, loan.sServicingPayments.Count);
            Assert.IsTrue(newAdjustment.LinkedServicingPaymentId.HasValue);

            var linkedPayment = loan.sServicingPayments.FirstOrDefault(payment => payment.Id == newAdjustment.LinkedServicingPaymentId.Value);
            AssertPaymentValues(amount, linkedPayment);
        }

        [Test, Combinatorial]
        public void GiveZeroNegativeAdjustmentWithPaymentLinkValue_AddsPayment(
            [Values(E_AdjustmentT.PrincipalReductionToCureToleranceViolation, E_AdjustmentT.PrincipalReductionToReduceCashToBorrower)] E_AdjustmentT adjustmentType, 
            [Values(0, -1)] decimal initialAmount)
        {
            var loan = this.GetTestLoan(clearAdjustmentsAndPayments: true);

            loan.sServicingPmtXmlContent = ServicingPaymentXml;

            var adjustmentList = new AdjustmentList();

            var newAdjustment = this.GetAdjustmentFromPredefinedAdjustmentList(adjustmentType);
            newAdjustment.Amount = initialAmount;
            adjustmentList.Add(newAdjustment);

            loan.sAdjustmentList = adjustmentList;
            loan.Save();

            loan = this.GetTestLoan();
            Assert.AreEqual(2, loan.sServicingPayments.Count);

            adjustmentList = new AdjustmentList();
            newAdjustment.Amount = 50M;
            adjustmentList.Add(newAdjustment);

            loan.sAdjustmentList = adjustmentList;
            loan.Save();

            loan = this.GetTestLoan();
            Assert.AreEqual(3, loan.sServicingPayments.Count);
        }

        [Test, Combinatorial]
        public void SetAdjustmentWithPaymentLinkZeroNegative_DeletesPayment(
            [Values(E_AdjustmentT.PrincipalReductionToCureToleranceViolation, E_AdjustmentT.PrincipalReductionToReduceCashToBorrower)] E_AdjustmentT adjustmentType,
            [Values(0, -1)] decimal newAmount)
        {
            var loan = this.GetTestLoan(clearAdjustmentsAndPayments: true);

            loan.sServicingPmtXmlContent = ServicingPaymentXml;

            var adjustmentList = new AdjustmentList();

            var newAdjustment = this.GetAdjustmentFromPredefinedAdjustmentList(adjustmentType);
            newAdjustment.Amount = 50M;
            adjustmentList.Add(newAdjustment);
            
            loan.sAdjustmentList = adjustmentList;
            loan.Save();

            loan = this.GetTestLoan();
            Assert.AreEqual(3, loan.sServicingPayments.Count);

            adjustmentList = new AdjustmentList();
            var oldId = newAdjustment.Id;
            var oldLinkedPaymentId = newAdjustment.LinkedServicingPaymentId;

            newAdjustment = this.GetAdjustmentFromPredefinedAdjustmentList(adjustmentType);
            newAdjustment.SetId(oldId.Value);
            newAdjustment.LinkedServicingPaymentId = oldLinkedPaymentId;
            newAdjustment.Amount = newAmount;

            adjustmentList.Add(newAdjustment);

            loan.sAdjustmentList = adjustmentList;
            loan.Save();

            loan = this.GetTestLoan();
            Assert.AreEqual(2, loan.sServicingPayments.Count);
            Assert.IsNull(newAdjustment.LinkedServicingPaymentId);

            var linkedPayment = loan.sServicingPayments.FirstOrDefault(payment => payment.Id == oldLinkedPaymentId);
            Assert.IsNull(linkedPayment);
        }

        [Test]
        [TestCase(E_AdjustmentT.PrincipalReductionToCureToleranceViolation)]
        [TestCase(E_AdjustmentT.PrincipalReductionToReduceCashToBorrower)]
        public void DeleteAdjustmentWithPaymentLink_DeletesPayment(E_AdjustmentT adjustmentType)
        {
            var loan = this.GetTestLoan(clearAdjustmentsAndPayments: true);

            loan.sServicingPmtXmlContent = ServicingPaymentXml;

            var adjustmentList = new AdjustmentList();

            var newAdjustment = this.GetAdjustmentFromPredefinedAdjustmentList(adjustmentType);
            newAdjustment.Amount = 50M;
            adjustmentList.Add(newAdjustment);

            loan.sAdjustmentList = adjustmentList;
            loan.Save();

            loan = this.GetTestLoan();
            Assert.AreEqual(3, loan.sServicingPayments.Count);

            adjustmentList = new AdjustmentList();
            loan.sAdjustmentList = adjustmentList;
            loan.Save();

            loan = this.GetTestLoan();
            Assert.AreEqual(0, loan.sAdjustmentList.GetEnumeratorWithoutSellerCredit().Count());
            Assert.AreEqual(2, loan.sServicingPayments.Count);
        }

        [Test]
        [TestCase(E_AdjustmentT.PrincipalReductionToCureToleranceViolation)]
        [TestCase(E_AdjustmentT.PrincipalReductionToReduceCashToBorrower)]
        public void ClearAdjustmentsThatPopulateTo1003_ClearsLinkedPayments(E_AdjustmentT adjustmentType)
        {
            var loan = this.GetTestLoan(clearAdjustmentsAndPayments: true);

            loan.sServicingPmtXmlContent = ServicingPaymentXml;

            var adjustmentList = new AdjustmentList();

            Adjustment firstAdjustment = this.GetAdjustmentFromPredefinedAdjustmentList(adjustmentType);
            firstAdjustment.Amount = 50M;
            firstAdjustment.SetIsPopulateToLineLOn1003(false);

            Adjustment secondAdjustment = this.GetAdjustmentFromPredefinedAdjustmentList(adjustmentType);
            secondAdjustment.Amount = 150M;
            secondAdjustment.SetIsPopulateToLineLOn1003(true);

            adjustmentList.Add(firstAdjustment);
            adjustmentList.Add(secondAdjustment);

            loan.sAdjustmentList = adjustmentList;
            loan.Save();

            loan = this.GetTestLoan();
            Assert.AreEqual(4, loan.sServicingPayments.Count);

            var loanAdjustmentList = loan.sAdjustmentList.GetEnumeratorWithoutSellerCredit();
            firstAdjustment = loanAdjustmentList.ElementAt(0);
            secondAdjustment = loanAdjustmentList.ElementAt(1);

            adjustmentList = new AdjustmentList();
            adjustmentList.Add(firstAdjustment);
            adjustmentList.Add(secondAdjustment);
            adjustmentList.ClearAdjustmentsThatPopulateTo1003();

            loan.sAdjustmentList = adjustmentList;
            loan.Save();

            loan = this.GetTestLoan();
            Assert.AreEqual(3, loan.sServicingPayments.Count);
            
            var linkedPayment = loan.sServicingPayments.FirstOrDefault(payment => payment.Id == secondAdjustment.LinkedServicingPaymentId.Value);
            Assert.IsNull(linkedPayment);
        }

        private CPageData GetTestLoan(bool clearAdjustmentsAndPayments = false, bool useTestDisbursementDate = true)
        {
            var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.loanId, typeof(AdjustmentServicingPaymentTest));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            loan.sDisclosureRegulationTLckd = true;
            loan.sDisclosureRegulationT = E_sDisclosureRegulationT.TRID;

            if (clearAdjustmentsAndPayments)
            {
                loan.sServicingPmtXmlContent = string.Empty;
                loan.sAdjustmentList = new AdjustmentList();
            }

            var disbursementDate = useTestDisbursementDate ? DisbursementDate : CDateTime.InvalidDateTime;
            loan.sDocMagicDisbursementDLckd = true;
            loan.sDocMagicDisbursementD_rep = loan.m_convertLos.ToDateTimeString(disbursementDate, displayTime: false);

            return loan;
        }

        private MockAdjustment GetAdjustmentFromPredefinedAdjustmentList(E_AdjustmentT type)
        {
            var predefinedAdjustment = AdjustmentsAndOtherCreditsData.PredefinedAdjustmentTypeMap[type];
            return GenerateNewAdjustment(
                type,
                predefinedAdjustment.Description,
                0M,
                predefinedAdjustment.PaidFromParty,
                predefinedAdjustment.PaidToParty,
                predefinedAdjustment.PopulateTo,
                predefinedAdjustment.POC,
                predefinedAdjustment.DFLP);
        }

        private MockAdjustment GenerateNewAdjustment(
            E_AdjustmentT type, 
            string description, 
            decimal amount, 
            E_PartyT paidFromParty, 
            E_PartyT paidToParty,
            E_PopulateToT populateTo, 
            bool poc, 
            bool dflp)
        {
            var newAdjustment = new MockAdjustment()
            {
                AdjustmentType = type,
                Description = description,
                Amount = amount,
                PaidFromParty = paidFromParty,
                PaidToParty = paidToParty,
                PopulateTo = populateTo
            };

            newAdjustment.SetId(Guid.NewGuid());
            newAdjustment.SetPocDflp(this.GetPocDflpSetting(poc, dflp));
            return newAdjustment;
        }

        private PocDflp GetPocDflpSetting(bool poc, bool dflp)
        {
            if (poc)
            {
                return PocDflp.Poc;
            }

            return dflp ? PocDflp.Dflp : PocDflp.Neither;
        }

        private void AssertPaymentValues(decimal amount, CServicingPaymentFields linkedPayment)
        {
            Assert.IsNotNull(linkedPayment);
            Assert.AreEqual(amount, linkedPayment.DueAmt);
            Assert.AreEqual(amount, linkedPayment.PaymentAmt);
            Assert.AreEqual(amount, linkedPayment.Principal);
        }

        private class MockAdjustment : Adjustment
        {
            public void SetId(Guid id)
            {
                this.Id = id;
            }
        }
    }
}
