﻿namespace LendOSolnTest.AdjustmentsAndOtherCredits
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using NUnit.Framework;
    using LendersOffice.ObjLib.AdjustmentsAndOtherCredits;
    using DataAccess;
    using LendersOffice.Common;

    [TestFixture]
    public class ProrationListTest
    {
        public readonly DateTime DefaultClosingDate = new DateTime(2015, 4, 15);
        public readonly LosConvert losConvert = new LosConvert();

        [Test]
        public void CreateList_NoManualAdditions_HasCityTaxItem()
        {
            var closingDate = CDateTime.Create(this.DefaultClosingDate);
            var list = new ProrationList(closingDate);

            var numCityTaxItems = list.Count(
                p => p.ProrationType == E_ProrationT.CityTownTaxes);

            Assert.AreEqual(1, numCityTaxItems);
        }


        [Test]
        public void Ids_BeforeAndAfterSerDeser_AreTheSame()
        {
            var json = @"
                {
	""ClosingDate_rep"" : ""5 / 19 / 2015"",
    ""PopulateTo"" : 0,
	""IsPopulateToLineLOn1003"" : false,
	""CityTownTaxes"" : {
                ""ProrationType"" : 1,
		""IsSystemProration"" : true,
		""IsPaidFromBorrowerToSeller"" : false,
		""IsPaidFromSellerToBorrower"" : true,
		""Description"" : ""City/Town Taxes"",
		""PaidThroughDate"" : ""5/12/2015"",
		""PaidFromDate"" : ""5/13/2015"",
		""PaidToDate"" : ""5/18/2015"",
		""Amount"" : ""$0.50"",
		""AmountPaidFromBorrowerToSeller"" : ""$0.00"",
		""AmountPaidFromSellerToBorrower"" : ""$0.50"",
		""PaidFromParty"" : 2,
		""PaidToParty"" : 1
    },
	""CountyTaxes"" : {
                ""ProrationType"" : 2,
		""IsSystemProration"" : true,
		""IsPaidFromBorrowerToSeller"" : false,
		""IsPaidFromSellerToBorrower"" : false,
		""Description"" : ""County Taxes"",
		""PaidThroughDate"" : ""5/18/2015"",
		""PaidFromDate"" : """",
		""PaidToDate"" : """",
		""Amount"" : ""$0.00"",
		""AmountPaidFromBorrowerToSeller"" : ""$0.00"",
		""AmountPaidFromSellerToBorrower"" : ""$0.00"",
		""PaidFromParty"" : 0,
		""PaidToParty"" : 0
    },
	""Assessments"" : {
                ""ProrationType"" : 3,
		""IsSystemProration"" : true,
		""IsPaidFromBorrowerToSeller"" : false,
		""IsPaidFromSellerToBorrower"" : false,
		""Description"" : ""Assessments"",
		""PaidThroughDate"" : """",
		""PaidFromDate"" : """",
		""PaidToDate"" : """",
		""Amount"" : ""$3.00"",
		""AmountPaidFromBorrowerToSeller"" : ""$0.00"",
		""AmountPaidFromSellerToBorrower"" : ""$0.00"",
		""PaidFromParty"" : 0,
		""PaidToParty"" : 0
    },
	""CustomItems"" : []
    }";

            var list1 = SerializationHelper.JsonNetDeserialize<ProrationList>(json);
            list1.GenerateIdsOnSerialization = true;
            var json2 = SerializationHelper.JsonNetSerialize(list1);
            var list2 = SerializationHelper.JsonNetDeserialize<ProrationList>(json2);

            Assert.That(list1.CityTownTaxes.Id.HasValue);
            Assert.That(list1.Assessments.Id.HasValue);
            Assert.That(list1.CountyTaxes.Id.HasValue);

            Assert.AreEqual(list1.CityTownTaxes.Id, list2.CityTownTaxes.Id);
            Assert.AreEqual(list1.Assessments.Id, list2.Assessments.Id);
            Assert.AreEqual(list1.CountyTaxes.Id, list2.CountyTaxes.Id);
        }

        [Test]
        public void CreateList_NoManualAdditions_HasCountyTaxItem()
        {
            var closingDate = CDateTime.Create(this.DefaultClosingDate);
            var list = new ProrationList(closingDate);

            var numCountyTaxItems = list.Count(
                p => p.ProrationType == E_ProrationT.CountyTaxes);

            Assert.AreEqual(1, numCountyTaxItems);
        }

        [Test]
        public void CreateList_NoManualAdditions_HasAssessmentItem()
        {
            var closingDate = CDateTime.Create(this.DefaultClosingDate);
            var list = new ProrationList(closingDate);

            var numAssessmentItems = list.Count(
                p => p.ProrationType == E_ProrationT.Assessments);

            Assert.AreEqual(1, numAssessmentItems);
        }

        [Test]
        public void CreateList_NoManualAdditions_AllItemsHaveSameClosingDate()
        {
            var closingDate = CDateTime.Create(this.DefaultClosingDate);
            var list = new ProrationList(closingDate);

            foreach (var item in list)
            {
                Assert.AreEqual(closingDate.ToString(losConvert), item.ClosingDate.ToString(losConvert));
            }
        }

        [Test]
        public void ClosingDate_UpdateAfterListCreation_UpdatesAllItemsToSameClosingDate()
        {
            var closingDate = CDateTime.Create(this.DefaultClosingDate);
            var list = new ProrationList(closingDate);

            var newClosingDate = CDateTime.Create(closingDate.DateTimeForComputation.AddMonths(1));
            list.ClosingDate = newClosingDate;

            foreach (var item in list)
            {
                Assert.AreEqual(newClosingDate.ToString(losConvert), item.ClosingDate.ToString(losConvert));
            }
        }

        [Test]
        public void DefaultItems_AfterDeserialize_NotDoubled()
        {
            var closingDate = CDateTime.Create(this.DefaultClosingDate);
            var list = new ProrationList(closingDate);

            var json = SerializationHelper.JsonNetSerialize(list);
            list = SerializationHelper.JsonNetDeserialize<ProrationList>(json);

            Assert.AreEqual(3, list.Count());
        }



        [Test]
        public void CityTownTaxItem_SetToWrongTypeInJson_FixedAfterDeserialize()
        {
            var list = SerializationHelper.JsonNetDeserialize<ProrationList>(JsonWithInvalidSystemTypes);

            Assert.AreEqual(E_ProrationT.CityTownTaxes, list.CityTownTaxes.ProrationType);
        }

        [Test]
        public void CountyTaxItem_SetToWrongTypeInJson_FixedAfterDeserialize()
        {
            var list = SerializationHelper.JsonNetDeserialize<ProrationList>(JsonWithInvalidSystemTypes);

            Assert.AreEqual(E_ProrationT.CountyTaxes, list.CountyTaxes.ProrationType);
        }

        [Test]
        public void AssessmentsItem_SetToWrongTypeInJson_FixedAfterDeserialize()
        {
            var list = SerializationHelper.JsonNetDeserialize<ProrationList>(JsonWithInvalidSystemTypes);

            Assert.AreEqual(E_ProrationT.Assessments, list.Assessments.ProrationType);
        }

        [Test]
        public void Equals_TwoDefaultInstances_IsTrue()
        {
            var l1 = new ProrationList();
            var l2 = new ProrationList();

            bool areEqual = l1.Equals(l2);

            Assert.AreEqual(true, areEqual);
        }

        [Test]
        public void Equals_DifferentCityTaxes_IsFalse()
        {
            var l1 = new ProrationList();
            l1.CityTownTaxes.Amount = 5;
            var l2 = new ProrationList();

            bool areEqual = l1.Equals(l2);

            Assert.AreEqual(false, areEqual);
        }

        [Test]
        public void Equals_DifferentCountyTaxes_IsFalse()
        {
            var l1 = new ProrationList();
            l1.CountyTaxes.Amount = 5;
            var l2 = new ProrationList();

            bool areEqual = l1.Equals(l2);

            Assert.AreEqual(false, areEqual);
        }

        [Test]
        public void Equals_DifferentAssessments_IsFalse()
        {
            var l1 = new ProrationList();
            l1.Assessments.Amount = 5;
            var l2 = new ProrationList();

            bool areEqual = l1.Equals(l2);

            Assert.AreEqual(false, areEqual);
        }

        [Test]
        public void Equals_OneWithExtraCustomItem_IsFalse()
        {
            var l1 = new ProrationList();
            var l2 = new ProrationList();

            l1.Add(new Proration());
            bool areEqual = l1.Equals(l2);

            Assert.AreEqual(false, areEqual);
        }

        [Test]
        public void Equals_EqualCustomItems_IsTrue()
        {
            var closingDate = CDateTime.Create(DefaultClosingDate);
            var l1 = new ProrationList(closingDate);
            var l2 = new ProrationList(closingDate);
            var proration1 = new Proration();
            var proration2 = new Proration();

            proration1.Description = proration2.Description = "desc";
            proration1.PaidThroughDate = proration2.PaidThroughDate = CDateTime.Create(new DateTime(2015, 4, 30));
            proration1.Amount = proration2.Amount = 1;
            l1.Add(proration1);
            l2.Add(proration2);
            bool areEqual = l1.Equals(l2);

            Assert.AreEqual(true, areEqual);
        }

        [Test]
        public void Equals_DifferentClosingDates_IsFalse()
        {
            var closingDate1= CDateTime.Create(DefaultClosingDate);
            var closingDate2= CDateTime.Create(closingDate1.DateTimeForComputation.AddDays(1));
            var l1 = new ProrationList(closingDate1);
            var l2 = new ProrationList(closingDate2);
            var proration1 = new Proration();
            var proration2 = new Proration();

            bool areEqual = l1.Equals(l2);

            Assert.AreEqual(false, areEqual);
        }

        private const string JsonWithInvalidSystemTypes = @"
            {
              ""ClosingDate_rep"": ""4/15/2015"",
              ""PopulateTo"": 0,
              ""CityTownTaxes"": {
                ""ProrationType"": 0,
                ""IsSystemProration"": true,
                ""IsPaidFromBorrowerToSeller"": false,
                ""IsPaidFromSellerToBorrower"": false,
                ""Description"": ""City/Town Taxes"",
                ""PaidThroughDate"": """",
                ""PaidFromDate"": """",
                ""PaidToDate"": """",
                ""Amount"": ""$0.00"",
                ""AmountPaidFromBorrowerToSeller"": ""$0.00"",
                ""AmountPaidFromSellerToBorrower"": ""$0.00"",
                ""PaidFromParty"": 0,
                ""PaidToParty"": 0,
                ""IsCustomFromBorrowerToSeller"": false,
                ""IsCustomFromSellerToBorrower"": false
              },
              ""CountyTaxes"": {
                ""ProrationType"": 0,
                ""IsSystemProration"": true,
                ""IsPaidFromBorrowerToSeller"": false,
                ""IsPaidFromSellerToBorrower"": false,
                ""Description"": ""County Taxes"",
                ""PaidThroughDate"": """",
                ""PaidFromDate"": """",
                ""PaidToDate"": """",
                ""Amount"": ""$0.00"",
                ""AmountPaidFromBorrowerToSeller"": ""$0.00"",
                ""AmountPaidFromSellerToBorrower"": ""$0.00"",
                ""PaidFromParty"": 0,
                ""PaidToParty"": 0,
                ""IsCustomFromBorrowerToSeller"": false,
                ""IsCustomFromSellerToBorrower"": false
              },
              ""Assessments"": {
                ""ProrationType"": 0,
                ""IsSystemProration"": true,
                ""IsPaidFromBorrowerToSeller"": false,
                ""IsPaidFromSellerToBorrower"": false,
                ""Description"": ""Assessments"",
                ""PaidThroughDate"": """",
                ""PaidFromDate"": """",
                ""PaidToDate"": """",
                ""Amount"": ""$0.00"",
                ""AmountPaidFromBorrowerToSeller"": ""$0.00"",
                ""AmountPaidFromSellerToBorrower"": ""$0.00"",
                ""PaidFromParty"": 0,
                ""PaidToParty"": 0,
                ""IsCustomFromBorrowerToSeller"": false,
                ""IsCustomFromSellerToBorrower"": false
              },
              ""CustomItems"": []
            }";

    }
}
