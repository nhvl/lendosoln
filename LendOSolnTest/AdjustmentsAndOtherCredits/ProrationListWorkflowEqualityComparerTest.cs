﻿namespace LendOSolnTest.AdjustmentsAndOtherCredits
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using NUnit.Framework;
    using LendersOffice.ObjLib.AdjustmentsAndOtherCredits;
    using DataAccess;

    [TestFixture]
    public class ProrationListWorkflowEqualityComparerTest
    {
        public readonly DateTime DefaultClosingDate = new DateTime(2015, 4, 15);

        [Test]
        public void Equals_TwoDefaultInstances_IsTrue()
        {
            var comparer = new ProrationListWorkflowEqualityComparer();
            var l1 = new ProrationList();
            var l2 = new ProrationList();

            bool areEqual = comparer.Equals(l1, l2);

            Assert.AreEqual(true, areEqual);
        }

        [Test]
        public void Equals_DifferentCityTaxes_IsFalse()
        {
            var comparer = new ProrationListWorkflowEqualityComparer();
            var l1 = new ProrationList();
            l1.CityTownTaxes.Amount = 5;
            var l2 = new ProrationList();

            bool areEqual = comparer.Equals(l1, l2);

            Assert.AreEqual(false, areEqual);
        }

        [Test]
        public void Equals_DifferentCountyTaxes_IsFalse()
        {
            var comparer = new ProrationListWorkflowEqualityComparer();
            var l1 = new ProrationList();
            l1.CountyTaxes.Amount = 5;
            var l2 = new ProrationList();

            bool areEqual = comparer.Equals(l1, l2);

            Assert.AreEqual(false, areEqual);
        }

        [Test]
        public void Equals_DifferentAssessments_IsFalse()
        {
            var comparer = new ProrationListWorkflowEqualityComparer();
            var l1 = new ProrationList();
            l1.Assessments.Amount = 5;
            var l2 = new ProrationList();

            bool areEqual = comparer.Equals(l1, l2);

            Assert.AreEqual(false, areEqual);
        }

        [Test]
        public void Equals_OneWithExtraCustomItem_IsFalse()
        {
            var comparer = new ProrationListWorkflowEqualityComparer();
            var l1 = new ProrationList();
            var l2 = new ProrationList();

            l1.Add(new Proration());
            bool areEqual = comparer.Equals(l1, l2);

            Assert.AreEqual(false, areEqual);
        }

        [Test]
        public void Equals_EqualCustomItems_IsTrue()
        {
            var comparer = new ProrationListWorkflowEqualityComparer();
            var closingDate = CDateTime.Create(DefaultClosingDate);
            var l1 = new ProrationList(closingDate);
            var l2 = new ProrationList(closingDate);
            var proration1 = new Proration();
            var proration2 = new Proration();

            proration1.Description = proration2.Description = "desc";
            proration1.PaidThroughDate = proration2.PaidThroughDate = CDateTime.Create(new DateTime(2015, 4, 30));
            proration1.Amount = proration2.Amount = 1;
            l1.Add(proration1);
            l2.Add(proration2);
            bool areEqual = comparer.Equals(l1, l2);

            Assert.AreEqual(true, areEqual);
        }
        [Test]
        public void Equals_DifferentClosingDates_IsTrue()
        {
            var comparer = new ProrationListWorkflowEqualityComparer();
            var closingDate1= CDateTime.Create(DefaultClosingDate);
            var closingDate2= CDateTime.Create(closingDate1.DateTimeForComputation.AddDays(1));
            var l1 = new ProrationList(closingDate1);
            var l2 = new ProrationList(closingDate2);
            var proration1 = new Proration();
            var proration2 = new Proration();

            bool areEqual = comparer.Equals(l1, l2);

            Assert.AreEqual(true, areEqual);
        }
    }
}
