﻿namespace LendOSolnTest.AdjustmentsAndOtherCredits
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using NUnit.Framework;
    using LendersOffice.ObjLib.AdjustmentsAndOtherCredits;
    using LendersOffice.Common;
    using DataAccess;

    [TestFixture]
    public class AdjustmentListTest
    {
        [Test]
        public void CreateList_NoManualAdditions_HasSellerCreditItem()
        {
            var list = new AdjustmentList();

            var numSellerCredits = list.Count(
                a => a.AdjustmentType == E_AdjustmentT.SellerCredit &&
                    a.PaidFromParty == E_PartyT.Seller && a.PaidToParty == E_PartyT.Borrower);

            Assert.AreEqual(1, numSellerCredits);
        }

        [Test]
        public void CreateList_SerializeWithJsonNet_DoesNotThrow()
        {
            var list = new AdjustmentList();

            SerializationHelper.JsonNetSerialize(list);
        }

        [Test]
        public void DefaultItems_SerializeDeserialize_NotDoubled()
        {
            var list = new AdjustmentList();
            var json = SerializationHelper.JsonNetSerialize(list);

            list = SerializationHelper.JsonNetDeserialize<AdjustmentList>(json);
            var numSellerCredits = list.Count(
                a => a.AdjustmentType == E_AdjustmentT.SellerCredit);

            Assert.AreEqual(1, numSellerCredits);
        }

        [Test]
        public void Ids_BeforeAndAfterSerDeser_AreTheSame()
        {
           var json = @"
                {
                    ""SellerCredit"": {
                    ""Description"": ""Invalid Description"",
                    ""IsSellerCredit"": true,
                    ""PaidFromParty"": 2,
                    ""IsPaidFromBorrower"": false,
                    ""IsPaidFromSeller"": true,
                    ""PaidToParty"": 1,
                    ""IsPaidToBorrower"": true,
                    ""IsPaidToSeller"": false,
                    ""Amount"": ""$0.00"",
                    ""BorrowerAmount"": ""$0.00"",
                    ""SellerAmount"": ""$0.00"",
                    ""PopulateTo"": 0
                  },
                  ""CustomItems"": []
                }";

            var list1 = SerializationHelper.JsonNetDeserialize<AdjustmentList>(json);
            list1.GenerateIdsOnSerialization = true;
            var json2 = SerializationHelper.JsonNetSerialize(list1);
            var list2 = SerializationHelper.JsonNetDeserialize<AdjustmentList>(json2);

            Assert.That(list1.SellerCredit.Id.HasValue);
            Assert.AreEqual(list1.SellerCredit.Id, list2.SellerCredit.Id);
        }


        [Test]
        public void SellerCredit_InvalidDescriptionSetInJson_FixedAfterDeserialize()
        {
            var json = @"
                {
                    ""SellerCredit"": {
                    ""Description"": ""Invalid Description"",
                    ""IsSellerCredit"": true,
                    ""PaidFromParty"": 2,
                    ""IsPaidFromBorrower"": false,
                    ""IsPaidFromSeller"": true,
                    ""PaidToParty"": 1,
                    ""IsPaidToBorrower"": true,
                    ""IsPaidToSeller"": false,
                    ""Amount"": ""$0.00"",
                    ""BorrowerAmount"": ""$0.00"",
                    ""SellerAmount"": ""$0.00"",
                    ""PopulateTo"": 0
                  },
                  ""CustomItems"": []
                }";

            var list = SerializationHelper.JsonNetDeserialize<AdjustmentList>(json);
            var sellerCredit = list.SellerCredit;

            StringAssert.AreEqualIgnoringCase(Adjustment.SellerCreditDesc, sellerCredit.Description);
        }

        [Test]
        public void SellerCredit_InvalidPaidFromSetInJson_FixedAfterDeserialize()
        {
            var json = @"
                {
                    ""SellerCredit"": {
                    ""Description"": ""Seller Credit"",
                    ""IsSellerCredit"": true,
                    ""PaidFromParty"": 0,
                    ""IsPaidFromBorrower"": false,
                    ""IsPaidFromSeller"": true,
                    ""PaidToParty"": 1,
                    ""IsPaidToBorrower"": true,
                    ""IsPaidToSeller"": false,
                    ""Amount"": ""$0.00"",
                    ""BorrowerAmount"": ""$0.00"",
                    ""SellerAmount"": ""$0.00"",
                    ""PopulateTo"": 0
                  },
                  ""CustomItems"": []
                }";

            var list = SerializationHelper.JsonNetDeserialize<AdjustmentList>(json);
            var sellerCredit = list.SellerCredit;

            Assert.AreEqual(E_PartyT.Seller, sellerCredit.PaidFromParty);
        }

        [Test]
        public void SellerCredit_InvalidPaidToSetInJson_FixedAfterDeserialize()
        {
            var json = @"
                {
                    ""SellerCredit"": {
                    ""Description"": ""Seller Credit"",
                    ""IsSellerCredit"": true,
                    ""PaidFromParty"": 2,
                    ""IsPaidFromBorrower"": false,
                    ""IsPaidFromSeller"": true,
                    ""PaidToParty"": 0,
                    ""IsPaidToBorrower"": true,
                    ""IsPaidToSeller"": false,
                    ""Amount"": ""$0.00"",
                    ""BorrowerAmount"": ""$0.00"",
                    ""SellerAmount"": ""$0.00"",
                    ""PopulateTo"": 0
                  },
                  ""CustomItems"": []
                }";

            var list = SerializationHelper.JsonNetDeserialize<AdjustmentList>(json);
            var sellerCredit = list.SellerCredit;

            Assert.AreEqual(E_PartyT.Borrower, sellerCredit.PaidToParty);
        }

        [Test]
        public void Equals_TwoDefaultLists_IsTrue()
        {
            var l1 = new AdjustmentList();
            var l2 = new AdjustmentList();

            bool areEqual = l1.Equals(l2);

            Assert.AreEqual(true, areEqual);
        }

        [Test]
        public void Equals_OneDifferentSellerCredits_IsFalse()
        {
            var l1 = new AdjustmentList();
            var l2 = new AdjustmentList();
            l1.SellerCredit.Amount = 5;

            bool areEqual = l1.Equals(l2);

            Assert.AreEqual(false, areEqual);
        }

        [Test]
        public void Equals_OneWithExtraCustomItem_IsFalse()
        {
            var l1 = new AdjustmentList();
            var l2 = new AdjustmentList();
            l2.Add(new Adjustment());

            bool areEqual = l1.Equals(l2);

            Assert.AreEqual(false, areEqual);
        }

        [Test]
        public void Equals_EqualCustomItems_IsTrue()
        {
            var l1 = new AdjustmentList();
            var l2 = new AdjustmentList();
            var adjustment1 = new Adjustment();
            var adjustment2 = new Adjustment();

            adjustment1.Description = adjustment2.Description = "Desc";
            adjustment1.PaidFromParty = adjustment2.PaidFromParty = E_PartyT.Borrower;
            adjustment1.PaidToParty = adjustment2.PaidToParty = E_PartyT.Seller;
            adjustment1.Amount = adjustment2.Amount = 15;
            l1.Add(adjustment1);
            l2.Add(adjustment2);
            bool areEqual = l1.Equals(l2);

            Assert.AreEqual(true, areEqual);
        }
    }
}
