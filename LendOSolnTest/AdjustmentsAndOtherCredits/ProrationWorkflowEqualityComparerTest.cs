﻿namespace LendOSolnTest.AdjustmentsAndOtherCredits
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using NUnit.Framework;
    using LendersOffice.ObjLib.AdjustmentsAndOtherCredits;
    using DataAccess;

    [TestFixture]
    public class ProrationWorkflowEqualityComparerTest
    {
        [Test]
        public void Equals_TwoDefaultInstances_IsTrue()
        {
            var comparer = new ProrationWorkflowEqualityComparer();
            var p1 = new Proration();
            var p2 = new Proration();

            bool areEqual = comparer.Equals(p1, p2);

            Assert.AreEqual(true, areEqual);
        }

        [Test]
        public void Equals_DifferentType_IsFalse()
        {
            var comparer = new ProrationWorkflowEqualityComparer();
            var p1 = new Proration(E_ProrationT.CityTownTaxes);
            var p2 = new Proration(E_ProrationT.CountyTaxes);

            bool areEqual = comparer.Equals(p1, p2);

            Assert.AreEqual(false, areEqual);
        }

        [Test]
        public void Equals_DifferentDescriptions_IsFalse()
        {
            var comparer = new ProrationWorkflowEqualityComparer();
            var p1 = new Proration();
            var p2 = new Proration();

            p1.Description = "desc1";
            p2.Description = "desc2";
            bool areEqual = comparer.Equals(p1, p2);

            Assert.AreEqual(false, areEqual);
        }

        [Test]
        public void Equals_DifferentPaidThroughDates_IsFalse()
        {
            var comparer = new ProrationWorkflowEqualityComparer();
            var p1 = new Proration();
            var p2 = new Proration();

            var dateString1 = "04/15/2015";
            var dateString2 = "04/17/2015";
            p1.PaidThroughDate = CDateTime.Create(dateString1, null);
            p2.PaidThroughDate = CDateTime.Create(dateString2, null);
            bool areEqual = comparer.Equals(p1, p2);

            Assert.AreEqual(false, areEqual);
        }

        [Test]
        public void Equals_DifferentAmounts_IsFalse()
        {
            var comparer = new ProrationWorkflowEqualityComparer();
            var p1 = new Proration();
            var p2 = new Proration();

            p1.Amount = 10;
            p2.Amount = 15;
            bool areEqual = comparer.Equals(p1, p2);

            Assert.AreEqual(false, areEqual);
        }

        [Test]
        public void Equals_DifferentClosingDates_IsTrue()
        {
            var comparer = new ProrationWorkflowEqualityComparer();
            var closingDate1 = CDateTime.Create("04/15/15", null);
            var closingDate2 = CDateTime.Create(closingDate1.DateTimeForComputation.AddDays(1));
            var p1 = new Proration();
            p1.ClosingDate = closingDate1;
            var p2 = new Proration();
            p2.ClosingDate = closingDate2;

            bool areEqual = comparer.Equals(p1, p2);

            Assert.AreEqual(true, areEqual);
        }

        [Test]
        public void Equals_FirstParamHasNullPaidThroughDate_IsFalse()
        {
            var comparer = new ProrationWorkflowEqualityComparer();
            var p1 = new Proration();
            p1.PaidThroughDate = null;
            var p2 = new Proration();

            bool areEqual = comparer.Equals(p1, p2);

            Assert.AreEqual(false, areEqual);
        }

        [Test]
        public void Equals_FirstParamWithNullPaidThroughDate_IsFalse()
        {
            var comparer = new ProrationWorkflowEqualityComparer();
            var p1 = new Proration();
            p1.PaidThroughDate = null;
            var p2 = new Proration();

            bool areEqual = comparer.Equals(p1, p2);

            Assert.AreEqual(false, areEqual);
        }

        [Test]
        public void Equals_ParamWithNullPaidThroughDate_IsFalse()
        {
            var comparer = new ProrationWorkflowEqualityComparer();
            var p1 = new Proration();
            var p2 = new Proration();
            p2.PaidThroughDate = null;

            bool areEqual = comparer.Equals(p1, p2);

            Assert.AreEqual(false, areEqual);
        }

        [Test]
        public void Equals_BothNullPaidThroughDate_IsTrue()
        {
            var comparer = new ProrationWorkflowEqualityComparer();
            var p1 = new Proration();
            p1.PaidThroughDate = null;
            var p2 = new Proration();
            p2.PaidThroughDate = null;

            bool areEqual = comparer.Equals(p1, p2);

            Assert.AreEqual(true, areEqual);
        }

        [Test]
        public void Equals_CallerWithNullClosingDate_IsTrue()
        {
            var comparer = new ProrationWorkflowEqualityComparer();
            var p1 = new Proration();
            p1.ClosingDate = null;
            var p2 = new Proration();

            bool areEqual = comparer.Equals(p1, p2);

            Assert.AreEqual(true, areEqual);
        }

        [Test]
        public void Equals_ParamWithNullClosingDate_IsTrue()
        {
            var comparer = new ProrationWorkflowEqualityComparer();
            var p1 = new Proration();
            var p2 = new Proration();
            p2.ClosingDate = null;

            bool areEqual = comparer.Equals(p1, p2);

            Assert.AreEqual(true, areEqual);
        }

        [Test]
        public void Equals_BothNullClosingDate_IsTrue()
        {
            var comparer = new ProrationWorkflowEqualityComparer();
            var p1 = new Proration();
            p1.ClosingDate = null;
            var p2 = new Proration();
            p2.ClosingDate = null;

            bool areEqual = comparer.Equals(p1, p2);

            Assert.AreEqual(true, areEqual);
        }
    }
}
