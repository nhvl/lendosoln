﻿namespace LendOSolnTest
{
    using System;
    using DataAccess;
    using LendOSolnTest.Common;
    using LqbGrammar.DataTypes;

    public enum TestFileType
    {
        RegularLoan,
        TestFile,
        UladLoan
    }

    /// <summary>
    /// Creates a temporary disposable loan in the database.  When this instance is disposed, the loan will be deleted and the adapters removed.
    /// </summary>
    public class LoanForIntegrationTest : IDisposable
    {
        private LoanForIntegrationTest(LendersOffice.Security.AbstractUserPrincipal principal, Guid loanId)
        {
            this.Principal = principal;
            this.Id = loanId;
            this.Identifier = DataObjectIdentifier<DataObjectKind.Loan, Guid>.Create(loanId);
        }

        public LendersOffice.Security.AbstractUserPrincipal Principal { get; }

        public Guid Id { get; }

        public DataObjectIdentifier<DataObjectKind.Loan, Guid> Identifier { get; }

        public static LoanForIntegrationTest Create(TestAccountType accountType, bool useUladDataLayer)
        {
            var type = useUladDataLayer ? TestFileType.UladLoan : TestFileType.RegularLoan;
            return Create(type, accountType);
        }

        public static LoanForIntegrationTest Create(TestFileType fileType, TestAccountType? accountType = null)
        {
            try
            {
                var principal = LoginTools.LoginAs(accountType ?? TestAccountType.LoTest001);
                var loanCreator = CLoanFileCreator.GetCreator(principal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);

                Guid loanId;
                switch (fileType)
                {
                    case TestFileType.RegularLoan:
                        loanId = loanCreator.CreateBlankLoanFile();
                        break;
                    case TestFileType.UladLoan:
                        loanId = loanCreator.CreateBlankUladLoanFile();
                        break;
                    case TestFileType.TestFile:
                        loanId = loanCreator.CreateNewTestFile();
                        break;
                    default:
                        throw new UnhandledEnumException(fileType);
                }

                return new LoanForIntegrationTest(principal, loanId);
            }
            catch
            {
                throw;
            }
        }

        public void Dispose()
        {
            try
            {
                if (this.Principal != null && this.Id != Guid.Empty)
                {
                    Tools.SystemDeclareLoanFileInvalid(this.Principal, this.Id, false, false);
                }
            }
            catch (Exception)
            {
            }
        }

        public CPageData CreateNewPageData() => CPageData.CreateUsingSmartDependency(this.Id, this.GetCallingType());

        public CFullAccessPageData CreateNewPageDataWithBypass() => CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.Id, this.GetCallingType());

        private Type GetCallingType()
        {
            Type thisType = this.GetType();
            var s = new System.Diagnostics.StackTrace();
            for (int i = 0; i < s.FrameCount; ++i)
            {
                Type foundType = s.GetFrame(i).GetMethod().ReflectedType;
                if (foundType != thisType)
                {
                    return foundType;
                }
            }

            return null;
        }
    }
}
