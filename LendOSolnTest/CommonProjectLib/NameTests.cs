﻿namespace LendOSolnTest.CommonProjectLib
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using CommonLib;
    using NUnit.Framework;

    /// <summary>
    /// Tests the name parsing within <see cref="CommonLib.Name"/>.
    /// While in no way comprehensive, the goal here is to get most of the common names covered.
    /// </summary>
    [TestFixture]
    public class NameTests
    {
        [Test]
        public void ParseName_FirstLast_FirstName()
        {
            Assert.AreEqual("JOHN", ParseName("John Testcase").FirstName);
        }
        [Test]
        public void ParseName_FirstLast_MiddleName()
        {
            Assert.AreEqual(string.Empty, ParseName("John Testcase").MiddleName);
        }
        [Test]
        public void ParseName_FirstLast_LastName()
        {
            Assert.AreEqual("TESTCASE", ParseName("John Testcase").LastName);
        }
        [Test]
        public void ParseName_FirstLast_Suffix()
        {
            Assert.AreEqual(string.Empty, ParseName("John Testcase").Suffix);
        }

        [Test]
        public void ParseName_FirstMiddleLast_FirstName()
        {
            Assert.AreEqual("JOHN", ParseName("John Michael Testcase").FirstName);
        }
        [Test]
        public void ParseName_FirstMiddleLast_MiddleName()
        {
            Assert.AreEqual("MICHAEL", ParseName("John Michael Testcase").MiddleName);
        }
        [Test]
        public void ParseName_FirstMiddleLast_LastName()
        {
            Assert.AreEqual("TESTCASE", ParseName("John Michael Testcase").LastName);
        }
        [Test]
        public void ParseName_FirstMiddleLast_Suffix()
        {
            Assert.AreEqual(string.Empty, ParseName("John Michael Testcase").Suffix);
        }


        [Test]
        public void ParseName_FirstMiddleLastSuffix_FirstName()
        {
            Assert.AreEqual("JOHN", ParseName("John Michael Testcase Jr").FirstName);
        }
        [Test]
        public void ParseName_FirstMiddleLastSuffix_MiddleName()
        {
            Assert.AreEqual("MICHAEL", ParseName("John Michael Testcase Jr").MiddleName);
        }
        [Test]
        public void ParseName_FirstMiddleLastSuffix_LastName()
        {
            Assert.AreEqual("TESTCASE", ParseName("John Michael Testcase Jr").LastName);
        }
        [Test]
        public void ParseName_FirstMiddleLastSuffix_Suffix()
        {
            Assert.AreEqual("JR", ParseName("John Michael Testcase Jr").Suffix);
        }

        [Test]
        public void ParseName_FirstMiddleLastCommaSuffix_FirstName()
        {
            Assert.AreEqual("JOHN", ParseName("John Michael Testcase, Jr").FirstName);
        }
        [Test]
        public void ParseName_FirstMiddleLastCommaSuffix_MiddleName()
        {
            Assert.AreEqual("MICHAEL", ParseName("John Michael Testcase, Jr").MiddleName);
        }
        [Test]
        public void ParseName_FirstMiddleLastCommaSuffix_LastName()
        {
            Assert.AreEqual("TESTCASE", ParseName("John Michael Testcase, Jr").LastName);
        }
        [Test]
        public void ParseName_FirstMiddleLastCommaSuffix_Suffix()
        {
            Assert.AreEqual("JR", ParseName("John Michael Testcase, Jr").Suffix);
        }

        [Test]
        public void ParseName_LastFirstMiddle_FirstName()
        {
            Assert.AreEqual("JOHN", ParseName("Testcase, John Michael").FirstName);
        }
        [Test]
        public void ParseName_LastFirstMiddle_MiddleName()
        {
            Assert.AreEqual("MICHAEL", ParseName("Testcase, John Michael").MiddleName);
        }
        [Test]
        public void ParseName_LastFirstMiddle_LastName()
        {
            Assert.AreEqual("TESTCASE", ParseName("Testcase, John Michael").LastName);
        }
        [Test]
        public void ParseName_LastFirstMiddle_Suffix()
        {
            Assert.AreEqual(string.Empty, ParseName("Testcase, John Michael").Suffix);
        }

        [Test]
        public void ParseName_LastNameEndsWithSuffixString_FirstName()
        {
            Assert.AreEqual("JOHN", ParseName("John Spaghetti").FirstName);
        }
        [Test]
        public void ParseName_LastNameEndsWithSuffixString_MiddleName()
        {
            Assert.AreEqual(string.Empty, ParseName("John Spaghetti").MiddleName);
        }
        [Test]
        public void ParseName_LastNameEndsWithSuffixString_LastName()
        {
            Assert.AreEqual("SPAGHETTI", ParseName("John Spaghetti").LastName);
        }
        [Test]
        public void ParseName_LastNameEndsWithSuffixString_Suffix()
        {
            Assert.AreEqual(string.Empty, ParseName("John Spaghetti").Suffix);
        }

        [Test]
        public void ParseName_SuffixedLastName_FirstName()
        {
            Assert.AreEqual("JOHN", ParseName("John Spaghett,i").FirstName);
        }
        [Test]
        public void ParseName_SuffixedLastName_MiddleName()
        {
            Assert.AreEqual(string.Empty, ParseName("John Spaghett,i").MiddleName);
        }
        [Test]
        public void ParseName_SuffixedLastName_LastName()
        {
            Assert.AreEqual("SPAGHETT", ParseName("John Spaghett,i").LastName);
        }
        [Test]
        public void ParseName_SuffixedLastName_Suffix()
        {
            Assert.AreEqual("I", ParseName("John Spaghett,i").Suffix);
        }

        private static Name ParseName(string name)
        {
            Name n = new Name();
            n.ParseName(name);
            return n;
        }
    }
}
