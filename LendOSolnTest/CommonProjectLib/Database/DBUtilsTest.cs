﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using CommonProjectLib.Logging;
using CommonProjectLib.Database;
using ConfigSystem.DataAccess;
using DataAccess;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data;

namespace LendOSolnTest.CommonProjectLib.Database
{
    [TestFixture]
    public class DBUtilsTest
    {
        [TestFixtureSetUp]
        public void Setup()
        {
        }

        [Test]
        public void ExecuteNoQuerySuccessTest()
        {
            int result = DBUtils.ExecuteNonQuery(SqlConnectionManager.Get(DbConnectionInfo.DefaultConnectionInfo), "WillPass", 0, new SqlParameter("@p1", 1), new SqlParameter("@p2", "asd"));
            Assert.That(result, Is.EqualTo(-1));
        }

        [Test]
        [ExpectedException]
        public void ExecuteNoQueryMissingParam()
        {
            DBUtils.ExecuteNonQuery(SqlConnectionManager.Get(DbConnectionInfo.DefaultConnectionInfo), "WillPass", 0, new SqlParameter("@p1", 1));

        }

        [Test]
        [ExpectedException]
        public void ExecuteNoQueryMissingParamWithRetries()
        {
            DBUtils.ExecuteNonQuery(SqlConnectionManager.Get(DbConnectionInfo.DefaultConnectionInfo), "WillPass", 3, new SqlParameter("@p1", 1));
        }

        [Test]
        public void ExecuteScalarPass()
        {
            object result = DBUtils.ExecuteScalar(SqlConnectionManager.Get(DbConnectionInfo.DefaultConnectionInfo), "WillPassWithResult", 
                new SqlParameter("@p1", 1),
                new SqlParameter("@p2", "varchar"));
            Assert.That(result, Is.EqualTo(1));
        }

        [Test]
        [ExpectedException]
        public void ExecuteScalarFail()
        {
            object result = DBUtils.ExecuteScalar(SqlConnectionManager.Get(DbConnectionInfo.DefaultConnectionInfo), "WillPassWithResult",
            new SqlParameter("@p1", 1));
        }

        [Test]
        public void ExecuteReaderSuccess()
        {
            using (DbDataReader reader = DBUtils.ExecuteReader(SqlConnectionManager.Get(DbConnectionInfo.DefaultConnectionInfo), "WillPassWithResult", new SqlParameter("@p1", 1),
                new SqlParameter("@p2", "varchar")))
            {
                Assert.That(reader.Read(), Is.True);
                Assert.That(reader[0], Is.EqualTo(1));
            }
        }



        [Test]
        [ExpectedException]
        public void ExecuteReaderFail()
        {
            using (DbDataReader reader = DBUtils.ExecuteReader(SqlConnectionManager.Get(DbConnectionInfo.DefaultConnectionInfo), "WillPassWithResult", new SqlParameter("@p1", 1)))
            {
                Assert.That(reader.Read(), Is.True);
                Assert.That(reader[0], Is.EqualTo(1));
            }
        }

        [Test]
        public void ExecuteReaderWithRetrySuccess()
        {
            using (DbDataReader reader = DBUtils.ExecuteReaderWithRetries(SqlConnectionManager.Get(DbConnectionInfo.DefaultConnectionInfo), "WillPassWithResult", 3, new SqlParameter("@p1", 1),
                new SqlParameter("@p2", "varchar")))
            {
                Assert.That(reader.Read(), Is.True);
                Assert.That(reader[0], Is.EqualTo(1));
            }
        }

        [Test]
        [ExpectedException]
        public void ExecuteReaderWithRetryFail()
        {
            using (DbDataReader reader = DBUtils.ExecuteReaderWithRetries(SqlConnectionManager.Get(DbConnectionInfo.DefaultConnectionInfo), "WillPassWithResult", 3, new SqlParameter("@p1", 1)))
            {
                Assert.That(reader.Read(), Is.True);
                Assert.That(reader[0], Is.EqualTo(1));
            }
        }

        [Test]
        [ExpectedException]
        public void ExecuteReaderWithRetryFailArg()
        {
            using (DbDataReader reader = DBUtils.ExecuteReaderWithRetries(SqlConnectionManager.Get(DbConnectionInfo.DefaultConnectionInfo), "WillPassWithResult", -1, new SqlParameter("@p1", 1), new SqlParameter("@p2", "asd")))
            {
     
            }
        }
    }
}
