﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using DataAccess;
using LendOSolnTest.Common;
using LendOSolnTest.Fakes;
using NUnit.Framework;

namespace LendOSolnTest.Rolodex
{
    [TestFixture]
    public class RolodexTest
    {
        [TestFixtureSetUp]
        public void Setup()
        {
            LoginTools.LoginAs(TestAccountType.LoTest001);
        }

        // Reasons encouraged.
        private static HashSet<string> KnownColumnsMissingFromLoan = new HashSet<string>()
        {
            "AgentN",             // Not available from CAgentFields.
            "AgentIsApproved",    // Not available from CAgentFields.
            "AgentComNmBranchNm", // Not needed for assigned loan agents. Used for contact entries tab due to large number of entries.
            "AgentIsAffiliated",   // Not directly in CAgentFields
            "SystemId", // This is a rolodex-specific ID field.
            "IsSettlementServiceProvider", // This is a rolodex-specific indicator used by the fee service.
            "TitleProviderCode" // This is only relevant at the rolodex level, we do not show it in the loan.
        };

        // What the data-layer returns is catered to what the stored procedure returns.
        // However, we do not remove any columns from the returned data set, and it contains
        // many more fields than are necessary.
        // Still thought it would be good for the check to go both ways.
        private static HashSet<string> KnownColumnsMissingFromStoredProcedure = new HashSet<string>()
        {
            "RecordId",
            "CaseNum",
            "EmployeeIDInCompany",
            "Notes",
            "OtherAgentRoleTDesc",
            "IsCommissionPaid",
            "PaymentNotes",
            "InvestorSoldDate",
            "InvestorBasisPoints",
            "IsListedInGFEProviderForm",
            "IsLenderAssociation",
            "IsLenderAffiliate",
            "IsLenderRelative",
            "HasLenderRelationship",
            "HasLenderAccountLast12Months",
            "IsUsedRepeatlyByLenderLast12Months",
            "ProviderItemNumber",
            "CompanyLendingLicenseXmlContent",
            "TaxId",
            "LoanOriginatorIdentifier",
            "CompanyLoanOriginatorIdentifier",
            "EmployeeId",
            "AgentSourceT",
            "ChumsId",
            "IsLender", 
            "IsOriginator", 
            "IsOriginatorAffiliate"
        };

        /// <summary>
        /// Verify that the data set returned for the agents stored on the file is
        /// in sync with what is returned by the ListRolodexByBrokerID stored
        /// procedure. If it isn't, we will just log a warning as this may signal
        /// that we want to update the GetAgentDataSetForRolodex or the stored
        /// procedure.
        /// </summary>
        [Test]
        public void EnsureCPageBaseInSyncWithStoredProcedure()
        {
            var dataLoan = FakePageData.Create();
            dataLoan.InitLoad();

            var agentDataSetForRolodex = dataLoan.GetAgentDataSetForRolodex(null, null);
            var columnNamesFromLoan = agentDataSetForRolodex.Tables[0].Columns
                .Cast<DataColumn>()
                .Select(column => column.ColumnName);

            var storedProcedureDataSet = new DataSet();

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerID", dataLoan.sBrokerId)
                                            , new SqlParameter("@NameFilter", "")
                                            , new SqlParameter("@TypeFilter", "")
                                            , new SqlParameter("@IsApprovedFilter", true)
                                        };

			DataSetHelper.Fill(storedProcedureDataSet, dataLoan.sBrokerId, "ListRolodexByBrokerID", parameters);

            var columnNamesFromStoredProcedure = storedProcedureDataSet.Tables[0].Columns
                .Cast<DataColumn>()
                .Select(column => column.ColumnName);

            var fieldsMissingFromLoan = columnNamesFromStoredProcedure.Except(columnNamesFromLoan)
                .Except(KnownColumnsMissingFromLoan);
            var fieldsMissingFromLoanStr = "";
            if (fieldsMissingFromLoan.Count() > 0)
            {
                fieldsMissingFromLoanStr = "The following fields were returned by the stored procedure "
                    + "ListRolodexByBrokerID but are not included when retrieving agent data from the loan. "
                    + "Please consider if they need to be added to CPageBase::GetAgentDataSetForRolodex. "
                    + "If not, please add to the KnownColumnsMissingFromLoan in RolodexTest:\r\n"
                    + string.Join(", ", fieldsMissingFromLoan.ToArray());
            }

            var fieldsMissingFromStoredProcedure = columnNamesFromLoan.Except(columnNamesFromStoredProcedure)
                .Except(KnownColumnsMissingFromStoredProcedure);
            var fieldsMissingFromStoredProcedureStr = "";

            if (fieldsMissingFromStoredProcedure.Count() > 0)
            {
                fieldsMissingFromStoredProcedureStr = "The following fields were returned by CPageBase::GetAgentDataSetForRolodex "
                    + "but are not included in the result set of the related stored procedure. "
                    + "Please consider if they need to be added to ListRolodexByBrokerID. "
                    + "If not, please add to KnownColumnsMissingFromStoredProcedure in RolodexTest:\r\n"
                    + string.Join(", ", fieldsMissingFromStoredProcedure.ToArray());
            }

            if (fieldsMissingFromLoanStr.Length > 0 || fieldsMissingFromStoredProcedureStr.Length > 0)
            {
                Assert.Ignore(fieldsMissingFromLoanStr + "\r\n" + fieldsMissingFromStoredProcedureStr);
            }


        }
    }
}
