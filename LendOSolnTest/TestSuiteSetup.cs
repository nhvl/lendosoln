﻿
using LqbGrammar;
using LqbGrammar.Exceptions;
using NUnit.Framework;

/// <summary>
/// This code will be executed once prior to any other unit test code because it is a setup
/// fixture in the global namespace.
/// </summary>
[SetUpFixture]
public class TestSuiteSetup
{
    /// <summary>
    /// DB ad hoc queries pass through the FOOL architectural framework, so factories need to be registered.
    /// For now we'll use the full registration.  In the future we are going to want to introduce mocks as
    /// needed and we'll need to revisit how to handle the startup code in this scenario.
    /// </summary>
    [SetUp]
    public void InitializeApplicationFramework()
    {
        // The VN team does unit testing by calling webservers in the LendersOfficeApp website, which is
        // already initialized.  To be successful, SetUpFixture and SetUp methods should be called
        // correctly.  Assuming that happens then this method will be called and attempt to
        // re-initialize.  To protect against this, we have to test whether the returned application
        // initializer is null, which means the application has already been initialized.
        IApplicationInitialize iAppInit = null;
        try
        {
            IExceptionHandlerFactory[] arrHandlers = ExceptionHandlerUtil.GetCoreExceptionHandlerFactories();
            iAppInit = LqbApplication.CreateInitializer(ApplicationType.UnitTest, arrHandlers);
            if (iAppInit != null)
            {
                const string AppName = "LendOSolnTest";
                iAppInit.SetName(AppName);

                Adapter.ApplicationInitializer.RegisterFactories(AppName, iAppInit);
                LendersOffice.ApplicationInitializer.RegisterFactories(AppName, iAppInit);
            }
        }
        finally
        {
            if (iAppInit != null)
            {
                iAppInit.Dispose();
                iAppInit = null;
            }
        }
    }
}
