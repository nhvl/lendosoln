﻿namespace LendOSolnTest
{
    using LendersOffice.Common;
    using NUnit.Framework;

    [TestFixture]
    public class LineEndingTest
    {

        public static string GetTestString(string lineEnding)
        {
            return "I'm a line." + lineEnding
                + "I'm a new line." + lineEnding
                + "I'm a new line." + lineEnding;
        }

        public string CrLfString = GetTestString("\r\n");
        public string LfCrString = GetTestString("\n\r");
        public string CrString = GetTestString("\r");
        public string RfString = GetTestString("\n");

        [Test]
        public void CrLfEqualityTest()
        {
            Assert.AreEqual(CrLfString.NormalizeNewLines(), CrLfString);
        }

        [Test]
        public void LfCrEqualityTest()
        {
            Assert.AreEqual(LfCrString.NormalizeNewLines(), CrLfString);
        }

        [Test]
        public void CrEqualityTest()
        {
            Assert.AreEqual(CrString.NormalizeNewLines(), CrLfString);              
        }

        [Test]
        public void LfEqualityTest()
        {
            Assert.AreEqual(RfString.NormalizeNewLines(), CrLfString);              
        }
    }
}
