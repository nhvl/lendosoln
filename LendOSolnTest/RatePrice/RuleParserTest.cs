﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using LendersOffice.RatePrice.Helpers;
using DataAccess;
using LendersOfficeApp.los.RatePrice;

namespace LendOSolnTest.RatePrice
{
    [TestFixture]
    public class RuleParserTest
    {
        [Test]
        public void NoneFlexRuleTest()
        {
            string simpleRule = "RULE\t\tNonFlex Rule\tLTV > 80\tX\t\t6\t7\t8\t9\t10\t11\t12\tstip-13\t14\t\t16\t17\t18\t19\t20f7b9bb-a4e6-472e-9ad1-7a40c78fb116\tX\t22";
            ParsedRuleInfo expectedRule = new ParsedRuleInfo
            {
                Description = "NonFlex Rule",
                Condition = "LTV > 80",
                sSkip = "X",
                sDisqualify = "",
                Rate = "6",
                Fees = "7",
                Margin = "8",
                QRateAdj = "9",
                TeaserRate = "10",
                MaxBackEndYsp = "11",
                MaxFrontEndYsp = "12",
                Stip = "stip-13",
                MaxDTI = "14",
                QScore = "16",
                QLTV = "17",
                QCLTV = "18",
                QLAmt = "19",
                RuleId = new Guid("20f7b9bb-a4e6-472e-9ad1-7a40c78fb116"),
                sLenderRule = "X",
                LockDaysAdj = "22"
            };

            AreEqual(expectedRule, simpleRule);
        }

        [Test]
        public void FlexRuleTest()
        {
            string flexRule = "RULE\t\tFlex Rule\tLTV > 85\tX\t\t6\t7\t8\t11\t12\tstip-13\t14\t\"QRATE = 9 ; TRATE = 10 ; QSCORE = 16 ; QLTV = 17 ; QCLTV += 18 ; QLAMT += 19\"\t978e3fa0-ea36-4c8a-a91f-1793cf99483a\t\t22";
            ParsedRuleInfo expectedRule = new ParsedRuleInfo
            {
                Description = "Flex Rule",
                Condition = "LTV > 85",
                sSkip = "X",
                sDisqualify = "",
                Rate = "6",
                Fees = "7",
                Margin = "8",
                QRateAdj = "9",
                TeaserRate = "10",
                MaxBackEndYsp = "11",
                MaxFrontEndYsp = "12",
                Stip = "stip-13",
                MaxDTI = "14",
                QScore = "16",
                QLTV = "17",
                QCLTV = "18",
                QLAmt = "19",
                RuleId = new Guid("978e3fa0-ea36-4c8a-a91f-1793cf99483a"),
                sLenderRule = "",
                LockDaysAdj = "22"
            };

            AreEqual(expectedRule, flexRule);

            StringAssert.Contains("QRATE = 9 ; TRATE = 10 ;", flexRule);
            flexRule = flexRule.Replace("QRATE = 9 ; TRATE = 10 ;", "QRATE , TRATE = 120 ;");
            expectedRule.QRateAdj = "120";
            expectedRule.TeaserRate = "120";
            AreEqual(expectedRule, flexRule);
        }


        [Test]
        public void NoneFlexDefaultRuleTest()
        {
            ParsedRuleInfo expectedRule = DefaultTestRule;
            AreEqual(expectedRule, RuleToString(expectedRule, useFlexFormat: false));
        }

        [Test]
        public void FlexDefaultRuleTest()
        {
            ParsedRuleInfo expectedRule = DefaultTestRule;
            string flexRuleString = RuleToString(expectedRule, useFlexFormat: true);

            StringAssert.Contains("QRATE = 9 ;", flexRuleString);
            StringAssert.Contains("QSCORE = 16 ;", flexRuleString);
            AreEqual(expectedRule, flexRuleString);

            // test operator ' += '
            StringAssert.Contains("QLTV = 17 ;", flexRuleString);
            flexRuleString = flexRuleString.Replace("QLTV = 17 ;", "QLTV += 17 ;");
            AreEqual(expectedRule, flexRuleString);
        }

        [Test]
        public void AssigneOneValueToMultipleVars_FlexRuleTest()
        {
            ParsedRuleInfo rule = DefaultTestRule;

            rule.QRateAdj = rule.TeaserRate = rule.QScore = "123";
            rule.QLTV = rule.QCLTV = "100";
            string ruleString = RuleToString(rule, useFlexFormat: true);

            StringAssert.Contains("QRATE , QSCORE , TRATE = 123", ruleString);
            StringAssert.Contains("QCLTV , QLTV = 100", ruleString);
            AreEqual(rule, ruleString);

            //Test operator ' += '
            ruleString = ruleString.Replace("QCLTV , QLTV = 100", "QCLTV , QLTV += 100");
            AreEqual(rule, ruleString);

        }

        ParsedRuleInfo DefaultTestRule //create new instance with default values.
        {
            get
            {
                return new ParsedRuleInfo
                {
                    Description = "Test Rule",
                    Condition = "LTV > 80",
                    sSkip = "X",
                    sDisqualify = "",
                    Rate = "6",
                    Fees = "7",
                    Margin = "8",
                    QRateAdj = "9",
                    TeaserRate = "10",
                    MaxBackEndYsp = "11",
                    MaxFrontEndYsp = "12",
                    Stip = "stip-13",
                    MaxDTI = "14",
                    QScore = "16",
                    QLTV = "17",
                    QCLTV = "18",
                    QLAmt = "19",
                    RuleId = Guid.NewGuid(),
                    sLenderRule = "X",
                    LockDaysAdj = "22"
                };
            }
        }

        void AreEqual(ParsedRuleInfo expected, ParsedRuleInfo actual)
        {
            Assert.AreEqual(expected.Description, actual.Description, "Description");
            Assert.AreEqual(expected.Condition, actual.Condition, "Condition");
            Assert.AreEqual(expected.sSkip, actual.sSkip, "sSkip");
            Assert.AreEqual(expected.Disqualify, actual.Disqualify, "Disqualify");
            Assert.AreEqual(expected.Rate, actual.Rate, "Rate");
            Assert.AreEqual(expected.Fees, actual.Fees, "Fees");
            Assert.AreEqual(expected.Margin, actual.Margin, "Fees");
            Assert.AreEqual(expected.QRateAdj, actual.QRateAdj, "QRateAdj");
            Assert.AreEqual(expected.TeaserRate, actual.TeaserRate, "TeaserRate");
            Assert.AreEqual(expected.MaxBackEndYsp, actual.MaxBackEndYsp, "MaxBackEndYsp");
            Assert.AreEqual(expected.MaxFrontEndYsp, actual.MaxFrontEndYsp, "MaxFrontEndYsp");
            Assert.AreEqual(expected.Stip, actual.Stip, "Stip");
            Assert.AreEqual(expected.MaxDTI, actual.MaxDTI, "MaxDTI");
            Assert.AreEqual(expected.QScore, actual.QScore, "QScore");
            Assert.AreEqual(expected.QLTV, actual.QLTV, "QLTV");
            Assert.AreEqual(expected.QCLTV, actual.QCLTV, "QCLTV");
            Assert.AreEqual(expected.QLAmt, actual.QLAmt, "QLAmt");
            Assert.AreEqual(expected.sLenderRule, actual.sLenderRule, "sLenderRule");
            Assert.AreEqual(expected.LockDaysAdj, actual.LockDaysAdj, "LockDaysAdj");
            Assert.AreEqual(expected.QBC, actual.QBC, "QBC");
            Assert.AreEqual(expected.RuleId, actual.RuleId, "RuleId");
        }

        void AreEqual(ParsedRuleInfo expected, string ruleString)
        {
            ParsedRuleInfo actualRule = ParserRule(ruleString, new List<string>() /*errorList*/ );
            AreEqual(expected, actualRule);
        }

        //Helper methods
        string RuleToString(ParsedRuleInfo rule, bool useFlexFormat = false)
        {
            string commonPrefix = $"RULE\t\t{rule.Description}\t{rule.Condition}\t{rule.sSkip}\t{rule.sDisqualify}\t{rule.Rate}\t{rule.Fees}\t{rule.Margin}";
            if (!useFlexFormat)
            {
                return commonPrefix + $"\t{rule.QRateAdj}\t{rule.TeaserRate}\t{rule.MaxBackEndYsp}\t{rule.MaxFrontEndYsp}\t{rule.Stip}\t{rule.MaxDTI}"
                       + $"\t\t{rule.QScore}\t{rule.QLTV}\t{rule.QCLTV}\t{rule.QLAmt}\t{rule.RuleId}\t{rule.sLenderRule}\t{rule.LockDaysAdj}";
            }

            Dictionary<string, List<string>> valueToVars = new Dictionary<string, List<string>>();
            Action<string, string> addVar = (string varName, string value) =>
            {
                if (string.IsNullOrEmpty(value))
                {
                    return;
                }

                List<string> vars = null;
                if (valueToVars.TryGetValue(value, out vars) == false)
                {
                    valueToVars.Add(value, vars = new List<string>());
                }

                vars.Add(varName);

            };

            addVar("QRATE", rule.QRateAdj);
            addVar("TRATE", rule.TeaserRate);
            addVar("QSCORE", rule.QScore);
            addVar("QLTV", rule.QLTV);
            addVar("QCLTV", rule.QCLTV);
            addVar("QLAMT", rule.QLAmt);

            StringBuilder sb = new StringBuilder();
            foreach (var entry in valueToVars)
            {
                if (sb.Length > 0)
                {
                    sb.Append(" ; ");
                }

                entry.Value.Sort();
                sb.Append(string.Join(" , ", entry.Value)).Append(" = ").Append(entry.Key);
            }

            string flex = sb.ToString();
            return commonPrefix + $"\t{rule.MaxBackEndYsp}\t{rule.MaxFrontEndYsp}\t{rule.Stip}\t{rule.MaxDTI}"
                       + $"\t\"{flex}\"\t{rule.RuleId}\t{rule.sLenderRule}\t{rule.LockDaysAdj}";
        }

        List<ParsedPolicyInfo> LoadPolicies(string data, List<string> errorList, bool assertEnable = true)
        {
            errorList.Clear();
            using (var sr = new StreamReader(new MemoryStream(Encoding.UTF8.GetBytes(data))))
            {
                PolicyParser pp = new PolicyParser();

                var parsedPolicyList = pp.GetPolicies(sr, '\t', errorList, null);
                if (assertEnable)
                {
                    Assert.AreEqual(0, errorList.Count, "The PolicyParser's error list:\r\n" + string.Join("\r\n", errorList)); // parser is ok
                }
                return parsedPolicyList?.ToList();
            }

        }

        ParsedRuleInfo ParserRule(string ruleData, List<string> errorList, bool assertEnable = true)
        {
            //NoFlex:
            //      TYPE,QBC,DESCRIPTION,POLICYID/CONDITION,AT/SKP,GRD/DIS,RATE,FEES,MARGIN,QRATE,TRATE,BACK-END MAX YSP,FRONT-END MAX YSP,STIPS,MAXDTI,DTI RANGE,QSCORE,QLTV,QCLTV,QLAMT,RULEID,IS LENDER RULE,LOCK DAYS
            //Flex
            //      TYPE,QBC,DESCRIPTION,POLICYID/CONDITION,AT/SKP,GRD/DIS,RATE,FEES,MARGIN,BACK-END MAX YSP,FRONT-END MAX YSP,STIPS,MAXDTI,FLEX,RULEID,IS LENDER RULE,LOCK DAYS,

            string policyHeader = "POLICY,,Foo pocily,fe640802-f422-42fd-a7eb-a75938dfba89,,,,,,,,,,,,,,,,,,,".Replace(",", "\t");

            var parsedPolicyList = LoadPolicies(policyHeader + "\r\n" + ruleData, errorList, assertEnable);

            if (errorList.Count > 0 && !assertEnable)
            {
                return null;
            }

            Assert.AreEqual(1, parsedPolicyList.Count, "parsedPolicyList.Count");
            Assert.IsNotNull(parsedPolicyList[0].GetRules(), "Expect: parsedPolicyList[0].GetRules() is not null");

            List<ParsedRuleInfo> parsedRules = parsedPolicyList[0].GetRules().ToList();
            Assert.AreEqual(1, parsedRules.Count, "parsedRules.Count");
            return parsedRules[0];
        }
    }

    [TestFixture]
    public class FlexParserTest
    {
        [Test]
        public void FlexVariablesTest()
        {
            var dict = PolicyParser.FlexParse("QRATE = 1 ; TRATE = 2 ; QSCORE = 3 ; QLTV += 4 ; QCLTV += 5 ; QLAMT += 6");
            Assert.AreEqual(dict[CAdjustTarget.E_Target.QrateAdjust], "1");
            Assert.AreEqual(dict[CAdjustTarget.E_Target.TeaserRateAdjust], "2");
            Assert.AreEqual(dict[CAdjustTarget.E_Target.Qscore], "3");
            Assert.AreEqual(dict[CAdjustTarget.E_Target.Qltv], "4");
            Assert.AreEqual(dict[CAdjustTarget.E_Target.Qcltv], "5");
            Assert.AreEqual(dict[CAdjustTarget.E_Target.QLAmt], "6");
            Assert.AreEqual(6, dict.Count);
        }

        [Test]
        public void AssignOneValueToMultipleKeyTest()
        {
            string val = "((TLAMT - LAMT) / PropertyValue) * 100 + 20";
            var dict = PolicyParser.FlexParse($"QRATE , TRATE , QSCORE = {val} ; QCLTV , QLAMT += 6");
            Assert.AreEqual(dict[CAdjustTarget.E_Target.QrateAdjust], val);
            Assert.AreEqual(dict[CAdjustTarget.E_Target.TeaserRateAdjust], val);
            Assert.AreEqual(dict[CAdjustTarget.E_Target.Qscore], val);
            Assert.AreEqual(dict[CAdjustTarget.E_Target.Qcltv], "6");
            Assert.AreEqual(dict[CAdjustTarget.E_Target.QLAmt], "6");
            Assert.AreEqual(5, dict.Count);
        }

        [Test]
        public void ReassignVarTest()
        {
            Dictionary<CAdjustTarget.E_Target, string> result = null;
            string errMsg;

            FlexError err = PolicyParser.FlexTryParse("QRATE , TRATE , QSCORE = 1 ; QSCORE = 5", out result, out errMsg);
            Assert.AreEqual(FlexError.AlreadySet, err, errMsg);
        }

        [Test]
        public void InvalidVarTest()
        {
            Dictionary<CAdjustTarget.E_Target, string> result = null;
            string errMsg;

            FlexError err = PolicyParser.FlexTryParse("QRATE = 1 ; RATE = 2", out result, out errMsg);
            Assert.AreEqual(FlexError.InvalidVar, err, errMsg);
        }
        
        [Test]
        public void InvalidCommaTest()
        {
            Dictionary<CAdjustTarget.E_Target, string> result = null;
            string errMsg;

            string[] arrExpr =
            {
                "QRATE, TRATE = 10",            //problem QRATE,
                "QRATE ,TRATE = 10",            //problem ,TRATE
                "QRATE ,    ,  TRATE = 10",     //problem ",    ,"
                ", QRATE , TRATE = 10",
                "QRATE = 10 ,",
                "QRATE = 10 , ",

            };

            foreach( var expr in arrExpr)
            {
                FlexError err = PolicyParser.FlexTryParse(expr, out result, out errMsg);
                Assert.AreEqual(FlexError.InvalidComma, err, errMsg);


                err = PolicyParser.FlexTryParse(expr.Replace("=", "+="), out result, out errMsg);
                Assert.AreEqual(FlexError.InvalidComma, err, errMsg);
            }
        }

        [Test]
        public void MissingVarTest()
        {
            Dictionary<CAdjustTarget.E_Target, string> result = null;
            string errMsg;

            string[] arrExpr =
            {
                "QRATE = 10 ,  = 20",   // problem "; = 20"
                "QRATE = 10 ; = 20",
                " = 20",

            };

            foreach (var expr in arrExpr)
            {
                FlexError err = PolicyParser.FlexTryParse(expr, out result, out errMsg);
                Assert.AreEqual(FlexError.MissingVar, err, errMsg);


                err = PolicyParser.FlexTryParse(expr.Replace("=", "+="), out result, out errMsg);
                Assert.AreEqual(FlexError.MissingVar, err, errMsg);
            }
        }


        [Test]
        public void InvalidSemicolonTest()
        {
            Dictionary<CAdjustTarget.E_Target, string> result = null;
            string errMsg;

            string[] arrExpr =
            {
                "QRATE = 10; TRATE = 10",       // problem 10;
                "QRATE = 10 ;TRATE = 10",       // problem ;TRATE
                "QRATE = 10 ;    ; TRATE = 10", // problem " ;    ;"
                "; QRATE = 10",                   
                "QRATE = 10 ;",

            };

            foreach (var expr in arrExpr)
            {
                FlexError err = PolicyParser.FlexTryParse(expr, out result, out errMsg);
                Assert.AreEqual(FlexError.InvalidSemicolon, err, errMsg);

                err = PolicyParser.FlexTryParse(expr.Replace("=", "+="), out result, out errMsg);
                Assert.AreEqual(FlexError.InvalidSemicolon, err, errMsg);
            }
        }

        [Test]
        public void MissingOperatorTest()
        {
            Dictionary<CAdjustTarget.E_Target, string> result = null;
            string errMsg;

            string[] arrExpr =
            {
                "QRATE= 10",       // QRATE=
                "QRATE =10",       // problem =10
            };

            foreach (var expr in arrExpr)
            {
                FlexError err = PolicyParser.FlexTryParse(expr, out result, out errMsg);
                Assert.AreEqual(FlexError.MissingOperator, err, errMsg);

                err = PolicyParser.FlexTryParse(expr.Replace("=", "+="), out result, out errMsg);
                Assert.AreEqual(FlexError.MissingOperator, err, errMsg);
            }
        }

    }
}