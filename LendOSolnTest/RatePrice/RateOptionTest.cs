﻿using System;

using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using LendersOfficeApp.los.RatePrice;
using DataAccess;


namespace LendOSolnTest.RatePrice
{
    [TestFixture]
    public class RateOptionTest
    {
        private List<CRateSheetGroup> m_rateGroupList = null;
        private string m_currentTest = string.Empty;

        // OPM 2950 Unit Test
        // 06/26/09 mf.
        [Test]
        public void RateSheetGroupVerify()
        {
            TestCompleteRateGroupList();
            TestCompleteIORateGroupList();
            TestGapRateGroupList();
            TestSingleGroupList();
        }

        private void TestCompleteRateGroupList()
        {
            m_currentTest = "CompleteList";
            // Complete rate option group set that is complete and continious
            // across Lock 0-45, PP 0-120, IO 0-60
            var rateSheetGroups = new[] {
                new { MinLock = 0, MaxLock = 15,  MinPp = 0, MaxPp = 60, MinIo = 0, MaxIo = 0 }
                , new { MinLock = 0, MaxLock = 15,  MinPp = 0, MaxPp = 60, MinIo = 1, MaxIo = 60 }
                , new { MinLock = 16, MaxLock = 30,  MinPp = 0, MaxPp = 60, MinIo = 0, MaxIo = 0 }
                , new { MinLock = 16, MaxLock = 30,  MinPp = 0, MaxPp = 60, MinIo = 1, MaxIo = 60 }
                , new { MinLock = 31, MaxLock = 45,  MinPp = 0, MaxPp = 60, MinIo = 0, MaxIo = 0 }
                , new { MinLock = 31, MaxLock = 45,  MinPp = 0, MaxPp = 60, MinIo = 1, MaxIo = 60 }
                , new { MinLock = 0, MaxLock = 15,  MinPp = 61, MaxPp = 120, MinIo = 0, MaxIo = 0 }
                , new { MinLock = 0, MaxLock = 15,  MinPp = 61, MaxPp = 120, MinIo = 1, MaxIo = 60 }
                , new { MinLock = 16, MaxLock = 30,  MinPp = 61, MaxPp = 120, MinIo = 0, MaxIo = 0 }
                , new { MinLock = 16, MaxLock = 30,  MinPp = 61, MaxPp = 120, MinIo = 1, MaxIo = 60 }
                , new { MinLock = 31, MaxLock = 45,  MinPp = 61, MaxPp = 120, MinIo = 0, MaxIo = 0 }
                , new { MinLock = 31, MaxLock = 45,  MinPp = 61, MaxPp = 120, MinIo = 1, MaxIo = 60 }
            };

            m_rateGroupList = new List<CRateSheetGroup>(rateSheetGroups.Length);
            foreach (var o in rateSheetGroups)
            {
                m_rateGroupList.Add(new CRateSheetGroup(o.MinLock, o.MaxLock, o.MinPp, o.MaxPp, o.MinIo, o.MaxIo));
            }

            RateGroupAssert(1, 1, 1, true);
            RateGroupAssert(0, 0, 0, true);
            RateGroupAssert(60, 0, 0, false, CRateSheetGroup.NO_RATES_FOR_LOCK);
            RateGroupAssert(0, 132, 0, false, CRateSheetGroup.NO_RATES_FOR_PP);
            RateGroupAssert(0, 0, 70, false, CRateSheetGroup.NO_IO_DISQUAL_MSG);
        }

        private void TestCompleteIORateGroupList()
        {
            m_currentTest = "CompleteListIO";

            // Complete rate option group set that is complete and continious
            // across Lock 0-45, PP 0-120, IO 1-60
            var rateGroups = new[] {
                 new { MinLock = 0, MaxLock = 15,  MinPp = 0, MaxPp = 60, MinIo = 1, MaxIo = 60 }
                , new { MinLock = 16, MaxLock = 30,  MinPp = 0, MaxPp = 60, MinIo = 1, MaxIo = 60 }
                , new { MinLock = 31, MaxLock = 45,  MinPp = 0, MaxPp = 60, MinIo = 1, MaxIo = 60 }
                , new { MinLock = 0, MaxLock = 15,  MinPp = 61, MaxPp = 120, MinIo = 1, MaxIo = 60 }
                , new { MinLock = 16, MaxLock = 30,  MinPp = 61, MaxPp = 120, MinIo = 1, MaxIo = 60 }
                , new { MinLock = 31, MaxLock = 45,  MinPp = 61, MaxPp = 120, MinIo = 1, MaxIo = 60 }
            };

            m_rateGroupList = new List<CRateSheetGroup>(rateGroups.Length);
            foreach (var o in rateGroups)
            {
                m_rateGroupList.Add(new CRateSheetGroup(o.MinLock, o.MaxLock, o.MinPp, o.MaxPp, o.MinIo, o.MaxIo));
            }

            RateGroupAssert(1, 1, 1, true);
            RateGroupAssert(0, 0, 60, true);
            RateGroupAssert(0, 0, 0, false, CRateSheetGroup.IO_DISQUAL_MSG);
            RateGroupAssert(60, 0, 0, false, CRateSheetGroup.IO_DISQUAL_MSG);
            RateGroupAssert(0, 132, 0, false, CRateSheetGroup.IO_DISQUAL_MSG);
        }

        private void TestGapRateGroupList()
        {
            m_currentTest = "GapList";
            // Single Segment missing
            var rateSheetGroups = new[] {
                new { MinLock = 0, MaxLock = 15, MinPp = 0, MaxPp = 60, MinIo = 0, MaxIo = 0 }
                , new { MinLock = 0, MaxLock = 15,  MinPp = 0, MaxPp = 60, MinIo = 1, MaxIo = 60 }
                , new { MinLock = 16, MaxLock = 30,  MinPp = 0, MaxPp = 60, MinIo = 0, MaxIo = 0 }
                //, new { MinLock = 16, MaxLock = 30,  MinPp = 0, MaxPp = 60, MinIo = 1, MaxIo = 60 }
                , new { MinLock = 31, MaxLock = 45,  MinPp = 0, MaxPp = 60, MinIo = 0, MaxIo = 0 }
                , new { MinLock = 31, MaxLock = 45,  MinPp = 0, MaxPp = 60, MinIo = 1, MaxIo = 60 }
                , new { MinLock = 0, MaxLock = 15,  MinPp = 61, MaxPp = 120, MinIo = 0, MaxIo = 0 }
                , new { MinLock = 0, MaxLock = 15,  MinPp = 61, MaxPp = 120, MinIo = 1, MaxIo = 60 }
                , new { MinLock = 16, MaxLock = 30,  MinPp = 61, MaxPp = 120, MinIo = 0, MaxIo = 0 }
                , new { MinLock = 16, MaxLock = 30,  MinPp = 61, MaxPp = 120, MinIo = 1, MaxIo = 60 }
                , new { MinLock = 31, MaxLock = 45,  MinPp = 61, MaxPp = 120, MinIo = 0, MaxIo = 0 }
                , new { MinLock = 31, MaxLock = 45,  MinPp = 61, MaxPp = 120, MinIo = 1, MaxIo = 60 }
            };

            m_rateGroupList = new List<CRateSheetGroup>(rateSheetGroups.Length);
            foreach (var o in rateSheetGroups)
            {
                m_rateGroupList.Add(new CRateSheetGroup(o.MinLock, o.MaxLock, o.MinPp, o.MaxPp, o.MinIo, o.MaxIo));
            }

            RateGroupAssert(16, 0, 12, false, CRateSheetGroup.NO_RATES_FOR_LOCK);

            // PP Gap 61-120
            rateSheetGroups = new[] {
                new { MinLock = 0, MaxLock = 15, MinPp = 0, MaxPp = 60, MinIo = 0, MaxIo = 0 }
                , new { MinLock = 0, MaxLock = 15,  MinPp = 0, MaxPp = 60, MinIo = 1, MaxIo = 60 }
                , new { MinLock = 16, MaxLock = 30,  MinPp = 0, MaxPp = 60, MinIo = 0, MaxIo = 0 }
                , new { MinLock = 16, MaxLock = 30,  MinPp = 0, MaxPp = 60, MinIo = 1, MaxIo = 60 }
                , new { MinLock = 31, MaxLock = 45,  MinPp = 0, MaxPp = 60, MinIo = 0, MaxIo = 0 }
                , new { MinLock = 31, MaxLock = 45,  MinPp = 0, MaxPp = 60, MinIo = 1, MaxIo = 60 }
                , new { MinLock = 0, MaxLock = 15,  MinPp = 121, MaxPp = 180, MinIo = 0, MaxIo = 0 }
                , new { MinLock = 0, MaxLock = 15,  MinPp = 121, MaxPp = 180, MinIo = 1, MaxIo = 60 }
                , new { MinLock = 16, MaxLock = 30,  MinPp = 121, MaxPp = 180, MinIo = 0, MaxIo = 0 }
                , new { MinLock = 16, MaxLock = 30,  MinPp = 121, MaxPp = 180, MinIo = 1, MaxIo = 60 }
                , new { MinLock = 31, MaxLock = 45,  MinPp = 121, MaxPp = 180, MinIo = 0, MaxIo = 0 }
                , new { MinLock = 31, MaxLock = 45,  MinPp = 121, MaxPp = 180, MinIo = 1, MaxIo = 60 }
            };

            m_rateGroupList = new List<CRateSheetGroup>(rateSheetGroups.Length);
            foreach (var o in rateSheetGroups)
            {
                m_rateGroupList.Add(new CRateSheetGroup(o.MinLock, o.MaxLock, o.MinPp, o.MaxPp, o.MinIo, o.MaxIo));
            }

            RateGroupAssert(16, 61, 0, false, CRateSheetGroup.NO_RATES_FOR_PP);
        }

        private void TestSingleGroupList()
        {
            m_currentTest = "SingleGroupList";

            // Only PP-restricted Group
           var rateSheetGroups = new[] {
                new { MinLock = int.MinValue, MaxLock = int.MaxValue, MinPp = 0, MaxPp = 60, MinIo = int.MinValue, MaxIo = int.MaxValue }
            };

            m_rateGroupList = new List<CRateSheetGroup>(rateSheetGroups.Length);
            foreach (var o in rateSheetGroups)
            {
                m_rateGroupList.Add(new CRateSheetGroup(o.MinLock, o.MaxLock, o.MinPp, o.MaxPp, o.MinIo, o.MaxIo));
            }

            RateGroupAssert(0, 30, 0, true);
            RateGroupAssert(0, 30, 0, true);
            RateGroupAssert(0, 90, 0, false, CRateSheetGroup.NO_RATES_FOR_PP);

            // ONLY Lock group
            rateSheetGroups = new[] {
                new { MinLock = 10, MaxLock = 30, MinPp = int.MinValue, MaxPp = int.MaxValue, MinIo = int.MinValue, MaxIo = int.MaxValue }
            };

            m_rateGroupList = new List<CRateSheetGroup>(rateSheetGroups.Length);
            foreach (var o in rateSheetGroups)
            {
                m_rateGroupList.Add(new CRateSheetGroup(o.MinLock, o.MaxLock, o.MinPp, o.MaxPp, o.MinIo, o.MaxIo));
            }

            RateGroupAssert(10, 0, 0, true);
            RateGroupAssert(15, 0, 0, true);
            RateGroupAssert(30, 0, 0, true);
            RateGroupAssert(0, 0, 0, false, CRateSheetGroup.NO_RATES_FOR_LOCK);
            RateGroupAssert(0, 90, 30, false, CRateSheetGroup.NO_RATES_FOR_LOCK);
            
            // NO keyword restriction group
            rateSheetGroups = new[] {
                new { MinLock = int.MinValue, MaxLock = int.MaxValue, MinPp = int.MinValue, MaxPp = int.MaxValue, MinIo = int.MinValue, MaxIo = int.MaxValue }
            };

            m_rateGroupList = new List<CRateSheetGroup>(rateSheetGroups.Length);
            foreach (var o in rateSheetGroups)
            {
                m_rateGroupList.Add(new CRateSheetGroup(o.MinLock, o.MaxLock, o.MinPp, o.MaxPp, o.MinIo, o.MaxIo));
            }
            
            RateGroupAssert(0, 0, 0, true);
            RateGroupAssert(2, 53, 90, true);
            RateGroupAssert(3, 3, 12, true);
        }


        private void RateGroupAssert(int lockdays, int ppmonths , int iomonths, bool expectedResult, string expectedMsg)
        {
            bool result = false;
            string disqualMsg = string.Empty;
            string assertMsg = m_currentTest + ": LOCK:" + lockdays.ToString() + " PP:" + ppmonths.ToString() + " IO:" + iomonths.ToString();

            result = CRateSheetGroup.VerifyRateOptionPricingSegment(m_rateGroupList,lockdays,ppmonths, iomonths, ref disqualMsg);

            if (expectedResult == true)
            {
                Assert.That(result, Is.True, assertMsg);
            }
            else
            {
                Assert.That(result, Is.False, assertMsg);
                Assert.That(disqualMsg,Is.EqualTo( expectedMsg), assertMsg );
            }
        }
        private void RateGroupAssert(int lockdays, int ppmonths, int iomonths, bool expectedResult)
        {
            RateGroupAssert(lockdays, ppmonths, iomonths, expectedResult, null);
        }


    }
}