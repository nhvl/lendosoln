/// Author: David Dao
/// 
/// Copy from Thien Test.
using System;

using System.Collections;

using NUnit.Framework;
using LendersOfficeApp.los.RatePrice;

namespace LendOSolnTest.RatePrice
{
    [TestFixture]
	public class LoanDataWrapperTest
	{
        [Test]
        public void Keyword2FieldNameMappingVerify() 
        {
            LoanDataWrapper.VerifyMapping();
        }

        [Test]
        public void HashtableCaseInsentitiveTest() 
        {
            Hashtable table = new CaseInsensitiveHashtable();

            table["test"] = "BLAH";
            table["Test"] = "FOO";

            Assert.AreEqual("FOO", table["test"]);
            Assert.AreEqual("FOO", table["Test"]);
            Assert.AreEqual("FOO", table["TEST"]);

            try 
            {
                table.Add("TEST", "HELLO");
                Assert.Fail("SHOULD FAILED");
            } 
            catch (ArgumentException) 
            {
            }
        }
	}

    
}
