/// Author: David Dao

using System;
using NUnit.Framework;

using DataAccess;

namespace LendOSolnTest
{
    [TestFixture]
	public class AlwaysRoundDownTest
	{
        [Test]
        public void RateRoundDownTest()
        {
            decimal[,] rateTest = {
                                      {0, 0}
                                      , {10.0M, 10.000M}
                                      , {10.001M, 10.001M}
                                      , {10.0010002M, 10.001M}
                                      , {10.0019999M, 10.001M}
                                      , {10.19999M, 10.199M}
                                      , {-10.0M, -10.000M}
                                      , {-10.001M, -10.001M}
                                      , {-10.0010002M, -10.001M}
                                      , {-10.0019999M, -10.001M}
                                      , {-10.0015M, -10.001M}

                                  };
            for (int i = 0; i < rateTest.Length / 2; i++) 
            {
                Assert.AreEqual(rateTest[i, 1], Tools.AlwaysRoundDown(rateTest[i, 0], 3));
            }
        }

        [Test]
        public void MoneyRoundDownTest() 
        {
            decimal[,] moneyTest = {
                                       {0, 0}
                                       , {10.0M, 10.00M}
                                       , {10.01M, 10.01M}
                                       , {10.010002M, 10.01M}
                                       , {10.019999M, 10.01M}
                                       , {10.199M, 10.19M}
                                       , {-10.0M, -10.00M}
                                       , {-10.01M, -10.01M}
                                       , {-10.010002M, -10.01M}
                                       , {-10.019999M, -10.01M}
                                       , {-10.015M, -10.01M}
                                   };
            for (int i = 0; i < moneyTest.Length / 2; i++) 
            {
                Assert.AreEqual(moneyTest[i, 1], Tools.AlwaysRoundDown(moneyTest[i, 0], 2));
            }

        }

	}
}
