﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Xml;
using NUnit.Framework;
using System.Threading.Tasks;

namespace LendOSolnTest.Controls
{
    public class Methods
    {
        public static string TestHttpMethod(string page, string method)
        {
            return TestHttpMethodWithCredential(page, CredentialCache.DefaultNetworkCredentials, method);
        }

        public static Task<Tuple<string, string>> TestHttpMethodAsync(string url, string method)
        {
            var tcs = new TaskCompletionSource<Tuple<string, string>>();
            var httpReq = SetupRequest(url, CredentialCache.DefaultNetworkCredentials);
            httpReq.Method = method;
            httpReq.ContentLength = 0;

            try
            {
                httpReq.BeginGetResponse(iar =>
                {
                    HttpWebResponse response = null;
                    try
                    {
                        using (response = (HttpWebResponse)httpReq.EndGetResponse(iar))
                        {
                            if (response.StatusCode != HttpStatusCode.OK)
                            {
                                tcs.SetResult(new Tuple<string, string>(url, response.StatusDescription));
                            } else
                            {
                                tcs.SetResult(new Tuple<string, string>(string.Empty, HttpStatusCode.OK.ToString()));
                            }
                        }
                    }
                    catch (Exception exc)
                    {
                        tcs.SetException(exc);
                    }
                }, null);
            }
            catch (Exception exc)
            {
                tcs.SetException(exc);
            }
            return tcs.Task;
        }

        public static string TestHttpMethodWithCredential(string page, ICredentials credentials, string method)
        {
            try
            {
                var httpReq = SetupRequest(page, credentials);
                //http://stackoverflow.com/questions/5915131/can-i-send-an-empty-http-post-webrequest-object-from-c-sharp-to-iis
                httpReq.Method = method;
                httpReq.ContentLength = 0;

                using (var httpRes = (HttpWebResponse)httpReq.GetResponse())
                {
                    if (httpRes.StatusCode != HttpStatusCode.OK)
                    {
                        return httpRes.StatusDescription;
                    }
                }
            }
            catch (Exception e)
            {
                return e.Message;
            }

            return string.Empty;
        }
        
        /// <summary>
        /// Setup common request for all
        /// </summary>
        /// <param name="page"></param>
        /// <param name="credentials"></param>
        /// <returns></returns>
        public static HttpWebRequest SetupRequest(string page, ICredentials credentials)
        {
            var httpReq = (HttpWebRequest)WebRequest.Create(page);
            httpReq.AllowAutoRedirect = false;
            httpReq.Credentials = credentials;
            httpReq.Timeout = 10000;
            httpReq.UserAgent = "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0)";
            //if (page.StartsWith("https")) ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
            return httpReq;
        }

        public static bool IsValidXml(string xmlString)
        {
            // var tagsWithData = new Regex("<\\w+>[^<]+</\\w+>");

            //Light checking
            if (string.IsNullOrEmpty(xmlString) || xmlString.StartsWith("<") == false)
            {
                return false;
            }
            var xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(xmlString);
            return true;
        }
        public static IEnumerable<string[]> ReadCSV(string fileName)
        {
            var data = new List<string[]>();
            var reader = new StreamReader(File.OpenRead(fileName));
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                var values = line.Split('\t');
                data.Add(new[] { values[0].Replace("\"", "").Trim(), values[1].Replace("\"", "").Trim() });
            }
            return data;
        }
    
    
    
    }
}
