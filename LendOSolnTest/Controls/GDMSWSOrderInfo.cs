﻿using LendersOffice.GDMS.Orders;

namespace LendOSolnTest.Controls
{
    class GDMSWSOrderInfo
    {
        public CredentialsObject credentials;
        public Order orderObject;
        public string sReturnFormat = "xml";
        public OrdersClient orderClient;
        public string MISMOXML;
        public SendEmailMessageObject emailToSend;
        public OrderIdentifier orderIdentification;
        public GDMSWSOrderInfo()
        {
            credentials = new CredentialsObject
                {CompanyId = 5, UserName = "LQBTestUser", Password = "LQBTestUser", UserType = 2};
            orderObject = new Order {Address1 = "123 Main Street", ProductID = 5608, FileNumber = 847612};
            //orderClient = new OrdersClient("BasicHttpBinding_IOrders1");
            orderClient = LendOSolnTest.Integrations.GlobalDMSTests.CreateOrderInstance();
            MISMOXML = "<REQUEST_GROUP MISMOVersionID=\"2.3\">" +
                       "<REQUESTING_PARTY _Identifier=\"196539\" _Name=\"Company Name\" _TransactionIdentifier=\"DEV_12345\"/>" +
                       "<REQUEST LoginAccountIdentifier=\"LQBTestUser\" LoginAccountPassword=\"LQBTestUser\" RequestDatetime=\"2009-04-03T13:15:30\">" +
                       "<REQUEST_DATA>" +
                       "<VALUATION_REQUEST MISMOVersionID=\"2.3\" _ActionType=\"StatusQuery\" _CommentText=\"test\"/>" +
                       "</REQUEST_DATA></REQUEST></REQUEST_GROUP>";

            //SendEmailCommunication
            emailToSend=new SendEmailMessageObject
                {
                    EmailMessage = "My email message",
                    SendEmailToUserType = SendEmailMessageObject.SendEmailTo.Office,
                    Subject = "This is the subject"
                };
            orderIdentification=new OrderIdentifier {FileNumber = 12345678};


        }
    }
}
