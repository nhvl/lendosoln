﻿using System.IO;
using System.Net;
using System.Text;
using System.Xml;

namespace LendOSolnTest.Controls
{
    public class CustomHttpPost
    {
        public XmlDocument _xmlReq;
        public string CreateRequest()
        {
            return "";
        }
        public string SendRequest(string url,string requestData)
        {
            var data = Encoding.ASCII.GetBytes(requestData);

            var wReq = WebRequest.Create(url);
            wReq.Method = "POST";
            wReq.ContentType = "text/xml";
            wReq.ContentLength = data.Length;
            wReq.Timeout = 60 * 1000;
            var newStream = wReq.GetRequestStream();
            newStream.Write(data, 0, data.Length);
            newStream.Close();

            var respObj = wReq.GetResponse();
            string responseString;
            using (var sr = new StreamReader(respObj.GetResponseStream()))
            {
                responseString = sr.ReadToEnd();
            }
            respObj.Close();
            return responseString;
        }

        protected XmlElement GetNewNode(string nodeName)
        {
            return _xmlReq.CreateElement(nodeName);
        }

        protected void AddAttribute(XmlNode sourceNode, string attribName, string attribValue)
        {
            var att = _xmlReq.CreateAttribute(attribName);
            att.Value = attribValue;
            if (sourceNode.Attributes != null) sourceNode.Attributes.Append(att);
        }
    }
}
