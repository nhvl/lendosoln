﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using LendersOffice.XsltExportReport;

namespace LendOSolnTest.XsltExportReport
{
    [TestFixture]
    public class XsltExportExtensionObjectTest
    {
        [Test]
        public void TestSubstring()
        {
            var testCaseList = new[] {
                                   new { Input="", StartIndex=0, Length=5, Expected=""},
                                   new { Input="Robert", StartIndex=-2, Length=52, Expected="Robert"},
                                   new { Input="Robert", StartIndex=2, Length=-2, Expected=""},


                                   new { Input="Bob", StartIndex=0, Length=5, Expected="Bob"},
                                   new { Input="Bob", StartIndex=0, Length=1, Expected="B"},
                                   new { Input="Bob", StartIndex=0, Length=0, Expected=""},

                                   new { Input="Robert", StartIndex=0, Length=3, Expected="Rob"},
                                   new { Input="Robert", StartIndex=2, Length=52, Expected="bert"},
                                   new { Input="Robert", StartIndex=51, Length=52, Expected=""},

                               };

            XsltExportExtensionObject extensionObject = new XsltExportExtensionObject();

            foreach (var testCase in testCaseList)
            {
                Assert.AreEqual(testCase.Expected, extensionObject.Substring(testCase.Input, testCase.StartIndex, testCase.Length),
                    testCase.Input + ", StartIndex=" + testCase.StartIndex + ", Length=" + testCase.Length);
            }

        }
    }
}
