﻿// <copyright file="ValidLoanDataMigrationsTest.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Eric Mallare
//    Date:   4/6/2016
// </summary>
namespace LendOSolnTest.LoanDataMigrationTest
{
    using System;
    using LendersOffice.Migration;
    using NUnit.Framework;
    using System.Linq;

    /// <summary>
    /// Some basic tests for the small data layer migration framework.
    /// </summary>
    [TestFixture]
    public class ValidLoanDataMigrationsTest
    {
        /// <summary>
        /// Checks if all enums except Version0 have an associated migration. This may be modified later on once we start removing really old migrations.
        /// This also checks for negative LoanVersionT values along the way.
        /// </summary>
        [Test]
        public void TestAllMigrationsHaveMatchingVersion()
        {
            LoanDataMigrationsRunner runner = new LoanDataMigrationsRunner("System", Guid.Empty, Guid.Empty, Guid.Empty, isSystemBatchMigration: true);
            foreach (LoanVersionT versionNum in Enum.GetValues(typeof(LoanVersionT)))
            {
                if (versionNum == LoanVersionT.Version0)
                {
                    continue;
                }

                Assert.GreaterOrEqual((int)versionNum, 0, "A LoanVersionT value is negative.");

                var migrationContainer = runner.GetMigrationbyVersionNumber(versionNum);
                Assert.IsNotNull(migrationContainer, "LoanVersionT value " + versionNum + " does not have an associated migration.");

                LoanDataMigration migration = migrationContainer.Item2(null);
                Assert.AreEqual(migration.GetType(), migrationContainer.Item1, "Mismatched types for version number " + versionNum + " in LoanDataMigrationsRunner::GetMigrationByVersionNumber.");
                Assert.AreEqual(versionNum, migration.AssociatedVersion, "LoanVersionT value " + versionNum + " does not match associated version number " + migration.AssociatedVersion + " for migration " + migration.Name);
            }
        }

        /// <summary>
        /// Checks if there are any duplicate entries in LoanVersionT.
        /// </summary>
        [Test]
        public void TestLoanVersionTDuplicates()
        {
            var enumValues = Enum.GetValues(typeof(LoanVersionT)).Cast<LoanVersionT>();
            Assert.AreEqual(enumValues.Distinct().Count(), enumValues.Count(), "There must be no duplicates in LoanVersionT");
        }

        /// <summary>
        /// Test that ensures that the LoanVersionT enum only contains sequential values.
        /// </summary>
        [Test]
        public void TestLoanVersionTSequential()
        {
            for (int enumValue = 0; enumValue < (int)LoanDataMigrationUtils.GetLatestVersion(); enumValue++)
            {
                Assert.IsTrue(Enum.IsDefined(typeof(LoanVersionT), enumValue), "LoanVersionT is missing value " + enumValue + ". Please ensure that LoanVersionT values are sequential.");
            }
        }
    }
}
