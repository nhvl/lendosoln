/// Author: David Dao

using System;
using NUnit.Framework;

using DataAccess;

namespace LendOSolnTest
{
    [TestFixture]
	public class LtvRoundingTest
	{
        [Test]
        public void LtvRounding() 
        {
            decimal[,] rateTest = {
                                      {0, 0}
                                      , {10.0M, 10.000M}
                                      , {10.001M, 10.001M}
                                      , {10.0010002M, 10.002M}
                                      , {10.9999999M, 10.999M}
                                      , {10.9980001M, 10.999M}
                                      , {10.0000001M, 10.001M}

                                  };
            for (int i = 0; i < rateTest.Length / 2; i++) 
            {
                Assert.AreEqual(rateTest[i, 1], Tools.LtvRounding(rateTest[i, 0]));
            }
        }
	}
}
