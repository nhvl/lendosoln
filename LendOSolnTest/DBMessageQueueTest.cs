using System;
using LendersOffice.ObjLib.DatabaseMessageQueue;
using LendersOffice.Constants;
using NUnit.Framework;
using DataAccess;

namespace LendOSolnTest
{
    [TestFixture]
    public class DBMessageQueueTest
    {
        public static readonly DBQueue DummyQueue;

        static DBMessageQueueTest()
        {

            int DummyQueueId  = (int)QueueType.Dummy;
            DummyQueue        = new DBQueue(DummyQueueId, "TestQueue");
        }

       
        [Test]
        public void CmpSendAndReceiveTest()
        {
            int idx = (new Random()).Next(1000);

            string subject1 = "Subject " + idx.ToString() + " *** " + Guid.NewGuid();
            string subject2 = "Other "   + idx.ToString() + " *** " + Guid.NewGuid();
            string data     = "Data "    + idx.ToString() + " *** " + Guid.NewGuid();

            
            DBMessageQueue mQ = new DBMessageQueue( DummyQueue );
            long count = mQ.Count;
            long id = mQ.Send( subject1, data, subject2 );
            Assert.That(mQ.Count, Is.EqualTo(count + 1));

            DBMessage msg = mQ.ReceiveById(id);
            Assert.That(mQ.Count, Is.EqualTo(count));

            Assert.AreEqual( data, msg.Data);
            Assert.AreEqual( subject1, msg.Subject1);
            Assert.AreEqual( subject2, msg.Subject2);

            mQ.Send(subject1, data, subject2);
            msg = mQ.Receive();

            Assert.That(mQ.Count, Is.EqualTo(count));

            Assert.AreEqual(data, msg.Data);
            Assert.AreEqual(subject1, msg.Subject1);
            Assert.AreEqual(subject2, msg.Subject2);

        }

        [Test]
        public void SubjectPeekAndDeleteTest()
        {
            DBMessageQueue mQ = new DBMessageQueue(DummyQueue);
            mQ.ReceiveAll();
            var items = mQ.PeekBySubject();
            Assert.That( items.Length, Is.EqualTo(0));


            string subject1 = "Subject";
            string data = "dotn care data";


            for (int x = 0; x < 10; x++)
            {
                for (int i = 0; i < 3; i++)
                {
                    mQ.Send(subject1 +i, data);
                }
            }


            DBMessage[] messages = mQ.PeekBySubject(); //subj1
            Assert.That(messages.Length, Is.EqualTo(10));
            Assert.That(messages[0].Subject1, Is.EqualTo("Subject0"));
            mQ.DequeueBySubject("Subject0", messages[0].Id, messages[9].Id);

            messages = mQ.PeekBySubject();
            Assert.That(messages.Length, Is.EqualTo(10));
            Assert.That(messages[0].Subject1, Is.EqualTo("Subject1"));
            mQ.DequeueBySubject("Subject1", messages[0].Id, messages[9].Id);

            messages = mQ.PeekBySubject();
            Assert.That(messages.Length, Is.EqualTo(10));
            Assert.That(messages[0].Subject1, Is.EqualTo("Subject2"));
            mQ.DequeueBySubject("Subject2", messages[0].Id, messages[9].Id);

            for (int x = 0; x < 10; x++)
            {
                for (int i = 0; i < 3; i++)
                {
                    mQ.Send(subject1 + i, data);
                }
            }



            messages = mQ.PeekBySubject(); //subj1
            Assert.That(messages.Length, Is.EqualTo(10));
            Assert.That(messages[0].Subject1, Is.EqualTo("Subject0"));
            mQ.DequeueBySubject("Subject0", messages[0].Id, messages[9].Id);

            messages = mQ.PeekBySubject();
            Assert.That(messages.Length, Is.EqualTo(10));
            Assert.That(messages[0].Subject1, Is.EqualTo("Subject1"));
            mQ.DequeueBySubject("Subject1", messages[0].Id, messages[9].Id);

            messages = mQ.PeekBySubject();
            Assert.That(messages.Length, Is.EqualTo(10));
            Assert.That(messages[0].Subject1, Is.EqualTo("Subject2"));
            mQ.DequeueBySubject("Subject2", messages[0].Id, messages[9].Id);


            items = mQ.PeekBySubject();
            Assert.That(items.Length, Is.EqualTo(0));


        }




    }

}
