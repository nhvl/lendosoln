﻿// Antonio Valencia
using DataAccess;
using LendersOffice.CreditReport;
using LendOSolnTest.Common;
using LendOSolnTest.Fakes;
using NUnit.Framework;

namespace LendOSolnTest.Credit
{
    [TestFixture]
    public class RepresantiveScoreTest
    {
        [TestFixtureSetUp]
        public void OnSetup()
        {
            LoginTools.LoginAs(TestAccountType.LoTest001);
        }

        [Test]
        public void CreditTests()
        {
            var tests = new[] { 
                new { File = "SampleCreditReports/TestCredit01.xml.config", BScore = 743, CScore = 0, Message = "3 Scores 1 Borrower"},
                new { File = "SampleCreditReports/TestCredit04_1score.xml.config", BScore = 745, CScore = 0, Message = "1 Score 1 Borrower" },
                new { File = "SampleCreditReports/TestCredit04_2score.xml.config", BScore = 622, CScore = 0, Message = "2 Score 1 Borrower" }, 
                new { File = "SampleCreditReports/TestCredit04_3score2b.xml.config", BScore = 743, CScore = 654, Message ="3 Score 2 Borrower" },
                new { File = "SampleCreditReports/TestCredit04_2score2b.xml.config",  BScore = 741, CScore = 654, Message = "2 Score 2 Borrower" },
                new { File = "SampleCreditReports/TestCredit04_1score2b.xml.config", BScore = 498, CScore = 627, Message = "1 Score 2 Borrower" }
            };

            foreach (var test in tests)
            {
                ICreditReport creditReport = CreditTestingUtils.LoadReportFromTestData(test.File);
                creditReport.aBorrowerCreditModeT = E_aBorrowerCreditModeT.Borrower;
                Assert.That(creditReport.RepresentativeScore, Is.EqualTo(test.BScore), test.Message);

                creditReport.aBorrowerCreditModeT = E_aBorrowerCreditModeT.Both;
                Assert.That(creditReport.RepresentativeScore, Is.EqualTo(test.BScore), test.Message);

                creditReport.aBorrowerCreditModeT = E_aBorrowerCreditModeT.Coborrower;
                Assert.That(creditReport.RepresentativeScore, Is.EqualTo(test.CScore), test.Message);
            }
        }

        [Test]
        public void ManualCreditTest()
        {
            var tests = new[] { 
                     new {  aBSsn="111-11-1111", aBExperian="600", aBTransUnion="700", aBEquifax="800", aBRepScore = "700",
                            aCSsn="", aCExperian="0",    aCTransUnion="0",    aCEquifax="0", aCRepScore = "0" },
                     new {  aBSsn="111-11-1111", aBExperian="0", aBTransUnion="700", aBEquifax="800", aBRepScore = "700",
                            aCSsn="", aCExperian="0",    aCTransUnion="0",    aCEquifax="0", aCRepScore = "0" },
                     new {  aBSsn="111-11-1111", aBExperian="0", aBTransUnion="0", aBEquifax="800", aBRepScore = "800",
                            aCSsn="", aCExperian="0",    aCTransUnion="0",    aCEquifax="0", aCRepScore = "0"  }, 
                     new {  aBSsn="111-11-1111", aBExperian="600", aBTransUnion="700", aBEquifax="800", aBRepScore = "700",
                            aCSsn="", aCExperian="455",    aCTransUnion="785",    aCEquifax="546", aCRepScore = "546" },
                     new {  aBSsn="111-11-1111", aBExperian="0", aBTransUnion="700", aBEquifax="800", aBRepScore = "700",
                            aCSsn="", aCExperian="455",    aCTransUnion="0",    aCEquifax="352", aCRepScore = "352" },
                     new {  aBSsn="111-11-1111", aBExperian="0", aBTransUnion="0", aBEquifax="800", aBRepScore = "800",
                            aCSsn="", aCExperian="400",    aCTransUnion="0",    aCEquifax="0", aCRepScore = "400"  }
            };
            
            foreach (var test in tests)
            {
                CPageData data = FakePageData.Create();
                data.CalcModeT = E_CalcModeT.PriceMyLoan;
                
                data.InitLoad();

                CAppData dataApp = data.GetAppData(0);
                dataApp.aBSsn = test.aBSsn;
                dataApp.aBExperianScorePe_rep = test.aBExperian;
                dataApp.aBTransUnionScorePe_rep = test.aBTransUnion;
                dataApp.aBEquifaxScorePe_rep = test.aBEquifax;
                dataApp.aCSsn = test.aCSsn;
                dataApp.aCExperianScorePe_rep = test.aCExperian;
                dataApp.aCTransUnionScorePe_rep = test.aCTransUnion;
                dataApp.aCEquifaxScorePe_rep = test.aCEquifax;

                ICreditReport rep = dataApp.CreditReportData.Value;
                rep.aBorrowerCreditModeT = E_aBorrowerCreditModeT.Borrower;
                Assert.That(rep.RepresentativeScore.ToString(), Is.EqualTo(test.aBRepScore));
                rep.aBorrowerCreditModeT = E_aBorrowerCreditModeT.Both;
                Assert.That(rep.RepresentativeScore.ToString(), Is.EqualTo(test.aBRepScore));
                rep.aBorrowerCreditModeT = E_aBorrowerCreditModeT.Coborrower;
                Assert.That(rep.RepresentativeScore.ToString(), Is.EqualTo(test.aCRepScore));
            }
        }
    }
}

