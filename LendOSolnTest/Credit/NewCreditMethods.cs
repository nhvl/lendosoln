// Antonio Valencia
using System;
using System.Collections.Generic;
using System.Xml;
using NUnit.Framework;
using CommonLib;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.CreditReport;
using LendersOffice.CreditReport.Mcl;
using LendOSolnTest.Common;

namespace LendOSolnTest.Credit
{
	/// <summary>
	/// This class contains credit test methods; 
	/// </summary>
	[TestFixture]
	public class CreditKeywordTest
	{
        [Test]
		public void HasMortgageIncludedInBKWithinTest() 
		{
			ICreditReport[] reports = new ICreditReport[3] { CreditTestingUtils.LoadReportFromTestData("TestHasMortgageIncludedInBKWithinTest1.xml" ), 
															 CreditTestingUtils.LoadReportFromTestData("TestHasMortgageIncludedInBKWithinTest2.xml"),
														     CreditTestingUtils.LoadReportFromTestData("TestHasMortgageIncludedInBKWithinTest3.xml") };
			
			DateTime[] datesForReports = new DateTime[3] { new DateTime( 2006,6,1 ), new DateTime( 2006, 12, 1 ), DateTime.MaxValue };
			
			Assert.That( reports.Length, Is.EqualTo( datesForReports.Length ) );
			int monthsSinceLiab; 
			int rep;
			for( int i = 0; i < reports.Length; i ++ ) 
			{
				rep = 0;
				monthsSinceLiab = (int)Math.Ceiling( (double) DateTime.Now.Subtract(datesForReports[i]).TotalDays / 30.0 );
				do
				{
					if ( datesForReports[i] != DateTime.MaxValue ) 
					{
						Assert.That( reports[i].HasMortgageIncludedInBKWithin( monthsSinceLiab ) , Is.True, "Barely Passing Test i=" +i+" r:" + rep); 
						Assert.That( reports[i].HasMortgageIncludedInBKWithin( monthsSinceLiab + 20 ), Is.True, "+ 20+ Barely Passing i="+i+" r:" + rep );
					}
					else 
					{
						Assert.That( reports[i].HasMortgageIncludedInBKWithin( 100 ), Is.False, "Should always fail (100) i="+i+" r:" + rep );
					}

					Assert.That( reports[i].HasMortgageIncludedInBKWithin( 0 ), Is.False, "0 Test i="+i+" r:" + rep );
					Assert.That( reports[i].HasMortgageIncludedInBKWithin( 10 ), Is.False, "10 Month i="+i+" r:" + rep   );
					rep++;
				} while ( rep < 5 );  //this is to test the caching as well
			}
		}

        [Test]
        [ExpectedException(typeof(GenericUserErrorMessageException))]
        public void FcCountX_SupplyCountNODParam_ThrowsException()
        {
            var creditReport = CreditTestingUtils.LoadReportFromTestData(
                "TestHasMortgageIncludedInBKWithinTest2.xml");

            creditReport.FcCountX("countnod=y");
        }

        [Test]
        [ExpectedException(typeof(GenericUserErrorMessageException))]
        public void HasFcX_SupplyCountNODParam_ThrowsException()
        {
            var creditReport = CreditTestingUtils.LoadReportFromTestData(
                "TestHasMortgageIncludedInBKWithinTest2.xml");

            creditReport.HasFcX("countnod=y");
        }

        [Test]
        public void ChargeOffCountX_MelindaTestcaseScenario1_Returns0()
        {
            var creditReport = CreditTestingUtils.LoadReportFromTestData(
                "melinda-testcase.xml");

            int count = creditReport.ChargeOffCountX("Within<=36");

            Assert.AreEqual(0, count);
        }

        [Test]
        public void ChargeOffCountX_MelindaTestcaseScenario2_Returns12()
        {
            var creditReport = CreditTestingUtils.LoadReportFromTestData(
                "melinda-testcase.xml");

            int count = creditReport.ChargeOffCountX("Within<=1000");

            Assert.AreEqual(12, count);
        }

        [Test]
        public void ChargeOffCountX_MelindaTestcaseScenario3_Returns9()
        {
            var creditReport = CreditTestingUtils.LoadReportFromTestData(
                "melinda-testcase.xml");

            int count = creditReport.ChargeOffCountX("T=U");

            Assert.AreEqual(9, count);
        }

        [Test]
        public void ChargeOffCountX_MelindaTestcaseScenario4_Returns0()
        {
            var creditReport = CreditTestingUtils.LoadReportFromTestData(
                "melinda-testcase.xml");

            int count = creditReport.ChargeOffCountX("Within<=36&T=O");

            Assert.AreEqual(0, count);
        }

        [Test]
        public void ChargeOffCountX_MelindaTestcaseScenario5_Returns12()
        {
            var creditReport = CreditTestingUtils.LoadReportFromTestData(
                "melinda-testcase.xml");

            int count = creditReport.ChargeOffCountX("Within<=1000&T=IRMOU");

            Assert.AreEqual(12, count);
        }

        [Test]
        public void ChargeOffCountX_CarlosTestcaseScenario1_Returns0()
        {
            var creditReport = CreditTestingUtils.LoadReportFromTestData(
                "carlos-testcase.xml");

            int count = creditReport.ChargeOffCountX("Within<=36");

            Assert.AreEqual(0, count);
        }

        [Test]
        public void ChargeOffCountX_CarlosTestcaseScenario2_Returns7()
        {
            var creditReport = CreditTestingUtils.LoadReportFromTestData(
                "carlos-testcase.xml");

            int count = creditReport.ChargeOffCountX("Within<=1000");

            Assert.AreEqual(7, count);
        }

        [Test]
        public void ChargeOffCountX_CarlosTestcaseScenario3_Returns6()
        {
            var creditReport = CreditTestingUtils.LoadReportFromTestData(
                "carlos-testcase.xml");

            int count = creditReport.ChargeOffCountX("T=R");

            Assert.AreEqual(6, count);
        }

        [Test]
        public void ChargeOffCountX_CarlosTestcaseScenario4_Returns0()
        {
            var creditReport = CreditTestingUtils.LoadReportFromTestData(
                "carlos-testcase.xml");

            int count = creditReport.ChargeOffCountX("Within<=36&T=R");

            Assert.AreEqual(0, count);
        }

        [Test]
        public void ChargeOffCountX_CarlosTestcaseScenario5_Returns6()
        {
            var creditReport = CreditTestingUtils.LoadReportFromTestData(
                "carlos-testcase.xml");

            int count = creditReport.ChargeOffCountX("Within<=1000&T=IRM");

            Assert.AreEqual(6, count);
        }

        [Test]
        public void ChargeOffCountX_LuisTestcaseScenario1_Returns0()
        {
            var creditReport = CreditTestingUtils.LoadReportFromTestData(
                "luis-testcase.xml");

            int count = creditReport.ChargeOffCountX("Within<=36");

            Assert.AreEqual(0, count);
        }

        [Test]
        public void ChargeOffCountX_LuisTestcaseScenario2_Returns2()
        {
            var creditReport = CreditTestingUtils.LoadReportFromTestData(
                "luis-testcase.xml");

            int count = creditReport.ChargeOffCountX("Within<=1000");

            Assert.AreEqual(2, count);
        }

        [Test]
        public void ChargeOffCountX_LuisTestcaseScenario3_Returns2()
        {
            var creditReport = CreditTestingUtils.LoadReportFromTestData(
                "luis-testcase.xml");

            int count = creditReport.ChargeOffCountX("T=U");

            Assert.AreEqual(2, count);
        }

        [Test]
        public void ChargeOffCountX_LuisTestcaseScenario4_Returns0()
        {
            var creditReport = CreditTestingUtils.LoadReportFromTestData(
                "luis-testcase.xml");

            int count = creditReport.ChargeOffCountX("Within<=36&T=R");

            Assert.AreEqual(0, count);
        }

        [Test]
        public void ChargeOffCountX_LuisTestcaseScenario5_Returns0()
        {
            var creditReport = CreditTestingUtils.LoadReportFromTestData(
                "luis-testcase.xml");

            int count = creditReport.ChargeOffCountX("Within<=1000&T=IRM");

            Assert.AreEqual(0, count);
        }

        [Test]
        public void ChargeOffCountX_SarahTestcaseScenario1_Returns0()
        {
            var creditReport = CreditTestingUtils.LoadReportFromTestData(
                "sarah-testcase.xml");

            int count = creditReport.ChargeOffCountX("Within<=36");

            Assert.AreEqual(0, count);
        }

        [Test]
        public void ChargeOffCountX_SarahTestcaseScenario2_Returns22()
        {
            var creditReport = CreditTestingUtils.LoadReportFromTestData(
                "sarah-testcase.xml");

            int count = creditReport.ChargeOffCountX("Within<=1000");

            Assert.AreEqual(22, count);
        }

        [Test]
        public void ChargeOffCountX_SarahTestcaseScenario3_Returns21()
        {
            var creditReport = CreditTestingUtils.LoadReportFromTestData(
                "sarah-testcase.xml");

            int count = creditReport.ChargeOffCountX("T=OU");

            Assert.AreEqual(21, count);
        }

        [Test]
        public void ChargeOffCountX_SarahTestcaseScenario4_Returns0()
        {
            var creditReport = CreditTestingUtils.LoadReportFromTestData(
                "sarah-testcase.xml");

            int count = creditReport.ChargeOffCountX("Within<=36&T=R");

            Assert.AreEqual(0, count);
        }

        [Test]
        public void ChargeOffCountX_SarahTestcaseScenario5_Returns1()
        {
            var creditReport = CreditTestingUtils.LoadReportFromTestData(
                "sarah-testcase.xml");

            int count = creditReport.ChargeOffCountX("Within<=1000&T=IRM");

            Assert.AreEqual(1, count);
        }

        [Test]
        public void HasFcCountX_MelindaTestcaseScenario1_Returns0()
        {
            var creditReport = CreditTestingUtils.LoadReportFromTestData(
                "melinda-testcase.xml");

            int count = creditReport.HasFcX("within<=240");

            Assert.AreEqual(0, count);
        }

        [Test]
        public void HasFcCountX_CarlosTestcaseScenario1_Returns1()
        {
            var creditReport = CreditTestingUtils.LoadReportFromTestData(
                "carlos-testcase.xml");

            int count = creditReport.HasFcX("within<=240");

            Assert.AreEqual(1, count);
        }

        [Test]
        public void HasFcCountX_NicoleTestcaseScenario1_Returns1()
        {
            var creditReport = CreditTestingUtils.LoadReportFromTestData(
                "nicole-testcase.xml");

            int count = creditReport.HasFcX("within<=240");

            Assert.AreEqual(1, count);
        }


        [Test]
        public void ChargeOffCountXNoParams_UnpaidCollectionPublicRecord_Returns1()
        {
            TestCreditReport creditReport = new TestCreditReport();
            var record = new TestPublicRecord()
            {
                dispositionType = E_CreditPublicRecordDispositionType.Unsatisfied,
                type = E_CreditPublicRecordType.Collection,
                lastEffectiveDate = DateTime.Today
            };
            var records = new List<TestPublicRecord>();
            records.Add(record);
            creditReport.allPublicRecords = new System.Collections.ArrayList(records);
            creditReport.allLiabilities = new System.Collections.ArrayList();

            var count = creditReport.ChargeOffCountX("");

            Assert.AreEqual(1, count);
        }

        [Test]
        public void ChargeOffCountXNoParams_PaidCollectionPublicRecord_Returns1()
        {
            TestCreditReport creditReport = new TestCreditReport();
            var record = new TestPublicRecord()
            {
                dispositionType = E_CreditPublicRecordDispositionType.Paid,
                type = E_CreditPublicRecordType.Collection,
                lastEffectiveDate = DateTime.Today
            };
            var records = new List<TestPublicRecord>();
            records.Add(record);
            creditReport.allPublicRecords = new System.Collections.ArrayList(records);
            creditReport.allLiabilities = new System.Collections.ArrayList();

            var count = creditReport.ChargeOffCountX("");

            Assert.AreEqual(1, count);
        }

        [Test]
        public void ChargeOffCountXNoParams_NoPublicRecordNoLiabilities_Returns0()
        {
            TestCreditReport creditReport = new TestCreditReport();
            creditReport.allPublicRecords = new System.Collections.ArrayList();
            creditReport.allLiabilities = new System.Collections.ArrayList();

            var count = creditReport.ChargeOffCountX("");

            Assert.AreEqual(0, count);
        }

        [Test]
        public void ChargeOffCountXNoParams_ChargeOffDerog_Returns1()
        {
            TestCreditReport creditReport = new TestCreditReport();
            creditReport.aBorrowerCreditModeT = E_aBorrowerCreditModeT.Borrower;
            creditReport.allPublicRecords = new System.Collections.ArrayList();
            var liability = new TestLiability()
            {
                isChargeOff = true,
                endDerogDate = DateTime.Today,
                liaOwnerT = E_LiaOwnerT.Borrower,
                liabilityType = E_DebtRegularT.Open
            };
            var liabilities = new List<TestLiability>();
            liabilities.Add(liability);
            creditReport.allLiabilities = new System.Collections.ArrayList(liabilities);

            var count = creditReport.ChargeOffCountX("");

            Assert.AreEqual(1, count);
        }

        [Test]
        public void ChargeOffCountXNoParams_CollectionDerog_Returns1()
        {
            TestCreditReport creditReport = new TestCreditReport();
            creditReport.aBorrowerCreditModeT = E_aBorrowerCreditModeT.Borrower;
            creditReport.allPublicRecords = new System.Collections.ArrayList();
            var liability = new TestLiability()
            {
                isChargeOff = true,
                endDerogDate = DateTime.Today,
                liaOwnerT = E_LiaOwnerT.Borrower,
                liabilityType = E_DebtRegularT.Open
            };
            var liabilities = new List<TestLiability>();
            liabilities.Add(liability);
            creditReport.allLiabilities = new System.Collections.ArrayList(liabilities);

            var count = creditReport.ChargeOffCountX("");

            Assert.AreEqual(1, count);
        }

        [Test]
        public void ChargeOffCountXNoParams_ChargeOffWithAdverseRatingList_Returns1()
        {
            TestCreditReport creditReport = new TestCreditReport();
            creditReport.aBorrowerCreditModeT = E_aBorrowerCreditModeT.Borrower;
            creditReport.allPublicRecords = new System.Collections.ArrayList();
            var adverseRatingList = new List<CreditAdverseRating>();
            adverseRatingList.Add(new CreditAdverseRating(E_AdverseType.ChargeOffOrCollection, DateTime.Today));
            var liability = new TestLiability()
            {
                isChargeOff = true,
                endDerogDate = DateTime.Today,
                liaOwnerT = E_LiaOwnerT.Borrower,
                liabilityType = E_DebtRegularT.Open,
                creditAdverseRatingList = new System.Collections.ArrayList(adverseRatingList)
            };
            var liabilities = new List<TestLiability>();
            liabilities.Add(liability);
            creditReport.allLiabilities = new System.Collections.ArrayList(liabilities);

            var count = creditReport.ChargeOffCountX("");

            Assert.AreEqual(1, count);
        }

        [Test]
        public void ChargeOffCountXNoParams_CollectionWithAdverseRatingList_Returns1()
        {
            TestCreditReport creditReport = new TestCreditReport();
            creditReport.aBorrowerCreditModeT = E_aBorrowerCreditModeT.Borrower;
            creditReport.allPublicRecords = new System.Collections.ArrayList();
            var adverseRatingList = new List<CreditAdverseRating>();
            adverseRatingList.Add(new CreditAdverseRating(E_AdverseType.ChargeOffOrCollection, DateTime.Today));
            var liability = new TestLiability()
            {
                isCollection = true,
                endDerogDate = DateTime.Today,
                liaOwnerT = E_LiaOwnerT.Borrower,
                liabilityType = E_DebtRegularT.Open,
                creditAdverseRatingList = new System.Collections.ArrayList(adverseRatingList)
            };
            var liabilities = new List<TestLiability>();
            liabilities.Add(liability);
            creditReport.allLiabilities = new System.Collections.ArrayList(liabilities);

            var count = creditReport.ChargeOffCountX("");

            Assert.AreEqual(1, count);
        }

        [Test]
        public void ChargeOffCountXNoParams_OneOfEachType_Returns5()
        {
            TestCreditReport creditReport = new TestCreditReport();
            creditReport.aBorrowerCreditModeT = E_aBorrowerCreditModeT.Borrower;
            creditReport.allPublicRecords = new System.Collections.ArrayList();
            var types = new E_DebtRegularT[] 
            { 
                E_DebtRegularT.Installment,
                E_DebtRegularT.Open,
                E_DebtRegularT.Other,
                E_DebtRegularT.Revolving,
                E_DebtRegularT.Mortgage
            };
            var liabilities = new List<TestLiability>();
            foreach (var type in types)
            {
                var liability = new TestLiability()
                {
                    isChargeOff = true,
                    endDerogDate = DateTime.Today,
                    liaOwnerT = E_LiaOwnerT.Borrower,
                    liabilityType = type,
                };
                liabilities.Add(liability);
            }
            creditReport.allLiabilities = new System.Collections.ArrayList(liabilities);

            var count = creditReport.ChargeOffCountX("");

            Assert.AreEqual(5, count);
        }

        [Test]
        public void ChargeOffCountXWithParams_TypeMortgage_Returns0()
        {
            TestCreditReport creditReport = new TestCreditReport();
            creditReport.aBorrowerCreditModeT = E_aBorrowerCreditModeT.Borrower;
            creditReport.allPublicRecords = new System.Collections.ArrayList();
            var types = new E_DebtRegularT[] 
            { 
                E_DebtRegularT.Installment,
                E_DebtRegularT.Open,
                E_DebtRegularT.Other,
                E_DebtRegularT.Revolving 
            };
            var liabilities = new List<TestLiability>();
            foreach (var type in types)
            {
                var liability = new TestLiability()
                {
                    isChargeOff = true,
                    endDerogDate = DateTime.Today,
                    liaOwnerT = E_LiaOwnerT.Borrower,
                    liabilityType = type,
                };
                liabilities.Add(liability);
            }
            creditReport.allLiabilities = new System.Collections.ArrayList(liabilities);

            var count = creditReport.ChargeOffCountX("t=m");

            Assert.AreEqual(0, count);
        }

        [Test]
        public void ChargeOffCountXWithParams_TypeMortgage_Returns1()
        {
            TestCreditReport creditReport = new TestCreditReport();
            creditReport.aBorrowerCreditModeT = E_aBorrowerCreditModeT.Borrower;
            creditReport.allPublicRecords = new System.Collections.ArrayList();
            var types = new E_DebtRegularT[] 
            { 
                E_DebtRegularT.Installment,
                E_DebtRegularT.Open,
                E_DebtRegularT.Other,
                E_DebtRegularT.Revolving,
                E_DebtRegularT.Mortgage
            };
            var liabilities = new List<TestLiability>();
            foreach (var type in types)
            {
                var liability = new TestLiability()
                {
                    isChargeOff = true,
                    endDerogDate = DateTime.Today,
                    liaOwnerT = E_LiaOwnerT.Borrower,
                    liabilityType = type,
                };
                liabilities.Add(liability);
            }
            creditReport.allLiabilities = new System.Collections.ArrayList(liabilities);

            var count = creditReport.ChargeOffCountX("t=m");

            Assert.AreEqual(1, count);
        }

        // This expects 1 even though there is no "Installment" liability
        // because a Mortgage liability is counted as an installment type.
        [Test]
        public void ChargeOffCountXWithParams_TypeInstallment_Returns1()
        {
            TestCreditReport creditReport = new TestCreditReport();
            creditReport.aBorrowerCreditModeT = E_aBorrowerCreditModeT.Borrower;
            creditReport.allPublicRecords = new System.Collections.ArrayList();
            var types = new E_DebtRegularT[] 
            { 
                E_DebtRegularT.Mortgage,
                E_DebtRegularT.Open,
                E_DebtRegularT.Other,
                E_DebtRegularT.Revolving 
            };
            var liabilities = new List<TestLiability>();
            foreach (var type in types)
            {
                var liability = new TestLiability()
                {
                    isChargeOff = true,
                    endDerogDate = DateTime.Today,
                    liaOwnerT = E_LiaOwnerT.Borrower,
                    liabilityType = type,
                };
                liabilities.Add(liability);
            }
            creditReport.allLiabilities = new System.Collections.ArrayList(liabilities);

            var count = creditReport.ChargeOffCountX("t=i");

            Assert.AreEqual(1, count);
        }

        // This expects 2 because a Mortgage liability is counted as an 
        // installment type.
        [Test]
        public void ChargeOffCountXWithParams_TypeInstallment_Returns2()
        {
            TestCreditReport creditReport = new TestCreditReport();
            creditReport.aBorrowerCreditModeT = E_aBorrowerCreditModeT.Borrower;
            creditReport.allPublicRecords = new System.Collections.ArrayList();
            var types = new E_DebtRegularT[] 
            { 
                E_DebtRegularT.Installment,
                E_DebtRegularT.Open,
                E_DebtRegularT.Other,
                E_DebtRegularT.Revolving,
                E_DebtRegularT.Mortgage
            };
            var liabilities = new List<TestLiability>();
            foreach (var type in types)
            {
                var liability = new TestLiability()
                {
                    isChargeOff = true,
                    endDerogDate = DateTime.Today,
                    liaOwnerT = E_LiaOwnerT.Borrower,
                    liabilityType = type,
                };
                liabilities.Add(liability);
            }
            creditReport.allLiabilities = new System.Collections.ArrayList(liabilities);

            var count = creditReport.ChargeOffCountX("t=i");

            Assert.AreEqual(2, count);
        }

        [Test]
        public void ChargeOffCountXWithParams_TypeRevolving_Returns0()
        {
            TestCreditReport creditReport = new TestCreditReport();
            creditReport.aBorrowerCreditModeT = E_aBorrowerCreditModeT.Borrower;
            creditReport.allPublicRecords = new System.Collections.ArrayList();
            var types = new E_DebtRegularT[] 
            { 
                E_DebtRegularT.Mortgage,
                E_DebtRegularT.Open,
                E_DebtRegularT.Other,
                E_DebtRegularT.Installment
            };
            var liabilities = new List<TestLiability>();
            foreach (var type in types)
            {
                var liability = new TestLiability()
                {
                    isChargeOff = true,
                    endDerogDate = DateTime.Today,
                    liaOwnerT = E_LiaOwnerT.Borrower,
                    liabilityType = type,
                };
                liabilities.Add(liability);
            }
            creditReport.allLiabilities = new System.Collections.ArrayList(liabilities);

            var count = creditReport.ChargeOffCountX("t=r");

            Assert.AreEqual(0, count);
        }

        [Test]
        public void ChargeOffCountXWithParams_TypeRevolving_Returns1()
        {
            TestCreditReport creditReport = new TestCreditReport();
            creditReport.aBorrowerCreditModeT = E_aBorrowerCreditModeT.Borrower;
            creditReport.allPublicRecords = new System.Collections.ArrayList();
            var types = new E_DebtRegularT[] 
            { 
                E_DebtRegularT.Installment,
                E_DebtRegularT.Open,
                E_DebtRegularT.Other,
                E_DebtRegularT.Revolving,
                E_DebtRegularT.Mortgage
            };
            var liabilities = new List<TestLiability>();
            foreach (var type in types)
            {
                var liability = new TestLiability()
                {
                    isChargeOff = true,
                    endDerogDate = DateTime.Today,
                    liaOwnerT = E_LiaOwnerT.Borrower,
                    liabilityType = type,
                };
                liabilities.Add(liability);
            }
            creditReport.allLiabilities = new System.Collections.ArrayList(liabilities);

            var count = creditReport.ChargeOffCountX("t=r");

            Assert.AreEqual(1, count);
        }

        [Test]
        public void ChargeOffCountXWithParams_TypeOpen_Returns0()
        {
            TestCreditReport creditReport = new TestCreditReport();
            creditReport.aBorrowerCreditModeT = E_aBorrowerCreditModeT.Borrower;
            creditReport.allPublicRecords = new System.Collections.ArrayList();
            var types = new E_DebtRegularT[] 
            { 
                E_DebtRegularT.Mortgage,
                E_DebtRegularT.Revolving,
                E_DebtRegularT.Other,
                E_DebtRegularT.Installment
            };
            var liabilities = new List<TestLiability>();
            foreach (var type in types)
            {
                var liability = new TestLiability()
                {
                    isChargeOff = true,
                    endDerogDate = DateTime.Today,
                    liaOwnerT = E_LiaOwnerT.Borrower,
                    liabilityType = type,
                };
                liabilities.Add(liability);
            }
            creditReport.allLiabilities = new System.Collections.ArrayList(liabilities);

            var count = creditReport.ChargeOffCountX("t=o");

            Assert.AreEqual(0, count);
        }

        [Test]
        public void ChargeOffCountXWithParams_TypeOpen_Returns1()
        {
            TestCreditReport creditReport = new TestCreditReport();
            creditReport.aBorrowerCreditModeT = E_aBorrowerCreditModeT.Borrower;
            creditReport.allPublicRecords = new System.Collections.ArrayList();
            var types = new E_DebtRegularT[] 
            { 
                E_DebtRegularT.Installment,
                E_DebtRegularT.Open,
                E_DebtRegularT.Other,
                E_DebtRegularT.Revolving,
                E_DebtRegularT.Mortgage
            };
            var liabilities = new List<TestLiability>();
            foreach (var type in types)
            {
                var liability = new TestLiability()
                {
                    isChargeOff = true,
                    endDerogDate = DateTime.Today,
                    liaOwnerT = E_LiaOwnerT.Borrower,
                    liabilityType = type,
                };
                liabilities.Add(liability);
            }
            creditReport.allLiabilities = new System.Collections.ArrayList(liabilities);

            var count = creditReport.ChargeOffCountX("t=o");

            Assert.AreEqual(1, count);
        }

        [Test]
        public void ChargeOffCountXWithParams_TypeOther_Returns0()
        {
            TestCreditReport creditReport = new TestCreditReport();
            creditReport.aBorrowerCreditModeT = E_aBorrowerCreditModeT.Borrower;
            creditReport.allPublicRecords = new System.Collections.ArrayList();
            var types = new E_DebtRegularT[] 
            { 
                E_DebtRegularT.Mortgage,
                E_DebtRegularT.Revolving,
                E_DebtRegularT.Open,
                E_DebtRegularT.Installment
            };
            var liabilities = new List<TestLiability>();
            foreach (var type in types)
            {
                var liability = new TestLiability()
                {
                    isChargeOff = true,
                    endDerogDate = DateTime.Today,
                    liaOwnerT = E_LiaOwnerT.Borrower,
                    liabilityType = type,
                };
                liabilities.Add(liability);
            }
            creditReport.allLiabilities = new System.Collections.ArrayList(liabilities);

            var count = creditReport.ChargeOffCountX("t=u");

            Assert.AreEqual(0, count);
        }

        [Test]
        public void ChargeOffCountXWithParams_TypeOther_Returns1()
        {
            TestCreditReport creditReport = new TestCreditReport();
            creditReport.aBorrowerCreditModeT = E_aBorrowerCreditModeT.Borrower;
            creditReport.allPublicRecords = new System.Collections.ArrayList();
            var types = new E_DebtRegularT[] 
            { 
                E_DebtRegularT.Installment,
                E_DebtRegularT.Open,
                E_DebtRegularT.Other,
                E_DebtRegularT.Revolving,
                E_DebtRegularT.Mortgage
            };
            var liabilities = new List<TestLiability>();
            foreach (var type in types)
            {
                var liability = new TestLiability()
                {
                    isChargeOff = true,
                    endDerogDate = DateTime.Today,
                    liaOwnerT = E_LiaOwnerT.Borrower,
                    liabilityType = type,
                };
                liabilities.Add(liability);
            }
            creditReport.allLiabilities = new System.Collections.ArrayList(liabilities);

            var count = creditReport.ChargeOffCountX("t=u");

            Assert.AreEqual(1, count);
        }

        [Test]
        public void ChargeOffCountXWithParams_TypeRevolvingOrMortgage_Returns3()
        {
            TestCreditReport creditReport = new TestCreditReport();
            creditReport.aBorrowerCreditModeT = E_aBorrowerCreditModeT.Borrower;
            creditReport.allPublicRecords = new System.Collections.ArrayList();
            var types = new E_DebtRegularT[] 
            { 
                E_DebtRegularT.Mortgage,
                E_DebtRegularT.Mortgage,
                E_DebtRegularT.Revolving,
                E_DebtRegularT.Other,
                E_DebtRegularT.Installment
            };
            var liabilities = new List<TestLiability>();
            foreach (var type in types)
            {
                var liability = new TestLiability()
                {
                    isChargeOff = true,
                    endDerogDate = DateTime.Today,
                    liaOwnerT = E_LiaOwnerT.Borrower,
                    liabilityType = type,
                };
                liabilities.Add(liability);
            }
            creditReport.allLiabilities = new System.Collections.ArrayList(liabilities);

            var count = creditReport.ChargeOffCountX("t=rm");

            Assert.AreEqual(3, count);
        }

        [Test]
        public void ChargeOffCountXWithParams_TypeRevolvingOrMortgageOrOpen_Returns3()
        {
            TestCreditReport creditReport = new TestCreditReport();
            creditReport.aBorrowerCreditModeT = E_aBorrowerCreditModeT.Borrower;
            creditReport.allPublicRecords = new System.Collections.ArrayList();
            var types = new E_DebtRegularT[] 
            { 
                E_DebtRegularT.Installment,
                E_DebtRegularT.Open,
                E_DebtRegularT.Other,
                E_DebtRegularT.Revolving,
                E_DebtRegularT.Mortgage
            };
            var liabilities = new List<TestLiability>();
            foreach (var type in types)
            {
                var liability = new TestLiability()
                {
                    isChargeOff = true,
                    endDerogDate = DateTime.Today,
                    liaOwnerT = E_LiaOwnerT.Borrower,
                    liabilityType = type,
                };
                liabilities.Add(liability);
            }
            creditReport.allLiabilities = new System.Collections.ArrayList(liabilities);

            var count = creditReport.ChargeOffCountX("t=rmo");

            Assert.AreEqual(3, count);
        }

        [Test]
        public void ChargeOffCountXWithParams_Within12MonthsWithAdverseRatingsOfOtherType_Returns0()
        {
            TestCreditReport creditReport = new TestCreditReport();
            creditReport.aBorrowerCreditModeT = E_aBorrowerCreditModeT.Borrower;
            creditReport.allPublicRecords = new System.Collections.ArrayList();
            var liabilities = new List<TestLiability>();
            var adverseRatingList = new List<CreditAdverseRating>();
            adverseRatingList.Add(new CreditAdverseRating(E_AdverseType.Late120, DateTime.Today));
            var liability = new TestLiability()
            {
                isChargeOff = true,
                endDerogDate = DateTime.Today.AddMonths(13),
                liaOwnerT = E_LiaOwnerT.Borrower,
                liabilityType = E_DebtRegularT.Mortgage,
                creditAdverseRatingList = new System.Collections.ArrayList(adverseRatingList)
            };
            liabilities.Add(liability);
            creditReport.allLiabilities = new System.Collections.ArrayList(liabilities);

            var count = creditReport.ChargeOffCountX("within<=12");

            Assert.AreEqual(0, count);
        }

        // If there's no adverse rating list for a liability, it will count it,
        // so this should return 1.
        [Test]
        public void ChargeOffCountXWithParams_Within12MonthsNoAdverseRatings_Returns1()
        {
            TestCreditReport creditReport = new TestCreditReport();
            creditReport.aBorrowerCreditModeT = E_aBorrowerCreditModeT.Borrower;
            creditReport.allPublicRecords = new System.Collections.ArrayList();
            var liabilities = new List<TestLiability>();
            var liability = new TestLiability()
            {
                isChargeOff = true,
                endDerogDate = DateTime.Today.AddMonths(13),
                liaOwnerT = E_LiaOwnerT.Borrower,
                liabilityType = E_DebtRegularT.Mortgage,
                creditAdverseRatingList = new System.Collections.ArrayList()
            };
            liabilities.Add(liability);
            creditReport.allLiabilities = new System.Collections.ArrayList(liabilities);

            var count = creditReport.ChargeOffCountX("within<=12");

            Assert.AreEqual(1, count);
        }

        [Test]
        public void ChargeOffCountXWithParams_Within12MonthsWithChargeOffAdverseRating_Returns1()
        {
            TestCreditReport creditReport = new TestCreditReport();
            creditReport.aBorrowerCreditModeT = E_aBorrowerCreditModeT.Borrower;
            creditReport.allPublicRecords = new System.Collections.ArrayList();
            var liabilities = new List<TestLiability>();
            var adverseRatingList = new List<CreditAdverseRating>();
            adverseRatingList.Add(new CreditAdverseRating(E_AdverseType.ChargeOffOrCollection, DateTime.Today));
            var liability = new TestLiability()
            {
                isChargeOff = true,
                endDerogDate = DateTime.Today.AddMonths(12),
                liaOwnerT = E_LiaOwnerT.Borrower,
                liabilityType = E_DebtRegularT.Mortgage,
                creditAdverseRatingList = new System.Collections.ArrayList(adverseRatingList)
            };
            liabilities.Add(liability);
            creditReport.allLiabilities = new System.Collections.ArrayList(liabilities);

            var count = creditReport.ChargeOffCountX("within<=12");

            Assert.AreEqual(1, count);
        }

        [Test]
        public void TestCreditPercentile()
        {
            ICreditReport report = CreditTestingUtils.LoadReportFromTestData("opm10299/mismo.xml");
            Assert.AreEqual(report.BorrowerScore.EquifaxPercentile, 68);
            Assert.AreEqual(report.BorrowerScore.TransUnionPercentile, 91);

            report = CreditTestingUtils.LoadReportFromTestData("opm10299/mcl.xml");
            Assert.AreEqual(report.BorrowerScore.EquifaxPercentile, 51);

            report = CreditTestingUtils.LoadReportFromTestData("SampleCreditReports/Test_kroll_mismo.xml");
            Assert.AreEqual(report.BorrowerScore.EquifaxPercentile, 38);
            Assert.AreEqual(report.BorrowerScore.TransUnionPercentile, 31);
            Assert.AreEqual(report.BorrowerScore.ExperianPercentile, 32);
        }

        public class TestCreditReport : AbstractCreditReport
        {
            public DateTime creditReportFirstIssuedDate;
            public override DateTime CreditReportFirstIssuedDate
            {
                get { return this.creditReportFirstIssuedDate; }
            }

            public DateTime creditReportLastUpdatedDate;
            public override DateTime CreditReportLastUpdatedDate
            {
                get { return this.creditReportLastUpdatedDate; }
            }

            public CreditScore borrowerScore;
            public override CreditScore BorrowerScore 
            {
                get { return borrowerScore; }
            }

            public CreditScore coborrowerScore;
            public override CreditScore CoborrowerScore 
            {
                get { return coborrowerScore; }
            }

            public BorrowerInfo borrowerInfo;
            public override BorrowerInfo BorrowerInfo 
            {
                get { return borrowerInfo; }
            }

            public BorrowerInfo coborrowerInfo;
            public override BorrowerInfo CoborrowerInfo 
            {
                get { return coborrowerInfo; }
            }

            public CreditBureauInformation experianContactInformation;
            public override CreditBureauInformation ExperianContactInformation 
            {
                get { return experianContactInformation; }
            }

            public CreditBureauInformation transUnionContactInformation;
            public override CreditBureauInformation TransUnionContactInformation 
            {
                get { return transUnionContactInformation; }
            }

            public CreditBureauInformation equifaxContactInformation;
            public override CreditBureauInformation EquifaxContactInformation 
            {
                get { return equifaxContactInformation; }
            }

            public string creditRatingCodeType;
            public override string CreditRatingCodeType 
            {
                get { return creditRatingCodeType; }
            }

            public bool swapPrimaryBorrower;
            public override bool SwapPrimaryBorrower 
            {
                get { return swapPrimaryBorrower; }
            }

            public CAppBase dataApp;
            public override CAppBase DataApp 
            {
                get { return dataApp; }
            }

            public System.Collections.ArrayList liabilitiesExcludeFromUnderwriting;
            public override System.Collections.ArrayList LiabilitiesExcludeFromUnderwriting 
            { 
                get { return liabilitiesExcludeFromUnderwriting; }
            }

            public System.Collections.ArrayList allLiabilities;
            public override System.Collections.ArrayList AllLiabilities 
            {
                get { return allLiabilities; }
            }

            public System.Collections.ArrayList allPublicRecords;
            public override System.Collections.ArrayList AllPublicRecords 
            {
                get { return allPublicRecords; }
            }

            public override void SetLoanData(CAppBase dataApp)
            {
                throw new NotImplementedException();
            }

            public override void Parse()
            {
                throw new NotImplementedException();
            }

            public override string GetLiabilityXml(string id)
            {
                throw new NotImplementedException();
            }

            public override string GetPublicRecordXml(string id)
            {
                throw new NotImplementedException();
            }
        }

        public class TestLiability : AbstractCreditLiability
        {
            public string id;
            public override string ID 
            {
                get { return id; }
            }

            public string creditorName;
            public override string CreditorName 
            {
                get { return creditorName; }
            }

            public string creditorAddress;
            public override string CreditorAddress 
            {
                get { return creditorAddress; }
            }

            public string creditorCity;
            public override string CreditorCity 
            {
                get { return creditorCity; }
            }

            public string creditorState;
            public override string CreditorState 
            {
                get { return creditorState; }
            }

            public string creditorZipcode;
            public override string CreditorZipcode 
            {
                get { return creditorZipcode; }
            }

            public string creditorPhone;
            public override string CreditorPhone 
            {
                get { return creditorPhone; }
            }

            public DateTime accountOpenedDate;
            public override DateTime AccountOpenedDate 
            {
                get { return accountOpenedDate; }
            }

            public DateTime accountReportedDate;
            public override DateTime AccountReportedDate 
            {
                get { return accountReportedDate; }
            }

            public decimal pastDueAmount;
            public override decimal PastDueAmount 
            {
                get { return pastDueAmount; }
            }

            public decimal highCreditAmount;
            public override decimal HighCreditAmount 
            {
                get { return highCreditAmount; }
            }

            public string accountIdentifier;
            public override string AccountIdentifier 
            {
                get { return accountIdentifier; }
            }

            public string termsMonthsCount;
            public override string TermsMonthsCount 
            {
                get { return termsMonthsCount; }
            }

            public decimal unpaidBalanceAmount;
            public override decimal UnpaidBalanceAmount 
            {
                get { return unpaidBalanceAmount; }
            }

            public override void UpdateUnpaidBalanceAmount(decimal value)
            {
                throw new NotImplementedException();
            }

            public string monthsRemainingCount;
            public override string MonthsRemainingCount 
            {
                get { return monthsRemainingCount; }
            }

            public decimal monthlyPaymentAmount;
            public override decimal MonthlyPaymentAmount 
            {
                get { return monthlyPaymentAmount; }
            }

            public override void UpdateMonthlyPaymentAmount(decimal value)
            {
                throw new NotImplementedException();
            }

            public bool isModified;
            public override bool IsModified 
            {
                get { return isModified; }
            }

            public bool isDischargedBk;
            public override bool IsDischargedBk 
            {
                get { return isDischargedBk; }
            }

            public string modifiedString;
            public override string ModifiedString 
            {
                get { return modifiedString; }
            }

            public bool isAuthorizedUser;
            public override bool IsAuthorizedUser 
            {
                get { return isAuthorizedUser; }
            }

            public bool isStudentLoansNotInRepayment;
            public override bool IsStudentLoansNotInRepayment 
            {
                get { return isStudentLoansNotInRepayment; }
            }

            public bool isGovMiscDebt;
            public override bool IsGovMiscDebt 
            {
                get { return isGovMiscDebt; }
            }

            public string late30;
            public override string Late30 
            {
                get { return late30; }
            }

            public string late60;
            public override string Late60 
            {
                get { return late60; }
            }

            public string late90;
            public override string Late90 
            {
                get { return late90; }
            }

            public string late120;
            public override string Late120 
            {
                get { return late120; }
            }

            public int monthsReviewedCount;
            public override int MonthsReviewedCount 
            {
                get { return monthsReviewedCount; }
            }

            public DateTime lastActivityDate;
            public override DateTime LastActivityDate 
            {
                get { return lastActivityDate; }
            }

            public E_DebtRegularT liabilityType;
            public override E_DebtRegularT LiabilityType 
            {
                get { return liabilityType; }
            }

            public E_LiaOwnerT liaOwnerT;
            public override E_LiaOwnerT LiaOwnerT 
            {
                get { return liaOwnerT; }
            }

            public bool isForeclosure;
            public override bool IsForeclosure 
            {
                get { return isForeclosure; }
            }

            public bool isBankruptcy;
            public override bool IsBankruptcy 
            {
                get { return isBankruptcy; }
            }

            public bool isChargeOff;
            public override bool IsChargeOff 
            {
                get { return isChargeOff; }
            }

            public DateTime paymentPatternStartDate;
            public override DateTime PaymentPatternStartDate 
            {
                get { return paymentPatternStartDate; }
            }

            public bool isCollection;
            public override bool IsCollection 
            {
                get { return isCollection; }
            }

            public bool isRepos;
            public override bool IsRepos 
            {
                get { return isRepos; }
            }

            public bool isInCreditCounseling;
            public override bool IsInCreditCounseling 
            {
                get { return isInCreditCounseling; }
            }

            public bool isLate;
            public override bool IsLate 
            {
                get { return isLate; }
            }

            public System.Collections.ArrayList creditAdverseRatingList;
            public override System.Collections.ArrayList CreditAdverseRatingList 
            {
                get { return creditAdverseRatingList; }
            }

            public bool isMedicalTradeline;
            public override bool IsMedicalTradeline 
            {
                get { return isMedicalTradeline; }
            }

            public bool isActive;
            public override bool IsActive 
            {
                get { return isActive; }
            }

            public bool hasBeenDerog;
            public override bool HasBeenDerog 
            {
                get { return hasBeenDerog; }
            }

            public bool isDerogCurrently;
            public override bool IsDerogCurrently 
            {
                get { return isDerogCurrently; }
            }

            public bool isLastStatusDerog;
            public override bool IsLastStatusDerog 
            {
                get { return isLastStatusDerog; }
            }

            public bool hasTerminalDerog;
            public override bool HasTerminalDerog 
            {
                get { return hasTerminalDerog; }
            }

            public DateTime endDerogDate;
            public override DateTime EndDerogDate
            {
                get { return endDerogDate; }
            }
        }

        public class TestPublicRecord : AbstractCreditPublicRecord
        {
            public string id;
            public override string ID 
            {
                get { return id; }
            }

            public DateTime reportedDate;
            public override DateTime ReportedDate 
            {
                get { return reportedDate; }
            }

            public DateTime dispositionDate;
            public override DateTime DispositionDate 
            {
                get { return dispositionDate; }
            }

            public E_CreditPublicRecordDispositionType dispositionType;
            public override E_CreditPublicRecordDispositionType DispositionType 
            {
                get { return dispositionType; }
            }

            public decimal bankruptcyLiabilitiesAmount;
            public override decimal BankruptcyLiabilitiesAmount 
            {
                get { return bankruptcyLiabilitiesAmount; }
            }

            public E_CreditPublicRecordType type;
            public override E_CreditPublicRecordType Type 
            {
                get { return type; }
            }

            public E_PublicRecordOwnerT publicRecordOwnerT;
            public override E_PublicRecordOwnerT PublicRecordOwnerT 
            {
                get { return publicRecordOwnerT; }
            }

            public DateTime lastEffectiveDate;
            public override DateTime LastEffectiveDate 
            {
                get { return lastEffectiveDate; }
            }

            public DateTime bkFileDate;
            public override DateTime BkFileDate 
            {
                get { return bkFileDate; }
            }

            public string courtName;
            public override string CourtName 
            {
                get { return courtName; }
            }
        }
	}
}
