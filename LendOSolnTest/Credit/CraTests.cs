﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using LendersOffice.CreditReport;
using LendersOffice.CreditReport.Mcl;
using LendersOffice.Constants;
using CommonLib;
using System.Xml;
using DataAccess;
using LendOSolnTest.Common;


using System.Linq;
using System.Text;


namespace LendOSolnTest.Credit
{
    [TestFixture] 
    public class CRAInfoTests
    {
        private static Guid x_craID = new Guid("35b20280-4b69-4ea1-9ddd-8a1883139f1f");
        private Guid x_brokerId;

        [TestFixtureSetUp]
        public void Setup()
        {
            x_brokerId = LoginTools.LoginAs(TestAccountType.LoanOfficer).BrokerId;
        }

        [Test]
        public void MclBetaCRARetrival()
        {
            CheckMclCra( MasterCRAList.FindById(x_craID, false) );

        }
        private void CheckMclCra(CRA cra)
        {

            CRA currentCra = MasterCRAList.FindById(x_craID, true, x_brokerId);
            Assert.That(currentCra, Is.Not.Null);
            Assert.That(currentCra.ID, Is.EqualTo(x_craID));
            Assert.That(currentCra.Protocol, Is.EqualTo(CreditReportProtocol.Mcl));
            Assert.That(currentCra.ProviderId, Is.EqualTo("CC"));
        }

        private void CheckLenderMclCraInfo(CRA cra)
        {
            LenderMappedCRA a = cra as LenderMappedCRA;
            Assert.That(a, Is.Not.Null);
            Assert.That(a.Username, Is.EqualTo ( "pml_test" )); 
            Assert.That(a.Password, Is.EqualTo("zyxzyx13")); 
            Assert.That(a.Protocol, Is.EqualTo(CreditReportProtocol.Mcl ));
            Assert.That(a.IncludeEquifax, Is.EqualTo(true));
            Assert.That(a.IncludeExperian, Is.EqualTo(true));
            Assert.That(a.IncludeFannie, Is.EqualTo(true)); 
     
        }
        [Test]
        public void MclBetaCRARetrivalF()
        {
            CRA a = MasterCRAList.FindById(x_craID, false, x_brokerId);
            Assert.That(a, Is.Not.TypeOf(typeof(LenderMappedCRA))); 
            CheckMclCra(a); 

        }
        [Test]
        public void MclBetaCRARetrivalT()
        {
            CRA a = MasterCRAList.FindById(x_craID, true, x_brokerId);
            Assert.That(a, Is.Not.TypeOf(typeof(LenderMappedCRA)));
            CheckMclCra(a); 
        }

        [Test]
        public void MclBetaLenderMapCRARetrivalT()
        {
            bool found = false;
            foreach (CRA car in MasterCRAList.RetrieveAvailableCras(x_brokerId))
            {
                if ((car as LenderMappedCRA )==null)
                {
                    CheckMclCra(car);
                    found = true;
                    break;
                }
            }
            Assert.That(found, Is.EqualTo(true), "lender mapped cra not found for current broker");
        }

        [Test]
        public void MclBetaLenderMapCRARetrivalF()
        {
            CRA a = MasterCRAList.FindById(x_craID, true, x_brokerId);
            Assert.That(a, Is.Not.TypeOf(typeof(LenderMappedCRA)));
            CheckMclCra(a);
        }
    }
}
