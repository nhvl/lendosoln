﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using LendersOffice.CreditReport;
using DataAccess;
namespace LendOSolnTest.Credit
{
    [TestFixture]
    public class BorrowerCreditModeTest
    {
        [Test]
        public void TradeCountTest_TestCredit01()
        {
            ICreditReport creditReport = CreditTestingUtils.LoadReportFromTestData("SampleCreditReports/TestCredit01.xml.config");

            var testData = new[] {
                               new { Mode = E_aBorrowerCreditModeT.Borrower, Expected=14}
                               , new { Mode = E_aBorrowerCreditModeT.Coborrower, Expected=0}
                               , new { Mode = E_aBorrowerCreditModeT.Both, Expected=14}
                           };

            foreach (var test in testData)
            {
                creditReport.aBorrowerCreditModeT = test.Mode;
                Assert.AreEqual(test.Expected, creditReport.TradeCount, creditReport.aBorrowerCreditModeT.ToString());
            }

        }
        [Test]
        public void TradeCountTest_TestCredit03()
        {
            var testData = new[] {
                               new { Mode = E_aBorrowerCreditModeT.Borrower, Expected=16}
                               , new { Mode = E_aBorrowerCreditModeT.Coborrower, Expected=15}
                               , new { Mode = E_aBorrowerCreditModeT.Both, Expected=29}
                           };

            ICreditReport creditReport = CreditTestingUtils.LoadReportFromTestData("SampleCreditReports/TestCredit03.xml.config");

            foreach (var test in testData)
            {
                creditReport.aBorrowerCreditModeT = test.Mode;
                Assert.AreEqual(test.Expected, creditReport.TradeCount, creditReport.aBorrowerCreditModeT.ToString());
            }

        }

        [Test]
        public void RevolveTradeCountOlderThan24_TestCredit01()
        {
            ICreditReport creditReport = CreditTestingUtils.LoadReportFromTestData("SampleCreditReports/TestCredit01.xml.config");

            var testData = new[] {
                               new { Mode = E_aBorrowerCreditModeT.Borrower, Expected=11} //Temp Fix. Needs to be updated (previous value: 9).    //8
                               , new { Mode = E_aBorrowerCreditModeT.Coborrower, Expected=0}
                               , new { Mode = E_aBorrowerCreditModeT.Both, Expected=11} //Temp Fix. Needs to be updated (previous value: 9).      //8
                           };

            foreach (var test in testData)
            {
                creditReport.aBorrowerCreditModeT = test.Mode;
                Assert.AreEqual(test.Expected, creditReport.RevolveTradeCountOlderThan(24), creditReport.aBorrowerCreditModeT.ToString());
            }

        }
        [Test]
        public void RevolveTradeCountOlderThan24_TestCredit03()
        {
            ICreditReport creditReport = CreditTestingUtils.LoadReportFromTestData("SampleCreditReports/TestCredit03.xml.config");

            var testData = new[] {
                               new { Mode = E_aBorrowerCreditModeT.Borrower, Expected=13} //Temp Fix. Needs to be updated (previous value: 11).      //9
                               , new { Mode = E_aBorrowerCreditModeT.Coborrower, Expected=10}
                               , new { Mode = E_aBorrowerCreditModeT.Both, Expected=21} //Temp Fix. Needs to be updated (previous value: 19).   //17
                           };

            foreach (var test in testData)
            {
                creditReport.aBorrowerCreditModeT = test.Mode;
                Assert.AreEqual(test.Expected, creditReport.RevolveTradeCountOlderThan(24), creditReport.aBorrowerCreditModeT.ToString());
            }

        }
        [Test]
        public void TestCredit03()
        {
            ICreditReport creditReport = CreditTestingUtils.LoadReportFromTestData("SampleCreditReports/TestCredit03.xml.config");

            var testData = new[] {
                               new { Mode = E_aBorrowerCreditModeT.Borrower, Expected=0}
                               , new { Mode = E_aBorrowerCreditModeT.Coborrower, Expected=4}
                               , new { Mode = E_aBorrowerCreditModeT.Both, Expected=4}
                           };

            foreach (var test in testData)
            {
                creditReport.aBorrowerCreditModeT = test.Mode;
                Assert.AreEqual(test.Expected, creditReport.MajorDerogCountX(""), creditReport.aBorrowerCreditModeT.ToString());
            }

        }
    }
}
