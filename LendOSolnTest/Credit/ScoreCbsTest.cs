﻿using System;
using System.Collections.Generic;
using DataAccess;
using LendOSolnTest.Common;
using LendOSolnTest.Fakes;
using NUnit.Framework;

namespace LendOSolnTest.Credit
{
    [TestFixture]
    public class ScoreCbsTest
    {
        [TestFixtureSetUp]
        public void OnSetup()
        {
            LoginTools.LoginAs(TestAccountType.LoTest001);
        }

        [Test]
        public void CBTest()
        {
            // Some simple score tests to make sure expected scores are returned.

            CPageData data = FakePageData.Create();
            data.InitLoad();
            data.CalcModeT = E_CalcModeT.PriceMyLoan;
            CAppData dataApp = data.GetAppData(0);
            string parameters;

            // Single borrower, 3 scores reported
            dataApp.aBSsn = "111-11-1111";
            dataApp.aBEquifaxScorePe_rep = "610"; 
            dataApp.aBExperianScorePe_rep = "611";
            dataApp.aBTransUnionScorePe_rep = "612";

            dataApp.aCSsn = "";
            dataApp.aCEquifaxScorePe_rep = "";
            dataApp.aCExperianScorePe_rep = "";
            dataApp.aCTransUnionScorePe_rep = "";

            // Isolating the Credit Bureaus
            parameters = "CB=E";
            Assert.That(dataApp.ScoreCBS(parameters), Is.EqualTo(610));
            parameters = "CB=X";
            Assert.That(dataApp.ScoreCBS(parameters), Is.EqualTo(611));
            parameters = "CB=T";
            Assert.That(dataApp.ScoreCBS(parameters), Is.EqualTo(612));

            // Single borrower, 2 scores reported
            dataApp.aBSsn = "111-11-1111";
            dataApp.aBEquifaxScorePe_rep = "0";
            dataApp.aBExperianScorePe_rep = "611";
            dataApp.aBTransUnionScorePe_rep = "612";

            dataApp.aCSsn = "";
            dataApp.aCEquifaxScorePe_rep = "";
            dataApp.aCExperianScorePe_rep = "";
            dataApp.aCTransUnionScorePe_rep = "";

            // Isolating the Credit Bureaus
            parameters = "CB=E";
            Assert.That(dataApp.ScoreCBS(parameters), Is.EqualTo(0));
            parameters = "CB=X";
            Assert.That(dataApp.ScoreCBS(parameters), Is.EqualTo(611));
            parameters = "CB=T";
            Assert.That(dataApp.ScoreCBS(parameters), Is.EqualTo(612));

            // Single borrower, 1 score reported
            dataApp.aBSsn = "111-11-1111";
            dataApp.aBEquifaxScorePe_rep = "0";
            dataApp.aBExperianScorePe_rep = "0";
            dataApp.aBTransUnionScorePe_rep = "612";

            dataApp.aCSsn = "";
            dataApp.aCEquifaxScorePe_rep = "";
            dataApp.aCExperianScorePe_rep = "";
            dataApp.aCTransUnionScorePe_rep = "";

            // Isolating the Credit Bureaus
            parameters = "CB=E";
            Assert.That(dataApp.ScoreCBS(parameters), Is.EqualTo(0));
            parameters = "CB=X";
            Assert.That(dataApp.ScoreCBS(parameters), Is.EqualTo(0));
            parameters = "CB=T";
            Assert.That(dataApp.ScoreCBS(parameters), Is.EqualTo(612));

            // Single borrower, 0 scores reported
            dataApp.aBSsn = "111-11-1111";
            dataApp.aBEquifaxScorePe_rep = "0";
            dataApp.aBExperianScorePe_rep = "0";
            dataApp.aBTransUnionScorePe_rep = "0";

            dataApp.aCSsn = "";
            dataApp.aCEquifaxScorePe_rep = "";
            dataApp.aCExperianScorePe_rep = "";
            dataApp.aCTransUnionScorePe_rep = "";

            // Isolating the Credit Bureaus
            parameters = "CB=E";
            Assert.That(dataApp.ScoreCBS(parameters), Is.EqualTo(0));
            parameters = "CB=X";
            Assert.That(dataApp.ScoreCBS(parameters), Is.EqualTo(0));
            parameters = "CB=T";
            Assert.That(dataApp.ScoreCBS(parameters), Is.EqualTo(0));
        }

        [Test]
        public void B2Test()
        {
            CPageData data = FakePageData.Create();
            data.InitLoad();
            data.CalcModeT = E_CalcModeT.PriceMyLoan;
            CAppData dataApp = data.GetAppData(0);
            string parameters;

            // Single borrower, 3 scores reported
            dataApp.aBSsn = "111-11-1111";
            dataApp.aBEquifaxScorePe_rep = "610";
            dataApp.aBExperianScorePe_rep = "611";
            dataApp.aBTransUnionScorePe_rep = "612";

            dataApp.aCSsn = "";
            dataApp.aCEquifaxScorePe_rep = "";
            dataApp.aCExperianScorePe_rep = "";
            dataApp.aCTransUnionScorePe_rep = "";

            parameters = "CB=EX&B2=L";
            Assert.That(dataApp.ScoreCBS(parameters), Is.EqualTo(610));
            parameters = "CB=EX&B2=H";
            Assert.That(dataApp.ScoreCBS(parameters), Is.EqualTo(611));

            // Single borrower, 2 scores reported
            dataApp.aBSsn = "111-11-1111";
            dataApp.aBEquifaxScorePe_rep = "0";
            dataApp.aBExperianScorePe_rep = "611";
            dataApp.aBTransUnionScorePe_rep = "612";

            dataApp.aCSsn = "";
            dataApp.aCEquifaxScorePe_rep = "";
            dataApp.aCExperianScorePe_rep = "";
            dataApp.aCTransUnionScorePe_rep = "";

            parameters = "CB=ET&B2=L";
            Assert.That(dataApp.ScoreCBS(parameters), Is.EqualTo(612));
            parameters = "CB=ET&B2=H";
            Assert.That(dataApp.ScoreCBS(parameters), Is.EqualTo(612));
            parameters = "CB=EXT&B2=L";
            Assert.That(dataApp.ScoreCBS(parameters), Is.EqualTo(611));
            parameters = "CB=EXT&B2=H";
            Assert.That(dataApp.ScoreCBS(parameters), Is.EqualTo(612));
        }

        [Test]
        public void B3Test()
        {
            CPageData data = FakePageData.Create();
            data.InitLoad();
            data.CalcModeT = E_CalcModeT.PriceMyLoan;
            CAppData dataApp = data.GetAppData(0);
            string parameters;

            dataApp.aBSsn = "111-11-1111";
            dataApp.aBEquifaxScorePe_rep = "610";
            dataApp.aBExperianScorePe_rep = "611";
            dataApp.aBTransUnionScorePe_rep = "612";

            dataApp.aCSsn = "";
            dataApp.aCEquifaxScorePe_rep = "";
            dataApp.aCExperianScorePe_rep = "";
            dataApp.aCTransUnionScorePe_rep = "";

            parameters = "CB=EXT&B3=L";
            Assert.That(dataApp.ScoreCBS(parameters), Is.EqualTo(610));
            parameters = "CB=EXT&B3=M";
            Assert.That(dataApp.ScoreCBS(parameters), Is.EqualTo(611));
            parameters = "CB=EXT&B3=H";
            Assert.That(dataApp.ScoreCBS(parameters), Is.EqualTo(612));

        }

        [Test]
        public void SoftTest()
        {
            CPageData data = FakePageData.Create();
            data.InitLoad();
            data.CalcModeT = E_CalcModeT.PriceMyLoan;
            CAppData dataApp = data.GetAppData(0);
            string parameters;

            dataApp.aBSsn = "111-11-1111";
            dataApp.aBEquifaxScorePe_rep = "610";
            dataApp.aBExperianScorePe_rep = "611";
            dataApp.aBTransUnionScorePe_rep = "612";

            dataApp.aCSsn = "222-22-2222";
            dataApp.aCEquifaxScorePe_rep = "0";
            dataApp.aCExperianScorePe_rep = "0";
            dataApp.aCTransUnionScorePe_rep = "0";

            parameters = "CB=EXT&Soft=Y";
            Assert.That(dataApp.ScoreCBS(parameters), Is.EqualTo(611));

            parameters = "CB=EXT&Soft=N";
            Assert.That(dataApp.ScoreCBS(parameters), Is.EqualTo(0));
        }

        [Test]
        public void CScoreTest()
        {
            CPageData data = FakePageData.Create();
            data.InitLoad();
            data.CalcModeT = E_CalcModeT.PriceMyLoan;
            CAppData dataApp = data.GetAppData(0);
            string parameters;

            dataApp.aBSsn = "111-11-1111";
            dataApp.aBEquifaxScorePe_rep = "610";
            dataApp.aBExperianScorePe_rep = "611";
            dataApp.aBTransUnionScorePe_rep = "612";

            dataApp.aCSsn = "222-22-2222";
            dataApp.aCEquifaxScorePe_rep = "710";
            dataApp.aCExperianScorePe_rep = "711";
            dataApp.aCTransUnionScorePe_rep = "712";

            parameters = "CScore=H";
            Assert.That(dataApp.ScoreCBS(parameters), Is.EqualTo(711));

            parameters = "CScore=L";
            Assert.That(dataApp.ScoreCBS(parameters), Is.EqualTo(611));
        }

        [Test]
        public void DefaultsTest()
        {
            // Verify that the default values given in the spec for case 201266
            // are returned when no parameters are provided.

            bool equifax, experian, transUnion, pwe, soft;
            string b2, b3, cScore;

            CAppBase.ScoreCBSParseParameters("",
                false, //verifyParams
                out equifax,
                out experian,
                out transUnion,
                out b2,          // Individual Score Determiner
                out b3,          // Individual Score Determiner
                out pwe,         // Primary Wage Earner
                out cScore,      // Combined Score
                out soft);       // Soft Score

            Assert.That(equifax, Is.True);
            Assert.That(experian, Is.True);
            Assert.That(transUnion, Is.True);
            Assert.That(b2, Is.EqualTo("l"));
            Assert.That(b3, Is.EqualTo("m"));
            Assert.That(pwe, Is.False);
            Assert.That(cScore, Is.EqualTo("l"));
            Assert.That(soft, Is.False);

        }

        [Test]
        public void SyntaxValidationTest()
        {
            // Quick simple test to make sure legal combinations are accepted

            List<string> allValidCombos = new List<string>();
            string[] validCb = new string[] { "E", "X", "T", "EX", "ET", "XT", "EXT" };
            string[] validB3 = new string[] { "L", "M", "H" };
            string[] validB2 = new string[] { "L", "H" };
            string[] validPwe = new string[] { "Y", "N" };
            string[] validCScore = new string[] { "L", "H" };
            string[] validSoft = new string[] { "Y", "N" };

            foreach (var cB in validCb)
                foreach (var b3 in validB3)
                    foreach (var b2 in validB2)
                        foreach (var pwe in validPwe)
                            foreach (var cScore in validCScore)
                                foreach (var soft in validSoft)
                                {
                                    allValidCombos.Add(string.Format("CB={0}&B3={1}&B2={2}&PWE={3}&CScore={4}&Soft={5}",
                                        cB, b3, b2, pwe, cScore, soft));
                                }

            CPageData data = FakePageData.Create();
            data.InitLoad();
            data.CalcModeT = E_CalcModeT.PriceMyLoan;
            CAppData dataApp = data.GetAppData(0);
            
            dataApp.aBSsn = "111-11-1111";
            dataApp.aBEquifaxScorePe_rep = "610";
            dataApp.aBExperianScorePe_rep = "611";
            dataApp.aBTransUnionScorePe_rep = "612";

            dataApp.aCSsn = "222-22-2222";
            dataApp.aCEquifaxScorePe_rep = "601";
            dataApp.aCExperianScorePe_rep = "602";
            dataApp.aCTransUnionScorePe_rep = "603";

            // Expect no error
            allValidCombos.ForEach(parameters =>
                dataApp.ScoreCBS(parameters, true));
        }

        [Test]
        public void InvalidSyntaxTest()
        {
            // Simple test to ensure invalid syntax is not allowed

            CPageData data = FakePageData.Create();
            data.InitLoad();
            data.CalcModeT = E_CalcModeT.PriceMyLoan;
            CAppData dataApp = data.GetAppData(0);

            dataApp.aBSsn = "111-11-1111";
            dataApp.aBEquifaxScorePe_rep = "610";
            dataApp.aBExperianScorePe_rep = "611";
            dataApp.aBTransUnionScorePe_rep = "612";

            dataApp.aCSsn = "222-22-2222";
            dataApp.aCEquifaxScorePe_rep = "601";
            dataApp.aCExperianScorePe_rep = "602";
            dataApp.aCTransUnionScorePe_rep = "603";

            Action<string> expectFail = p =>
               {
                   try
                   {
                       dataApp.ScoreCBS(p, true);
                       Assert.Fail("ScoreCBS allowed: " + p);
                   }
                   catch (CBaseException)
                   {
                       // Expected Fail
                   }
               };

            expectFail("INVALID");
            expectFail("INVALID=INVALID");
            expectFail("CB=INVALID");
            expectFail("B3=INVALID");
            expectFail("B2=INVALID");
            expectFail("PWE=INVALID");
            expectFail("CScore=INVALID");
            expectFail("Soft=INVALID");
        }
    }

}

