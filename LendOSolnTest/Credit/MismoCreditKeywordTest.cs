﻿namespace LendOSolnTest.Credit
{
    using NUnit.Framework;

    [TestFixture]
    public class MismoCreditKeywordTest
    {
        [Test]
        public void HasFcX_NoBankruptcyNoChargeOffNoCollectionNoForeclosure_ReturnsFalse()
        {
            var creditReport = CreditTestingUtils.LoadReportFromTestData(
                "TestHasMortgageIncludedInBKWithinTest2.xml");

            int hasForeclosure = creditReport.HasFcX("");

            Assert.AreEqual(0, hasForeclosure);
        }

        [Test]
        public void HasFcX_HasForeclosure_ReturnsTrue()
        {
            var creditReport = CreditTestingUtils.LoadReportFromTestData(
                "MismoTestCreditReportWithForeclosure.xml");

            int hasForeclosure = creditReport.HasFcX("");

            Assert.AreEqual(1, hasForeclosure);
        }

        [Test]
        public void HasFcX_HasChargeOffOrCollection_ReturnsFalse()
        {
            var creditReport = CreditTestingUtils.LoadReportFromTestData(
                "MismoTestCreditReportWithChargeOffOrCollection.xml");

            int hasForeclosure = creditReport.HasFcX("");

            Assert.AreEqual(0, hasForeclosure);
        }

        [Test]
        public void HasFcX_HasChargeOff_ReturnsFalse()
        {
            var creditReport = CreditTestingUtils.LoadReportFromTestData(
                "MismoTestCreditReportWithChargeOff.xml");

            int hasForeclosure = creditReport.HasFcX("");

            Assert.AreEqual(0, hasForeclosure);
        }

        [Test]
        public void HasFcX_HasCollection_ReturnsFalse()
        {
            var creditReport = CreditTestingUtils.LoadReportFromTestData(
                "MismoTestCreditReportWithCollection.xml");

            int hasForeclosure = creditReport.HasFcX("");

            Assert.AreEqual(0, hasForeclosure);
        }

        [Test]
        public void HasFcX_HasChargeOffAndForeclosureInPaymentHistory_ReturnsTrue()
        {
            var creditReport = CreditTestingUtils.LoadReportFromTestData(
                "MismoTestCreditReportWithChargeOffAndFc.xml");

            int hasForeclosure = creditReport.HasFcX("");

            Assert.AreEqual(1, hasForeclosure);
        }

        [Test]
        public void FcCountX_NoBankruptcyNoChargeOffNoCollectionNoForeclosure_ReturnsZero()
        {
            var creditReport = CreditTestingUtils.LoadReportFromTestData(
                "TestHasMortgageIncludedInBKWithinTest2.xml");

            int numForeclosures = creditReport.FcCountX("");

            Assert.AreEqual(0, numForeclosures);
        }

        [Test]
        public void FcCountX_HasForeclosure_ReturnsNonZero()
        {
            var creditReport = CreditTestingUtils.LoadReportFromTestData(
                "MismoTestCreditReportWithForeclosure.xml");

            int numForeclosures = creditReport.FcCountX("");

            Assert.Greater(numForeclosures, 0);
        }

        [Test]
        public void FcCountX_HasChargeOffOrCollection_ReturnsZero()
        {
            var creditReport = CreditTestingUtils.LoadReportFromTestData(
                "MismoTestCreditReportWithChargeOffOrCollection.xml");

            int numForeclosures = creditReport.FcCountX("");

            Assert.AreEqual(0, numForeclosures);
        }

        [Test]
        public void FcCountX_HasChargeOff_ReturnsZero()
        {
            var creditReport = CreditTestingUtils.LoadReportFromTestData(
                "MismoTestCreditReportWithChargeOff.xml");

            int numForeclosures = creditReport.FcCountX("");

            Assert.AreEqual(0, numForeclosures);
        }

        [Test]
        public void FcCountX_HasCollection_ReturnsZero()
        {
            var creditReport = CreditTestingUtils.LoadReportFromTestData(
                "MismoTestCreditReportWithCollection.xml");

            int numForeclosures = creditReport.FcCountX("");

            Assert.AreEqual(0, numForeclosures);
        }

        [Test]
        public void FcCountX_HasChargeOffAndForeclosureInPaymentHistory_ReturnsNonZero()
        {
            var creditReport = CreditTestingUtils.LoadReportFromTestData(
                "MismoTestCreditReportWithChargeOffAndFc.xml");

            int numForeclosures = creditReport.FcCountX("");

            Assert.Greater(numForeclosures, 0);
        }
    }
}
