﻿namespace LendOSolnTest.Credit
{
    using NUnit.Framework;

    [TestFixture]
    public class MclCreditKeywordTest
    {
        [Test]
        public void HasFcX_NoBankruptcyNoChargeOffNoCollectionNoForeclosure_ReturnsFalse()
        {
            var creditReport = CreditTestingUtils.LoadReportFromTestData(
                "MclTestCreditReportNoChargeOffNoBkNoFc.xml");

            int hasForeclosure = creditReport.HasFcX("");

            Assert.AreEqual(0, hasForeclosure);
        }

        [Test]
        public void HasFcX_HasForeclosure_ReturnsTrue()
        {
            var creditReport = CreditTestingUtils.LoadReportFromTestData(
                "MclTestCreditReportWithForeclosure.xml");

            int hasForeclosure = creditReport.HasFcX("");

            Assert.AreEqual(1, hasForeclosure);
        }

        [Test]
        public void HasFcX_HasChargeOff_ReturnsFalse()
        {
            var creditReport = CreditTestingUtils.LoadReportFromTestData(
                "MclTestCreditReportWithChargeOff.xml");

            int hasForeclosure = creditReport.HasFcX("");

            Assert.AreEqual(0, hasForeclosure);
        }

        [Test]
        public void HasFcX_HasCollection_ReturnsFalse()
        {
            var creditReport = CreditTestingUtils.LoadReportFromTestData(
                "MclTestCreditReportWithCollection.xml");

            int hasForeclosure = creditReport.HasFcX("");

            Assert.AreEqual(0, hasForeclosure);
        }

        [Test]
        public void HasFcX_HasChargeOffAndForeclosureInPaymentHistory_ReturnsTrue()
        {
            var creditReport = CreditTestingUtils.LoadReportFromTestData(
                "MclTestCreditReportWithChargeOffAndFc.xml");

            int hasForeclosure = creditReport.HasFcX("");

            Assert.AreEqual(1, hasForeclosure);
        }

        [Test]
        public void FcCountX_NoBankruptcyNoChargeOffNoCollectionNoForeclosure_ReturnsZero()
        {
            var creditReport = CreditTestingUtils.LoadReportFromTestData(
                "MclTestCreditReportNoChargeOffNoBkNoFc.xml");

            int numForeclosures = creditReport.FcCountX("");

            Assert.AreEqual(0, numForeclosures);
        }

        [Test]
        public void FcCountX_HasForeclosure_ReturnsNonZero()
        {
            var creditReport = CreditTestingUtils.LoadReportFromTestData(
                "MclTestCreditReportWithForeclosure.xml");

            int numForeclosures = creditReport.FcCountX("");

            Assert.Greater(numForeclosures, 0);
        }

        [Test]
        public void FcCountX_HasChargeOff_ReturnsZero()
        {
            var creditReport = CreditTestingUtils.LoadReportFromTestData(
                "MclTestCreditReportWithChargeOff.xml");

            int numForeclosures = creditReport.FcCountX("");

            Assert.AreEqual(0, numForeclosures);
        }

        [Test]
        public void FcCountX_HasCollection_ReturnsZero()
        {
            var creditReport = CreditTestingUtils.LoadReportFromTestData(
                "MclTestCreditReportWithCollection.xml");

            int numForeclosures = creditReport.FcCountX("");

            Assert.AreEqual(0, numForeclosures);
        }

        [Test]
        public void FcCountX_HasChargeOffAndForeclosureInPaymentHistory_ReturnsNonZero()
        {
            var creditReport = CreditTestingUtils.LoadReportFromTestData(
                "MismoTestCreditReportWithChargeOffAndFc.xml");

            int numForeclosures = creditReport.FcCountX("");

            Assert.Greater(numForeclosures, 0);
        }
    }
}
