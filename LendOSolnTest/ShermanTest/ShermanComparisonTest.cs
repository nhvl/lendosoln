﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using System.Runtime.InteropServices;
using System.Xml.Linq;
using LendersOffice.Security;
using LendersOffice.Common;
using LendersOffice.Conversions;
using DataAccess;
using LendOSolnTest.Fakes;
using LendersOffice.Constants;
using LendOSolnTest.Common;

namespace LendOSolnTest.ShermanTest
{
    [TestFixture]
    public class ShermanComparisonTest
    {
        private const string ShermanInputForm = "ShermanTest/ShermanInput_{0}.xml";

        private Dictionary<string, string> ComparingFields;
        private Dictionary<string, string> lqbShermanMapping;
        private Dictionary<string, string> lqbShermanFeeMapping;
        private ScexWrapper scex;
        private FakePageData TestLqbLoan;

        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            scex = new ScexWrapper();

            lqbShermanMapping = new Dictionary<string, string>() {
                { "IntRate",               nameof(CPageData.sRAdjFloorR) },
                { "InterestPmts",          nameof(CPageData.sIOnlyMon) },
                { "LoanDate",              "sConsummationD" },
                { "IntStartDate",          "sConsummationD" },
                { "PmtDate",               nameof(CPageData.sSchedDueD1) },
                { "Proceeds",              nameof(CPageData.sFinalLAmt) },
                { "Term",                  nameof(CPageData.sTerm) },
                { "AmortTerm",             nameof(CPageData.sDue) },
                { "TeaserRate",            nameof(CPageData.sNoteIR) },
                { "TeaserTerm",            nameof(CPageData.sRAdj1stCapMon) },
                { "Index",                 nameof(CPageData.sRAdjIndexR) },
                { "Margin",                nameof(CPageData.sRAdjMarginR) },
                { "TermStep",              nameof(CPageData.sRAdjCapMon) },
                { "AnnualRateIncrease",    nameof(CPageData.sRAdjCapR) },
                { "MaxRateIncrease",       nameof(CPageData.sRAdjLifeCapR) },
                { "MinRate",               nameof(CPageData.sRAdjFloorR) },
                { "FirstRateIncrease",     nameof(CPageData.sRAdj1stCapR) },
                { "RoundRate",             nameof(CPageData.sRAdjRoundToR) }
            };

            lqbShermanFeeMapping = new Dictionary<string, string>() {
                { "Entry",               nameof(LoanClosingCostFee.TotalAmount) },
                { "MaxValue",            nameof(LoanClosingCostFee.TotalAmount) },
                { "AddToFinChg",         nameof(LoanClosingCostFee.IsApr) }
            };

            ComparingFields = new Dictionary<string, string>() { { "RegZAPR", nameof(CPageData.sApr) } };
        } 

        [TearDown]
        public void Teardown()
        {
            FakePageDataUtilities.Instance.Reset();
        }

        [SetUp]
        public void Setup()
        {
            FakePageData dataLoan = FakePageData.Create();
            dataLoan.InitLoad();
            TestLqbLoan = dataLoan;
        }

        [Test]
        public void CompareShermanARMLoan()
        {
            TestLqbLoan.sIOnlyMon = 0;
            TestLqbLoan.sFinMethT = E_sFinMethT.ARM;


            TestLqbLoan.sConsummationDLckd = true;
            TestLqbLoan.sConsummationD_rep = "02/01/2017";

            TestLqbLoan.sSchedDueD1Lckd = true;
            TestLqbLoan.sSchedDueD1 = CDateTime.Create(new DateTime(2017, 3, 1));

            TestLqbLoan.sLAmtLckd = true;
            TestLqbLoan.sLAmtCalc = 400000.00M;

            TestLqbLoan.sTerm = 360;
            TestLqbLoan.sDue = 360;
            TestLqbLoan.sNoteIR = 4.000M;

            TestLqbLoan.sRAdj1stCapMon = 84;
            TestLqbLoan.sRAdjIndexR = 1.71M;
            TestLqbLoan.sRAdjMarginR = 3.75M;
            TestLqbLoan.sRAdjCapMon = 12;
            TestLqbLoan.sRAdjCapR = 2.000M;
            TestLqbLoan.sRAdjLifeCapR = 5;
            TestLqbLoan.sRAdjFloorR = 3.75M;
            TestLqbLoan.sRAdj1stCapR = 5M;
            TestLqbLoan.sRAdjRoundToR = 0.125M;


            string json = @"[{""aff"":false,""apr"":true,""bene"":0,""bene_id"":""00000000-0000-0000-0000-000000000000"",""can_shop"":false,""desc"":""Test"",""did_shop"":false,""disc_sect"":0,
                                ""f"":{""base"":""$100.00"",""p"":""0.000%"",""pb"":""$100,000.00"",""period"":""1"",""pt"":0,""t"":0},
                                ""fha"":false,""hudline"":""805"",""id"":""b9e53fdb-6a9b-4af6-b499-07b96530fe81"",""is_optional"":false,""is_system"":false,""is_title"":false,""legacy"":0,
                                ""mismo"":0,""pmts"":null,""prov"":0,""prov_choice"":0,""responsible"":0,""section"":12,""total"":""$1,001.00"",""tp"":false,
                                ""typeid"":""00000000-0000-0000-0000-000000000000"",""va"":false}]";
            TestLqbLoan.CopysClosingCostSetJson(json);

            TestComparingLoan(TestLqbLoan);
        }
        
        private void TestComparingLoan(FakePageData lqbLoan)
        {
            var shermanType = lqbLoan.sFinMethT == E_sFinMethT.ARM ? "ARM" : "EQUAL_PMT";
            var shermanTypeIn = "in" + shermanType;
            var shermanTypeOut = "out" + shermanType;

            XDocument shermanXml = XDocument.Load(TestDataRootFolder.Location + string.Format(ShermanInputForm, shermanType));
            
            XDocument shermanxmlOuput = XDocument.Parse(scex.Calculate(shermanXml.ToString()));
            var logBuilder = new StringBuilder();
            logBuilder.AppendLine(shermanXml.ToString());
            logBuilder.AppendLine(shermanxmlOuput.ToString());
            logBuilder.AppendLine("INPUT:");
            foreach (XElement tag in shermanXml.Element(shermanTypeIn).Elements())
            {
                string loanField;
                if (lqbShermanMapping.TryGetValue(tag.Name.ToString(), out loanField))
                {
                    var loanVal = PageDataUtilities.GetValue(lqbLoan, null, loanField);
                    string log = string.Format("\tSherman.{0} = {1}. LendingQBLoan.{2} = {3}.", tag.Name.ToString(), shermanXml.Element(shermanTypeIn).Element(tag.Name).Value, loanField, loanVal);
                    logBuilder.AppendLine(log);
                }
            }
            var firstFee = lqbLoan.sClosingCostSet.GetFees(x => true).Cast<BorrowerClosingCostFee>().FirstOrDefault();
            if (firstFee != null)
            {
                var feeElement = shermanXml.Element(shermanTypeIn).Element("Fee");
                logBuilder.AppendLine(string.Format("\tSherman.Fee.{0} = {1}. LendingQBLoan.Fee.{2} = {3}.", "Entry", feeElement.Attribute("Entry").Value, "TotalAmount", firstFee.TotalAmount_rep));
                logBuilder.AppendLine(string.Format("\tSherman.Fee.{0} = {1}. LendingQBLoan.Fee.{2} = {3}.", "MaxValue", feeElement.Attribute("MaxValue").Value, "TotalAmount", firstFee.TotalAmount_rep));
                logBuilder.AppendLine(string.Format("\tSherman.Fee.{0} = {1}. LendingQBLoan.Fee.{2} = {3}.", "AddToFinChg", feeElement.Attribute("AddToFinChg").Value, "IsApr", firstFee.IsApr));
            }
            logBuilder.AppendLine(lqbLoan.sClosingCostSet.GetFees(x => true).Cast<BorrowerClosingCostFee>().FirstOrDefault().TotalAmount_rep);

            logBuilder.AppendLine("OUTPUT:");
            foreach (var keypairs in ComparingFields)
            {
                var shermanField = keypairs.Key;
                var loanField = keypairs.Value;

                string shermanValue = shermanxmlOuput.Element(shermanTypeOut).Descendants(shermanField).First<XElement>().Value;

                string lqbValue = GetLoanFieldValue(lqbLoan, loanField);
                string log = string.Format("\tSherman.{0} = {1}. LendingQBLoan.{2} = {3}.", shermanField, shermanValue, loanField, lqbValue);
                logBuilder.AppendLine(log);
                Assert.AreEqual(shermanValue, lqbValue, "Result not match." + logBuilder.ToString());
            }
        }

        private string GetLoanFieldValue(CPageData loanData, string loanFieldId)
        {
            var stringValue = PageDataUtilities.GetValue(loanData, null, loanFieldId);
            return stringValue.Replace("%", string.Empty).Replace("$", string.Empty);
        }
    }

    class ScexWrapper
    {
        // private member variables
        private int _Error;  // signals if an error occurred during call to SCEX
                             //  0 = successful calculation.
                             //  1 = Not enough space allocated for input buffer.
                             //      The calling app should never see this error
                             //      message, as it is taken care of by the
                             //      ::Calculate() method.
                             //  2 = Not enough space allocated for output buffer.
                             //      The calling app should never see this error
                             //      message, as it is taken care of by the
                             //      ::Calculate() method.
                             //  3 = XML Output is not well formed.
                             //  4 = XML Output is not valid.
                             // -1 = XML Input is not well formed. XML Input
                             //      violates at least one rule defining well-
                             //      formed data.
                             // -2 = XML Input is not valid. Please see the SCEX
                             //      documentation for the format of valid XML
                             //      queries sent to the SCEX.
                             // -3 = A windows error has occurred within the SCEX.

        private StringBuilder _BufferIn;   // input buffer used to call xmlcalc()
        private StringBuilder _BufferOut;  // output buffer used in call to xmlcalc()
        private string _DataDirPath;       // default data directory path

        //
        // ScexWrapper::xmlcalc()
        //  Import the xmlcalc() routine from the SCEX.DLL. This is
        //  declared as private because the SCEX::Calculate() method
        //  provides a safer, C# interface to this routine.
        //
        [DllImport(@"SCEX\SCEX.DLL")]
        private static extern int
        xmlcalc_ddp([MarshalAs(UnmanagedType.LPStr)]
                     StringBuilder xmlIn,
                     [MarshalAs(UnmanagedType.LPStr)]
                     StringBuilder xmlOut,
                     int lenIn,
                     int lenOut,
                     ref int reqIn,
                     ref int reqOut,
                     string DataDirPath
                     );

        //
        // ScexWrapper::ScexWrapper()
        //  Constructor which allocates space for our buffers, etc.
        //
        public ScexWrapper()
        {

            // allocate and initialize out buffers
            _BufferIn = new StringBuilder("", 4 * 1024);
            _BufferOut = new StringBuilder("", 8 * 1024);
            _DataDirPath = "";

            // set error field to zero
            _Error = 0;

        } // end SCEXWrapper::SCEXWrapper()

        //
        // Error
        //  Read-only public property to access the private error
        //  field.
        //
        public int Error
        {
            get { return _Error; }

        } // end property Error

        //
        // DataDirPath
        //  Public property to access the data directory path field.
        //
        public string DataDirPath
        {
            get { return _DataDirPath; }
            set { _DataDirPath = value; }

        } // end property Error

        //
        // ScexWrapper::Calculate()
        //  Public interface to the SCEX DLL's xmlcalc_ddp() routine.
        //  This is a simple wrapper function which accepts a
        //  single paramenter (the XML input for the SCEX) and
        //  then returns the result of the query as a string.
        //  This routine implements the lower level memory
        //  allocation for the result buffer, to hide some of
        //  the complexity. If an error is encountered, this
        //  routine will return an empty string and set the
        //  Error field to the appropriate integral error code.
        //
        // Parameters:
        //  xmlIn - the xml input query to be sent to the SCEX.
        //
        // Returns:
        //  If no errors are encountered, this method will
        //  return the xml output from the SCEX. If an error is
        //  encountered, then an empty string is returned, and
        //  the Error field is set to the integral error code
        //  returned by the SCEX.
        //
        public string Calculate(string xmlIn)
        {

            // set the Error code to zero (successful calculation)
            _Error = 0;

            // set the initial sizes of our input and output buffers
            // and initialize result to zero
            int iReqIn = 0;
            int iReqOut = 0;
            int iResult = 0;

            // allocate and initialize buffers for input and output now
            _BufferIn.Remove(0, _BufferIn.Length);
            _BufferIn.Append(xmlIn);
            _BufferOut.Remove(0, _BufferOut.Length);

            // loop until the buffers are sized as needed
            do
            {

                // resize the input and output buffers, as necessary
                if (iReqIn > _BufferIn.Capacity) _BufferIn.Capacity = iReqIn + 1;
                if (iReqOut > _BufferOut.Capacity) _BufferOut.Capacity = iReqOut + 1;

                // call xmlcalc now!
                iResult = xmlcalc_ddp(_BufferIn, _BufferOut,
                                       _BufferIn.Capacity, _BufferOut.Capacity,
                                       ref iReqIn, ref iReqOut,
                                       _DataDirPath
                                       );

                // continue looping while we get buffer sizing results
            } while ((iResult == 1) || (iResult == 2)); // end do-while

            // set error field to result
            _Error = iResult;

            // and return the output buffer as a string
            return (_BufferOut.ToString());

        } // end SCEXWrapper.Calculate

    }
}