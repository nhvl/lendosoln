/// Author: David Dao

using System;
using System.Data.Common;
using LendersOffice.Admin;
using LendersOffice.Security;
using LendOSolnTest.Common;
using NUnit.Framework;
using PriceMyLoan.Security;

namespace LendOSolnTest
{
    [TestFixture]
	public class VerifyTestAccountsSetup
	{
        private Guid m_mainBranchId = Guid.Empty;
        private Guid m_branch1BranchId = Guid.Empty;

        [TestFixtureSetUp]
        public void Setup() 
        {
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.LoTest001);
            using (DbDataReader reader = BranchDB.GetBranches(principal.BrokerId)) 
            {
                while(reader.Read()) 
                {
                    Guid id = (Guid) reader["BranchId"];
                    string name = (string) reader["Name"];
                    if (name == "Main Branch")
                        m_mainBranchId = id;
                    else if (name == "Branch 1")
                        m_branch1BranchId = id;
                }
            }
        }
        [Test]
        public void VerifyCorporateUnderwriter() 
        {
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.Corporate_Underwriter);

            Verify(principal, "Underwriter" /* firstName */, "Corporate" /* lastName */,
                true /* isCorporateAccess */, false /* isBranchAccess */, false /* isIndividualAccess */,
                true /* isUnderwriter */, false /* isLockDesk */, false /* isProcessor */, 
                false /* isLoanOfficer */, false /* isLenderAccExc */, false /* isManager */,
                m_mainBranchId /* branch */);
        }

        [Test]
        public void VerifyCorporateLockDesk() 
        {
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.Corporate_LockDesk);

            Verify(principal, "LockDesk" /* firstName */, "Corporate" /* lastName */,
                true /* isCorporateAccess */, false /* isBranchAccess */, false /* isIndividualAccess */,
                false /* isUnderwriter */, true /* isLockDesk */, false /* isProcessor */, 
                false /* isLoanOfficer */, false /* isLenderAccExc */, false /* isManager */,
                m_mainBranchId /* branch */);
        }

        [Test]
        public void VerifyCorporateProcessor() 
        {
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.Corporate_Processor);

            Verify(principal, "Processor" /* firstName */, "Corporate" /* lastName */,
                true /* isCorporateAccess */, false /* isBranchAccess */, false /* isIndividualAccess */,
                false /* isUnderwriter */, false /* isLockDesk */, true /* isProcessor */, 
                false /* isLoanOfficer */, false /* isLenderAccExc */, false /* isManager */,
                m_mainBranchId /* branch */);
            
        }

        [Test]
        public void VerifyCorporateLoanOfficer() 
        {
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.Corporate_LoanOfficer);

            Verify(principal, "LoanOfficer" /* firstName */, "Corporate" /* lastName */,
                true /* isCorporateAccess */, false /* isBranchAccess */, false /* isIndividualAccess */,
                false /* isUnderwriter */, false /* isLockDesk */, false /* isProcessor */, 
                true /* isLoanOfficer */, false /* isLenderAccExc */, false /* isManager */,
                m_mainBranchId /* branch */);
            
        }
        [Test]
        public void VerifyCorporateLenderAccountExec() 
        {
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.Corporate_LenderAccountExec);

            Verify(principal, "AccountExec" /* firstName */, "Corporate" /* lastName */,
                true /* isCorporateAccess */, false /* isBranchAccess */, false /* isIndividualAccess */,
                false /* isUnderwriter */, false /* isLockDesk */, false /* isProcessor */, 
                false /* isLoanOfficer */, true /* isLenderAccExc */, false /* isManager */,
                m_mainBranchId /* branch */);
            
        }
        [Test]
        public void VerifyCorporateManager() 
        {
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.Corporate_Manager);

            Verify(principal, "Manager" /* firstName */, "Corporate" /* lastName */,
                true /* isCorporateAccess */, false /* isBranchAccess */, false /* isIndividualAccess */,
                false /* isUnderwriter */, false /* isLockDesk */, false /* isProcessor */, 
                false /* isLoanOfficer */, false /* isLenderAccExc */, true /* isManager */,
                m_mainBranchId /* branch */);
            
        }

        [Test]
        public void VerifyBranch1Underwriter() 
        {
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.Branch1_Underwriter);

            Verify(principal, "Underwriter" /* firstName */, "Branch1" /* lastName */,
                false /* isCorporateAccess */, true /* isBranchAccess */, false /* isIndividualAccess */,
                true /* isUnderwriter */, false /* isLockDesk */, false /* isProcessor */, 
                false /* isLoanOfficer */, false /* isLenderAccExc */, false /* isManager */,
                m_branch1BranchId /* branch */);
            
        }

        [Test]
        public void VerifyBranch1LockDesk() 
        {
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.Branch1_LockDesk);

            Verify(principal, "LockDesk" /* firstName */, "Branch1" /* lastName */,
                false /* isCorporateAccess */, true /* isBranchAccess */, false /* isIndividualAccess */,
                false /* isUnderwriter */, true /* isLockDesk */, false /* isProcessor */, 
                false /* isLoanOfficer */, false /* isLenderAccExc */, false /* isManager */,
                m_branch1BranchId /* branch */);
            
        }

        [Test]
        public void VerifyBranch1Processor() 
        {
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.Branch1_Processor);

            Verify(principal, "Processor" /* firstName */, "Branch1" /* lastName */,
                false /* isCorporateAccess */, true /* isBranchAccess */, false /* isIndividualAccess */,
                false /* isUnderwriter */, false /* isLockDesk */, true /* isProcessor */, 
                false /* isLoanOfficer */, false /* isLenderAccExc */, false /* isManager */,
                m_branch1BranchId /* branch */);
            
        }
        [Test]
        public void VerifyBranch1LoanOfficer() 
        {
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.Branch1_LoanOfficer);

            Verify(principal, "LoanOfficer" /* firstName */, "Branch1" /* lastName */,
                false /* isCorporateAccess */, true /* isBranchAccess */, false /* isIndividualAccess */,
                false /* isUnderwriter */, false /* isLockDesk */, false /* isProcessor */, 
                true /* isLoanOfficer */, false /* isLenderAccExc */, false /* isManager */,
                m_branch1BranchId /* branch */);
            
        }
        [Test]
        public void VerifyBranch1LenderAccountExec() 
        {
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.Branch1_LenderAccountExec);

            Verify(principal, "AccountExec" /* firstName */, "Branch1" /* lastName */,
                false /* isCorporateAccess */, true /* isBranchAccess */, false /* isIndividualAccess */,
                false /* isUnderwriter */, false /* isLockDesk */, false /* isProcessor */, 
                false /* isLoanOfficer */, true /* isLenderAccExc */, false /* isManager */,
                m_branch1BranchId /* branch */);
            
        }
        [Test]
        public void VerifyBranch1Manager() 
        {
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.Branch1_Manager);

            Verify(principal, "Manager" /* firstName */, "Branch1" /* lastName */,
                false /* isCorporateAccess */, true /* isBranchAccess */, false /* isIndividualAccess */,
                false /* isUnderwriter */, false /* isLockDesk */, false /* isProcessor */, 
                false /* isLoanOfficer */, false /* isLenderAccExc */, true /* isManager */,
                m_branch1BranchId /* branch */);
            
        }



        [Test]
        public void VerifyMainUnderwriter() 
        {
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.MainBranch_Underwriter);

            Verify(principal, "Underwriter" /* firstName */, "Main" /* lastName */,
                false /* isCorporateAccess */, true /* isBranchAccess */, false /* isIndividualAccess */,
                true /* isUnderwriter */, false /* isLockDesk */, false /* isProcessor */, 
                false /* isLoanOfficer */, false /* isLenderAccExc */, false /* isManager */,
                m_mainBranchId /* branch */);
            
        }

        [Test]
        public void VerifyMainLockDesk() 
        {
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.MainBranch_LockDesk);

            Verify(principal, "LockDesk" /* firstName */, "Main" /* lastName */,
                false /* isCorporateAccess */, true /* isBranchAccess */, false /* isIndividualAccess */,
                false /* isUnderwriter */, true /* isLockDesk */, false /* isProcessor */, 
                false /* isLoanOfficer */, false /* isLenderAccExc */, false /* isManager */,
                m_mainBranchId /* branch */);
            
        }

        [Test]
        public void VerifyMainProcessor() 
        {
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.MainBranch_Processor);

            Verify(principal, "Processor" /* firstName */, "Main" /* lastName */,
                false /* isCorporateAccess */, true /* isBranchAccess */, false /* isIndividualAccess */,
                false /* isUnderwriter */, false /* isLockDesk */, true /* isProcessor */, 
                false /* isLoanOfficer */, false /* isLenderAccExc */, false /* isManager */,
                m_mainBranchId /* branch */);
            
        }
        [Test]
        public void VerifyMainLoanOfficer() 
        {
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.MainBranch_LoanOfficer);

            Verify(principal, "LoanOfficer" /* firstName */, "Main" /* lastName */,
                false /* isCorporateAccess */, true /* isBranchAccess */, false /* isIndividualAccess */,
                false /* isUnderwriter */, false /* isLockDesk */, false /* isProcessor */, 
                true /* isLoanOfficer */, false /* isLenderAccExc */, false /* isManager */,
                m_mainBranchId /* branch */);
            
        }
        [Test]
        public void VerifyMainLenderAccountExec() 
        {
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.MainBranch_LenderAccountExec);

            Verify(principal, "AccountExec" /* firstName */, "Main" /* lastName */,
                false /* isCorporateAccess */, true /* isBranchAccess */, false /* isIndividualAccess */,
                false /* isUnderwriter */, false /* isLockDesk */, false /* isProcessor */, 
                false /* isLoanOfficer */, true /* isLenderAccExc */, false /* isManager */,
                m_mainBranchId /* branch */);
            
        }
        [Test]
        public void VerifyMainManager() 
        {
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.MainBranch_Manager);

            Verify(principal, "Manager" /* firstName */, "Main" /* lastName */,
                false /* isCorporateAccess */, true /* isBranchAccess */, false /* isIndividualAccess */,
                false /* isUnderwriter */, false /* isLockDesk */, false /* isProcessor */, 
                false /* isLoanOfficer */, false /* isLenderAccExc */, true /* isManager */,
                m_mainBranchId /* branch */);
            
        }

        [Test]
        public void VerifyIndividualUnderwriter() 
        {
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.Underwriter);

            Verify(principal, "Underwriter" /* firstName */, "Individual" /* lastName */,
                false /* isCorporateAccess */, false /* isBranchAccess */, true /* isIndividualAccess */,
                true /* isUnderwriter */, false /* isLockDesk */, false /* isProcessor */, 
                false /* isLoanOfficer */, false /* isLenderAccExc */, false /* isManager */,
                m_mainBranchId /* branch */);
            
        }
        
        [Test]
        public void VerifyIndividualLockDesk() 
        {
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.LockDesk);

            Verify(principal, "LockDesk" /* firstName */, "Individual" /* lastName */,
                false /* isCorporateAccess */, false /* isBranchAccess */, true /* isIndividualAccess */,
                false /* isUnderwriter */, true /* isLockDesk */, false /* isProcessor */, 
                false /* isLoanOfficer */, false /* isLenderAccExc */, false /* isManager */,
                m_mainBranchId /* branch */);
            
        }
        [Test]
        public void VerifyIndividualProcessor() 
        {
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.Processor);

            Verify(principal, "Processor" /* firstName */, "Individual" /* lastName */,
                false /* isCorporateAccess */, false /* isBranchAccess */, true /* isIndividualAccess */,
                false /* isUnderwriter */, false /* isLockDesk */, true /* isProcessor */, 
                false /* isLoanOfficer */, false /* isLenderAccExc */, false /* isManager */,
                m_mainBranchId /* branch */);
            
        }
        [Test]
        public void VerifyIndividualLoanOfficer() 
        {
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.LoanOfficer);

            Verify(principal, "LoanOfficer" /* firstName */, "Individual" /* lastName */,
                false /* isCorporateAccess */, false /* isBranchAccess */, true /* isIndividualAccess */,
                false /* isUnderwriter */, false /* isLockDesk */, false /* isProcessor */, 
                true /* isLoanOfficer */, false /* isLenderAccExc */, false /* isManager */,
                m_mainBranchId /* branch */);
            
        }
        [Test]
        public void VerifyIndividualLenderAccountExec() 
        {
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.LenderAccountExec);

            Verify(principal, "AccountExec" /* firstName */, "Individual" /* lastName */,
                false /* isCorporateAccess */, false /* isBranchAccess */, true /* isIndividualAccess */,
                false /* isUnderwriter */, false /* isLockDesk */, false /* isProcessor */, 
                false /* isLoanOfficer */, true /* isLenderAccExc */, false /* isManager */,
                m_mainBranchId /* branch */);
            
        }
        [Test]
        public void VerifyIndividualManager() 
        {
            AbstractUserPrincipal principal = LoginTools.LoginAs(TestAccountType.Manager);

            Verify(principal, "Manager" /* firstName */, "Individual" /* lastName */,
                false /* isCorporateAccess */, false /* isBranchAccess */, true /* isIndividualAccess */,
                false /* isUnderwriter */, false /* isLockDesk */, false /* isProcessor */, 
                false /* isLoanOfficer */, false /* isLenderAccExc */, true /* isManager */,
                m_mainBranchId /* branch */);
            
        }

        [Test]
        public void VerifyPmlAccounts() 
        {
            PriceMyLoanPrincipal principal = LoginTools.LoginAs(TestAccountType.Pml_Supervisor) as PriceMyLoanPrincipal;

            Assert.IsNotNull(principal);

            Assert.AreEqual("Supervisor", principal.FirstName, "First Name");
            Assert.AreEqual("PML", principal.LastName, "Last Name");
            Assert.AreEqual(true, principal.IsPmlManager, "Is Supervisor");

            Guid supervisorId = principal.EmployeeId;

            principal = LoginTools.LoginAs(TestAccountType.Pml_User0) as PriceMyLoanPrincipal;
            Assert.IsNotNull(principal);

            Assert.AreEqual("User0", principal.FirstName, "First Name");
            Assert.AreEqual("PML", principal.LastName, "Last Name");
            Assert.AreEqual(false, principal.IsPmlManager, "Is Supervisor");
            Assert.AreEqual(supervisorId, principal.PmlExternalManagerEmployeeID);

            principal = LoginTools.LoginAs(TestAccountType.Pml_User1) as PriceMyLoanPrincipal;
            Assert.IsNotNull(principal);

            Assert.AreEqual("User1", principal.FirstName, "First Name");
            Assert.AreEqual("PML", principal.LastName, "Last Name");
            Assert.AreEqual(false, principal.IsPmlManager, "Is Supervisor");
            Assert.AreEqual(supervisorId, principal.PmlExternalManagerEmployeeID);    
        
            principal = LoginTools.LoginAs(TestAccountType.Pml_User2) as PriceMyLoanPrincipal;
            Assert.IsNotNull(principal);

            Assert.AreEqual("User2", principal.FirstName, "First Name");
            Assert.AreEqual("PML", principal.LastName, "Last Name");
            Assert.AreEqual(false, principal.IsPmlManager, "Is Supervisor");
            Assert.AreEqual(Guid.Empty, principal.PmlExternalManagerEmployeeID);
        }
        private void Verify(AbstractUserPrincipal principal, string firstName, string lastName, bool isCorporateAccess, bool isBranchAccess, bool isIndividualAccess,
            bool isUnderwriter, bool isLockDesk, bool isProcessor, bool isLoanOfficer, bool isLenderAccExec, bool isManager, Guid branch) 
        {
            Assert.AreEqual(firstName, principal.FirstName, "First Name");
            Assert.AreEqual(lastName, principal.LastName, "Last Name");
            Assert.AreEqual(branch, principal.BranchId, "Branch");
            Assert.AreEqual(isCorporateAccess, principal.HasPermission(Permission.BrokerLevelAccess), "Broker Access Level");
            Assert.AreEqual(isBranchAccess, principal.HasPermission(Permission.BranchLevelAccess), "Branch Access Level");
            Assert.AreEqual(isIndividualAccess, principal.HasPermission(Permission.IndividualLevelAccess), "Individual Access Level");
            Assert.AreEqual(isUnderwriter, principal.HasRole(E_RoleT.Underwriter), "Underwriter");
            Assert.AreEqual(isLockDesk, principal.HasRole(E_RoleT.LockDesk), "Lock Desk");
            Assert.AreEqual(isProcessor, principal.HasRole(E_RoleT.Processor), "Processor");
            Assert.AreEqual(isLoanOfficer, principal.HasRole(E_RoleT.LoanOfficer), "Loan Officer");
            Assert.AreEqual(isLenderAccExec, principal.HasRole(E_RoleT.LenderAccountExecutive), "Lender Account Executive");
            Assert.AreEqual(isManager, principal.HasRole(E_RoleT.Manager), "Manager");

        }


	}
}
