﻿using System;
using System.IO;
using System.ServiceModel;
using DataAccess;
using LendersOffice.Constants;
using LendersOffice.GDMS.FileUpload;
using LendersOffice.GDMS.LookupMethods;
using LendersOffice.GDMS.Orders;
using LendersOffice.ObjLib.Conversions.GlobalDMS;
using LendOSolnTest.Common;
using LendOSolnTest.Controls;
using NUnit.Framework;

namespace LendOSolnTest.Integrations
{
    [TestFixture]
    [Category(CategoryConstants.NewBuildServerIgnore)]
    public class GlobalDMSTests
    {
        private static readonly LendersOffice.ObjLib.Conversions.GlobalDMS.GDMSCredentials productionCredentials = new LendersOffice.ObjLib.Conversions.GlobalDMS.GDMSCredentials()
        {
            CompanyId = 0,
            UserName = "UserName",
            Password = "Password",
            UserType = 2, // 1 - Vendor/Appraiser, 2 - Client, 3 - Staff
            ExportToStage = false
        };

        [Test]
        [Category(CategoryConstants.ExpectFailureOnLocalhost)]
        public void Production_LookupMethods_TestConnection()
        {
            TestConnectionLookupMethodsClient(productionCredentials);
        }

        [Test]
        [Category(CategoryConstants.ExpectFailureOnLocalhost)]
        public void Production_FileUpload_TestConnection()
        {
            TestConnectionFileUploadClient(productionCredentials);
        }

        [Test]
        [Category(CategoryConstants.ExpectFailureOnLocalhost)]
        public void Production_Orders_TestConnection()
        {
            TestConnectionOrdersClient(productionCredentials);
        }

        private static void TestConnectionLookupMethodsClient(GDMSCredentials credentials)
        {
            TestConnectionWithGDMS(
                () => new LendersOffice.ObjLib.Conversions.GlobalDMS.LookupMethodsClient(credentials),
                client =>
                {
                    var products = client.GetProducts();
                    var propertyTypes = client.GetPropertyTypes();
                });
        }

        private static void TestConnectionFileUploadClient(GDMSCredentials credentials)
        {
            TestConnectionWithGDMS(
                () => new LendersOffice.ObjLib.Conversions.GlobalDMS.FileUploadClient(credentials, "Test.xml", 0, 0, new MemoryStream(System.Text.Encoding.UTF8.GetBytes("<Example />"))),
                client =>
                {
                    var products = client.SubmitReport();
                });
        }

        private static void TestConnectionOrdersClient(GDMSCredentials credentials)
        {
            TestConnectionWithGDMS(
                () => new LendersOffice.ObjLib.Conversions.GlobalDMS.OrdersClient(credentials),
                client =>
                {
                    var orders = client.GetOrders();
                });
        }

        private static void TestConnectionWithGDMS<TGDMSClient>(Func<TGDMSClient> createClient, Action<TGDMSClient> loadData) where TGDMSClient : GDMSClient
        {
            using (var client = createClient())
            {
                try
                {
                    loadData(client);
                }
                catch (GDMSErrorResponseException)
                {
                    // This is probably the normal case for invalid credentials
                }
                catch (GDMSConnectionResponseBaseException)
                {
                    // Another Exception response from GlobalDMS, but this still indicates a successful connection.
                }
                catch (System.ServiceModel.CommunicationException exc)
                {
                    // CommunicationException is a failure
                    Tools.LogWarning(exc);
                    Assert.Fail(exc.Message);
                }
                catch (Exception exc)
                {
                    Tools.LogWarning(exc);
                    Assert.Fail(exc.Message);
                }
            }
        }

        [Test]
        [Ignore("Broken until Tim Fixes it")]
        public void LookupMethods_AuthenticateUser()
        {
            var message = "";
            bool sucess;
            try
            {
                var credentials = new LendersOffice.GDMS.LookupMethods.CredentialsObject
                    {CompanyId = 5, UserName = "LQBTestUser", Password = "LQBTestUser", UserType = 2};
                // var lm = new LookupMethodsClient("BasicHttpBinding_ILookupMethods1");
                var lm = CreateLookupMethodsInstance();
                sucess = lm.AuthenticateUser(credentials);
                lm.Close();
                if (!sucess) { message = "Login failed."; }
            }
            catch (Exception e)
            {
                sucess = false;
                message = e.Message;
            }
            Assert.AreEqual(true, sucess, message);
        }

        [Test]
        [Ignore("Broken until Tim Fixes it")]
        public void Orders_AddStaffOrder()
        {
            string message;
            bool sucess;
            try
            {
                var orInf = new GDMSWSOrderInfo();
                message = orInf.orderClient.AddStaffOrder(orInf.credentials, orInf.orderObject,
                                                          orInf.sReturnFormat);
                orInf.orderClient.Close();
                sucess = Methods.IsValidXml(message);
            }
            catch (Exception e)
            {
                sucess = false;
                message = e.Message;
            }
            Assert.AreEqual(true, sucess, message);
        }

        [Test]
        [Ignore("Broken until Tim Fixes it")]
        public void Orders_UpdateStaffOrder()
        {
            string message;
            bool sucess;
            try
            {
                var orInf = new GDMSWSOrderInfo();
                message = orInf.orderClient.UpdateStaffOrder(orInf.credentials, orInf.orderObject,
                                                             orInf.sReturnFormat);
                orInf.orderClient.Close();
                sucess = Methods.IsValidXml(message);
            }
            catch (Exception e)
            {
                sucess = false;
                message = e.Message;
            }
            Assert.AreEqual(true, sucess, message);
        }

        [Test]
        [Ignore("Broken until Tim Fixes it")]
        public void Orders_OrderPropertyValuation()
        {
            string message;
            bool sucess;
            try
            {
                var orInf = new GDMSWSOrderInfo();
                message = orInf.orderClient.OrderPropertyValuation(orInf.MISMOXML);
                orInf.orderClient.Close();
                sucess = Methods.IsValidXml(message);
            }
            catch (Exception e)
            {
                sucess = false;
                message = e.Message;
            }
            Assert.AreEqual(true, sucess, message);
        }

        [Test]
        [Ignore("Broken until Tim Fixes it")]
        public void Orders_SendEmailCommunication()
        {
            string message;
            bool sucess;
            try
            {
                var orInf = new GDMSWSOrderInfo();
                message = orInf.orderClient.SendEmailCommunication(orInf.credentials, orInf.emailToSend,
                                                                   orInf.orderIdentification, orInf.sReturnFormat);
                orInf.orderClient.Close();
                sucess = Methods.IsValidXml(message);
            }
            catch (Exception e)
            {
                sucess = false;
                message = e.Message;
            }
            Assert.AreEqual(true, sucess, message);
        }

        [Test]
        [Ignore("Broken until Tim Fixes it")]
        public void FileUpload_SubmitReport()
        {
            var message = "";
            bool sucess;
            try
            {
                string path = TempFileUtils.NewTempFilePath();
                var fileTransferInfo = new FileTransferInfo
                    {
                        CompanyId = 5,
                        UserName = "LQBTestUser",
                        Password = "LQBTestUser",
                        UserType = 2,
                        FileName = path,
                        RecordKeyToAttachTo = 836882,
                        ViewableByClient = true,
                        ViewableByVendor = true,
                        FileTypeId = 28
                    };
                // var fu = new FileUploadClient("BasicHttpBinding_IFileUpload1");
                var fu = CreateFileUploadInstance();
                UploadResults result;
                using (var file = File.Create(path))
                {
                    // do something with it
                    result = fu.SubmitReport(fileTransferInfo, file);
                }
                File.Delete(path);

                fu.Close();
                if (!result.success)
                {
                    switch (result.message)
                    {
                        case "Invalid or missing credentials":
                        case "Failed to upload file":
                            sucess = true;
                            break;
                        default:
                            sucess = false;
                            message = result.message;
                            break;
                    }
                }
                else
                {
                    sucess = true;
                }
                //sucess = IsValidXml(message);
            }
            catch (Exception e)
            {
                sucess = false;
                message = e.Message;
            }
            Assert.AreEqual(true, sucess, message);
        }


        private static LendersOffice.GDMS.LookupMethods.LookupMethodsClient CreateLookupMethodsInstance()
        {
            var url = ConstStage.TestIntegrations_GlobalDMS_LookupMethods;
            Assert.IsTrue(Uri.IsWellFormedUriString(url, UriKind.Absolute), string.Format("Invalid URL: {0}", url));
            return new LendersOffice.GDMS.LookupMethods.LookupMethodsClient(GetDefaultBinding(), new EndpointAddress(url));
        }
        public static LendersOffice.GDMS.Orders.OrdersClient CreateOrderInstance()
        {
            var url = ConstStage.TestIntegrations_GlobalDMS_Orders;
            Assert.IsTrue(Uri.IsWellFormedUriString(url, UriKind.Absolute), string.Format("Invalid URL: {0}", url));
            return new LendersOffice.GDMS.Orders.OrdersClient(GetDefaultBinding(), new EndpointAddress(url));
        }

        private static LendersOffice.GDMS.FileUpload.FileUploadClient CreateFileUploadInstance()
        {
            var url = ConstStage.TestIntegrations_GlobalDMS_FileUpload;
            Assert.IsTrue(Uri.IsWellFormedUriString(url, UriKind.Absolute), string.Format("Invalid URL: {0}", url));
            return new LendersOffice.GDMS.FileUpload.FileUploadClient(GetDefaultBinding(WSMessageEncoding.Mtom), new EndpointAddress(url));
        }

        static BasicHttpBinding GetDefaultBinding()
        {
            return GetDefaultBinding(WSMessageEncoding.Text);
        }
        //http://stackoverflow.com/questions/54579/wcf-configuration-without-a-config-file
        static BasicHttpBinding GetDefaultBinding(WSMessageEncoding messageEncoding)
        {
            var binding = new BasicHttpBinding
            {
                SendTimeout = TimeSpan.FromMinutes(1),
                OpenTimeout = TimeSpan.FromMinutes(1),
                CloseTimeout = TimeSpan.FromMinutes(1),
                ReceiveTimeout = TimeSpan.FromMinutes(10),
                AllowCookies = false,
                BypassProxyOnLocal = false,
                HostNameComparisonMode = HostNameComparisonMode.StrongWildcard,
                MessageEncoding = messageEncoding,
                TextEncoding = System.Text.Encoding.UTF8,
                TransferMode = TransferMode.Buffered,
                UseDefaultWebProxy = true
            };
            binding.ReaderQuotas.MaxDepth = 32;
            binding.ReaderQuotas.MaxStringContentLength = 8192;
            binding.ReaderQuotas.MaxArrayLength = 16384;
            binding.ReaderQuotas.MaxBytesPerRead = 4096;
            binding.ReaderQuotas.MaxNameTableCharCount = 16384;

            binding.Security.Mode = BasicHttpSecurityMode.None;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None;
            binding.Security.Transport.ProxyCredentialType = HttpProxyCredentialType.None;
            binding.Security.Message.ClientCredentialType = BasicHttpMessageCredentialType.UserName;
            binding.Security.Message.AlgorithmSuite = System.ServiceModel.Security.SecurityAlgorithmSuite.Default;
            // I think most (or all) of these are defaults--I just copied them from app.config:
            return binding;
        }
    }

}

