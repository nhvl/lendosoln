﻿namespace LendOSolnTest.Integrations
{
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Integration.DocumentCapture;
    using LqbGrammar.DataTypes;
    using NUnit.Framework;

    [TestFixture]
    public class KtaAuthenticationHandlerTest
    {
        [Test]
        [ExpectedException(typeof(CBaseException))]
        public void BadRequest_ThrowsException()
        {
            var request = new KtaAuthenticationRequest();
            var handler = new KtaAuthenticationHandler(request);
            Assert.Fail("Expected exception from invalid request.");
        }

        [Test]
        public void VerifyCookieData()
        {
            var request = this.GetRequest();
            var handler = new KtaAuthenticationHandler(request);
            handler.Generate();

            var cookieData = EncryptionHelper.DecryptKta(handler.EncryptedCookieData);
            var cookiePieces = cookieData.Split('|');

            Assert.AreEqual(3, cookiePieces.Length);
            Assert.AreEqual(request.UserLogin, cookiePieces[2], "Expected user login to be present in cookie data");
        }

        [Test]
        public void VerifyPayloadData()
        {
            var request = this.GetRequest();
            var handler = new KtaAuthenticationHandler(request);
            handler.Generate();

            var payloadData = EncryptionHelper.DecryptKta(handler.EncryptedXmlPayload);
            var payload = Tools.CreateXmlDoc(payloadData);

            Assert.AreEqual(request.UserLogin, payload.DocumentElement.SelectSingleNode("//UserLogin")?.InnerText);
            Assert.AreEqual(request.KtaUsername, payload.DocumentElement.SelectSingleNode("//KtaLogin")?.InnerText);
            Assert.AreEqual(request.KtaPassword.Value, payload.DocumentElement.SelectSingleNode("//KtaPass")?.InnerText);
            Assert.AreEqual(request.RequestId.ToString(), payload.DocumentElement.SelectSingleNode("//OcrId")?.InnerText);
            Assert.AreEqual(request.LoanName, payload.DocumentElement.SelectSingleNode("//LoanName")?.InnerText);
        }

        [Test]
        public void VerifyCookieAndPayloadDataMatches()
        {
            var request = this.GetRequest();
            var handler = new KtaAuthenticationHandler(request);
            handler.Generate();

            var cookieData = EncryptionHelper.DecryptKta(handler.EncryptedCookieData);
            var cookiePieces = cookieData.Split('|');

            var payloadData = EncryptionHelper.DecryptKta(handler.EncryptedXmlPayload);
            var payload = Tools.CreateXmlDoc(payloadData);

            Assert.AreEqual(cookiePieces[0], payload.DocumentElement.SelectSingleNode("//Id")?.InnerText);
            Assert.AreEqual(cookiePieces[1], payload.DocumentElement.SelectSingleNode("//RequestTime")?.InnerText);
            Assert.AreEqual(cookiePieces[2], payload.DocumentElement.SelectSingleNode("//UserLogin")?.InnerText);
        }

        private KtaAuthenticationRequest GetRequest()
        {
            return new KtaAuthenticationRequest()
            {
                CustomerCode = "PMLXXXX",
                KtaUsername = "ktausername",
                KtaPassword = "ktapass",
                UserLogin = "ktauserlogin",
                RequestId = OcrRequestId.Generate(),
                LoanName = "ktaloan"
            };
        }
    }
}
