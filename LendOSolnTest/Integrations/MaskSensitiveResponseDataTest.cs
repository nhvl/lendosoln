﻿namespace LendOSolnTest.Integrations
{
    using System.IO;
    using DataAccess;
    using LendersOffice.Integration.Irs4506T;
    using LendersOffice.Integration.MortgageInsurance;
    using LendOSolnTest.Common;
    using LQB4506T.LQB4506TPost;
    using NUnit.Framework;

    [TestFixture]
    public class MaskSensitiveResponseDataTest
    {
        [Test]
        public void MiResponse_DoesMask()
        {
            var response = this.GetSampleMiResponse();
            var maskedResponseXml = MIResponseProvider.MaskSensitiveResponseData(response);

            var xmlDoc = Tools.CreateXmlDoc(maskedResponseXml);
            var ssnNode = xmlDoc.SelectSingleNode("RESPONSE_GROUP/RESPONSE/RESPONSE_DATA/MI_RESPONSE/BORROWER/@_SSN");
            Assert.AreEqual("***-**-9999", ssnNode.InnerText);
        }

        [Test]
        public void MiResponse_AfterMask_IsValidResponseXml()
        {
            var response = this.GetSampleMiResponse();
            var maskedResponseXml = MIResponseProvider.MaskSensitiveResponseData(response);

            var maskedResponse = new MIResponseProvider(maskedResponseXml, MIResponseDataSource.LQBDB);

            Assert.IsNotNull(maskedResponse);
            Assert.IsTrue(maskedResponse.HasResponse);
            Assert.IsTrue(maskedResponse.HasResponseInfo);
            Assert.AreEqual("***-**-9999", maskedResponse.ResponseInfo.Borrower._SSN);
        }

        [Test]
        public void Lqb4506TPostResponse_DoesMask()
        {
            var response = this.GetSampleLqb4506TPostResponse();
            var maskedResponseXml = Irs4506TOrderInfo.MaskSensitiveLqbPostResponseData(response);

            var xmlDoc = Tools.CreateXmlDoc(maskedResponseXml);
            var ssnNode = xmlDoc.SelectSingleNode("LQB4506TPost/Data/Borrower/@_SSN");
            Assert.AreEqual("***-**-9999", ssnNode.InnerText);
        }

        [Test]
        public void Lqb4506TPostResponse_AfterMask_IsValidResponseXml()
        {
            var response = this.GetSampleLqb4506TPostResponse();
            var maskedResponseXml = Irs4506TOrderInfo.MaskSensitiveLqbPostResponseData(response);

            var post = new Lqb4506TPost();

            using (var stringReader = new StringReader(maskedResponseXml))
            using (var xmlReader = System.Xml.XmlReader.Create(stringReader))
            {
                post.ReadXml(xmlReader);
            }

            Assert.IsNotNull(post.Data?.Borrower);
            Assert.AreEqual("***-**-9999", post.Data.Borrower.Ssn);
        }

        private string GetSampleMiResponse()
        {
            return TestUtilities.ReadFile("SampleMiResponse.xml");
        }

        private string GetSampleLqb4506TPostResponse()
        {
            return TestUtilities.ReadFile("SampleLqb4506TPostResponse.xml");
        }
    }
}
