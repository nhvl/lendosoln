﻿namespace LendOSolnTest.Integrations
{
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using LendOSolnTest.Common;
    using LendOSolnTest.Controls;
    using NUnit.Framework;

    [TestFixture]
    [Category(CategoryConstants.NewBuildServerIgnore)]
    [Category(CategoryConstants.ExpectFailureOnLocalhost)]
    public class _4506VendorsTests : CustomHttpPost
    {
        [Test]
        public void Test()
        {
            var invalidUrlCases = new List<VendorTestCase>();
            var failCases = new List<VendorTestCase>();
            var errorMessages = new List<string>();

            foreach (var @case in VendorTestCase._4506Cases())
            {
                var url = @case.Url;
                if (string.IsNullOrEmpty(url)) { continue; }

                if (!Uri.IsWellFormedUriString(url, UriKind.Absolute)) { invalidUrlCases.Add(@case); continue; }

                var error = Vendor4506T(url);
                if (string.IsNullOrEmpty(error)) { continue; }

                failCases.Add(@case);
                errorMessages.Add(error);
            }

            if ((invalidUrlCases.Count + failCases.Count) < 1) { return; }

            Assert.Fail(VendorTestCase.FailMessage(invalidUrlCases, failCases, errorMessages));
        }

        /// <summary>
        /// Try to send request and check the result is valid XML.
        /// </summary>
        /// <param name="url"></param>
        /// <returns>`null` if passed. Otherwise, an error string is returned.</returns>
        string Vendor4506T(string url)
        {
            try
            {
                var res = SendRequest(url.Trim(), CreateRequest());
                return (Methods.IsValidXml(res) ? null :
                    string.Format("Error: Invalid XML result.{0}{1}", Environment.NewLine, res));
            }
            catch (Exception e)
            {
                return string.Format("Exception: {0}", e.Message);
            }
        }        

        private new string CreateRequest()
        {
            _xmlReq = new XmlDocument();

            // Root LQBAppraisalRequest Node
            var rootNode = _xmlReq.CreateElement("LQB4506TOrderRequest");
            AddAttribute(rootNode, "Username", "testuser");
            AddAttribute(rootNode, "Password", "password");
            AddAttribute(rootNode, "AccountID", "12345");
            _xmlReq.AppendChild(rootNode);

            var sNode = GetNewNode("Source");
            var snNode = GetNewNode("SourceName");
            snNode.InnerText = "LendingQB";
            sNode.AppendChild(snNode);
            snNode = GetNewNode("TransactionID");
            snNode.InnerText = "08034-41541-1423413-14413";
            sNode.AppendChild(snNode);
            snNode = GetNewNode("LoanID");
            snNode.InnerText = "35415HH-D886SF-D79W90-K3SN0L";
            sNode.AppendChild(snNode);
            snNode = GetNewNode("LoanName");
            snNode.InnerText = "TEST LOAN";
            sNode.AppendChild(snNode);
            rootNode.AppendChild(sNode);

            return _xmlReq.OuterXml;
        }
    }
}
