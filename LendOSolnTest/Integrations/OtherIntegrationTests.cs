﻿using System;
using System.Net;
using System.Xml;
using System.Linq;
using LendersOffice.Constants;
using LendOSolnTest.Common;
using LendOSolnTest.Controls;
using NUnit.Framework;
using MeridianLink.CCGateway;
using System.Collections.Generic;
using LendOSolnTest.Fakes;
using DataAccess;
using LendersOffice.Lon;
using System.Threading.Tasks;

namespace LendOSolnTest.Integrations
{
    [TestFixture]
    [Category(CategoryConstants.NewBuildServerIgnore)]
    public class OtherIntegrationTests:CustomHttpPost
    {
        [TestFixtureSetUp]
        public void InitFixture()
        {
            LoginTools.LoginAs(TestAccountType.LoTest001);
        }

        #region Test
        [Test]
        [Explicit("Test not valid in dev")]
        public void FreddieMac()
        {
            var url = ConstStage.TestIntegrations_OtherIntegration_FreddieMac;
            Assert.IsTrue(Uri.IsWellFormedUriString(url, UriKind.Absolute), string.Format("Invalid URL: {0}", url));
            Methods.TestHttpMethodWithCredential(url, new NetworkCredential("merilink_lpr1", "Cal1f0rn1a"), WebRequestMethods.Http.Get);
        }

        [Test]
        [Explicit("Test not valid in dev")]
        public void DataVerify()
        {
            var url = ConstStage.TestIntegrations_OtherIntegration_DataVerify;
            Assert.IsTrue(Uri.IsWellFormedUriString(url, UriKind.Absolute), string.Format("Invalid URL: {0}", url));
            TestDRIVE(url);
        }

        [Test]
        [Category(CategoryConstants.ExpectFailureOnLocalhost)]
        public void CreditReportingAgency()
        {
            var invalidUrlCases = new List<VendorTestCase>();
            var failCases = new List<VendorTestCase>();
            var errorMessages = new List<string>();
            var urls = new List<string>();

            foreach (var @case in VendorTestCase.CreditReportingAgencyCase())
            {
                var url = @case.Url;

                if (string.IsNullOrEmpty(url)) { continue; }
                if (!Uri.IsWellFormedUriString(url, UriKind.Absolute)) { invalidUrlCases.Add(@case); continue; }
                urls.Add(url);
            }

            Task.Factory.ContinueWhenAll(urls.Select(url => Methods.TestHttpMethodAsync(url, WebRequestMethods.Http.Get)).ToArray(),
                completedTasks =>
                {
                    foreach (Task<Tuple<string,string>> t in completedTasks)
                    {
                        failCases.Add(new VendorTestCase(t.Result.Item1, t.Result.Item1));
                        errorMessages.Add(t.Result.Item2);
                    }
                });

            if ((invalidUrlCases.Count + failCases.Count) < 1) { return; }
            Assert.Fail(VendorTestCase.FailMessage(invalidUrlCases, failCases, errorMessages));
        }

        [Test]
        public void CCPayment()
        {
            //https://lqbopm/default.asp?219525
            //Create nunit test to ensure LQB system can connect to cc payment
            var url = AppSettings.Linkpoint_Webservice_Url;
            if (String.IsNullOrEmpty(url)) return;
            Assert.IsTrue(Uri.IsWellFormedUriString(url, UriKind.Absolute), string.Format("Invalid URL: {0}", url));
            Methods.TestHttpMethod(url, WebRequestMethods.Http.Get);
        }
        [Test]
        public void LON()
        {
            //https://lqbopm/default.asp?398900
            string status;
            try
            {
                CPageData loanData = FakePageData.Create();
                status = UpdateLONMainClass.ProcessMessage(loanData, ConstStage.LON_USERNAME, ConstStage.LON_PASSWORD);
            }
            catch (Exception e)
            {
                status = e.Message;
            }

            Assert.IsTrue(string.IsNullOrWhiteSpace(status)
                || status.StartsWith("Account Disabled", true, System.Globalization.CultureInfo.CurrentUICulture)
                || status.StartsWith("Loanstatus Disabled", true, System.Globalization.CultureInfo.CurrentUICulture)
                || status.StartsWith("Invalid Username", true, System.Globalization.CultureInfo.CurrentUICulture)
                || status.StartsWith("Invalid Broker ID", true, System.Globalization.CultureInfo.CurrentUICulture)
                || status.StartsWith("Missing Broker ID", true, System.Globalization.CultureInfo.CurrentUICulture)
                || status.StartsWith("Missing STRLOANNUMBER", true, System.Globalization.CultureInfo.CurrentUICulture)
                || status.StartsWith("Invalid LoanNumber", true, System.Globalization.CultureInfo.CurrentUICulture), status);

        }
        #endregion

        #region function
        public void TestDRIVE(string url)
        {
            var message = "";
            object httpStatus;
            try
            {
                //var dr = new CustomHttpPost2(url);
                //var res = dr.MakeHTTPRequest(dr.GetDRIVERequestString(userName, passWord));
                var res = SendRequest(url, CreateRequest());
                if (!Methods.IsValidXml(res))
                    //if (!res.Contains("DRIVEResponse"))
                {
                    httpStatus = "Error";
                    message = res;
                }
                else { httpStatus = HttpStatusCode.OK; }
            }
            catch (Exception e)
            {
                httpStatus = "Exception";
                message = e.Message;
            }
            Assert.AreEqual(HttpStatusCode.OK, httpStatus, message);
        }

        /// <summary>
        /// DRIVE Request DVDirectIntegration CBC.doc
        /// </summary>
        /// <returns></returns>
        private new string CreateRequest()
        {
            _xmlReq = new XmlDocument();

            // Root DRIVERequest Node
            var rootNode = _xmlReq.CreateElement("DRIVERequest");
            AddAttribute(rootNode, "Version", "1.00");
            _xmlReq.AppendChild(rootNode);

            // Authentication Node
            var aNode = GetNewNode("Authentication");
            AddAttribute(aNode, "Password", "");
            AddAttribute(aNode, "Username", "");
            rootNode.AppendChild(aNode);

            // BatchRequest Node
            var bNode = GetNewNode("BatchRequest");
            AddAttribute(bNode, "CustomerBatchIdentifier", "testbatch1");
            rootNode.AppendChild(bNode);

            // Loan Node
            var lNode = GetNewNode("Loan");
            AddAttribute(lNode, "LoanNumber", "testloan1");
            bNode.AppendChild(lNode);

            // Property Information Node
            var prNode = GetNewNode("PropertyInformation");
            AddAttribute(prNode, "StreetAddress", "1 Main Street");
            AddAttribute(prNode, "ZIP", "75201");
            AddAttribute(prNode, "PropertyUsageType", "Primary Residence");
            AddAttribute(prNode, "City", "Chicago");
            AddAttribute(prNode, "RefinancePurpose", "No Cash-Out");
            AddAttribute(prNode, "State", "IL");
            AddAttribute(prNode, "LoanPurpose", "Purchase");
            AddAttribute(prNode, "ValuationDate", "2006-05-13");
            AddAttribute(prNode, "AppraisedValue", "100000");
            AddAttribute(prNode, "PropertyType", "Single Family");
            lNode.AppendChild(prNode);

            // Borrower Node
            var brNode = GetNewNode("Borrower");
            AddAttribute(brNode, "StreetAddress", "1 Main Street");
            AddAttribute(brNode, "ZIP", "75201");
            AddAttribute(brNode, "FirstName", "Bob");
            AddAttribute(brNode, "LastName", "Smith");
            AddAttribute(brNode, "SSN", "111222233");
            lNode.AppendChild(brNode);


            return _xmlReq.OuterXml;
        }

        #endregion
    }
}
