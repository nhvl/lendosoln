﻿namespace LendOSolnTest.Integrations
{
    using System;
    using System.Xml.Linq;
    using LendersOffice.Integration.UcdDelivery.FreddieMac;
    using NUnit.Framework;

    [TestFixture]
    public class LoanClosingAdvisorTests
    {
        XNamespace soap = "http://schemas.xmlsoap.org/soap/envelope/";
        XNamespace wss = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
        XNamespace lcla = "http://www.freddiemac.com/lcla/LCLALoanEvaluationService/schemas";
        XNamespace fre = "http://www.efreddiemac.com";
        XNamespace mismo = "http://www.mismo.org/residential/2009/schemas";

        private static FreddieMacUcdDeliveryRequestData CreateRequestData()
        {
            var lclaEvaluationRequest = new LoanClosingAdvisorLoanEvaluationRequest
            {
                CorrelationId = "12345678",
                RelationshipWithFreddie = LendersOffice.Admin.LenderRelationshipWithFreddieMac.Seller,
                SellerIdentifier = "123456",
                FreddieUserName = "TestFreddieUserName",
                FreddiePassword = "TestFreddiePassword",
                UcdFile = new System.Lazy<string>(() => System.IO.File.ReadAllText(System.IO.Path.Combine(Common.TestDataRootFolder.Location, "UcdExample.xml")))
            };

            var requestData = new FreddieMacUcdDeliveryRequestData(Guid.Empty, Guid.Empty, null, null, null, Guid.Empty, LendersOffice.Admin.LenderRelationshipWithFreddieMac.Undefined);
            requestData.LoanClosingAdvisorRequest = lclaEvaluationRequest;
            return requestData;
        }

        /// <summary>
        /// Traverses a simple XElment path while asserting that each element exists.
        /// </summary>
        /// <returns>The element at the end of the path.</returns>
        private XElement AssertElementPathExists(XElement element, params XName[] elementPath)
        {
            var currentElement = element;
            Assert.IsNotNull(element, "Base element was null.");
            string elementPathString = element.Name.LocalName.ToString();
            foreach (XName elementName in elementPath)
            {
                currentElement = currentElement.Element(elementName);
                elementPathString = $"{elementPathString}/{elementName.LocalName}";
                Assert.IsNotNull(currentElement, $"Element \"{elementPathString}\" was not found.");
            }
            return currentElement;
        }

        [Test]
        public void GenerateRequestBody_ValidUcdInput_SoapFormat()
        {
            var requestData = CreateRequestData();

            var provider = new LoanClosingAdvisorRequestProvider(requestData);
            var lclaEvaluationRequest = requestData.LoanClosingAdvisorRequest;
            var soapRequest = provider.SerializeRequest();
            var soapXRequest = XDocument.Parse(soapRequest);
            var envelopeElement = soapXRequest.Element(soap + "Envelope");
            Assert.IsNotNull(envelopeElement);

            var usernameTokenElement = AssertElementPathExists(envelopeElement, soap + "Header", wss + "Security", wss + "UsernameToken");
            Assert.AreEqual(lclaEvaluationRequest.FreddieUserName, usernameTokenElement.Element(wss + "Username").Value);
            Assert.AreEqual(lclaEvaluationRequest.FreddiePassword, usernameTokenElement.Element(wss + "Password").Value);

            var serviceRequestElement = AssertElementPathExists(envelopeElement, soap + "Body", lcla + "LCLALoanEvaluationServiceRequest");
            Assert.AreEqual(lclaEvaluationRequest.CorrelationId, serviceRequestElement.Element(lcla + "CorrelationID").Value);

            var customerIdElement = AssertElementPathExists(serviceRequestElement, lcla + "CustomerIdentifications", lcla + "CustomerIdentification");
            Assert.AreEqual(lclaEvaluationRequest.RelationshipWithFreddie.ToString("G"), customerIdElement.Element(lcla + "CustomerIdentifierType").Value);
            Assert.AreEqual(lclaEvaluationRequest.SellerIdentifier, customerIdElement.Element(lcla + "CustomerIdentifier").Value);

            var vendorIdElement = AssertElementPathExists(serviceRequestElement, lcla + "VendorIdentification");
            Assert.AreEqual("000184", vendorIdElement.Element(lcla + "VendorIdentifier").Value);
            Assert.AreEqual("MeridianLink", vendorIdElement.Element(lcla + "VendorName").Value);
            Assert.AreEqual("LendingQB", vendorIdElement.Element(lcla + "VendorSoftware").Value);
            Assert.AreEqual(DateTime.Now.Year.ToString(), vendorIdElement.Element(lcla + "VendorSoftwareVersion").Value);

            // Ensure the mismo serializes by checking the borrower.
            var borrowerPartyElement = AssertElementPathExists(envelopeElement,
                soap + "Body",
                lcla + "LCLALoanEvaluationServiceRequest",
                mismo + "MESSAGE",
                mismo + "DOCUMENT_SETS",
                mismo + "DOCUMENT_SET",
                mismo + "DOCUMENTS",
                mismo + "DOCUMENT",
                mismo + "DEAL_SETS",
                mismo + "DEAL_SET",
                mismo + "DEALS",
                mismo + "DEAL",
                mismo + "PARTIES",
                mismo + "PARTY");

            Assert.AreEqual("Mary", borrowerPartyElement.Element(mismo + "INDIVIDUAL").Element(mismo + "NAME").Element(mismo + "FirstName").Value);
            Assert.AreEqual("Borrower", borrowerPartyElement.Element(mismo + "ROLES").Element(mismo + "ROLE").Element(mismo + "ROLE_DETAIL").Element(mismo + "PartyRoleType").Value);

            var embeddedContentXml = AssertElementPathExists(envelopeElement,
                soap + "Body",
                lcla + "LCLALoanEvaluationServiceRequest",
                mismo + "MESSAGE",
                mismo + "DOCUMENT_SETS",
                mismo + "DOCUMENT_SET",
                mismo + "DOCUMENTS",
                mismo + "DOCUMENT",
                mismo + "VIEWS",
                mismo + "VIEW",
                mismo + "VIEW_FILES",
                mismo + "VIEW_FILE",
                mismo + "FOREIGN_OBJECT",
                mismo + "EmbeddedContentXML").Value;
            Assert.AreNotEqual(string.Empty, embeddedContentXml);
        }

        [Test]
        public void LogRequestBody_ValidUcdInput_SoapFormat()
        {
            var requestData = CreateRequestData();

            var provider = new LoanClosingAdvisorRequestProvider(requestData);
            var lclaEvaluationRequest = requestData.LoanClosingAdvisorRequest;
            var logXRequest = LoanClosingAdvisorRequestProvider.CreateXml(provider.Request.LoanClosingAdvisorRequest, forLogging: true);

            var usernameTokenElement = AssertElementPathExists(logXRequest, soap + "Header", wss + "Security", wss + "UsernameToken");
            Assert.AreEqual(string.Empty, usernameTokenElement.Element(wss + "Username").Value);
            Assert.AreEqual(string.Empty, usernameTokenElement.Element(wss + "Password").Value);

            var embeddedContentXml = AssertElementPathExists(logXRequest,
                soap + "Body",
                lcla + "LCLALoanEvaluationServiceRequest",
                mismo + "MESSAGE",
                mismo + "DOCUMENT_SETS",
                mismo + "DOCUMENT_SET",
                mismo + "DOCUMENTS",
                mismo + "DOCUMENT",
                mismo + "VIEWS",
                mismo + "VIEW",
                mismo + "VIEW_FILES",
                mismo + "VIEW_FILE",
                mismo + "FOREIGN_OBJECT",
                mismo + "EmbeddedContentXML").Value;
            Assert.AreEqual(string.Empty, embeddedContentXml);
        }
    }
}
