using System;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Xml;
using LendersOffice.Constants;
using LendOSolnTest.Common;
using LendOSolnTest.Controls;
using NUnit.Framework;

namespace LendOSolnTest.Integrations
{
    [TestFixture]
    [Category(CategoryConstants.NewBuildServerIgnore)]
    public class DocumentVendorsTests : CustomHttpPost
    {
        [Test]
        public void DocMagic()
        {
            //iOPM 231465
            var url = VendorTestCase.DocumentCases("DocMagic - Production").Url;
            if (string.IsNullOrEmpty(url)) { return; }
            Assert.IsTrue(Uri.IsWellFormedUriString(url, UriKind.Absolute), string.Format("Invalid URL: {0}", url));
            DocumentVendors(url, "xmlRequest", null);
        }
        [Test]
        [Explicit("Test not valid in dev")]
        public void DocuTech()
        {
            var url = VendorTestCase.DocumentCases("DocuTech (JX Server)").Url;
            if (string.IsNullOrEmpty(url)) { return; }
            Assert.IsTrue(Uri.IsWellFormedUriString(url, UriKind.Absolute), string.Format("Invalid URL: {0}", url));
            DocumentVendors(url, "xmlRequest", null);
        }

        [Test]
        [Ignore("Broken until Tim Fixes it")]
        public void Docs_on_Demand()
        {
            var url = VendorTestCase.DocumentCases("Docs on Demand").Url;
            if (string.IsNullOrEmpty(url)) { return; }
            Assert.IsTrue(Uri.IsWellFormedUriString(url, UriKind.Absolute), string.Format("Invalid URL: {0}", url));
            DocumentVendors(url, "**APPLICATION/XML**", null);
        }

        [Test]
        [Explicit("Test not valid in dev")]
        public void IDS()
        {
            var url = VendorTestCase.DocumentCases("IDS STAGE").Url;
            if (string.IsNullOrEmpty(url)) { return; }
            Assert.IsTrue(Uri.IsWellFormedUriString(url, UriKind.Absolute), string.Format("Invalid URL: {0}", url));
            DocumentVendors(url, "request", "&authentication=c88b33d4bdb840ec95bcd931595cb448");
        }

        private void DocumentVendors(string url, string parameterName, string extraParameters)
        {
            var message = "";
            object httpStatus;
            try
            {
                var res = SendRequest(url, CreateRequest(), parameterName, extraParameters);
                if (!Methods.IsValidXml(res))
                {
                    httpStatus = "Error";
                    message = res;
                }
                else { httpStatus = HttpStatusCode.OK; }
            }
            catch (Exception e)
            {
                httpStatus = "Exception";
                message = e.Message;
            }
            Assert.AreEqual(HttpStatusCode.OK, httpStatus, message);
        }

        private static string SendRequest(string url, string requestData, string parameterName, string extraParameters)
        {
            var bytes =
                Encoding.UTF8.GetBytes((string.IsNullOrEmpty(parameterName) ? "xmlRequest" : parameterName) + "=" +
                                       HttpUtility.UrlEncode(requestData) + extraParameters);

            var webRequest = (HttpWebRequest) WebRequest.Create(url);
            webRequest.KeepAlive = false;
            webRequest.Method = "POST";
            webRequest.Timeout = 200000;
            webRequest.ContentLength = bytes.Length;
            webRequest.ContentType = "application/x-www-form-urlencoded";


            var newStream = webRequest.GetRequestStream();
            newStream.Write(bytes, 0, bytes.Length);
            newStream.Close();

            var respObj = webRequest.GetResponse();
            string responseString;
            using (var sr = new StreamReader(respObj.GetResponseStream()))
            {
                responseString = sr.ReadToEnd();
            }
            respObj.Close();
            return responseString;
        }

        private new string CreateRequest()
        {
            _xmlReq = new XmlDocument();

            // Root LQBDocumentRequest Node
            var rootNode = _xmlReq.CreateElement("LQBDocumentRequest");
            AddAttribute(rootNode, "xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            AddAttribute(rootNode, "xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
            AddAttribute(rootNode, "xmlns", "http://tempuri.org/LQBDocumentRequest");
            _xmlReq.AppendChild(rootNode);

            //RequestSource Node
            var sNode = GetNewNode("RequestSource");
            var snNode = GetNewNode("SourceName");
            snNode.InnerText = "LQBSampleID";
            sNode.AppendChild(snNode);
            snNode = GetNewNode("TransactionID");
            snNode.InnerText = "a2d4c4fc-4535-4396-bf0c-038aaa8ebf1e";
            sNode.AppendChild(snNode);
            rootNode.AppendChild(sNode);

            //Authentication Node
            sNode = GetNewNode("Authentication");
            snNode = GetNewNode("UserName");
            snNode.InnerText = "sample@lendingqb.com";
            sNode.AppendChild(snNode);
            snNode = GetNewNode("AccountID");
            snNode.InnerText = "123456";
            sNode.AppendChild(snNode);
            snNode = GetNewNode("Password");
            snNode.InnerText = "password";
            sNode.AppendChild(snNode);
            rootNode.AppendChild(sNode);

            //Request Node
            sNode = GetNewNode("Request");
            AddAttribute(sNode, "RequestType", "AccountInfo");
            snNode = GetNewNode("AccountInfoRequest");
            AddAttribute(snNode, "RequestLoanPrograms", "N");
            AddAttribute(snNode, "RequestPermissions", "Y");
            AddAttribute(snNode, "RequestEnabledPackages", "N");
            sNode.AppendChild(snNode);
            rootNode.AppendChild(sNode);

            return _xmlReq.OuterXml;
        }
    }
}
