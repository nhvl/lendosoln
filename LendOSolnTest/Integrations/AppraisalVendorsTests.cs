﻿using System;
using System.Collections.Generic;
using System.Xml;
using LendOSolnTest.Common;
using LendOSolnTest.Controls;
using NUnit.Framework;

namespace LendOSolnTest.Integrations
{
    [TestFixture]
    [Category(CategoryConstants.NewBuildServerIgnore)]
    public class AppraisalVendorsTests : CustomHttpPost
    {
        [Test]
        public void Test()
        {
            var invalidUrlCases = new List<VendorTestCase>();
            var failCases = new List<VendorTestCase>();
            var errorMessages = new List<string>();

            foreach (var @case in VendorTestCase.AppraisalCases())
            {
                var url = @case.Url;
                if (string.IsNullOrEmpty(url)) { continue; }

                if (!Uri.IsWellFormedUriString(url, UriKind.Absolute)) { invalidUrlCases.Add(@case); continue; }

                var error = Appraisal(url);
                if (string.IsNullOrEmpty(error)) { continue; }

                failCases.Add(@case);
                errorMessages.Add(error);
            }

            if ((invalidUrlCases.Count + failCases.Count) < 1) { return; }

            Assert.Fail(VendorTestCase.FailMessage(invalidUrlCases, failCases, errorMessages));
        }

        /// <summary>
        /// Try to send request and check the result is valid XML.
        /// </summary>
        /// <param name="url"></param>
        /// <returns>`null` if passed. Otherwise, an error string is returned.</returns>
        string Appraisal(string url)
        {
            try
            {
                var res = SendRequest(url.Trim(), CreateRequest());
                return (Methods.IsValidXml(res) ? null :
                    string.Format("Error: Invalid XML result.{0}{1}", Environment.NewLine, res));
            }
            catch (Exception e)
            {
                return string.Format("Exception: {0}", e.Message);
            }
        }

        private new string CreateRequest()
        {
            _xmlReq = new XmlDocument();

            // Root LQBAppraisalRequest Node
            var rootNode = _xmlReq.CreateElement("LQBAppraisalRequest");
            AddAttribute(rootNode, "Username", "testuser2");
            AddAttribute(rootNode, "Password", "passwrod1");
            AddAttribute(rootNode, "AccountID", "12345");
            _xmlReq.AppendChild(rootNode);

            // Authentication Node
            var aNode = GetNewNode("AccountInfoRequest");
            AddAttribute(aNode, "RequestProducts", "Y");
            AddAttribute(aNode, "RequestBillingMethods", "Y");
            rootNode.AppendChild(aNode);
            return _xmlReq.OuterXml;

        }
    }
}
