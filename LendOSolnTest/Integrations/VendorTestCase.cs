﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using LendersOffice.Constants;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Integration.Appraisals;
using LendersOffice.Integration.DocumentVendor;
using LendersOffice.Integration.Irs4506T;

namespace LendOSolnTest.Integrations
{
    public class VendorTestCase
    {
        public string Name { get; set; }
        public string Url { get; set; }

        public VendorTestCase()
        {

        }

        public VendorTestCase(string name, string url)
        {
            Name = name;
            Url = url.Trim();
        }

        public static List<VendorTestCase> _4506Cases()
        {
            IEnumerable<Irs4506TVendorConfiguration> listVendor = Irs4506TVendorConfiguration.ListActiveVendor();
            List<VendorTestCase> list = new List<VendorTestCase>();

            foreach (Irs4506TVendorConfiguration vendor in listVendor)
            {
                if (vendor.EnableConnectionTest)
                {
                    list.Add(new VendorTestCase(vendor.VendorName, vendor.ExportPath));
                }
            }
            return list;
        }

        public static List<VendorTestCase> AppraisalCases()
        {
            IEnumerable<AppraisalVendorConfig> listVendor = AppraisalVendorConfig.ListAll();
            List<VendorTestCase> list = new List<VendorTestCase>();

            foreach (AppraisalVendorConfig appraisal in listVendor)
            {
                if (appraisal.EnableConnectionTest)
                {
                    //Case 311338 - remove Got Appraisal from unitTest
                    if (!appraisal.PrimaryExportPath.Contains("gotodin.com"))
                        list.Add(new VendorTestCase(appraisal.VendorName, appraisal.PrimaryExportPath));
                }
            }
            return list;
        }

        public static VendorTestCase DocumentCases(string vendorName)
        {
            List<VendorConfig> listVendor = DocumentVendorFactory.AvailableVendors();
            VendorTestCase list = new VendorTestCase();
            foreach (VendorConfig vendor in listVendor)
            {
                if (vendor.VendorName == vendorName && vendor.EnableConnectionTest)
                {
                    list = new VendorTestCase(vendor.VendorName, vendor.PrimaryExportPath.ToString());
                }
            }
            return list;
        }

        public static List<VendorTestCase> CreditReportingAgencyCase()
        {
            List<VendorTestCase> list = new List<VendorTestCase>();

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "INTERNAL_USER_ONLY_ListAllCras"))
            {
                while (reader.Read())
                {
                    int n;
                    bool isValid;
                    if ((!int.TryParse(reader["ComUrl"].ToString(), out n)) && bool.TryParse(reader["IsValid"].ToString(), out isValid)  && isValid)
                    {
                        list.Add(new VendorTestCase(reader["ComNm"].ToString(), reader["ComUrl"].ToString()));
                    }
                }
            }
            return list;
        }

        public static readonly VendorTestCase[] LoanPQCases = new[]
        { 
            new VendorTestCase("Secure Inovistar"    , ConstStage.TestIntegrations_LoansPQ_Secure_Inovistar    ), // Test not valid in dev
            new VendorTestCase("Eplqa"               , ConstStage.TestIntegrations_LoansPQ_Eplqa               ),
            new VendorTestCase("PS"                  , ConstStage.TestIntegrations_LoansPQ_PS                  ),
            new VendorTestCase("Californiacu"        , ConstStage.TestIntegrations_LoansPQ_Californiacu        ), // Test not valid in dev
            new VendorTestCase("CS"                  , ConstStage.TestIntegrations_LoansPQ_CS                  ),
            new VendorTestCase("PFFCU"               , ConstStage.TestIntegrations_LoansPQ_PFFCU               ), // Test not valid in dev
            new VendorTestCase("Kinecta"             , ConstStage.TestIntegrations_LoansPQ_Kinecta             ), // Test not valid in dev
            new VendorTestCase("Calcoastcu"          , ConstStage.TestIntegrations_LoansPQ_Calcoastcu          ), // Test not valid in dev
            new VendorTestCase("Demo"                , ConstStage.TestIntegrations_LoansPQ_Demo                ),
            new VendorTestCase("Onpoint"             , ConstStage.TestIntegrations_LoansPQ_Onpoint             ),
            new VendorTestCase("Oregoncommunitycu"   , ConstStage.TestIntegrations_LoansPQ_Oregoncommunitycu   ),
            new VendorTestCase("Secure"              , ConstStage.TestIntegrations_LoansPQ_Secure              ),
            new VendorTestCase("ES"                  , ConstStage.TestIntegrations_LoansPQ_ES                  ), // Test not valid in dev
            new VendorTestCase("Innovistar Sumitloan", ConstStage.TestIntegrations_LoansPQ_Innovistar_Sumitloan), // Test not valid in dev
            new VendorTestCase("Aacreditunion"       , ConstStage.TestIntegrations_LoansPQ_Aacreditunion       ), // Test not valid in dev
            new VendorTestCase("Beta"                , ConstStage.TestIntegrations_LoansPQ_Beta                ), // Test not valid in dev
            new VendorTestCase("JHA"                 , ConstStage.TestIntegrations_LoansPQ_JHA                 ),
            new VendorTestCase("Coastal24"           , ConstStage.TestIntegrations_LoansPQ_Coastal24           ), // Test not valid in dev
            new VendorTestCase("BCU"                 , ConstStage.TestIntegrations_LoansPQ_BCU                 ), // Test not valid in dev
            new VendorTestCase("Summitcreditunion"   , ConstStage.TestIntegrations_LoansPQ_Summitcreditunion   ),
            new VendorTestCase("WS"                  , ConstStage.TestIntegrations_LoansPQ_WS                  ),
            new VendorTestCase("Join"                , ConstStage.TestIntegrations_LoansPQ_Join                ),
            new VendorTestCase("JDCU"                , ConstStage.TestIntegrations_LoansPQ_JDCU                ),
            new VendorTestCase("Bankoncit"           , ConstStage.TestIntegrations_LoansPQ_Bankoncit           ),
            new VendorTestCase("CACU"                , ConstStage.TestIntegrations_LoansPQ_CACU                ), // Test not valid in dev
            new VendorTestCase("Epl"                 , ConstStage.TestIntegrations_LoansPQ_Epl                 ), // Test not valid in dev
            new VendorTestCase("JhaDemo"             , ConstStage.TestIntegrations_LoansPQ_JhaDemo             ),
        };

        public static string FailMessage(IList<VendorTestCase> invalidUrlCases, IList<VendorTestCase> failCases, IList<string> errorMessages)
        {
            IEnumerable<string> lines = new string[0];

            if (invalidUrlCases.Count() > 0)
            {
                lines = lines.Concat(new[] { "Cases with invalid URL:" }).Concat(
                    invalidUrlCases.Select(c => string.Format("  - {0}: {1}", c.Name, c.Url)));
            }

            if (failCases.Count() > 0)
            {
                lines = lines.Concat(new[] { "Cases FAILED:" }).Concat(
                    failCases.Select((c, i) => string.Format("  - {1}{0}        {2}", Environment.NewLine,
                        c.Name, Indent(errorMessages.ElementAt(i), 8))));
            }
            return string.Join(Environment.NewLine, lines);
        }

        static string Indent(string lines, int indent)
        {
            return string.IsNullOrWhiteSpace(lines) ? lines :
                    Regex.Replace(lines, "\r\n|\r|\n", "$0" + new string(' ', indent));
        }
    }
}
