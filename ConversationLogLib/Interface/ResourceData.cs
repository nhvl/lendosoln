﻿namespace ConversationLogLib.Interface
{
    using System;
    using System.Runtime.Serialization;
    using System.Security.Permissions;

    /// <summary>
    /// Data used to identify a resource in the calling system.
    /// </summary>
    [Serializable]
    public class ResourceData : ISerializable
    {
        /// <summary>
        /// Coded value that represents a loan file in the conversation log system.
        /// </summary>
        public const string LoanFile = "LF";

        /// <summary>
        /// Version for the implementation, used to ensure proper de-serialization.
        /// </summary>
        private const int Version = 201702;

        /// <summary>
        /// Initializes a new instance of the <see cref="ResourceData"/> class.
        /// </summary>
        public ResourceData()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ResourceData"/> class.
        /// </summary>
        /// <param name="info">Object from which the serialized fields are pulled.</param>
        /// <param name="context">The parameter is not used.</param>
        private ResourceData(SerializationInfo info, StreamingContext context)
        {
            int version = info.GetInt32("version");

            this.ResourceType = info.GetString("resourceType");
            this.Identifier = info.GetString("identifier");
        }

        /// <summary>
        /// Gets or sets a resource type identifier, must be one of the const values provided by this class.
        /// </summary>
        /// <value>A resource type identifier.</value>
        public string ResourceType { get; set; }

        /// <summary>
        /// Gets or sets an identifier from the calling system.
        /// </summary>
        /// <value>An identifier from the calling system.</value>
        public string Identifier { get; set; }

        /// <summary>
        /// Add instance data to the serializer.
        /// </summary>
        /// <param name="info">Object to which the instance data is added.</param>
        /// <param name="context">The parameter is not used.</param>
        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("version", Version);
            info.AddValue("resourceType", this.ResourceType);
            info.AddValue("identifier", this.Identifier);
        }
    }
}
