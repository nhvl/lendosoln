﻿namespace ConversationLogLib.Interface
{
    using System;
    using System.Runtime.Serialization;
    using System.Security.Permissions;

    /// <summary>
    /// A category will be identified by a primary key and it's name.
    /// </summary>
    [Serializable]
    public sealed class CategoryData : ISerializable
    {
        /// <summary>
        /// Version for the implementation, used to ensure proper de-serialization.
        /// </summary>
        private const int Version = 201707;

        /// <summary>
        /// Initializes a new instance of the <see cref="CategoryData"/> class.
        /// </summary>
        public CategoryData()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CategoryData"/> class.
        /// </summary>
        /// <param name="info">Object from which the serialized fields are pulled.</param>
        /// <param name="context">The parameter is not used.</param>
        private CategoryData(SerializationInfo info, StreamingContext context)
        {
            int version = info.GetInt32("version");

            this.Id = info.GetInt64("id");
            this.Name = info.GetString("name");
            this.OwnerId = info.GetString("ownerId");
            this.Active = info.GetBoolean("active");

            if (version >= 201707)
            {
                if (info.GetBoolean("hasDefaultPermissionLevelId"))
                {
                    this.DefaultPermissionLevelId = info.GetInt32("defaultPermissionLevelId");
                }
            }

            if (version >= 201704)
            {
                this.DisplayName = info.GetString("displayName");
            }
            else
            {
                this.DisplayName = this.Name;
            }
        }

        /// <summary>
        /// Gets or sets unique identifier for a category.
        /// </summary>
        /// <value>Unique identifier for a category.</value>
        public long Id { get; set; }

        /// <summary>
        /// Gets or sets the name of the category.
        /// </summary>
        /// <value>The name of the category.</value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the display name of the category.
        /// </summary>
        /// <value>The name to be displayed to users of the category.</value>
        public string DisplayName { get; set; }

        /// <summary>
        /// Gets or sets the owner identifier for the category.
        /// </summary>
        /// <value>The owner identifier for the category.</value>
        public string OwnerId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the category is active.
        /// </summary>
        /// <value>The active flag.</value>
        public bool Active { get; set; }

        /// <summary>
        /// Gets or sets the id of the default permission level associated with this category.
        /// </summary>
        /// <value>The id of the default permission level associated with this category, or null if none.</value>
        public int? DefaultPermissionLevelId { get; set; }

        /// <summary>
        /// Add instance data to the serializer.
        /// </summary>
        /// <param name="info">Object to which the instance data is added.</param>
        /// <param name="context">The parameter is not used.</param>
        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("version", Version);
            info.AddValue("id", this.Id);
            info.AddValue("name", this.Name);
            info.AddValue("ownerId", this.OwnerId);
            info.AddValue("active", this.Active);
            info.AddValue("displayName", this.DisplayName);
            info.AddValue("hasDefaultPermissionLevelId", this.DefaultPermissionLevelId.HasValue);

            if (this.DefaultPermissionLevelId.HasValue)
            {
                info.AddValue("defaultPermissionLevelId", this.DefaultPermissionLevelId.Value);   
            }
        }
    }
}
