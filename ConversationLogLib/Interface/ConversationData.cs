﻿namespace ConversationLogLib.Interface
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using System.Security.Permissions;

    /// <summary>
    /// A conversation is a tree of comments rooted by a single initiating post.
    /// </summary>
    [Serializable]
    public class ConversationData : ISerializable
    {
        /// <summary>
        /// Version for the implementation, used to ensure proper de-serialization.
        /// </summary>
        private const int Version = 201707;

        /// <summary>
        /// Initializes a new instance of the <see cref="ConversationData"/> class.
        /// </summary>
        public ConversationData()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConversationData"/> class.
        /// </summary>
        /// <param name="info">Object from which the serialized fields are pulled.</param>
        /// <param name="context">The parameter is not used.</param>
        private ConversationData(SerializationInfo info, StreamingContext context)
        {
            int version = info.GetInt32("version");

            this.CategoryId = info.GetInt64("categoryId");
            this.CategoryName = info.GetString("categoryName");
            this.OwnerId = info.GetString("ownerId");
            this.FirstPost = (CommentNode)info.GetValue("firstPost", typeof(CommentNode));
            this.Comments = (List<CommentData>)info.GetValue("comments", typeof(List<CommentData>));

            if (version >= 201707)
            {
                bool hasPermissionLevelId = info.GetBoolean("hasPermissionLevelId");
                if (hasPermissionLevelId)
                {
                    this.PermissionLevelId = info.GetInt32("permissionLevelId");
                }
            }
        }

        /// <summary>
        /// Gets or sets the identifier for the category to which this conversation is attached.
        /// </summary>
        /// <value>The identifier for the category to which this conversation is attached.</value>
        public long CategoryId { get; set; }

        /// <summary>
        /// Gets or sets the name of the category to which this conversation is attached.
        /// </summary>
        /// <value>The name of the category to which this conversation is attached.</value>
        public string CategoryName { get; set; }

        /// <summary>
        /// Gets or sets the identifier for the owner of the category to which this conversation is attached.
        /// </summary>
        /// <value>The identifier for the owner of the category to which this conversation is attached.</value>
        public string OwnerId { get; set; }

        /// <summary>
        /// Gets or sets the root of the conversation tree.
        /// </summary>
        /// <value>The root of the conversation tree.</value>
        public CommentNode FirstPost { get; set; }

        /// <summary>
        /// Gets or sets the comments that make of the conversation.  They
        /// will be ordered so that all comments in a category are grouped 
        /// together and following the current category order.
        /// </summary>
        /// <value>List of CommentData instances.</value>
        public List<CommentData> Comments { get; set; }

        /// <summary>
        /// Gets or sets the id of the permission level associated with the conversation. <para></para>
        /// Null will mean not yet migrated, which will be interpreted as the Default.
        /// </summary>
        /// <value>The id of the permission level associated with the conversation, or null if not yet migrated.</value>
        public long? PermissionLevelId { get; set; }

        /// <summary>
        /// Add instance data to the serializer.
        /// </summary>
        /// <param name="info">Object to which the instance data is added.</param>
        /// <param name="context">The parameter is not used.</param>
        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("version", Version);
            info.AddValue("categoryId", this.CategoryId);
            info.AddValue("categoryName", this.CategoryName);
            info.AddValue("ownerId", this.OwnerId);
            info.AddValue("firstPost", this.FirstPost);
            info.AddValue("comments", this.Comments);
            info.AddValue("hasPermissionLevelId", this.PermissionLevelId.HasValue);

            if (this.PermissionLevelId.HasValue)
            {
                info.AddValue("permissionLevelId", this.PermissionLevelId.Value);
            }
        }
    }
}
