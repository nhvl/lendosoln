﻿namespace ConversationLogLib.Interface
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using System.Security.Permissions;

    /// <summary>
    /// Comments are organized into a tree structure, with the nodes represented by this class.
    /// </summary>
    [Serializable]
    public class CommentNode : ISerializable
    {
        /// <summary>
        /// Version for the implementation, used to ensure proper de-serialization.
        /// </summary>
        private const int Version = 201702;

        /// <summary>
        /// Initializes a new instance of the <see cref="CommentNode"/> class.
        /// </summary>
        public CommentNode()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CommentNode"/> class.
        /// </summary>
        /// <param name="info">Object from which the serialized fields are pulled.</param>
        /// <param name="context">The parameter is not used.</param>
        private CommentNode(SerializationInfo info, StreamingContext context)
        {
            int version = info.GetInt32("version");

            this.CommentId = info.GetInt64("commentId");
            this.ChildNodes = (List<CommentNode>)info.GetValue("childNodes", typeof(List<CommentNode>));
        }

        /// <summary>
        /// Gets or sets the identifier of the commment associated with this node.
        /// </summary>
        /// <value>The identifier of the commment associated with this node.</value>
        public long CommentId { get; set; }

        /// <summary>
        /// Gets or sets the child nodes, or empty if there aren't any.
        /// </summary>
        /// <value>The child nodes.</value>
        public List<CommentNode> ChildNodes { get; set; }

        /// <summary>
        /// Add instance data to the serializer.
        /// </summary>
        /// <param name="info">Object to which the instance data is added.</param>
        /// <param name="context">The parameter is not used.</param>
        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("version", Version);
            info.AddValue("commentId", this.CommentId);
            info.AddValue("childNodes", this.ChildNodes);
        }
    }
}
