﻿namespace ConversationLogLib.Interface
{
    using System;
    using System.Runtime.Serialization;
    using System.Security.Permissions;

    /// <summary>
    /// Data associated with a user comment.
    /// </summary>
    [Serializable]
    public sealed class CommentData : ISerializable
    {
        /// <summary>
        /// Version for the implementation, used to ensure proper de-serialization.
        /// </summary>
        private const int Version = 201707;

        /// <summary>
        /// Initializes a new instance of the <see cref="CommentData"/> class.
        /// </summary>
        public CommentData()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CommentData"/> class.
        /// </summary>
        /// <param name="info">Object from which the serialized fields are pulled.</param>
        /// <param name="context">The parameter is not used.</param>
        private CommentData(SerializationInfo info, StreamingContext context)
        {
            int version = info.GetInt32("version");

            this.Id = info.GetInt64("id");
            this.CreatedUTC = (DateTime)info.GetValue("created", typeof(DateTime));
            this.CommenterId = info.GetString("commenterId");
            this.Fullname = info.GetString("fullname");
            this.Content = (CommentContent)info.GetValue("content", typeof(CommentContent));
            if (version >= 201707)
            {
                var isHidden = info.GetBoolean("isHidden");
                if (isHidden)
                {
                    this.HiddenByPersonId = info.GetInt64("hiddenByPersonId");
                    this.HiderFullname = info.GetString("hiderFullName");
                }
                else
                {
                    this.HiddenByPersonId = null;
                }
            }
            else
            {
                this.HiddenByPersonId = null;
            }
        }

        /// <summary>
        /// Gets or sets a unique identifier for a comment.
        /// </summary>
        /// <value>A unique identifier for a comment.</value>
        public long Id { get; set; }

        /// <summary>
        /// Gets or sets the timestamp when the comment was created, in UTC.
        /// </summary>
        /// <value>The timestamp when the comment was created, in UTC.</value>
        public DateTime CreatedUTC { get; set; }

        /// <summary>
        /// Gets or sets an identifer for the user that made the comment, the identifier is relevant to the calling system.
        /// </summary>
        /// <value>An identifer for the user that made the comment.</value>
        public string CommenterId { get; set; }

        /// <summary>
        /// Gets or sets the fullname for the user that made the comment.
        /// </summary>
        /// <value>The fullname for the user that made the comment.</value>
        public string Fullname { get; set; }

        /// <summary>
        /// Gets or sets the fullname for the user that hid the comment, or null if no such person.
        /// </summary>
        /// <value>The fullname for the user that hid the comment, or null if no such person.</value>
        public string HiderFullname { get; set; }

        /// <summary>
        /// Gets or sets the text content of the comment.
        /// </summary>
        /// <value>The text content of the comment.</value>
        public CommentContent Content { get; set; }

        /// <summary>
        /// Gets or sets the id of the person who hid the comment.  Null if never hidden before.
        /// </summary>
        /// <value>The id of the person who hid the comment.  Null if never hidden before.</value>
        public long? HiddenByPersonId { get; set; }

        /// <summary>
        /// Add instance data to the serializer.
        /// </summary>
        /// <param name="info">Object to which the instance data is added.</param>
        /// <param name="context">The parameter is not used.</param>
        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("version", Version);
            info.AddValue("id", this.Id);
            info.AddValue("created", this.CreatedUTC);
            info.AddValue("commenterId", this.CommenterId);
            info.AddValue("fullname", this.Fullname);
            info.AddValue("content", this.Content);
            info.AddValue("isHidden", this.HiddenByPersonId.HasValue);
            if (this.HiddenByPersonId.HasValue)
            {
                info.AddValue("hiddenByPersonId", this.HiddenByPersonId.Value);
                info.AddValue("hiderFullName", this.HiderFullname);
            }
        }
    }
}
