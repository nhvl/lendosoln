﻿namespace ConversationLogLib.Interface
{
    using System;
    using System.Data.SqlClient;
    using System.IO;
    using System.Security.Cryptography;

    /// <summary>
    /// Utility for encryption/decryption of comments.
    /// </summary>
    public static class EncryptionManager
    {
        /// <summary>
        /// The cryptographic key.
        /// </summary>
        private static byte[] key = { 97, 127, 148, 225, 160, 74, 249, 170, 97, 225, 200, 103, 138, 75, 185, 167, 96, 77, 172, 43, 30, 57, 89, 251, 148, 107, 107, 167, 240, 28, 5, 14 };

        /// <summary>
        /// The cryptographic initialization vector.
        /// </summary>
        private static byte[] iv = { 106, 80, 205, 40, 66, 87, 164, 111, 21, 6, 160, 110, 26, 178, 184, 84 };

        /// <summary>
        /// Encrypt comment text.
        /// </summary>
        /// <param name="text">The comment text.</param>
        /// <returns>The encrypted text.</returns>
        public static byte[] Encrypt(string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                return null;
            }

            byte[] inputBuffer = System.Text.Encoding.UTF8.GetBytes(text);
            using (SymmetricAlgorithm cryptoService = GetEncryptionAlgorithm())
            {
                MemoryStream memoryStream = null;
                CryptoStream cryptoStream = null;

                try
                {
                    ICryptoTransform encryptor = cryptoService.CreateEncryptor();

                    memoryStream = new MemoryStream();
                    cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);

                    cryptoStream.Write(inputBuffer, 0, inputBuffer.Length);
                    cryptoStream.FlushFinalBlock();
                    cryptoStream.Flush();

                    return memoryStream.ToArray();
                }
                finally
                {
                    if (cryptoStream != null)
                    {
                        cryptoStream.Close();
                    }

                    if (memoryStream != null)
                    {
                        memoryStream.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Decrypt the results of an earlier encryption of a comment.
        /// </summary>
        /// <param name="encrypted">The encrypted comment.</param>
        /// <returns>The decrypted comment.</returns>
        public static string Decrypt(byte[] encrypted)
        {
            if (encrypted == null || encrypted.Length == 0)
            {
                return null;
            }

            using (SymmetricAlgorithm cryptoService = GetEncryptionAlgorithm())
            {
                cryptoService.Key = key;
                cryptoService.IV = iv;

                MemoryStream memoryStream = null;
                CryptoStream cryptoStream = null;

                try
                {
                    ICryptoTransform decryptor = cryptoService.CreateDecryptor();

                    memoryStream = new MemoryStream(encrypted, 0, encrypted.Length);
                    cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);

                    StreamReader streamReader = new StreamReader(cryptoStream, System.Text.Encoding.UTF8);
                    return streamReader.ReadToEnd();
                }
                finally
                {
                    if (cryptoStream != null)
                    {
                        cryptoStream.Close();
                    }

                    if (memoryStream != null)
                    {
                        memoryStream.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Decrypt a database connection string if it has userid and password.
        /// </summary>
        /// <param name="dsn">The connection string.</param>
        /// <returns>The same connection string after decrypting the userid and password.</returns>
        public static string DecryptDbConnection(string dsn)
        {
            var builder = new SqlConnectionStringBuilder(dsn);
            if (builder.IntegratedSecurity)
            {
                return dsn; 
            }

            byte[] userId = Convert.FromBase64String(builder.UserID);
            byte[] pwd = Convert.FromBase64String(builder.Password);

            builder.UserID = Decrypt(userId);
            builder.Password = Decrypt(pwd);

            return builder.ConnectionString;
        }

        /// <summary>
        /// Retrieve the encryption algorithm.
        /// </summary>
        /// <returns>The encryption algorithm.</returns>
        private static SymmetricAlgorithm GetEncryptionAlgorithm()
        {
            SymmetricAlgorithm algorithm = new RijndaelManaged();
            algorithm.Key = key;
            algorithm.IV = iv;

            return algorithm;
        }
    }
}
