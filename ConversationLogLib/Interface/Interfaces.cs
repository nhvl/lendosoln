﻿namespace ConversationLogLib.Interface
{
    using System.Collections.Generic;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Interface used to interact with the Conversation Log Service.
    /// </summary>
    public interface IConversationLog
    {
        /// <summary>
        /// Add new category or modify an existing category.
        /// </summary>
        /// <param name="securityToken">Security token for the calling principal.</param>
        /// <param name="ownerId">An identifier for the owner of the category.</param>
        /// <param name="categoryName">The name of the category.</param>
        /// <param name="isActive">True if the category is to be active, false otherwise.</param>
        /// <param name="displayName">The display name for the category.</param>
        /// <param name="defaultPermissionLevelId">The default permission level id for the category or null if blank.</param>
        /// <returns>The resultant category data, or null if not successful.</returns>
        CategoryData InsertOrUpdateCategory(SecurityToken securityToken, string ownerId, string categoryName, bool isActive, string displayName, int? defaultPermissionLevelId);

        /// <summary>
        /// Retrieve the categories in the current order.
        /// </summary>
        /// <param name="securityToken">Security token for the calling principal.</param>
        /// <param name="ownerId">An identifier for the owner of the categories.</param>
        /// <returns>The categories in the current order, or null if not successful.</returns>
        List<CategoryData> GetAllCategories(SecurityToken securityToken, string ownerId);

        /// <summary>
        /// Set the order for conversations returned, which will be grouped by categories.
        /// </summary>
        /// <param name="securityToken">Security token for the calling principal.</param>
        /// <param name="ownerId">An identifier indicating the owner of all the categories in the list.</param>
        /// <param name="categoryIds">The complete ordered list of categories.</param>
        /// <returns>True if successful, false otherwise.</returns>
        bool SetCategoryOrder(SecurityToken securityToken, string ownerId, List<long> categoryIds);

        /// <summary>
        /// Start a new conversation by posting a comment.
        /// </summary>
        /// <param name="securityToken">Security token for the calling principal.</param>
        /// <param name="resource">The resource associated with the comment.</param>
        /// <param name="categoryId">An identifier for the category returned by a previous call to GetActiveCategories.</param>
        /// <param name="comment">The comment content.</param>
        /// <param name="permissionLevelId">The id of the permission level associated with the conversation, or null if not yet migrated.</param>
        /// <returns>The conversation data for the record created within the Conversation Log system (but with the comment's text set to null), or null if not successful.</returns>
        ConversationData BeginConveration(SecurityToken securityToken, ResourceData resource, long categoryId, CommentContent comment, long? permissionLevelId);

        /// <summary>
        /// Reply to an existing comment.
        /// </summary>
        /// <param name="securityToken">Security token for the calling principal.</param>
        /// <param name="commentId">An identifier for the comment to which this reply is made.</param>
        /// <param name="comment">The comment content.</param>
        /// <returns>The comment data for the record created within the Conversation Log system (but with the text set to null), or null if not successful.</returns>
        CommentData MakeReply(SecurityToken securityToken, long commentId, CommentContent comment);

        /// <summary>
        /// Hides an existing comment.
        /// </summary>
        /// <param name="securityToken">Security token for the calling principal.</param>
        /// <param name="commentId">An identifier of the comment to be hidden.</param>
        /// <returns>True iff it succeeded.</returns>
        bool HideComment(SecurityToken securityToken, long commentId);

        /// <summary>
        /// Unhides an existing comment.
        /// </summary>
        /// <param name="securityToken">Security token for the calling principal.</param>
        /// <param name="commentId">An identifier of the comment to be unhidden.</param>
        /// <returns>True iff it succeeded.</returns>
        bool ShowComment(SecurityToken securityToken, long commentId);

        /// <summary>
        /// Retrieve all the comments that are associated with a resource.
        /// The comments are organized into conversation trees, with all 
        /// conversations under a category listed adjacent to one another 
        /// with the categories in the current order.
        /// </summary>
        /// <param name="securityToken">Security token for the calling principal.</param>
        /// <param name="resource">The resource.</param>
        /// <returns>The list of conversations, or null if not successful.</returns>
        List<ConversationData> GetAllConversations(SecurityToken securityToken, ResourceData resource);

        /// <summary>
        /// Gets the permission level id for the specified comment, or -1 if an error occurred.
        /// </summary>
        /// <param name="securityToken">Security token for the calling principal.</param>
        /// <param name="commentId">The id of the comment we want the permission level of.</param>
        /// <returns>The permission level id of the conversation that the comment belongs to, or null if it hasn't been set to the default by migration. <para></para> -1 indicates error occurred.</returns>
        long? GetPermissionLevelIdByCommentReference(SecurityToken securityToken, long commentId);
    }

    /// <summary>
    /// Interface used for logging errors and info.
    /// </summary>
    public interface ILogger
    {
        /// <summary>
        /// Logs an error with the specified message and exception.
        /// </summary>
        /// <param name="message">The message associated with the error.</param>
        /// <param name="e">The exception to also be logged.</param>
        void LogError(string message, System.Exception e);

        /// <summary>
        /// Logs the specified message as an info.
        /// </summary>
        /// <param name="message">The message to be logged.</param>
        void LogInfo(string message);
    }
}
