﻿namespace ConversationLogLib.Interface
{
    /// <summary>
    /// Provide the name of the service so both client and server
    /// applications can wire together.
    /// </summary>
    public static class ConversationLogServiceName
    {
        /// <summary>
        /// The name to use when registering the server class and
        /// retrieving a proxy to that class.
        /// </summary>
        public const string Value = "ConversationLogManager";
    }
}
