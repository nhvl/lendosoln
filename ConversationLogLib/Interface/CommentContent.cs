﻿namespace ConversationLogLib.Interface
{
    using System;
    using System.Runtime.Serialization;
    using System.Security.Permissions;

    /// <summary>
    /// Content of a comment which is the comment text + any attached data.
    /// </summary>
    [Serializable]
    public sealed class CommentContent : ISerializable
    {
        /// <summary>
        /// Version for the implementation, used to ensure proper de-serialization.
        /// </summary>
        private const int Version = 201702;

        /// <summary>
        /// Initializes a new instance of the <see cref="CommentContent"/> class.
        /// </summary>
        public CommentContent()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CommentContent"/> class.
        /// </summary>
        /// <param name="info">Object from which the serialized fields are pulled.</param>
        /// <param name="context">The parameter is not used.</param>
        private CommentContent(SerializationInfo info, StreamingContext context)
        {
            int version = info.GetInt32("version");

            this.EncryptedText = Convert.FromBase64String(info.GetString("text"));
        }

        /// <summary>
        /// Gets or sets the encrypted comment text.
        /// </summary>
        /// <value>The encrypted comment text.</value>
        public byte[] EncryptedText { get; set; }

        /// <summary>
        /// Add instance data to the serializer.
        /// </summary>
        /// <param name="info">Object to which the instance data is added.</param>
        /// <param name="context">The parameter is not used.</param>
        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("version", Version);
            info.AddValue("text", Convert.ToBase64String(this.EncryptedText));
        }
    }
}
