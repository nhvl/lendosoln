﻿namespace ConversationLogLib
{
    using System;
    using System.Collections.Generic;
    using ConversationLogLib.Server.DAL.DIL;
    using ConversationLogLib.Server.DAL.DSL;

    /// <summary>
    /// Interface for unit tests so we don't have to
    /// expose everthing as public.  Other than the 
    /// tests of the stored procedures themselves, the 
    /// unit tests calling these methods should register
    /// a mock for the stored procedure driver, else 
    /// the tests are going to raise a null reference 
    /// exception on the SqlConnection.
    /// </summary>
    public static class UnitTestHelper
    {
        /// <summary>
        /// Unit tests for Business Logic Layer.
        /// </summary>
        public static class BLL
        {
            /// <summary>
            /// Test the AuthorizationManager code.
            /// </summary>
            public static class AuthorizationManager
            {
                // nothing to test right now, the code currenly just blindly grants all permissions
            }

            /// <summary>
            /// Test the CategoryManager code.
            /// </summary>
            public static class CategoryManager
            {
                // nothing to test right now, the code is currently merely a pass-through.
            }

            /// <summary>
            /// Test the CommentManager code.
            /// </summary>
            public static class CommentManager
            {
                // other than using the AuthorizationManager and EncryptionManager, the code is merely a pass-through.
            }
        }

        /// <summary>
        /// Unit tests for the Client Interface Layer.
        /// </summary>
        public static class CIL
        {
            /// <summary>
            /// Test the EncryptionManager code.
            /// </summary>
            public static class EncryptionManager
            {
                /// <summary>
                /// Test that the round-trip encryption process is successful.
                /// </summary>
                /// <param name="text">The text to be encrypted.</param>
                /// <returns>True if test is successful, false otherwise.</returns>
                public static bool EncryptThenDecrypt(string text)
                {
                    var encrypted = ConversationLogLib.Interface.EncryptionManager.Encrypt(text);
                    var decrypted = ConversationLogLib.Interface.EncryptionManager.Decrypt(encrypted);
                    return text == decrypted;
                }
            }
        }

        /// <summary>
        /// Unit tests for the Data Access Layer.
        /// </summary>
        public static class DAL
        {
            /// <summary>
            /// Initializes static members of the <see cref="CIL" /> class.
            /// </summary>
            /// <summary>
            /// Unit tests for the Business Data Layer.
            /// </summary>
            public static class BDL
            {
                // nothing to test right now, the code is currently merely a pass-through.
            }

            /// <summary>
            /// Unit tests fro the Data Interface Layer.
            /// </summary>
            public static class DIL
            {
                /// <summary>
                /// Test the CategoryBridge code.
                /// </summary>
                public static class CategoryBridgeTester
                {
                    /// <summary>
                    /// Test the InsertOrUpdateCategory method.
                    /// </summary>
                    /// <param name="ownerId">Owner identifier.</param>
                    /// <param name="categoryName">Category name.</param>
                    /// <param name="isActive">Active flag.</param>
                    /// <param name="displayName">Display name.</param>
                    /// <param name="defaultPermissionLevelId">Default permission level id or null if blank.</param>
                    /// <returns>True if test is successful, false otherwise.</returns>
                    public static bool InsertOrUpdateCategory(string ownerId, string categoryName, bool isActive, string displayName, int? defaultPermissionLevelId)
                    {
                        var category = CategoryBridge.InsertOrUpdateCategory(ownerId, categoryName, isActive, displayName, defaultPermissionLevelId);
                        if (category.Id <= 0)
                        {
                            return false;
                        }

                        if (category.Name != categoryName)
                        {
                            return false;
                        }

                        if (category.Active != isActive)
                        {
                            return false;
                        }

                        if (category.DisplayName != displayName)
                        {
                            return false;
                        }

                        if (category.DefaultPermissionLevelId != defaultPermissionLevelId)
                        {
                            return false;
                        }

                        return true;
                    }

                    /// <summary>
                    /// Test the GetAllCategories method.
                    /// </summary>
                    /// <param name="ownerId">Owner identifier.</param>
                    /// <param name="expectedCount">The expected number of categories.</param>
                    /// <returns>True if test is successful, false otherwise.</returns>
                    public static bool GetAllCategories(string ownerId, int expectedCount)
                    {
                        var list = CategoryBridge.GetAllCategories(ownerId);
                        if (list.Count != expectedCount)
                        {
                            return false;
                        }

                        foreach (var item in list)
                        {
                            if (item.OwnerId != ownerId)
                            {
                                return false;
                            }
                        }

                        return true;
                    }

                    /// <summary>
                    /// Test the SetCategoryOrder method.
                    /// </summary>
                    /// <param name="ownerId">Owner identifier.</param>
                    /// <param name="categoryIds">The ordered list of category identifiers.</param>
                    /// <returns>True if test is successful, false otherwise.</returns>
                    public static bool SetCategoryOrder(string ownerId, long[] categoryIds)
                    {
                        var catList = new List<long>(categoryIds);
                        CategoryBridge.SetCategoryOrder(ownerId, catList);
                        return true;
                    }
                }

                /// <summary>
                /// Test the CommentBridge code.
                /// </summary>
                public static class CommentBridgeTester
                {
                    /// <summary>
                    /// Test the BeginConversation method.
                    /// </summary>
                    /// <param name="userId">User identifier.</param>
                    /// <param name="fullname">Fullname of user.</param>
                    /// <param name="resourceType">Resource type.</param>
                    /// <param name="resourceId">Resource identifier.</param>
                    /// <param name="categoryId">Category identifier.</param>
                    /// <param name="comment">Comment content.</param>
                    /// <param name="permissionLevelId">The id of the permission level associated with the conversation, or null if not yet migrated.</param>
                    /// <returns>True if test is successful, false otherwise.</returns>
                    public static bool BeginConversation(string userId, string fullname, string resourceType, string resourceId, long categoryId, byte[] comment, long? permissionLevelId)
                    {
                        var data = CommentBridge.BeginConversation(userId, fullname, resourceType, resourceId, categoryId, comment, permissionLevelId);
                        if (data.CategoryId != categoryId)
                        {
                            return false;
                        }

                        if (data.Comments.Count != 1)
                        {
                            return false;
                        }

                        var outComment = data.Comments[0];
                        if (outComment.Id <= 0)
                        {
                            return false;
                        }

                        if (outComment.Fullname != fullname)
                        {
                            return false;
                        }
                        
                        if (data.PermissionLevelId != permissionLevelId)
                        {
                            return false;
                        }

                        return true;
                    }

                    /// <summary>
                    /// Test the MakeReply method.
                    /// </summary>
                    /// <param name="userId">User identifier.</param>
                    /// <param name="fullname">Fullname of user.</param>
                    /// <param name="commentId">Identifier of comment to which this is the reply.</param>
                    /// <param name="comment">Comment content.</param>
                    /// <returns>True if test is successful, false otherwise.</returns>
                    public static bool MakeReply(string userId, string fullname, long commentId, byte[] comment)
                    {
                        var outComment = CommentBridge.MakeReply(userId, fullname, commentId, comment);
                        if (outComment.Id <= 0)
                        {
                            return false;
                        }

                        if (outComment.Fullname != fullname)
                        {
                            return false;
                        }

                        return true;
                    }

                    /// <summary>
                    /// Test the GetAllComments method.
                    /// </summary>
                    /// <param name="resourceType">Resource type.</param>
                    /// <param name="resourceId">Resource identifier.</param>
                    /// <param name="expectedConversationCount">Expected number of conversations.</param>
                    /// <param name="expectedCommentCount">Expected number of comments.</param>
                    /// <returns>True if test is successful, false otherwise.</returns>
                    public static bool GetAllComments(
                        string resourceType,
                        string resourceId,
                        int expectedConversationCount,
                        int expectedCommentCount)
                    {
                        var data = CommentBridge.GetAllConversations(resourceType, resourceId);
                        if (data.Count != expectedConversationCount)
                        {
                            return false;
                        }

                        int countComment = 0;
                        foreach (var conversation in data)
                        {
                            countComment += conversation.Comments.Count;
                        }

                        if (countComment != expectedCommentCount)
                        {
                            return false;
                        }

                        return true;
                    }
                }
            }

            /// <summary>
            /// Unit tests for the Data Storage Layer.
            /// </summary>
            public static class DSL
            {
                /// <summary>
                /// Test the ManageCategories code.
                /// </summary>
                public static class ManageCategoriesTester
                {
                    /// <summary>
                    /// Test the InsertOrUpdateCatetory method.
                    /// </summary>
                    /// <param name="ownerId">Owner identifier.</param>
                    /// <param name="catname">Category name.</param>
                    /// <param name="active">Active flag.</param>
                    /// <param name="displayName">Display name.</param>
                    /// <param name="defaultPermissionLevelId">Default permission level id or null if blank.</param>
                    /// <returns>True if test is successful, false otherwise.</returns>
                    public static bool InsertOrUpdateCatetory(string ownerId, string catname, bool active, string displayName, int? defaultPermissionLevelId)
                    {
                        var category = ManageCategories.InsertOrUpdateCategory(null, ownerId, catname, active, displayName, defaultPermissionLevelId);
                        if (category.ID <= 0)
                        {
                            return false;
                        }

                        if (category.OwnerId != ownerId)
                        {
                            return false;
                        }

                        if (category.CategoryName != catname)
                        {
                            return false;
                        }

                        if (category.DisplayName != displayName)
                        {
                            return false;
                        }

                        if (category.Active != active)
                        {
                            return false;
                        }

                        if (category.DefaultPermissionLevelId != defaultPermissionLevelId)
                        {
                            return false;
                        }

                        return true;
                    }

                    /// <summary>
                    /// Test the DeleteCategoryOrders method.
                    /// </summary>
                    /// <param name="ownerId">Owner identifier.</param>
                    /// <returns>True if test is successful, false otherwise.</returns>
                    public static bool DeleteCategoryOrders(string ownerId)
                    {
                        ManageCategories.DeleteCategoryOrders(null, null, ownerId);
                        return true;
                    }

                    /// <summary>
                    /// Test the InsertCategoryOrder method.
                    /// </summary>
                    /// <param name="catId">The associated category id.</param>
                    /// <param name="ordinal">The ordinal value assigned to the category.</param>
                    /// <returns>True if test is successful, false otherwise.</returns>
                    public static bool InsertCategoryOrder(long catId, int ordinal)
                    {
                        ManageCategories.InsertCategoryOrder(null, null, catId, ordinal);
                        return true;
                    }

                    /// <summary>
                    /// Test the GetAllCategories method.
                    /// </summary>
                    /// <param name="ownerId">Owner identifier.</param>
                    /// <param name="expectedRows">The number of rows that the SP should return.</param>
                    /// <returns>True if test is successful, false otherwise.</returns>
                    public static bool GetAllCategories(string ownerId, int expectedRows)
                    {
                        var list = ManageCategories.GetAllCategories(null, ownerId);
                        if (list.Count != expectedRows)
                        {
                            return false;
                        }

                        foreach (var item in list)
                        {
                            if (item.OwnerId != ownerId)
                            {
                                return false;
                            }
                        }

                        return true;
                    }
                }

                /// <summary>
                /// Test the ManageCommenters code.
                /// </summary>
                public static class ManageCommentersTester
                {
                    /// <summary>
                    /// Test the InsertOrUpdateCommenter method.
                    /// </summary>
                    /// <param name="identifier">Account identifier.</param>
                    /// <param name="fullname">Fullname of account holder.</param>
                    /// <returns>True if test is successful, false otherwise.</returns>
                    public static bool InsertOrUpdateCommenter(string identifier, string fullname)
                    {
                        var commenter = ManageCommenters.InsertOrUpdateCommenter(null, null, identifier, fullname);
                        if (commenter.ID <= 0)
                        {
                            return false;
                        }

                        if (commenter.Identifier != identifier)
                        {
                            return false;
                        }

                        if (commenter.Fullname != fullname)
                        {
                            return false;
                        }

                        return true;
                    }
                }

                /// <summary>
                /// Test the ManageConversations code.
                /// </summary>
                public static class ManageConversationsTester
                {
                    /// <summary>
                    /// Test the BeginConversation method.
                    /// </summary>
                    /// <param name="catId">Category identifier.</param>
                    /// <param name="commenterId">Commenter identfier.</param>
                    /// <param name="resourceType">Resource type.</param>
                    /// <param name="resourceId">Resource identifier.</param>
                    /// <param name="content">Comment content.</param>
                    /// <param name="permissionLevelId">The id of the permission level associated with the conversation, or null if not yet migrated.</param>
                    /// <returns>True if test is successful, false otherwise.</returns>
                    public static bool BeginConversation(long catId, long commenterId, string resourceType, string resourceId, byte[] content, long? permissionLevelId)
                    {
                        var tuple = ManageConversations.BeginConversation(null, null, catId, commenterId, resourceType, resourceId, content, permissionLevelId);
                        var comment = tuple.Item1;
                        if (comment.CommenterId != commenterId)
                        {
                            return false;
                        }

                        if (comment.Content.Length != content.Length)
                        {
                            return false;
                        }

                        var category = tuple.Item2;
                        if (category.ID != catId)
                        {
                            return false;
                        }

                        return true;
                    }

                    /// <summary>
                    /// Test the ReplyToComment method.
                    /// </summary>
                    /// <param name="parentId">Identifier for the comment to which this comment is a reply.</param>
                    /// <param name="commenterId">Commenter identifier.</param>
                    /// <param name="content">Comment content.</param>
                    /// <returns>True if test is successful, false otherwise.</returns>
                    public static bool ReplyToComment(long parentId, long commenterId, byte[] content)
                    {
                        var comment = ManageConversations.ReplyToComment(null, null, parentId, commenterId, content);
                        if (comment.CommenterId != commenterId)
                        {
                            return false;
                        }

                        if (comment.Content.Length != content.Length)
                        {
                            return false;
                        }

                        return true;
                    }

                    /// <summary>
                    /// Test the GetAllComments method.
                    /// </summary>
                    /// <param name="resourceType">Resource type.</param>
                    /// <param name="resourceId">Resource identifier.</param>
                    /// <param name="expectedConversationCount">Expected number of conversation rows.</param>
                    /// <param name="expectedCommentCount">Expected number of comment rows.</param>
                    /// <param name="expectedCategoryCount">Expected number of category rows.</param>
                    /// <param name="expectedCommenterCount">Expected number of commenter rows.</param>
                    /// <returns>True if test is successful, false otherwise.</returns>
                    public static bool GetAllComments(
                        string resourceType,
                        string resourceId,
                        int expectedConversationCount,
                        int expectedCommentCount,
                        int expectedCategoryCount,
                        int expectedCommenterCount)
                    {
                        var tuple = ManageConversations.GetAllConversations(null, resourceType, resourceId);
                        var list = tuple.Item1;
                        if (list.Count != expectedConversationCount)
                        {
                            return false;
                        }

                        foreach (var item in list)
                        {
                            if (item.ResourceType != resourceType)
                            {
                                return false;
                            }

                            if (item.ResourceIdentifier != resourceId)
                            {
                                return false;
                            }
                        }

                        if (tuple.Item2.Count != expectedCommentCount)
                        {
                            return false;
                        }

                        if (tuple.Item3.Count != expectedCategoryCount)
                        {
                            return false;
                        }

                        if (tuple.Item4.Count != expectedCommenterCount)
                        {
                            return false;
                        }

                        return true;
                    }
                }

                /// <summary>
                /// Test the actual stored procedures.
                /// </summary>
                public static class StoredProcedureTester
                {
                    /// <summary>
                    /// Flag to ensure initialization of the connection string is only done once.
                    /// </summary>
                    private static bool initialized;

                    /// <summary>
                    /// Initializes static members of the <see cref="StoredProcedureTester" /> class.
                    /// </summary>
                    internal static void Initialize()
                    {
                        const string Dsn = "Data Source=11.12.13.23;Initial Catalog=LoDevConvo;User ID=LoDevConvo_User;Password=r!chGazelle28";

                        if (!initialized)
                        {
                            DatabaseConnection.SetConnection(Dsn);
                            initialized = true;
                        }
                    }

                    /// <summary>
                    /// Test the ManageCategories stored procedure calls.
                    /// </summary>
                    public static class ManageCategories
                    {
                        /// <summary>
                        /// Test the InsertOrUpdateCategory stored procedure.
                        /// </summary>
                        /// <returns>True if test is successful, false otherwise.</returns>
                        public static bool InsertOrUpdateCategory()
                        {
                            StoredProcedureTester.Initialize();

                            using (var conn = DatabaseConnection.GetConnection())
                            {
                                string ownerId = Guid.NewGuid().ToString();
                                string categoryName = "Triathlon";
                                bool active = true;
                                string displayName = "Biathlon";
                                int? defaultPermissionLevelId = null;

                                var category = ConversationLogLib.Server.DAL.DSL.ManageCategories.InsertOrUpdateCategory(conn, ownerId, categoryName, active, displayName, defaultPermissionLevelId);

                                if (category.ID <= 0)
                                {
                                    return false;
                                }

                                if (category.OwnerId != ownerId)
                                {
                                    return false;
                                }

                                if (category.CategoryName != categoryName)
                                {
                                    return false;
                                }

                                if (category.Active != active)
                                {
                                    return false;
                                }

                                if (category.DisplayName != displayName)
                                {
                                    return false;
                                }

                                if (category.DefaultPermissionLevelId != null)
                                {
                                    return false;
                                }

                                return true;
                            }
                        }

                        /// <summary>
                        /// Test the DeleteCategoryOrders stored procedure.
                        /// </summary>
                        /// <returns>True if test is successful, false otherwise.</returns>
                        public static bool DeleteCategoryOrders()
                        {
                            StoredProcedureTester.Initialize();

                            using (var conn = DatabaseConnection.GetConnection())
                            {
                                string ownerId = "1d482652-88b0-4497-b7a6-d44a9f6689c6";
                                ConversationLogLib.Server.DAL.DSL.ManageCategories.DeleteCategoryOrders(conn, null, ownerId);
                            }

                            return true;
                        }

                        /// <summary>
                        /// Test the InsertCategoryOrder stored procedure.
                        /// </summary>
                        /// <returns>True if test is successful, false otherwise.</returns>
                        public static bool InsertCategoryOrder()
                        {
                            StoredProcedureTester.Initialize();

                            using (var conn = DatabaseConnection.GetConnection())
                            {
                                long id = 53;
                                ConversationLogLib.Server.DAL.DSL.ManageCategories.InsertCategoryOrder(conn, null, id, 1);
                            }

                            return true;
                        }

                        /// <summary>
                        /// Test the GetAllCategories stored procedure.
                        /// </summary>
                        /// <returns>True if test is successful, false otherwise.</returns>
                        public static bool GetAllCategories()
                        {
                            StoredProcedureTester.Initialize();

                            using (var conn = DatabaseConnection.GetConnection())
                            {
                                string ownerId = "1d482652-88b0-4497-b7a6-d44a9f6689c6";
                                var list = ConversationLogLib.Server.DAL.DSL.ManageCategories.GetAllCategories(conn, ownerId);

                                foreach (var category in list)
                                {
                                    if (category.ID <= 0)
                                    {
                                        return false;
                                    }

                                    if (string.IsNullOrEmpty(category.CategoryName))
                                    {
                                        return false;
                                    }

                                    if (category.OwnerId != ownerId)
                                    {
                                        return false;
                                    }

                                    if (string.IsNullOrEmpty(category.DisplayName))
                                    {
                                        return false;
                                    }
                                }
                            }

                            return true;
                        }
                    }

                    /// <summary>
                    /// Test the ManageCommenters stored procedure calls.
                    /// </summary>
                    public static class ManageCommenters
                    {
                        /// <summary>
                        /// Test the InsertOrUpdateCommenter stored procedure.
                        /// </summary>
                        /// <returns>True if test is successful, false otherwise.</returns>
                        public static bool InsertOrUpdateCommenter()
                        {
                            StoredProcedureTester.Initialize();

                            using (var conn = DatabaseConnection.GetConnection())
                            {
                                string identifier = Guid.NewGuid().ToString();
                                string fullname = "Recently Departed";

                                var commenter = ConversationLogLib.Server.DAL.DSL.ManageCommenters.InsertOrUpdateCommenter(conn, null, identifier, fullname);
                                if (commenter.ID <= 0)
                                {
                                    return false;
                                }

                                if (commenter.Identifier != identifier)
                                {
                                    return false;
                                }

                                if (commenter.Fullname != fullname)
                                {
                                    return false;
                                }
                            }

                            return true;
                        }
                    }

                    /// <summary>
                    /// Test the ManageConversations stored procedure calls.
                    /// </summary>
                    public static class ManageConversations
                    {
                        /// <summary>
                        /// Test the BeginConversation stored procedure.
                        /// </summary>
                        /// <returns>True if test is successful, false otherwise.</returns>
                        public static bool BeginConversation()
                        {
                            StoredProcedureTester.Initialize();

                            using (var conn = DatabaseConnection.GetConnection())
                            {
                                long categoryId = 52;
                                long commenterId = 29;
                                string resourceType = "LF";
                                string resourceId = Guid.NewGuid().ToString();
                                string message = "Hi Alan";
                                byte[] content = System.Text.Encoding.UTF8.GetBytes(message);
                                long? permissionLevelId = null;

                                var tuple = ConversationLogLib.Server.DAL.DSL.ManageConversations.BeginConversation(conn, null, categoryId, commenterId, resourceType, resourceId, content, permissionLevelId);
                                var comment = tuple.Item1;
                                var category = tuple.Item2;

                                if (comment.ID <= 0)
                                {
                                    return false;
                                }

                                if (comment.ConversationId <= 0)
                                {
                                    return false;
                                }

                                if (comment.Content.Length != content.Length)
                                {
                                    return false;
                                }

                                if (comment.CommenterId != commenterId)
                                {
                                    return false;
                                }

                                if (comment.CreatedUTC <= DateTime.Now)
                                {
                                    // utc is greater because Greenwich is to the east of us
                                    return false;
                                }

                                if (category.ID != categoryId)
                                {
                                    return false;
                                }

                                if (string.IsNullOrEmpty(category.CategoryName))
                                {
                                    return false;
                                }

                                if (string.IsNullOrEmpty(category.DisplayName))
                                {
                                    return false;
                                }

                                if (string.IsNullOrEmpty(category.OwnerId))
                                {
                                    return false;
                                }
                            }

                            return true;
                        }

                        /// <summary>
                        /// Test the ReplyToComment stored procedure.
                        /// </summary>
                        /// <returns>True if test is successful, false otherwise.</returns>
                        public static bool ReplyToComment()
                        {
                            StoredProcedureTester.Initialize();

                            using (var conn = DatabaseConnection.GetConnection())
                            {
                                long parentId = 52;
                                long commenterId = 29;
                                string message = "Speak! Speak! For the love of God why won't you speak?!";
                                byte[] content = System.Text.Encoding.UTF8.GetBytes(message);

                                var comment = ConversationLogLib.Server.DAL.DSL.ManageConversations.ReplyToComment(conn, null, parentId, commenterId, content);
                                if (comment.ID <= 0)
                                {
                                    return false;
                                }

                                if (comment.ConversationId <= 0)
                                {
                                    return false;
                                }

                                if (comment.Content.Length != content.Length)
                                {
                                    return false;
                                }

                                if (comment.CommenterId != commenterId)
                                {
                                    return false;
                                }

                                if (comment.CreatedUTC <= DateTime.Now)
                                {
                                    // utc is greater because Greenwich is to the east of us
                                    return false;
                                }
                            }

                            return true;
                        }

                        /// <summary>
                        /// Test the GetAllComments stored procedure.
                        /// </summary>
                        /// <returns>True if test is successful, false otherwise.</returns>
                        public static bool GetAllComments()
                        {
                            StoredProcedureTester.Initialize();

                            using (var conn = DatabaseConnection.GetConnection())
                            {
                                string resourceType = "LF";
                                string resourceId = "b0d290c8-3547-4ea5-99e1-fa42b532cec3";

                                var tuple = ConversationLogLib.Server.DAL.DSL.ManageConversations.GetAllConversations(conn, resourceType, resourceId);

                                if (tuple.Item1.Count <= 0)
                                {
                                    return false;
                                }

                                if (tuple.Item2.Count <= 0)
                                {
                                    return false;
                                }

                                if (tuple.Item3.Count <= 0)
                                {
                                    return false;
                                }

                                if (tuple.Item4.Count <= 0)
                                {
                                    return false;
                                }
                            }

                            return true;
                        }
                    }
                }
            }
        }
    }
}
