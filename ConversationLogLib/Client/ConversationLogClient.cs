﻿namespace ConversationLogLib.Client
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Runtime.Remoting.Channels;
    using System.Runtime.Remoting.Channels.Tcp;
    using ConversationLogLib.Interface;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// This is the class that is used to communicate with the Conversation Log Service.
    /// </summary>
    public class ConversationLogClient : IConversationLog
    {
        /// <summary>
        /// Proxy to the conversation log service.
        /// </summary>
        private IConversationLog service;

        /// <summary>
        /// Initializes a new instance of the <see cref="ConversationLogClient"/> class.
        /// </summary>
        /// <param name="ip">The location of the conversation log service.</param>
        /// <param name="port">The port for the conversation log service.</param>
        public ConversationLogClient(string ip, int port)
        {
            string address = string.Format("tcp://{0}:{1}/{2}", ip, port.ToString(), ConversationLogServiceName.Value);
            this.service = (IConversationLog)Activator.GetObject(typeof(IConversationLog), address);
        }

        /// <summary>
        /// Start a new conversation by posting a comment.
        /// </summary>
        /// <param name="securityToken">Security token for the calling principal.</param>
        /// <param name="resource">The resource associated with the comment.</param>
        /// <param name="categoryId">An identifier for the category returned by a previous call to GetActiveCategories.</param>
        /// <param name="comment">The comment content.</param>
        /// <param name="permissionLevelId">The id of the permission level associated with the conversation, or null if not yet migrated.</param>
        /// <returns>The conversation data for the record created within the Conversation Log system (but with the comment's text set to null), or null if not successful.</returns>
        public ConversationData BeginConveration(SecurityToken securityToken, ResourceData resource, long categoryId, CommentContent comment, long? permissionLevelId)
        {
            return this.service.BeginConveration(securityToken, resource, categoryId, comment, permissionLevelId);
        }

        /// <summary>
        /// Retrieve the categories in the current order.
        /// </summary>
        /// <param name="securityToken">Security token for the calling principal.</param>
        /// <param name="ownerId">An identifier for the owner of the categories.</param>
        /// <returns>The categories in the current order, or null if not successful.</returns>
        public List<CategoryData> GetAllCategories(SecurityToken securityToken, string ownerId)
        {
            return this.service.GetAllCategories(securityToken, ownerId);
        }

        /// <summary>
        /// Retrieve all the comments that are associated with a resource.
        /// The comments are organized into conversation trees, with all 
        /// conversations under a category listed adjacent to one another 
        /// with the categories in the current order.
        /// </summary>
        /// <param name="securityToken">Security token for the calling principal.</param>
        /// <param name="resource">The resource.</param>
        /// <returns>The list of conversations, or null if not successful.</returns>
        public List<ConversationData> GetAllConversations(SecurityToken securityToken, ResourceData resource)
        {
            return this.service.GetAllConversations(securityToken, resource);
        }

        /// <summary>
        /// Gets the permission level id for the specified comment.
        /// </summary>
        /// <param name="securityToken">Security token for the calling principal.</param>
        /// <param name="commentId">The id of the comment we want the permission level of.</param>
        /// <returns>The permission level id of the conversation that the comment belongs to, or null if it hasn't been set to the default by migration.</returns>
        public long? GetPermissionLevelIdByCommentReference(SecurityToken securityToken, long commentId)
        {
            var id = this.service.GetPermissionLevelIdByCommentReference(securityToken, commentId);
            if (id == -1)
            {
                throw new DeveloperException(ErrorMessage.FromTemplate(ErrorMessageTemplate.NotFound, "comment"));
            }
            else
            {
                return id;
            }
        }

        /// <summary>
        /// Add new category or modify an existing category.
        /// </summary>
        /// <param name="securityToken">Security token for the calling principal.</param>
        /// <param name="ownerId">An identifier for the owner of the category.</param>
        /// <param name="categoryName">The name of the category.</param>
        /// <param name="isActive">True if the category is to be active, false otherwise.</param>
        /// <param name="displayName">The display name for the categoyr.</param>
        /// <param name="defaultPermissionLevelId">Default permission level id or null if blank.</param>
        /// <returns>The resultant category data, or null if not successful.</returns>
        public CategoryData InsertOrUpdateCategory(SecurityToken securityToken, string ownerId, string categoryName, bool isActive, string displayName, int? defaultPermissionLevelId)
        {
            return this.service.InsertOrUpdateCategory(securityToken, ownerId, categoryName, isActive, displayName, defaultPermissionLevelId);
        }

        /// <summary>
        /// Reply to an existing comment.
        /// </summary>
        /// <param name="securityToken">Security token for the calling principal.</param>
        /// <param name="commentId">An identifier for the comment to which this reply is made.</param>
        /// <param name="comment">The comment content.</param>
        /// <returns>The comment data for the record created within the Conversation Log system (but with the text set to null), or null if not successful.</returns>
        public CommentData MakeReply(SecurityToken securityToken, long commentId, CommentContent comment)
        {
            return this.service.MakeReply(securityToken, commentId, comment);
        }

        /// <summary>
        /// Hides an existing comment.
        /// </summary>
        /// <param name="securityToken">Security token for the calling principal.</param>
        /// <param name="commentId">An identifier of the comment to be hidden.</param>
        /// <returns>True iff it succeeded.</returns>
        public bool HideComment(SecurityToken securityToken, long commentId)
        {
            return this.service.HideComment(securityToken, commentId);
        }

        /// <summary>
        /// Unhides an existing comment.
        /// </summary>
        /// <param name="securityToken">Security token for the calling principal.</param>
        /// <param name="commentId">An identifier of the comment to be unhidden.</param>
        /// <returns>True iff it succeeded.</returns>
        public bool ShowComment(SecurityToken securityToken, long commentId)
        {
            return this.service.ShowComment(securityToken, commentId);
        }

        /// <summary>
        /// Set the order for conversations returned, which will be grouped by categories.
        /// </summary>
        /// <param name="securityToken">Security token for the calling principal.</param>
        /// <param name="ownerId">An identifier indicating the owner of all the categories in the list.</param>
        /// <param name="categoryIds">The complete ordered list of categories.</param>
        /// <returns>True if successful, null otherwise.</returns>
        public bool SetCategoryOrder(SecurityToken securityToken, string ownerId, List<long> categoryIds)
        {
            return this.service.SetCategoryOrder(securityToken, ownerId, categoryIds);
        }
    }
}
