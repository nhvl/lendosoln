﻿namespace ConversationLogLib.Server.DAL.DIL
{
    using System.Collections.Generic;
    using System.Data.Common;
    using ConversationLogLib.Interface;
    using ConversationLogLib.Server.DAL.DSL;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Map the BDL category management calls into DIL calls.
    /// </summary>
    internal static class CategoryBridge
    {
        /// <summary>
        /// Add new category or modify an existing category.
        /// </summary>
        /// <param name="ownerId">An identifier for the owner of the category.</param>
        /// <param name="categoryName">The name of the category.</param>
        /// <param name="isActive">True if the category is to be active, false otherwise.</param>
        /// <param name="displayName">The display name for the category.</param>
        /// <param name="defaultPermissionLevelId">The default permission level id for the category or null if blank.</param>
        /// <returns>The resultant category data.</returns>
        public static CategoryData InsertOrUpdateCategory(string ownerId, string categoryName, bool isActive, string displayName, int? defaultPermissionLevelId)
        {
            Category cat;
            using (var conn = DatabaseConnection.GetConnection())
            {
                cat = ManageCategories.InsertOrUpdateCategory(conn, ownerId, categoryName, isActive, displayName, defaultPermissionLevelId);
            }

            return ConvertUp(cat);
        }

        /// <summary>
        /// Retrieve the categories in the current order.
        /// </summary>
        /// <param name="ownerId">An identifier for the owner of the categories.</param>
        /// <returns>The categories in the current order.</returns>
        public static List<CategoryData> GetAllCategories(string ownerId)
        {
            List<Category> listCat;
            using (var conn = DatabaseConnection.GetConnection())
            {
                listCat = ManageCategories.GetAllCategories(conn, ownerId);
            }

            var listOut = new List<CategoryData>();
            foreach (var cat in listCat)
            {
                var data = ConvertUp(cat);
                listOut.Add(data);
            }

            return listOut;
        }

        /// <summary>
        /// Set the order for conversations returned, which will be grouped by categories.
        /// </summary>
        /// <param name="ownerId">An identifier indicating the owner of all the categories in the list.</param>
        /// <param name="categoryIds">The complete ordered list of categories.</param>
        public static void SetCategoryOrder(string ownerId, List<long> categoryIds)
        {
            ValidateInputCategoryOrders(ownerId, categoryIds);

            using (var conn = DatabaseConnection.GetConnection())
            {
                DbTransaction tran = null;
                try
                {
                    if (conn != null)
                    {
                        if (conn.State != System.Data.ConnectionState.Open)
                        {
                            conn.Open();
                        }

                        tran = conn.BeginTransaction();
                    }

                    int ordinal = 0;
                    ManageCategories.DeleteCategoryOrders(conn, tran, ownerId);
                    foreach (long id in categoryIds)
                    {
                        ManageCategories.InsertCategoryOrder(conn, tran, id, ++ordinal);
                    }

                    if (tran != null)
                    {
                        tran.Commit();
                    }
                }
                catch
                {
                    if (tran != null)
                    {
                        tran.Rollback();
                    }

                    throw;
                }
            }
        }

        /// <summary>
        /// Verifies that we can set the order from the input categories, or throws.
        /// </summary>
        /// <param name="ownerId">An identifier indicating the owner of all the categories in the list.</param>
        /// <param name="categoryIds">The complete ordered list of categories as the user intends.</param>
        private static void ValidateInputCategoryOrders(string ownerId, List<long> categoryIds)
        {
            var candidateCategoryIds = new HashSet<long>(categoryIds);
            if (candidateCategoryIds.Count != categoryIds.Count)
            {
                throw new LqbGrammar.Exceptions.DeveloperException(ErrorMessage.FromTemplate(ErrorMessageTemplate.ListContainsDuplicates, nameof(ValidateInputCategoryOrders), "category", ownerId));
            }

            var existingCategoryIds = new HashSet<long>();
            var existingCategories = GetAllCategories(ownerId);
            foreach (var category in existingCategories)
            {
                existingCategoryIds.Add(category.Id);
                if (!candidateCategoryIds.Contains(category.Id))
                {
                    throw new LqbGrammar.Exceptions.DeveloperException(ErrorMessage.FromTemplate(ErrorMessageTemplate.IsMissingObjectInstance, nameof(ValidateInputCategoryOrders), "category", category.Id.ToString(), ownerId));
                }
            }

            // It is necessary to check for the existence of the category id's since otherwise we could add category orders to another owner (the owner is not part of the FK).
            foreach (var candidateCategoryId in candidateCategoryIds)
            {
                if (!existingCategoryIds.Contains(candidateCategoryId))
                {
                    throw new LqbGrammar.Exceptions.DeveloperException(ErrorMessage.FromTemplate(ErrorMessageTemplate.HasExtraObject, nameof(ValidateInputCategoryOrders), "category", candidateCategoryId.ToString(), ownerId));
                }
            }
        }

        /// <summary>
        /// Convert an instance of the Category data transport class to an instance of the CategoryData class.
        /// </summary>
        /// <param name="cat">An instance of the Category data transport class.</param>
        /// <returns>An instance of the CategoryData class.</returns>
        private static CategoryData ConvertUp(Category cat)
        {
            var data = new CategoryData();
            data.Id = cat.ID;
            data.Name = cat.CategoryName;
            data.OwnerId = cat.OwnerId;
            data.Active = cat.Active;
            data.DisplayName = cat.DisplayName;
            data.DefaultPermissionLevelId = cat.DefaultPermissionLevelId;
            return data;
        }
    }
}
