﻿namespace ConversationLogLib.Server.DAL.DIL
{
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Xml;
    using ConversationLogLib.Interface;
    using ConversationLogLib.Server.DAL.DSL;

    /// <summary>
    /// Map the BDL comment management calls into DIL calls.
    /// </summary>
    internal static class CommentBridge
    {
        /// <summary>
        /// Start a new conversation by posting a comment.
        /// </summary>
        /// <param name="userId">The identifier for the user account making this comment.</param>
        /// <param name="fullname">The fullname of the user making this comment.</param>
        /// <param name="resourceType">The type of resource to which the conversation is attached.</param>
        /// <param name="resourceId">The identifier for the resource to which the conversation is attached.</param>
        /// <param name="categoryId">An identifier for the category returned by a previous call to GetActiveCategories.</param>
        /// <param name="comment">The comment content.</param>
        /// <param name="permissionLevelId">The id of the permission level associated with the conversation, or null if not yet migrated.</param>
        /// <returns>The conversation data for the record created within the Conversation Log system (but with the comment's text set to null).</returns>
        public static ConversationData BeginConversation(string userId, string fullname, string resourceType, string resourceId, long categoryId, byte[] comment, long? permissionLevelId)
        {
            using (var conn = DatabaseConnection.GetConnection())
            {
                DbTransaction tran = null;
                try
                {
                    if (conn != null)
                    {
                        if (conn.State != System.Data.ConnectionState.Open)
                        {
                            conn.Open();
                        }

                        tran = conn.BeginTransaction();
                    }

                    var commenter = ManageCommenters.InsertOrUpdateCommenter(conn, tran, userId, fullname);
                    var tuple = ManageConversations.BeginConversation(conn, tran, categoryId, commenter.ID, resourceType, resourceId, comment, permissionLevelId);

                    var data = new ConversationData();
                    data.CategoryId = tuple.Item2.ID;
                    data.CategoryName = tuple.Item2.CategoryName;
                    data.OwnerId = tuple.Item2.OwnerId;
                    data.PermissionLevelId = permissionLevelId;
                    data.FirstPost = new CommentNode();
                    data.FirstPost.CommentId = tuple.Item1.ID;
                    data.Comments = new List<CommentData>();

                    var commentData = ConvertUp(tuple.Item1, commenter, hider: null);
                    data.Comments.Add(commentData);

                    if (tran != null)
                    {
                        tran.Commit();
                    }

                    return data;
                }
                catch
                {
                    if (tran != null)
                    {
                        tran.Rollback();
                    }

                    throw;
                }
            }
        }
        
        /// <summary>
        /// Reply to an existing comment.
        /// </summary>
        /// <param name="userId">The identifier for the user account of the user issuing the reply.</param>
        /// <param name="fullname">The fullname of the user issuing the reply.</param>
        /// <param name="commentId">An identifier for the comment to which this reply is made.</param>
        /// <param name="comment">The comment content.</param>
        /// <returns>The comment data for the record created within the Conversation Log system (but with the text set to null).</returns>
        public static CommentData MakeReply(string userId, string fullname, long commentId, byte[] comment)
        {
            using (var conn = DatabaseConnection.GetConnection())
            {
                DbTransaction tran = null;
                try
                {
                    if (conn != null)
                    {
                        if (conn.State != System.Data.ConnectionState.Open)
                        {
                            conn.Open();
                        }

                        tran = conn.BeginTransaction();
                    }

                    var commenter = ManageCommenters.InsertOrUpdateCommenter(conn, tran, userId, fullname);
                    var databaseComment = ManageConversations.ReplyToComment(conn, tran, commentId, commenter.ID, comment);

                    var data = ConvertUp(databaseComment, commenter, hider: null);

                    if (tran != null)
                    {
                        tran.Commit();
                    }

                    return data;
                }
                catch
                {
                    if (tran != null)
                    {
                        tran.Rollback();
                    }

                    throw;
                }
            }
        }

        /// <summary>
        /// Hides the coment with the specified id.
        /// </summary>
        /// <param name="userId">The id of the user who is hiding the comment.</param>
        /// <param name="fullname">The full name of the user who is hiding the comment.</param>
        /// <param name="commentId">The id of the comment to be hidden.</param>
        public static void HideComment(string userId, string fullname, long commentId)
        {
            using (var conn = DatabaseConnection.GetConnection())
            {
                DbTransaction tran = null;
                try
                {
                    if (conn != null)
                    {
                        if (conn.State != System.Data.ConnectionState.Open)
                        {
                            conn.Open();
                        }

                        tran = conn.BeginTransaction();
                    }

                    var commenter = ManageCommenters.InsertOrUpdateCommenter(conn, tran, userId, fullname);
                    ManageConversations.HideComment(conn, tran, commentId, commenter.ID);
                    
                    if (tran != null)
                    {
                        tran.Commit();
                    }

                    return;
                }
                catch
                {
                    if (tran != null)
                    {
                        tran.Rollback();
                    }

                    throw;
                }
            }
        }

        /// <summary>
        /// Unhides the comment with the specified id.
        /// </summary>
        /// <param name="commentId">The id of the comment to be unhidden.</param>
        public static void ShowComment(long commentId)
        {
            using (var conn = DatabaseConnection.GetConnection())
            {
                DbTransaction tran = null;
                try
                {
                    if (conn != null)
                    {
                        if (conn.State != System.Data.ConnectionState.Open)
                        {
                            conn.Open();
                        }

                        tran = conn.BeginTransaction();
                    }
                    
                    ManageConversations.ShowComment(conn, tran, commentId);

                    if (tran != null)
                    {
                        tran.Commit();
                    }

                    return;
                }
                catch
                {
                    if (tran != null)
                    {
                        tran.Rollback();
                    }

                    throw;
                }
            }
        }

        /// <summary>
        /// Retrieve all the comments that are associated with a resource.
        /// The comments are organized into conversation trees, with all 
        /// conversations under a category listed adjacent to one another 
        /// with the categories in the current order.
        /// </summary>
        /// <param name="resourceType">The type of resource to which the conversation is attached.</param>
        /// <param name="resourceId">The identifier for the resource to which the conversation is attached.</param>
        /// <returns>The list of conversations.</returns>
        public static List<ConversationData> GetAllConversations(string resourceType, string resourceId)
        {
            List<Conversation> listConversations;
            Dictionary<long, Comment> dictComments;
            List<Category> listCategories;
            Dictionary<long, Commenter> dictCommenters;

            using (var conn = DatabaseConnection.GetConnection())
            {
                var tuple = ManageConversations.GetAllConversations(conn, resourceType, resourceId);
                listConversations = tuple.Item1;
                dictComments = tuple.Item2;
                listCategories = tuple.Item3;
                dictCommenters = tuple.Item4;
            }

            var listOut = new List<ConversationData>();
            foreach (var category in listCategories)
            {
                ExtractForCategory(listOut, category, listConversations, dictComments, dictCommenters);
            }

            return listOut;
        }

        /// <summary>
        /// Gets the permission level id for the speicified comment.
        /// </summary>
        /// <param name="commentId">The id of the comment we want the permission level of.</param>
        /// <returns>The permission level id of the conversation that the comment belongs to, or null if it hasn't been set to the default by migration.</returns>
        public static long? GetPermissionLevelIdByCommentReference(long commentId)
        {
            using (var conn = DatabaseConnection.GetConnection())
            {
                return ManageConversations.GetPermissionLevelIdByCommentReference(conn, commentId);
            }
        }

        /// <summary>
        /// Extract data from database classes and place into data transport classes.
        /// </summary>
        /// <param name="listOut">List of conversation data transport classes.</param>
        /// <param name="category">Database category class.</param>
        /// <param name="listConversations">List of conversation database classes.</param>
        /// <param name="dictComments">Dictionary of comment database classes.</param>
        /// <param name="dictCommenters">Dictionary of commenter database classes.</param>
        private static void ExtractForCategory(List<ConversationData> listOut, Category category, List<Conversation> listConversations, Dictionary<long, Comment> dictComments, Dictionary<long, Commenter> dictCommenters)
        {
            foreach (var conv in listConversations)
            {
                if (conv.CategoryId != category.ID)
                {
                    continue;
                }

                var data = new ConversationData();
                data.CategoryId = category.ID;
                data.CategoryName = category.CategoryName;
                data.OwnerId = category.OwnerId;
                data.Comments = new List<CommentData>();
                PopulateComments(conv.ID, data.Comments, dictComments, dictCommenters);

                XmlElement root = conv.CommentTree.DocumentElement;
                data.FirstPost = ProcessTreeNode(root);
                data.PermissionLevelId = conv.PermissionLevelId;

                listOut.Add(data);
            }
        }

        /// <summary>
        /// Populate the comments for data transport with the data from database classes.
        /// </summary>
        /// <param name="conversationId">The converation identifier.</param>
        /// <param name="list">List of data transport comment classes.</param>
        /// <param name="dictComments">Dictionary of database comment classes.</param>
        /// <param name="dictCommenters">Dictionary of database commenter classes.</param>
        private static void PopulateComments(long conversationId, List<CommentData> list, Dictionary<long, Comment> dictComments, Dictionary<long, Commenter> dictCommenters)
        {
            foreach (long id in dictComments.Keys)
            {
                var comment = dictComments[id];
                if (comment.ConversationId == conversationId)
                {
                    var commenter = dictCommenters[comment.CommenterId];
                    Commenter? hider = comment.HiddenByPersonId.HasValue ? dictCommenters[comment.HiddenByPersonId.Value] : (Commenter?)null;

                    var data = ConvertUp(comment, commenter, hider);
                    list.Add(data);
                }
            }
        }

        /// <summary>
        /// Recursive method that generates a tree of CommentNode instances from an XML comment tree.
        /// </summary>
        /// <param name="elem">The current element of the comment tree.</param>
        /// <returns>A CommentNode instance that represents the XML element.</returns>
        private static CommentNode ProcessTreeNode(XmlElement elem)
        {
            long id = long.Parse(elem.GetAttribute("id"));

            var node = new CommentNode();
            node.CommentId = id;

            var nodeList = elem.SelectNodes("C");
            if (nodeList.Count > 0)
            {
                node.ChildNodes = new List<CommentNode>();
                foreach (XmlElement child in nodeList)
                {
                    node.ChildNodes.Add(ProcessTreeNode(child));
                }
            }

            return node;
        }

        /// <summary>
        /// Transfer data from database comment and commenter classes to the CommentData data transport class.
        /// </summary>
        /// <param name="comment">A comment database class.</param>
        /// <param name="commenter">A commenter database class.</param>
        /// <param name="hider">A nullable commenter database class.</param>
        /// <returns>A data transport class CommentData.</returns>
        private static CommentData ConvertUp(Comment comment, Commenter commenter, Commenter? hider)
        {
            var data = new CommentData();
            data.CommenterId = commenter.Identifier;
            data.Content = new CommentContent();
            data.Content.EncryptedText = comment.Content;
            data.CreatedUTC = comment.CreatedUTC;
            data.Fullname = commenter.Fullname;
            if (hider.HasValue)
            {
                data.HiderFullname = hider.Value.Fullname;
            }

            data.Id = comment.ID;
            data.HiddenByPersonId = comment.HiddenByPersonId;
            return data;
        }
    }
}
