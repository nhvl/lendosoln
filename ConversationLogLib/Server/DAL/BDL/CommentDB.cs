﻿namespace ConversationLogLib.Server.DAL.BDL
{
    using System.Collections.Generic;
    using ConversationLogLib.Interface;
    using ConversationLogLib.Server.DAL.DIL;

    /// <summary>
    /// Manage comments in the database.
    /// </summary>
    internal static class CommentDB
    {
        /// <summary>
        /// Start a new conversation by posting a comment.
        /// </summary>
        /// <param name="userId">The identifier for the user account making this comment.</param>
        /// <param name="fullname">The fullname of the user making this comment.</param>
        /// <param name="resource">The resource associated with the comment.</param>
        /// <param name="categoryId">An identifier for the category returned by a previous call to GetActiveCategories.</param>
        /// <param name="comment">The comment content.</param>
        /// <param name="permissionLevelId">The id of the permission level associated with the conversation, or null if not yet migrated.</param>
        /// <returns>The conversation data for the record created within the Conversation Log system (but with the comment's text set to null).</returns>
        public static ConversationData BeginConveration(string userId, string fullname, ResourceData resource, long categoryId, CommentContent comment, long? permissionLevelId)
        {
            return CommentBridge.BeginConversation(userId, fullname, resource.ResourceType, resource.Identifier, categoryId, comment.EncryptedText, permissionLevelId);
        }

        /// <summary>
        /// Reply to an existing comment.
        /// </summary>
        /// <param name="userId">The identifier for the user account of the user issuing the reply.</param>
        /// <param name="fullname">The fullname of the user issuing the reply.</param>
        /// <param name="commentId">An identifier for the comment to which this reply is made.</param>
        /// <param name="comment">The comment content.</param>
        /// <returns>The comment data for the record created within the Conversation Log system (but with the text set to null).</returns>
        public static CommentData MakeReply(string userId, string fullname, long commentId, CommentContent comment)
        {
            return CommentBridge.MakeReply(userId, fullname, commentId, comment.EncryptedText);
        }

        /// <summary>
        /// Hides the comment specified.
        /// </summary>
        /// <param name="userId">The id of the user who is hiding the comment.</param>
        /// <param name="fullname">The full name of the user who is hiding the comment.</param>
        /// <param name="commentId">The id of the commment to be hidden.</param>
        public static void HideComment(string userId, string fullname, long commentId)
        {
            CommentBridge.HideComment(userId, fullname, commentId);
        }

        /// <summary>
        /// Unhides the specified comment.
        /// </summary>
        /// <param name="commentId">The id of the comment to be unhidden.</param>
        public static void ShowComment(long commentId)
        {
            CommentBridge.ShowComment(commentId);
        }

        /// <summary>
        /// Retrieve all the comments that are associated with a resource.
        /// The comments are organized into conversation trees, with all 
        /// conversations under a category listed adjacent to one another 
        /// with the categories in the current order.
        /// </summary>
        /// <param name="resource">The resource.</param>
        /// <returns>The list of conversations.</returns>
        public static List<ConversationData> GetAllConversations(ResourceData resource)
        {
            return CommentBridge.GetAllConversations(resource.ResourceType, resource.Identifier);
        }

        /// <summary>
        /// Gets the permission level id for the specified comment.
        /// </summary>
        /// <param name="commentId">The id of the comment we want the permission level of.</param>
        /// <returns>The permission level id of the conversation that the comment belongs to, or null if it hasn't been set to the default by migration.</returns>
        public static long? GetPermissionLevelIdByCommentReference(long commentId)
        {
            return CommentBridge.GetPermissionLevelIdByCommentReference(commentId);
        }
    }
}
