﻿namespace ConversationLogLib.Server.DAL.BDL
{
    using System.Collections.Generic;
    using ConversationLogLib.Interface;
    using ConversationLogLib.Server.DAL.DIL;

    /// <summary>
    /// Manage categories in the database.
    /// </summary>
    public static class CategoryDB
    {
        /// <summary>
        /// Add new category or modify an existing category.
        /// </summary>
        /// <param name="ownerId">An identifier for the owner of the category.</param>
        /// <param name="categoryName">The name of the category.</param>
        /// <param name="isActive">True if the category is to be active, false otherwise.</param>
        /// <param name="displayName">The display name for the category.</param>
        /// <param name="defaultPermissionLevelId">The default permission level id for the category or null if blank.</param>
        /// <returns>The resultant category data.</returns>
        public static CategoryData InsertOrUpdateCategory(string ownerId, string categoryName, bool isActive, string displayName, int? defaultPermissionLevelId)
        {
            return CategoryBridge.InsertOrUpdateCategory(ownerId, categoryName, isActive, displayName, defaultPermissionLevelId);
        }

        /// <summary>
        /// Retrieve the categories in the current order.
        /// </summary>
        /// <param name="ownerId">An identifier for the owner of the categories.</param>
        /// <returns>The categories in the current order.</returns>
        public static List<CategoryData> GetAllCategories(string ownerId)
        {
            return CategoryBridge.GetAllCategories(ownerId);
        }

        /// <summary>
        /// Set the order for conversations returned, which will be grouped by categories.
        /// </summary>
        /// <param name="ownerId">An identifier indicating the owner of all the categories in the list.</param>
        /// <param name="categoryIds">The complete ordered list of categories.</param>
        public static void SetCategoryOrder(string ownerId, List<long> categoryIds)
        {
            CategoryBridge.SetCategoryOrder(ownerId, categoryIds);
        }
    }
}
