﻿namespace ConversationLogLib.Server.DAL.DSL
{
    /// <summary>
    /// This represents a row in the CategoryOrder table.
    /// </summary>
    internal struct CategoryOrder
    {
        /// <summary>
        /// Gets or sets the primary key for the row.
        /// </summary>
        /// <value>The primary key for the row.</value>
        public long ID { get; set; }

        /// <summary>
        /// Gets or sets the associated Category's primary key.
        /// </summary>
        /// <value>The associated Category's primary key.</value>
        public long CategoryId { get; set; }

        /// <summary>
        /// Gets or sets the ordinal value that defines the relative placement of the associated category within a total order.
        /// </summary>
        /// <value>The ordinal value that defines the relative placement of the associated category within a total order.</value>
        public int Ordinal { get; set; }
    }
}
