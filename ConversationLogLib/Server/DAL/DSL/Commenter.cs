﻿namespace ConversationLogLib.Server.DAL.DSL
{
    /// <summary>
    /// This represents a row in the Commenter table.
    /// </summary>
    internal struct Commenter
    {
        /// <summary>
        /// Gets or sets the primary key for this row.
        /// </summary>
        /// <value>The primary key for this row.</value>
        public long ID { get; set; }

        /// <summary>
        /// Gets or sets an identifer from the calling system that is associated with the account that generated this comment.
        /// </summary>
        /// <value>An identifer from the calling system that is associated with the account that generated this comment.</value>
        public string Identifier { get; set; }

        /// <summary>
        /// Gets or sets the fullname of the user that owns the account that generated this comment.
        /// </summary>
        /// <value>The fullname of the user that owns the account that generated this comment.</value>
        public string Fullname { get; set; }
    }
}
