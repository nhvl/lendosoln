﻿namespace ConversationLogLib.Server.DAL.DSL
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Xml;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Utility for managing conversations in the database.
    /// </summary>
    internal static class ManageConversations
    {
        /// <summary>
        /// Begin a new conversation.
        /// </summary>
        /// <param name="conn">The database connection.</param>
        /// <param name="tran">A transaction, or null.</param>
        /// <param name="catId">The category with the conversation is associated.</param>
        /// <param name="commenterId">The commenter that initiates the conversation.</param>
        /// <param name="resourceType">The type of resource to which the conversation is attached.</param>
        /// <param name="resourceId">The identifier for the resource to which the conversation is attached.</param>
        /// <param name="content">The content of the comment that kicks off the conversation.</param>
        /// <param name="permissionLevelId">The id of the permission level associated with the conversation, or null if not yet migrated.</param>
        /// <returns>The inserted comment and conversation.</returns>
        public static Tuple<Comment, Category> BeginConversation(DbConnection conn, DbTransaction tran, long catId, long commenterId, string resourceType, string resourceId, byte[] content, long? permissionLevelId)
        {
            StoredProcedureName proc = StoredProcedureName.Create("BeginConversation").Value;

            var parameters = new SqlParameter[6];

            parameters[0] = new SqlParameter("@CategoryId", catId);
            parameters[1] = new SqlParameter("@CommenterId", commenterId);

            parameters[2] = new SqlParameter("@ResourceType", SqlDbType.VarChar);
            parameters[2].Value = resourceType;

            parameters[3] = new SqlParameter("@ResourceId", SqlDbType.VarChar);
            parameters[3].Value = resourceId;

            parameters[4] = new SqlParameter("@CommentContent", SqlDbType.VarBinary);
            parameters[4].Value = content;

            parameters[5] = new SqlParameter("@PermissionLevelId", SqlDbType.Int);
            parameters[5].Value = (object)permissionLevelId ?? DBNull.Value;

            var factory = GenericLocator<IStoredProcedureDriverFactory>.Factory;
            var driver = factory.Create(TimeoutInSeconds.Default);

            using (var ds = driver.ExecuteDataSet(conn, tran, proc, parameters))
            {
                var row = ds.Tables[0].Rows[0];
                var comm = ReadComment(row);

                row = ds.Tables[1].Rows[0];
                var cat = ManageCategories.ReadCategory(row);

                return new Tuple<Comment, Category>(comm, cat);
            }
        }

        /// <summary>
        /// Add a comment to a pre-existing conversation as a reply to an earlier comment.
        /// </summary>
        /// <param name="conn">The database connection.</param>
        /// <param name="tran">A transaction, or null.</param>
        /// <param name="parentId">The comment to which this is a reply.</param>
        /// <param name="commenterId">The commenter that initiates the conversation.</param>
        /// <param name="content">The content of the comment that kicks off the conversation.</param>
        /// <returns>The inserted comment.</returns>
        public static Comment ReplyToComment(DbConnection conn, DbTransaction tran, long parentId, long commenterId, byte[] content)
        {
            StoredProcedureName proc = StoredProcedureName.Create("ReplyToComment").Value;

            var parameters = new SqlParameter[3];

            parameters[0] = new SqlParameter("@ParentId", parentId);
            parameters[1] = new SqlParameter("@CommenterId", commenterId);

            parameters[2] = new SqlParameter("@CommentContent", SqlDbType.VarBinary);
            parameters[2].Value = content;

            var factory = GenericLocator<IStoredProcedureDriverFactory>.Factory;
            var driver = factory.Create(TimeoutInSeconds.Default);

            using (var ds = driver.ExecuteDataSet(conn, tran, proc, parameters))
            {
                var row = ds.Tables[0].Rows[0];
                var comm = ReadComment(row);

                return comm;
            }
        }

        /// <summary>
        /// Hides the comment and sets the id of the person who hid it.
        /// </summary>
        /// <param name="conn">The database connection.</param>
        /// <param name="tran">A transaction, or null.</param>
        /// <param name="commentId">The id of the comment to be hidden.</param>
        /// <param name="hiderId">The id of the person hiding the comment.</param>
        public static void HideComment(DbConnection conn, DbTransaction tran, long commentId, long hiderId)
        {
            StoredProcedureName proc = StoredProcedureName.Create("HideComment").Value;

            var parameters = new List<SqlParameter>();

            parameters.Add(new SqlParameter("@CommentId", commentId));
            parameters.Add(new SqlParameter("@PersonId", hiderId));

            var factory = GenericLocator<IStoredProcedureDriverFactory>.Factory;
            var driver = factory.Create(TimeoutInSeconds.Default);

            driver.ExecuteNonQuery(conn, tran, proc, parameters);
        }

        /// <summary>
        /// Unhides the comment.
        /// </summary>
        /// <param name="conn">The database connection.</param>
        /// <param name="tran">A transaction, or null.</param>
        /// <param name="commentId">The id of the comment to unhide.</param>
        public static void ShowComment(DbConnection conn, DbTransaction tran, long commentId)
        {
            StoredProcedureName proc = StoredProcedureName.Create("ShowComment").Value;

            var parameters = new List<SqlParameter>();

            parameters.Add(new SqlParameter("@CommentId", commentId));

            var factory = GenericLocator<IStoredProcedureDriverFactory>.Factory;
            var driver = factory.Create(TimeoutInSeconds.Default);

            driver.ExecuteNonQuery(conn, tran, proc, parameters);
        }

        /// <summary>
        /// Retrieve all comments that have been made on a given resource.
        /// </summary>
        /// <param name="conn">The database connection.</param>
        /// <param name="resourceType">The type of resource to which the conversation is attached.</param>
        /// <param name="resourceId">The identifier for the resource to which the conversation is attached.</param>
        /// <returns>Conversations, Comments, Categories and Commenters associated with the given resource.</returns>
        public static Tuple<List<Conversation>, Dictionary<long, Comment>, List<Category>, Dictionary<long, Commenter>> GetAllConversations(DbConnection conn, string resourceType, string resourceId)
        {
            StoredProcedureName proc = StoredProcedureName.Create("GetAllComments").Value;

            var parameters = new SqlParameter[2];

            parameters[0] = new SqlParameter("@ResourceType", SqlDbType.VarChar);
            parameters[0].Value = resourceType;

            parameters[1] = new SqlParameter("@ResourceId", SqlDbType.VarChar);
            parameters[1].Value = resourceId;

            var factory = GenericLocator<IStoredProcedureDriverFactory>.Factory;
            var driver = factory.Create(TimeoutInSeconds.Default);

            using (var ds = driver.ExecuteDataSet(conn, null, proc, parameters))
            {
                var conversations = new List<Conversation>();
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    var conv = ReadConversation(row);
                    conversations.Add(conv);
                }

                var comments = new Dictionary<long, Comment>();
                foreach (DataRow row in ds.Tables[1].Rows)
                {
                    var comm = ReadComment(row);
                    comments[comm.ID] = comm;
                }

                var categories = new List<Category>();
                foreach (DataRow row in ds.Tables[2].Rows)
                {
                    var cat = ManageCategories.ReadCategory(row);
                    categories.Add(cat);
                }

                var commenters = new Dictionary<long, Commenter>();
                foreach (DataRow row in ds.Tables[3].Rows)
                {
                    var comm = ManageCommenters.ReadCommenter(row);
                    commenters[comm.ID] = comm;
                }

                return new Tuple<List<Conversation>, Dictionary<long, Comment>, List<Category>, Dictionary<long, Commenter>>(conversations, comments, categories, commenters);
            }
        }

        /// <summary>
        /// Gets the permission level id for the speicified comment.
        /// </summary>
        /// <param name="conn">The connection to use when executing the query.</param>
        /// <param name="commentId">The id of the comment we want the permission level of.</param>
        /// <returns>The permission level id of the conversation that the comment belongs to, or null if it hasn't been set to the default by migration.</returns>
        public static long? GetPermissionLevelIdByCommentReference(DbConnection conn, long commentId)
        {
            StoredProcedureName proc = StoredProcedureName.Create("GetPermissionLevelIdByCommentId").Value;

            var parameters = new SqlParameter[1];

            parameters[0] = new SqlParameter("@CommentId", SqlDbType.BigInt);
            parameters[0].Value = commentId;

            var factory = GenericLocator<IStoredProcedureDriverFactory>.Factory;
            var driver = factory.Create(TimeoutInSeconds.Default);

            using (var reader = driver.ExecuteReader(conn, null, proc, parameters))
            {
                if (reader.Read())
                {
                    return RecordHelper.AsNullableStruct<int>(reader, "PermissionLevelId");
                }
                else
                {
                    throw new DeveloperException(ErrorMessage.FromTemplate(ErrorMessageTemplate.NotFound, "comment"));
                }
            }
        }

        /// <summary>
        /// Read a row from the Conversation table.
        /// </summary>
        /// <param name="row">A Conversation row.</param>
        /// <returns>The parsed row data.</returns>
        private static Conversation ReadConversation(DataRow row)
        {
            var conv = new Conversation();
            conv.ID = (long)row["ID"];
            conv.CategoryId = (long)row["CategoryId"];
            conv.ResourceType = (string)row["ResourceType"];
            conv.ResourceIdentifier = (string)row["ResourceIdentifier"];
            conv.PermissionLevelId = RecordHelper.AsNullableStruct<int>(row, "PermissionLevelId");

            var sqlXml = (string)row["CommentTree"]; // weird, but this is how the xml is returned!
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(sqlXml);
            conv.CommentTree = doc;

            return conv;
        }

        /// <summary>
        /// Read a row from the Comment table.
        /// </summary>
        /// <param name="row">A Comment row.</param>
        /// <returns>The parsed row data.</returns>
        private static Comment ReadComment(DataRow row)
        {
            var comm = new Comment();
            comm.ID = (long)row["ID"];
            comm.CreatedUTC = DateTime.SpecifyKind(Convert.ToDateTime(row["CreatedUTC"]), DateTimeKind.Utc);
            comm.ConversationId = (long)row["ConversationId"];
            comm.CommenterId = (long)row["CommenterId"];
            comm.Content = (byte[])row["Content"];
            comm.HiddenByPersonId = RecordHelper.AsNullableStruct<long>(row, "HiddenByPersonId");

            return comm;
        }
    }
}
