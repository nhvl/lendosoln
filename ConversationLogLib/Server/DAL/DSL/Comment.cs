﻿namespace ConversationLogLib.Server.DAL.DSL
{
    using System;

    /// <summary>
    /// This represents a row in the Comment table.
    /// </summary>
    internal struct Comment
    {
        /// <summary>
        /// Gets or sets the primary key for this row.
        /// </summary>
        /// <value>The primary key for this row.</value>
        public long ID { get; set; }

        /// <summary>
        /// Gets or sets the creation timestamp for this row.
        /// </summary>
        /// <value>The creation timestamp for this row.</value>
        public DateTime CreatedUTC { get; set; }

        /// <summary>
        /// Gets or sets this comment's containing conversation.
        /// </summary>
        /// <value>This comment's containing conversation.</value>
        public long ConversationId { get; set; }

        /// <summary>
        /// Gets or sets the commenter that made this comment.
        /// </summary>
        /// <value>The commenter that made this comment.</value>
        public long CommenterId { get; set; }

        /// <summary>
        /// Gets or sets the content of this comment.
        /// </summary>
        /// <value>The content of this comment.</value>
        public byte[] Content { get; set; }

        /// <summary>
        /// Gets or sets the id of the person who hid the comment.  Null if never hidden before.
        /// </summary>
        /// <value>The id of the person who hid the comment.  Null if never hidden before.</value>
        public long? HiddenByPersonId { get; set; }
    }
}
