﻿namespace ConversationLogLib.Server.DAL.DSL
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Utility for managing categories in the database.
    /// </summary>
    internal static class ManageCategories
    {
        /// <summary>
        /// Insert or update a category row in the database.
        /// </summary>
        /// <param name="conn">The database connection.</param>
        /// <param name="ownerId">An identifier in a client application that references the owner of this category.</param>
        /// <param name="catname">The name of the category.</param>
        /// <param name="active">True if the category is active, false otherwise.</param>
        /// <param name="displayName">The display name for the category.</param>
        /// <param name="defaultPermissionLevelId">The default permission level id for the category.</param>
        /// <returns>The row data for the category.</returns>
        public static Category InsertOrUpdateCategory(DbConnection conn, string ownerId, string catname, bool active, string displayName, int? defaultPermissionLevelId)
        {
            StoredProcedureName proc = StoredProcedureName.Create("InsertOrUpdateCategory").Value;

            var parameters = new SqlParameter[5];

            parameters[0] = new SqlParameter("@OwnerId", SqlDbType.VarChar);
            parameters[0].Value = ownerId;

            parameters[1] = new SqlParameter("@CategoryName", SqlDbType.VarChar);
            parameters[1].Value = catname;

            parameters[2] = new SqlParameter("@Active", SqlDbType.Bit);
            parameters[2].Value = active;

            parameters[3] = new SqlParameter("@DisplayName", SqlDbType.VarChar);
            parameters[3].Value = displayName;

            parameters[4] = new SqlParameter("@DefaultPermissionLevelId", SqlDbType.Int);
            parameters[4].Value = (object)defaultPermissionLevelId ?? DBNull.Value;

            var factory = GenericLocator<IStoredProcedureDriverFactory>.Factory;
            var driver = factory.Create(TimeoutInSeconds.Default);

            using (var ds = driver.ExecuteDataSet(conn, null, proc, parameters))
            {
                var table = ds.Tables[0];
                var row = table.Rows[0];

                var cat = new Category();
                cat.ID = (long)row["ID"];
                cat.OwnerId = (string)row["OwnerId"];
                cat.CategoryName = (string)row["CategoryName"];
                cat.Active = (bool)row["Active"];
                cat.DisplayName = (string)row["DisplayName"];
                cat.DefaultPermissionLevelId = RecordHelper.AsNullableStruct<int>(row, "DefaultPermissionLevelId");

                return cat;
            }
        }

        /// <summary>
        /// Delete all entries from the CategoryOrder table assocated with the owner identifier.
        /// </summary>
        /// <param name="conn">The database connection.</param>
        /// <param name="tran">A transaction, or null.</param>
        /// <param name="ownerId">The owner identifier.</param>
        public static void DeleteCategoryOrders(DbConnection conn, DbTransaction tran, string ownerId)
        {
            StoredProcedureName proc = StoredProcedureName.Create("DeleteCategoryOrders").Value;

            var parameters = new SqlParameter[1];
            parameters[0] = new SqlParameter("@OwnerId", SqlDbType.VarChar);
            parameters[0].Value = ownerId;

            var factory = GenericLocator<IStoredProcedureDriverFactory>.Factory;
            var driver = factory.Create(TimeoutInSeconds.Default);

            driver.ExecuteNonQuery(conn, tran, proc, parameters);
        }

        /// <summary>
        /// Insert an entry into the CategoryOrder table.
        /// </summary>
        /// <param name="conn">The database connection.</param>
        /// <param name="tran">A transaction, or null.</param>
        /// <param name="catId">The primary key of the associated category.</param>
        /// <param name="ordinal">The ordinal value.</param>
        public static void InsertCategoryOrder(DbConnection conn, DbTransaction tran, long catId, int ordinal)
        {
            StoredProcedureName proc = StoredProcedureName.Create("InsertCategoryOrder").Value;

            var parameters = new SqlParameter[2];

            parameters[0] = new SqlParameter("@CategoryId", catId);
            parameters[1] = new SqlParameter("@Ordinal", ordinal);

            var factory = GenericLocator<IStoredProcedureDriverFactory>.Factory;
            var driver = factory.Create(TimeoutInSeconds.Default);

            driver.ExecuteNonQuery(conn, tran, proc, parameters);
        }

        /// <summary>
        /// Retrieve an ordered list of all the categories associated with the owner identifier.
        /// </summary>
        /// <param name="conn">The database connection.</param>
        /// <param name="ownerId">The owner identifier.</param>
        /// <returns>The ordered list of categories.</returns>
        public static List<Category> GetAllCategories(DbConnection conn, string ownerId)
        {
            StoredProcedureName proc = StoredProcedureName.Create("GetAllCategories").Value;

            var parameters = new SqlParameter[1];
            parameters[0] = new SqlParameter("@OwnerId", SqlDbType.VarChar);
            parameters[0].Value = ownerId;

            var factory = GenericLocator<IStoredProcedureDriverFactory>.Factory;
            var driver = factory.Create(TimeoutInSeconds.Default);

            var list = new List<Category>();
            using (var reader = driver.ExecuteReader(conn, null, proc, parameters))
            {
                while (reader.Read())
                {
                    Category cat = new Category();
                    cat.ID = (long)reader["ID"];
                    cat.OwnerId = (string)reader["OwnerId"];
                    cat.CategoryName = (string)reader["CategoryName"];
                    cat.Active = (bool)reader["Active"];
                    cat.DisplayName = (string)reader["DisplayName"];
                    cat.DefaultPermissionLevelId = RecordHelper.AsNullableStruct<int>(reader, "DefaultPermissionLevelId");
                    list.Add(cat);
                }
            }

            return list;
        }

        /// <summary>
        /// Read a row from the Category table.
        /// </summary>
        /// <param name="row">A Category row.</param>
        /// <returns>The parsed row data.</returns>
        public static Category ReadCategory(DataRow row)
        {
            Category cat = new Category();
            cat.ID = (long)row["ID"];
            cat.OwnerId = (string)row["OwnerId"];
            cat.CategoryName = (string)row["CategoryName"];
            cat.Active = (bool)row["Active"];
            cat.DisplayName = (string)row["DisplayName"];
            cat.DefaultPermissionLevelId = RecordHelper.AsNullableStruct<int>(row, "DefaultPermissionLevelId");

            return cat;
        }
    }
}
