﻿namespace ConversationLogLib.Server.DAL.DSL
{
    /// <summary>
    /// This represents a row in the Category table.
    /// </summary>
    internal struct Category
    {
        /// <summary>
        /// Gets or sets the primary key for the row.
        /// </summary>
        /// <value>The primary key for the row.</value>
        public long ID { get; set; }

        /// <summary>
        /// Gets or sets the owner identifier for the category.
        /// </summary>
        /// <value>The owner identifier for the category.</value>
        public string OwnerId { get; set; }

        /// <summary>
        /// Gets or sets the name of the category.  This cannot change.
        /// </summary>
        /// <value>The name of the category.</value>
        public string CategoryName { get; set; }

        /// <summary>
        /// Gets or sets the name of the category as a user would see it. <para/>
        /// It can change.
        /// </summary>
        /// <value>The name of the category.</value>
        public string DisplayName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the category is active.
        /// </summary>
        /// <value>An enable flag for the category.</value>
        public bool Active { get; set; }

        /// <summary>
        /// Gets or sets the id of the default permission level associated with this category.
        /// </summary>
        /// <value>The id of the default permission level associated with this category, or null if none.</value>
        public int? DefaultPermissionLevelId { get; set; }
    }
}
