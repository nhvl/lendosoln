﻿namespace ConversationLogLib.Server.DAL.DSL
{
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Utility for managing commenters in the database.
    /// </summary>
    internal static class ManageCommenters
    {
        /// <summary>
        /// Insert or update a commenter row in the database.
        /// </summary>
        /// <param name="conn">The database connection.</param>
        /// <param name="tran">A transaction, or null.</param>
        /// <param name="identifier">An identifier in a client application that references the commenter.</param>
        /// <param name="fullname">The fullname of the commenter.</param>
        /// <returns>The row data for the commenter.</returns>
        public static Commenter InsertOrUpdateCommenter(DbConnection conn, DbTransaction tran, string identifier, string fullname)
        {
            StoredProcedureName proc = StoredProcedureName.Create("InsertOrUpdateCommenter").Value;

            var parameters = new SqlParameter[2];

            parameters[0] = new SqlParameter("@Identifier", SqlDbType.VarChar);
            parameters[0].Value = identifier;

            parameters[1] = new SqlParameter("@Fullname", SqlDbType.VarChar);
            parameters[1].Value = fullname;

            var factory = GenericLocator<IStoredProcedureDriverFactory>.Factory;
            var driver = factory.Create(TimeoutInSeconds.Default);

            using (var ds = driver.ExecuteDataSet(conn, tran, proc, parameters))
            {
                return ReadCommenter(ds.Tables[0].Rows[0]);
            }
        }

        /// <summary>
        /// Read a row from the Commenter table.
        /// </summary>
        /// <param name="row">A Commenter row.</param>
        /// <returns>The parsed row data.</returns>
        public static Commenter ReadCommenter(DataRow row)
        {
            var comm = new Commenter();
            comm.ID = (long)row["ID"];
            comm.Identifier = (string)row["Identifier"];
            comm.Fullname = (string)row["Fullname"];
            return comm;
        }
    }
}
