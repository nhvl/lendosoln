﻿namespace ConversationLogLib.Server.DAL.DSL
{
    using System.Xml;

    /// <summary>
    /// This represents a row in the Conversation table.
    /// </summary>
    internal struct Conversation
    {
        /// <summary>
        /// Gets or sets the primary key for this row.
        /// </summary>
        /// <value>The primary key for this row.</value>
        public long ID { get; set; }

        /// <summary>
        /// Gets or sets the category associated with this conversation.
        /// </summary>
        /// <value>The category associated with this conversation.</value>
        public long CategoryId { get; set; }

        /// <summary>
        /// Gets or sets the type of resource to which the conversation is attached.
        /// </summary>
        /// <value>The type of resource to which the conversation is attached.</value>
        public string ResourceType { get; set; }

        /// <summary>
        /// Gets or sets the identifier for the resource to which the conversation is attached.
        /// </summary>
        /// <value>The identifier for the resource to which the conversation is attached.</value>
        public string ResourceIdentifier { get; set; }

        /// <summary>
        /// Gets or sets the conversation's structure, represented as a tree using XML.
        /// </summary>
        /// <value>The conversation's structure, represented as a tree using XML.</value>
        public XmlDocument CommentTree { get; set; }

        /// <summary>
        /// Gets or sets the id of the permission level associated with the conversation. <para></para>
        /// Null will mean not yet migrated, which will be interpreted as the Default.
        /// </summary>
        /// <value>The id of the permission level associated with the conversation, or null if not yet migrated.</value>
        public long? PermissionLevelId { get; set; }
    }
}
