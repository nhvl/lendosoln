﻿namespace ConversationLogLib.Server.DAL.DSL
{
    using System;
    using System.Data;

    /// <summary>
    /// Methods that are convenient for assisting with datarecords.<para></para>
    /// Copied from StoredProcedureHelper.cs.
    /// </summary>
    /// <remarks>
    /// TODO: Move this elsewhere, or combine with StoredProcedureHelper.cs in a common lib.
    /// </remarks>
    public class RecordHelper
    {
        /// <summary>
        /// Note this is copied from IDataRecordExtensions of StoredProcedureHelper.cs.<para></para>
        /// Returns the value from the row, which could be null.
        /// </summary>
        /// <typeparam name="T">The type of struct we want the null or value of.</typeparam>
        /// <param name="reader">The reader that houses the data.</param>
        /// <param name="columnName">The column of interest.</param>
        /// <returns>The value from the row, or null.</returns>
        public static T? AsNullableStruct<T>(IDataRecord reader, string columnName) where T : struct
        {
            if (reader[columnName] == DBNull.Value)
            {
                return null;
            }
            else
            {
                return (T)reader[columnName];
            }
        }

        /// <summary>
        /// Note this is copied from IDataRecordExtensions of StoredProcedureHelper.cs.<para></para>
        /// Returns the value from the row, which could be null.
        /// </summary>
        /// <typeparam name="T">The type of struct we want the null or value of.</typeparam>
        /// <param name="row">The row that houses the data.</param>
        /// <param name="columnName">The column of interest.</param>
        /// <returns>The value from the row, or null.</returns>
        public static T? AsNullableStruct<T>(DataRow row, string columnName) where T : struct
        {
            if (row[columnName] == DBNull.Value)
            {
                return null;
            }
            else
            {
                return (T)row[columnName];
            }
        }
    }
}
