﻿namespace ConversationLogLib.Server.DAL.DSL
{
    using System.Data.Common;
    using System.Data.SqlClient;

    /// <summary>
    /// Storage location for the database connection string.
    /// </summary>
    public static class DatabaseConnection
    {
        /// <summary>
        /// The database connection string.
        /// </summary>
        private static string connectionString;

        /// <summary>
        /// Set the connection string, called by the hosting application.
        /// </summary>
        /// <param name="connectionString">The database connection string.</param>
        public static void SetConnection(string connectionString)
        {
            DatabaseConnection.connectionString = connectionString;
        }

        /// <summary>
        /// Retrieve a database connection, called by the DAL code.
        /// </summary>
        /// <returns>A database connection.</returns>
        internal static DbConnection GetConnection()
        {
            return string.IsNullOrEmpty(connectionString) ? null : new SqlConnection(connectionString);
        }
    }
}
