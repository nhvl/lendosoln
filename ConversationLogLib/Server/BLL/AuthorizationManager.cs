﻿namespace ConversationLogLib.Server.BLL
{
    using ConversationLogLib.Interface;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Authenticate the user can carry out the requested operations.
    /// </summary>
    public static class AuthorizationManager
    {
        /// <summary>
        /// Check the user's ability to modify a category.
        /// </summary>
        /// <param name="securityToken">The user data.</param>
        /// <param name="ownerId">The category owner.</param>
        /// <returns>True if permission is granted, false otherwise.</returns>
        public static bool CanWriteCategory(SecurityToken securityToken, string ownerId)
        {
            return true;
        }

        /// <summary>
        /// Check the user's ability to read category data.
        /// </summary>
        /// <param name="securityToken">The user data.</param>
        /// <param name="ownerId">The category owner.</param>
        /// <returns>True if permission is granted, false otherwise.</returns>
        public static bool CanReadCategory(SecurityToken securityToken, string ownerId)
        {
            return true;
        }

        /// <summary>
        /// Check the user's ability to add a comment to a resource under a category.
        /// </summary>
        /// <param name="securityToken">The user data.</param>
        /// <param name="resource">The resource.</param>
        /// <param name="categoryId">The category.</param>
        /// <returns>True if permission is granted, false otherwise.</returns>
        public static bool CanWriteComment(SecurityToken securityToken, ResourceData resource, long categoryId)
        {
            return true;
        }

        /// <summary>
        /// Check the user's ability to reply to a comment.
        /// </summary>
        /// <param name="securityToken">The user data.</param>
        /// <param name="commentId">The comment.</param>
        /// <returns>True if permission is granted, false otherwise.</returns>
        public static bool CanReplyComment(SecurityToken securityToken, long commentId)
        {
            return true;
        }

        /// <summary>
        /// Check the user's ability to hide a comment, show a hidden comment, or view a comment that's been hidden.
        /// </summary>
        /// <param name="securityToken">The user data.</param>
        /// <param name="commentId">The comment.</param>
        /// <returns>True if permission is granted, false otherwise.</returns>
        public static bool CanHideComment(SecurityToken securityToken, long commentId)
        {
            return true;
        }

        /// <summary>
        /// Check the user's ability to read conversations related to a resource.
        /// </summary>
        /// <param name="securityToken">The user data.</param>
        /// <param name="resource">The resource.</param>
        /// <returns>True if permission is granted, false otherwise.</returns>
        public static bool CanReadComments(SecurityToken securityToken, ResourceData resource)
        {
            return true;
        }
    }
}
