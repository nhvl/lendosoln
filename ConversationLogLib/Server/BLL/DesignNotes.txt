﻿The Business Logic Layer (BLL) is where the business-specific logic is located.
In the context of the Conversation Log System, this consists of authenticating
that the calling user has the proper permissions, and to handle the data 
encryption/decryption.