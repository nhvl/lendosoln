﻿namespace ConversationLogLib.Server.BLL
{
    using System.Collections.Generic;
    using ConversationLogLib.Interface;
    using ConversationLogLib.Server.DAL.BDL;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Authorize the user to carry out the category operations and then
    /// perform those operations.
    /// </summary>
    internal static class CategoryManager
    {
        /// <summary>
        /// Add new category or modify an existing category.
        /// </summary>
        /// <param name="securityToken">Security token for the calling principal.</param>
        /// <param name="ownerId">An identifier for the owner of the category.</param>
        /// <param name="categoryName">The name of the category.</param>
        /// <param name="isActive">True if the category is to be active, false otherwise.</param>
        /// <param name="displayName">The display name for the category.</param>
        /// <param name="defaultPermissionLevelId">The default permission level id for the category or null if blank.</param>
        /// <returns>The resultant category data.</returns>
        public static CategoryData InsertOrUpdateCategory(SecurityToken securityToken, string ownerId, string categoryName, bool isActive, string displayName, int? defaultPermissionLevelId)
        {
            if (!AuthorizationManager.CanWriteCategory(securityToken, ownerId))
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            return CategoryDB.InsertOrUpdateCategory(ownerId, categoryName, isActive, displayName, defaultPermissionLevelId);
        }

        /// <summary>
        /// Retrieve the categories in the current order.
        /// </summary>
        /// <param name="securityToken">Security token for the calling principal.</param>
        /// <param name="ownerId">An identifier for the owner of the categories.</param>
        /// <returns>The categories in the current order.</returns>
        public static List<CategoryData> GetAllCategories(SecurityToken securityToken, string ownerId)
        {
            if (!AuthorizationManager.CanReadCategory(securityToken, ownerId))
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            return CategoryDB.GetAllCategories(ownerId);
        }

        /// <summary>
        /// Set the order for conversations returned, which will be grouped by categories.
        /// </summary>
        /// <param name="securityToken">Security token for the calling principal.</param>
        /// <param name="ownerId">An identifier indicating the owner of all the categories in the list.</param>
        /// <param name="categoryIds">The complete ordered list of categories.</param>
        public static void SetCategoryOrder(SecurityToken securityToken, string ownerId, List<long> categoryIds)
        {
            if (!AuthorizationManager.CanWriteCategory(securityToken, ownerId))
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            CategoryDB.SetCategoryOrder(ownerId, categoryIds);
        }
    }
}
