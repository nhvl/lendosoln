﻿namespace ConversationLogLib.Server.BLL
{
    using System.Collections.Generic;
    using ConversationLogLib.Interface;
    using ConversationLogLib.Server.DAL.BDL;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Authorize the user to carry out the comment/conversation operations and then
    /// perform those operations.
    /// </summary>
    internal static class CommentManager
    {
        /// <summary>
        /// Start a new conversation by posting a comment.
        /// </summary>
        /// <param name="securityToken">Security token for the calling principal.</param>
        /// <param name="resource">The resource associated with the comment.</param>
        /// <param name="categoryId">An identifier for the category returned by a previous call to GetActiveCategories.</param>
        /// <param name="comment">The comment content.</param>
        /// <param name="permissionLevelId">The id of the permission level associated with the conversation, or null if not yet migrated.</param>
        /// <returns>The conversation data for the record created within the Conversation Log system (but with the comment's text set to null).</returns>
        public static ConversationData BeginConveration(SecurityToken securityToken, ResourceData resource, long categoryId, CommentContent comment, long? permissionLevelId)
        {
            if (!AuthorizationManager.CanWriteComment(securityToken, resource, categoryId))
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            return CommentDB.BeginConveration(securityToken.UserId.ToString(), securityToken.Fullname.ToString(), resource, categoryId, comment, permissionLevelId);
        }

        /// <summary>
        /// Reply to an existing comment.
        /// </summary>
        /// <param name="securityToken">Security token for the calling principal.</param>
        /// <param name="commentId">An identifier for the comment to which this reply is made.</param>
        /// <param name="comment">The comment content.</param>
        /// <returns>The comment data for the record created within the Conversation Log system (but with the text set to null).</returns>
        public static CommentData MakeReply(SecurityToken securityToken, long commentId, CommentContent comment)
        {
            if (!AuthorizationManager.CanReplyComment(securityToken, commentId))
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            return CommentDB.MakeReply(securityToken.UserId.ToString(), securityToken.Fullname.ToString(), commentId, comment);
        }

        /// <summary>
        /// Hides an existing comment.
        /// </summary>
        /// <param name="securityToken">Security token for the calling principal.</param>
        /// <param name="commentId">An identifier of the comment to be hidden.</param>
        public static void HideComment(SecurityToken securityToken, long commentId)
        {
            if (!AuthorizationManager.CanHideComment(securityToken, commentId))
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            CommentDB.HideComment(securityToken.UserId.ToString(), securityToken.Fullname.ToString(), commentId);
        }

        /// <summary>
        /// Unhides an existing comment.
        /// </summary>
        /// <param name="securityToken">Security token for the calling principal.</param>
        /// <param name="commentId">An identifier of the comment to be unhidden.</param>
        public static void ShowComment(SecurityToken securityToken, long commentId)
        {
            if (!AuthorizationManager.CanHideComment(securityToken, commentId))
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            CommentDB.ShowComment(commentId);
        }

        /// <summary>
        /// Retrieve all the comments that are associated with a resource.
        /// The comments are organized into conversation trees, with all 
        /// conversations under a category listed adjacent to one another 
        /// with the categories in the current order.
        /// </summary>
        /// <param name="securityToken">Security token for the calling principal.</param>
        /// <param name="resource">The resource.</param>
        /// <returns>The list of conversations.</returns>
        public static List<ConversationData> GetAllConversations(SecurityToken securityToken, ResourceData resource)
        {
            if (!AuthorizationManager.CanReadComments(securityToken, resource))
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            return CommentDB.GetAllConversations(resource);
        }

        /// <summary>
        /// Gets the permission level id for the speicified comment.
        /// </summary>
        /// <param name="securityToken">Security token for the calling principal.</param>
        /// <param name="commentId">The id of the comment we want the permission level of.</param>
        /// <returns>The permission level id of the conversation that the comment belongs to, or null if it hasn't been set to the default by migration.</returns>
        public static long? GetPermissionLevelIdByCommentReference(SecurityToken securityToken, long commentId)
        {
            return CommentDB.GetPermissionLevelIdByCommentReference(commentId);
        }
    }
}
