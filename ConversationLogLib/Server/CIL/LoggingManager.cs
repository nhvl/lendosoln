﻿namespace ConversationLogLib.Server.CIL
{
    using Interface;

    /// <summary>
    /// Encapsulate the mechanism for logging messages.
    /// </summary>
    public static class LoggingManager
    {
        /// <summary>
        /// Implements logging methods.
        /// </summary>
        private static ILogger logger;

        /// <summary>
        /// Initialize the logging implementation.
        /// </summary>
        /// <param name="serviceLogger">Objects that has a logging mechanism.</param>
        public static void SetServiceLogger(ILogger serviceLogger)
        {
            logger = serviceLogger;
        }

        /// <summary>
        /// Log an informational message.
        /// </summary>
        /// <param name="message">The message.</param>
        public static void LogMessage(string message)
        {
            if (logger != null)
            {
                logger.LogInfo(message);
            }
        }

        /// <summary>
        /// Log an error message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="e">The exception.</param>
        public static void LogError(string message, System.Exception e)
        {
            if (logger != null)
            {
                logger.LogError(message, e);
            }
        }
    }
}
