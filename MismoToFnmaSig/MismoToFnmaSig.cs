﻿using System;
using System.Text;
using System.Xml;
using System.Xml.Xsl;

namespace MismoToFnmaSig
{
	/// <summary>
	/// A utility class to convert reports from Mismo to Fannie Mae's SIG format
	/// </summary>
	/// <remarks>
	/// The process has two steps:
	///   1: Convert from Mismo to intermediate format
	///   2: Convert from intermediate format to Fannie Mae's SIG format
	/// </remarks>
	public abstract class MismoToFnmaSig
	{
		private const string MISMO_VER_ID_ATTR = "MISMOVersionID";

		private XslCompiledTransform m_Transformer;
		private XmlDocument m_xmlParsingInstructions;

		protected virtual string MismoVersionID { get { return string.Empty; } }
		
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="a_XsltStylesheetFile">
		/// Path to a stylesheet that is used to convert data to intermediate format
		/// </param>
		/// <param name="a_XmlParsingFile">
		/// Path to an xml file that contains the parsing instructions used to
		/// convert data from intermediate format to SIG format
		/// </param>
		public MismoToFnmaSig(string a_XsltStylesheetFile, string a_XmlParsingFile)
		{
			this.m_Transformer = new XslCompiledTransform();
			this.m_Transformer.Load(a_XsltStylesheetFile);
			this.m_xmlParsingInstructions = new XmlDocument();
			this.m_xmlParsingInstructions.Load(a_XmlParsingFile);
		}

		/// <summary>
		/// Do the conversion
		/// </summary>
		/// <param name="a_xmlMismo">XmlDocument that contains the MISMO report</param>
		/// <param name="a_sAgencyID">FNMA Agency ID</param>
		/// <param name="a_sAgencyName">FNMA Agency Name (CRA Name)</param>
		/// <returns>A string that contains the report in SIG format</returns>
		public string Convert(XmlDocument a_xmlMismo, string a_sAgencyID, string a_sAgencyName)
		{
            if (string.IsNullOrEmpty(this.MismoVersionID))
            {
                throw new ApplicationException("Invalid Mismo Version ID.");
            }
            string[] allowableVersionList = this.MismoVersionID.Split(';');
            bool isMatchMismo = false;
            foreach (var mismoVersion in allowableVersionList) {
                if (a_xmlMismo.DocumentElement.GetAttribute(MISMO_VER_ID_ATTR) == mismoVersion) 
                {
                    isMatchMismo = true;
                    break;
                }
            }
            if (isMatchMismo == false) {
                throw new ApplicationException("Invalid Mismo Version ID.");
            }

			using (System.IO.StringWriter writer = new System.IO.StringWriter())
			{
				XsltUtil util = new XsltUtil();
				XsltArgumentList args = new XsltArgumentList();
				args.AddExtensionObject("urn:XsltUtil", util);
				args.AddParam("AgencyID", string.Empty, a_sAgencyID);
				args.AddParam("AgencyName", string.Empty, a_sAgencyName);

				this.m_Transformer.Transform(a_xmlMismo.CreateNavigator(), args, writer);

				CreditDataToSig converter = new CreditDataToSig(this.m_xmlParsingInstructions);
				return converter.Execute(writer.ToString());
			}
		}
	}
}
