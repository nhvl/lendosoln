﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Xml;
using System.Xml.XPath;
using System.Text;

namespace MismoToFnmaSig
{
	/// <summary>
	/// Extension functions that are used by the transform stylesheet
	/// </summary>
	public class XsltUtil
	{
		/*
		The following variables, functions are copied from MclObjLib.Credit.FnmaSigUtil.
		Function and parameter name may be modified.
		*/
		#region CODE COPIED FROM MclObjLib.Credit.FnmaSigUtil
		
		private static Dictionary<string, string> m_dicRiskFactorCode_FES = new Dictionary<string, string>();
		private static Dictionary<string, string> m_dicRiskFactorCode_EXP = new Dictionary<string, string>();
		private static Dictionary<string, string> m_dicRiskFactorCode_EQF = new Dictionary<string, string>();
		private static Dictionary<string, string> m_dicRiskFactorCode_TUC = new Dictionary<string, string>();

		private string Lookup(Dictionary<string, string> a_Dictionary, string a_sKey)
		{
			a_sKey = Util.SafeString(a_sKey);
			if (null == a_Dictionary || a_sKey.Length == 0)
				return a_sKey;

			// 9/15/2009 - The classic FICO risk factors listed here are only those factors that differ between
			// Fannie Mae and the repositories.  As such, any unmapped factor should be returned unchanged.
			// This nuance was missed in a subsequent refactoring (r18282/case 24383).  OPM 40203.
			if (a_Dictionary.ContainsKey(a_sKey))
				return a_Dictionary[a_sKey];
			else
				return a_sKey;	
		}
		private Dictionary<string, string> GetRiskFactorCodes_EQF()
		{
			if (m_dicRiskFactorCode_EQF.Count == 0)
			{
				lock (m_dicRiskFactorCode_EQF)
				{
					if (m_dicRiskFactorCode_EQF.Count == 0)
					{
						m_dicRiskFactorCode_EQF.Add("5", "25");
						m_dicRiskFactorCode_EQF.Add("9", "27");
						m_dicRiskFactorCode_EQF.Add("11", "35");
						m_dicRiskFactorCode_EQF.Add("12", "36");
						m_dicRiskFactorCode_EQF.Add("8", "41");
						m_dicRiskFactorCode_EQF.Add("98", "43");
						m_dicRiskFactorCode_EQF.Add("99", "45");
						m_dicRiskFactorCode_EQF.Add("25", "46");
					}
				}
			}
			return m_dicRiskFactorCode_EQF;
		}
		private Dictionary<string, string> GetRiskFactorCodes_EXP()
		{
			if (m_dicRiskFactorCode_EXP.Count == 0)
			{
				lock (m_dicRiskFactorCode_EXP)
				{
					if (m_dicRiskFactorCode_EXP.Count == 0)
					{
						m_dicRiskFactorCode_EXP.Add("5", "25");
						m_dicRiskFactorCode_EXP.Add("9", "27");
						m_dicRiskFactorCode_EXP.Add("11", "35");
						m_dicRiskFactorCode_EXP.Add("12", "36");
						m_dicRiskFactorCode_EXP.Add("8", "41");
						m_dicRiskFactorCode_EXP.Add("46", "42");
						m_dicRiskFactorCode_EXP.Add("98", "43");
						m_dicRiskFactorCode_EXP.Add("99", "45");
						m_dicRiskFactorCode_EXP.Add("25", "46");
						m_dicRiskFactorCode_EXP.Add("36", "47");
						m_dicRiskFactorCode_EXP.Add("37", "49");
					}
				}
			}
			return m_dicRiskFactorCode_EXP;
		}
		private Dictionary<string, string> GetRiskFactorCodes_TUC()
		{
			if (m_dicRiskFactorCode_TUC.Count == 0)
			{
				lock (m_dicRiskFactorCode_TUC)
				{
					if (m_dicRiskFactorCode_TUC.Count == 0)
					{
						m_dicRiskFactorCode_TUC.Add("27", "19");
						m_dicRiskFactorCode_TUC.Add("5", "25");
						m_dicRiskFactorCode_TUC.Add("9", "27");
						m_dicRiskFactorCode_TUC.Add("4", "32");
						m_dicRiskFactorCode_TUC.Add("3", "33");
						m_dicRiskFactorCode_TUC.Add("31", "34");
						m_dicRiskFactorCode_TUC.Add("11", "35");
						m_dicRiskFactorCode_TUC.Add("12", "36");
						m_dicRiskFactorCode_TUC.Add("19", "37");
						m_dicRiskFactorCode_TUC.Add("8", "41");
						m_dicRiskFactorCode_TUC.Add("97", "43");
						m_dicRiskFactorCode_TUC.Add("98", "44");
						m_dicRiskFactorCode_TUC.Add("99", "45");
						m_dicRiskFactorCode_TUC.Add("26", "48");
					}
				}
			}
			return m_dicRiskFactorCode_TUC;
		}
		private Dictionary<string, string> GetRiskFactorCodes_FES()
		{
			if (m_dicRiskFactorCode_FES.Count == 0)
			{
				lock (m_dicRiskFactorCode_FES)
				{
					if (m_dicRiskFactorCode_FES.Count == 0)
					{
						// 9/3/2008 - Although we're expecting to receive the two-character alpha score factor codes from PRBC/FICO,
						// we've observed some integer format codes.  Handle both.  OPM 24383.
						m_dicRiskFactorCode_FES.Add("AC", "50");
						m_dicRiskFactorCode_FES.Add("122", "50");
						m_dicRiskFactorCode_FES.Add("AG", "51");
						m_dicRiskFactorCode_FES.Add("126", "51");
						m_dicRiskFactorCode_FES.Add("AL", "52");
						m_dicRiskFactorCode_FES.Add("161", "52");
						m_dicRiskFactorCode_FES.Add("AT", "53");
						m_dicRiskFactorCode_FES.Add("169", "53");
						m_dicRiskFactorCode_FES.Add("AV", "54");
						m_dicRiskFactorCode_FES.Add("201", "54");
						m_dicRiskFactorCode_FES.Add("BC", "55");
						m_dicRiskFactorCode_FES.Add("208", "55");
						m_dicRiskFactorCode_FES.Add("BE", "56");
						m_dicRiskFactorCode_FES.Add("220", "56");
						m_dicRiskFactorCode_FES.Add("CE", "57");
						m_dicRiskFactorCode_FES.Add("266", "57");
						m_dicRiskFactorCode_FES.Add("CJ", "58");
						m_dicRiskFactorCode_FES.Add("301", "58");
						m_dicRiskFactorCode_FES.Add("CO", "59");
						m_dicRiskFactorCode_FES.Add("306", "59");
						m_dicRiskFactorCode_FES.Add("CY", "60");
						m_dicRiskFactorCode_FES.Add("326", "60");
						m_dicRiskFactorCode_FES.Add("DA", "61");
						m_dicRiskFactorCode_FES.Add("328", "61");
						m_dicRiskFactorCode_FES.Add("DJ", "62");
						m_dicRiskFactorCode_FES.Add("337", "62");
						m_dicRiskFactorCode_FES.Add("DP", "63");
						m_dicRiskFactorCode_FES.Add("363", "63");
						m_dicRiskFactorCode_FES.Add("EK", "64");
						m_dicRiskFactorCode_FES.Add("384", "64");
						m_dicRiskFactorCode_FES.Add("EO", "65");
						m_dicRiskFactorCode_FES.Add("388", "65");
						m_dicRiskFactorCode_FES.Add("ET", "66");
						m_dicRiskFactorCode_FES.Add("453", "66");
						m_dicRiskFactorCode_FES.Add("EX", "67");
						m_dicRiskFactorCode_FES.Add("457", "67");
						m_dicRiskFactorCode_FES.Add("FG", "68");
						m_dicRiskFactorCode_FES.Add("486", "68");
						m_dicRiskFactorCode_FES.Add("FI", "69");
						m_dicRiskFactorCode_FES.Add("488", "69");
						m_dicRiskFactorCode_FES.Add("FM", "70");
						m_dicRiskFactorCode_FES.Add("512", "70");
						m_dicRiskFactorCode_FES.Add("GJ", "71");
						m_dicRiskFactorCode_FES.Add("535", "71");
						m_dicRiskFactorCode_FES.Add("GN", "72");
						m_dicRiskFactorCode_FES.Add("539", "72");
						m_dicRiskFactorCode_FES.Add("GO", "73");
						m_dicRiskFactorCode_FES.Add("540", "73");
						m_dicRiskFactorCode_FES.Add("GR", "74");
						m_dicRiskFactorCode_FES.Add("543", "74");
						m_dicRiskFactorCode_FES.Add("GW", "75");
						m_dicRiskFactorCode_FES.Add("548", "75");
						m_dicRiskFactorCode_FES.Add("GX", "76");
						m_dicRiskFactorCode_FES.Add("549", "76");
						m_dicRiskFactorCode_FES.Add("HE", "77");
						m_dicRiskFactorCode_FES.Add("556", "77");
						m_dicRiskFactorCode_FES.Add("HO", "78");
						m_dicRiskFactorCode_FES.Add("566", "78");
						m_dicRiskFactorCode_FES.Add("HS", "79");
						m_dicRiskFactorCode_FES.Add("570", "79");
						m_dicRiskFactorCode_FES.Add("IA", "80");
						m_dicRiskFactorCode_FES.Add("578", "80");
						m_dicRiskFactorCode_FES.Add("IJ", "81");
						m_dicRiskFactorCode_FES.Add("607", "81");
						m_dicRiskFactorCode_FES.Add("IM", "82");
						m_dicRiskFactorCode_FES.Add("610", "82");
						m_dicRiskFactorCode_FES.Add("IO", "83");
						m_dicRiskFactorCode_FES.Add("612", "83");
						m_dicRiskFactorCode_FES.Add("IZ", "84");
						m_dicRiskFactorCode_FES.Add("623", "84");
						m_dicRiskFactorCode_FES.Add("JL", "85");
						m_dicRiskFactorCode_FES.Add("665", "85");
						m_dicRiskFactorCode_FES.Add("JP", "86");
						m_dicRiskFactorCode_FES.Add("669", "86");
						m_dicRiskFactorCode_FES.Add("JR", "87");
						m_dicRiskFactorCode_FES.Add("671", "87");
						m_dicRiskFactorCode_FES.Add("KF", "88");
						m_dicRiskFactorCode_FES.Add("685", "88");
						m_dicRiskFactorCode_FES.Add("KT", "89");
						m_dicRiskFactorCode_FES.Add("699", "89");
						m_dicRiskFactorCode_FES.Add("KV", "90");
						m_dicRiskFactorCode_FES.Add("701", "90");
						m_dicRiskFactorCode_FES.Add("LI", "91");
						m_dicRiskFactorCode_FES.Add("724", "91");
						m_dicRiskFactorCode_FES.Add("LP", "92");
						m_dicRiskFactorCode_FES.Add("731", "92");
						m_dicRiskFactorCode_FES.Add("LZ", "93");
						m_dicRiskFactorCode_FES.Add("741", "93");
						m_dicRiskFactorCode_FES.Add("MD", "94");
						m_dicRiskFactorCode_FES.Add("745", "94");
						m_dicRiskFactorCode_FES.Add("MK", "95");
						m_dicRiskFactorCode_FES.Add("752", "95");
						m_dicRiskFactorCode_FES.Add("MP", "96");
						m_dicRiskFactorCode_FES.Add("757", "96");
						m_dicRiskFactorCode_FES.Add("MT", "97");
						m_dicRiskFactorCode_FES.Add("761", "97");
						m_dicRiskFactorCode_FES.Add("NC", "98");
						m_dicRiskFactorCode_FES.Add("770", "98");
						m_dicRiskFactorCode_FES.Add("NN", "99");
						m_dicRiskFactorCode_FES.Add("781", "99");
						m_dicRiskFactorCode_FES.Add("NW", "100");
						m_dicRiskFactorCode_FES.Add("790", "100");
						m_dicRiskFactorCode_FES.Add("OB", "101");
						m_dicRiskFactorCode_FES.Add("795", "101");
						m_dicRiskFactorCode_FES.Add("OJ", "102");
						m_dicRiskFactorCode_FES.Add("803", "102");
						m_dicRiskFactorCode_FES.Add("ON", "103");
						m_dicRiskFactorCode_FES.Add("807", "103");
						m_dicRiskFactorCode_FES.Add("OR", "104");
						m_dicRiskFactorCode_FES.Add("811", "104");
						m_dicRiskFactorCode_FES.Add("OU", "105");
						m_dicRiskFactorCode_FES.Add("814", "105");
						m_dicRiskFactorCode_FES.Add("OV", "106");
						m_dicRiskFactorCode_FES.Add("815", "106");
						m_dicRiskFactorCode_FES.Add("PG", "107");
						m_dicRiskFactorCode_FES.Add("826", "107");
						m_dicRiskFactorCode_FES.Add("PI", "108");
						m_dicRiskFactorCode_FES.Add("828", "108");
						m_dicRiskFactorCode_FES.Add("PS", "109");
						m_dicRiskFactorCode_FES.Add("868", "109");
						m_dicRiskFactorCode_FES.Add("PV", "110");
						m_dicRiskFactorCode_FES.Add("891", "110");
						m_dicRiskFactorCode_FES.Add("PZ", "111");
						m_dicRiskFactorCode_FES.Add("895", "111");
						m_dicRiskFactorCode_FES.Add("QB", "112");
						m_dicRiskFactorCode_FES.Add("897", "112");
						m_dicRiskFactorCode_FES.Add("QF", "113");
						m_dicRiskFactorCode_FES.Add("901", "113");
						m_dicRiskFactorCode_FES.Add("QG", "114");
						m_dicRiskFactorCode_FES.Add("902", "114");
						m_dicRiskFactorCode_FES.Add("QU", "115");
						m_dicRiskFactorCode_FES.Add("916", "115");
						m_dicRiskFactorCode_FES.Add("QV", "116");
						m_dicRiskFactorCode_FES.Add("917", "116");
						m_dicRiskFactorCode_FES.Add("RD", "117");
						m_dicRiskFactorCode_FES.Add("965", "117");
						m_dicRiskFactorCode_FES.Add("RK", "118");
						m_dicRiskFactorCode_FES.Add("972", "118");
						m_dicRiskFactorCode_FES.Add("RO", "119");
						m_dicRiskFactorCode_FES.Add("976", "119");
						m_dicRiskFactorCode_FES.Add("RR", "120");
						m_dicRiskFactorCode_FES.Add("979", "120");
						m_dicRiskFactorCode_FES.Add("RS", "121");
						m_dicRiskFactorCode_FES.Add("1101", "121");
						m_dicRiskFactorCode_FES.Add("RT", "122");
						m_dicRiskFactorCode_FES.Add("1102", "122");
						m_dicRiskFactorCode_FES.Add("RU", "123");
						m_dicRiskFactorCode_FES.Add("1103", "123");
						m_dicRiskFactorCode_FES.Add("RX", "124");
						m_dicRiskFactorCode_FES.Add("1106", "124");
						m_dicRiskFactorCode_FES.Add("RY", "125");
						m_dicRiskFactorCode_FES.Add("1107", "125");
						m_dicRiskFactorCode_FES.Add("RZ", "126");
						m_dicRiskFactorCode_FES.Add("1108", "126");
						m_dicRiskFactorCode_FES.Add("SA", "127");
						m_dicRiskFactorCode_FES.Add("1109", "127");
						m_dicRiskFactorCode_FES.Add("SB", "128");
						m_dicRiskFactorCode_FES.Add("1110", "128");
						m_dicRiskFactorCode_FES.Add("SC", "129");
						m_dicRiskFactorCode_FES.Add("1111", "129");
						m_dicRiskFactorCode_FES.Add("SD", "130");
						m_dicRiskFactorCode_FES.Add("1112", "130");
						m_dicRiskFactorCode_FES.Add("SE", "131");
						m_dicRiskFactorCode_FES.Add("1113", "131");
						m_dicRiskFactorCode_FES.Add("SF", "132");
						m_dicRiskFactorCode_FES.Add("1114", "132");
						m_dicRiskFactorCode_FES.Add("SG", "133");
						m_dicRiskFactorCode_FES.Add("1115", "133");
						m_dicRiskFactorCode_FES.Add("SH", "134");
						m_dicRiskFactorCode_FES.Add("1116", "134");
						m_dicRiskFactorCode_FES.Add("SK", "135");
						m_dicRiskFactorCode_FES.Add("1119", "135");
						m_dicRiskFactorCode_FES.Add("SL", "136");
						m_dicRiskFactorCode_FES.Add("1120", "136");
						m_dicRiskFactorCode_FES.Add("SM", "137");
						m_dicRiskFactorCode_FES.Add("1121", "137");
						m_dicRiskFactorCode_FES.Add("SN", "138");
						m_dicRiskFactorCode_FES.Add("1122", "138");
						m_dicRiskFactorCode_FES.Add("SO", "139");
						m_dicRiskFactorCode_FES.Add("1123", "139");
						m_dicRiskFactorCode_FES.Add("SP", "140");
						m_dicRiskFactorCode_FES.Add("1124", "140");
						m_dicRiskFactorCode_FES.Add("SQ", "141");
						m_dicRiskFactorCode_FES.Add("1125", "141");
						m_dicRiskFactorCode_FES.Add("SR", "142");
						m_dicRiskFactorCode_FES.Add("1126", "142");
						m_dicRiskFactorCode_FES.Add("SS", "143");
						m_dicRiskFactorCode_FES.Add("1127", "143");
						m_dicRiskFactorCode_FES.Add("ST", "144");
						m_dicRiskFactorCode_FES.Add("1128", "144");
						m_dicRiskFactorCode_FES.Add("SU", "145");
						m_dicRiskFactorCode_FES.Add("1129", "145");
						m_dicRiskFactorCode_FES.Add("SV", "146");
						m_dicRiskFactorCode_FES.Add("1130", "146");
						m_dicRiskFactorCode_FES.Add("SW", "147");
						m_dicRiskFactorCode_FES.Add("1131", "147");
						m_dicRiskFactorCode_FES.Add("SX", "148");
						m_dicRiskFactorCode_FES.Add("1132", "148");
						m_dicRiskFactorCode_FES.Add("SZ", "149");
						m_dicRiskFactorCode_FES.Add("1134", "149");
						m_dicRiskFactorCode_FES.Add("TA", "150");
						m_dicRiskFactorCode_FES.Add("1135", "150");
						m_dicRiskFactorCode_FES.Add("TB", "151");
						m_dicRiskFactorCode_FES.Add("1136", "151");
						m_dicRiskFactorCode_FES.Add("TC", "152");
						m_dicRiskFactorCode_FES.Add("1137", "152");
						m_dicRiskFactorCode_FES.Add("TD", "153");
						m_dicRiskFactorCode_FES.Add("1138", "153");
						m_dicRiskFactorCode_FES.Add("VE", "154");
						m_dicRiskFactorCode_FES.Add("1191", "154");
						m_dicRiskFactorCode_FES.Add("VF", "155");
						m_dicRiskFactorCode_FES.Add("1192", "155");
						m_dicRiskFactorCode_FES.Add("VG", "156");
						m_dicRiskFactorCode_FES.Add("1193", "156");
						m_dicRiskFactorCode_FES.Add("VH", "157");
						m_dicRiskFactorCode_FES.Add("1194", "157");
						m_dicRiskFactorCode_FES.Add("VI", "158");
						m_dicRiskFactorCode_FES.Add("1195", "158");
						m_dicRiskFactorCode_FES.Add("VJ", "159");
						m_dicRiskFactorCode_FES.Add("1196", "159");
					}
				}
			}
			return m_dicRiskFactorCode_FES;
		}

		/// <summary>
		/// Calculates the number of 30, 60, and 90 day delinquencies from a payment history grid fragment.
		/// </summary>
		/// <param name="sHistory">A partial FNMA History MOP pattern</param>
		/// <returns>
		/// The number of 30. 60, and 90 day delinquencies;  
		/// these will not be specified if no grid is passed in.
		/// </returns>
		public XPathNodeIterator LateCount(string sHistory)
		{
			XmlDocument xmlDoc = new XmlDocument();
			XmlElement xmlElem = xmlDoc.CreateElement("LateCount");
			xmlDoc.AppendChild(xmlElem);

			if (!string.IsNullOrEmpty(sHistory))
			{
				int n30, n60, n90;
				n30 = n60 = n90 = 0;
				foreach (char cMOP in sHistory)
				{
					if (cMOP == '2')
						n30++;
					else if (cMOP == '3')
						n60++;
					else if (cMOP >= '4' && cMOP <= '6')
						n90++;
				}

				xmlElem.SetAttribute("_30Days", n30.ToString());
				xmlElem.SetAttribute("_60Days", n60.ToString());
				xmlElem.SetAttribute("_90Days", n90.ToString());
			}

			return xmlDoc.CreateNavigator().Select("*");
		}

        /// <summary>
        /// FNMA does not allow '-' in SSN. Remove all dash from ssn.
        /// </summary>
        /// <param name="ssn"></param>
        /// <returns></returns>
        public string Ssn(string ssn)
        {
            if (string.IsNullOrEmpty(ssn))
            {
                return "";
            }
            return ssn.Trim().Replace("-", "");
        }
		/// <summary>
		/// Translates a bureau remark code into a FNMA remark code.
		/// </summary>
		/// <param name="a_sBureau">Bureau name (Equifax, Experian, or TransUnion)</param>
		/// <param name="sRemarkCode">Equifax Narrative Code, Experian Enhanced Special Comment Code, or TransUnion Remarks Code</param>
		/// <returns>FNMA Remarks Code</returns>
		public string RemarkCode(string a_sBureau, string a_sRemarkCode)
		{
			if (string.IsNullOrEmpty(a_sRemarkCode)) return String.Empty;

			switch (a_sBureau)
			{
				case "Equifax":
					{
						switch (a_sRemarkCode)
						{
							case "AA": return "E0001";
							case "AB": return "E0002";
							case "AC": return "E0003";
							case "AE": return "E0004";
							case "AF": return "E0005";
							case "AH": return "E0006";
							case "AI": return "E0007";
							case "AJ": return "E0008";
							case "AL": return "E0010";
							case "AM": return "E0011";
							case "AN": return "E0012";
							case "AO": return "E0013";
							case "AP": return "E0014";
							case "AQ": return "E0015";
							case "AR": return "E0016";
							case "AS": return "E0017";
							case "AT": return "E0018";
							case "AU": return "E0019";
							case "AV": return "E0020";
							case "AW": return "E0021";
							case "AX": return "E0022";
							case "AY": return "E0023";
							case "AZ": return "E0024";
							case "BB": return "E0026";
							case "BC": return "E0027";
							case "BD": return "E0028";
							case "BE": return "E0029";
							case "BG": return "E0030";
							case "BH": return "E0031";
							case "BK": return "E0032";
							case "BL": return "E0033";
							case "BM": return "E0034";
							case "BN": return "E0035";
							case "BO": return "E0036";
							case "BP": return "E0037";
							case "BQ": return "E0038";
							case "BR": return "E0039";
							case "BS": return "E0040";
							case "BT": return "E0041";
							case "BU": return "E0042";
							case "BV": return "E0043";
							case "BW": return "E0044";
							case "BX": return "E0045";
							case "BY": return "E0046";
							case "BZ": return "E0047";
							case "CA": return "E0048";
							case "CB": return "E0049";
							case "CD": return "E0050";
							case "CE": return "E0051";
							case "CF": return "E0052";
							case "CG": return "E0053";
							case "CH": return "E0054";
							case "CI": return "E0055";
							case "CJ": return "E0056";
							case "CK": return "E0057";
							case "CL": return "E0058";
							case "CM": return "E0059";
							case "CN": return "E0060";
							case "CP": return "E0062";
							case "CQ": return "E0063";
							case "CS": return "E0065";
							case "CT": return "E0066";
							case "CU": return "E0067";
							case "CV": return "E0068";
							case "CW": return "E0069";
							case "CX": return "E0070";
							case "CY": return "E0071";
							case "CZ": return "E0072";
							case "DA": return "E0073";
							case "DB": return "E0074";
							case "DC": return "E0075";
							case "DD": return "E0076";
							case "DE": return "E0077";
							case "DG": return "E0079";
							case "DH": return "E0080";
							case "DI": return "E0081";
							case "DJ": return "E0082";
							case "DK": return "E0083";
							case "DL": return "E0084";
							case "DM": return "E0085";
							case "DN": return "E0086";
							case "DO": return "E0087";
							case "DP": return "E0088";
							case "DQ": return "E0089";
							case "DS": return "E0090";
							case "DT": return "E0091";
							case "DU": return "E0092";
							case "DV": return "E0093";
							case "DW": return "E0094";
							case "DX": return "E0095";
							case "DZ": return "E0097";
							case "EA": return "E0098";
							case "EB": return "E0099";
							case "EC": return "E0100";
							case "ED": return "E0101";
							case "EE": return "E0102";
							case "EF": return "E0103";
							case "EG": return "E0104";
							case "EH": return "E0105";
							case "EI": return "E0106";
							case "EJ": return "E0107";
							case "EK": return "E0108";
							case "EL": return "E0109";
							case "EM": return "E0110";
							case "EN": return "E0111";
							case "EO": return "E0112";
							case "EP": return "E0113";
							case "EQ": return "E0114";
							case "ER": return "E0115";
							case "ES": return "E0116";
							case "ET": return "E0117";
							case "EU": return "E0118";
							case "EV": return "E0119";
							case "EX": return "E0120";
							case "EY": return "E0121";
							case "EZ": return "E0122";
							case "FA": return "E0123";
							case "FB": return "E0124";
							case "FC": return "E0125";
							case "FD": return "E0126";
							case "FE": return "E0127";
							case "FF": return "E0128";
							case "FG": return "E0129";
							case "FH": return "E0130";
							case "FL": return "E0131";
							case "FM": return "E0132";
							case "FO": return "E0137";
							case "FP": return "E0138";
							case "FQ": return "E0139";
							case "FR": return "E0140";
							case "FS": return "E0141";
							case "FT": return "E0142";
							case "FU": return "E0143";
							case "FV": return "E0144";
							case "FW": return "E0145";
							case "FX": return "E0146";
							case "AG": return "E0147";
							case "FZ": return "E0148";
							case "GA": return "E0149";
							case "GB": return "E0150";
							case "GC": return "E0151";
							case "GD": return "E0152";
							case "GE": return "E0153";
							case "GF": return "E0154";
							case "GH": return "E0155";
							case "GI": return "E0156";
							case "GJ": return "E0157";
							case "GK": return "E0158";
							case "GL": return "E0159";
							case "GM": return "E0160";
							case "GN": return "E0161";
							case "GO": return "E0162";
							case "GP": return "E0163";
							case "GQ": return "E0164";
							case "GR": return "E0165";
							case "GS": return "E0166";
							case "HF": return "E0167";
							case "HL": return "E0168";
							case "HM": return "E0169";
							case "HN": return "E0170";
							case "HO": return "E0171";
							case "HP": return "E0172";
							case "HQ": return "E0173";
							case "HR": return "E0174";
							case "HS": return "E0175";
							case "HT": return "E0176";
							case "HU": return "E0177";
							case "HV": return "E0178";
							case "HW": return "E0179";
							case "HX": return "E0180";
							case "IA": return "E0181";
							case "IB": return "E0182";
							case "IC": return "E0183";
							case "ID": return "E0184";
							case "IE": return "E0185";
							case "IF": return "E0186";
							case "IG": return "E0187";
							case "IH": return "E0188";
							case "II": return "E0189";
							case "IJ": return "E0190";
							case "IK": return "E0191";
							case "IL": return "E0192";
							case "IM": return "E0193";
							case "IN": return "E0194";
							case "IP": return "E0195";
							case "IQ": return "E0196";
							case "IR": return "E0197";
							case "IT": return "E0198";
							case "IU": return "E0199";
							case "IV": return "E0200";
							case "IW": return "E0201";
							case "IX": return "E0202";
							case "IZ": return "E0203";
							case "JA": return "E0204";
							case "JD": return "E0205";
							case "JE": return "E0206";
							case "JF": return "E0207";
							case "JG": return "E0208";
							case "JH": return "E0209";
							case "JI": return "E0210";
							default: return String.Empty;
						}
					}
				case "Experian":
					{
						switch (a_sRemarkCode)
						{
							case "69": return "R0002";
							case "71": return "R0036";
							case "72": return "R0054";
							case "08": return "R0070";
							case "09": return "R0071";
							case "10": return "R0072";
							case "11": return "R0073";
							case "12": return "R0074";
							case "13": return "R0075";
							case "14": return "R0076";
							case "18": return "R0078";
							case "19": return "R0079";
							case "20": return "R0080";
							case "22": return "R0081";
							case "25": return "R0082";
							case "26": return "R0083";
							case "27": return "R0084";
							case "28": return "R0085";
							case "29": return "R0086";
							case "30": return "R0087";
							case "31": return "R0088";
							case "34":
							case "73": return "R0091";
							case "35": return "R0092";
							case "38": return "R0093";
							case "39": return "R0094";
							case "40": return "R0095";
							case "41": return "R0096";
							case "42": return "R0097";
							case "44": return "R0099";
							case "45": return "R0100";
							case "46": return "R0101";
							case "47": return "R0102";
							case "48": return "R0103";
							case "49": return "R0104";
							case "50": return "R0105";
							case "51": return "R0106";
							case "52": return "R0107";
							case "53": return "R0108";
							case "54": return "R0109";
							case "55": return "R0110";
							case "56": return "R0111";
							case "57": return "R0137";
							case "58": return "R0138";
							case "59": return "R0139";
							case "60": return "R0140";
							case "61": return "R0141";
							case "62": return "R0142";
							case "63": return "R0143";
							case "64": return "R0144";
							case "65": return "R0145";
							case "66": return "R0146";
							case "67": return "R0147";
							case "68": return "R0148";
							case "76": return "R0149";
							case "77": return "R0150";
							case "78": return "R0151";
							case "83": return "R0152";
							case "84": return "R0153";
							case "24": return "R0154";
							case "88": return "R0155";
							case "89": return "R0156";
							case "90": return "R0157";
							case "CH": return "R0161";
							default: return String.Empty;
						}
					}
				case "TransUnion":
					{
						switch (a_sRemarkCode)
						{
							case "AA": return "T0001";
							case "AAP": return "T0002";
							case "AB": return "T0003";
							case "AC": return "T0004";
							case "ACR": return "T0005";
							case "ACT": return "T0006";
							case "AD": return "T0007";
							case "AE": return "T0008";
							case "AF": return "T0009";
							case "AFR": return "T0010";
							case "AG": return "T0011";
							case "AH": return "T0012";
							case "AI": return "T0013";
							case "AJ": return "T0014";
							case "AJP":
							case "AJX": return "T0015";
							case "AK": return "T0016";
							case "AL": return "T0017";
							case "AM": return "T0018";
							case "AMD": return "T0019";
							case "AN": return "T0020";
							case "AND": return "T0021";
							case "AOA": return "T0022";
							case "AP": return "T0023";
							case "AR": return "T0024";
							case "AS": return "T0025";
							case "ASM": return "T0026";
							case "AT": return "T0027";
							case "BDC": return "T0028";
							case "BDI": return "T0029";
							case "BDM": return "T0030";
							case "BKL": return "T0031";
							case "BTA": return "T0032";
							case "CAG": return "T0033";
							case "CBC": return "T0034";
							case "CBG": return "T0035";
							case "CBL": return "T0036";
							case "CBR": return "T0037";
							case "CCA": return "T0038";
							case "CCC": return "T0039";
							case "CCG": return "T0040";
							case "CD":
							case "DIS": return "T0041";
							case "CHK": return "T0042";
							case "CLA": return "T0043";
							case "CLB": return "T0044";
							case "CLC": return "T0045";
							case "CLO": return "T0046";
							case "CLS": return "T0047";
							case "CNR": return "T0048";
							case "COL": return "T0049";
							case "CPM": return "T0050";
							case "CPP": return "T0051";
							case "CSA": return "T0052";
							case "CSF": return "T0053";
							case "CTR": return "T0054";
							case "CTS":
							case "CTX": return "T0055";
							case "CWC": return "T0056";
							case "DEC": return "T0057";
							case "DGX": return "T0058";
							case "DLF": return "T0060";
							case "DLU": return "T0061";
							case "DP":
							case "DRP": return "T0062";
							case "DM": return "T0063";
							case "DRC":
							case "DRX": return "T0064";
							case "DRG": return "T0065";
							case "DRS": return "T0066";
							case "ELN": return "T0067";
							case "ER":
							case "ERX": return "T0068";
							case "ET": return "T0069";
							case "ETA": return "T0070";
							case "ETD": return "T0071";
							case "FCL": return "T0072";
							case "FFB": return "T0073";
							case "FHA": return "T0074";
							case "FHI": return "T0075";
							case "FPD": return "T0076";
							case "FPI": return "T0077";
							case "FPS": return "T0078";
							case "FRD": return "T0079";
							case "GEA": return "T0080";
							case "GFS": return "T0081";
							case "GGG": return "T0082";
							case "GMD": return "T0083";
							case "GOF": return "T0084";
							case "GOP": return "T0085";
							case "GVC": return "T0086";
							case "HEQ": return "T0087";
							case "IA":
							case "INA": return "T0088";
							case "IB": return "T0089";
							case "ICP": return "T0090";
							case "INC": return "T0091";
							case "INP": return "T0092";
							case "INS": return "T0093";
							case "JUD": return "T0094";
							case "LBR": return "T0095";
							case "LEA": return "T0096";
							case "MCC": return "T0097";
							case "MOV": return "T0098";
							case "ND": return "T0099";
							case "NIB": return "T0100";
							case "NIR":
							case "NIX": return "T0101";
							case "NPA": return "T0102";
							case "OFS": return "T0103";
							case "PAL": return "T0104";
							case "PBB": return "T0105";
							case "PBD": return "T0106";
							case "PCL": return "T0107";
							case "PDD": return "T0108";
							case "PFC": return "T0109";
							case "PLP": return "T0110";
							case "PNR": return "T0111";
							case "POA": return "T0112";
							case "PPA": return "T0113";
							case "PPC": return "T0114";
							case "PPD": return "T0115";
							case "PPL": return "T0116";
							case "PRD": return "T0117";
							case "PRL": return "T0118";
							case "PTD": return "T0119";
							case "PWG": return "T0120";
							case "RAC": return "T0121";
							case "RDP": return "T0122";
							case "REA": return "T0123";
							case "RFN": return "T0124";
							case "RLD": return "T0125";
							case "RLP": return "T0126";
							case "RPD": return "T0127";
							case "RPF": return "T0128";
							case "RPO": return "T0129";
							case "RRE": return "T0130";
							case "RS": return "T0131";
							case "RVC": return "T0132";
							case "RVD": return "T0133";
							case "RVE": return "T0134";
							case "RVN": return "T0135";
							case "RVP": return "T0136";
							case "RVR": return "T0137";
							case "SBB": return "T0138";
							case "SDL": return "T0139";
							case "SET": return "T0140";
							case "SGL": return "T0141";
							case "SIL": return "T0142";
							case "SK": return "T0143";
							case "SLA": return "T0144";
							case "SLD": return "T0145";
							case "SLP": return "T0146";
							case "SPD": return "T0147";
							case "SPL": return "T0148";
							case "SRG": return "T0149";
							case "STL": return "T0150";
							case "STU": return "T0151";
							case "TRF": return "T0152";
							case "TRL": return "T0153";
							case "TTR": return "T0154";
							case "UDL": return "T0155";
							case "UGL": return "T0156";
							case "VAR": return "T0157";
							case "VAL": return "T0158";
							case "VCC": return "T0159";
							case "VPD": return "T0160";
							case "VPI": return "T0161";
							case "WEP": return "T0162";
							case "ACQ": return "T0163";
							case "AID": return "T0164";
							case "BAL": return "T0165";
							case "BCD": return "T0166";
							case "BKC": return "T0167";
							case "BKD": return "T0168";
							case "BKW": return "T0169";
							case "BRC": return "T0170";
							case "BRR": return "T0171";
							case "CAD": return "T0172";
							case "CBD": return "T0173";
							case "CBT": return "T0174";
							case "CCD": return "T0175";
							case "CDC": return "T0176";
							case "CDD": return "T0177";
							case "CDL": return "T0178";
							case "CDR": return "T0179";
							case "CDT": return "T0180";
							case "CED": return "T0181";
							case "CFD": return "T0182";
							case "CPB": return "T0183";
							case "CRC": return "T0184";
							case "CRD": return "T0185";
							case "CRL": return "T0186";
							case "CRR": return "T0187";
							case "CRT": return "T0188";
							case "CRV": return "T0189";
							case "CTC": return "T0190";
							case "CTV": return "T0191";
							case "DGR": return "T0192";
							case "ETB": return "T0193";
							case "ETI": return "T0194";
							case "ETO": return "T0195";
							case "ETS": return "T0196";
							case "FTB": return "T0197";
							case "FTO": return "T0198";
							case "FTS": return "T0199";
							case "IRB": return "T0200";
							case "IRE": return "T0201";
							case "IRO": return "T0202";
							case "JUG": return "T0203";
							case "PDE": return "T0204";
							case "PDI": return "T0205";
							case "PLL": return "T0206";
							case "REP": return "T0207";
							case "WCD": return "T0208";
							case "WPC": return "T0209";
							case "WPD": return "T0210";
							case "WRC": return "T0211";
							case "WRR": return "T0212";
							default: return String.Empty;
						}
					}
				default:
					return String.Empty;
			}
		}

		/// <summary>
		/// Converts an MCL Exclusion Factor code into a FNMA ExceptionCode
		/// </summary>
		/// <param name="a_sScoringModelID">FNMA ScoringModelID</param>
		/// <param name="sExclusionCode">MCL Exclusion Factor code</param>
		/// <returns>FNMA ExceptionCode</returns>
		public string ExceptionCode(string a_sScoringModelID, string a_sExclusionCode)
		{
			switch (a_sScoringModelID)
			{
				case "2":	// Equifax DAS
					{
						if (a_sExclusionCode == "F")
							return "I";
						else if (!string.IsNullOrEmpty(a_sExclusionCode))
							return "Z";
						else
							break;
					}

				case "3":	// Equifax BEACON 96
				case "10":	// Equifax Enhanced BEACON
				case "13":	// Equifax BEACON 5.0
					{
						switch (a_sExclusionCode)
						{
							case "L":
							case "B1":
							case "L0":
							case "P1":
								return "D";
							case "M":
							case "P":
							case "X":
							case "F1":
								return "N";
							case "N":
							case "F2":
							case "L1":
							case "L2":
							case "L4":
							case "P2":
							case "P3":
							case "P5":
							case "R0":
								return "I";
							case "O":
							case "R":
							case "F3":
							case "L3":
							case "P4":
								return "R";
							case "F4":
								return "X";
							default:
								{
									if (!string.IsNullOrEmpty(a_sExclusionCode))
										return "Z";
									break;
								}
						}
						break;
					}

				case "8":	// TransUnion Delphi
				case "9":	// TransUnion FICO Classic 98
				case "12":	// TransUnion FICO Classic 95
				case "14":	// TransUnion FICO Classic 04
					{
						switch (a_sExclusionCode)
						{
							case "SC2":
								return "D";
							case "SC3":
								return "I";
							case "AO02":
							case "AO03":
							case "AO05":
							case "AO06":
							case "SC4":
							case "SC5":
							case "SCC":
							case "SCM":
								return "Z";
							default:
								break;
						}
						break;
					}

				case "4":	// Experian MDS
				case "5":	// Experian National Generic RISC
				case "6":	// Experian FICO v2
				case "7":	// Experian MDS Bankruptcy
				case "11":	// Experian FICO v1
				case "15":	// Experian FICO v3
					{
						int nCode = Util.SafeInt(a_sExclusionCode);
						if (nCode >= 9000)
						{
							switch (nCode)
							{
								case 9001:
									return "D";
								case 9002:
									return "X";
								case 9003:
									return "R";
								case 9004:
									return "I";
								default:
									return "Z";
							}
						}
						break;
					}
			}

			return string.Empty;
		}

		/// <summary>
		/// Translates a conventional Risk Factor code into a FNMA Risk Factor code
		/// </summary>
		/// <param name="a_sScoringModelID">FNMA ScoringModelID</param>
		/// <param name="sFactorCode">Bureau risk factor code</param>
		/// <returns>FNMA Risk Factor Code</returns>
		public string RiskFactorCode(string a_sScoringModelID, string sFactorCode)
		{
			switch (a_sScoringModelID)
			{
				case "2" :		// Equifax DAS						// uncommented out by Chuong
				case "3":		// Equifax BEACON 96
				case "10":		// Equifax Enhanced BEACON
				case "13":		// Equifax BEACON 5.0
					return Lookup(GetRiskFactorCodes_EQF(), Util.SafeInt(sFactorCode).ToString());
				case "8":		// TransUnion Delphi				// uncommented out by Chuong
				case "9":		// TransUnion FICO Classic 98
				case "12":		// TransUnion FICO Classic 95
				case "14":		// TransUnion FICO Classic 04
					return Lookup(GetRiskFactorCodes_TUC(), Util.SafeInt(sFactorCode).ToString());
				case "4":		// Experian MDS						// uncommented out by Chuong
				case "5":		// Experian National Generic RISC	// uncommented out by Chuong
				case "6":		// Experian FICO v2
				case "7":		// Experian MDS Bankruptcy			// uncommented out by Chuong
				case "11":		// Experian FICO v1
				case "15":		// Experian FICO v3
					return Lookup(GetRiskFactorCodes_EXP(), Util.SafeInt(sFactorCode).ToString());
				case "19":
					return Lookup(GetRiskFactorCodes_FES(), sFactorCode);
				default:
					return sFactorCode;
			}
		}

		#endregion CODE COPIED FROM MclObjLib.Credit.FnmaSigUtil

		public XsltUtil(){}
		
		/// <summary>
		/// Convert from MISMO AccountOwnershipType to SIG ECOA
		/// </summary>
		public string ECOA(string a_sMismoAccountOwnershipType)
		{
			switch (a_sMismoAccountOwnershipType)
			{
				// used in Public Record, Tradeline
				case "Individual": 
					return "I";
				// used in Public Record
				case "JointParticipating":
				case "JointContractualLiability":
				case "Joint": 
					return "J";
				// used in Public Record, Tradeline
				case "Terminated": 
					return "T";
				case "Maker": 
					return "M";	
				case "Comaker": 
					return "C";
				case "AuthorizedUser":
					return "A";
				case "Undesignated":
					return "U";						
				default: 
					return string.Empty;
			}
		}

		/// <summary>
        /// Convert month from MISMO format (ccyy-MM) || (ccyy-MM-DD) to SIG format (MMccyy)
		/// </summary>
		public string FormatMonth(string a_sMismoMonth)
		{
			if (string.IsNullOrEmpty(a_sMismoMonth)) return string.Empty;

            string tmpMismoMonth = a_sMismoMonth;
            
            if (tmpMismoMonth.Length == 7 && tmpMismoMonth.IndexOf("-") == 4)
            {
                return Util.Right(a_sMismoMonth, 2) + Util.Left(a_sMismoMonth, 4);
            }
            else if (tmpMismoMonth.Length == 10 && tmpMismoMonth.IndexOf("-") == 4 && tmpMismoMonth.IndexOf("-",5) == 7)
            {
                tmpMismoMonth = tmpMismoMonth.Substring(0, 7);
                return Util.Right(tmpMismoMonth, 2) + Util.Left(tmpMismoMonth, 4);
            }
            else
            {
                return string.Empty;
            }
		}

		public string PublicRecordTypeCode(string a_sMismoPublicRecordType)
		{
			switch (a_sMismoPublicRecordType)
			{
				case "BankruptcyChapter7":
				case "BankruptcyChapter7Involuntary":
				case "BankruptcyChapter7Voluntary":
					return "01";
				case "BankruptcyChapter11":
					return "02";
				case "BankruptcyChapter12":
					return "03";
				case "BankruptcyChapter13":
					return "04";
				case "BankruptcyTypeUnknown":
					return "05";
				case "TaxLienFederal":
					return "06";
				case "TaxLienState":
					return "07";
				case "TaxLienCounty":
					return "08";
				case "TaxLienCity":
					return "09";
				case "TaxLienOther":
					return "12";
				case "Judgment":
					return "14";
				//case "Annulment":
				//	return "18"							no longer valid code
				case "Garnishment":
					return "22";
				case "Foreclosure":
					return "24";
				case "ForcibleDetainer":
					return "32";
				case "Attachment":
					return "36";
				case "Lien":
					return "40";
				case "Trusteeship":
					return "44";

				//case "SupportDebt":
				//case "Collection":
				//case "CustodyAgreement":
				//case "DivorceDecree":
				//case "FicticiousName":
				//case "FinancialCounseling":
				//case "FinancingStatement":
				//case "LawSuit":
				//case "NonResponsibility":
				//case "NoticeOfDefault":
				//case "Other":
				//case "PublicSale":
				//case "RealEstateRecording":
				//case "Repossession":
				//case "Unknown":
				//case "UnlawfulDetainer":
				default:
					return String.Empty;
			}
		
		}

		public string PublicRecordClass(string a_sTypeCode)
		{
			switch (a_sTypeCode)
			{
				case "01":
				case "02":
				case "03":
				case "04":
				case "05":
					return "1";	// Bankruptcy
				case "13":
				case "14":
				case "15":
				case "16":
				case "17":
				case "32":
				case "49":
					return "2";	// Civil or Judgment
				case "06":
				case "07":
				case "08":
				case "09":
				case "10":
				case "11":
				case "12":
				case "36":
				case "37":
				case "40":
					return "3";	// Lien
				case "24":
				case "28":
				case "30":
				case "34":
				case "39":
				case "45":
				case "46":
				case "47":
				case "50":
					return "4";	// Real Estate
				case "18":
				case "20":
				case "21":
				case "53":
					return "5";	// Marital Status
				case "19":
				case "22":
				case "25":
				case "26":
				case "27":
				case "29":
				case "31":
				case "33":
				case "35":
				case "38":
				case "41":
				case "42":
				case "43":
				case "44":
				case "48":
				case "51":
					return "6";	// Miscellaneous
				default:
					return String.Empty;
			}
		}

		public string PublicRecordStatusCode(string a_sMismoPublicRecordStatus)
		{
			switch (a_sMismoPublicRecordStatus)
			{
				//case "Discharged":
				//	return "01";					// no longer valid code
				//case "InvoluntarilyDischarged":
				//	return "02";					// no longer valid code
				//case "VoluntarilyDischarged":
				//	return "03";					// no longer valid code
				//case "Granted":
				//	return "04";					// no longer valid code
				case "Released":
					return "05";
				case "Satisfied":
					return "06";
				case "Appealed":
					return "07";
				case "Dismissed":
					return "08";
				case "Pending":
					return "09";
				case "Settled":
					return "10";
				case "Nonadjudicated":
					return "11";
				//case "Completed":
				//	return "13";					// no longer valid code
				case "Vacated":
					return "14";
				case "Paid":
					return "19";
				case "Cancelled":
					return "20";
				//case "RealEstateSold":
				//	return "21";					// no longer valid code
				//case "Adjudicated":
				//	return "22";					// no longer valid code
				case "Filed":
					return "23";
				//case "Unreleased":
				//case "Unsatisfied":
				//case "Converted":
				//case "Distributed":
				//case "Other":
				//case "PaidNotSatisfied":
				//case "Rescinded":
				//case "Unknown":
				//case "Withdrawn":
				default:
					return string.Empty;
			}		
		}

		/// <summary>
		/// Converts inquiry date from MISMO format (ccyy-MM-dd) to SIG format (MMddccyy)
		/// </summary>
		public string InquiryDate(string a_sMismoInquiryDate)
		{
			if (string.IsNullOrEmpty(a_sMismoInquiryDate)) return string.Empty;
			string[] arrParts = a_sMismoInquiryDate.Split('-');
			if (arrParts.Length != 3) return string.Empty;
			return arrParts[1] + arrParts[2] + arrParts[0];
		} 
		
		/// <summary>
		/// Gets MOP (Manner of payment) from MISMO Rating
		/// </summary>
		public string MOPFromRating(string a_sMismoRatingType)
		{
			switch (a_sMismoRatingType)
			{
				case "TooNew"						: return "0";
				case "AsAgreed"						: return "1";
				case "Late30Days"					: return "2";
				case "Late60Days"					: return "3";
				case "Late90Days"					: return "4";
				case "LateOver120Days"				: return "5";
				case "WageEarnerPlan"				:
				case "BankruptcyOrWageEarnerPlan"	: return "7";
				case "Foreclosure"					:
				case "Repossession"					:
				case "ForeclosureOrRepossession"	: return "8";
				case "Collection"					:
				case "ChargeOff"					:
				case "CollectionOrChargeOff"		: return "9";
				//case "NoDataAvailable"			: return "U";
				default								: return string.Empty;
			}
		}
		
		/// <summary>
		/// Gets SIG Credit Type Code from MISMO Account Type and AccountTypeCode
		/// </summary>
		public string CreditTypeCode(string a_sMismoAccountType, string a_sAccountTypeCode)
		{
			switch (a_sMismoAccountType)
			{
				case "CreditLine"	: return "C";
				case "Installment"	: return "I";
				case "Open"			: return "O";
				case "Mortgage"		: return "M";
				case "Revolving"	: return "R";
				default				: 
					if (a_sAccountTypeCode == "YY")
						return "Y";
					else	
						return string.Empty;
			}
		}

		/// <summary>
		/// Gets SIG Account Type Code from MISMO Credit Loan Type
		/// </summary>		
		public string AccountTypeCode(string a_sMismoCreditLoanType)
		{
			switch (a_sMismoCreditLoanType)
			{
				case "ApplianceOrFurniture"				: return "AF";
				//no longer valid code
				//case "CollectionAttorney"				: return "AG";
				case "AutoLease"						: return "AL";
				case "Airplane"							: return "AP";
				case "AutoRefinance"					: return "AR";
				case "AutoLoanEquityTransfer"			: return "AT";
				case "Automobile"						: return "AU";
				case "Agriculture"						: return "AX";
				case "BusinessCreditCard"				: return "BC";
				case "GovernmentBenefit"				: return "BEN";
				case "RevolvingBusinessLines"			: return "BPG";
				case "Boat"								: return "BT";
				case "Business"							: return "BUS";
				case "CheckCreditOrLineOfCredit"		: return "C/LN";
				case "CombinedCreditPlan"				: return "CB";
				case "CreditCard"						: return "CC";
				case "CommercialLineOfCredit"			: return "CE";
				case "CommercialCreditObligation"		: return "CG";
				case "ChargeAccount"					: return "CH";
				case "Comaker"							: return "CM";
				case "ChildSupport"						: return "CP";
				case "ConditionalSalesContract"			: return "CR";
				case "ConventionalRealEstateMortgage"	: return "CV";
				case "CommercialMortgage"				: return "CY";
				case "DebitCard"						: return "DC";
				case "DepositRelated"					: return "DR";
				case "DebtCounselingService"			: return "DS";
				case "Educational"						: return "EDUC";
				case "Employment"						: return "EMP";
				case "ManualMortgage"					: return "EXM";
				case "FactoringCompanyAccount"			: return "FC";
				case "AttorneyFees"						: return "FE";
				case "FHAComakerNotBorrower"			: return "FH/C";
				case "FHAHomeImprovement"				: return "FHA";
				case "FamilySupport"					: return "FM";
				//no longer valid code
				//case "FarmersHomeAdministrationFHMA"	: return "FMH";
				case "FHARealEstateMortgage"			: return "FR";
				case "FinanceStatement"					: return "FS";
				case "GovernmentEmployeeAdvance"		: return "GA";
				case "GovernmentFeeForService"			: return "GE";
				case "GovernmentFine"					: return "GF";
				case "GovernmentGrant"					: return "GG";
				case "GovernmentOverpayment"			: return "GO";
				case "GovernmentSecuredDirectLoan"		: return "GS";
				case "GovernmentSecuredGuaranteeLoan"	: return "GS";
				case "GovernmentUnsecuredDirectLoan"	: return "GU";
				case "GovernmentUnsecuredGuaranteeLoan"	: return "GU";
				case "HouseholdGoodsAndOtherCollateralAuto": return "H+O";
				case "HomeEquityLineOfCredit"			: return "HEL";
				case "HouseholdGoods"					: return "HG";
				case "HouseholdGoodsSecured"			: return "HHG";
				case "HomeImprovement"					: return "HI";
				case "InsuranceClaims"					: return "IN";
				case "InstallmentLoan"					: return "INS";
				case "InstallmentSalesContract"			: return "IS";
				case "Lease"							: return "LE";
				case "LenderPlacedInsurance"			: return "LI";
				case "CreditLineSecured"				: return "LS";
				case "MobileHome"						: return "MT";
				case "MedicalDebt"						: return "MD";
				case "GovernmentMiscellaneousDebt"		: return "MSC";
				case "NoteLoan"							: return "NT";
				case "NoteLoanWithComaker"				: return "NTCM";
				case "PersonalLoan"						: return "PL";
				case "PartiallySecured"					: return "PS";
				case "RealEstateMortgageWithoutOtherCollateral": return "R/O";
				case "RentalAgreement"					: return "RA";
				case "ReturnedCheck"					: return "RC";
				case "Recreational"						: return "RD";
				case "RealEstateSpecificTypeUnknown"	: return "RE";
				case "RefundAnticipationLoan"			: return "RF";
				case "RealEstateJuniorLiens"			: return "RL";
				case "Mortgage"							: return "RM";
				case "RealEstateLoanEquityTransfer"		: return "RT";
				case "RecreationalVehicle"				: return "RV";
				case "SecuredByCosigner"				: return "S/CO";
				case "SummaryOfAccountsWithSameStatus"	: return "SA";
				case "SecuredCreditCard"				: return "SC";
				case "Secured"							: return "SE";
				case "SecuredHomeImprovement"			: return "SHI";
				case "SecondMortgage"					: return "SM";
				case "SemiMonthlyMortgagePayment"		: return "SMP";
				case "SinglePaymentLoan"				: return "SPL";
				case "SpouseSupport"					: return "SU";
				case "Title1Loan"						: return "T/L";
				case "TimeSharedLoan"					: return "TSL";
				case "Unsecured"						: return "UNS";
				case "VeteransAdministrationLoan"		: return "VA";
				case "VeteransAdministrationRealEstateMortgage": return "VM";
				case "Collection"						: return "YY";
				//case "Consolidation":
				//case "FederalConsolidatedLoan":
				//case "Refinance":
				//case "ResidentialLoan":
				//case "UnknownLoanType": 
				//case "BiMonthlyMortgageTermInYears":
				//case "ConstructionLoan":
				//case "DeferredStudentLoan":
				//case "Other":
				default									: return string.Empty;
			}
		}

		/// <summary>
		/// Gets SIG Account Terms Type Code from MISMO TermsSourceType
		/// </summary>
		public string AccountTermsTypeCode(string a_sMismoTermsSourceType)
		{
			if (a_sMismoTermsSourceType == "Calculated") return "C";
			if (a_sMismoTermsSourceType == "Provided") return "P";
			return string.Empty;
		}

		/// <summary>
		/// Gets SIG AccountStatusCode from MISMO AccountStatusType
		/// </summary>
		public string AccountStatusCode(string a_sMismoAccountStatusType)
		{
			switch (a_sMismoAccountStatusType)
			{
				case "Closed"		: return "C";
				case "Open"			: return "S";
				case "Frozen"		: return "F";
				case "Paid"			: return "P";
				case "Transferred"	: return "T";
				case "Refinanced"	: return "R";
				default: return string.Empty;
			}
		}

		/// <summary>
		/// Gets SIG History MOP Code from MISMO Payment Pattern Data
		/// </summary>
		/// <remarks>
		/// MISMO
		/// C = Current
		/// 1 = 30 +
		/// 2 = 60 +
		/// 3 = 90 +
		/// 4 = 120 +
		/// 5 = 150 +
		/// 6 = 180 +
		/// 7 = wage earner
		/// 8 = Repossession/Foreclosure
		/// 9 = ChargeOff/Collection
		/// N = no activity
		/// X = no data available 
		/// SIG
		/// 0 = Too new
		/// 1 = Current
		/// 2 = 30 +
		/// 3 = 60 +
		/// 4 = 90 +
		/// 5 = 120 +
		/// 6 = 150 +
		/// 7 = wage earner or bancruptcy
		/// 8 = repossession or foreclosure
		/// 9 = collection or charge-off
		/// U = unrated
		/// - = unreported
		/// </remarks>
		public string HistoryMOP(string a_sMismoPaymentPatternData)
		{
			if (string.IsNullOrEmpty(a_sMismoPaymentPatternData)) return string.Empty;
			char[] arrPaymentPattern = Util.Left(a_sMismoPaymentPatternData, 36).ToCharArray();
			char[] arrHistoryMOP = new char[arrPaymentPattern.Length];
			for (int i = 0; i < arrHistoryMOP.Length; i++)
			{
				arrHistoryMOP[i] = this.MOPFromPaymentPattern(arrPaymentPattern[i]);
			}

			return new string(arrHistoryMOP);
		}
		
		public char MOPFromPaymentPattern(char a_cMismoPaymentPatternData)
		{
			switch (a_cMismoPaymentPatternData)
			{
				case 'C': return '1';
				case '1': return '2';
				case '2': return '3';
				case '3': return '4';
				case '4': return '5';
				case '5': return '6';
				case '6': return '6';
				case '7': return '7';
				case '8': return '8';
				case '9': return '9';
				default	: return '-';
			}		
		}

		/// <summary>
		/// Gets SIG Scoring Model ID from MISMO ModelNameType
		/// </summary>
		/// <param name="a_sBureau"></param>
		/// <param name="a_sMismoModelNameType"></param>
		/// <returns></returns>
		public string ScoringModelID(string a_sBureau, string a_sMismoModelNameType, string a_sMismoModelNameTypeOtherDescription)
		{
			if (a_sMismoModelNameType == "Other" && a_sMismoModelNameTypeOtherDescription == "Credit Quote")
				return "1";

			if (a_sBureau == "Equifax")
			{
				 switch (a_sMismoModelNameType)
				 {
					case "EquifaxDAS":
					case "EquifaxEnhancedDAS":
						return "2";
					case "EquifaxBeacon":
					case "EquifaxBeaconAuto":
					case "EquifaxBeaconBankcard":
					case "EquifaxBeaconInstallment":
					case "EquifaxBeaconPersonalFinance":
						return "3";	
					case "EquifaxEnhancedBeacon":
						return "10";
					case "EquifaxBeacon5.0":
					case "EquifaxBeacon5.0Auto":
					case "EquifaxBeacon5.0Bankcard":
					case "EquifaxBeacon5.0Installment":
					case "EquifaxBeacon5.0PersonalFinance":
						return "13";
					//case "EquifaxMarketMax":
					//case "EquifaxMortgageScore":
					//case "EquifaxPinnacle":
					//case "EquifaxPinnacle2.0":
                     case "Other":
                        switch (a_sMismoModelNameTypeOtherDescription)
                        {
                            case "BEACON 5.0 FICO":
                            case "EquifaxFACTABeacon5.0":
                                return "13";
                            default:
                                return string.Empty;
                        }
					default: return string.Empty;
				 }
			}
			else if (a_sBureau == "Experian")
			{
				 switch (a_sMismoModelNameType)
				 {
					case "ExperianOldNationalRisk":
					case "ExperianNewNationalRisk":
					case "ExperianNewNationalEquivalency":
						return "5";
					case "ExperianFairIsaac":
					case "ExperianFairIsaacAuto":
					case "ExperianFairIsaacBankcard":
					case "ExperianFairIsaacInstallment":
					case "ExperianFairIsaacPersonalFinance":
					//case "ExperianFairIsaacAdvanced":
					//case "ExperianFairIsaacAdvanced2.0":	// not sure if this is New Experian Fair Isaac Model (FICO II)
					//case "ExperianScorexPLUS":
						return "6";
					case "ExperianMDSBankruptcyII":
						return "7";						
					case "Other":
						switch (a_sMismoModelNameTypeOtherDescription)
						{
							case "MDS":
								return "4";
							case "OriginalExperianFairIsaac":
							case "OriginalExperianFairIsaacAuto":
							case "OriginalExperianFairIsaacBankcard":
							case "OriginalExperianFairIsaacInstallment":
							case "OriginalExperianFairIsaacPersonalFinance":
								return "11";
							// For ExperianFICOClassicV3, return "6" instead of "15", based on
							// Credit Agencies System Integration Guide - 10th Edition - Aug 22, 2008:
							//   "Fannie Mae does not accept the /Fair Isaac Risk Model v3 
							//   (Scoring Model ID Code = 15) in the fixed file format (.DAT)"
							 case "ExperianFICOClassicV3":
								return "6";
                            case "FICO v2":
                                return "6"; // 10/18/2011 dd - For Funding Suite Credit Report
							default: return string.Empty;
						}
					default: return string.Empty;
				 }
			}
			else if (a_sBureau == "TransUnion")
			{
				switch (a_sMismoModelNameType)
				{
					case "TransUnionDelphi":
					case "TransUnionNewDelphi":
						return "8";
					case "FICORiskScoreClassic98":
					case "FICORiskScoreClassicAuto98":
					case "FICORiskScoreClassicBankcard98":
					case "FICORiskScoreClassicInstallmentLoan98":
					case "FICORiskScoreClassicPersonalFinance98":
						return "9";
					case "TransUnionEmpirica":
					case "TransUnionEmpiricaAuto":
					case "TransUnionEmpiricaBankcard":
					case "TransUnionEmpiricaInstallment":
					case "TransUnionEmpiricaPersonalFinance":
						return "12";
					case "FICORiskScoreClassic04":
						return "14";
                    case "Other":
                        if (a_sMismoModelNameTypeOtherDescription == "FICO Classic 04")
                        {
                            // 10/18/2011 dd - Use by Funding Suite credit report.
                            return "14";
                        }
                        else
                        {
                            return string.Empty;
                        }
					//case "TransUnionPrecision":
					//case "FICORiskScoreNextGen00":
					//case "TransUnionPrecision03":
					//case "FICORiskScoreNextGen03":
					default: return string.Empty;
				}
			}
			else if (a_sBureau == "Other")
			{
				switch (a_sMismoModelNameType)
				{
					case "FICOExpansionScore":
						return "19";
					default: return string.Empty;
				}
			}
			return string.Empty;
		}
	}
}
