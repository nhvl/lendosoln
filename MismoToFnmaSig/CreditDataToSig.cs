﻿using System;
using System.IO;
using System.Xml;
using System.Text;

namespace MismoToFnmaSig
{

	/// <summary>
	/// Utility class to convert data from intermediate format to Fannie Mae's SIG format
	/// </summary>
	internal class CreditDataToSig
	{
		private XmlDocument m_xmlParsing;

		public CreditDataToSig(XmlDocument a_xmlParsingInstructions)
		{
			this.m_xmlParsing = a_xmlParsingInstructions;
		}

		public string Execute(string a_sInput)
		{
			XmlDocument xmlData = new XmlDocument();
			try
			{
				xmlData.LoadXml(a_sInput);
			}
			catch (Exception ex)
			{
				throw new ApplicationException("Failed to load data.", ex);
			}

			try
			{
				StringBuilder sb = new StringBuilder(1000);
				using (TextWriter writer = new StringWriter(sb))
				{
					this.ProcessSegments(xmlData.DocumentElement, m_xmlParsing.DocumentElement, writer);
				}
				return sb.ToString();
			}
			catch (Exception ex)
			{
				throw new ApplicationException("Failed to convert data.", ex);
			}
		}

		/// <summary>
		/// Process segments in the document.
		/// </summary>
		/// <param name="a_xmlData">Root element of the data document</param>
		/// <param name="a_xmlParsing">Root element of the parsing document</param>
		/// <param name="a_writer">TextWriter.</param>
		private void ProcessSegments(XmlElement a_xmlData, XmlElement a_xmlParsing, TextWriter a_writer)
		{
			foreach (XmlElement xmlParsingSeg in a_xmlParsing.SelectNodes("SEGMENT"))
			{
				string sSegName = Util.SafeString(xmlParsingSeg.GetAttribute("name"));
				if (sSegName.Length == 0) sSegName = "SEGMENT";
				foreach (XmlElement xmlSegData in a_xmlData.SelectNodes(sSegName))
				{
					string sID = xmlParsingSeg.GetAttribute("id");
					if (!string.IsNullOrEmpty(sID)) a_writer.Write(sID);

					this.ProcessFields(xmlSegData, xmlParsingSeg, a_writer);

					// Recursive call
					this.ProcessSegments(xmlSegData, xmlParsingSeg, a_writer);
				}
			}
		}

		/// <summary>
		/// Processes each data field in a segment.
		/// </summary>
		/// <param name="a_xmlData">Segment Data</param>
		/// <param name="a_xmlParsingSeg">Corresponding parsing element</param>
		/// <param name="a_writer">TextWriter.</param>
		private void ProcessFields(XmlElement a_xmlData, XmlElement a_xmlParsingSeg, TextWriter a_writer)
		{
			foreach (XmlElement xmlField in a_xmlParsingSeg.SelectNodes("FIELD"))
			{
				int nSize = Util.SafeInt(xmlField.GetAttribute("size"));
				string sFieldName = Util.SafeString(xmlField.GetAttribute("name"));
				string sFieldType = Util.SafeString(xmlField.GetAttribute("field_type"));
				if (sFieldType.Length == 0) sFieldType = "normal";
				switch (sFieldType)
				{
					case "normal":
						this.WriteField(a_xmlData.GetAttribute(sFieldName), nSize, a_writer);
						break;
					case "yn":
					case "default":
						this.WriteField(xmlField.GetAttribute("default_value"), nSize, a_writer);
						break;
				}
			}
			a_writer.WriteLine();
		}

		/// <summary>
		/// Writes a formatted segment.
		/// </summary>
		/// <param name="a_sValue">Field value to write.</param>
		/// <param name="a_nLen">Length of the field value.</param>
		/// <param name="a_writer">TextWriter.</param>
		private void WriteField(string a_sValue, int a_nLen, TextWriter a_writer)
		{
			a_writer.Write((Util.Left(Util.SafeString(a_sValue), a_nLen)).PadRight(a_nLen));
		}		
	}
}
