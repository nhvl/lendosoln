﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MismoToFnmaSig
{
	public abstract class Util
	{
		public static string SafeString(object a_obj)
		{
			return SafeString(a_obj, true);
		}
		public static string SafeString(object a_obj, bool a_bTrim)
		{
			try
			{
				if (Convert.IsDBNull(a_obj) || null == a_obj) return string.Empty;
				return (a_bTrim) ? Convert.ToString(a_obj).Trim() : Convert.ToString(a_obj);
			}
			catch
			{
				return string.Empty;
			}
		}
		public static int SafeInt(string a_sInput)
		{
			try
			{
				if (string.IsNullOrEmpty(a_sInput)) return 0;
				return Convert.ToInt32(a_sInput);
			}
			catch
			{
				return 0;
			}
		}
		public static int SafeInt(object a_obj)
		{
			try
			{
				if (Convert.IsDBNull(a_obj) || null == a_obj) return 0;
				return Convert.ToInt32(a_obj);
			}
			catch
			{
				return 0;
			}
		}		
		public static string Left(string a_sInput, int a_nLen)
		{
			if (string.IsNullOrEmpty(a_sInput)) return string.Empty;

			if (a_sInput.Length <= a_nLen)
				return a_sInput;
			else
				return a_sInput.Substring(0, a_nLen);
		}
		public static string Right(string a_sInput, int a_nLen)
		{
			if (string.IsNullOrEmpty(a_sInput)) return string.Empty;

			if (a_sInput.Length <= a_nLen)
				return a_sInput;
			else
				return a_sInput.Substring(a_sInput.Length - a_nLen, a_nLen);
		}

		public static string Mid(string a_sInput, int a_nStart)
		{
			if (string.IsNullOrEmpty(a_sInput) || a_nStart >= a_sInput.Length) 
				return string.Empty;
			return a_sInput.Substring(a_nStart);
		}

		public static string Mid(string a_sInput, int a_nStart, int a_nLen)
		{
			if (string.IsNullOrEmpty(a_sInput) || a_nStart >= a_sInput.Length)
				return string.Empty;

			if (a_nLen > (a_sInput.Length - a_nStart))
				return a_sInput.Substring(a_nStart);
			else
				return a_sInput.Substring(a_nStart, a_nLen);
		}		
	}

}
