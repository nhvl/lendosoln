﻿namespace LendersOfficeContinuousService
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Configure class.
    /// </summary>
    internal class Config
    {
        /// <summary>
        /// Critical Exception Set.
        /// </summary>
        private static HashSet<string> criticalExceptionSet;

        /// <summary>
        /// Tthe time for refresh data.
        /// </summary>
        private static object criticalExceptionRefreshTime = DateTime.MinValue;

        /// <summary>
        /// A lock object.
        /// </summary>
        private static object lockObj = new object();

        /// <summary>
        /// Gets Critical Exception Set.
        /// </summary>
        public static HashSet<string> CriticalExceptionSet
        {
            get
            {
                const int RefreshInMinute = 1;

                if (criticalExceptionSet == null)
                {
                    criticalExceptionRefreshTime = DateTime.Now.AddMinutes(RefreshInMinute);
                    return criticalExceptionSet = GetCriticalExceptionSet();
                }

                bool needReload = false;
                if (DateTime.Now >= (DateTime)criticalExceptionRefreshTime)
                {
                    lock (lockObj)
                    {
                        if (DateTime.Now >= (DateTime)criticalExceptionRefreshTime)
                        {
                            needReload = true;
                            criticalExceptionRefreshTime = DateTime.Now.AddMinutes(RefreshInMinute);
                        }
                    }
                }

                if (needReload)
                {
                    ConfigurationManager.RefreshSection("appSettings");
                    criticalExceptionSet = GetCriticalExceptionSet();
                }

                return criticalExceptionSet;
            }
        }

        /// <summary>
        /// Create Critical Exception Set from ConfigurationManager.AppSettings["CriticalExceptionList"].
        /// </summary>
        /// <returns>HashSet object.</returns>
        private static HashSet<string> GetCriticalExceptionSet()
        {
            string str = ConfigurationManager.AppSettings["CriticalExceptionList"];
            if (string.IsNullOrEmpty(str))
            {
                str = "System.TypeInitializationException";
            }

            return new HashSet<string>(str.Split(new char[] { ' ', '\t', '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries));
        }
    }
}
