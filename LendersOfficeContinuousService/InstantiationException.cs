﻿namespace LendersOfficeContinuousService
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Cannot create object.
    /// </summary>
    [Serializable]
    public class InstantiationException : CriticalException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InstantiationException" /> class.
        /// </summary>
        /// <param name="msg">The error message.</param>
        public InstantiationException(string msg) : base(msg)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InstantiationException" /> class.
        /// </summary>
        /// <param name="msg">The error message.</param>
        /// <param name="innerException">Do not pass empty string.</param>
        public InstantiationException(string msg, Exception innerException) : base(msg, innerException)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InstantiationException" /> class.
        /// This contructor allows exceptions to be marhalled accross remoting boundaries.
        /// </summary>
        /// <param name="info">Object to which the instance data is added.</param>
        /// <param name="context">The streaming context.</param>
        protected InstantiationException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
