﻿// <copyright file="AutoloadAppDomain.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: David Dao
//  Date:   1/4/2011 4:11:53 PM
// </summary>
namespace LendersOfficeContinuousService
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.InteropServices;
    using System.Text;
    using System.Threading;

    /// <summary>
    /// An application domain that is automatically loaded based on its dll changing.
    /// </summary>
    [SuppressMessage(category: "LendingQBStyleCop.LendingQBCustomRules", checkId: "LB1003:CannotSwallowGeneralException", Justification = "If an unknown exception happens try again before crashing.")]
    internal class AutoloadAppDomain
    {
        /// <summary>
        /// This lock used to synchronize "append logs to C:\Temp\Test.log" when calling runnables.InitializeApplicationFramework().
        /// </summary>
        private static object initLock = new object();

        /// <summary>
        /// A lock object for <see cref="isNeedReload"/>.
        /// </summary>
        private object lockObj = new object();

        /// <summary>
        /// The full name of the assembly.
        /// </summary>
        private string assemblyFullName = null;

        /// <summary>
        /// The setting for the app domain.
        /// </summary>
        private AppDomainSetting setting;

        /// <summary>
        /// Request abnormal shutdown.
        /// </summary>
        private Action abnormalShutdow;

        /// <summary>
        /// The list of running threads.
        /// </summary>
        private List<Tuple<Thread, ThreadContext>> threads;

        /// <summary>
        /// Shutdown token.
        /// </summary>
        private CancellationTokenSource shutdownTokenSource = new CancellationTokenSource();

        /// <summary>
        /// Allow to run IRunnable object.
        /// </summary>
        private ManualResetEvent proxyAvailable = new ManualResetEvent(false);

        /// <summary>
        /// The monitor thread is used to refresh IRunnable lifetime and reload appDomain.
        /// </summary>
        private Thread monitorThread = null;

        /// <summary>
        /// FileSystemWatcher's reported time.
        /// </summary>
        private object modifiedDate = DateTime.MinValue; //// Using object because reads/writes of reference type are atomic

        /// <summary>
        /// RunnableContext object.
        /// </summary>
        private RunnableContext runnableContext = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="AutoloadAppDomain" /> class.
        /// </summary>
        /// <param name="setting">The configuration settings for the app domain.</param>
        /// <param name="abnormalShutdow">Run this action for abnormallly shutdown.</param>
        public AutoloadAppDomain(AppDomainSetting setting, Action abnormalShutdow)
        {
            this.setting = setting;
            this.abnormalShutdow = abnormalShutdow;

            FileSystemWatcher watcher = new FileSystemWatcher(setting.ApplicationBase);
            watcher.Changed += new FileSystemEventHandler(this.Watcher_Changed);
            watcher.Renamed += new RenamedEventHandler(this.Watcher_Renamed);
            watcher.NotifyFilter = NotifyFilters.LastWrite;
            watcher.EnableRaisingEvents = true;
        }

        /// <summary>
        /// Gets the full path for the dll used by this app domain.
        /// </summary>
        public string LibraryFullPath
        {
            get { return this.setting.LibraryFullPath; }
        }

        /// <summary>
        /// Gets a user friendly description for the current runnable.
        /// </summary>
        public string ApplicationName
        {
            get { return this.setting.ApplicationName; }
        }

        /// <summary>
        /// Create a monitor thread and "runnable" threads.
        /// </summary>
        public void Start()
        {
            if (this.threads != null)
            {
                return;
            }

            this.threads = new List<Tuple<Thread, ThreadContext>>();
            for (int i = 0; i < this.setting.LibraryList.Count; i++)
            {
                var item = this.setting.LibraryList[i];
                for (int count = 0; count < item.NumThreads; count++)
                {
                    var threadContext = new ThreadContext(item, this.threads.Count /*runableIdx*/, new ManualResetEvent(true)); 
                    Thread thread = new Thread(() => this.Execute(threadContext));
                    this.threads.Add(Tuple.Create(thread, threadContext));
                }
            }

            this.monitorThread = new Thread(() => this.MonitorProcess());
            this.monitorThread.Start();

            foreach (var threadInfo in this.threads)
            {
                threadInfo.Item1.Start();
            }
        }

        /// <summary>
        /// Shut down this AutoloadAppDomain object by ending all its threads.
        /// </summary>
        public void PrepareToshutdown()
        {
            this.shutdownTokenSource.Cancel();
            this.TriggerUnload();
        }

        /// <summary>
        /// Prepare to shut down this AutoloadAppDomain object.
        /// </summary>
        /// <param name="startTime">The input started time.</param>
        public void Shutdown(Stopwatch startTime)
        {
            this.PrepareToshutdown(); //// for safety

            if (this.threads == null)
            {
                return;
            }

            bool needThreadAbort = false;
            foreach (var threadInfo in this.threads)
            {
                var thread = threadInfo.Item1;
                long remain = Settings.ShutdownTimeoutMs - startTime.ElapsedMilliseconds;
                bool abortFlag = remain <= 0 && thread.ThreadState != System.Threading.ThreadState.Stopped && thread.ThreadState != System.Threading.ThreadState.Unstarted;
                if (abortFlag || (remain > 0 && !thread.Join((int)remain)))
                {
                    thread.Abort();
                    needThreadAbort = true;
                }
            }

            if (this.monitorThread != null && this.monitorThread.ThreadState != System.Threading.ThreadState.Stopped && this.monitorThread.ThreadState != System.Threading.ThreadState.Unstarted)
            {
                long remain = Settings.ShutdownTimeoutMs + 2000 /* extra time for monitor thread's unloading appDomain*/ - startTime.ElapsedMilliseconds;
                if (remain <= 0 || !this.monitorThread.Join((int)remain))
                {
                    this.monitorThread.Abort();
                    needThreadAbort = true;
                }
            }

            System.Diagnostics.Trace.WriteLine(this.setting.ApplicationName + $" - Stop {startTime.ElapsedMilliseconds:N0} ms *** needThreadAbort: {needThreadAbort} ", "INFO");
        }

        /// <summary>
        /// Check whether the input exception/ its descendant inner exception belongs critical exception list.
        /// </summary>
        /// <param name="exc">The exception.</param>
        /// <returns>Return true if it belongs critical exception.</returns>
        private static bool IsCriticalException(Exception exc)
        {
            try
            {
                HashSet<string> criticalException = Config.CriticalExceptionSet;
                while (exc != null)
                {
                    if (criticalException.Contains(exc.GetType().FullName))
                    {
                        return true;
                    }

                    exc = exc.InnerException;
                }
            }
            catch (Exception otherExc)
            {
                System.Diagnostics.Trace.WriteLine("IsCriticalException() error: " + otherExc + "\r\n" + otherExc.StackTrace, "ERROR");
            }

            return false;
        }

        /// <summary>
        /// Assembly Resolve Event Handler.
        /// </summary>
        /// <param name="source">The source of the event.</param>
        /// <param name="e">The event data.</param>
        /// <returns>The assembly that resolves the type, assembly, or resource; or null if the assembly cannot be resolved.</returns>
        private static Assembly ResolveEventHandler(object source, ResolveEventArgs e)
        {
            if (e.Name == "ConversationLogLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null")
            {
                Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
                foreach (Assembly assembly in assemblies)
                {
                    if (e.Name == assembly.FullName)
                    {
                        return assembly;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// The monitor thread is used to refresh "remote object" lifetime.
        /// </summary>
        private void MonitorProcess()
        {
            try
            {
                do
                {
                    if (this.shutdownTokenSource.Token.IsCancellationRequested)
                    {
                        break;
                    }

                    var runnableCtx = this.runnableContext;

                    if (runnableCtx != null)
                    {
                        runnableCtx.Touch();
                    }

                    DateTime lastModify = (DateTime)this.modifiedDate;
                    if (lastModify.AddSeconds(30) > DateTime.Now)
                    {
                        // data is not old enough
                        continue;
                    }

                    if (runnableCtx != null)
                    {
                        if (!runnableCtx.StopReq)
                        {
                            continue;
                        }

                        this.WaitAllThreadIdle(returnIfShutdown: true);
                        if (this.shutdownTokenSource.IsCancellationRequested)
                        {
                            break;
                        }

                        this.RemoveRunnableContext(runnableCtx);
                    }

                    RunnableContext newRunnableContext = null;

                    try
                    {
                        newRunnableContext = this.CreateRunnableContext(); //// return null if shutdownTokenSource.IsCancellationRequested == true
                    }
                    catch (Exception exc1)
                    {
                        System.Diagnostics.Trace.WriteLine("MonitorProcess for : " + this.setting.ApplicationName + " - " + exc1 + "\r\n" + exc1.StackTrace, "ERROR");
                        this.abnormalShutdow();
                        return;
                    }

                    lock (this.lockObj)
                    {
                        this.runnableContext = newRunnableContext;
                        if (this.shutdownTokenSource.IsCancellationRequested)
                        {
                            break;
                        }

                        this.runnableContext.Touch();
                        this.proxyAvailable.Set();
                    }
                }
                while (this.shutdownTokenSource.Token.WaitHandle.WaitOne(10 * 1000) == false);

                //// last unload appDomain

                if (this.runnableContext != null)
                {
                    this.WaitAllThreadIdle(returnIfShutdown: false);
                    this.runnableContext.UnloadAppDomain();
                }
            }
            catch (ThreadAbortException)
            {
                throw;
            }
            catch (Exception exc)
            {
                System.Diagnostics.Trace.WriteLine("MonitorProcess for : " + this.setting.ApplicationName + " - " + exc + "\r\n" + exc.StackTrace, "ERROR");
                this.abnormalShutdow();
            }
        }

        /// <summary>
        /// Wait idle for all "runnable" thread.
        /// </summary>
        /// <param name="returnIfShutdown">Stop to wait if shutdownToken is requested.</param>
        /// <returns>True if all idle.</returns>
        private bool WaitAllThreadIdle(bool returnIfShutdown)
        {
            if (!returnIfShutdown)
            {
                var handles = this.threads.Select(i => i.Item2.IdleEvent).ToArray();
                return WaitHandle.WaitAll(handles, Settings.ShutdownTimeoutMs);
            }

            WaitHandle[] waitHandles = { this.shutdownTokenSource.Token.WaitHandle, null };

            Stopwatch startTime = Stopwatch.StartNew();
            foreach (var threadInfo in this.threads)
            {
                long remain = Settings.ShutdownTimeoutMs - startTime.ElapsedMilliseconds;
                if (remain <= 0)
                {
                    return false;
                }

                ThreadContext threadContext = threadInfo.Item2;
                waitHandles[1] = threadContext.IdleEvent;
                int waitIdx = WaitHandle.WaitAny(waitHandles, (int)remain);
                if (waitIdx != 1)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Executes the runnable for the app domain.
        /// </summary>
        /// <param name="threadContext">ThreadContext object.</param>
        private void Execute(ThreadContext threadContext)
        {
            WaitHandle[] waitHandles = 
            {
                this.shutdownTokenSource.Token.WaitHandle,
                this.proxyAvailable
            };

            try
            {
                int runableIdx = threadContext.RunableIdx, pauseIntervalMs = threadContext.SettingItem.PauseIntervalMs;
                while (true)
                {
                    if (WaitHandle.WaitAny(waitHandles) == 0)
                    {
                        return;
                    }

                    RunnableContext context = null;
                    lock (this.lockObj)
                    {
                        context = this.runnableContext;
                    }

                    bool needUnload = this.setting.IsAlwaysUnload;
                    try
                    {
                        threadContext.IdleEvent.Reset();
                        if (context != null && !context.StopReq && !this.shutdownTokenSource.IsCancellationRequested)
                        {
                            context.Runnables.Run(runableIdx);
                        }                        
                    }
                    catch (ThreadAbortException)
                    {
                        throw;
                    }
                    catch (CriticalException)
                    {
                        throw;
                    }
                    catch (Exception exc)
                    {
                        System.Diagnostics.Trace.WriteLine(this.setting.ApplicationName + " - " + exc + "\r\n" + exc.StackTrace, "ERROR");
                        if (IsCriticalException(exc))
                        {
                            throw new CriticalException("Run(...) error", exc);
                        }

                        //// try to re-use current proxy if exception was not RemotingException/AppDomainUnloadedException
                        if (exc is System.Runtime.Remoting.RemotingException || exc is AppDomainUnloadedException)
                        {
                            needUnload = true;
                        }
                    }
                    finally
                    {
                        threadContext.IdleEvent.Set();
                    }
                    
                    if (needUnload)
                    {
                        lock (this.lockObj)
                        {
                            if (context != null && object.ReferenceEquals(context, this.runnableContext))
                            {
                                this.TriggerUnload();
                            }
                        }

                        if (this.setting.IsAlwaysUnload)
                        {
                            System.Diagnostics.Trace.WriteLine(this.setting.ApplicationName + " - Force Unload");
                        }
                    }

                    this.shutdownTokenSource.Token.WaitHandle.WaitOne(pauseIntervalMs);
                }
            }
            catch (ThreadAbortException)
            {
                throw;
            }
            catch (CriticalException)
            {
                this.abnormalShutdow();
            }
        }

        /// <summary>
        /// Create RunnableContext.
        /// </summary>
        /// <returns>RunnableContext object. Note: return null if shutdownTokenSource.IsCancellationRequested = true.</returns>
        private RunnableContext CreateRunnableContext()
        {
            int numberOfTries = 5;
            AppDomain appDomain = null;
            for (int i = 0; i < numberOfTries; i++)
            {
                try
                {
                    if (appDomain != null)
                    {
                        AppDomain.Unload(appDomain);
                    }
                }
                catch (Exception)
                {
                }

                appDomain = null;

                try
                {
                    var proxyRunnablesType = typeof(ProxyRunnables);
                    AppDomainSetup setup = new AppDomainSetup();
                    setup.ShadowCopyFiles = "true";
                    setup.ApplicationName = this.setting.ApplicationName;
                    setup.ApplicationBase = AppDomain.CurrentDomain.SetupInformation.ApplicationBase;
                    setup.ConfigurationFile = this.setting.ConfigurationFile;

                    appDomain = AppDomain.CreateDomain(this.setting.ApplicationName, AppDomain.CurrentDomain.Evidence, setup);
                    var runnables = (ProxyRunnables)appDomain.CreateInstanceAndUnwrap(
                        proxyRunnablesType.Assembly.FullName, 
                        proxyRunnablesType.FullName ?? string.Empty);
                    runnables.SetSourceAssembly(this.setting.LibraryFullPath);

                    appDomain.AssemblyResolve += ResolveEventHandler;

                    lock (initLock)
                    {
                        runnables.InitializeApplicationFramework();
                    }

                    foreach (var item in this.setting.LibraryList)
                    {
                        for (int count = 0; count < item.NumThreads; count++)
                        {
                            runnables.RegisterAndGetIndexFor(item.ItemType);
                        }
                    }

                    System.Diagnostics.Trace.WriteLine("Load " + this.setting.ApplicationName + " successfully.");

                    return new RunnableContext(appDomain, runnables);
                }
                catch (InvalidOperationException)
                {
                    System.Diagnostics.Trace.WriteLine("Failed to Load " + this.setting.ApplicationName);
                    throw;
                }
                catch (ArgumentException)
                {
                    System.Diagnostics.Trace.WriteLine("Failed to Load runnable " + this.setting.ApplicationName);
                    throw;
                }
                catch (ThreadAbortException)
                {
                    throw; // Someone try to stop the thread. Let it bubble up.
                }
                catch (OutOfMemoryException)
                {
                    // 5/27/2015 dd - Let the services error out.
                    string debugInfo = "Unable to load " + this.setting.ApplicationName + Environment.NewLine;
                    debugInfo += "ApplicationBase=" + this.setting.ApplicationBase + Environment.NewLine;
                    debugInfo += "AssemblyFulName=" + this.assemblyFullName + Environment.NewLine;
                    debugInfo += "LibraryFullPath=" + this.setting.LibraryFullPath + Environment.NewLine;
                    System.Diagnostics.Trace.WriteLine(debugInfo, "ERROR");
                    throw;
                }
                catch (Exception exc)
                {
                    string debugInfo = "Unable to load " + this.setting.ApplicationName + Environment.NewLine;
                    debugInfo += "ApplicationBase=" + this.setting.ApplicationBase + Environment.NewLine;
                    debugInfo += "AssemblyFulName=" + this.assemblyFullName + Environment.NewLine;
                    debugInfo += "LibraryFullPath=" + this.setting.LibraryFullPath + Environment.NewLine;

                    if (i == numberOfTries - 1)
                    {
                        System.Diagnostics.Trace.WriteLine(debugInfo + exc.ToString());
                        throw;
                    }
                    else
                    {
                        System.Diagnostics.Trace.WriteLine("Tries #" + i + ": " + debugInfo + exc.ToString(), "WARN");

                        //// Sleep for 5 seconds and then resume.
                        if (this.shutdownTokenSource.Token.WaitHandle.WaitOne(5000))  
                        {
                            return null;
                        }
                    }
                }
            }

            throw new Exception("LoadAppDomain. Should never get down here");
        }

        /// <summary>
        /// Unloads the domain and tries 5 times if the unload is failing.
        /// </summary>
        /// <param name="context">Runnable object.</param>
        private void RemoveRunnableContext(RunnableContext context)
        {
            if (context == null)
            {
                return;
            }

            int numberOfTries = 5;

            for (int i = 0; i < numberOfTries; i++)
            {
                try
                {
                    context.UnloadAppDomain();
                    return;
                }
                catch (Exception exc)
                {
                    if (i == numberOfTries - 1)
                    {
                        System.Diagnostics.Trace.WriteLine(exc.ToString(), "ERROR");
                        throw;
                    }
                    else
                    {
                        System.Diagnostics.Trace.WriteLine("Tries #" + i + ": " + exc.ToString(), "ERROR");
                        if (this.shutdownTokenSource.Token.WaitHandle.WaitOne(5000))
                        {
                            return;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Sets the domain reload bit when called.
        /// </summary>
        /// <param name="sender">The object that triggered the event.</param>
        /// <param name="e">The arguments for the change event.</param>
        private void Watcher_Changed(object sender, FileSystemEventArgs e)
        {
            if (this.shutdownTokenSource.IsCancellationRequested)
            {
                return;
            }

            this.modifiedDate = DateTime.Now;
            this.TriggerUnload();
        }

        /// <summary>
        /// Sets the domain reload bit to trigger a reload after the dll has changed.
        /// </summary>
        /// <param name="sender">The object that triggered the event.</param>
        /// <param name="e">The event arguments for the event.</param>
        private void Watcher_Renamed(object sender, RenamedEventArgs e)
        {
            if (this.shutdownTokenSource.IsCancellationRequested)
            {
                return;
            }

            System.Diagnostics.Trace.WriteLine("File renamed: {e.Name}", "INFO");
            this.modifiedDate = DateTime.Now;
            this.TriggerUnload();
        }

        /// <summary>
        /// Flags the domain for reload.
        /// </summary>
        private void TriggerUnload()
        {
            lock (this.lockObj)
            {
                if (this.runnableContext != null)
                {
                    this.runnableContext.Stop();
                    this.proxyAvailable.Reset();
                }
            }
        }

        /// <summary>
        /// The fixed context for "IRunnable" thread.
        /// </summary>
        private class ThreadContext
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="ThreadContext" /> class.
            /// </summary>
            /// <param name="settingItem">Runnable setting item.</param>
            /// <param name="runableIdx">The index of the IRunnable to run.</param>
            /// <param name="idleEvent">Thread's idle status.</param>
            public ThreadContext(LibrarySettingItem settingItem, int runableIdx, ManualResetEvent idleEvent)
            {
                this.SettingItem = settingItem;
                this.RunableIdx = runableIdx;
                this.IdleEvent = idleEvent;
            }

            /// <summary>
            /// Gets/Sets runnable setting item.
            /// </summary>
            public LibrarySettingItem SettingItem { get; private set; }

            /// <summary>
            /// Gets/Sets the index of the IRunnable to run.
            /// </summary>
            public int RunableIdx { get; private set; }

            /// <summary>
            /// Gets/sets the thread idle status.
            /// </summary>
            public ManualResetEvent IdleEvent { get; private set; }
        }

        /// <summary>
        /// Wrapper for AppDomain and IRunnable objects.
        /// </summary>
        private class RunnableContext
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="RunnableContext" /> class.
            /// </summary>
            /// <param name="appDomain">AppDomain object.</param>
            /// <param name="proxy">ProxyRunnables object.</param>
            public RunnableContext(AppDomain appDomain, ProxyRunnables proxy)
            {
                this.CurrentAppDomain = appDomain;
                this.Runnables = proxy;
            }

            /// <summary>
            /// Gets or sets application domain.
            /// </summary>
            public AppDomain CurrentAppDomain { get; set; }

            /// <summary>
            /// Gets or sets ProxyRunnables object.
            /// </summary>
            public ProxyRunnables Runnables { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether stop request received.
            /// </summary>
            public bool StopReq { get; set; }

            /// <summary>
            /// Request to stop.
            /// </summary>
            public void Stop()
            {
                try
                {
                    if (!this.StopReq)
                    {
                        this.StopReq = true;
                        if (this.Runnables != null && this.CurrentAppDomain != null)
                        {
                            this.Runnables.Stop();
                        }
                    }
                }
                catch (Exception)
                {
                }
            }

            /// <summary>
            /// Unload AppDomain.
            /// </summary>
            public void UnloadAppDomain()
            {
                if (this.CurrentAppDomain != null)
                {
                    AppDomain.Unload(this.CurrentAppDomain);
                    this.CurrentAppDomain = null;
                }
            }

            /// <summary>
            /// This method is empty. Call it to refresh its lifetime.
            /// </summary>
            public void Touch()
            {
                try
                {
                    if (this.Runnables != null)
                    {
                        this.Runnables.Touch();
                    }
                }
                catch (Exception)
                {
                }
            }
        }
    }
}
