﻿// <copyright file="Settings.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: David Dao
//  Date:   1/4/2011 4:11:53 PM
// </summary>
namespace LendersOfficeContinuousService
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Xml;

    /// <summary>
    /// Gives access to the configuration settings for the service.
    /// </summary>
    internal class Settings
    {
        /// <summary>
        /// The library location.
        /// </summary>
        private static string filePath;

        /// <summary>
        /// The list of <see cref="IRunnable"/> the service will run.
        /// </summary>
        private static List<AppDomainSetting> list;

        /// <summary>
        /// Initializes static members of the <see cref="Settings" /> class.
        /// </summary>
        static Settings()
        {
            Settings.filePath = AppDomain.CurrentDomain.BaseDirectory + @"\" + ConfigurationManager.AppSettings["library"];
            Settings.Load();
        }

        /// <summary>
        /// Gets a set of <see cref="IRunnable"/> the service will run.
        /// </summary>
        public static IEnumerable<AppDomainSetting> AppDomainSettingList
        {
            get { return Settings.list; }
        }

        /// <summary>
        /// Gets/sets shutdown timeout in ms.
        /// </summary>
        public static int ShutdownTimeoutMs { get; private set; }

        /// <summary>
        /// Parses and loads the library list xml.
        /// </summary>
        private static void Load()
        {
            int shutdownTimeout = -1;
            if (int.TryParse(ConfigurationManager.AppSettings["ShutdownTimeoutMs"], out shutdownTimeout) == false || shutdownTimeout <= 0)
            {
                shutdownTimeout = 30000; // Default.
            }

            Settings.ShutdownTimeoutMs = shutdownTimeout;

            XmlDocument document = new XmlDocument();
            document.Load(Settings.filePath);
            Settings.list = new List<AppDomainSetting>();

            foreach (XmlElement el in document.SelectNodes("//libraries/library"))
            {
                int ms = -1;

                if (int.TryParse(el.GetAttribute("pause_interval_ms"), out ms) == false || ms <= 0)
                {
                    ms = 5000; // Default.
                }

                int numThreads = 1;
                if (int.TryParse(el.GetAttribute("num_of_threads"), out numThreads) == false || numThreads < 1)
                {
                    numThreads = 1; // Default.
                }

                var items = new List<LibrarySettingItem>();
                items.Add(new LibrarySettingItem(el.GetAttribute("type"), ms, numThreads));

                Settings.list.Add(new AppDomainSetting(el.GetAttribute("path"), el.GetAttribute("always_unload"), items));
            }

            foreach (XmlElement el in document.SelectNodes("//libraries/appDomain"))
            {
                var items = new List<LibrarySettingItem>();
                foreach (XmlElement item in el.SelectNodes("item"))
                {
                    int ms = -1;

                    if (int.TryParse(item.GetAttribute("pause_interval_ms"), out ms) == false)
                    {
                        ms = 5000; // Default.
                    }

                    if (ms <= 0)
                    {
                        ms = 5000;
                    }

                    int numThreads = 1;
                    if (int.TryParse(item.GetAttribute("num_of_threads"), out numThreads) == false || numThreads < 1)
                    {
                        numThreads = 1; // Default.
                    }

                    items.Add(new LibrarySettingItem(item.GetAttribute("type"), ms, numThreads));
                }

                if (items.Count > 0)
                {
                    Settings.list.Add(new AppDomainSetting(el.GetAttribute("path"), "false", items));
                }
            }
        }
    }
}
