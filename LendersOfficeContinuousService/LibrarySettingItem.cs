﻿// <copyright file="LibrarySettingItem.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: David Dao
//  Date:   1/4/2011 4:11:53 PM
// </summary>
namespace LendersOfficeContinuousService
{
    using System;
    using System.IO;

    /// <summary>
    /// Represents an entry in the settings.config.
    /// </summary>
    internal class LibrarySettingItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LibrarySettingItem" /> class.
        /// </summary>
        /// <param name="itemType">The type that will be loaded and ran.</param>
        /// <param name="pauseIntervalMs">The time in milliseconds in between execution.</param>
        /// <param name="numThreads">Count how many threads those run this item.</param>
        public LibrarySettingItem(string itemType, int pauseIntervalMs, int numThreads)
        {
            if (string.IsNullOrEmpty(itemType))
            {
                throw new ArgumentException("ItemType cannot be null or empty.");
            }

            this.ItemType = itemType;
            this.PauseIntervalMs = pauseIntervalMs;
            this.NumThreads = numThreads;
        }

        /// <summary>
        /// Gets the full namespace and type of the code that will be executed.
        /// </summary>
        /// <value>The full namespace and type of the <see cref="IRunnable"/>.</value>
        public string ItemType { get; private set; }

        /// <summary>
        /// Gets the amount of time to wait in milliseconds to wait before executing the code again.
        /// </summary>
        /// <value>The time in milliseconds to wait before executions.</value>
        public int PauseIntervalMs { get; private set; }

        /// <summary>
        /// Gets number of threads those use this item.
        /// </summary>
        public int NumThreads { get; private set; }
    }
}
