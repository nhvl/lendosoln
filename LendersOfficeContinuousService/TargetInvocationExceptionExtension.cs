﻿namespace LendersOfficeContinuousService
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Class containing extension method to log inner exception of reflection <see cref="TargetInvocationException"/>.
    /// </summary>
    internal static class TargetInvocationExceptionExtension
    {
        /// <summary>
        /// Logs the inner exception message of <see cref="TargetInvocationException"/>.
        /// </summary>
        /// <param name="exception">The <see cref="TargetInvocationException"/> thrown by application.</param>
        /// <returns>The inner exception message of throwing <see cref="TargetInvocationException"/>.</returns>
        public static string GetMessageForLogs(this System.Reflection.TargetInvocationException exception)
        {
            return (exception.InnerException ?? exception).Message;
        }
    }
}
