﻿// <copyright file="ProxyRunnables.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Antonio Valencia
//  Date:   6/22/2016
// </summary>
namespace LendersOfficeContinuousService
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.Remoting.Lifetime;
    using System.Threading;
    using ServiceExecutionLogging;

    /// <summary>
    /// Responsible for loading IRunnables from a given DLL. The object will then be able to 
    /// execute the Run method for each one by using reflection.
    /// </summary>
    public class ProxyRunnables : MarshalByRefObject
    {
        /// <summary>
        /// The IRunnable interface name. 
        /// </summary>
        private const string RequiredInterfaceName = "IRunnable";

        /// <summary>
        /// The IRunnableEx interface name. 
        /// </summary>
        private const string RequiredCancellableInterfaceName = "ICancellableRunnable";

        /// <summary>
        /// The name of the Run method.
        /// </summary>
        private const string RunMethodName = "Run";

        /// <summary>
        /// Request to stop all runable items.
        /// </summary>
        private static CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

        /// <summary>
        /// Registered types. 
        /// </summary>
        private List<Type> runnableTypes;

        /// <summary>
        /// The set of loaded IRunnables. 
        /// </summary>
        private List<object> runnableObjects;

        /// <summary>
        /// The set of loaded IRunnables. 
        /// </summary>
        private List<bool> initThreadStatuses;

        /// <summary>
        /// The assembly the IRunnables exist in.
        /// </summary>
        private Assembly targetAssembly;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProxyRunnables" /> class.
        /// </summary>
        public ProxyRunnables()
        {
            this.runnableTypes = new List<Type>();
            this.runnableObjects = new List<object>();
            this.initThreadStatuses = new List<bool>();
        }

        /// <summary>
        /// Will attempt to look for application initializer and invoke the method.
        /// </summary>
        public void InitializeApplicationFramework()
        {
            if (this.targetAssembly == null)
            {
                throw new InvalidOperationException("Assembly has not been loaded.");
            }

            var type = this.targetAssembly.GetType("LendersOffice.ContinuousServiceApplicationInitializer");

            if (type != null)
            {
                var method = type.GetMethod("InitializeApplicationFramework", BindingFlags.Public | BindingFlags.Static);
                if (method != null)
                {
                    method.Invoke(null, null);
                }
            }
        }

        /// <summary>
        /// Sets the assembly where the IRunnables will be loaded from.
        /// </summary>
        /// <param name="path">The path of the DLL.</param>
        public void SetSourceAssembly(string path)
        {
            this.targetAssembly = Assembly.LoadFrom(path);
        }

        /// <summary>
        /// Loads a IRunnable with the given name and returns its id for execution.
        /// </summary>
        /// <param name="typeName">The name of the type to load.</param>
        /// <returns>The index of the runnable or -1 if it could not be loaded.</returns>
        public int RegisterAndGetIndexFor(string typeName)
        {
            if (this.targetAssembly == null)
            {
                throw new InvalidOperationException("Assembly has not been loaded.");
            }

            var type = this.targetAssembly.GetType(typeName);

            if (!IsLoadableModule(type))
            {
                throw new ArgumentException("Could not load " + typeName);
            }

            this.runnableTypes.Add(type);
            this.runnableObjects.Add(Activator.CreateInstance(type));
            this.initThreadStatuses.Add(false);
            return this.runnableTypes.Count - 1;
        }

        /// <summary>
        /// Triggers the run method on the IRunnable.
        /// </summary>
        /// <param name="index">The index of the IRunnable to run.</param>
        public void Run(int index)
        {
            var itemType = this.runnableTypes[index];
            var item = this.runnableObjects[index];

            if (item == null)
            {
                try
                {
                    item = Activator.CreateInstance(itemType);
                }
                catch (Exception innerException)
                {
                    throw new InstantiationException($"Cannot create {itemType.Name} object", innerException);
                }
            }
            else
            {
                this.runnableObjects[index] = null;
            }

            var threadId = System.Threading.Thread.CurrentThread?.ManagedThreadId ?? 0; // Unlikely for the current thread to be null, but just in case
            var logger = new Logger(itemType.Name);
            var stopwatch = Stopwatch.StartNew();

            try
            {
                if (!this.initThreadStatuses[index])
                {
                    this.InitializeModuleName(itemType.FullName);
                }

                logger.Record(Logger.StatusType.STARTED, $"{Environment.MachineName} - Thread ID {threadId}");

                bool supportCancelToken = itemType.GetTypeInfo().ImplementedInterfaces.Any(p => p.Name.Equals(RequiredCancellableInterfaceName));

                if (supportCancelToken)
                {
                    var methodInfo = itemType.GetMethod(RunMethodName, new Type[] { typeof(CancellationToken) });
                    methodInfo.Invoke(item, new object[] { cancellationTokenSource.Token });
                }
                else
                {
                    var methodInfo = itemType.GetMethod(RunMethodName, new Type[] { });
                    methodInfo.Invoke(item, new object[] { });
                }

                logger.Record(Logger.StatusType.OK, $"{Environment.MachineName} - Thread ID {threadId} - Executed in {stopwatch.ElapsedMilliseconds} ms.");
                this.initThreadStatuses[index] = true;
            }
            catch (TargetInvocationException exc)
            {
                logger.Record(Logger.StatusType.FAILED, $"{Environment.MachineName} - Thread ID {threadId} - {exc.GetMessageForLogs()}");
                throw;
            }
            catch (Exception exc)
            {
                logger.Record(Logger.StatusType.FAILED, $"{Environment.MachineName} - Thread ID {threadId} - {exc.Message}");
                throw;
            }
        }

        /// <summary>
        /// Request to stop IRunnable object.
        /// </summary>
        public void Stop()
        {
            System.Diagnostics.Trace.WriteLine("Runnable: cancellationTokenSource.Cancel()");
            cancellationTokenSource.Cancel();
        }

        /// <summary>
        /// Call this empty method to renew its lifetime in case it is "remote object".
        /// </summary>
        public void Touch()
        {
        }

        /// <summary>
        /// Obtains a lifetime service object to control the lifetime policy for this instance. 
        /// Reference: http://zaidox.com/net-remote-object-lifetime.html .
        /// </summary>
        /// <returns>
        /// An object of type <see cref="ILease" /> used to control the lifetime 
        /// policy for this instance. 
        /// </returns>
        public override object InitializeLifetimeService()
        {
            var lease = (ILease)base.InitializeLifetimeService();

            if (lease.CurrentState == LeaseState.Initial)
            {
                lease.InitialLeaseTime = TimeSpan.FromMinutes(8);
                lease.SponsorshipTimeout = TimeSpan.FromMinutes(5);
                lease.RenewOnCallTime = TimeSpan.FromSeconds(30); 
            }

            return lease;
        }

        /// <summary>
        /// Checks whether a type can be instantiated for loading.
        /// </summary>
        /// <param name="type">The type to check.</param>
        /// <returns>True if the type can be created false otherwise.</returns>
        private static bool IsLoadableModule(Type type)
        {
            var typeInfo = type.GetTypeInfo();
            return !typeInfo.IsAbstract
                && !typeInfo.IsInterface
                && typeInfo.ImplementedInterfaces.Any(p => p.Name.Equals(RequiredInterfaceName) || p.Name.Equals(RequiredCancellableInterfaceName))
                && typeInfo.DeclaredConstructors.Any(c => !c.IsStatic && c.IsPublic && c.GetParameters().Length == 0);
        }

        /// <summary>
        /// Customize module name for log.
        /// </summary>
        /// <param name="moduleName">Module name.</param>
        private void InitializeModuleName(string moduleName)
        {
            if (this.targetAssembly == null)
            {
                throw new InvalidOperationException("Assembly has not been loaded.");
            }

            var type = this.targetAssembly.GetType("LendersOffice.ObjLib.Logging.ModuleNameContext");

            if (type != null)
            {
                var method = type.GetMethod("SetModuleName", BindingFlags.Public | BindingFlags.Static, null, new Type[] { typeof(string) }, null);
                if (method != null)
                {
                    method.Invoke(null, new object[] { moduleName });
                }
            }
        }
    }
}
