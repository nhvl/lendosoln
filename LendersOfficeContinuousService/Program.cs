﻿// <copyright file="Program.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: David Dao
//  Date:   1/4/2011 4:11:53 PM
// </summary>
namespace LendersOfficeContinuousService
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceProcess;
    using System.Text;

    /// <summary>
    /// The main entry point class.
    /// </summary>
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        public static void Main()
        {
            ServiceBase[] servicesToRun = { new LendersOfficeContinuousService() };
            ServiceBase.Run(servicesToRun);
        }
    }
}
