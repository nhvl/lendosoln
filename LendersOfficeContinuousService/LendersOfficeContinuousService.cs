﻿// <copyright file="LendersOfficeContinuousService.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: David Dao
//  Date:   1/4/2011 4:11:53 PM
// </summary>
namespace LendersOfficeContinuousService
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.ServiceProcess;
    using System.Threading;
    using System.Threading.Tasks;
    using Newtonsoft.Json;

    /// <summary>
    /// The main service class.
    /// </summary>
    [SuppressMessage(category: "LendingQBStyleCop.LendingQBCustomRules", checkId: "LB1003:CannotSwallowGeneralException", Justification = "Cannot allow the service to crash.")]
    public partial class LendersOfficeContinuousService : ServiceBase
    {
        /// <summary>
        /// The list of app domains that are currently executing.
        /// </summary>
        private List<AutoloadAppDomain> loadedAppDomains;

        /// <summary>
        /// Shutdown token.
        /// </summary>
        private CancellationTokenSource shutdownServiceToken = new CancellationTokenSource();

        /// <summary>
        /// Did application finish to initilize data for OnStart().
        /// </summary>
        private ManualResetEvent initEvent = new ManualResetEvent(false);

        /// <summary>
        /// Abnormal shutdown or normal shutdown.
        /// </summary>
        private bool? isAbnornalShutdown = null;

        /// <summary>
        /// Lock object.
        /// </summary>
        private object lockObj = new object();

        /// <summary>
        /// The monitor thread is used to refresh IRunnable lifetime and reload appDomain.
        /// </summary>
        private Thread shutdownThread = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="LendersOfficeContinuousService" /> class.
        /// </summary>
        public LendersOfficeContinuousService()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Starts the service.
        /// </summary>
        /// <param name="args">The arguments passed into the service.</param>
        protected override void OnStart(string[] args)
        {
            try
            {
                System.Diagnostics.Trace.WriteLine("LendersOfficeContinuousService is starting.");

                this.loadedAppDomains = new List<AutoloadAppDomain>();

                AppDomain.MonitoringIsEnabled = true;

                foreach (var setting in Settings.AppDomainSettingList)
                {
                    if (this.shutdownServiceToken.IsCancellationRequested)
                    {
                        break;
                    }

                    try
                    {
                        AutoloadAppDomain appDomain = new AutoloadAppDomain(setting, this.AbnormalShutdown);

                        appDomain.Start();
                        this.loadedAppDomains.Add(appDomain);

                        System.Diagnostics.Trace.WriteLine("Started " + setting.ApplicationName);
                    }
                    catch (Exception exc)
                    {
                        System.Diagnostics.Trace.WriteLine("(OnStart exception and abnormal exit) Unable to instantiate " + setting.ApplicationName + " in " + setting.LibraryFullPath + Environment.NewLine + exc + Environment.NewLine + exc.StackTrace, "ERROR");
                        this.AbnormalShutdown();  // opm 473879: Force restart service when a Runnable is failed
                        break;
                    }
                }
            }
            finally
            {
                this.shutdownThread = new Thread(() => this.StopProcess());
                this.shutdownThread.Start();

                this.initEvent.Set();
            }
        }

        /// <summary>
        /// Stops the service by ending all threads and stopping the listener.
        /// </summary>
        protected override void OnStop()
        {
            lock (this.lockObj)
            {
                if (!this.isAbnornalShutdown.HasValue)
                {
                    this.isAbnornalShutdown = false;
                }
            }

            this.shutdownServiceToken.Cancel();
            this.initEvent.WaitOne(2 * 60 * 1000);
            if (this.shutdownThread != null)
            {
                this.shutdownThread.Join();
            }
        }

        /// <summary>
        /// Stop process.
        /// </summary>
        protected void StopProcess()
        {
            this.shutdownServiceToken.Token.WaitHandle.WaitOne();

            Trace.WriteLine("LendersOfficeContinuousService is stopping");

            foreach (var appDomain in this.loadedAppDomains)
            {
                appDomain.PrepareToshutdown();
            }

            Stopwatch startTime = Stopwatch.StartNew();
            foreach (var appDomain in this.loadedAppDomains)
            {
                appDomain.Shutdown(startTime);
            }

            if (this.isAbnornalShutdown.Value)
            {
                Trace.WriteLine("LendersOfficeContinuousService abnormal shutdown.");
                this.ExitCode = 1;
                throw new Exception("Abnormal shutdown");
            }
        }

        /// <summary>
        /// Request Abnormal Shutdown.
        /// </summary>
        private void AbnormalShutdown()
        {
            if (!this.shutdownServiceToken.IsCancellationRequested)
            {
                lock (this.lockObj)
                {
                    if (!this.isAbnornalShutdown.HasValue)
                    {
                        this.isAbnornalShutdown = true;
                        this.shutdownServiceToken.Cancel();
                    }
                }
            }
        }
    }
}
