﻿namespace LendersOfficeContinuousService
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Cannot create object.
    /// </summary>
    [Serializable]
    public class CriticalException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CriticalException" /> class.
        /// </summary>
        /// <param name="msg">The error message.</param>
        public CriticalException(string msg) : base(msg)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CriticalException" /> class.
        /// </summary>
        /// <param name="msg">The error message.</param>
        /// <param name="innerException">Do not pass empty string.</param>
        public CriticalException(string msg, Exception innerException) : base(msg, innerException)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CriticalException" /> class.
        /// This contructor allows exceptions to be marhalled accross remoting boundaries.
        /// </summary>
        /// <param name="info">Object to which the instance data is added.</param>
        /// <param name="context">The streaming context.</param>
        protected CriticalException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        /// <summary>
        /// Get object data.
        /// </summary>
        /// <param name="info">A SerializationInfo object that contains the information required to serialize the CriticalException instance.</param>
        /// <param name="context">The streaming context.</param>
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
    }
}
