﻿namespace LendersOfficeContinuousService
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// AutoloadAppDomain setting that contains multiple runnable items.
    /// </summary>
    internal class AppDomainSetting
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppDomainSetting" /> class.
        /// </summary>
        /// <param name="fullPath">The full path of the dll.</param>
        /// <param name="alwaysUnload">A value indicating whether the app domain should be reloaded.</param>
        /// <param name="items">The list of LibrarySettingItem object.</param>
        public AppDomainSetting(string fullPath, string alwaysUnload, List<LibrarySettingItem> items)
        {
            if (string.IsNullOrEmpty(fullPath))
            {
                throw new ArgumentException("FullPath and itemType cannot be null or empty.");
            }

            this.IsAlwaysUnload = false;

            if (string.IsNullOrEmpty(alwaysUnload) == false && items.Count == 1)
            {
                this.IsAlwaysUnload = alwaysUnload.Equals("true", StringComparison.OrdinalIgnoreCase);
            }

            var info = new System.IO.FileInfo(fullPath);

            if (info.Exists == false)
            {
                throw new ArgumentException(fullPath + " does not found");
            }

            string nameWithoutExtension = info.Name.Replace(info.Extension, string.Empty);

            this.LibraryFullPath = fullPath;
            this.ApplicationBase = info.DirectoryName;
            this.ApplicationName = "LendersOfficeContinuousService";
            this.ConfigurationFile = this.ApplicationBase + @"\" + nameWithoutExtension + ".config";
            this.LibraryList = items;
        }

        /// <summary>
        /// Gets the application base for the dll.
        /// </summary>
        /// <value>The application base.</value>
        public string ApplicationBase { get; private set; }

        /// <summary>
        /// Gets the application name for logging purposes.
        /// </summary>
        /// <value>The application name.</value>
        public string ApplicationName { get; private set; }

        /// <summary>
        /// Gets the configuration file path.
        /// </summary>
        /// <value>The configuration file path.</value>
        public string ConfigurationFile { get; private set; }

        /// <summary>
        /// Gets the dll full path containing the code to be executed.
        /// </summary>
        /// <value>The full path of the dll.</value>
        public string LibraryFullPath { get; private set; }

        /// <summary>
        /// Gets a value indicating whether to unload the AppDomain after each run.  
        /// This bit is introduce so that Loan pricing engine upload/ loan pricing engine download 
        /// cleanup the memory after each run on 3/11/2015.
        /// </summary>
        /// <value>A value indicating whether to unload the app domain. </value>
        public bool IsAlwaysUnload { get; private set; }

        /// <summary>
        /// Gets or set the list of LibrarySettingItem.
        /// </summary>
        public List<LibrarySettingItem> LibraryList { get; private set; }
    }
}
