﻿// <copyright file="ProjectInstaller.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: David Dao
//  Date:   1/4/2011 4:11:53 PM
// </summary>
namespace LendersOfficeContinuousService
{
    using System.ComponentModel;
    using System.Configuration.Install;
    using System.ServiceProcess;

    /// <summary>
    /// The service installer.
    /// </summary>
    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProjectInstaller" /> class.
        /// </summary>
        public ProjectInstaller()
        {
            this.InitializeComponent();
            this.Install();
        }

        /// <summary>
        /// Installs the service.
        /// </summary>
        private void Install()
        {
            ServiceProcessInstaller processInstaller = new ServiceProcessInstaller();
            processInstaller.Account = ServiceAccount.LocalSystem;
            processInstaller.Username = null;
            processInstaller.Password = null;

            ServiceInstaller serviceInstaller = new ServiceInstaller();
            serviceInstaller.DisplayName = "LendersOfficeContinuous";
            serviceInstaller.ServiceName = "LendersOfficeContinuous";
            serviceInstaller.StartType = ServiceStartMode.Automatic;

            this.Installers.AddRange(new Installer[] 
                                    {
                                        processInstaller,
                                        serviceInstaller
                                    });
        }
    }
}
