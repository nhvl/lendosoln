# Lenders Office Continuous Service

## Summary
The purpose of this document is to help developers set up Lenders Office Continuous Service as a Windows service on their local machine. This includes setting up the multiple config files necessary to run the service properly.

Continuous Service can run any `IRunnable` type from any assembly in our solution. The assembly and the type must be specified in a file named `settings.config`, located in the same folder as LendersOfficeContinuousService.exe. The service will continuously call `Run()` on the specified `IRunnable` types, waiting for the time interval specified in `settings.config` in between calls.

Config files may also need to be created for each of the assemblies specified in `settings.config`. These config files must be located in the same folder as the assembly and must be named the same as the assembly except for the file extension. For example, for the assembly `/path/to/assembly/LendersOfficeLib.dll`, Continuous Service will try to load `/path/to/assembly/LendersOfficeLib.config`. 

Continuous Service also creates an http server on the specified port (by default http://*:8746/service/). The query list will return a copy of what is currently running. 

## Installation 

1. Launch `cmd`
2. `cd` into the folder containing `LendersOfficeContinuousService.exe` 
3. Run `%WINDIR%\Microsoft.NET\Framework64\v4.0.30319\InstallUtil.exe LendersOfficeContinuousService.exe`

## Running Service

1. Open `services.exe`
2. Right click on LendersOfficeContinuousService and run.


## Enabling Logging

Add a file called `site.config` in the same folder containing `LendersOfficeContinuousService.exe` with the following contents:

```xml
<appSettings>
  <add key="LendingQBTraceListenerConnection" value="<yourqueueconnectionstring>" />
</appSettings>
```

If you set up PB log for LendersOfficeApp, the value for `LendingQBTraceListenerConnection` should be the same as the value for `MsMqPbLogConnectStr` is the site.config file for LendersOfficeApp.

`yourqueueconnectionstring` should look something like this:   
    `FormatName:DIRECT=OS:<PcName>\private$\DevLogMSMQ`

## Setting up Runnables

Create a `settings.config` file in the same folder containing `LendersOfficeContinuousService.exe` with the following content:

```xml
<?xml version="1.0" encoding="utf-8" ?>
<libraries>
    <library path="<DLL Path Containing Runnable>" type="<Namespace + Type>" pause_interval_ms="<pauseInterval>"/>  
</libraries>
```

A separate `library` element will need to be added for each `IRunnable` type you want the service to run.

## Configuring Libraries
You may also need to add a config file for the assemblies specified in `settings.config`. These config files must be in the same folder as the assembly and must be named the same as the assembly, except for the file extension.

For example, to run `IRunnable` types in `LendersOfficeLib.dll`, you will need to create a file called `LendersOfficeLib.config` in the same folder as `LendersOfficeLib.dll`.

The contents of these config files should be as follows:

```xml
<?xml version="1.0"?>
<configuration>
  <startup>
    <supportedRuntime version="v4.0" sku=".NETFramework,Version=v4.5"/> 
  </startup>
  <appSettings file="site.config">
    <add key="SecureDBConnStrReadOnly" value="Data Source=11.12.13.23;Initial Catalog=lodevsecure;User ID=&quot;9JKYxUdMsvGI0pFn+CzLbe81gp2pXDsmUwf1WNTxnaM=&quot;;Password=&quot;PDeouUvME37vld8KWzmd3w==&quot;" />
    <add key="SecureConfigFilePath" value="C:\SecureData\ConnectionStringKey.xml" />
  </appSettings> 
</configuration>
```

You may need to add more settings to this config depending on what you need to test.

**NOTE: You will need to restart Continuous Service in order for changes to the config file to take effect.**
