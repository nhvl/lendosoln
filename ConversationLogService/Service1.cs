﻿namespace ConversationLogService
{
    using System;
    using System.Collections;
    using System.Runtime.Remoting;
    using System.Runtime.Remoting.Channels;
    using System.Runtime.Remoting.Channels.Tcp;
    using System.ServiceProcess;
    using ConversationLogLib.Interface;
    using ConversationLogLib.Server.CIL;
    using LendersOffice.Constants;
    using LqbGrammar;
    using LqbGrammar.Exceptions;

    public partial class Service1 : ServiceBase
    {
        /// <summary>
        /// Define the name of the service
        /// </summary>
        public const string AppName = "ConversationLogService";

        /// <summary>
        /// The .net remoting channel.
        /// </summary>
        private TcpChannel channel;

        public Service1()
        {
            InitializeComponent();
            InitializeApplication();
        }

        private void InitializeApplication()
        {
            IExceptionHandlerFactory[] arrHandlers = ExceptionHandlerUtil.GetCoreExceptionHandlerFactories();
            using (IApplicationInitialize iAppInit = LqbApplication.CreateInitializer(arrHandlers))
            {
                const string AppName = Service1.AppName;
                 iAppInit.SetName(AppName);

                Adapter.ApplicationInitializer.RegisterFactories(AppName, iAppInit);
                LendersOffice.ApplicationInitializer.RegisterFactories(AppName, iAppInit);
            }

            LoggingManager.SetServiceLogger(new LqbLogger());
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                Start();
            }
            catch(Exception ex)
            {
                LoggingManager.LogError("[ConversationLogServiceFailedToStart] ConversationLog did not start.", ex);

                // The reason I'm throwing the exception is to give immediate feedback to the person launching the service.
                // If we automate launching the service, we can instead email critical on failure to start.
                throw;
            }
        }

        /// <summary>
        /// Error handling can be done at the top level <see cref="OnStart(string[])"/>.
        /// </summary>
        private void Start()
        {
            string connectionString = DataAccess.DbConnectionStringManager.GetConnectionString(DataAccess.DataSrc.ConversationLog);
            ConversationLogLib.Server.DAL.DSL.DatabaseConnection.SetConnection(connectionString);

            int port = ConstStage.ConversationLogPort;
            if (port > 0)
            {
                try
                {
                    IDictionary props = new Hashtable();
                    props["name"] = "ConversationLogTcp_" + port.ToString();
                    props["port"] = port;
                    this.channel = new TcpChannel(props, null, null);
                    ChannelServices.RegisterChannel(this.channel, false);

                    RemotingConfiguration.RegisterWellKnownServiceType(typeof(ConversationLogServer), ConversationLogServiceName.Value, WellKnownObjectMode.Singleton);
                    LoggingManager.LogMessage(string.Format("Listening on port {0}...", port.ToString()));
                }
                catch (Exception ex)
                {
                    LoggingManager.LogError("Error opening channel: " + ex.Message, ex);
                    throw;
                }
            }
            else
            {
                var message = "Port not configured in configuration file or database.";
                LoggingManager.LogError(message, null);
                throw new DataAccess.CBaseException(message, message);
            }
        }

        protected override void OnStop()
        {
            if (this.channel != null)
            {
                this.channel.StopListening(null);
                ChannelServices.UnregisterChannel(this.channel);
                this.channel = null;
            }
        }
    }
}
