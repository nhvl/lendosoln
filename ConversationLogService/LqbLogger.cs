﻿namespace ConversationLogService
{
    using System;
    using ConversationLogLib.Interface;
    using DataAccess;

    public class LqbLogger : ILogger
    {
        public void LogError(string message, Exception e)
        {
            Tools.LogError(message, e);
        }

        public void LogInfo(string message)
        {
            Tools.LogInfo(message);
        }
    }
}
