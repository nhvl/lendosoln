@SET Id="OrderMIPolicyDelegated"
@SET DefaultValue=true
@SET Notes="OPM 242468 Initial Release"
@SET FailureMessage="You do not have permission to order MI Policy with delegated underwriting authority."

ScheduleExecutable.exe AddOperationToSystemConfig ScheduleExecutable.Migrations.ParameterizedSystemConfigOperationMigrator %Id% %DefaultValue% %Notes% %FailureMessage%

@SET Id="OrderMIPolicyNonDelegated"
@SET FailureMessage="You do not have permission to order MI Policy without delegated underwriting authority."

ScheduleExecutable.exe AddOperationToSystemConfig ScheduleExecutable.Migrations.ParameterizedSystemConfigOperationMigrator %Id% %DefaultValue% %Notes% %FailureMessage%

@SET Id="OrderMIQuote"
@SET FailureMessage="You do not have permission to order MI Quotes."

ScheduleExecutable.exe AddOperationToSystemConfig ScheduleExecutable.Migrations.ParameterizedSystemConfigOperationMigrator %Id% %DefaultValue% %Notes% %FailureMessage%