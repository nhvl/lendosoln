@SET Id="UploadEDocs"
@SET DefaultValue=true
@SET Notes="OPM 475715"
@SET FailureMessage="You do not have permission to upload EDocs to this file."

ScheduleExecutable.exe AddOperationToSystemConfig ScheduleExecutable.Migrations.ParameterizedSystemConfigOperationMigrator %Id% %DefaultValue% %Notes% %FailureMessage%