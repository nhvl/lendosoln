@SET Id="LeadToLoanConversion"
@SET DefaultValue=true
@SET Notes="OPM 302277"
@SET FailureMessage="You do not have permission to convert lead to loan."

ScheduleExecutable.exe AddOperationToSystemConfig ScheduleExecutable.Migrations.ParameterizedSystemConfigOperationMigrator %Id% %DefaultValue% %Notes% %FailureMessage%