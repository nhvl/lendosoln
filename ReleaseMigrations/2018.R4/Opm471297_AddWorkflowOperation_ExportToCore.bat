@SET Id="ExportToCoreSystem"
@SET DefaultValue=false
@SET Notes="OPM 471297, OPM 367036"
@SET FailureMessage="You do not have permission to export data to the core."

ScheduleExecutable.exe AddOperationToSystemConfig ScheduleExecutable.Migrations.ParameterizedSystemConfigOperationMigrator %Id% %DefaultValue% %Notes% %FailureMessage%