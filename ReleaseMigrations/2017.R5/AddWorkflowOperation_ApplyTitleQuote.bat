@SET Id="ApplyTitleQuote"
@SET DefaultValue=false
@SET Notes="OPM 460497"
@SET FailureMessage="You do not have permission to apply a title quote."

ScheduleExecutable.exe AddOperationToSystemConfig ScheduleExecutable.Migrations.ParameterizedSystemConfigOperationMigrator %Id% %DefaultValue% %Notes% %FailureMessage%
