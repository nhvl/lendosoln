@SET Id="OrderTitleQuote"
@SET DefaultValue=false
@SET Notes="OPM 460497"
@SET FailureMessage="You do not have permission to order a title quote."

ScheduleExecutable.exe AddOperationToSystemConfig ScheduleExecutable.Migrations.ParameterizedSystemConfigOperationMigrator %Id% %DefaultValue% %Notes% %FailureMessage%
