@SET Id="GenerateUCD"
@SET DefaultValue=false
@SET Notes="OPM 461397"
@SET FailureMessage="You do not have permission to Generate UCD."

ScheduleExecutable.exe AddOperationToSystemConfig ScheduleExecutable.Migrations.ParameterizedSystemConfigOperationMigrator %Id% %DefaultValue% %Notes% %FailureMessage%
