@SET Id="OrderSSA89"
@SET DefaultValue=false
@SET Notes="OPM 458436"
@SET FailureMessage="You do not have permission to order SSA-89."

ScheduleExecutable.exe AddOperationToSystemConfig ScheduleExecutable.Migrations.ParameterizedSystemConfigOperationMigrator %Id% %DefaultValue% %Notes% %FailureMessage%
