$saveFormat = 12 # docx
$current_directory = pwd

$documents_path = Join-Path $current_directory -ChildPath 'Opm463833'
$backup_path = Join-Path $documents_path -ChildPath 'Backup'
$save_path = Join-Path $documents_path -ChildPath 'MigratedDocs'

New-Item -ItemType directory -Path $backup_path > $null
New-Item -ItemType directory -Path $save_path > $null

Write-Host "Converting to DOCX. If prompted to convert a file by Word, select 'Recover Text from Any File'."
$word_app = New-Object -ComObject Word.Application
$word_app.Visible = $False

# This filter will find .doc as well as .docx documents
Get-ChildItem -Path $documents_path -Filter *.unmigrated | ForEach-Object {

    $document = $word_app.Documents.Open($_.FullName)

    $docx_filename = "$($_.DirectoryName)\$($_.BaseName).docx"

    $document.SaveAs([ref] $docx_filename, [ref] $saveFormat)

    $document.Close()
	
	Move-Item $docx_filename -Destination $save_path
	Move-Item $_.FullName -Destination $backup_path
}

$word_app.Quit()

Write-Host "Documents converted. Please create a ZIP of the 'Opm463833/Backup' folder and attach to case 463833. To continue the migration, copy the 'Opm463833/MigratedDocs' folder to a production machine then run the next Powershell script outside of that folder to write the documents back to FileDB."