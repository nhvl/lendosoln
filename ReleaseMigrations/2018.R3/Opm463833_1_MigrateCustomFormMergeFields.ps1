$current_directory = pwd
$documents_path = Join-Path $current_directory -ChildPath 'Opm463833'
New-Item -ItemType directory -Path $documents_path > $null

Write-Host "Pulling documents from FileDB"
& '.\ScheduleExecutable.exe' 'ScheduleExecutable.Migrations.Opm463833_MigrateCustomWordFormMergeFields::PullFiles' $documents_path

Write-Host "Documents copied to folder Opm463833. Copy that folder to a local machine with Word installed, then continue the migration by running the next Powershell script outside of that folder."
