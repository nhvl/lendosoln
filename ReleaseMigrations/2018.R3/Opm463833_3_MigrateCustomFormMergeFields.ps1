$current_directory = pwd
$save_path = Join-Path $current_directory -ChildPath 'MigratedDocs'

Write-Host "Migrating mail merge fields and writing back to FileDB"
& '.\ScheduleExecutable.exe' 'ScheduleExecutable.Migrations.Opm463833_MigrateCustomWordFormMergeFields::MigrateFields' $save_path

Write-Host "Migration complete."
