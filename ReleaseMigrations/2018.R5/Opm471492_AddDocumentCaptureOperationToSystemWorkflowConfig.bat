@SET Id="UseDocumentCapture"
@SET DefaultValue=true
@SET Notes="OPM 471492"
@SET FailureMessage="You do not have permission to use document capture."

ScheduleExecutable.exe AddOperationToSystemConfig ScheduleExecutable.Migrations.ParameterizedSystemConfigOperationMigrator %Id% %DefaultValue% %Notes% %FailureMessage%