﻿using System;


namespace SendReportToThirdParty
{
    public class ReportingException : Exception
    {
        #region Variables
        private string m_sUserMessage;
        #endregion

        public ReportingException(string userMessage) : base(userMessage)
        {
            m_sUserMessage = userMessage;
        }

        public override string ToString()
        {
            return string.Format("{0}{1}", m_sUserMessage, Environment.NewLine);
        }

    }
}
