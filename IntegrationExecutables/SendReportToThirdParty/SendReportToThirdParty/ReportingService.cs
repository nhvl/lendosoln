﻿using System;
using System.Threading;
using System.Text;
using System.IO;
using System.Collections.Generic;
using SendReportToThirdParty.com.lendersoffice.Auth;
using SendReportToThirdParty.com.lendersoffice.Reporting;

namespace SendReportToThirdParty
{
    public class ReportingService
    {
        #region Variables
        private AuthService m_authService;
        private Reporting m_reporting;
        private string m_sLender, m_sTicket;
        private Dictionary<string, string> m_batchExportRequests;
        public List<string> m_reportFiles;
        #endregion

        public ReportingService(string lender, string[] login)
        {
            #region instantiateVariables
            m_sLender = lender;
            m_authService = new AuthService();
            m_sTicket = m_authService.GetUserAuthTicket(login[0], login[1]);
            m_reporting = new Reporting();
            m_reporting.Timeout = 1000000;
            m_batchExportRequests = new Dictionary<string, string>();
            m_reportFiles = new List<string>();
            #endregion
        }

        public void generateCustomReport(string sCustomReportName)
        {
            string response = m_reporting.RetrieveCustomReport(m_sTicket, sCustomReportName, true);
            if (LOXML.ParseResponseStatusFromXml(response) == LOXML.ResponseStatus.ERROR)
                throw new ReportingException("Unable to retrieve Custom Report: " + Environment.NewLine + sCustomReportName + Environment.NewLine + response);
            Directory.CreateDirectory(m_sLender);
            string path = m_sLender + "\\" + sCustomReportName + ".csv";
            FileIO.WriteToFile(path, response);
            m_reportFiles.Add(path);
        }

        public void AddBatchExportToQueue(string batchExport, string batchExportArg)
        {
            string response = isBatchExportArgXmlFile(batchExportArg) ? AddBatchExportToQueueByXml(batchExport, batchExportArg) : AddBatchExportToQueueByCustomReport(batchExport, batchExportArg);
            if (LOXML.ParseResponseStatusFromXml(response) == LOXML.ResponseStatus.ERROR)
                throw new ReportingException("Unable to Request Batch Export: " + Environment.NewLine + batchExport + Environment.NewLine + response);
            m_batchExportRequests.Add(batchExport,LOXML.ExtractBatchExportID(response));
        }

        private bool isBatchExportArgXmlFile(string batchExportArg)
        {
            return batchExportArg.ToLower().Contains(".xml");
        }
        private string AddBatchExportToQueueByXml(string batchExport, string batchExportArg)
        {
            return m_reporting.AddBatchExportToQueueWithLoanList(m_sTicket, FileIO.LoadFile(batchExportArg), batchExport);
        }
        private string AddBatchExportToQueueByCustomReport(string batchExport, string batchExportArg)
        {
            return m_reporting.AddBatchExportToQueue(m_sTicket, batchExportArg, batchExport);
        }

        public void DownloadBatchExports()
        {
            foreach (string batchExportName in m_batchExportRequests.Keys)
            {
                string requestID = "";
                m_batchExportRequests.TryGetValue(batchExportName, out requestID);
                string response = m_reporting.RetrieveBatchExport(m_sTicket, requestID);
                LOXML.ResponseStatus status = LOXML.ParseResponseStatusFromXml(response);
                int timeout = 7200000; //timeout at 120 minutes
                int timeInterval = 30000; //Sleep for 30 seconds
                int timeElapsed = 0;
                while (status == LOXML.ResponseStatus.PENDING && timeElapsed <= timeout)
                {
                    Thread.Sleep(timeInterval);
                    timeElapsed += timeInterval;
                    response = m_reporting.RetrieveBatchExport(m_sTicket, requestID);
                    status = LOXML.ParseResponseStatusFromXml(response);
                }
                switch (status)
                {
                    case LOXML.ResponseStatus.COMPLETE:
                        string fileType = LOXML.ExtractBatchExportFileType(response);
                        string fileContents = LOXML.ParseBatchExportResponseForFileContents(response);
                        Directory.CreateDirectory(m_sLender);
                        string path = m_sLender + @"\" + batchExportName + "." + fileType;
                        FileIO.Base64DecodeToFile(path, fileContents);
                        m_reportFiles.Add(path);
                        break;
                    case LOXML.ResponseStatus.ERROR:
                        throw new TimeoutException("Retrieval of batch Export "+batchExportName+" failed. Request ID "+requestID+" returns error."+Environment.NewLine+response);
                    case LOXML.ResponseStatus.PENDING:
                        throw new TimeoutException("Retrieval of batch Export "+batchExportName+" failed. Request ID "+requestID+" could not be retrieved with in "+timeout+" ms. It is still PENDING.");
                    default:
                        throw new InvalidOperationException("Retrieval of batch Export "+batchExportName+" failed due to unhandled ResponseStatus enum. Status is "+status);
                }
            }
        }
    }
}