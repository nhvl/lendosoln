﻿using System;
using System.Text;
using System.IO;

namespace SendReportToThirdParty
{
    public static class FileIO
    {
        public static string Base64EncodeFile(string sFilePath)
        {
            byte[] PDFBytes = File.ReadAllBytes(sFilePath);
            return Convert.ToBase64String(PDFBytes);
        }

        public static void Base64DecodeToFile(string sFilePath, string sData)
        {
            try
            {
                using (FileStream fs = new FileStream(sFilePath, FileMode.Create, FileAccess.ReadWrite, FileShare.Read))
                {
                    byte[] decoded_byte = Convert.FromBase64String(sData);
                    fs.Write(decoded_byte, 0, decoded_byte.Length);
                }
            }
            catch (Exception exc)
            {
                throw new BaseException("Error in Base64Decode", exc);
            }
        }

        public static string LoadFile(string sFilePath)
        {
            string sResult = "";
            if (File.Exists(sFilePath))
            {
                using (FileStream fsStream = new FileStream(sFilePath, FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    using (StreamReader srReader = new StreamReader(fsStream))
                    {
                        sResult = GetContents(srReader);
                    }
                }
            }
            return sResult;
        }

        public static string TargetBorrowerFile(string sFolder, string sLoanNumber, string sBorrowerName)
        {
            return string.Format("{0}\\{1}_{2}.fnm", sFolder, sLoanNumber, sBorrowerName);
        }
        public static string TargetFile(string sFolder, string sFileName)
        {
            return string.Format("{0}\\{1}", sFolder, PostFixDatetime(sFileName));
        }

        public static void WriteToFile(string sFilePath, string sText)
        {
            using (FileStream fs = new FileStream(sFilePath, FileMode.Create, FileAccess.ReadWrite, FileShare.Read))
            {
                byte[] btData = System.Text.Encoding.ASCII.GetBytes(sText);
                fs.Write(btData, 0, btData.Length);
            }
        }

        public static void WriteToFile(string sFilePath, byte[] byteData)
        {
            using (FileStream fs = new FileStream(sFilePath, FileMode.Create, FileAccess.ReadWrite, FileShare.Read))
            {
                fs.Write(byteData, 0, byteData.Length);
            }
        }

        // Returns a string containing the file data read using the given reader
        private static string GetContents(StreamReader srReader)
        {
            const int BUFFER_SIZE = 50000;
            char[] chBuffer = new char[BUFFER_SIZE];
            int n = 0;

            StringBuilder sb = new StringBuilder();
            while ((n = srReader.Read(chBuffer, 0, BUFFER_SIZE)) > 0)
            {
                sb.Append(chBuffer, 0, n);
            }
            return sb.ToString();
        }

        private static string PostFixDatetime(string str)
        {
            string pf = DateTime.Now.ToString("_yyMMdd_HHmmss");
            int extIndex = str.LastIndexOf(".");

            if (extIndex > -1)
                return str.Substring(0, extIndex) + pf + str.Substring(extIndex);
            return str + pf;
        }
    }
}
