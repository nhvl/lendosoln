﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace SendReportToThirdParty
{
    public static class LOXML
    {
        public enum ResponseStatus
        {
            OK,
            COMPLETE,
            ERROR,
            PENDING
        }

        public static ResponseStatus ParseResponseStatusFromXml(string response)
        {
            string status;
            try
            {
                status = CreateXmlDoc(response).SelectSingleNode("//result").Attributes["status"].InnerText;
            }
            catch (XmlException)
            {
                return ResponseStatus.OK; //valid csv output from Custom Reports will throw XmlException. This is Ok.
            }
            if (status.Equals("OK", StringComparison.OrdinalIgnoreCase))
                return ResponseStatus.OK;
            else if (status.Equals("COMPLETE", StringComparison.OrdinalIgnoreCase))
                return ResponseStatus.COMPLETE;
            else if (status.Equals("ERROR", StringComparison.OrdinalIgnoreCase))
                return ResponseStatus.ERROR;
            else if (status.Equals("PENDING", StringComparison.OrdinalIgnoreCase))
                return ResponseStatus.PENDING;
            else
                throw new InvalidOperationException("Unhandled ResponseStatus Enum");
        }

        public static string ExtractBatchExportID(string response)
        {
            return CreateXmlDoc(response).SelectSingleNode("//batchExportID").InnerText;
        }

        public static string ExtractBatchExportFileType(string response)
        {
            return CreateXmlDoc(response).SelectSingleNode("//contents").Attributes["outputtype"].InnerText;
        }

        public static string ParseBatchExportResponseForFileContents(string sLOXMLResponse)
        {
            string sContents = string.Empty;
            XmlDocument responseDoc = CreateXmlDoc(sLOXMLResponse);
            XmlNode xResult = responseDoc.SelectSingleNode("//result");

            string sStatus = string.Empty;

            if (xResult != null)
            {
                XmlAttribute xStatus = xResult.Attributes["status"];
                sStatus = (xStatus != null) ? xStatus.InnerText.Trim() : string.Empty;
            }

            if (sStatus.Equals("COMPLETE", StringComparison.OrdinalIgnoreCase))
            {
                // to-do: create a class container for the outputtype and any other data that might be useful
                foreach (XmlNode xResultData in xResult)
                {
                    if (xResultData.Name.Equals("contents", StringComparison.OrdinalIgnoreCase))
                    {
                        sContents = xResultData.InnerText;
                    }
                    else
                    {
                        continue;
                    }
                }
            }

            return sContents;
        }

        #region Copied from LosUtils
        public static XmlDocument CreateXmlDoc(string xmlText)
        {
            try
            {
                return CreateXmlDocImpl(xmlText);
            }
            catch (XmlException)
            {
                xmlText = SantizeXmlString(xmlText);
                return CreateXmlDocImpl(xmlText);
            }
        }

        private static XmlDocument CreateXmlDocImpl(string xmlText)
        {
            XmlDocument xmlDoc = new XmlDocument();

            if (null == xmlText || xmlText.Trim() == "")
                return xmlDoc;

            XmlTextReader readerData = null;

            try
            {
                if (xmlText != null && xmlText.Length <= (4 * 1024))
                {
                    readerData = new XmlTextReader(xmlText, XmlNodeType.Document, null);
                }
                else
                    readerData = new XmlTextReader(new StringReader(xmlText));

                readerData.XmlResolver = null;
                xmlDoc.Load(readerData);
            }
            finally
            {
                if (null != readerData)
                    readerData.Close();
            }
            return xmlDoc;
        }

        /// <summary>
        /// Remove all illegal XML characters from string. The list of allowable characters http://www.w3.org/TR/REC-xml/#charsets
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        private static string SantizeXmlString(string xml)
        {
            StringBuilder sb = new StringBuilder(xml.Length);
            foreach (char ch in xml.ToCharArray())
            {
                if (IsLegalXmlChar(ch))
                {
                    sb.Append(ch);
                }
            }
            return sb.ToString();
        }

        private static bool IsLegalXmlChar(int ch)
        {
            //The list of allowable characters http://www.w3.org/TR/REC-xml/#charsets
            return (ch == 0x9 || ch == 0xA || ch == 0xD || (ch >= 0x20 && ch <= 0xD7FF) || (ch >= 0xE000 && ch <= 0xFFFD) || (ch >= 0x10000 && ch <= 0x10FFFF));
        }
        #endregion
    }
}
