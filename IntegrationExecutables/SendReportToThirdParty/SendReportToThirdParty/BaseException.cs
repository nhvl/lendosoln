﻿using System;

namespace SendReportToThirdParty
{
    public class BaseException : Exception
    {
        #region Variables
        private string m_sUserMessage;
        private string m_sException;
        #endregion

        public BaseException(string userMessage, Exception innerException) : base(userMessage, innerException)
        {
            m_sUserMessage = userMessage;
            m_sException = innerException.ToString();
        }

        public override string ToString()
        {
            return string.Format("{0}{2}{1}{2}", m_sUserMessage, m_sException, Environment.NewLine);
        }
    }
}
