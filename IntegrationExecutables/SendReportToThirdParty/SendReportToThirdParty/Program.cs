﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Mail;
using WinSCP;

namespace SendReportToThirdParty
{
    class Program
    {
        static void Main(string[] args)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            foreach (string lender in AppSettings.Lenders)
            {
                try
                {
                    #region Retrieve AppSettings Data
                    string[] login = AppSettings.Login(lender);
                    string[] customReports = AppSettings.CustomReports(lender);
                    string[] batchExports = AppSettings.BatchExports(lender);
                    string batchExportArg; // this is set per element of m_batchExports
                    string[] postInfo = AppSettings.PostInfo(lender);
                    #endregion

                    #region Run Reports
                    ReportingService lenderReport = new ReportingService(lender, login);
                    foreach (string customReport in customReports)
                    {
                        lenderReport.generateCustomReport(customReport);
                    }
                    foreach (string batchExport in batchExports)
                    {
                        batchExportArg = AppSettings.BatchExportArg(lender, batchExport);
                        lenderReport.AddBatchExportToQueue(batchExport, batchExportArg);
                    }
                    lenderReport.DownloadBatchExports();
                    #endregion

                    #region Send Reports

                    if (postInfo[0].Equals("SMTP", StringComparison.OrdinalIgnoreCase))
                    {
                        SmtpClient smtpClient = new SmtpClient(AppSettings.SMTP_SERVER);
                        MailMessage message = new MailMessage("support@lendingqb.com", postInfo[1], "Custom Reports And Batch Exports For "+lender,"");
                        Attachment attachment;
                        foreach(string report in lenderReport.m_reportFiles)
                        {
                            attachment = new Attachment(report);
                            message.Attachments.Add(attachment);
                        }
                        smtpClient.Send(message);
                    }

                    else
                    {
                        SessionOptions sessionOptions = new SessionOptions
                        {
                            Protocol = GetProtocol(postInfo[0]),
                            HostName = postInfo[1],
                            UserName = postInfo[2],
                            Password = postInfo[3],
                            PortNumber = Convert.ToInt32(postInfo[4]),
                            //SshHostKeyFingerprint = sshHostKeyFingerPrint
                            };
                            if (string.IsNullOrEmpty(postInfo[5]) == false)
                            {
                                sessionOptions.SshHostKeyFingerprint = postInfo[5];
                            }
                            string destPath = postInfo[6];
                            using (Session session = new Session())
                            {
                                //Connect
                                session.Open(sessionOptions);

                                // Upload files
                                TransferOptions transferOptions = new TransferOptions();
                                // 4/7/2014 dd - For some odd reason the SFTP 65.174.253.95 will fail if
                                // we choose the Automatic mode. Therefore just set it as ASCII.
                                transferOptions.TransferMode = TransferMode.Ascii;
                                

                                TransferOperationResult transferResult;
                                foreach (string report in lenderReport.m_reportFiles)
                                {
                                    
                                    transferResult = session.PutFiles(report, destPath, false, transferOptions);
                                    transferResult.Check(); // Throw on any error
                                }

                            }
                    }
                    #endregion
                }
                catch (Exception e)
                {
                    Console.WriteLine("ERROR: " + e);
                    SmtpClient smtpClient = new SmtpClient(AppSettings.SMTP_SERVER);
                    MailMessage message = new MailMessage("support@lendingqb.com", "isaacr@meridianlink.com", "Send Custom Report/Batch Export to "+lender+" failed.", e.ToString());
                    smtpClient.Send(message);
                    continue;
                }
            }
        }
        private static Protocol GetProtocol(string value)
        {
            if (value.Equals("sftp", StringComparison.OrdinalIgnoreCase))
            {
                return Protocol.Sftp;
            }
            else if (value.Equals("ftp", StringComparison.OrdinalIgnoreCase))
            {
                return Protocol.Ftp;
            }
            else
            {
                throw new ArgumentException("Invalid Post Info Type: " + value + ". Please Check Appsettings.");
            }
        }
    }
}
