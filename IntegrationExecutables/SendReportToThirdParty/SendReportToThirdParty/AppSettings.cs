﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SendReportToThirdParty
{
    class AppSettings
    {
        public static string[] Lenders
        {
            get 
            {
                try
                {
                    return System.Configuration.ConfigurationManager.AppSettings["Lenders"].Split(';');
                }
                catch (NullReferenceException)
                {
                    return new string[0];
                }
            }
        }
        public static string[] Login(string lender)
        {
            return System.Configuration.ConfigurationManager.AppSettings[lender + "_Login"].Split(';');
        }
        public static string[] CustomReports(string lender)
        {
            try
            {
                return System.Configuration.ConfigurationManager.AppSettings[lender + "_CustomReports"].Split(';');
            }
            catch (NullReferenceException)
            {
                return new string[0];
            }
        }
        public static string[] BatchExports(string lender)
        {
            try
            {
                return System.Configuration.ConfigurationManager.AppSettings[lender + "_BatchExports"].Split(';');
            }
            catch (NullReferenceException)
            {
                return new string[0];
            }
        }
        public static string BatchExportArg(string lender, string batchExport)
        {
            return System.Configuration.ConfigurationManager.AppSettings[lender + "_" + batchExport + "_Arg"];
        }

        public static string[] PostInfo(string login)
        {
            return System.Configuration.ConfigurationManager.AppSettings[login + "_PostInfo"].Split(';');
        }
        public static string SMTP_SERVER
        {
            get { return System.Configuration.ConfigurationManager.AppSettings["SMTP_SERVER"]; }
        }
    }
}
