﻿namespace UndisclosedDebtNotification.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Fakes;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class SendEmailTest
    {
        private MockEmailSender emailSender = new MockEmailSender();
        private System.Collections.Specialized.NameValueCollection appSettings;
        private const string SupportEmail = "support@lendingqb.com"; // not really, but it doesn't matter

        [TestInitialize]
        public void Setup()
        {
            this.appSettings = FakeServiceLocator.CreateNewAppSettingsFromCurrent();
            this.appSettings["SMTP_SERVER"] = "smtp.meridianlink.com";
            this.appSettings["SUPPORT_EMAIL"] = SupportEmail;
            ServiceLocator.ReplaceServiceLocator(new FakeServiceLocator(appSettings: this.appSettings, emailSender: this.emailSender));
        }

        [TestMethod]
        public void SendErrorMessageToClient_AllFine_SendsOneEmail()
        {
            CallSendEmailMessageToClient(MockEmailSender.NoThrowEmailAddress, MockEmailSender.NoThrowAltEmailAddress);

            Assert.AreEqual(1, this.emailSender.EmailsSentCount);
            Assert.AreEqual(MockEmailSender.NoThrowEmailAddress, this.emailSender.EmailAddressesContacted.Single());
        }

        [TestMethod]
        public void SendErrorMessageToClient_AllFineThrowEmailOnce_SendsOneEmail()
        {
            CallSendEmailMessageToClient(MockEmailSender.ThrowOnceEmailAddress, MockEmailSender.NoThrowAltEmailAddress);

            Assert.AreEqual(2, this.emailSender.EmailTryCount(MockEmailSender.ThrowOnceEmailAddress));
            Assert.AreEqual(1, this.emailSender.EmailsSentCount);
            Assert.AreEqual(MockEmailSender.ThrowOnceEmailAddress, this.emailSender.EmailAddressesContacted.Single());
        }

        [TestMethod]
        public void SendErrorMessageToClient_AllFineThrowEmailTwice_FallsToBackup()
        {
            CallSendEmailMessageToClient(MockEmailSender.ThrowTwiceEmailAddress, MockEmailSender.NoThrowEmailAddress);

            Assert.AreEqual(2, this.emailSender.EmailTryCount(MockEmailSender.ThrowTwiceEmailAddress));
            Assert.AreEqual(1, this.emailSender.EmailsSentCount);
            Assert.AreEqual(MockEmailSender.NoThrowEmailAddress, this.emailSender.EmailAddressesContacted.Single());
        }

        [TestMethod]

        public void SendErrorMessageToClient_AllFineThrowEmailTwiceBackupThrowOnce_FallsToBackup()
        {
            CallSendEmailMessageToClient(MockEmailSender.ThrowTwiceEmailAddress, MockEmailSender.ThrowOnceEmailAddress);

            Assert.AreEqual(2, this.emailSender.EmailTryCount(MockEmailSender.ThrowTwiceEmailAddress));
            Assert.AreEqual(1, this.emailSender.EmailsSentCount);
            Assert.AreEqual(MockEmailSender.ThrowOnceEmailAddress, this.emailSender.EmailAddressesContacted.Single());
        }

        [TestMethod]
        public void SendErrorMessageToClient_AllFineThrowEmailTwiceBackupThrowTwice_FallsToSupport()
        {
            CallSendEmailMessageToClient(MockEmailSender.AlwaysThrowEmailAddress, MockEmailSender.ThrowTwiceEmailAddress);

            Assert.AreEqual(2, this.emailSender.EmailTryCount(MockEmailSender.AlwaysThrowEmailAddress));
            Assert.AreEqual(2, this.emailSender.EmailTryCount(MockEmailSender.ThrowTwiceEmailAddress));
            Assert.AreEqual(1, this.emailSender.EmailTryCount(SupportEmail));
            Assert.AreEqual(1, this.emailSender.EmailsSentCount);
            Assert.AreEqual(SupportEmail, this.emailSender.EmailAddressesContacted.Single());
        }

        [TestMethod]
        public void SendErrorMessageToClient_AllFineThrowEmailTwiceBackupThrowTwice_EmailToSupportIncludesFailedEmails()
        {
            // Trying to test: send the notification to LQB support and include a statement that the notification to the
            // client failed; that way LQB can follow up with the client to make them aware of the problem & re-configure
            // the notification email address as needed
            CallSendEmailMessageToClient(MockEmailSender.AlwaysThrowEmailAddress, MockEmailSender.ThrowTwiceEmailAddress);

            Assert.AreEqual(SupportEmail, this.emailSender.EmailAddressesContacted.Single());
            Assert.IsTrue(this.emailSender.LastEmailBody.Contains(MockEmailSender.AlwaysThrowEmailAddress));
            Assert.IsTrue(this.emailSender.LastEmailBody.Contains(MockEmailSender.ThrowTwiceEmailAddress));
        }

        [TestMethod]
        public void SendErrorMessageToClient_AllFineThrowEmailTwiceBackupThrowTwiceNoSupportEmail_FallsToDeveloper()
        {
            this.appSettings["SUPPORT_EMAIL"] = null;
            CallSendEmailMessageToClient(MockEmailSender.AlwaysThrowEmailAddress, MockEmailSender.ThrowTwiceEmailAddress);

            Assert.AreEqual(2, this.emailSender.EmailTryCount(MockEmailSender.AlwaysThrowEmailAddress));
            Assert.AreEqual(2, this.emailSender.EmailTryCount(MockEmailSender.ThrowTwiceEmailAddress));
            Assert.AreEqual(0, this.emailSender.EmailTryCount(SupportEmail));
            Assert.AreEqual(1, this.emailSender.EmailsSentCount);
            Assert.AreEqual(SendEmail.DeveloperErrorFallbackEmail, this.emailSender.EmailAddressesContacted.Single());
        }

        private static void CallSendEmailMessageToClient(string errorEmail, string backupErrorEmail)
        {
            var requestData = CreateGetRequestData(errorEmail, backupErrorEmail);
            string[] errorMessages = new[] { "error message" };
            var requestType = requestData.RequestType;
            string orderResult = "some result string";

            SendEmail.SendErrorMessageToClient(requestData, errorMessages, requestType, orderResult);
        }

        public static GetDeactivateRequestData CreateGetRequestData(string errorEmail, string backupErrorEmail)
        {
            var loanApp = new LoanNmAppId("123456789", new Guid("aaa1157e-f6a5-4d24-a81d-6b08f4378b41"));
            var dataDictionary = new Dictionary<string, string>
                {
                    { "sSpAddr", "1600 W Sunflower Avenue, Suite 200" },
                    { "sSpCity", "Costa Mesa" },
                    { "sSpState", "CA" },
                    { "sSpZip", "92626" },
                    { "aCrOd", "6/23/2016" },
                    { "aUdnOrderId", "12345" },
                    { "aAppId", loanApp.AppId.ToString() },
                    { "aBAddr", "1124 Bristol St" },
                    { "aBCity", "Costa Mesa" },
                    { "aBState", "CA" },
                    { "aBZip", "92626" },
                    { "aBAddrYrs", "15" },
                    { "aBFirstNm", "Harold" },
                    { "aBLastNm", "Hall" },
                    { "aBSsn", "123-45-6789" },
                    { "aBTypeT", "0" },
                    { "aBDob", "1/1/1970" },
                };

            var request = new GetDeactivateRequestData(dataDictionary, loanApp, errorEmail, backupErrorEmail, E_RequestType.GET);
            Assert.AreEqual(request.IsValid(), true);

            return request;
        }
    }
}
