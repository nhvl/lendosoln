﻿namespace UndisclosedDebtNotification.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Fakes;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class ProgramTest
    {
        private const string ClientErrorEmail = "errorEmail1@lendingqb.com";
        private Lender lender;
        private FakeLqbWebService.FakeWebServiceFactory fakeWebservices;
        private MockEmailSender mockEmailSender;

        [TestInitialize]
        public void Setup()
        {
            string customerCode = "TEST123";
            Guid brokerId = new Guid("72614456-6715-412d-8a7c-32413e992f3e");
            IEnumerable<Guid> branches = new[] { new Guid("cdb1eb57-5c20-408e-b0f3-7dda03922900") };
            var craList = new Dictionary<Guid, string> { { new Guid("83151f35-89ff-4a25-b3b6-d1934f03c528"), "http://localhost/" } };

            var appSettings = FakeServiceLocator.CreateNewAppSettingsFromCurrent();
            appSettings["SMTP_SERVER"] = "smtp.meridianlink.com";
            appSettings[customerCode + "_MCL_CREDENTIALS"] = "username;password";
            appSettings[customerCode + "_LQB_CREDENTIALS"] = "username;password";
            appSettings[customerCode + "_LOAN_PURPOSES"] = "Purchase; RefiRateTerm; RefinanceCashout; Construction; ConstructionPerm; Other; FHAStreamlinedRefinance; VAIRRRL; HomeEquity;";
            appSettings[customerCode + "_LOAN_TYPES"] = "Conventional; FHA; VA; USDARural; Other;";
            appSettings[customerCode + "_LENDER_CRA_COM_ID"] = craList.First().Key.ToString();
            appSettings[customerCode + "_CHANNELS"] = "Retail";
            appSettings[customerCode + "_C_STATUS"] = "Retail: sSubmitD;";
            appSettings[customerCode + "_ERROR_ROLE"] = "Retail: LoanRep,1throw@lendingqb.com";
            appSettings[customerCode + "_CONDITION_CATEGORY"] = "My Condition Category";
            appSettings[customerCode + "_DOCTYPE"] = "My DocType";

            var database = new SingleLenderFakeDB(craList, brokerId, branches);
            this.fakeWebservices = new FakeLqbWebService.FakeWebServiceFactory();
            this.mockEmailSender = new MockEmailSender();

            ServiceLocator.ReplaceServiceLocator(new FakeServiceLocator(appSettings, database, this.fakeWebservices.Create, this.mockEmailSender));
            MclRequest.LookupCompanyUrls(database);
            this.lender = new Lender(customerCode, E_RequestType.GET, ServiceLocator.Instance.CreateDB("fake connection string"));
            Assert.IsTrue(this.lender.IsComplete());
        }

        #region ImportResultToLQB
        // Currently missing:
        // - testing that the save is sending the correct sort of data
        // - behavior when there's no data to import
        // - NEW/DEACTIVATE save testing
        [TestMethod]
        public void ImportResultToLQB_SimpleGetRequestSaveAndPdfSuccessful_Successful()
        {
            var scenario = this.CreateSimpleGetRequestScenario();
            this.fakeWebservices.SaveResult = FakeLqbWebService.FakeSaveResult.Success();
            this.fakeWebservices.UploadResult = FakeLqbWebService.FakeSaveResult.Success();

            bool result = Program.ImportResultToLQB(scenario.Lender, scenario.RequestData, scenario.Responses, scenario.RequestType);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void ImportResultToLQB_SimpleGetRequestSaveAndPdfSuccessful_DoesNotEmail()
        {
            var scenario = this.CreateSimpleGetRequestScenario();
            this.fakeWebservices.SaveResult = FakeLqbWebService.FakeSaveResult.Success();
            this.fakeWebservices.UploadResult = FakeLqbWebService.FakeSaveResult.Success();

            bool result = Program.ImportResultToLQB(scenario.Lender, scenario.RequestData, scenario.Responses, scenario.RequestType);
            Assert.AreEqual(0, this.mockEmailSender.EmailsSentCount);
            Assert.IsFalse(this.mockEmailSender.SentEmailTo(ClientErrorEmail));
        }

        [TestMethod]
        public void ImportResultToLQB_SimpleGetRequestSaveUnsuccessfulPdfSuccessful_Unsuccessful()
        {
            var scenario = this.CreateSimpleGetRequestScenario();
            this.fakeWebservices.SaveResult = FakeLqbWebService.FakeSaveResult.Exception(service => new Exception("Can't save this"));
            this.fakeWebservices.UploadResult = FakeLqbWebService.FakeSaveResult.Success();

            bool result = Program.ImportResultToLQB(scenario.Lender, scenario.RequestData, scenario.Responses, scenario.RequestType);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void ImportResultToLQB_SimpleGetRequestSaveUnsuccessfulPdfSuccessful_EmailsClient()
        {
            var scenario = this.CreateSimpleGetRequestScenario();
            this.fakeWebservices.SaveResult = FakeLqbWebService.FakeSaveResult.Exception(service => new Exception("Can't save this"));
            this.fakeWebservices.UploadResult = FakeLqbWebService.FakeSaveResult.Success();

            bool result = Program.ImportResultToLQB(scenario.Lender, scenario.RequestData, scenario.Responses, scenario.RequestType);
            Assert.AreEqual(1, this.mockEmailSender.EmailsSentCount);
            Assert.IsTrue(this.mockEmailSender.SentEmailTo(ClientErrorEmail));
        }

        [TestMethod]
        public void ImportResultToLQB_SimpleGetRequestSaveUnsuccessfulOncePdfSuccessful_Successful()
        {
            var scenario = this.CreateSimpleGetRequestScenario();
            this.fakeWebservices.SaveResult = FakeLqbWebService.FakeSaveResult.ExceptionFirstTimes(1, service => new Exception("Can't save this"));
            this.fakeWebservices.UploadResult = FakeLqbWebService.FakeSaveResult.Success();

            bool result = Program.ImportResultToLQB(scenario.Lender, scenario.RequestData, scenario.Responses, scenario.RequestType);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void ImportResultToLQB_SimpleGetRequestSaveUnsuccessfulOncePdfSuccessful_DoesNotEmail()
        {
            var scenario = this.CreateSimpleGetRequestScenario();
            this.fakeWebservices.SaveResult = FakeLqbWebService.FakeSaveResult.ExceptionFirstTimes(1, service => new Exception("Can't save this"));
            this.fakeWebservices.UploadResult = FakeLqbWebService.FakeSaveResult.Success();

            bool result = Program.ImportResultToLQB(scenario.Lender, scenario.RequestData, scenario.Responses, scenario.RequestType);
            Assert.AreEqual(0, this.mockEmailSender.EmailsSentCount);
            Assert.IsFalse(this.mockEmailSender.SentEmailTo(ClientErrorEmail));
        }

        [TestMethod]
        public void ImportResultToLQB_SimpleGetRequestSaveUnsuccessfulTwicePdfSuccessful_Unsuccessful()
        {
            var scenario = this.CreateSimpleGetRequestScenario();
            this.fakeWebservices.SaveResult = FakeLqbWebService.FakeSaveResult.ExceptionFirstTimes(2, service => new Exception("Can't save this"));
            this.fakeWebservices.UploadResult = FakeLqbWebService.FakeSaveResult.Success();

            bool result = Program.ImportResultToLQB(scenario.Lender, scenario.RequestData, scenario.Responses, scenario.RequestType);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void ImportResultToLQB_SimpleGetRequestSaveUnsuccessfulTwicePdfSuccessful_EmailsClient()
        {
            var scenario = this.CreateSimpleGetRequestScenario();
            this.fakeWebservices.SaveResult = FakeLqbWebService.FakeSaveResult.ExceptionFirstTimes(2, service => new Exception("Can't save this"));
            this.fakeWebservices.UploadResult = FakeLqbWebService.FakeSaveResult.Success();

            bool result = Program.ImportResultToLQB(scenario.Lender, scenario.RequestData, scenario.Responses, scenario.RequestType);
            Assert.AreEqual(1, this.mockEmailSender.EmailsSentCount);
            Assert.IsTrue(this.mockEmailSender.SentEmailTo(ClientErrorEmail));
        }

        [TestMethod]
        public void ImportResultToLQB_SimpleGetRequestSaveSuccessfulPdfUnsuccessful_Unsuccessful()
        {
            var scenario = this.CreateSimpleGetRequestScenario();
            this.fakeWebservices.SaveResult = FakeLqbWebService.FakeSaveResult.Success();
            this.fakeWebservices.UploadResult = FakeLqbWebService.FakeSaveResult.Exception(service => new Exception("Can't save this"));

            bool result = Program.ImportResultToLQB(scenario.Lender, scenario.RequestData, scenario.Responses, scenario.RequestType);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void ImportResultToLQB_SimpleGetRequestSaveSuccessfulPdfUnsuccessful_EmailsClient()
        {
            var scenario = this.CreateSimpleGetRequestScenario();
            this.fakeWebservices.SaveResult = FakeLqbWebService.FakeSaveResult.Success();
            this.fakeWebservices.UploadResult = FakeLqbWebService.FakeSaveResult.Exception(service => new Exception("Can't save this"));

            bool result = Program.ImportResultToLQB(scenario.Lender, scenario.RequestData, scenario.Responses, scenario.RequestType);
            Assert.AreEqual(1, this.mockEmailSender.EmailsSentCount);
            Assert.IsTrue(this.mockEmailSender.SentEmailTo(ClientErrorEmail));
        }

        [TestMethod]
        public void ImportResultToLQB_SimpleGetRequestSaveSuccessfulPdfUnsuccessfulOnce_Successful()
        {
            var scenario = this.CreateSimpleGetRequestScenario();
            this.fakeWebservices.SaveResult = FakeLqbWebService.FakeSaveResult.Success();
            this.fakeWebservices.UploadResult = FakeLqbWebService.FakeSaveResult.ExceptionFirstTimes(1, service => new Exception("Can't save this"));

            bool result = Program.ImportResultToLQB(scenario.Lender, scenario.RequestData, scenario.Responses, scenario.RequestType);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void ImportResultToLQB_SimpleGetRequestSaveSuccessfulPdfUnsuccessfulOnce_DoesNotEmail()
        {
            var scenario = this.CreateSimpleGetRequestScenario();
            this.fakeWebservices.SaveResult = FakeLqbWebService.FakeSaveResult.Success();
            this.fakeWebservices.UploadResult = FakeLqbWebService.FakeSaveResult.ExceptionFirstTimes(1, service => new Exception("Can't save this"));

            bool result = Program.ImportResultToLQB(scenario.Lender, scenario.RequestData, scenario.Responses, scenario.RequestType);
            Assert.AreEqual(0, this.mockEmailSender.EmailsSentCount);
            Assert.IsFalse(this.mockEmailSender.SentEmailTo(ClientErrorEmail));
        }

        [TestMethod]
        public void ImportResultToLQB_SimpleGetRequestSaveSuccessfulPdfUnsuccessfulTwice_Unsuccessful()
        {
            var scenario = this.CreateSimpleGetRequestScenario();
            this.fakeWebservices.SaveResult = FakeLqbWebService.FakeSaveResult.Success();
            this.fakeWebservices.UploadResult = FakeLqbWebService.FakeSaveResult.ExceptionFirstTimes(2, service => new Exception("Can't save this"));

            bool result = Program.ImportResultToLQB(scenario.Lender, scenario.RequestData, scenario.Responses, scenario.RequestType);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void ImportResultToLQB_SimpleGetRequestSaveSuccessfulPdfUnsuccessfulTwice_EmailsClient()
        {
            var scenario = this.CreateSimpleGetRequestScenario();
            this.fakeWebservices.SaveResult = FakeLqbWebService.FakeSaveResult.Success();
            this.fakeWebservices.UploadResult = FakeLqbWebService.FakeSaveResult.ExceptionFirstTimes(2, service => new Exception("Can't save this"));

            bool result = Program.ImportResultToLQB(scenario.Lender, scenario.RequestData, scenario.Responses, scenario.RequestType);
            Assert.AreEqual(1, this.mockEmailSender.EmailsSentCount);
            Assert.IsTrue(this.mockEmailSender.SentEmailTo(ClientErrorEmail));
        }

        /// <remarks>
        /// The fallback site, webservices.lendingqb.com, is difficult to test directly.  Instead, I'm settling
        /// for two tests: One that successfully falls back to a different url, and one that cannot successfully
        /// fall back when the Edocs and webservices domains are both down.
        /// </remarks>
        [TestMethod]
        public void ImportResultToLQB_SimpleGetRequestEdocsSiteFailsToResolve_FallbackUrlSuccessful()
        {
            var scenario = this.CreateSimpleGetRequestScenario();
            this.fakeWebservices.SaveResult = FakeLqbWebService.FakeSaveResult.Success();
            this.fakeWebservices.UploadResult = FakeLqbWebService.FakeSaveResult.Exception(
                service => service.Url.StartsWith("https://edocs.lendingqb.com", StringComparison.OrdinalIgnoreCase)
                    ? new System.Net.WebException("Couldn't touch this", System.Net.WebExceptionStatus.NameResolutionFailure)
                    : null);

            bool result = Program.ImportResultToLQB(scenario.Lender, scenario.RequestData, scenario.Responses, scenario.RequestType);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void ImportResultToLQB_SimpleGetRequestEdocsAndWebservicesSitesFailToResolve_Unsuccessful()
        {
            var scenario = this.CreateSimpleGetRequestScenario();
            this.fakeWebservices.SaveResult = FakeLqbWebService.FakeSaveResult.Success();
            this.fakeWebservices.UploadResult = FakeLqbWebService.FakeSaveResult.Exception(
                service => service.Url.StartsWith("https://edocs.lendingqb.com", StringComparison.OrdinalIgnoreCase) || service.Url.StartsWith("https://webservices.lendingqb.com", StringComparison.OrdinalIgnoreCase)
                    ? new System.Net.WebException("Couldn't touch this", System.Net.WebExceptionStatus.NameResolutionFailure)
                    : null);

            bool result = Program.ImportResultToLQB(scenario.Lender, scenario.RequestData, scenario.Responses, scenario.RequestType);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void ImportResultToLQB_SimpleGetRequestEdocsSiteFailsToResolveAndFallbackFails_Unsuccessful()
        {
            var scenario = this.CreateSimpleGetRequestScenario();
            this.fakeWebservices.SaveResult = FakeLqbWebService.FakeSaveResult.Success();
            this.fakeWebservices.UploadResult = FakeLqbWebService.FakeSaveResult.Exception(
                service => service.Url.StartsWith("https://edocs.lendingqb.com", StringComparison.OrdinalIgnoreCase)
                    ? new System.Net.WebException("Couldn't touch this", System.Net.WebExceptionStatus.NameResolutionFailure)
                    : new Exception("Can't do this"));

            bool result = Program.ImportResultToLQB(scenario.Lender, scenario.RequestData, scenario.Responses, scenario.RequestType);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void ImportResultToLQB_SimpleGetRequestEdocsSiteFailsToResolveAndFallbackFailsOnce_Successful()
        {
            var scenario = this.CreateSimpleGetRequestScenario();
            this.fakeWebservices.SaveResult = FakeLqbWebService.FakeSaveResult.Success();
            this.fakeWebservices.UploadResult = FakeLqbWebService.FakeSaveResult.ExceptionFirstTimes(2,
                service => service.Url.StartsWith("https://edocs.lendingqb.com", StringComparison.OrdinalIgnoreCase)
                    ? new System.Net.WebException("Couldn't touch this", System.Net.WebExceptionStatus.NameResolutionFailure)
                    : new Exception("Can't do this"));

            bool result = Program.ImportResultToLQB(scenario.Lender, scenario.RequestData, scenario.Responses, scenario.RequestType);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void ImportResultToLQB_SimpleGetRequestEdocsSiteFailsToResolveAndFallbackFailsTwice_Unsuccessful()
        {
            var scenario = this.CreateSimpleGetRequestScenario();
            this.fakeWebservices.SaveResult = FakeLqbWebService.FakeSaveResult.Success();
            this.fakeWebservices.UploadResult = FakeLqbWebService.FakeSaveResult.ExceptionFirstTimes(3,
                service => service.Url.StartsWith("https://edocs.lendingqb.com", StringComparison.OrdinalIgnoreCase)
                    ? new System.Net.WebException("Couldn't touch this", System.Net.WebExceptionStatus.NameResolutionFailure)
                    : new Exception("Can't do this"));

            bool result = Program.ImportResultToLQB(scenario.Lender, scenario.RequestData, scenario.Responses, scenario.RequestType);
            Assert.IsFalse(result);
        }
        #endregion ImportResultToLQB

        private ImportResultScenario CreateSimpleGetRequestScenario()
        {
            return new ImportResultScenario(this.lender, CreateGetRequestData(), new[] { CreateResponse() }, E_RequestType.GET);
        }

        public static GetDeactivateRequestData CreateGetRequestData()
        {
            var loanApp = new LoanNmAppId("123456789", new Guid("aaa1157e-f6a5-4d24-a81d-6b08f4378b41"));
            var dataDictionary = new Dictionary<string, string>
                {
                    { "sSpAddr", "1600 W Sunflower Avenue, Suite 200" },
                    { "sSpCity", "Costa Mesa" },
                    { "sSpState", "CA" },
                    { "sSpZip", "92626" },
                    { "aCrOd", "6/23/2016" },
                    { "aUdnOrderId", "12345" },
                    { "aAppId", loanApp.AppId.ToString() },
                    { "aBAddr", "1124 Bristol St" },
                    { "aBCity", "Costa Mesa" },
                    { "aBState", "CA" },
                    { "aBZip", "92626" },
                    { "aBAddrYrs", "15" },
                    { "aBFirstNm", "Harold" },
                    { "aBLastNm", "Hall" },
                    { "aBSsn", "123-45-6789" },
                    { "aBTypeT", "0" },
                    { "aBDob", "1/1/1970" },
                };

            var request = new GetDeactivateRequestData(dataDictionary, loanApp, ClientErrorEmail, "errorbackup@lendingqb.com", E_RequestType.GET);
            Assert.AreEqual(request.IsValid(), true);

            return request;
        }

        private static ResponseData CreateResponse()
        {
            return MclXml.ReadUDNResponse(@"
<OUTPUT>
  <RESPONSE>
    <CREDITDATA>
      <HEADER report_id=""10"" reference_number=""UDN Test Loan 6282016"" customer_number=""LQB"" customer_name=""LQB Tester"" date_ordered=""7/5/2016"" report_owner=""LQB TESTER"" />
      <NOTIFICATION_MONITORINGS>
        <NOTIFICATION_MONITORING bureau=""EQUIFAX"" source_bureau=""EF"" start_date=""6/22/2016"" end_date="""" status=""ACTIVE"" />
      </NOTIFICATION_MONITORINGS>
      <NOTIFICATIONS>
        <TRADELINES>
          <TRADELINE source_bureau=""EF"" notification_date=""7/5/2016"" creditor_info="""" acct_type=""mortgage"" opened_date=""6/22/2016"" balance=""$100.00"" credit_limit=""$1000.00"" status="""" reported_date=""7/5/2016"" terms="""" industry=""Finance"" remark="""" />
        </TRADELINES>
      </NOTIFICATIONS>
      <EMBEDDED_FILE>
        <DOCUMENT>File Content</DOCUMENT>
      </EMBEDDED_FILE>
    </CREDITDATA>
  </RESPONSE>
</OUTPUT>");
        }

        private class ImportResultScenario
        {
            public ImportResultScenario(Lender lender, RequestData request, IEnumerable<ResponseData> responses, E_RequestType requestType)
            {
                this.Lender = lender;
                this.RequestData = request;
                this.Responses = responses;
                this.RequestType = requestType;
            }

            public Lender Lender { get; }
            public RequestData RequestData { get; }
            public IEnumerable<ResponseData> Responses { get; }
            public E_RequestType RequestType { get; }
        }


        private class SingleLenderFakeDB : FakeDB
        {
            public SingleLenderFakeDB(Dictionary<Guid, string> serviceCompanyUrlsById, Guid brokerId, IEnumerable<Guid> branches)
            {
                this.ServiceCompanyUrlsById = serviceCompanyUrlsById;
                this.BrokerId = brokerId;
                this.Branches = branches;
            }

            public Dictionary<Guid, string> ServiceCompanyUrlsById { get; }
            public Guid BrokerId { get; }
            public IEnumerable<Guid> Branches { get; }

            public override Dictionary<Guid, string> GetServiceCompanyIdsAndUrls()
            {
                return new Dictionary<Guid, string>(this.ServiceCompanyUrlsById);
            }

            public override Guid GetBrokerID(string sCustomerCode)
            {
                return this.BrokerId;
            }

            public override HashSet<Guid> GetBranchIDsByBrokerID(Guid brokerID)
            {
                return new HashSet<Guid>(this.Branches);
            }
        }

    }
}
