﻿namespace UndisclosedDebtNotification.Test
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Linq;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class RequestTest
    {
        private const string ErrorEmail = "test@lendingqb.com";
        private const string BackupEmail = "testbackup@lendingqb.com";
        private const string Username = "test";
        private const string Password = "password";

        private static readonly Guid TestGuid = new Guid("11111111-1111-1111-1111-111111111111");
        private static readonly LoanNmAppId Application = new LoanNmAppId("UDN Test Loan", RequestTest.TestGuid);
        private static readonly Credentials MclCredentials =
            new Credentials(RequestTest.Username, RequestTest.Password);

        private readonly List<KeyValuePair<string, string>> borrowerData;
        private readonly List<KeyValuePair<string, string>> coborrowerData;
        private readonly Dictionary<string, string> baseDataDictionary;

        public RequestTest()
        {
            this.baseDataDictionary = new Dictionary<string, string>()
            {
                { "sSpAddr", "1600 W Sunflower Avenue, Suite 200" },
                { "sSpCity", "Costa Mesa" },
                { "sSpState", "CA" },
                { "sSpZip", "92626" },
                { "aCrOd", "6/23/2016" },
                { "aUdnOrderId", "12345" },
                { "MclCreditReport_Found", "true" },
                { "MclCreditReport_ExternalFileId", "67890" },
                { "MclCreditReport_ComId", RequestTest.TestGuid.ToString() }
            };

            this.borrowerData = new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>("aBAddr", "1124 Bristol St"),
                new KeyValuePair<string, string>("aBCity", "Costa Mesa"),
                new KeyValuePair<string, string>("aBState", "CA"),
                new KeyValuePair<string, string>("aBZip", "92626"),
                new KeyValuePair<string, string>("aBAddrYrs", "15"),
                new KeyValuePair<string, string>("aBFirstNm", "Harold"),
                new KeyValuePair<string, string>("aBLastNm", "Hall"),
                new KeyValuePair<string, string>("aBSsn", "123-45-6789"),
                new KeyValuePair<string, string>("aBTypeT", "0"),
                new KeyValuePair<string, string>("aBDob", "1/1/1970"),
                new KeyValuePair<string, string>("MclCreditReport_BorrowerSSN", "123-45-6789")
            };

            this.coborrowerData = new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>("aCAddr", "1124 Bristol St"),
                new KeyValuePair<string, string>("aCCity", "Costa Mesa"),
                new KeyValuePair<string, string>("aCState", "CA"),
                new KeyValuePair<string, string>("aCZip", "92626"),
                new KeyValuePair<string, string>("aCAddrYrs", "15"),
                new KeyValuePair<string, string>("aCFirstNm", "Sarah"),
                new KeyValuePair<string, string>("aCMidNm", "Christina"),
                new KeyValuePair<string, string>("aCLastNm", "Testcase"),
                new KeyValuePair<string, string>("aCSuffix", "II"),
                new KeyValuePair<string, string>("aCSsn", "987-65-4321"),
                new KeyValuePair<string, string>("aCTypeT", "1"),
                new KeyValuePair<string, string>("aCDob", "1/1/1970"),
                new KeyValuePair<string, string>("MclCreditReport_CoBorrowerSSN", "987-65-4321")
            };
        }

        [TestInitialize]
        public void Setup()
        {
            ServiceLocator.ReplaceServiceLocator(new Fakes.FakeServiceLocator());
            ServiceLocator.Instance.AppSettings["SUPPORT_EMAIL"] = "michaell2@meridianlink.com";
        }

        [TestMethod]
        public void Request_RequestType_IsCorrect()
        {
            var testData = this.GetTestData(true, false);

            RequestData request = new NewRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail);

            Assert.AreEqual(E_RequestType.NEW, request.RequestType);
            Assert.AreEqual(E_RequestType.NEW.ToString(), request.RequestType_rep);

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            Assert.AreEqual(E_RequestType.DEACTIVATE, request.RequestType);
            Assert.AreEqual(E_RequestType.DEACTIVATE.ToString(), request.RequestType_rep);

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            Assert.AreEqual(E_RequestType.GET, request.RequestType);
            Assert.AreEqual(E_RequestType.GET.ToString(), request.RequestType_rep);
        }

        [TestMethod]
        public void Request_RequestTypeSingleApplicationBorrowerOnly_IsApplicantBorrowerType()
        {
            var testData = this.GetTestData(true, false);

            RequestData request = new NewRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail);

            Assert.AreEqual(E_Borrower.APPLICANT, request.Borrower);
            Assert.AreEqual(E_Borrower.APPLICANT.ToString(), request.Borrower_rep);

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            Assert.AreEqual(E_Borrower.APPLICANT, request.Borrower);
            Assert.AreEqual(E_Borrower.APPLICANT.ToString(), request.Borrower_rep);

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            Assert.AreEqual(E_Borrower.APPLICANT, request.Borrower);
            Assert.AreEqual(E_Borrower.APPLICANT.ToString(), request.Borrower_rep);

            testData.Remove("aUdnOrderId");

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            Assert.AreEqual(E_Borrower.APPLICANT, request.Borrower);
            Assert.AreEqual(E_Borrower.APPLICANT.ToString(), request.Borrower_rep);

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            Assert.AreEqual(E_Borrower.APPLICANT, request.Borrower);
            Assert.AreEqual(E_Borrower.APPLICANT.ToString(), request.Borrower_rep);
        }

        [TestMethod]
        public void Request_RequestTypeSingleApplicationCoborrowerOnly_IsApplicantBorrowerType()
        {
            var testData = this.GetTestData(false, true);

            RequestData request = new NewRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail);

            Assert.AreEqual(E_Borrower.APPLICANT, request.Borrower);
            Assert.AreEqual(E_Borrower.APPLICANT.ToString(), request.Borrower_rep);

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            Assert.AreEqual(E_Borrower.APPLICANT, request.Borrower);
            Assert.AreEqual(E_Borrower.APPLICANT.ToString(), request.Borrower_rep);

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            Assert.AreEqual(E_Borrower.APPLICANT, request.Borrower);
            Assert.AreEqual(E_Borrower.APPLICANT.ToString(), request.Borrower_rep);

            testData.Remove("aUdnOrderId");

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            Assert.AreEqual(E_Borrower.APPLICANT, request.Borrower);
            Assert.AreEqual(E_Borrower.APPLICANT.ToString(), request.Borrower_rep);

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            Assert.AreEqual(E_Borrower.APPLICANT, request.Borrower);
            Assert.AreEqual(E_Borrower.APPLICANT.ToString(), request.Borrower_rep);
        }

        [TestMethod]
        public void Request_RequestTypeJointApplication_IsJointBorrowerType()
        {
            var testData = this.GetTestData(true, true);

            RequestData request = new NewRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail);

            Assert.AreEqual(E_Borrower.JOINT, request.Borrower);
            Assert.AreEqual(E_Borrower.JOINT.ToString(), request.Borrower_rep);

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            Assert.AreEqual(E_Borrower.JOINT, request.Borrower);
            Assert.AreEqual(E_Borrower.JOINT.ToString(), request.Borrower_rep);

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            Assert.AreEqual(E_Borrower.JOINT, request.Borrower);
            Assert.AreEqual(E_Borrower.JOINT.ToString(), request.Borrower_rep);

            testData.Remove("aUdnOrderId");

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            Assert.AreEqual(E_Borrower.JOINT, request.Borrower);
            Assert.AreEqual(E_Borrower.JOINT.ToString(), request.Borrower_rep);

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            Assert.AreEqual(E_Borrower.JOINT, request.Borrower);
            Assert.AreEqual(E_Borrower.JOINT.ToString(), request.Borrower_rep);
        }

        [TestMethod]
        public void Request_ReferenceNumber_IsCorrect()
        {
            var testData = this.GetTestData(true, false);

            RequestData request = new NewRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail);

            Assert.AreEqual(RequestTest.Application.LoanNm, request.Application.LoanNm);

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            Assert.AreEqual(RequestTest.Application.LoanNm, request.Application.LoanNm);

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            Assert.AreEqual(RequestTest.Application.LoanNm, request.Application.LoanNm);
        }

        [TestMethod]
        public void Request_ApplicationId_IsCorrect()
        {
            var testData = this.GetTestData(true, false);

            RequestData request = new NewRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail);

            Assert.AreEqual(RequestTest.Application.AppId, request.Application.AppId);

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            Assert.AreEqual(RequestTest.Application.AppId, request.Application.AppId);

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            Assert.AreEqual(RequestTest.Application.AppId, request.Application.AppId);
        }

        [TestMethod]
        public void Request_ErrorNotificationEmail_IsCorrect()
        {
            var testData = this.GetTestData(true, false);

            RequestData request = new NewRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail);

            Assert.AreEqual(RequestTest.ErrorEmail, request.ErrorNotificationEmail);

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            Assert.AreEqual(RequestTest.ErrorEmail, request.ErrorNotificationEmail);

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            Assert.AreEqual(RequestTest.ErrorEmail, request.ErrorNotificationEmail);
        }

        [TestMethod]
        public void Request_BackupErrorNotificationEmail_IsCorrect()
        {
            var testData = this.GetTestData(true, false);

            RequestData request = new NewRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail);

            Assert.AreEqual(RequestTest.BackupEmail, request.BackupErrorNotificationEmail);

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            Assert.AreEqual(RequestTest.BackupEmail, request.BackupErrorNotificationEmail);

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            Assert.AreEqual(RequestTest.BackupEmail, request.BackupErrorNotificationEmail);
        }

        [TestMethod]
        public void Request_ApplicantInfoBorrowerOnly_IsCorrect()
        {
            var testData = this.GetTestData(true, false);

            RequestData request = new NewRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail);

            TestBorrowerOnlyInformation(request.Applicant);

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            TestBorrowerOnlyInformation(request.Applicant);

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            TestBorrowerOnlyInformation(request.Applicant);

            testData.Remove("aUdnOrderId");

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            TestBorrowerOnlyInformation(request.Applicant);

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            TestBorrowerOnlyInformation(request.Applicant);
        }

        [TestMethod]
        public void Request_ApplicantInfoCoborrowerOnly_IsCorrect()
        {
            var testData = this.GetTestData(false, true);

            RequestData request = new NewRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail);

            TestCoborrowerOnlyInformation(request.Applicant);

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            TestCoborrowerOnlyInformation(request.Applicant);

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            TestCoborrowerOnlyInformation(request.Applicant);

            testData.Remove("aUdnOrderId");

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            TestCoborrowerOnlyInformation(request.Applicant);

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            TestCoborrowerOnlyInformation(request.Applicant);
        }

        [TestMethod]
        public void Request_ApplicantInfoJoint_IsCorrect()
        {
            var testData = this.GetTestData(true, true);

            RequestData request = new NewRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail);

            TestJointInformation(request);

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            TestJointInformation(request);

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            TestJointInformation(request);

            testData.Remove("aUdnOrderId");

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            TestJointInformation(request);

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            TestJointInformation(request);
        }

        [TestMethod]
        public void Request_ApplicantInfoBorrowerOnly_BorrowerSsnIsAppIdentifyingSsn()
        {
            var testData = this.GetTestData(true, false);

            RequestData request = new NewRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail);

            Assert.AreEqual("123-45-6789", request.AppIdentifyingSsn);

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            Assert.AreEqual("123-45-6789", request.AppIdentifyingSsn);

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            Assert.AreEqual("123-45-6789", request.AppIdentifyingSsn);

            testData.Remove("aUdnOrderId");

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            Assert.AreEqual("123-45-6789", request.AppIdentifyingSsn);

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            Assert.AreEqual("123-45-6789", request.AppIdentifyingSsn);
        }

        [TestMethod]
        public void Request_ApplicantInfoCoborrowerOnly_AppIdentifyingSsnIsBlank()
        {
            var testData = this.GetTestData(false, true);

            RequestData request = new NewRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail);

            Assert.AreEqual(string.Empty, request.AppIdentifyingSsn);

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            Assert.AreEqual(string.Empty, request.AppIdentifyingSsn);

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            Assert.AreEqual(string.Empty, request.AppIdentifyingSsn);

            testData.Remove("aUdnOrderId");

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            Assert.AreEqual(string.Empty, request.AppIdentifyingSsn);

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            Assert.AreEqual(string.Empty, request.AppIdentifyingSsn);
        }

        [TestMethod]
        public void Request_ApplicantInfoJoint_BorrowerSsnIsAppIdentifyingSsn()
        {
            var testData = this.GetTestData(true, true);

            RequestData request = new NewRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail);

            Assert.AreEqual("123-45-6789", request.AppIdentifyingSsn);

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            Assert.AreEqual("123-45-6789", request.AppIdentifyingSsn);

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            Assert.AreEqual("123-45-6789", request.AppIdentifyingSsn);

            testData.Remove("aUdnOrderId");

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            Assert.AreEqual("123-45-6789", request.AppIdentifyingSsn);

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            Assert.AreEqual("123-45-6789", request.AppIdentifyingSsn);
        }

        [TestMethod]
        public void Request_BorrowerInfoDataSpecified_IsValid()
        {
            var testData = this.GetTestData(true, false);

            RequestData request = new NewRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail);

            Assert.IsTrue(request.IsValid());

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            Assert.IsTrue(request.IsValid());

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            Assert.IsTrue(request.IsValid());

            testData.Remove("aUdnOrderId");

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            Assert.IsTrue(request.IsValid());

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            Assert.IsTrue(request.IsValid());
        }

        [TestMethod]
        public void Request_CoborrowerInfoDataSpecified_IsValid()
        {
            var testData = this.GetTestData(false, true);

            RequestData request = new NewRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail);

            Assert.IsTrue(request.IsValid());

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            Assert.IsTrue(request.IsValid());

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            Assert.IsTrue(request.IsValid());

            testData.Remove("aUdnOrderId");

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            Assert.IsTrue(request.IsValid());

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            Assert.IsTrue(request.IsValid());
        }

        [TestMethod]
        public void Request_JointInfoDataSpecified_IsValid()
        {
            var testData = this.GetTestData(true, true);

            RequestData request = new NewRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail);

            Assert.IsTrue(request.IsValid());

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            Assert.IsTrue(request.IsValid());

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            Assert.IsTrue(request.IsValid());

            testData.Remove("aUdnOrderId");

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            Assert.IsTrue(request.IsValid());

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            Assert.IsTrue(request.IsValid());
        }

        [TestMethod]
        public void Request_ApplicantNameBorrowerOnly_IsCorrect()
        {
            var testData = this.GetTestData(true, false);

            RequestData request = new NewRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail);

            Assert.AreEqual("Hall, Harold", Tools.FormatApplicantName(request));

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            Assert.AreEqual("Hall, Harold", Tools.FormatApplicantName(request));

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            Assert.AreEqual("Hall, Harold", Tools.FormatApplicantName(request));
        }

        [TestMethod]
        public void Request_ApplicantNameCoborrowerOnly_IsCorrect()
        {
            var testData = this.GetTestData(false, true);

            RequestData request = new NewRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail);

            Assert.AreEqual("Testcase, Sarah", Tools.FormatApplicantName(request));

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            Assert.AreEqual("Testcase, Sarah", Tools.FormatApplicantName(request));

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            Assert.AreEqual("Testcase, Sarah", Tools.FormatApplicantName(request));
        }

        [TestMethod]
        public void Request_ApplicantNameJoint_IsCorrect()
        {
            var testData = this.GetTestData(true, true);

            RequestData request = new NewRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail);

            Assert.AreEqual("Hall, Harold / Testcase, Sarah", Tools.FormatApplicantName(request));

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            Assert.AreEqual("Hall, Harold / Testcase, Sarah", Tools.FormatApplicantName(request));

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            Assert.AreEqual("Hall, Harold / Testcase, Sarah", Tools.FormatApplicantName(request));
        }

        [TestMethod]
        public void NewRequest_CreditOrderedDate_IsCorrect()
        {
            var testData = this.GetTestData(true, false);

            var request = new NewRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail);

            Assert.IsTrue(request.StartDate.HasValue);
            Assert.AreEqual(DateTime.Parse("6/23/2016"), request.StartDate);
            Assert.AreEqual("6/23/2016", request.StartDate_rep);
        }

        [TestMethod]
        public void NewRequest_CurrentAddress_IsCorrect()
        {
            var testData = this.GetTestData(true, false);

            var request = new NewRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail);

            Assert.IsTrue(request.CurrentAddress.IsValid());
            Assert.AreEqual("1124 Bristol St", request.CurrentAddress.StreetAddress, true);
            Assert.AreEqual("Costa Mesa", request.CurrentAddress.City, true);
            Assert.AreEqual("CA", request.CurrentAddress.State);
            Assert.AreEqual("92626", request.CurrentAddress.Zip);
        }

        [TestMethod]
        public void NewRequest_SubjectAddress_IsCorrect()
        {
            var testData = this.GetTestData(true, false);

            var request = new NewRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail);

            Assert.IsTrue(request.PropertyAddress.IsValid());
            Assert.AreEqual("1600 W Sunflower Avenue, Suite 200", request.PropertyAddress.StreetAddress, true);
            Assert.AreEqual("Costa Mesa", request.PropertyAddress.City, true);
            Assert.AreEqual("CA", request.PropertyAddress.State);
            Assert.AreEqual("92626", request.PropertyAddress.Zip);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void NewRequest_BorrowerOnlyNameDataIncomplete_ThrowsException()
        {
            var testData = this.GetTestData(true, false);

            testData.Remove("aBFirstNm");

            var request = new NewRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail);

            Assert.Fail("Expected InvalidOperationException due to incomplete borrower name data.");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void NewRequest_CoborrowerOnlyNameDataIncomplete_ThrowsException()
        {
            var testData = this.GetTestData(false, true);

            testData.Remove("aCFirstNm");

            var request = new NewRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail);

            Assert.Fail("Expected InvalidOperationException due to incomplete coborrower name data.");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void NewRequest_JointNameDataIncomplete_ThrowsException()
        {
            var testData = this.GetTestData(true, true);
            
            testData.Remove("aBFirstNm");
            testData.Remove("aCFirstNm");

            var request = new NewRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail);

            Assert.Fail("Expected InvalidOperationException due to incomplete borrower and coborrower name data.");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void NewRequest_BorrowerOnlyInvalidSsn_ThrowsException()
        {
            var testData = this.GetTestData(true, false);

            testData["aBSsn"] = "12345";

            var request = new NewRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail);

            Assert.Fail("Expected InvalidOperationException due to invalid borrower SSN.");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void NewRequest_CoborrowerOnlyInvalidSsn_ThrowsException()
        {
            var testData = this.GetTestData(false, true);

            testData["aCSsn"] = "12345";

            var request = new NewRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail);

            Assert.Fail("Expected InvalidOperationException due to invalid coborrower SSN.");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void NewRequest_JointInvalidSsn_ThrowsException()
        {
            var testData = this.GetTestData(true, true);

            testData["aBSsn"] = "12345";
            testData["aCSsn"] = "12345";

            var request = new NewRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail);

            Assert.Fail("Expected InvalidOperationException due to invalid borrower and coborrower SSN.");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void NewRequest_CreditOrderedDateMissing_ThrowsException()
        {
            var testData = this.GetTestData(true, false);

            testData.Remove("aCrOd");

            var request = new NewRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail);

            Assert.Fail("Expected InvalidOperationException due to missing credit ordered date.");
        }

        [TestMethod]
        public void GetDeactivateRequest_UdnOrderId_IsCorrect()
        {
            var testData = this.GetTestData(true, false);

            var request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            Assert.IsTrue(request.ReportId.HasValue);
            Assert.AreEqual(12345, request.ReportId);

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            Assert.IsTrue(request.ReportId.HasValue);
            Assert.AreEqual(12345, request.ReportId);
        }

        [TestMethod]
        public void GetDeactivateRequest_UdnOrderIdSpecified_DoesNotHaveAssociatedMclCra()
        {
            var testData = this.GetTestData(true, false);

            var request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            Assert.IsFalse(request.MclComId.HasValue);

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            Assert.IsFalse(request.MclComId.HasValue);
        }

        [TestMethod]
        public void GetDeactivateRequest_MclFallback_IsValid()
        {
            var testData = this.GetTestData(true, false);

            testData.Remove("aUdnOrderId");

            var request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            Assert.IsTrue(request.IsValid());
            TestBorrowerOnlyInformation(request.Applicant);

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            Assert.IsTrue(request.IsValid());
            TestBorrowerOnlyInformation(request.Applicant);
        }

        [TestMethod]
        public void GetDeactivateRequest_MclFallbackNoMclCraSpecified_IsInvalid()
        {
            var testData = this.GetTestData(true, false);

            testData.Remove("aUdnOrderId");
            testData.Remove("MclCreditReport_ComId");

            var request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            Assert.IsFalse(request.IsValid());

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            Assert.IsFalse(request.IsValid());
        }

        [TestMethod]
        public void GetDeactivateRequest_MclFallback_ReportIdIsCorrect()
        {
            var testData = this.GetTestData(true, false);

            testData.Remove("aUdnOrderId");

            var request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            Assert.IsTrue(request.ReportId.HasValue);
            Assert.AreEqual(67890, request.ReportId);

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            Assert.IsTrue(request.ReportId.HasValue);
            Assert.AreEqual(67890, request.ReportId);
        }

        [TestMethod]
        public void GetDeactivateRequest_MclFallback_MclComIdIsCorrect()
        {
            var testData = this.GetTestData(true, false);

            testData.Remove("aUdnOrderId");

            var request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            Assert.IsTrue(request.MclComId.HasValue);
            Assert.AreEqual(RequestTest.TestGuid, request.MclComId.Value);

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            Assert.IsTrue(request.MclComId.HasValue);
            Assert.AreEqual(RequestTest.TestGuid, request.MclComId.Value);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void GetDeactivateRequest_BorrowerOnlyNameDataIncomplete_ThrowsException()
        {
            var testData = this.GetTestData(true, false);

            testData.Remove("aBFirstNm");

            var request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            Assert.Fail("Expected InvalidOperationException due to incomplete borrower name data.");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void GetDeactivateRequest_CoborrowerOnlyNameDataIncomplete_ThrowsException()
        {
            var testData = this.GetTestData(false, true);

            testData.Remove("aCFirstNm");

            var request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            Assert.Fail("Expected InvalidOperationException due to incomplete coborrower name data.");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void GetDeactivateRequest_JointNameDataIncomplete_ThrowsException()
        {
            var testData = this.GetTestData(true, true);

            testData.Remove("aBFirstNm");
            testData.Remove("aCFirstNm");

            var request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            Assert.Fail("Expected InvalidOperationException due to incomplete borrower and coborrower name data.");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void GetDeactivateRequest_BorrowerOnlyInvalidSsn_ThrowsException()
        {
            var testData = this.GetTestData(true, false);

            testData["aBSsn"] = "12345";

            var request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            Assert.Fail("Expected InvalidOperationException due to invalid borrower SSN.");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void GetDeactivateRequest_CoborrowerOnlyInvalidSsn_ThrowsException()
        {
            var testData = this.GetTestData(false, true);

            testData["aCSsn"] = "12345";

            var request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            Assert.Fail("Expected InvalidOperationException due to invalid coborrower SSN.");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void GetDeactivateRequest_JointInvalidSsn_ThrowsException()
        {
            var testData = this.GetTestData(true, true);

            testData["aBSsn"] = "12345";
            testData["aCSsn"] = "12345";

            var request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            Assert.Fail("Expected InvalidOperationException due to invalid borrower and coborrower SSN.");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void GetDeactivateRequest_NoFileNumber_ThrowsException()
        {
            var testData = this.GetTestData(true, false);

            testData.Remove("aUdnOrderId");
            testData.Remove("MclCreditReport_ExternalFileId");

            var request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            Assert.Fail("Expected InvalidOperationException due to missing values for aUdnOrderId and MclCreditReport_ExternalFileId.");
        }

        [TestMethod]
        public void GetDeactivateRequest_MclFallbackBorrowerOnlyIncompleteNameData_IsInvalid()
        {
            var testData = this.GetTestData(true, false);

            testData.Remove("aUdnOrderId");
            testData.Remove("aBFirstNm");

            var request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            Assert.IsFalse(request.IsValid());

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            Assert.IsFalse(request.IsValid());
        }

        [TestMethod]
        public void GetDeactivateRequest_MclFallbackCoborrowerOnlyIncompleteNameData_IsInvalid()
        {
            var testData = this.GetTestData(false, true);

            testData.Remove("aUdnOrderId");
            testData.Remove("aCFirstNm");

            var request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            Assert.IsFalse(request.IsValid());

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            Assert.IsFalse(request.IsValid());
        }

        [TestMethod]
        public void GetDeactivateRequest_MclFallbackJointIncompleteNameData_IsInvalid()
        {
            var testData = this.GetTestData(true, true);

            testData.Remove("aUdnOrderId");
            testData.Remove("aBFirstNm");
            testData.Remove("aCFirstNm");

            var request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            Assert.IsFalse(request.IsValid());

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            Assert.IsFalse(request.IsValid());
        }

        [TestMethod]
        public void GetDeactivateRequest_MclFallbackBorrowerOnlyNoMatchingSsn_IsInvalid()
        {
            var testData = this.GetTestData(true, false);

            testData.Remove("aUdnOrderId");
            testData["aBSsn"] = "12345";

            var request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            Assert.IsFalse(request.IsValid());

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            Assert.IsFalse(request.IsValid());

            testData = this.GetTestData(true, false);

            testData.Remove("aUdnOrderId");
            testData["MclCreditReport_BorrowerSSN"] = "54321";

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            Assert.IsFalse(request.IsValid());

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            Assert.IsFalse(request.IsValid());
        }

        [TestMethod]
        public void GetDeactivateRequest_MclFallbackCoborrowerOnlyNoMatchingSsn_IsInvalid()
        {
            var testData = this.GetTestData(false, true);

            testData.Remove("aUdnOrderId");
            testData["aCSsn"] = "12345";

            var request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            Assert.IsFalse(request.IsValid());

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            Assert.IsFalse(request.IsValid());

            testData = this.GetTestData(false, true);

            testData.Remove("aUdnOrderId");
            testData["MclCreditReport_CoBorrowerSSN"] = "54321";

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            Assert.IsFalse(request.IsValid());

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            Assert.IsFalse(request.IsValid());
        }

        [TestMethod]
        public void GetDeactivateRequest_MclFallbackJointNoMatchingSsn_IsInvalid()
        {
            var testData = this.GetTestData(true, true);

            testData.Remove("aUdnOrderId");
            testData["aBSsn"] = "12345";
            testData["aCSsn"] = "12345";

            var request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            Assert.IsFalse(request.IsValid());

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            Assert.IsFalse(request.IsValid());

            testData = this.GetTestData(true, true);

            testData.Remove("aUdnOrderId");
            testData["MclCreditReport_BorrowerSSN"] = "54321";
            testData["MclCreditReport_CoBorrowerSSN"] = "54321";

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            Assert.IsFalse(request.IsValid());

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            Assert.IsFalse(request.IsValid());
        }

        [TestMethod]
        public void Request_BasicRequestXmlData_IsCorrect()
        {
            var testData = this.GetTestData(true, false);

            RequestData request = new NewRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail);

            TestBasicRequestXmlInformation(request, E_RequestType.NEW);

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            TestBasicRequestXmlInformation(request, E_RequestType.GET);

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            TestBasicRequestXmlInformation(request, E_RequestType.DEACTIVATE);

            testData.Remove("aUdnOrderId");

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            TestBasicRequestXmlInformation(request, E_RequestType.GET);

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            TestBasicRequestXmlInformation(request, E_RequestType.DEACTIVATE);
        }

        [TestMethod]
        public void Request_JointRequestXmlData_IsCorrect()
        {
            var testData = this.GetTestData(true, true);

            RequestData request = new NewRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail);

            TestJointRequestXmlInformation(request);

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            TestJointRequestXmlInformation(request);

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            TestJointRequestXmlInformation(request);

            testData.Remove("aUdnOrderId");

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.GET);

            TestJointRequestXmlInformation(request);

            request = new GetDeactivateRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail, E_RequestType.DEACTIVATE);

            TestJointRequestXmlInformation(request);
        }

        [TestMethod]
        [ExpectedException(typeof(NoValidApplicantTypesException))]
        public void Request_Borrower_NonTitleSpouseThrowsException()
        {
            var testData = this.GetTestData(true, false);

            testData["aBTypeT"] = LQBFields.ApplicantType.NonTitleSpouse.ToString();

            var request = new NewRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail);

            Assert.Fail("Expected NoValidApplicantTypesException due to invalid borrower type.");
        }

        [TestMethod]
        [ExpectedException(typeof(NoValidApplicantTypesException))]
        public void Request_Borrower_TitleOnlyThrowsException()
        {
            var testData = this.GetTestData(true, false);

            testData["aBTypeT"] = LQBFields.ApplicantType.TitleOnly.ToString();

            var request = new NewRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail);

            Assert.Fail("Expected NoValidApplicantTypesException due to invalid borrower type.");
        }

        [TestMethod]
        [ExpectedException(typeof(NoValidApplicantTypesException))]
        public void Request_Coborrower_NonTitleSpouseThrowsException()
        {
            var testData = this.GetTestData(false, true);

            testData["aCTypeT"] = LQBFields.ApplicantType.NonTitleSpouse.ToString();

            var request = new NewRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail);

            Assert.Fail("Expected NoValidApplicantTypesException due to invalid coborrower type.");
        }

        [TestMethod]
        [ExpectedException(typeof(NoValidApplicantTypesException))]
        public void Request_Coborrower_TitleOnlyThrowsException()
        {
            var testData = this.GetTestData(false, true);

            testData["aCTypeT"] = LQBFields.ApplicantType.TitleOnly.ToString();

            var request = new NewRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail);

            Assert.Fail("Expected NoValidApplicantTypesException due to invalid coborrower type.");
        }

        [TestMethod]
        public void Request_Joint_ValidBorrowerInvalidCoborrowerIsValid()
        {
            var testData = this.GetTestData(true, true);

            testData["aCTypeT"] = LQBFields.ApplicantType.NonTitleSpouse.ToString();

            var request = new NewRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail);

            Assert.IsTrue(request.IsValid());

            testData["aCTypeT"] = LQBFields.ApplicantType.TitleOnly.ToString();

            request = new NewRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail);

            Assert.IsTrue(request.IsValid());
        }

        [TestMethod]
        public void Request_Joint_InvalidBorrowerValidCoborrowerIsValid()
        {
            var testData = this.GetTestData(true, true);

            testData["aBTypeT"] = LQBFields.ApplicantType.NonTitleSpouse.ToString();

            var request = new NewRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail);

            Assert.IsTrue(request.IsValid());

            testData["aBTypeT"] = LQBFields.ApplicantType.TitleOnly.ToString();

            request = new NewRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail);

            Assert.IsTrue(request.IsValid());
        }

        [TestMethod]
        [ExpectedException(typeof(NoValidApplicantTypesException))]
        public void Request_Joint_InvalidBorrowerInvalidCoborrowerThrowsException()
        {
            var testData = this.GetTestData(true, true);

            testData["aBTypeT"] = LQBFields.ApplicantType.NonTitleSpouse.ToString();

            testData["aCTypeT"] = LQBFields.ApplicantType.TitleOnly.ToString();

            var request = new NewRequestData(testData, RequestTest.Application, RequestTest.ErrorEmail, RequestTest.BackupEmail);

            Assert.Fail("Expected NoValidApplicantTypesException due to invalid borrower and coborrower types.");
        }

        private Dictionary<string, string> GetTestData(bool includeBorrower, bool includeCoborrower)
        {
            if (!includeBorrower && !includeCoborrower)
            {
                throw new ArgumentException();
            }

            var testDataDictionary = new Dictionary<string, string>(this.baseDataDictionary);

            if (includeBorrower)
            {
                foreach (var keyValuePair in this.borrowerData)
                {
                    testDataDictionary[keyValuePair.Key] = keyValuePair.Value;
                }
            }

            if (includeCoborrower)
            {
                foreach (var keyValuePair in this.coborrowerData)
                {
                    testDataDictionary[keyValuePair.Key] = keyValuePair.Value;
                }
            }

            return testDataDictionary;
        }

        private void TestBorrowerOnlyInformation(Borrower applicant)
        {
            Assert.AreEqual("Harold", applicant.Firstname, true);
            Assert.AreEqual(string.Empty, applicant.Middlename);
            Assert.AreEqual("Hall", applicant.Surname, true);
            Assert.AreEqual(string.Empty, applicant.Suffix);
            Assert.AreEqual("123456789", applicant.Ssn);
            Assert.AreEqual("1/1/1970", applicant.Dob);
        }

        private void TestCoborrowerOnlyInformation(Borrower applicant)
        {
            Assert.AreEqual("Sarah", applicant.Firstname, true);
            Assert.AreEqual("Christina", applicant.Middlename, true);
            Assert.AreEqual("Testcase", applicant.Surname, true);
            Assert.AreEqual("II", applicant.Suffix, true);
            Assert.AreEqual("987654321", applicant.Ssn);
            Assert.AreEqual("1/1/1970", applicant.Dob);
        }

        private void TestJointInformation(RequestData request)
        {
            TestBorrowerOnlyInformation(request.Applicant);

            TestCoborrowerOnlyInformation(request.Spouse);
        }

        private void TestBasicRequestXmlInformation(RequestData request, E_RequestType expectedRequestType)
        {
            var requestXml = XDocument.Parse(MclXml.WriteUDNRequest(RequestTest.MclCredentials, request)).Root;

            Assert.IsNotNull(requestXml);

            var userInfoElement = requestXml.Element("USER_INFO");

            Assert.IsNotNull(userInfoElement);
            Assert.IsNotNull(userInfoElement.Attribute("login"));
            Assert.IsNotNull(userInfoElement.Attribute("password"));

            Assert.AreEqual(RequestTest.Username, userInfoElement.Attribute("login").Value);
            Assert.AreEqual(RequestTest.Password, userInfoElement.Attribute("password").Value);

            var requestElement = requestXml.Element("REQUEST");

            Assert.IsNotNull(requestElement);
            Assert.IsNotNull(requestElement.Attribute("request_type"));
            Assert.IsNotNull(requestElement.Element("DM_ORDER_DETAIL"));
            Assert.IsNotNull(requestElement.Element("APPLICANT"));

            Assert.IsNull(requestElement.Element("SPOUSE"));

            Assert.AreEqual(expectedRequestType.ToString(), requestElement.Attribute("request_type").Value);

            var orderDetail = requestElement.Element("DM_ORDER_DETAIL");

            Assert.AreEqual(E_Borrower.APPLICANT.ToString(), orderDetail.Attribute("borrower").Value);

            var applicant = requestElement.Element("APPLICANT");

            Assert.IsNotNull(applicant.Attribute("middlename"));
            Assert.AreEqual(string.Empty, applicant.Attribute("middlename").Value);

            Assert.AreEqual("Harold", applicant.Attribute("firstname").Value, true);
            Assert.AreEqual("Hall", applicant.Attribute("surname").Value, true);
            Assert.AreEqual(string.Empty, applicant.Attribute("suffix").Value);
            Assert.AreEqual("123456789", applicant.Attribute("ssn").Value);
            Assert.AreEqual("1/1/1970", applicant.Attribute("dob").Value);
        }

        private void TestJointRequestXmlInformation(RequestData request)
        {
            var requestXml = XDocument.Parse(MclXml.WriteUDNRequest(RequestTest.MclCredentials, request)).Root;

            Assert.IsNotNull(requestXml);
            Assert.IsNotNull(requestXml.Element("REQUEST"));
            Assert.IsNotNull(requestXml.Element("REQUEST").Element("SPOUSE"));

            var spouse = requestXml.Element("REQUEST").Element("SPOUSE");

            Assert.AreEqual("Sarah", spouse.Attribute("firstname").Value, true);
            Assert.AreEqual("Christina", spouse.Attribute("middlename").Value, true);
            Assert.AreEqual("Testcase", spouse.Attribute("surname").Value, true);
            Assert.AreEqual("II", spouse.Attribute("suffix").Value, true);
            Assert.AreEqual("987654321", spouse.Attribute("ssn").Value);
            Assert.AreEqual("1/1/1970", spouse.Attribute("dob").Value);
        }
    }
}
