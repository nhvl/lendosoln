﻿namespace UndisclosedDebtNotification.Test
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class AddressTest
    {
        private const string StreetAddress = "1124 Bristol Street";
        private const string City = "Costa Mesa";
        private const string State = "CA";
        private const string Zip = "92626";
        private const string YearsAtAddress = "5";

        [TestMethod]
        public void Address_StreetAddress_IsCorrect()
        {
            var address = this.CreateAddress();

            Assert.AreEqual(AddressTest.StreetAddress, address.StreetAddress);
        }

        [TestMethod]
        public void Address_City_IsCorrect()
        {
            var address = this.CreateAddress();

            Assert.AreEqual(AddressTest.City, address.City);
        }

        [TestMethod]
        public void Address_State_IsCorrect()
        {
            var address = this.CreateAddress();

            Assert.AreEqual(AddressTest.State, address.State);
        }

        [TestMethod]
        public void Address_Zip_IsCorrect()
        {
            var address = this.CreateAddress();

            Assert.AreEqual(AddressTest.Zip, address.Zip);
        }

        [TestMethod]
        public void Address_YearsAtAddress_IsCorrect()
        {
            var address = this.CreateAddress(true);

            Assert.AreEqual(AddressTest.YearsAtAddress, address.YearsAtAddress);
        }

        [TestMethod]
        public void Address_ValidData_IsValid()
        {
            var address = this.CreateAddress();

            Assert.IsTrue(address.IsValid());
        }

        [TestMethod]
        public void Address_InvalidData_IsInvalid()
        {
            var address = new Address(AddressTest.StreetAddress, null, AddressTest.State, AddressTest.Zip);

            Assert.IsFalse(address.IsValid());

            address = new Address(AddressTest.StreetAddress, AddressTest.City, null, AddressTest.Zip);

            Assert.IsFalse(address.IsValid());

            address = new Address(AddressTest.StreetAddress, AddressTest.City, AddressTest.State, null);

            Assert.IsFalse(address.IsValid());
        }

        private Address CreateAddress(bool includeYearsAtAddress = false)
        {
            return new Address(
                AddressTest.StreetAddress, 
                AddressTest.City, 
                AddressTest.State, 
                AddressTest.Zip,
                includeYearsAtAddress ? AddressTest.YearsAtAddress : null);
        }
    }
}
