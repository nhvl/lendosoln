﻿namespace UndisclosedDebtNotification.Test
{
    using System.Collections.Generic;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class ResponseTest
    {
        private Dictionary<string, string> attributes;

        private const string OrderId = "10";
        private const string ReferenceNumber = "UDN Test Loan 6282016";
        private const string CustomerNumber = "LQB";
        private const string CustomerName = "LQB Tester";
        private const string DateOrdered = "7/5/2016";
        private const string ReportOwner = "LQB TESTER";
        private const string Bureau = "EQUIFAX";
        private const string SourceBureau = "EF";
        private const string StartDate = "6/22/2016";
        private const string ErrorMessage = "Error message";
        private const string AccountType = "mortgage";
        private const string Balance = "$100.00";
        private const string CreditLimit = "$1000.00";
        private const string Industry = "Finance";
        private const string FileContent = "File Content";
        private const string BureauErrorMessage = "Bureau error";
        private const string ExpectedBureauErrorMessageFormat = "Equifax: Bureau error";

        [TestInitialize]
        public void Setup()
        {
            this.attributes = new Dictionary<string, string>()
            {
                { "source_bureau", ResponseTest.SourceBureau },
                { "notification_date", ResponseTest.DateOrdered },
                { "creditor_info", string.Empty },
                { "acct_type", ResponseTest.AccountType },
                { "opened_date", ResponseTest.StartDate },
                { "balance", ResponseTest.Balance },
                { "credit_limit", ResponseTest.CreditLimit },
                { "status", string.Empty },
                { "reported_date", ResponseTest.DateOrdered },
                { "terms", string.Empty },
                { "industry", ResponseTest.Industry },
                { "remark", string.Empty }
            };
        }

        [TestMethod]
        public void ResponseData_UnsuccessfulRequest_StoresMclErrorMessage()
        {
            var responseXml = @"
<OUTPUT>
  <INETAPI_ERROR>Error message</INETAPI_ERROR>
</OUTPUT>
";

            var responseData = MclXml.ReadUDNResponse(responseXml);

            Assert.IsNotNull(responseData);

            Assert.IsFalse(responseData.Success);

            Assert.AreEqual(ResponseTest.ErrorMessage, responseData.ErrorMessage);
        }

        [TestMethod]
        public void ResponseData_SuccessfulNewRequest_StoresReportId()
        {
            var responseXml = @"
<OUTPUT>
  <RESPONSE>
    <DM_ORDER_DETAIL report_id=""10"" status_code=""READY"" />
  </RESPONSE>
</OUTPUT>
";

            var responseData = MclXml.ReadUDNResponse(responseXml);

            Assert.IsNotNull(responseData);

            Assert.IsTrue(responseData.Success);

            Assert.AreEqual(ResponseTest.OrderId, responseData.ReportId);
        }

        [TestMethod]
        public void ResponseData_SuccessfulGetRequest_StoresNotifications()
        {
            var responseXml = this.CreateGetResponse();

            var responseData = MclXml.ReadUDNResponse(responseXml);

            Assert.IsNotNull(responseData);

            Assert.IsTrue(responseData.Success);

            Assert.AreEqual(0, responseData.ErrorMessagesFromCreditBureaus.Length);

            Assert.AreEqual(1, responseData.NotificationData.Count);

            var notification = responseData.NotificationData[0];

            Assert.AreEqual("TRADELINE", notification.TypeName);

            Assert.AreEqual(ResponseTest.ReferenceNumber, notification.Headings.ReferenceNumber);

            Assert.AreEqual(ResponseTest.OrderId, notification.Headings.ReportId);

            foreach (var attribute in notification.Attributes)
            {
                Assert.AreEqual(this.attributes[attribute.Name], attribute.RawValue);
            }
        }
        
        [TestMethod]
        public void ResponseData_SuccessfulGetRequest_StoresDocument()
        {
            var responseXml = this.CreateGetResponse();

            var responseData = MclXml.ReadUDNResponse(responseXml);

            Assert.IsNotNull(responseData);

            Assert.IsTrue(responseData.Success);

            Assert.AreEqual(ResponseTest.FileContent, responseData.EmbeddedBase64Document);
        }

        [TestMethod]
        public void ResponseData_SuccessfulGetRequest_StoresBureauErrorMessages()
        {
            var responseXml = this.CreateGetResponse(true);

            var responseData = MclXml.ReadUDNResponse(responseXml);

            Assert.IsNotNull(responseData);

            Assert.IsTrue(responseData.Success);

            Assert.AreEqual(1, responseData.ErrorMessagesFromCreditBureaus.Length);

            Assert.AreEqual(ResponseTest.ExpectedBureauErrorMessageFormat, responseData.ErrorMessagesFromCreditBureaus[0]);
        }

        private string CreateGetResponse(bool includeBureauErrorMessage = false)
        {
            if (includeBureauErrorMessage)
            {
                return @"
<OUTPUT>
  <RESPONSE>
    <CREDITDATA>
      <HEADER report_id=""10"" reference_number=""UDN Test Loan 6282016"" customer_number=""LQB"" customer_name=""LQB Tester"" date_ordered=""7/5/2016"" report_owner=""LQB TESTER"" />
      <NOTIFICATION_MONITORINGS>
        <NOTIFICATION_MONITORING bureau=""EQUIFAX"" source_bureau=""EF"" start_date=""6/22/2016"" end_date="""" status=""ERROR"">
          <ERROR>Bureau error</ERROR>
        </NOTIFICATION_MONITORING>
      </NOTIFICATION_MONITORINGS>
      <NOTIFICATIONS>
        <TRADELINES>
          <TRADELINE source_bureau=""EF"" notification_date=""7/5/2016"" creditor_info="""" acct_type=""mortgage"" opened_date=""6/22/2016"" balance=""$100.00"" credit_limit=""$1000.00"" status="""" reported_date=""7/5/2016"" terms="""" industry=""Finance"" remark="""" />
        </TRADELINES>
      </NOTIFICATIONS>
      <EMBEDDED_FILE>
        <DOCUMENT>File Content</DOCUMENT>
      </EMBEDDED_FILE>
    </CREDITDATA>
  </RESPONSE>
</OUTPUT>";
            }

            return @"
<OUTPUT>
  <RESPONSE>
    <CREDITDATA>
      <HEADER report_id=""10"" reference_number=""UDN Test Loan 6282016"" customer_number=""LQB"" customer_name=""LQB Tester"" date_ordered=""7/5/2016"" report_owner=""LQB TESTER"" />
      <NOTIFICATION_MONITORINGS>
        <NOTIFICATION_MONITORING bureau=""EQUIFAX"" source_bureau=""EF"" start_date=""6/22/2016"" end_date="""" status=""ACTIVE"" />
      </NOTIFICATION_MONITORINGS>
      <NOTIFICATIONS>
        <TRADELINES>
          <TRADELINE source_bureau=""EF"" notification_date=""7/5/2016"" creditor_info="""" acct_type=""mortgage"" opened_date=""6/22/2016"" balance=""$100.00"" credit_limit=""$1000.00"" status="""" reported_date=""7/5/2016"" terms="""" industry=""Finance"" remark="""" />
        </TRADELINES>
      </NOTIFICATIONS>
      <EMBEDDED_FILE>
        <DOCUMENT>File Content</DOCUMENT>
      </EMBEDDED_FILE>
    </CREDITDATA>
  </RESPONSE>
</OUTPUT>";
        }
    }
}
