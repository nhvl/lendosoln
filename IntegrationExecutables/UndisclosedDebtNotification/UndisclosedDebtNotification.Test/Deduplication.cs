﻿namespace UndisclosedDebtNotification.Test
{
    using System.Collections.Generic;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class Deduplication
    {
        private NotificationDataItem notificationItemEmptyRemark;
        private NotificationDataItem notificationItemNonexistentRemark;
        private NotificationDataItem notificationItemPresentRemark;
        private NotificationDataItem notificationItemPresentRemark2;

        /// <summary>
        /// All comparisons for matching between the same object and itself should be true.
        /// </summary>
        [TestMethod]
        public void EmptyIdentity()
        {
            TestCommentMatching(notificationItemEmptyRemark, notificationItemEmptyRemark, true);
        }

        /// <summary>
        /// If a new notification contains a new attribute, but the contents are empty, we want to treat them as equal and not create a new condition.
        /// </summary>
        [TestMethod]
        public void EmptyNewVsNonexistentAttributeInComment()
        {
            TestCommentMatching(notificationItemEmptyRemark, notificationItemNonexistentRemark, true);
        }

        /// <summary>
        /// If the notification has an empty value where the existing condition has a value, the attributes are considered not equal (we have insufficient information).
        /// </summary>
        [TestMethod]
        public void EmptyNewVsPresentAttributeInComment()
        {
            TestCommentMatching(notificationItemEmptyRemark, notificationItemPresentRemark, false);
        }

        /// <summary>
        /// All comparisons for matching between the same object and itself should be true.
        /// </summary>
        [TestMethod]
        public void NonexistentIdentity()
        {
            TestCommentMatching(notificationItemNonexistentRemark, notificationItemNonexistentRemark, true);
        }

        /// <summary>
        /// If both have a nonexistent or empty attribute, they are considered
        /// </summary>
        [TestMethod]
        public void NonexistentNewVsEmptyAttributeInComment()
        {
            TestCommentMatching(notificationItemNonexistentRemark, notificationItemEmptyRemark, true);
        }

        /// <summary>
        /// If the notification has a nonexistent value where the existing condition has a value, the attributes are considered not equal (we have insufficient information).
        /// </summary>
        [TestMethod]
        public void NonexistentNewVsPresentAttributeInComment()
        {
            TestCommentMatching(notificationItemNonexistentRemark, notificationItemPresentRemark, false);
        }

        /// <summary>
        /// All comparisons for matching between the same object and itself should be true.
        /// </summary>
        [TestMethod]
        public void PresentIdentity()
        {
            TestCommentMatching(notificationItemPresentRemark, notificationItemPresentRemark, true);
        }

        /// <summary>
        /// When the new attribute contains information and the existing record does not contain that attribute, they are not considered duplicates.
        /// </summary>
        [TestMethod]
        public void PresentNewVsNonexistentAttributeInComment()
        {
            TestCommentMatching(notificationItemPresentRemark, notificationItemNonexistentRemark, false);
        }

        /// <summary>
        /// When the new attribute contains information and the existing record does not contain information for that attribute, they are not considered duplicates.
        /// </summary>
        [TestMethod]
        public void PresentNewVsEmptyAttributeInComment()
        {
            TestCommentMatching(notificationItemPresentRemark, notificationItemEmptyRemark, false);
        }

        /// <summary>
        /// When the new attribute contains information and the existing record contains different information, they are not considered duplicates.
        /// </summary>
        [TestMethod]
        public void PresentNewVsDifferentPresentAttributeInComment()
        {
            TestCommentMatching(notificationItemPresentRemark, notificationItemPresentRemark2, false);
            TestCommentMatching(notificationItemPresentRemark2, notificationItemPresentRemark, false);
        }

        [TestMethod]
        public void MultipleCommentsMatch()
        {
            Assert.AreEqual(notificationItemPresentRemark.MatchesComment(new List<string> { notificationItemEmptyRemark.ToString(), notificationItemPresentRemark.ToString(), notificationItemPresentRemark.ToString() }), true);
        }

        [TestMethod]
        public void OneCommentMatches()
        {
            Assert.AreEqual(notificationItemPresentRemark.MatchesComment(new List<string> { notificationItemPresentRemark.ToString(), notificationItemPresentRemark2.ToString(), notificationItemPresentRemark2.ToString() }), true);
        }

        [TestMethod]
        public void NoCommentsMatch()
        {
            Assert.AreEqual(notificationItemPresentRemark.MatchesComment(new List<string> { notificationItemPresentRemark2.ToString(), notificationItemPresentRemark2.ToString(), notificationItemPresentRemark2.ToString() }), false);
        }

        [TestMethod]
        public void EmptyCommentList()
        {
            Assert.AreEqual(notificationItemPresentRemark.MatchesComment(new List<string>()), false);
        }

        /// <summary>
        /// Sets up three NotificationDataItems with the same <see cref="NotificationHeadings"/> information as well as the same description and notification type.
        /// The difference between them is the "remark" attribute, which is either present with a value, present with an empty string value, or not present at all.
        /// </summary>
        [TestInitialize]
        public void Setup()
        {
            NotificationHeadings headings = new NotificationHeadings(reportId: "12345", referenceNumber: "09876", consumer: "Carlos Consumer");
            var attributes = new List<NotificationAttribute>()
            {
                new NotificationAttribute("source_bureau", "TU", NotificationMapping.ToCreditBureau),
                new NotificationAttribute("notification_date", "May 20, 2016"),
                new NotificationAttribute("remark", "")
            };
            notificationItemEmptyRemark = new NotificationDataItem(headings, "This is the notification description", "NOTIFICATION TYPE", attributes);

            attributes = new List<NotificationAttribute>()
            {
                new NotificationAttribute("source_bureau", "TU", NotificationMapping.ToCreditBureau),
                new NotificationAttribute("notification_date", "May 20, 2016"),
            };
            notificationItemNonexistentRemark = new NotificationDataItem(headings, "This is the notification description", "NOTIFICATION TYPE", attributes);

            attributes = new List<NotificationAttribute>()
            {
                new NotificationAttribute("source_bureau", "TU", NotificationMapping.ToCreditBureau),
                new NotificationAttribute("notification_date", "May 20, 2016"),
                new NotificationAttribute("remark", "this is the remark")
            };
            notificationItemPresentRemark = new NotificationDataItem(headings, "This is the notification description", "NOTIFICATION TYPE", attributes);

            attributes = new List<NotificationAttribute>()
            {
                new NotificationAttribute("source_bureau", "TU", NotificationMapping.ToCreditBureau),
                new NotificationAttribute("notification_date", "May 20, 2016"),
                new NotificationAttribute("remark", "this is another different remark")
            };
            notificationItemPresentRemark2 = new NotificationDataItem(headings, "This is the notification description", "NOTIFICATION TYPE", attributes);
        }

        private static void TestCommentMatching(NotificationDataItem newItem, NotificationDataItem existingCommentItem, bool expectedCommentMatch)
        {
            string notificationText = existingCommentItem.ToString();
            Assert.AreEqual(newItem.MatchesComment(new List<string> { notificationText }), expectedCommentMatch);
        }
    }
}