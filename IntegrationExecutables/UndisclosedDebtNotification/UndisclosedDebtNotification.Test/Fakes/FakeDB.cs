﻿namespace UndisclosedDebtNotification.Test.Fakes
{
    using System;
    using System.Collections.Generic;

    public class FakeDB : DB
    {
        public FakeDB()
            : base()
        {
        }

        public override Dictionary<Guid, string> GetServiceCompanyIdsAndUrls()
        {
            throw new NotImplementedException();
        }

        public override Guid GetBrokerID(string sCustomerCode)
        {
            throw new NotImplementedException();
        }

        public override HashSet<Guid> GetBranchIDsByBrokerID(Guid brokerID)
        {
            throw new NotImplementedException();
        }

        public override List<LoanNmAppId> GetAppCandidates(Lender lender, Guid branchId, int channel, E_RequestType requestType)
        {
            throw new NotImplementedException();
        }
    }
}
