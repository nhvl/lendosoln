﻿namespace UndisclosedDebtNotification.Test.Fakes
{
    using System;
    using System.Collections.Specialized;
    using com.lendingqb.Auth;
    using com.lendingqb.Loan;

    public class FakeServiceLocator : ServiceLocator
    {
        private DB database;
        private Func<FakeLqbWebService> createFakeWebService;
        private EmailSender emailSender;

        public FakeServiceLocator(NameValueCollection appSettings = null, DB database = null, Func<FakeLqbWebService> createFakeWebService = null, EmailSender emailSender = null)
        {
            this.AppSettings = appSettings ?? new NameValueCollection();
            this.database = database;
            this.createFakeWebService = createFakeWebService;
            this.emailSender = emailSender;
        }

        public override NameValueCollection AppSettings { get; }

        public static NameValueCollection CreateNewAppSettingsFromCurrent()
        {
            return new NameValueCollection(ServiceLocator.Instance.AppSettings);
        }

        public override DB CreateDB(string connectionString)
        {
            return this.database ?? new FakeDB();
        }

        public override IAuthService CreateAuthService(string url)
        {
            var service = (this.createFakeWebService?.Invoke() ?? new FakeLqbWebService());
            service.Url = url;
            return service;
        }

        public override ILoan CreateLoan(string url)
        {
            var service = (this.createFakeWebService?.Invoke() ?? new FakeLqbWebService());
            service.Url = url;
            return service;
        }

        public override EmailSender CreateEmailSender(string host)
        {
            return this.emailSender ?? new FakeEmailSender();
        }
    }
}
