﻿namespace UndisclosedDebtNotification.Test.Fakes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;
    using com.lendingqb.Auth;
    using com.lendingqb.Loan;

    public class FakeLqbWebService : IAuthService, ILoan
    {
        public string Url { get; set; }

        public virtual string GetUserAuthTicket(string userName, string passWord)
        {
            return @"<MagicTicket works=""true""/>";
        }

        public System.Security.Cryptography.X509Certificates.X509CertificateCollection ClientCertificates
        {
            get
            {
                return new System.Security.Cryptography.X509Certificates.X509CertificateCollection();
            }
        }

        public virtual string Load(string sTicket, string sLNm, string sXmlQuery, int format)
        {
            return @"<LOXmlFormat version=""1.0"">
  <loan>
    <collection id=""sCondDataSet"">
      <record index=""1"">
        <field id=""ID"">A82K</field>
        <field id=""DoneDate"">10/4/2016</field>
        <field id=""AssignedTo"">Test AssignedTo</field>
        <field id=""Category"">Some Category</field>
        <field id=""CondDesc""></field>
        <field id=""DueDate"">10/3/2016</field>
        <field id=""FollowUpDate""></field>
        <field id=""Notes""></field>
        <field id=""Owner"">Test Owner</field>
        <field id=""PermissionLevel"">A permission Level</field>
        <field id=""Status"">Open</field>
        <field id=""ToBeAssigned""></field>
        <field id=""ToBeOwnedBy""></field>
        <field id=""FirstComment"">report_id: 4506
reference_number: 123
Consumer: Fake Consumer
INQUIRY
source_bureau: TransUnion
notification_date: 10/1/2016
company: TESTCRA
industry: Miscellaneous and public record
permissible_purpose: 
inquiry_date: 9/16/2016
reissue_date: 
remark: </field>
      </record>
    </collection>
  </loan>
</LOXmlFormat>";
        }

        public virtual FakeSaveResult SaveResult { get; set; }
        public virtual FakeSaveResult UploadResult { get; set; }

        public virtual string Save(string sTicket, string sLNm, string sDataContent, int format)
        {
            return (this.SaveResult ?? FakeSaveResult.Exception(service => new NotImplementedException())).Generate(this);
        }

        public virtual string UploadPDFDocumentToApp(string sTicket, string sLNm, Guid aAppId, string documentType, string notes, string sDataContent)
        {
            return (this.UploadResult ?? FakeSaveResult.Exception(service => new NotImplementedException())).Generate(this); ;
        }

        public class FakeSaveResult
        {
            public FakeSaveResult(bool isSuccess, IEnumerable<string> errorMessages = null, Func<FakeLqbWebService, Exception> exceptionMaker = null)
            {
                this.IsSuccess = isSuccess;
                this.ErrorMessages = errorMessages;
                this.ExceptionMaker = exceptionMaker;
            }

            public IEnumerable<string> ErrorMessages { get; private set; }
            public Func<FakeLqbWebService, Exception> ExceptionMaker { get; private set; }
            public bool IsSuccess { get; private set; }

            public static FakeSaveResult Success()
            {
                return new FakeSaveResult(true);
            }

            public static FakeSaveResult Error(params string[] errorMessages)
            {
                return new FakeSaveResult(false, errorMessages);
            }

            public static FakeSaveResult Exception(Func<FakeLqbWebService, Exception> throwSomething)
            {
                return new FakeSaveResult(true, null, throwSomething);
            }

            public static FakeSaveResult ExceptionFirstTimes(int desiredFailureCount, Func<FakeLqbWebService, Exception> throwSomething)
            {
                int failureCount = 0;
                return new FakeSaveResult(
                    true,
                    null,
                    service => failureCount++ < desiredFailureCount ? throwSomething(service) : null);
            }

            public string Generate(FakeLqbWebService fakeLqbWebService)
            {
                var exception = this.ExceptionMaker?.Invoke(fakeLqbWebService);
                if (exception != null)
                {
                    throw exception;
                }

                return (new XElement(
                    "result",
                    new XAttribute("status", this.IsSuccess ? "OK" : "Error"),
                    this.ErrorMessages?.Select(msg => new XElement("error", msg)))).ToString();
            }
        }

        public class FakeWebServiceFactory
        {
            private List<FakeLqbWebService> services = new List<FakeLqbWebService>();

            public FakeLqbWebService Create()
            {
                var service = new FakeLqbWebService();
                services.Add(service);
                return service;
            }

            public FakeSaveResult SaveResult
            {
                set
                {
                    foreach (var service in services)
                    {
                        service.SaveResult = value;
                    }
                }
            }
            public FakeSaveResult UploadResult
            {
                set
                {
                    foreach (var service in services)
                    {
                        service.UploadResult = value;
                    }
                }
            }
        }
    }
}
