﻿namespace UndisclosedDebtNotification.Test.Fakes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Mail;

    public class MockEmailSender : FakeEmailSender
    {
        public static readonly string NoThrowEmailAddress = "nothrow@lendingqb.com";
        public static readonly string NoThrowAltEmailAddress = "nothrowalt@lendingqb.com";
        public static readonly string ThrowOnceEmailAddress = "1throw@lendingqb.com";
        public static readonly string ThrowTwiceEmailAddress = "2throw@lendingqb.com";
        public static readonly string AlwaysThrowEmailAddress = "throw@lendingqb.com";
        private Dictionary<string, int> trySendEmailCountByAddress = new Dictionary<string, int>(StringComparer.OrdinalIgnoreCase);
        private Dictionary<string, int> sentEmailCountByAddress = new Dictionary<string, int>(StringComparer.OrdinalIgnoreCase);

        public override void Send(MailMessage message)
        {
            IncrementEmailCounts(trySendEmailCountByAddress, message.To);
            var emailAddressCausingThrow = message.To.FirstOrDefault(this.ShouldThrow);
            if (emailAddressCausingThrow != null)
            {
                throw new SmtpException("Throwing because of " + emailAddressCausingThrow);
            }

            this.LastEmailBody = message.Body;
            this.EmailsSentCount += 1;
            IncrementEmailCounts(sentEmailCountByAddress, message.To);
        }

        public int EmailsSentCount { get; private set; }

        public string LastEmailBody { get; private set; }

        public bool SentEmailTo(string emailAddress)
        {
            int count;
            return this.sentEmailCountByAddress.TryGetValue(emailAddress, out count) && count > 0;
        }

        public int EmailTryCount(string emailAddress)
        {
            int count;
            return this.trySendEmailCountByAddress.TryGetValue(emailAddress, out count) ? count : 0;
        }

        public IEnumerable<string> EmailAddressesContacted
        {
            get { return this.sentEmailCountByAddress.Keys; }
        }

        private bool ShouldThrow(MailAddress email)
        {
            var match = System.Text.RegularExpressions.Regex.Match(email.User, @"^(\d*)throw$", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            var countString = match.Groups[1].Value;

            return match.Success && (string.IsNullOrEmpty(countString) || int.Parse(countString) >= trySendEmailCountByAddress[email.Address]);
        }

        private static void IncrementEmailCounts(Dictionary<string, int> dict, IEnumerable<MailAddress> emailAddresses)
        {
            foreach (var email in emailAddresses)
            {
                int currentCount;
                dict[email.Address] = 1 + (dict.TryGetValue(email.Address, out currentCount) ? currentCount : 0);
            }
        }
    }
}
