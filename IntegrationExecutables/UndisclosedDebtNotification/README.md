# Undisclosed Debt Notification (UDN)

This tool integrates with Mortgage Credit Link (MCL)'s implementation of UDN to allow automatic
ordering of the service on matching loan applications in LendingQB.  For detailed documentation
on the business design of the service and the client set up process, see the [intranet
 article](https://intranet/Projects/LendingQB/Integrations/Undisclosed_Debt_Notification_(UDN)).

## Running the Executable
This executable runs in two modes: "New" and "Get".  These correspond to the ordering/terminating
and retrieval tasks, respectively.  To run the exe, one of these values should be passed in as
the first argument (case insensitive), e.g.

> UndisclosedDebtNotification.exe New


### Configuration settings
This executable relies on a series of configuration files, most essentially the
[app.config](UndisclosedDebtNotification/app.config).  In addition to the lender and global
settings in the config, there are two global-level settings files the exe will use when
processing Get responses.

The first is [Notifications.ss.xml](UndisclosedDebtNotification/Notifications.ss.xml), which
is a subset of the skeleton schema provided by MCL for their requests.  This is used by the
service to understand the data and data types being read in a response.  If MCL adds a new
notification type, this is where it will be updated.

The second file is [conditions.txt](UndisclosedDebtNotification/conditions.txt), which specifies
the format for conditions to import into LendingQB.
