﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UndisclosedDebtNotification
{
    public class Lender
    {
        private StringBuilder m_log;
        private StringBuilder m_errorLog;

        private readonly string CustomerCode;

        public Guid BrokerID { get; private set; }
        public HashSet<Guid> BranchIDs { get; private set; }
        public LoanWebService LoanService { get; private set; }
        public bool ErrorLogNonEmpty
        {
            get
            {
                return m_errorLog.Length > 0;
            }
        }
        internal string GeneralLog
        {
            get { return this.m_log.ToString(); }
        }
        public string AllLogsForDebug
        {
            get
            {
                return "The log for this lender until now:" + Environment.NewLine + Environment.NewLine
                    + m_log + Environment.NewLine + Environment.NewLine
                    + "Error Log:" + Environment.NewLine + Environment.NewLine
                    + m_errorLog + Environment.NewLine + Environment.NewLine
                    + "Application Settings Error Log:" + Environment.NewLine + Environment.NewLine
                    + SettingsErrorLog.RetrieveAndClear();
            }
        }
        public LenderSettings Settings { get; private set; }

        public Lender(string sCustomerCode, E_RequestType requestType, DB database)
        {
            m_log = new StringBuilder();
            m_errorLog = new StringBuilder();

            this.CustomerCode = sCustomerCode;
            BrokerID = database.GetBrokerID(sCustomerCode);
            if (BrokerID != Guid.Empty)
            {
                BranchIDs = database.GetBranchIDsByBrokerID(BrokerID);

                Settings = CreateLenderSettings(sCustomerCode, requestType);

                CheckBranchIDSubset(sCustomerCode, BranchIDs, Settings.BranchIdsToExclude);
                CheckBranchIDSubset(sCustomerCode, BranchIDs, Settings.BranchIdsToInclude);

                if (Settings.BranchIdsToInclude.Any())
                {
                    BranchIDs.IntersectWith(Settings.BranchIdsToInclude);
                }

                BranchIDs.ExceptWith(Settings.BranchIdsToExclude);
                if (!BranchIDs.Any())
                {
                    throw new Exception("Lender does not have any branches currently enabled.  If the Lender should be disabled, please remove the customer code from the LENDERS setting.");
                }

                foreach (var mappedBranchId in this.Settings.BranchCraComIds.Keys)
                {
                    if (!this.BranchIDs.Contains(mappedBranchId))
                    {
                        SettingsErrorLog.LogFormat(
                            "Branch ID '{0}' does not exist for lender '{1}' but has a CRA assigned in the lender's settings.",
                            mappedBranchId,
                            sCustomerCode);
                    }
                }

                LoanService = new LoanWebService(Settings.LQB, Settings.LqbCertificate, AppSettings.ENVIRONMENT, m_errorLog);
            }
        }

        private static void CheckBranchIDSubset(string sCustomerCode, HashSet<Guid> allBranchIDs, HashSet<Guid> subsetOfBranchIds)
        {
            if (!subsetOfBranchIds.IsSubsetOf(allBranchIDs))
            {
                HashSet<Guid> extraIds = new HashSet<Guid>(subsetOfBranchIds);
                extraIds.ExceptWith(allBranchIDs);
                SettingsErrorLog.Log(sCustomerCode + " has one or more BranchIds configured that were not found in the set of all the lender's branches: ", extraIds, ", ");
            }
        }

        private static LenderSettings CreateLenderSettings(string sCustomerCode, E_RequestType requestType)
        {
            if (requestType == E_RequestType.GET)
                return new Get.LenderGetSettings(sCustomerCode);

            return new ActivationDeactivation.LenderActivationSettings(sCustomerCode);
        }

        public bool IsComplete()
        {
            return BrokerID != Guid.Empty
                && LoanService.IsComplete()
                && Settings.IsComplete();
        }

        #region Log Methods
        public void LogBranchAndChannel(Guid sBranchID, int channel)
        {
            Logger.LogInfo("BranchId='" + sBranchID + "', Channel='" + LQBFields.Channel_rep(channel) + "'", this.CustomerCode);
            m_log.AppendFormatLine("BranchId='{0}', Channel='{1}'", sBranchID, LQBFields.Channel_rep(channel));
        }

        public void LogDBQueryResult(E_RequestType requestType, long elapsedTime, int records)
        {
            m_log.AppendFormatLine("  GetAppsForUDNActivationDeactivation - {0} ElapsedTime: {1}, Records: {2}", requestType, elapsedTime, records);
        }

        public void LogErrorAsReasonForSkip(LoanNmAppId candidateApp, Exception exc)
        {
            LogReasonForSkip(candidateApp, exc.GetType().FullName);

            m_errorLog.AppendFormat("{0} occurred on sLNm='{1}', aAppId='{2}'{3}{3}{4}{3}{3}{3}",
                exc.GetType().Name,
                candidateApp.LoanNm,
                candidateApp.AppId,
                Environment.NewLine,
                exc);
        }

        public void LogUDNErrorMessage(LoanNmAppId candidateApp, IEnumerable<string> errorMessages)
        {
            string reason = "Error Message(s) from UDN: '" + errorMessages.Aggregate((str1, str2) => str1 + "'; '" + str2) + "'";

            LogReasonForSkip(candidateApp, reason);
        }

        public void LogReasonForSkip(LoanNmAppId candidateApp, string reason)
        {
            LogAppResult(candidateApp, "Skipping: " + reason);
        }

        public void LogSuccess(LoanNmAppId candidateApp, E_RequestType requestType)
        {
            LogAppResult(candidateApp, string.Format("UDN {0}.", Tools.GetRequestAction(requestType)));
        }

        private void LogAppResult(LoanNmAppId candidateApp, string result)
        {
            m_log.AppendFormatLine("    aAppId='{0}', sLNm='{1}' {2}",
                candidateApp.AppId, candidateApp.LoanNm, result);
        }
        #endregion
    }
}
