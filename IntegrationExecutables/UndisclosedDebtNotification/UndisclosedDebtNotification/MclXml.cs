﻿namespace UndisclosedDebtNotification
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;

    public static class MclXml
    {
        public static string WriteUDNRequest(Credentials mclCredentials, RequestData requestData)
        {
            var requestElement = CreateRequestElement(requestData);
            var userInfoElement = new XElement(
                "USER_INFO",
                new XAttribute("login", mclCredentials.Username),
                new XAttribute("password", mclCredentials.Password)
            );
            
            return new XElement("INPUT", userInfoElement, requestElement).ToString();
        }

        public static ResponseData ReadUDNResponse(string sResponse)
        {
            try
            {
                XElement xRoot = XDocument.Parse(sResponse).Root;
                XElement xResponse = xRoot.Name.LocalName == "RESPONSE" ? xRoot : xRoot.Element("RESPONSE"); // At time of development, MCL has a bug where Get reponses are usually not wrapped in <OUTPUT> tags

                ResponseData response = new ResponseData();
                response.Success = xResponse != null;
                if (!response.Success)
                {
                    response.ErrorMessage = xRoot.Element("INETAPI_ERROR").Value;
                }
                else
                {
                    response.ReportId = xResponse.Element("DM_ORDER_DETAIL").SafeAttributeValue("report_id");

                    XElement xCreditData = xResponse.Element("CREDITDATA");
                    if (xCreditData != null)
                    {
                        ReadNotificationMonitorings(response, xCreditData.Element("NOTIFICATION_MONITORINGS"));
                        NotificationHeadings headings = DetermineNotificationHeadings(xCreditData);
                        MclNotification.ReadNotifications(response, headings, xCreditData.Element("NOTIFICATIONS"));
                        XElement xEmbeddedFile = xCreditData.Element("EMBEDDED_FILE");
                        response.EmbeddedBase64Document = xEmbeddedFile == null ? null : xEmbeddedFile.Element("DOCUMENT").Value;
                    }
                }

                return response;
            }
            catch (Exception exc)
            {
                throw new Exception("Failed to read MCL's xml response.  Response was: \"" + sResponse + "\"", exc);
            }
        }

        private static XElement CreateRequestElement(RequestData requestData)
        {
            var requestElement = new XElement("REQUEST", new XAttribute("request_type", requestData.RequestType_rep));

            requestElement.Add(CreateOrderDetail(requestData));
            requestElement.Add(new XElement(
                "APPLICANT",
                new XAttribute("firstname", requestData.Applicant.Firstname),
                new XAttribute("surname", requestData.Applicant.Surname),
                CreateXAttribute("middlename", requestData.Applicant.Middlename),
                new XAttribute("suffix", requestData.Applicant.Suffix),
                new XAttribute("ssn", requestData.Applicant.Ssn),
                CreateXAttribute("dob", requestData.Applicant.Dob)
            ));

            if (requestData.Spouse != null)
            {
                requestElement.Add(new XElement(
                    "SPOUSE",
                    new XAttribute("firstname", requestData.Spouse.Firstname),
                    new XAttribute("surname", requestData.Spouse.Surname),
                    CreateXAttribute("middlename", requestData.Spouse.Middlename),
                    new XAttribute("suffix", requestData.Spouse.Suffix),
                    new XAttribute("ssn", requestData.Spouse.Ssn),
                    CreateXAttribute("dob", requestData.Spouse.Dob)
                ));
            }

            if (requestData.RequestType == E_RequestType.NEW)
            {
                AddAddressData(requestElement, (NewRequestData)requestData);
            }

            return requestElement;
        }

        private static XElement CreateOrderDetail(RequestData requestData)
        {
            var orderDetailElement = new XElement("DM_ORDER_DETAIL", new XAttribute("borrower", requestData.Borrower_rep));

            if (requestData is NewRequestData)
            {
                var newRequestData = (NewRequestData)requestData;
                orderDetailElement.Add(new XAttribute("start_date", newRequestData.StartDate_rep));
                orderDetailElement.Add(new XAttribute("reference_number", newRequestData.Application.LoanNm));
            }
            else
            {
                orderDetailElement.Add(new XAttribute("report_id", ((GetDeactivateRequestData)requestData).ReportId));
            }

            return orderDetailElement;
        }

        private static void AddAddressData(XElement requestElement, NewRequestData requestData)
        {
            requestElement.Add(new XElement(
                "CURRENT_ADDRESS",
                CreateXAttribute("streetaddress", requestData.CurrentAddress.StreetAddress),
                new XAttribute("city", requestData.CurrentAddress.City),
                new XAttribute("state", requestData.CurrentAddress.State),
                new XAttribute("zip", requestData.CurrentAddress.Zip),
                CreateXAttribute("years_at_address", requestData.CurrentAddress.YearsAtAddress)
            ));

            requestElement.Add(new XElement(
                "PROPERTY_ADDRESS",
                CreateXAttribute("streetaddress", requestData.PropertyAddress.StreetAddress),
                new XAttribute("city", requestData.PropertyAddress.City),
                new XAttribute("state", requestData.PropertyAddress.State),
                new XAttribute("zip", requestData.PropertyAddress.Zip)
            ));
        }

        private static void ReadNotificationMonitorings(ResponseData response, XElement xNotificationMonitorings)
        {
            if (null == xNotificationMonitorings)
                return;

            var xNotificationMonitors = xNotificationMonitorings.Elements("NOTIFICATION_MONITORING").Where(el => null != el.Attribute("status"));

            var deactivationDates = xNotificationMonitors
                .Where(el => el.Attribute("status").Value == "DEACTIVE" && null != el.Attribute("end_date"))
                .Select(el => el.Attribute("end_date").Value);
            foreach (var date in deactivationDates)
            {
                DateTime dt;
                if (DateTime.TryParse(date, out dt) && (response.DeactivationDate == null || dt < response.DeactivationDate))
                    response.DeactivationDate = dt;
            }

            response.ErrorMessagesFromCreditBureaus = xNotificationMonitors
                .Where(el => el.Attribute("status").Value == "ERROR")
                .Select(el => NotificationMapping.ToCreditBureau(el.Attribute("source_bureau").Value) + ": " + el.Element("ERROR").Value)
                .ToArray();
        }

        private static NotificationHeadings DetermineNotificationHeadings(XElement xCreditData)
        {
            XElement xHeader = xCreditData.Element("HEADER");
            string report_id = xHeader.SafeAttributeValue("report_id");
            string reference_number = xHeader.SafeAttributeValue("reference_number");
            string consumer = string.Empty;

            XElement xConsumer = xCreditData.Element("CONSUMER");
            List<string> name = new List<string>(4);
            name.AddAttributeIfExists(xConsumer, "firstname", false);
            name.AddAttributeIfExists(xConsumer, "middlename", false);
            name.AddAttributeIfExists(xConsumer, "surname", false);
            name.AddAttributeIfExists(xConsumer, "suffix", false);
            if (name.Count > 0)
            {
                consumer = name.Aggregate((s1, s2) => s1 + " " + s2);
            }

            return new NotificationHeadings(report_id, reference_number, consumer);
        }

        private static void AddAttributeIfExists(this List<string> list, XElement element, string attributeName, bool includeAttributeName)
        {
            string value = element.SafeAttributeValue(attributeName);
            if (!string.IsNullOrEmpty(value))
            {
                if (includeAttributeName)
                    list.Add(NotificationFormatting.AttributeString(attributeName, value));
                else
                    list.Add(value);
            }
        }

        /// <summary>
        /// Does not create attribute for null value (Prevents ArgumentNullExceptions)
        /// </summary>
        /// <returns>null if value is null; new XAttribute otherwise.</returns>
        private static XAttribute CreateXAttribute(XName name, object value)
        {
            if (value == null)
                return null;

            return new XAttribute(name, value);
        }
    }
}
