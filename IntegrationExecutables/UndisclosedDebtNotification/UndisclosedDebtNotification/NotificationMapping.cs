﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UndisclosedDebtNotification
{
    public static class NotificationMapping
    {
        public static Func<string, string> DetermineMapping(string schemaValue)
        {
            switch (schemaValue)
            {
                case "s:money": return ToMoney;
                case "t:ecoa": return ToEcoa;
                case "t:kindOfBusiness": return ToKindOfBusiness;
                case "t:permissiblePurpose": return ToPermissiblePurpose;
                case "t:tradeAccountType": return ToTradeAccountType;
                case "XP|EF|TU": return ToCreditBureau;
                case "":
                case "N|s:date":
                case "s:date":
                default:
                    return ToIdentity;
            }
        }

        private static string ToIdentity(string value)
        {
            return value;
        }

        public static string ToCreditBureau(string value)
        {
            switch (value)
            {
                case "XP": return "Experian";
                case "EF": return "Equifax";
                case "TU": return "TransUnion";
                default:
                    return value;
            }
        }

        private static string ToMoney(string value)
        {
            decimal d;
            if (decimal.TryParse(value, out d))
                return d.ToString("C");

            return value;
        }

        private static string ToTradeAccountType(string value)
        {
            switch (value)
            {
                case "II": return "Installment";
                case "IM": return "Mortgage";
                case "IA": return "Auto";
                case "IE": return "Education";
                case "IL": return "Lease";
                case "IC": return "Co-signer";
                case "RR": return "Revolving";
                case "RC": return "Credit Card";
                case "RO": return "Open Account";
                case "YY": return "Collection";
                case "OT": return "Other";
                case "OO": return "Open Account";
                default:
                    return value;
            }
        }

        private static string ToEcoa(string value)
        {
            switch (value)
            {
                case "I": return "Individual";
                case "B": return "Individual (Borrower)";
                case "C": return "Individual (Consumer)";
                case "J": return "Joint";
                case "A": return "Authorized User";
                case "P": return "Participating account";
                case "S": return "Co-maker (borrower is co-signer, primary is not spouse)";
                case "M": return "Maker (borrower is primary with non-spouse co-signer)";
                case "T": return "Terminated";
                case "X": return "Deceased";
                case "N": return "?";
                case "U": return "Undesignated";
                default:
                    return value;
            }
        }

        private static string ToKindOfBusiness(string value)
        {
            switch (value)
            {
                case "A": return "Automotive";
                case "B": return "Banks and S and L's";
                case "C": return "Clothing";
                case "D": return "Department, variety, local, regional, national chains";
                case "E": return "Employment";
                case "F": return "Finance, personal";
                case "G": return "Groceries";
                case "H": return "Home furnishing";
                case "I": return "Insurance";
                case "J": return "Jewelry, cameras, computers";
                case "K": return "Contractors";
                case "L": return "Lumber, building material, hardware";
                case "M": return "Medical and related health";
                case "N": return "Credit card and travel/entertainment";
                case "O": return "Oil companies";
                case "P": return "Personal services other than medical";
                case "Q": return "Finance, non-personal";
                case "R": return "Real estate and public accomodations";
                case "S": return "Sporting goods";
                case "T": return "Farm and garden supplies";
                case "U": return "Utilities and fuel";
                case "V": return "Government";
                case "W": return "Wholesale";
                case "X": return "Advertising";
                case "Y": return "Collection services";
                case "Z": return "Miscellaneous and public record";
                default:
                    return value;
            }
        }

        private static string ToPermissiblePurpose(string value)
        {
            switch (value)
            {
                case "BC": return "Bank/Credit Card";
                case "RL": return "Retail Loan";
                case "SL": return "Student Loan";
                case "HL": return "Home Equity Loan";
                case "ML": return "Mortgage Loan";
                case "BL": return "Business Loan";
                case "IL": return "Installment Loan";
                case "RM": return "Recreational Merchandise";
                case "AL": return "Auto Loan";
                case "LE": return "Auto Lease";
                case "RR": return "Rental";
                case "UU": return "Utility";
                case "CP": return "Credit Card Processor";
                default:
                    return value;
            }
        }
    }
}
