﻿namespace UndisclosedDebtNotification
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Represents a request for UDN activation using the
    /// NEW request type.
    /// </summary>
    public class NewRequestData : RequestData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NewRequestData"/>
        /// class.
        /// </summary>
        /// <param name="dataDictionary">
        /// The dictionary containing the data required to populate the request,
        /// including the credit ordered date, current address data, and subject
        /// property address data.
        /// </param>
        /// <param name="application">
        /// The application for the request.
        /// </param>
        /// <param name="errorEmail">
        /// The email address used when the request encounters an error.
        /// </param>
        /// <param name="backupEmail">The backup email address.</param>
        public NewRequestData(Dictionary<string, string> dataDictionary, LoanNmAppId application, string errorEmail, string backupEmail)
            : base(dataDictionary, application, errorEmail, backupEmail, E_RequestType.NEW)
        {
        }

        /// <summary>
        /// Gets the value for the date when UDN monitoring 
        /// should be started.
        /// </summary>
        /// <value>
        /// The <see cref="DateTime"/> when UDN monitoring
        /// should be started.
        /// </value>
        public DateTime? StartDate { get; private set; }

        /// <summary>
        /// Gets the value for the the <see cref="StartDate"/> 
        /// of the request in string format for the XML request.
        /// </summary>
        /// <value>
        /// The <see cref="StartDate"/> as a <see cref="string"/>.
        /// </value>
        public string StartDate_rep
        {
            get { return this.StartDate.Value.ToShortDateString(); }
        }

        /// <summary>
        /// Gets the value for the current address of the applicant.
        /// </summary>
        /// <value>
        /// The <see cref="Address"/> representing the current address
        /// of the applicant.
        /// </value>
        public Address CurrentAddress { get; private set; }

        /// <summary>
        /// Gets the value for the subject property address.
        /// </summary>
        /// <value>
        /// The <see cref="Address"/> representing the subject
        /// property address.
        /// </value>
        public Address PropertyAddress { get; private set; }

        /// <summary>
        /// Determines whether this instance is valid, namely that
        /// a valid credit ordered date is on file and the current
        /// and subject property addresses have been filled out.
        /// </summary>
        /// <returns>
        /// True if the request is valid, false otherwise.
        /// </returns>
        public override bool IsValid()
        {
            return base.IsValid() 
                && this.StartDate.HasValue 
                && this.CurrentAddress.IsValid() 
                && this.PropertyAddress.IsValid();
        }

        /// <summary>
        /// Populates applicant and spouse data from the supplied
        /// <paramref name="dataDictionary"/>.
        /// </summary>
        /// <param name="dataDictionary">
        /// The dictionary containing borrower and/or co-borrower data
        /// used to populate the request.
        /// </param>
        protected override void PopulateApplicantData(Dictionary<string, string> dataDictionary)
        {
            var aCrOd = Tools.GetStringValueFromDictionary(dataDictionary, "aCrOd");

            DateTime startDate;
            if (!DateTime.TryParse(aCrOd, out startDate))
            {
                var clientErrorMessage = string.Format(
                    "The UDN service could not be ordered for an application on loan {0} because there is no Credit " +
                    "Report Ordered date (field aCrOd). Please fill out this date for all applications on file.",
                    this.Application.LoanNm);

                SendEmail.SendErrorMessageToClient(this, new[] { clientErrorMessage }, E_RequestType.NEW, "failed");

                throw new InvalidOperationException(string.Format(
                    "The credit ordered date '{0}' for the loan is blank or not a valid date.",
                    aCrOd));
            }

            base.PopulateApplicantData(dataDictionary);

            this.StartDate = startDate;

            this.PropertyAddress = new Address(
                Tools.GetStringValueFromDictionary(dataDictionary, "sSpAddr"),
                Tools.GetStringValueFromDictionary(dataDictionary, "sSpCity"),
                Tools.GetStringValueFromDictionary(dataDictionary, "sSpState"),
                Tools.GetStringValueFromDictionary(dataDictionary, "sSpZip"));

            this.CurrentAddress = new Address(
                Tools.GetStringValueFromDictionary(dataDictionary, "a" + this.Applicant.FieldPrefix + "Addr"),
                Tools.GetStringValueFromDictionary(dataDictionary, "a" + this.Applicant.FieldPrefix + "City"),
                Tools.GetStringValueFromDictionary(dataDictionary, "a" + this.Applicant.FieldPrefix + "State"),
                Tools.GetStringValueFromDictionary(dataDictionary, "a" + this.Applicant.FieldPrefix + "Zip"),
                Tools.GetStringValueFromDictionary(dataDictionary, "a" + this.Applicant.FieldPrefix + "AddrYrs"));
        }
    }
}
