﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Text.RegularExpressions;
using System.IO;

namespace UndisclosedDebtNotification
{
    public static class MclNotification
    {
        /// <summary>
        /// Reads Notification data from the xml into a ResponseData element.
        /// </summary>
        /// <param name="response">The ResponseData element to add Notifications and NotificationData as lists of strings.</param>
        /// <param name="notificationHeadings">The common headings to notifications from the given request</param>
        /// <param name="xNotifications">The NOTIFICATIONS element containing the notifications to be read.</param>
        public static void ReadNotifications(ResponseData response, NotificationHeadings notificationHeadings, XElement xNotifications)
        {
            if (null == xNotifications)
                return;
            
            response.NotificationData = new List<NotificationDataItem>();
            foreach (var notificationType in NotificationTypes)
            {
                Dictionary<string, Func<string, string>> attributeFormats = AttributeFormatMappingsByXPath[notificationType.XPath];
                foreach (XElement notification in xNotifications.XPathSelectElements(notificationType.XPath))
                {
                    var notificationDataAttributes = new List<NotificationAttribute>(notificationType.Attributes.Length);
                    string[] values = new string[notificationType.Attributes.Length];
                    for (int i = 0; i < notificationType.Attributes.Length; ++i)
                    {
                        string name = notificationType.Attributes[i];
                        string value = notification.SafeAttributeValue(name);
                        notificationDataAttributes.Add(new NotificationAttribute(name, value, attributeFormats[name]));
                        if (value != null)
                            values[i] = attributeFormats[name](value);
                    }
                    response.NotificationData.Add(new NotificationDataItem(notificationHeadings, string.Format(notificationType.Format, values), notification.Name.LocalName, notificationDataAttributes));
                }
            }
        }

        private const string ConditionRegexFormat = @"^([\w/]+): ""(.*)""$";
        private static readonly List<ConditionFormat> NotificationTypes;
        private static Func<string, string, string> JoinByNewLine = (str1, str2) => str1 + NotificationFormatting.NewLine + str2;


        /// <summary>
        /// Contains format mappings for the attributes, grouped by the XPath under which they appear.
        /// </summary>
        /// <example>Pass in "//MAJOR_DEROGATORIES/MAJOR_DEROGATORY", followed by "balance" to get <c>string ToMoney(string value)</c></example>
        private static readonly Dictionary<string, Dictionary<string, Func<string, string>>> AttributeFormatMappingsByXPath;

        static MclNotification()
        {
            Regex ConditionFileLine = new Regex(ConditionRegexFormat);
            IEnumerable<string> conditionLines = File.ReadAllLines(AppSettings.CONDITION_FORMAT_FILE_PATH)
                .Select(str => (str ?? string.Empty).Trim())
                .Where(str => !string.IsNullOrEmpty(str));

            NotificationTypes = new List<ConditionFormat>(conditionLines.Count());
            AttributeFormatMappingsByXPath = new Dictionary<string, Dictionary<string, Func<string, string>>>();
            XElement notificationSchema = OpenNotificationSchema(AppSettings.NOTIFICATION_SCHEMA_FILE_PATH);
            List<string> invalidConditions = new List<string>(0);
            foreach (string condition in conditionLines)
            {
                Match match = ConditionFileLine.Match(condition);
                if (match.Success)
                {
                    string xPath = match.Groups[1].Value;
                    var attributeFormats = DetermineAttributeFormatMappings(xPath, notificationSchema);
                    AttributeFormatMappingsByXPath.Add(xPath, attributeFormats);
                    NotificationTypes.Add(new ConditionFormat(xPath, match.Groups[2].Value, attributeFormats.Keys.ToArray()));
                }
                else
                {
                    invalidConditions.Add("Condition was [" + condition + "]");
                }
            }

            if (invalidConditions.Count > 0)
            {
                SendEmail.SendMessageToSupport(
                    "Invalid Condition Format",
                    string.Format(
                        "Read invalid condition(s):{0}{1}{0}{0}Expect condition to match /{2}/",
                        Environment.NewLine,
                        invalidConditions.Aggregate(JoinByNewLine),
                        ConditionRegexFormat));
            }
        }

        private static XElement OpenNotificationSchema(string path)
        {
            string notificationSchema = string.Concat(
                @"<Wrapper xmlns:s=""http://SkeletonSchema.info/Structure/"">", // Wrapper allows us to add the namespace
                    File.ReadAllText(path),
                @"</Wrapper>");
            return XDocument.Parse(notificationSchema).Element("Wrapper").Element("NOTIFICATIONS");
        }

        private static Dictionary<string, Func<string, string>> DetermineAttributeFormatMappings(string xPath, XElement notificationSchema)
        {
            XElement schemaElement = notificationSchema.XPathSelectElement(xPath);
            var validAttributes = schemaElement.Attributes().Where(attr => attr.Name.NamespaceName == string.Empty); // ignore s:mult="*", for instance
            var attributeMappings = new Dictionary<string, Func<string, string>>(validAttributes.Count(), StringComparer.Ordinal);
            foreach (var attribute in validAttributes)
            {
                attributeMappings.Add(attribute.Name.LocalName, NotificationMapping.DetermineMapping(attribute.Value));
            }

            return attributeMappings;
        }
    }
}
