﻿namespace UndisclosedDebtNotification
{
    /// <summary>
    /// Represents an address such as the applicant's current address
    /// or the subject property address.
    /// </summary>
    public class Address
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Address"/>
        /// class.
        /// </summary>
        /// <param name="streetAddress">
        /// The street address.
        /// </param>
        /// <param name="city">
        /// The city for the address.
        /// </param>
        /// <param name="state">
        /// The state of the address.
        /// </param>
        /// <param name="zip">
        /// The ZIP code for the address.
        /// </param>
        /// <param name="yearsAtAddress">
        /// The number of years the applicant has been living 
        /// at the address.
        /// </param>
        public Address(string streetAddress, string city, string state, string zip, string yearsAtAddress = null)
        {
            this.StreetAddress = streetAddress;
            this.City = city;
            this.State = state;
            this.Zip = zip;
            this.YearsAtAddress = yearsAtAddress;
        }
        
        /// <summary>
        /// Gets the value for the street address.
        /// </summary>
        /// <value>
        /// The <see cref="string"/> street address.
        /// </value>
        public string StreetAddress { get; private set; }

        /// <summary>
        /// Gets the value for the city.
        /// </summary>
        /// <value>
        /// The <see cref="string"/> city.
        /// </value>
        public string City { get; private set; }

        /// <summary>
        /// Gets the value for the state.
        /// </summary>
        /// <value>
        /// The <see cref="string"/> state.
        /// </value>
        public string State { get; private set; }

        /// <summary>
        /// Gets the value for the ZIP code.
        /// </summary>
        /// <value>
        /// The <see cref="string"/> ZIP code.
        /// </value>
        public string Zip { get; private set; }

        /// <summary>
        /// Gets the value for the number of years the
        /// applicant has been living at the address.
        /// </summary>
        /// <value>
        /// The <see cref="string"/> number of years that the 
        /// applicant has been living at the address.
        /// </value>
        public string YearsAtAddress { get; private set; }

        /// <summary>
        /// Determines whether this instance is valid for a UDN
        /// request by having the required city, state, and ZIP 
        /// information filled out.
        /// </summary>
        /// <returns>
        /// True if this instance is valid, false otherwise.
        /// </returns>
        public bool IsValid()
        {
            return !string.IsNullOrEmpty(this.City) 
                && !string.IsNullOrEmpty(this.State) 
                && !string.IsNullOrEmpty(this.Zip);
        }
    }
}
