﻿namespace UndisclosedDebtNotification
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Xml.Linq;

    public class DB
    {
        private readonly string connectionString;

        static DB()
        {
            Adapter.Drivers.ConnectionStringProviderHelper.Register("UndisclosedDebtNotification");
        }

        public DB(string databaseName)
        {
            this.connectionString = Adapter.Drivers.ConnectionStringProviderHelper.GetMainConnectionStringByDatabaseName(databaseName, needWriteAccess: false);
        }

        /// <summary>
        /// A default constructor for faking.
        /// </summary>
        protected DB()
        {
        }

        public virtual Guid GetBrokerID(string sCustomerCode)
        {
            using (var connection = new SqlConnection(this.connectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand(@"GetBrokerIdByCustomerCode", connection);
                cmd.Parameters.AddWithValue("CustomerCode", sCustomerCode);
                cmd.CommandTimeout = 60;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return new Guid(reader["BrokerId"].ToString());
                    }
                    else
                    {
                        return Guid.Empty;
                    }
                }
            }
        }

        public virtual Dictionary<Guid, string> GetServiceCompanyIdsAndUrls()
        {
            Dictionary<Guid, string> companyDictionary = new Dictionary<Guid, string>();
            using (var connection = new SqlConnection(this.connectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand(@"ListAllCras", connection);
                cmd.CommandTimeout = 180;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        if ((int)reader["ProtocolType"] == 0)
                        {
                            companyDictionary.Add((Guid)reader["ComId"], (string)reader["ComUrl"]);
                        }
                    }
                }
            }

            return companyDictionary;
        }

        public virtual HashSet<Guid> GetBranchIDsByBrokerID(Guid brokerID)
        {
            HashSet<Guid> branchIDs = new HashSet<Guid>();
            using (var connection = new SqlConnection(this.connectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand(@"ListBranchByBrokerID", connection);
                cmd.Parameters.AddWithValue("BrokerID", brokerID);
                cmd.CommandTimeout = 60;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        branchIDs.Add(new Guid(reader["BranchID"].ToString()));
                    }
                }
            }

            return branchIDs;
        }

        public virtual List<LoanNmAppId> GetAppCandidates(Lender lender, Guid branchId, int channel, E_RequestType requestType)
        {
            List<LoanNmAppId> appsToRequest = new List<LoanNmAppId>();
            long elapsedTime;

            using (var connection = new SqlConnection(this.connectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand(@"GetAppsForUDNActivationDeactivation", connection);
                cmd.Parameters.AddWithValue("sBrokerId", lender.BrokerID);
                cmd.Parameters.AddWithValue("sBranchId", branchId);
                cmd.Parameters.AddWithValue("sBranchChannelT", channel);

                bool isActivation = requestType == E_RequestType.NEW;
                cmd.Parameters.AddWithValue("IsActivation", isActivation);

                if (isActivation || requestType == E_RequestType.DEACTIVATE)
                {
                    var settings = (ActivationDeactivation.LenderActivationSettings)lender.Settings;

                    if (isActivation)
                    {
                        cmd.Parameters.Add(StatusSqlParameter(settings.ActivationTrigger(channel), true));

                        cmd.Parameters.AddWithValue("AcceptableLoanTypeXml", GenerateXmlParameter(settings.AcceptableLoanTypes));

                        cmd.Parameters.AddWithValue("AcceptableLoanPurposeXml", GenerateXmlParameter(settings.AcceptableLoanPurposes));
                    }

                    foreach (var trigger in settings.DeactivationTriggers)
                    {
                        cmd.Parameters.Add(StatusSqlParameter(trigger, false));
                    }
                }
                else //if (requestType == E_RequestType.GET)
                {
                    foreach (var trigger in LQBFields.TriggerFields)
                    {
                        cmd.Parameters.Add(StatusSqlParameter(trigger, false));
                    }
                }

                cmd.CommandTimeout = 180;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
                sw.Start();
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    sw.Stop();
                    elapsedTime = sw.ElapsedMilliseconds;
                    while (reader.Read())
                    {
                        appsToRequest.Add(new LoanNmAppId((string)reader["sLNm"], (Guid)reader["aAppId"]));
                    }
                }
            }

            lender.LogDBQueryResult(requestType, elapsedTime, appsToRequest.Count);
            return appsToRequest;
        }

        private static SqlParameter StatusSqlParameter(string sStatusName, bool isActivationStatus)
        {
            return new SqlParameter("Include_" + sStatusName, isActivationStatus);
        }

        private static string GenerateXmlParameter(List<int> values)
        {
            var valueElements = values.SelectOrNull(value => new XElement("item", value));

            return new XElement("root", valueElements).ToString(SaveOptions.DisableFormatting);
        }
    }

    public struct LoanNmAppId
    {
        public readonly string LoanNm;
        public readonly Guid AppId;

        public LoanNmAppId(string loanNm, Guid appId)
        {
            LoanNm = loanNm;
            AppId = appId;
        }
    }
}
