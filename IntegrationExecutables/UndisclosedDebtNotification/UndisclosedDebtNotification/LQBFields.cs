﻿namespace UndisclosedDebtNotification
{
    using System;
    using System.Linq;

    /// <summary>
    /// Methods without a CustomerCode parameter will not log.
    /// </summary>
    public class LQBFields
    {
        public enum ApplicantType
        {
            Individual = 0,
            CoSigner = 1,
            TitleOnly = 2,
            NonTitleSpouse = 3,
            Undefined = 4
        }

        #region Trigger Fields
        //When adding a field, add here and add a new bit to the Stored Procedure
        internal static string[] TriggerFields
        {
            get
            {
                return new string[] {
                "sLeadD",
                "sOpenedD",
                "sPreQualD",
                "sSubmitD",
                "sPreProcessingD",
                "sProcessingD",
                "sDocumentCheckD",
                "sDocumentCheckFailedD",
                "sLoanSubmittedD",
                "sPreUnderwritingD",
                "sUnderwritingD",
                "sPreApprovD",
                "sEstCloseD",
                "sApprovD",
                "sConditionReviewD",
                "sFinalUnderwritingD",
                "sPreDocQCD",
                "sClearToCloseD",
                "sDocsOrderedD",
                "sDocsDrawnD",
                "sDocsD",
                "sDocsBackD",
                "sFundingConditionsD",
                "sFundD",
                "sRecordedD",
                "sFinalDocsD",
                "sClosedD",
                "sSubmittedForPurchaseReviewD",
                "sInPurchaseReviewD",
                "sPrePurchaseConditionsD",
                "sSubmittedForFinalPurchaseD",
                "sInFinalPurchaseReviewD",
                "sClearToPurchaseD",
                "sPurchasedD",
                "sReadyForSaleD",
                "sShippedToInvestorD",
                "sSuspendedByInvestorD",
                "sCondSentToInvestorD",
                "sAdditionalCondSentD",
                "sLPurchaseD",
                "sCounterOfferD",
                "sOnHoldD",
                "sCanceledD",
                "sSuspendedD",
                "sRejectD",
                "sWithdrawnD",
                "sArchivedD"
                };
            }
        }
        #endregion
        #region Channels
        private static string[] channels = new string[] {
                "",// = 0
                "Retail",// = 1
                "Wholesale",// = 2
                "Correspondent",// = 3
                "Broker"// = 4
        };
        #endregion
        #region Roles
        private static string[] roles = new string[] {
                "BrokerProcessor",
                "CallCenterAgent",
                "Closer",
                "CollateralAgent",
                "CreditAuditor",
                "DisclosureDesk",
                "DocDrawer",
                "Funder",
                "Insuring",
                "JuniorProcessor",
                "JuniorUnderwriter",
                "LegalAuditor",
                "LenderAccExec",
                "LoanOfficerAssistant",
                "LoanOpener",
                "LoanRep", //Loan Officer
                "LockDesk",
                "Manager",
                "PostCloser",
                "Processor",
                "Purchaser",
                "QCCompliance",
                "RealEstateAgent",
                "Secondary",
                "Servicing",
                "Shipper",
                "Underwriter"
        };
        #endregion
        #region Loan purposes
        private static string[] loanPurposes = new[]
        {
            "Purchase",
            "RefiRateTerm",
            "RefinanceCashout",
            "Construction",
            "ConstructionPerm",
            "Other",
            "FHAStreamlinedRefinance",
            "VAIRRRL",
            "HomeEquity"
        };
        #endregion
        #region Loan types
        private static string[] loanTypes = new[]
        {
            "Conventional",
            "FHA",
            "VA",
            "USDARural",
            "Other"
        };
        #endregion

        public static bool TryConfirmField(string sFieldName, out string sCorrectFieldName, string sCustomerCode)
        {
            bool bSuccess = TryConfirmField(sFieldName, out sCorrectFieldName);
            if (!bSuccess)
                SettingsErrorLog.LogFormat("{0}: Field not found: '{1}'", sCustomerCode, sFieldName);

            return bSuccess;
        }

        public static bool TryGetIntChannel(string sChannelName, out int sChannel, string sCustomerCode)
        {
            bool bSuccess = TryGetIntChannel(sChannelName, out sChannel);
            if (!bSuccess)
                SettingsErrorLog.LogFormat("{0}: Channel not found: '{1}'", sCustomerCode, sChannelName);

            return bSuccess;
        }

        public static bool TryGetIntLoanPurpose(string loanPurpose, out int parsedLoanPurpose, string customerCode)
        {
            var success = TryGetIntLoanPurpose(loanPurpose, out parsedLoanPurpose);

            if (!success)
            {
                SettingsErrorLog.LogFormat("{0}: Loan purpose not found: '{1}'", customerCode, loanPurpose);
            }

            return success;
        }

        public static bool TryGetIntLoanType(string loanType, out int parsedLoanType, string customerCode)
        {
            var success = TryGetIntLoanType(loanType, out parsedLoanType);

            if (!success)
            {
                SettingsErrorLog.LogFormat("{0}: Loan type not found: '{1}'", customerCode, loanType);
            }

            return success;
        }

        public static bool TryConfirmRole(string sRoleName, out string sCorrectRole, string sCustomerCode)
        {
            bool bSuccess = TryConfirmRole(sRoleName, out sCorrectRole);
            if (!bSuccess)
               SettingsErrorLog.LogFormat("{0}: Role not found: '{1}'", sCustomerCode, sRoleName);

            return bSuccess;
        }

        public static string Channel_rep(int channel)
        {
            if (channel < channels.Length && channel >= 0)
            {
                return channels[channel];
            }
            return string.Empty;
        }

        public static bool TryConfirmField(string sFieldName, out string sCorrectFieldName)
        {
            return TryConfirmString(TriggerFields, sFieldName, out sCorrectFieldName);
        }

        public static bool TryGetIntChannel(string sChannelName, out int sChannel)
        {
            sChannel = GetArrayIndex(channels, sChannelName);
            return sChannel != -1;
        }

        public static bool TryGetIntLoanPurpose(string loanPurpose, out int parsedLoanPurpose)
        {
            parsedLoanPurpose = GetArrayIndex(loanPurposes, loanPurpose);
            return parsedLoanPurpose != -1;
        }

        public static bool TryGetIntLoanType(string loanType, out int parsedLoanType)
        {
            parsedLoanType = GetArrayIndex(loanTypes, loanType);
            return parsedLoanType != -1;
        }

        public static bool TryConfirmRole(string sRoleName, out string sCorrectRole)
        {
            if (sRoleName.Equals("LoanOfficer", StringComparison.OrdinalIgnoreCase))
                sRoleName = "LoanRep"; //LoanOfficer
            return TryConfirmString(roles, sRoleName, out sCorrectRole);
        }

        private static bool TryConfirmString(string[] stringArray, string key, out string KeyMatch)
        {
            KeyMatch = stringArray.FirstOrDefault(role => role.Equals((key ?? string.Empty).Trim(), StringComparison.OrdinalIgnoreCase));
            return KeyMatch != null;
        }

        private static int GetArrayIndex(string[] searchArray, string searchString)
        {
            searchString = (searchString ?? string.Empty).Trim();

            return Array.FindIndex(searchArray, arrayItem => arrayItem.Equals(searchString, StringComparison.OrdinalIgnoreCase));
        }
    }
}
