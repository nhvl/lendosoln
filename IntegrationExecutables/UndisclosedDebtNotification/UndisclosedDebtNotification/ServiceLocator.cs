﻿namespace UndisclosedDebtNotification
{
    using System.Collections.Specialized;

    public class ServiceLocator
    {
        protected ServiceLocator()
        {
        }

        public static ServiceLocator Instance { get; private set; } = new ServiceLocator();

        public static void ReplaceServiceLocator(ServiceLocator locator)
        {
            Instance = locator;
        }

        public virtual NameValueCollection AppSettings
        {
            get { return System.Configuration.ConfigurationManager.AppSettings; }
        }

        public virtual DB CreateDB(string databaseName)
        {
            return new DB(databaseName);
        }

        public virtual com.lendingqb.Auth.IAuthService CreateAuthService(string url)
        {
            return new com.lendingqb.Auth.AuthService() { Url = url };
        }

        public virtual com.lendingqb.Loan.ILoan CreateLoan(string url)
        {
            return new com.lendingqb.Loan.Loan() { Url = url };
        }

        public virtual EmailSender CreateEmailSender(string host)
        {
            return new EmailSender(host);
        }
    }
}
