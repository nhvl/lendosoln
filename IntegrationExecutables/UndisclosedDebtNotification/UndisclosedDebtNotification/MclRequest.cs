﻿namespace UndisclosedDebtNotification
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Text;

    public static class MclRequest
    {
        private static Dictionary<Guid, string> m_companyBaseUrlDictionary;
        public static void LookupCompanyUrls(DB database)
        {
            m_companyBaseUrlDictionary = database.GetServiceCompanyIdsAndUrls();
        }

        /// <summary>
        /// Determines whether the credit reporting agency with the 
        /// specified <paramref name="craComId"/> is a known MCL CRA.
        /// </summary>
        /// <param name="craComId">
        /// The unique ID for the CRA.
        /// </param>
        /// <returns>
        /// True if the CRA is a known MCL CRA, false otherwise.
        /// </returns>
        public static bool IsValidCra(Guid craComId)
        {
            return MclRequest.m_companyBaseUrlDictionary.ContainsKey(craComId);
        }

        public static List<ResponseData> SendRequest(Credentials mclCredentials, Guid craComId, RequestData requestData, E_RequestType requestType)
        {
            var responses = new List<ResponseData>(2);
            if (requestData.Borrower == E_Borrower.JOINT && requestType == E_RequestType.GET)
            {
                requestData.Borrower = E_Borrower.APPLICANT;
                responses.Add(SendRequest(mclCredentials, craComId, requestData));
                requestData.Borrower = E_Borrower.SPOUSE;
                responses.Add(SendRequest(mclCredentials, craComId, requestData));
            }
            else
            {
                responses.Add(SendRequest(mclCredentials, craComId, requestData));
            }

            return responses;
        }

        private static ResponseData SendRequest(Credentials mclCredentials, Guid craComId, RequestData requestData)
        {
            string url = MclRequest.GetUrl(craComId);

            string xmlRequest = MclXml.WriteUDNRequest(mclCredentials, requestData);
            byte[] bytes = Encoding.UTF8.GetBytes(xmlRequest);

            string sResponse = SendRequestToMCL(url, bytes);

            return MclXml.ReadUDNResponse(sResponse);
        }

        private static string GetUrl(Guid comId)
        {
            string baseUrl;
            if (m_companyBaseUrlDictionary.TryGetValue(comId, out baseUrl))
            {
                return GetCompleteUDNUrl(baseUrl);
            }

            throw new KeyNotFoundException(string.Format("Company Url was not found for ComId='{0}'", comId));
        }

        private static string GetCompleteUDNUrl(string baseUrl)
        {
            return baseUrl + "/inetapi/get_debt_monitoring.aspx";
        }

        private static string SendRequestToMCL(string url, byte[] bytes)
        {
            #region Code from CreditReportServer.cs/MclCreditReportRequest.cs
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);

            webRequest.KeepAlive = false; // 11/4/2004 dd - DO NOT REMOVE THIS LINE. Else you will get "Cannot access a disposed object named "System.Net.TlsStream"" error randomly.
            webRequest.ContentType = "application/x-www-form-urlencoded";

            webRequest.ContentType = "application/xml";
            webRequest.Method = "POST";

            webRequest.ContentLength = bytes.Length;

            using (Stream stream = webRequest.GetRequestStream())
            {
                stream.Write(bytes, 0, bytes.Length);
            }

            using (WebResponse webResponse = webRequest.GetResponse())
            {
                StringBuilder sb = new StringBuilder();
                using (Stream stream = webResponse.GetResponseStream())
                {
                    byte[] buffer = new byte[60000];
                    int size = stream.Read(buffer, 0, buffer.Length);
                    while (size > 0)
                    {
                        sb.Append(System.Text.Encoding.UTF8.GetString(buffer, 0, size));
                        size = stream.Read(buffer, 0, buffer.Length);
                    }
                }
                return sb.ToString();
            }
            #endregion
        }
    }
}
