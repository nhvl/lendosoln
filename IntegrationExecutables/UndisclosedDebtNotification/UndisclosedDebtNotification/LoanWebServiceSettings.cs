﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UndisclosedDebtNotification
{
    public static class LoanWebServiceSettings
    {
        private const string LOCALDOMAIN = "http://localhost/lendersofficeapp/los/webservice/";
        private const string DEVDOMAIN = "http://dev.lendersoffice.com/los/webservice/";
        private const string PRODDOMAIN = "https://webservices.lendingqb.com/los/webservice/";
        private const string CMGDOMAIN = "http://lqb.cmgmortgage.com/los/webservice/";
        private const string EDOCSPRODURL = "https://edocs.lendingqb.com/los/webservice/loan.asmx";

        public enum Environment
        {
            DEV,
            LOCAL,
            PRODUCTION,
            CMG
        }

        public static string AuthURL(Environment testEnv)
        {
            return ServiceDomain(testEnv) + "AuthService.asmx";
        }
        public static string LoanURL(Environment testEnv)
        {
            return ServiceDomain(testEnv) + "Loan.asmx";
        }
        public static string EdocsURL(Environment testEnv)
        {
            switch (testEnv)
            {
                case Environment.PRODUCTION: return EDOCSPRODURL;
                case Environment.CMG:
                case Environment.DEV:
                case Environment.LOCAL:
                default:
                    return LoanURL(testEnv);
            }
        }

        public static string EdocsBackupURL(Environment environment)
        {
            switch (environment)
            {
                case Environment.PRODUCTION: return LoanURL(environment);
                default: return EdocsURL(environment);
            }
        }

        private static string ServiceDomain(Environment serviceEnv)
        {
            switch (serviceEnv)
            {
                case Environment.CMG: return CMGDOMAIN;
                case Environment.PRODUCTION: return PRODDOMAIN;
                case Environment.DEV: return DEVDOMAIN;
                case Environment.LOCAL:
                default: return LOCALDOMAIN;
            }
        }

    }
}
