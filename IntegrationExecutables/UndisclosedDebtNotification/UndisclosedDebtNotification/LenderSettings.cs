﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace UndisclosedDebtNotification
{
    public abstract class LenderSettings
    {
        private Dictionary<int, string[]> m_errorRoleDictionary;

        public string CustomerCode { get; private set; }
        public Credentials MCL { get; private set; }
        public Credentials LQB { get; private set; }
        public X509Certificate2 LqbCertificate { get; private set; }
        public List<int> Channels { get; private set; }
        public HashSet<Guid> BranchIdsToExclude { get; private set; }
        public HashSet<Guid> BranchIdsToInclude { get; private set; }

        /// <summary>
        /// Gets the value identifying the credit reporting agency
        /// used at the lender level.
        /// </summary>
        /// <value>
        /// The <see cref="Guid"/> for the CRA at the lender-level.
        /// </value>
        public Guid? LenderCraComId { get; private set; }

        /// <summary>
        /// Gets the value for the mappings between a lender's branches
        /// and the credit reporting agencies configured for those branches.
        /// </summary>
        /// <value>
        /// The <see cref="Dictionary{Guid, Guid}"/> containing the
        /// branch to CRA mappings.
        /// </value>
        public Dictionary<Guid, Guid> BranchCraComIds { get; private set; }

        public LenderSettings(string sCustomerCode)
        {
            CustomerCode = sCustomerCode;
            MCL = AppSettings.LENDER_MCL_CREDENTIALS(sCustomerCode);
            LQB = AppSettings.LENDER_LQB_CREDENTIALS(sCustomerCode);
            this.LqbCertificate = AppSettings.LENDER_LQB_CERTIFICATE(sCustomerCode);
            Channels = AppSettings.LENDER_CHANNELS(sCustomerCode);
            m_errorRoleDictionary = AppSettings.LENDER_ERROR_ROLE(sCustomerCode);
            BranchIdsToExclude = AppSettings.LENDER_BRANCH_EXCLUDE(sCustomerCode);
            BranchIdsToInclude = AppSettings.LENDER_BRANCH_INCLUDE(sCustomerCode);
            this.LenderCraComId = AppSettings.LENDER_CRA_COM_ID(sCustomerCode);
            this.BranchCraComIds = AppSettings.LENDER_BRANCH_CRA_COM_IDS(sCustomerCode);
        }

        public string ErrorRole(int channel)
        {
            return m_errorRoleDictionary[channel][0];
        }

        public string ErrorRoleEmailBackup(int channel)
        {
            return m_errorRoleDictionary[channel][1];
        }

        /// <summary>
        /// Obtains the ID for the credit report agency assigned
        /// to the branch with the specified <paramref name="branchId"/>.
        /// </summary>
        /// <param name="branchId">
        /// The <see cref="Guid"/> identifying the branch.
        /// </param>
        /// <returns>
        /// The <see cref="Guid"/> identifying the CRA if one is 
        /// explictly assigned to the branch, <see cref="LenderCraComId"/>
        /// otherwise.
        /// </returns>
        public Guid GetCraComId(Guid branchId)
        {
            Guid craComId;

            if (!this.BranchCraComIds.TryGetValue(branchId, out craComId))
            {
                craComId = this.LenderCraComId.Value;
            }

            return craComId;
        }

        public virtual bool IsComplete()
        {
            return null != MCL
                && null != LQB
                && null != Channels
                && this.HasValidCras()
                && Tools.VerifyDictionaryLoadedAllKeys(m_errorRoleDictionary, Channels,
                    channel => SettingsErrorLog.LogNotFoundForChannel(CustomerCode, "Error Role/Email", channel));
        }

        /// <summary>
        /// Determines whether the lender has a known MCL credit reporting 
        /// agency specified at the lender-level and, for each of the lender's 
        /// branches with a CRA specified, that the associated branch CRA is a 
        /// known MCL CRA.
        /// </summary>
        /// <returns>
        /// True if the lender has a CRA specified and all lender-level and 
        /// branch-level CRAs are known MCL CRAs, false otherwise.
        /// </returns>
        private bool HasValidCras()
        {
            var validBranchCraComIds = this.BranchCraComIds.Values.All(craComId =>
                MclRequest.IsValidCra(craComId));

            var validLenderCraComId = this.LenderCraComId.HasValue 
                && MclRequest.IsValidCra(this.LenderCraComId.Value);

            if (!validBranchCraComIds || !validLenderCraComId)
            {
                SettingsErrorLog.LogFormat(
                    "The lender-level and/or branch-level CRAs configured for {0} are invalid or are not MCL CRAs. Please verify the configuration.",
                    this.CustomerCode);

                return false;
            }

            return true;
        }
    }
}
