﻿namespace UndisclosedDebtNotification
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Represents a request to get data for or deactivate
    /// UDN monitoring.
    /// </summary>
    public class GetDeactivateRequestData : RequestData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GetDeactivateRequestData"/>
        /// class.
        /// </summary>
        /// <param name="dataDictionary">
        /// The dictionary containing the data required to populate the request,
        /// including the UDN or MCL report ID.
        /// </param>
        /// <param name="application">
        /// The application for the request.
        /// </param>
        /// <param name="errorEmail">
        /// The email address used when the request encounters an error.
        /// </param>
        /// <param name="backupEmail">The backup email address.</param>
        /// <param name="requestType">
        /// The type of the request, either <see cref="E_RequestType.GET"/> or 
        /// <see cref="E_RequestType.DEACTIVATE"/>.
        /// </param>
        public GetDeactivateRequestData(Dictionary<string, string> dataDictionary, LoanNmAppId application, string errorEmail, string backupEmail, E_RequestType requestType)
            : base(dataDictionary, application, errorEmail, backupEmail, requestType)
        {
        }

        /// <summary>
        /// Gets the value for the report ID that was used to 
        /// activate UDN monitoring.
        /// </summary>
        /// <value>
        /// The <see cref="int"/> report ID.
        /// </value>
        public int? ReportId { get; private set; }

        /// <summary>
        /// Gets the value for the id of MCL credit reporting agency
        /// associated with the order.
        /// </summary>
        /// <value>
        /// The <see cref="Guid"/> identifying the CRA or null
        /// if the order does not have an associated MCL CRA.
        /// </value>
        public Guid? MclComId { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether the report data 
        /// for the request was loaded using MCL credit report data, 
        /// rather than values on the loan file.
        /// </summary>
        /// <value>
        /// True if the request is using MCL data as a fallback,
        /// false otherwise.
        /// </value>
        private bool UsingMclDataFallback { get; set; }

        /// <summary>
        /// Determines whether this instance is valid, namely that a report 
        /// ID is on file and, if the request uses MCL data, that there is 
        /// an ID for the associated MCL credit reporting agency.
        /// </summary>
        /// <returns>
        /// True if this instance is valid, false otherwise.
        /// </returns>
        public override bool IsValid()
        {
            return base.IsValid()
                && this.ReportId.HasValue
                && (!this.UsingMclDataFallback || this.MclComId.HasValue);
        }

        /// <summary>
        /// Populates applicant and spouse data from the supplied
        /// <paramref name="dataDictionary"/>.
        /// </summary>
        /// <param name="dataDictionary">
        /// The dictionary containing borrower and/or co-borrower data
        /// used to populate the request.
        /// </param>
        protected override void PopulateApplicantData(Dictionary<string, string> dataDictionary)
        {
            string parseTemp;
            int reportIdTemp;

            if (dataDictionary.TryGetValue("aUdnOrderId", out parseTemp) && int.TryParse(parseTemp, out reportIdTemp))
            {
                this.ReportId = reportIdTemp;

                base.PopulateApplicantData(dataDictionary);
            }
            else if (Tools.GetBoolValueFromDictionary(dataDictionary, "MclCreditReport_Found")
                && dataDictionary.TryGetValue("MclCreditReport_ExternalFileId", out parseTemp)
                && int.TryParse(parseTemp, out reportIdTemp))
            {
                this.ReportId = reportIdTemp;
                this.UsingMclDataFallback = true;

                this.PopulateApplicantDataFromMclCreditReport(dataDictionary);

                Logger.LogInfo("Could not obtain aUdnOrderId from payload, falling back to MCL credit report.", this.Application.LoanNm);
            }
            else
            {
                var emailBody = string.Format(
                    "Undisclosed Debt Notification could not be {0} for an application on loan {1} because the UDN File Number is blank. " +
                    "Please verify the UDN service information for all applications on file.",
                    Tools.GetRequestAction(this.RequestType),
                    this.Application.LoanNm);

                SendEmail.SendErrorMessageToClient(this, new[] { emailBody }, this.RequestType, "failed");

                SendEmail.SendMessageToSupport(
                    "Invalid UDN GET/DEACTIVATE Request",
                    emailBody,
                    string.Format("Loan {0}, Application {1}", this.Application.LoanNm, this.Application.AppId),
                    this.RequestType);

                throw new InvalidOperationException(
                    "No valid UDN order ID or MCL credit report ID is associated with the request.");
            }
        }

        /// <summary>
        /// Populates applicant and spouse data using MCL credit report data.
        /// </summary>
        /// <param name="dataDictionary">
        /// The dictionary containing the MCL credit report data.
        /// </param>
        private void PopulateApplicantDataFromMclCreditReport(Dictionary<string, string> dataDictionary)
        {
            string temp;
            if (dataDictionary.TryGetValue("MclCreditReport_ComId", out temp))
            {
                this.MclComId = Tools.GetSafeGuid(temp);
            }

            //// They all should be in the format XXX-XX-XXXX
            string aBSsn = Tools.GetStringValueFromDictionary(dataDictionary, "aBSsn");
            string aCSsn = Tools.GetStringValueFromDictionary(dataDictionary, "aCSsn");
            this.AppIdentifyingSsn = aBSsn;
            string MclBSsn = Tools.GetStringValueFromDictionary(dataDictionary, "MclCreditReport_BorrowerSSN");
            string MclCSsn = Tools.GetStringValueFromDictionary(dataDictionary, "MclCreditReport_CoBorrowerSSN");

            if (this.SsnRegex.IsMatch(MclBSsn))
            {
                if (MclBSsn == aBSsn)
                {
                    this.Applicant = new Borrower(dataDictionary, 'B');
                }
                else if (MclBSsn == aCSsn)
                {
                    this.Applicant = new Borrower(dataDictionary, 'C');
                }
            }

            if (this.SsnRegex.IsMatch(MclCSsn))
            {
                if (MclCSsn == aBSsn)
                {
                    this.Spouse = new Borrower(dataDictionary, 'B');
                }
                else if (MclCSsn == aCSsn)
                {
                    this.Spouse = new Borrower(dataDictionary, 'C');
                }
            }

            if (this.Applicant != null)
            {
                if (this.Spouse != null)
                {
                    this.Borrower = E_Borrower.JOINT;
                }
                else
                {
                    this.Borrower = E_Borrower.APPLICANT;
                }
            }
            else if (this.Spouse != null)
            {
                this.Applicant = this.Spouse; //// Per correspondence with Wesley from MCL we send it as APPLICANT in this case.
                this.Spouse = null;
                this.Borrower = E_Borrower.APPLICANT;
            }
        }
    }
}
