﻿namespace UndisclosedDebtNotification
{
    public enum E_Borrower
    {
        Undefined = 0,
        APPLICANT = 1,
        SPOUSE = 2,
        JOINT = 3
    }
}
