﻿namespace UndisclosedDebtNotification
{
    public enum E_RequestType
    {
        Undefined = 0,
        ACTIVATE = 1,
        DEACTIVATE = 2,
        GET = 3,
        UPDATE = 4,
        NEW = 5
    }
}
