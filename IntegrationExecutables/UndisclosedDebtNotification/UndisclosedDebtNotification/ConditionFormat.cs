﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace UndisclosedDebtNotification
{
    public class ConditionFormat
    {
        /// <summary>
        /// Gets the XPath of the given condition, where all the arguments of the condition's format can be found as attributes.
        /// </summary>
        public string XPath { get; private set; }

        /// <summary>
        /// Gets the names of the arguments to the composite format string Format.  These will need to be read from the xml.
        /// </summary>
        public string[] Attributes { get; private set; }

        /// <summary>
        /// Gets the composite format string for the condition.
        /// </summary>
        public string Format { get; private set; }

        public ConditionFormat(string xPath, string conditionFormat, string[] attributes)
        {
            XPath = xPath;
            Attributes = attributes;
            Format = conditionFormat;
            int i = 0;
            foreach (string attributeName in Attributes)
            {
                Format = Format.Replace("<" + attributeName + ">", "{" + i++ + "}");
            }
        }
    }
}
