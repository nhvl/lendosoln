﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UndisclosedDebtNotification
{
    public class ImportGetDataFailureException : Exception
    {
        public string ClientMessage { get; }

        public ImportGetDataFailureException(string clientMessage, Exception innerException)
            : base("Unable to save GET response: " + clientMessage, innerException)
        {
            this.ClientMessage = clientMessage;
        }
    }
}
