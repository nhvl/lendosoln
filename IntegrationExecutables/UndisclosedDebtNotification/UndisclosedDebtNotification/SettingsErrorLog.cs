﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UndisclosedDebtNotification
{
    public static class SettingsErrorLog
    {
        private static StringBuilder log = new StringBuilder();

        public static string RetrieveAndClear()
        {
            string errorLog = log.ToString();
            log.Length = 0;
            return errorLog;
        }

        public static void LogNotFoundForChannel(string sCustomerCode, string whatNotFound, int channel)
        {
            LogFormat("{0}: {1} not found for channel '{2}'", sCustomerCode, whatNotFound, LQBFields.Channel_rep(channel));
        }

        public static void Log(string value)
        {
            log.AppendLine(value);
        }

        public static void Log<T>(string description, IEnumerable<T> enumerable, string delimiter)
        {
            StringBuilder sb = new StringBuilder(description);
            foreach (T value in enumerable)
            {
                sb.Append(value + delimiter);
            }

            if (delimiter != null && enumerable.Any())
                sb.Remove(sb.Length - delimiter.Length, delimiter.Length);

            log.AppendLine(sb.ToString());
        }

        public static void LogFormat(string format, params object[] args)
        {
            log.AppendFormatLine(format, args);
        }

        public static void AppendFormatLine(this StringBuilder sb, string format, params object[] args)
        {
            sb.AppendFormat(format + Environment.NewLine, args);
        }
    }
}
