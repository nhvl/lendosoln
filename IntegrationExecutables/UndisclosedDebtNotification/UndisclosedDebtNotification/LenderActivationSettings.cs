﻿namespace UndisclosedDebtNotification.ActivationDeactivation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Represents the Lender Settings for Activation/Deactivation requests.
    /// </summary>
    public class LenderActivationSettings : LenderSettings
    {
        private Dictionary<int, string> m_triggerDictionary;

        public List<string> DeactivationTriggers { get; private set; }

        public List<int> AcceptableLoanPurposes { get; private set; }

        public List<int> AcceptableLoanTypes { get; private set; }

        public string ActivationTrigger(int sChannel)
        {
            return m_triggerDictionary[sChannel];
        }

        public LenderActivationSettings(string sCustomerCode) : base(sCustomerCode)
        {
            m_triggerDictionary = AppSettings.LENDER_C_STATUS(sCustomerCode);
            DeactivationTriggers = AppSettings.LENDER_D_STATUSES(sCustomerCode);
            this.AcceptableLoanPurposes = AppSettings.LENDER_LOAN_PURPOSES(sCustomerCode);
            this.AcceptableLoanTypes = AppSettings.LENDER_LOAN_TYPES(sCustomerCode);
        }

        public override bool IsComplete()
        {
            return base.IsComplete()
                && Tools.VerifyDictionaryLoadedAllKeys(m_triggerDictionary, Channels,
                    channel => SettingsErrorLog.LogNotFoundForChannel(CustomerCode, "C_Status", channel));
        }
    }
}
