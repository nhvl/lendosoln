﻿namespace UndisclosedDebtNotification
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class Borrower
    {
        private enum E_NameSuffix
        {
            Undefined = 0,
            SR = 1,
            JR = 2,
            I = 3,
            II = 4,
            III = 5,
            IV = 6,
            V = 7,
            VI = 8,
            VII = 9,
            VIII = 10,
            IX = 11
        }

        #region Private Member Variables
        E_NameSuffix m_suffix = E_NameSuffix.Undefined;
        string m_ssn = null;
        #endregion
        #region Public Properties
        public char FieldPrefix { get; private set; }
        public string Firstname { get; set; }
        public string Surname { get; set; }
        public string Middlename { get; set; }
        private readonly static E_NameSuffix MAX_SUFFIX = Enum.GetValues(typeof(E_NameSuffix)).Cast<E_NameSuffix>().Max();
        public string Suffix
        {
            get { return EnumMappings.E_NameSuffix[(int)m_suffix]; }
            set
            {
                try
                {
                    m_suffix = (E_NameSuffix)Enum.Parse(typeof(E_NameSuffix), value, true);
                }
                catch (ArgumentNullException)
                {
                    m_suffix = E_NameSuffix.Undefined;
                }
                catch (ArgumentException)
                {
                    m_suffix = E_NameSuffix.Undefined;
                }
                catch (OverflowException)
                {
                    m_suffix = E_NameSuffix.Undefined;
                }

                if (m_suffix > MAX_SUFFIX)
                {
                    m_suffix = E_NameSuffix.Undefined;
                }
            }
        }
        /// <summary>
        /// Removes dashes from XXX-XX-XXXX
        /// </summary>
        public string Ssn
        {
            get { return m_ssn; }
            set { m_ssn = (value ?? string.Empty).Trim().Replace("-", ""); }
        }
        public string Dob { get; set; }
        #endregion

        public Borrower() { }
        public Borrower(Dictionary<string, string> dataDictionary, char BC)
        {
            if (BC != 'B' && BC != 'C')
                throw new Exception("Must be Borrower or Co-Borrower; 'B' or 'C' only valid chars; may not be '" + BC + "'");

            FieldPrefix = BC;
            Firstname   = Tools.GetStringValueFromDictionary(dataDictionary, "a" + BC + "FirstNm");
            Surname     = Tools.GetStringValueFromDictionary(dataDictionary, "a" + BC + "LastNm");
            Middlename  = Tools.GetStringValueFromDictionary(dataDictionary, "a" + BC + "MidNm");
            Suffix      = Tools.GetStringValueFromDictionary(dataDictionary, "a" + BC + "Suffix");
            Ssn         = Tools.GetStringValueFromDictionary(dataDictionary, "a" + BC + "Ssn");
            Dob         = Tools.GetStringValueFromDictionary(dataDictionary, "a" + BC + "Dob");
        }

        public bool IsValid()
        {
            return !string.IsNullOrEmpty(Firstname)
                && !string.IsNullOrEmpty(Surname)
                && !string.IsNullOrEmpty(Ssn);
            //Ssn was matched to regex when determining applicant/spouse
        }
    }
}
