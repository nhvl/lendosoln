﻿namespace UndisclosedDebtNotification
{
    using System;

    /// <summary>
    /// The exception that is thrown when an application does not have 
    /// any valid  applicant types, i.e. there is no applicant on file 
    /// with an <see cref="ApplicantType"/> of <see cref="ApplicantType.Individual"/> 
    /// or <see cref="ApplicantType.CoSigner"/>.
    /// </summary>
    public class NoValidApplicantTypesException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NoValidApplicantTypesException"/>
        /// class with the specified <paramref name="message"/>.
        /// </summary>
        /// <param name="message">
        /// The <see cref="string"/> error message.
        /// </param>
        public NoValidApplicantTypesException(string message) : base(message)
        {
        }
    }
}
