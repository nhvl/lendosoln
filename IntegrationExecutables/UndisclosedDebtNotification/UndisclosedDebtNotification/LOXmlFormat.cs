﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml;
using System.IO;

namespace UndisclosedDebtNotification
{
    public static class LOXmlFormat
    {
        private const string LOXML_VERSION = "1.0";

        public static string WriteLoadRequest(List<string> loanFieldIds, List<string> applicantFieldIds, List<string> loanCollections)
        {
            loanFieldIds.RemoveAll(str => string.IsNullOrEmpty(str));
            loanCollections.RemoveAll(str => string.IsNullOrEmpty(str));
            applicantFieldIds.RemoveAll(str => string.IsNullOrEmpty(str));
            return WriteLOXmlFormatRequest(
                loanFieldIds.Select(id => CreateField(id)),
                applicantFieldIds.Count == 0 ? null
                : new XElement("applicant",
                    applicantFieldIds.Select(id => CreateField(id))
                ),
                loanCollections.Select(id => new XElement("collection", new XAttribute("id", id)))
            );
        }

        public static HashSet<string> ReadLoadConditionsResponse(string sLOXmlResponse)
        {
            XDocument xLOXml = XDocument.Parse(sLOXmlResponse);

            var conditionSet = xLOXml.Descendants("loan").Elements("collection").WhereId("sCondDataSet").FirstOrDefault();
            if (conditionSet == null)
                return new HashSet<string>();

            return new HashSet<string>(conditionSet.Descendants("field").WhereId("FirstComment").Select(cond => cond.Value), StringComparer.OrdinalIgnoreCase);
        }

        public static Dictionary<string, string> ReadLoadResponse(string sLOXmlResponse, Guid aAppId)
        {
            XDocument xLOXml = XDocument.Parse(sLOXmlResponse);
            Dictionary<string, string> LOXmlValues = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

            ReadElementsById(LOXmlValues, xLOXml.Descendants("loan").Elements("field"));

            var applicants = xLOXml.Descendants("applicant");
            //Find the app that matches our AppId
            var applicant = applicants.FirstOrDefault(app => aAppId == new Guid(app.Elements("field").WhereId("aAppId").First().Value));
            if (applicant != null)
            {
                ReadElementsById(LOXmlValues, applicant.Elements("field"));
            }

            return LOXmlValues;
        }

        private static IEnumerable<XElement> WhereId(this IEnumerable<XElement> elements, string id)
        {
            return elements.Where(el => string.Equals(el.SafeAttributeValue("id"), id, StringComparison.OrdinalIgnoreCase));
        }

        private static void ReadElementsById(Dictionary<string, string> idValueDictionary, IEnumerable<XElement> elements)
        {
            XAttribute xID;
            foreach (XElement el in elements)
            {
                xID = el.Attribute("id");
                if (xID != null && !string.IsNullOrEmpty(el.Value) && !idValueDictionary.ContainsKey(xID.Value))
                {
                    idValueDictionary.Add(xID.Value, el.Value);
                }
            }
        }

        /// <summary>
        /// Returns null rather than throwing an Exception when called on a null value.
        /// </summary>
        public static IEnumerable<TResult> SelectOrNull<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, TResult> selector)
        {
            if (source == null)
                return null;

            return source.Select(selector);
        }

        public static string WriteSaveRequest(Dictionary<string, string> loanFields, Dictionary<string, List<Dictionary<string, string>>> loanCollectionsOfRecords, Dictionary<string, Dictionary<string, string>> applications)
        {
            return WriteLOXmlFormatRequest(
                loanFields.SelectOrNull(field => CreateField(field.Key, field.Value)),
                loanCollectionsOfRecords.SelectOrNull(collection =>
                    new XElement("collection",
                        new XAttribute("id", collection.Key),
                        collection.Value.SelectOrNull(record =>
                            new XElement("record",
                                record.SelectOrNull(field => CreateField(field.Key, field.Value)))))),
                applications.SelectOrNull(app =>
                    new XElement("applicant",
                        new XAttribute("id", app.Key),
                        app.Value.SelectOrNull(appField => CreateField(appField.Key, appField.Value))))
            );
        }

        /// <summary>
        /// Creates a new XElement for a field:
        /// &lt;field id="[id]" /&gt;
        /// </summary>
        private static XElement CreateField(string id)
        {
            return CreateField(id, null);
        }

        /// <summary>
        /// Creates a new XElement for a field:
        /// &lt;field id="[id]"&gt;[value]&lt;field&gt;
        /// </summary>
        private static XElement CreateField(string id, object value)
        {
            return new XElement("field",
                new XAttribute("id", id),
                value);
        }

        /// <summary>
        /// Writes a new request
        /// &lt;LOXmlFormat version="1.0"&gt;
        ///     &lt;loan&gt;
        ///         [content]
        ///     &lt;/loan&gt;
        /// &lt;/LOXmlFormat&gt;
        /// </summary>
        /// <param name="content"></param>
        /// <returns>The xml string.</returns>
        private static string WriteLOXmlFormatRequest(params object[] content)
        {
            XDocument LOXmlRequest = new XDocument(
                new XElement("LOXmlFormat",
                    new XAttribute("version", LOXML_VERSION),
                    new XElement("loan", content)
                )
            );
            return LOXmlRequest.ToString();
        }

        public static bool ReadSaveResponse(string resultXml, out string errorMessage)
        {
            XDocument xResponse = XDocument.Parse(resultXml);
            XElement xResult = xResponse.Element("result");
            errorMessage = xResult.HasElements ? xResult.ToString() : null;
            return xResult.Attribute("status").Value.StartsWith("OK", StringComparison.OrdinalIgnoreCase);
        }
    }
}
