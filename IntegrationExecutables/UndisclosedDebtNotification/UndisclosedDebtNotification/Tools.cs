﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Security.Cryptography.X509Certificates;

namespace UndisclosedDebtNotification
{
    public static class Tools
    {
        public static void SetupServicePointCallback()
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            ServicePointManager.ServerCertificateValidationCallback = delegate(object sender, X509Certificate certificate, X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
            {
                if (sslPolicyErrors == System.Net.Security.SslPolicyErrors.None)
                {
                    return true; //If there are no errors then the certificate is valid
                }
                else if (sender is HttpWebRequest)
                {
                    return true; //All URL's used by the application are included in the config file, so approve
                }

                return false;
            };
        }

        /// <summary>
        /// False if not found/not able to be parsed
        /// </summary>
        public static bool GetBoolValueFromDictionary(Dictionary<string, string> dictionary, string key)
        {
            bool value;
            string stringValue;
            if (dictionary.TryGetValue(key, out stringValue) && bool.TryParse(stringValue, out value))
                return value;

            return false;
        }

        /// <summary>
        /// Returns string.Empty on not found
        /// </summary>
        public static string GetStringValueFromDictionary(Dictionary<string, string> dictionary, string key)
        {
            return GetStringValueFromDictionary(dictionary, key, string.Empty);
        }

        public static string GetStringValueFromDictionary(Dictionary<string, string> dictionary, string key, string defaultValue)
        {
            string value;
            if (!dictionary.TryGetValue(key, out value))
                value = defaultValue;

            return value;
        }

        public static string SafeAttributeValue(this System.Xml.Linq.XElement element, string attributeName)
        {
            if (null != element && null != element.Attribute(attributeName))
            {
                return element.Attribute(attributeName).Value;
            }

            return null;
        }

        public static bool VerifyDictionaryLoadedAllKeys<TKey, TValue>(Dictionary<TKey, TValue> dictionary, IEnumerable<TKey> keys, Action<TKey> onKeyNotFound)
        {
            bool keysAllFound = true;
            foreach (TKey key in keys)
            {
                if (!dictionary.ContainsKey(key))
                {
                    onKeyNotFound(key);
                    keysAllFound = false;
                }
            }

            return keysAllFound;
        }

        /// <summary>
        /// Attempts to parse the specified <paramref name="guidString"/>
        /// into a <see cref="Guid"/>.
        /// </summary>
        /// <param name="guidString">
        /// The <see cref="string"/> representation.
        /// </param>
        /// <returns>
        /// A new <see cref="Guid"/> instance if <paramref name="guidString"/>
        /// is a valid representation, null otherwise.
        /// </returns>
        public static Guid? GetSafeGuid(string guidString)
        {
            if (!string.IsNullOrEmpty(guidString))
            {
                try
                {
                    return new Guid(guidString);
                }
                catch (FormatException)
                {
                }
            }

            return null;
        }

        public static Exception UnhandledEnumException(Enum e)
        {
            return new InvalidOperationException("Unhandled Enum: " + e.GetType().Name + "." + e);
        }

        /// <summary>
        /// Obtains the user-friendly UDN request name.
        /// </summary>
        /// <param name="requestType">
        /// The <see cref="E_RequestType"/> for the request.
        /// </param>
        /// <returns>
        /// A user-friendly <see cref="string"/> name for the
        /// UDN request.
        /// </returns>
        /// <exception cref="InvalidOperationException">
        /// The UDN request type is <see cref="E_RequestType.UPDATE"/>.
        /// </exception>
        public static string GetRequestName(E_RequestType requestType)
        {
            switch (requestType)
            {
                case E_RequestType.Undefined:
                    return string.Empty;
                case E_RequestType.ACTIVATE:
                    return "Activation";
                case E_RequestType.NEW:
                    return "New";
                case E_RequestType.DEACTIVATE:
                    return "Deactivation";
                case E_RequestType.GET:
                    return "Get";
                default:
                    throw UnhandledEnumException(requestType);
            }
        }

        /// <summary>
        /// Obtains the user-friendly UDN request action for error emails.
        /// </summary>
        /// <param name="requestType">
        /// The <see cref="E_RequestType"/> for the request.
        /// </param>
        /// <returns>
        /// A user-friendly <see cref="string"/> for the
        /// UDN request type.
        /// </returns>
        /// <exception cref="InvalidOperationException">
        /// The UDN request type is <see cref="E_RequestType.Undefined"/> or
        /// <see cref="E_RequestType.UPDATE"/>.
        /// </exception>
        /// <remarks>
        /// In this context, <see cref="E_RequestType.ACTIVATE"/> and
        /// <see cref="E_RequestType.NEW"/> are synonyms representing the
        /// ordering of UDN monitoring for an application.
        /// </remarks>
        public static string GetRequestAction(E_RequestType requestType)
        {
            switch (requestType)
            {
                case E_RequestType.ACTIVATE:
                case E_RequestType.NEW:
                    return "ordered";
                case E_RequestType.DEACTIVATE:
                    return "deactivated";
                case E_RequestType.GET:
                    return "retrieved";
                default:
                    throw UnhandledEnumException(requestType);
            }
        }

        /// <summary>
        /// Formats the applicant name(s) of the specified <paramref name="request"/>
        /// as Surname, Firstname
        /// </summary>
        /// <param name="applicant">
        /// The <see cref="RequestData"/> with the applicant name(s) to format.
        /// </param>
        /// <returns>
        /// The formatted applicant name(s).
        /// </returns>
        public static string FormatApplicantName(RequestData request)
        {
            switch (request.Borrower)
            {
                case E_Borrower.Undefined:
                case E_Borrower.APPLICANT:
                    return FormatName(request.Applicant);
                case E_Borrower.SPOUSE:
                    return FormatName(request.Spouse);
                case E_Borrower.JOINT:
                    return string.Format("{0} / {1}", FormatName(request.Applicant), FormatName(request.Spouse));
                default:
                    throw UnhandledEnumException(request.Borrower);
            }
        }

        /// <summary>
        /// Formats the name of the specified <paramref name="borrower"/>
        /// as Surname, Firstname.
        /// </summary>
        /// <param name="borrower">
        /// The <see cref="Borrower"/> to format.
        /// </param>
        /// <returns>
        /// The formatted name.
        /// </returns>
        private static string FormatName(Borrower borrower)
        {
            if (borrower == null)
            {
                return string.Empty;
            }

            return string.Format("{0}, {1}", borrower.Surname, borrower.Firstname);
        }
    }
}
