﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UndisclosedDebtNotification
{
    /// <summary>
    /// Represents the data contained in the response.
    /// </summary>
    public class ResponseData
    {
        /// <summary>
        /// Gets or sets a value indicating whether the request was successful.
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// Gets or sets the error message when Success is false (returned by INETAPI_ERROR, the MCL error response).
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Gets or sets the object representing each notification returned in a Get request.
        /// </summary>
        public List<NotificationDataItem> NotificationData { get; set; }

        /// <summary>
        /// Gets or sets a document embedded as a file in Base64 within a response to a Get request.
        /// </summary>
        public string EmbeddedBase64Document { get; set; }

        /// <summary>
        /// Gets or sets the error messages returned from UDN with an ERROR status (sent by Credit Bureaus).
        /// </summary>
        public string[] ErrorMessagesFromCreditBureaus { get; set; }

        /// <summary>
        /// Gets or sets the end_date returned with a DEACTIVE status within a response to a Get request.
        /// </summary>
        public DateTime? DeactivationDate { get; set; }

        /// <summary>
        /// Gets or sets the report id for the response.
        /// </summary>
        public string ReportId { get; set; }

        /// <summary>
        /// Initializes a blank instance of the ResponseData class.
        /// </summary>
        public ResponseData()
        {
            NotificationData = new List<NotificationDataItem>();
        }
    }
}
