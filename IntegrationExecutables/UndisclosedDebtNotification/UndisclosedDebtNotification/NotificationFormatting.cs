﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace UndisclosedDebtNotification
{
    static class NotificationFormatting
    {
        /// <summary>
        /// Writes an attribute name and value in the form used in LQB conditions. Also used for writing report ID, reference number, and consumer in <see cref="NotificationHeadings"/>.
        /// </summary>
        /// <param name="attributeName">Name of the attribute to write.</param>
        /// <param name="value">Value of the attribute.</param>
        /// <returns>A formatted string containing the attribute name and value.</returns>
        public static string AttributeString(string attributeName, string value)
        {
            return $"{attributeName}: {value}";
        }

        /// <summary>
        /// Use Unix newlines since Task comments are stored with this in LQB.
        /// </summary>
        public static readonly string NewLine = "\n";
    }
}
