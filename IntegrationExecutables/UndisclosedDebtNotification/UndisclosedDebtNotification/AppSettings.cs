﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

namespace UndisclosedDebtNotification
{
    public static class AppSettings
    {
        private static readonly string defaultAssignedToRole = "role_underwriter";

        private static readonly string defaultOwnedByRole = "role_underwriter";

        /// <summary>
        /// The default database connection string.  Not to be confused with <seealso cref="LENDER_DSN(string)"/>,
        /// which refers to a particular lender's database connection string.
        /// </summary>
        public static string DSN
        {
            get { return ServiceLocator.Instance.AppSettings["DSN"]; }
        }

        public static string LENDER_DSN(string sCustomerCode)
        {
            return ServiceLocator.Instance.AppSettings[LENDER_DSN_KEY(sCustomerCode)] ?? DSN; // fallback to the general database
        }

        public static string SMTP_SERVER
        {
            get { return ServiceLocator.Instance.AppSettings["SMTP_SERVER"]; }
        }

        public static string SUPPORT_EMAIL
        {
            get { return ServiceLocator.Instance.AppSettings["SUPPORT_EMAIL"]; }
        }

        public static string CONDITION_FORMAT_FILE_PATH
        {
            get { return GetSettingsValue("CONDITION_FORMAT_FILE_PATH"); }
        }

        public static string NOTIFICATION_SCHEMA_FILE_PATH
        {
            get { return GetSettingsValue("NOTIFICATION_SCHEMA_FILE_PATH"); }
        }

        public static LoanWebServiceSettings.Environment ENVIRONMENT
        {
            get
            {
                string name = ServiceLocator.Instance.AppSettings["ENVIRONMENT"];
                if (name == null)
                    return LoanWebServiceSettings.Environment.PRODUCTION;

                try
                {
                    return (LoanWebServiceSettings.Environment)Enum.Parse(typeof(LoanWebServiceSettings.Environment), name, true);
                }
                catch (ArgumentException)
                {
                    SettingsErrorLog.Log("ENVIRONMENT had an invalid value: '" + name + "' It must either be 'DEV', 'LOCAL', 'PRODUCTION', 'CMG', or null (PRODUCTION)");
                    return LoanWebServiceSettings.Environment.PRODUCTION;
                }
            }
        }

        public static string[] LENDERS
        {
            get { return GetSettingsArray("LENDERS"); }
        }

        /// <summary>
        /// Gets the key in the config to the lender's connection string.
        /// </summary>
        private static string LENDER_DSN_KEY(string sCustomerCode)
        {
            string sKey = sCustomerCode + "_DSN_KEY";
            return GetSettingsValue(sKey, true);
        }

        public static Credentials LENDER_MCL_CREDENTIALS(string sCustomerCode)
        {
            string sKey = sCustomerCode + "_MCL_CREDENTIALS";
            return GetCredentials(sKey);
        }

        public static Credentials LENDER_LQB_CREDENTIALS(string sCustomerCode)
        {
            string sKey = sCustomerCode + "_LQB_CREDENTIALS";
            return GetCredentials(sKey);
        }

        public static X509Certificate2 LENDER_LQB_CERTIFICATE(string sCustomerCode)
        {
            string sCertPathKey = sCustomerCode + "_LQB_CERTIFICATE_PATH";
            string sCertPasswordKey = sCustomerCode + "_LQB_CERTIFICATE_PASSWORD";

            string path = GetSettingsValue(sCertPathKey, isSettingOptional: true);
            string password = GetSettingsValue(sCertPasswordKey, isSettingOptional: true);
            if (!string.IsNullOrEmpty(path) && !string.IsNullOrEmpty(password))
            {
                return new X509Certificate2(path, password);
            }

            return null;
        }

        /// <summary>
        /// Obtains the unique ID for lender-level credit reporting
        /// agency.
        /// </summary>
        /// <param name="sCustomerCode">
        /// The <see cref="string"/> customer code for the lender.
        /// </param>
        /// <returns>
        /// A <see cref="Guid"/> identifying the credit reporting agency
        /// at the lender level or null if no setting exists in the configuration.
        /// </returns>
        public static Guid? LENDER_CRA_COM_ID(string sCustomerCode)
        {
            string sKey = sCustomerCode + "_LENDER_CRA_COM_ID";

            var craComSetting = GetSettingsValue(sKey);

            var parsedGuid = Tools.GetSafeGuid(craComSetting);

            if (!parsedGuid.HasValue)
            {
                SettingsErrorLog.Log(sKey + ": '" + craComSetting + "' is not a valid Guid.");
            }

            return parsedGuid;
        }

        public static List<int> LENDER_CHANNELS(string sCustomerCode)
        {
            List<int> channels = new List<int>();
            string sKey = sCustomerCode + "_CHANNELS";
            string[] sChannels = GetSettingsArray(sKey, StringSplitOptions.None); // Keep empty string since it is a valid channel name
            int iChannel;
            foreach (string sChannel in sChannels)
            {
                if (LQBFields.TryGetIntChannel(sChannel, out iChannel, sCustomerCode))
                {
                    channels.Add(iChannel);
                }
            }

            return channels;
        }

        public static List<int> LENDER_LOAN_PURPOSES(string sCustomerCode)
        {
            var loanPurposes = new List<int>();

            string sKey = sCustomerCode + "_LOAN_PURPOSES";
            string[] purposes = GetSettingsArray(sKey);
            int parsedPurpose;
            foreach (var loanPurpose in purposes)
            {
                if (LQBFields.TryGetIntLoanPurpose(loanPurpose, out parsedPurpose, sCustomerCode))
                {
                    loanPurposes.Add(parsedPurpose);
                }
            }

            return loanPurposes;
        }

        public static List<int> LENDER_LOAN_TYPES(string sCustomerCode)
        {
            var loanTypes = new List<int>();

            string sKey = sCustomerCode + "_LOAN_TYPES";
            string[] types = GetSettingsArray(sKey);
            int parsedType;
            foreach (var loanType in types)
            {
                if (LQBFields.TryGetIntLoanType(loanType, out parsedType, sCustomerCode))
                {
                    loanTypes.Add(parsedType);
                }
            }

            return loanTypes;
        }

        public static Dictionary<int, string> LENDER_C_STATUS(string sCustomerCode)
        {
            Dictionary<int, string> statusDictionary = new Dictionary<int, string>();
            string[] channelStatuses = GetSettingsArray(sCustomerCode + "_C_STATUS");
            int channelNum;
            string statusName;
            foreach (string channelStatus in channelStatuses.Where(str => str.Contains(':')))
            {
                string channel = channelStatus.Split(':')[0].Trim();
                string status = channelStatus.Split(':')[1].Trim();
                if (LQBFields.TryGetIntChannel(channel, out channelNum, sCustomerCode) && LQBFields.TryConfirmField(status, out statusName, sCustomerCode))
                {
                    if (statusDictionary.ContainsKey(channelNum))
                    {
                        SettingsErrorLog.Log(sCustomerCode + ": Channel '" + channel + "' appears more than once in the channel status list.");
                        continue;
                    }

                    statusDictionary.Add(channelNum, statusName);
                }
            }

            return statusDictionary;
        }

        public static HashSet<Guid> LENDER_BRANCH_EXCLUDE(string sCustomerCode)
        {
            return LENDER_BRANCHES(sCustomerCode, false);
        }

        public static HashSet<Guid> LENDER_BRANCH_INCLUDE(string sCustomerCode)
        {
            return LENDER_BRANCHES(sCustomerCode, true);
        }

        private static HashSet<Guid> LENDER_BRANCHES(string sCustomerCode, bool lookForInclusiveBranchFilter)
        {
            HashSet<Guid> branchIds = new HashSet<Guid>();
            string sKey = sCustomerCode + "_BRANCH_" + (lookForInclusiveBranchFilter ? "INCLUDE" : "EXCLUDE");
            string[] ids = GetSettingsArray(sKey, isSettingOptional: true);
            foreach (string sBranchId in ids)
            {
                try
                {
                    branchIds.Add(new Guid(sBranchId));
                }
                catch (FormatException)
                {
                    SettingsErrorLog.Log(sKey + ": '" + sBranchId + "' is not valid Guid.");
                }
            }

            return branchIds;
        }

        /// <summary>
        /// Obtains the mappings between a lender's branches and the unique IDs for
        /// the branch-level CRAs.
        /// </summary>
        /// <param name="sCustomerCode">
        /// The <see cref="string"/> customer code for the lender.
        /// </param>
        /// <returns>
        /// A <see cref="Dictionary{Guid, Guid}"/> containing the mappings between
        /// branches and CRAs.
        /// </returns>
        public static Dictionary<Guid, Guid> LENDER_BRANCH_CRA_COM_IDS(string sCustomerCode)
        {
            var branchCraComIds = new Dictionary<Guid, Guid>();

            string sKey = sCustomerCode + "_BRANCH_CRA_COM_IDS";

            foreach (var branchComId in GetSettingsArray(sKey))
            {
                var branchComIdPair = branchComId.Split(':');

                if (branchComIdPair.Length != 2)
                {
                    SettingsErrorLog.Log(
                        sKey + 
                        ": Branch-CRA mapping is not well-defined. Debug string = '" +
                        branchComIdPair + "'");

                    continue;
                }

                var branchId = Tools.GetSafeGuid(branchComIdPair[0]);

                if (!branchId.HasValue)
                {
                    SettingsErrorLog.Log(sKey + ": '" + branchComIdPair[0] + "' is not a valid Guid.");
                    throw new InvalidOperationException();
                }
                else if (branchCraComIds.ContainsKey(branchId.Value))
                {
                    SettingsErrorLog.Log(sKey + ": Branch '" + branchId + "' is defined twice in the configuration.");
                    continue;
                }

                var craComId = Tools.GetSafeGuid(branchComIdPair[1]);

                if (!craComId.HasValue)
                {
                    SettingsErrorLog.Log(sKey + ": '" + branchComIdPair[1] + "' is not a valid Guid.");
                    throw new InvalidOperationException();
                }

                branchCraComIds.Add(branchId.Value, craComId.Value);
            }

            return branchCraComIds;
        }

        /// <summary>
        /// E.G. Retail:Processor,timothyj@meridianlink.com;Wholesale:Underwriter,brianb@lendingqb.com
        ///      Retail:,timothyj@meridianlink.com;
        ///      Retail:Processor;
        /// </summary>
        public static Dictionary<int, string[]> LENDER_ERROR_ROLE(string sCustomerCode)
        {
            Dictionary<int, string[]> errorRoleDictionary = new Dictionary<int, string[]>();
            string sKey = sCustomerCode + "_ERROR_ROLE";
            string[] errorRoles = GetSettingsArray(sKey);
            int channel;
            foreach (string errorRole in errorRoles)
            {
                string[] channelError = errorRole.Split(':');
                if (channelError.Length < 2)
                {
                    SettingsErrorLog.Log(sCustomerCode + ": Error Role for '" + channelError[0] + "' is not well-defined. Debug string = '" + errorRole + "'");
                    continue;
                }
                if (LQBFields.TryGetIntChannel(channelError[0], out channel, sCustomerCode))
                {
                    if (errorRoleDictionary.ContainsKey(channel))
                    {
                        SettingsErrorLog.Log(sCustomerCode + ": Channel '" + channelError[0] + "' appears more than once in the channel error role list.");
                        continue;
                    }

                    string[] roleBackUpEmail = CommaSeparatedRoleEmailToArray(channelError[1], sCustomerCode);
                    errorRoleDictionary.Add(channel, roleBackUpEmail);
                }
            }

            return errorRoleDictionary;
        }

        public static List<string> LENDER_D_STATUSES(string sCustomerCode)
        {
            List<string> statuses = new List<string>();
            string sKey = sCustomerCode + "_D_STATUSES";
            string[] rawStatuses = GetSettingsArray(sKey);
            string clearedStatus;
            foreach (var status in rawStatuses)
            {
                if (LQBFields.TryConfirmField(status.Trim(), out clearedStatus, sCustomerCode))
                {
                    statuses.Add(clearedStatus);
                }
            }

            return statuses;
        }

        public static string LENDER_CONDITION_CATEGORY(string sCustomerCode)
        {
            string sKey = sCustomerCode + "_CONDITION_CATEGORY";
            return GetSettingsValue(sKey);
        }

        public static string LENDER_TASK_PERMISSION_LEVEL(string sCustomerCode)
        {
            string sKey = sCustomerCode + "_TASK_PERMISSION_LEVEL";
            string value = GetSettingsValue(sKey, true);
            int taskPermissionLevelId;
            if (!int.TryParse(value, out taskPermissionLevelId))
            {
                if (value != null)
                {
                    SettingsErrorLog.Log(sKey + " must be the integer corresponding to the Task Permission Level ID to import conditions. Value was '" + taskPermissionLevelId + "'");
                }

                return null;
            }

            return taskPermissionLevelId.ToString();
        }

        public static string LENDER_DOCTYPE(string sCustomerCode)
        {
            string sKey = sCustomerCode + "_DOCTYPE";
            return GetSettingsValue(sKey);
        }

        public static bool ConditionIsHidden(string customerCode)
        {
            string key = customerCode + "_CONDITION_IS_HIDDEN";
            bool result;
            return bool.TryParse(GetSettingsValue(key, isSettingOptional: true), out result) ? result : true;
        }

        public static string AssignedToRole(string customerCode)
        {
            string key = customerCode + "_ASSIGNED_TO_ROLE";
            return GetSettingsValue(key, isSettingOptional: true) ?? defaultAssignedToRole;
        }

        public static string OwnedByRole(string customerCode)
        {
            string key = customerCode + "_OWNED_BY_ROLE";
            return GetSettingsValue(key, isSettingOptional: true) ?? defaultOwnedByRole;
        }

        private static Credentials GetCredentials(string sKey)
        {
            string[] cred = GetSettingsArray(sKey);
            if (cred.Length != 2)
            {
                SettingsErrorLog.Log(sKey + " must be in the form 'Username;Password'");
                return null;
            }

            return new Credentials(cred[0].Trim(), cred[1].Trim());
        }

        private static string GetSettingsValue(string sKey, bool isSettingOptional = false)
        {
            string value = ServiceLocator.Instance.AppSettings[sKey];
            if (!isSettingOptional && value == null)
            {
                SettingsErrorLog.Log(sKey + " was expected but not found in App.config");
            }

            return value;
        }

        /// <summary>
        /// turns value1;value2 into string[] { value1, value2 }
        /// <para>Removes empty entries</para>
        /// </summary>
        private static string[] GetSettingsArray(string sKey, bool isSettingOptional = false)
        {
            return GetSettingsArray(sKey, StringSplitOptions.RemoveEmptyEntries, isSettingOptional);
        }

        /// <summary>
        /// turns value1;value2 into string[] { value1, value2 }
        /// </summary>
        private static string[] GetSettingsArray(string sKey, StringSplitOptions splitOptions, bool isSettingOptional = false)
        {
            return (GetSettingsValue(sKey, isSettingOptional) ?? string.Empty).Split(new char[] { ';' }, splitOptions);
        }

        private static string[] CommaSeparatedRoleEmailToArray(string roleEmailCsv, string sCustomerCode)
        {
            string roleName, backUpEmail;
            string[] roleEmail = roleEmailCsv.Split(',');
            LQBFields.TryConfirmRole(roleEmail[0].Trim(), out roleName, sCustomerCode);
            if (roleEmail.Length > 1)
            {
                backUpEmail = roleEmail[1].Trim();
            }
            else
            {
                backUpEmail = null;
            }
            return new string[] { roleName, backUpEmail };
        }
    }
}
