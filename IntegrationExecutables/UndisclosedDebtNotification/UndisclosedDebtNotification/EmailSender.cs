﻿namespace UndisclosedDebtNotification
{
    using System.Net.Mail;

    /// <summary>
    /// A thin wrapper around <see cref="System.Net.Mail.SmtpClient"/> to allow mocking.
    /// </summary>
    public class EmailSender
    {
        private SmtpClient internalClient;

        protected EmailSender()
        {
        }

        public EmailSender(string host)
        {
            this.internalClient = new SmtpClient(host);
        }

        public virtual void Send(MailMessage message)
        {
            this.internalClient.Send(message);
        }
    }
}
