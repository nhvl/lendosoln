﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UndisclosedDebtNotification
{
    public class NotificationHeadings
    {
        public string ReportId { get; private set; }
        public string ReferenceNumber { get; private set; }
        public string Consumer { get; private set; }

        public NotificationHeadings(string reportId, string referenceNumber, string consumer)
        {
            this.ReportId = reportId;
            this.ReferenceNumber = referenceNumber;
            this.Consumer = consumer;
        }

        /// <summary>
        /// Determines if this <see cref="NotificationHeadings"/> object is equal to another object.
        /// </summary>
        /// <param name="obj">Another <see cref="NotificationHeadings"/> object.</param>
        /// <returns>Whether or not the objects have the same <see cref="ReportId"/>, <see cref="ReferenceNumber"/>, and <see cref="Consumer"/>.</returns>
        public override bool Equals(object obj)
        {
            NotificationHeadings headings = obj as NotificationHeadings;
            if (headings == null || this.ReportId != headings.ReportId || this.ReferenceNumber != headings.ReferenceNumber || this.Consumer != headings.Consumer)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Implemented so that this object can be safely added to a dictionary.
        /// </summary>
        /// <returns>Concatenates the <see cref="ReportId"/>, <see cref="ReferenceNumber"/>, and <see cref="Consumer"/> and returns the hashcode of the resulting string.</returns>
        public override int GetHashCode()
        {
            return (ReportId + ReferenceNumber + Consumer).GetHashCode();
        }

        /// <summary>
        /// Writes the notification heading info in the <see cref="NotificationFormatting.AttributeString(string, string)"/> format, separated by newlines.
        /// </summary>
        /// <returns>The notification heading info formatted as a newline-separated string.</returns>
        public override string ToString()
        {
            return
                $"{NotificationFormatting.AttributeString("report_id", this.ReportId)}{NotificationFormatting.NewLine}" +
                $"{NotificationFormatting.AttributeString("reference_number", this.ReferenceNumber)}{NotificationFormatting.NewLine}" +
                $"{NotificationFormatting.AttributeString("Consumer", this.Consumer)}";
        }
    }
}
