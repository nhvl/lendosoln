﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;

namespace UndisclosedDebtNotification
{
    public static class SendEmail
    {
        public static readonly string DeveloperErrorFallbackEmail = "timothyj@meridianlink.com";

        public static void SendErrorMessageToSupport(Exception exception, E_RequestType requestType)
        {
            SendErrorMessageToSupport(exception, null, null, requestType);
        }

        public static void SendErrorMessageToSupport(Exception exception, Lender lender, string customerCode, E_RequestType requestType)
        {
            StringBuilder messageBody = new StringBuilder();
            messageBody.AppendFormatLine(
                "An exception occurred in the {0} tool{1}." + Environment.NewLine,
                Tools.GetRequestName(requestType),
                string.IsNullOrEmpty(customerCode) ? null : " for " + customerCode);

            messageBody.AppendLine(exception.ToString() + Environment.NewLine);
            if (lender != null)
            {
                messageBody.Append(lender.AllLogsForDebug);
            }

            SendMessageToSupport(exception.GetType().Name, messageBody.ToString(), customerCode, requestType);
        }

        public static void SendErrorLogsToSupport(Lender lender, E_RequestType requestType)
        {
            if (lender.ErrorLogNonEmpty)
                SendMessageToSupport("Errors occured for Lender.", lender.AllLogsForDebug, lender.Settings.CustomerCode, requestType);
        }

        public static void SendMessageToSupport(string subject, string messageBody)
        {
            SendMessageToSupport(subject, messageBody, null, E_RequestType.Undefined);
        }

        public static void SendMessageToSupport(string subject, string messageBody, string customerCode, E_RequestType requestType)
        {
            Logger.LogInfo(messageBody, customerCode);

            MailMessage message = new MailMessage("do-not-reply@lendingqb.com", AppSettings.SUPPORT_EMAIL,
                string.Format("UndisclosedDebtNotification -{0}- {1} - {2}", customerCode, Tools.GetRequestName(requestType), subject),
                messageBody);

            TransmitEmail(message, new string[] { DeveloperErrorFallbackEmail });
        }

        public static void SendErrorMessageToClient(RequestData requestData, IEnumerable<string> errorMessages, E_RequestType requestType, string orderResult)
        {
            MailMessage message = new MailMessage("do-not-reply@lendingqb.com", requestData.ErrorNotificationEmail);
            message.Subject = "UDN " + Tools.GetRequestName(requestType) + " Order for " + requestData.Application.LoanNm + " " + orderResult;
            StringBuilder bodyBuilder = new StringBuilder();
            bodyBuilder.AppendLine("<style>table, td {");
            bodyBuilder.AppendLine("border: 1px solid black;");
            bodyBuilder.AppendLine("border-collapse: collapse;}");
            bodyBuilder.AppendLine(".title {width: 120px;}");
            bodyBuilder.AppendLine("table {width: 500px;}");
            bodyBuilder.AppendLine("</style>");
            bodyBuilder.AppendLine("Undisclosed Debt Notification service order " + orderResult + ":");
            bodyBuilder.AppendLine("<table>");
            bodyBuilder.AppendFormat("<tr><td class='title'>Loan Number</td><td>{0}</td></tr>", requestData.Application.LoanNm);
            bodyBuilder.AppendFormat("<tr><td class='title'>Borrower</td><td>{0}</td></tr>", Tools.FormatApplicantName(requestData));

            if (requestData is GetDeactivateRequestData)
            {
                bodyBuilder.AppendFormat("<tr><td class='title'>UDN Order #</td><td>{0}</td></tr>", ((GetDeactivateRequestData)requestData).ReportId);
            }

            foreach (var errorMessage in errorMessages)
            {
                bodyBuilder.AppendFormat("<tr><td class='title'>Error Message</td><td>{0}</td></tr>", errorMessage);
            }

            bodyBuilder.AppendLine("</table>");
            message.Body = bodyBuilder.ToString();
            message.IsBodyHtml = true;

            TransmitEmail(message, new string[] { requestData.BackupErrorNotificationEmail, AppSettings.SUPPORT_EMAIL, DeveloperErrorFallbackEmail });
        }

        private static void TransmitEmail(MailMessage message, IEnumerable<string> backupEmailList)
        {
            if (string.IsNullOrEmpty(AppSettings.SMTP_SERVER))
            {
                return;
            }

            try
            {
                var smtpClient = ServiceLocator.Instance.CreateEmailSender(AppSettings.SMTP_SERVER);
                smtpClient.Send(message);
                return;
            }
            catch (SmtpException)
            {
            }

            try
            {
                var smtpClient = ServiceLocator.Instance.CreateEmailSender(AppSettings.SMTP_SERVER);
                smtpClient.Send(message);
                return;
            }
            catch (SmtpException) when (backupEmailList.Any())
            {
                string failedEmail = message.To.ToString();
                IEnumerable<string> remainingEmails = backupEmailList.Where(email => !string.IsNullOrEmpty(email) && !string.Equals(failedEmail, email, StringComparison.OrdinalIgnoreCase));
                string nextEmail = remainingEmails.FirstOrDefault();
                if (nextEmail == null)
                {
                    throw;
                }

                message.To.Clear();
                message.To.Add(nextEmail);
                if (!string.IsNullOrEmpty(failedEmail))
                {
                    string failureMessage = "You are receiving this notification because the email to " + failedEmail + " failed.";
                    if (message.IsBodyHtml)
                    {
                        failureMessage = "<p>" + failureMessage + "</p>";
                    }

                    message.Body += Environment.NewLine + failureMessage;
                }

                TransmitEmail(message, remainingEmails.Skip(1));
            }
        }

        public static void SettingsErrorMessage(string customerCode, E_RequestType requestType)
        {
            string messageBody = customerCode + " failed to load completely.  Please verify settings." + Environment.NewLine
                + Environment.NewLine
                + "Error log for application settings:" + Environment.NewLine
                + SettingsErrorLog.RetrieveAndClear();
            SendMessageToSupport("Settings contained errors", messageBody, customerCode, requestType);
        }
    }
}
