﻿using System;

namespace UndisclosedDebtNotification
{
    public class NotificationAttribute
    {
        public string Name { get; private set; }

        public string RawValue { get; private set; }

        /// <summary>
        /// The value of the attribute, formatted for display purposes in LQB comments or other situations where a user might see it.
        /// </summary>
        /// <example>
        /// For the "source_bureau" attribute, when the constructor is given <see cref="NotificationMapping.ToCreditBureau(string)"/> as <see cref="DisplayMethod"/>, 
        /// this will convert the <see cref="RawValue"/> from "XP", "EF", "TU", to "Experian", "Equifax", "TransUnion", respectively.
        /// </example>
        public string DisplayValue
        {
            get
            {
                return DisplayMethod(RawValue);
            }
        }

        /// <summary>
        /// Used to map the <see cref="RawValue"/> to the <see cref="DisplayValue"/>
        /// </summary>
        private Func<string, string> DisplayMethod { get; set; }

        /// <summary>
        /// Provides a 2-parameter constructor which makes the display method simply return the raw value. <see cref="RawValue"/> and <see cref="DisplayValue"/> will be the same.
        /// </summary>
        /// <param name="name">Name for the attribute.</param>
        /// <param name="rawValue">Raw and display value for the attribute.</param>
        public NotificationAttribute(string name, string rawValue) : this(name, rawValue, value => value)
        {
        }

        /// <summary>
        /// Creates an instance of the <see cref="NotificationAttribute"/> class.
        /// </summary>
        /// <param name="name">The name of the attribute.</param>
        /// <param name="rawValue">The "raw" value of the attribute, as read from the input xml.</param>
        /// <param name="displayFunc">The function used to convert this attribute's <see cref="RawValue"/> into its <see cref="DisplayValue"/>.</param>
        public NotificationAttribute(string name, string rawValue, Func<string,string> displayFunc)
        {
            this.Name = name;
            this.RawValue = rawValue;
            this.DisplayMethod = displayFunc;
        }

        /// <summary>
        /// Determines if this attribute is equal to another by comparing their names and display values.
        /// </summary>
        /// <param name="obj">The other <see cref="NotificationAttribute"/> to compare for equality.</param>
        /// <returns>Whether or not the two attributes are equal.</returns>
        public override bool Equals(object obj)
        {
            NotificationAttribute otherAttribute = obj as NotificationAttribute;
            if (otherAttribute == null || this.Name != otherAttribute.Name || this.DisplayValue != otherAttribute.DisplayValue)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Formats the attribute as an attribute string.
        /// </summary>
        /// <returns>The attribute formatted with <see cref="NotificationFormatting.AttributeString(string, string)"/></returns>
        public override string ToString()
        {
            return NotificationFormatting.AttributeString(this.Name, this.DisplayValue);
        }

        /// <summary>
        /// Implemented so that this object can be safely added to a dictionary.
        /// </summary>
        /// <returns>Concatenates the <see cref="Name"/> and <see cref="RawValue"/> and returns the hashcode of the resulting string.</returns>
        public override int GetHashCode()
        {
            return (Name + RawValue).GetHashCode();
        }
    }
}