﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UndisclosedDebtNotification.Get
{
    /// <summary>
    /// Represents the Lender Settings for Get requests.
    /// </summary>
    public class LenderGetSettings : LenderSettings
    {
        public string ConditionCategory { get; private set; }
        public string TaskPermissionLevel { get; private set; }
        public string Doctype { get; private set; }
        public bool ConditionIsHidden { get; private set; }
        public string AssignedToRole { get; private set; }
        public string OwnedByRole { get; private set; }

        public LenderGetSettings(string sCustomerCode) : base(sCustomerCode)
        {
            ConditionCategory = AppSettings.LENDER_CONDITION_CATEGORY(sCustomerCode);
            TaskPermissionLevel = AppSettings.LENDER_TASK_PERMISSION_LEVEL(sCustomerCode);
            Doctype = AppSettings.LENDER_DOCTYPE(sCustomerCode);
            ConditionIsHidden = AppSettings.ConditionIsHidden(sCustomerCode);
            AssignedToRole = AppSettings.AssignedToRole(sCustomerCode);
            OwnedByRole = AppSettings.OwnedByRole(sCustomerCode);
        }

        public override bool IsComplete()
        {
            return base.IsComplete()
                && null != ConditionCategory
                ////&& null != TaskPermissionLevel // We'll let this default to the default in LendingQB if user fails to input a valid value
                && null != Doctype;
        }
    }
}
