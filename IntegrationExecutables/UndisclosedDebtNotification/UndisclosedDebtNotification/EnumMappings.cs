﻿namespace UndisclosedDebtNotification
{
    public static class EnumMappings
    {
        public static readonly string[] E_RequestType =
        {
            "",
            "ACTIVATE",
            "DEACTIVATE",
            "GET",
            "UPDATE",
            "NEW"
        };

        public static readonly string[] E_Borrower = 
        {
            "",
            "APPLICANT",
            "SPOUSE",
            "JOINT"
        };

        public static readonly string[] E_NameSuffix = 
        {
            "",
            "SR",
            "JR",
            "I",
            "II",
            "III",
            "IV",
            "V",
            "VI",
            "VII",
            "VIII",
            "IX"
        };
    }
}
