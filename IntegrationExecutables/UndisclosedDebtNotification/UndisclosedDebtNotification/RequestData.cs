﻿namespace UndisclosedDebtNotification
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;

    /// <summary>
    /// Represents a request for UDN.
    /// </summary>
    public abstract class RequestData
    {
        /// <summary>
        /// Provides a means for matching social security numbers.
        /// </summary>
        protected readonly Regex SsnRegex = new Regex(@"\d{3}-\d{2}-\d{4}", RegexOptions.Compiled);

        /// <summary>
        /// Initializes a new instance of the <see cref="RequestData"/> class.
        /// </summary>
        /// <param name="dataDictionary">
        /// The dictionary containing the data required to populate the request.
        /// </param>
        /// <param name="application">
        /// The application for the request.
        /// </param>
        /// <param name="errorEmail">
        /// The email address used when the encounters an error.
        /// </param>
        /// <param name="backupEmail">The backup email address.</param>
        /// <param name="requestType">
        /// The <see cref="E_RequestType"/> of the request.
        /// </param>
        protected RequestData(Dictionary<string, string> dataDictionary, LoanNmAppId application, string errorEmail, string backupEmail, E_RequestType requestType)
        {
            this.Application = application;
            this.ErrorNotificationEmail = errorEmail;
            this.BackupErrorNotificationEmail = backupEmail;
            this.RequestType = requestType;

            this.PopulateApplicantData(dataDictionary);
        }

        /// <summary>
        /// Gets the value for the application of the request.
        /// </summary>
        /// <value>
        /// The <see cref="LoanNmAppId"/> representing the application
        /// for the request.
        /// </value>
        public LoanNmAppId Application { get; private set; }

        /// <summary>
        /// Gets the value for the type of the request.
        /// </summary>
        /// <value>
        /// The <see cref="E_RequestType"/> for the request.
        /// </value>
        public E_RequestType RequestType { get; private set; }

        /// <summary>
        /// Gets the value for the <see cref="RequestType"/> in
        /// string format.
        /// </summary>
        /// <value>
        /// The <see cref="RequestType"/> in <see cref="string"/>
        /// format.
        /// </value>
        public string RequestType_rep
        {
            get { return EnumMappings.E_RequestType[(int)this.RequestType]; }
        }

        public static RequestData Create(E_RequestType requestType, Dictionary<string, string> fieldDictionary, LoanNmAppId candidate, string notificationEmail, string errorBackupEmail)
        {
            if (requestType == E_RequestType.NEW)
            {
                return new NewRequestData(fieldDictionary, candidate, notificationEmail, errorBackupEmail);
            }

            return new GetDeactivateRequestData(fieldDictionary, candidate, notificationEmail, errorBackupEmail, requestType);
        }

        /// <summary>
        /// Gets or sets the value for the type of borrower for
        /// the request.
        /// </summary>
        /// <value>
        /// The <see cref="E_Borrower"/> for the request.
        /// </value>
        public E_Borrower Borrower { get; set; }

        /// <summary>
        /// Gets the value for the <see cref="Borrower"/> in
        /// string format.
        /// </summary>
        /// <value>
        /// The <see cref="Borrower"/> type in <see cref="string"/>
        /// format.
        /// </value>
        public string Borrower_rep
        {
            get { return EnumMappings.E_Borrower[(int)this.Borrower]; }
        }

        /// <summary>
        /// Gets or sets the value for the applicant for the request.
        /// </summary>
        /// <value>
        /// The <see cref="Borrower"/> applicant for the
        /// request.
        /// </value>
        public Borrower Applicant { get; protected set; }

        /// <summary>
        /// Gets or sets the value for the spouse for the request.
        /// </summary>
        /// <value>
        /// The <see cref="Borrower"/> spouse for the 
        /// request.
        /// </value>
        public Borrower Spouse { get; protected set; }

        /// <summary>
        /// Gets or sets the value for the social security number that
        /// identifies the applicant.
        /// </summary>
        /// <value>
        /// The <see cref="string"/> social security number that
        /// identifies the request.
        /// </value>
        public string AppIdentifyingSsn { get; protected set; }

        /// <summary>
        /// Gets the value for the email address that will be
        /// notified when a request encounters an error.
        /// </summary>
        /// <value>
        /// The <see cref="string"/> notification email address.
        /// </value>
        public string ErrorNotificationEmail { get; private set; }

        /// <summary>
        /// Gets the backup email address to <see cref="ErrorNotificationEmail"/>.
        /// </summary>
        /// <value>
        /// The backup email address for error notifications.
        /// </value>
        public string BackupErrorNotificationEmail { get; private set; }

        /// <summary>
        /// Obtains a list of dependent loan fields based on the <see cref="requestType"/>.
        /// </summary>
        /// <param name="requestType">
        /// The <see cref="E_RequestType"/> of the request.
        /// </param>
        /// <returns>
        /// A <see cref="List{T}"/> of dependent loan fields.
        /// </returns>
        public static List<string> GetDependentLoanFields(E_RequestType requestType)
        {
            var fields = new List<string>();

            if (requestType == E_RequestType.NEW)
            {
                fields.AddRange(new[]
                {
                    "sSpAddr",
                    "sSpCity",
                    "sSpState",
                    "sSpZip"
                });
            }

            return fields;
        }

        /// <summary>
        /// Obtains a list of dependent application fields based on the <see cref="requestType"/>.
        /// </summary>
        /// <param name="requestType">
        /// The <see cref="E_RequestType"/> of the request.
        /// </param>
        /// <returns>
        /// A <see cref="List{T}"/> of application loan fields.
        /// </returns>
        public static List<string> GetDependentAppFields(E_RequestType requestType)
        {
            var fields = new List<string>()
            {
                "aAppId",
                "aBFirstNm", "aBLastNm", "aBMidNm", "aBSuffix", "aBSsn", "aBDob", "aBTypeT",
                "aCFirstNm", "aCLastNm", "aCMidNm", "aCSuffix", "aCSsn", "aCDob", "aCTypeT",
            };

            if (requestType == E_RequestType.NEW)
            {
                fields.AddRange(new[]
                {
                    "aCrOd",
                    "aBAddr", "aBCity", "aBState", "aBZip", "aBAddrYrs",
                    "aCAddr", "aCCity", "aCState", "aCZip", "aCAddrYrs",
                });
            }
            else
            {
                fields.AddRange(new[]
                {
                    "aUdnOrderId",
                    "MCLCreditDataForUDN"
                });
            }

            return fields;
        }

        /// <summary>
        /// Determines whether this request is valid, namely that
        /// a valid request type, borrower type, applicant, and reference
        /// number are on file.
        /// </summary>
        /// <returns>
        /// True if the request is valid, false otherwise.
        /// </returns>
        public virtual bool IsValid()
        {
            return this.RequestType != E_RequestType.Undefined 
                && this.Borrower != E_Borrower.Undefined 
                && this.Applicant != null && this.Applicant.IsValid() 
                && (this.Spouse == null || this.Spouse.IsValid());
        }

        /// <summary>
        /// Populates applicant and spouse data from the supplied
        /// <paramref name="dataDictionary"/>.
        /// </summary>
        /// <param name="dataDictionary">
        /// The dictionary containing borrower and/or co-borrower data
        /// used to populate the request.
        /// </param>
        protected virtual void PopulateApplicantData(Dictionary<string, string> dataDictionary)
        {
            this.Borrower = E_Borrower.APPLICANT;

            var aBTypeT = this.SafeParseApplicantType(Tools.GetStringValueFromDictionary(dataDictionary, "aBTypeT"));
            var aBFirstNm = Tools.GetStringValueFromDictionary(dataDictionary, "aBFirstNm");
            var aBLastNm = Tools.GetStringValueFromDictionary(dataDictionary, "aBLastNm");
            var aBSsn = Tools.GetStringValueFromDictionary(dataDictionary, "aBSsn");
            this.AppIdentifyingSsn = aBSsn;

            var aCTypeT = this.SafeParseApplicantType(Tools.GetStringValueFromDictionary(dataDictionary, "aCTypeT"));
            var aCFirstNm = Tools.GetStringValueFromDictionary(dataDictionary, "aCFirstNm");
            var aCLastNm = Tools.GetStringValueFromDictionary(dataDictionary, "aCLastNm");
            var aCSsn = Tools.GetStringValueFromDictionary(dataDictionary, "aCSsn");
            
            var borrowerDataSpecified = this.AllNonEmpty(aBFirstNm, aBLastNm) && this.SsnRegex.IsMatch(aBSsn);
            var coborrowerDataSpecified = this.AllNonEmpty(aCFirstNm, aCLastNm) && this.SsnRegex.IsMatch(aCSsn);

            if (borrowerDataSpecified && this.IsValidApplicantType(aBTypeT))
            {
                this.Applicant = new Borrower(dataDictionary, 'B');

                if (coborrowerDataSpecified && this.IsValidApplicantType(aCTypeT))
                {
                    this.Borrower = E_Borrower.JOINT;
                    this.Spouse = new Borrower(dataDictionary, 'C');
                }
            }
            else if (coborrowerDataSpecified && this.IsValidApplicantType(aCTypeT))
            {
                this.Applicant = new Borrower(dataDictionary, 'C');
            }
            else
            {
                this.HandleApplicantError(
                    borrowerDataSpecified,
                    coborrowerDataSpecified,
                    aBTypeT,
                    aCTypeT);
            }
        }

        /// <summary>
        /// Determines whether the specified <paramref name="fields"/>
        /// are not null or the empty string.
        /// </summary>
        /// <param name="fields">
        /// The <see cref="string"/> name fields to verify.
        /// </param>
        /// <returns>
        /// True if the name fields are not null or empty, false otherwise.
        /// </returns>
        private bool AllNonEmpty(params string[] fields)
        {
            return fields.All(field => !string.IsNullOrEmpty(field));
        }

        /// <summary>
        /// Parses the specified <paramref name="applicantType"/> into a 
        /// <see cref="LQBFields.ApplicantType"/> constant.
        /// </summary>
        /// <param name="applicantType">
        /// The <see cref="string"/> representing the applicant type.
        /// </param>
        /// <returns>
        /// The parsed <see cref="LQBFields.ApplicantType"/> or 
        /// <see cref="LQBFields.ApplicantType.Undefined"/> if the parse 
        /// was unsuccessful.
        /// </returns>
        private LQBFields.ApplicantType SafeParseApplicantType(string applicantType)
        {
            try
            {
                return (LQBFields.ApplicantType)Enum.Parse(typeof(LQBFields.ApplicantType), applicantType);
            }
            catch (ArgumentException)
            {
                return LQBFields.ApplicantType.Undefined;
            }
        }

        /// <summary>
        /// Determines whether the specified <paramref name="applicantType"/> is valid.
        /// </summary>
        /// <param name="applicantType">
        /// The <see cref="LQBFields.ApplicantType"/> to check.
        /// </param>
        /// <returns>
        /// True if <paramref name="applicantType"/> is <see cref="LQBFields.ApplicantType.Individual"/> 
        /// or <see cref="LQBFields.ApplicantType.CoSigner"/>, false otherwise.
        /// </returns>
        private bool IsValidApplicantType(LQBFields.ApplicantType applicantType)
        {
            return applicantType == LQBFields.ApplicantType.Individual || applicantType == LQBFields.ApplicantType.CoSigner;
        }

        /// <summary>
        /// Handles an applicant parsing error in the event
        /// that the borrower's or co-borrower's type was invalid
        /// but the data supplied for first name, last name, etc.
        /// was valid.
        /// </summary>
        /// <param name="borrowerDataSpecified">
        /// Whether the borrower's data was specified.
        /// </param>
        /// <param name="coborrowerDataSpecified">
        /// Whether the co-borrower's data was specified.
        /// </param>
        /// <param name="aBTypeT">
        /// The <see cref="LQBFields.ApplicantType"/> of the borrower.
        /// </param>
        /// <param name="aCTypeT">
        /// The <see cref="LQBFields.ApplicantType"/> of the co-borrower.
        /// </param>
        private void HandleApplicantError(
            bool borrowerDataSpecified,
            bool coborrowerDataSpecified,
            LQBFields.ApplicantType aBTypeT,
            LQBFields.ApplicantType aCTypeT)
        {
            if (!borrowerDataSpecified && !coborrowerDataSpecified)
            {
                var clientErrorMessage = string.Format(
                    "The UDN service could not be {0} for an application on loan {1} because the borrower's first name, " +
                    "last name, and social security number are required. Please verify the information for all borrowers on file.",
                    Tools.GetRequestAction(this.RequestType),
                    this.Application.LoanNm);

                SendEmail.SendErrorMessageToClient(this, new[] { clientErrorMessage }, this.RequestType, "failed");

                throw new InvalidOperationException(string.Format(
                    "No valid borrower or coborrower was specified in the loan for the {0} request.",
                    Tools.GetRequestName(this.RequestType)));
            }

            var hasBorrowerWithInvalidType = borrowerDataSpecified && !this.IsValidApplicantType(aBTypeT);
            var hasCoborrowerWithInvalidType = coborrowerDataSpecified && !this.IsValidApplicantType(aCTypeT);

            var errorMessage = string.Format(
                "Application does not have any borrowers with 'Individual' or 'CoSigner' types for the {0} request. Found {1}{2}{3}.",
                Tools.GetRequestName(this.RequestType),
                hasBorrowerWithInvalidType ? "borrower with type " + aBTypeT : string.Empty,
                hasBorrowerWithInvalidType && hasCoborrowerWithInvalidType ? " and " : string.Empty,
                hasCoborrowerWithInvalidType ? "coborrower with type " + aCTypeT : string.Empty);

            throw new NoValidApplicantTypesException(errorMessage);
        }
    }
}
