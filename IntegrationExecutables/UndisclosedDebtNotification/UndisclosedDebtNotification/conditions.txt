//TRADELINES/TRADELINE: "UDN Tradeline: <creditor_info> <acct_type> <balance>"

//DELINQUENCIES/DELINQUENCY: "UDN Delinquency: <creditor_info> <acct_type> <status>"

//INQUIRIES/INQUIRY: "UDN Inquiry: <company> <inquiry_date>"

//PUBLIC_RECORDS/PUBLIC_RECORD: "UDN Public Record: <record_type> <file_date>"

//MAJOR_DEROGATORIES/MAJOR_DEROGATORY: "UDN Major Derog: <creditor_info> <status> <balance>"

//TRADELINE_BANKRUPTCIES/TRADELINE_BANKRUPTCY: "UDN BK: <creditor_info> <type>"

//HIGH_UTILIZATIONS/HIGH_UTILIZATION: "UDN High Util: <creditor_info> <type> <balance>"

//FROZEN_FILES/FROZEN_FILE: "UDN Frozen File: <source_bureau> <notification_date>"

//SCHEDULED_PAYMENTS/SCHEDULED_PAYMENT: "UDN Scheduled Payment: <creditor_info> <terms> <status>"

//BALANCE_CHANGES/BALANCE_CHANGE: "UDN Balance Change: <creditor_info> <acct_type> <balance>"
