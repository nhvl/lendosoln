﻿namespace UndisclosedDebtNotification
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public static class Program
    {
        private static E_RequestType ParseRequestTypeForCurrentRun(string[] args)
        {
            E_RequestType parsedRequestType = (E_RequestType)Enum.Parse(typeof(E_RequestType), args[0], true);
            switch (parsedRequestType)
            {
                case E_RequestType.ACTIVATE:
                    return E_RequestType.NEW;
                case E_RequestType.NEW:
                case E_RequestType.GET:
                    return parsedRequestType;
                default:
                    throw new InvalidOperationException("Invalid Request Type for this run.  Input value was [" + args[0] + "]");
            }
        }

        public static void Main(string[] args)
        {
            E_RequestType requestType = E_RequestType.Undefined;
            try
            {
                requestType = ParseRequestTypeForCurrentRun(args);
                Tools.SetupServicePointCallback();
                MclRequest.LookupCompanyUrls(ServiceLocator.Instance.CreateDB(AppSettings.DSN));

                foreach (string sCustomerCode in AppSettings.LENDERS)
                {
                    Lender lender = null;
                    try
                    {
                        var lenderDatabase = ServiceLocator.Instance.CreateDB(AppSettings.LENDER_DSN(sCustomerCode));
                        lender = new Lender(sCustomerCode, requestType, lenderDatabase);
                        if (!lender.IsComplete())
                        {
                            SendEmail.SettingsErrorMessage(sCustomerCode, requestType);
                            continue;
                        }

                        ProcessLoansByBranchChannel(lender, requestType, lenderDatabase);

                        Logger.LogInfo(lender.GeneralLog, sCustomerCode);
                        SendEmail.SendErrorLogsToSupport(lender, requestType);
                    }
                    catch (Exception exc)
                    {
                        SendEmail.SendErrorMessageToSupport(exc, lender, sCustomerCode, requestType);
                    }
                }
            }
            catch (Exception exc)
            {
                SendEmail.SendErrorMessageToSupport(exc, requestType);
            }
        }

        private static void ProcessLoansByBranchChannel(Lender lender, E_RequestType requestType, DB lenderDatabase)
        {
            foreach (var branchID in lender.BranchIDs)
            {
                var craComId = lender.Settings.GetCraComId(branchID);

                foreach (var channel in lender.Settings.Channels)
                {
                    lender.LogBranchAndChannel(branchID, channel);

                    List<LoanNmAppId> appCandidates = lenderDatabase.GetAppCandidates(lender, branchID, channel, requestType);
                    ProcessAppCandidates(appCandidates, lender, craComId, channel, requestType);

                    if (requestType == E_RequestType.NEW)
                    {
                        requestType = E_RequestType.DEACTIVATE;
                        appCandidates = lenderDatabase.GetAppCandidates(lender, branchID, channel, requestType);
                        ProcessAppCandidates(appCandidates, lender, craComId, channel, requestType);
                        requestType = E_RequestType.NEW;
                    }
                }
            }
        }

        private static void ProcessAppCandidates(List<LoanNmAppId> appCandidates, Lender lender, Guid craComId, int channel, E_RequestType requestType)
        {
            foreach (var candidateApp in appCandidates)
            {
                try
                {
                    RequestData requestData = lender.LoanService.Load(candidateApp, lender.Settings.ErrorRole(channel), lender.Settings.ErrorRoleEmailBackup(channel), requestType);

                    if (!requestData.IsValid())
                    {
                        lender.LogReasonForSkip(candidateApp, "The data was not valid");
                        continue;
                    }

                    var effectiveCraComId = craComId;
                    if (requestData is GetDeactivateRequestData && ((GetDeactivateRequestData)requestData).MclComId.HasValue)
                    {
                        effectiveCraComId = ((GetDeactivateRequestData)requestData).MclComId.Value;
                    }

                    List<ResponseData> responses = MclRequest.SendRequest(lender.Settings.MCL, effectiveCraComId, requestData, requestType);

                    if (responses.TrueForAll(r => r.Success))
                    {
                        if (ImportResultToLQB(lender, requestData, responses, requestType))
                            lender.LogSuccess(candidateApp, requestType);
                        else
                            lender.LogReasonForSkip(candidateApp, "Save into LQB unsuccessful - see logs");
                    }
                    else
                    {
                        IEnumerable<string> errorMessages = responses.Where(r => !r.Success).Select(r => r.ErrorMessage);
                        SendEmail.SendErrorMessageToClient(requestData, errorMessages, requestType, "failed");
                        lender.LogUDNErrorMessage(candidateApp, errorMessages);
                    }
                }
                catch (NoValidApplicantTypesException exc)
                {
                    // An application that has only non-title spouses or title-only 
                    // borrowers is a valid loan scenario that should not have UDN
                    // monitoring.
                    lender.LogReasonForSkip(candidateApp, exc.Message);
                }
                catch (Exception exc)
                {
                    lender.LogErrorAsReasonForSkip(candidateApp, exc);
                }
            }
        }

        public static bool ImportResultToLQB(Lender lender, RequestData requestData, IEnumerable<ResponseData> responses, E_RequestType requestType)
        {
            try
            {
                bool success = true;
                if (requestType == E_RequestType.NEW || requestType == E_RequestType.DEACTIVATE)
                {
                    // NEW and DEACTIVATE requests will have only one response.
                    success = lender.LoanService.ImportNewDeactivateResponse(requestData, requestType, responses.First());
                }
                else if (requestType == E_RequestType.GET)
                {
                    foreach (var responseData in responses)
                    {
                        success &= lender.LoanService.ImportGetResponse(requestData, responseData, (Get.LenderGetSettings)lender.Settings);
                    }

                    IEnumerable<string> errorMessagesFromCreditBureaus = responses.Where(r => null != r.ErrorMessagesFromCreditBureaus)
                        .SelectMany(r => r.ErrorMessagesFromCreditBureaus)
                        .Distinct();

                    if (errorMessagesFromCreditBureaus.Any())
                    {
                        SendEmail.SendErrorMessageToClient(requestData, errorMessagesFromCreditBureaus, requestType, "returned an error");
                    }
                }

                return success;
            }
            catch (ImportGetDataFailureException exc)
            {
                SendEmail.SendErrorMessageToClient(requestData, new[] { exc.ClientMessage }, requestType, "failed to save");
                return false;
            }
        }
    }
}
