﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace UndisclosedDebtNotification
{
    class Logger
    {
        public static void LogInfo(string message, string customerCode)
        {
            Trace.Write("UndisclosedDebtNotification - " + customerCode + ":" + Environment.NewLine + message, "UndisclosedDebtNotification");
        }
    }
}
