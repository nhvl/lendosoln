﻿namespace UndisclosedDebtNotification.com.lendingqb.Auth
{
    /// <summary>
    /// An interface to represent the available web service calls to AuthService.asmx.
    /// </summary>
    /// <remarks>
    /// Generated via visual studio initially, this interface is a minimal addition to Reference.cs that
    /// will allow us to inject tests into the code.
    /// </remarks>
    public interface IAuthService
    {
        string Url { get; set; }

        string GetUserAuthTicket(string userName, string passWord);

        System.Security.Cryptography.X509Certificates.X509CertificateCollection ClientCertificates
        {
            get;
        }
    }

    /// <summary>
    /// A partial to tie this interface to the subset of calls we want to allow.
    /// </summary>
    public partial class AuthService : IAuthService
    {
        // implementation in Reference.cs, but this avoids updating the generated file.
    }
}
