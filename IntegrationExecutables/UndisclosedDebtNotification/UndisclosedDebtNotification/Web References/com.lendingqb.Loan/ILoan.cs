﻿namespace UndisclosedDebtNotification.com.lendingqb.Loan
{
    using System;

    /// <summary>
    /// An interface to represent the available web service calls to Loan.asmx.
    /// </summary>
    /// <remarks>
    /// Generated via visual studio initially, this interface is a minimal addition to Reference.cs that
    /// will allow us to inject tests into the code.
    /// </remarks>
    public interface ILoan
    {
        string Url { get; set; }
        string Load(string sTicket, string sLNm, string sXmlQuery, int format);
        string Save(string sTicket, string sLNm, string sDataContent, int format);
        string UploadPDFDocumentToApp(string sTicket, string sLNm, Guid aAppId, string documentType, string notes, string sDataContent);
    }

    /// <summary>
    /// A partial to tie this interface to the subset of calls we want to allow.
    /// </summary>
    public partial class Loan : ILoan
    {
        // implementation in Reference.cs, but this avoids updating the generated file.
    }
}
