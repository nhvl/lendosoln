﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using UndisclosedDebtNotification.com.lendingqb.Auth;
using UndisclosedDebtNotification.com.lendingqb.Loan;

namespace UndisclosedDebtNotification
{
    public class LoanWebService
    {
        private string m_authTicket = null;
        private LoanWebServiceSettings.Environment m_environment;
        private ILoan m_loan = null;
        private ILoan m_edocsLoan = null;
        private StringBuilder m_lenderErrorLog = null;

        public LoanWebService(Credentials credentials, X509Certificate2 certificate, LoanWebServiceSettings.Environment environment, StringBuilder lenderErrorLog)
        {
            if (credentials == null) return; // Prevent NullReference; this case will get caught by the isComplete check.

            m_environment = environment;
            IAuthService authService = ServiceLocator.Instance.CreateAuthService(LoanWebServiceSettings.AuthURL(environment));
            if (certificate != null)
            {
                authService.ClientCertificates.Add(certificate);
            }

            m_authTicket = authService.GetUserAuthTicket(credentials.Username, credentials.Password);
            m_loan = ServiceLocator.Instance.CreateLoan(LoanWebServiceSettings.LoanURL(environment));
            m_edocsLoan = ServiceLocator.Instance.CreateLoan(LoanWebServiceSettings.EdocsURL(environment));
            m_lenderErrorLog = lenderErrorLog;
        }

        public bool IsComplete()
        {
            return null != m_authTicket
                && null != m_loan
                && null != m_edocsLoan
                && null != m_lenderErrorLog;
        }

        private static readonly List<string> EmptyList = new List<string>(0);

        public RequestData Load(LoanNmAppId candidate, string errorRole, string errorBackupEmail, E_RequestType requestType)
        {
            var loanFields = RequestData.GetDependentLoanFields(requestType);
            var notifiedEmployeeRoleEmailField = string.IsNullOrEmpty(errorRole) ? string.Empty : "sEmployee" + errorRole + "Email";
            loanFields.Add(notifiedEmployeeRoleEmailField);

            string sXmlQuery = LOXmlFormat.WriteLoadRequest(
                loanFields, 
                RequestData.GetDependentAppFields(requestType), 
                EmptyList);
            string sLOXmlResponse = m_loan.Load(m_authTicket, candidate.LoanNm, sXmlQuery, 0);

            var fieldDictionary = LOXmlFormat.ReadLoadResponse(sLOXmlResponse, candidate.AppId);

            var notificationEmail = Tools.GetStringValueFromDictionary(fieldDictionary, notifiedEmployeeRoleEmailField, errorBackupEmail);
            return RequestData.Create(requestType, fieldDictionary, candidate, notificationEmail, errorBackupEmail);
        }

        public bool ImportNewDeactivateResponse(RequestData requestData, E_RequestType requestType, ResponseData responseData)
        {
            var applications = DefineUDNSaveDate(requestData.AppIdentifyingSsn, requestType, DateTime.Now);

            var typeOfSave = GetDateFieldToUpdate(requestType);

            if (requestType == E_RequestType.NEW)
            {
                if (string.IsNullOrEmpty(responseData.ReportId))
                {
                    m_lenderErrorLog.AppendLine("Could not save result from NEW request: no report ID was returned.");
                    return false;
                }

                applications[requestData.AppIdentifyingSsn].Add("aUdnOrderId", responseData.ReportId);

                typeOfSave += "/aUdnOrderId";
            }

            string requestXml = LOXmlFormat.WriteSaveRequest(null, null, applications);
            return Save(requestData.Application.LoanNm, requestXml, typeOfSave);
        }

        public bool ImportGetResponse(RequestData request, ResponseData response, Get.LenderGetSettings lenderGetSettings)
        {
            bool importedConditions = false;
            bool success = true;
            Dictionary<string, string> loanFields = null;
            Dictionary<string, List<Dictionary<string, string>>> collectionsOfRecords = null;
            Dictionary<string, Dictionary<string, string>> applications = null;

            List<NotificationDataItem> notificationsToImportToLQB = FilterNotificationsToImportIntoLQB(response.NotificationData, request.Application.LoanNm);
            if (notificationsToImportToLQB.Count > 0)
            {
                List<Dictionary<string, string>> records = new List<Dictionary<string,string>>(notificationsToImportToLQB.Count);
                string today = DateTime.Today.ToShortDateString();
                foreach (NotificationDataItem notification in notificationsToImportToLQB)
                {
                    records.Add(new Dictionary<string, string>
                    {
                        {"Category", lenderGetSettings.ConditionCategory},
                        {"OwnedBy", lenderGetSettings.OwnedByRole},
                        {"AssignedTo", lenderGetSettings.AssignedToRole},
                        {"CondDesc", notification.Description},
                        {"DueDate", today},
                        {"Comments", notification.ToString()},
                        {"TaskPermissionLevelId", lenderGetSettings.TaskPermissionLevel},
                        {"CondIsHidden", lenderGetSettings.ConditionIsHidden.ToString()}
                    });
                }

                collectionsOfRecords = new Dictionary<string, List<Dictionary<string, string>>> { { "sCondDataSet", records } };
                importedConditions = true;
            }

            if (response.DeactivationDate.HasValue)
            {
                applications = DefineUDNSaveDate(request.AppIdentifyingSsn, E_RequestType.DEACTIVATE, response.DeactivationDate.Value);
            }

            if (loanFields != null || collectionsOfRecords != null || applications != null)
            {
                string requestXml = LOXmlFormat.WriteSaveRequest(loanFields, collectionsOfRecords, applications);
                success &= SaveNotifications(request.Application.LoanNm, requestXml);
            }

            if (importedConditions && success && !string.IsNullOrEmpty(response.EmbeddedBase64Document))
            {
                string resultXml = this.UploadPDFDocumentToApp(
                    m_authTicket,
                    request.Application.LoanNm,
                    request.Application.AppId,
                    lenderGetSettings.Doctype,
                    "Uploaded from UDN service " + DateTime.Now.ToString(),
                    response.EmbeddedBase64Document);
                string errorMessage;
                success &= LOXmlFormat.ReadSaveResponse(resultXml, out errorMessage);
                LogSaveError("Embedded Base64 document from Notifications", request.Application.LoanNm, null, errorMessage);
            }

            return success;
        }

        /// <summary>
        /// Handles condition de-duplication by calculating the notifications that do not already have matching conditions in LQB.
        /// Uses Condition Description (Task Subject) to determine if match.
        /// </summary>
        /// <param name="notificationsFromMcl">The list of notifications from MCL, which will become the condition description.</param>
        /// <param name="sLNm">The Loan Number to check conditions.</param>
        /// <returns>An array of notification indexes that are not already in LendingQB.</returns>
        private List<NotificationDataItem> FilterNotificationsToImportIntoLQB(List<NotificationDataItem> notificationsFromMcl, string sLNm)
        {
            if (notificationsFromMcl == null || notificationsFromMcl.Count == 0)
                return new List<NotificationDataItem>(0);

            string xmlQuery = LOXmlFormat.WriteLoadRequest(EmptyList, EmptyList, new List<string> { "sCondDataSet_all" });
            string sLOXmlResponse = m_loan.Load(m_authTicket, sLNm, xmlQuery, 0);

            IEnumerable<string> lqbFirstComments = LOXmlFormat.ReadLoadConditionsResponse(sLOXmlResponse);

            List<NotificationDataItem> filteredNotifications = notificationsFromMcl.Where(notification => !notification.MatchesComment(lqbFirstComments)).ToList();
            return filteredNotifications;
        }

        private bool Save(string sLNm, string requestXml, string typeOfSave)
        {
            string resultXml = m_loan.Save(m_authTicket, sLNm, requestXml, 0);
            string errorMessage;
            bool success = LOXmlFormat.ReadSaveResponse(resultXml, out errorMessage);
            return !LogSaveError(typeOfSave, sLNm, requestXml, errorMessage) && success;
        }

        private static Dictionary<string, Dictionary<string, string>> DefineUDNSaveDate(string appIdentifyingSsn, E_RequestType requestType, DateTime date)
        {
            string fieldName = GetDateFieldToUpdate(requestType);
            return new Dictionary<string, Dictionary<string, string>>
            {
                { appIdentifyingSsn, new Dictionary<string, string> { {fieldName, date.ToString()} } }
            };
        }

        private static string GetDateFieldToUpdate(E_RequestType requestType)
        {
            switch (requestType)
            {
                case E_RequestType.NEW: return "aUDNOrderedD";
                case E_RequestType.DEACTIVATE: return "aUDNDeactivatedD";
                default:
                    throw Tools.UnhandledEnumException(requestType);
            }
        }

        /// <summary>
        /// Returns false if there is no errorMessage to log; true if an error has been logged.
        /// </summary>
        private bool LogSaveError(string typeOfSave, string sLNm, string requestXml, string errorMessage)
        {
            if (null == errorMessage)
                return false;

            m_lenderErrorLog.AppendLine("Warnings/Errors while saving " + typeOfSave + " to loan file \"" + sLNm + "\":"
                + Environment.NewLine + "=== REQUEST XML ===" + Environment.NewLine + requestXml + Environment.NewLine
                + Environment.NewLine + "=== RESPONSE ===" + Environment.NewLine + errorMessage + Environment.NewLine);

            return true;
        }

        private bool SaveNotifications(string sLNm, string requestXml, int tryCount = 0)
        {
            const int tryLimit = 2;
            try
            {
                tryCount += 1;
                return this.Save(sLNm, requestXml, "Notifications");
            }
            catch when (tryCount < tryLimit)
            {
                return this.SaveNotifications(sLNm, requestXml, tryCount);
            }
            catch (Exception exc)
            {
                throw new ImportGetDataFailureException("Failed to upload one or more conditions", exc);
            }
        }

        /// <summary>
        /// Wraps the method on <paramref name="edocsLoan"/> to allow retries.
        /// </summary>
        private string UploadPDFDocumentToApp(string sTicket, string sLNm, Guid aAppId, string documentType, string notes, string sDataContent, int tryCount = 0)
        {
            const int tryLimit = 2;
            string initialUrl = this.m_edocsLoan.Url;
            try
            {
                tryCount += 1;
                return this.m_edocsLoan.UploadPDFDocumentToApp(sTicket, sLNm, aAppId, documentType, notes, sDataContent);
            }
            catch (System.Net.WebException exc) when (
                tryCount == 1
                && exc.Status == System.Net.WebExceptionStatus.NameResolutionFailure
                && initialUrl != LoanWebServiceSettings.EdocsBackupURL(this.m_environment))
            {
                this.m_edocsLoan.Url = LoanWebServiceSettings.EdocsBackupURL(this.m_environment);
                return this.UploadPDFDocumentToApp(sTicket, sLNm, aAppId, documentType, notes, sDataContent, tryCount: 0); // start over when falling back
            }
            catch when (tryCount < tryLimit)
            {
                return this.UploadPDFDocumentToApp(sTicket, sLNm, aAppId, documentType, notes, sDataContent, tryCount);
            }
            catch (Exception exc)
            {
                throw new ImportGetDataFailureException("Failed to upload the report to EDocs", exc);
            }
            finally
            {
                if (this.m_edocsLoan.Url != initialUrl)
                {
                    this.m_edocsLoan.Url = initialUrl;
                }
            }
        }
    }
}