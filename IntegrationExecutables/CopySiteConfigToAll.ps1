param(
  [string]$SiteConfigPath = ([System.IO.Path]::Combine((Split-Path -Parent $MyInvocation.MyCommand.Definition), 'site.config')), # This file's current location + site.config
  [string]$TargetIntegrationExecutablePaths = (Split-Path -Parent $MyInvocation.MyCommand.Definition), # This file's current location
  [string]$CopiedFileName = 'site.config'
)

if (!(Test-Path $SiteConfigPath)) {
  Write-Warning "Cannot find site.config at '$SiteConfigPath'"
  return;
}

$Targets = 'SecondaryChangeReportV2\SCReport.Test',
  'SecondaryChangeReportV2\SecondaryChangeReport',
  'SendDataToClientDB\SendDataToClientDB',
  'SendDataToFIC\SendDataToFIC',
  'SendDataToMCM\SendDataToMCM',
  'SendDataToSI\SendDataToSI',
  'SendLoanStatusToCRMNow\SendLoanStatusToCRMNow',
  'SendReportToThirdParty\SendReportToThirdParty',
  'UndisclosedDebtNotification\UndisclosedDebtNotification'

$Targets | ForEach-Object {
  $targetPath = [System.IO.Path]::Combine($TargetIntegrationExecutablePaths, $_, $CopiedFileName)
  Copy-Item $SiteConfigPath $targetPath
}
