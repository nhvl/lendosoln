﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;

namespace SendLoanStatusToCRMNow
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Net.Mail.SmtpClient smtpClient = new System.Net.Mail.SmtpClient(AppSettings.SMTP_SERVER);
            SetupServicePointCallback();
            Adapter.Drivers.ConnectionStringProviderHelper.Register("SendLoanStatusToCRMNow");
            string connectionString = Adapter.Drivers.ConnectionStringProviderHelper.GetMainConnectionStringByDatabaseName(AppSettings.DSN, needWriteAccess: false);
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                foreach (string lender in AppSettings.Lenders)
                {
                    try
                    {
                        List<XElement> loanList = new List<XElement>();
                        #region query
                        SqlCommand cmd = new SqlCommand("RetrieveCRMNowLoanStatus", conn);
                        cmd.CommandTimeout = 60;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("CustomerCode", lender);
                        SqlParameter timeParam = new SqlParameter("LastTransactionTime", System.Data.SqlDbType.SmallDateTime);
                        timeParam.Value = GetLastTransactionTime(lender);
                        cmd.Parameters.Add(timeParam);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                XElement loan = new XElement("Loan");
                                loan.Add(new XElement("LoanNumber", reader[0]));
                                if (!reader[1].Equals(""))
                                {
                                    loan.Add(new XElement("Id", reader[1]));
                                }
                                loan.Add(new XElement("Status", reader[2]));
                                loan.Add(new XElement("StatusNumber", reader[3]));
                                string statusTime = Convert.ToDateTime(reader[4]).ToString("u");
                                loan.Add(new XElement("StatusDate",statusTime.Substring(0,statusTime.Length-1)));
                                loanList.Add(loan);
                            }
                            reader.NextResult();
                            reader.Read();
                            string transactionCompleted = ((System.DateTime)reader[0]).ToShortDateString(); //sSubmitD does not have time precision
                            SetLastTransactionTime(lender,transactionCompleted);
                        }
                        #endregion
                        if (loanList.Count > 0)
                        {
                            int batch = 0;
                            int batchIndex = 0;
                            int batchSize = 10;
                            var loanBatches = loanList.GroupBy(x => batchIndex++ / batchSize);
                            foreach (IEnumerable<XElement> loanBatch in loanBatches)
                            {
                                XElement postData = new XElement("Loans");
                                postData.Add(new XAttribute("Vendor", "LendingQB"));
                                postData.Add(loanBatch);

                                System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage("support@lendingqb.com", "isaacr@meridianlink.com", "CRMNow MIQ Export for " + lender + ", batch " + batch, postData.ToString());
                                smtpClient.Send(message);
                                try
                                {
                                    PostXml(postData, AppSettings.URL(lender));
                                }
                                catch (Exception e)
                                {
                                    System.Net.Mail.MailMessage errorMessage = new System.Net.Mail.MailMessage("support@lendingqb.com", "isaacr@meridianlink.com", "CRMNow MIQ Exception Thrown For " + lender + ", batch " + batch + ".", e.ToString());
                                    smtpClient.Send(errorMessage);
                                    continue;
                                }
                                finally
                                {
                                    batch++;
                                }
                            }
                        }
                    }

                    catch (Exception e)
                    {
                        System.Net.Mail.MailMessage errorMessage = new System.Net.Mail.MailMessage("support@lendingqb.com", "isaacr@meridianlink.com", "CRMNow MIQ Lender Exception Thrown For " + lender + ".", e.ToString());
                        smtpClient.Send(errorMessage);
                        continue;
                    }
                }
            }
        }

        private static void SetupServicePointCallback()
        {
            ServicePointManager.ServerCertificateValidationCallback = delegate(object sender, X509Certificate certificate, X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
            {
                if (sslPolicyErrors == System.Net.Security.SslPolicyErrors.None)
                {
                    return true; //If there are no errors then the certificate is valid
                }
                else if (sender is HttpWebRequest)
                {
                    return true; //All URL's used by the application are included in the config file, so approve
                }

                return false;
            };
        }

        private static string GetLastTransactionTime(string lender)
        {
            string sFilePath = lender + "_lastTransaction.txt";
            string sResult = System.DateTime.Today.AddDays(-1).ToString();   //fallback date
            if (File.Exists(sFilePath))
            {
                using (FileStream fsStream = new FileStream(sFilePath, FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    using (StreamReader srReader = new StreamReader(fsStream))
                    {
                        sResult = srReader.ReadToEnd();
                    }
                }
            }
            return sResult;
        }

        private static void SetLastTransactionTime(string lender, string lastTransactionTime)
        {
            string sFilePath = lender + "_lastTransaction.txt";
            using (StreamWriter writer = new StreamWriter(sFilePath, false))
            {
                writer.Write(lastTransactionTime);
            }
        }

        private static void PostXml(XElement postData, string postUrl)
        {
            XElement responseXml;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(postUrl);
            request.Method = "POST";
            request.ContentType = "text/xml";

            XmlWriter writer;
            XmlWriterSettings writerSettings = new XmlWriterSettings();
            writerSettings.Indent = true;

            using (Stream dataStream = request.GetRequestStream())
            {
                using (writer = XmlWriter.Create(dataStream, writerSettings))
                {
                    postData.Save(writer);
                    writer.Flush();
                }
            }
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                string reponseString;
                using (Stream dataStream = response.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(dataStream))
                    {
                        reponseString = reader.ReadToEnd();
                    }
                }

                try
                {
                    responseXml = XElement.Parse(reponseString);
                }
                catch
                {
                    throw new Exception("Response is not valid xml:\n" + reponseString);
                }
            }
           ValidateResponseXml(responseXml);
        }

        private static void ValidateResponseXml(XElement responseXml)
        {
            var errorLoans = from e in responseXml.Descendants("Loan") where (e.Element("Status") == null ? true : e.Element("Status").Value != "OK") select e;
            if (errorLoans.Count() != 0)
            {
                string errorMessage = "MortgageIQ response xml indicates possible error in the following loan files:\n";
                foreach (XElement errorLoan in errorLoans)
                {
                    errorMessage += errorLoan;
                }
                throw new Exception(errorMessage);
            }
        }
    }
}
