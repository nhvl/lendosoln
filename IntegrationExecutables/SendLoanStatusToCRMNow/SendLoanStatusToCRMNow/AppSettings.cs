﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SendLoanStatusToCRMNow
{
    public static class AppSettings
    {
        public static string DSN
        {
            get { return System.Configuration.ConfigurationManager.AppSettings["DSN"]; }
        }
        public static string[] Lenders
        {
            get
            {
                try
                {
                    return System.Configuration.ConfigurationManager.AppSettings["LENDERS"].Split(';');
                }
                catch (NullReferenceException)
                {
                    return new string[0];
                }
            }
        }
        public static string URL(string lender)
        {
            return System.Configuration.ConfigurationManager.AppSettings[lender + "_URL"];
        }
        public static string SMTP_SERVER
        {
            get { return System.Configuration.ConfigurationManager.AppSettings["SMTP_SERVER"]; }
        }
    }

}
