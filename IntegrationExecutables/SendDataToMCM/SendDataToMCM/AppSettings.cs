﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace SendDataToMCM
{
    class AppSettings
    {
        public static string DSN
        {
            get { return ConfigurationManager.AppSettings["DSN"]; }
        }
        public static string[] LENDERS
        {
            get { return ConfigurationManager.AppSettings["LENDERS"].Split(';'); }
        }

        public static string FTP_HOST
        {
            get { return ConfigurationManager.AppSettings["FTP_HOST"]; }
        }
        public static string[] FTP_LOGINS
        {
            get { return ConfigurationManager.AppSettings["FTP_LOGINS"].Split(';'); }
        }
        public static string[] FTP_PASSWORDS
        {
            get { return ConfigurationManager.AppSettings["FTP_PASSWORDS"].Split(';'); }
        }
        public static string RECIPIENT
        {
            get { return ConfigurationManager.AppSettings["RECIPIENT"]; }
        }
        public static string TEMP_FILE
        {
            get { return ConfigurationManager.AppSettings["TEMP_FILE"]; }
        }
        public static string SMTP_SERVER
        {
            get { return ConfigurationManager.AppSettings["SMTP_SERVER"]; }
        }
    }
}
