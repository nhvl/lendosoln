﻿using System;
using System.IO;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SendDataToMCM
{
    class Column
    {
        string m_fieldName;
        string m_dbFieldName;
        public Column(string fieldName, string dbFieldName)
        {
            m_fieldName = fieldName;
            m_dbFieldName = dbFieldName;
        }
        public string FieldName
        {
            get { return m_fieldName; }
        }
        public string DBFieldName
        {
            get { return m_dbFieldName; }
        }
    }
    class ReportGenerator
    {
        public void Generate(SqlDataReader reader, System.IO.Stream writer)
        {
            StreamWriter streamWriter = new StreamWriter(writer);
            // write header
            for (int i = 0; i < reader.FieldCount; i++)
                WriteCSVField(i, reader.GetName(i), streamWriter);
            streamWriter.WriteLine();

            // write body
            while (reader.Read())
            {
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    WriteCSVField(i, Convert.ToString(reader[i]), streamWriter);
                }
                streamWriter.WriteLine();
            }
            streamWriter.Flush();
        }
        void WriteCSVField(int index, string value, StreamWriter writer)
        {
            value = value.Replace("\"", "\"\"").Replace("\r", " ").Replace("\n", " ");
            if (index == 0)
                writer.Write("\"{0}\"", value);
            else
                writer.Write(",\"{0}\"", value);
        }
    }
}
