﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Xml;
using WinSCP;
using System.Threading;
using System.Net;

namespace SendDataToMCM
{
    class Program
    {
        private const string FtpHostName = "www.mortcap.com";

        private static void TestFtpNet()
        {
            Console.WriteLine("UserName:[" + AppSettings.FTP_LOGINS[0] + "], Pwd:[" + AppSettings.FTP_PASSWORDS[0] + "]");

            FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://" + FtpHostName + "/test.txt");

            request.Method = WebRequestMethods.Ftp.UploadFile;

            request.Credentials = new NetworkCredential(AppSettings.FTP_LOGINS[0], AppSettings.FTP_PASSWORDS[0]);

            byte[] fileContent = null;
            using (StreamReader source = new StreamReader(AppSettings.TEMP_FILE))
            {
                fileContent = Encoding.ASCII.GetBytes(source.ReadToEnd());
            }

            request.ContentLength = fileContent.Length;

            string statusDescription = string.Empty;
            FtpStatusCode statusCode;
            using (Stream requestStream = request.GetRequestStream())
            {
                requestStream.Write(fileContent, 0, fileContent.Length);
                requestStream.Close();

                using (FtpWebResponse response = (FtpWebResponse)request.GetResponse())
                {
                    statusDescription = response.StatusDescription;
                    statusCode = response.StatusCode;
                }
            }

            Console.WriteLine("StatusDescription=" + statusDescription + ". StatusCode=" + statusCode);
        }
        private static void Test()
        {
            Console.WriteLine("HostName: [" + AppSettings.FTP_HOST + "], UserName:[" + AppSettings.FTP_LOGINS[0] + "], Pwd:[" + AppSettings.FTP_PASSWORDS[0] + "]");

            SessionOptions sessionOptions = new SessionOptions
            {
                Protocol = Protocol.Ftp,
                HostName = AppSettings.FTP_HOST, //"www.mortcap.com",
                UserName = AppSettings.FTP_LOGINS[0],
                Password = AppSettings.FTP_PASSWORDS[0]
                
            };
            
            using (Session session = new Session())
            {
                session.DebugLogPath = "DebugLog.txt";
                session.SessionLogPath = "SessionLog.txt";
                session.Open(sessionOptions);
                //TransferOptions transferOptions = new TransferOptions();
                //transferOptions.TransferMode = TransferMode.Automatic;

                //TransferOperationResult transferResult = session.PutFiles(AppSettings.TEMP_FILE, "test.txt", false, transferOptions);
                //transferResult.Check();

                //Console.WriteLine("Is Success=" + transferResult.IsSuccess);
                Console.WriteLine("Success");
            }

        }

        static void ftpConn_ConnectionFailed()
        {
            Console.WriteLine("Connection Failed");
        }

        private static bool UploadFiles(Session session, string localFilePath, string remoteFilePath)
        {
            TransferOptions transferOptions = new TransferOptions();
            transferOptions.TransferMode = TransferMode.Automatic;

            TransferOperationResult transferResult = session.PutFiles(AppSettings.TEMP_FILE, remoteFilePath, false, transferOptions);
            transferResult.Check();

            return transferResult.IsSuccess;

        }

        private static bool UploadFiles(string userName, string password, string localFilePath, string remoteFilePath)
        {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://" + FtpHostName + "/" + remoteFilePath);

            request.Method = WebRequestMethods.Ftp.UploadFile;

            request.Credentials = new NetworkCredential(userName, password);

            byte[] fileContent = null;
            using (StreamReader source = new StreamReader(localFilePath))
            {
                fileContent = Encoding.ASCII.GetBytes(source.ReadToEnd());
            }

            request.ContentLength = fileContent.Length;

            string statusDescription = string.Empty;
            FtpStatusCode statusCode;
            using (Stream requestStream = request.GetRequestStream())
            {
                requestStream.Write(fileContent, 0, fileContent.Length);
                requestStream.Close();

                using (FtpWebResponse response = (FtpWebResponse)request.GetResponse())
                {
                    statusDescription = response.StatusDescription;
                    statusCode = response.StatusCode;
                }
            }

            File.AppendAllText(@"log.txt", DateTime.Now.ToString() + " - Upload " + remoteFilePath + " - " + statusCode + " - " + statusDescription);
            return true;
        }

        private static void SaveReportOnly()
        {
            ProgramData programData = new ProgramData();

            StringBuilder debugString = new StringBuilder();
            debugString.AppendLine("[" + DateTime.Now.ToString() + "]");

            using (SqlConnection conn = new SqlConnection(GetConnectionString(AppSettings.DSN)))
            {
                conn.Open();
                for (int lenderIdx = 0; lenderIdx < AppSettings.LENDERS.Length; lenderIdx++)
                {
                    string lender = AppSettings.LENDERS[lenderIdx];
                    LenderData lenderData = programData.GetLenderData(lender);

                    // LOCKED FILE
                    SqlCommand cmd = new SqlCommand(@"RetrieveMCMLockAndClose", conn);
                    cmd.CommandTimeout = 180;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("CustomerCode", lender);
                    cmd.Parameters.AddWithValue("Funded", 0);
                    SaveToTempFile(cmd, lender, "MCMLCKD.CSV");

                    // FUNDED FILE
                    cmd = new SqlCommand(@"RetrieveMCMLockAndClose", conn);
                    cmd.CommandTimeout = 180;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("CustomerCode", lender);
                    cmd.Parameters.AddWithValue("Funded", 1);
                    SaveToTempFile(cmd, lender, "MCMCL.CSV");

                    // 1/3/2013 dd - OPM 108499 - Send FALLOUT FILE everyday.
                    /// FALLOUT FILE
                    //if (lenderData.FalloutUploadDate.Month != DateTime.Now.Month) // check to see if we should upload the fallout file or not
                    //{
                    cmd = new SqlCommand(@"RetrieveMCMFallout", conn);
                    cmd.CommandTimeout = 180;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("CustomerCode", lender);
                    SaveToTempFile(cmd, lender, "MCMFALL.CSV");


                    /// TRADES FILE -- to be sent everytime we upload the lock/close loan files
                    cmd = new SqlCommand(@"RetrieveMCMTrades", conn);
                    cmd.CommandTimeout = 180;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("CustomerCode", lender);
                    SaveToTempFile(cmd, lender, "MCMTRDS.CSV");

                }
            }

        }

        private static string GetConnectionString(string databaseName)
        {
            return Adapter.Drivers.ConnectionStringProviderHelper.GetMainConnectionStringByDatabaseName(databaseName, needWriteAccess: false);
        }

        // 11/28/2012 dd - BECAREFUL WHEN RUN THIS EXECUTE OFF THE REGULAR SCHEDULE.
        // If file upload to MCM not during the regular schedule then MCM may inquire about it.
        static void Main(string[] args)
        {
            Adapter.Drivers.ConnectionStringProviderHelper.Register("SendDataToMCM");
            if (args.Length > 0 && args[0] == "test")
            {
                TestFtpNet();
                return;
            }
            else if (args.Length > 0 && args[0] == "report")
            {
                SaveReportOnly();
                return;
            }

            for (int currentCount = 0; currentCount < 3; currentCount++)
            {
                ProgramData programData = new ProgramData();

                StringBuilder debugString = new StringBuilder();
                debugString.AppendLine("[" + DateTime.Now.ToString() + "]");
                bool hasError = false;
                string lender = string.Empty;
                try
                {
                    using (SqlConnection conn = new SqlConnection(GetConnectionString(AppSettings.DSN)))
                    {
                        conn.Open();
                        for (int lenderIdx = 0; lenderIdx < AppSettings.LENDERS.Length; lenderIdx++)
                        {
                            lender = AppSettings.LENDERS[lenderIdx];
                            LenderData lenderData = programData.GetLenderData(lender);
                            string userName = AppSettings.FTP_LOGINS[0];
                            string password = AppSettings.FTP_PASSWORDS[0];
                            SessionOptions sessionOptions = new SessionOptions
                            {
                                Protocol = Protocol.Ftp,
                                HostName = "www.mortcap.com",
                                UserName = AppSettings.FTP_LOGINS[0],
                                Password = AppSettings.FTP_PASSWORDS[0]
                            };

                            using (Session session = new Session())
                            {

                                //session.DisableVersionCheck = true;
                                //session.DebugLogPath = lender + "_log.log";
                                //session.SessionLogPath = lender + "_session.log";
                                //session.XmlLogPath = lender + "_xml.log";
                                //session.Open(sessionOptions);

                                // LOCKED FILE
                                SqlCommand cmd = new SqlCommand(@"RetrieveMCMLockAndClose", conn);
                                cmd.CommandTimeout = 180;
                                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                                cmd.Parameters.AddWithValue("CustomerCode", lender);
                                cmd.Parameters.AddWithValue("Funded", 0);
                                SaveToTempFile(cmd, lender, "MCMLCKD.CSV");

                                if (UploadFiles(userName, password, AppSettings.TEMP_FILE, "MCMLCKD.CSV") == false)
                                //if (UploadFiles(session, AppSettings.TEMP_FILE, "MCMLCKD.CSV") == false)
                                {
                                    debugString.AppendFormat("FAILED TO UPLOAD MCMLCKD.CSV TO {0}", AppSettings.FTP_LOGINS[0]);
                                    debugString.AppendLine();
                                    hasError = true;
                                }

                                // FUNDED FILE
                                cmd = new SqlCommand(@"RetrieveMCMLockAndClose", conn);
                                cmd.CommandTimeout = 180;
                                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                                cmd.Parameters.AddWithValue("CustomerCode", lender);
                                cmd.Parameters.AddWithValue("Funded", 1);
                                SaveToTempFile(cmd, lender, "MCMCL.CSV");

                                if (UploadFiles(userName, password, AppSettings.TEMP_FILE, "MCMCL.CSV") == false)
                                //if (UploadFiles(session, AppSettings.TEMP_FILE, "MCMCL.CSV") == false)
                                {
                                    debugString.AppendFormat("FAILED TO UPLOAD MCMCL.CSV TO {0}", AppSettings.FTP_LOGINS[0]);
                                    debugString.AppendLine();
                                    hasError = true;
                                }

                                // 1/3/2013 dd - OPM 108499 - Send FALLOUT FILE everyday.
                                /// FALLOUT FILE
                                //if (lenderData.FalloutUploadDate.Month != DateTime.Now.Month) // check to see if we should upload the fallout file or not
                                //{
                                cmd = new SqlCommand(@"RetrieveMCMFallout", conn);
                                cmd.CommandTimeout = 180;
                                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                                cmd.Parameters.AddWithValue("CustomerCode", lender);
                                SaveToTempFile(cmd, lender, "MCMFALL.CSV");

                                if (UploadFiles(userName, password, AppSettings.TEMP_FILE, "MCMFALL.CSV") == false)
                                //if (UploadFiles(session, AppSettings.TEMP_FILE, "MCMFALL.CSV") == false)
                                {
                                    debugString.AppendFormat("FAILED TO UPLOAD MCMFALL.CSV TO {0}", AppSettings.FTP_LOGINS[0]);
                                    debugString.AppendLine();
                                    hasError = true;
                                }
                                else
                                {
                                    lenderData.FalloutUploadDate = DateTime.Now;
                                }

                                /// TRADES FILE -- to be sent everytime we upload the lock/close loan files
                                cmd = new SqlCommand(@"RetrieveMCMTrades", conn);
                                cmd.CommandTimeout = 180;
                                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                                cmd.Parameters.AddWithValue("CustomerCode", lender);
                                SaveToTempFile(cmd, lender, "MCMTRDS.CSV");

                                if (UploadFiles(userName, password, AppSettings.TEMP_FILE, "MCMTRDS.CSV") == false)
                                //if (UploadFiles(session, AppSettings.TEMP_FILE, "MCMTRDS.CSV") == false)
                                {
                                    debugString.AppendFormat("FAILED TO UPLOAD MCMTRDS.CSV TO {0}", AppSettings.FTP_LOGINS[0]);
                                    debugString.AppendLine();
                                    hasError = true;
                                }
                                else
                                {
                                    lenderData.TradeUploadDate = DateTime.Now;
                                }
                            }
                        }
                    }
                    return;
                }
                catch (Exception ex)
                {
                    SmtpClient smtpClient = new SmtpClient(AppSettings.SMTP_SERVER);
                    MailMessage message = new MailMessage("support@pricemyloan.com", "davidd@lendingqb.com", Environment.MachineName + " - SendDataToMCM - " + lender + ". Count=" + currentCount, ex.ToString());
                    smtpClient.Send(message);
                    Thread.Sleep(5000);
                }
                finally
                {
                    if (hasError)
                    {
                        SmtpClient smtpClient = new SmtpClient(AppSettings.SMTP_SERVER);
                        MailMessage message = new MailMessage("support@pricemyloan.com", "davidd@lendingqb.com", Environment.MachineName + " - SendDataToMCM - " + lender, debugString.ToString());
                        smtpClient.Send(message);
                        Console.WriteLine(debugString.ToString());
                    }
                    programData.Save();
                    debugString.AppendLine("Complete");
                    File.WriteAllText("status.txt", debugString.ToString());
                }
            }
        }
        static void SaveToTempFile(SqlCommand cmd, string lender, string backupFileName)
        {
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                // generate CSV data
                byte[] data;
                using (MemoryStream ms = new MemoryStream())
                {
                    ReportGenerator generator = new ReportGenerator();
                    generator.Generate(reader, ms);
                    ms.Flush();
                    data = ms.ToArray();
                }
                string s = System.Text.ASCIIEncoding.ASCII.GetString(data);

                // save data
                using (FileStream writer = new FileStream(AppSettings.TEMP_FILE, FileMode.Create))
                {
                    writer.Write(data, 0, data.Length);
                }

                string archiveFolder = @"Archives\" + DateTime.Now.ToString("yyyy_MM_dd_HH_mm") + @"\" + lender;

                if (Directory.Exists(archiveFolder) == false)
                {
                    Directory.CreateDirectory(archiveFolder);
                }
                File.WriteAllText(archiveFolder + @"\" + backupFileName, s);

            }
        }
    }
}
