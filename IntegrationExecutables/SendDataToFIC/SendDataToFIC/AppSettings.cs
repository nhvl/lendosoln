﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SendDataToFIC
{
    class AppSettings
    {
        public static string DSN
        {
            get { return System.Configuration.ConfigurationManager.AppSettings["DSN"]; }
        }
        public static string[] LENDERS
        {
            get { return System.Configuration.ConfigurationManager.AppSettings["LENDERS"].Split(';'); }
        }
        public static string[] FTP_LOGINS
        {
            get { return System.Configuration.ConfigurationManager.AppSettings["FTP_LOGINS"].Split(';'); }
        }
        public static string[] FTP_PASSWORDS
        {
            get { return System.Configuration.ConfigurationManager.AppSettings["FTP_PASSWORDS"].Split(';'); }
        }
        public static string RECIPIENT
        {
            get { return System.Configuration.ConfigurationManager.AppSettings["RECIPIENT"]; }
        }
        public static string TEMP_FILE
        {
            get { return System.Configuration.ConfigurationManager.AppSettings["TEMP_FILE"]; }
        }
        public static string SMTP_SERVER
        {
            get { return System.Configuration.ConfigurationManager.AppSettings["SMTP_SERVER"]; }
        }
    }
}
