﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;

namespace SendDataToFIC
{
    class ProgramData
    {
        XmlDocument m_data;
        public ProgramData()
        {
            m_data = new XmlDocument();

            string filename = GetDataFileName();
            if (File.Exists(filename))
                m_data.Load(filename);
            else
                m_data.LoadXml("<data/>");
        }
        public LenderData GetLenderData(string lender)
        {
            XmlElement xe = (XmlElement)m_data.DocumentElement.SelectSingleNode(lender);

            if (xe == null)
            {
                xe = m_data.CreateElement(lender);
                m_data.DocumentElement.AppendChild(xe);
            }
            return new LenderData(xe);
        }
        public void Save()
        {
            m_data.Save(GetDataFileName());
        }
        string GetDataFileName()
        {
            return string.Format("{0}\\data.xml", Environment.CurrentDirectory);
        }
    }
    class LenderData
    {
        XmlElement m_lenderData;
        public LenderData(XmlElement lenderData)
        {
            m_lenderData = lenderData;
        }
        public DateTime TradeUploadDate
        {
            get
            {
                try
                {
                    return Convert.ToDateTime(m_lenderData.GetAttribute("trade_upload_date"));
                }
                catch
                {
                    return DateTime.MinValue;
                }
            }
            set
            {
                m_lenderData.SetAttribute("trade_upload_date", value.ToShortDateString());
            }
        }
        public DateTime FalloutUploadDate
        {
            get
            {
                try
                {
                    return Convert.ToDateTime(m_lenderData.GetAttribute("fallout_upload_date"));
                }
                catch
                {
                    return DateTime.MinValue;
                }
            }
            set
            {
                m_lenderData.SetAttribute("fallout_upload_date", value.ToShortDateString());
            }
        }
    }
}
