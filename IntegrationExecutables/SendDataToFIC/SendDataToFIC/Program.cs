﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Xml;

namespace SendDataToFIC
{
    class Program
    {//data@flatironscapital.com
        static void Main(string[] args)
        {
            System.Net.Mail.SmtpClient smtpClient = new SmtpClient(AppSettings.SMTP_SERVER);
            ProgramData programData = new ProgramData();

            try
            {
                Adapter.Drivers.ConnectionStringProviderHelper.Register("SendDataToFIC");
                string connectionString = Adapter.Drivers.ConnectionStringProviderHelper.GetMainConnectionStringByDatabaseName(AppSettings.DSN, needWriteAccess: false);
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    for (int lenderIdx = 0; lenderIdx < AppSettings.LENDERS.Length; lenderIdx++)
                    {
                        string lender = AppSettings.LENDERS[lenderIdx];
                        LenderData lenderData = programData.GetLenderData(lender);

                        SqlCommand cmd = new SqlCommand(@"RetrieveFICData", conn);
                        cmd.CommandTimeout = 180;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("CustomerCode", lender);

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            // generate CSV data
                            byte[] data;
                            using (MemoryStream ms = new MemoryStream())
                            {
                                ReportGenerator generator = new ReportGenerator();
                                generator.Generate(reader, ms);
                                ms.Flush();
                                data = ms.ToArray();
                            }
                            string s = System.Text.ASCIIEncoding.ASCII.GetString(data);

                            // create mail message

                            MailMessage message = new MailMessage();
                            message.From = new MailAddress("support@pricemyloan.com");
                            

                            string[] recipientAddresses = AppSettings.RECIPIENT.Split(';');
                            foreach (var recipient in recipientAddresses)
                            {
                                message.To.Add(recipient);
                            }

                            //MailMessage message = new MailMessage("support@pricemyloan.com", AppSettings.RECIPIENT);
                            
                            message.Subject = lender;
                            Attachment attachment;
                            using (MemoryStream ms = new MemoryStream(data))
                            {
                                attachment = new Attachment(ms, string.Format("{0}.CSV", lender));
                                message.Attachments.Add(attachment);

                                // send message
                                smtpClient.Send(message);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                smtpClient = new SmtpClient(AppSettings.SMTP_SERVER);
                MailMessage message = new MailMessage("support@pricemyloan.com", "davidd@lendingqb.com", "SendDataToFIC", ex.ToString());
                smtpClient.Send(message);
            }
            finally
            {
                programData.Save();
            }
        }
    }
}
