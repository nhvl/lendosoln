# SendDataToClientDB

This tool is dedicated to the goal of writing a copy of the lender's data to a SQL database
for their access.  This is accomplished via an executable, [SendDataToClientDB](SendDataToClientDB),
which loads and transmits the data to a web service, [ReceiveDataFromLQB](ReceiveDataFromLQB),
hosted on the lender's server.

For details on the business design of the service and client set up process, see the [intranet 
article](https://intranet/Projects/LendingQB/Integrations/SQL_Data_Subscription_Service).

### Clients for the service
Due to changes in the business requirements for this tool, the executable supports sending
data to multiple recipients ("receivers") as of changes made in early-mid 2016 for
[Case 240089](https://lqbopm/default.asp?240089).  In most cases, third party vendors implement
their own version of [Client.asmx](ReceiveDataFromLQB/Client.asmx) for us to push data to.  This
additional target should generally not affect development of the main tool.  The main observable
difference in the code is a prevalence of variables named `receiverName` instead of
`customerCode`. These are very similar, but the receiver is intended as a more generic concept.
Thus, for most of the code and docs we will describe receivers of the data, but these are
usually just lenders.
