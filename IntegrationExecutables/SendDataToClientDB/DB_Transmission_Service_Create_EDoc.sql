--     SQL Script Name: DB_Transmission_Service_Create_Table_EDoc.sql

CREATE TABLE [dbo].[EDoc](
	[DocumentId] [uniqueidentifier] NOT NULL,
	[sLId] [uniqueidentifier] NULL,
	[aAppId] [uniqueidentifier] NULL,
	[CreatedDate] [datetime] NULL,
	[DocTypeId] [int] NOT NULL,
	[DocTypeName] [varchar](60) NULL,
	[DocTypeIsValid] [bit] NULL,
	[InternalDescription] [varchar](200) NOT NULL,
	[IsValid] [bit] NOT NULL,
	[FolderId] [int] NULL,
	[FolderName] [varchar](50) NULL,
	[LastModifiedDate] [datetime] NULL,
	[NumPages] [int] NOT NULL,
	[PublicDescription] [varchar](200) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[StatusDescription] [varchar](200) NOT NULL,
 CONSTRAINT [PK_EDoc_DocumentId] PRIMARY KEY CLUSTERED 
(
	[DocumentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[EDocs_Audit_History](
	[DocumentId] [uniqueidentifier] NOT NULL,
	[ModifiedByUserId] [uniqueidentifier] NULL,
	[Version] [int] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Description] [varchar](200) NOT NULL,
	[Details] [varchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Version] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO