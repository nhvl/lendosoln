ALTER TABLE dbo.Loan
ADD LastUpdatedDate datetime NULL DEFAULT CURRENT_TIMESTAMP;
GO

CREATE TRIGGER dbo.SetLastUpdatedDate
ON dbo.Loan
AFTER UPDATE
AS
BEGIN
    IF NOT UPDATE(LastUpdatedDate)
    BEGIN
        UPDATE t
            SET t.LastUpdatedDate = CURRENT_TIMESTAMP
            FROM dbo.Loan AS t
            INNER JOIN inserted AS i ON t.sLId = i.sLId
    END
END
GO
