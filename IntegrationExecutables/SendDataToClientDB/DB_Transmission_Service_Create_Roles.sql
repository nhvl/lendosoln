-- Set up ROLE table
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ROLE]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ROLE](
	[RoleId] [uniqueidentifier] NOT NULL,
	[RoleDesc] [varchar](21) NOT NULL,
	[RoleModifiableDesc] [varchar](50) NOT NULL CONSTRAINT [DF__Role__RoleModifiableDesc]  DEFAULT (''),
 CONSTRAINT [PK_ROLE] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 50) ON [PRIMARY]
) ON [PRIMARY]
END
GO

INSERT [dbo].[ROLE] ([RoleId], [RoleDesc], [RoleModifiableDesc]) VALUES (N'e0534f45-2ae1-4a8c-952e-00520f445819', N'Underwriter', N'Underwriter')
INSERT [dbo].[ROLE] ([RoleId], [RoleDesc], [RoleModifiableDesc]) VALUES (N'74097291-502a-4f50-9018-11a6a302b12e', N'DocDrawer', N'Doc Drawer')
INSERT [dbo].[ROLE] ([RoleId], [RoleDesc], [RoleModifiableDesc]) VALUES (N'86c86cf3-feb6-400f-80dc-123363a79605', N'Administrator', N'Administrator')
INSERT [dbo].[ROLE] ([RoleId], [RoleDesc], [RoleModifiableDesc]) VALUES (N'763f27c8-eac4-45ff-ad65-15e27bdc41f1', N'Purchaser', N'Purchaser')
INSERT [dbo].[ROLE] ([RoleId], [RoleDesc], [RoleModifiableDesc]) VALUES (N'1764dffd-ebf9-4375-accd-1b822b39fa63', N'LoanOpener', N'Loan Opener')
INSERT [dbo].[ROLE] ([RoleId], [RoleDesc], [RoleModifiableDesc]) VALUES (N'e34352c6-45a8-4239-b2d1-218748c8b120', N'CollateralAgent', N'Collateral Agent')
INSERT [dbo].[ROLE] ([RoleId], [RoleDesc], [RoleModifiableDesc]) VALUES (N'274981e4-8c18-4204-b97b-2f537922ccd9', N'ExternalSecondary', N'Secondary (External)')
INSERT [dbo].[ROLE] ([RoleId], [RoleDesc], [RoleModifiableDesc]) VALUES (N'89811c63-6a28-429a-bb45-3152449d854e', N'ExternalPostCloser', N'Post-Closer (External)')
INSERT [dbo].[ROLE] ([RoleId], [RoleDesc], [RoleModifiableDesc]) VALUES (N'6c83465c-d3e9-4950-918b-331de262b4a5', N'CreditAuditor', N'Credit Auditor')
INSERT [dbo].[ROLE] ([RoleId], [RoleDesc], [RoleModifiableDesc]) VALUES (N'a24e962e-301e-4436-bca1-37e5d2227a30', N'PostCloser', N'Post-Closer')
INSERT [dbo].[ROLE] ([RoleId], [RoleDesc], [RoleModifiableDesc]) VALUES (N'270a9a82-f572-4810-8b8d-3d8d0eedc65f', N'QCCompliance', N'QC Compliance')
INSERT [dbo].[ROLE] ([RoleId], [RoleDesc], [RoleModifiableDesc]) VALUES (N'9067077d-6bd3-4e70-99c2-4a4aaf7811d7', N'LockDesk', N'Lock Desk')
INSERT [dbo].[ROLE] ([RoleId], [RoleDesc], [RoleModifiableDesc]) VALUES (N'27424fdc-d6c8-432c-860a-4eca064f924f', N'Shipper', N'Shipper')
INSERT [dbo].[ROLE] ([RoleId], [RoleDesc], [RoleModifiableDesc]) VALUES (N'd8dc85b9-629e-4488-95ab-5dec71fd8018', N'Insuring', N'Insuring')
INSERT [dbo].[ROLE] ([RoleId], [RoleDesc], [RoleModifiableDesc]) VALUES (N'5b02d42c-1f89-4bec-8a29-63872837fc1d', N'Telemarketer', N'Call Center Agent')
INSERT [dbo].[ROLE] ([RoleId], [RoleDesc], [RoleModifiableDesc]) VALUES (N'270a7689-eeed-49f9-b438-69ac322ea834', N'Closer', N'Closer')
INSERT [dbo].[ROLE] ([RoleId], [RoleDesc], [RoleModifiableDesc]) VALUES (N'89572cfa-a3ee-4513-9b25-71a212ad5d09', N'Processor', N'Processor')
INSERT [dbo].[ROLE] ([RoleId], [RoleDesc], [RoleModifiableDesc]) VALUES (N'1386d205-f5fb-435e-afa8-730cddeb824e', N'DisclosureDesk', N'Disclosure Desk')
INSERT [dbo].[ROLE] ([RoleId], [RoleDesc], [RoleModifiableDesc]) VALUES (N'c5058d4e-bbc2-425b-b4a3-73b8cd66d896', N'Servicing', N'Servicing')
INSERT [dbo].[ROLE] ([RoleId], [RoleDesc], [RoleModifiableDesc]) VALUES (N'e4949811-34dc-4050-adf5-78a06990e24e', N'LoanOfficerAssistant', N'Loan Officer Assistant')
INSERT [dbo].[ROLE] ([RoleId], [RoleDesc], [RoleModifiableDesc]) VALUES (N'ad4661df-4dca-4fe4-a60d-85424fb937fe', N'JuniorUnderwriter', N'Junior Underwriter')
INSERT [dbo].[ROLE] ([RoleId], [RoleDesc], [RoleModifiableDesc]) VALUES (N'f98c17f5-27ad-4097-acfb-88f6a754acf3', N'Accountant', N'Accountant')
INSERT [dbo].[ROLE] ([RoleId], [RoleDesc], [RoleModifiableDesc]) VALUES (N'a9604616-6c20-4d0f-be97-89913cf11ae3', N'LegalAuditor', N'Legal Auditor')
INSERT [dbo].[ROLE] ([RoleId], [RoleDesc], [RoleModifiableDesc]) VALUES (N'f544df6a-5c55-4568-9579-97d74ed0f752', N'Manager', N'Manager')
INSERT [dbo].[ROLE] ([RoleId], [RoleDesc], [RoleModifiableDesc]) VALUES (N'6dfc9b0e-11e3-48c1-b410-a1b0b5363b69', N'Agent', N'Loan Officer')
INSERT [dbo].[ROLE] ([RoleId], [RoleDesc], [RoleModifiableDesc]) VALUES (N'4a03cf0f-d2e8-4201-b161-a5bc9e87fde3', N'JuniorProcessor', N'Junior Processor')
INSERT [dbo].[ROLE] ([RoleId], [RoleDesc], [RoleModifiableDesc]) VALUES (N'74ed15e7-c06d-4eab-9cb0-a64ab412fbe5', N'RealEstateAgent', N'Real Estate Agent')
INSERT [dbo].[ROLE] ([RoleId], [RoleDesc], [RoleModifiableDesc]) VALUES (N'1f51b219-5f29-4729-bad4-ab2acc670f24', N'Secondary', N'Secondary')
INSERT [dbo].[ROLE] ([RoleId], [RoleDesc], [RoleModifiableDesc]) VALUES (N'b3785253-d9e8-4a3f-b0ec-d02d6fcf8538', N'BrokerProcessor', N'Processor (External)')
INSERT [dbo].[ROLE] ([RoleId], [RoleDesc], [RoleModifiableDesc]) VALUES (N'552eba07-ea55-404f-8c00-dc696eaa04bb', N'Consumer', N'Consumer')
INSERT [dbo].[ROLE] ([RoleId], [RoleDesc], [RoleModifiableDesc]) VALUES (N'4faa1c9e-3188-4d82-9734-e4991f6ad50c', N'LenderAccountExec', N'Lender Account Executive')
INSERT [dbo].[ROLE] ([RoleId], [RoleDesc], [RoleModifiableDesc]) VALUES (N'9658e876-a3ec-4b3e-b081-f0f955719820', N'Funder', N'Funder')

-- Set up ROLE_ASSIGNMENT table
SET ANSI_PADDING ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ROLE_ASSIGNMENT]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ROLE_ASSIGNMENT](
	[EmployeeId] [uniqueidentifier] NOT NULL,
	[RoleId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_ROLE_ASSIGNMENT] PRIMARY KEY CLUSTERED 
(
	[EmployeeId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 50) ON [PRIMARY]
) ON [PRIMARY]
END
GO

