﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using SendDataToClientDB.ReceiveDataFromLQBClientService;

namespace SendDataToClientDB
{
    /// <summary>
    /// Stores LQB's data response in a list of Dictionary records for processing and sending to the ReceiveDataFromLQB client.
    /// </summary>
    /// <typeparam name="TFieldId">
    /// A field identifier used to index the mapping from field ID to DB type. This must be an <see cref="Enum"/> type.
    /// (I wanted to make strings usable as well, but this runs into problems with the <see cref="ToIdFieldPair(XElement, IDictionary{TFieldId, DBTypes})"/> method converting strings into <see cref="TFieldId"/>. If necessary an "IParsable" could be added to allow more flexibility here.)
    /// </typeparam>
    public class LQBData<TFieldId> where TFieldId : struct, IConvertible
    {
        /// <summary>
        /// Creates the object directly as a container for a list of <see cref="Dictionary{TFieldId, Field}"/> records.
        /// </summary>
        /// <param name="records">The dictionary which maps field ID to field value for the data export.</param>
        public LQBData(IEnumerable<Dictionary<TFieldId, Field>> records)
        {
            if (!typeof(TFieldId).IsEnum)
            {
                throw new InvalidOperationException("TFieldId must be an Enum type.");
            }

            if (records != null)
            {
                this.Records = records.ToList();
            }
            else
            {
                this.Records = new List<Dictionary<TFieldId, Field>>();
            }
        }

        /// <summary>
        /// A list of database records / objects, each represented by a dictionary mapping the field IDs of the object to the values of those fields.
        /// </summary>
        public List<Dictionary<TFieldId, Field>> Records { get; private set; }

        /// <summary>
        /// Iterates through each dictionary in <see cref="Records"/> to compile a complete list of <see cref="TFieldId"/> values present.
        /// </summary>
        /// <returns>A list of all <see cref="TFieldId"/> values among all dictionaries in <see cref="Records"/>.</returns>
        /// <exception cref="InvalidOperationException">If <see cref="Records"/> is a null or empty list.</exception>
        public List<TFieldId> AllFieldIds()
        {
            return this.Records.SelectMany(dict => dict.Keys).Distinct().ToList();
        }

        /// <summary>
        /// Splits the list of records in this LQBData object's <see cref="Records"/> "table" into multiple tables by the list of fields that should be included in each output table.
        /// </summary>
        /// <param name="idsForData">
        /// A variable number of ICollections containing <see cref="TFieldId"/>values to filter by. 
        /// Each input collection (not including nulls) will result in a corresponding LQBData in the output.
        /// </param>
        /// <returns>A list of N <see cref="LQBData{TFieldId}"/>, where N is the number of parameters.
        /// Each LQBData in the return list will have the same number of "record" dictionaries in its <see cref="Records"/> as in this LQBData.
        /// Each LQBData with index I in the return list will have only IDs in its dictionary corresponding to the Ith parameter to the method.
        /// </returns>
        /// <example>
        /// this.Records:
        /// TFieldId keys in dictionaries:  UserFirstNm | UserLastNm    | UserId
        ///                                 ---------------------------------
        /// Dictionary values:              Sam         | Sample        | 01
        ///                                 Fred        | Fake          | 02
        ///                                 Dummy       | User          | 03
        ///
        /// Method params: { UserFirstNm, UserId }, { UserLastNm, UserId}
        /// 
        /// Return List[0]:                 UserFirstNm | UserId
        ///                                 --------------------
        /// Dictionary values:              Sam         | 01
        ///                                 Fred        | 02
        ///                                 Dummy       | 03
        /// 
        /// Return List[1]:                 UserLastNm | UserId
        ///                                 --------------------
        /// Dictionary values:              Sample     | 01
        ///                                 Fake       | 02
        ///                                 User       | 03
        /// </example>
        /// <exception cref="ArgumentNullException">The parameters were all null, or there were no parameters passed to the method.</exception>
        public List<LQBData<TFieldId>> SplitForMultipleTables(params ICollection<TFieldId>[] idsForData)
        {
            if (idsForData == null || idsForData.Count() == 0 || idsForData.Any(item => item == null))
            {
                throw new ArgumentNullException("Parameters must be non-null ID collections.");
            }

            // Initialize all these nested collections
            var splitDataContainers = new List<List<Dictionary<TFieldId, Field>>>(idsForData.Length);
            for (int recordGroupIndex = 0; recordGroupIndex < splitDataContainers.Capacity; recordGroupIndex++)
            {
                splitDataContainers.Add(new List<Dictionary<TFieldId, Field>>(this.Records.Count));
                for (int recordIndex = 0; recordIndex < splitDataContainers[recordGroupIndex].Capacity; recordIndex++)
                {
                    splitDataContainers[recordGroupIndex].Add(new Dictionary<TFieldId, Field>(idsForData[recordGroupIndex].Count()));
                }
            }

            for (int recordIndex = 0; recordIndex < this.Records.Count; recordIndex++)
            {
                Dictionary<TFieldId, Field> record = this.Records[recordIndex];
                for (int fieldIndex = 0; fieldIndex < record.Count; fieldIndex++)
                {
                    KeyValuePair<TFieldId, Field> fieldInfo = record.ElementAt(fieldIndex);
                    for (int idListIndex = 0; idListIndex < idsForData.Length; idListIndex++)
                    {
                        if (idsForData[idListIndex].Contains(fieldInfo.Key))
                        {
                            splitDataContainers[idListIndex][recordIndex].Add(fieldInfo.Key, fieldInfo.Value);
                        }
                    }
                }
            }

            return splitDataContainers.Select(list => new LQBData<TFieldId>(list)).ToList();
        }

        /// <summary>
        /// Converts a "field" XML element into a pair of FieldId and Field elements for adding into a record dictionary.
        /// </summary>
        /// <param name="fieldElement">The field element, containing the value and optionally a type attribute.</param>
        /// <param name="fieldTypeMapping">A mapping for looking up the database type if one is not found.</param>
        /// <returns>A pair of FieldId and Field elements for adding into a record dictionary.</returns>
        /// <exception cref="ArgumentException">If the Field ID in <paramref name="fieldElement"/> cannot be parsed into a <see cref="TFieldId"/>.</exception>
        private static KeyValuePair<TFieldId, Field> ToIdFieldPair(XElement fieldElement, IDictionary<TFieldId, DBTypes> fieldTypeMapping)
        {
            TFieldId fieldId = (TFieldId)Enum.Parse(typeof(TFieldId), fieldElement.Attribute("id").Value);

            DBTypes dbType = Tools.SafeParseEnum<DBTypes>(fieldElement.AttributeSafeValue("type")) ?? fieldTypeMapping[fieldId];

            Field fieldValue = new Field(fieldElement.Value, dbType);
            if (!Field.SafeDbTypes.Contains(dbType) && string.IsNullOrEmpty(fieldValue.Value))
            {
                fieldValue.Value = null;
            }

            return new KeyValuePair<TFieldId, Field>(fieldId, fieldValue);
        }

        /// <summary>
        /// Creates an instance of the <see cref="LQBData{TFieldId}"/> object, parsing the input xml data into a list of records loaded from LendingQB. Adds Database types to the data response.
        /// </summary>
        /// <param name="xmlToParse">The XML data response from LQB web services, parsed into an XElement. The format must follow an Xpath like:
        /// &lt;LendingQBLoanData>
        ///     &lt;[dataTypeName]>
        ///         &lt;field id=&quot;fieldId&quot; type=&quot;dbType&quot;>
        ///             value
        ///         &lt;/field>
        ///     &lt;/[dataTypeName]>
        /// &lt;/LendingQBLoanData>
        /// </param>
        /// <param name="fieldTypeMapping">The dictionary used for mapping field IDs to their corresponding database types on the local database.</param>
        /// <param name="contentType">The content type being loaded from LendingQB.</param>
        /// <returns>A new <see cref="LQBData{TFieldId}"/> object initialized with the results of parsing <paramref name="xmlToParse"/>.</returns>
        public static LQBData<TFieldId> Parse(XElement xmlToParse, IDictionary<TFieldId, DBTypes> fieldTypeMapping, E_ContentType contentType)
        {
            var records = new List<Dictionary<TFieldId, Field>>();
            foreach (XElement record in xmlToParse.Descendants(XMLDocument.GetRecordLabel(contentType)))
            {
                IEnumerable<XElement> filteredXmlFields = record.Descendants("field").Where(
                    fieldElement =>
                    Tools.SafeParseEnum<TFieldId>(fieldElement.Attribute("id").Value) != null
                    && fieldTypeMapping.ContainsKey(Tools.SafeParseEnum<TFieldId>(fieldElement.Attribute("id").Value).Value));

                Dictionary<TFieldId, Field> filteredFieldDictionary = filteredXmlFields.Select(fieldElement => ToIdFieldPair(fieldElement, fieldTypeMapping)).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);

                records.Add(filteredFieldDictionary);
            }

            return new LQBData<TFieldId>(records);
        }
    }
}