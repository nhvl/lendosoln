﻿namespace SendDataToClientDB
{
    using System;
    using System.Diagnostics;
    using System.Xml.Linq;

    /// <summary>
    /// Logger that will log everything in one big XML file. Benefits of this is that all the logs are located in one place and XML is easy to parse using XPaths.
    /// Natural nesting of XML also lets us get timings for operations in the context of the parent operation.
    /// This logger is an IDisposable so it is best to use it in a using that wraps around whatever operation you're interested in. This lets
    /// the logger stop the timer for that operation no matter what and thus will always give us timing logs.
    /// </summary>
    public class XmlLogger : IDisposable
    {
        /// <summary>
        /// The root element for this logger.
        /// </summary>
        private XElement root;

        /// <summary>
        /// The timer for this logger.
        /// </summary>
        private Stopwatch stopwatch;

        /// <summary>
        /// Whether we want to send this to the Trace logger when it disposes itself.
        /// </summary>
        private bool shouldLogOnDispose;

        /// <summary>
        /// Whether we want to run the timer for this log.
        /// </summary>
        private bool shouldTime;


        /// <summary>
        /// Initializes a new instance of the <see cref="XmlLogger"/> class.
        /// </summary>
        /// <param name="logName">The name of the log.</param>
        /// <param name="shouldTime">Whether the log should be timing.</param>
        /// <param name="shouldLogOnDispose">Whether we should send the log to the Trace logger when it disposes. Should only be set to true for the absolute base log.</param>
        public XmlLogger(string logName, bool shouldTime, bool shouldLogOnDispose = false)
        {
            this.root = new XElement(logName);
            this.shouldLogOnDispose = shouldLogOnDispose;
            this.shouldTime = shouldTime;

            if (this.shouldTime)
            {
                this.stopwatch = new Stopwatch();
                this.stopwatch.Start();
            }
        }

        /// <summary>
        /// Creates another XmlLogger that has its root element as the subelement of this log.
        /// </summary>
        /// <param name="logName">The name of the log.</param>
        /// <param name="shouldTime">If this should time or not.</param>
        /// <returns>The sub XmlLogger.</returns>
        public XmlLogger CreateSubLog(string logName, bool shouldTime)
        {
            var subLog = new XmlLogger(logName, shouldTime);
            this.root.Add(subLog.root);

            return subLog;
        }

        /// <summary>
        /// Creates a simple child element containing the message.
        /// Useful for logging metadata information.
        /// </summary>
        /// <param name="infoKey">The name of the element.</param>
        /// <param name="message">The contents of the element.</param>
        public void LogElement(string infoKey, string message)
        {
            this.root.Add(new XElement(infoKey, message ?? string.Empty));
        }

        /// <summary>
        /// Sets the element as a child element of this logger.
        /// If you want to use a fancy element, use this.
        /// </summary>
        /// <param name="element"></param>
        public void LogElement(XElement element)
        {
            this.root.Add(element);
        }

        /// <summary>
        /// Logs an error log as a child element.
        /// </summary>
        /// <param name="message">The error message.</param>
        public void LogErrorElement(string message)
        {
            this.LogElement("Error", message);
        }

        /// <summary>
        /// Logs an exception log as a child element.
        /// </summary>
        /// <param name="friendlyExceptionMessage">The friendly exception message.</param>
        /// <param name="exceptionType">The exception type.</param>
        public void LogExceptionElement(string friendlyExceptionMessage, Type exceptionType)
        {
            var exceptionElement = new XElement("Exception", friendlyExceptionMessage ?? string.Empty);
            exceptionElement.Add(new XAttribute("Type", exceptionType.FullName));
            this.root.Add(exceptionElement);
        }

        /// <summary>
        /// Sets an attribute to the root element.
        /// </summary>
        /// <param name="attributeKey">The attribute name.</param>
        /// <param name="message">The message.</param>
        public void LogAttribute(string attributeKey, string message)
        {
            this.root.Add(new XAttribute(attributeKey, message ?? string.Empty));
        }

        /// <summary>
        /// Will stop the timer if timing is enabled.
        /// Will log to the Trace logger if this should log on dispose.
        /// </summary>
        public void Dispose()
        {
            if (this.shouldTime)
            {
                this.stopwatch.Stop();
                this.root.Add(new XAttribute("EllapsedMilliseconds", this.stopwatch.ElapsedMilliseconds));
            }

            if (shouldLogOnDispose)
            {
                Trace.Write(this.root.ToString(SaveOptions.DisableFormatting));
            }
        }
    }
}
