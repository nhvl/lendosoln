﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using SendDataToClientDB.ReceiveDataFromLQBClientService;
using System.Xml.Linq;
using System.Linq;

namespace SendDataToClientDB
{
    public static class XMLDocument
    {
        /// <summary>
        /// Creates an XML-formatted string of data records, with element names based on the given content type. 
        /// The product is suitable for sending to the ReceiveDataFromLQB client service.
        /// </summary>
        /// <typeparam name="TFieldId">The field ID type to be used for specifying field ID. Usually string, but may be an enum type like <see cref="SendDataToClientDB.com.lendingqb.Employee.EmployeeFieldId"/>.</typeparam>
        /// <param name="listOfRecords">The list of DB records to write as XML.</param>
        /// <param name="contentType">The content type of the DB records. This is used to determine the xml label for the records.</param>
        /// <returns>An XML-formatted string for the ReceiveDataFromLQB client.</returns>
        public static string ToXMLString<TFieldId>(IEnumerable<Dictionary<TFieldId, Field>> listOfRecords, E_ContentType contentType)
        {
            IEnumerable<XElement> recordElements;
            if (listOfRecords == null)
            {
                throw new ArgumentNullException("listOfRecords cannot be null.");
            }
            if (listOfRecords.Any(record => record == null))
            {
                throw new ArgumentException("listOfRecords may not contain null dictionaries.");
            }

            recordElements = listOfRecords
                .Select(record => new XElement(GetRecordLabel(contentType), record.Select(CreateFieldElement)));

            return new XElement("LendingQBLoanData", new XAttribute("version", AppSettings.VERSION), recordElements).ToString(SaveOptions.DisableFormatting);
        }

        public static List<Dictionary<string, Field>> FromXMLString(string xmlString, E_ContentType contentType)
        {
            return XDocument.Parse(xmlString).Root.Elements(GetRecordLabel(contentType)).Select(CreateRecordObject).ToList();
        }

        /// <summary>
        /// Creates an <see cref="XElement"/> representing the field information in the given pair of field ID and <see cref="Field"/>.
        /// </summary>
        /// <typeparam name="TFieldId">The type of field identifier for identifying DB fields. ToString() will be called on the key for XML serialization. </typeparam>
        /// <param name="field">A pair of <typeparamref name="TFieldId"/> and <see cref="Field"/> representing a data point for transmitting to a database.</param>
        /// <returns>An <see cref="XElement"/> representing the data point, suitable for XML serialization.</returns>
        private static XElement CreateFieldElement<TFieldId>(KeyValuePair<TFieldId, Field> field)
        {
            return new XElement(
                "field",
                new XAttribute("id", field.Key.ToString()),
                new XAttribute("type", field.Value.DBFormat),
                field.Value.Value == null ? (object)new XAttribute("null", "true") : field.Value.Value);
        }

        private static Dictionary<string, Field> CreateRecordObject(XElement recordElement)
        {
            var record = new Dictionary<string, Field>();
            foreach (var fieldElement in recordElement.Elements("field"))
            {
                string fieldValue = fieldElement.AttributeSafeValue("null") == "true" ? null : fieldElement.Value;
                record.Add(
                    fieldElement.Attribute("id").Value,
                    new Field(fieldValue, (DBTypes)Enum.Parse(typeof(DBTypes), fieldElement.AttributeSafeValue("type"))));
            }

            return record;
        }

        /// <summary>
        /// Maps content types to their associated record labels which are to be used in exported XML.
        /// </summary>
        private static readonly Dictionary<E_ContentType, string> recordLabels = new Dictionary<E_ContentType, string>()
        {
            { E_ContentType.Loan, "loan" },
            { E_ContentType.App, "app" },
            { E_ContentType.Task, "task" },
            { E_ContentType.OriginatingCompany, "originatingCompany" },
            { E_ContentType.OriginatingCompanyRelationship, "originatingCompanyRelationship" },
            { E_ContentType.PMLUser, "pmlUser" },
            { E_ContentType.EDoc, "eDoc" },
            { E_ContentType.TaskEDocAssociation, "taskEDocAssociation" },
            { E_ContentType.Employee, "employee" },
            { E_ContentType.Employee2, "employee" },
            { E_ContentType.EDocsAuditHistory, "eDoc" },
            { E_ContentType.EmployeeRoleAssignment, "employeeRole" },
            { E_ContentType.MortgagePool, "pool" }
        };

        /// <summary>
        /// Gets the associated record label for a content type.
        /// </summary>
        /// <exception cref="Tools.UnhandledContentType(E_ContentType)">Thrown if the given value for contentType is not present in <see cref="recordLabels"/>.</exception>
        /// <param name="contentType">The content type.</param>
        /// <returns>The associated xml label for the content type.</returns>
        internal static string GetRecordLabel(E_ContentType contentType)
        {
            string label;
            if (recordLabels.TryGetValue(contentType, out label))
            {
                return label;
            }

            throw Tools.UnhandledContentType(contentType);
        }

        /// <summary>
        /// Provides safe navigation to attribute values.
        /// </summary>
        /// <param name="element">The element containing the attribute.</param>
        /// <param name="name">The <seealso cref="XName"/> of the attribute to get.</param>
        /// <returns>The value of the attribute that has the specified <seealso cref="XName"/>; null if there is no attribute with the specified name.</returns>
        /// <remarks>This is just a fill for <code>element.Attribute(name)?.Value</code>.  It should be replaced once the newer language syntax is supported.</remarks>
        public static string AttributeSafeValue(this XElement element, XName name)
        {
            var attribute = element.Attribute(name);
            if (attribute != null)
            {
                return attribute.Value;
            }

            return null;
        }
    }
}
