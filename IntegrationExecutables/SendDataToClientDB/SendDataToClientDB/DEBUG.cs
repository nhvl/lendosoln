﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;

namespace SendDataToClientDB
{
    public static class DEBUG
    {
        public static void WriteLoanToConsole(List<Dictionary<string, Field>> loans, List<Dictionary<string, Field>> apps)
        {
            TextWriter tOut = Console.Out;
            tOut.WriteLine(loans.Count + " " + apps.Count);
            for (int i = 0; i < 1; i++)
            {
                Dictionary<string, Field> loan = (Dictionary<string, Field>)loans[i];
                tOut.WriteLine("{" + string.Join(",", loan.Select(kv => kv.Key.ToString() + "=" + ((string.IsNullOrEmpty(kv.Value.Value)) ? "" : kv.Value.Value)).ToArray()) + "}");
                Dictionary<string, Field> app = (Dictionary<string, Field>)apps[i];
                tOut.WriteLine("{" + string.Join(",", app.Select(kv => kv.Key.ToString() + "=" + ((string.IsNullOrEmpty(kv.Value.Value)) ? "" : kv.Value.Value)).ToArray()) + "}");
            }
        }

        public static void WriteToFile(string sFilePath, string sText)
        {
            using (FileStream fs = new FileStream(sFilePath, FileMode.Create, FileAccess.ReadWrite, FileShare.Read))
            {
                byte[] btData = System.Text.Encoding.UTF8.GetBytes(sText);
                fs.Write(btData, 0, btData.Length);
            }
        }

        public static DebugInfo Info = new DebugInfo();

        public class DebugInfo
        {
            private StringBuilder log = new StringBuilder();

            public void LogLender(string lenderName)
            {
                log.AppendLine("Lender: " + lenderName + ", " + DateTime.Now);
            }

            public void LogLoans(int loanCount, int taskCount)
            {
                log.AppendLine("Sending Batch: " + loanCount + " loans, " + taskCount + " tasks at " + DateTime.Now);
            }

            public void LogTransmissionCompleted(TimeSpan elapsedTime)
            {
                log.AppendLine("  Completed in " + elapsedTime);
            }

            public void LogTransmissionGiveUp(TimeSpan elapsedTime)
            {
                log.AppendLine("  Throwing exception after " + elapsedTime);
            }

            private string previousResult;

            public void LogRetryPreSleep(string result)
            {
                if (previousResult == result)
                {
                    result = "Same as previous result"; // avoid logging a long exception more than we need
                }
                else
                {
                    previousResult = result;
                }

                log.AppendLine("  Sleeping at " + DateTime.Now + "; will retry since result was: " + result);
            }

            public void LogRetryingNow()
            {
                log.AppendLine("  Retrying at " + DateTime.Now);
            }

            public void LogData(int recordCount, SendDataToClientDB.ReceiveDataFromLQBClientService.E_ContentType contentType)
            {
                log.AppendLine("Sending " + recordCount + " " + contentType + " records at " + DateTime.Now);
            }

            public override string ToString()
            {
                return log.ToString() + "End Time: " + DateTime.Now;
            }
        }
    }
}
