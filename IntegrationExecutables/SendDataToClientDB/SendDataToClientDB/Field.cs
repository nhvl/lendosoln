﻿using System;

namespace SendDataToClientDB
{
    // The S in SDBx stands for safe/not null
    public enum DBTypes
    {
        DBbigint,
        DBbit,
        DBchar,
        DBdatetime,
        DBdecimal,
        DBint,
        DBmoney,
        DBsmalldatetime,
        DBtext,
        DBtinyint,
        DBuniqueidentifier,
        DBvarchar,
        SDBbigint,
        SDBbit,
        SDBchar,
        SDBdatetime,
        SDBdecimal,
        SDBint,
        SDBmoney,
        SDBsmalldatetime,
        SDBtext,
        SDBtinyint,
        SDBuniqueidentifier,
        SDBvarchar
    }
    
    public class Field
    {
        #region Variables
        private string m_sValue;
        private DBTypes m_eDBFormat;

        public string Value
        {
            get { return m_sValue; }
            set { m_sValue = value; }
        }

        public string DBFormat
        {
            get { return m_eDBFormat.ToString("G"); }
        }

        public DBTypes DBFormatEnum
        {
            get { return m_eDBFormat; }
            set { m_eDBFormat = value; }
        }
        #endregion

        public static DBTypes[] SafeDbTypes = new[]
        {
            DBTypes.SDBbigint,
            DBTypes.SDBbit,
            DBTypes.SDBchar,
            DBTypes.SDBdatetime,
            DBTypes.SDBdecimal,
            DBTypes.SDBint,
            DBTypes.SDBmoney,
            DBTypes.SDBsmalldatetime,
            DBTypes.SDBtext,
            DBTypes.SDBtinyint,
            DBTypes.SDBuniqueidentifier,
            DBTypes.SDBvarchar
        };

        public Field(string sValue, DBTypes eSQLFormat)
        {
            m_sValue = sValue;
            m_eDBFormat = eSQLFormat;
        }

        public override string ToString()
        {
            return string.Format(
                "Field{{Value: {0}, DBFormat: {1}}}",
                Stringify(this.Value),
                this.DBFormat);
        }

        private static string Stringify(string input)
        {
            if (input == null)
            {
                return "null";
            }
            else if (input.Contains("\""))
            {
                return "@\"" + input.Replace("\"", "\"\"") + "\"";
            }

            return "\"" + input + "\"";
        }
    }
}
