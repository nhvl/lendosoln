﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

namespace SendDataToClientDB
{
    public static class AppSettings
    {
        public static string DEBUG
        {
            get { return ConfigurationManager.AppSettings["DEBUG"]; }
        }

        public static string DEBUG_OUTPUT_FILE_PATH
        {
            get { return ConfigurationManager.AppSettings["DEBUG_OUTPUT_FPATH"]; }
        }

        private static Dictionary<string, string> connectionStringByCustomerCode;

        public static string DSN_ByCustomerCode(string customerCode)
        {
            if (connectionStringByCustomerCode == null)
            {
                connectionStringByCustomerCode = LoadAllConnectionStringsByCustomerCode();
            }

            string connectionString;
            if (connectionStringByCustomerCode.TryGetValue(customerCode, out connectionString))
            {
                return connectionString;
            }
            else
            {
                throw new Exception("Unable to load DSN for CustomerCode \"" + customerCode + "\"");
            }
        }

        private static Dictionary<string, string> LoadAllConnectionStringsByCustomerCode()
        {
            var customerCodeDictionary = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
            int consecutiveFailureCount = 0;
            for (int i = 0; consecutiveFailureCount < 3; ++i)
            {
                string lenderString = DSN_Lenders(i);
                if (lenderString == null)
                {
                    ++consecutiveFailureCount;
                    continue;
                }

                consecutiveFailureCount = 0; 
                var lenders = new HashSet<string>(
                    lenderString.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(str => str.Trim())
                    .Where(str => !string.IsNullOrEmpty(str)));
                if (!lenders.Any())
                {
                    continue;
                }

                string connectionString = DSN(i);
                if (connectionString == null)
                {
                    throw new Exception("Unable to load connection string for index=[" + i.ToString("00") + "], but it had one or lenders configured"); 
                }

                foreach (var customerCode in lenders)
                {
                    customerCodeDictionary.Add(customerCode, connectionString);
                }
            }

            return customerCodeDictionary;
        }

        private static string DSN(int index)
        {
            return ConfigurationManager.AppSettings["DSN_" + index.ToString("00")];
        }

        private static string DSN_Lenders(int index)
        {
            return ConfigurationManager.AppSettings["DSN_" + index.ToString("00") + "_LENDERS"];
        }

        public static string SUPPORT_EMAIL
        {
            get { return ConfigurationManager.AppSettings["SUPPORT_EMAIL"] ?? "support@lendingqb.com"; }
        }

        public static string[] RECEIVERS
        {
            get { return ConfigurationManager.AppSettings["RECEIVERS"].Split(';'); }
        }

        public static string RECEIVER_EMAIL_ADDRESS(string receiver)
        {
            return RetrieveReceiverSetting(receiver, "EMAIL", string.Empty);
        }

        private static ReceiveDataFromLQBClientService.Client clientService;
        private static string clientServiceCustomerCode;
        public static ReceiveDataFromLQBClientService.Client ClientService(string receiver)
        {
            if (clientService == null || !string.Equals(receiver, clientServiceCustomerCode, StringComparison.OrdinalIgnoreCase))
            {
                clientService = new SendDataToClientDB.ReceiveDataFromLQBClientService.Client();
                clientService.Timeout = Tools.TimeoutLength;
                clientService.Url = RECEIVER_SERVICE_URL(receiver);
                clientServiceCustomerCode = receiver;
            }

            return clientService;
        }

        public static string RECEIVER_SERVICE_URL(string receiver)
        {
            return RetrieveReceiverSetting(receiver,"URL", string.Empty);
        }

        public static string RECEIVER_USERNAME(string receiver)
        {
            return RetrieveReceiverSetting(receiver, "USERNAME", string.Empty);
        }

        public static string RECEIVER_PASSWORD(string receiver)
        {
            return RetrieveReceiverSetting(receiver, "PASSWORD", string.Empty);
        }

        public static X509Certificate2 RECEIVER_CERTIFICATE(string receiver)
        {
            string certPath = RetrieveReceiverSetting(receiver, "CERTIFICATE_PATH", string.Empty);
            string certPassword = RetrieveReceiverSetting(receiver, "CERTIFICATE_PASSWORD", string.Empty);

            X509Certificate2 certificate = null;
            if (!string.IsNullOrEmpty(certPath) && !string.IsNullOrEmpty(certPassword))
            {
                certificate = new X509Certificate2(certPath, certPassword);
            }

            return certificate;
        }

        public static Guid RECEIVER_APP_CODE(string receiver)
        {
            return new Guid(RetrieveReceiverSetting(receiver, "APP_CODE", null));
        }

        public static string RECEIVER_LENDER(string receiver)
        {
            // Use ConfigurationManager here unlike the other receiver prefix settings because RetrieveReceiverSetting uses this in its fallback.
            return ConfigurationManager.AppSettings[receiver + "_LENDER"];
        }

        public static string RECEIVER_CUSTOMER_CODE(string receiver)
        {
            return RECEIVER_LENDER(receiver) ?? receiver;
        }

        private static string RetrieveReceiverSetting(string receiverName, string settingName, string defaultValue)
        {
            return ConfigurationManager.AppSettings[receiverName + "_" + settingName] ?? ConfigurationManager.AppSettings[RECEIVER_LENDER(receiverName) + "_" + settingName] ?? defaultValue;
        }

        private static string RetrieveSetting(string sKey, string defaultValue)
        {
            return ConfigurationManager.AppSettings[sKey] ?? defaultValue;
        }

        public static string RECEIVER_LOAN_EXPORT_FORMAT(string receiver)
        {
            return RetrieveReceiverSetting(receiver, "LOAN_EXPORT_FORMAT", AppSettings.DEFAULT_LOAN_EXPORT_FORMAT);
        }

        public static string RECEIVER_APP_EXPORT_FORMAT(string receiver)
        {
            return RetrieveReceiverSetting(receiver, "APP_EXPORT_FORMAT", AppSettings.DEFAULT_APP_EXPORT_FORMAT);
        }

        private static string GENERAL_SECRET_KEY
        {
            get { return ConfigurationManager.AppSettings["SECRETKEY"]; }
        }
        internal static string SECRET_KEY(string receiver)
        {
            return RetrieveReceiverSetting(receiver, "SECRETKEY", GENERAL_SECRET_KEY);
        }

        public static string SMTP_SERVER
        {
            get { return ConfigurationManager.AppSettings["SMTP_SERVER"]; }
        }

        public static string VERSION
        {
            get { return ConfigurationManager.AppSettings["PROJECTVERSION"]; }
        }

        public static string LOXML_VERSION
        {
            get { return ConfigurationManager.AppSettings["LOXML_VERSION"]; }
        }

        public static string DEFAULT_LOAN_EXPORT_FORMAT
        {
            get { return ConfigurationManager.AppSettings["DEFAULT_LOAN_EXPORT_FORMAT"]; }
        }

        private static string DEFAULT_APP_EXPORT_FORMAT
        {
            get { return ConfigurationManager.AppSettings["DEFAULT_APP_EXPORT_FORMAT"]; }
        }

        public static string BATCH_EXPORT_KEY
        {
            get { return ConfigurationManager.AppSettings["BATCH_EXPORT_KEY"]; }
        }

        public static string WEBSERVICE_DOMAIN
        {
            get { return ConfigurationManager.AppSettings["WEBSERVICE_DOMAIN"]; }
        }

        public static string ServiceMonitorUrl
        {
            get { return ConfigurationManager.AppSettings["ServiceMonitorUrl"]; }
        }
    }
}
