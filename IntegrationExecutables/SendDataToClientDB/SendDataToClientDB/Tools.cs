﻿using System;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using SendDataToClientDB.ReceiveDataFromLQBClientService;
using System.Text;
using System.Net.Mail;

namespace SendDataToClientDB
{
    public static class Tools
    {

        private static string[] receiversWithoutOriginatingCompanyRelationshipData = new[]
        {
            ////"PML0158", // Mid-Island
            ////"PML0214", // WHEDA
            "PML0223", // First Tech
            "PML0226", // NFLP
            ////"PML0229", // JMAC
            "PML0246", // Lenox
            "PML0263", // MWF
            ////"PML0270", // MMI
            ////"PML0277", // Inlanta
            "MTGPEER",
            "MTGPEER_TEST",
        };

        public static void SetupServicePointCallback()
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            ServicePointManager.ServerCertificateValidationCallback = delegate(object sender, X509Certificate certificate, X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
            {
                if (sslPolicyErrors == System.Net.Security.SslPolicyErrors.None)
                {
                    return true; //If there are no errors then the certificate is valid
                }
                else if (sender is HttpWebRequest)
                {
                    return true; //All URL's used by the application are included in the config file, so approve
                }

                return false;
            };
        }

        /// <summary>
        /// Timeout value for sending data, in milliseconds. (Currently 5 min.)
        /// </summary>
        public const int TimeoutLength = ((TimeoutMinutes * 60) + TimeoutAdditionalSeconds) * 1000;
        const int TimeoutMinutes = 5;
        const int TimeoutAdditionalSeconds = 0;

        public static IEnumerable<E_ContentType> PMLContentTypesToSend(string receiver, ExecutionOptions options)
        {
            // Current last run is 5:50 PM; we target all transmissions after
            if ((!options.DisablePmlTimespan && DateTime.Now.TimeOfDay < new TimeSpan(17, 50, 0))
                || !options.SendingTypes.Contains(TransmissionType.PML))
            {
                return Enumerable.Empty<E_ContentType>();
            }

            if (receiversWithoutOriginatingCompanyRelationshipData.Contains(receiver, StringComparer.OrdinalIgnoreCase))
            {
                return new[] { E_ContentType.OriginatingCompany, E_ContentType.PMLUser };
            }

            // Send the core data before the relationships to make sure there are no foreign key issues
            return new[] { E_ContentType.OriginatingCompany, E_ContentType.OriginatingCompanyRelationship, E_ContentType.PMLUser };
        }

        /// <summary>
        /// Returns an indicator of whether the receiver supports deletion by primary key.
        /// </summary>
        /// <remarks>
        /// Needed since we'll be phasing <seealso cref="E_ContentType.OriginatingCompanyRelationship"/> into use,
        /// which requires this deletion ability.  In the long run, we'll remove this flag and just always have
        /// the ability to delete records by primary key.
        /// </remarks>
        internal static bool EnableDeletionByPrimaryKey(string receiver)
        {
            return !receiversWithoutOriginatingCompanyRelationshipData.Contains(receiver, StringComparer.OrdinalIgnoreCase);
        }

        public static Exception UnhandledContentType(E_ContentType contentType)
        {
            return new InvalidOperationException("Unhandled enum value=E_ContentType." + contentType.ToString());
        }

        internal static bool ShouldSendMortgagePoolData(string receiverName, ExecutionOptions options)
        {
            E_ContentType[] contentTypes;
            return options.SendingTypes.Contains(TransmissionType.MortgagePool)
                && ExtraOtherContent.TryGetValue(receiverName, out contentTypes) && contentTypes.Contains(E_ContentType.MortgagePool);
        }

        public static bool ShouldSendEmployeeData(string receiverName, ExecutionOptions options)
        {
            E_ContentType[] contentTypes;
            return options.SendingTypes.Contains(TransmissionType.Employee) 
                && ExtraOtherContent.TryGetValue(receiverName, out contentTypes) && contentTypes.Contains(E_ContentType.Employee);
        }

        public static bool ShouldSendEdocsData(string receiverName, ExecutionOptions options)
        {
            E_ContentType[] contentTypes;
            return options.SendingTypes.Contains(TransmissionType.EDocs)
                && ExtraOtherContent.TryGetValue(receiverName, out contentTypes) && contentTypes.Contains(E_ContentType.EDoc);
        }

        public static bool ShouldSendEmployeeRoleAssignmentData(string receiverName)
        {
            E_ContentType[] contentTypes;
            return ExtraOtherContent.TryGetValue(receiverName, out contentTypes) && contentTypes.Contains(E_ContentType.EmployeeRoleAssignment);
        }

        public static bool IsLenderLogging(string receiver)
        {
            return ReceiversWithLogging.Contains(receiver, StringComparer.OrdinalIgnoreCase);
        }

        private static string[] ReceiversWithLogging =
        {
            "PML0214", // WHEDA
            "PML0223", // First Tech
            "PML0270"  // Michigan Mutual (MMI)
        };

        /// <summary>
        /// Currently all except Lenox, who will have the data in <seealso cref="ExtraLoanContent"/>.
        /// </summary>
        public static bool IsSendTaskData(string receiver)
        {
            ////return true;
            return !"PML0246".Equals(receiver, StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Currently<para/>
        /// JMAC: EDoc<para/>
        /// MWF: EDoc<para/>
        /// Lenox: Task
        /// </summary>
        public static IEnumerable<E_ContentType> ExtraLoanContent(string receiver)
        {
            if ("PML0229".Equals(receiver, StringComparison.OrdinalIgnoreCase))
            {
                return new E_ContentType[] { E_ContentType.EDoc };
            }
            else if ("PML0263".Equals(receiver, StringComparison.OrdinalIgnoreCase))
            {
                return new E_ContentType[] { E_ContentType.EDoc };
            }
            else if ("PML0246".Equals(receiver, StringComparison.OrdinalIgnoreCase))
            {
                return new E_ContentType[] { E_ContentType.Task };
            }
            else if ("PML0299".Equals(receiver, StringComparison.OrdinalIgnoreCase))
            {
                return new[] { E_ContentType.TaskEDocAssociation };
            }
            else if (receiver.Equals("THINHTEST2", StringComparison.OrdinalIgnoreCase) || receiver.Equals("THINHTEST2_RECEIVER2", StringComparison.OrdinalIgnoreCase))
            {
                return new E_ContentType[] { E_ContentType.TaskEDocAssociation};
            }

            return new E_ContentType[0];
        }

        /// <summary>
        /// Maps a receiver name (Key, string) to a list of Extra Other Content types that should be sent for the user.
        ///     Valid content types are <see cref="E_ContentType.EDoc"/> and <see cref="E_ContentType.Employee"/>. Others are not currently checked (09/2016).
        ///     Could be changed to a ReadOnlyDictionary when we switch to .NET 4.5+
        /// Lender List:
        /// Mid-Island: EDoc
        /// WHEDA: EDoc
        /// Lenox: Employee
        /// Parkside: EDoc
        /// </summary>
        public static IDictionary<string, E_ContentType[]> ExtraOtherContent = new Dictionary<string, E_ContentType[]>()
        {
            { "PML0158", new E_ContentType[] { E_ContentType.EDoc } },
            { "PML0214", new E_ContentType[] { E_ContentType.EDoc, E_ContentType.MortgagePool } },
            { "PML0229", new E_ContentType[] { } },
            { "PML0246", new E_ContentType[] { E_ContentType.Employee } },
            { "PML0263", new E_ContentType[] { } },
            { "PML0270", new E_ContentType[] { E_ContentType.MortgagePool } },
            { "PML0299", new[] { E_ContentType.EDoc, E_ContentType.Employee, E_ContentType.EmployeeRoleAssignment } },
            { "THINHTEST2", new E_ContentType[] { E_ContentType.EDoc, E_ContentType.Employee, E_ContentType.EmployeeRoleAssignment, E_ContentType.MortgagePool } }
        };

        /// <summary>
        /// Loads a byte array in UTF-8 Encoding. If the string begins with<para />
        /// the UTF-8 byte order mark, it is removed.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string GetUTF8String(byte[] data)
        {
            byte[] utf8Preamble = Encoding.UTF8.GetPreamble();
            if (data.StartsWith(utf8Preamble))
            {
                return Encoding.UTF8.GetString(data, utf8Preamble.Length, data.Length - utf8Preamble.Length);
            }
            else
            {
                return Encoding.UTF8.GetString(data);
            }
        }

        /// <summary>
        /// Determines whether the beginning of this array matches the specified array.
        /// </summary>
        /// <param name="value">The array to compare.</param>
        /// <returns><see langword="true"/> if value matches the beginning of this string; otherwise, <see langword="false"/>.</returns>
        /// <exception cref="System.ArgumentNullException"><param name="thisArray"/> or <param name="array"/> is null.</exception>
        /// <exception cref="System.InvalidOperationException"><param name="otherArray"/> is longer than <paramref name="thisArray"/>.</exception>
        /// <remarks>This method could easily be made generic with IEquatable&lt;T&gt; as the only requirement, but as 
        /// this has only one use for now, that would be inefficient.</remarks>
        public static bool StartsWith(this byte[] thisArray, byte[] otherArray)
        {
            if (thisArray == null || otherArray == null)
            {
                throw new ArgumentNullException(thisArray == null ? "thisArray" : "otherArray");
            }

            if (thisArray.Length < otherArray.Length)
            {
                throw new InvalidOperationException("otherArray is longer than thisArray");
            }

            for (int i = 0; i < otherArray.Length; ++i)
            {
                if (thisArray[i] != otherArray[i])
                {
                    return false;
                }
            }

            return true;
        }

        public static bool IsDataDifferent(string csvFilePath, List<Dictionary<string, Field>> dataBook)
        {
            string csvTempFilePath = csvFilePath.Replace(".csv","_temp.csv");
            WriteToCSV(csvTempFilePath, dataBook);
            if (!File.Exists(csvFilePath))
            {
                File.Move(csvTempFilePath, csvFilePath);
                return true;
            }

            bool isDifferent = IsDataDifferent(csvFilePath, csvTempFilePath);
            if (isDifferent)
            {
                File.Delete(csvFilePath);
                File.Move(csvTempFilePath, csvFilePath);
            }
            else
            {
                File.Delete(csvTempFilePath);
            }

            return isDifferent;
        }

        private static bool IsDataDifferent(string file1Path, string file2Path)
        {
            if (file1Path == file2Path) return false;

            int file1Byte;
            int file2Byte;
            using (FileStream fs1 = new FileStream(file1Path, FileMode.Open, FileAccess.Read))
            using (FileStream fs2 = new FileStream(file2Path, FileMode.Open, FileAccess.Read))
            {
                if (fs1.Length != fs2.Length)
                    return true;

                do
                {
                    file1Byte = fs1.ReadByte();
                    file2Byte = fs2.ReadByte();
                } while (file1Byte == file2Byte && file1Byte != -1);
            }
            return file1Byte != file2Byte;
        }

        private static void WriteToCSV(string csvFilePath, List<Dictionary<string, Field>> dataBook)
        {
            if (dataBook.Count == 0)
            {
                File.WriteAllText(csvFilePath, "");
                return;
            }
            IEnumerable<string> fieldNames = dataBook.First().Keys;
            Field val;
            using (var writer = new StreamWriter(csvFilePath))
            {
                foreach (var data in dataBook)
                {
                    foreach (var fieldName in fieldNames)
                    {
                        if (data.TryGetValue(fieldName, out val))
                            writer.Write(val.Value + ',');
                    }
                    writer.WriteLine();
                }
            }
        }

        public static TEnum? SafeParseEnum<TEnum>(string value) where TEnum : struct
        {
            try
            {
                return (TEnum?)Enum.Parse(typeof(TEnum), value);
            }
            catch (ArgumentException)
            {
                return null;
            }
            catch (OverflowException)
            {
                return null;
            }
        }

        public static string Quantify(int count, string itemName, string itemNames)
        {
            return count + " " + (count == 1 || count == -1 ? itemName : itemNames);
        }
    }
}
