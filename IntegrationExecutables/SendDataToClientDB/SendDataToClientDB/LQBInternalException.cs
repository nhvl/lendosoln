﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SendDataToClientDB
{
    /// <summary>
    /// A wrapper Exception designed to prevent sharing internal LendingQB information.
    /// </summary>
    public class LQBInternalException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LQBInternalException"/> class.
        /// </summary>
        /// <param name="internalException">The exception from LendingQB that may contain sensitive information.</param>
        public LQBInternalException(Exception internalException)
            : base("Internal error occurred while retrieving values from LendingQB.")
        {
            this.InternalException = internalException;
        }

        /// <summary>
        /// Gets an exception intended to be private to LendingQB.  This value is not a part of <see cref="ToString()"/>.
        /// </summary>
        public Exception InternalException { get; private set; }

        /// <summary>
        /// Gets a message intended to be private to LendingQB.  This value is not a part of <see cref="ToString()"/>.
        /// </summary>
        public string InternalMessage
        {
            get
            {
                LQBInternalException asInternal = this.InternalException as LQBInternalException;
                return this.InternalException.ToString()
                    + (asInternal == null ? null : Environment.NewLine + Environment.NewLine + asInternal.InternalMessage);

            }
        }
    }
}
