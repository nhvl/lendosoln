﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Xml.Linq;
using SendDataToClientDB.com.lendingqb.Employee;
using SendDataToClientDB.LoadData.WebServices;
using SendDataToClientDB.ReceiveDataFromLQBClientService;

namespace SendDataToClientDB
{
    class Program
    {
        static void Main(string[] args)
        {
            Logger globalLevelLogger = null;
            ServiceMonitorClient.Args = args;
            try
            {
                using (var globalXmlLogger = new XmlLogger("SendDataToClientDB", shouldTime: true, shouldLogOnDispose: true))
                {
                    globalLevelLogger = Logger.CreateAndStartRunLogger(args);
                    globalXmlLogger.LogAttribute("GlobalId", globalLevelLogger.LogId);

                    var exeOptions = new ExecutionOptions(args);
                    if (exeOptions.InfoOnlyMode)
                    {
                        // Help queries should be fast and without side effects.
                        return;
                    }
                    globalLevelLogger.LogInfo("Enabled sending types for this run: " + string.Join(", ", exeOptions.SendingTypes.Select(contentType => contentType.ToString()).ToArray()), "Start Run");

                    Tools.SetupServicePointCallback();
                    ////Post to Service Monitor Framework here, after asserting TLS 1.2.
                    ServiceMonitorClient.TryPostStatus(ServiceMonitorStatus.STARTED, "Global log ID: " + globalLevelLogger.LogId);
                    foreach (string receiverName in exeOptions.ReceiverNames)
                    {
                        var receiverLogger = globalLevelLogger.CreateReceiverLogger(receiverName);
                        using (var receiverXmlLogger = globalXmlLogger.CreateSubLog("Receiver", shouldTime: true))
                        {
                            receiverXmlLogger.LogAttribute("ReceiverName", receiverName);
                            string receiverCustomerCode = AppSettings.RECEIVER_CUSTOMER_CODE(receiverName);
                            try
                            {
                                var database = new DB(AppSettings.DSN_ByCustomerCode(receiverCustomerCode));
                                Guid sBrokerID = database.GetBrokerID(receiverCustomerCode);
                                if (sBrokerID == Guid.Empty || string.IsNullOrEmpty(AppSettings.RECEIVER_SERVICE_URL(receiverName)))
                                {
                                    string brokerNotFoundMessage = "Unable to find the broker id or receiver service URL; CustomerCode was \"" + receiverCustomerCode + "\"";
                                    ServiceMonitorClient.TryPostStatus(ServiceMonitorStatus.FAILED, brokerNotFoundMessage);
                                    receiverLogger.LogEnd(brokerNotFoundMessage);
                                    receiverXmlLogger.LogErrorElement(brokerNotFoundMessage);
                                    continue;
                                }

                                if (exeOptions.SendingTypes.Contains(TransmissionType.Loan))
                                {
                                    ProcessModifiedLoans(sBrokerID, database, receiverName, exeOptions, receiverLogger, receiverXmlLogger);
                                }
                                ProcessPMLOriginatingCompanyAndUserData(sBrokerID, database, receiverName, exeOptions, receiverLogger, receiverXmlLogger);
                                if (Tools.ShouldSendEmployeeData(receiverName, exeOptions))
                                {
                                    ProcessEmployeeData(receiverName, receiverLogger, receiverXmlLogger);
                                }
                                if ((Tools.PMLContentTypesToSend(receiverName, exeOptions).Contains(E_ContentType.PMLUser) || Tools.ShouldSendEmployeeData(receiverName, exeOptions)) && Tools.ShouldSendEmployeeRoleAssignmentData(receiverName))
                                {
                                    ProcessEmployeeRoleAssignmentData(database, sBrokerID, receiverName, receiverLogger, receiverXmlLogger);
                                }
                                if (Tools.ShouldSendEdocsData(receiverName, exeOptions))
                                {
                                    ProcessEdocsData(receiverName, receiverLogger, receiverXmlLogger);
                                }
                                if (Tools.ShouldSendMortgagePoolData(receiverName, exeOptions))
                                {
                                    ProcessMortgagePoolData(sBrokerID, database, receiverName, receiverLogger, receiverXmlLogger);
                                }
                            }
                            catch (Exception ex)
                            {
                                receiverXmlLogger.LogExceptionElement(ex.Message, ex.GetType());
                                string internalMessage = ex is LQBInternalException ? ((LQBInternalException)ex).InternalMessage : null;
                                SendFailureMessage(
                                    clientMessage: ex.ToString(),
                                    internalMessage: internalMessage,
                                    receiverName: receiverName,
                                    customerCode: receiverCustomerCode);
                                receiverLogger.LogError(ex);
                                receiverLogger.LogEnd(internalMessage ?? (ex.GetType() + ": " + ex.Message));
                            }
                        }
                    }

                if (ServiceMonitorClient.LastKnownStatus != ServiceMonitorStatus.FAILED)
                {
                    ServiceMonitorClient.TryPostStatus(ServiceMonitorStatus.OK, "Global log ID: "+ globalLevelLogger.LogId);
                }
                    globalLevelLogger.LogEnd(null);
                }
            }
            catch (Exception exc)
            {
                SendFailureMessage(exc.ToString());
                if (globalLevelLogger != null)
                {
                    globalLevelLogger.LogError(exc);
                    globalLevelLogger.LogEnd(exc.ToString());
                }
            }
        }

        private static void ProcessModifiedLoans(Guid sBrokerID, DB database, string receiver, ExecutionOptions options, Logger logger, XmlLogger xmlLogger)
        {
            using (var processModifiedLoansLogger = xmlLogger.CreateSubLog("ProcessModifiedLoans", shouldTime: true))
            {
                Guid receiverAppCode = AppSettings.RECEIVER_APP_CODE(receiver);
                List<Dictionary<string, Field>> additionalLoansLoadedFromDB = null;
                List<Dictionary<string, Field>> loanBatch = database.GetBatchOfModifiedLoans(ref additionalLoansLoadedFromDB, sBrokerID, receiver, receiverAppCode);
                List<Dictionary<string, Field>> tasks = new List<Dictionary<string, Field>>();
                BatchExportService exporter = new BatchExportService(receiver);
                DateTime dtLastMod = DateTime.MinValue;
                int iFailToClearTracker = 0;
                bool bChurning = false;
                bool bLastRoundSucceeded = true;

                if (Tools.IsLenderLogging(receiver) && loanBatch.Count == 0)
                {
                    using (var populateLoggingTableLogger = processModifiedLoansLogger.CreateSubLog("PopulateLoggingTable", shouldTime: true))
                    {
                        SendLoans(null, null, tasks, 0, receiver, logger, populateLoggingTableLogger); // populates their logging table with a 0 loan count record
                    }
                }

                int loanBatchNumber = 0;
                int totalProcessedLoanCount = 0;
                while (loanBatch.Count > 0 && !bChurning && bLastRoundSucceeded)
                {
                    loanBatchNumber += 1;
                    totalProcessedLoanCount += loanBatch.Count;
                    List<Guid> loanIDs = new List<Guid>(loanBatch.Count);
                    foreach (Dictionary<string, Field> loan in loanBatch)
                    {
                        Guid sLId = new Guid(loan["sLId"].Value);
                        loanIDs.Add(sLId);

                        DateTime dtLoanLastMod = Convert.ToDateTime(loan["LastModifiedD"].Value);
                        if (dtLastMod.CompareTo(dtLoanLastMod) < 0)
                        {
                            dtLastMod = dtLoanLastMod;
                        }

                        if (Tools.IsSendTaskData(receiver))
                        {
                            tasks.AddRange(database.GetLoanTasks(sBrokerID, sLId, receiver));
                        }
                    }

                    logger.CreateAndStartLoanBatchLogger(loanBatchNumber, receiverAppCode, loanIDs, tasks, dtLastMod);
                    using (var processLoanBatchLogger = processModifiedLoansLogger.CreateSubLog($"ProcessLoanBatch", shouldTime: true))
                    {
                        processLoanBatchLogger.LogAttribute("BatchNumber", loanBatchNumber.ToString());
                        processLoanBatchLogger.LogElement("LoanCount", loanBatch.Count.ToString());
                        processLoanBatchLogger.LogElement("TaskCount", tasks.Count.ToString());

                        string loxmlIDList = LOXML.CreateLOXMLListOfLoanIDs(loanIDs);
                        string loanBatchExport = exporter.ExportLoanData(loxmlIDList, processLoanBatchLogger);
                        string appBatchExport = exporter.ExportAppData(loxmlIDList, processLoanBatchLogger);

                        bool bSuccess;
                        using (var sendLoanBatchLogger = processLoanBatchLogger.CreateSubLog("SendLoanBatch", shouldTime: true))
                        {
                            bSuccess = SendLoans(loanBatchExport, appBatchExport, tasks, loanBatch.Count, receiver, logger, sendLoanBatchLogger);
                        }

                        foreach (E_ContentType contentType in Tools.ExtraLoanContent(receiver).TakeWhile(contentType => bSuccess))
                        {
                            if (contentType == E_ContentType.EDoc && !options.SendingTypes.Contains(TransmissionType.EDocs))
                            {
                                continue;
                            }

                            List<Dictionary<string, Field>> contentForAllLoans = new List<Dictionary<string, Field>>();
                            foreach (Guid loanId in loanIDs)
                            {
                                contentForAllLoans.AddRange(database.GetExtraLoanData(loanId, receiver, sBrokerID, contentType));
                            }

                            bSuccess = SendData(contentForAllLoans, receiver, contentType, logger, processLoanBatchLogger);
                        }

                        if (bSuccess)
                        {
                            if (additionalLoansLoadedFromDB.Count == 0)
                            {
                                // Only clear when we've passed through all the batches of loaded loans; otherwise, we risk clearing loans that have not yet been sent.
                                iFailToClearTracker = ClearProcessedLoansWithFailureCount(sBrokerID, dtLastMod, database, iFailToClearTracker, receiverAppCode);
                                bChurning = JustChurning(iFailToClearTracker);
                            }

                            if (!bChurning)
                            {
                                loanBatch = database.GetBatchOfModifiedLoans(ref additionalLoansLoadedFromDB, sBrokerID, receiver, receiverAppCode);
                                tasks.Clear();
                            }
                        }

                        bLastRoundSucceeded = bSuccess;
                    }
                }

                logger.LogInfo(
                    "Finished transmitting " + Tools.Quantify(loanBatchNumber, "batch", "batches") + " for a total of "
                    + Tools.Quantify(totalProcessedLoanCount, "loan", "loans") + " processed.  Final transmission successful? " + bLastRoundSucceeded,
                    "End LoanTransmission");
            }
        }

        /// <summary>
        /// Loads employee data from LQB web services.
        /// Some processing is done to the data to add the database types to the transmission, which is handled by the <see cref="LQBData{TFieldId}"/> class.
        /// Then, the data is sent through to the ReceiveDataFromLQB Client.asmx on the receiver side using <see cref="SendData(List{Dictionary{string, Field}}, string, E_ContentType)"/>.
        /// This process is done in 2 parts depending on the LQB database table where the data points will reside.
        /// </summary>
        /// <param name="receiverName">The name of the receiver of the data.</param>
        /// <param name="receiverLogger">The parent logging object for recording to logs.</param>
        private static int ProcessEmployeeData(string receiverName, Logger receiverLogger, XmlLogger xmlLogger)
        {
            using (var processEmployeeDataLogger = xmlLogger.CreateSubLog("ProcessEmployeeData", shouldTime: true))
            {
                EmployeeService employeeWebService = new EmployeeService(receiverName);
                DateTime? lastModifiedDate = AppSettings.ClientService(receiverName).EmployeeLastModified(receiverName, AppSettings.SECRET_KEY(receiverName));

                receiverLogger.LogInfo("Finding Employee records modified since " + lastModifiedDate, "Start Employee");
                var lqbExport = employeeWebService.FetchEmployeeContentExports(lastModifiedDate, processEmployeeDataLogger);
                List<LQBData<EmployeeFieldId>> exportSplitByTable = lqbExport.SplitForMultipleTables(EmployeeService.Employee1FieldTypes.Keys, EmployeeService.Employee2FieldTypes.Keys);
                List<Dictionary<EmployeeFieldId, Field>> employee1Export = exportSplitByTable[0].Records;
                List<Dictionary<EmployeeFieldId, Field>> employee2Export = exportSplitByTable[1].Records;

                int totalRecords = employee1Export.Count;
                var employeeLogger = receiverLogger.CreateAndStartContentTypeLogger(E_ContentType.Employee, employee1Export.Count, EmployeeDataBatchSize);
                int batchNumber = 0;
                while (employee1Export.Count > 0 && employee2Export.Count > 0)
                {
                    using (var employeeBatchLogger = processEmployeeDataLogger.CreateSubLog($"SendEmployeeBatch", shouldTime: true))
                    {
                        employeeBatchLogger.LogAttribute("BatchNumber", batchNumber.ToString());

                        List<Dictionary<EmployeeFieldId, Field>> employee1Batch = employee1Export.Take(EmployeeDataBatchSize).ToList();
                        List<Dictionary<EmployeeFieldId, Field>> employee2Batch = employee2Export.Take(EmployeeDataBatchSize).ToList();

                        if (SendData(employee1Batch, receiverName, E_ContentType.Employee, employeeLogger, employeeBatchLogger) && SendData(employee2Batch, receiverName, E_ContentType.Employee2, employeeLogger, employeeBatchLogger))
                        {
                            // Advance export if successful
                            employee1Export = employee1Export.Skip(EmployeeDataBatchSize).ToList();
                            employee2Export = employee2Export.Skip(EmployeeDataBatchSize).ToList();
                            batchNumber++;
                        }
                        else
                        {
                            var message = "Sending an Employee data batch was not successful. " + employee1Export.Count + " records left unexported. Aborting employee data processing.";
                            employeeBatchLogger.LogErrorElement(message);
                            throw new ApplicationException(message);
                        }
                    }
                }

                if (totalRecords > 0)
                {
                    employeeLogger.LogEnd(null);
                }

            return totalRecords;
            }
        }

        /// <summary>
        /// Loads and sends all employee role assignment data for the current Broker ID.
        /// </summary>
        /// <param name="database">Database access object for loading DB data.</param>
        /// <param name="brokerId">Broker ID.</param>
        /// <param name="receiverName">The name of the receiver of the data.</param>
        /// <param name="receiverLogger">The parent logging object for recording to logs.</param>
        /// <param name="xmlLogger">The xml logger.</param>
        private static void ProcessEmployeeRoleAssignmentData(DB database, Guid brokerId, string receiverName, Logger receiverLogger, XmlLogger xmlLogger)
        {
            using (var processEmployeeRolAssignmentDataLogger = xmlLogger.CreateSubLog("ProcessEmployeeRoleAssignment", shouldTime: true))
            {
                bool sendBUserAssignments = Tools.ExtraOtherContent[receiverName].Contains(E_ContentType.Employee);
                var roleAssignmentData = database.GetRoleAssignments(brokerId, receiverName, sendBUserAssignments, sendPUsers: true);
                var roleAssignmentHash = new HashSet<Tuple<Guid, Guid>>(roleAssignmentData.Select(roleAssignment => new Tuple<Guid, Guid>(new Guid(roleAssignment["EmployeeId"].Value), new Guid(roleAssignment["RoleId"].Value))));
                var employeeLogger = receiverLogger.CreateAndStartContentTypeLogger(E_ContentType.EmployeeRoleAssignment, roleAssignmentData.Count);

                var existingRecords = GetRecordsInClient(receiverName, E_ContentType.EmployeeRoleAssignment);
                var existingRecordsHash = new HashSet<Tuple<Guid, Guid>>(existingRecords.Select(existing => new Tuple<Guid, Guid>(new Guid(existing["EmployeeId"].Value), new Guid(existing["RoleId"].Value))));
                var recordsToDelete = existingRecords.Where(record => !roleAssignmentHash.Contains(new Tuple<Guid, Guid>(new Guid(record["EmployeeId"].Value), new Guid(record["RoleId"].Value)))).ToList();
                employeeLogger.LogInfo("Deleting " + recordsToDelete.Count + " role assignments not found in LendingQB.", "Delete Employee Roles");
                DeleteRecordsByPrimaryKey(recordsToDelete, receiverName, E_ContentType.EmployeeRoleAssignment, xmlLogger);

                var recordsToSend = roleAssignmentData.Where(record => !existingRecordsHash.Contains(new Tuple<Guid, Guid>(new Guid(record["EmployeeId"].Value), new Guid(record["RoleId"].Value)))).ToList();
                employeeLogger.LogInfo("Sending " + recordsToSend.Count + " role assignments not found in client DB.", "Send Employee Roles");
                SendDataInBatches(recordsToSend, EmployeeRoleAssignmentBatchSize, receiverName, E_ContentType.EmployeeRoleAssignment, employeeLogger, xmlLogger);
                employeeLogger.LogEnd(null);
            }
        }

        private static readonly int EmployeeDataBatchSize = 200;

        private static readonly int EDocsAuditHistoryBatchSize = 500;

        private static readonly int EDocsMetadataBatchSize = 2000;

        private static readonly int EmployeeRoleAssignmentBatchSize = 1000;

        private static readonly int PoolBatchSize = 100;

        /// <summary>
        /// Loads EDocs data from LQB web services, processes it for the receiver DB, and then sends it to the receiver.
        /// </summary>
        /// <param name="receiver">The name of the receiver to send data to.</param>
        private static void ProcessEdocsData(string receiver, Logger receiverLogger, XmlLogger xmlLogger)
        {
            using (var processEdocsLogger = xmlLogger.CreateSubLog("ProcessEdocsData", shouldTime: true))
            {
                EDocsServiceProvider eDocsService = new EDocsServiceProvider(receiver);

                int highestVersionNumberLoaded = AppSettings.ClientService(receiver).LargestEDocsVersionNumber(receiver, AppSettings.SECRET_KEY(receiver));

                /// Note: EDocs metadata is loaded from LQB in batches, since it can be fairly large. Batch size is determined by the <see cref="EDocsServiceProvider.ListEDocsAuditHistory(string, string, int)"/> method in LQB.
                EDocsServiceProvider.EDocsContent exportSplitByTable = eDocsService.FetchEDocsContentExports(highestVersionNumberLoaded, processEdocsLogger);
                var auditHistoryData = exportSplitByTable.EDocsAuditHistoryData.Records;
                var eDocsData = exportSplitByTable.EDocsData.Records;

                bool success = true;
                int batchCount = 0;
                int totalEdocAuditHistoryRecordCount = 0;
                while (success && auditHistoryData.Any())
                {
                    using (var edocsBatchLogger = processEdocsLogger.CreateSubLog($"SendEDocsBatch", shouldTime: true))
                    {
                        edocsBatchLogger.LogAttribute("BatchNumber", batchCount.ToString());
                        edocsBatchLogger.LogElement("EDocCount", eDocsData.Count.ToString());
                        edocsBatchLogger.LogElement("AuditHistoryCount", auditHistoryData.Count.ToString());

                        batchCount += 1;
                        totalEdocAuditHistoryRecordCount += auditHistoryData.Count;
                        receiverLogger.CreateAndStartEdocBatchLogger(batchCount, auditHistoryData.Count, eDocsData.Count, highestVersionNumberLoaded);
                        highestVersionNumberLoaded = auditHistoryData.Select(dict => int.Parse(dict[EDocsServiceProvider.EDocsAuditHistoryFieldId.Version].Value)).Max();
                        // Send core EDoc data to the receiver before Audit History data in case something goes wrong.
                        //      If EDoc core data is sent but Audit History data isn't, the highest version isn't increased and it will all get re-sent next time the service runs.
                        //      If it were sent the other way around, we would never send the corresponding EDocs (bad).
                        success = SendDataInBatches(eDocsData, EDocsMetadataBatchSize, receiver, E_ContentType.EDoc, receiverLogger, edocsBatchLogger)
                            && SendDataInBatches(auditHistoryData, EDocsAuditHistoryBatchSize, receiver, E_ContentType.EDocsAuditHistory, receiverLogger, edocsBatchLogger);

                        // Load additional batch of data from LQB
                        exportSplitByTable = eDocsService.FetchEDocsContentExports(highestVersionNumberLoaded, edocsBatchLogger);
                        auditHistoryData = exportSplitByTable.EDocsAuditHistoryData.Records;
                        eDocsData = exportSplitByTable.EDocsData.Records;
                    }
                }

                receiverLogger.LogInfo("Sent " + Tools.Quantify(totalEdocAuditHistoryRecordCount, "edoc audit", "edoc audits") + " in " + Tools.Quantify(batchCount, "batch", "batches"), "End EDocTransmission");
            }
        }

        private static void ProcessMortgagePoolData(Guid brokerId, DB database, string receiverName, Logger receiverLogger, XmlLogger parentXmlLogger)
        {
            using (var processModifiedLoansLogger = parentXmlLogger.CreateSubLog(nameof(ProcessMortgagePoolData), shouldTime: true))
            {
                var poolService = new MortgagePoolService(receiverName);
                var poolData = poolService.LoadAllMortgagePools(processModifiedLoansLogger);
                var poolsToDelete = GetRecordsToDelete(poolData.Records.Select(pool => pool.ToDictionary(item => item.Key.ToString("G"), item => item.Value)), GetRecordsInClient(receiverName, E_ContentType.MortgagePool));

                DeleteRecordsByPrimaryKey(poolsToDelete.ToList(), receiverName, E_ContentType.MortgagePool, parentXmlLogger);
                SendDataInBatches(poolData.Records, PoolBatchSize, receiverName, E_ContentType.MortgagePool, receiverLogger, parentXmlLogger);
            }
        }

        /// <summary>
        /// Sends data to the receiver by first dividing it into batches of a given size.
        /// </summary>
        /// <typeparam name="TFieldId">The field ID type. Usually a string or Enum type.</typeparam>
        /// <param name="data">The data to be sent, as a list of "record" dictionaries for the receiver DB.</param>
        /// <param name="batchSize">The maximum size of a batch to send.</param>
        /// <param name="receiverName">The name of the receiver of the data.</param>
        /// <param name="contentType">The content type of the data to be sent.</param>
        /// <returns>Whether or not the batches were sent successfully.</returns>
        private static bool SendDataInBatches<TFieldId>(List<Dictionary<TFieldId, Field>> data, int batchSize, string receiverName, E_ContentType contentType, Logger logger, XmlLogger xmlLogger)
        {
            return Batch(data, batchSize).All(batch => SendData(batch, receiverName, contentType, logger, xmlLogger));
        }

        /// <summary>
        /// Divides the input list into a set of smaller lists of a given size. 
        /// If the remainder is smaller than the given batch size, it will be placed in the last list.
        /// </summary>
        /// <typeparam name="T">The type contained by the input and output lists</typeparam>
        /// <param name="source">The list to be broken into batches.</param>
        /// <param name="batchSize">The size of batch to divide <paramref name="source"/> into. The last list may be smaller.</param>
        /// <returns>An IEnumerable of lists size <paramref name="batchSize"/> or smaller.</returns>
        /// <example>
        /// var list = new List&lt;int>() { 0, 1, 4, 5, 6, 7, 9, 10, 3, 12 };
        /// Batch(list, 3) = { { 0, 1, 4 }, { 5, 6, 7 }, { 9, 10, 3 }, { 12 } }
        /// </example>
        private static IEnumerable<List<T>> Batch<T>(List<T> source, int batchSize)
        {
            for (int i = 0; i < source.Count / batchSize; i++)
            {
                yield return source.GetRange(i * batchSize, batchSize);
            }

            int remainder = source.Count % batchSize;
            if (remainder > 0)
            {
                yield return source.GetRange(source.Count - remainder, remainder);
            }
        }

        private static int ClearProcessedLoansWithFailureCount(Guid sBrokerID, DateTime dtLastMod, DB database, int iFailToClearTracker, Guid receiverAppCode)
        {
            if (IsDebug())
            {
                iFailToClearTracker = 1000;
            }
            else
            {
                iFailToClearTracker = (database.ClearProcessedLoansFromModList(sBrokerID, dtLastMod, receiverAppCode) > 0) ? 0 : iFailToClearTracker + 1;
            }
            return iFailToClearTracker;
        }

        private static bool IsDebug()
        {
            return !string.IsNullOrEmpty(AppSettings.DEBUG);
        }

        // Three strikes w/o any records cleared means the process is just churning files that the lender is modifying
        private static bool JustChurning(int iFailToClearCount)
        {
            return (iFailToClearCount >= 3);
        }

        private static bool SendLoans(string loanXML, string appXML, List<Dictionary<string, Field>> tasks, int loanCount, string receiver, Logger logger, XmlLogger xmlLogger)
        {
            bool bSuccess = true;

            DEBUG.Info.LogLoans(loanCount, tasks.Count);
            string sLoansXML = string.IsNullOrEmpty(loanXML) ? XMLDocument.ToXMLString(new List<Dictionary<string, Field>>(0), E_ContentType.Loan) : loanXML;
            string sAppsXML = string.IsNullOrEmpty(appXML) ? XMLDocument.ToXMLString(new List<Dictionary<string, Field>>(0), E_ContentType.App) : appXML;
            string sTasksXML = tasks.Count > 0 ? XMLDocument.ToXMLString(tasks, E_ContentType.Task) : string.Empty;

            if (IsDebug())
            {
                long nowInTicks = DateTime.Now.Ticks;
                DEBUG.WriteToFile(AppSettings.DEBUG_OUTPUT_FILE_PATH + nowInTicks + "loans" + ".xml", sLoansXML);
                DEBUG.WriteToFile(AppSettings.DEBUG_OUTPUT_FILE_PATH + nowInTicks + "apps" + ".xml", sAppsXML);
                DEBUG.WriteToFile(AppSettings.DEBUG_OUTPUT_FILE_PATH + nowInTicks + "tasks" + ".xml", sTasksXML);
            }
            else
            {
                string sResult = SendDataWithRetry(
                        receiver,
                        xmlLogger,
                        client =>
                            string.IsNullOrEmpty(sTasksXML)
                            ? client.ReplicateLoans(sLoansXML, sAppsXML, receiver, AppSettings.SECRET_KEY(receiver))
                            : client.ReplicateLoansIncludingTasks(sLoansXML, sAppsXML, sTasksXML, receiver, AppSettings.SECRET_KEY(receiver)));

                if (!string.Equals(sResult, "OK"))
                {
                    logger.LogResponse(sResult);
                    SendFailureMessage(string.Format("Data transmission to {0} failed:{1}{2}", receiver, System.Environment.NewLine, sResult), receiverName: receiver);
                    bSuccess = false;
                }
            }
            return bSuccess;
        }

        private static string SendDataWithRetry(string receiverName, XmlLogger xmlLogger, Func<Client, string> sendData)
        {
            var stopwatch = System.Diagnostics.Stopwatch.StartNew();
            int exceptionCount = 0;
            const int MaxExceptionCount = 3;

            while (true)
            {
                using (var sendDataWithRetryLogger = xmlLogger.CreateSubLog($"SendDataWithRetry", shouldTime: true))
                {
                    sendDataWithRetryLogger.LogAttribute("AttemptNumber", exceptionCount.ToString());
                    try
                    {
                        string result = sendData(AppSettings.ClientService(receiverName));
                        DEBUG.Info.LogTransmissionCompleted(stopwatch.Elapsed);
                        return result;
                    }
                    catch (InvalidOperationException exception)
                    {
                        if (++exceptionCount > MaxExceptionCount)
                        {
                            DEBUG.Info.LogTransmissionGiveUp(stopwatch.Elapsed);
                            sendDataWithRetryLogger.LogExceptionElement($"Exception limit reached. Giving up. Message: {exception.Message}", exception.GetType());
                            throw;
                        }

                        var sleepInMs = 30000;

                        DEBUG.Info.LogRetryPreSleep(exception.ToString());
                        sendDataWithRetryLogger.LogElement(new XElement("Sleep", new XAttribute("DurationInMilliseconds", sleepInMs.ToString())));

                        System.Threading.Thread.Sleep(sleepInMs);
                        DEBUG.Info.LogRetryingNow();
                    }
                    catch (Exception e)
                    {
                        DEBUG.Info.LogTransmissionGiveUp(stopwatch.Elapsed);
                        sendDataWithRetryLogger.LogExceptionElement($"Irrecoverable exception while sending data with retries.", e.GetType());
                        throw;
                    }
                }
            }
        }

        private static void ProcessPMLOriginatingCompanyAndUserData(Guid sBrokerID, DB database, string receiver, ExecutionOptions options, Logger receiverLogger, XmlLogger xmlLogger)
        {
            foreach (var contentType in Tools.PMLContentTypesToSend(receiver, options))
            {
                ProcessPMLData(sBrokerID, database, receiver, contentType, receiverLogger, xmlLogger);
            }
        }

        private static bool ProcessPMLData(Guid sBrokerID, DB database, string receiver, E_ContentType contentType, Logger receiverLogger, XmlLogger xmlLogger)
        {
            using (var processPmlDataLogger = xmlLogger.CreateSubLog($"ProcessPmlData", shouldTime: true))
            {
                processPmlDataLogger.LogAttribute("ContentType", contentType.ToString());

                const int PmlDataBatchSize = 100; // chosen somewhat arbitrarily
                List<Dictionary<string, Field>> dataBook = database.GetDataByBrokerID(sBrokerID, receiver, contentType);
                string dataFileName = "SendDataToClientDB_" + receiver + "_" + contentType.ToString() + ".csv";
                if (!Tools.IsDataDifferent(dataFileName, dataBook))
                {
                    var message = contentType.ToString() + " data is not different. Skipping PML data processing.";
                    receiverLogger.LogInfo(message, "Skip ProcessPMLData");
                    processPmlDataLogger.LogElement("Skip", message);
                    return true;
                }

                bool success = false;
                Logger pmlDataLogger = null;
                try
                {
                    pmlDataLogger = receiverLogger.CreateAndStartContentTypeLogger(contentType, dataBook.Count, PmlDataBatchSize);
                    if (Tools.EnableDeletionByPrimaryKey(receiver))
                    {
                        var recordsInClient = GetRecordsInClient(receiver, contentType);
                        var recordsToDelete = GetRecordsToDelete(dataBook, recordsInClient);

                        processPmlDataLogger.LogElement("RecordsToDeleteCount", recordsToDelete.Count.ToString());
                        processPmlDataLogger.LogElement("RecordsToSendCount", dataBook.Count.ToString());

                        success = Batch(recordsToDelete, PmlDataBatchSize).All(batch => DeleteRecordsByPrimaryKey(batch, receiver, contentType, processPmlDataLogger)) &&
                                  Batch(dataBook, PmlDataBatchSize).All(batch => SendPMLData(batch, receiver, contentType, pmlDataLogger, processPmlDataLogger));
                    }
                    else
                    {
                        processPmlDataLogger.LogElement("RecordsToSendCount", dataBook.Count.ToString());
                        success = Batch(dataBook, PmlDataBatchSize).All(batch => SendPMLData(batch, receiver, contentType, pmlDataLogger, processPmlDataLogger));
                    }

                    return success;
                }
                finally
                {
                    if (!success)
                    {
                        System.IO.File.Delete(dataFileName);
                    }

                    if (pmlDataLogger != null)
                    {
                        pmlDataLogger.LogEnd(null);
                    }
                }
            }
        }

        /// <summary>
        /// Gets a list of all the records belonging to <paramref name="recordsCurrentlyInClient"/> that will not
        /// be overwritten by the import of an entry in <paramref name="recordsBeingImported"/>.
        /// </summary>
        private static List<Dictionary<string, Field>> GetRecordsToDelete(IEnumerable<Dictionary<string, Field>> recordsBeingImported, IEnumerable<Dictionary<string, Field>> recordsCurrentlyInClient)
        {
            var recordsToDelete = new List<Dictionary<string, Field>>();
            foreach (var clientRecord in recordsCurrentlyInClient)
            {
                if (!RecordsContain(recordsBeingImported, clientRecord))
                {
                    recordsToDelete.Add(clientRecord);
                }
            }

            return recordsToDelete;
        }

        /// <summary>
        /// Returns a value indicating whether <paramref name="recordCollection"/> contains an entry
        /// matching the every field inside <paramref name="record"/>.
        /// </summary>
        private static bool RecordsContain(IEnumerable<Dictionary<string, Field>> recordCollection, Dictionary<string, Field> record)
        {
            foreach (var recordEntry in recordCollection)
            {
                if (record.All(datapoint => RecordMatchesDatapoint(recordEntry, datapoint)))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Returns a value indicating if <paramref name="record"/> contains a data point matching
        /// the field id and value of <paramref name="datapoint"/>.
        /// </summary>
        private static bool RecordMatchesDatapoint(Dictionary<string, Field> record, KeyValuePair<string, Field> datapoint)
        {
            Field field;
            return record.TryGetValue(datapoint.Key, out field) && string.Equals(datapoint.Value.Value, field.Value, StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Retrieves a list of all primary keys to records of the specified content type currently
        /// in the receiver's database.
        /// </summary>
        private static List<Dictionary<string, Field>> GetRecordsInClient(string receiver, E_ContentType contentType)
        {
            // A little math on how we got this:
            // The biggest primary key record right now seems to be up to 270 characters.
            // 2 characters per byte (.NET uses UTF-16) and 1 KB per 1000 bytes means this should top out at 2700 KB.
            // The goal is to keep this below the transmission size limit, which is configured on the client's machine
            // Most likely, it is currently set somewhere around 4-8 MB.
            const int batchSize = 5000;

            // We use this for efficiency, but not to drive actual implementation.  If by chance someone is adding records to the
            // database while we're reading, we shouldn't break so easily.
            int numberOfRecords = AppSettings.ClientService(receiver).CountRecords(contentType, receiver, AppSettings.SECRET_KEY(receiver));

            var records = new List<Dictionary<string, Field>>(numberOfRecords);
            bool stillAddingItems = true;
            while (stillAddingItems)
            {
                string xmlResponse = AppSettings.ClientService(receiver).ListRecords(contentType, batchSize, records.Count, receiver, AppSettings.SECRET_KEY(receiver));
                int countBefore = records.Count;
                records.AddRange(XMLDocument.FromXMLString(xmlResponse, contentType));
                stillAddingItems = countBefore != records.Count;
            }

            return records;
        }

        /// <summary>
        /// Given a set of records specified by <paramref name="data"/>, transmits them to the receiver for deletion.
        /// </summary>
        private static bool DeleteRecordsByPrimaryKey(List<Dictionary<string, Field>> data, string receiverName, E_ContentType contentType, XmlLogger xmlLogger)
        {
            bool success = true;
            DEBUG.Info.LogData(data.Count, contentType);
            string dataXML = XMLDocument.ToXMLString(data, contentType);
            if (IsDebug())
            {
                DEBUG.WriteToFile(AppSettings.DEBUG_OUTPUT_FILE_PATH + DateTime.Now.Ticks + contentType.ToString() + ".deletion.xml", dataXML);
            }
            else
            {
                using (var deleteRecordsLogger = xmlLogger.CreateSubLog("DeleteRecords", shouldTime: true))
                {
                    deleteRecordsLogger.LogElement("RecordCount", data.Count.ToString());
                    string result = SendDataWithRetry(receiverName, deleteRecordsLogger, client => client.DeleteDataByPrimaryKey(dataXML, contentType, receiverName, AppSettings.SECRET_KEY(receiverName)));

                    if (!string.Equals(result, "OK"))
                    {
                        SendFailureMessage(string.Format(contentType.ToString() + " Data transmission to {0} failed on deletion:{1}{2}", receiverName, System.Environment.NewLine, result), receiverName: receiverName);
                        success = false;
                    }
                }
            }

            return success;
        }

        private static bool SendPMLData(List<Dictionary<string, Field>> data, string receiverName, E_ContentType contentType, Logger logger, XmlLogger xmlLogger)
        {
            bool success = true;
            var dataCount = data.Count;
            DEBUG.Info.LogData(dataCount, contentType);
            string dataXML = XMLDocument.ToXMLString(data, contentType);
            if (IsDebug())
            {
                DEBUG.WriteToFile(AppSettings.DEBUG_OUTPUT_FILE_PATH + DateTime.Now.Ticks + contentType.ToString() + ".xml", dataXML);
            }
            else
            {
                using (var sendPmlDatalogger = xmlLogger.CreateSubLog("SendPMLData", shouldTime: true))
                {
                    sendPmlDatalogger.LogElement("RecordCount", dataCount.ToString());
                    string result = SendDataWithRetry(receiverName, sendPmlDatalogger, client => client.ReplicatePMLData(dataXML, contentType, receiverName, AppSettings.SECRET_KEY(receiverName)));

                    if (!string.Equals(result, "OK"))
                    {
                        logger.LogResponse(result);
                        SendFailureMessage(string.Format(contentType.ToString() + " Data transmission to {0} failed:{1}{2}", receiverName, System.Environment.NewLine, result), receiverName: receiverName);
                        success = false;
                    }
                }
            }
            return success;
        }

        private static bool SendData<TFieldId>(IEnumerable<Dictionary<TFieldId, Field>> data, string receiverName, E_ContentType contentType, Logger logger, XmlLogger xmlLogger)
        {
            bool success = true;
            var dataCount = data.Count();
            DEBUG.Info.LogData(dataCount, contentType);
            string dataXML = XMLDocument.ToXMLString(data, contentType);
            if (IsDebug())
            {
                DEBUG.WriteToFile(AppSettings.DEBUG_OUTPUT_FILE_PATH + DateTime.Now.Ticks + contentType.ToString() + ".xml", dataXML);
            }
            else
            {
                using (var sendDataLogger = xmlLogger.CreateSubLog("SendData", shouldTime: true))
                {
                    sendDataLogger.LogAttribute("ContentType", contentType.ToString());
                    sendDataLogger.LogElement("DataCount", dataCount.ToString());

                    string result = SendDataWithRetry(receiverName, sendDataLogger, client => client.ReplicateData(dataXML, contentType, receiverName, AppSettings.SECRET_KEY(receiverName)));

                    if (!string.Equals(result, "OK"))
                    {
                        logger.LogResponse(result);
                        SendFailureMessage(string.Format(contentType.ToString() + " Data transmission to {0} failed:{1}{2}", receiverName, System.Environment.NewLine, result), receiverName: receiverName);
                        success = false;
                    }
                }
            }

            return success;
        }

        private static void SendFailureMessage(string clientMessage, string internalMessage = null, string receiverName = null, string customerCode = null)
        {
            ServiceMonitorClient.TryPostStatus(ServiceMonitorStatus.FAILED, internalMessage ?? clientMessage);
            SmtpClient smtpClient = new SmtpClient(AppSettings.SMTP_SERVER);
            string receiverDescription = (string.IsNullOrEmpty(customerCode) || receiverName == customerCode) ? receiverName : receiverName + " for " + customerCode;
            MailMessage message = new MailMessage(
                "do-not-reply@lendingqb.com",
                AppSettings.SUPPORT_EMAIL,
                "Data Replication Tool: SendDataToClientDB " + receiverDescription,
                clientMessage + Environment.NewLine + Environment.NewLine +
                internalMessage + Environment.NewLine + Environment.NewLine +
                "Debug Log: " + Environment.NewLine + DEBUG.Info.ToString());
            smtpClient.Send(message);

            if (!string.IsNullOrEmpty(receiverName))
            {
                string toReceiverAddress = AppSettings.RECEIVER_EMAIL_ADDRESS(receiverName);
                if (!string.IsNullOrEmpty(toReceiverAddress))
                {
                    MailMessage lenderMessage = new MailMessage("do-not-reply@lendingqb.com", toReceiverAddress, "LendingQB Data Cache Replication Service Error", clientMessage);
                    smtpClient.Send(lenderMessage);
                }
            }
        }
    }
}