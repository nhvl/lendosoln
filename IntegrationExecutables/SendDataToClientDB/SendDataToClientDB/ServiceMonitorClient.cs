﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace SendDataToClientDB
{
    enum ServiceMonitorStatus
    {
        OK = 0,
        FAILED = 1,
        STARTED = 2
    }

    static class ServiceMonitorClient
    {
        private static JavaScriptSerializer serializer = new JavaScriptSerializer();
        public static string[] Args { private get; set; }
        public static ServiceMonitorStatus LastKnownStatus { get; private set; }

        public static void TryPostStatus(ServiceMonitorStatus status, string log)
        {
            try
            {
                LastKnownStatus = status;
                PostStatus(status, log);
            }
            catch (Exception e)
            {
                Logger.CreateServiceMonitorFailureLogger().LogError(e);
            }
        }

        private static void PostStatus(ServiceMonitorStatus status, string log)
        {
            var message = new
            {
                StatusType = status,
                ServiceName = "SendDataToClientDB " + string.Join(" ", Args),
                LogData = log
            };

            using (WebClient wc = new WebClient())
            {
                wc.Headers.Add(HttpRequestHeader.ContentType, "application/json");
                wc.UploadString(new Uri(AppSettings.ServiceMonitorUrl), serializer.Serialize(message));
            }
        }
    }
}