﻿namespace SendDataToClientDB
{
    using System;
    using com.lendingqb.AuthService;
    using com.lendingqb.Reporting;
    using com.lendingqb.Employee;
    using com.lendingqb.EDocs;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;
    using System.Xml.XPath;
    using ReceiveDataFromLQBClientService;
    using System.Security.Cryptography.X509Certificates;

    public abstract class LQBWebServiceProvider
    {
        private string authTicket;
        private DateTime authTicketExpireTime;
        private static readonly TimeSpan authTicketTimeLimit = new TimeSpan(3, 59, 45);

        /// <summary>
        /// The authentication ticket that validates interaction with the reporting service.
        /// </summary>
        internal string AuthTicket
        {
            get
            {
                if (this.authTicketExpireTime <= DateTime.Now || string.IsNullOrEmpty(this.authTicket))
                {
                    try
                    {
                        AuthService authService = LqbWebservices.AuthenticationService;
                        X509Certificate2 cert = AppSettings.RECEIVER_CERTIFICATE(this.ReceiverName);
                        if (cert != null)
                        {
                            authService.ClientCertificates.Add(cert);
                        }

                        this.authTicket = authService.GetUserAuthTicket(AppSettings.RECEIVER_USERNAME(this.ReceiverName), AppSettings.RECEIVER_PASSWORD(this.ReceiverName));
                        this.authTicketExpireTime = DateTime.Now.Add(authTicketTimeLimit);
                    }
                    catch (Exception exc)
                    {
                        throw new LQBInternalException(exc);
                    }
                }

                return this.authTicket;
            }
        }

        /// <summary>
        /// Gets the receiver's name. This is used to determine which account to use when authenticating with LQB web services.
        /// </summary>
        public string ReceiverName { get; private set; }

        /// <summary>
        /// The Secret key used for authenticating the SendDataToClientDB process with LendingQB. Overridden by some child classes.
        /// </summary>
        protected virtual string SecretKey { get { return ""; } }

        public LQBWebServiceProvider(string receiver)
        {
            this.ReceiverName = receiver;
        }

        /// <summary>
        /// Loads a data response from LendingQB web services using the specified loading method.
        /// </summary>
        /// <typeparam name="TFieldId">The Field ID enum type used in the mapping from field ID to DB type.</typeparam>
        /// <typeparam name="TVersionType">The type of the parameter passed to LendingQB for determining which records to send back (eg. Last Modified Date -> DateTime, Version -> int).</typeparam>
        /// <param name="lqbWebServiceMethod">A function which calls LQB web service methods for retrieving records. Takes an Authentication Ticket as parameters.</param>
        /// <param name="versionSpecifier">The value of the parameter passed to LendingQB for determining which records to send back.</param>
        /// <param name="fieldTypeLookup">A dictionary for mapping a field ID to the type that should be used to represent it in the database.</param>
        /// <returns>A response object containing the string response loaded and processed from LQB and whether or not it succeeded.</returns>
        public LQBData<TFieldId> LoadLQBDataAndAddDbTypes<TFieldId>(Func<string, string> lqbWebServiceMethod, IDictionary<TFieldId, DBTypes> fieldTypeLookup, E_ContentType contentType, XmlLogger xmlLogger) where TFieldId : struct, IConvertible
        {
            using (var loadLqbDataLogger = xmlLogger.CreateSubLog("LoadLqbData", shouldTime: true))
            {
                loadLqbDataLogger.LogAttribute("ContentType", contentType.ToString());

                XElement lqbResponseXml;
                using (var loadLQBDataLogger = loadLqbDataLogger.CreateSubLog($"CallWebservice", shouldTime: true))
                {
                    lqbResponseXml = XElement.Parse(lqbWebServiceMethod(this.AuthTicket));
                }

                // I noticed the parsing taking a long time while testing, so best to put a timer around it.
                using (var parseResponseLogger = loadLqbDataLogger.CreateSubLog($"ParseWebserviceResponse", shouldTime: true))
                {
                    XElement resultStatusElement = lqbResponseXml.Element("result");
                    if (resultStatusElement != null && resultStatusElement.Attribute("status").Value != "OK")
                    {
                        parseResponseLogger.LogErrorElement("Error encountered.");
                        throw new Exception(string.Join(";" + Environment.NewLine, resultStatusElement.XPathSelectElements("Errors/Error").Select(element => element.Value).ToArray()));
                    }

                    return LQBData<TFieldId>.Parse(lqbResponseXml, fieldTypeLookup, contentType);
                }
            }
        }
    }
}