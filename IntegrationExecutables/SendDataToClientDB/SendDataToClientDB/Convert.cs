﻿using System;
using System.Data.SqlClient;

namespace SendDataToClientDB
{
    public static class DBConvert
    {
        public static string GetDBObjectAsString(string sField, SqlDataReader SQLReader)
        {
            return (SQLReader[sField] is DBNull) ? null : SQLReader[sField].ToString();
        }

        /// <summary>
        /// Gets a string value for a <see cref="DateTime"/> that can round-trip through <seealso cref="DateTime.Parse"/>.
        /// </summary>
        public static string GetNullableDateTimeAsStringWithFullPrecision(string sField, SqlDataReader SQLReader)
        {
            return SQLReader[sField] is DBNull ? null : ((DateTime)SQLReader[sField]).ToString("o"); // Use the round-trip format specifier
        }

        /*
        public static string GetNullableBoolAsString(string sField, SqlDataReader SQLReader)
        {
            return (SQLReader[sField] is DBNull) ? null : (bool)SQLReader[sField]);
        }

        public static string GetNullableDateTimeAsString(string sField, SqlDataReader SQLReader)
        {
            return (SQLReader[sField] is DBNull) ? null : (DateTime)SQLReader[sField]);
        }

        public static string GetNullableDecimalAsString(string sField, SqlDataReader SQLReader)
        {
            return (SQLReader[sField] is DBNull) ? null : Convert.ToDecimal(SQLReader[sField]);
        }

        public static string GetNullableIDAsString(string sField, SqlDataReader SQLReader)
        {
            return (SQLReader[sField] is DBNull) ? null : (Guid)SQLReader[sField];
        }

        public static string GetNullableIntAsString(string sField, SqlDataReader SQLReader)
        {
            return (SQLReader[sField] is DBNull) ? null : (int)SQLReader[sField];
        }

        public static string GetNullableString(string sField, SqlDataReader SQLReader)
        {
            return (SQLReader[sField] is DBNull) ? null : (string)SQLReader[sField];
        }
        */
    }
}
