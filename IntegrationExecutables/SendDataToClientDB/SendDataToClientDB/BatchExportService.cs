﻿// <copyright file="BatchExportService.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Justin Lara
//  Date:   December 11, 2014
// </summary>
namespace SendDataToClientDB
{
    using System;
    using System.Collections.Generic;
    using SendDataToClientDB.com.lendingqb.AuthService;
    using SendDataToClientDB.com.lendingqb.Reporting;
    using System.Text;

    /// <summary>
    /// A wrapper class that encapsulates interactions with the authentication and reporting
    /// web services to provide batch export functionality for the SendDataToClientDB tool.
    /// </summary>
    public class BatchExportService : LQBWebServiceProvider
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BatchExportService" /> class, setting up the web services for interaction.
        /// </summary>
        /// <param name="receiver">The customer code of the lender whose data will be exported.</param>
        public BatchExportService(string receiver) : base(receiver)
        {
        }

        /// <summary>
        /// Runs a list of loan files through the batch exporter and returns the output.
        /// </summary>
        /// <param name="loxmlLoanList">An LOXML list of loan files to export.</param>
        /// <param name="lender">The customer code of the lender.</param>
        /// <param name="exportFormat">The batch export to use to load the data.</param>
        /// <returns>A string containing the batch export output.</returns>
        private string ExportData(string loxmlLoanList, string exportFormat, XmlLogger xmlLogger)
        {
            using (var batchExportLogger = xmlLogger.CreateSubLog("CallBatchExport", shouldTime: true))
            {
                batchExportLogger.LogAttribute("ExportFormat", exportFormat);

                string contents;
                bool batchExportSucceeded;
                try
                {
                    string batchExportResponse = LqbWebservices.ReportingService.RunSynchronousBatchExportWithLoanList(
                        this.AuthTicket,
                        loxmlLoanList,
                        exportFormat,
                        AppSettings.BATCH_EXPORT_KEY);
                    batchExportSucceeded = LOXML.ParseBatchExportReponseForFileContents(batchExportResponse, out contents);
                }
                catch (Exception exc)
                {
                    batchExportLogger.LogExceptionElement(exc.Message, exc.GetType());
                    throw new LQBInternalException(exc);
                }

                if (batchExportSucceeded && !string.IsNullOrEmpty(contents))
                {
                    byte[] data = Convert.FromBase64String(contents);
                    return contents = Tools.GetUTF8String(data);
                }
                else
                {
                    batchExportLogger.LogErrorElement("The batch export was unable to complete.");
                    throw new Exception("The batch export was unable to complete. Error message: " + contents);
                }
            }
        }

        public string ExportLoanData(string loxmlLoanList, XmlLogger xmlLogger)
        {
            return ExportData(loxmlLoanList, AppSettings.RECEIVER_LOAN_EXPORT_FORMAT(this.ReceiverName), xmlLogger);
        }

        public string ExportAppData(string loxmlLoanList, XmlLogger xmlLogger)
        {
            return ExportData(loxmlLoanList, AppSettings.RECEIVER_APP_EXPORT_FORMAT(this.ReceiverName), xmlLogger);
        }
    }
}