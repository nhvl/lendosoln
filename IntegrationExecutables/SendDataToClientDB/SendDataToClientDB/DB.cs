﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using SendDataToClientDB.ReceiveDataFromLQBClientService;
using SendDataToClientDB.LoadData;

namespace SendDataToClientDB
{
    public class DB
    {
        private readonly string connectionString;

        static DB()
        {
            Adapter.Drivers.ConnectionStringProviderHelper.Register("SendDataToClientDB");
        }

        public DB(string databaseName)
        {
            this.connectionString = Adapter.Drivers.ConnectionStringProviderHelper.GetMainConnectionStringByDatabaseName(databaseName, needWriteAccess: false);
        }

        public virtual int ClearProcessedLoansFromModList(Guid sBrokerID, DateTime dtLastModified, Guid lenderAppCode)
        {
            using (var connection = new SqlConnection(this.connectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand(@"ClearIntegrationModifiedFilesByBrokerId", connection);
                cmd.CommandTimeout = 60;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@LastModifiedD", dtLastModified);
                cmd.Parameters.AddWithValue("@BrokerId", sBrokerID);
                cmd.Parameters.AddWithValue("@AppCode", lenderAppCode);
                return cmd.ExecuteNonQuery();
            }
        }

        public virtual Guid GetBrokerID(string sCustomerCode)
        {
            using (var connection = new SqlConnection(this.connectionString))
            {
                connection.Open();
                DEBUG.Info.LogLender(sCustomerCode);
                SqlCommand cmd = new SqlCommand(@"GetBrokerIdByCustomerCode", connection);
                cmd.CommandTimeout = 60;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("CustomerCode", sCustomerCode);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return new Guid(reader["BrokerId"].ToString());
                    }
                    else
                    {
                        return Guid.Empty;
                    }
                }
            }
        }

        /// <summary>
        /// Loads Modified loans in batches. Since we remove loans by modified date, we include ties, but the batches returned will
        /// match the batch size requested (extra values will be stored in <paramref name="loansLoadedFromDB"/>).
        /// </summary>
        /// <param name="loansLoadedFromDB">The existing list of loans that have already been loaded from the DB, but not processed. This
        /// will hold any overflow if the batch is larger than the specified size.</param>
        /// <param name="sBrokerID">The broker ID of the lender.</param>
        /// <param name="receiverName">The receiver name for the ability to customize the data set retrieved.</param>
        /// <param name="receiverAppCode">The receiver app code which tracks the lender's modified files to be sent to this receiver.</param>
        /// <returns>The batch of loans to process and send to the client DB.</returns>
        public virtual List<Dictionary<string, Field>> GetBatchOfModifiedLoans(ref List<Dictionary<string, Field>> loansLoadedFromDB, Guid sBrokerID, string receiverName, Guid receiverAppCode)
        {
            const int LoanBatchSize = 5;
            if (loansLoadedFromDB == null || loansLoadedFromDB.Count == 0)
            {
                using (var connection = new SqlConnection(this.connectionString))
                {
                    connection.Open();
                    LoanData loan = new LoanData(receiverName);
                    SqlCommand cmd = new SqlCommand(@"ListModifiedLoansByBrokerIDwithCache", connection);
                    cmd.Parameters.AddWithValue("BrokerId", sBrokerID);
                    cmd.Parameters.AddWithValue("AppCode", receiverAppCode);
                    loansLoadedFromDB = GetData(loan, cmd);
                }
            }

            int batchSize = loansLoadedFromDB.Count > LoanBatchSize ? LoanBatchSize : loansLoadedFromDB.Count;
            var loanBatch = loansLoadedFromDB.GetRange(0, batchSize);
            loansLoadedFromDB.RemoveRange(0, batchSize);
            return loanBatch;
        }

        public virtual List<Dictionary<string, Field>> GetLoanTasks(Guid sBrokerID, Guid sLoanID, string receiverName)
        {
            using (var connection = new SqlConnection(this.connectionString))
            {
                connection.Open();
                LoanTaskData task = new LoanTaskData(receiverName);
                SqlCommand cmd = new SqlCommand(@"TASK_ListAllTasksByBrokerIdLoanId", connection);
                cmd.Parameters.AddWithValue("BrokerId", sBrokerID);
                cmd.Parameters.AddWithValue("LoanId", sLoanID);
                return GetData(task, cmd);
            }
        }

        public virtual List<Dictionary<string, Field>> GetTaskEDocAssociation(Guid loanId, Guid documentId, string receiverName)
        {
            using (var connection = new SqlConnection(this.connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(@"DOCCONDITION_GetByLoanDocument", connection);
                command.Parameters.Add(new SqlParameter("LoanId", loanId));
                command.Parameters.Add(new SqlParameter("DocumentId", documentId));

                return GetData(new LoanTaskEDocAssociationData(receiverName), command);
            }
        }

        /// <summary>
        /// Gets a list of role assignment data points to send over to the client.
        /// </summary>
        /// <param name="brokerId">The broker ID.</param>
        /// <param name="receiverName">The receiver name.</param>
        /// <param name="sendBUsers">Whether to include "B" user role assignments in the transmission.</param>
        /// <param name="sendPUsers">Whether to include "P" user role assignments in the transmission.</param>
        /// <returns>A data book of role assignment info to send.</returns>
        public virtual List<Dictionary<string, Field>> GetRoleAssignments(Guid brokerId, string receiverName, bool sendBUsers, bool sendPUsers)
        {
            using (var connection = new SqlConnection(this.connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(@"GetEmployeeRoleAssignmentsByBrokerId", connection);
                command.Parameters.Add(new SqlParameter("@BrokerId", brokerId));
                command.Parameters.Add(new SqlParameter("@Include_B_Users", sendBUsers));
                command.Parameters.Add(new SqlParameter("@Include_P_Users", sendPUsers));

                return GetData(new EmployeeRoleAssignmentData(receiverName), command);
            }
        }

        public virtual List<Dictionary<string, Field>> GetDataByBrokerID(Guid brokerID, string receiverName, E_ContentType contentType)
        {
            ILoadData loadData;
            string storedProcedure;
            switch (contentType)
            {
                case E_ContentType.OriginatingCompany:
                    loadData = new OriginatingCompanyData(receiverName);
                    storedProcedure = @"ListAllPmlBrokerByBrokerId";
                    break;
                case E_ContentType.OriginatingCompanyRelationship:
                    loadData = new OriginatingCompanyRelationshipData(receiverName);
                    storedProcedure = @"PmlBrokerRelationships_RetrieveAllByBrokerId";
                    break;
                case E_ContentType.PMLUser:
                    loadData = new PMLUserData(receiverName);
                    storedProcedure = @"RetrievePmlUsersByBrokerID";
                    break;
                default:
                    throw Tools.UnhandledContentType(contentType);
            }

            using (var connection = new SqlConnection(this.connectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand(storedProcedure, connection);
                cmd.Parameters.AddWithValue("BrokerId", brokerID);
                return GetData(loadData, cmd);
            }
        }

        public virtual List<Dictionary<string, Field>> GetExtraLoanData(Guid loanID, string receiverName, Guid brokerID, E_ContentType contentType)
        {
            ILoadData loadData;
            string storedProcedure;
            switch (contentType)
            {
                case E_ContentType.Task:
                    return this.GetLoanTasks(brokerID, loanID, receiverName);
                case E_ContentType.EDoc:
                    loadData = new LoanEDocData(receiverName);
                    storedProcedure = @"EDOCS_FetchAllDocsForLoan";
                    break;
                case E_ContentType.TaskEDocAssociation:
                    loadData = new LoanTaskEDocAssociationData(receiverName);
                    storedProcedure = @"DOCCONDITION_GetByLoan";
                    break;
                default:
                    throw Tools.UnhandledContentType(contentType);
            }

            using (var connection = new SqlConnection(this.connectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand(storedProcedure, connection);
                cmd.Parameters.Add(new SqlParameter("LoanId", loanID));
                return GetData(loadData, cmd);
            }
        }

        private static List<Dictionary<string, Field>> GetData(ILoadData data, SqlCommand cmd)
        {
            List<Dictionary<string, Field>> dataBook = new List<Dictionary<string, Field>>();
            cmd.CommandTimeout = 180;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    dataBook.Add(data.LoadData(reader));
                }
            }
            return dataBook;
        }
    }
}