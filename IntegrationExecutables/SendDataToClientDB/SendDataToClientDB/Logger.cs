﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using SendDataToClientDB.ReceiveDataFromLQBClientService;

namespace SendDataToClientDB
{
    /// <summary>
    /// The <see cref="Logger"/> class provides a stackable group of logs tied together by a common id.
    /// </summary>
    /// <remarks>
    /// The core of this idea is that each process we want to log represents a unique scope
    /// that we may want to search and observe.  We do this by providing building an id for
    /// each log recursively from the stack of <see cref="Logger"/> objects, currently handled
    /// in <seealso cref="BuildLogIdStack(StringBuilder)"/>.<para/>
    /// The core methods on this object are the methods beginning with "Log", as these represent
    /// an actual logging operation.  The "Create" prefixed methods are helpers to construct a
    /// new logger.
    /// </remarks>
    public class Logger
    {
        public string LogId { get; private set; }
        private readonly string name;
        private readonly Logger parent;
        private readonly Stopwatch stopwatch;

        private Logger(string logId, string name, Logger parent = null, bool useStopwatch = false)
        {
            this.LogId = logId;
            this.name = name;
            this.parent = parent;
            if (useStopwatch)
            {
                this.stopwatch = new Stopwatch();
            }
        }

        public static Logger CreateAndStartRunLogger(string[] args)
        {
            // Mimicking the shorten guid method from the lib
            string logId = Convert.ToBase64String(Guid.NewGuid().ToByteArray())
                .Replace('/', '_')
                .Replace('+', '-')
                .TrimEnd('=');
            var logger = new Logger(logId, "Run", useStopwatch: true);
            logger.LogStart("args [" + (args.Length > 0 ? "\"" + string.Join("\", \"", args) + "\"" : null) + "]");
            return logger;
        }

        public static Logger CreateServiceMonitorFailureLogger()
        {
            return new Logger("", "ServiceMonitorLogging");
        }

        public Logger CreateReceiverLogger(string receiverName)
        {
            return new Logger(receiverName, "Receiver", this); // After discussion with ISE team, we want the receiver in the ID stack, but the log is not helpful
        }

        public Logger CreateAndStartLoanBatchLogger(int batchCount, Guid receiverAppCode, List<Guid> loanIDs, List<Dictionary<string, Field>> tasks, DateTime lastModifiedDate)
        {
            var logger = new Logger("#" + batchCount, "LoanBatch", this);
            var stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("Last modified date for this batch: " + lastModifiedDate.ToString("o"));
            stringBuilder.AppendLine(Tools.Quantify(loanIDs.Count, "loan", "loans") + " and " + Tools.Quantify(tasks.Count, "related task", "related tasks") + Environment.NewLine);

            stringBuilder.AppendLine("Loan IDs:");
            foreach (var id in loanIDs)
            {
                stringBuilder.AppendLine("  " + id);
            }

            logger.LogStart(stringBuilder.ToString());
            return logger;
        }

        internal Logger CreateAndStartEdocBatchLogger(int batchCount, int auditHistoryRecordCount, int edocRecordCount, int version)
        {
            var logger = new Logger("#" + batchCount, E_ContentType.EDocsAuditHistory.ToString() + "Batch", this);
            logger.LogStart(Tools.Quantify(auditHistoryRecordCount, "audit item", "audit items") + " on " + Tools.Quantify(edocRecordCount, "edoc", "edocs") + " beginning with version " + version);
            return logger;
        }

        public Logger CreateAndStartContentTypeLogger(E_ContentType contentType, int count, int batchSize = 0)
        {
            return this.CreateAndStartContentTypeLogger(contentType, "Sending " + Tools.Quantify(count, "record", "records") + (batchSize > 0 ? " in batches of " + batchSize : null));
        }

        public Logger CreateAndStartContentTypeLogger(E_ContentType contentType, string message)
        {
            var logger = new Logger(contentType.ToString(), contentType.ToString(), this, useStopwatch: true);
            logger.LogStart(message);
            return logger;
        }

        public void LogStart(string message)
        {
            if (this.stopwatch != null)
            {
                this.stopwatch.Start();
            }

            this.LogInfo(message, "Start " + this.name);
        }

        public void LogEnd(string message)
        {
            string timingDetails = null;
            if (this.stopwatch != null)
            {
                this.stopwatch.Stop();
                timingDetails = "Total Time: " + this.stopwatch.Elapsed + Environment.NewLine;
            }

            this.LogInfo(timingDetails + message, "End " + this.name);
        }

        public void LogInfo(string message, string category)
        {
            Trace.Write("{" + this.BuildLogIdStack() + "}" + Environment.NewLine + message, category);
        }

        public void LogResponse(string response)
        {
            this.LogInfo("Response:" + Environment.NewLine + response, "Response " + this.name);
        }

        public void LogError(Exception exception)
        {
            this.LogInfo(exception.ToString(), "Error " + this.name);
            if (exception is LQBInternalException)
            {
                this.LogError(((LQBInternalException)exception).InternalException);
            }
        }

        private StringBuilder BuildLogIdStack(StringBuilder stack = null)
        {
            ////// Once we get C# 6 working in the build process, this could be
            ////stack = stack?.Append(" | " + this.logId) ?? new StringBuilder(this.logId);
            ////return this.parent?.BuildLogIdStack(stack) ?? stack;
            stack = stack == null ? new StringBuilder(this.LogId) : stack.Append(" | " + this.LogId);
            return this.parent == null ? stack : this.parent.BuildLogIdStack(stack);
        }
    }
}
