﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SendDataToClientDB.ReceiveDataFromLQBClientService;

namespace SendDataToClientDB
{
    public class ExecutionOptions
    {
        /// <summary>
        /// A general help message for displaying when asked for help in the command prompt.
        /// </summary>
        private static readonly string GeneralHelpMessage =
            Environment.NewLine +
            "** SendDataToClientDB **" + Environment.NewLine +
            "An executable that loads and sends loan data to a client's SQL server through ReceiveDataFromLQB." + Environment.NewLine +
            Environment.NewLine +
            "Usage:" + Environment.NewLine +
            "SendDataToClientDB.exe [<receivers>...] [--send <transmission-types>...]" + Environment.NewLine +
            "    [--receivers <receivers>...] [--disable-pml-timespan] [? | --help]" + Environment.NewLine +
            Environment.NewLine +
            "Options:" + Environment.NewLine +
            Environment.NewLine +
            "--receivers <receivers>... : A list of receiver names to receive data. " + Environment.NewLine +
            "    If supplied first, the \"--receivers\" delimiter is optional. " + Environment.NewLine +
            "    Receivers are processed in the order listed." + Environment.NewLine +
            "    If no receivers are provided, they will be loaded from the app.config." + Environment.NewLine +
            Environment.NewLine +
            "--send <transmission-types>... : A list of transmission types to execute. " + Environment.NewLine +
            "    If not provided, all transmission types will be sent. " + Environment.NewLine +
            "    Recognized transmission-type values: " + string.Join(", ", Enum.GetValues(typeof(TransmissionType)).Cast<TransmissionType>().Select(value => value.ToString()).ToArray()) + "." + Environment.NewLine +
            Environment.NewLine +
            "--disable-pml-timespan : If this argument is provided, allows PML data export" + Environment.NewLine +
            "    regardless of time of day." + Environment.NewLine +
            Environment.NewLine +
            "?, --help : Display this help message." + Environment.NewLine +
            Environment.NewLine +
            "Examples:" + Environment.NewLine +
            Environment.NewLine +
            "\"SendDataToClientDB.exe PML0999\"" + Environment.NewLine +
            "- Send all data types to receiver \"PML0999\"" + Environment.NewLine +
            Environment.NewLine +
            "\"SendDataToClientDB.exe --receivers PML0987 --send EDocs\"" + Environment.NewLine +
            "- Send only EDocs data to receiver \"PML0987\"" + Environment.NewLine +
            Environment.NewLine +
            "\"SendDataToClientDB.exe --send PML EDocs --disable-pml-timespan\"" + Environment.NewLine +
            "- Send EDocs and PML data to all configured receivers." + Environment.NewLine +
            "  PML data will be sent even if the time is not after 6pm.";

        public List<string> ReceiverNames { get; private set; }

        /// <summary>
        /// Gets a set of types that should be sent to the receiver. Defaults to containing all content types.
        /// Note: Some combinations may not have the desired effect.
        /// For example: <see cref="E_ContentType.App"/> and <see cref="E_ContentType.Task"/> data can both only be sent if <see cref="E_ContentType.Loan"/> data can be sent.
        /// <see cref="E_ContentType.EDoc"/> data has the same requirement on <see cref="E_ContentType.Loan"/>, 
        ///   unless sent separately because of being included in the receiver's <see cref="Tools.ExtraOtherContent"/>.
        /// </summary>
        public HashSet<TransmissionType> SendingTypes { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the PML data 5:50PM - 12:00AM time restriction should be disabled.
        /// Disabling is useful for scheduling transmissions of only PML, OC, etc data at specified times.
        /// Keeping enabled is useful for schedules including all content types.
        /// </summary>
        public bool DisablePmlTimespan { get; private set; }

        /// <summary>
        /// Used when options like "?" or "--help" are provided, so as not to trigger a run of the program if the user is only asking for info.
        /// </summary>
        public bool InfoOnlyMode { get; private set; } = false;

        public IEnumerable<T> CaptureNextArgumentGroup<T>(Queue<string> queue, Func<string, T> transformFunction)
        {
            List<string> returnList = new List<string>();
            while (queue.Count > 0)
            {
                if (string.IsNullOrEmpty(queue.Peek()))
                {
                    queue.Dequeue();
                    continue;
                }
                else if (queue.Peek().StartsWith("-", StringComparison.OrdinalIgnoreCase)
                    || queue.Peek().StartsWith("/", StringComparison.OrdinalIgnoreCase)
                    || queue.Peek().StartsWith("?", StringComparison.OrdinalIgnoreCase))
                {
                    yield break;
                }
                else
                {
                    yield return transformFunction(queue.Dequeue());
                }
            }
        }

        /// <summary>
        /// Parses the given command line arguments into an instance of this class.
        /// </summary>
        /// <param name="commandLineArgs">
        /// The command line argument strings to parse.
        /// Format:
        /// [receivers..] [--send <see cref="E_ContentType"/>s..] [--receivers receivers..] [--disable-pml-timespan] [?]
        /// </param>
        public ExecutionOptions(string[] commandLineArgs)
        {
            this.ReceiverNames = new List<string>();
            this.SendingTypes = new HashSet<TransmissionType>();

            if (commandLineArgs == null)
            {
                commandLineArgs = new string[] { };
            }

            var argsQueue = new Queue<string>(commandLineArgs);
            // Take any arguments that come before a "-" or "/" option and add them to the receiver list.
            // This makes the command line argument model compatible with older versions.
            ReceiverNames.AddRange(CaptureNextArgumentGroup(argsQueue, arg => arg));
            while (argsQueue.Count > 0)
            {
                string optionArgument = argsQueue.Dequeue();

                // The user is asking for help information about the program. 
                // Give the information and exit early.
                if (optionArgument != null
                    && (optionArgument.Equals("?", StringComparison.OrdinalIgnoreCase)
                    || optionArgument.Contains("help")))
                {
                    Console.Out.WriteLine(GeneralHelpMessage);
                    this.InfoOnlyMode = true;
                    this.ReceiverNames = new List<string>();
                    return;
                }

                if (optionArgument != null && optionArgument.Equals("--send", StringComparison.OrdinalIgnoreCase))
                {
                    IEnumerable<TransmissionType?> contentTypes = CaptureNextArgumentGroup(argsQueue, arg => Tools.SafeParseEnum<TransmissionType>(arg));
                    foreach (TransmissionType contentType in contentTypes.Where(contentType => contentType != null))
                    {
                        SendingTypes.Add(contentType);
                    }
                }

                if (optionArgument != null && optionArgument.Equals("--receivers", StringComparison.OrdinalIgnoreCase))
                {
                    ReceiverNames.AddRange(CaptureNextArgumentGroup(argsQueue, arg => arg));
                }

                if (optionArgument != null && optionArgument.Equals("--disable-pml-timespan", StringComparison.OrdinalIgnoreCase))
                {
                    this.DisablePmlTimespan = true;
                }
            }

            // Default value: If no command-line receivers, load from config file.
            if (this.ReceiverNames.Count == 0)
            {
                this.ReceiverNames = AppSettings.RECEIVERS.ToList();
            }

            // Default value: If no command-line sending types, send all types.
            if (this.SendingTypes.Count == 0)
            {
                this.SendingTypes = new HashSet<TransmissionType>(Enum.GetValues(typeof(TransmissionType)).Cast<TransmissionType>());
            }
        }

        public override string ToString()
        {
            return "Receiver names: [\"" + string.Join("\", \"", this.ReceiverNames.ToArray()) + "\"]" + Environment.NewLine 
                + "Content Types: [\"" + string.Join("\", \"", this.SendingTypes.Select(type => type.ToString("g")).ToArray()) + "\"]";
        }

    }
}
