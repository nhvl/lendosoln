﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace SendDataToClientDB
{
    public static class LOXML
    {
        /// <summary>
        /// Retrieves the contents of a batch export response.
        /// </summary>
        /// <param name="sLOXMLResponse">The raw XML response from the batch export</param>
        /// <param name="sContents">Populates with the result if the batch export was successful or an error message otherwise.</param>
        /// <returns>True if the batch export was successful, false otherwise.</returns>
        public static bool ParseBatchExportReponseForFileContents(string sLOXMLResponse, out string sContents)
        {
            sContents = string.Empty;
            XmlDocument responseDoc = CreateXmlDoc(sLOXMLResponse);
            XmlNode xResult = responseDoc.SelectSingleNode("//result");

            string sStatus = string.Empty;

            if (xResult != null)
            {
                XmlAttribute xStatus = xResult.Attributes["status"];
                sStatus = (xStatus != null) ? xStatus.InnerText.Trim() : string.Empty;
            }

            // Determine whether to look for export contents or an error message.
            string contentsNode;
            bool batchExportSucceeeded;
            if (sStatus.Equals("COMPLETE", StringComparison.OrdinalIgnoreCase))
            {
                contentsNode = "contents";
                batchExportSucceeeded = true;
            }
            else
            {
                contentsNode = "message";
                batchExportSucceeeded = false;
            }

            // to-do: create a class container for the outputtype and any other data that might be useful
            foreach (XmlNode xResultData in xResult)
            {
                if (xResultData.Name.Equals(contentsNode, StringComparison.OrdinalIgnoreCase))
                {
                    sContents = xResultData.InnerText;
                }
                else
                {
                    continue;
                }
            }

            return batchExportSucceeeded;
        }

        /// <summary>
        /// Creates an LOXML list of loan numbers for calling the batch exporter.
        /// </summary>
        /// <param name="loanNmList">A list of loan numbers.</param>
        /// <returns>An LOXML list of loan numbers for calling the batch exporter.</returns>
        public static string CreateLOXMLListOfLoanNums(List<string> loanNmList)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Encoding = new UTF8Encoding(); // Use this instead of Encoding.UTF8 since Encoding.UTF8 includes a byte order mark (it calls new UTF8Encoding(true))
                settings.OmitXmlDeclaration = true;
                settings.ConformanceLevel = ConformanceLevel.Fragment;

                XmlWriter xmlWriter = XmlWriter.Create(memoryStream, settings);
                xmlWriter.WriteStartElement("LOXmlFormat");
                xmlWriter.WriteAttributeString("version", AppSettings.LOXML_VERSION);

                foreach (string loanNm in loanNmList)
                {
                    xmlWriter.WriteStartElement("loan");
                    xmlWriter.WriteStartElement("field");
                    xmlWriter.WriteAttributeString("id", "sLNm");
                    xmlWriter.WriteString(loanNm.ToString());
                    xmlWriter.WriteEndElement(); // </field>
                    xmlWriter.WriteEndElement(); // </loan>
                }

                xmlWriter.WriteEndElement(); // </LOXmlFormat>
                xmlWriter.Flush();
                return Encoding.UTF8.GetString(memoryStream.GetBuffer(), 0, (int)memoryStream.Position);
            }
        }

        /// <summary>
        /// Creates an LOXML list of loan IDs for calling the batch exporter.
        /// </summary>
        /// <param name="loanIDs">A dictionary in which the keys are loan IDs.</param>
        /// <returns>An LOXML list of loan IDs for calling the batch exporter.</returns>
        public static string CreateLOXMLListOfLoanIDs(Dictionary<Guid, string> loanIDs)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Encoding = new UTF8Encoding(); // Use this instead of Encoding.UTF8 since Encoding.UTF8 includes a byte order mark (it calls new UTF8Encoding(true))
                settings.OmitXmlDeclaration = true;
                settings.ConformanceLevel = ConformanceLevel.Fragment;

                XmlWriter xmlWriter = XmlWriter.Create(memoryStream, settings);
                xmlWriter.WriteStartElement("LOXmlFormat");
                xmlWriter.WriteAttributeString("version", AppSettings.LOXML_VERSION);

                foreach (KeyValuePair<Guid, string> loan in loanIDs)
                {
                    xmlWriter.WriteStartElement("loan");
                    xmlWriter.WriteStartElement("field");
                    xmlWriter.WriteAttributeString("id", "sLId");
                    xmlWriter.WriteString(loan.Key.ToString());
                    xmlWriter.WriteEndElement(); // </field>
                    xmlWriter.WriteEndElement(); // </loan>
                }

                xmlWriter.WriteEndElement(); // </LOXmlFormat>
                xmlWriter.Flush();
                return Encoding.UTF8.GetString(memoryStream.GetBuffer(), 0, (int)memoryStream.Position);
            }
        }


        /// <summary>
        /// Creates an LOXML list of loan IDs for calling the batch exporter.
        /// </summary>
        /// <param name="loanIDs">A enumeration of loan IDs.</param>
        /// <returns>An LOXML list of loan IDs for calling the batch exporter.</returns>
        public static string CreateLOXMLListOfLoanIDs(IEnumerable<Guid> loanIDs)
        {
            var el = new XElement(
                "LOXmlFormat",
                new XAttribute("version", "1.0"),
                loanIDs.Select(loanID =>
                    new XElement(
                        "loan",
                        new XElement(
                            "field",
                            new XAttribute("id", "sLId"),
                            loanID))));
            return el.ToString(SaveOptions.DisableFormatting);
        }

        #region Copied from LosUtils
        public static XmlDocument CreateXmlDoc(string xmlText)
        {
            try
            {
                return CreateXmlDocImpl(xmlText);
            }
            catch (XmlException)
            {
                xmlText = SantizeXmlString(xmlText);
                return CreateXmlDocImpl(xmlText);
            }
        }

        private static XmlDocument CreateXmlDocImpl(string xmlText)
        {
            XmlDocument xmlDoc = new XmlDocument();

            if (null == xmlText || xmlText.Trim() == "")
                return xmlDoc;

            XmlTextReader readerData = null;

            try
            {
                if (xmlText != null && xmlText.Length <= (4 * 1024))
                {
                    readerData = new XmlTextReader(xmlText, XmlNodeType.Document, null);
                }
                else
                    readerData = new XmlTextReader(new StringReader(xmlText));

                readerData.XmlResolver = null;
                xmlDoc.Load(readerData);
            }
            finally
            {
                if (null != readerData)
                    readerData.Close();
            }
            return xmlDoc;
        }

        /// <summary>
        /// Remove all illegal XML characters from string. The list of allowable characters http://www.w3.org/TR/REC-xml/#charsets
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        private static string SantizeXmlString(string xml)
        {
            StringBuilder sb = new StringBuilder(xml.Length);
            foreach (char ch in xml.ToCharArray())
            {
                if (IsLegalXmlChar(ch))
                {
                    sb.Append(ch);
                }
            }
            return sb.ToString();
        }

        private static bool IsLegalXmlChar(int ch)
        {
            //The list of allowable characters http://www.w3.org/TR/REC-xml/#charsets
            return (ch == 0x9 || ch == 0xA || ch == 0xD || (ch >= 0x20 && ch <= 0xD7FF) || (ch >= 0xE000 && ch <= 0xFFFD) || (ch >= 0x10000 && ch <= 0x10FFFF));
        }
        #endregion
    }
}