# SendDataToClientDB

This console app handles pushing data to the web service located on the client's server, defined
in [Client.asmx](../ReceiveDataFromLQB/Client.asmx).

## Running the Executable
The executable runs with arguments specifying the names of the receivers who will be sent data.
If no names are specified, it falls back on the list of receivers specified in
[App.config](App.config).  It runs for each receiver in sequence, so the
production task that runs a separate instance of the executable for each receiver, e.g.

> SendDataToClientDB.exe PML0000

While we could specify several receiver names here (e.g.
`SendDataToClientDB.exe PML0001 PML0002 PML0003`), running them separately provides a simple
parallel processing.  We switched to this method when the size of the service data grew and
it became common for the service to run nearly continuously (for several hours) for a single client.
