﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SendDataToClientDB.ReceiveDataFromLQBClientService;

namespace SendDataToClientDB
{
    /// <summary>
    /// Represents the different bundled transmission types that can be sent by the service.
    /// Each value represents a grouping of content types that are logically grouped together and should be sent or omitted together.
    /// Related to <see cref="E_ContentType"/>, but not as specific.
    /// </summary>
    public enum TransmissionType
    {
        /// <summary>
        /// Represents the transmission of the following content types:
        /// <para>- Loan (<see cref="E_ContentType.Loan"/>)</para>
        /// <para>- App (<see cref="E_ContentType.App"/>)</para>
        /// <para>- Task (<see cref="E_ContentType.Task"/>)</para>
        /// </summary>
        Loan,

        /// <summary>
        /// Represents the transmission of EDocs (<see cref="E_ContentType.EDoc"/>)
        /// and if applicable, EDocs Audit History (<see cref="E_ContentType.EDocsAuditHistory"/>)
        /// and/or EDoc-Task association (<see cref="E_ContentType.TaskEDocAssociation"/>) data.
        /// </summary>
        EDocs,

        /// <summary>
        /// Represents the transmission of PML-related content types:
        /// <list type="bullet">
        ///     <item><description>PML User (<see cref="E_ContentType.PMLUser"/>).</description></item>
        ///     <item><description>Originating Company (<see cref="E_ContentType.OriginatingCompany"/>).</description></item>
        /// </list>
        /// <para></para>
        /// <para>- Originating Company (<see cref="E_ContentType.OriginatingCompany"/>)</para>
        /// <para>- Originating company relationships (<see cref="E_ContentType.OriginatingCompanyRelationship"/>)</para>
        /// </summary>
        PML,

        /// <summary>
        /// Represents the transmission of "B"-user (lender-level) employee content types:
        /// - Employee (<see cref="E_ContentType.Employee"/> and <see cref="E_ContentType.Employee2"/>)
        /// </summary>
        Employee,

        /// <summary>
        /// Represents the transmission of Mortgage Pool (<see cref="E_ContentType.MortgagePool"/>) data.
        /// </summary>
        MortgagePool
    }
}
