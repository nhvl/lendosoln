﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SendDataToClientDB.com.lendingqb.AuthService;
using SendDataToClientDB.com.lendingqb.EDocs;
using SendDataToClientDB.com.lendingqb.Employee;
using SendDataToClientDB.com.lendingqb.Reporting;

namespace SendDataToClientDB
{
    public static class LqbWebservices
    {
        /// <summary>
        /// Allows interaction with the reporting service.
        /// </summary>
        private static Reporting reportingService;

        /// <summary>
        /// Allows interaction with the employee service.
        /// </summary>
        private static Employee employeeService;

        /// <summary>
        /// Allows interaction with the EDocs service.
        /// </summary>
        private static EDocsService eDocsService;

        /// <summary>
        /// Gets a SOAP protocol to allow interaction with the authentication service.
        /// </summary>
        /// <remarks>
        /// Client certificates can modify this instance so no caching.
        /// </remarks>
        public static AuthService AuthenticationService
        {
            get
            {
                return new AuthService() { Url = AppSettings.WEBSERVICE_DOMAIN + "AuthService.asmx" };
            }
        }

        /// <summary>
        /// Gets a SOAP protocol to allow interaction with the reporting service.
        /// </summary>
        public static Reporting ReportingService
        {
            get
            {
                if (reportingService == null)
                {
                    reportingService = new Reporting() { Url = AppSettings.WEBSERVICE_DOMAIN + "Reporting.asmx" };
                }

                return reportingService;
            }
        }

        /// <summary>
        /// Gets a SOAP protocol to allow interaction with the employee service.
        /// </summary>
        public static Employee EmployeeService
        {
            get
            {
                if (employeeService == null)
                {
                    employeeService = new Employee() { Url = AppSettings.WEBSERVICE_DOMAIN + "Employee.asmx" };
                }

                return employeeService;
            }
        }

        /// <summary>
        /// Gets a SOAP protocol to allow interaction with the EDocs service.
        /// </summary>
        public static EDocsService EDocsService
        {
            get
            {
                if (eDocsService == null)
                {
                    eDocsService = new EDocsService() { Url = AppSettings.WEBSERVICE_DOMAIN + "EDocsService.asmx" };
                }

                return eDocsService;
            }
        }
    }
}
