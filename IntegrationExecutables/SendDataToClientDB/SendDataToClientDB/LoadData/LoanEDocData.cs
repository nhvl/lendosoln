﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SendDataToClientDB.LoadData
{
    public class LoanEDocData : LoadAbstractData
    {
        public LoanEDocData(string receiverName)
            : base(receiverName)
        {
            this.fieldList.Add("DocumentId", DBTypes.SDBuniqueidentifier);
            #region Destination table: EDoc
            this.fieldList.Add("sLId", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("aAppId", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("CreatedDate", DBTypes.DBdatetime);
            this.fieldList.Add("DocTypeId", DBTypes.SDBint);
            this.fieldList.Add("DocTypeName", DBTypes.DBvarchar);
            this.fieldList.Add("DocTypeIsValid", DBTypes.DBbit);
            this.fieldList.Add("InternalDescription", DBTypes.SDBvarchar);
            this.fieldList.Add("IsValid", DBTypes.SDBbit);
            this.fieldList.Add("FolderId", DBTypes.DBint);
            this.fieldList.Add("FolderName", DBTypes.DBvarchar);
            this.fieldList.Add("LastModifiedDate", DBTypes.DBdatetime);
            this.fieldList.Add("NumPages", DBTypes.SDBint);
            this.fieldList.Add("PublicDescription", DBTypes.SDBvarchar);
            this.fieldList.Add("Status", DBTypes.SDBtinyint);
            this.fieldList.Add("StatusDescription", DBTypes.SDBvarchar);
            #endregion

            // can provide different sets of fields to the client per their need, e.g. entire cache vs. secondary fields only
        }
    }
}
