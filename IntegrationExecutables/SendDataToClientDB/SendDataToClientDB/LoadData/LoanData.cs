using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace SendDataToClientDB.LoadData
{
    public class LoanData : LoadAbstractData
    {
        const string LastModifiedDateFieldName = "LastModifiedD";
        public LoanData(string receiverName)
            : base(receiverName)
        {
            this.fieldList.Add("sLId", DBTypes.SDBuniqueidentifier);
            this.fieldList.Add(LastModifiedDateFieldName, DBTypes.DBdatetime);

            // Since these go through the Batch Exporter and are not output to clients directly, do not add client-specific data
        }

        protected override Field CreateField(string fieldName, DBTypes dbType, SqlDataReader sqlReader)
        {
            if (dbType == DBTypes.DBdatetime && fieldName == LastModifiedDateFieldName)
            {
                return new Field(DBConvert.GetNullableDateTimeAsStringWithFullPrecision(fieldName, sqlReader), dbType);
            }

            return base.CreateField(fieldName, dbType, sqlReader);
        }
    }
}
