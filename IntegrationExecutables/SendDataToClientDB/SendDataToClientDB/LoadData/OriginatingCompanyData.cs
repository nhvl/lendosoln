﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SendDataToClientDB.LoadData
{
    public class OriginatingCompanyData : LoadAbstractData
    {
        public OriginatingCompanyData(string receiverName) : base(receiverName)
        {
            this.fieldList.Add("PmlBrokerId", DBTypes.SDBuniqueidentifier);
            #region Destination table: Originating_Company
            this.fieldList.Add("CompanyId", DBTypes.SDBvarchar);
            this.fieldList.Add("Name", DBTypes.SDBvarchar);
            this.fieldList.Add("Addr", DBTypes.SDBvarchar);
            this.fieldList.Add("City", DBTypes.SDBvarchar);
            this.fieldList.Add("State", DBTypes.SDBchar);
            this.fieldList.Add("Zip", DBTypes.SDBchar);
            this.fieldList.Add("Phone", DBTypes.SDBvarchar);
            this.fieldList.Add("Fax", DBTypes.SDBvarchar);
            this.fieldList.Add("TaxId", DBTypes.SDBvarchar);
            this.fieldList.Add("NmlsIdentifier", DBTypes.SDBvarchar);
            this.fieldList.Add("IsActive", DBTypes.SDBbit);
            this.fieldList.Add("LicenseXmlContent", DBTypes.DBvarchar);
            this.fieldList.Add("PrincipalName1", DBTypes.SDBvarchar);
            this.fieldList.Add("PrincipalName2", DBTypes.SDBvarchar);
            this.fieldList.Add("OriginatorCompensationPercent", DBTypes.SDBdecimal);
            this.fieldList.Add("OriginatorCompensationBaseT", DBTypes.SDBint);
            this.fieldList.Add("OriginatorCompensationMaxAmount", DBTypes.SDBmoney);
            this.fieldList.Add("OriginatorCompensationMinAmount", DBTypes.SDBmoney);
            this.fieldList.Add("OriginatorCompensationFixedAmount", DBTypes.SDBmoney);
            this.fieldList.Add("OriginatorCompensationNotes", DBTypes.SDBvarchar);
            this.fieldList.Add("OriginatorCompensationLastModifiedDate", DBTypes.DBdatetime);
            this.fieldList.Add("OriginatorCompensationAuditXml", DBTypes.SDBvarchar);
            this.fieldList.Add("CustomPricingPolicyField1", DBTypes.SDBvarchar);
            this.fieldList.Add("CustomPricingPolicyField2", DBTypes.SDBvarchar);
            this.fieldList.Add("CustomPricingPolicyField3", DBTypes.SDBvarchar);
            this.fieldList.Add("CustomPricingPolicyField4", DBTypes.SDBvarchar);
            this.fieldList.Add("CustomPricingPolicyField5", DBTypes.SDBvarchar);
            this.fieldList.Add("BranchId", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("LpePriceGroupId", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("CorrespondentBranchId", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("CorrespondentLpePriceGroupId", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("MiniCorrespondentBranchId", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("MiniCorrespondentLpePriceGroupId", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("Roles", DBTypes.DBvarchar);
            this.fieldList.Add("UnderwritingAuthority", DBTypes.DBint);
            this.fieldList.Add("BrokerRoleStatusT", DBTypes.DBint);
            this.fieldList.Add("MiniCorrRoleStatusT", DBTypes.DBint);
            this.fieldList.Add("CorrRoleStatusT", DBTypes.DBint);
            this.fieldList.Add("PmlCompanyTierId", DBTypes.DBint);
            #endregion

            // can provide different sets of fields to the client per their need, e.g. entire cache vs. secondary fields only
        }
    }
}
