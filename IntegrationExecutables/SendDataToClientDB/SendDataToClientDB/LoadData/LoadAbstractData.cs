﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace SendDataToClientDB.LoadData
{
    public interface ILoadData
    {
        Dictionary<string, Field> LoadData(SqlDataReader SQLReader);
    }

    public abstract class LoadAbstractData : ILoadData
    {
        protected string ReceiverName { get; private set; }
        protected Dictionary<string, DBTypes> fieldList;

        public LoadAbstractData(string receiverName)
        {
            this.ReceiverName = receiverName;
            this.fieldList = new Dictionary<string, DBTypes>(StringComparer.OrdinalIgnoreCase);
        }

        public virtual Dictionary<string, Field> LoadData(SqlDataReader sqlReader)
        {
            var dataBook = new Dictionary<string, Field>(StringComparer.OrdinalIgnoreCase);
            foreach (var field in this.fieldList)
            {
                dataBook.Add(field.Key, this.CreateField(field.Key, field.Value, sqlReader));
            }

            return dataBook;
        }

        protected virtual Field CreateField(string fieldName, DBTypes dbType, SqlDataReader sqlReader)
        {
            return new Field(DBConvert.GetDBObjectAsString(fieldName, sqlReader), dbType);
        }
    }
}
