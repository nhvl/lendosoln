﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SendDataToClientDB.LoadData
{
    public class EmployeeRoleAssignmentData : LoadAbstractData
    {
        public EmployeeRoleAssignmentData(string receiverName)
            : base(receiverName)
        {
            #region Destination table: ROLE_ASSIGNMENT
            this.fieldList.Add("EmployeeId", DBTypes.SDBuniqueidentifier);
            this.fieldList.Add("RoleId", DBTypes.SDBuniqueidentifier);
            #endregion
        }
    }
}
