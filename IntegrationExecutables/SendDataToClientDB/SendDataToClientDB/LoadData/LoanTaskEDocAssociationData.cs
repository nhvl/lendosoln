﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SendDataToClientDB.LoadData
{
    public class LoanTaskEDocAssociationData : LoadAbstractData
    {
        public LoanTaskEDocAssociationData(string receiverName)
            : base(receiverName)
        {
            // We need to construct TaskId_DocumentId manually
            ////this.fieldList.Add("TaskId_DocumentId", DBTypes.SDBvarchar);
            #region Destination table: Task_EDoc_Association
            this.fieldList.Add("DocumentId", DBTypes.SDBuniqueidentifier);
            this.fieldList.Add("TaskId", DBTypes.SDBvarchar);
            this.fieldList.Add("sLId", DBTypes.SDBuniqueidentifier);
            #endregion
        }

        public override Dictionary<string, Field> LoadData(System.Data.SqlClient.SqlDataReader sqlReader)
        {
            var data = base.LoadData(sqlReader);
            data.Add("TaskId_DocumentId", new Field(data["TaskId"].Value + '_' + data["DocumentId"].Value, DBTypes.SDBvarchar));
            return data;
        }
    }
}
