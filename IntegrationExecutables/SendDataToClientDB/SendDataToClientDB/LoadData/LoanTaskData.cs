﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SendDataToClientDB.LoadData
{
    public class LoanTaskData : LoadAbstractData
    {
        public LoanTaskData(string receiverName) : base(receiverName)
        {
            this.fieldList.Add("LoanId", DBTypes.SDBuniqueidentifier);
            this.fieldList.Add("TaskId", DBTypes.DBvarchar);
            #region Destination table: Task
            this.fieldList.Add("BorrowerNmCached", DBTypes.SDBvarchar);
            this.fieldList.Add("CondCategoryId", DBTypes.DBint);
            this.fieldList.Add("CondDeletedByUserNm", DBTypes.SDBvarchar);
            this.fieldList.Add("CondDeletedDate", DBTypes.DBdatetime);
            this.fieldList.Add("CondInternalNotes", DBTypes.SDBvarchar);
            this.fieldList.Add("CondIsDeleted", DBTypes.SDBbit);
            this.fieldList.Add("CondIsHidden", DBTypes.SDBbit);
            this.fieldList.Add("CondMessageId", DBTypes.DBvarchar);
            this.fieldList.Add("LoanNumCached", DBTypes.SDBvarchar);
            this.fieldList.Add("TaskAssignedUserFullName", DBTypes.SDBvarchar);
            this.fieldList.Add("TaskAssignedUserId", DBTypes.SDBuniqueidentifier);
            this.fieldList.Add("TaskClosedDate", DBTypes.DBdatetime);
            this.fieldList.Add("TaskClosedUserFullName", DBTypes.SDBvarchar);
            this.fieldList.Add("TaskClosedUserId", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("TaskCreatedByUserId", DBTypes.SDBuniqueidentifier);
            this.fieldList.Add("TaskCreatedDate", DBTypes.SDBsmalldatetime);
            this.fieldList.Add("TaskDueDate", DBTypes.DBsmalldatetime);
            this.fieldList.Add("TaskDueDateCalcDays", DBTypes.SDBint);
            this.fieldList.Add("TaskDueDateCalcField", DBTypes.SDBvarchar);
            this.fieldList.Add("TaskDueDateLocked", DBTypes.SDBbit);
            this.fieldList.Add("TaskFollowUpDate", DBTypes.DBsmalldatetime);
            this.fieldList.Add("TaskHistoryXml", DBTypes.SDBvarchar);
            this.fieldList.Add("TaskIsCondition", DBTypes.SDBbit);
            this.fieldList.Add("TaskLastModifiedDate", DBTypes.SDBsmalldatetime);
            this.fieldList.Add("TaskOwnerFullName", DBTypes.SDBvarchar);
            this.fieldList.Add("TaskOwnerUserId", DBTypes.SDBuniqueidentifier);
            this.fieldList.Add("TaskPermissionLevelId", DBTypes.SDBint);
            this.fieldList.Add("TaskResolvedUserId", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("TaskStatus", DBTypes.SDBtinyint);
            this.fieldList.Add("TaskSubject", DBTypes.SDBvarchar);
            #endregion

            // can provide different sets of fields to the client per their need, e.g. entire cache vs. secondary fields only
        }
    }
}
