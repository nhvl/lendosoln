﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using SendDataToClientDB.com.lendingqb.EDocs;
using SendDataToClientDB.ReceiveDataFromLQBClientService;

namespace SendDataToClientDB.LoadData.WebServices
{
    /// <summary>
    /// Provides methods for interacting with the LendingQB EDocsService web service.
    /// </summary>
    internal class EDocsServiceProvider : LQBWebServiceProvider
    {
        /// <summary>
        /// Enumerates the possible field names that may be returned from the <see cref="EDocsService.ListEDocsAuditHistory(string, string, int)"/> web method.
        /// </summary>
        public enum EDocsAuditHistoryFieldId
        {
            /// <summary>
            /// Document ID.
            /// </summary>
            DocumentId = 0,

            /// <summary>
            /// ID of User who modified the doc.
            /// </summary>
            ModifiedByUserId = 1,

            /// <summary>
            /// Version number in EDocs audit history table.
            /// </summary>
            Version = 2,

            /// <summary>
            /// EDoc modified date.
            /// </summary>
            ModifiedDate = 3,

            /// <summary>
            /// Change Description.
            /// </summary>
            Description = 4,

            /// <summary>
            /// Change Details.
            /// </summary>
            Details = 5
        }

        /// <summary>
        /// Contains a mapping from field ID to database type for EDocs data in the "dbo.EDocs" table on the receiver side.
        /// </summary>
        public static readonly IDictionary<EDocsFieldId, DBTypes> EDocsTableFieldTypes = new Dictionary<EDocsFieldId, DBTypes>()
        {
            { EDocsFieldId.DocumentId, DBTypes.SDBuniqueidentifier },
            { EDocsFieldId.sLId, DBTypes.DBuniqueidentifier },
            { EDocsFieldId.aAppId, DBTypes.DBuniqueidentifier },
            { EDocsFieldId.CreatedDate, DBTypes.DBdatetime },
            { EDocsFieldId.DocTypeId, DBTypes.SDBint },
            { EDocsFieldId.DocTypeName, DBTypes.DBvarchar },
            { EDocsFieldId.DocTypeIsValid, DBTypes.DBbit },
            { EDocsFieldId.InternalDescription, DBTypes.SDBvarchar },
            { EDocsFieldId.IsValid, DBTypes.SDBbit },
            { EDocsFieldId.FolderId, DBTypes.DBint },
            { EDocsFieldId.FolderName, DBTypes.DBvarchar },
            { EDocsFieldId.LastModifiedDate, DBTypes.DBdatetime },
            { EDocsFieldId.NumPages, DBTypes.SDBint },
            { EDocsFieldId.PublicDescription, DBTypes.SDBvarchar },
            { EDocsFieldId.Status, DBTypes.SDBtinyint },
            { EDocsFieldId.StatusDescription, DBTypes.SDBvarchar }
        };

        /// <summary>
        /// Contains a mapping from field ID to database type for EDocs data in the "dbo.EDocs_Audit_History" table on the receiver side.
        /// </summary>
        public static readonly IDictionary<EDocsAuditHistoryFieldId, DBTypes> EDocsAuditHistoryTableFieldTypes = new Dictionary<EDocsAuditHistoryFieldId, DBTypes>()
        {
            { EDocsAuditHistoryFieldId.DocumentId, DBTypes.SDBuniqueidentifier },
            { EDocsAuditHistoryFieldId.ModifiedByUserId, DBTypes.DBuniqueidentifier },
            { EDocsAuditHistoryFieldId.Version, DBTypes.SDBint },
            { EDocsAuditHistoryFieldId.ModifiedDate, DBTypes.SDBdatetime },
            { EDocsAuditHistoryFieldId.Description, DBTypes.SDBvarchar },
            { EDocsAuditHistoryFieldId.Details, DBTypes.SDBvarchar }
        };

        /// <summary>
        /// Initializes the EDocs Service Provider.
        /// </summary>
        /// <param name="receiver">The receiver name that will be used for loading the data, and sending the loaded data to the receiver.</param>
        public EDocsServiceProvider(string receiver) : base(receiver)
        {
        }

        /// <summary>
        /// A secret key to authenticate the service with LendingQB Web Services.
        /// </summary>
        protected override string SecretKey { get { return "peqOIaCzVkpcQkPqHyzT"; } }

        /// <summary>
        /// Gets the EDocs records for the "EDocsAuditHistory" and "EDoc" table from LQB web services for sending to the client's DB.
        /// The Audit History data is loaded first, which determines which Document IDs to load from the 
        /// </summary>
        /// <param name="version">The version to pass to LQB for the earliest version number among EDocs Audit History records to send.</param>
        /// <returns>An object containing the EDocsAuditHistory data returned and the EDocs data returned from LQB web services.</returns>
        public EDocsContent FetchEDocsContentExports(int version, XmlLogger xmlLogger)
        {
            // Need to load Audit History data before the EDocs core data so we know which DocumentIDs to query for.
            LQBData<EDocsAuditHistoryFieldId> auditHistoryData =
                LoadLQBDataAndAddDbTypes(
                    (authTicket) => LqbWebservices.EDocsService.ListEDocsAuditHistory(authTicket, this.SecretKey, version),
                    EDocsAuditHistoryTableFieldTypes,
                    E_ContentType.EDocsAuditHistory,
                    xmlLogger);

            IEnumerable<Guid> documentIdsToLoad = auditHistoryData.Records.Select(dict => new Guid(dict[EDocsAuditHistoryFieldId.DocumentId].Value)).Distinct();

            LQBData<EDocsFieldId> eDocsData =
                LoadLQBDataAndAddDbTypes(
                    (authTicket) => LqbWebservices.EDocsService.ListEDocsByDocIds(authTicket, this.SecretKey, documentIdsToLoad.ToArray(), EDocsTableFieldTypes.Keys.Cast<com.lendingqb.EDocs.EDocsFieldId>().ToArray()),
                    EDocsTableFieldTypes,
                    E_ContentType.EDoc,
                    xmlLogger);

            return new EDocsContent(eDocsData, auditHistoryData);
        }

        /// <summary>
        /// Contains EDocs and EDocs Audit History data loaded from LQB web services.
        /// </summary>
        public struct EDocsContent
        {
            public LQBData<EDocsFieldId> EDocsData;
            public LQBData<EDocsAuditHistoryFieldId> EDocsAuditHistoryData;

            public EDocsContent(LQBData<EDocsFieldId> eDocs, LQBData<EDocsAuditHistoryFieldId> auditHistory)
            {
                this.EDocsData = eDocs;
                this.EDocsAuditHistoryData = auditHistory;
            }
        }
    }
}
