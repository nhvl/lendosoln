﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using SendDataToClientDB.com.lendingqb.Employee;
using SendDataToClientDB.ReceiveDataFromLQBClientService;

namespace SendDataToClientDB
{
    internal class EmployeeService : LQBWebServiceProvider
    {
        /// <summary>
        /// Contains a mapping from field ID to database type for Employee data in the "dbo.EMPLOYEE" table on the receiver side.
        /// </summary>
        public static readonly IDictionary<EmployeeFieldId, DBTypes> Employee1FieldTypes = new Dictionary<EmployeeFieldId, DBTypes>()
        {
            {EmployeeFieldId.EmployeeId, DBTypes.SDBuniqueidentifier},
            {EmployeeFieldId.BranchId, DBTypes.DBuniqueidentifier},
            {EmployeeFieldId.UserFirstNm, DBTypes.DBvarchar},
            {EmployeeFieldId.UserLastNm, DBTypes.DBvarchar},
            {EmployeeFieldId.IsActive, DBTypes.DBbit},
            {EmployeeFieldId.Addr, DBTypes.DBvarchar},
            {EmployeeFieldId.City, DBTypes.DBvarchar},
            {EmployeeFieldId.State, DBTypes.DBvarchar},
            {EmployeeFieldId.Zip, DBTypes.DBvarchar},
            {EmployeeFieldId.Phone, DBTypes.DBvarchar},
            {EmployeeFieldId.Fax, DBTypes.DBvarchar},
            {EmployeeFieldId.StartD, DBTypes.DBsmalldatetime},
            {EmployeeFieldId.NewOnlineLoanEventNotifOptionT, DBTypes.DBint},
            {EmployeeFieldId.Email, DBTypes.DBvarchar},
            {EmployeeFieldId.NotesByEmployer, DBTypes.DBtext},
            {EmployeeFieldId.EmployeeUserId, DBTypes.DBuniqueidentifier},
            {EmployeeFieldId.EmployeeLicenseNumber, DBTypes.DBvarchar},
            {EmployeeFieldId.CommissionPointOfLoanAmount, DBTypes.DBdecimal},
            {EmployeeFieldId.CommissionPointOfGrossProfit, DBTypes.DBdecimal},
            {EmployeeFieldId.CommissionMinBase, DBTypes.DBmoney},
            {EmployeeFieldId.LicenseXmlContent, DBTypes.DBvarchar},
            {EmployeeFieldId.CellPhone, DBTypes.DBvarchar},
            {EmployeeFieldId.Pager, DBTypes.DBvarchar},
            {EmployeeFieldId.NmlsIdentifier, DBTypes.DBvarchar},
            {EmployeeFieldId.OriginatorCompensationAuditXml, DBTypes.DBvarchar},
            {EmployeeFieldId.OriginatorCompensationPercent, DBTypes.DBdecimal},
            {EmployeeFieldId.OriginatorCompensationBaseT, DBTypes.DBint},
            {EmployeeFieldId.OriginatorCompensationMinAmount, DBTypes.DBmoney},
            {EmployeeFieldId.OriginatorCompensationMaxAmount, DBTypes.DBmoney},
            {EmployeeFieldId.OriginatorCompensationFixedAmount, DBTypes.DBmoney},
            {EmployeeFieldId.OriginatorCompensationNotes, DBTypes.DBvarchar},
            {EmployeeFieldId.OriginatorCompensationLastModifiedDate, DBTypes.DBdatetime},
            {EmployeeFieldId.TaskRelatedEmailOptionT, DBTypes.DBint},
            {EmployeeFieldId.OriginatorCompensationSetLevelT, DBTypes.DBint},
            {EmployeeFieldId.EmployeeIDInCompany, DBTypes.DBvarchar},
            {EmployeeFieldId.EmployeeStartD, DBTypes.DBsmalldatetime},
            {EmployeeFieldId.EmployeeTerminationD, DBTypes.DBsmalldatetime},
            {EmployeeFieldId.CustomPricingPolicyField1Fixed, DBTypes.DBvarchar},
            {EmployeeFieldId.CustomPricingPolicyField2Fixed, DBTypes.DBvarchar},
            {EmployeeFieldId.CustomPricingPolicyField3Fixed, DBTypes.DBvarchar},
            {EmployeeFieldId.CustomPricingPolicyField4Fixed, DBTypes.DBvarchar},
            {EmployeeFieldId.CustomPricingPolicyField5Fixed, DBTypes.DBvarchar},
            {EmployeeFieldId.CustomPricingPolicyFieldSource, DBTypes.DBint},
            {EmployeeFieldId.ChumsId, DBTypes.DBvarchar},
            {EmployeeFieldId.CanBeMemberOfMultipleTeams, DBTypes.DBbit},
            {EmployeeFieldId.UserMiddleNm, DBTypes.DBvarchar},
            {EmployeeFieldId.UserSuffix, DBTypes.DBvarchar},
            {EmployeeFieldId.UserNmOnLoanDocs, DBTypes.DBvarchar},
            {EmployeeFieldId.UserNmOnLoanDocsLckd, DBTypes.DBbit},
            {EmployeeFieldId.FavoritePageIdsJSON, DBTypes.DBvarchar},
            {EmployeeFieldId.CanContactSupport, DBTypes.DBbit},
            {EmployeeFieldId.LoginNm, DBTypes.DBvarchar},
            {EmployeeFieldId.NeedToAcceptLatestAgreement, DBTypes.DBbit},
            {EmployeeFieldId.PasswordExpirationD, DBTypes.DBsmalldatetime},
            {EmployeeFieldId.PasswordExpirationPeriod, DBTypes.DBint},
            {EmployeeFieldId.RecentLoginD, DBTypes.DBdatetime},
            {EmployeeFieldId.IsActiveDirectoryUser, DBTypes.DBbit}
        };

        /// <summary>
        /// Contains a mapping from field ID to database type for Employee data in the "dbo.EMPLOYEE_2" table on the receiver side.
        /// </summary>
        public static readonly IDictionary<EmployeeFieldId, DBTypes> Employee2FieldTypes = new Dictionary<EmployeeFieldId, DBTypes>()
        {
            {EmployeeFieldId.UserId, DBTypes.SDBuniqueidentifier },
            {EmployeeFieldId.EmployeeId, DBTypes.SDBuniqueidentifier },
            {EmployeeFieldId.Permissions, DBTypes.DBchar},
            {EmployeeFieldId.IsAccountOwner, DBTypes.DBbit},
            {EmployeeFieldId.ByPassBgCalcForGfeAsDefault, DBTypes.DBbit},
            {EmployeeFieldId.LenderAccExecEmployeeId, DBTypes.DBuniqueidentifier},
            {EmployeeFieldId.LastUsedCreditProtocolId, DBTypes.DBuniqueidentifier},
            {EmployeeFieldId.LockDeskEmployeeId, DBTypes.DBuniqueidentifier},
            {EmployeeFieldId.SelectedPipelineCustomReportId, DBTypes.DBuniqueidentifier},
            {EmployeeFieldId.ManagerEmployeeId, DBTypes.DBuniqueidentifier},
            {EmployeeFieldId.LpePriceGroupId, DBTypes.DBuniqueidentifier},
            {EmployeeFieldId.UnderwriterEmployeeId, DBTypes.DBuniqueidentifier},
            {EmployeeFieldId.ProcessorEmployeeId, DBTypes.DBuniqueidentifier},
            {EmployeeFieldId.LpeRsExpirationByPassPermission, DBTypes.DBvarchar},
            {EmployeeFieldId.MustLogInFromTheseIpAddresses, DBTypes.DBvarchar},
            {EmployeeFieldId.IsQuickPricerEnabled, DBTypes.DBbit},
            {EmployeeFieldId.LastModifiedDate, DBTypes.DBdatetime },
            {EmployeeFieldId.AssignedPrintGroups, DBTypes.DBvarchar},
            {EmployeeFieldId.JuniorUnderwriterEmployeeID, DBTypes.DBuniqueidentifier},
            {EmployeeFieldId.JuniorProcessorEmployeeID, DBTypes.DBuniqueidentifier},
            {EmployeeFieldId.ShowQMStatusInPml2, DBTypes.DBbit},
            {EmployeeFieldId.LoanOfficerAssistantEmployeeID, DBTypes.DBuniqueidentifier},
            {EmployeeFieldId.EnabledIpRestriction, DBTypes.DBbit},
            {EmployeeFieldId.EnabledMultiFactorAuthentication, DBTypes.DBbit},
            {EmployeeFieldId.EnabledClientDigitalCertificateInstall, DBTypes.DBbit},
            {EmployeeFieldId.UsePml2AsQuickPricer, DBTypes.DBbit},
            {EmployeeFieldId.SupportEmail, DBTypes.DBvarchar},
            {EmployeeFieldId.SupportEmailVerified, DBTypes.DBbit}
        };

        /// <summary>
        /// Initializes the Employee service.
        /// </summary>
        /// <param name="receiver">The name of the receiver for which data is being loaded.</param>
        public EmployeeService(string receiver) : base(receiver)
        {
        }

        /// <summary>
        /// The secret key used by LQB webservices to verify that it's being queried by an LQB service.
        /// </summary>
        protected override string SecretKey { get { return "5EJgr8sACCgbJKes00hm"; } }

        /// <summary>
        /// Gets the Employee records for both the "EMPLOYEE" AND "EMPLOYEE_2" table from LQB web services as a single export.
        /// </summary>
        /// <param name="lastModifiedDateTime">The lastModifiedDateTime to pass to LQB for the earliest last modified date among employees to send.</param>
        public LQBData<EmployeeFieldId> FetchEmployeeContentExports(DateTime? lastModifiedDateTime, XmlLogger xmlLogger)
        {
            IDictionary<EmployeeFieldId, DBTypes> fieldTypeLookup = Employee1FieldTypes.Union(Employee2FieldTypes).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
            EmployeeFieldId[] fieldsToRetrieve = fieldTypeLookup.Keys.ToArray();

            return LoadLQBDataAndAddDbTypes((authTicket) => LqbWebservices.EmployeeService.ExportEmployees(authTicket, this.SecretKey, fieldsToRetrieve, lastModifiedDateTime), fieldTypeLookup, E_ContentType.Employee, xmlLogger);
        }
    }
}