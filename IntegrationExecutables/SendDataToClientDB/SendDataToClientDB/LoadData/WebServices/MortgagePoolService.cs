﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SendDataToClientDB.LoadData.WebServices
{
    internal class MortgagePoolService : LQBWebServiceProvider
    {
        /// <summary>
        /// Not used for this webservice.
        /// </summary>
        protected override string SecretKey => null;

        public enum MortgagePoolFieldId
        {
            PoolId,
            BrokerId,
            OpenedD,
            DeliveredD,
            ClosedD,
            IssueD,
            SettlementD,
            InitialPmtD,
            UnpaidBalanceD,
            MaturityD,
            AgencyT,
            BasePoolNumber,
            IssueTypeCode,
            PoolTypeCode,
            IssuerId,
            CustodianId,
            SecurityR,
            PoolTerm,
            AmortizationMethodT,
            AmortizationT,
            SecurityRMargin,
            SecurityChangeD,
            PoolIndexT,
            IsBondFinancePool,
            CertAndAgreementT,
            IsFormHUD11711ASendToDocumentCustodian,
            IsEnabled,
            PIAccountNum,
            PIRoutingNum,
            SearchMinimumStatusT,
            InvestorName,
            CommitmentType,
            SettlementType,
            DeliveryType,
            CommitmentNum,
            CommitmentDate,
            CommitmentExpires,
            CommitmentBasisLckd,
            CommitmentBasis,
            Tolerance,
            BasePricingCalcType,
            BasePricingPoolLevelLckd,
            BasePricingPoolLevelManual,
            BasePricingManual,
            BasePricingTable,
            LLPAType,
            LLPAManual,
            SRPType,
            SRPManual,
            SRPPoolLevel,
            BlendPairOffCalcType,
            BlendPairOffMarketPrice,
            BookEntryD,
            PoolPrefix,
            PoolSuffix,
            MortgageT,
            SellerID,
            ServicerID,
            DocumentCustodianID,
            RemittanceD,
            StructureT,
            FeatureCodes,
            OwnershipPc,
            IsInterestOnly,
            IsBalloon,
            IsAssumable,
            ARMPlanNum,
            RoundingT,
            InterestRateChangeLookbackDays,
            MBSMarginPc,
            AccrualRateStructureType,
            FixedServicingFeePc,
            MinAccrualR,
            MaxAccrualR,
            PoolTradeNumbers,
            InternalId,
            EarlyDeliveryDate,
            AssignDate,
            ConfirmDate,
            Notes,
            TaxAndInsuranceAccountNumber,
            TaxAndInsuranceRoutingNumber,
            SettlementAccountNumber1,
            SettlementRoutingNumber1,
            SettlementABAName1,
            SettlementThirdPartyAccountName1,
            SettlementAccountNumber2,
            SettlementRoutingNumber2,
            SettlementABAName2,
            SettlementThirdPartyAccountName2,
            GovernmentBondFinance,
            GovernmentBondFinancingProgramType,
            GovernmentBondFinancingProgramName,
            SecurityInvestorOriginalSubscriptionAmount1,
            SecurityInvestorOriginalSubscriptionAmount2,
            TaxId,
            UniqueId
        }

        public static readonly IDictionary<MortgagePoolFieldId, DBTypes> MortgagePoolFieldTypes = new Dictionary<MortgagePoolFieldId, DBTypes>()
        {
            {MortgagePoolFieldId.PoolId, DBTypes.SDBvarchar},
            {MortgagePoolFieldId.OpenedD, DBTypes.DBdatetime},
            {MortgagePoolFieldId.DeliveredD, DBTypes.DBdatetime},
            {MortgagePoolFieldId.ClosedD, DBTypes.DBdatetime},
            {MortgagePoolFieldId.IssueD, DBTypes.DBdatetime},
            {MortgagePoolFieldId.SettlementD, DBTypes.DBdatetime},
            {MortgagePoolFieldId.InitialPmtD, DBTypes.DBdatetime},
            {MortgagePoolFieldId.UnpaidBalanceD, DBTypes.DBdatetime},
            {MortgagePoolFieldId.MaturityD, DBTypes.DBdatetime},
            {MortgagePoolFieldId.AgencyT, DBTypes.SDBint},
            {MortgagePoolFieldId.BasePoolNumber, DBTypes.SDBvarchar},
            {MortgagePoolFieldId.IssueTypeCode, DBTypes.SDBchar},
            {MortgagePoolFieldId.PoolTypeCode, DBTypes.SDBvarchar},
            {MortgagePoolFieldId.IssuerId, DBTypes.SDBvarchar},
            {MortgagePoolFieldId.CustodianId, DBTypes.SDBvarchar},
            {MortgagePoolFieldId.SecurityR, DBTypes.SDBdecimal},
            {MortgagePoolFieldId.PoolTerm, DBTypes.SDBint},
            {MortgagePoolFieldId.AmortizationMethodT, DBTypes.SDBint},
            {MortgagePoolFieldId.AmortizationT, DBTypes.SDBint},
            {MortgagePoolFieldId.SecurityRMargin, DBTypes.SDBdecimal},
            {MortgagePoolFieldId.SecurityChangeD, DBTypes.DBdatetime},
            {MortgagePoolFieldId.PoolIndexT, DBTypes.SDBint},
            {MortgagePoolFieldId.IsBondFinancePool, DBTypes.SDBbit},
            {MortgagePoolFieldId.CertAndAgreementT, DBTypes.SDBint},
            {MortgagePoolFieldId.IsFormHUD11711ASendToDocumentCustodian, DBTypes.SDBbit},
            {MortgagePoolFieldId.IsEnabled, DBTypes.SDBbit},
            {MortgagePoolFieldId.PIAccountNum, DBTypes.SDBvarchar},
            {MortgagePoolFieldId.PIRoutingNum, DBTypes.SDBvarchar},
            {MortgagePoolFieldId.SearchMinimumStatusT, DBTypes.SDBint},
            {MortgagePoolFieldId.InvestorName, DBTypes.DBvarchar},
            {MortgagePoolFieldId.CommitmentType, DBTypes.SDBtinyint},
            {MortgagePoolFieldId.SettlementType, DBTypes.SDBtinyint},
            {MortgagePoolFieldId.DeliveryType, DBTypes.SDBtinyint},
            {MortgagePoolFieldId.CommitmentNum, DBTypes.SDBvarchar},
            {MortgagePoolFieldId.CommitmentDate, DBTypes.DBdatetime},
            {MortgagePoolFieldId.CommitmentExpires, DBTypes.DBdatetime},
            {MortgagePoolFieldId.CommitmentBasisLckd, DBTypes.SDBbit},
            {MortgagePoolFieldId.CommitmentBasis, DBTypes.SDBmoney},
            {MortgagePoolFieldId.Tolerance, DBTypes.SDBdecimal},
            {MortgagePoolFieldId.BasePricingCalcType, DBTypes.SDBtinyint},
            {MortgagePoolFieldId.BasePricingPoolLevelLckd, DBTypes.SDBbit},
            {MortgagePoolFieldId.BasePricingPoolLevelManual, DBTypes.SDBdecimal},
            {MortgagePoolFieldId.BasePricingManual, DBTypes.SDBmoney},
            {MortgagePoolFieldId.BasePricingTable, DBTypes.SDBvarchar},
            {MortgagePoolFieldId.LLPAType, DBTypes.SDBtinyint},
            {MortgagePoolFieldId.LLPAManual, DBTypes.SDBmoney},
            {MortgagePoolFieldId.SRPType, DBTypes.SDBtinyint},
            {MortgagePoolFieldId.SRPManual, DBTypes.SDBmoney},
            {MortgagePoolFieldId.SRPPoolLevel, DBTypes.SDBdecimal},
            {MortgagePoolFieldId.BlendPairOffCalcType, DBTypes.SDBtinyint},
            {MortgagePoolFieldId.BlendPairOffMarketPrice, DBTypes.SDBdecimal},
            {MortgagePoolFieldId.BookEntryD, DBTypes.DBdatetime},
            {MortgagePoolFieldId.PoolPrefix, DBTypes.SDBvarchar},
            {MortgagePoolFieldId.PoolSuffix, DBTypes.SDBvarchar},
            {MortgagePoolFieldId.MortgageT, DBTypes.SDBtinyint},
            {MortgagePoolFieldId.SellerID, DBTypes.SDBvarchar},
            {MortgagePoolFieldId.ServicerID, DBTypes.SDBvarchar},
            {MortgagePoolFieldId.DocumentCustodianID, DBTypes.SDBvarchar},
            {MortgagePoolFieldId.RemittanceD, DBTypes.DBint},
            {MortgagePoolFieldId.StructureT, DBTypes.SDBtinyint},
            {MortgagePoolFieldId.FeatureCodes, DBTypes.SDBvarchar},
            {MortgagePoolFieldId.OwnershipPc, DBTypes.SDBdecimal},
            {MortgagePoolFieldId.IsInterestOnly, DBTypes.SDBbit},
            {MortgagePoolFieldId.IsBalloon, DBTypes.SDBbit},
            {MortgagePoolFieldId.IsAssumable, DBTypes.SDBbit},
            {MortgagePoolFieldId.ARMPlanNum, DBTypes.SDBvarchar},
            {MortgagePoolFieldId.RoundingT, DBTypes.SDBtinyint},
            {MortgagePoolFieldId.InterestRateChangeLookbackDays, DBTypes.SDBint},
            {MortgagePoolFieldId.MBSMarginPc, DBTypes.SDBdecimal},
            {MortgagePoolFieldId.AccrualRateStructureType, DBTypes.SDBtinyint},
            {MortgagePoolFieldId.FixedServicingFeePc, DBTypes.SDBdecimal},
            {MortgagePoolFieldId.MinAccrualR, DBTypes.DBdecimal},
            {MortgagePoolFieldId.MaxAccrualR, DBTypes.DBdecimal},
            {MortgagePoolFieldId.PoolTradeNumbers, DBTypes.SDBvarchar},
            {MortgagePoolFieldId.EarlyDeliveryDate, DBTypes.DBdatetime},
            {MortgagePoolFieldId.AssignDate, DBTypes.DBdatetime},
            {MortgagePoolFieldId.ConfirmDate, DBTypes.DBdatetime},
            {MortgagePoolFieldId.Notes, DBTypes.SDBvarchar},
            {MortgagePoolFieldId.TaxAndInsuranceAccountNumber, DBTypes.SDBvarchar},
            {MortgagePoolFieldId.TaxAndInsuranceRoutingNumber, DBTypes.SDBvarchar},
            {MortgagePoolFieldId.SettlementAccountNumber1, DBTypes.SDBvarchar},
            {MortgagePoolFieldId.SettlementRoutingNumber1, DBTypes.SDBvarchar},
            {MortgagePoolFieldId.SettlementABAName1, DBTypes.SDBvarchar},
            {MortgagePoolFieldId.SettlementThirdPartyAccountName1, DBTypes.SDBvarchar},
            {MortgagePoolFieldId.SettlementAccountNumber2, DBTypes.SDBvarchar},
            {MortgagePoolFieldId.SettlementRoutingNumber2, DBTypes.SDBvarchar},
            {MortgagePoolFieldId.SettlementABAName2, DBTypes.SDBvarchar},
            {MortgagePoolFieldId.SettlementThirdPartyAccountName2, DBTypes.SDBvarchar},
            {MortgagePoolFieldId.GovernmentBondFinance, DBTypes.SDBbit},
            {MortgagePoolFieldId.GovernmentBondFinancingProgramType, DBTypes.SDBtinyint},
            {MortgagePoolFieldId.GovernmentBondFinancingProgramName, DBTypes.SDBvarchar},
            {MortgagePoolFieldId.SecurityInvestorOriginalSubscriptionAmount1, DBTypes.SDBmoney},
            {MortgagePoolFieldId.SecurityInvestorOriginalSubscriptionAmount2, DBTypes.SDBmoney},
            {MortgagePoolFieldId.TaxId, DBTypes.SDBvarchar},
            {MortgagePoolFieldId.UniqueId, DBTypes.SDBuniqueidentifier},
        };

        private static TradesAndPools.TradesAndPoolsSoapClient CreateSoapClient() =>
            new TradesAndPools.TradesAndPoolsSoapClient("TradesAndPoolsSoap", new System.ServiceModel.EndpointAddress(AppSettings.WEBSERVICE_DOMAIN + "TradesAndPools.asmx"));

        private static string GetXmlQuery() => new XElement("pool", 
            MortgagePoolFieldTypes.Keys.Select(fieldId => new XElement("field", new XAttribute("id", fieldId.ToString("G")))))
            .ToString(SaveOptions.DisableFormatting);

        public MortgagePoolService(string receiver) : base(receiver)
        {
        }

        public static string LoadMortgagePoolsFromWebServices(string authTicket)
        {
            using (var tradesAndPools = CreateSoapClient())
            {
                return tradesAndPools.LoadAllMortgagePools(authTicket, GetXmlQuery());
            }
        }

        public LQBData<MortgagePoolFieldId> LoadAllMortgagePools(XmlLogger xmlLogger)
        {
            using (var mortgagePoolLogger = xmlLogger.CreateSubLog(nameof(LoadAllMortgagePools), true))
            {
                return this.LoadLQBDataAndAddDbTypes(LoadMortgagePoolsFromWebServices, MortgagePoolFieldTypes, ReceiveDataFromLQBClientService.E_ContentType.MortgagePool, xmlLogger);
            }
        }
    }
}
