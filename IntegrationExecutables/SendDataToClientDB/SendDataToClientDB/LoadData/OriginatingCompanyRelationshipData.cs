﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SendDataToClientDB.LoadData
{
    public class OriginatingCompanyRelationshipData : LoadAbstractData
    {
        public OriginatingCompanyRelationshipData(string receiverName) : base(receiverName)
        {
            this.fieldList.Add("PmlBrokerId", DBTypes.SDBuniqueidentifier);
            this.fieldList.Add("OCRoleT", DBTypes.SDBint);
            this.fieldList.Add("EmployeeRoleT", DBTypes.SDBint);
            this.fieldList.Add("EmployeeId", DBTypes.SDBuniqueidentifier);
            this.fieldList.Add("EmployeeName", DBTypes.DBvarchar);
        }
    }
}
