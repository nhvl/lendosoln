﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SendDataToClientDB.LoadData
{
    public class PMLUserData : LoadAbstractData
    {
        public PMLUserData(string receiverName) : base(receiverName)
        {
            this.fieldList.Add("UserId", DBTypes.SDBuniqueidentifier);
            this.fieldList.Add("PmlBrokerId", DBTypes.SDBuniqueidentifier);
            #region Destination table: PML_User
            this.fieldList.Add("EmployeeId", DBTypes.SDBuniqueidentifier);
            this.fieldList.Add("UserFirstNm", DBTypes.SDBvarchar);
            this.fieldList.Add("UserLastNm", DBTypes.SDBvarchar);
            this.fieldList.Add("Addr", DBTypes.SDBvarchar);
            this.fieldList.Add("City", DBTypes.SDBvarchar);
            this.fieldList.Add("State", DBTypes.SDBchar);
            this.fieldList.Add("Zip", DBTypes.SDBchar);
            this.fieldList.Add("Phone", DBTypes.SDBvarchar);
            this.fieldList.Add("Fax", DBTypes.SDBvarchar);
            this.fieldList.Add("CellPhone", DBTypes.SDBvarchar);
            this.fieldList.Add("Pager", DBTypes.SDBvarchar);
            this.fieldList.Add("Email", DBTypes.SDBvarchar);
            this.fieldList.Add("BranchId", DBTypes.SDBuniqueidentifier);
            this.fieldList.Add("CommissionPointOfLoanAmount", DBTypes.SDBdecimal);
            this.fieldList.Add("CommissionPointOfGrossProfit", DBTypes.SDBdecimal);
            this.fieldList.Add("CommissionMinBase", DBTypes.SDBmoney);
            this.fieldList.Add("NotesByEmployer", DBTypes.SDBtext);
            this.fieldList.Add("OriginatorCompensationSetLevelT", DBTypes.SDBint);
            this.fieldList.Add("OriginatorCompensationPercent", DBTypes.SDBdecimal);
            this.fieldList.Add("OriginatorCompensationBaseT", DBTypes.SDBint);
            this.fieldList.Add("OriginatorCompensationMinAmount", DBTypes.SDBmoney);
            this.fieldList.Add("OriginatorCompensationMaxAmount", DBTypes.SDBmoney);
            this.fieldList.Add("OriginatorCompensationFixedAmount", DBTypes.SDBmoney);
            this.fieldList.Add("OriginatorCompensationNotes", DBTypes.SDBvarchar);
            this.fieldList.Add("OriginatorCompensationLastModifiedDate", DBTypes.DBdatetime);
            this.fieldList.Add("OriginatorCompensationAuditXml", DBTypes.SDBvarchar);
            this.fieldList.Add("CustomPricingPolicyFieldSource", DBTypes.SDBint);
            this.fieldList.Add("CustomPricingPolicyField1Fixed", DBTypes.SDBvarchar);
            this.fieldList.Add("CustomPricingPolicyField2Fixed", DBTypes.SDBvarchar);
            this.fieldList.Add("CustomPricingPolicyField3Fixed", DBTypes.SDBvarchar);
            this.fieldList.Add("CustomPricingPolicyField4Fixed", DBTypes.SDBvarchar);
            this.fieldList.Add("CustomPricingPolicyField5Fixed", DBTypes.SDBvarchar);
            this.fieldList.Add("IsActive", DBTypes.SDBbit);
            this.fieldList.Add("LoginNm", DBTypes.SDBvarchar);
            this.fieldList.Add("IsLoanOfficer", DBTypes.DBbit);
            this.fieldList.Add("IsBrokerProcessor", DBTypes.DBbit);
            this.fieldList.Add("IsExternalSecondary", DBTypes.DBbit);
            this.fieldList.Add("IsExternalPostCloser", DBTypes.DBbit);
            this.fieldList.Add("IsPmlManager", DBTypes.SDBbit);
            this.fieldList.Add("PmlExternalManagerFullName", DBTypes.DBvarchar);
            this.fieldList.Add("PmlExternalManagerEmployeeID", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("ManagerFullName", DBTypes.DBvarchar);
            this.fieldList.Add("ManagerEmployeeID", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("ProcessorFullName", DBTypes.DBvarchar);
            this.fieldList.Add("ProcessorEmployeeID", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("JuniorProcessorFullName", DBTypes.DBvarchar);
            this.fieldList.Add("JuniorProcessorEmployeeID", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("LenderAccExecFullName", DBTypes.DBvarchar);
            this.fieldList.Add("LenderAccExecEmployeeID", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("BrokerProcessorFullName", DBTypes.DBvarchar);
            this.fieldList.Add("BrokerProcessorEmployeeId", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("UnderwriterFullName", DBTypes.DBvarchar);
            this.fieldList.Add("UnderwriterEmployeeID", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("JuniorUnderwriterFullName", DBTypes.DBvarchar);
            this.fieldList.Add("JuniorUnderwriterEmployeeID", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("LockDeskFullName", DBTypes.DBvarchar);
            this.fieldList.Add("LockDeskEmployeeID", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("LpePriceGroupId", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("UseOriginatingCompanyLpePriceGroupId", DBTypes.DBbit);
            this.fieldList.Add("Lender_TPO_LandingPageConfigId", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("MiniCorrManagerEmployeeID", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("MiniCorrManagerFullName", DBTypes.DBvarchar);
            this.fieldList.Add("MiniCorrProcessorEmployeeID", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("MiniCorrProcessorFullName", DBTypes.DBvarchar);
            this.fieldList.Add("MiniCorrJuniorProcessorEmployeeID", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("MiniCorrJuniorProcessorFullName", DBTypes.DBvarchar);
            this.fieldList.Add("MiniCorrLenderAccExecEmployeeID", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("MiniCorrLenderAccExecFullName", DBTypes.DBvarchar);
            this.fieldList.Add("MiniCorrExternalPostCloserEmployeeID", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("MiniCorrExternalPostCloserFullName", DBTypes.DBvarchar);
            this.fieldList.Add("MiniCorrUnderwriterEmployeeID", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("MiniCorrUnderwriterFullName", DBTypes.DBvarchar);
            this.fieldList.Add("MiniCorrJuniorUnderwriterEmployeeID", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("MiniCorrJuniorUnderwriterFullName", DBTypes.DBvarchar);
            this.fieldList.Add("MiniCorrCreditAuditorEmployeeID", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("MiniCorrCreditAuditorFullName", DBTypes.DBvarchar);
            this.fieldList.Add("MiniCorrLegalAuditorEmployeeID", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("MiniCorrLegalAuditorFullName", DBTypes.DBvarchar);
            this.fieldList.Add("MiniCorrLockDeskEmployeeID", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("MiniCorrLockDeskFullName", DBTypes.DBvarchar);
            this.fieldList.Add("MiniCorrPurchaserEmployeeID", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("MiniCorrPurchaserFullName", DBTypes.DBvarchar);
            this.fieldList.Add("MiniCorrSecondaryEmployeeID", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("MiniCorrSecondaryFullName", DBTypes.DBvarchar);
            this.fieldList.Add("MiniCorrespondentBranchId", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("UseOriginatingCompanyMiniCorrBranchId", DBTypes.DBbit);
            this.fieldList.Add("MiniCorrespondentPriceGroupId", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("UseOriginatingCompanyMiniCorrPriceGroupId", DBTypes.DBbit);
            this.fieldList.Add("MiniCorrespondentLandingPageID", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("CorrManagerEmployeeID", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("CorrManagerFullName", DBTypes.DBvarchar);
            this.fieldList.Add("CorrProcessorEmployeeID", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("CorrProcessorFullName", DBTypes.DBvarchar);
            this.fieldList.Add("CorrJuniorProcessorEmployeeID", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("CorrJuniorProcessorFullName", DBTypes.DBvarchar);
            this.fieldList.Add("CorrLenderAccExecEmployeeID", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("CorrLenderAccExecFullName", DBTypes.DBvarchar);
            this.fieldList.Add("CorrExternalPostCloserEmployeeID", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("CorrExternalPostCloserFullName", DBTypes.DBvarchar);
            this.fieldList.Add("CorrUnderwriterEmployeeID", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("CorrUnderwriterFullName", DBTypes.DBvarchar);
            this.fieldList.Add("CorrJuniorUnderwriterEmployeeID", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("CorrJuniorUnderwriterFullName", DBTypes.DBvarchar);
            this.fieldList.Add("CorrCreditAuditorEmployeeID", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("CorrCreditAuditorFullName", DBTypes.DBvarchar);
            this.fieldList.Add("CorrLegalAuditorEmployeeID", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("CorrLegalAuditorFullName", DBTypes.DBvarchar);
            this.fieldList.Add("CorrLockDeskEmployeeID", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("CorrLockDeskFullName", DBTypes.DBvarchar);
            this.fieldList.Add("CorrPurchaserEmployeeID", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("CorrPurchaserFullName", DBTypes.DBvarchar);
            this.fieldList.Add("CorrSecondaryEmployeeID", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("CorrSecondaryFullName", DBTypes.DBvarchar);
            this.fieldList.Add("CorrespondentBranchId", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("UseOriginatingCompanyCorrBranchId", DBTypes.DBbit);
            this.fieldList.Add("CorrespondentPriceGroupId", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("UseOriginatingCompanyCorrPriceGroupId", DBTypes.DBbit);
            this.fieldList.Add("CorrespondentLandingPageID", DBTypes.DBuniqueidentifier);
            this.fieldList.Add("IsUserTPO", DBTypes.SDBbit);
            this.fieldList.Add("CanApplyForIneligibleLoanPrograms", DBTypes.DBbit);
            this.fieldList.Add("CanRunPricingEngineWithoutCreditReport", DBTypes.DBbit);
            this.fieldList.Add("CanSubmitWithoutCreditReport", DBTypes.DBbit);
            this.fieldList.Add("PmlLevelAccess", DBTypes.SDBint);
            this.fieldList.Add("NmlsIdentifier", DBTypes.SDBvarchar);
            this.fieldList.Add("LicenseXmlContent", DBTypes.SDBvarchar);
            this.fieldList.Add("PasswordExpirationD", DBTypes.DBsmalldatetime);
            this.fieldList.Add("PasswordExpirationPeriod", DBTypes.DBint);
            this.fieldList.Add("RecentLoginD", DBTypes.DBdatetime);
            #endregion

            // can provide different sets of fields to the client per their need, e.g. entire cache vs. secondary fields only
        }
    }
}
