--     SQL Script Name: DB_Transmission_Service_Create_Employee_Tables.sql

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Employee](
    [EmployeeId] [uniqueidentifier] NOT NULL,
    [BranchId] [uniqueidentifier] NULL,
    [UserFirstNm] [varchar](21) NULL,
    [IsActive] [bit] NULL,
    [UserLastNm] [varchar](21) NULL,
    [Addr] [varchar](60) NULL,
    [City] [varchar](36) NULL,
    [State] [char](2) NULL,
    [Zip] [char](5) NULL,
    [Phone] [varchar](21) NULL,
    [Fax] [varchar](21) NULL,
    [StartD] [smalldatetime] NULL,
    [NewOnlineLoanEventNotifOptionT] [int] NULL,
    [Email] [varchar](80) NULL,
    [NotesByEmployer] [text] NULL,
    [EmployeeUserId] [uniqueidentifier] NULL,
    [EmployeeLicenseNumber] [varchar](30) NULL,
    [CommissionPointOfLoanAmount] [decimal](9, 3) NULL,
    [CommissionPointOfGrossProfit] [decimal](9, 3) NULL,
    [CommissionMinBase] [money] NULL,
    [LicenseXmlContent] [varchar](max) NULL,
    [CellPhone] [varchar](21) NULL,
    [Pager] [varchar](21) NULL,
    [NmlsIdentifier] [varchar](50) NULL,
    [IsUserTPO] [bit] NULL,
    [OriginatorCompensationAuditXml] [varchar](max) NULL,
    [OriginatorCompensationPercent] [decimal](9, 3) NULL,
    [OriginatorCompensationBaseT] [int] NULL,
    [OriginatorCompensationMinAmount] [money] NULL,
    [OriginatorCompensationMaxAmount] [money] NULL,
    [OriginatorCompensationFixedAmount] [money] NULL,
    [OriginatorCompensationNotes] [varchar](200) NULL,
    [OriginatorCompensationLastModifiedDate] [datetime] NULL,
    [TaskRelatedEmailOptionT] [int] NULL,
    [OriginatorCompensationSetLevelT] [int] NULL,
    [EmployeeIDInCompany] [varchar](30) NULL,
    [EmployeeStartD] [smalldatetime] NULL,
    [EmployeeTerminationD] [smalldatetime] NULL,
    [CustomPricingPolicyField1Fixed] [varchar](25) NULL,
    [CustomPricingPolicyField2Fixed] [varchar](25) NULL,
    [CustomPricingPolicyField3Fixed] [varchar](25) NULL,
    [CustomPricingPolicyField4Fixed] [varchar](25) NULL,
    [CustomPricingPolicyField5Fixed] [varchar](25) NULL,
    [CustomPricingPolicyFieldSource] [int] NULL,
    [ChumsId] [varchar](8) NULL,
    [CanBeMemberOfMultipleTeams] [bit] NULL,
    [UserMiddleNm] [varchar](21) NULL,
    [UserSuffix] [varchar](10) NULL,
    [UserNmOnLoanDocs] [varchar](100) NULL,
    [UserNmOnLoanDocsLckd] [bit] NULL,
    [FavoritePageIdsJSON] [varchar](max) NULL,
    [CanContactSupport] [bit] NULL,
    [LoginNm] [varchar](36) NULL,
    [NeedToAcceptLatestAgreement] [bit] NULL,
    [PasswordExpirationD] [smalldatetime] NULL,
    [RecentLoginD] [datetime] NULL,
    [PasswordExpirationPeriod] [int] NULL,
    [IsActiveDirectoryUser] [bit] NULL
 CONSTRAINT [PK_EmployeeId] PRIMARY KEY CLUSTERED 
(
    [EmployeeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

CREATE TABLE [dbo].[Employee_2](
    [UserId] [uniqueidentifier] NOT NULL,
    [EmployeeId] [uniqueidentifier] NOT NULL,
    [Permissions] [char](255) NULL,
    [IsAccountOwner] [bit] NULL,
    [ByPassBgCalcForGfeAsDefault] [bit] NULL,
    [LenderAccExecEmployeeId] [uniqueidentifier] NULL,
    [LastUsedCreditProtocolId] [uniqueidentifier] NULL,
    [LockDeskEmployeeId] [uniqueidentifier] NULL,
    [SelectedPipelineCustomReportId] [uniqueidentifier] NULL,
    [ManagerEmployeeId] [uniqueidentifier] NULL,
    [LpePriceGroupId] [uniqueidentifier] NULL,
    [UnderwriterEmployeeId] [uniqueidentifier] NULL,
    [ProcessorEmployeeId] [uniqueidentifier] NULL,
    [LpeRsExpirationByPassPermission] [varchar](100) NULL,
    [MustLogInFromTheseIpAddresses] [varchar](200) NULL,
    [IsQuickPricerEnabled] [bit] NULL,
    [LastModifiedDate] [datetime] NULL,
    [AssignedPrintGroups] [varchar](max) NULL,
    [JuniorUnderwriterEmployeeID] [uniqueidentifier] NULL,
    [JuniorProcessorEmployeeID] [uniqueidentifier] NULL,
    [ShowQMStatusInPml2] [bit] NULL,
    [LoanOfficerAssistantEmployeeID] [uniqueidentifier] NULL,
    [EnabledIpRestriction] [bit] NULL,
    [EnabledMultiFactorAuthentication] [bit] NULL,
    [EnabledClientDigitalCertificateInstall] [bit] NULL,
    [UsePml2AsQuickPricer] [bit] NULL,
    [SupportEmail] [varchar](320) NULL,
    [SupportEmailVerified] [bit] NULL,
 CONSTRAINT [PK_UserId] PRIMARY KEY CLUSTERED 
(
    [UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO