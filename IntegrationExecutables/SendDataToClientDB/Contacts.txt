------------
WHEDA (PML0214)
------------
When sending a zip file with web services files, rename as .txt to prevent firewall block.

Joe Tuschl
Phone: (608)267-1452
E-mail: joe.tuschl@wheda.com
E-mail for web service files: jctuschl@tds.net

Marty Maloney
Quality Control Analyst
Phone: 608-266-2724 or 800-334-6873 ext 62724
marty.maloney@wheda.com

Include Julie Warne (julie.warne@wheda.com) on fields she requests

------------
JMAC (PML0229)
------------
Per Anthony's request, we're just including them on any fields we add to the service.
At this point, Anthony's only included when needed.
Ryan handles updating the database and servers; Nahum makes most of the field requests.

Ryan Allcorn
IT Specialist | JMAC Lending, Inc.
Direct (949) 390-2682 
ryan.allcorn@jmaclending.com

Anthony Pham
Controller | JMAC Lending, Inc.
Direct (949) 390-2616
anthony@jmaclending.com

Nahum Chan
IT Support / Report Writer
Direct (949) 390-2645
nahum.chan@jmaclending.com

------------
Lenox (PML0246)
------------
Carlito Dizon
??
carlito.dizon@lenoxhomeloans.com

------------
NFLP (PML0226)
------------
Matt Herzog
?? - He's a technical guy; made the setup really easy.
matt.herzog@nflp.com

------------
IMCU (PML0227)
------------
David McSpadden
?? - Seems to be in charge of their servers
davidm@imcu.com

Daralyn Schneider
Mortgage Operations Manager
DSchneider@IMCU.com