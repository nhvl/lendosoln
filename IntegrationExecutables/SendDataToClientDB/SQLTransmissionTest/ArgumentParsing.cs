﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SendDataToClientDB;

namespace SQLTransmissionTest
{
    /// <summary>
    /// Tests the <see cref="ExecutionOptions"/> class.
    /// Primarily makes sure that the <see cref="ExecutionOptions(string[])"/> constructor
    /// parses arguments as expected into the various <see cref="ExecutionOptions"/> properties.
    /// </summary>
    [TestClass]
    public class ArgumentParsing
    {
        // Expected default: the list of receivers from the app settings.
        private List<string> receiversDefault = AppSettings.RECEIVERS.ToList();
        // Expected default: all content types (to preserve existing functionality).
        private HashSet<TransmissionType> contentTypesDefault = new HashSet<TransmissionType>(Enum.GetValues(typeof(TransmissionType)).Cast<TransmissionType>());

        [TestMethod]
        public void NoArguments()
        {
            var exOptions = new ExecutionOptions(new string[] { });
            Assert.IsTrue(receiversDefault.SequenceEqual(exOptions.ReceiverNames));
            Assert.IsTrue(contentTypesDefault.SetEquals(exOptions.SendingTypes));
        }

        [TestMethod]
        public void NullArgument()
        {
            var exOptions = new ExecutionOptions(null);
            Assert.IsTrue(receiversDefault.SequenceEqual(exOptions.ReceiverNames));
            Assert.IsTrue(contentTypesDefault.SetEquals(exOptions.SendingTypes));
        }

        /// <summary>
        /// We should be able to update the executable without also needing to update the place it's invoked (Task Scheduler).
        /// This test ensures backward-compatibility.
        /// </summary>
        [TestMethod]
        public void LegacyStyleArguments()
        {
            var receivers = new string[] { "PML0999", "PML0768", "PML0998" };
            var exOptions = new ExecutionOptions(receivers);
            Assert.IsTrue(receivers.SequenceEqual(exOptions.ReceiverNames));
            Assert.IsTrue(contentTypesDefault.SetEquals(exOptions.SendingTypes));
            Assert.IsFalse(exOptions.InfoOnlyMode);
        }

        /// <summary>
        /// The help argument is special. It should not run a transmission and instead give info about using the program.
        /// </summary>
        [TestMethod]
        public void HelpArgument()
        {
            var args = new string[] { "?" };
            var exOptions = new ExecutionOptions(args);
            Assert.IsTrue(exOptions.InfoOnlyMode);

            // Invoking help should have no side-effects, even if valid other arguments are supplied.
            args = new string[] { "?", "--receivers", "Receiver1", "Receiver2", "--disable-pml-timespan" };
            exOptions = new ExecutionOptions(args);
            Assert.IsTrue(exOptions.InfoOnlyMode);
            Assert.IsFalse(exOptions.ReceiverNames.Any());
            Assert.IsFalse(exOptions.SendingTypes.Any());
            Assert.IsFalse(exOptions.DisablePmlTimespan);
        }

        [TestMethod]
        public void TypicalCommandLineArgs()
        {
            var args = new string[] { "PML0999", "--send", TransmissionType.Loan.ToString() };
            var exOptions = new ExecutionOptions(args);
            Assert.AreEqual("PML0999", exOptions.ReceiverNames[0]);
            Assert.IsTrue(exOptions.SendingTypes.Contains(TransmissionType.Loan));
        }

        /// <summary>
        /// Arguments that don't start with -, /, ? (flags & delimiters) after the --disable-pml-timespan or similar flag arguments should not be recognized.
        /// </summary>
        [TestMethod]
        public void ExtraArgumentsInUndefinedPositions()
        {
            var args = new string[] { "Receiver", "--disable-pml-timespan", "MySpecialReceiver" };
            var exOptions = new ExecutionOptions(args);
            Assert.IsTrue(exOptions.ReceiverNames.Contains("Receiver"));
            Assert.IsFalse(exOptions.ReceiverNames.Contains("MySpecialReceiver"));
            Assert.IsTrue(exOptions.DisablePmlTimespan);
            Assert.IsFalse(exOptions.InfoOnlyMode);
        }

        /// <summary>
        /// A list of test cases for the <see cref="MiscArgumentTests"/> test.
        /// The values stored in the tuples represent different sections of the command line arguments.
        /// </summary>
        public static List<Tuple<IEnumerable<string>, IEnumerable<TransmissionType>, IEnumerable<string>>> testCases
            = new List<Tuple<IEnumerable<string>, IEnumerable<TransmissionType>, IEnumerable<string>>>
        {
                new Tuple<IEnumerable<string>, IEnumerable<TransmissionType>, IEnumerable<string>>(
                    new string[] { "" },
                    new TransmissionType[] { },
                    new string[] { }),
                new Tuple<IEnumerable<string>, IEnumerable<TransmissionType>, IEnumerable<string>>(
                    new string[] { "THINHTEST2" },
                    new TransmissionType[] { },
                    new string[] { "THINHTEST2_RECEIVER2" }),
                new Tuple<IEnumerable<string>, IEnumerable<TransmissionType>, IEnumerable<string>>(
                    new string[] { "THINHTEST2" },
                    new TransmissionType[] { TransmissionType.Loan, TransmissionType.Employee, TransmissionType.EDocs, TransmissionType.PML },
                    new string[] { }),
                new Tuple<IEnumerable<string>, IEnumerable<TransmissionType>, IEnumerable<string>>(
                    new string[] { "THINHTEST2" },
                    new TransmissionType[] { TransmissionType.Loan, TransmissionType.Employee, TransmissionType.Loan, TransmissionType.EDocs, TransmissionType.PML, TransmissionType.Loan, TransmissionType.PML, TransmissionType.PML },
                    new string[] { }),

        };

        /// <summary>
        /// Runs <see cref="testCases"/> tests.
        /// </summary>
        [TestMethod]
        public void MiscArgumentTests()
        {
            foreach (Tuple<IEnumerable<string>, IEnumerable<TransmissionType>, IEnumerable<string>> testCase in testCases)
            {
                AssertArgumentsParsedCorrectly(testCase.Item1, testCase.Item2, testCase.Item3);
            }
        }

        /// <summary>
        /// This is not an exhaustive test by a long shot.
        /// Quirks of argument parsing can be demonstrated and recorded here.
        /// </summary>
        [TestMethod]
        public void WeirdArgs()
        {
            var args = new string[] 
            {
                "324\\=16 \r\n\t 154&%#^^#@/><html>HELLO, WORLD ಠ_ಠ</html>",
                "--send", "--send", "--receivers", "--unrecognized-command",
                "--send", TransmissionType.Loan.ToString(),
                "LAON", "LONE", "LOAN", "loan", 2.ToString(),
                "--receivers", "THINHTEST20", "☹, ☺, ☻",
                "--disable-pml-timespan"
            };
            var exOptions = new ExecutionOptions(args);
            Assert.AreEqual(args[0], exOptions.ReceiverNames[0]);
            Assert.AreEqual("THINHTEST20", exOptions.ReceiverNames[1]);
            Assert.IsTrue(exOptions.SendingTypes.Contains(TransmissionType.Loan));
            // Arguable whether it is intuitive behavior to parse numbers as enums, but it is expected:
            Assert.IsTrue(exOptions.SendingTypes.Contains((TransmissionType)2));
            Assert.AreEqual(true, exOptions.DisablePmlTimespan);
        }

        /// <summary>
        /// Takes some generic test case values for standard command line argument positions and <see cref="Assert"/>s that <see cref="ExecutionOptions"/> object parses them as expected.
        /// </summary>
        /// <param name="preArgReceivers">Receivers supplied as the first arguments, before any other argument types.</param>
        /// <param name="contentTypes">Content types to add after the "--send" delimiter.</param>
        /// <param name="postArgReceivers">Receivers to add after the "--receivers" delimiter.</param>
        public void AssertArgumentsParsedCorrectly(IEnumerable<string> preArgReceivers, IEnumerable<TransmissionType> contentTypes, IEnumerable<string> postArgReceivers)
        {
            var args = new List<string>();
            args.AddRange(preArgReceivers);
            args.Add("--send");
            args.AddRange(contentTypes.Select(type => type.ToString()));
            args.Add("--receivers");
            args.AddRange(postArgReceivers);
            var exOptions = new ExecutionOptions(args.ToArray());
            var receivers = preArgReceivers.ToList();
            receivers.AddRange(postArgReceivers);
            if (receivers == null || receivers.All(val => string.IsNullOrEmpty(val)))
            {
                Assert.IsTrue(receiversDefault.SequenceEqual(exOptions.ReceiverNames));
            }
            else
            {
                Assert.IsTrue(receivers.SequenceEqual(exOptions.ReceiverNames), $"Receivers input: [{string.Join(", ", receivers)}] didn't match parsed receivers: [{string.Join(", ", exOptions.ReceiverNames)}]");
            }

            if (contentTypes == null || !contentTypes.Any())
            {
                Assert.IsTrue(contentTypesDefault.SetEquals(exOptions.SendingTypes));
            }
            else
            {
                Assert.IsTrue(exOptions.SendingTypes.SetEquals(contentTypes), $"Sending types input: [{string.Join(", ", contentTypes)}] didn't match parsed content Types: [{string.Join(", ", exOptions.SendingTypes)}]");
            }
        }
    }
}
