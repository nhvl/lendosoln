﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using SendDataToClientDB;
using SendDataToClientDB.com.lendingqb.Employee;
using SendDataToClientDB.com.lendingqb.EDocs;
using SendDataToClientDB.ReceiveDataFromLQBClientService;

namespace SQLTransmissionTest
{
    /// <summary>
    /// Centralizes the data that will be provided to tests for generating and verifying their data.
    /// To make sure that tests do not affect one another, good practice is to do one of the following:
    ///     1. Use methods here which construct new objects when called, so that each test gets its own instance of the object and can modify the resulting object.
    ///     2. Use read-only members and collections which cannot be modified by the tests.
    /// </summary>
    public static class DataForTests
    {
        public static LQBData<EmployeeFieldId> OneRecordEmployeeDataContainer()
        {
            return new LQBData<EmployeeFieldId>(OneRecordEmployeeDataBook());
        }

        public static IEnumerable<Dictionary<EmployeeFieldId, Field>> OneRecordEmployeeDataBook()
        {
            return new List<Dictionary<EmployeeFieldId, Field>>()
            {
                new Dictionary<EmployeeFieldId, Field>()
                {
                    { EmployeeFieldId.UserFirstNm, new Field("Sam", DBTypes.DBvarchar) },
                    { EmployeeFieldId.UserLastNm, new Field("Sample", DBTypes.DBvarchar) },
                    { EmployeeFieldId.UserId, new Field("00000000-0000-0000-0000-000000000000", DBTypes.SDBuniqueidentifier) },
                    { EmployeeFieldId.LoginNm, new Field("samples", DBTypes.DBvarchar) },
                    { EmployeeFieldId.IsActiveDirectoryUser, new Field("true", DBTypes.DBbit) },
                    { EmployeeFieldId.Fax, new Field(null, DBTypes.DBvarchar) }
                }
            };
        }

        public static LQBData<EmployeeFieldId> FiveRecordEmployeeDataContainer()
        {
            return new LQBData<EmployeeFieldId>(FiveRecordEmployeeDataBook());
        }

        public static IEnumerable<Dictionary<EmployeeFieldId, Field>> FiveRecordEmployeeDataBook()
        {
            return new List<Dictionary<EmployeeFieldId, Field>>()
            {
                new Dictionary<EmployeeFieldId, Field>()
                {
                    { EmployeeFieldId.UserFirstNm, new Field("Sam", DBTypes.DBvarchar) },
                    { EmployeeFieldId.UserLastNm, new Field("Sample", DBTypes.DBvarchar) },
                    { EmployeeFieldId.UserId, new Field("00000000-0000-0000-0000-000000000000", DBTypes.SDBuniqueidentifier) },
                    { EmployeeFieldId.LoginNm, new Field("samples", DBTypes.DBvarchar) },
                    { EmployeeFieldId.IsActiveDirectoryUser, new Field("true", DBTypes.DBbit) },
                    { EmployeeFieldId.Fax, new Field(null, DBTypes.DBvarchar) }
                },
                new Dictionary<EmployeeFieldId, Field>()
                {
                    { EmployeeFieldId.UserFirstNm, new Field("Tess", DBTypes.DBvarchar) },
                    { EmployeeFieldId.UserLastNm, new Field("Test", DBTypes.DBvarchar) },
                    { EmployeeFieldId.UserId, new Field("00000000-0000-0000-0000-000000000001", DBTypes.SDBuniqueidentifier) },
                    { EmployeeFieldId.LoginNm, new Field("ttest", DBTypes.DBvarchar) },
                    { EmployeeFieldId.IsActiveDirectoryUser, new Field("false", DBTypes.DBbit) },
                    { EmployeeFieldId.Fax, new Field(null, DBTypes.DBvarchar) }
                },
                new Dictionary<EmployeeFieldId, Field>()
                {
                    { EmployeeFieldId.UserFirstNm, new Field("TESTUSER", DBTypes.DBvarchar) },
                    { EmployeeFieldId.UserLastNm, new Field("BOT", DBTypes.DBvarchar) },
                    { EmployeeFieldId.UserId, new Field("12345678-1234-1234-1234-123456789012", DBTypes.SDBuniqueidentifier) },
                    { EmployeeFieldId.LoginNm, new Field("BOTTESTER", DBTypes.DBvarchar) },
                    { EmployeeFieldId.IsActiveDirectoryUser, new Field("false", DBTypes.DBbit) },
                    { EmployeeFieldId.Fax, new Field("949-100-3890", DBTypes.DBvarchar) }
                },
                new Dictionary<EmployeeFieldId, Field>()
                {
                    { EmployeeFieldId.UserFirstNm, new Field("Count", DBTypes.DBvarchar) },
                    { EmployeeFieldId.UserLastNm, new Field("DracuTest", DBTypes.DBvarchar) },
                    { EmployeeFieldId.UserId, new Field("00000000-0000-0000-0000-0000000000AB", DBTypes.SDBuniqueidentifier) },
                    { EmployeeFieldId.LoginNm, new Field("transylmania", DBTypes.DBvarchar) },
                    { EmployeeFieldId.IsActiveDirectoryUser, new Field("true", DBTypes.DBbit) },
                    { EmployeeFieldId.Fax, new Field(null, DBTypes.DBvarchar) }
                },
                new Dictionary<EmployeeFieldId, Field>()
                {
                    { EmployeeFieldId.UserFirstNm, new Field("Sam", DBTypes.DBvarchar) },
                    { EmployeeFieldId.UserLastNm, new Field("Sample II", DBTypes.DBvarchar) },
                    { EmployeeFieldId.UserId, new Field("22222222-2222-2222-2222-222222222222", DBTypes.SDBuniqueidentifier) },
                    { EmployeeFieldId.LoginNm, new Field("samples2", DBTypes.DBvarchar) },
                    { EmployeeFieldId.IsActiveDirectoryUser, new Field("false", DBTypes.DBbit) },
                    { EmployeeFieldId.Fax, new Field(null, DBTypes.DBvarchar) }
                },
            };
        }

        public static readonly string FiveRecordEmployeeOutputString = "<LendingQBLoanData version=\"1.5\"><employee><field id=\"UserFirstNm\" type=\"DBvarchar\">Sam</field><field id=\"UserLastNm\" type=\"DBvarchar\">Sample</field><field id=\"UserId\" type=\"SDBuniqueidentifier\">00000000-0000-0000-0000-000000000000</field><field id=\"LoginNm\" type=\"DBvarchar\">samples</field><field id=\"IsActiveDirectoryUser\" type=\"DBbit\">true</field><field id=\"Fax\" type=\"DBvarchar\" null=\"true\" /></employee><employee><field id=\"UserFirstNm\" type=\"DBvarchar\">Tess</field><field id=\"UserLastNm\" type=\"DBvarchar\">Test</field><field id=\"UserId\" type=\"SDBuniqueidentifier\">00000000-0000-0000-0000-000000000001</field><field id=\"LoginNm\" type=\"DBvarchar\">ttest</field><field id=\"IsActiveDirectoryUser\" type=\"DBbit\">false</field><field id=\"Fax\" type=\"DBvarchar\" null=\"true\" /></employee><employee><field id=\"UserFirstNm\" type=\"DBvarchar\">TESTUSER</field><field id=\"UserLastNm\" type=\"DBvarchar\">BOT</field><field id=\"UserId\" type=\"SDBuniqueidentifier\">12345678-1234-1234-1234-123456789012</field><field id=\"LoginNm\" type=\"DBvarchar\">BOTTESTER</field><field id=\"IsActiveDirectoryUser\" type=\"DBbit\">false</field><field id=\"Fax\" type=\"DBvarchar\">949-100-3890</field></employee><employee><field id=\"UserFirstNm\" type=\"DBvarchar\">Count</field><field id=\"UserLastNm\" type=\"DBvarchar\">DracuTest</field><field id=\"UserId\" type=\"SDBuniqueidentifier\">00000000-0000-0000-0000-0000000000AB</field><field id=\"LoginNm\" type=\"DBvarchar\">transylmania</field><field id=\"IsActiveDirectoryUser\" type=\"DBbit\">true</field><field id=\"Fax\" type=\"DBvarchar\" null=\"true\" /></employee><employee><field id=\"UserFirstNm\" type=\"DBvarchar\">Sam</field><field id=\"UserLastNm\" type=\"DBvarchar\">Sample II</field><field id=\"UserId\" type=\"SDBuniqueidentifier\">22222222-2222-2222-2222-222222222222</field><field id=\"LoginNm\" type=\"DBvarchar\">samples2</field><field id=\"IsActiveDirectoryUser\" type=\"DBbit\">false</field><field id=\"Fax\" type=\"DBvarchar\" null=\"true\" /></employee></LendingQBLoanData>";

        public static IEnumerable<Dictionary<EDocsFieldId, Field>> OneRecordEDocDataBook()
        {
            return new List<Dictionary<EDocsFieldId, Field>>()
            {
                new Dictionary<EDocsFieldId, Field>()
                {
                    { EDocsFieldId.DocTypeName, new Field("CERTIFICATE SUBMITTED", DBTypes.DBvarchar) },
                    { EDocsFieldId.CreatedDate, new Field("2016-09-29 12:28:01.783", DBTypes.DBdatetime) },
                    { EDocsFieldId.LastModifiedDate, new Field("2016-09-29 12:28:13.530", DBTypes.DBdatetime) },
                    { EDocsFieldId.NumPages, new Field("2", DBTypes.SDBint) },
                    { EDocsFieldId.DocumentId, new Field("A46EA288-25A5-4866-94C6-61766FEDC38A", DBTypes.SDBuniqueidentifier) },
                    { EDocsFieldId.DocTypeId, new Field("1701", DBTypes.SDBint) },
                    { EDocsFieldId.FolderName, new Field("LENDINGQB", DBTypes.DBvarchar) }
                },
            };
        }

        public static IEnumerable<Dictionary<EDocsFieldId, Field>> FiveRecordEDocDataBook()
        {
            return new List<Dictionary<EDocsFieldId, Field>>()
            {
                new Dictionary<EDocsFieldId, Field>()
                {
                    { EDocsFieldId.DocTypeName, new Field("CERTIFICATE SUBMITTED", DBTypes.DBvarchar) },
                    { EDocsFieldId.CreatedDate, new Field("2016-09-29 12:28:01.783", DBTypes.DBdatetime) },
                    { EDocsFieldId.LastModifiedDate, new Field("2016-09-29 12:28:13.530", DBTypes.DBdatetime) },
                    { EDocsFieldId.NumPages, new Field("2", DBTypes.SDBint) },
                    { EDocsFieldId.DocumentId, new Field("A46EA288-25A5-4866-94C6-61766FEDC38A", DBTypes.SDBuniqueidentifier) },
                    { EDocsFieldId.DocTypeId, new Field("1701", DBTypes.SDBint) },
                    { EDocsFieldId.FolderName, new Field("LENDINGQB", DBTypes.DBvarchar) }
                },
                new Dictionary<EDocsFieldId, Field>()
                {
                    { EDocsFieldId.DocTypeName, new Field("MORTGAGE INSURANCE DOCUMENT", DBTypes.DBvarchar) },
                    { EDocsFieldId.CreatedDate, new Field("2016-09-21 15:26:11.600", DBTypes.DBdatetime) },
                    { EDocsFieldId.LastModifiedDate, new Field("2016-09-21 15:26:23.073", DBTypes.DBdatetime) },
                    { EDocsFieldId.NumPages, new Field("2", DBTypes.SDBint) },
                    { EDocsFieldId.DocumentId, new Field("3A4131A6-08ED-42D1-8C2F-AA5DE1EDA7B5", DBTypes.SDBuniqueidentifier) },
                    { EDocsFieldId.DocTypeId, new Field("2876", DBTypes.SDBint) },
                    { EDocsFieldId.FolderName, new Field("MORTGAGE INSURANCE DOCUMENTS", DBTypes.DBvarchar) }
                },
                new Dictionary<EDocsFieldId, Field>()
                {
                    { EDocsFieldId.DocTypeName, new Field("112150134 DOC TYPE 4", DBTypes.DBvarchar) },
                    { EDocsFieldId.CreatedDate, new Field("2016-09-19 19:32:15.880", DBTypes.DBdatetime) },
                    { EDocsFieldId.LastModifiedDate, new Field("2016-09-19 19:36:31.547", DBTypes.DBdatetime) },
                    { EDocsFieldId.NumPages, new Field("1", DBTypes.SDBint) },
                    { EDocsFieldId.DocumentId, new Field("53454BFF-71E5-4381-9EA7-3E637E9A16A4", DBTypes.SDBuniqueidentifier) },
                    { EDocsFieldId.DocTypeId, new Field("4428", DBTypes.SDBint) },
                    { EDocsFieldId.FolderName, new Field("112150134 Folder 1", DBTypes.DBvarchar) }
                },
                new Dictionary<EDocsFieldId, Field>()
                {
                    { EDocsFieldId.DocTypeName, new Field("CREDIT REPORT", DBTypes.DBvarchar) },
                    { EDocsFieldId.CreatedDate, new Field("2016-09-06 17:58:41.577", DBTypes.DBdatetime) },
                    { EDocsFieldId.LastModifiedDate, new Field("2016-09-06 17:58:55.170", DBTypes.DBdatetime) },
                    { EDocsFieldId.NumPages, new Field("6", DBTypes.SDBint) },
                    { EDocsFieldId.DocumentId, new Field("E142AD20-9576-4681-B078-31C073DBEB47", DBTypes.SDBuniqueidentifier) },
                    { EDocsFieldId.DocTypeId, new Field("183", DBTypes.SDBint) },
                    { EDocsFieldId.FolderName, new Field("CREDIT", DBTypes.DBvarchar) }
                },
                new Dictionary<EDocsFieldId, Field>()
                {
                    { EDocsFieldId.DocTypeName, new Field("SADA", DBTypes.DBvarchar) },
                    { EDocsFieldId.CreatedDate, new Field("2016-08-23 14:23:58.863", DBTypes.DBdatetime) },
                    { EDocsFieldId.LastModifiedDate, new Field("2016-08-23 14:24:09.140", DBTypes.DBdatetime) },
                    { EDocsFieldId.NumPages, new Field("8", DBTypes.SDBint) },
                    { EDocsFieldId.DocumentId, new Field("A8543249-5FAF-4D38-9BCB-F48B66C81D89", DBTypes.SDBuniqueidentifier) },
                    { EDocsFieldId.DocTypeId, new Field("13538", DBTypes.SDBint) },
                    { EDocsFieldId.FolderName, new Field(null, DBTypes.DBvarchar) }
                },
            };
        }

        public static readonly string FiveRecordEDocOutputString = "<LendingQBLoanData version=\"1.5\"><eDoc><field id=\"DocTypeName\" type=\"DBvarchar\">CERTIFICATE SUBMITTED</field><field id=\"CreatedDate\" type=\"DBdatetime\">2016-09-29 12:28:01.783</field><field id=\"LastModifiedDate\" type=\"DBdatetime\">2016-09-29 12:28:13.530</field><field id=\"NumPages\" type=\"SDBint\">2</field><field id=\"DocumentId\" type=\"SDBuniqueidentifier\">A46EA288-25A5-4866-94C6-61766FEDC38A</field><field id=\"DocTypeId\" type=\"SDBint\">1701</field><field id=\"FolderName\" type=\"DBvarchar\">LENDINGQB</field></eDoc><eDoc><field id=\"DocTypeName\" type=\"DBvarchar\">MORTGAGE INSURANCE DOCUMENT</field><field id=\"CreatedDate\" type=\"DBdatetime\">2016-09-21 15:26:11.600</field><field id=\"LastModifiedDate\" type=\"DBdatetime\">2016-09-21 15:26:23.073</field><field id=\"NumPages\" type=\"SDBint\">2</field><field id=\"DocumentId\" type=\"SDBuniqueidentifier\">3A4131A6-08ED-42D1-8C2F-AA5DE1EDA7B5</field><field id=\"DocTypeId\" type=\"SDBint\">2876</field><field id=\"FolderName\" type=\"DBvarchar\">MORTGAGE INSURANCE DOCUMENTS</field></eDoc><eDoc><field id=\"DocTypeName\" type=\"DBvarchar\">112150134 DOC TYPE 4</field><field id=\"CreatedDate\" type=\"DBdatetime\">2016-09-19 19:32:15.880</field><field id=\"LastModifiedDate\" type=\"DBdatetime\">2016-09-19 19:36:31.547</field><field id=\"NumPages\" type=\"SDBint\">1</field><field id=\"DocumentId\" type=\"SDBuniqueidentifier\">53454BFF-71E5-4381-9EA7-3E637E9A16A4</field><field id=\"DocTypeId\" type=\"SDBint\">4428</field><field id=\"FolderName\" type=\"DBvarchar\">112150134 Folder 1</field></eDoc><eDoc><field id=\"DocTypeName\" type=\"DBvarchar\">CREDIT REPORT</field><field id=\"CreatedDate\" type=\"DBdatetime\">2016-09-06 17:58:41.577</field><field id=\"LastModifiedDate\" type=\"DBdatetime\">2016-09-06 17:58:55.170</field><field id=\"NumPages\" type=\"SDBint\">6</field><field id=\"DocumentId\" type=\"SDBuniqueidentifier\">E142AD20-9576-4681-B078-31C073DBEB47</field><field id=\"DocTypeId\" type=\"SDBint\">183</field><field id=\"FolderName\" type=\"DBvarchar\">CREDIT</field></eDoc><eDoc><field id=\"DocTypeName\" type=\"DBvarchar\">SADA</field><field id=\"CreatedDate\" type=\"DBdatetime\">2016-08-23 14:23:58.863</field><field id=\"LastModifiedDate\" type=\"DBdatetime\">2016-08-23 14:24:09.140</field><field id=\"NumPages\" type=\"SDBint\">8</field><field id=\"DocumentId\" type=\"SDBuniqueidentifier\">A8543249-5FAF-4D38-9BCB-F48B66C81D89</field><field id=\"DocTypeId\" type=\"SDBint\">13538</field><field id=\"FolderName\" type=\"DBvarchar\" null=\"true\" /></eDoc></LendingQBLoanData>";

        public static IEnumerable<Dictionary<string, Field>> EmptyStringDataBook()
        {
            return new List<Dictionary<string, Field>>();
        }

        public static IEnumerable<Dictionary<EmployeeFieldId, Field>> EmptyEmployeeDataBook()
        {
            return new List<Dictionary<EmployeeFieldId, Field>>();
        }
        public static IEnumerable<Dictionary<EDocsFieldId, Field>> EmptyEDocsDataBook()
        {
            return new List<Dictionary<EDocsFieldId, Field>>();
        }

        public static readonly string EmptyRecordsString = "<LendingQBLoanData version=\"1.5\" />";

        public static IEnumerable<Dictionary<string, Field>> TwoRecordSimpleDataBook()
        {
            return new List<Dictionary<string, Field>>()
            {
                new Dictionary<string, Field>
                {
                    { "field 1", new Field("item1field1Value", DBTypes.DBchar) },
                    { "field 2", new Field("100", DBTypes.DBint) }
                },
                new Dictionary<string, Field>
                {
                    { "field 1", new Field("item2field1Value", DBTypes.DBvarchar) },
                    { "field 2", new Field("false", DBTypes.DBbit) }
                }
            };
        }

        public static readonly IReadOnlyDictionary<E_ContentType, string> SimpleRecordsOutputStrings = new ReadOnlyDictionary<E_ContentType, string>(new Dictionary<E_ContentType, string>()
        {
            {E_ContentType.Loan, "<LendingQBLoanData version=\"1.5\"><loan><field id=\"field 1\" type=\"DBchar\">item1field1Value</field><field id=\"field 2\" type=\"DBint\">100</field></loan><loan><field id=\"field 1\" type=\"DBvarchar\">item2field1Value</field><field id=\"field 2\" type=\"DBbit\">false</field></loan></LendingQBLoanData>"},
            {E_ContentType.App, "<LendingQBLoanData version=\"1.5\"><app><field id=\"field 1\" type=\"DBchar\">item1field1Value</field><field id=\"field 2\" type=\"DBint\">100</field></app><app><field id=\"field 1\" type=\"DBvarchar\">item2field1Value</field><field id=\"field 2\" type=\"DBbit\">false</field></app></LendingQBLoanData>"},
            {E_ContentType.Task, "<LendingQBLoanData version=\"1.5\"><task><field id=\"field 1\" type=\"DBchar\">item1field1Value</field><field id=\"field 2\" type=\"DBint\">100</field></task><task><field id=\"field 1\" type=\"DBvarchar\">item2field1Value</field><field id=\"field 2\" type=\"DBbit\">false</field></task></LendingQBLoanData>"},
            {E_ContentType.EDoc, "<LendingQBLoanData version=\"1.5\"><eDoc><field id=\"field 1\" type=\"DBchar\">item1field1Value</field><field id=\"field 2\" type=\"DBint\">100</field></eDoc><eDoc><field id=\"field 1\" type=\"DBvarchar\">item2field1Value</field><field id=\"field 2\" type=\"DBbit\">false</field></eDoc></LendingQBLoanData>"},
            {E_ContentType.EDocsAuditHistory, "<LendingQBLoanData version=\"1.5\"><eDoc><field id=\"field 1\" type=\"DBchar\">item1field1Value</field><field id=\"field 2\" type=\"DBint\">100</field></eDoc><eDoc><field id=\"field 1\" type=\"DBvarchar\">item2field1Value</field><field id=\"field 2\" type=\"DBbit\">false</field></eDoc></LendingQBLoanData>"},
            {E_ContentType.Employee, "<LendingQBLoanData version=\"1.5\"><employee><field id=\"field 1\" type=\"DBchar\">item1field1Value</field><field id=\"field 2\" type=\"DBint\">100</field></employee><employee><field id=\"field 1\" type=\"DBvarchar\">item2field1Value</field><field id=\"field 2\" type=\"DBbit\">false</field></employee></LendingQBLoanData>"},
            {E_ContentType.Employee2, "<LendingQBLoanData version=\"1.5\"><employee><field id=\"field 1\" type=\"DBchar\">item1field1Value</field><field id=\"field 2\" type=\"DBint\">100</field></employee><employee><field id=\"field 1\" type=\"DBvarchar\">item2field1Value</field><field id=\"field 2\" type=\"DBbit\">false</field></employee></LendingQBLoanData>"},
            {E_ContentType.PMLUser, "<LendingQBLoanData version=\"1.5\"><pmlUser><field id=\"field 1\" type=\"DBchar\">item1field1Value</field><field id=\"field 2\" type=\"DBint\">100</field></pmlUser><pmlUser><field id=\"field 1\" type=\"DBvarchar\">item2field1Value</field><field id=\"field 2\" type=\"DBbit\">false</field></pmlUser></LendingQBLoanData>"},
            {E_ContentType.OriginatingCompany, "<LendingQBLoanData version=\"1.5\"><originatingCompany><field id=\"field 1\" type=\"DBchar\">item1field1Value</field><field id=\"field 2\" type=\"DBint\">100</field></originatingCompany><originatingCompany><field id=\"field 1\" type=\"DBvarchar\">item2field1Value</field><field id=\"field 2\" type=\"DBbit\">false</field></originatingCompany></LendingQBLoanData>"},
        });

        public static IEnumerable<Dictionary<string, Field>> TwoRecordSimpleDataBookWithNulls()
        {
            return new List<Dictionary<string, Field>>()
            {
                new Dictionary<string, Field>
                {
                    { "field 1", new Field("item1field1Value", DBTypes.DBchar) },
                    { "field 2", new Field("100", DBTypes.DBint) },
                    { "field 3", new Field(null, DBTypes.DBmoney) }
                },
                new Dictionary<string, Field>
                {
                    { "field 1", new Field("", DBTypes.DBvarchar) },
                    { "field 2", new Field("false", DBTypes.DBbit) },
                    { "field 3", new Field("103", DBTypes.DBdecimal) }
                }
            };
        }

        public static readonly string SimpleRecordsWithNullsString = "<LendingQBLoanData version=\"1.5\"><loan><field id=\"field 1\" type=\"DBchar\">item1field1Value</field><field id=\"field 2\" type=\"DBint\">100</field><field id=\"field 3\" type=\"DBmoney\" null=\"true\" /></loan><loan><field id=\"field 1\" type=\"DBvarchar\"></field><field id=\"field 2\" type=\"DBbit\">false</field><field id=\"field 3\" type=\"DBdecimal\">103</field></loan></LendingQBLoanData>";

        public static IEnumerable<Dictionary<string, Field>> SimpleDataBookWithNullRecords()
        {
            return new List<Dictionary<string, Field>>()
            {
                null,
                null,
                new Dictionary<string, Field>
                {
                    { "field 1", new Field("item1field1Value", DBTypes.DBchar) },
                    { "field 2", new Field("100", DBTypes.DBint) },
                    { "field 3", new Field(null, DBTypes.DBmoney) }
                },
                null,
                new Dictionary<string, Field>
                {
                    { "field 1", new Field("", DBTypes.DBvarchar) },
                    { "field 2", new Field("false", DBTypes.DBbit) },
                    { "field 3", new Field("103", DBTypes.DBdecimal) }
                },
                null,
                null
            };
        }
    }
}