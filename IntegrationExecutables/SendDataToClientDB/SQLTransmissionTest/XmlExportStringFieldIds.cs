﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SendDataToClientDB;
using SendDataToClientDB.ReceiveDataFromLQBClientService;

namespace SQLTransmissionTest
{

    [TestClass]
    public class XmlExportStringFieldIds
    {
        /// <summary>
        /// Generates XML for a null input list and empty input list. Both should have the same data-less export.
        /// </summary>
        [TestMethod]
        public void EmptyExportMatches()
        {
            Assert.AreEqual(DataForTests.EmptyRecordsString, XMLDocument.ToXMLString(DataForTests.EmptyStringDataBook(), E_ContentType.Loan));
        }

        /// <summary>
        /// Verifies that passing a null record list throws <see cref="ArgumentNullException"/>.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException), "ArgumentNullException is expected for passing null list/book to XMLDocument.ToXMLString()")]
        public void NullListThrows()
        {
            Assert.AreEqual(DataForTests.EmptyRecordsString, XMLDocument.ToXMLString<string>(null, E_ContentType.Loan));
        }


        /// <summary>
        /// Gets XML for the same data with different content types to verify that the xml structure is as expected, and the content type mappings affect the XML as expected.
        /// </summary>
        [TestMethod]
        public void SimpleExportsMatch()
        {
            foreach (E_ContentType contentType in new E_ContentType[] { E_ContentType.Loan, E_ContentType.App, E_ContentType.Task, E_ContentType.EDoc, E_ContentType.EDocsAuditHistory, E_ContentType.Employee, E_ContentType.Employee2, E_ContentType.PMLUser, E_ContentType.OriginatingCompany })
            {
                Assert.AreEqual(DataForTests.SimpleRecordsOutputStrings[contentType], XMLDocument.ToXMLString(DataForTests.TwoRecordSimpleDataBook(), contentType));
            }
        }

        /// <summary>
        /// Verifies that null field values are exported correctly.
        /// </summary>
        [TestMethod]
        public void SimpleExportWithNullsMatches()
        {
            Assert.AreEqual(DataForTests.SimpleRecordsWithNullsString, XMLDocument.ToXMLString(DataForTests.TwoRecordSimpleDataBookWithNulls(), E_ContentType.Loan));
        }

        /// <summary>
        /// If a content type is not handled in the export code (added to the recordLabels dictionary), an InvalidOperationException should be thrown.
        /// </summary>
        [TestMethod]
        public void ThrowsUnhandledContentException()
        {
            // Won't work if int.MaxValue is a backing value for the enum.
            E_ContentType invalidContentType = (E_ContentType)(Enum.GetValues(typeof(E_ContentType)).Cast<int>().Max() + 1);
            try
            {
                XMLDocument.ToXMLString(DataForTests.TwoRecordSimpleDataBook(), invalidContentType);
                Assert.Fail("Exception was expected but was not thrown for E_ContentType value of " + invalidContentType.ToString());
            }
            catch (InvalidOperationException exc)
            {
                Assert.AreEqual("Unhandled enum value=E_ContentType." + invalidContentType.ToString(), exc.Message);
            }
        }

        /// <summary>
        /// Makes sure that all of the current values of <see cref="E_ContentType"/> are handled by the XML writer.
        /// If a new content type is added and not handled by the XML export code, this test will fail.
        /// </summary>
        [TestMethod]
        public void AllDefinedContentTypesHandled()
        {
            IEnumerable<E_ContentType> contentTypes = Enum.GetValues(typeof(E_ContentType)).Cast<E_ContentType>();
            List<E_ContentType> failedContentTypes = new List<E_ContentType>();
            foreach(E_ContentType contentType in contentTypes)
            {
                try
                {
                    XMLDocument.ToXMLString(DataForTests.TwoRecordSimpleDataBook(), contentType);
                }
                catch (InvalidOperationException)
                {
                    failedContentTypes.Add(contentType);
                }
            }
            if(failedContentTypes.Any())
            {
                Assert.Fail("The following content types were not handled: " + string.Join(", ", failedContentTypes));
            }
        }

        /// <summary>
        /// Verify that the export skips null Dictionaries in the list when creating the export.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "listOfRecords may not contain null dictionaries.")]
        public void NullDictionariesThrow()
        {
            Assert.AreEqual(DataForTests.SimpleRecordsWithNullsString, XMLDocument.ToXMLString(DataForTests.SimpleDataBookWithNullRecords(), E_ContentType.Loan));
        }
    }
}
