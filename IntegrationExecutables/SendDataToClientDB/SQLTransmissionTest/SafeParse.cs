﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SendDataToClientDB;

namespace SQLTransmissionTest
{
    [TestClass]
    public class SafeParse
    {
        [TestMethod]
        public void SimpleStringParseReturnsCorrectValue()
        {
            SimpleEnum? outEnum = Tools.SafeParseEnum<SimpleEnum>("One");
            Assert.IsNotNull(outEnum);
            Assert.AreEqual(SimpleEnum.One, outEnum.Value);
        }

        [TestMethod]
        public void SimpleIntParseReturnsCorrectValue()
        { 
            SimpleEnum? outEnum = Tools.SafeParseEnum<SimpleEnum>("1");
            Assert.IsNotNull(outEnum);
            Assert.AreEqual(SimpleEnum.One, outEnum.Value);
        }

        [TestMethod]
        public void FlagsStringParseReturnsCorrectValue()
        {
            FlagsEnum? outEnum = Tools.SafeParseEnum<FlagsEnum>("One, Two");
            Assert.IsNotNull(outEnum);
            Assert.AreEqual(FlagsEnum.One | FlagsEnum.Two, outEnum.Value);
        }

        [TestMethod]
        public void NonMatchingStringParseReturnsNull()
        {
            SimpleEnum? outEnum = Tools.SafeParseEnum<SimpleEnum>("Fish");
            Assert.IsNull(outEnum);
        }

        [TestMethod]
        public void OutOfRangeIntParseReturnsNull()
        {
            SimpleEnum? outEnum = Tools.SafeParseEnum<SimpleEnum>(long.MaxValue.ToString());
            Assert.IsNull(outEnum);
        }

        private enum SimpleEnum
        {
            Zero = 0,
            One = 1,
            Two = 2,
            Three = 3,
            Four = 4,

            Seven = 7
        }

        [Flags]
        private enum FlagsEnum
        {
            Zero = 0,
            One = 1,
            Two = 2,
            Four = 4,
            Eight = 8
        }
    }

    
}
