﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SendDataToClientDB;
using SendDataToClientDB.com.lendingqb.Employee;
using SendDataToClientDB.ReceiveDataFromLQBClientService;

namespace SQLTransmissionTest
{
    [TestClass]
    public class LqbDataImport
    {
        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void LQBDataListConstructorNonEnumThrows()
        {
            var lqbData = new LQBData<int>(new List<Dictionary<int, Field>>());
        }

        /// <summary>
        /// Temporary to show that the constructor doesn't throw when expected not to. In the future, other tests will implicitly cover this.
        /// </summary>
        [TestMethod]
        public void LQBDataListConstructorEnum()
        {
            var lqbData = new LQBData<EmployeeFieldId>(DataForTests.FiveRecordEmployeeDataBook());
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void LQBDataParseNonEnumThrows()
        {
            var lqbData = LQBData<int>.Parse(new XElement("dummy"), new Dictionary<int, DBTypes>(), E_ContentType.App);
        }
    }
}
