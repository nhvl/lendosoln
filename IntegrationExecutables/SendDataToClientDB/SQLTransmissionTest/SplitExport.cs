﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SendDataToClientDB;
using SendDataToClientDB.com.lendingqb.Employee;

namespace SQLTransmissionTest
{
    /// <summary>
    /// 
    /// 
    /// TODO: Implement <see cref="LQBData{TFieldId}.Equals(object o)"/> and use it for value comparisons in these tests.
    /// </summary>
    [TestClass]
    public class SplitExport
    {
        // Null and empty input tests
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SplitNullInListThrows()
        {
            LQBData<EmployeeFieldId> simpleDataExport = DataForTests.OneRecordEmployeeDataContainer();
            simpleDataExport.SplitForMultipleTables(null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SplitManyNullsInListThrows()
        {
            LQBData<EmployeeFieldId> simpleDataExport = DataForTests.OneRecordEmployeeDataContainer();
            simpleDataExport.SplitForMultipleTables(null, null, null, null, null);
        }

        [TestMethod]
        public void SplitEmptyListReturnsInitializedButEmptyList()
        {
            LQBData<EmployeeFieldId> simpleDataExport = DataForTests.OneRecordEmployeeDataContainer();
            var splitExport = simpleDataExport.SplitForMultipleTables(new List<EmployeeFieldId>());
            Assert.IsNotNull(splitExport);
            Assert.AreEqual(1, splitExport.Count);
            Assert.IsNotNull(splitExport[0]);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SplitSomeNullsInListThrows()
        {
            LQBData<EmployeeFieldId> simpleDataExport = DataForTests.OneRecordEmployeeDataContainer();
            var splitExport = simpleDataExport.SplitForMultipleTables(null, new List<EmployeeFieldId>() { EmployeeFieldId.UserFirstNm }, null, null, new List<EmployeeFieldId>() { EmployeeFieldId.UserLastNm }, null);
            var expectedExport = simpleDataExport.SplitForMultipleTables(new List<EmployeeFieldId>() { EmployeeFieldId.UserFirstNm }, new List<EmployeeFieldId>() { EmployeeFieldId.UserLastNm });
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SplitOnEmptyParamListThrows()
        {
            LQBData<EmployeeFieldId> simpleDataExport = DataForTests.OneRecordEmployeeDataContainer();
            simpleDataExport.SplitForMultipleTables();
        }

        // Tests on an LQBData container with only one record
        [TestMethod]
        public void SimpleSplitOneParameterReturnsOneList()
        {
            var fullIdList = DataForTests.OneRecordEmployeeDataContainer().Records[0].Keys;
            LQBData<EmployeeFieldId> simpleDataExport = DataForTests.OneRecordEmployeeDataContainer();
            List<LQBData<EmployeeFieldId>> splitExport = simpleDataExport.SplitForMultipleTables(fullIdList);
            Assert.AreEqual(1, splitExport.Count);
            Assert.AreEqual(simpleDataExport.Records.Count, splitExport[0].Records.Count);
            foreach (EmployeeFieldId id in fullIdList)
            {
                Assert.IsTrue(splitExport[0].Records[0].ContainsKey(id));
            }
            Assert.AreEqual(fullIdList.Count, splitExport[0].Records[0].Count);
        }

        [TestMethod]
        public void SimpleSplitFourParametersReturnsFourLists()
        {
            var fullIdList = DataForTests.OneRecordEmployeeDataContainer().Records[0].Keys;
            LQBData<EmployeeFieldId> simpleDataExport = DataForTests.OneRecordEmployeeDataContainer();
            List<LQBData<EmployeeFieldId>> splitExport = simpleDataExport.SplitForMultipleTables(fullIdList, fullIdList, fullIdList, fullIdList);
            Assert.AreEqual(4, splitExport.Count);
            foreach(LQBData<EmployeeFieldId> data in splitExport)
            {
                Assert.AreEqual(fullIdList.Count, data.Records[0].Count);
                Assert.IsTrue(fullIdList.Union(data.Records[0].Keys).Count() == fullIdList.Intersect(data.Records[0].Keys).Count());
            }
        }

        [TestMethod]
        public void SimpleSplitSmallListCorrect()
        {
            var smallIdList = new List<EmployeeFieldId>() { EmployeeFieldId.UserId, EmployeeFieldId.UserFirstNm };
            LQBData<EmployeeFieldId> simpleDataExport = DataForTests.OneRecordEmployeeDataContainer();
            List<LQBData<EmployeeFieldId>> splitExport = simpleDataExport.SplitForMultipleTables(smallIdList);

            Assert.IsTrue(smallIdList.Union(splitExport[0].Records[0].Keys).Count() == smallIdList.Intersect(splitExport[0].Records[0].Keys).Count());
        }

        [TestMethod]
        public void SimpleSplitEmptyListEmptyReturn()
        {
            var emptyIdList = new List<EmployeeFieldId>() { };
            LQBData<EmployeeFieldId> simpleDataExport = DataForTests.OneRecordEmployeeDataContainer();
            List<LQBData<EmployeeFieldId>> splitExport = simpleDataExport.SplitForMultipleTables(emptyIdList);

            Assert.AreEqual(1, splitExport.Count);
            Assert.AreEqual(0, splitExport[0].Records[0].Count);
        }

        // Tests on LQBData container with five records
        [TestMethod]
        public void SplitFourParametersReturnsFourLists()
        {
            var fullIdList = DataForTests.FiveRecordEmployeeDataContainer().Records[0].Keys.ToList();
            var smallerIdList = new List<EmployeeFieldId>() { EmployeeFieldId.UserId, EmployeeFieldId.LoginNm , EmployeeFieldId.UserFirstNm};
            var largerIdList = new List<EmployeeFieldId>() { EmployeeFieldId.UserLastNm, EmployeeFieldId.IsActiveDirectoryUser, EmployeeFieldId.Fax, EmployeeFieldId.UserId };
            var emptyIdList = new List<EmployeeFieldId>() { };
            List<List<EmployeeFieldId>> parameters = new List<List<EmployeeFieldId>> { fullIdList, smallerIdList, largerIdList, emptyIdList };

            LQBData<EmployeeFieldId> complexDataExport = DataForTests.FiveRecordEmployeeDataContainer();
            List<LQBData<EmployeeFieldId>> splitExport = complexDataExport.SplitForMultipleTables(fullIdList, smallerIdList, largerIdList, emptyIdList);

            Assert.AreEqual(parameters.Count, splitExport.Count);
            for (int i = 0; i < parameters.Count; i++)
            {
                LQBData<EmployeeFieldId> splitExportItem = splitExport[i];
                Assert.AreEqual(complexDataExport.Records.Count, splitExportItem.Records.Count);
                Assert.AreEqual(parameters[i].Count, splitExportItem.Records[0].Count);
                Assert.IsTrue(parameters[i].Union(splitExportItem.Records[0].Keys).Count() == parameters[i].Intersect(splitExportItem.Records[0].Keys).Count());
            }
            Assert.AreEqual(splitExport[0].Records[0][EmployeeFieldId.UserId], splitExport[2].Records[0][EmployeeFieldId.UserId]);
        }
    }
}
