﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SendDataToClientDB;
using SendDataToClientDB.ReceiveDataFromLQBClientService;

namespace SQLTransmissionTest
{
    [TestClass]
    public class XmlExportEnumFieldIds
    {
        /// <summary>
        /// Generates XML for an empty input list. Should have the same data-less export.
        /// </summary>
        [TestMethod]
        public void EmptyExportMatches()
        {
            Assert.AreEqual(DataForTests.EmptyRecordsString, XMLDocument.ToXMLString(DataForTests.EmptyEmployeeDataBook(), E_ContentType.Loan));
            Assert.AreEqual(DataForTests.EmptyRecordsString, XMLDocument.ToXMLString(DataForTests.EmptyEDocsDataBook(), E_ContentType.Loan));
        }

        /// <summary>
        /// Attempts to generate XML for a null input list and verifies that the correct exception is thrown.
        /// </summary>
        [TestMethod]
        public void NullListThrows()
        {
            try
            {
                Assert.AreEqual(DataForTests.EmptyRecordsString, XMLDocument.ToXMLString<SendDataToClientDB.com.lendingqb.EDocs.EDocsFieldId>(null, E_ContentType.Loan));
                Assert.Fail();
            }
            catch (ArgumentNullException)
            { }
            try
            {
                Assert.AreEqual(DataForTests.EmptyRecordsString, XMLDocument.ToXMLString<SendDataToClientDB.com.lendingqb.Employee.EmployeeFieldId>(null, E_ContentType.Loan));
                Assert.Fail();
            }
            catch (ArgumentNullException)
            { }
        }

        /// <summary>
        /// Gets XML for the same data with different content types to verify that the xml structure is as expected, and the content type mappings affect the XML as expected.
        /// </summary>
        [TestMethod]
        public void SimpleExportsMatch()
        {
            Assert.AreEqual(DataForTests.FiveRecordEmployeeOutputString, XMLDocument.ToXMLString(DataForTests.FiveRecordEmployeeDataBook(), E_ContentType.Employee));
            Assert.AreEqual(DataForTests.FiveRecordEDocOutputString, XMLDocument.ToXMLString(DataForTests.FiveRecordEDocDataBook(), E_ContentType.EDoc));
        }
    }
}
