param(
  [Parameter(Mandatory=$true)]
  [string]$CustomerCode,
  [string]$Path = '.\ReceiveDataFromLQB.sln',
  [string]$MSBuildPath = 'C:\Program Files (x86)\MSBuild\14.0\Bin\amd64\MSBuild.exe'
)

Write-Host
Write-Host "Building $Path"
& $MSBuildPath /v:m /t:Rebuild /p:Configuration=Release /p:Platform="Any CPU" /nologo $Path

if ($LastExitCode -ne 0) {
  Write-Host
  Write-Host "Build Failed. Aborting."
  return
}

$ResultDirectory = ".\$CustomerCode\ReceiveDataFromLQB"
Write-Host
Write-Host "Copying result to $ResultDirectory"
New-Item $ResultDirectory, "$ResultDirectory\bin" -Type Directory > $null

# Copy \bin, Client.asmx, and Web.config (setting the customercode)
Copy-Item '.\ReceiveDataFromLQB\bin\*' -Destination "$ResultDirectory\bin" -Exclude '*.dll.config'
Copy-Item .\ReceiveDataFromLQB\Client.asmx $ResultDirectory
Get-Content .\ReceiveDataFromLQB\Web.config |
  ForEach-Object { $_ -replace '"PML0000"', "`"$CustomerCode`"" } |
  Out-File "$ResultDirectory\Web.config"

# We'll need PowerShell 5 to get access to .NET 4.5 and built in zipping utilities
Write-Host
Write-Host "You'll have to manually zip ReceiveDataFromLQB in $ResultDirectory"
Write-Host
Read-Host -Prompt 'Press Enter to continue'
