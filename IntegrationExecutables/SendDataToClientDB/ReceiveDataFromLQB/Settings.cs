﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReceiveDataFromLQB
{
    public static class Settings
    {
        public static bool IsDebug
        {
            get { return bool.Parse(GetSettingsValue("IsDebug", "false")); }
        }
        public static string DebugFilePath
        {
            get { return GetSettingsValue("DebugFilePath", "debug.log"); }
        }

        public static bool RemoveDeletedLoans
        {
            get { return bool.Parse(GetSettingsValue("RemoveDeletedLoans", "false")); }
        }

        public static string Loan_Table
        {
            get { return GetSettingsValue("Loan_Table", "dbo.LOAN"); }
        }
        //public static string Agents_Table
        //{
        //    get { return GetSettingsValue("Agents_Table", "dbo.AGENTS"); }
        //}
        public static string Application_Table
        {
            get { return GetSettingsValue("Application_Table", "dbo.APPLICATION"); }
        }
        //public static string Charges_Table
        //{
        //    get { return GetSettingsValue("Charges_Table", "dbo.CHARGES"); }
        //}
        //public static string Disclosure_Table
        //{
        //    get { return GetSettingsValue("Disclosure_Table", "dbo.DISCLOSURE"); }
        //}
        //public static string Finance_Secondary_Table
        //{
        //    get { return GetSettingsValue("Finance_Secondary_Table", "dbo.FINANCE_SECONDARY"); }
        //}
        //public static string Funding_Shipping_Table
        //{
        //    get { return GetSettingsValue("Funding_Shipping_Table", "dbo.FUNDING_SHIPPING"); }
        //}
        public static string Logging_Table
        {
            get { return GetSettingsValue("Logging_Table", "dbo.LOGGING"); }
        }
        //public static string Property_Table
        //{
        //    get { return GetSettingsValue("Property_Table", "dbo.PROPERTY"); }
        //}
        //public static string Reporting_Table
        //{
        //    get { return GetSettingsValue("Reporting_Table", "dbo.REPORTING"); }
        //}
        //public static string Servicing_Table
        //{
        //    get { return GetSettingsValue("Servicing_Table", "dbo.SERVICING"); }
        //}
        //public static string Status_Table
        //{
        //    get { return GetSettingsValue("Status_Table", "dbo.STATUS"); }
        //}
        //public static string Underwriting_Table
        //{
        //    get { return GetSettingsValue("Underwriting_Table", "dbo.UNDERWRITING"); }
        //}
        //public static string Custom_Table
        //{
        //    get { return GetSettingsValue("Custom_Table", "dbo.CUSTOM"); }
        //}
        public static string Task_Table
        {
            get { return GetSettingsValue("Task_Table", "dbo.TASK"); }
        }

        public static string Employee_Table
        {
            get { return GetSettingsValue("Employee_Table", "dbo.EMPLOYEE"); }
        }

        public static string Employee2_Table
        {
            get { return GetSettingsValue("Employee2_Table", "dbo.EMPLOYEE_2"); }
        }

        public static string Employee_RoleAssignment_Table
        {
            get { return GetSettingsValue(nameof(Employee_RoleAssignment_Table), "dbo.ROLE_ASSIGNMENT"); }
        }

        public static string Table(E_ContentType contentType)
        {
            switch (contentType)
            {
                case E_ContentType.Task: return Task_Table;
                case E_ContentType.OriginatingCompany: return Originating_Company_Table;
                case E_ContentType.OriginatingCompanyRelationship: return Originating_Company_Relationship_Table;
                case E_ContentType.PMLUser: return PML_User_Table;
                case E_ContentType.EDoc: return EDoc_Table;
                case E_ContentType.TaskEDocAssociation: return Task_EDoc_Association_Table;
                case E_ContentType.Employee: return Employee_Table;
                case E_ContentType.Employee2: return Employee2_Table;
                case E_ContentType.EmployeeRoleAssignment: return Employee_RoleAssignment_Table;
                case E_ContentType.EDocsAuditHistory: return EDocs_Audit_History_Table;
                case E_ContentType.MortgagePool: return Mortgage_Pool_Table;
                default:
                    throw UnhandledContentType(contentType);
            }
        }
        public static string Originating_Company_Table
        {
            get { return GetSettingsValue("Originating_Company_Table", "dbo.ORIGINATING_COMPANY"); }
        }
        public static string Originating_Company_Relationship_Table
        {
            get { return GetSettingsValue("Originating_Company_Relationship_Table", "dbo.ORIGINATING_COMPANY_RELATIONSHIP"); }
        }
        public static string PML_User_Table
        {
            get { return GetSettingsValue("PML_User_Table", "dbo.PML_USER"); }
        }
        public static string EDoc_Table
        {
            get { return GetSettingsValue("EDocs_Table", "dbo.EDOC"); }
        }
        public static string EDocs_Audit_History_Table
        {
            get { return GetSettingsValue("EDocs_Audit_History_Table", "dbo.EDocs_Audit_History"); }
        }
        public static string Mortgage_Pool_Table
        {
            get { return GetSettingsValue("Mortgage_Pool_Table", "dbo.MORTGAGE_POOL"); }
        }
        public static string Task_EDoc_Association_Table
        {
            get { return GetSettingsValue("TaskEDoc_Association_Table", "dbo.TASK_EDOC_ASSOCIATION"); }
        }
        internal static string CustomerCode
        {
            get { return GetSettingsValue("CustomerCode", null); }
        }
        /// <summary>
        /// Check the CustomerCode against the config file
        /// </summary>
        internal static bool IsCorrectCustomerCode(string customerCode)
        {
            return CustomerCode == null || CustomerCode.Equals(customerCode, StringComparison.OrdinalIgnoreCase);
        }

        private static string GetSettingsValue(string key, string defaultValue)
        {
            string value = System.Configuration.ConfigurationManager.AppSettings[key];
            
            return string.IsNullOrEmpty(value) ? defaultValue : value;
        }

        public static InvalidOperationException UnhandledContentType(E_ContentType contentType)
        {
            return new InvalidOperationException("Unhandled enum value=E_ContentType." + contentType.ToString());
        }

        /// <summary>
        /// Validates the customer code against the config, and the key against the key for the customer code.
        /// </summary>
        /// <param name="customerCode">The customer code for this lender.</param>
        /// <param name="key">The secret key to allow access to this lender's tool.</param>
        /// <exception cref="System.Exception"><paramref name="customerCode"/> or <paramref name="key"/> is an unexpected value.</exception>
        internal static void ValidateCredentials(string customerCode, string key)
        {
            if (!Settings.IsCorrectCustomerCode(customerCode) || !Settings.IsCorrectSecretKey(key, customerCode))
            {
                throw new Exception("Authentication Error");
            }
        }

        private static readonly Dictionary<string, string> SecretKeys = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
        {
            // Usually just generated through Console.WriteLine(Guid.NewGuid().ToString("N").Substring(20).ToUpper());
            { "MTGPEER_PML0220", "826384D3EC06" },
            { "PML0158", "85606383BC7B" },
            { "PML0214", "5616CB3C635A" },
            { "PML0215", "GD0QY5KZCZAG" },
            { "PML0223", "03KEJAJG8UL0" },
            { "PML0226", "689F95285BEA" },
            { "PML0229", "A386CF261D0F" },
            { "PML0241", "QVQ7ZQI2ZXCK" },
            { "PML0246", "E1C7228D36FB" },
            { "PML0256", "B235A4B692D9" },
            { "PML0263", "E63FF69B7645" },
            { "PML0270", "FE6D17709BC1" },
            { "PML0272", "8CE8F6341960" },
            { "PML0277", "6BBD47DA0A59" },
            { "PML0291", "3C18FD46AB84" },
            { "PML0295", "sKjaUvz7N19jMXAen0Vy" },
            { "PML0299", "A83D2FB4BD9C" },
            { "PML0312", "8CAE69B198FB" },
            { "PML0319", "6206E85C9CB7" },
            { "PML0328", "835E4577E92E" },
            { "THINHTEST2", "A386CF261D0F" },
            { "THINHTEST2_RECEIVER2", "03KEJAJG8UL0" }
        };

        internal static bool IsCorrectSecretKey(string key, string customerCode)
        {
            string secretKey;
            if (SecretKeys.TryGetValue(customerCode, out secretKey))
            {
                return secretKey == key;
            }
            else
            {
                throw new Exception("Authentication Error: Unable to load secret key");
            }
        }
    }
}
