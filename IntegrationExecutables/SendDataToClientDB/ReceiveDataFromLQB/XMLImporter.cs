﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace ReceiveDataFromLQB
{
    public enum E_ContentType
    {
        Loan = 0,
        App = 1,
        Task = 2,
        OriginatingCompany = 3,
        PMLUser = 4,
        EDoc = 5,
        TaskEDocAssociation = 6,
        Employee = 7,
        Employee2 = 8,
        EDocsAuditHistory = 9,
        OriginatingCompanyRelationship = 10,
        EmployeeRoleAssignment = 11,
        MortgagePool = 12,
    }
    public static class XMLImporter
    {
        public static List<Dictionary<string, Field>> Unpack(string sDoc, E_ContentType contentType)
        {
            List<Dictionary<string, Field>> records = new List<Dictionary<string, Field>>();
            XmlDocument XMLdoc = CreateXmlDoc(sDoc);

            XmlElement root = (XmlElement)XMLdoc.SelectSingleNode("//LendingQBLoanData");
            if (root == null)
            {
                throw new XmlException("Missing LendingQBLoanData root element.");
            }

            string sRecordName = GetRecordName(contentType);

            foreach (XmlElement XMLrecord in XMLdoc.SelectNodes("//" + sRecordName))
            {
                records.Add(ReadFields(XMLrecord));
            }
            return records;
        }

        public static string Pack(List<Dictionary<string, Field>> records, E_ContentType contentType)
        {
            string recordName = GetRecordName(contentType);
            var settings = new XmlWriterSettings
            {
                OmitXmlDeclaration = true,
            };
            using (var stringWriter = new StringWriter())
            {
                using (var xmlWriter = XmlWriter.Create(stringWriter, settings))
                {
                    xmlWriter.WriteStartElement("LendingQBLoanData");
                    foreach (var record in records)
                    {
                        xmlWriter.WriteStartElement(recordName);
                        WriteFields(xmlWriter, record);
                        xmlWriter.WriteEndElement();
                    }

                    xmlWriter.WriteEndElement();
                }

                return stringWriter.ToString();
            }
        }

        private static XmlDocument CreateXmlDoc(string sXMLText)
        {
            try
            {
                return CreateXmlDocImpl(sXMLText);
            }
            catch (XmlException)
            {
                sXMLText = SanitizeXmlString(sXMLText);
                return CreateXmlDocImpl(sXMLText);
            }
        }

        private static XmlDocument CreateXmlDocImpl(string sXMLText)
        {
            XmlDocument xmlDoc = new XmlDocument();

            if (null == sXMLText || (sXMLText = sXMLText.Trim()) == string.Empty)
                return xmlDoc;

            XmlTextReader readerData = null;

            try
            {
                if (sXMLText != null && sXMLText.Length <= (4 * 1024))
                {
                    // it runs faster because it uses more memory
                    readerData = new XmlTextReader(sXMLText, XmlNodeType.Document, null);
                }
                else
                {
                    readerData = new XmlTextReader(new StringReader(sXMLText));
                }

                readerData.XmlResolver = null;
                xmlDoc.Load(readerData);
            }
            finally
            {
                if (null != readerData)
                    readerData.Close();
            }
            return xmlDoc;
        }

        private static bool IsLegalXmlChar(int ch)
        {
            //The list of allowable characters http://www.w3.org/TR/REC-xml/#charsets
            return (ch == 0x9 || ch == 0xA || ch == 0xD || (ch >= 0x20 && ch <= 0xD7FF) || (ch >= 0xE000 && ch <= 0xFFFD) || (ch >= 0x10000 && ch <= 0x10FFFF));
        }

        private static bool IsNull(XmlElement XMLfield)
        {
            bool bIsNull = false;

            if (XMLfield.HasAttribute("null"))
            {
                bIsNull = string.Equals(XMLfield.GetAttribute("null").Trim().ToLower(), "true");
            }

            return bIsNull;
        }

        private static Dictionary<string, Field> ReadFields(XmlElement XMLrecord)
        {
            Dictionary<string, Field> record = new Dictionary<string, Field>();

            foreach (XmlElement XMLfield in XMLrecord.SelectNodes("field"))
            {
                string sValue = IsNull(XMLfield) ? null : XMLfield.InnerText;
                record.Add(XMLfield.GetAttribute("id").Trim(), new Field(sValue, XMLfield.GetAttribute("type").Trim(), XMLfield.GetAttribute("table").Trim()));
            }

            return record;
        }

        private static void WriteFields(XmlWriter xmlWriter, Dictionary<string, Field> record)
        {
            foreach (var kvp in record)
            {
                xmlWriter.WriteStartElement("field");
                xmlWriter.WriteAttributeString("id", kvp.Key);
                xmlWriter.WriteAttributeString("type", kvp.Value.DBFormat.ToString());
                if (!string.IsNullOrEmpty(kvp.Value.Table))
                {
                    xmlWriter.WriteAttributeString("table", kvp.Value.Table.ToString());
                }

                if (kvp.Value.Value == null)
                {
                    xmlWriter.WriteAttributeString("null", "true");
                }
                else
                {
                    xmlWriter.WriteValue(kvp.Value.Value);
                }

                xmlWriter.WriteEndElement(); // </field>
            }
        }

        private static string SanitizeXmlString(string sXML)
        {
            StringBuilder sb = new StringBuilder(sXML.Length);
            foreach (char ch in sXML.ToCharArray())
            {
                if (IsLegalXmlChar(ch))
                {
                    sb.Append(ch);
                }
            }
            return sb.ToString();
        }

        private static string GetRecordName(E_ContentType contentType)
        {
            switch (contentType)
            {
                case E_ContentType.Loan: return "loan";
                case E_ContentType.App: return "app";
                case E_ContentType.Task: return "task";
                case E_ContentType.OriginatingCompany: return "originatingCompany";
                case E_ContentType.OriginatingCompanyRelationship: return "originatingCompanyRelationship";
                case E_ContentType.PMLUser: return "pmlUser";
                case E_ContentType.EDocsAuditHistory:
                case E_ContentType.EDoc: return "eDoc";
                case E_ContentType.TaskEDocAssociation: return "taskEDocAssociation";
                case E_ContentType.Employee:
                case E_ContentType.Employee2: return "employee";
                case E_ContentType.EmployeeRoleAssignment: return "employeeRole";
                case E_ContentType.MortgagePool: return "pool";
                default:
                    throw Settings.UnhandledContentType(contentType);
            }
        }
    }
}
