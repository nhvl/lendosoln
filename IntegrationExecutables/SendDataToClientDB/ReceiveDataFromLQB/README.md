# ReceiveDataFromLQB

This web app provides a web service target for the SendDataToClientDB executable.  It
should be hosted via IIS like any other web app.  The main entry point is [Client.asmx](Client.asmx).

A couple things are of note here:

1. We compile and send this to clients, so changes you make to your personal config
should not be committed.
2. We use this as a reference source to any additional receivers a lender may need to
set up.  This means the code should be kept pretty clean and stripped of the
secret keys (or other sensitive data) prior to sending externally.
