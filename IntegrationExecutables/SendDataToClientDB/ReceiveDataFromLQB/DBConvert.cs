﻿using System;
using System.Linq;
using System.Data.SqlClient;

namespace ReceiveDataFromLQB
{
    public static class DBConvert
    {
        public static SqlParameter GetParameter(string sParamName, Field dataField)
        {
            try
            {
                switch (dataField.DBFormat)
                {
                    case DBTypes.DBbigint:
                        return (dataField.Value == null) ? DBNullParam(sParamName) : GetSafeLongParam(sParamName, dataField.Value);
                    case DBTypes.DBbit:
                        return (dataField.Value == null) ? DBNullParam(sParamName) : GetSafeBoolParam(sParamName, dataField.Value);
                    case DBTypes.DBchar:
                        return (dataField.Value == null) ? DBNullParam(sParamName) : GetSafeAnsiStringParam(sParamName, dataField.Value);
                    case DBTypes.DBdatetime:
                        return (dataField.Value == null) ? DBNullParam(sParamName) : GetSafeDateTimeParam(sParamName, dataField.Value);
                    case DBTypes.DBdecimal:
                        return (dataField.Value == null) ? DBNullParam(sParamName) : GetSafeDecimalParam(sParamName, dataField.Value);
                    case DBTypes.DBint:
                        return (dataField.Value == null) ? DBNullParam(sParamName) : GetSafeIntParam(sParamName, dataField.Value);
                    case DBTypes.DBmoney:
                        return (dataField.Value == null) ? DBNullParam(sParamName) : GetSafeDecimalParam(sParamName, dataField.Value);
                    case DBTypes.DBsmalldatetime:
                        return (dataField.Value == null) ? DBNullParam(sParamName) : GetSafeDateTimeParam(sParamName, dataField.Value);
                    case DBTypes.DBtext:
                        return (dataField.Value == null) ? DBNullParam(sParamName) : GetSafeAnsiStringParam(sParamName, dataField.Value);
                    case DBTypes.DBtinyint:
                        return (dataField.Value == null) ? DBNullParam(sParamName) : GetSafeByteParam(sParamName, dataField.Value);
                    case DBTypes.DBuniqueidentifier:
                        return (dataField.Value == null) ? DBNullParam(sParamName) : GetSafeGuidParam(sParamName, dataField.Value);
                    case DBTypes.DBvarchar:
                        return (dataField.Value == null) ? DBNullParam(sParamName) : GetSafeAnsiStringParam(sParamName, dataField.Value);
                    case DBTypes.SDBbigint:
                        return GetSafeLongParam(sParamName, dataField.Value);
                    case DBTypes.SDBbit:
                        return GetSafeBoolParam(sParamName, dataField.Value);
                    case DBTypes.SDBchar:
                        return GetSafeAnsiStringParam(sParamName, dataField.Value);
                    case DBTypes.SDBdatetime:
                        return GetSafeDateTimeParam(sParamName, dataField.Value);
                    case DBTypes.SDBdecimal:
                        return GetSafeDecimalParam(sParamName, dataField.Value);
                    case DBTypes.SDBint:
                        return GetSafeIntParam(sParamName, dataField.Value);
                    case DBTypes.SDBmoney:
                        return GetSafeDecimalParam(sParamName, dataField.Value);
                    case DBTypes.SDBsmalldatetime:
                        return GetSafeDateTimeParam(sParamName, dataField.Value);
                    case DBTypes.SDBtext:
                        return GetSafeAnsiStringParam(sParamName, dataField.Value);
                    case DBTypes.SDBtinyint:
                        return GetSafeByteParam(sParamName, dataField.Value);
                    case DBTypes.SDBuniqueidentifier:
                        return GetSafeGuidParam(sParamName, dataField.Value);
                    case DBTypes.SDBvarchar:
                    default:
                        return GetSafeAnsiStringParam(sParamName, dataField.Value);
                }
            }
            catch(Exception)
            {
                throw new Exception(string.Format("Parameterization failed for {0}. Expected format: {1}. Value: {2}", sParamName, dataField.DBFormat.ToString(), (dataField.Value == null) ? "null" : dataField.Value)) ;
            }
        }

        private static SqlParameter GetSafeBoolParam(string sParamName, string sValue)
        {
            bool bValue;
            bValue = (String.Equals("true", sValue, StringComparison.OrdinalIgnoreCase)) ? true : false;
            return new SqlParameter("@" + sParamName, bValue);
        }

        private static SqlParameter GetSafeDateTimeParam(string sParamName, string sValue)
        {
            return (string.IsNullOrEmpty(sValue)) ? new SqlParameter("@" + sParamName, System.Data.SqlTypes.SqlDateTime.MinValue) :
                new SqlParameter("@" + sParamName, DateTime.Parse(sValue.Trim()));
        }

        private static SqlParameter GetSafeDecimalParam(string sParamName, string sValue)
        {
            if (string.IsNullOrEmpty(sValue))
            {
                sValue = "0.000";
            }
            return new SqlParameter("@" + sParamName, Convert.ToDecimal(sValue.Trim()));
        }

        private static SqlParameter GetSafeGuidParam(string sParamName, string sValue)
        {
            Guid gValue = (string.IsNullOrEmpty(sValue)) ? Guid.Empty : new Guid(sValue.Trim());
            return new SqlParameter("@" + sParamName, gValue);
        }

        private static SqlParameter GetSafeByteParam(string sParamName, string sValue)
        {
            if (string.IsNullOrEmpty(sValue))
            {
                sValue = "0";
            }
            return new SqlParameter("@" + sParamName, Convert.ToByte(sValue.Trim()));
        }

        private static SqlParameter GetSafeIntParam(string sParamName, string sValue)
        {
            if (string.IsNullOrEmpty(sValue))
            {
                sValue = "0";
            }
            return new SqlParameter("@" + sParamName, Convert.ToInt32(sValue.Trim()));
        }

        private static SqlParameter GetSafeLongParam(string sParamName, string sValue)
        {
            if(string.IsNullOrEmpty(sValue))
            {
                sValue = "0";
            }
            return new SqlParameter("@" + sParamName, Convert.ToInt64(sValue.Trim()));
        }

        private static SqlParameter GetSafeAnsiStringParam(string sParamName, string sValue)
        {
            if (sValue == null)
            {
                sValue = "";
            }

            var param = new SqlParameter("@" + sParamName, sValue);
            param.DbType = System.Data.DbType.AnsiString;
            return param;
        }

        private static SqlParameter DBNullParam(string sParamName)
        {
            return new SqlParameter("@" + sParamName, DBNull.Value);
        }
    }
}
