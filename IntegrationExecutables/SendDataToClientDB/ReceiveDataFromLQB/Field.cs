﻿using System;

namespace ReceiveDataFromLQB
{
    // The S in SDBx stands for safe/not null
    public enum DBTypes
    {
        DBbigint,
        DBbit,
        DBchar,
        DBdatetime,
        DBdecimal,
        DBint,
        DBmoney,
        DBsmalldatetime,
        DBtext,
        DBtinyint,
        DBuniqueidentifier,
        DBvarchar,
        SDBbigint,
        SDBbit,
        SDBchar,
        SDBdatetime,
        SDBdecimal,
        SDBint,
        SDBmoney,
        SDBsmalldatetime,
        SDBtext,
        SDBtinyint,
        SDBuniqueidentifier,
        SDBvarchar
    }
    
    public class Field
    {
        #region Variables
        private string m_sValue;
        private DBTypes m_eDBFormat;

        public string Value
        {
            get { return m_sValue; }
        }

        public DBTypes DBFormat
        {
            get { return m_eDBFormat; }
        }

        public string Table { get; private set; }
        #endregion

        public Field(string sValue, string sSQLFormat, string table)
            : this(sValue, (DBTypes)Enum.Parse(typeof(DBTypes), sSQLFormat), table)
        {
        }

        public Field(string value, DBTypes sqlFormat, string table)
        {
            m_sValue = value;
            m_eDBFormat = sqlFormat;
            this.Table = table;
        }
    }
}
