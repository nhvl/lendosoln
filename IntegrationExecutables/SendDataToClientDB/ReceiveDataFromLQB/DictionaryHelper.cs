﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReceiveDataFromLQB
{
    public static class DictionaryHelper
    {
        public static Dictionary<string, Dictionary<string, Field>> ExtractDataByTables(Dictionary<string, Field> data)
        {
            Dictionary<string, Dictionary<string, Field>> dataByTable = new Dictionary<string, Dictionary<string, Field>>(StringComparer.OrdinalIgnoreCase);
            foreach (var pair in data)
            {
                if (!string.IsNullOrEmpty(pair.Value.Table))
                {
                    if (!dataByTable.ContainsKey(pair.Value.Table))
                    {
                        dataByTable.Add(pair.Value.Table, new Dictionary<string, Field>(data.Comparer));
                    }

                    dataByTable[pair.Value.Table].Add(pair.Key, pair.Value);
                }
            }

            var keys = dataByTable[TableFields.KeyTable];
            dataByTable.Remove(TableFields.KeyTable);
            foreach (var table in dataByTable.Values)
            {
                foreach (var key in keys)
                {
                    table.Add(key.Key, key.Value);
                }
            }

            return dataByTable;
        }

        public static List<Dictionary<string, Dictionary<string, Field>>> ExtractLoanAppsByTable(List<Dictionary<string, Field>> allApps, Guid loanId)
        {
            var loanApps = GetLoanApps(allApps, loanId.ToString());
            var appList = new List<Dictionary<string, Dictionary<string, Field>>>(loanApps.Count);
            foreach (var app in loanApps)
            {
                appList.Add(ExtractDataByTables(app));
            }

            return appList;
        }

        public static List<Dictionary<string, Field>> GetLoanApps(List<Dictionary<string, Field>> apps, string sLId)
        {
            return GetLoanLinkedData(apps, sLId, E_ContentType.App);
        }

        public static List<Dictionary<string, Field>> GetLoanTasks(List<Dictionary<string, Field>> tasks, string sLId)
        {
            return GetLoanLinkedData(tasks, sLId, E_ContentType.Task);
        }

        private static List<Dictionary<string, Field>> GetLoanLinkedData(List<Dictionary<string, Field>> data, string sLId, E_ContentType contentType)
        {
            string fieldKeyLoanId = TableFields.LoanIDKeyName(contentType);
            List<Dictionary<string, Field>> loanData = new List<Dictionary<string, Field>>(data.Count);
            foreach (Dictionary<string, Field> dataBook in data)
            {
                Field fLoanID = null;
                if (dataBook.TryGetValue(fieldKeyLoanId, out fLoanID))
                {
                    if (string.Equals(sLId, fLoanID.Value, StringComparison.OrdinalIgnoreCase))
                    {
                        loanData.Add(dataBook);
                    }
                }
            }

            return loanData;
        }

        public static bool LoanIsDeleted(Dictionary<string, Field> Loan)
        {
            return !GetSafeBool(Loan, "IsValid");
        }

        public static Guid GetSafeGuid(Dictionary<string, Field> Book, string sKey)
        {
            Field fValue;
            bool bSafe = (Book.TryGetValue(sKey, out fValue)) && (fValue.Value != null);
            return bSafe ? new Guid(fValue.Value) : Guid.Empty;
        }

        public static int GetSafeInt(Dictionary<string, Field> Book, string sKey)
        {
            Field fValue;
            bool bSafe = (Book.TryGetValue(sKey, out fValue)) && (fValue.Value != null);
            return bSafe ? Int32.Parse(fValue.Value.Trim()) : 0;
        }
        private static bool GetSafeBool(Dictionary<string, Field> Book, string sKey)
        {
            bool bValue;
            Field fValue;
            return Book.TryGetValue(sKey, out fValue) && bool.TryParse(fValue.Value, out bValue) ? bValue : false;
        }

        public static Field GetPrimaryKey(Dictionary<string, Field> book, string primaryKeyName)
        {
            Field value;
            if (book.TryGetValue(primaryKeyName, out value))
            {
                return value;
            }
            else
            {
                throw new Exception("Unable to find Primary Key \"" + primaryKeyName + "\" in field record lookup");
            }
        }

        public static Field GetValue(Dictionary<string, Field> Book, string sKey)
        {
            Field fValue = null;
            Book.TryGetValue(sKey, out fValue);
            return fValue;
        }

        public static void VerifyKeysForTable(Dictionary<string, Field> Book, IEnumerable<string> sFields)
        {
            foreach (string sField in sFields)
            {
                if (!Book.ContainsKey(sField))
                    throw new Exception("Dictionary does not contain expected field: " + sField);
            }
        }
    }
}
