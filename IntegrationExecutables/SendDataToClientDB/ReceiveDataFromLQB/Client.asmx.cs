﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace ReceiveDataFromLQB
{
    /// <summary>
    /// This web service is used for loan data replication on a client DB.
    /// </summary>
    [WebService(Namespace = "http://www.lendersoffice.com/replication/")]
    public class Client : System.Web.Services.WebService
    {
        #region Variables
        private int m_iLogID;
        private int m_iNumLoanRecordsUpdated;
        private string m_sCustomerCode;
        #endregion

        [WebMethod(Description = "Counts the loan records in the Loan Table.")]
        public int LoanCount()
        {
            return DB.RecordCount(E_ContentType.Loan);
        }

        [WebMethod(Description = "Get the latest last modified date among employee records in the local database. [sCustomerCode] The lender's LendingQB customer code.")]
        public DateTime? EmployeeLastModified(string sCustomerCode, string sKey)
        {
            Settings.ValidateCredentials(sCustomerCode, sKey);
            return DB.LatestEmployeeModifiedDate();
        }

        [WebMethod(Description = "Get the largest version number among EDocs records in the local database. [sCustomerCode] The lender's LendingQB customer code.")]
        public int LargestEDocsVersionNumber(string sCustomerCode, string sKey)
        {
            Settings.ValidateCredentials(sCustomerCode, sKey);
            return DB.LargestEDocsVersionNumber();
        }

        [WebMethod(Description = "Copy loan data to the local database. [Loans] An XML document of loans to copy. [Apps] An XML document of Applications associated with the loan files. [sCustomerCode] The lender's LendingQB customer code.")]
        public string ReplicateLoans(string sLoans, string sApps, string sCustomerCode, string sKey)
        {
            List<Dictionary<string, Field>> LoanBook = XMLImporter.Unpack(sLoans, E_ContentType.Loan);
            List<Dictionary<string, Field>> AppBook = XMLImporter.Unpack(sApps, E_ContentType.App);
            List<Dictionary<string, Field>> TaskBook = new List<Dictionary<string, Field>>(0);

            return ReplicateLoansImpl(LoanBook, AppBook, TaskBook, sCustomerCode, sKey);
        }

        [WebMethod(Description = "Copy loan data to the local database. [Loans] An XML document of loans to copy. [Apps] An XML document of Applications associated with the loan files. [Tasks] An XML document of Tasks associated with the loan files. [sCustomerCode] The lender's LendingQB customer code.")]
        public string ReplicateLoansIncludingTasks(string sLoans, string sApps, string sTasks, string sCustomerCode, string sKey)
        {
            List<Dictionary<string, Field>> LoanBook = XMLImporter.Unpack(sLoans, E_ContentType.Loan);
            List<Dictionary<string, Field>> AppBook = XMLImporter.Unpack(sApps, E_ContentType.App);
            List<Dictionary<string, Field>> TaskBook = XMLImporter.Unpack(sTasks, E_ContentType.Task);

            return ReplicateLoansImpl(LoanBook, AppBook, TaskBook, sCustomerCode, sKey);
        }

        private string ReplicateLoansImpl(List<Dictionary<string, Field>> Loans, List<Dictionary<string, Field>> Apps, List<Dictionary<string, Field>> Tasks, string sCustomerCode, string sKey)
        {
            m_iLogID = -1;
            m_iNumLoanRecordsUpdated = 0;
            m_sCustomerCode = sCustomerCode;
            try
            {
                Settings.ValidateCredentials(sCustomerCode, sKey);
                TableFields.ApplyCustomerCodeToTableFields(sCustomerCode);
                switch (sCustomerCode)
                {
                    case "PMLTEST5":
                        break;
                    case "PML0214":
                    case "PML0223":
                    case "PML0270":
                        ReplicateFullCacheAndLog(Loans, Apps, Tasks);
                        break;
                    default:
                        ReplicateFullCache(Loans, Apps, Tasks);
                        break;
                }
            }
            catch (Exception exc)
            {
                string sExc = exc.ToString();
                try
                {
                    DB.LogResult(m_iLogID, m_iNumLoanRecordsUpdated, sExc, false);
                    return sExc;
                }
                catch
                {
                    return sExc; // The loan/app record related exception is more important than the log related exception, so return it
                }
            }
            return "OK";
        }

        private void ReplicateFullCacheAndLog(List<Dictionary<string, Field>> Loans, List<Dictionary<string, Field>> Apps, List<Dictionary<string, Field>> Tasks)
        {
            m_iLogID = DB.LogStartTime();
            ReplicateFullCache(Loans, Apps, Tasks);
            DB.LogResult(m_iLogID, m_iNumLoanRecordsUpdated, string.Empty, true);
        }

        private void ReplicateFullCache(List<Dictionary<string, Field>> Loans, List<Dictionary<string, Field>> Apps, List<Dictionary<string, Field>> Tasks)
        {
            if (Loans.Count > 0)
            {
                Dictionary<string, Field> LoanSanity = Loans[0];
                Dictionary<string, Field> AppSanity = Apps[0];
                Dictionary<string, Field> TaskSanity = Tasks.Count > 0 ? Tasks[0] : null;
                TableFields.ConfirmReceiptOfFullCache(LoanSanity, AppSanity, TaskSanity);

                foreach (Dictionary<string, Field> loan in Loans)
                {
                    Guid sLId = DictionaryHelper.GetSafeGuid(loan, TableFields.LoanPrimaryKey.Single());
                    Dictionary<string, Dictionary<string, Field>> loanByTable = DictionaryHelper.ExtractDataByTables(loan);
                    List<Dictionary<string, Dictionary<string, Field>>> loanAppsByTable = DictionaryHelper.ExtractLoanAppsByTable(Apps, sLId);

                    if ((Settings.RemoveDeletedLoans || m_sCustomerCode.Equals("PML0214", StringComparison.OrdinalIgnoreCase)) && DictionaryHelper.LoanIsDeleted(loan))
                    {
                        // Use the first app on the loan to determine which tables to delete from.
                        DB.DeleteLoanData(sLId, new E_ContentType[] { E_ContentType.Task }, loanByTable.Keys, loanAppsByTable[0].Keys);
                        continue;
                    }

                    DB.InsertOrUpdateLoanRecordAllTables(loanByTable);

                    List<Guid> loanAppIDs = new List<Guid>();
                    foreach (Dictionary<string, Dictionary<string, Field>> appByTable in loanAppsByTable)
                    {
                        Field appId = DictionaryHelper.GetPrimaryKey(appByTable[TableFields.ApplicationTable], TableFields.AppPrimaryKey.Single());
                        loanAppIDs.Add(new Guid(appId.Value));

                        DB.InsertOrUpdateAppRecordAllTables(appByTable);
                    }

                    DB.DeleteAppsDeletedFromLQB(sLId, loanAppIDs, loanAppsByTable[0].Keys);

                    foreach (Dictionary<string, Field> task in DictionaryHelper.GetLoanTasks(Tasks, sLId.ToString()))
                    {
                        DB.InsertOrUpdateTaskRecord(task);
                    }

                    m_iNumLoanRecordsUpdated++;
                }
            }
        }

        [WebMethod(Description = "Copy Originating Company or PML User data to the local database. [data] An XML document of the data to copy. [contentType] OriginatingCompany OR PMLUser. [customerCode] The lender's LendingQB customer code")]
        public string ReplicatePMLData(string data, E_ContentType contentType, string customerCode, string key)
        {
            if (contentType != E_ContentType.OriginatingCompany
                && contentType != E_ContentType.OriginatingCompanyRelationship
                && contentType != E_ContentType.PMLUser)
            {
                throw Settings.UnhandledContentType(contentType);
            }

            return ReplicateData(data, contentType, customerCode, key);
        }

        //private string ReplicateOriginatingCompaniesAndPMLUsers(List<Dictionary<string, Field>> DataBook, E_ContentType ContentType, string sCustomerCode, string sKey)
        //{
        //    try
        //    {
        //        if (Settings.IsCorrectCustomerCode(sCustomerCode)
        //         && Settings.IsCorrectSecretKey(sKey, sCustomerCode) && DataBook.Count > 0)
        //        {
        //            Dictionary<string, Field> dataSanity = (Dictionary<string, Field>)DataBook[0];
        //            TableFields.ConfirmReceiptOfFullCache(dataSanity, ContentType);
        //            foreach (Dictionary<string, Field> record in DataBook)
        //            {
        //                Guid PrimaryKey = DictionaryHelper.GetSafeGuid(record, TableFields.PrimaryKeyName(ContentType));
        //                DB.InsertOrUpdatePmlDataRecord(record, PrimaryKey, ContentType);
        //            }
        //        }
        //    }
        //    catch (Exception exc)
        //    {
        //        return exc.ToString();
        //    }
        //    return "OK";
        //}

        [WebMethod(Description = "Copy generic data to the local database. [data] An XML document of the data to copy. [contentType] The type of content to copy. [customerCode] The lender's LendingQB customer code")]
        public string ReplicateData(string data, E_ContentType contentType, string customerCode, string key)
        {
            if (contentType != E_ContentType.Task
                && contentType != E_ContentType.OriginatingCompany
                && contentType != E_ContentType.OriginatingCompanyRelationship
                && contentType != E_ContentType.PMLUser
                && contentType != E_ContentType.EDoc
                && contentType != E_ContentType.TaskEDocAssociation
                && contentType != E_ContentType.Employee
                && contentType != E_ContentType.Employee2
                && contentType != E_ContentType.EDocsAuditHistory
                && contentType != E_ContentType.EmployeeRoleAssignment
                && contentType != E_ContentType.MortgagePool)
            {
                throw Settings.UnhandledContentType(contentType);
            }

            List<Dictionary<string, Field>> dataBook = XMLImporter.Unpack(data, contentType);
            try
            {
                if (dataBook.Count > 0)
                {
                    Settings.ValidateCredentials(customerCode, key);
                    Dictionary<string, Field> dataSanity = dataBook[0];
                    TableFields.ConfirmReceiptOfFullCache(dataSanity, contentType);

                    foreach (Dictionary<string, Field> record in dataBook)
                    {
                        DB.InsertOrUpdateRecord(record, contentType);
                    }
                }
            }
            catch (Exception exc)
            {
                return exc.ToString();
            }

            return "OK";
        }

        [WebMethod(Description = "Counts the number of records of a given type of data from the local database."
            + " [contentType] The type of content to load."
            + " [customerCode] The lender's LendingQB customer code")]
        public int CountRecords(E_ContentType contentType, string customerCode, string key)
        {
            Settings.ValidateCredentials(customerCode, key);
            return DB.RecordCount(contentType);
        }

        [WebMethod(Description = "List primary keys for a given type of data from the local database."
            + " [contentType] The type of content to load."
            + " [maxCount] The maximum number of records to load. If less than 0, will load all records, which might be slow."
            + " [startIndex] The starting index of the primary keys, used in tandem with maxCount for pagination."
            + " [customerCode] The lender's LendingQB customer code")]
        public string ListRecords(E_ContentType contentType, int maxCount, int startIndex, string customerCode, string key)
        {
            try
            {
                Settings.ValidateCredentials(customerCode, key);
                var records = DB.GetOrderedRecordPrimaryKeys(contentType, startIndex, maxCount); // IMPORTANT: The primary keys never have sensitive data, which we shouldn't be able to get out.
                return XMLImporter.Pack(records, contentType);
            }
            catch (Exception exc)
            {
                return exc.ToString();
            }
        }

        [WebMethod(Description = "Delete generic data from the local database by primary key."
            + " [data] An XML document of the data to delete."
            + " [contentType] The type of content to delete."
            + " [customerCode] The lender's LendingQB customer code")]
        public string DeleteDataByPrimaryKey(string data, E_ContentType contentType, string customerCode, string key)
        {
            try
            {
                List<Dictionary<string, Field>> dataBook = XMLImporter.Unpack(data, contentType);
                if (dataBook.Count > 0)
                {
                    Settings.ValidateCredentials(customerCode, key);
                    Dictionary<string, Field> dataSanity = dataBook[0];
                    TableFields.ConfirmReceiptOfFullCache(dataSanity, contentType);

                    foreach (Dictionary<string, Field> record in dataBook)
                    {
                        DB.DeleteRecordByPrimaryKey(record, contentType);
                    }
                }
            }
            catch (Exception exc)
            {
                return exc.ToString();
            }

            return "OK";
        }
    }
}
