﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Data;
using System.Data.SqlTypes;

namespace ReceiveDataFromLQB
{
    public class DB
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sLoanID"></param>
        /// <param name="additionalContentTypes"></param>
        /// <param name="loanTableNames"></param>
        /// <param name="appTableNames"></param>
        public static void DeleteLoanData(Guid sLoanID, E_ContentType[] additionalContentTypes, ICollection<string> loanTableNames, ICollection<string> appTableNames)
        {
            List<E_ContentType> loanContentTypes = new List<E_ContentType> { E_ContentType.Loan, E_ContentType.App };
            if (additionalContentTypes != null && additionalContentTypes.Length > 0)
            {
                loanContentTypes.AddRange(additionalContentTypes);
            }

            Dictionary<E_ContentType, ICollection<string>> tables = new Dictionary<E_ContentType, ICollection<string>>
            {
                { E_ContentType.Loan, loanTableNames },
                { E_ContentType.App, appTableNames },
                { E_ContentType.Task, new string[0] },
                { E_ContentType.EDoc, new string[0] },
                { E_ContentType.TaskEDocAssociation, new string[0] }
            };

            loanContentTypes.Reverse(); // Want to remove loan records last, so reverse
            List<SqlParameter> parameters;
            string sSQL;
            foreach (var contentType in loanContentTypes)
            {
                string primaryKeyTable = TableFields.GetPrimaryKeyTable(contentType);
                foreach (string tableName in tables[contentType])
                {
                    if (!string.Equals(tableName, primaryKeyTable, StringComparison.OrdinalIgnoreCase))
                    {
                        sSQL = GetDeleteByLoanIDStatement(sLoanID, tableName, contentType, out parameters);
                        ExecuteNonQuery(sSQL, parameters);
                    }
                }

                // Delete from primary key table for last, so that deletion isn't hindered by key violations
                sSQL = GetDeleteByLoanIDStatement(sLoanID, primaryKeyTable, contentType, out parameters);
                ExecuteNonQuery(sSQL, parameters);
            }
        }

        private static string GetDeleteByLoanIDStatement(Guid sLoanID, string sTableName, E_ContentType contentType, out List<SqlParameter> parameters)
        {
            string sLIDKey = TableFields.LoanIDKeyName(contentType);

            parameters = new List<SqlParameter>() { new SqlParameter("@" + sLIDKey, sLoanID) };

            return @"DELETE FROM " + sTableName + " WHERE " + sLIDKey + "=@" + sLIDKey;
        }

        public static void DeleteAppsDeletedFromLQB(Guid sLoanID, List<Guid> appsToKeep, ICollection<string> appTables)
        {
            foreach (string tableName in appTables.Reverse())
            {
                if (!string.Equals(tableName, TableFields.ApplicationTable, StringComparison.OrdinalIgnoreCase))
                {
                    DeleteApp(sLoanID, tableName, appsToKeep);
                }
            }

            // Delete from Application table last, as it contains the primary key that is the root of all the others.
            DeleteApp(sLoanID, TableFields.ApplicationTable, appsToKeep);
        }

        private static void DeleteApp(Guid sLoanID, string tableName, List<Guid> appsToKeep)
        {
            List<SqlParameter> parameters;
            string sSQL = GetDeleteByLoanIDStatement(sLoanID, tableName, E_ContentType.App, out parameters);
            string appPrimaryKeyName = TableFields.AppPrimaryKey.Single();
            foreach (Guid appId in appsToKeep)
            {
                sSQL = sSQL + " AND " + appPrimaryKeyName + " <> '" + appId.ToString() + "'";
            }

            ExecuteNonQuery(sSQL, parameters);
        }

        /// <summary>
        /// Extracts the primary keys from <paramref name="record"/> for the specified type and deletes the
        /// record from the database.
        /// </summary>
        /// <param name="record">The record to delete.</param>
        /// <param name="contentType">The type of record to delete.</param>
        public static void DeleteRecordByPrimaryKey(Dictionary<string, Field> record, E_ContentType contentType)
        {
            var primaryKeyNames = TableFields.PrimaryKeyNames(contentType);
            var parameters = new List<SqlParameter>(primaryKeyNames.Count);
            foreach (var primaryKeyName in primaryKeyNames)
            {
                parameters.Add(DBConvert.GetParameter(primaryKeyName, DictionaryHelper.GetPrimaryKey(record, primaryKeyName)));
            }

            string sql = "DELETE FROM " + Settings.Table(contentType) + " WHERE " + GetPrimaryKeyWhereClause(primaryKeyNames);
            ExecuteNonQuery(sql, parameters);
        }

        public static void InsertOrUpdateLoanRecordAllTables(Dictionary<string, Dictionary<string, Field>> recordByTable)
        {
            InsertOrUpdateRecordAllTables(recordByTable, E_ContentType.Loan);
        }

        public static void InsertOrUpdateAppRecordAllTables(Dictionary<string, Dictionary<string, Field>> recordByTable)
        {
            InsertOrUpdateRecordAllTables(recordByTable, E_ContentType.App);
        }

        private static void InsertOrUpdateRecordAllTables(Dictionary<string, Dictionary<string, Field>> recordByTable, E_ContentType contentType)
        {
            // Insert into Primary Key table first to prevent possible key violations.
            string primaryKeyTable = TableFields.GetPrimaryKeyTable(contentType);
            InsertOrUpdateRecord(recordByTable[primaryKeyTable], primaryKeyTable, contentType);
            foreach (KeyValuePair<string, Dictionary<string, Field>> table in recordByTable)
            {
                if (!string.Equals(table.Key, primaryKeyTable, StringComparison.OrdinalIgnoreCase))
                {
                    InsertOrUpdateRecord(table.Value, table.Key, contentType);
                }
            }
        }

        public static void InsertOrUpdateTaskRecord(Dictionary<string, Field> record)
        {
            InsertOrUpdateRecord(record, E_ContentType.Task);
        }
        public static void InsertOrUpdateRecord(Dictionary<string, Field> record, E_ContentType contentType)
        {
            string sTableName = Settings.Table(contentType);
            InsertOrUpdateRecord(record, sTableName, contentType);
        }
        private static void InsertOrUpdateRecord(Dictionary<string, Field> record, string sTableName, E_ContentType contentType)
        {
            if (RecordExists(record, sTableName, contentType))
            {
                UpdateRecord(record, sTableName, contentType);
            }
            else
            {
                InsertRecord(record, sTableName, contentType);
            }
        }

        public static void LogResult(int iLogID, int iNumLoanRecordsUpdated, string sErrorMessage, bool bSuccess)
        {
            if (iLogID > -1)
            {
                string sPKey = "UniqueID";
                string sTEndFieldNm = "TransmissionEndTime";
                string sLoanCountFieldNm = "TotalLoansUpdated";
                string sErrorMsgFieldNm = "TransmissionErrorMsg";
                string sTStatusFieldNm = "TransmissionStatus";

                if (sErrorMessage.Length > 4000)
                {
                    sErrorMessage = sErrorMessage.Substring(0, 4000);
                }

                List<SqlParameter> parameters = new List<SqlParameter>();

                parameters.Add(new SqlParameter("@" + sPKey, iLogID));
                parameters.Add(new SqlParameter("@" + sTEndFieldNm, DateTime.Now));
                parameters.Add(new SqlParameter("@" + sLoanCountFieldNm, (iNumLoanRecordsUpdated > 0) ? iNumLoanRecordsUpdated : (object)0));
                parameters.Add(new SqlParameter("@" + sErrorMsgFieldNm, sErrorMessage));
                parameters.Add(new SqlParameter("@" + sTStatusFieldNm, (bSuccess) ? "Success" : "Failure"));

                string sSQL = @"UPDATE " + Settings.Logging_Table + " SET " + string.Join(", ", SetFieldsEqualToParameterName(new string[] { sTEndFieldNm, sLoanCountFieldNm, sErrorMsgFieldNm, sTStatusFieldNm })) + " " +
                    "WHERE " + sPKey + "=@" + sPKey;

                ExecuteNonQuery(sSQL, parameters);
            }
        }

        public static int LogStartTime()
        {
            string sTStartFieldNm = "TransmissionStartTime";
            string sLoanCountFieldNm = "TotalLoansUpdated";
            string sTStatusFieldNm = "TransmissionStatus";
            string[] fieldNamesToInsert = new string[] { sTStartFieldNm, sLoanCountFieldNm, sTStatusFieldNm };

            List<SqlParameter> parameters = new List<SqlParameter>();

            parameters.Add(new SqlParameter("@" + sTStartFieldNm, DateTime.Now));
            parameters.Add(new SqlParameter("@" + sLoanCountFieldNm, (object)0));
            parameters.Add(new SqlParameter("@" + sTStatusFieldNm, "Success"));

            string sSQL = @"INSERT INTO " + Settings.Logging_Table + " (" + string.Join(", ", fieldNamesToInsert) + ") " +
                "OUTPUT INSERTED.UniqueID VALUES (@" + string.Join(", @", fieldNamesToInsert) + ");";

            return (int)ExecuteScalar(sSQL, parameters);
        }

        private static string[] SetFieldsEqualToParameterName(ICollection<string> fields)
        {
            string[] fieldsSetEqual = new string[fields.Count];
            int i = 0;
            foreach (var field in fields)
            {
                fieldsSetEqual[i++] = field + "=@" + field;
            }

            return fieldsSetEqual;
        }

        private static string GetPrimaryKeyWhereClause(ICollection<string> primaryKeyNames)
        {
            return string.Join(" AND ", SetFieldsEqualToParameterName(primaryKeyNames));
        }

        private static SqlConnection GetConnection()
        {
            return new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DSN"].ConnectionString);
        }

        private static List<SqlParameter> GetSQLParams(ICollection<string> fieldNames, Dictionary<string, Field> record)
        {
            List<SqlParameter> parameters = new List<SqlParameter>(fieldNames.Count);
            List<string> missingFields = new List<string>();
            foreach (string field in fieldNames)
            {
                Field dataField = DictionaryHelper.GetValue(record, field);
                if (dataField != null)
                {
                    parameters.Add(DBConvert.GetParameter(field, dataField));
                }
                else
                {
                    missingFields.Add(field);
                }
            }

            if (missingFields.Any())
            {
                throw new Exception("Unable to find fields \"" + string.Join("\", \"", missingFields.ToArray()) + "\" in record lookup");
            }

            return parameters;
        }

        private static void InsertRecord(Dictionary<string, Field> record, string sTableName, E_ContentType contentType)
        {
            string[] fieldNames = record.Keys.ToArray();

            List<SqlParameter> parameters = GetSQLParams(fieldNames, record);

            string sSQL = @"INSERT INTO " + sTableName + " (" + string.Join(", ", fieldNames) + ") "
                + "VALUES (@" + string.Join(", @", fieldNames) + ");";

            ExecuteNonQuery(sSQL, parameters);
        }

        private static void UpdateRecord(Dictionary<string, Field> record, string sTableName, E_ContentType contentType)
        {
            var primaryKeyNames = TableFields.PrimaryKeyNames(contentType);
            List<string> fieldNamesWithoutPrimaryKeys = record.Keys.Except(primaryKeyNames).ToList();

            IEnumerable<SqlParameter> parameters = GetSQLParams(primaryKeyNames, record).Concat(GetSQLParams(fieldNamesWithoutPrimaryKeys, record));

            string sSQL = @"UPDATE " + sTableName + " SET " + string.Join(", ", SetFieldsEqualToParameterName(fieldNamesWithoutPrimaryKeys)) + " " +
                "WHERE " + GetPrimaryKeyWhereClause(primaryKeyNames);

            ExecuteNonQuery(sSQL, parameters);
        }

        private static bool RecordExists(Dictionary<string, Field> record, string sTableName, E_ContentType contentType)
        {
            var primaryKeyNames = TableFields.PrimaryKeyNames(contentType);

            string sQuery = @"SELECT COUNT(*) AS NumRecords FROM " + sTableName + " WHERE " + GetPrimaryKeyWhereClause(primaryKeyNames);
            List<SqlParameter> parameters = new List<SqlParameter>(primaryKeyNames.Count);
            foreach (var primaryKeyName in primaryKeyNames)
            {
                parameters.Add(DBConvert.GetParameter(primaryKeyName, DictionaryHelper.GetPrimaryKey(record, primaryKeyName)));
            }

            using (IDataReader reader = DB.ExecuteReader(sQuery, parameters))
            {
                if (reader.Read())
                {
                    int cnt = (int)reader["NumRecords"];
                    return cnt > 0;
                }
            }

            return false;
        }

        /// <summary>
        /// Gets the primary keys for the specified content type, filtered to a subset of the ordered list of primary keys.
        /// </summary>
        /// <param name="contentType">The type of the record.</param>
        /// <param name="startRow">The 0-indexed starting row for the selection, assuming the count is restricted.</param>
        /// <param name="count">The maximum number of records to return.  Pass a non-positive value to simply return every record, ignoring <paramref name="startRow"/>.</param>
        /// <returns>A list of the primary key portion of records found in the database.</returns>
        /// <remarks>
        /// This is intended to allow pagination through the primary keys of a given type, so we can do:
        /// <c>GetOrderedPrimarkyKeys(contentType, 0, 100)</c> followed by <c>GetOrderedPrimarkyKeys(contentType, 100, 100)</c>
        /// to get the first 200 primary keys in two batches.
        /// </remarks>
        public static List<Dictionary<string, Field>> GetOrderedRecordPrimaryKeys(E_ContentType contentType, int startRow, int count)
        {
            if (startRow < 0)
            {
                throw new ArgumentOutOfRangeException("startRow", "the starting row must be greater than or equal to zero");
            }

            string sql;
            IEnumerable<SqlParameter> parameters;
            string primaryKeyTable = TableFields.GetPrimaryKeyTable(contentType);
            var primaryKeyNames = TableFields.PrimaryKeyNames(contentType);
            string primaryKeyNameString = string.Join(", ", primaryKeyNames.ToArray());
            if (count > 0)
            {
                sql = @"WITH OrderedTable AS ("
                    + "SELECT ROW_NUMBER() OVER (ORDER BY " + primaryKeyNameString + ") AS RowNumber, " + primaryKeyNameString
                    + " FROM " + primaryKeyTable
                    // ROW_NUMBER() uses a 1-based index, but we'll keep a 0-based index to maintain consistency with the rest of the C# calling code
                    + ") SELECT " + primaryKeyNameString + " FROM OrderedTable WHERE RowNumber > @StartRow AND RowNumber <= @EndRow";
                parameters = new[]
                {
                    new SqlParameter("@StartRow", startRow),
                    new SqlParameter("@EndRow", startRow + count)
                };
            }
            else
            {
                sql = "SELECT " + primaryKeyNameString + " FROM " + primaryKeyTable + " ORDER BY " + primaryKeyNameString;
                parameters = Enumerable.Empty<SqlParameter>();
            }

            List<Dictionary<string, Field>> records = new List<Dictionary<string, Field>>();
            using (IDataReader reader = DB.ExecuteReader(sql, parameters))
            {
                while (reader.Read())
                {
                    Dictionary<string, Field> record = new Dictionary<string, Field>(primaryKeyNames.Count);
                    foreach (var keyName in primaryKeyNames)
                    {
                        string value = reader[keyName].ToString(); // primary keys should always be simple data types like guids, ints, or strings
                        record.Add(keyName, new Field(value, DBTypes.SDBvarchar, null)); // the DB type shouldn't matter, but varchar should pass through whatever data without error
                    }

                    records.Add(record);
                }
            }

            return records;
        }

        internal static int RecordCount(E_ContentType contentType)
        {
            string sQuery = @"SELECT COUNT(*) AS NumRecords FROM " + TableFields.GetPrimaryKeyTable(contentType);
            using (IDataReader reader = DB.ExecuteReader(sQuery, new SqlParameter[0]))
            {
                if (reader.Read())
                {
                    return (int)reader["NumRecords"];
                }
            }

            throw new Exception("Unable to read record count while running query [" + sQuery + "]");
        }

        internal static DateTime? LatestEmployeeModifiedDate()
        {
            string sQuery =
                @"SELECT 
                    MAX(LastModifiedDate)
                    as LatestEmployeeModifiedDate 
                FROM dbo.EMPLOYEE_2";
            using (IDataReader reader = ExecuteReader(sQuery, new SqlParameter[] { new SqlParameter("@MinDateTime", SqlDateTime.MinValue) }))
            {
                if (reader.Read())
                {
                    if(reader.IsDBNull(reader.GetOrdinal("LatestEmployeeModifiedDate")))
                    {
                        return null;
                    }
                    return (DateTime)reader["LatestEmployeeModifiedDate"];
                }
            }

            throw new Exception("Unable to read Latest Employee Modified Date while running query [" + sQuery + "]");
        }

        /// <summary>
        /// Gets the largest "Version" value among EDocs records in the "EDocs_Audit_History" table of the database.
        /// </summary>
        /// <returns>The largest EDocs Version number in the database, or 0 if no records are found.</returns>
        internal static int LargestEDocsVersionNumber()
        {
            string query =
                @"SELECT
                    CASE
                    WHEN MAX([Version]) IS NULL THEN 0
                    ELSE MAX([Version])
                    END
                    as LargestEDocsVersionNumber
                FROM EDocs_Audit_History";
            using (IDataReader reader = ExecuteReader(query, new SqlParameter[] { }))
            {
                if (reader.Read())
                {
                    return (int)reader["LargestEDocsVersionNumber"];
                }
            }

            throw new Exception("Unable to read Largest EDocs Version Number while running query [" + query + "]");
        }

        private static string GetString(IEnumerable<SqlParameter> parameters)
        {
            var sb = new System.Text.StringBuilder();
            foreach (var param in parameters)
            {
                sb.AppendLine(param.ParameterName + " (" + param.SqlDbType + ") = '" + param.SqlValue + "';");
            }

            return sb.ToString();
        }

        private static IDataReader ExecuteReader(string sqlQuery, IEnumerable<SqlParameter> parameters)
        {
            if (Settings.IsDebug)
            {
                System.IO.File.AppendAllText(Settings.DebugFilePath, sqlQuery + Environment.NewLine + GetString(parameters) + Environment.NewLine);
                return new SqlMockDataReader();
            }

            SqlConnection conn = GetConnection();

            SqlCommand command = new SqlCommand(sqlQuery, conn);

            command.Connection.Open();
            if (null != parameters)
            {
                foreach (SqlParameter p in parameters)
                {
                    command.Parameters.Add(p);
                }
            }
            try
            {
                SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);
                return reader;
            }
            catch
            {
                conn.Close();
                throw;
            }
        }

        private static int ExecuteNonQuery(string sqlQuery, IEnumerable<SqlParameter> parameters)
        {
            if (Settings.IsDebug)
            {
                System.IO.File.AppendAllText(Settings.DebugFilePath, sqlQuery + Environment.NewLine + GetString(parameters) + Environment.NewLine);
                return 0;
            }

            using (SqlConnection conn = GetConnection())
            {
                try
                {
                    SqlCommand command = new SqlCommand(sqlQuery, conn);
                    if (null != parameters)
                    {
                        foreach (SqlParameter p in parameters)
                        {
                            command.Parameters.Add(p);
                        }
                    }
                    conn.Open();
                    return command.ExecuteNonQuery();
                }
                catch (SqlException exc)
                {
                    throw new SqlDebugException(exc, sqlQuery, parameters);
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        private static object ExecuteScalar(string sqlQuery, IEnumerable<SqlParameter> parameters)
        {
            if (Settings.IsDebug)
            {
                System.IO.File.AppendAllText(Settings.DebugFilePath, sqlQuery + Environment.NewLine + GetString(parameters) + Environment.NewLine);
                return 1;
            }

            using (SqlConnection conn = GetConnection())
            {
                try
                {
                    SqlCommand command = new SqlCommand(sqlQuery, conn);
                    if (null != parameters)
                    {
                        foreach (SqlParameter p in parameters)
                        {
                            command.Parameters.Add(p);
                        }
                    }
                    conn.Open();
                    return command.ExecuteScalar();
                }
                catch (SqlException exc)
                {
                    throw new SqlDebugException(exc, sqlQuery, parameters);
                }
                finally
                {
                    conn.Close();
                }
            }
        }
    }

    public class SqlDebugException : Exception
    {
        public SqlDebugException(SqlException sqlException, string sqlQuery, IEnumerable<SqlParameter> sqlParameters)
            : base(string.Empty, sqlException)
        {
            query = sqlQuery;
            parameters = sqlParameters;
        }

        private string query;

        private IEnumerable<SqlParameter> parameters;

        public override string ToString()
        {
            var debug = new System.Text.StringBuilder(base.ToString());
            debug.AppendLine(Environment.NewLine
                + Environment.NewLine
                + "Debug Information:" + Environment.NewLine
                + "SQL Query:" + Environment.NewLine
                + query + Environment.NewLine
                + "Parameters:");
            if (null != parameters)
            {
                foreach (var param in parameters)
                {
                    debug.AppendLine(param.ParameterName + " (Size: " + param.Size + ") = '" + param.SqlValue + "'");
                }
            }

            return debug.ToString();
        }
    }
}
