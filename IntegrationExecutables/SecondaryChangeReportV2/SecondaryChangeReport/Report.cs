using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using CsvHelper;

namespace SecondaryChangeReport
{
    public class Report
    {
        public string[] Header { get; }
        public Loan[]   Loans { get; }
        public Loan     this[string loanNumber]
        {
            get
            {
                Debug.Assert(_loanNumber2IndexDict != null);

                var i = _loanNumber2IndexDict.GetOrDefault(loanNumber, -1);
                return (((i < 0) || (Loans.Length <= i)) ? null : Loans[_loanNumber2IndexDict[loanNumber]]);
            }
        }
        
        public Report(string[] header, IEnumerable<string[]> loans)
        {
            Debug.Assert(header != null);
            Header = header;
            
            _loanNumberIndex = header.IndexOf(SqlField.LoanNumberFieldId);
            _loanStatusIndex = header.IndexOf(SqlField.LoanStatusFieldId);

            Loans = loans.Select(loan => new Loan(loan, _loanNumberIndex, _loanStatusIndex)).ToArray();
            
            _loanNumber2IndexDict = Loans
                .Select((record, i) => new KeyValuePair<string, int>(record.LoanNumber, i))
                .ToDictionary(p => p.Key, p => p.Value);
        }

        public static Report FromCsv(string filename)
        {
            string[] header;
            var rows = new List<string[]>();

            using (var textReader = new StreamReader(filename))
            using (var csv = new CsvParser(textReader))
            {
                header = csv.Read();

                string[] row;
                while ((row = csv.Read()) != null)
                {
                    rows.Add(row);
                }
            }
            return new Report(header, rows);
        }

        readonly int _loanNumberIndex;
        readonly int _loanStatusIndex;
        readonly Dictionary<string, int> _loanNumber2IndexDict;
    }
}
