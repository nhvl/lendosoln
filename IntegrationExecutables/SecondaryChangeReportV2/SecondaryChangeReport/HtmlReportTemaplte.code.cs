using System.Collections.Generic;
using System.IO;

namespace SecondaryChangeReport
{
    partial class HtmlReportTemplate
    {
        public Loan[]                       MissingFromMaster   { get; set; }
        public Loan[]                       MissingFromNewData  { get; set; }
        public string[][][]                 MismatchRecords     { get; set; }
        public string[]                     Headers             { get; set; }
        public IDictionary<string, string>  FieldId2Description { get; set; }
        public string                       sStatusTOptions     { get; set; }

        public static string Get(Loan[] missingFromMaster, Loan[] missingFromNewData, string[][][] mismatchRecords, string[] headers, IDictionary<string, string> fieldId2Description)
        {
            var t = new HtmlReportTemplate
            {
                MissingFromMaster = missingFromMaster,
                MissingFromNewData = missingFromNewData,
                MismatchRecords = mismatchRecords,
                Headers = headers,
            };

            return t.TransformText();
        }

        public static string Write(StreamWriter stream,
            Loan[] missingFromMaster, Loan[] missingFromNewData, string[][][] mismatchRecords,
            string[] headers, IDictionary<string, string> fieldId2Description,
            int[] sStatusTOptions)
        {
            var t = new HtmlReportTemplate
            {
                MissingFromMaster = missingFromMaster,
                MissingFromNewData = missingFromNewData,
                MismatchRecords = mismatchRecords,
                Headers = headers,
                FieldId2Description = fieldId2Description,

                m_streamWriter = stream,
                sStatusTOptions = sStatusTOptions.StringJoin(status => EnumHelper.Pretty("sStatusT", status.ToString()), ", "),
            };

            return t.TransformText();
        }

        string FieldDescription(int fieldIndex)
        {
            return FieldDescription(Headers[fieldIndex]);
        }
        string FieldDescription(string fieldId)
        {
            return FieldId2Description.GetOrDefault(fieldId, fieldId);
        }

        string ValueA(string[][] compareResult, int fieldIndex)
        {
            var fieldId = Headers[fieldIndex];
            return EnumHelper.Pretty(fieldId, compareResult[0][fieldIndex]);
        }
        string ValueB(string[][] compareResult, int fieldIndex)
        {
            var fieldId = Headers[fieldIndex];
            return EnumHelper.Pretty(fieldId, compareResult[1][fieldIndex]);
        }

        string LoanStatus(Loan loan)
        {
            var fieldId = SqlField.LoanStatusFieldId;
            return EnumHelper.Pretty(fieldId, loan.LoanStatus);
        }
        
        StreamWriter m_streamWriter;
        public new void Write(string text)
        {
            if (m_streamWriter != null) m_streamWriter.Write(text);
            else base.Write(text);
        }
    }
}
