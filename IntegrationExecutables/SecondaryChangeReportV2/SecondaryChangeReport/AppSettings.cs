﻿using System;
using System.Configuration;
using System.IO;

namespace SecondaryChangeReport
{
    public static class AppSettings
    {
        public static string AppDir => AppDomain.CurrentDomain.BaseDirectory;

        public static string ResultDir => Path.Combine(AppDir, ConfigurationManager.AppSettings["resultFolder"]);

        public static string ResultFolder(string pmlId)
        {
            return Path.Combine(ResultDir, pmlId+@"\");
        }

        public static string ArchiveFolder(string pmlId)
        {
            return Path.Combine(ResultFolder(pmlId), @"Archive\");
        }
        public static string MasterFile(string pmlId)
        {
            return Path.Combine(ResultFolder(pmlId), ConfigurationManager.AppSettings["master"]); 
        }
        public static string NewDataFile (string pmlId)
        {
            return Path.Combine(ResultFolder(pmlId), ConfigurationManager.AppSettings["newdata"]); 
        }
        public static string OutputFile (string pmlId)
        {
            return Path.Combine(ResultFolder(pmlId), ConfigurationManager.AppSettings["output"]);
        }

        public static string DbConnectionDatabaseName => ConfigurationManager.AppSettings["dbconnection"];

        public static string BrokersCsvFile => Path.Combine(AppDir, ConfigurationManager.AppSettings["BrokersCsvFile"]);

        public static string SqlFieldCsvFile => Path.Combine(AppDir, ConfigurationManager.AppSettings["SqlFieldCsvFile"]);

        public static string LoanProfileFolder => Path.Combine(AppDir, ConfigurationManager.AppSettings["LoanProfileFolder"]);

        public static string EnumDescriptionCsvFile => Path.Combine(AppDir, ConfigurationManager.AppSettings["EnumDescriptionCsvFile"]);

        public static string EmailSMTP     => ConfigurationManager.AppSettings["emailSMTP"];
        public static string EmailUserName => ConfigurationManager.AppSettings["emailUserName"];
        public static string EmailPassword => ConfigurationManager.AppSettings["emailPassword"];

        public static string SupportEmail => ConfigurationManager.AppSettings["supportEmail"];

        public static string ServiceMonitorUrl => ConfigurationManager.AppSettings["ServiceMonitorUrl"];

        public static int MaxLogSize { get; }

        static AppSettings()
        {
            try { MaxLogSize = int.Parse(ConfigurationManager.AppSettings["MaxLogSize"]); }
            catch { MaxLogSize =(5 * 1024 * 1024); }
        }
    }
}
