namespace SecondaryChangeReport.Contract
{
    public interface ILogger
    {
        void Log(string messageType, string message);
        void Log(string messageType, string message, int line);
    }
}