namespace SecondaryChangeReport.Contract
{
    public interface IMailer
    {
        void SendEmail(string subject, string messages);
        void SendEmailWithAttachment(string destinationEmail, string subject, string attachment);
    }
}