using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using CsvHelper;

namespace SecondaryChangeReport
{
    public static class CsvEx
    {
        public static void ExportLoansToFile(IEnumerable<object[]> loans, string filePath, string[] header)
        {
            if (filePath.IsNullOrWhiteSpace()) throw new ArgumentNullException(nameof(filePath));

            Directory.CreateDirectory(Path.GetDirectoryName(filePath));

            using (var csv = new CsvWriter(new StreamWriter(filePath)))
            {
                foreach (var h in header){ csv.WriteField(h); }
                csv.NextRecord();

                foreach (var loan in loans)
                {
                    foreach (var field in loan) csv.WriteField(field);
                    csv.NextRecord();
                }
            }
        }

        public static int Export(IDataReader reader, string file)
        {
            new FileInfo(file).Directory.Create();
            using (var textWriter = File.CreateText(file))
            using (var csv = new CsvWriter(textWriter))
            {
                for (var i = 0; i < reader.FieldCount; i++)
                {
                    csv.WriteField(reader.GetName(i));
                }
                csv.NextRecord();

                int nRecords = 0;
                while (reader.Read())
                {
                    for (var i = 0; i < reader.FieldCount; i++)
                    {
                        csv.WriteField(reader[i]);
                    }
                    csv.NextRecord();
                    ++nRecords;
                }
                return nRecords;
            }
        }
    }
}