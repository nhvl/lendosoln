using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SecondaryChangeReport
{
    public class CompareReport
    {
        readonly string[] _header;
        readonly Loan[] _missingFromMaster;
        readonly Loan[] _missingFromNewData;
        readonly string[][][] _mismatchRecords;

        public CompareReport(Report master, Report newdata, LoanProfile loanProfile)
        {
            _missingFromMaster = newdata.Loans.Where(l => master[l.LoanNumber] == null).ToArray();
            _missingFromNewData = master.Loans.Where(l => newdata[l.LoanNumber] == null).ToArray();

            _header = loanProfile.Fields.Select(f => f.Field).ToArray();
            var masterIndice = GetFieldIndice(loanProfile, master.Header);
            var newdataIndice = GetFieldIndice(loanProfile, newdata.Header);

            if (masterIndice.Contains(-1) || newdataIndice.Contains(-1)) _mismatchRecords = new string[0][][];
            else
            {
                _mismatchRecords = newdata.Loans
                    .Select(newRec => new {newRec, masterRec = master[newRec.LoanNumber]})
                    .Where(p => p.masterRec != null)
                    .Select(p => Compare(
                        GetFields(p.masterRec, masterIndice),
                        GetFields(p.newRec, newdataIndice),
                        true))  // TODO: v1.I skip, v1.II no skip
                    .Where(compareResult => compareResult != null)
                    .ToArray();
            }
        }

        static int[] GetFieldIndice(LoanProfile profile, string[] header)
        {
            var header2Index = header.ToDictionary((h,i) => h, (h,i) => i);
            return profile.Fields.Select(f => header2Index.GetOrDefault(f.Field, -1)).ToArray();
        }

        static string[] GetFields(Loan loan, int[] fIndice)
        {
            return fIndice.Select(i => ((i < loan.Fields.Length) ? loan.Fields[i] : null)).ToArray();
        }

        public void Save(string filePath, IDictionary<string, string> fieldId2Description, int[] sStatusTOptions)
        {
            using (var writer = new StreamWriter(filePath))
            {
                HtmlReportTemplate.Write(writer, _missingFromMaster, _missingFromNewData, _mismatchRecords, _header, fieldId2Description , sStatusTOptions);
            }
        }

        /// <returns>return fields if mismatch, else return null</returns>
        public static string[][] Compare(string[] oldFields, string[] newFields, bool skipIfNewFieldsAdded)
        {
            // If new field added, it won't report as mismatch
            if (skipIfNewFieldsAdded && (oldFields.Length < newFields.Length)) return null;

            int maxLength = Math.Max(oldFields.Length, newFields.Length);
            string[][] ret =
            {
                new string[maxLength],
                new string[maxLength]
            };

            bool mismatchFound = false;
            for (int i = 0; i < maxLength; i++)
            {
                if ((oldFields.Length <= i) || (oldFields[i].IsNullOrEmpty()))
                    ret[0][i] = "<BLANK>";
                else
                    ret[0][i] = oldFields[i];

                if ((newFields.Length <= i) || (newFields[i].IsNullOrEmpty()))
                    ret[1][i] = "<BLANK>";
                else
                    ret[1][i] = newFields[i];

                if ((ret[0][i] != ret[1][i]) && (oldFields.Length > i))
                    mismatchFound = true;
            }

            return mismatchFound ? ret : null;
        }
    }
}
