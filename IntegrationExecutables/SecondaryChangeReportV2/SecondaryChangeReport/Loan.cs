﻿using System.Globalization;

namespace SecondaryChangeReport
{
    public class Loan
    {
        public string[] Fields { get; }
        public readonly int LoanNumberIndex;
        public readonly int LoanStatusIndex;
        public string LoanNumber => Fields[LoanNumberIndex];

        public string LoanStatus => ((LoanStatusIndex == -1) ? string.Empty : Fields[LoanStatusIndex]);

        public Loan(string[] fields, int loanNumberIndex, int loanStatusIndex)
        {
            Fields = fields;
            LoanNumberIndex = loanNumberIndex;
            LoanStatusIndex = loanStatusIndex;

            for (int i = 0; i < Fields.Length; i++)
            {
                // skip loan number. Problem was encountered when we had long loan number (i.e.100521100000000667).
                // It would be converted it to 1.00521E+17 (100521100000000000)
                if (i == LoanNumberIndex) continue;

                int intValue;
                double dblValue;
                if (int.TryParse(Fields[i], out intValue)) Fields[i] = intValue.ToString(CultureInfo.InvariantCulture);
                else if (double.TryParse(Fields[i], out dblValue)) Fields[i] = dblValue.ToString(CultureInfo.InvariantCulture);
            }
        }
    }
}