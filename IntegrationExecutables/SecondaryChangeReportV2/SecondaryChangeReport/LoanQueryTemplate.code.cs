using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace SecondaryChangeReport
{
    partial class LoanQueryTemplate
    {
        public string   DefaultTable    { get; set; }
        SqlField[]      Fields          { get; set; }
        public string[] FromTables      { get; set; }
        public int[]    sStatusTOptions { get; set; }

        public static string Get(string defaultDbTable, Dictionary<string, SqlField> dField2DbName, string[] fields, int[] sStatusTOptions)
        {
            Debug.Assert(!sStatusTOptions.IsNullOrEmpty());

            var fromTables = fields
                .Select(f => dField2DbName[f].DbTable)
                .Where( t =>
                    !t.IsNullOrWhiteSpace() &&
                    !string.Equals(t, defaultDbTable, StringComparison.InvariantCultureIgnoreCase))
                .Distinct()
                .ToArray();

            var lqt = new LoanQueryTemplate
            {
                DefaultTable    = defaultDbTable,
                Fields          = fields.Select(f => dField2DbName[f]).ToArray(),
                FromTables      = fromTables,
                sStatusTOptions = sStatusTOptions,
            };
            return lqt.TransformText();
        }
    }
}
