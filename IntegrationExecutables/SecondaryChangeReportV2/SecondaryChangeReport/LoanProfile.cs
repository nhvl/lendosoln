using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CsvHelper;
using CsvHelper.Configuration;

namespace SecondaryChangeReport
{
    public class LoanProfile
    {
        public string                 Name            { get; set; }
        public LoanFieldDescription[] Fields          { get; set; }
        public string                 SqlQuery        { get; }

        #region ctor
        LoanProfile(string name, LoanFieldDescription[] fields)
            : this(name, fields, Broker.Default_sStatusTOptions)
        {}

        LoanProfile(string name, LoanFieldDescription[] fields, int[] sStatusTOptions)
        {
            Name = name;
            Fields = fields;

            var fs = fields.Select(f => f.Field)
                .Concat(new[]{ SqlField.LoanNumberFieldId, SqlField.LoanStatusFieldId })
                .Distinct()
                .ToArray();
            SqlQuery = SqlField.GenerateQuery(fs, sStatusTOptions);
        }

        LoanProfile(LoanProfile p, int[] sStatusTOptions)
            :this(p.Name, p.Fields, sStatusTOptions)
        {}


        LoanProfile()
        {
            throw new NotImplementedException();
        }

        static LoanProfile FromCsv(string csvFile)
        {
            LoanFieldDescription[] loanFieldDescriptions;
            try
            {
                loanFieldDescriptions = LoanFieldDescription.FromCsv(csvFile);
            }
            catch (CsvHelper.CsvMissingFieldException e)
            {
                throw new FormatException($"{e.Message} ({csvFile})", e);
            }
            catch (CsvHelperException e)
            {
                throw new FormatException($"{e.Message} ({csvFile})", e);
            }
            catch (Exception e)
            {
                throw new FormatException($"{e.Message} ({csvFile})", e);
            }

            try { return new LoanProfile(Path.GetFileNameWithoutExtension(csvFile), loanFieldDescriptions); }
            catch (Exception e)
            {
                throw new FormatException($"{e.Message} ({csvFile})", e);
            }
        }
        #endregion

        /// <summary>
        /// Broker -> LoanProfile
        /// </summary>
        public static LoanProfile For(Broker broker)
        {
            var p = ForPml(broker.PmlId);
            return broker.sStatusTOptions.IsNullOrEmpty() ||
                   broker.sStatusTOptions == Broker.Default_sStatusTOptions
                ? p
                : new LoanProfile(p, broker.sStatusTOptions);
        }

        /// <summary>
        /// (PmlId) -> LoanProfile
        /// </summary>
        /// <param name="pmlId">PML ID</param>
        /// <returns>Loan profile for that PML ID</returns>
        public static LoanProfile ForPml(string pmlId)
        {
            return (PmlId2Profiles.ContainsKey(pmlId) ? PmlId2Profiles[pmlId]:
                    PmlId2Profiles.GetOrDefault("Default"));
        }

        // (PmlId) -> LoanProfile
        private static Dictionary<string, LoanProfile> _pmlId2Profiles;
        public static Dictionary<string, LoanProfile> PmlId2Profiles
        {
            get
            {
                return _pmlId2Profiles ?? (_pmlId2Profiles =
                           Directory.GetFiles(AppSettings.LoanProfileFolder, "*.csv")
                               .Select(FromCsv)
                               .ToDictionary(p => p.Name, p => p));
            }
        }
    }

    public class LoanFieldDescription
    {
        public string Field { get; set; }
        public string Description { get; set; }

        public static LoanFieldDescription[] FromCsv(string csvFile)
        {
            using (var textReader = new StreamReader(csvFile))
            {
                using (var csv = new CsvReader(textReader))
                {
                    return csv.GetRecords<LoanFieldDescription>().ToArray();
                }
            }
        }
    }

    public class SqlField
    {
        public const string DefaultDbTable    = "LOAN_FILE_CACHE";
        public const string LoanNumberFieldId = "sLNm";
        public const string LoanStatusFieldId = "sStatusT";
        
        public string Type            { get; set; }
        public string FieldId         { get; set; }
        public string DbTable         { get; set; }
        public string DbColumnId      { get; set; }

        public SqlField()
        {
            // for Deserializtion
        }

        public static SqlField[] FromCsv(string csvFile)
        {
            using (var textReader = new StreamReader(csvFile))
            {
                using (var csv = new CsvReader(textReader))
                {
                    //csv.Configuration.RegisterClassMap<SqlFieldCsvMap>();

                    var fs = csv.GetRecords<SqlField>().ToArray();
                    
                    // set default values
                    foreach (var field in fs)
                    {
                        if (field.DbTable.IsNullOrWhiteSpace()) field.DbTable = DefaultDbTable;
                        if (field.DbColumnId.IsNullOrWhiteSpace()) field.DbColumnId = field.FieldId;
                    }

                    return fs;
                }
            }
        }

        /// <summary>
        /// Sql query with @brokerId parameter: "SELECT ... FROM ... WHERE @brokerId"
        /// </summary>
        public static string GenerateQuery(string[] fields, int[] sStatusTOptions)
        {
            if (!fields.All(FieldId2FieldDict.ContainsKey)) throw new ApplicationException("unknown field(s)");

            return LoanQueryTemplate.Get(DefaultDbTable, FieldId2FieldDict, fields, sStatusTOptions);
        }

        /// <summary>
        /// (fieldId) -> SqlField. eg: "sStatusT" -> ("sStatusT", "LOAN_FILE_CACHE")
        /// </summary>
        public static readonly Dictionary<string, SqlField> FieldId2FieldDict;

        static SqlField()
        {
            FieldId2FieldDict = FromCsv(AppSettings.SqlFieldCsvFile).ToDictionary(f => f.FieldId, f => f);
        }
    }

    public class SqlFieldCsvMap : CsvClassMap<SqlField>
    {
        public SqlFieldCsvMap()
        {
            Map(m => m.FieldId          ).Name("FieldId"        );
            Map(m => m.Type             ).Name("Type"           );
            Map(m => m.DbTable          ).Name("DbTable"        );
            Map(m => m.DbColumnId       ).Name("DbColumnId"     );
        }
    }
}
