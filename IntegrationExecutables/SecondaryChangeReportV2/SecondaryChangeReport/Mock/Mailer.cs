﻿using System;
using System.Diagnostics;
using SecondaryChangeReport.Contract;

namespace SecondaryChangeReport.Mock
{
    public class Mailer : IMailer
    {
        public void SendEmail(string subject, string messages)
        {
            Trace.TraceInformation("Email: [{1}]{0}    {2}", Environment.NewLine, subject, messages);
        }

        public void SendEmailWithAttachment(string destinationEmail, string subject, string attachment)
        {
            Trace.TraceInformation("Email: [{1}] to [{2}]{0}    {2}{0}    {3}", Environment.NewLine, subject, destinationEmail, attachment);
        }
    }
}