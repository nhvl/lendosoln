using System;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using SecondaryChangeReport.Contract;

namespace SecondaryChangeReport
{
    public class Processor
    {
        public ILogger Logger { get; set; }
        public IMailer Mailer { get; set; }

        public Processor(ILogger logger, IMailer mailer)
        {
            Logger = logger;
            Mailer = mailer;
        }

        public void Process()
        {
            Processor.Process(this.Logger, this.Mailer, AppSettings.BrokersCsvFile);
        }

        public string Verify(string brokersCsvFile = null)
        {
            return Processor.Verify(this.Logger, this.Mailer, string.IsNullOrEmpty(brokersCsvFile) ? AppSettings.BrokersCsvFile : brokersCsvFile);
        }

        static string Verify(ILogger logger, IMailer mailer, string brokersCsvFile)
        {
            Broker[] brokers;
            try { brokers = Broker.FromCsv(brokersCsvFile); }
            catch (Exception e) { return $"Load BrokerCsvFile fail: {e.Message} ({Path.GetFullPath(brokersCsvFile)})"; }

            //try { Broker.LoadBrokerId(brokers); }
            //catch (Exception e) { return e.Message; }

            try { Broker.LoadQuery(brokers); }
            catch (Exception e) { return e.ToString(); }
            Broker.LoadQuery(brokers);

            var brokerErrors = brokers.Select(broker =>
                {
                    var error = broker.IsValid();
                    if (string.IsNullOrEmpty(error)) return null;

                    // Skip this since we did not load BrokerId from DB
                    if (error == Broker.Error_NoBroker) return null; // return $"ERROR when load broker [{broker.PmlId}]: Can't find broker id.";

                    return $"ERROR when load broker [{broker.PmlId}]: {error}";
                }).Where(s => s != null)
                .ToArray();
            if (brokerErrors.Length > 0) return string.Join(Environment.NewLine, brokerErrors);

            return null;
        }

        static void Process(ILogger logger, IMailer mailer, string brokersCsvFile)
        {
            var brokers = Broker.FromCsv(brokersCsvFile);
            Broker.LoadBrokerId(brokers);
            Broker.LoadQuery(brokers);

            foreach (var broker in brokers)
            {
                if (broker.IsSkip())
                {
                    logger.Log("INFO", $"SKIP Broker({broker.PmlId}): Not in Run hours", 1);
                    continue;
                }

                var error = broker.IsValid();
                if (!string.IsNullOrEmpty(error))
                {
                    logger.Log("ERROR", $"SKIP Broker({broker.PmlId}): {error}", 5);
                    if (error == Broker.Error_NoBroker)
                    {
                        mailer.SendEmail("Secondary Report can't find broker id for: " + broker.PmlId, null);
                    }
                    continue;
                }

                try { Process(broker, logger, mailer); }
                catch (Exception ex)
                {
                    mailer.SendEmail(
                        $"Secondary Report Error for [{broker.PmlId}]",
                        $"Exception:\r\n{ex}");
                }
            }
        }

        static void Process(Broker broker, ILogger logger, IMailer mailer)
        {
            var pmlId = broker.PmlId;

            ArchiveResult(pmlId);

            #region Query from DB and save to CSV
            {
                logger.Log("INFO", "Secondary Report is getting loan files from database for " + pmlId, 1);
                var csvFile = CsvFileToWrite(pmlId);
                int n;

                try { n = broker.LoadLoanToCsv(csvFile); }
                catch(SqlException e) { throw new ApplicationException("Failed to load loans", e); }
                catch(Exception e) { throw new ApplicationException($"Failed to load loans and save file [{csvFile}]", e); }

                if (n < 1)
                {
                    mailer.SendEmail($"Secondary Change Report Error for: {pmlId}", "No data from query that can be used for comparison.");
                    // No loan files in the database
                    return;
                }
            }
            #endregion

            #region Compare and report
            var report = GetCompareReport(broker, logger, mailer);
            if (report == null) return;
            
            var outputFile = AppSettings.OutputFile(pmlId);
            report.Save(outputFile, broker.LoanProfile.Fields.ToDictionary(f => f.Field, f => f.Description), broker.sStatusTOptions);

            logger.Log("INFO", $"Secondary Report just saved result and going to email for {pmlId}", 1);
            mailer.SendEmailWithAttachment(broker.BrokerEmail, "Secondary Report", outputFile);
            #endregion
        }

        static string CsvFileToWrite(string pmlId)
        {
            var pmlFolder = AppSettings.ResultFolder(pmlId);
            var masterCsvFile = AppSettings.MasterFile(pmlId);
            var newdataFile = AppSettings.NewDataFile(pmlId);

            Directory.CreateDirectory(pmlFolder);

            if ((!File.Exists(masterCsvFile)) && (!File.Exists(newdataFile)))
            {
                return masterCsvFile;
            }

            if (File.Exists(masterCsvFile) && File.Exists(newdataFile)) File.Delete(masterCsvFile);
            if (File.Exists(newdataFile))
            {
                File.Copy(newdataFile, masterCsvFile);
                File.Delete(newdataFile);
            }

            return newdataFile;
        }

        static CompareReport GetCompareReport(Broker broker, ILogger logger, IMailer mailer)
        {
            string pmlId = broker.PmlId;

            var masterFile = AppSettings.MasterFile(pmlId);
            var newDataFile = AppSettings.NewDataFile(pmlId);

            logger.Log("INFO", "Secondary Report is checking to see if master file or newdata file exists or not for " + pmlId, 1);
            if (!File.Exists(masterFile))
            {
                mailer.SendEmail("Secondary Report Error for " + pmlId, "Master file is missing");
                return null;
            }
            if (!File.Exists(newDataFile))
            {
                mailer.SendEmail("Secondary Report Error for " + pmlId, "New data file is missing");
                return null;
            }

            Report master;
            {
                logger.Log("INFO", $"Secondary Report is going to parse master file for PML=[{pmlId}]", 1);
                try { master = Report.FromCsv(masterFile); }
                catch (Exception e)
                {
                    logger.Log("ERROR",
                        $"Secondary Report: Parsed master file FAIL. PML=[{pmlId}], File=[{Path.GetFullPath(masterFile)}]. {e}.", 1);
                    return null;
                }
            }

            Report newdata;
            {
                logger.Log("INFO", $"Secondary Report is going to parse newdata file for PML=[{pmlId}]", 1);
                try { newdata = Report.FromCsv(newDataFile); }
                catch (Exception e)
                {
                    logger.Log("ERROR",
                        $"Secondary Report: Parsed newdata file FAIL. PML=[{pmlId}], File=[{Path.GetFullPath(masterFile)}]. {e}.", 1);
                    return null;
                }
            }

            return new CompareReport(master, newdata, broker.LoanProfile);
        }

        /// <summary>
        /// Copy files from result folder to archive folder <para/>
        /// Also Remove files from Archive that older than 2 month
        /// </summary>
        static void ArchiveResult(string pmlId)
        {
            var archiveFolder = AppSettings.ArchiveFolder(pmlId);
            var resultFolder = AppSettings.ResultFolder(pmlId);

            //if (!Directory.Exists(archiveFolder))
            Directory.CreateDirectory(archiveFolder);

            //Delete the files in Archive folder if they're older than 2 months
            DeleteFilesHasNotAccessedFrom(archiveFolder, DateTime.Now.AddMonths(-2));

            var srcFiles = Directory.GetFiles(resultFolder);
            foreach (var file in srcFiles)
            {
                string destfile = Path.GetFileNameWithoutExtension(file);
                destfile = $@"{destfile}_{DateTime.Now:MM_dd_yyyy_HH}{Path.GetExtension(file)}";

                string destPath = Path.Combine(archiveFolder, destfile);
                File.Copy(file, destPath, true);
            }
        }

        static void DeleteFilesHasNotAccessedFrom(string directory, DateTime pivot)
        {
            var oldFiles = Directory.GetFiles(directory).Where(file => File.GetLastAccessTime(file) < pivot);
            foreach (var file in oldFiles) File.Delete(file);
        }
    }
}
