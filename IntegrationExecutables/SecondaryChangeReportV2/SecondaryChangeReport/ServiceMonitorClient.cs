﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using SecondaryChangeReport.Contract;

namespace SecondaryChangeReport
{
    enum ServiceMonitorStatus
    {
        OK = 0,
        FAILED = 1,
        STARTED = 2
    }

    static class ServiceMonitorClient
    {
        private static JavaScriptSerializer serializer = new JavaScriptSerializer();
        public static ILogger Logger { private get; set; }

        public static void TryPostStatus(ServiceMonitorStatus status, string log)
        {
            try
            {
                PostStatus(status, log);
            }
            catch (Exception e)
            {
                if (Logger == null)
                {
                    System.Diagnostics.Trace.TraceError(e.ToString());
                }
                else
                {
                    Logger.Log("ERROR", e.ToString());
                }
            }
        }

        private static void PostStatus(ServiceMonitorStatus status, string log)
        {
            var message = new
            {
                StatusType = status,
                ServiceName = "SecondaryChangeReport - " + AppSettings.DbConnectionDatabaseName,
                LogData = log
            };

            using (WebClient wc = new WebClient())
            {
                wc.Headers.Add(HttpRequestHeader.ContentType, "application/json");
                wc.UploadString(new Uri(AppSettings.ServiceMonitorUrl), serializer.Serialize(message));
            }
        }
    }
}