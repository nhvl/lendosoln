﻿using System;
using System.Data.SqlClient;

namespace SecondaryChangeReport
{
    public static class DbHelper
    {
        static DbHelper()
        {
            Adapter.Drivers.ConnectionStringProviderHelper.Register("SecondaryChangeReport");
        }

        public static T Query<T>(string queryString, Func<SqlDataReader, T> tr)
        {
            using (var command = new SqlCommand(queryString))
            {
                return Query(command, tr);
            }
        }

        public static T Query<T>(SqlCommand command, Func<SqlDataReader, T> tr)
        {
            string connectionString = Adapter.Drivers.ConnectionStringProviderHelper.GetMainConnectionStringByDatabaseName(AppSettings.DbConnectionDatabaseName, needWriteAccess: false);
            T ret = default(T);

            using (var connection = new SqlConnection(connectionString))
            {
                command.Connection = connection;
                connection.Open();

                using (var reader = command.ExecuteReader())
                {
                    ret = tr(reader);    
                }

                connection.Close();
            }

            return ret;
        }

        public static object GetField(this SqlDataReader reader, LoanFieldDescription f)
        {
            return reader[f.Field];
        }

        public static int SafeInt(object v)
        {
            if (null == v || Convert.IsDBNull(v))
            {
                return 0;
            }
            return Convert.ToInt32(v);
        }

        public static double SafeDouble(object v)
        {
            if (null == v || Convert.IsDBNull(v))
            {
                return 0;
            }
            return Convert.ToDouble(v);
        }

        public static string SafeString(object v)
        {
            if (null == v || Convert.IsDBNull(v))
            {
                return string.Empty;
            }
            return Convert.ToString(v);
        }
    }
}