﻿using System;
using SecondaryChangeReport.Contract;

namespace SecondaryChangeReport.Provider
{
    public class ConsoleLogger : ILogger
    {
        public ConsoleLogger()
        {

        }

    	public void Log(string messageType, string message)
        {
        	Console.WriteLine($"{messageType}: {message}");
        }

        public void Log(string messageType, string message, int line)
        {
            Console.WriteLine($"{messageType}: {message}");
        }
    }
}