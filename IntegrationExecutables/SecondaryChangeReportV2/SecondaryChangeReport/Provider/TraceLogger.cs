﻿using System.Diagnostics;
using System.IO;
using SecondaryChangeReport.Contract;

namespace SecondaryChangeReport.Provider
{
    public class TraceLogger : ILogger
    {
        public TraceLogger()
        {
            const string logFilePath = @".\SecondarayChangeReport.log";
            try
            {
                var logFile = new FileInfo(logFilePath);

                if (logFile.Exists && logFile.Length > AppSettings.MaxLogSize)
                {
                    logFile.Delete();
                    File.WriteAllText(logFilePath, string.Empty);
                }
            }
            catch { }
        }

    	public void Log(string messageType, string message)
        {
        	this.Log(messageType, message, 0);
        }

        public void Log(string messageType, string message, int line)
        {
            switch (messageType)
            {
                case "INFO": 
                    Trace.TraceInformation("@line({1}): {0}", message, line);
                    break;
                case "WARN":
                    Trace.TraceWarning("@line({1}): {0}", message, line);
                    break;
                case "ERROR":
                    Trace.TraceError("@line({1}): {0}", message, line);
                    break;
                default:
                    Trace.TraceInformation("[{0}]@line({2}): {1}", messageType, message, line);
                    break;
            }
        }
    }
}