﻿using System;
using System.IO;
using System.Net;
using System.Net.Mail;
using SecondaryChangeReport.Contract;

namespace SecondaryChangeReport.Provider
{
    public class Mailer : IMailer
    {
        public ILogger Logger { get; set; }

        public Mailer(ILogger logger)
        {
            Logger = logger;
        }

        SmtpClient GetSmtpClient()
        {
            var host = AppSettings.EmailSMTP;

            if (host == "smtp.gmail.com")
            {
                return new SmtpClient(host, 587)
                {
                    UseDefaultCredentials = true,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    Credentials = new NetworkCredential(AppSettings.EmailUserName, AppSettings.EmailPassword),
                    Timeout = 20000,
                };
            }

            return new SmtpClient(host);
        }

        public void SendEmail(string subject, string messages)
        {
            try
            {
                var smtp = GetSmtpClient();
                smtp.Send(AppSettings.SupportEmail, AppSettings.SupportEmail, subject, messages);
            }
            catch (Exception e)
            {
                Logger.Log("ERROR", "Exception caught in SendEmail(): "+ e.Message, 5);
            }
        }

        public void SendEmailWithAttachment(string destinationEmail, string subject, string attachment)
        {
            using (var message = new MailMessage(AppSettings.SupportEmail, destinationEmail, subject, string.Empty))
            {
                if (new FileInfo(attachment).Length < 600) message.Body = "No changes";
                else
                {
                    message.Body = "See attached file";
                    message.Attachments.Add(new Attachment(attachment));
                }

                var smtp = GetSmtpClient();
                try { smtp.Send(message); }
                catch (Exception e)
                {
                    SendEmail("Exception caught in SendEmailWithAttachment() for destination email: " + destinationEmail, e.ToString());
                }
            }
        }
    }
}