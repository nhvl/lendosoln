﻿using SecondaryChangeReport.Contract;

namespace SecondaryChangeReport.Provider
{
    public class PBLogger : ILogger
    {
        #if !MockPB
        static readonly PaulBunyan.PBLogger Logger;

        static PBLogger()
        {
            Logger = new PaulBunyan.PBLogger();
        }
        #endif

        public void Log(string messageType, string message)
        {
            #if !MockPB
            Logger.Log(messageType, message);
            #endif
        }

        public void Log(string messageType, string message, int line)
        {
            #if !MockPB
            Logger.Log(messageType, message, line);
            #endif
        }
    }
}