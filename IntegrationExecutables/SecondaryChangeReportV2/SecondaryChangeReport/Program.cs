﻿using System;
using System.Diagnostics;
using SecondaryChangeReport.Contract;
using SecondaryChangeReport.Provider;

namespace SecondaryChangeReport
{
    static class Program
    {
        static void Main(string[] args)
        {
            ServiceMonitorClient.TryPostStatus(ServiceMonitorStatus.STARTED, string.Join(" ", args));
            ILogger logger;
            try { 
                logger = 
                    #if MockPB
                        new TraceLogger()
                    #else
                        new PBLogger()
                    #endif
                ; 
            }
            catch (Exception e) {
                Trace.TraceError("PaulBunyan Logger initalize error: {0}", e);
                ServiceMonitorClient.TryPostStatus(ServiceMonitorStatus.FAILED, e.ToString());
                return;
            }

            ServiceMonitorClient.Logger = logger;

            IMailer mailer = 
                #if MockEmail
                    new Mock.Mailer();
                #else
                    new Provider.Mailer(logger);
                #endif

            try
            {
                var p = new Processor(logger, mailer);
                if ((args.Length > 0) && args[0].Equals("verify", StringComparison.OrdinalIgnoreCase))
                {
                    var error = p.Verify(args.Length > 1 ? args[1] : null);
                    if (string.IsNullOrEmpty(error))
                    {
                        ServiceMonitorClient.TryPostStatus(ServiceMonitorStatus.OK, "");
                        Environment.Exit(0);
                    }
                    else
                    {
                        ServiceMonitorClient.TryPostStatus(ServiceMonitorStatus.FAILED, error);
                        Console.WriteLine(error);
                        Environment.Exit(1);
                    }
                }
                else
                {
                    p.Process();
                }

                ServiceMonitorClient.TryPostStatus(ServiceMonitorStatus.OK, "");
            }
            catch (Exception e)
            {
                string unhandleErrorMessage = $"Unhandle error: {e}";
                ServiceMonitorClient.TryPostStatus(ServiceMonitorStatus.FAILED, unhandleErrorMessage);
                logger.Log("ERROR", unhandleErrorMessage, 0);
                mailer.SendEmail("SecondaryChangeReport Error", unhandleErrorMessage);
            }
        }
    }
}
