﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;

namespace SecondaryChangeReport
{
    public static class HelperEx
    {
        public static bool IsNullOrWhiteSpace(this string s)
        {
            return s == null || s.All(char.IsWhiteSpace);
        }

        public static bool IsNullOrEmpty<T>(this T[] xs)
        {
            return ((xs == null) || (xs.Length < 1));
        }

        public static bool IsNullOrEmpty<T>(this IEnumerable<T> xs)
        {
            return ((xs == null) || !xs.Any());
        }

        public static int IndexOf<T>(this T[] xs, T x)
        {
            return Array.IndexOf(xs, x);
        }

        public static string StringJoin(this string[] xs, string seperator)
        {
            return string.Join(seperator, xs);
        }

        public static string StringJoin(this IEnumerable<string> values, string separator)
        {
            if (values == null) throw new ArgumentNullException(nameof(values));

            if (separator == null) separator = string.Empty;

            using (var en = values.GetEnumerator())
            {
                if (!en.MoveNext()) return string.Empty;

                var result = new StringBuilder();
                if (en.Current != null) result.Append(en.Current);

                while (en.MoveNext())
                {
                    result.Append(separator);
                    if (en.Current != null) result.Append(en.Current);
                }
                return result.ToString();
            }           
        }

        public static string StringJoin<T>(this T[] xs, string seperator)
        {
            return xs.Select(x => x.ToString()).StringJoin(seperator);
        }

        public static string StringJoin<T>(this T[] xs, Func<T, string> map, string seperator)
        {
            return xs.Select(map).StringJoin(seperator);
        }

        public static Dictionary<TKey, TSource> ToDictionary<TSource, TKey>(this TSource[] source, Func<TSource, int, TKey> keySelector)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            if (keySelector == null) throw new ArgumentNullException(nameof(keySelector));

            var dictionary = new Dictionary<TKey, TSource>();

            for (int i = 0; i < source.Length; i++)
            {
                TSource source1 = source[i];
                dictionary.Add(keySelector(source1, i), source1);
            }

            return dictionary;
        }

        public static Dictionary<TKey, TElement> ToDictionary<TSource, TKey, TElement>(this TSource[] source, Func<TSource, int, TKey> keySelector, Func<TSource, int, TElement> elementSelector)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            if (keySelector == null) throw new ArgumentNullException(nameof(keySelector));
            if (elementSelector == null) throw new ArgumentNullException(nameof(elementSelector));

            var dictionary = new Dictionary<TKey, TElement>();

            for (int i = 0; i < source.Length; i++)
            {
                TSource source1 = source[i];
                dictionary.Add(keySelector(source1, i), elementSelector(source1, i));
            }

            return dictionary;
        }

        public static TS GetOrDefault<TK, TS>(this IDictionary<TK, TS> dict, TK key)
        {
            return (dict.ContainsKey(key) ? dict[key] : default(TS));
        }
        public static TS GetOrDefault<TK, TS>(this IDictionary<TK, TS> dict, TK key, TS @default)
        {
            return (dict.ContainsKey(key) ? dict[key] : @default);
        }
    }

    public static class EmailEx
    {
        public static bool IsValidateEmails(string emails)
        {
            if (emails.IsNullOrWhiteSpace()) return false;

            try { using (new MailMessage("support@lendingqb.com", emails)) { } }
            catch { return false; }

            return true;
        }
    }
}