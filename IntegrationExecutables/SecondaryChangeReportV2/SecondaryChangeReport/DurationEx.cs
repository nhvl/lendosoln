﻿using System;
using System.Text.RegularExpressions;

namespace SecondaryChangeReport
{
    public static class DurationEx
    {
        public static DateTime Since(string s)
        {
            var mY = new Regex("\\b(\\d+)Y\\b").Match(s);
            var mM = new Regex("\\b(\\d+)M\\b").Match(s);
            var mD = new Regex("\\b(\\d+)D\\b").Match(s);

            var y = mY.Success ? int.Parse(mY.Groups[1].Value) : 0;
            var m = mM.Success ? int.Parse(mM.Groups[1].Value) : 0;
            var d = mD.Success ? int.Parse(mD.Groups[1].Value) : 0;

            var now = DateTime.Now.Date;
            
            return now.AddYears(-y).AddMonths(-m).AddDays(-d);
        }

    }
}
