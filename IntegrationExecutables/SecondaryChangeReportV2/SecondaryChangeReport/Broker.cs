using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using CsvHelper;
using CsvHelper.Configuration;
using CsvHelper.TypeConversion;

namespace SecondaryChangeReport
{
    public class Broker
    {
        public string      PmlId           { get; set; }
        public string      BrokerEmail     { get; set; }
        public int[]       RunHours        { get; set; }
        public int[]       sStatusTOptions { get; set; }
        public string      sRLcKdDMin      { get; set; }
        public string      sRLcKdDFrom     { get; set; }
        //-------------------------------------------
        public string      BrokerId        { get; set; }
        public LoanProfile LoanProfile     { get; set; }

        internal static readonly int[] Default_sStatusTOptions =
            (new[]
                {
                    E_sStatusT.Loan_Open               , // 0,
                    E_sStatusT.Loan_Prequal            , // 1,
                    E_sStatusT.Loan_Registered         , // 3,
                    E_sStatusT.Loan_PreProcessing      , // 29,
                    E_sStatusT.Loan_Processing         , // 22,
                    E_sStatusT.Loan_DocumentCheck      , // 30,
                    E_sStatusT.Loan_DocumentCheckFailed, // 31,
                    E_sStatusT.Loan_Preapproval        , // 2,
                    E_sStatusT.Loan_LoanSubmitted      , // 28,
                    E_sStatusT.Loan_PreUnderwriting    , // 32,
                    E_sStatusT.Loan_Underwriting       , // 13,
                    E_sStatusT.Loan_Approved           , // 4,
                    E_sStatusT.Loan_ConditionReview    , // 33,
                    E_sStatusT.Loan_FinalUnderwriting  , // 23,
                    E_sStatusT.Loan_PreDocQC           , // 34,
                    E_sStatusT.Loan_ClearToClose       , // 21,
                    E_sStatusT.Loan_DocsOrdered        , // 35,
                    E_sStatusT.Loan_DocsDrawn          , // 36,
                    E_sStatusT.Loan_Docs               , // 5,
                    E_sStatusT.Loan_DocsBack           , // 24,
                    E_sStatusT.Loan_FundingConditions  , // 25,
                    E_sStatusT.Loan_Funded             , // 6,
                    E_sStatusT.Loan_Recorded           , // 19,
                    E_sStatusT.Loan_FinalDocs          , // 26,
                    E_sStatusT.Loan_Closed             , // 11,
                    E_sStatusT.Loan_Suspended          , // 8,
                    E_sStatusT.Loan_Canceled           , // 9,
                    E_sStatusT.Loan_Rejected           , // 10,
                    E_sStatusT.Loan_CounterOffer       , // 47,
                    E_sStatusT.Loan_Withdrawn          , // 48,
                    E_sStatusT.Loan_OnHold              , // 7,
                })
            .Distinct().OrderBy(s => s)
            .Cast<int>()
            .ToArray()
            ;
        internal const string Default_sRLcKdDMin = "1/1/2016";

        #region ctor
        public Broker()
        {
            // for Deserialization
        }

        public static Broker[] FromCsv(string csvFile)
        {
            using (var textReader = File.OpenText(csvFile))
            using (var csv = new CsvReader(textReader))
            {
                csv.Configuration.RegisterClassMap<BrokerCsvMap>();
                var bs = csv.GetRecords<Broker>().ToArray();
                foreach (var b in bs)
                {
                    b.BrokerEmail     = b.BrokerEmail.Replace(';', ',');
                    b.RunHours        = b.RunHours.IsNullOrEmpty() ? b.RunHours :
                                        b.RunHours.Distinct().OrderBy(s => s).ToArray();
                    b.sStatusTOptions = b.sStatusTOptions.IsNullOrEmpty() ? Default_sStatusTOptions :
                                        b.sStatusTOptions.Distinct().OrderBy(s => s).ToArray();
                    b.sRLcKdDMin      = b.sRLcKdDFrom.IsNullOrWhiteSpace() && b.sRLcKdDMin.IsNullOrWhiteSpace() ? Default_sRLcKdDMin : b.sRLcKdDMin;
                }
                return bs;
            }
        }
        #endregion

        #region methods
        /// <returns>Error string or `null`</returns>
        public string IsValid()
        {
            if (this.PmlId.IsNullOrWhiteSpace())
            {
                return "No PML Id";
            }
            if (!EmailEx.IsValidateEmails(this.BrokerEmail))
            {
                return $"Invalid Email ({this.BrokerEmail}).";
            }
            // ------------
            if (this.LoanProfile == null || this.LoanProfile.SqlQuery.IsNullOrWhiteSpace())
            {
                return "No Loan Query Profile";
            }
            if (this.BrokerId.IsNullOrWhiteSpace())
            {
                return Error_NoBroker;
            }
            return null;
        }

        public bool IsSkip(){ return IsSkip(DateTime.Now); }
        public bool IsSkip(DateTime date)
        {
            if (RunHours.IsNullOrEmpty()) return false;
            var currentHour = date.Hour;
            return !(RunHours.Contains(currentHour));
        }

        /// <returns>Loan[]</returns>
        [Obsolete]
        public object[][] LoadLoan()
        {
            Debug.Assert(this.LoanProfile != null);
            Debug.Assert(!LoanProfile.SqlQuery.IsNullOrWhiteSpace());

            using (var command = new SqlCommand(LoanProfile.SqlQuery) { CommandTimeout = 150 })
            {
                command.Parameters.Add(new SqlParameter("brokerId", BrokerId));
                command.Parameters.Add(sRLcKdDFrom.IsNullOrWhiteSpace()
                    ? new SqlParameter("sRLckdDMin", sRLcKdDMin)
                    : new SqlParameter("sRLckdDMin", System.Data.SqlDbType.DateTime) { Value = DurationEx.Since(sRLcKdDFrom) }
                );
                
                var ls = DbHelper.Query(command, reader =>
                {
                    var loans = new List<object[]>();

                    while (reader.Read())
                    {
                        loans.Add(this.LoanProfile.Fields.Select(reader.GetField).ToArray());
                    }
                    return loans;
                });

                return ls.ToArray();
            }
        }

        /// <returns>Number of records</returns>
        public int LoadLoanToCsv(string file)
        {
            Debug.Assert(this.LoanProfile != null);
            Debug.Assert(!LoanProfile.SqlQuery.IsNullOrWhiteSpace());

            using (var command = new SqlCommand(LoanProfile.SqlQuery) { CommandTimeout = 150 })
            {
                command.Parameters.Add(new SqlParameter("brokerId", BrokerId));
                command.Parameters.Add(sRLcKdDFrom.IsNullOrWhiteSpace()
                    ? new SqlParameter("sRLckdDMin", sRLcKdDMin)
                    : new SqlParameter("sRLckdDMin", System.Data.SqlDbType.DateTime) { Value = DurationEx.Since(sRLcKdDFrom) }
                );
                return DbHelper.Query(command, reader => CsvEx.Export(reader, file));
            }
        }

        public string[] LoanCsvHeader()
        {
            return this.LoanProfile.Fields.Select(f => f.Field).ToArray();
        }
        #endregion

        public static void LoadBrokerId(Broker[] brokers)
        {
            Debug.Assert(brokers.All(b => !b.PmlId.IsNullOrWhiteSpace()));

            SqlCommand command;
            {
                var query =
                    $@"SELECT [CustomerCode], [BrokerId] FROM [Broker] WHERE {
                        brokers.Select((b, i) => $"([CustomerCode] = @cc{i})").StringJoin(" OR ")
                    }";
                var paramaters =
                    brokers.Select((b, i) => new SqlParameter($"cc{i}", b.PmlId)).ToArray();

                command = new SqlCommand(query);
                command.Parameters.AddRange(paramaters);
            }

            Dictionary<string, string> pmlId2BrokerIdDict;
            using (command)
            {
                pmlId2BrokerIdDict = DbHelper.Query(command, reader =>
                {
                    var dict = new Dictionary<string, string>(brokers.Length);
                    while (reader.Read())
                    {
                        var brokerId = reader["BrokerId"].ToString();
                        var pmlId = reader["CustomerCode"].ToString();
                        Debug.Assert(!dict.ContainsKey(pmlId));
                        dict.Add(pmlId, brokerId);
                    }
                    return dict;
                });
            }

            foreach (var broker in brokers)
            {
                var pmlId = broker.PmlId;

                if (!pmlId2BrokerIdDict.ContainsKey(pmlId)) continue;
                var brokerId = pmlId2BrokerIdDict[pmlId];

                Debug.Assert(broker.BrokerId == null);
                broker.BrokerId = brokerId;
            }
        }

        public static void LoadQuery(Broker[] brokers)
        {
            foreach (var broker in brokers)
            {
                broker.LoanProfile = LoanProfile.For(broker);
            }
        }

        public const string Error_NoBroker = "No BrokerId";
    }

    #region CSV Helper class
    public sealed class BrokerCsvMap : CsvClassMap<Broker>
    {
        public BrokerCsvMap()
        {
            Map(m => m.PmlId            ).Name("PmlId");
            Map(m => m.RunHours         ).Name("RunHours").TypeConverter<BrokerRunHoursConverter>();
            Map(m => m.BrokerEmail      ).Name("BrokerEmail");
            Map(m => m.sStatusTOptions  ).Name("sStatusTOptions").TypeConverter<BrokerRunHoursConverter>();
            Map(m => m.BrokerId         ).Ignore();
            Map(m => m.LoanProfile      ).Ignore();
            Map(m => m.sRLcKdDMin       ).Name("sRLcKdDMin");
            Map(m => m.sRLcKdDFrom      ).Name("sRLcKdDFrom");
        }
    }

    public class BrokerRunHoursConverter : ITypeConverter
    {
        public string ConvertToString(TypeConverterOptions options, object value)
        {
            Debug.Assert(value is int[]);

            var hours = value as int[];
            return hours.IsNullOrEmpty() ? string.Empty : hours.StringJoin(" ");
        }

        public object ConvertFromString(TypeConverterOptions options, string text)
        {
            if (string.IsNullOrEmpty(text)) return new int[0];
            try
            {
                return text.Split(';').Where(h => !h.IsNullOrWhiteSpace()).Select(h => int.Parse(h.Trim())).ToArray();
            }
            catch (FormatException e)
            {
                throw new FormatException($"Incorrect Run Hours format: \"{text}\"", e);
            }
        }

        public bool CanConvertFrom(Type type)
        {
            return ((type == typeof(string)) || (type == typeof(int[])));
        }

        public bool CanConvertTo(Type type)
        {
            return ((type == typeof(string)) || (type == typeof(int[])));
        }
    }
    #endregion
}
