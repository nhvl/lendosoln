using System;
using System.Collections.Generic;
using System.IO;

using CsvHelper;

namespace SecondaryChangeReport
{
    public static class EnumHelper
    {
        static readonly IDictionary<string, IDictionary<string, string>> _dict;
        
        static EnumHelper()
        {
            var file = AppSettings.EnumDescriptionCsvFile;

            _dict = ParseDict(file);
        }

        static Dictionary<string, IDictionary<string, string>> ParseDict(string file)
        {
            using (var testReader = new StreamReader(file))
            using (var csv = new CsvParser(testReader))
            {
                var ret = new Dictionary<string, IDictionary<string, string>>();

                //var header = csv.Read();

                const int iFieldId = 0;
                const int iEnumVal = 1;
                const int iDescription = 2;

                string[] row;
                IDictionary<string, string> currentFieldDict = null;
                while ((row = csv.Read()) != null)
                {
                    var fieldId = row[iFieldId];
                    var value = row[iEnumVal];
                    var description = row[iDescription];

                    if (!fieldId.IsNullOrWhiteSpace())
                    {
                        if (!ret.ContainsKey(fieldId)) ret.Add(fieldId, new Dictionary<string, string>());

                        currentFieldDict = ret[fieldId];
                    }

                    if (currentFieldDict == null)
                        throw new FormatException($"EnumDescriptionCsvFile[{file}] invalid format");

                    currentFieldDict.Add(value, description);
                }

                return ret;
            }
        }

        public static string Pretty(string field, string value)
        {
            if (!_dict.ContainsKey(field)) return value;
            var d = _dict[field];
            if (!d.ContainsKey(value)) return $"Unknown({value})";
            return d[value];
        }
    }
}