# Secondary Change Report

Author: [Vien Luong](mailto:vienl@dory.vn)
Date: 2014-12-19

The version 2 of Secondary Change Report is not compatible with previous version.
Please **do not** config the v2 to run with files produced by v1.

# Configuration

`SecondaryChangeReportV2\SecondaryChangeReport\App.config`:

- `resultFolder`: the folder that store the result when running Secondary Change Report.
    It contains a list of folder for each Broker with folder name as Broker's PML code.

- `master`, `newdata`, `output`: the result files in Broker folder.

- `dbconnection`: the name of the database to connect to in SQL server.  The name will go through the adapter library to resolve into a full connection string.

- `BrokersCsvFile` is a path to a CSV file with Brokers information.
    - This file has 6 column with header names `PmlId,RunHours,BrokerEmail,sStatusTOptions,sRLcKdDMin,sRLcKdDFrom`.

    - If you want to include multiple hours for the 2nd column `RunHours`,
        separate each value with `;`, e.g. `12;14`.

    - Similarity with `sStatusTOptions`, you separate each value with `;`.
        If you don't provide any value for this column,
        the default one will be used.
        `0;1;3;29;22;30;31;2;28;32;13;4;33;23;34;21;35;36;5;24;25;6;19;26;11;8;9;10;47;48;7`.

    - If you want to query on loans `WHERE sRLcKdD < sRLcKdDMin`, provide value for column `sRLcKdDMin`.
        
        If you want to query on loans `WHERE sRLcKdD < (Now - sRLcKdDFrom)`, provide value for column `sRLcKdDFrom`.
        The format of `sRLcKdDFrom` is `[?Y] [?M] [?D]`, for example: `1Y`, `6M`, `15D` or `1Y 9M 10D`.

        If you not provide both `sRLcKdDMin` nor `sRLcKdDFrom`, the default `1/1/2016` will be used.
        If there are both `sRLcKdDMin` and `sRLcKdDFrom`, `sRLcKdDFrom` will be used.

    By default, it points to `SecondaryChangeReportV2\SecondaryChangeReport\settings\brokers.csv`.
    In this file, I have included all the brokers' info in previous versions.

- `SqlFieldCsvFile` is a path to a CSV file that contain the SQL Field description.
    This file has 4 column with header names `FieldId,Type,DbTable,DbColumnId`.

    - The `FieldId` is an identification string that will be used in later files (`Profile/Default.csv`, `EnumDescriptionCsvFile`).

    - The `DbTable` is the name of a table in Database like `LOAN_FILE_CACHE_2`, `LOAN_FILE_F`.
        If this field is empty, by default, it will be `LOAN_FILE_CACHE`

    - The `DbColumnId` is the column id in Database.

- `LoanProfileFolder` is point to a folder that contains the profiles for brokers.
    Each profile is a CSV file with 2 column `Field,Description`.

    - The `Field` column is the Id of the field that need to export.
        These fields should be described in `SqlFieldCsvFile`.

    - The `Description` column will be used to generate friendly HTML report.

    For each broker (for eg. broker PML0171),
    if there is a profile file with the same name (for eg. `PML0171.csv`)
    then that profile will be applied for that broker.
    Otherwise, the default profile `Default.csv` will be applied.










