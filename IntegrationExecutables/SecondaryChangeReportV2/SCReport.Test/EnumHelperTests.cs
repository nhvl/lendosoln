﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SecondaryChangeReport;
using SecondaryChangeReport.Mock;

namespace SCReport.Test
{
    [TestClass]
    public class EnumHelperTests
    {
        [TestMethod]
        public void Test()
        {
            Assert.AreEqual("ARM", EnumHelper.Pretty("sFinMethT", "1"));
            Assert.AreEqual("Fixed", EnumHelper.Pretty("sFinMethT", "0"));
            Assert.AreEqual("Graduated Payment", EnumHelper.Pretty("sFinMethT", "2"));
            Assert.AreEqual("Unknown(3)", EnumHelper.Pretty("sFinMethT", "3"));
        }
    }
}
