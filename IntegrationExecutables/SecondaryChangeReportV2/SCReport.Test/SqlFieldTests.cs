﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SecondaryChangeReport;

namespace SCReport.Test
{
    [TestClass]
    public class SqlFieldTests
    {
        [TestMethod]
        public void FromCsvTest()
        {
            var fs = SqlField.FromCsv("./settings/sqlFields.csv");
            Assert.AreEqual(54, fs.Length);

            var ids = new HashSet<string>(fs.Select(f => f.FieldId));
            Assert.IsTrue(ids.Contains(SqlField.LoanNumberFieldId));
            Assert.IsTrue(ids.Contains(SqlField.LoanStatusFieldId));

            foreach (var f in fs)
            {
                Console.WriteLine("[{0}] [{1}] [{2}] [{3}]",
                    f.FieldId,
                    f.Type,
                    f.DbTable,
                    f.DbColumnId);
            }
        }
    }
}