using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SecondaryChangeReport;

namespace SCReport.Test
{
    [TestClass]
    public class LoanProfileTest
    {
        [TestMethod]
        public void TestLoad()
        {
            var profileDict = LoanProfile.PmlId2Profiles;

            CollectionAssert.IsSubsetOf(new[] { "Default", "AUTOTEST", "PML0171", "PML0229", "PML0247" }, profileDict.Keys);

            foreach (var p in profileDict)
            {
                var name = p.Key;
                var profile = p.Value;
                Console.WriteLine("[{0}] [{1}]", name, profile.Fields.Length);
            }
        }

        [TestMethod]
        public void TestSqlGen()
        {
            foreach (var profile in LoanProfile.PmlId2Profiles.Values)
            {
                var query = profile.SqlQuery;
                Console.WriteLine("--- {0} -------", profile.Name);
                Console.WriteLine(query);
                Console.WriteLine("---------------");
                Console.WriteLine();
            }
        }
    }
}