using System;
using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SecondaryChangeReport;

namespace SCReport.Test
{
    [TestClass]
    public class CompareReportTests
    {
        [TestMethod]
        public void Test()
        {
            var brokers = Broker.FromCsv(AppSettings.BrokersCsvFile);
            Broker.LoadQuery(brokers);
            //Broker.LoadBrokerId(brokers);

            var broker = brokers.First(b => b.PmlId == "AUTOTEST");
            var d = broker.LoanProfile.Fields.ToDictionary(f => f.Field, f => f.Description);

            var r1 = Report.FromCsv(@"data/master.csv");
            var r2 = Report.FromCsv(@"data/newdata.csv");
            var r = new CompareReport(r1, r2, broker.LoanProfile);
            var htmlFile = @"data/out.html";
            r.Save(htmlFile, d, broker.sStatusTOptions);
            Console.WriteLine("Create report at [{0}]", Path.GetFullPath(htmlFile));
        }
    }
}