using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SecondaryChangeReport;

namespace SCReport.Test
{
    [TestClass]
    public class BrokerTests
    {
        [TestMethod]
        public void GetBrokersFromCsv()
        {
            var brokers = Broker.FromCsv(AppSettings.BrokersCsvFile);

            foreach (var broker in brokers)
            {
                var error = broker.IsValid();
                Console.WriteLine("[{0}] [{1}] [{2}] [{3}] {4} {5} {6}",
                    broker.PmlId,
                    broker.RunHours.StringJoin(";"),
                    broker.BrokerEmail,
                    broker.sStatusTOptions.StringJoin(";"),
                    broker.sRLcKdDMin,
                    broker.sRLcKdDFrom,
                    error ?? string.Empty
                    );

                //Assert.IsTrue(error.IsNullOrEmpty());
            }
            Console.WriteLine();
        }

        [TestMethod]
        public void GetBrokerIdFromPmlId()
        {
            var brokers = Broker.FromCsv(AppSettings.BrokersCsvFile);
            Broker.LoadBrokerId(brokers);

            foreach (var broker in brokers)
            {
                if (broker.BrokerId.IsNullOrEmpty())
                    Console.WriteLine("PmlId=[{0}] have no broker id", broker.PmlId);
                else
                    Console.WriteLine("PmlId=[{0}]\tBrokerId=[{1}]", broker.PmlId, broker.BrokerId);
            }
        }

        [TestMethod]
        public void LoadQuery()
        {
            var brokers = Broker.FromCsv(AppSettings.BrokersCsvFile);
            Broker.LoadQuery(brokers);
            foreach (var broker in brokers)
            {
                Assert.IsNotNull(broker.LoanProfile);
                Assert.IsNotNull(broker.LoanProfile.SqlQuery);

                Console.WriteLine(broker.PmlId);
                Console.WriteLine(broker.LoanProfile.SqlQuery);
                Console.WriteLine();
                Console.WriteLine();
            }
        }

        [TestMethod]
        public void LoadLoanAndSaveToCsv()
        {
            var brokers = Broker.FromCsv(AppSettings.BrokersCsvFile);
            Broker.LoadQuery(brokers);
            Broker.LoadBrokerId(brokers);
            foreach (var broker in brokers)
            {
                var outCsv = $@"./out/{broker.PmlId}.csv";
                var n = broker.LoadLoanToCsv(outCsv);

                Console.WriteLine("Export {1} loans to file [{0}]", Path.GetFullPath(outCsv), n);

            }
        }
    }
}
