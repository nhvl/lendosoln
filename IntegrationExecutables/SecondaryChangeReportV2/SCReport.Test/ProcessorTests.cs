using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SecondaryChangeReport;
using SecondaryChangeReport.Provider;

namespace SCReport.Test
{
    [TestClass]
    public class ProcessorTests
    {
        [TestMethod]
        public void Verify()
        {
            Assert.IsNull(Verify(AppSettings.BrokersCsvFile));
        }

        [TestMethod]
        public void VerifyIncorrectCsv()
        {
            Assert.IsTrue(Verify("./settings/enum.csv").Contains("Fields 'PmlId' do not exist"));
        }

        [TestMethod]
        public void VerifyCsvWithLotOfComma()
        {
            Assert.AreEqual(null, Verify("./settings/brokersWithLotOfComma.csv"));
        }

        [TestMethod]
        public void VerifyCsvWithMissingComma()
        {
            Assert.AreEqual(null, Verify("./settings/brokersWithMissingComma.csv"));
        }

        [TestMethod]
        public void VerifyCsvWithMissingData()
        {
            Assert.IsTrue(Verify("./settings/brokersWithMissingData.csv").Contains("Incorrect Run Hours format"));
        }

        [TestMethod]
        public void VerifyCsvWithPML000ERROR()
        {
            var error = Verify("./settings-bad/brokersPML000ERROR.csv");
            Console.WriteLine(error);
            Assert.IsTrue(error.Contains("Fields \'Field\' do not exist in the CSV file."));
        }


        string Verify(string brokerCsvFile)
        {
            var logger = new ConsoleLogger();
            var mailer = new SecondaryChangeReport.Mock.Mailer();

            var p = new Processor(logger, mailer);
            return p.Verify(brokerCsvFile);
        }
    }
}
