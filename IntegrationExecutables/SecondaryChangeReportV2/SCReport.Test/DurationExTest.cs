﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SecondaryChangeReport;

namespace SCReport.Test
{
    [TestClass]
    public class DurationExTest
    {
        [TestMethod]
        public void TestMethod1()
        {
            var now = DateTime.Now.Date;
            var oneYearFromNow = DurationEx.Since("1Y");
            var oneMonthFromNow = DurationEx.Since("1M");
            var oneDayFromNow = DurationEx.Since("1D");
            var _1Y1M1DFromNow = DurationEx.Since("1Y 1M 1D");
            Console.WriteLine("1Y = {0}", oneYearFromNow);
            Console.WriteLine("1M = {0}", oneMonthFromNow);
            Console.WriteLine("1D = {0}", oneDayFromNow);
            Console.WriteLine("1Y 1M 1D = {0}", _1Y1M1DFromNow);

            Assert.AreEqual(now, DurationEx.Since("1Y").AddYears(1));
            Assert.AreEqual(now, DurationEx.Since("1M").AddMonths(1));
            Assert.AreEqual(now, DurationEx.Since("1D").AddDays(1));
            Assert.AreEqual(now, DurationEx.Since("1Y 1M 1D").AddYears(1).AddMonths(1).AddDays(1));
        }
    }
}
