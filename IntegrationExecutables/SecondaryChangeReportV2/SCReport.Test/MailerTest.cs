using System;
using System.Net.Mail;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SecondaryChangeReport;

namespace SCReport.Test
{
    [TestClass]
    public class MailerTest
    {
        [TestMethod]
        public void Test()
        {
            string subject = "SUBJECT";
            var brokers = Broker.FromCsv(AppSettings.BrokersCsvFile);
            foreach (Broker broker in brokers)
            {
                string destinationEmail = broker.BrokerEmail;
                try
                {
                    using (new MailMessage(AppSettings.SupportEmail, destinationEmail, subject, string.Empty))
                    {
                        //Console.WriteLine(message.To);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    Assert.Fail(destinationEmail);
                }
            }
        }

        [TestMethod]
        public void TestSendMail()
        {
            var logger = new SecondaryChangeReport.Provider.ConsoleLogger();
            var mailer = new SecondaryChangeReport.Provider.Mailer(logger);
            mailer.SendEmail("SecondaryChangeReport Test Subject", "SecondaryChangeReport Test Body");
            mailer.SendEmailWithAttachment("napxer@gmail.com", "SecondaryChangeReport Test Subject", "./settings/brokers.csv");
        }
    }
}