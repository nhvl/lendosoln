﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Net.Mail;

namespace SendDataToSI
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Net.Mail.SmtpClient smtpClient = new SmtpClient(AppSettings.SMTP_SERVER);

            try
            {
                Adapter.Drivers.ConnectionStringProviderHelper.Register("SendDataToSI");
                string connectionString = Adapter.Drivers.ConnectionStringProviderHelper.GetMainConnectionStringByDatabaseName(AppSettings.DSN, needWriteAccess: false);
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    foreach (string lender in AppSettings.LENDERS)
                    {
                        // query data from db
                        SqlCommand cmd = new SqlCommand(@"RetrieveSIData", conn);
                        cmd.CommandTimeout = 60;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("CustomerCode", lender);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            // generate CSV data
                            byte[] data;
                            using (MemoryStream ms = new MemoryStream())
                            {
                                ReportGenerator generator = new ReportGenerator();
                                generator.Generate(reader, ms);
                                ms.Flush();
                                data = ms.ToArray();
                            }
                            string s = System.Text.ASCIIEncoding.ASCII.GetString(data);
                            File.WriteAllText(lender + ".CSV", s);
                            // create mail message
                            MailMessage message = new MailMessage("support@lendingqb.com", AppSettings.RECIPIENT);
                            message.Subject = lender;
                            Attachment attachment;
                            using (MemoryStream ms = new MemoryStream(data))
                            {
                                attachment = new Attachment(ms, string.Format("{0}.CSV", lender));
                                message.Attachments.Add(attachment);

                                // send message
                                smtpClient.Send(message);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MailMessage message = new MailMessage("support@lendingqb.com", "brianb@lendingqb.com", "SendDataToSI", ex.ToString());
                smtpClient.Send(message);
            }
        }
    }
}
