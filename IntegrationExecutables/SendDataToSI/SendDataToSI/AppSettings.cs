﻿using System;
using System.IO;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SendDataToSI
{
    class AppSettings
    {
        public static string DSN
        {
            get { return System.Configuration.ConfigurationManager.AppSettings["DSN"]; }
        }
        public static string[] LENDERS
        {
            get { return System.Configuration.ConfigurationManager.AppSettings["LENDERS"].Split(';'); }
        }
        public static string RECIPIENT
        {
            get { return System.Configuration.ConfigurationManager.AppSettings["RECIPIENT"]; }
        }
        public static string SMTP_SERVER
        {
            get { return System.Configuration.ConfigurationManager.AppSettings["SMTP_SERVER"]; }
        }
    }
}
