﻿using System;

namespace UploadSellSideInfoToLQB
{
    public class MissingAppSettingException : Exception
    {
        public MissingAppSettingException(string sAppSettingName) : base(string.Format("Upload failed. AppSetting {0} was not found. Please add an {0} AppSetting to the config file and retry.", sAppSettingName)) { }
    }
}
