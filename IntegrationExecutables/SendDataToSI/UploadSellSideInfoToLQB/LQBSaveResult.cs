﻿using System;

namespace UploadSellSideInfoToLQB
{
    public class LQBSaveResult
    {
        public enum SaveStatusT
        {
            OK = 0,
            WARNING,
            ERROR
        };

        #region Variables
        private SaveStatusT m_eStatus = SaveStatusT.OK;
        private string m_sWarning;
        private string m_sError;

        public string Error
        { 
            get { return m_sError; } 
        }

        public string Warning
        {
            get { return m_sWarning; }
        }

        public SaveStatusT Status
        {
            get { return m_eStatus; }
        }
        #endregion

        public LQBSaveResult(SaveStatusT status, string sMessage)
        {
            m_eStatus = status;
            switch (status)
            {
                case SaveStatusT.ERROR:
                    m_sError = sMessage;
                    break;
                case SaveStatusT.WARNING:
                    m_sWarning = sMessage;
                    break;
                case SaveStatusT.OK:
                default:
                    break;
            }
        }
    }
}
