﻿using System;
using System.IO;
using UploadSellSideInfoToLQB.LoanSvc;

namespace UploadSellSideInfoToLQB
{
    public class LoanProcessor
    {
        #region Variables
        private Loan m_loanService;
        #endregion

        public LoanProcessor()
        {
            m_loanService = ServiceProvider.LoanService();
        }

        public LQBSaveResult Save(string sAuthTicket, LoanData loan, int iFormat)
        {
            string sResult = m_loanService.Save(sAuthTicket, loan.LoanNumber, GenerateLOXML(loan), iFormat);
            return XMLDocument.ResultFromLQBSave(sResult, loan.LoanNumber);
        }

        private static string GenerateLOXML(LoanData loan)
        {
            return XMLDocument.ToLOXML(loan);
        }
    }
}
