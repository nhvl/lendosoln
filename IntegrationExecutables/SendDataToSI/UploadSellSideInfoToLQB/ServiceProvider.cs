﻿using System;
using UploadSellSideInfoToLQB.AuthSvc;
using UploadSellSideInfoToLQB.LoanSvc;

namespace UploadSellSideInfoToLQB
{
    public static class ServiceProvider
    {
        public static AuthService AuthenticationService()
        {
            AuthService auth = new AuthService();
            auth.Url = Settings.AuthURL(Settings.ServiceEnvironment);
            return auth;
        }

        public static Loan LoanService()
        {
            Loan loan = new Loan();
            loan.Url = Settings.LoanURL(Settings.ServiceEnvironment);
            return loan;
        }
    }
}
