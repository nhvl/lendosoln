﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace UploadSellSideInfoToLQB
{
    public class SIFormat
    {
        #region Variables
        private int m_iCommittedTimeColIndex = -1;
        private int m_iDeliveryDateColIndex = -1;
        private int m_iInvestorCommitmentNumColIndex = -1;
        private int m_iInvestorNameColIndex = -1;
        private int m_iLoanNumColIndex = -1;
        private int m_iNoteRate = -1;
        private int m_iTotalPriceColIndex = -1;
        private Dictionary<string, int> m_customDictionaryColIndices;

        public SIFormat(Dictionary<string, string>.KeyCollection customColumnNames)
        {
            m_customDictionaryColIndices = new Dictionary<string, int>();
            foreach(string customField in customColumnNames)
            {
                m_customDictionaryColIndices.Add(customField, -1);
            }
        }

        public int CommittedTimeColumnNum
        {
            get { return m_iCommittedTimeColIndex; }
            set { m_iCommittedTimeColIndex = value; }
        }

        public int DeliveryDateColumnNum
        {
            get { return m_iDeliveryDateColIndex; }
            set { m_iDeliveryDateColIndex = value; }
        }

        public int InvestorCommitmentColumnNum
        {
            get { return m_iInvestorCommitmentNumColIndex; }
            set { m_iInvestorCommitmentNumColIndex = value; }
        }

        public int InvestorNameColumnNum
        {
            get { return m_iInvestorNameColIndex; }
            set { m_iInvestorNameColIndex = value; }
        }

        public int LoanNumberColumnNum
        {
            get { return m_iLoanNumColIndex; }
            set { m_iLoanNumColIndex = value; }
        }

        public int NoteRateColumnNum
        {
            get { return m_iNoteRate; }
            set { m_iNoteRate = value; }
        }

        public int TotalPriceColumnNum
        {
            get { return m_iTotalPriceColIndex; }
            set { m_iTotalPriceColIndex = value; }
        }

        public Dictionary<string, int> CustomDictionaryColumnNums
        {
            get { return m_customDictionaryColIndices; }
        }
        #endregion

        public bool HasExpectedColumns()
        {
            return (IsIndexValid(m_iDeliveryDateColIndex) && IsIndexValid(m_iInvestorCommitmentNumColIndex)
                && IsIndexValid(m_iInvestorNameColIndex) && IsIndexValid(m_iLoanNumColIndex)
                && IsIndexValid(m_iTotalPriceColIndex) && IsIndexValid(m_iCommittedTimeColIndex)
                && IsIndexValid(m_iNoteRate) && (m_customDictionaryColIndices.Count == 0 || m_customDictionaryColIndices.All(c => IsIndexValid(c.Value))));
        }

        private bool IsIndexValid(int index)
        { 
            return (index >= 0);
        }
    }
}
