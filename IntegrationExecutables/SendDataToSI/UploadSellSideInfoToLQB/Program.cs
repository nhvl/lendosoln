﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Windows.Forms;

namespace UploadSellSideInfoToLQB
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Out.WriteLine("Working...");
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

            try
            {
                Dictionary<string, string> customColumns = Settings.CustomColumns;
                List<LoanData> loanPackage = CSVReader.ReadLoansFromSIFile(customColumns.Keys);

                if (loanPackage.Count > 0)
                {
                    Authenticator auth = new Authenticator();
                    string sTicket = auth.Authenticate();

                    if (auth.Authenticated)
                    {
                        UploadLoansToLQB(loanPackage, sTicket);
                    }
                }
                else
                {
                    MessageBox.Show("Upload failed. Could not find any loan data in the report: " + Settings.INPUT_CSV, Settings.EXE_NAME, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (KeyNotFoundException mExc)
            {
                MessageBox.Show(mExc.Message, Settings.EXE_NAME, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (MissingAppSettingException mExc)
            {
                MessageBox.Show(mExc.Message, Settings.EXE_NAME, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private static void UploadLoansToLQB(List<LoanData> loanPackage, string sAuthTicket)
        {
            LoanProcessor LQBLoanProcessor = new LoanProcessor();
            bool bHasErrors = false;
            bool bHasWarnings = false;
            StringBuilder sErrors = new StringBuilder();
            StringBuilder sWarnings = new StringBuilder();

            foreach (LoanData loan in loanPackage)
            {
                try
                {
                    LQBSaveResult sResult = LQBLoanProcessor.Save(sAuthTicket, loan, 0);
                    switch (sResult.Status)
                    {
                        case LQBSaveResult.SaveStatusT.ERROR:
                            bHasErrors = true;
                            sErrors.AppendLine(sResult.Error);
                            break;
                        case LQBSaveResult.SaveStatusT.OK:
                            break;
                        case LQBSaveResult.SaveStatusT.WARNING:
                            bHasWarnings = true;
                            sWarnings.AppendLine(sResult.Warning);
                            break;
                        default:
                            throw new ApplicationException("An unrecognized status type was returned by the Save API.");
                    }
                }
                catch (Exception ex)
                {
                    bHasErrors = true;
                    sErrors.AppendLine("Aborting upload due to error. See error below:" + System.Environment.NewLine + ex.ToString());
                    break;
                }
            }

            if (bHasErrors)
            {
                MessageBox.Show("Upload to LQB failed with errors: " + System.Environment.NewLine + sErrors.ToString(), Settings.EXE_NAME, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            if (bHasWarnings)
            {
                MessageBox.Show("LQB returned the following warnings: " + System.Environment.NewLine + sWarnings.ToString(), Settings.EXE_NAME, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            
            if(!(bHasErrors || bHasWarnings))
            {
                MessageBox.Show("Upload Finished.", Settings.EXE_NAME, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
