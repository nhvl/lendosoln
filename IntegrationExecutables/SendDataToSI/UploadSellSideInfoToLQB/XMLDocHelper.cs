﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Linq;
namespace UploadSellSideInfoToLQB
{
    public static class XMLDocHelper
    {
        public static XmlDocument CreateXmlDoc(string xmlText)
        {
            try
            {
                return CreateXmlDocImpl(xmlText);
            }
            catch (XmlException)
            {
                xmlText = SantizeXmlString(xmlText);
                return CreateXmlDocImpl(xmlText);
            }
        }

        public static bool IsXML(string sInputString)
        {
            if (String.IsNullOrEmpty(sInputString))
            {
                return false;
            }

            try { XDocument.Parse(sInputString); }
            catch (XmlException)
            {
                return false;
            }

            return true;
        }

        public static Dictionary<string, string> LoadInvestorMap()
        {
            Dictionary<string, string> investorMap = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
            string sMapFileContentsXML = FileIO.LoadFile("Resources/InvestorMap.xml");
            XmlDocument xMapDoc = CreateXmlDoc(sMapFileContentsXML);

            XmlElement xRoot = (XmlElement)xMapDoc.SelectSingleNode("//InvestorMap");
            if (xRoot != null)
            {
                foreach (XmlElement xInvestor in xMapDoc.SelectNodes("//Investor"))
                {
                    string sInvestorName = xInvestor.GetAttribute("name").Trim();
                    string sRolodexID = xInvestor.InnerText.Trim();
                    investorMap.Add(sInvestorName, sRolodexID);
                }
            }

            return investorMap;
        }

        #region Helpers copied from LosUtils.cs, LendOSoln
        /// <summary>
        /// Remove all illegal XML characters from string. The list of allowable characters http://www.w3.org/TR/REC-xml/#charsets
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static string SantizeXmlString(string xml)
        {
            StringBuilder sb = new StringBuilder(xml.Length);
            foreach (char ch in xml.ToCharArray())
            {
                if (IsLegalXmlChar(ch))
                {
                    sb.Append(ch);
                }
            }
            return sb.ToString();
        }

        private static bool IsLegalXmlChar(int ch)
        {
            //The list of allowable characters http://www.w3.org/TR/REC-xml/#charsets
            return (ch == 0x9 || ch == 0xA || ch == 0xD || (ch >= 0x20 && ch <= 0xD7FF) || (ch >= 0xE000 && ch <= 0xFFFD) || (ch >= 0x10000 && ch <= 0x10FFFF));
        }

        private static XmlDocument CreateXmlDocImpl(string xmlText)
        {
            XmlDocument xmlDoc = new XmlDocument();

            if (null == xmlText || xmlText.Trim() == "")
                return xmlDoc;

            XmlTextReader readerData = null;

            try
            {
                if (xmlText != null && xmlText.Length <= (4 * 1024))
                {
                    readerData = new XmlTextReader(xmlText, XmlNodeType.Document, null);
                }
                else
                    readerData = new XmlTextReader(new StringReader(xmlText));

                // OPM 4796 : tell XmlDocument to not parse any DTD file.
                readerData.XmlResolver = null;
                xmlDoc.Load(readerData);
            }
            finally
            {
                if (null != readerData)
                {
                    readerData.Close();
                }
            }
            return xmlDoc;
        }
        #endregion
    }
}
