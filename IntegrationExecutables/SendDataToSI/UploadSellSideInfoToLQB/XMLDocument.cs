﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Linq;

namespace UploadSellSideInfoToLQB
{
    public static class XMLDocument
    {
        public static LQBSaveResult ResultFromLQBSave(string sSaveResultXML, string sLoanNumber)
        {
            XDocument xDoc = XDocument.Parse(sSaveResultXML);

            string sStatus = xDoc.Element("result").Attribute("status").Value;
            switch(sStatus)
            {
                case "Error":
                    return new LQBSaveResult(LQBSaveResult.SaveStatusT.ERROR, LoanMessage(xDoc.Element("result").Element("error").Value, sLoanNumber));
                case "OKWithWarning":
                    return new LQBSaveResult(LQBSaveResult.SaveStatusT.WARNING, LoanMessage(xDoc.Element("result").Element("warning").Value, sLoanNumber));
                case "OK": 
                    return new LQBSaveResult(LQBSaveResult.SaveStatusT.OK, "OK");
                default:
                    return new LQBSaveResult(LQBSaveResult.SaveStatusT.ERROR, LoanMessage("Failed to recognize response from the Save API: " + System.Environment.NewLine + xDoc.ToString(), sLoanNumber));
            }
        }

        public static string ToLOXML(LoanData loan)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Encoding = Encoding.ASCII;
                settings.OmitXmlDeclaration = true;
                settings.ConformanceLevel = ConformanceLevel.Fragment;
                
                XmlWriter xmlWriter = XmlWriter.Create(memoryStream, settings);
                xmlWriter.WriteStartElement("LOXmlFormat");
                xmlWriter.WriteAttributeString("version", "1.0");
                xmlWriter.WriteStartElement("loan");

                WriteField(xmlWriter, "sinvestorrolodexid", loan.InvestorRolodexID);
                WriteField(xmlWriter, "sinvestorlockconfnum", loan.InvestorCommitmentNumber);
                //Calculate Expiration from commitment date and lock period, which is the diff from delivery date.
                WriteField(xmlWriter, "sInvestorLockRLckExpiredDLckd", "false");
                WriteField(xmlWriter, "sInvestorLockDeliveryExpiredDLckd", "false");
                WriteField(xmlWriter, "sInvestorLockRLckdD", loan.CommittedDate.ToString("d"));
                string committmentPeriod = (loan.DeliveryDate - loan.CommittedDate).TotalDays.ToString();
                WriteField(xmlWriter, "sInvestorLockRLckdDays", committmentPeriod);
                //Assume Mandatory Investor Commitment.
                WriteField(xmlWriter, "sInvestorLockCommitmentT", "1");
                foreach(KeyValuePair<string, string> customColumn in loan.CustomColumns)
                {
                    WriteField(xmlWriter, Settings.CustomColumns[customColumn.Key], customColumn.Value);
                }

                decimal dFee = ComputeFeeFromPrice(loan.TotalPrice);
                if (dFee != decimal.MinValue)
                {
                    WriteBackEndLockCollection(xmlWriter, loan.NoteRate, dFee.ToString());
                }

                xmlWriter.WriteEndElement(); // </loan>
                xmlWriter.WriteEndElement(); // </LOXmlFormat>
                xmlWriter.Flush();
                return ASCIIEncoding.ASCII.GetString(memoryStream.GetBuffer(), 0, (int)memoryStream.Position);
            }
        }

        private static decimal ComputeFeeFromPrice(string sPrice)
        {
            decimal dPrice = ParseDecimal(sPrice);
            return (dPrice == decimal.MinValue) ? dPrice : 100 - dPrice;
        }

        private static string LoanMessage(string sMessage, string sLoanNumber)
        {
            return string.Format("Loan {0}: {1}", sLoanNumber, sMessage);
        }

        private static decimal ParseDecimal(string sPrice)
        {
            decimal dPrice = decimal.MinValue;

            decimal.TryParse(sPrice.Trim(), NumberStyles.Number, null, out dPrice);
            return dPrice;
        }

        private static void WriteBackEndLockCollection(XmlWriter xmlWriter, string sRate, string sFee)
        {
            xmlWriter.WriteStartElement("collection");
            xmlWriter.WriteAttributeString("id", "sinvestorratelocktable");
            xmlWriter.WriteStartElement("record");
            WriteField(xmlWriter, "sInvestorLockPriceRowLockedT", "Adjusted");
            WriteField(xmlWriter, "Rate", sRate);
            WriteField(xmlWriter, "Fee", sFee);
            WriteField(xmlWriter, "Lock", "True");
            WriteField(xmlWriter, "LockReason", "Locked via the OBSS Shipping Report importer.");
            xmlWriter.WriteEndElement(); // </record>
            xmlWriter.WriteEndElement(); // </collection>
        }

        private static void WriteField(XmlWriter xmlWriter, string sFieldID, string sValue)
        {
            if (string.IsNullOrEmpty(sValue))
            {
                sValue = "";
            }
            xmlWriter.WriteStartElement("field");
            xmlWriter.WriteAttributeString("id", sFieldID);
            xmlWriter.WriteString(sValue);
            xmlWriter.WriteEndElement();
        }
    }
}
