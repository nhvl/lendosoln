﻿using System;
using System.Collections.Generic;

namespace UploadSellSideInfoToLQB
{
    public class LoanData
    {
        #region Variables
        private DateTime m_sCommittedDate;
        private DateTime m_sDeliveryDate;
        private string m_sInvestorCommitmentNum;
        private string m_sInvestorRolodexID;
        private string m_sLoanNum;
        private string m_sNoteRate;
        private string m_sTotalPrice;
        private Dictionary<string, string> m_customColumns;

        public DateTime CommittedDate
        {
            get { return m_sCommittedDate; }
            set { m_sCommittedDate = value; }
        }
        public DateTime DeliveryDate
        {
            get { return m_sDeliveryDate; }
            set { m_sDeliveryDate = value; }
        }
        public string InvestorCommitmentNumber
        {
            get { return m_sInvestorCommitmentNum; }
            set { m_sInvestorCommitmentNum = value; }
        }
        public string InvestorRolodexID
        {
            get { return m_sInvestorRolodexID; }
            set { m_sInvestorRolodexID = value; }
        }
        public string LoanNumber
        {
            get { return m_sLoanNum; }
            set { m_sLoanNum = value; }
        }
        public string NoteRate
        {
            get { return m_sNoteRate; }
            set { m_sNoteRate = value; }
        }
        public string TotalPrice
        {
            get { return m_sTotalPrice; }
            set { m_sTotalPrice = value; }
        }

        public Dictionary<string, string> CustomColumns
        {
            get { return m_customColumns; }
        }
        #endregion

        public LoanData()
        {
            m_customColumns = new Dictionary<string, string>();
        }

        public bool HasLoanData()
        {
            return !string.IsNullOrEmpty(m_sLoanNum);
        }
    }
}
