﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace UploadSellSideInfoToLQB
{
    static class CSVReader
    {
        private static Dictionary<string, string> m_investorMap = XMLDocHelper.LoadInvestorMap();
        private static string COMMA = "&Comma";

        public static List<LoanData> ReadLoansFromSIFile(Dictionary<string, string>.KeyCollection customColumnNames)
        {
            string sInputData = FileIO.LoadFile(Settings.INPUT_CSV.Trim());

            return ParseSIFile(sInputData, customColumnNames);
        }

        private static List<LoanData> ParseSIFile(string sFileContent, Dictionary<string, string>.KeyCollection customColumnNames)
        {
            string[] sRows = sFileContent.Split(new string[] { System.Environment.NewLine }, System.StringSplitOptions.RemoveEmptyEntries);
            Regex rxHeaderRow = new Regex(@"Loan[\s]*Number");
            Match matchLD;
            SIFormat SIData = null;
            List<LoanData> loans = new List<LoanData>();
            string sHeaderRow = null;
            int iHeaderIndex = 0;

            for (int i = 0; i < sRows.Length; i++)
            {
                matchLD = rxHeaderRow.Match(sRows[i]);
                if (matchLD.Success)
                {
                    sHeaderRow = EscapeCommas(sRows[i]);
                    if (HasMoreThanOneColumn(sHeaderRow))
                    {
                        iHeaderIndex = i;
                        SIData = FindColumnIndices(sHeaderRow, customColumnNames);
                        break;
                    }
                }
            }

            if (SIData.HasExpectedColumns())
            {
                for (int j = iHeaderIndex + 1; j < sRows.Length; j++)
                {
                    LoanData loan = ReadLoanData(sRows[j], SIData);
                    if (loan.HasLoanData())
                    {
                        loans.Add(loan);
                    }
                }
            }
            return loans;
        }

        private static SIFormat FindColumnIndices(string sHeaderRow, Dictionary<string, string>.KeyCollection customColumnNames)
        {
            SIFormat SIColumnInfo = new SIFormat(customColumnNames);
            Regex rxLoan = new Regex(@"Loan[\s]*Number");
            Regex rxInvestor = new Regex(@"Investor[\w\s]*Name");
            Regex rxInvestorCommitment = new Regex(@"Investor[\w\s]*Commit");
            Regex rxPrice = new Regex(@"Total[\w\s]*Price");
            Regex rxDelivery = new Regex(@"Priced[\w\s]*Delivery[\w\s]*Date");
            Regex rxCommittedTime = new Regex(@"Committed[\s]Time");
            Regex rxInvestorLoanNumber = new Regex(@"Investor[\s]Loan[\s]#");
            Regex rxNoteRate = new Regex(@"Note[\w\s]*Rate");
            
            string[] sColumns = SplitRowIntoCells(sHeaderRow);

            for (int i = 0; i<sColumns.Length; i++)
            {
                if (rxLoan.Match(sColumns[i]).Success)
                {
                    SIColumnInfo.LoanNumberColumnNum = i;
                }
                else if (rxInvestor.Match(sColumns[i]).Success)
                {
                    SIColumnInfo.InvestorNameColumnNum = i;
                }
                else if (rxInvestorCommitment.Match(sColumns[i]).Success)
                {
                    SIColumnInfo.InvestorCommitmentColumnNum = i;
                }
                else if (rxPrice.Match(sColumns[i]).Success)
                {
                    SIColumnInfo.TotalPriceColumnNum = i;
                }
                else if (rxDelivery.Match(sColumns[i]).Success)
                {
                    SIColumnInfo.DeliveryDateColumnNum = i;
                }

                else if (rxCommittedTime.Match(sColumns[i]).Success)
                {
                    SIColumnInfo.CommittedTimeColumnNum = i;
                }

                else if (rxNoteRate.Match(sColumns[i]).Success)
                {
                    SIColumnInfo.NoteRateColumnNum = i;
                }

                else
                {
                    string columnName = sColumns[i].Trim();
                    if (SIColumnInfo.CustomDictionaryColumnNums.ContainsKey(columnName))
                    {
                        SIColumnInfo.CustomDictionaryColumnNums[columnName] = i;
                    }
                }
            }

            return SIColumnInfo;
        }

        private static LoanData ReadLoanData(string sLoanDataRow, SIFormat columnIndices)
        {
            LoanData loan = new LoanData();
            string[] sLoanCells = SplitRowIntoCells(EscapeCommas(sLoanDataRow));
            loan.LoanNumber = ReadCell(sLoanCells, columnIndices.LoanNumberColumnNum);
            
            if(!string.IsNullOrEmpty(loan.LoanNumber))
            {
                string committedTime = ReadCell(sLoanCells, columnIndices.CommittedTimeColumnNum);
                loan.CommittedDate = DateTime.Parse(committedTime).Date;
                string deliveryDate = ReadCell(sLoanCells, columnIndices.DeliveryDateColumnNum);
                loan.DeliveryDate = DateTime.Parse(deliveryDate).Date;
                loan.InvestorCommitmentNumber = ReadCell(sLoanCells, columnIndices.InvestorCommitmentColumnNum);
                string investorName = ReadCell(sLoanCells, columnIndices.InvestorNameColumnNum);
                loan.InvestorRolodexID = m_investorMap[investorName.Trim()];
                loan.NoteRate = ReadCell(sLoanCells, columnIndices.NoteRateColumnNum);
                loan.TotalPrice = ReadCell(sLoanCells, columnIndices.TotalPriceColumnNum);
                foreach(KeyValuePair<string,int> customColumn in columnIndices.CustomDictionaryColumnNums)
                {
                    loan.CustomColumns.Add(customColumn.Key, ReadCell(sLoanCells, customColumn.Value));
                }
            }
            return loan;
        }

        // Escape any commas that aren't column delimiters (e.g. Address = "123 Anywhere, Unit B")
        private static string EscapeCommas(string sRow)
        {
            Regex rxComma = new Regex(@"""[^"",]+,[^""]+""");
            Match mComma = rxComma.Match(sRow);
            while (mComma.Success)
            {
                sRow = sRow.Replace(mComma.Value, mComma.Value.Replace(",", COMMA));
                mComma = mComma.NextMatch();
            }
            return sRow;
        }

        private static string[] SplitRowIntoCells(string sRow)
        {
            string[] sCells = sRow.Split(',');
            for (int i = 0; i < sCells.Length; i++)
            {
                sCells[i] = UnEscapeCommas(sCells[i]);
            }
            return sCells;
        }

        private static bool HasMoreThanOneColumn(string sRow)
        {
            return (sRow.IndexOf(',') >= 0) ? true : false;
        }

        private static string ReadCell(string[] sCells, int index)
        {
            string sCell = "";
            if (sCells.Length > index)
            {
                sCell = UnEscapeCommas(TrimCell(sCells[index]));
            }
            return sCell;
        }

        private static string TrimCell(string sCell)
        {
            return sCell.Trim().TrimStart('"').TrimEnd('"').Trim();
        }

        private static string UnEscapeCommas(string sCell)
        {
            return sCell.Replace(COMMA, ",");
        }
    }
}
