﻿using System;
using System.Text;
using System.IO;
using System.Security;
using System.Windows.Forms;

namespace UploadSellSideInfoToLQB
{
    public static class FileIO
    {
        public static void WriteToFile(string sFilePath, string sText)
        {
            using (FileStream fs = new FileStream(sFilePath, FileMode.Create, FileAccess.ReadWrite, FileShare.Read))
            {
                byte[] btData = System.Text.Encoding.ASCII.GetBytes(sText);
                fs.Write(btData, 0, btData.Length);
            }
        }

        public static string LoadFile(string sFilePath)
        {
            string sResult = "";
            if (File.Exists(sFilePath))
            {
                try
                {
                    using (FileStream fsStream = new FileStream(sFilePath, FileMode.Open, FileAccess.Read, FileShare.Read))
                    {
                        using (StreamReader srReader = new StreamReader(fsStream))
                        {
                            sResult = GetContents(srReader);
                        }
                    }
                }
                catch (IOException)
                {
                    MessageBox.Show("Could not open the SI report. If it is open in another window (e.g. excel), please close it and retry the upload." + System.Environment.NewLine + Settings.INPUT_CSV, Settings.EXE_NAME, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (SecurityException)
                {
                    MessageBox.Show("Could not open the SI report. If it is open in another window (e.g. excel), please close it and retry the upload." + System.Environment.NewLine + Settings.INPUT_CSV, Settings.EXE_NAME, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (UnauthorizedAccessException)
                {
                    MessageBox.Show("Could not open the SI report. Please make sure that you have read access to it." + System.Environment.NewLine + Settings.INPUT_CSV, Settings.EXE_NAME, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            return sResult;
        }

        // Returns a string containing the file data read using the given reader
        private static string GetContents(StreamReader srReader)
        {
            const int BUFFER_SIZE = 50000;
            char[] chBuffer = new char[BUFFER_SIZE];
            int n = 0;

            StringBuilder sb = new StringBuilder();
            while ((n = srReader.Read(chBuffer, 0, BUFFER_SIZE)) > 0)
            {
                sb.Append(chBuffer, 0, n);
            }
            return sb.ToString();
        }
    }
}
