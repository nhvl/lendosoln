﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;

namespace UploadSellSideInfoToLQB
{
    public static class Settings
    {
        private const string LOCALDOMAIN = "http://localhost/lendersofficeapp/los/webservice/";
        private const string DEVDOMAIN = "http://dev.lendersoffice.com/los/webservice/";
        private const string PRODDOMAIN = "https://webservices.lendingqb.com/los/webservice/";

        private static Dictionary<string, string> m_customColumns;

        public enum Environment
        {
            DEV,
            LOCAL,
            PRODUCTION
        }

        public static string AuthURL(Environment testEnv)
        {
            return ServiceDomain(testEnv) + "AuthService.asmx";
        }

        public static string EXE_NAME
        {
            get { return "Upload Sell-side Info To LQB"; }
        }

        public static string INPUT_CSV
        {
            get { return ConfigurationManager.AppSettings["SHIPPING_REPORT_CSV"] ?? MissingAppSetting("SHIPPING_REPORT_CSV"); }
        }

        public static Dictionary<string, string> CustomColumns
        {
            get
            {
                if (m_customColumns == null)
                {
                    LoadCustomColumns();
                }

                return m_customColumns;
            }
        }

        public static string LoanURL(Environment testEnv)
        {
            return ServiceDomain(testEnv) + "Loan.asmx";
        }

        public static string LOGIN
        {
            get { return ConfigurationManager.AppSettings["LOGIN"] ?? MissingAppSetting("LOGIN"); }
        }

        public static string PASSWORD
        {
            get { return ConfigurationManager.AppSettings["PASSWORD"] ?? MissingAppSetting("PASSWORD"); }
        }

        public static Settings.Environment ServiceEnvironment
        {
            get { return (string.IsNullOrEmpty(Settings.DEBUG)) ? Settings.Environment.PRODUCTION : Settings.Environment.LOCAL; }
        }

        private static string DEBUG
        {
            get { return ConfigurationManager.AppSettings["DEBUG"]; }
        }

        private static string MissingAppSetting(string sAppSettingName)
        {
            throw new MissingAppSettingException(sAppSettingName);
        }

        private static string ServiceDomain(Environment serviceEnv)
        {
            switch (serviceEnv)
            {
                case Environment.PRODUCTION: return PRODDOMAIN;
                case Environment.DEV: return DEVDOMAIN;
                case Environment.LOCAL:
                default: return LOCALDOMAIN;
            }
        }

        private static void LoadCustomColumns()
        {
            m_customColumns = new Dictionary<string, string>();
            string customColumnPrefix = "CustomColumn_";
            string[] keys = ConfigurationManager.AppSettings.AllKeys;

            foreach (string key in keys)
            {
                if (key.StartsWith(customColumnPrefix))
                {
                    m_customColumns.Add(key.Replace(customColumnPrefix, ""), ConfigurationManager.AppSettings[key]);
                }
            }
        }
    }
}
