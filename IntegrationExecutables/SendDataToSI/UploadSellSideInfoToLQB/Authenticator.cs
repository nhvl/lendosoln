﻿using System;
using System.Windows.Forms;
using UploadSellSideInfoToLQB.AuthSvc;

namespace UploadSellSideInfoToLQB
{
    public class Authenticator
    {
        #region Variables
        private AuthService m_authService;
        private bool m_bIsAuthenticated = false;

        public bool Authenticated
        {
            get { return m_bIsAuthenticated; }
        }
        #endregion

        public string Authenticate()
        {
            m_authService = ServiceProvider.AuthenticationService();
            
            return GetTicket();
        }

        private string GetTicket()
        {
            string sTicket = null;

            try
            {
                sTicket = m_authService.GetUserAuthTicket(Settings.LOGIN.Trim(), Settings.PASSWORD.Trim());
                m_bIsAuthenticated = true;
            }
            catch (Exception)
            {
                MessageBox.Show("Authentication failed. Aborting upload. Please make sure that the config file contains your current login credentials and retry.", Settings.EXE_NAME, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return sTicket;
        }
    }
}
