﻿namespace ScheduleExecutable.SFTPXsltExport
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using DataAccess;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.XsltExportReport;
    using WinSCP;

    public class SFTPXsltExportCallBack : IXsltExportCallBack
    {
        #region IXsltExportCallBack Members

        public void OperationComplete(Guid brokerId, XsltExportRequest request, XsltExportResult result)
        {

            if (result.HasError)
            {
                Tools.LogErrorWithCriticalTracking("Report Id: " + result.ReportId + " of " + result.ReportName + " failed. Search in PB Log for detail message.");
                return;
            }

            string hostName = request.GetCallBackParameter("HostName");
            string login = request.GetCallBackParameter("Login");
            string password = request.GetCallBackParameter("Password");
            string destPath = request.GetCallBackParameter("DestPath");// "/home/capmkts/confirm_files/LQB/";
            string localPath = result.OutputFileLocation;
            string usenet = request.GetCallBackParameter("UseNet"); // 12/28/2015 - dd - Determine if we should use FTP in .NET framework.
            string usePassive = request.GetCallBackParameter("UsePassive"); // 7/5/2016 - dd - Have a way to specify FTP.net to use non passive transfer.

            if (usenet == "True")
            {
                UploadFile(hostName, login, password, localPath, destPath, usePassive);
            }
            else
            {
                UploadFileUsingWinSCP(hostName, login, password, localPath, destPath, request);
            }
        }

        #endregion

        private Protocol GetProtocol(string value)
        {
            if (value.Equals("sftp", StringComparison.OrdinalIgnoreCase))
            {
                return Protocol.Sftp;
            }
            else if (value.Equals("ftp", StringComparison.OrdinalIgnoreCase))
            {
                return Protocol.Ftp;
            }
            else if (value.Equals("scp", StringComparison.OrdinalIgnoreCase))
            {
                return Protocol.Scp;
            }
            else
            {
                throw new CBaseException("Unsupport protocol=[" + value + "]", "Unsupport protocol=[" + value + "]");
            }
        }

        private void UploadFileUsingWinSCP(string ftpHostName, string login, string password, string localFilePath, string remoteFilePath, XsltExportRequest request)
        {
            Protocol protocol = GetProtocol(request.GetCallBackParameter("Protocol"));
            int portNumber = int.Parse(request.GetCallBackParameter("Port"));
            string sshHostKeyFingerPrint = request.GetCallBackParameter("SshFingerPrint");// "ssh-rsa 2048 56:4f:e7:3f:2b:ee:06:d9:5d:14:f7:b5:38:78:e9:13";
            string ftpSecure = request.GetCallBackParameter("FtpSecure"); // Valid values are : Implicit, ExplicitSsl, ExplicitTls
            string sslHostFingerPrint = request.GetCallBackParameter("SslHostFingerPrint");

            UploadFileUsingWinSCPImpl(ftpHostName, login, password, localFilePath, remoteFilePath, protocol, portNumber, sshHostKeyFingerPrint, ftpSecure, sslHostFingerPrint);
        }

        public static void UploadFileUsingWinSCP(string ftpHostName, string login, string password, string localFilePath, string remoteFilePath, Protocol protocol, int portNumber, string sshHostKeyFingerPrint, string ftpSecure, string sslHostFingerPrint, bool disableResumeSupport = false)
        {
            UploadFileUsingWinSCPImpl(ftpHostName, login, password, localFilePath, remoteFilePath, protocol, portNumber, sshHostKeyFingerPrint, ftpSecure, sslHostFingerPrint, disableResumeSupport);
        }

        private static void UploadFileUsingWinSCPImpl(string ftpHostName, string login, string password, string localFilePath, string remoteFilePath, Protocol protocol, int portNumber, string sshHostKeyFingerPrint, string ftpSecure, string sslHostFingerPrint, bool disableResumeSupport = false)
        {
            try
            {
                SessionOptions sessionOptions = new SessionOptions
                {
                    Protocol = protocol,
                    HostName = ftpHostName,
                    UserName = login,
                    Password = password,
                    PortNumber = portNumber,
                    //SshHostKeyFingerprint = sshHostKeyFingerPrint
                };
                if (string.IsNullOrEmpty(sshHostKeyFingerPrint) == false)
                {
                    sessionOptions.SshHostKeyFingerprint = sshHostKeyFingerPrint;
                }

                if (string.IsNullOrEmpty(ftpSecure) == false)
                {
                    if (ftpSecure == "Implicit")
                    {
                        sessionOptions.FtpSecure = FtpSecure.Implicit;
                    }
                    else if (ftpSecure == "ExplicitSsl")
                    {
                        sessionOptions.FtpSecure = FtpSecure.Explicit;
                    }
                    else if (ftpSecure == "ExplicitTls")
                    {
                        sessionOptions.FtpSecure = FtpSecure.Explicit;
                    }
                }

                if (string.IsNullOrEmpty(sslHostFingerPrint) == false)
                {
                    sessionOptions.TlsHostCertificateFingerprint = sslHostFingerPrint;
                }

                using (Session session = new Session())
                {
                    //Connect
                    session.Open(sessionOptions);

                    // Upload files
                    TransferOptions transferOptions = new TransferOptions();
                    transferOptions.TransferMode = TransferMode.Automatic;
                    transferOptions.PreserveTimestamp = false;

                    if (disableResumeSupport)
                    {
                        TransferResumeSupport resumeSupport = new TransferResumeSupport
                        {
                            State = TransferResumeSupportState.Off
                        };

                        transferOptions.ResumeSupport = resumeSupport;
                    }

                    TransferOperationResult transferResult;
                    transferResult = session.PutFiles(localFilePath, remoteFilePath, false, transferOptions);

                    // Throw on any error
                    transferResult.Check();
                }
            }
            catch (Exception exc)
            {
                Tools.LogErrorWithCriticalTracking("Unable to submit SFTP to " + ftpHostName + ". UserName=" + login, exc);
                throw;
            }
        }

        public static void UploadFile(string ftpHostName, string login, string password, string localFilePath, string remoteFilePath, string usePassive)
        {
            try
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://" + ftpHostName + "/" + remoteFilePath);

                request.Method = WebRequestMethods.Ftp.UploadFile;

                request.Credentials = new NetworkCredential(login, password);

                if (usePassive == "False")
                {
                    request.UsePassive = false;
                }
                else if (usePassive == "True")
                {
                    request.UsePassive = true;
                }
                byte[] fileContent = BinaryFileHelper.ReadAllBytes(localFilePath);

                request.ContentLength = fileContent.Length;

                string statusDescription = string.Empty;
                FtpStatusCode statusCode;
                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(fileContent, 0, fileContent.Length);
                    requestStream.Close();

                    using (FtpWebResponse response = (FtpWebResponse)request.GetResponse())
                    {
                        statusDescription = response.StatusDescription;
                        statusCode = response.StatusCode;
                    }
                }
                Tools.LogInfo("FtpUpload", "Host:[" + ftpHostName + "], login:[" + login + "]. RemotePath:[" + remoteFilePath + ", StatusCode:[" + statusCode + "]. Status:[" + statusDescription + "]");
            }
            catch (Exception exc)
            {
                Tools.LogError("FtpUpload Host:[" + ftpHostName + "], login:[" + login + "]", exc);
                throw;
            }
        }
    }
}
