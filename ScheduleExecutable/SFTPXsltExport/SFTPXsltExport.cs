﻿namespace ScheduleExecutable.SFTPXsltExport
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.XsltExportReport;
    using LqbGrammar.DataTypes;

    class SFTPXsltExport
    {
        /// <summary>
        /// Note: the xml attribute "password" is actually an encrypted password.
        /// </summary>
        public static void Execute(string[] args)
        {
            string exportName = args[1];

            string settingsFile = "SFTPXsltExportSettings.xml";

            XDocument xdoc = XDocument.Load(settingsFile);

            foreach (XElement el in xdoc.Root.Elements("item"))
            {
                string id = SafeString(el, "id");
                if (id.Equals(exportName, StringComparison.OrdinalIgnoreCase) == false)
                {
                    continue;
                }

                ExportFromXElement(el, exportName, brokerId: null);

                return;
            }
        }

        public static void ExportFromXElement(XElement el, string exportName, Guid? brokerId)
        {
            string mapName = SafeString(el, "map_name");
            Guid userId = SafeGuid(el, "userid");
            Guid reportId = SafeGuid(el, "reportid");
            string protocol = SafeString(el, "protocol");
            string hostName = SafeString(el, "host_name");
            string login = SafeString(el, "login");
            string password = EncryptionHelper.Decrypt(SafeString(el, "password"));
            string port = SafeString(el, "port");
            string sshFingerPrint = SafeString(el, "ssh_finger_print");
            string destPath = SafeString(el, "dest_path").Replace("{DATE}", DateTime.Now.ToString("MMddyyyy")).Replace("{TIME}", DateTime.Now.ToString("HHmmss"));
            string useNet = SafeString(el, "use_net");
            string ftpSecure = SafeString(el, "ftp_secure");
            string sslHostFingerPrint = SafeString(el, "ssl_host_finger_print");
            string usePassive = SafeString(el, "use_passive");
            LqbAbsoluteUri? additionalPostUrl = LqbAbsoluteUri.Create(SafeString(el, "additional_post_url"));

            List<KeyValuePair<string, string>> parameterList = new List<KeyValuePair<string, string>>();
            parameterList.Add(new KeyValuePair<string, string>("Protocol", protocol));
            parameterList.Add(new KeyValuePair<string, string>("HostName", hostName));
            parameterList.Add(new KeyValuePair<string, string>("Login", login));
            parameterList.Add(new KeyValuePair<string, string>("Password", password));
            parameterList.Add(new KeyValuePair<string, string>("Port", port));
            parameterList.Add(new KeyValuePair<string, string>("SshFingerPrint", sshFingerPrint));
            parameterList.Add(new KeyValuePair<string, string>("DestPath", destPath));
            parameterList.Add(new KeyValuePair<string, string>("UseNet", useNet));
            parameterList.Add(new KeyValuePair<string, string>("FtpSecure", ftpSecure));
            parameterList.Add(new KeyValuePair<string, string>("SslHostFingerPrint", sslHostFingerPrint));
            parameterList.Add(new KeyValuePair<string, string>("UsePassive", usePassive));
            parameterList.Add(new KeyValuePair<string, string>("url", additionalPostUrl.ToString()));

            if (!brokerId.HasValue)
            {
                Guid tempBrokerId = Guid.Empty;
                DbConnectionInfo.GetConnectionInfoByUserId(userId, out tempBrokerId);
                brokerId = tempBrokerId;
            }

            SFTPXsltExportCallBack callBack = new SFTPXsltExportCallBack();

            var xsltRequest = XsltExportRequest.CreateFromCustomReport(brokerId.Value, userId, reportId, mapName, callBack, parameterList);

            XsltExportResult result = XsltExport.ExecuteSynchronously(xsltRequest);

            if (additionalPostUrl.HasValue)
            {
                IXsltExportCallBack postCallBack = new HttpPostXsltExportCallback();
                postCallBack.OperationComplete(brokerId.Value, xsltRequest, result);
            }
        }

        private static string SafeString(XElement el, string attrName)
        {
            XAttribute attr = el.Attribute(attrName);
            if (attr != null)
            {
                return attr.Value;
            }
            return string.Empty;
        }
        private static Guid SafeGuid(XElement el, string attrName)
        {
            string v = SafeString(el, attrName);

            if (string.IsNullOrEmpty(v) == false)
            {
                try
                {
                    return new Guid(v);
                }
                catch (FormatException)
                {
                }
            }
            return Guid.Empty;
        }
    }


}
