﻿namespace ScheduleExecutable
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Xml.Linq;
    using DataAccess;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.XsltExportReport;

    class TemplateEmailXsltExport
    {
        /// <summary>
        /// Execute all member notification for FirstTech
        /// </summary>
        /// <param name="args"></param>
        public static void ExecuteMemberNotificationForPML0223(string[] args)
        {
            string[] notificationEmailList = {
                                             "TemplateEmail_Abandoned.xml",
                                             "TemplateEmail_AppraisalReceived.xml",
                                             "TemplateEmail_ApprovedDate.xml",
                                             "TemplateEmail_ClearToClose.xml",
                                             "TemplateEmail_Closed.xml",
                                             "TemplateEmail_Opened.xml",
                                             "TemplateEmail_RateLocked.xml",
                                             //"TemplateEmail_EstClosed.xml" // 11/15/2013 dd - Temporary disable per OPM 144759

                                             //"TemplateEmail_Production_Abandoned.xml",
                                             //"TemplateEmail_Production_AppraisalReceived.xml",
                                             //"TemplateEmail_Production_ApprovedDate.xml",
                                             //"TemplateEmail_Production_ClearToClose.xml",
                                             //"TemplateEmail_Production_Closed.xml",
                                             //"TemplateEmail_Production_Opened.xml",
                                             //"TemplateEmail_Production_RateLocked.xml",
                                             //"TemplateEmail_Production_EstClosed.xml"
                                         };
            string rootPath = args[1];
            StringBuilder debugMessage = new StringBuilder();

            try
            {

                //string TemplateEmail_EstClosed_Processed_File = "TemplateEmail_EstClosed_Processed.xml";
                foreach (string notificationEmail in notificationEmailList)
                {
                    debugMessage.AppendLine("Process " + notificationEmail);
                    HashSet<Guid> excludeList = new HashSet<Guid>();

                    List<KeyValuePair<Guid, DateTime>> processedList = new List<KeyValuePair<Guid,DateTime>>();

                    #region Load up previous processed loan
                    string processedFile = notificationEmail.Replace(".xml", "_Processed.xml");
                    if (FileOperationHelper.Exists(processedFile))
                    {
                        XDocument xdoc = XDocument.Load(processedFile);

                        processedList = new List<KeyValuePair<Guid, DateTime>>();

                        foreach (XElement el in xdoc.Root.Elements())
                        {
                            Guid sLId = new Guid(el.Attribute("sLId").Value);
                            DateTime dt = DateTime.Parse(el.Attribute("dt").Value);

                            if (dt.AddMonths(3) > DateTime.Today)
                            {
                                // 7/2/2013 dd - Only include in exclude list loan that processes less than 3 months.
                                // Assume that Estimate Closing Date will not be 3 months in future.

                                excludeList.Add(sLId);

                                processedList.Add(new KeyValuePair<Guid, DateTime>(sLId, dt));
                            }
                        }
                    }
                    #endregion

                    XsltExportResult result = ExecuteImpl(rootPath + "\\" + notificationEmail, excludeList);
                    debugMessage.AppendLine("    HasError=" + result.HasError + ", Count: " + result.LoanIdList.Count());

                    // 7/2/2013 dd - For Estimate Close Report we need to keep track a list of loan 
                    // that already processed.

                    if (result.HasError == false)
                    {
                        foreach (Guid sLId in result.LoanIdList)
                        {
                            processedList.Add(new KeyValuePair<Guid, DateTime>(sLId, DateTime.Today));
                        }

                        // Rewrite the processed list.
                        XDocument xdoc = new XDocument();
                        xdoc.Add(new XElement("root"));

                        foreach (var o in processedList)
                        {
                            xdoc.Root.Add(new XElement("item", new XAttribute("sLId", o.Key.ToString()),
                                new XAttribute("dt", o.Value.ToString("MM/dd/yyyy"))));
                        }
                        xdoc.Save(processedFile);
                    }

                }
            }
            catch (Exception exc)
            {
                debugMessage.AppendLine(exc.ToString());
                throw;
            }
            finally
            {
                Tools.LogInfo(debugMessage.ToString());
            }
        }
        /// <summary>
        /// Execute the Batch Xslt Export and then send email using templating.
        /// </summary>
        /// <param name="args"></param>
        public static void Execute(string[] args)
        {
            string settingsFile = args[1];

            ExecuteImpl(settingsFile, null);
        }

        private static XsltExportResult ExecuteImpl(string settingsFile, HashSet<Guid> excludeList)
        {
            XDocument xdoc = XDocument.Load(settingsFile);

            XElement settingsElement = xdoc.Root.Element("settings");

            string mapName = SafeString(settingsElement, "map_name");
            Guid userId = SafeGuid(settingsElement, "userid");
            Guid reportId = SafeGuid(settingsElement, "reportid");

            string templateEmailXml = xdoc.Root.Element("email").ToString();

            EmailTemplatingXsltExportCallBack callBack = new EmailTemplatingXsltExportCallBack();

            List<KeyValuePair<string, string>> parameterList = new List<KeyValuePair<string, string>>();
            parameterList.Add(new KeyValuePair<string, string>("TemplateEmailXml", templateEmailXml));

            Guid brokerId = Guid.Empty;

            DbConnectionInfo.GetConnectionInfoByUserId(userId, out brokerId);
            var xsltRequest = XsltExportRequest.CreateFromCustomReport(brokerId, userId, reportId, mapName, callBack, parameterList, excludeList);

            XsltExportResult result = XsltExport.ExecuteSynchronously(xsltRequest);
            return result;
        }
        private static string SafeString(XElement el, string attrName)
        {
            XAttribute attr = el.Attribute(attrName);
            if (attr != null)
            {
                return attr.Value;
            }
            return string.Empty;
        }
        private static Guid SafeGuid(XElement el, string attrName)
        {
            string v = SafeString(el, attrName);

            if (string.IsNullOrEmpty(v) == false)
            {
                try
                {
                    return new Guid(v);
                }
                catch (FormatException)
                {
                }
            }
            return Guid.Empty;
        }
    }
}
