﻿namespace ScheduleExecutable
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading;
    using DataAccess;
    using EDocs;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.Drivers.SqlServerDB;
    using LqbGrammar.DataTypes;

    public sealed class EDocBrokerExport
    {

        class EDocInfo
        {
            public string LoanName { get; private set; }
            public Guid DocumentId { get; private set; }
            public string LastName { get; private set; }
            public string DocType { get; private set; }
            public string Folder { get; private set; }
            public string FileDBKey { get; private set; }
            public string PathFileName { get; set; }
            public string PathFolderName { get; set; }

            public EDocInfo(IDataReader reader)
            {
                LoanName = reader["slnm"].ToString();
                Folder = reader["FolderName"].ToString();
                DocType = reader["DocTypeName"].ToString();
                LastName = reader["ablastnm"].ToString();
                DocumentId = (Guid)reader["DocumentId"];
                FileDBKey = reader["fileDbKey_CurrentRevision"].ToString();

            }
        }

        private Guid m_brokerId = Guid.Empty;
        private const string LogPrefixEDocBrokerExport = "EDocExportLog_Broker_";
        private string m_EdocFolderExport;  
        private string m_sLogFile;
        private string m_sErrorFile; 
        private HashSet<Guid> m_exportedDocs = new HashSet<Guid>();
        private List<EDocInfo> m_docsToExport = new List<EDocInfo>();
        private Dictionary<string, int> m_fileNameCount = new Dictionary<string,int>(StringComparer.OrdinalIgnoreCase);
        private Random r = new Random();

        public EDocBrokerExport(Guid brokerId)
        {
            m_brokerId = brokerId;
            m_sLogFile = LogPrefixEDocBrokerExport + brokerId.ToString("N") + ".txt"; 
            m_EdocFolderExport = "EDocFolderExport_" + brokerId.ToString("N");
            m_sErrorFile = "EDocFolderExport_" + brokerId.ToString("N") + "_error.txt";

            if (!Directory.Exists(m_EdocFolderExport))
            {
                Directory.CreateDirectory(m_EdocFolderExport);
            }
        }

        private string GetFileName(EDocInfo info)
        {
            return CleanFileName(info.Folder + "_" + info.DocType);
        }

        public void Export()
        {
            #region see if we already exported any docs.
            System.Console.WriteLine("Checking log file...");
            if (FileOperationHelper.Exists(m_sLogFile))
            {
                foreach (string line in TextFileHelper.ReadLines(this.m_sLogFile))
                {
                    this.m_exportedDocs.Add(new Guid(line));
                }
            }
            #endregion


            #region load docs for export
            System.Console.WriteLine("Loading All Docs For Broker To Memory..");
            PolulateExportList();
            #endregion

            #region
            HashSet<string> folderNames = new HashSet<string>();
            int migrationCount = 0;
            System.Console.WriteLine("Beginning copy process...");
            long timeTaken = 0;
            foreach (EDocInfo edoc in m_docsToExport)
            {
                #region create folder if it does not exist
                if (!folderNames.Contains(edoc.PathFolderName))
                {
                    folderNames.Add(edoc.PathFolderName);
                    if (!Directory.Exists(edoc.PathFolderName))
                    {
                        try
                        {
                            Directory.CreateDirectory(edoc.PathFolderName);
                        }

                        catch (IOException)
                        {
                            System.Console.WriteLine(edoc.PathFolderName);
                            throw;
                        }
                    }
                }
                #endregion


                timeTaken += Time(() => ExportDoc(edoc));
                migrationCount++;
                System.Console.Clear();
                long timeRemaining = (timeTaken / migrationCount) * (m_docsToExport.Count - migrationCount);
                System.Console.WriteLine("{0}/{1} copied. ETA: {2:0.00} minute(s).", migrationCount, m_docsToExport.Count, TimeSpan.FromMilliseconds(timeRemaining).TotalMinutes);
            }
            #endregion
        }

        private void PolulateExportList()
        {
            // Tested by copying code to the test module and removing the call to ProcessEDocInfo()

            string sql = @"select slnm, foldername, doctypename,  a.ablastnm, DocumentId, fileDbKey_CurrentRevision
                            from edocs_document e
	                        join loan_file_cache c on e.slid = c.slid 
	                        join application_a a on a.aappid = e.aappid
	                        join EDOCS_DOCUMENT_TYPE d on d.doctypeid = e.doctypeid 
	                        join edocs_folder f on f.folderid = d.folderid
                            where e.brokerid = @id and c.IsValid = 1";

            var listParams = new SqlParameter[] { new SqlParameter("@id", this.m_brokerId) };

            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                while (reader.Read())
                {
                    EDocInfo info = new EDocInfo(reader);
                    ProcessEDocInfo(info);
                }
            };

            DBSelectUtility.ProcessDBData(this.m_brokerId, sql, null, listParams, readHandler);
        }

        private void ProcessEDocInfo(EDocInfo info)
        {
            if (!m_exportedDocs.Contains(info.DocumentId))
            {
                string folderName = string.Format("{0}/{1}_{2}", m_EdocFolderExport, CleanFileName(info.LoanName.ToUpper()), CleanFileName(info.LastName.ToUpper()));
                string filename = string.Format("{0}/{1}", folderName, GetFileName(info));
                int number;
                if (m_fileNameCount.TryGetValue(filename, out number))
                {
                    number += 1;
                    m_fileNameCount[filename] = number;
                    filename += "_" + number;
                }
                else
                {
                    number = 0;
                    m_fileNameCount.Add(filename, number);
                }
                info.PathFileName = filename + ".pdf";
                info.PathFolderName = folderName;
                m_docsToExport.Add(info);
            }
        }

        public static long Time(Action action)
        {
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            sw.Reset();
            sw.Start();
            action();
            sw.Stop();
            return sw.ElapsedMilliseconds;
        }
        private void ExportDoc(EDocInfo edoc)
        {
            string path = "";
            try
            {
                path = FileDBTools.CreateCopy(E_FileDB.EDMS, edoc.FileDBKey);
            }
            catch (FileNotFoundException)
            {
                System.Console.WriteLine("File missing dbkey:{0} Loan Name: {1} DocId: {2}", edoc.FileDBKey, edoc.LoanName, edoc.DocumentId);
                TextFileHelper.AppendString(m_sErrorFile, "File missing dbkey:" + edoc.FileDBKey + " LoanName:" + edoc.LoanName + " DocID:" + edoc.DocumentId + Environment.NewLine);
                return; 
            }
            string updatedPath = path + "_ready.pdf";

            PDFPageIDManager pageId = new PDFPageIDManager(path); //we need to remove our png markers
            pageId.DeletePageIds();
            pageId.Save(updatedPath);


            try
            {
                FileOperationHelper.Move(updatedPath, edoc.PathFileName);
            }
            catch (IOException e)
            {
                if (e.Message.Contains("file exists"))
                {
                    edoc.PathFileName = edoc.PathFileName.Replace(".pdf", edoc.DocumentId.ToString("N") +".pdf" );
                    System.Console.WriteLine("{0} file already exist moving file to edoc pathfilename", edoc.PathFileName);
                    FileOperationHelper.Move(updatedPath, edoc.PathFileName);
                    //Regex r = new Regex("_([0-9]+).pdf");
                    //Match match;
                    //int num = 0;
                    //string searchPattern = edoc.FileName + "*.pdf";

                    //string[] files = Directory.GetFiles(folderName, searchPattern, SearchOption.AllDirectories);

                    //foreach (string file in files)
                    //{
                    //    match = r.Match(file);
                    //    if (match.Success)
                    //    {
                    //        var curNum = int.Parse(match.Groups[1].Value);
                    //        if (curNum > num)
                    //        {
                    //            num = curNum;
                    //        }
                    //    }
                    //}

                    //num += 1;

                    //FileOperationHelper.Move(updatedPath, string.Format("{0}/{1}_{2}.pdf", folderName, GetFileName(edoc), num));
                }
                else 
                {
                    throw;
                }
            }
            TextFileHelper.AppendString(m_sLogFile, edoc.DocumentId.ToString("N") + Environment.NewLine); //record the export.
            TextFileHelper.AppendString(m_sLogFile + "_idtopath.txt", edoc.DocumentId + " " + edoc.PathFileName + Environment.NewLine);
            FileOperationHelper.Delete(path);
        }

        private static string CleanFileName(string fileName)
        {
            return Path.GetInvalidFileNameChars().Aggregate(fileName, (current, c) => current.Replace(c.ToString(), string.Empty));
        }
    }
}
