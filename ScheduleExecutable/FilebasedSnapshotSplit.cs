﻿using DataAccess;
using LendersOffice.RatePrice.FileBasedPricing;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ScheduleExecutable
{
    class FilebasedSnapshotSplit
    {
        private class SnapshotInfo
        {
            public int  Id { get; set; }
            public string FileKey;
        }

        public static void Execute(string[] args)
        {
            if (args.Length > 2)
            {
                Console.WriteLine("Usage: ScheduleExecutable SnapshotSplit [snapshotId]");
                return;
            }

            if (args.Length == 1)
            {
                AutomateSnapshotSplit();
                return;
            }


            string snapshotId = args[1];
            int subSnapshotCount = FileBasedSnapshot.SnapshotSplit(snapshotId);

            Console.WriteLine($"Create {subSnapshotCount} subsnapshots from {snapshotId}");
        }

        private static void AutomateSnapshotSplit()
        {
            DateTime exitTime = DateTime.Now.AddMinutes(15);
            while(DateTime.Now <= exitTime)
            {
                SnapshotInfo snapshotInfo = GetUnsplitSnapshotInfo();
                if (snapshotInfo == null)
                {
                    Thread.Sleep(30 * 1000);
                    continue;
                }

                const int MaxTry = 3;
                for(int i=1; i <= MaxTry; i++)
                {
                    try
                    {
                        FileBasedSnapshot.SnapshotSplit(snapshotInfo.FileKey);
                        break;
                    }
                    catch( Exception exc)
                    {
                        if (IsCriticalException(exc) )
                        {
                            Tools.LogError($"SnapshotSplit({snapshotInfo.FileKey}): has critical exception.", exc);
                            return;
                        }

                        if( i== MaxTry)
                        {
                            Tools.LogError($"[SnapshotSplit_UnableToProcess] FileKey = {snapshotInfo.FileKey} : skip", exc);
                            break;
                        }
                        else
                        {
                            Thread.Sleep(5 * 1000);
                        }                        
                    }                    
                }

                UpdateSplitStatus(snapshotInfo);
                exitTime = DateTime.Now.AddMinutes(15);
            }            
        }

        private static SnapshotInfo GetUnsplitSnapshotInfo()
        {
            using (var reader = StoredProcedureHelper.ExecuteReaderWithRetries(DataSrc.LpeSrc, "LPE_FILE_KEY_LIST_GetUnsplitSnapshot", 3))
            {
                if (reader.Read())
                {
                    return new SnapshotInfo()
                    {
                        Id = (int)reader["Id"],
                        FileKey = (string)reader["FileKey"]
                    };
                }

               return null;
            }
        }

        private static void UpdateSplitStatus(SnapshotInfo snapshotInfo)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@Id", snapshotInfo.Id),
                new SqlParameter("@FileKey", snapshotInfo.FileKey),
            };

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LpeSrc, "LPE_FILE_KEY_LIST_UpdateSplitStatus", 3, parameters);
        }

        private static bool IsCriticalException(Exception exc)
        {
            try
            {
                while( exc != null)
                {
                    if ( exc is System.OutOfMemoryException || exc is System.TypeInitializationException || 
                         exc.Message.IndexOf("There is not enough space on the disk") >= 0)
                    {
                        return true;
                    }

                    exc = exc.InnerException;
                }
            }
            catch
            {                
            }

            return false;
        }
    }
}
