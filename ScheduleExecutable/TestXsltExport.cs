﻿// <copyright file="TestXsltExport.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: David Dao
//    Date:   8/21/2014 4:55:26 PM 
// </summary>
namespace ScheduleExecutable
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Xml.Linq;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.XsltExportReport;

    /// <summary>
    ///
    /// </summary>
    class TestXsltExport
    {
        public static void Execute(string[] args)
        {
            string map = args[1];
            Console.WriteLine("Execute XsltExport: Map=[" + map + "]");
            if (FileOperationHelper.Exists("XsltTest.xml") == false)
            {
                Console.WriteLine("XsltTest.xml is not found.");
                return;
            }
            XDocument xdoc = XDocument.Load("XsltTest.xml");

            Guid userId = new Guid(xdoc.Root.Attribute("userid").Value);

            List<Guid> loanIdList = new List<Guid>();
            foreach (XElement el in xdoc.Root.Elements("loan"))
            {
                loanIdList.Add(new Guid(el.Attribute("sLId").Value));
            }

            Console.WriteLine("Execute using UserId=" + userId);

            XsltExportRequest request = new XsltExportRequest(userId, loanIdList, map, null, null);

            XsltExportWorker worker = new XsltExportWorker(request, false);

            XsltExportResult result = worker.Execute();

            Console.WriteLine("Complete.");
            if (result.HasError)
            {
                foreach (var str in result.ErrorList)
                {
                    Console.WriteLine("Error: " + str);
                }
            }
            else
            {
                Console.WriteLine("Output File at: " + result.OutputFileLocation);
            }
        }

    }
}