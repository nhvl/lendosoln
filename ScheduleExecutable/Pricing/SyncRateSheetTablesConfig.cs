﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleExecutable.Pricing
{
    /// <summary>
    /// Represents the settings for sync rate sheets. This is use for Json serialization. Do not add logic.
    /// </summary>
    public class SyncRateSheetTablesConfig
    {
        /// <summary>
        /// Gets or sets the host name to upload pricing snapshot to.
        /// </summary>
        public string Host { get; set; }

        /// <summary>
        /// Gets or sets the secret key.
        /// </summary>
        public string SecretKey { get; set; }

        /// <summary>
        /// Gets or sets the last uploaded date.
        /// </summary>
        public DateTime LastUpdated { get; set; }
    }
}
