﻿namespace ScheduleExecutable.Pricing
{
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.DistributeUnderwriting;
    using LendersOffice.ObjLib.Resource;
    using LendersOfficeApp.los.RatePrice;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    public class StandaloneRunner
    {
        private int secondsToSleepInBetweenIdleRuns { get; set; }
        private LpeTaskReceiver taskReceiver;
        private Action<string> logger; 

        public StandaloneRunner(int secondsToSleepInBetweenIdleRuns, Action<string> logger)
        {
            this.secondsToSleepInBetweenIdleRuns = secondsToSleepInBetweenIdleRuns;
            this.taskReceiver = new LpeTaskReceiver();
            this.logger = logger;
        }


        public void Run(CancellationToken cancelToken)
        {
            while (!cancelToken.IsCancellationRequested)
            {
                try
                {
                    var pricingTask = taskReceiver.GetNextTask();

                    if (pricingTask == null)
                    {
                        var delay = Task.Delay(TimeSpan.FromSeconds(this.secondsToSleepInBetweenIdleRuns), cancelToken);
                        var continueWith = delay.ContinueWith(task => { });
                        continueWith.Wait();
                        continue;
                    }

                    DateTime started = DateTime.Now;
                    long timeTaken = Tools.Time(() => this.taskReceiver.RunTask(pricingTask));
                    var time = Math.Round(timeTaken / 1000M, 2);
                    logger($"Running pricing {Tools.ShortenGuid(pricingTask.RequestData.LogId)} {pricingTask.Header.LpeTaskT} for {pricingTask.Header.UserName} with {pricingTask.RequestData.ProductIdList.Count} products. Time Started: {started.ToString("H:mm:ss")} Ended: {DateTime.Now.ToString("H:mm:ss")} Taken: {time} seconds. ");
                }
                catch (Exception e)
                {
                    logger(e.ToString());
                }
            }

            logger("Cancellation requested.");
        }


        public static void Execute(string[] args)
        {
            bool isDev = args.Any(p => p.ToLower().Equals("development"));
            Action<string> logger = (s) => { };
            int parallelRuns;

            string threadCount = args.FirstOrDefault(p => p.ToLower().StartsWith("-t"));

            if (string.IsNullOrEmpty(threadCount))
            {
                parallelRuns = Environment.ProcessorCount * 2;
            }
            else
            {
                parallelRuns = int.Parse(threadCount.Substring(2));
            }

        
            if (args.Any(p => p.ToLower().Equals("log")))
            {
                logger = Console.WriteLine;
            }

            logger($"Running on {parallelRuns} threads. MaximumConcurrencyLevel Set: {TaskScheduler.Default.MaximumConcurrencyLevel}");

            var cancelToken = new CancellationTokenSource();

            Console.CancelKeyPress += (sender, eArgs) => {
                logger($"Cancel was received triggering a app reload on next run finish.");
                cancelToken.Cancel();
                eArgs.Cancel = true;
            };

            FileSystemWatcher watcher = new FileSystemWatcher();
            watcher.Path = System.IO.Directory.GetCurrentDirectory();
            watcher.NotifyFilter = NotifyFilters.LastWrite;
            watcher.Filter = "*.dll*";
            watcher.Changed += (sender, fArgs) =>
            {
                logger($"Changes detected in {fArgs.Name} triggering reload on run finish.");
                cancelToken.Cancel();
                watcher.EnableRaisingEvents = false;
            };

            watcher.EnableRaisingEvents = true;

            if (!ResourceManager.Instance.AreGenericResourcesAvailable())
            {
                logger("Error: Could not find PmlShared/PmlCommon");
                EmailUtilities.SendToCritical($"{ConstAppDavid.ServerName} LQB Is Not Configured Correctly", "PmlShared or PmlCommon cannot be resolved.");
            }

            Tools.EnsurePdfRasterizerIsConfiguredAndRunning(validateLicense: !isDev);
            DistributeUnderwritingSettings.OverrideCalcMode(E_ServerT.CalcOnly);

            logger("Press ctrl-c to terminate gracefully.");

            if (args.Any(p=>p.Equals("debug", StringComparison.OrdinalIgnoreCase)))
            {
                System.Diagnostics.Debugger.Launch();
            }

            //Load Snapshot Data -- earlier the better.
            LpeDataProvider.StartUp();

            // Trigger slow app start code.
            CFieldInfoTable.GetInstance();

            logger("Startup Prep Complete.");

            List<Task> tasks = new List<Task>();
            for (int i = 0; i < parallelRuns;  i++)
            {
                var task = Task.Factory.StartNew(() => {
                    StandaloneRunner runner = new StandaloneRunner(1, logger);
                    runner.Run(cancelToken.Token);
                },  cancelToken.Token);

                tasks.Add(task);
            }

            Task.WaitAll(tasks.ToArray()); 

            // The app will not terminate if there are any waitone events.
            LoadLoanProgramOnDemand.TriggerExit();
        }
    }
}
