﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Net;
using System.Threading;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Drivers.FileSystem;
using LendersOffice.RatePrice.Model;
using LqbGrammar.DataTypes;

namespace ScheduleExecutable.Pricing
{
    /// <summary>
    /// Sync INVESTOR_XLS_FILE, LPE_ACCEPTABLE_RS_FILE, LPE_ACCEPTABLE_RS_FILE_VERSION tables.
    /// </summary>
    public static class SyncRatesheetTables
    {
        /// <summary>
        /// dd 8/24/2017 - Until we can isolate and extract the functionality of RateSheet database, we need
        /// to have a way that upload any changes in INVESTOR_XLS_FILE, LPE_ACCEPTABLE_RS_FILE, LPE_ACCEPTABLE_RS_FILE_VERSION in prodution to
        /// other environment (i.e AWS). These tables are use to determine if pricing should be expire. Therefore it need to be update within minute
        /// or two. This method will get a list of recent changes and upload to approach end point to perform sync.
        /// 
        /// </summary>
        /// <param name="args"></param>
        public static void Execute(string[] args)
        {
            if (args.Length != 2)
            {
                Console.WriteLine("Usage: ScheduleExecutable.exe SyncRatesheetTables {config.json}");
                return;
            }

            var path = LocalFilePath.Create(args[1]);
            var configJson = TextFileHelper.ReadFile(path.Value);
            var config = SerializationHelper.JsonNetDeserialize<SyncRateSheetTablesConfig>(configJson);

            string endPoint = config.Host;
            string password = config.SecretKey;

            DateTime lastCheckedDate = config.LastUpdated == DateTime.MinValue ? DateTime.Now.AddDays(-1) : config.LastUpdated;

            int numberOfNoModification = 0;

            DateTime investorXlsFileLastCheckedDate = lastCheckedDate;
            DateTime lpeAcceptableRsFileLastCheckedDate = lastCheckedDate;
            DateTime lpeAcceptableRsFileVersionLastCheckedDate = lastCheckedDate;

            while (true)
            {
                Stopwatch gatherDeltaStopwatch = Stopwatch.StartNew();
                RatesheetDatabaseSyncRequest syncRequest = new RatesheetDatabaseSyncRequest();
                syncRequest.InvestorXlsFileList = new List<InvestorXlsFile>();
                syncRequest.LpeAcceptableRsFileList = new List<LpeAcceptableRsFile>();
                syncRequest.LpeAcceptableRsFileVersionList = new List<LpeAcceptableRsFileVersion>();

                investorXlsFileLastCheckedDate = ListRecentModify(syncRequest.InvestorXlsFileList, investorXlsFileLastCheckedDate);
                lpeAcceptableRsFileLastCheckedDate = ListRecentModify(syncRequest.LpeAcceptableRsFileList, lpeAcceptableRsFileLastCheckedDate);
                lpeAcceptableRsFileVersionLastCheckedDate = ListRecentModify(syncRequest.LpeAcceptableRsFileVersionList, lpeAcceptableRsFileVersionLastCheckedDate);
                gatherDeltaStopwatch.Stop();

                if (syncRequest.InvestorXlsFileList.Count == 0 && syncRequest.LpeAcceptableRsFileList.Count == 0 && syncRequest.LpeAcceptableRsFileVersionList.Count == 0)
                {
                    numberOfNoModification++;
                    // No modified record. Take a break from checking if last 5 checks return zero result.
                    if (numberOfNoModification >= 5)
                    {
                        return;
                    }
                    Thread.Sleep(30000); // Sleep for 30 seconds and try again
                    continue;
                }
                numberOfNoModification = 0; // Reset no result count.
                string json = SerializationHelper.JsonNetSerialize(syncRequest);

                Stopwatch uploadStopwatch = Stopwatch.StartNew();
                Upload(endPoint, password, json);
                uploadStopwatch.Stop();

                // 2019-02-07 - dd - Use the minimum date;
                lastCheckedDate = investorXlsFileLastCheckedDate < lpeAcceptableRsFileLastCheckedDate ? investorXlsFileLastCheckedDate : lpeAcceptableRsFileLastCheckedDate;
                lastCheckedDate = lastCheckedDate < lpeAcceptableRsFileVersionLastCheckedDate ? lastCheckedDate : lpeAcceptableRsFileVersionLastCheckedDate;

                config.LastUpdated = lastCheckedDate;

                string updatedJson = SerializationHelper.JsonNetSerialize<SyncRateSheetTablesConfig>(config);
                TextFileHelper.WriteString(path.Value, updatedJson, false);
                Tools.LogInfo("SyncRatesheetTables", $"Synced InvestorXslFileList={syncRequest.InvestorXlsFileList.Count} LastCheckedDate={investorXlsFileLastCheckedDate}, " +
                                                     $"LpeAcceptableRsFileList={syncRequest.LpeAcceptableRsFileList.Count} LastCheckedDate={lpeAcceptableRsFileLastCheckedDate}, " +
                                                     $"LpeAcceptableRsFileVersionList={syncRequest.LpeAcceptableRsFileVersionList.Count} LastCheckedDate={lpeAcceptableRsFileVersionLastCheckedDate}. Gather Time={gatherDeltaStopwatch.ElapsedMilliseconds:0,0}ms, " +
                                                     $"Upload Time={uploadStopwatch.ElapsedMilliseconds:0,0}ms");

                Thread.Sleep(30000); // Let sleep for 30 seconds before check again.

            }
        }

        private static void Upload(string endPoint, string password, string json)
        {
            string sb = string.Empty;
            Exception lastException = null;

            for (int i = 0; i < 3; i++)
            {
                try
                {
                    using (var webClient = new WebClient())
                    {
                        webClient.Headers.Add("x-lqb-secret-key", password);
                        string url = endPoint + "/uploadlpe.aspx?op=SyncRateSheetTables";
                        webClient.UploadString(url, json);
                        return;
                    }
                }
                catch (WebException exc)
                {
                    // Retries few times.
                    sb = exc.ToString();
                    lastException = exc;
                }
            }
            Console.WriteLine(sb);
            throw lastException;
        }
        private static DateTime ListRecentModify(List<InvestorXlsFile> list, DateTime lastCheckedDate)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@ModifiedD", lastCheckedDate)
            };

            DateTime lastModifiedDate = lastCheckedDate;

            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.RateSheet, "INVESTOR_XLS_FILE_ListModifiedSince", parameters))
            {
                while (reader.Read())
                {
                    InvestorXlsFile record = new InvestorXlsFile();
                    record.EffectiveDateTimeColumnOffset = (int)reader["EffectiveDateTimeColumnOffset"];
                    record.EffectiveDateTimeLandMarkText = (string)reader["EffectiveDateTimeLandMarkText"];
                    record.EffectiveDateTimeRowOffset = (int)reader["EffectiveDateTimeRowOffset"];
                    record.EffectiveDateTimeWorksheetName = (string)reader["EffectiveDateTimeWorksheetName"];
                    record.FileVersionDateTimeInUtc = ((DateTime)reader["FileVersionDateTime"]).ToUniversalTime();
                    record.InvestorXlsFileId = (long)reader["InvestorXlsFileId"];
                    record.InvestorXlsFileName = (string)reader["InvestorXlsFileName"];

                    DateTime modifiedDate = (DateTime)reader["ModifiedD"];

                    if (modifiedDate > lastModifiedDate)
                    {
                        lastModifiedDate = modifiedDate;
                    }

                    list.Add(record);
                }
            }

            return lastModifiedDate;
        }

        private static DateTime ListRecentModify(List<LpeAcceptableRsFile> list, DateTime lastCheckedDate)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@ModifiedD", lastCheckedDate)
            };

            DateTime lastModifiedDate = lastCheckedDate;
            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.RateSheet, "LPE_ACCEPTABLE_RS_FILE_ListModifiedSince", parameters))
            {
                while (reader.Read())
                {
                    LpeAcceptableRsFile record = new LpeAcceptableRsFile();
                    record.DeploymentType = (string)reader["DeploymentType"];
                    record.IsBothRsMapAndBotWorking = (bool)reader["IsBothRsMapAndBotWorking"];
                    record.InvestorXlsFileId = (long)reader["InvestorXlsFileId"];
                    record.LpeAcceptableRsFileId = (string)reader["LpeAcceptableRsFileId"];

                    DateTime modifiedDate = (DateTime)reader["ModifiedD"];

                    if (modifiedDate > lastModifiedDate)
                    {
                        lastModifiedDate = modifiedDate;
                    }

                    list.Add(record);
                }
            }

            return lastModifiedDate;
        }

        private static DateTime ListRecentModify(List<LpeAcceptableRsFileVersion> list, DateTime lastCheckedDate)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@ModifiedD", lastCheckedDate)
            };

            DateTime lastModifiedDate = lastCheckedDate;
            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.RateSheet, "LPE_ACCEPTABLE_RS_FILE_VERSION_ListModifiedSince", parameters))
            {
                while (reader.Read())
                {
                    LpeAcceptableRsFileVersion record = new LpeAcceptableRsFileVersion();
                    record.EntryCreatedDInUtc = ((DateTime)reader["EntryCreatedD"]).ToUniversalTime();
                    record.FirstEffectiveDateTimeInUtc = ((DateTime)reader["FirstEffectiveDateTime"]).ToUniversalTime();
                    record.LatestEffectiveDateTimeInUtc = ((DateTime)reader["LatestEffectiveDateTime"]).ToUniversalTime();
                    record.LpeAcceptableRsFileId = (string)reader["LpeAcceptableRsFileId"];
                    record.VersionNumber = (long)reader["VersionNumber"];

                    DateTime modifiedDate = (DateTime)reader["ModifiedD"];

                    if (modifiedDate > lastModifiedDate)
                    {
                        lastModifiedDate = modifiedDate;
                    }

                    list.Add(record);
                }
            }

            return lastModifiedDate;
        }

    }
}
