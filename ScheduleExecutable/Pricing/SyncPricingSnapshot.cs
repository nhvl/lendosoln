﻿using System;
using System.IO;
using System.Net;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Drivers.FileSystem;
using LendersOfficeApp.los.RatePrice;
using LqbGrammar.DataTypes;

namespace ScheduleExecutable.Pricing
{
    public static class SyncPricingSnapshot
    {
        public static void Execute(string[] args)
        {
            if (args.Length != 2)
            {
                Console.WriteLine("Usage: ScheduleExecutable.exe SyncPricingSnapshot {config.json}");
                return;
            }

            var path = LocalFilePath.Create(args[1]);
            var json = TextFileHelper.ReadFile(path.Value);
            var config = SerializationHelper.JsonNetDeserialize<SyncPricingSnapshotConfig>(json);

            if (string.IsNullOrEmpty(config.Host) || string.IsNullOrEmpty(config.SecretKey))
            {
                throw new ArgumentException("Host or SecretKey cannot be empty.");
            }
            CLpeReleaseInfo releaseInfo = CLpeReleaseInfo.GetLatestReleaseInfo();
            
            DateTime dataLastModifiedD = releaseInfo.DataLastModifiedD;
            var fileKey = releaseInfo.FileKey;

            if (fileKey.Value.Equals(config.LastUploadedFileKey))
            {
                // This snapshot already uploaded.
                Console.WriteLine("Snapshot already upload.");
                return;
            }

            string url = config.Host + "/uploadlpe.aspx?op=UploadSnapshotAndMarkReady";

            using (var webClient = new WebClient())
            {
                webClient.Headers.Add("x-lqb-secret-key", config.SecretKey);
                webClient.Headers.Add("x-lqb-file-name", fileKey.Value);
                webClient.Headers.Add("x-lqb-data-last-modified", dataLastModifiedD.ToString());

                if (config.IsFileKeyOnly)
                {
                    webClient.UploadData(url, new byte[] { });
                }
                else
                {
                    try
                    {
                        FileDBTools.UseFile(E_FileDB.PricingSnapshot, fileKey.Value, fileInfo => webClient.UploadFile(url, fileInfo.FullName));

                    }
                    catch (FileNotFoundException)
                    {
                        FileDBTools.UseFile(E_FileDB.Normal, fileKey.Value, fileInfo => webClient.UploadFile(url, fileInfo.FullName));
                    }
                }
                
                config.LastUploadedFileKey = fileKey.Value;

                string updatedJson = SerializationHelper.JsonNetSerialize<SyncPricingSnapshotConfig>(config);
                TextFileHelper.WriteString(path.Value, updatedJson, false);
            }

        }
    }
}
