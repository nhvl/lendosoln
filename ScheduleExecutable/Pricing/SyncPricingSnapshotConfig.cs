﻿namespace ScheduleExecutable.Pricing
{
    /// <summary>
    /// Represents the settings for sync pricing snapshot. This is use for Json serialization. Do not add logic.
    /// </summary>
    public class SyncPricingSnapshotConfig
    {
        /// <summary>
        /// Gets or sets the host name to upload pricing snapshot to.
        /// </summary>
        public string Host { get; set; }

        /// <summary>
        /// Gets or sets the secret key.
        /// </summary>
        public string SecretKey { get; set; }

        /// <summary>
        /// Gets or sets the last uploaded snapshot file key.
        /// </summary>
        public string LastUploadedFileKey { get; set; }

        /// <summary>
        /// Gets or sets whether upload only snapshot file key or actual snapshot file.
        /// </summary>
        public bool IsFileKeyOnly { get; set; }
    }
}
