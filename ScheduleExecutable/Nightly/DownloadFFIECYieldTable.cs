﻿using System;
using LendersOffice.FFIEC;

namespace ScheduleExecutable.Nightly
{
    // 9/13/2013 dd - OPM 67223 - Download the Yield Table nightly.
    class DownloadFFIECYieldTable
    {
        public static void Execute(string[] args)
        {
            if (args.Length > 1 && args[1].Equals("lpqproxy", StringComparison.OrdinalIgnoreCase))
            {
                YieldTable.DownloadFromLoansPQProxy();
            }
            else
            {
                YieldTable.DownloadFromWebsite();
            }
        }
    }
}