﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Email;
using LendersOffice.Constants;

namespace ScheduleExecutable.Nightly
{
    /// <summary>
    /// 6/17/2013 dd - OPM 124658 - Because FirstTech give us a list of loan numbers to use. 
    /// Therefore when a reserve list almost deplete we need to notify lender.
    /// </summary>
    class CheckFirstTechLoanReserveList
    {
        public static void Execute(string[] args)
        {
            Guid brokerId = new Guid("E9F006C9-CB83-4531-8925-08FCCF4ADD63"); // FirstTech PML0223
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId)
                                            , new SqlParameter("@Type", "OSI")
                                        };
            
            int count = int.MaxValue;
            int threshold = 2000;

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "RESERVE_STRING_LIST_GetRemaining", parameters))
            {
                if (reader.Read())
                {
                    count = (int)reader["RemainingCount"];
                }
            }

            if (count < threshold)
            {
                // 6/17/2013 dd - Send out email to alert client.
                CBaseEmail email = new CBaseEmail(brokerId);
                email.From = ConstStage.SenderEmailAddressOfSupport;

                // 2/9/2016 - dd - Per OPM 236417 - Add more email to the notification.
                email.To = "EnterpriseSolutions@firsttechfed.com,Robin.Helt@firsttechfed.com,Tamera.Jette@firsttechfed.com,Stephanie.Hansen@firsttechfed.com,Arthur.chen@firsttechfed.com,matt.hicks@firsttechfed.com"; // 6/18/2013 dd - Per OPM 124658.
                email.Subject = "OSI Reserve Loan Number is running low.";
                email.Message = "There are " + count + " OSI loan number remain in LendingQB reserve list. Please provide a new reserve list.";

                email.Send();
            }
        }
    }
}
