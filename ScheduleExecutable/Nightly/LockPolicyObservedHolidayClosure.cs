﻿namespace ScheduleExecutable.Nightly
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using LendersOffice.Constants;
    using LendersOffice.Migration;
    using LendersOffice.ObjLib.LockPolicies;
    using ServiceMonitor = ServiceExecutionLogging.Logger;
    using DataAccess;

    /// <summary>
    /// A nightly process (and catch up process) to convert calculated observed holidays into past holiday records.
    /// </summary>
    public static class LockPolicyObservedHolidayClosure
    {
        /// <summary>
        /// A one time migration to update lock policy closures to use the new calculated holidays.
        /// </summary>
        /// <param name="args">Ignored; would be the arguments passed in.</param>
        /// <remarks>Safe to kill midway through and restart; this code should be removed after the 2016 October release.</remarks>
        public static void MigrateOldHolidayDates(string[] args)
        {
            int lenderCount = 0;
            foreach (var brokerId in Tools.GetAllActiveBrokers())
            {
                foreach (var lockPolicyId in RetrieveLockPolicyIdsForBroker(brokerId))
                {
                    LockDeskClosures.MigrateOldNonCalculatedHolidayDates(brokerId, lockPolicyId);
                }

                if (++lenderCount % 10 == 0)
                {
                    Console.WriteLine($"Migrated {lenderCount} lenders.");
                }
            }

            Console.WriteLine("Migration of all active lenders complete!");
        }

        /// <summary>
        /// Updates the lock policies to include any calculated holiday observance today.
        /// </summary>
        /// <param name="args">The arguments passed to <see cref="Program.Main(string[])"/>.</param>
        public static void RunNightlyProcess(string[] args)
        {
            RecordObservedHolidaysToLockPolicy(new[] { DateTime.Today });
        }

        /// <summary>
        /// Runs the nightly process for a specified list of dates.
        /// </summary>
        /// <param name="args">The arguments passed to <see cref="Program.Main(string[])"/>.</param>
        public static void RunUpdateForDates(string[] args)
        {
            if (args == null || args.Length <= 1)
            {
                Console.WriteLine("Must specify which dates need to be updated.");
                return;
            }

            DateTime[] datesToRunFor = ParseDates(args.Skip(1)); // args[0] is the name of the action and needs to be skipped
            RecordObservedHolidaysToLockPolicy(datesToRunFor, "_Custom");
        }

        /// <summary>
        /// Records any observed holidays in the specified date range into the past closures.
        /// </summary>
        /// <param name="datesToRunFor">The dates to look for observed holidays in.</param>
        /// <param name="serviceNameSuffix">A suffix to append to the name of the service for logging.</param>
        private static void RecordObservedHolidaysToLockPolicy(DateTime[] datesToRunFor, string serviceNameSuffix = null)
        {
            var monitor = new ServiceMonitor("LockPolicyObservedHolidayClosureUpdate" + serviceNameSuffix);
            if (monitor.IsImproperlyConfigured())
            {
                var email = new LendersOffice.Email.CBaseEmail(ConstAppDavid.SystemBrokerGuid)
                {
                    From = ConstStage.DefaultDoNotReplyAddress,
                    To = ConstStage.ExceptionDeveloperSupport,
                    CCRecipient = "timothyj@meridianlink.com",
                    Subject = "Lock Policy Observed Holiday Service Monitoring Config issue",
                    Message = "The Lock Policy Observed Holiday Closure Update process failed to load a properly configured service monitoring framework connection."
                };

                LendersOffice.Common.EmailUtilities.SendEmail(email);
            }

            monitor.Record(ServiceMonitor.StatusType.STARTED);
            try
            {
                if (datesToRunFor == null || !datesToRunFor.Any())
                {
                    monitor.Record(ServiceMonitor.StatusType.FAILED, "No valid dates specified.");
                    return;
                }

                foreach (var brokerId in Tools.GetAllActiveBrokers())
                {
                    foreach (var lockPolicyId in RetrieveLockPolicyIdsForBroker(brokerId))
                    {
                        LockDeskClosures.ConvertObservedHolidayToPastEntry(brokerId, lockPolicyId, datesToRunFor);
                    }
                }

                monitor.Record(ServiceMonitor.StatusType.OK, string.Join(", ", datesToRunFor));
            }
            catch (Exception exc) when (LogFailure(monitor, exc))
            {
            }
        }

        /// <summary>
        /// Logs a failure to the service monitor and to standard LQB logging.
        /// </summary>
        /// <param name="monitor">The service monitor to log to.</param>
        /// <param name="exc">The exception to log.</param>
        /// <returns><see langword="false"/> always, so that exception filters will not catch.</returns>
        private static bool LogFailure(ServiceMonitor monitor, Exception exc)
        {
            monitor.Record(ServiceMonitor.StatusType.FAILED, exc.Message);
            DataAccess.Tools.LogError(exc);
            return false;
        }

        /// <summary>
        /// Given a lender's broker id, retrieves the ids of the lock policies of that lender.
        /// </summary>
        /// <param name="brokerId">The broker id to look up the policies for.</param>
        /// <returns>An iteration through the lock policy ids.</returns>
        private static IEnumerable<Guid> RetrieveLockPolicyIdsForBroker(Guid brokerId)
        {
            var parameters = new[] { new System.Data.SqlClient.SqlParameter("@BrokerId", brokerId) };
            using (var reader = DataAccess.StoredProcedureHelper.ExecuteReader(brokerId, "LockPolicy_RetrieveAllNamesForBroker", parameters))
            {
                return reader.Cast<System.Data.IDataRecord>().Select(record => (Guid)record["LockPolicyId"]).ToList();
            }
        }

        /// <summary>
        /// Parses a list of Dates from a string array, beginning at the specified index.
        /// </summary>
        /// <param name="dateStrings">The strings to parse as dates.</param>
        /// <returns>An array of parsed <seealso cref="DateTime"/> objects if all parsing was successful, the empty array otherwise.</returns>
        private static DateTime[] ParseDates(IEnumerable<string> dateStrings)
        {
            List<DateTime> parsedDates = new List<DateTime>();
            List<string> invalidDates = new List<string>();
            foreach (var dateString in dateStrings.Where(dateString => !string.IsNullOrEmpty(dateString)))
            {
                DateTime result;
                if (DateTime.TryParse(dateString, out result))
                {
                    parsedDates.Add(result.Date);
                }
                else
                {
                    invalidDates.Add(dateString);
                }
            }

            foreach (var invalidDate in invalidDates)
            {
                Console.WriteLine("Unable to recognize date \"" + invalidDate + "\"");
            }

            if (invalidDates.Any())
            {
                Console.WriteLine("Giving up on parsed dates due to failure, as this could indicate one date being parsed as two strings.");
                return new DateTime[0];
            }

            return parsedDates.ToArray();
        }
    }
}
