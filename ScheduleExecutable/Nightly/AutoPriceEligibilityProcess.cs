﻿using DataAccess;
using LendersOffice.Common;
using LendersOffice.ObjLib.DatabaseMessageQueue;
using LendersOffice.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ScheduleExecutable.Nightly
{
    /// <summary>
    /// Overnight process that runs pricing.  It can be called in two modes:
    /// 1: LOAD mode: Load up the queue with all applicable loanids.
    /// 2: RUN mode: Process the queue.
    /// 
    /// We plan to do LOAD mode once per night, and RUN mode continuously at night.
    /// </summary>
    class AutoPriceEligibilityProcess
    {
        public static void Execute(string[] args)
        {
            if ( args.Length < 2 )
            {
                System.Console.WriteLine("Pass in command (LOAD or RUN).");
                return;
            }

            var cmd = args[1];
            if (cmd.Equals("load", StringComparison.OrdinalIgnoreCase))
            {
                // The Load.  Load the queue with all applicable loans
                // at applicable lenders.
                AutoPriceEligibility.EnqueueAllApplicableLoans();
            }
            else if (cmd.Equals("run", StringComparison.OrdinalIgnoreCase))
            {
                // This is the actual run.  Pull up loans from the queue and
                // process them.

                // Can pass in limit for number of loans to process in the run.
                int maxLoansToProcess = int.MaxValue;
                if (args.Length > 2)
                {
                    if (!int.TryParse(args[2], out maxLoansToProcess))
                    {
                        maxLoansToProcess = int.MaxValue;
                    }
                }

                // Can pass in limit for amount of time to process in one run.
                int maxProcessingSeconds = (int)TimeSpan.FromHours(4).TotalSeconds;
                if (args.Length > 3)
                {
                    if (!int.TryParse(args[3], out maxProcessingSeconds))
                    {
                        maxProcessingSeconds = (int) TimeSpan.FromHours(4).TotalSeconds;
                    }
                }

                Run(maxLoansToProcess, maxProcessingSeconds);
            }
            else
            {
                System.Console.WriteLine($"Unknown command:{args[0]}.  Pass in command (LOAD or RUN).");
            }
        }

        private static void Run(int maxLoans, int maxSeconds)
        {
            // Process loans considering both the time limit and loan file limit.
            int loansProcessed = 0;
            int loansSkipped = 0;
            var startTime = DateTime.Now;
            var maxTime = startTime.AddSeconds(maxSeconds);

            DBMessageQueue dbQueue = new DBMessageQueue(ConstMsg.AutoPriceEligibilityQueue);

            try
            {
                while (loansProcessed < maxLoans
                    && DateTime.Now < maxTime)
                {
                    try
                    {
                        DBMessage message = dbQueue.Receive();

                        if (message == null)
                        {
                            break;
                        }

                        var loanId = new Guid(message.Subject1);
                        bool loanSkipped;
                        AutoPriceEligibility.RunPriceAndEligibilityCheck(loanId, out loanSkipped);
                        loansProcessed++;

                        if (loanSkipped)
                        {
                            loansSkipped++;
                        }
                    }
                    catch (DBMessageQueueException e)
                    {
                        Tools.LogWarning("DBMQ Exception", e);
                        break;
                    }
                }
            }
            finally
            {
                // For tracking usage.
                var timeSpent = DateTime.Now -startTime;
                var totalSeconds = Math.Round(timeSpent.TotalSeconds, 2);
                var secondsPerLoan = Math.Round((loansProcessed != 0) ? (totalSeconds / loansProcessed) : 0, 2);

                Tools.LogInfo($"[AutoPrice] Ran {loansProcessed} loans in {totalSeconds} seconds. {secondsPerLoan} seconds per loan." 
                    + ( loansSkipped != 0 ? $" {loansSkipped} loans were skipped because they were unchanged." : string.Empty));
            }

        }

    }
}
