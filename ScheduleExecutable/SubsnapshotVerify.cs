﻿using CommonProjectLib.Caching;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.DistributeUnderwriting;
using LendersOffice.Drivers.Gateways;
using LendersOffice.RatePrice;
using LendersOffice.Security;
using LendersOfficeApp.los.RatePrice;
using LendersOfficeApp.RatePrice.XmlResultPricingCmpUtils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace ScheduleExecutable
{
    class SubsnapshotVerify
    {
        public static void Execute(string[] args)
        {
            if (args.Length != 2)
            {
                Console.WriteLine("Usage: ScheduleExecutable SubsnapshotVerify loandIds.txt");
                return;
            }

            string reportFname = "PricingSummary-" + DateTime.Now.ToString("yyyyMMdd-HHmmss") + ".txt";

            string[] loanList = File.ReadAllLines(args[1]);
            int count = 0;
            foreach (var id in loanList)
            {
                if (string.IsNullOrEmpty(id))
                {
                    continue;
                }

                Guid loanId = new Guid(id);
                var result = ComparePricing(loanId);
                if( ++count == 1)
                {
                    WriteToSummaryFile(reportFname, "loanId,result, subsnapshot pricing in second, fullsnapshot pricing in second, subsnapshot / full snapshot pricing %");
                }

                if( result.FullSnapshotPricingInMs > 0)
                {
                    double ratio = result.SubsnapshotPricingInMs * 100.0 / result.FullSnapshotPricingInMs;
                    WriteToSummaryFile(reportFname, $"{loanId}, {(result.IsEqual ? "Pass" : "Fail")}, {result.SubsnapshotPricingInMs/1000.0 : 0.000}, {result.FullSnapshotPricingInMs / 1000.0: 0.000}, {ratio:0.00}%");
                }
                else
                {
                    WriteToSummaryFile(reportFname, $"{loanId}, Exception");
                }
            }

            LoadLoanProgramOnDemand.TriggerExit();
        }

        private class CompareResult
        {
            public bool IsEqual = false;
            public int FullSnapshotPricingInMs = -1;
            public int SubsnapshotPricingInMs = -1;
        }

        private static CompareResult ComparePricing(Guid loanId)
        {
            string path = Directory.GetCurrentDirectory();
            var sbLog = new StringBuilder();
            int fullSnapshotDuration = 0, subsnapshotDuration=0;

            try
            {
                sbLog.AppendLine($"Call Init({loanId})");
                HybridLoanProgramSetOption historicalOpts = Init(loanId);
                sbLog.Clear();

                AbstractUserPrincipal principle = PrincipalFactory.CurrentPrincipal as AbstractUserPrincipal;

                sbLog.AppendLine($"LoanId: {loanId}");
                sbLog.AppendLine($"BrokerId: {principle.BrokerDB.BrokerID}");
                sbLog.AppendLine($"HybridLoanProgramSetOption")
                     .AppendLine($"    LoanProgramTemplate: {historicalOpts.LoanProgramTemplate}")
                     .AppendLine($"    RateOption: {historicalOpts.RateOptions}")
                     .AppendLine($"    ApplicablePolicies: {historicalOpts.ApplicablePolicies}")
                     .AppendLine($"    SnapshotContentKey: {historicalOpts.SnapshotContentKey}")
                     .AppendLine($"    PriceGroupOptions: {historicalOpts.PriceGroupOptions}")
                     .AppendLine($"    PriceGroupContentKey: {historicalOpts.PriceGroupContentKey}")
                     .AppendLine($"    FeeServiceOptions: {historicalOpts.FeeServiceOptions}")
                     .AppendLine($"    FeeServiceRevisionId: {historicalOpts.FeeServiceRevisionId}\r\n");

                Tools.LogInfo("********* Full Filebased Snapshot Pricing **********");
                historicalOpts.UseSubsnapshot = false;
                UnderwritingResultItem fullSnapshotPricing = InternalPricing(loanId, historicalOpts, out fullSnapshotDuration);
                sbLog.AppendLine($"Full snapshot pricing {fullSnapshotDuration:N0} ms");

                if( fullSnapshotDuration == 0)
                {
                    fullSnapshotDuration = 1; 
                }

                Tools.LogInfo("********* Filebased Sub-snapshot Pricing **********");
                historicalOpts.UseSubsnapshot = true;
                UnderwritingResultItem subsnapshotPricing = InternalPricing(loanId, historicalOpts, out subsnapshotDuration);
                sbLog.AppendLine($"Subsnapshot pricing {subsnapshotDuration:N0} ms");

                double ratio = subsnapshotDuration * 100.0 / fullSnapshotDuration;
                sbLog.AppendLine($"subsnapshot/fullsnapshot pricing duration ratio: {ratio:0.00}%");

                XmlDocument fullSnapshotXml = ToXml(fullSnapshotPricing, onlyEligiblePrg: true);
                XmlDocument subsnapshotXml = ToXml(subsnapshotPricing, onlyEligiblePrg: true);

                string outputStr;
                bool cmpResult = XmlResultPricingCmp.Compare(fullSnapshotXml.OuterXml, subsnapshotXml.OuterXml, out outputStr);
                sbLog.AppendLine().AppendLine($"Comparing result: {cmpResult}");

                if (cmpResult == false)
                {
                    string folder = Path.Combine(path, "Result_" + Guid.NewGuid().ToString());
                    Directory.CreateDirectory(folder);

                    string fullSnapshotFname = Path.Combine(folder, "fullSnapshotResult.xml");
                    string subsnapshotFname = Path.Combine(folder, "subsnapshotReulst.xml");
                    string cmpResultFname = Path.Combine(folder, "CmpResult.txt");

                    XmlResultPricingCmp.XmlToFile(fullSnapshotFname, ToXml(fullSnapshotPricing, onlyEligiblePrg: false).OuterXml);
                    XmlResultPricingCmp.XmlToFile(subsnapshotFname, ToXml(subsnapshotPricing, onlyEligiblePrg: false).OuterXml);

                    sbLog.AppendLine().AppendLine(outputStr);

                    TextFileHelper.WriteString(cmpResultFname, sbLog.ToString(), false);
                }

                return new CompareResult()
                {
                    IsEqual = cmpResult,
                    FullSnapshotPricingInMs = fullSnapshotDuration,
                    SubsnapshotPricingInMs = subsnapshotDuration,
                };
            }
            catch (Exception ex)
            {
                string exceptionType = ex.GetType().FullName;
                int idx = exceptionType.LastIndexOf(".");
                string fnamePrefix = (idx >= 0) ? "Result" + exceptionType.Substring(idx + 1) + "_" : "ResultException_";
                string exceptionLogFile = Path.Combine(path, fnamePrefix + Guid.NewGuid().ToString() + ".txt");

                sbLog.AppendLine(Environment.NewLine +
                                 "Exception type: " + ex.GetType().FullName + Environment.NewLine + Environment.NewLine
                                 + "Error message : " + ex.Message + Environment.NewLine + ex.StackTrace);

                TextFileHelper.WriteString(exceptionLogFile, sbLog.ToString(), false);

                if (ex.Message.Contains("There is not enough space on the disk"))
                {
                    throw;
                }

                return new CompareResult()
                {
                    IsEqual = false,
                    FullSnapshotPricingInMs = -1,
                    SubsnapshotPricingInMs = -1,
                };
            }               
        }

        private static UnderwritingResultItem InternalPricing(Guid loanId, HybridLoanProgramSetOption option, out int durationInMs)
        {
            var sw = Stopwatch.StartNew();

            UnderwritingResultItem resultItem = null;
            Guid requestId = Guid.Empty;

            for (int i=0; i < 2; i++)
            {
                requestId = DistributeUnderwritingEngine.RequestInternalPricingResult(
                                                                PrincipalFactory.CurrentPrincipal,
                                                                loanId,
                                                                E_sPricingModeT.InternalBrokerPricing,
                                                                option,
                                                                isAutoProcess: true);
                try
                {
                    resultItem = LpeDistributeResults.RetrieveResultsSynchronous(requestId);
                    break;
                }
                catch (DistributeTimeoutException exc)
                {
                    // If there is an LPE timeout, allow it to retry.  We 
                    // have seen memory issues cause a pricing server to
                    // take the request for processing, and not compute a result.
                    Tools.LogError($"Pricing result timeout: {loanId.ToString()}, retry: {i}.", exc);
                }

                Tools.SleepAndWakeupRandomlyWithin(300, 1200);
            }

            durationInMs = (int)sw.ElapsedMilliseconds;

            return resultItem;
        }

        // Get AbstractUserPrincipal, run normal pricing, return HybridLoanProgramSetOption object.
        private static HybridLoanProgramSetOption Init(Guid loanId)
        {
            var dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(AutoPriceEligibility));

            dataLoan.ByPassFieldSecurityCheck = true;
            dataLoan.InitLoad();

            AbstractUserPrincipal runningPrincipal = GetPrincipalForRunning(dataLoan);
            Thread.CurrentPrincipal = runningPrincipal;

            // historical setting
            var lastUsedSnapshot = dataLoan.SubmissionSnapshots.Where(
                                    p => p.SubmissionType == E_sPricingModeT.InternalBrokerPricing
                                    || p.SubmissionType == E_sPricingModeT.RegularPricing
                                    || p.SubmissionType == E_sPricingModeT.Undefined)
                                .OrderBy(p => p.SubmissionDate).LastOrDefault();
            HybridLoanProgramSetOption historicalOpts = HybridLoanProgramSetOption.CreateHistoricalPricingOption(dataLoan.BrokerDB.HistoricalPricingSettings, lastUsedSnapshot);

            // Run normal pricing to make sure: current related policies loaded in memory
            HybridLoanProgramSetOption normalOpts = HybridLoanProgramSetOption.GetCurrentSnapshotOption(runningPrincipal.BrokerDB);
            int duration;
            InternalPricing(loanId, normalOpts, out duration);

            return historicalOpts;
        }

        private static AbstractUserPrincipal GetPrincipalForRunning(CPageData dataLoan)
        {
            // Pricing requires a real principal.
            // So we do this similar to how the rate monitor does.
            Guid employeeId;
            if (dataLoan.BranchChannelT == E_BranchChannelT.Correspondent)
            {
                employeeId = dataLoan.sEmployeeSecondaryId != Guid.Empty ?
                    dataLoan.sEmployeeSecondaryId
                    : dataLoan.sEmployeeExternalSecondaryId;
            }
            else
            {
                employeeId = dataLoan.sEmployeeLoanRepId;
            }

            if (employeeId == Guid.Empty)
            {
                // Per PDE consider this invalid scenario.
                // LO or secondary has to be assigned for result to be valid.
                throw new GenericUserErrorMessageException("No valid LO or Secondary.");
            }

            Guid? userId;
            char? userType;
            AbstractUserPrincipal userPrincipal = null;
            if (Tools.TryGetUserInfoByEmployeeId(dataLoan.sBrokerId, employeeId, out userId, out userType))
            {
                if (userId.HasValue && userType.HasValue)
                {
                    userPrincipal = PrincipalFactory.RetrievePrincipalForUser(
                        dataLoan.sBrokerId,
                        userId.Value,
                        char.ToString(userType.Value));
                }
            }

            if (userPrincipal == null)
            {
                throw new GenericUserErrorMessageException("Could not get user Principal. EmployeeId:" + employeeId);
            }

            return userPrincipal;
        }

        private static XmlDocument ToXml(UnderwritingResultItem item, bool onlyEligiblePrg)
        {
            XmlDocument doc = new XmlDocument();
            XmlElement root = doc.CreateElement("results");
            doc.AppendChild(root);

            XmlElement elApplicantPrices = doc.CreateElement("CApplicantPrices");
            root.AppendChild(elApplicantPrices);

            foreach(CApplicantPriceXml p in onlyEligiblePrg ? item.GetEligibleLoanProgramList() : item.GetApplicantPriceList())
            {
                XmlElement el = CApplicantPriceXml.ToXmlElement(p, doc);
                elApplicantPrices.AppendChild(el);
            }

            return doc;
        }

        private static void WriteToSummaryFile(string fname , string msg)
        {
            try
            {
                Action<TextFileHelper.LqbTextWriter> writeHandler = delegate (TextFileHelper.LqbTextWriter sw)
                {
                    sw.AppendLine(msg);
                };

                TextFileHelper.OpenForAppend(fname, writeHandler);
            }
            catch (SystemException)
            {
            }
        }
    }
}
