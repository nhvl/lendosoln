﻿// <copyright file="LoanInspection.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>This is LoanInspection class.</summary>
// <author>david</author>
// <date>$date$</author>
namespace ScheduleExecutable
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Drivers.Gateways;

    /// <summary></summary>
    public class LoanInspection
    {
        private List<KeyValuePair<string, PropertyInfo>> m_propertyList = null;
        private List<string> m_dependencyFieldList = null;

        private HashSet<string> m_skipPropertySet = new HashSet<string>(StringComparer.OrdinalIgnoreCase)
        {
            "lpeRunModeT", "IsSpStateAllowedPp"
        };

        private class Item
        {
            public string Key { get; set; }
            public string OldValue { get; set; }
            public string NewValue { get; set; }
        }

        private bool IsInclude(PropertyInfo prop)
        {
            if (m_skipPropertySet.Contains(prop.Name))
            {
                return false;
            }

            if (prop.PropertyType == typeof(bool))
            {
                return true;
            }

            if (prop.PropertyType.IsEnum)
            {
                return true;
            }

            if (prop.Name.EndsWith("_rep") == false)
            {
                // 4/11/2015 dd - Only interest in _rep field for now.
                return false;
            }

            if (prop.PropertyType != typeof(string))
            {
                return false;
            }


            return true;
        }
        private void Initialize()
        {
            Type cpageDataType = typeof(CPageData);

            var propertyList = cpageDataType.GetProperties(BindingFlags.Instance | BindingFlags.Public);

            m_propertyList = new List<KeyValuePair<string, PropertyInfo>>();
            m_dependencyFieldList = new List<string>();
            foreach (PropertyInfo prop in propertyList)
            {
                if (IsInclude(prop) == false)
                {
                    continue;
                }

                string name = prop.Name.Replace("_rep", "");

                m_propertyList.Add(new KeyValuePair<string, PropertyInfo>(name, prop));
                m_dependencyFieldList.Add(name);
            }
        }

        public string InspectLoanAsJson(Guid sLId)
        {
            if (null == m_propertyList)
            {
                Initialize();
            }

            CPageData dataLoan = new NotEnforceAccessControlPageData(sLId, m_dependencyFieldList);
            dataLoan.InitLoad();

            Dictionary<string, string> result = new Dictionary<string, string>();

            foreach (var item in m_propertyList)
            {
                object obj = null;
                try
                {
                    obj = item.Value.GetValue(dataLoan, null);
                }
                catch (Exception exc)
                {
                    Console.WriteLine("Unable to export field:[" + item.Key + "].");
                    Console.WriteLine("    " + exc.ToString());
                    Console.WriteLine();
                    Console.WriteLine();
                    obj = "ERROR";
                }
                string value = obj == null ? string.Empty : obj.ToString();
                result[item.Key] = value;

            }

            string json = SerializationHelper.JsonNetSerialize(result);

            return SerializationHelper.JsonBeautify(json);
        }

        public static void Execute(string[] args)
        {
            Stopwatch sw = Stopwatch.StartNew();

            if (args.Length < 4)
            {
                Console.WriteLine("Usage: ScheduleExecutable LoanInspection {cmd} {sLId} {output_file}");
                return;
            }

            string cmd = args[1];

            if (cmd.Equals("Export", StringComparison.OrdinalIgnoreCase))
            {
                Guid sLId = new Guid(args[2]);
                string outputFile = args[3];

                LoanInspection loanInspection = new LoanInspection();

                string json = loanInspection.InspectLoanAsJson(sLId);

                TextFileHelper.WriteString(outputFile, json, false);
            }
            else if (cmd.Equals("Compare", StringComparison.OrdinalIgnoreCase))
            {
                string file1 = args[2];
                string file2 = args[3];
                string outputFileName = args.Length == 5 ? args[4] : "InspectionDiff.json";

                CompareFromFileDictionaries(outputFileName, file1, file2);
            }

            Console.WriteLine("Execute in " + sw.ElapsedMilliseconds + "ms.");
        }

        private static void CompareFromFileDictionaries(string outputFileName, string file1, string file2)
        {
            string json = TextFileHelper.ReadFile(file1);

            Dictionary<string, string> dictionary1 = SerializationHelper.JsonNetDeserialize<Dictionary<string, string>>(json);

            json = TextFileHelper.ReadFile(file2);

            Dictionary<string, string> dictionary2 = SerializationHelper.JsonNetDeserialize<Dictionary<string, string>>(json);

            List<Item> list = new List<Item>();

            foreach (var o in dictionary1)
            {
                string v1 = o.Value;

                string v2;

                Item item = null;

                if (dictionary2.TryGetValue(o.Key, out v2))
                {
                    if (v1 != v2)
                    {
                        item = new Item();
                        item.Key = o.Key;
                        item.OldValue = v1;
                        item.NewValue = v2;
                    }

                    dictionary2.Remove(o.Key);
                }
                else
                {
                    item = new Item();
                    item.Key = o.Key;
                    item.OldValue = v1;
                    item.NewValue = null;
                }

                if (item != null)
                {
                    list.Add(item);
                }
            }

            foreach (var o in dictionary2)
            {
                Item item = new Item();
                item.Key = o.Key;
                item.OldValue = null;
                item.NewValue = o.Value;

                list.Add(item);
            }

            if (list.Count > 0)
            {
                json = SerializationHelper.JsonNetSerialize(list);
                json = SerializationHelper.JsonBeautify(json);

                TextFileHelper.WriteString(outputFileName, json, false);
                Console.WriteLine($"There are {list.Count} differences. Report '{outputFileName}' is generated.");
            }
            else
            {
                Console.WriteLine("There are no different. No report generate.");
            }
        }
    }
}
