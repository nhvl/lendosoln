﻿// <copyright file="ExpiredCacheCleanup.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: David Dao
//    Date:   8/21/2014 5:20:40 PM 
// </summary>
namespace ScheduleExecutable.Cleanup
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DataAccess;

    /// <summary>
    ///
    /// </summary>
    class ExpiredCacheCleanup
    {
        /// <summary>
        /// This is to be run periodically to remove the expired temp cache entries.
        /// </summary>
        public static void Execute(string[] args)
        {
            int numberOfDbEntries, numberOfFileEntries;
            AutoExpiredTextCache.EnforceExpiration(out numberOfDbEntries, out numberOfFileEntries);
            Tools.LogInfo(string.Format("ExpiredCacheCleanup.exe: Deleted {0} from DB, {1} from FileDB.", numberOfDbEntries, numberOfFileEntries));
        }
    }
}