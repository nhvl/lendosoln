﻿// <copyright file="CredcoCleanup.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: David Dao
//    Date:   8/21/2014 5:16:52 PM 
// </summary>
namespace ScheduleExecutable.Cleanup
{
    using System;
    using System.Data.Common;
    using System.Data.SqlClient;
    using DataAccess;
    using LendersOffice.CreditReport;
    using LendersOffice.CreditReport.Credco;

    /// <summary>
    /// This program should get run at each midnight to clean up Credco Credit Report that is over 5 days old.
    /// In agreement with Credco, we are not allow to store the raw data of credit report past 5 days. If client
    /// want to view old credit report they have to reorder the report with exact information to avoid double charge.
    /// </summary>
    class CredcoCleanup
    {
        public static void Execute(string[] args)
        {
            int successfulCount = 0;
            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connInfo, "ListOldCredcoCreditReports", null))
                {
                    while (reader.Read())
                    {
                        try
                        {
                            Guid applicationID = (Guid)reader["owner"];
                            Guid dbFileKey = (Guid)reader["DbFileKey"];
                            string reportId = (string)reader["ExternalFileId"];

                            CreditReportServer.DeleteCreditReport(connInfo, applicationID);

                            // 2/14/2006 dd - Replace the actual credit report with a text inform user that we delete credit report.
                            byte[] data = System.Text.Encoding.UTF8.GetBytes(Credco_CreditReport.CreateDeleteNotice(DateTime.Now, reportId));
                            FileDBTools.WriteData(E_FileDB.Normal, dbFileKey.ToString(), data);

                            successfulCount++;

                        }
                        catch (Exception exc)
                        {
                            Tools.LogErrorWithCriticalTracking(exc);
                            throw;
                        }
                    }
                }
            }
            Tools.LogInfo(string.Format("CredcoCleanup.exe : Delete {0} successful.", successfulCount));

        }
    }
}