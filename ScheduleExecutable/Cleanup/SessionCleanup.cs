﻿// <copyright file="SessionCleanup.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Matthew Flynn
//    Date:   8/29/2016 5:22:12 PM 
// </summary>
namespace ScheduleExecutable.Cleanup
{
    using System;
    using System.Data.SqlClient;
    using DataAccess;
    using LendersOffice.Constants;
    
    /// <summary>
    /// Removes session rows for old logged out sessions.
    /// </summary>
    public class SessionCleanup
    {
        /// <summary>
        /// Run session cleanup.
        /// </summary>
        /// <param name="args">Running arguments.</param>
        public static void Execute(string[] args)
        {
            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                SqlParameter[] parameters = new SqlParameter[]
                {  
                    new SqlParameter("@DateCutoff", DateTime.Now.AddMinutes(-ConstApp.LOCookieExpiresInMinutes))
                };

                StoredProcedureHelper.ExecuteNonQuery(connInfo, "RemoveLoginSessionsByDate", 1, parameters);
            }
        }
    }
}
