﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using LendersOffice.RatePrice.FileBasedPricing;
using LendersOfficeApp.los.RatePrice;

namespace ScheduleExecutable
{
    class ExportLpeSnapshot
    {

        public static void CreateSnapshot(string[] args)
        {
            FileBasedPricingUtilities.CreateSnapshot();
        }

        public static void Execute(string[] args)
        {
            // 12/31/2013 dd - TODO: Add some verification code later.
            
        }

        private static bool Compare(StringBuilder sb, PmiManager expectPmiManager, PmiManager actualPmiManager)
        {
            var orderExpectIdList = expectPmiManager.PmiPolicyIds.OrderBy(o => o);
            var orderActualIdList = actualPmiManager.PmiPolicyIds.OrderBy(o => o);
            bool isMatch = true;

            for (int i = 0; i < orderExpectIdList.Count();i++ )
            {
                var expectId = orderExpectIdList.ElementAt(i);
                var actualId = orderActualIdList.ElementAt(i);
                if (expectId != actualId)
                {
                    isMatch = false;
                    sb.AppendLine("PMI Manager PmiPolicyIds is not match.");
                    sb.AppendLine("Expect Id");
                    for (int j = 0; j < orderExpectIdList.Count(); j++)
                    {

                        sb.AppendLine("     " + orderExpectIdList.ElementAt(j));
                    }
                    sb.AppendLine("Actual Id");
                    for (int j = 0; j < orderActualIdList.Count(); j++)
                    {

                        sb.Append("     " + orderActualIdList.ElementAt(j));
                    }
                    break;
                }
            }

            foreach (E_PmiCompanyT company in Enum.GetValues(typeof(E_PmiCompanyT)))
            {
                foreach (E_sProdConvMIOptionT miOption in Enum.GetValues(typeof(E_sProdConvMIOptionT)))
                {
                    RtPmiProgram expectProgram = expectPmiManager.GetPmiProgram(company, miOption);
                    RtPmiProgram actualProgram = actualPmiManager.GetPmiProgram(company, miOption);
                    bool isProgramMatch = Compare(sb, expectProgram, actualProgram);
                    if (isProgramMatch == false)
                    {
                        isMatch = false;
                        sb.AppendLine("PMI Manager does not match for PmiCompanyT=" + company + ". MiOPtion=" + miOption);

                    }
                }
            }
            return isMatch;
        }

        private static bool Compare(StringBuilder sb, RtPmiProgram expectProgram, RtPmiProgram actualProgram)
        {
            if (expectProgram == null && actualProgram == null)
            {
                return true;
            }
            if (expectProgram == null || actualProgram == null)
            {
                return false;
            }
            if (expectProgram.Policies.Count != actualProgram.Policies.Count)
            {
                sb.Append("Policy Count=" + expectProgram.Policies.Count + " Actual Count=" + actualProgram.Policies.Count);
                return false;
            }
            foreach (var id in expectProgram.Policies)
            {
                if (actualProgram.Policies.Contains(id) == false)
                {
                    sb.AppendLine("    Policies for Pmi Program does not match." + expectProgram.PmiProgramId);
                    return false;
                }
            }
            return true;
        }
        private static bool Compare(StringBuilder sb, Guid lLpTemplateId, IEnumerable<Guid> policyIdList, FileBasedLpePriceGroup lpePriceGroup)
        {
            HashSet<Guid> set0 = new HashSet<Guid>(policyIdList);
            HashSet<Guid> set1 = new HashSet<Guid>();
            foreach (var program in lpePriceGroup.ProgramList)
            {
                if (program.lLpTemplateId == lLpTemplateId)
                {
                    if (program.ApplicablePricePolicyList != null)
                    {
                        foreach (var id in program.ApplicablePricePolicyList)
                        {
                            set1.Add(id);
                        }
                    }
                    break;
                }

            }

            bool isMatch = false;

            if (set0.Count == set1.Count) {
                isMatch = true;
                foreach (var o in set0)
                {
                    if (set1.Contains(o) == false)
                    {
                        isMatch = false;
                        break;
                    }
                }
            }
            if (isMatch)
            {
                //sb.AppendLine("PriceGroupId:" + lpePriceGroup.LpePriceGroupId + ", lLpTemplateId:" + lLpTemplateId + ". MATCHED");

            }
            else
            {
                sb.AppendLine("PriceGroupId:" + lpePriceGroup.LpePriceGroupId + ", lLpTemplateId:" + lLpTemplateId + ". NOT MATCHED");
                sb.AppendLine("    EXPECTED:");
                foreach (var id in set0)
                {
                    if (set1.Contains(id) == false)
                    {
                        sb.AppendLine("     " + id);
                    }
                }
                sb.AppendLine("    DOES NOT EXPECT:");
                foreach (var id in set1)
                {
                    if (set0.Contains(id) == false)
                    {
                        sb.AppendLine("     " + id);
                    }
                }
            }
            return isMatch;
            //throw new NotImplementedException();
        }

    }
}
