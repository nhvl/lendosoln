﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.Gateways;

    /// <summary>
    /// Adds a broker's Legal Entity Identifier (LEI) to a list of
    /// the lender's loans.
    /// </summary>
    public class AddLenderIdentifierToLoansMigrator : Migrator<Guid>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AddLenderIdentifierToLoansMigrator"/> class.
        /// </summary>
        /// <param name="customerCode">
        /// The customer code for the broker.
        /// </param>
        /// <param name="legalEntityIdentifer">
        /// The legal entity identifier for the broker.
        /// </param>
        /// <param name="loanIds">
        /// The list of loans to migrate.
        /// </param>
        private AddLenderIdentifierToLoansMigrator(string customerCode, IEnumerable<Guid> loanIds) 
            : base(nameof(AddLenderIdentifierToLoansMigrator) + "_" + customerCode)
        {
            this.MigrationHelper = new CustomGUIDMigrationHelper(nameof(AddLenderIdentifierToLoansMigrator) + "_" + customerCode, () => loanIds);
        }

        /// <summary>
        /// Migrates the loans for the lender specified in <paramref name="args"/>.<para></para>
        /// Expected format: AddLenderIdentifierToLoansMigrator customer_code loan_reference_number_file_path .
        /// </summary>
        /// <param name="args">
        /// The arguments for the migrator.
        /// </param>
        public static void MigrateLoansByCustomerCode(string[] args)
        {
            if (args.Length != 2)
            {
                Console.WriteLine("Usage: AddLenderIdentifierToLoansMigrator customer_code loan_reference_number_file_path");
                return;
            }
            
            var brokerId = Tools.GetBrokerIdByCustomerCode(args[0]);
            var broker = BrokerDB.RetrieveById(brokerId);

            if (!ConfirmBroker(broker.Name))
            {
                Console.WriteLine("Quitting.");
                return;
            }

            if (string.IsNullOrWhiteSpace(broker.LegalEntityIdentifier))
            {
                Console.WriteLine(broker.Name + " does not have an LEI on file. Quitting.");
                return;
            }

            var loanIds = GetLoanIds(args[1], brokerId);
            var migrator = new AddLenderIdentifierToLoansMigrator(args[0], loanIds);
            migrator.Migrate();
        }

        /// <summary>
        /// Performs the migration for the loan with the specified <paramref name="loanId"/>.
        /// </summary>
        /// <param name="loanId">
        /// The ID of the loan to migrate.
        /// </param>
        /// <returns>
        /// True if the migration was successful, false otherwise.
        /// </returns>
        protected override bool MigrationFunction(Guid loanId)
        {
            try
            {
                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(AddLenderIdentifierToLoansMigrator));
                loan.ByPassFieldSecurityCheck = true;
                loan.InitSave(ConstAppDavid.SkipVersionCheck);

                loan.sLegalEntityIdentifier = loan.Branch.LegalEntityIdentifier;
                loan.Save();

                return true;
            }
            catch (Exception exc)
            {
                Tools.LogError(exc);
                Console.WriteLine(exc.ToString());
                return false;
            }
        }

        /// <summary>
        /// Verifies that the customer code entered in the arguments
        /// matches the broker to be migrated.
        /// </summary>
        /// <param name="brokerName">
        /// The name of the broker being migrated.
        /// </param>
        /// <returns>
        /// True if the entered customer code matches the broker's name,
        /// false otherwise.
        /// </returns>
        private static bool ConfirmBroker(string brokerName)
        {
            Console.WriteLine("This migration will add the legal entity identifier to loans for broker '" + brokerName + "', enter Yes to continue.");
            var response = Console.ReadLine();
            return string.Equals("yes", response, StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Obtains the ids of the loans from the loan reference number file.
        /// </summary>
        /// <param name="path">
        /// The path to the file containing loan reference numbers.
        /// </param>
        /// <param name="brokerId">
        /// The ID of the broker that will have loans migrated.
        /// </param>
        /// <returns>
        /// The list of loan ids to migrate.
        /// </returns>
        private static IEnumerable<Guid> GetLoanIds(string path, Guid brokerId)
        {
            var loanIds = new LinkedList<Guid>();
            var loanReferenceNumbers = TextFileHelper.ReadLines(path);

            foreach (var referenceNumber in loanReferenceNumbers)
            {
                // Include inactive loans in the search since these will be loan files
                // provided to us by the client that may be reactivated.
                var loanId = Tools.GetLoanIdByLoanReferenceNumber(brokerId, referenceNumber.Trim(), includeInactive: true);
                if (loanId == Guid.Empty)
                {
                    Console.WriteLine("Could not locate loan with reference number " + referenceNumber);
                }
                else
                {
                    loanIds.AddLast(loanId);
                }
            }

            return loanIds;
        }
    }
}
