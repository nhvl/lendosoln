﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using LendersOffice.ConfigSystem.Operations;

    public class ParameterizedSystemConfigOperationMigrator : SystemConfigOperationMigrator
    {
        private readonly OperationAddition operationToAdd;
        public ParameterizedSystemConfigOperationMigrator(string operationId, string defaultValueString, string notes, string failureMessage)
        {
            bool defaultValue;
            if (!WorkflowOperations.Exists(operationId))
            {
                throw new InvalidOperationException($"Unable to locate operation [{operationId}]");
            }
            else if (!bool.TryParse(defaultValueString, out defaultValue))
            {
                throw new InvalidOperationException($"Unable to parse boolean parameter defaultValue from [{defaultValueString}]");
            }

            this.operationToAdd = new OperationAddition(
                WorkflowOperations.Get(operationId),
                defaultValue,
                notes,
                failureMessage);
        }

        protected override IEnumerable<OperationAddition> GetNewOperations()
        {
            Console.WriteLine(
                "Adding operation:" + Environment.NewLine
                + LendersOffice.Common.SerializationHelper.JsonBeautify(LendersOffice.Common.SerializationHelper.JsonNetAnonymousSerialize(this.operationToAdd)));

            return new[] { this.operationToAdd };
        }
    }
}
