﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Security;

namespace ScheduleExecutable.Migrations
{
    public class DetectTemplatedSpecialFhaPreparers : Migrator<Guid>
    {
        private readonly E_PreparerFormT[] specialFhaPreparerTypes = new[]
        {
            E_PreparerFormT.FHAAddendumUnderwriter,
            E_PreparerFormT.FHAAddendumMortgagee,
            E_PreparerFormT.FHA203kWorksheetUnderwriter,
            E_PreparerFormT.FHAAnalysisAppraisalUnderwriter,
            E_PreparerFormT.FHA92900LtUnderwriter
        };

        private Dictionary<Guid, string> customerCodeByBrokerId = new Dictionary<Guid, string>();

        public DetectTemplatedSpecialFhaPreparers()
            : base(nameof(DetectTemplatedSpecialFhaPreparers))
        {
            this.MigrationHelper = new LoanMigrationHelper(
                nameof(DetectTemplatedSpecialFhaPreparers),
                isTemplate: true,
                isValid: null,
                excludeQp: false);
        }

        public static void Run(string[] args)
        {
            var migrator = new DetectTemplatedSpecialFhaPreparers();
            migrator.Migrate();
        }

        protected override bool MigrationFunction(Guid loanId)
        {
            CPageData loan = null;

            try
            {
                loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    loanId,
                    typeof(DetectTemplatedSpecialFhaPreparers));
                loan.InitLoad();

                var modifiedPreparers = new List<object>();

                foreach (var preparerType in this.specialFhaPreparerTypes)
                {
                    var preparer = loan.GetPreparerOfForm(preparerType, E_ReturnOptionIfNotExist.ReturnEmptyObject);

                    if (preparer.ChumsIdLocked
                        || preparer.NameLocked
                        || !string.IsNullOrEmpty(preparer.ChumsId)
                        || !string.IsNullOrEmpty(preparer.PreparerName))
                    {
                        modifiedPreparers.Add(new
                        {
                            PreparerFormT = preparer.PreparerFormT.ToString(),
                            ChumsIdLocked = preparer.ChumsIdLocked,
                            ChumsId = preparer.ChumsId,
                            NameLocked = preparer.NameLocked,
                            PreparerName = preparer.PreparerName,
                        });
                    }
                }

                if (modifiedPreparers.Any())
                {
                    this.MigrationHelper.WriteToBackup(new
                    {
                        sLNm = loan.sLNm,
                        IsValid = loan.IsValid,
                        CustomerCode = this.GetCustomerCode(loan.sBrokerId),
                        ModifiedPreparers = modifiedPreparers
                    });
                }

                return true;
            }
            catch (LoanNotFoundException e)
            {
                Tools.LogWarning("[DetectTemplatedSpecialFhaPreparers] Unable to find loan " + loanId.ToString(), e);
                return false;
            }
            catch (AccessDenied e)
            {
                Tools.LogWarning("[DetectTemplatedSpecialFhaPreparers] Access denied for loan " + loanId.ToString(), e);
                return false;
            }
            catch (Exception e)
            {
                Tools.LogError("[DetectTemplatedSpecialFhaPreparers] Unexpected error for loan " + loanId.ToString(), e);
                return false;
            }
        }

        private string GetCustomerCode(Guid brokerId)
        {
            string customerCode = null;
            if (!this.customerCodeByBrokerId.TryGetValue(brokerId, out customerCode))
            {
                customerCode = BrokerDB.RetrieveNameAndCustomerCodeFromDb(brokerId).Item2;
                this.customerCodeByBrokerId.Add(brokerId, customerCode);
            }

            return customerCode;
        }
    }
}
