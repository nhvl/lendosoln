﻿namespace ScheduleExecutable.Migrations
{
    using CommonProjectLib.Common;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.ObjLib.StatusEvents;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Migrator for Touches Feature.
    /// </summary>
    public class TouchesMigrator : Migrator<Guid>
    {
        private static string fileName;

        /// <summary>
        /// Initializes a new instance of the <see cref="TouchesMigrator"/> class.
        /// </summary>
        public TouchesMigrator() : base(nameof(TouchesMigrator))
        {
            this.MigrationHelper = new CustomGUIDMigrationHelper(nameof(TouchesMigrator), () => TextFileHelper.ReadLines(fileName).Select((stringId) => Guid.Parse(stringId)));
        }

        public static void RunTouchesMigrator(string[] args)
        {
            Console.ReadLine();

            if (args.Length != 2)
            {
                Console.WriteLine("Usage: ScheduleExecutable RunTouchesMigrator [FileName]");
                return;
            }

            fileName = args[1];

            TouchesMigrator migrator = new TouchesMigrator();

            migrator.Migrate();
        }

        protected override bool MigrationFunction(Guid id)
        {
            try
            {
                StatusEvent.MigrateStatusEvents(id);
                return true;
            }
            catch (CBaseException e)
            {
                DataAccess.Tools.LogError("Error migrating slid <" + id.ToString() + "> for Touches Migrator.", e);
                return false;
            }
        }
    }
}
