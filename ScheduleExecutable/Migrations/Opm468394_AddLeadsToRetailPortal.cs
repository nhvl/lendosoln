﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Linq;

    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.ObjLib.PMLNavigation;
    using LqbGrammar.DataTypes;

    public class Opm468394_AddLeadsToRetailPortal : Migrator<Guid>
    {
        public Opm468394_AddLeadsToRetailPortal()
            : base(nameof(Opm468394_AddLeadsToRetailPortal))
        {
            this.MigrationHelper = new BrokerMigrationHelper(this.Name);
        }

        public static void Run(string[] args)
        {
            var migrator = new Opm468394_AddLeadsToRetailPortal();
            migrator.Migrate();
        }

        protected override bool MigrationFunction(Guid id)
        {
            try
            {
                BrokerDB broker = BrokerDB.RetrieveById(id);
                NavigationConfiguration navConfig = broker.GetPmlNavigationConfiguration(null);  // Note: This auto adds new nodes to end.
                FolderNode rootNode = navConfig.RootNode;

                // Find, remove, and reinsert Create New Lead Folder in correct place.
                FolderNode createLeadFolder = rootNode.Children.FirstOrDefault(n => n.Id == NavigationConfiguration.SystemNode.CreateLeadFolder) as FolderNode;
                rootNode.Children.Remove(createLeadFolder);

                int creatLoanFolderIndex = rootNode.Children.IndexOf(rootNode.Children.FirstOrDefault(n => n.Id == NavigationConfiguration.SystemNode.CreateLoanFolder));
                rootNode.Children.Insert(creatLoanFolderIndex, createLeadFolder);   // Insert BEFORE Create New Loan folder.

                // Find, remove, and reinsert Lead Pipeline in correct place.
                FolderNode pipelineFolder = rootNode.Children.FirstOrDefault(n => n.Id == NavigationConfiguration.SystemNode.PipelineFolder) as FolderNode;
                NavigationNode leadsNode = pipelineFolder.Children.FirstOrDefault(n => n.Id == NavigationConfiguration.SystemNode.Leads);
                pipelineFolder.Children.Remove(leadsNode);

                int loansNodeIndex = pipelineFolder.Children.IndexOf(pipelineFolder.Children.FirstOrDefault(n => n.Id == NavigationConfiguration.SystemNode.Loans));
                pipelineFolder.Children.Insert(loansNodeIndex + 1, leadsNode);  // Insert After Loans pipeline.

                broker.SetPmlNavigationConfiguration(null, navConfig);
                broker.Save();
            }
            catch (Exception e)
            {
                string msg = $"[Opm468394_AddLeadsToRetailPortal] Migration failed for BrokerId=[{id}].";
                Tools.LogError(msg, e);
                return false;
            }

            return true;
        }
    }
}
