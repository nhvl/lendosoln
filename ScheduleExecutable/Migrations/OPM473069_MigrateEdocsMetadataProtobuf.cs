﻿using System;
using System.IO;

namespace ScheduleExecutable.Migrations
{
    /// <summary>
    /// Case 473069 - Migrate metadata from FileDB to SQL.
    /// This migration is not optimize for speed. It use the EdocsDocument.Save to perform migration. 
    /// 8/14/2018 - dd - Once all metadata store in SQL we can remove this migration.
    /// </summary>
    public class OPM473069_MigrateEdocsMetadataProtobuf
    {
        public static void Execute(string[] args)
        {
            if (args.Length != 2)
            {
                Console.WriteLine("Usage: ScheduleExecutable ScheduleExecutable.Migrations.OPM473069_MigrateEdocsMetadataProtobuf::Execute {brokerid} {loanid.txt}.");
                return;
            }

            Guid brokerId = new Guid(args[0]);
            string[] loanList = File.ReadAllLines(args[1]);

            var repository = EDocs.EDocumentRepository.GetSystemRepository(brokerId);
            int currentCount = 0;
            foreach (var id in loanList)
            {
                if (string.IsNullOrEmpty(id))
                {
                    continue;
                }

                Guid loanId = new Guid(id);
                var documentList = repository.GetDocumentsByLoanId(loanId);

                if (documentList != null)
                {
                    foreach (var document in documentList)
                    {
                        repository.Save(document);
                    }
                    Console.WriteLine($"{DateTime.Now}. Processed {currentCount} out of {loanList.Length} loans.");
                }
                currentCount++;

            }
        }
    }
}
