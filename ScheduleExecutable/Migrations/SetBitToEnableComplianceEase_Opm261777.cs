﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using DataAccess;
    using LendersOffice.Admin;

    /// <summary>
    /// We are adding a new bit field to enable ComplianceEase rather than having it enabled
    /// for all vendors. This migration will enable the bit for any broker that has ComplianceEase
    /// credentials stored at the broker-level or the user-level.
    /// </summary>
    public class SetBitToEnableComplianceEase_Opm261777: Migrator<Guid>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SetBitToEnableComplianceEase_Opm261777"/> class.
        /// </summary>
        /// <param name="name">The migration name.</param>
        public SetBitToEnableComplianceEase_Opm261777(string name)
            : base(name)
        {
            this.MigrationHelper = new CustomGUIDMigrationHelper(name, RetrieveBrokersWithComplianceEaseCredentials);
        }

        /// <summary>
        /// Triggers the migration to run.
        /// </summary>
        /// <param name="args">A set of string arguments.</param>
        public static void RunMigration(string[] args)
        {
            var migrator = new SetBitToEnableComplianceEase_Opm261777(nameof(SetBitToEnableComplianceEase_Opm261777));
            migrator.Migrate();
        }

        /// <summary>
        /// Retrieves all unique broker IDs where a ComplianceEase username is stored at either the
        /// broker-level or the user-level.
        /// </summary>
        /// <returns>A list of brokers that should have ComplianceEase enabled.</returns>
        public static IEnumerable<Guid> RetrieveBrokersWithComplianceEaseCredentials()
        {
            var brokerIds = new List<Guid>();
            var sql = @"
                SELECT DISTINCT bro.BrokerId 
                FROM BROKER_USER bu
                    JOIN EMPLOYEE emp ON bu.EmployeeId = emp.EmployeeId
                    JOIN BRANCH bra ON emp.BranchId = bra.BranchId
                    JOIN BROKER bro ON bra.BrokerId = bro.BrokerId
                WHERE bro.Status = 1
                    AND emp.IsActive = 1
                    AND bro.IsEnableComplianceEaseIntegration = 0
                    AND (DATALENGTH(bro.ComplianceEaseUserName) > 0
                    OR DATALENGTH(bu.ComplianceEaseUserName) > 0)";

            Action<IDataReader> readHandler = delegate (IDataReader reader)
            {
                while (reader.Read())
                {
                    brokerIds.Add((Guid)reader["BrokerId"]);
                }
            };

            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                DBSelectUtility.ProcessDBData(connInfo, sql, null, null, readHandler);
            }

            return brokerIds;
        }

        /// <summary>
        /// Enables the ComplianceEase integration for a given broker.
        /// </summary>
        /// <param name="id">The broker to migrate.</param>
        /// <returns>A boolean indicating whether the migration was successful.</returns>
        protected override bool MigrationFunction(Guid id)
        {
            try
            {
                BrokerDB broker = BrokerDB.RetrieveById(id);
                broker.IsEnableComplianceEaseIntegration = true;
                if (broker.Save())
                {
                    return true;
                }

                throw new Exception();
            }
            catch (Exception)
            {
                Tools.LogError($"Failed to enable ComplianceEase integration for broker {id.ToString()}.");
            }

            return false;
        }
    }
}
