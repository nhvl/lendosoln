﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Data;
    using System.Data.SqlClient;
    using System.Security.Cryptography;
    using DataAccess;
    using Adapter;

    class Opm281099_PopulateFileDbEncryptionKeys
    {
        public static void Execute(string[] args)
        {
            byte[] key = new byte[32];
            SqlParameter[] parameters = new SqlParameter[2];
            using (var conn = DbAccessUtils.GetConnection(DataSrc.LOShare))
            {
                conn.OpenWithRetry();

                int count = 0;
                Action<IDataReader> readHandler = delegate(IDataReader reader)
                {
                    if (reader.Read())
                    {
                        count = Convert.ToInt32(reader["Total"]);
                    }
                };

                DBSelectUtility.ProcessDBData(conn, null, "SELECT COUNT(*) AS Total FROM [dbo].[FILE_DB_ENCRYPTION_KEYS] WITH(NOLOCK);", null, null, readHandler);
                if (count > 0) return; // this process has already run successfully

                using (var random = RNGCryptoServiceProvider.Create())
                {
                    for (int i = 0; i <= 0xFFFF; ++i)
                    {
                        random.GetBytes(key);

                        string base64 = Convert.ToBase64String(key);

                        parameters[0] = new SqlParameter("@id", i);
                        parameters[1] = DbAccessUtils.SqlParameterForVarchar("@key", base64);

                        DBInsertUtility.InsertNoKey(conn, null, "INSERT INTO [dbo].[FILE_DB_ENCRYPTION_KEYS] ([Id], [Key]) VALUES (@id, @key)", null, parameters);
                    }
                }
            }
        }
    }
}
