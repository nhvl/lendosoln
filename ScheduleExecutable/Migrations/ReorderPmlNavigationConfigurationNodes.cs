﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Linq;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.PMLNavigation;

    public class ReorderPmlNavigationConfigurationNodes : Migrator<Guid>
    {
        private readonly Guid nodeIdToMove;

        private readonly Guid nodeIdToBeAfter;

        public ReorderPmlNavigationConfigurationNodes(Guid nodeIdToMove, Guid nodeIdToBeAfter)
            : base(nameof(ReorderPmlNavigationConfigurationNodes) + "(Move [" + nodeIdToMove + "] to be after [" + nodeIdToBeAfter + "])")
        {
            this.nodeIdToMove = nodeIdToMove;
            this.nodeIdToBeAfter = nodeIdToBeAfter;
            this.MigrationHelper = new BrokerMigrationHelper(this.Name);
        }

        public static void MoveNodeBeforeOther(string[] args)
        {
            if (args?.Length != 2)
            {
                Console.WriteLine("Aborting. Please pass two arguments, like \"{NodeIdToMove}\" \"{NodeIdToBeAfter}\"");
                return;
            }

            Guid? nodeIdToMove = args[0].ToNullable<Guid>(Guid.TryParse);
            Guid? nodeIdToBeAfter = args[1].ToNullable<Guid>(Guid.TryParse);
            Console.WriteLine($"MoveNodeBeforeOther:  NodeIdToMove: {nodeIdToMove?.ToString() ?? "null"}, NodeIdToBeAfter: {nodeIdToBeAfter?.ToString() ?? "null"}");
            if (!nodeIdToMove.HasValue || !nodeIdToBeAfter.HasValue)
            {
                Console.WriteLine("Aborting because an arument failed to parse as a guid.");
                return;
            }

            var migrator = new ReorderPmlNavigationConfigurationNodes(nodeIdToMove.Value, nodeIdToBeAfter.Value);
            migrator.Migrate();
        }

        protected override bool MigrationFunction(Guid id)
        {
            try
            {
                BrokerDB broker = BrokerDB.RetrieveById(id);
                NavigationConfiguration navConfig = broker.GetPmlNavigationConfiguration(null);  // Note: This auto adds new nodes to end.

                Tuple<NavigationNode, FolderNode> foundNodeAndParent = FindNodeById(navConfig.RootNode, this.nodeIdToMove);
                if (foundNodeAndParent == null)
                {
                    return true; // Node to move not found; no action to take
                }

                NavigationNode nodeToMove = foundNodeAndParent.Item1;
                FolderNode existingParentNode = foundNodeAndParent.Item2;
                existingParentNode.Children.Remove(nodeToMove);

                foundNodeAndParent = FindNodeById(navConfig.RootNode, this.nodeIdToBeAfter);
                if (foundNodeAndParent == null)
                {
                    return true; // Node target not found; no action to take
                }

                NavigationNode nodeBeforeTarget = foundNodeAndParent.Item1;
                FolderNode targetParentNode = foundNodeAndParent.Item2;

                int indexOfNodeBefore = targetParentNode.Children.IndexOf(nodeBeforeTarget);
                targetParentNode.Children.Insert(indexOfNodeBefore + 1, nodeToMove);

                broker.SetPmlNavigationConfiguration(null, navConfig);
                broker.Save();
                return true;
            }
            catch (Exception e)
            {
                DataAccess.Tools.LogError("ReorderTPONavigationConfiguration failed for BrokerId=[" + id + "]", e);
                return false;
            }
        }

        /// <summary>
        /// Recursively searches for the node with the specified <paramref name="id"/>.
        /// </summary>
        /// <param name="folder">The folder to search.</param>
        /// <param name="id">The identifier of the folder to find.</param>
        /// <returns>The node and its parent, or null if not found.</returns>
        private static Tuple<NavigationNode, FolderNode> FindNodeById(FolderNode folder, Guid id)
        {
            NavigationNode node = folder.Children.FirstOrDefault(n => n.Id == id);
            if (node != null)
            {
                return Tuple.Create(node, folder);
            }

            foreach (FolderNode subfolder in folder.Children.Where(n => n is FolderNode))
            {
                var foundNode = FindNodeById(subfolder, id);
                if (foundNode != null)
                {
                    return foundNode;
                }
            }

            return null;
        }
    }
}
