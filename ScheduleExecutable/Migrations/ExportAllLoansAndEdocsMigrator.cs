﻿// <copyright file="ExportAllLoansAndEdocsMigrator.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//      Author: Jhairo Erazo
//      Date: 12/12/2016
// </summary>
namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using DataAccess;
    using EDocs;
    using LendersOffice.Admin;
    using LendersOffice.Conversions.Mismo3.Version3;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.Integration.DocumentVendor;
    using LendersOffice.Migration;

    /// <summary>
    /// Used to export all VALID loan files and their attached Docs for a single Broker.
    /// </summary>
    class ExportAllLoansAndEdocsMigrator : Migrator<Guid>
    {
        /// <summary>
        /// Broker ID.
        /// </summary>
        private Guid brokerId;

        /// <summary>
        /// A BrokerDB object.
        /// </summary>
        private BrokerDB brokerDb;

        /// <summary>
        /// Broker Customer Code.
        /// </summary>
        private string customerCode;

        /// <summary>
        /// Output folder path.
        /// </summary>
        private string path;

        /// <summary>
        /// Log Writer.
        /// </summary>
        private TextFileHelper.LqbTextWriter log;

        /// <summary>
        /// DocType Name cache.
        /// </summary>
        private Dictionary<int, string> docTypeNamesById; 

        /// <summary>
        /// How long should filedb sleepfor? 
        /// </summary>
        private int? fileDbSleepTime; 

        /// <summary>
        /// Instantiates the migrator.
        /// </summary>
        /// <param name="name">Name used for log files.</param>
        /// <param name="customerCode">Broker customer code.</param>
        /// <param name="brokerId">Broker ID.</param>
        public ExportAllLoansAndEdocsMigrator(string name, string customerCode, Guid brokerId, int? fileDBSleepTime)
            :base(name)
        {
            this.brokerId = brokerId;
            this.customerCode = customerCode;
            this.path = Path.Combine(Directory.GetCurrentDirectory(), customerCode);

            this.docTypeNamesById = new Dictionary<int, string>();
            var docTypes = EDocumentDocType.GetDocTypesByBroker(brokerId, false);
            var invalidChars = Path.GetInvalidFileNameChars();

            foreach (DocType doctype in docTypes)
            {
                string cleanDocType = new string(doctype.FolderAndDocTypeName.Where(x => !invalidChars.Contains(x)).ToArray());
                this.docTypeNamesById.Add(doctype.Id, cleanDocType.Replace(' ', '_'));
            }

            this.brokerDb = BrokerDB.RetrieveById(brokerId);
            this.fileDbSleepTime = fileDBSleepTime;
            this.MigrationHelper = new CustomGUIDMigrationHelper(name, () => MigrationUtils.GetAllLoansUsingGivenBrokers(false, true, true, new Guid[] { this.brokerId }));
        }

        /// <summary>
        /// Exports all loan files and their associated EDocs for the given broker.
        /// </summary>
        /// <param name="args">Either a broker ID or a customer code.</param>
        /// <remarks>
        /// Creates a folder named after the broker's customer code. Then creates a subfolder for each loan file,
        /// dumping a MISMO export of the loan into that folder and another subfolder called "edocs" containing all that loans EDocs as PDFs.
        /// </remarks>
        public static void ExportAllLoansAndEdocs(string[] args)
        {
            if (args.Length < 2)
            {
                Console.WriteLine("A Broker ID or Customer Code must be passed in as an argument.");
                return;
            }

            Guid brokerId;
            string customerCode;

            // Check if argument is a Guid.
            if (Guid.TryParse(args[1], out brokerId))
            {
                // Found BrokerId, now get customer code.
                Tuple<string, string> nameAndCc = BrokerDB.RetrieveNameAndCustomerCodeFromDb(brokerId);
                customerCode = nameAndCc.Item2;
            }
            else
            {
                // Assume argument is customer code. Use to find Broker ID.
                customerCode = args[1];
                brokerId = Tools.GetBrokerIdByCustomerCode(customerCode);
            }

            int? sleepTime = new int?(); 
            int val; 
            if (args.Length == 3 && int.TryParse(args[2], out val))
            {
                sleepTime = val;
                Console.WriteLine($"Will sleep {val}ms between filedb calls.");
            }

            string name = $"ExportAllLoansAndEdocs_{customerCode}";
            ExportAllLoansAndEdocsMigrator migrator = new ExportAllLoansAndEdocsMigrator(name, customerCode, brokerId, sleepTime);
            migrator.Migrate();
        }

        /// <summary>
        /// Creates a folder to dump migration info into.
        /// </summary>
        protected override void BeginMigration()
        {
            Directory.CreateDirectory(path);

            // Create log file (NOTE: This has to be done after directory is created).
            string logPath = Path.Combine(path, "log.txt");
            bool isResume = FileOperationHelper.Exists(logPath);

            this.log = TextFileHelper.OpenAppend_UNSAFE(logPath);
            this.log.AutoFlush = true;

            if (isResume)
            {
                this.log.AppendLine(string.Empty); // Empty line between loan files to make logs easier to read.
                this.log.AppendLine("Resuming from previous export attempt. Please look at preceding log items.");
            }

            this.log.AppendLine($"ExportAllLoansAndEdocs Start. BrokerId=[{this.brokerId}]. CustomerCode=[{this.customerCode}]. Start time=[{DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")}].");

            this.log.AppendLine(string.Empty); // Empty line between loan files to make logs easier to read.
        }

        /// <summary>
        /// Closes stream for writing log.
        /// </summary>
        protected override void EndMigration()
        {
            this.log.AppendLine($"ExportAllLoansAndEdocs Finished. BrokerId=[{this.brokerId}]. CustomerCode=[{this.customerCode}]. End time=[{DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")}].");

            this.log.Flush();
            this.log.Close();
        }

        /// <summary>
        /// Migration function. Exports the loan to a MISMO file, then exports it's EDocs to PDF.
        /// </summary>
        /// <param name="id">A Loan ID.</param>
        /// <returns>Bool indicating if export was successful.</returns>
        protected override bool MigrationFunction(Guid id)
        {
            try
            {
                this.log.AppendLine($"Processing Loan with LoanId=[{id}]");

                // Retrieve loan from DB.
                CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(id, typeof(ExportAllLoansAndEdocsMigrator));
                dataLoan.InitLoad();

                // Create folder for loan.
                string loanPath = Path.Combine(this.path, dataLoan.sLNm);
                Directory.CreateDirectory(loanPath);

                // Export MISMO.
                bool success = this.ExportMismo33(dataLoan, loanPath);

                // Export Edocs.
                success &= this.ExportEdocs(id, loanPath);

                // Log success.
                if (success)
                {
                    this.log.AppendLine($"Successfully processed Loan with LoanId=[{id}] LoanName=[{dataLoan.sLNm}].");
                }
                else
                {
                    string msg = $"Processing Loan with LoanId=[{id}] LoanName=[{dataLoan.sLNm}] completed with SOME ISSUES.";
                    this.log.AppendLine(msg + " Please look at preceding log items.");
                    Console.WriteLine(msg + " See log for more details.");
                }

                this.log.AppendLine(string.Empty); // Empty line between loan files to make logs easier to read.

                return true;
            }
            catch (Exception e)
            {
                string msg = $"FAILED to process Loan with LoanId=[{id}].";
                this.log.AppendLine(msg);
                this.log.AppendLine(e.ToString());
                this.log.AppendLine(string.Empty); // Empty line between loan files to make logs easier to read.

                this.log.AppendLine(msg + " See log for more details.");

                return false;
            }            
        }

        /// <summary>
        /// Exports all EDocs for a loan.
        /// </summary>
        /// <param name="loanId">The Loan ID.</param>
        /// <param name="loanPath">Directory to output file to.</param>
        /// <returns>Bool indicating if export was successful.</returns>
        private bool ExportEdocs(Guid loanId, string loanPath)
        {
            bool success = true;

            try
            {
                // Create EDocs Folder.
                string edocPath = Path.Combine(loanPath, "edoc");
                Directory.CreateDirectory(edocPath);

                // Fetch EDocs
                EDocumentRepository repository = EDocumentRepository.GetSystemRepository(this.brokerId);
                foreach (EDocument doc in repository.GetDocumentsByLoanId(loanId))
                {
                    try
                    {
                        string tempPath;
                        try
                        {
                            tempPath = doc.GetPDFTempFile_Current();
                        }
                        catch (FileNotFoundException)
                        {
                            tempPath = doc.GetPDFTempFile_Original();
                        }

                        string finalPath = Path.Combine(edocPath, $"{this.docTypeNamesById[doc.DocumentTypeId]}_{doc.DocumentId.ToString("N")}_{doc.LastModifiedDate.ToString("yyyyMMddHHmmss")}.pdf");

                        if (FileOperationHelper.Exists(finalPath))
                        {
                            FileOperationHelper.Delete(finalPath);
                        }

                        FileOperationHelper.Move(tempPath, finalPath);
                        if (this.fileDbSleepTime.HasValue)
                        {
                            Thread.Sleep(this.fileDbSleepTime.Value);
                        }
                    }
                    catch (Exception e)
                    {
                        success = false;

                        this.log.AppendLine($"FAILED to export EDocs. LoanId=[{loanId}]. DocTypeName=[{doc.DocTypeName}].");
                        this.log.AppendLine(e.ToString());
                    }
                }
            }
            catch (Exception e)
            {
                this.log.AppendLine($"FAILED to export EDocs due to unexpected exception for Loan with LoanId=[{loanId}].");
                this.log.AppendLine(e.ToString());

                return false;
            }

            return success;
        }

        /// <summary>
        /// Exports loan to MISMO 3.3 XML.
        /// </summary>
        /// <param name="dataLoan">Loan data.</param>
        /// <param name="loanPath">Directory to output file to.</param>
        /// <returns>Bool indicating if export was successful.</returns>
        private bool ExportMismo33(CPageData dataLoan, string loanPath)
        {
            try
            {
                var disclosedArchives = dataLoan.sClosingCostArchive.ToDictionary(
                                a => a.Id,
                                a => a.IsDisclosed);

                bool validClosingDisclosureOnFile =
                    dataLoan.sClosingDisclosureDatesInfo.ClosingDisclosureDatesList.Any(
                        cd => cd.ArchiveId == Guid.Empty
                        || (disclosedArchives.ContainsKey(cd.ArchiveId) && disclosedArchives[cd.ArchiveId]));

                VendorConfig.DocumentPackage documentPackage = VendorConfig.DocumentPackage.InvalidPackage;
                if (validClosingDisclosureOnFile)
                {
                    documentPackage = new VendorConfig.DocumentPackage(
                        "Generic Document Package",
                        VendorConfig.PackageType.Closing,
                        hasGFE: false,
                        hasTIL: false,
                        hasLoanEstimate: false,
                        hasClosingDisclosure: true,
                        triggersPtmBilling: false);
                }
                else
                {
                    bool validLoanEstimateOnFile =
                        dataLoan.sLoanEstimateDatesInfo.LoanEstimateDatesList.Any(
                            le => le.ArchiveId == Guid.Empty
                            || (disclosedArchives.ContainsKey(le.ArchiveId) && disclosedArchives[le.ArchiveId]));

                    documentPackage = new VendorConfig.DocumentPackage(
                        "Generic Document Package",
                        validLoanEstimateOnFile ? VendorConfig.PackageType.Redisclosure : VendorConfig.PackageType.Initial,
                        hasGFE: false,
                        hasTIL: false,
                        hasLoanEstimate: true,
                        hasClosingDisclosure: false,
                        triggersPtmBilling: false);
                }

                string xml = Mismo33RequestProvider.SerializeMessage(
                    loanIdentifier: dataLoan.sLId,
                    vendorAccountID: null,
                    transactionID: Guid.NewGuid().ToString(),
                    docPackage: documentPackage,
                    includeIntegratedDisclosureExtension: true,
                    principal: null,
                    vendor: brokerDb.Mismo33SidebarLinkDocumentVendor,
                    useMismo3DefaultNamespace: true,
                    isLenderExportMode: true);

                string filePath = Path.Combine(loanPath, dataLoan.sLNm + ".xml");
                TextFileHelper.WriteString(filePath, xml, false);

                return true;
            }
            catch (Exception e)
            {
                this.log.AppendLine($"FAILED to export MISMO 3.3 for Loan with LoanId=[{dataLoan.sLId}] LoanName=[{dataLoan.sLNm}].");
                this.log.AppendLine(e.ToString());
                this.log.AppendLine(string.Empty);
                this.log.AppendLine("Stack Trace:");
                this.log.AppendLine(e.StackTrace);

                return false;
            }
        }
    }
}
