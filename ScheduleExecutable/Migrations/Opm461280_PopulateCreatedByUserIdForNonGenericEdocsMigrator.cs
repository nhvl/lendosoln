﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using DataAccess;
    using EDocs;

    /// <summary>
    /// Populates the "Created By User Id" column for non-generic edocs.
    /// </summary>
    public class Opm461280_PopulateCreatedByUserIdForNonGenericEdocsMigrator
    {
        /// <summary>
        /// Tracks the CreatedDate date/time where we should start pulling document rows
        /// to build the next batch.
        /// </summary>
        private DateTime startBatchAtDate;

        /// <summary>
        /// Runs the migration.
        /// </summary>
        /// <param name="args">
        /// The parameter is not used.
        /// </param>
        public static void Run(string[] args)
        {
            var migrator = new Opm461280_PopulateCreatedByUserIdForNonGenericEdocsMigrator();
            migrator.Migrate();
        }

        /// <summary>
        /// Runs the migration.
        /// </summary>
        private void Migrate()
        {
            const string Sql = @"
                UPDATE TOP(1) EDOCS_DOCUMENT
                SET CreatedByUserId = @CreatedByUserId
                WHERE BrokerId = @BrokerId AND DocumentId = @DocumentId
                ";

            foreach (var connectionInfo in DbConnectionInfo.ListAll())
            {
                // DateTime.MinValue underflows the SQL datetime, so we'll start at a more recent date
                // that should nonetheless occur before any loans in our system.
                this.startBatchAtDate = DateTime.Parse("1/1/1900");

                IEnumerable<CreatedByUserData> documentBatch;
                do
                {
                    documentBatch = this.GetNextDocumentBatch(connectionInfo);

                    foreach (var documentToMigrate in documentBatch)
                    {
                        try
                        {
                            SqlParameter[] parameters =
                            {
                                new SqlParameter("@BrokerId", documentToMigrate.BrokerId),
                                new SqlParameter("@CreatedByUserId", documentToMigrate.CreatedByUserId),
                                new SqlParameter("@DocumentId", documentToMigrate.DocumentId)
                            };

                            DBUpdateUtility.Update(documentToMigrate.BrokerId, Sql, LqbGrammar.DataTypes.TimeoutInSeconds.Thirty, parameters);
                        }
                        catch (Exception exc)
                        {
                            var message = $"Failed to migrate doc {documentToMigrate.DocumentId} to add " +
                                $"user ID {documentToMigrate.CreatedByUserId} for broker {documentToMigrate.BrokerId}: {exc}";

                            Console.WriteLine(message);
                            Tools.LogInfo(nameof(Opm461280_PopulateCreatedByUserIdForNonGenericEdocsMigrator), message);
                        }
                    }

                } while (documentBatch.Any());
            }
        }

        /// <summary>
        /// Gets the list of documents to migrate.
        /// </summary>
        /// <param name="connection">The DB connection to use.</param>
        /// <returns>
        /// The list of documents to migrate.
        /// </returns>
        private IEnumerable<CreatedByUserData> GetNextDocumentBatch(DbConnectionInfo connection)
        {
            var unprocessedDocumentsByBrokerId = new Dictionary<Guid, LinkedList<Guid>>();

            const string Sql = @"
                SELECT TOP 10000 BrokerId, DocumentId, CreatedDate
                FROM EDOCS_DOCUMENT
                WHERE IsValid = 1
                    AND CreatedByUserId IS NULL
                    AND CreatedDate >= @StartBatchAtDate
                ORDER BY CreatedDate
                ";

            Action<IDataReader> readerCode = reader =>
            {
                while (reader.Read())
                {
                    var brokerId = (Guid)reader["BrokerId"];

                    LinkedList<Guid> documentIds;
                    if (!unprocessedDocumentsByBrokerId.TryGetValue(brokerId, out documentIds))
                    {
                        documentIds = new LinkedList<Guid>();
                        unprocessedDocumentsByBrokerId.Add(brokerId, documentIds);
                    }

                    var documentCreatedDate = (DateTime)reader["CreatedDate"];
                    if (documentCreatedDate > this.startBatchAtDate)
                    {
                        this.startBatchAtDate = documentCreatedDate;
                    }

                    documentIds.AddLast((Guid)reader["DocumentId"]);
                }
            };

            SqlParameter[] parameters =
            {
                new SqlParameter("@StartBatchAtDate", this.startBatchAtDate)
            };

            DBSelectUtility.ProcessDBData(
                connection,
                Sql,
                LqbGrammar.DataTypes.TimeoutInSeconds.Thirty,
                parameters: parameters,
                readerCode: readerCode);

            return this.ProcessDocumentIds(unprocessedDocumentsByBrokerId);
        }

        /// <summary>
        /// Processes the documents supplied by scanning the audit history for the oldest
        /// audit item associated with a user.
        /// </summary>
        /// <param name="documentListByBrokerId">
        /// The unprocessed document list.
        /// </param>
        /// <returns>
        /// The created by user data for documents with an audit history item associated
        /// with a modifying user.
        /// </returns>
        private IEnumerable<CreatedByUserData> ProcessDocumentIds(Dictionary<Guid, LinkedList<Guid>> documentListByBrokerId)
        {
            var documentList = new LinkedList<CreatedByUserData>();

            foreach (var brokerIdDocumentPair in documentListByBrokerId)
            {
                foreach (var documentId in brokerIdDocumentPair.Value)
                {
                    try
                    {
                        var auditHistory = EDocumentAuditEvent.RetrieveAuditEventsForDoc(brokerIdDocumentPair.Key, documentId);
                        var oldestAuditEntry = auditHistory
                            .Where(auditEvent => auditEvent.UserId.HasValue)
                            .OrderBy(auditEvent => auditEvent.ModifiedDate)
                            .FirstOrDefault();

                        if (oldestAuditEntry != null)
                        {
                            documentList.AddLast(new CreatedByUserData()
                            {
                                BrokerId = brokerIdDocumentPair.Key,
                                DocumentId = documentId,
                                CreatedByUserId = oldestAuditEntry.UserId.Value
                            });
                        }
                    }
                    catch (Exception exc)
                    {
                        var message = $"Failed to process document {brokerIdDocumentPair.Value} for broker {brokerIdDocumentPair.Key}, skipping: {exc}";
                        Console.WriteLine(message);
                        Tools.LogInfo(nameof(Opm461280_PopulateCreatedByUserIdForNonGenericEdocsMigrator), message);
                    }
                }
            }

            return documentList;
        }

        /// <summary>
        /// Provides a simple container for created by user data.
        /// </summary>
        private class CreatedByUserData
        {
            /// <summary>
            /// Gets or sets the ID of the broker for the document.
            /// </summary>
            /// <value>
            /// The ID of the broker for the document.
            /// </value>
            public Guid BrokerId { get; set; }

            /// <summary>
            /// Gets or sets the ID of the user that created the document.
            /// </summary>
            /// <value>
            /// The ID of the user that created the document.
            /// </value>
            public Guid CreatedByUserId { get; set; }

            /// <summary>
            /// Gets or sets the ID of the document.
            /// </summary>
            /// <value>
            /// The ID of the document.
            /// </value>
            public Guid DocumentId { get; set; }
        }
    }
}
