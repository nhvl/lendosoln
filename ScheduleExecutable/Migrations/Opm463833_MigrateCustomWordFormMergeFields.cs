﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.CustomMergeFields;
    using LendersOffice.Drivers.FileSystem;
    using LqbGrammar.DataTypes;
    using DocumentFormat.OpenXml;
    using DocumentFormat.OpenXml.Packaging;
    using DocumentFormat.OpenXml.Wordprocessing;
    using System.Text.RegularExpressions;

    /// <summary>
    /// Migrates merge fields on custom word forms.
    /// </summary>
    public class Opm463833_MigrateCustomWordFormMergeFields
    {
        private const string XmlNamespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
        private const string MergeFieldMarker = "MERGEFIELD";

        private HashSet<string> lqbMergeFieldIds;

        public static void PullFiles(string[] args)
        {
            if (args.Length != 1)
            {
                throw new InvalidOperationException("Must specify migration directory.");
            }

            var migrator = new Opm463833_MigrateCustomWordFormMergeFields();
            migrator.PullFilesFromFileDb(args[0]);
        }

        public static void MigrateFields(string[] args)
        {
            if (args.Length != 1)
            {
                throw new InvalidOperationException("Must specify migration directory.");
            }

            var migrator = new Opm463833_MigrateCustomWordFormMergeFields();
            migrator.MigrateMergeFields(args[0]);
        }

        public void PullFilesFromFileDb(string migrationDirectory)
        {
            foreach (var fileDbKey in this.RetrieveCustomWordFormFileDbKeys())
            {
                try
                {
                    FileDBTools.UseFile(E_FileDB.Normal, fileDbKey, fileInfo =>
                    {
                        var destinationFileName = Path.Combine(migrationDirectory, fileDbKey + ".unmigrated");
                        fileInfo.CopyTo(destinationFileName);
                    });
                }
                catch (Exception e)
                {
                    Tools.LogError("[OPM 463833] Failed to pull file with FileDB key " + fileDbKey, e);
                }
            }
        }

        public void MigrateMergeFields(string migrationDirectory)
        {
            lqbMergeFieldIds = new HashSet<string>(CustomMergeFieldRepository.FieldIdsByWordCode.Keys, StringComparer.OrdinalIgnoreCase);

            foreach (var file in Directory.GetFiles(migrationDirectory, "*.docx"))
            {
                try
                {
                    this.MigrateMergeFieldsForFile(file);
                    this.WriteFileToFileDb(file);
                }
                catch (Exception e)
                {
                    Tools.LogError("[OPM 463833] Failed to migrate file " + file, e);
                }
            }
        }

        private IEnumerable<string> RetrieveCustomWordFormFileDbKeys()
        {
            var fileDbKeys = new LinkedList<string>();

            const string Sql = "SELECT FileDBKey FROM CUSTOM_LETTER";
            Action<IDataReader> readerCode = reader =>
            {
                while (reader.Read())
                {
                    fileDbKeys.AddLast(reader["FileDBKey"].ToString());
                }
            };

            foreach (var connectionInfo in DbConnectionInfo.ListAll())
            {
                DBSelectUtility.ProcessDBData(connectionInfo, Sql, TimeoutInSeconds.Thirty, null, readerCode);
            }
            
            return fileDbKeys;
        }

        private void MigrateMergeFieldsForFile(string file)
        {
            using (var document = WordprocessingDocument.Open(file, isEditable: true))
            {
                var mainDocument = document.MainDocumentPart.Document;

                foreach (var element in mainDocument.Descendants())
                {
                    this.MigrateMergeFieldsByElementType(element);
                }
            }
        }

        private void MigrateMergeFieldsByElementType(OpenXmlElement element)
        {
            var descendantsToRemove = new List<OpenXmlElement>();

            foreach (var descendant in element.Descendants())
            {
                if (descendant is FieldChar)
                {
                    var fieldChar = (FieldChar)descendant;

                    var charFieldType = fieldChar.GetAttribute("fldCharType", XmlNamespace).Value;
                    if (charFieldType.EqualsOneOf("begin", "end", "separate"))
                    {
                        descendantsToRemove.Add(fieldChar);
                    }

                    continue;
                }

                if (descendant is Text)
                {
                    var textField = (Text)descendant;

                    // We want to maintain any whitespace present in the text field
                    // to ensure we don't push text together when replacing the content.
                    var lqbTextContent = Regex.Replace(textField.Text, @"[«|»]", string.Empty);
                    if (lqbMergeFieldIds.Contains(lqbTextContent.Trim()))
                    {
                        textField.Text = $"«{lqbTextContent}»";
                    }

                    continue;
                }

                if (descendant is FieldCode)
                {
                    var fieldCode = (FieldCode)descendant;
                    if (fieldCode.InnerText.Contains(MergeFieldMarker, StringComparison.OrdinalIgnoreCase))
                    {
                        descendantsToRemove.Add(fieldCode);
                    }

                    continue;
                }

                if (descendant is SimpleField)
                {
                    var mergeField = (SimpleField)descendant;

                    var mergeFieldName = mergeField.Descendants<Text>().FirstOrDefault()?.Text ?? string.Empty;

                    // We want to maintain any whitespace present in the merge field
                    // to ensure we don't push text together when replacing the content.
                    if (lqbMergeFieldIds.Contains(mergeFieldName.Trim()))
                    {
                        var newMergeFieldText = new Text($"«{mergeFieldName}»");
                        var newMergeField = new Run(newMergeFieldText);
                        mergeField.Parent.ReplaceChild(newMergeField, mergeField);
                    }

                    continue;
                }
            }

            descendantsToRemove.ForEach(el => el.Remove());
        }

        private void WriteFileToFileDb(string file)
        {
            var fileDbKey = Path.GetFileNameWithoutExtension(file);
            FileDBTools.WriteFile(E_FileDB.Normal, fileDbKey, file);

            var filePath = LocalFilePath.Create(file).Value;
            FileOperationHelper.Delete(filePath);
        }
    }
}
