﻿using DataAccess;
using LendersOffice.ObjLib.AdjustmentsAndOtherCredits;
using LendersOfficeApp.los.AdjustmentsAndOtherCreditsSetup;
using System;
using System.Collections.Generic;
using System.Data.Common;
using Adapter;

namespace ScheduleExecutable.Migrations
{
    public class PredefinedAdjustmentsMigrator
    {
        public static void AddToPredefinedAdjustmentTable(string[] args)
        {
            if (args.Length > 2)
            {
                Console.WriteLine("Usage: ScheduleExecutable AddToPredefinedAdjustmentTable");
                return;
            }

            List<PredefinedAdjustment> adjustmentsToAdd = new List<PredefinedAdjustment>();
            adjustmentsToAdd.Add(new PredefinedAdjustment(-1, 0, "Employer Assisted Housing", 4, 1, false, 1, false));
            adjustmentsToAdd.Add(new PredefinedAdjustment(-1, 1, "Lease Purchase Fund", 2, 1, false, 1, false));
            adjustmentsToAdd.Add(new PredefinedAdjustment(-1, 2, "Relocation Funds", 4, 1, false, 1, false));
            adjustmentsToAdd.Add(new PredefinedAdjustment(-1, 4, "Other", 0, 0, false, 0, false));
            AddPredefinedAdjustmentsToBrokers(adjustmentsToAdd, dflpEnabled: false);
        }

        public static void AddTrid2017PrincipalReductionAdjustmentsToBrokers(string[] args)
        {
            var adjustmentsToAdd = new List<PredefinedAdjustment>(2)
            {
                AdjustmentsAndOtherCreditsData.PredefinedAdjustmentTypeMap[E_AdjustmentT.PrincipalReductionToReduceCashToBorrower],
                AdjustmentsAndOtherCreditsData.PredefinedAdjustmentTypeMap[E_AdjustmentT.PrincipalReductionToCureToleranceViolation]
            };

            AddPredefinedAdjustmentsToBrokers(adjustmentsToAdd, dflpEnabled: true);
        }

        private static void AddPredefinedAdjustmentsToBrokers(List<PredefinedAdjustment> adjustmentsToAdd, bool dflpEnabled)
        {
            foreach (Guid brokerId in Tools.GetAllActiveBrokers())
            {
                using (DbConnection conn = DbAccessUtils.GetConnection(brokerId))
                {
                    conn.OpenWithRetry();

                    using (DbTransaction tx = conn.BeginTransaction())
                    {
                        try
                        {
                            foreach (PredefinedAdjustment adj in adjustmentsToAdd)
                            {
                                PredefinedAdjustment.AddPredefinedAdjustment(conn, tx, adj, brokerId, dflpEnabled);
                            }

                            tx.Commit();
                        }
                        catch
                        {
                            tx.Rollback();
                            throw;
                        }
                    }
                }
            }
        }
    }
}
