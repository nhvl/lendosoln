﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Common.SerializationTypes;
    using LendersOffice.Constants;
    using LqbGrammar.DataTypes;
    using LendersOffice.Drivers.SqlServerDB;
    using System.Data;

    public class MigrateCCArchiveLoanDataToFileDB : Migrator<Guid>
    {
        public MigrateCCArchiveLoanDataToFileDB(string name)
            : base(name)
        {
                this.MigrationHelper = new CustomGUIDMigrationHelper(name, GetLoanIdsWithNonEmptyClosingCostArchiveXml);
        }

        protected IEnumerable<Guid> GetLoanIdsWithNonEmptyClosingCostArchiveXml()
        {
            // Unit test done via a copy/paste of this method to the testing library
            var loanIds = new List<Guid>();
            var selectAllValidLoansWithClosingCostArchives = @"
select b.slid
from loan_file_b b 
	join loan_file_cache lc on b.slid = lc.slid 
	join broker bro on lc.sbrokerid = bro.brokerid
where lc.isvalid = 1
	and bro.status = 1
	and b.sclosingcostarchivexmlcontent <> ''
	and lc.sloanfilet <> 2";

            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                while (reader.Read())
                {
                    loanIds.Add((Guid)reader["slid"]);
                }
            };

            foreach (var connectionInfo in DbConnectionInfo.ListAll())
            {
                DBSelectUtility.ProcessDBData(connectionInfo, selectAllValidLoansWithClosingCostArchives, null, null, readHandler);
            }

            return loanIds;
        }

        protected override bool MigrationFunction(Guid loanId)
        {
            try
            {
                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    loanId,
                    typeof(MigrateCCArchiveLoanDataToFileDB));
                loan.InitSave(ConstAppDavid.SkipVersionCheck);

                if (loan.sClosingCostArchive.Any() &&
                    loan.sClosingCostArchive.TrueForAll(a => a.FileDBContentVersion == 0))
                {
                    loan.FlushClosingCostArchives();
                    loan.Save();
                }

                return true;
            }
            catch (Exception e)
            {
                Tools.LogError(
                    "[CCArchiveMigration] Unexpected exception while moving archive content for file: " + loanId.ToString(),
                    e);

                return false;
            }
        }
    }
}
