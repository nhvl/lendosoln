﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;
using LqbGrammar.DataTypes;

namespace ScheduleExecutable.Migrations
{
    public class Opm419653_BatchCloseTasksPml0245 : BatchTaskCloser
    {
        public static void CloseTasks(string[] args)
        {
            // Mega Capital Funding PML0245 on Production.
            Guid brokerId = new Guid("9913bd61-53cc-4872-b501-f3eac73f1b8d");
            var migrator = new Opm419653_BatchCloseTasksPml0245(brokerId);
            migrator.Migrate();
        }

        public Opm419653_BatchCloseTasksPml0245(Guid brokerId)
            :base("Opm419653_BatchCloseTasksPml0245", brokerId)
        {
        }

        protected override IEnumerable<string> GetTaskIds()
        {
            var taskIds = new LinkedList<string>();

            // All tasks and conditions that are in loan files 90 days old or older.
            // All tasks and conditions that are in denied, canceled, and suspended loan status.
            // Don't touch templates.
            var sql = @"
                select taskid
                from task t join loan_file_cache c on t.loanid = c.slid
                where t.brokerid = @brokerId
                    and c.istemplate = 0 -- don't touch templates
                    and c.isvalid = 1 -- ignore deleted loans
                    and
                    (
                        (c.screatedd is not null and c.screatedd < @ninetyDaysAgo) -- loan is 90 days old or older
                        or
                        (c.sstatust is not null and c.sstatust in (10 /*Denied*/, 9 /*Cancelled*/, 8 /*Suspended*/))
                    )	 
                    and t.TaskStatus <> 2 -- task is not closed
            ";

            var parameters = new[]
            {
                new SqlParameter("@brokerId", this.BrokerId),
                new SqlParameter("@ninetyDaysAgo", DateTime.Today.AddDays(-90))
            };

            Action<IDataReader> readerCode = reader =>
            {
                while (reader.Read())
                {
                    taskIds.AddLast((string)reader["taskid"]);
                }
            };

            DBSelectUtility.ProcessDBData(this.BrokerId, sql, TimeoutInSeconds.Thirty, parameters, readerCode);

            return taskIds;
        }
    }
}
