﻿namespace ScheduleExecutable.Migrations
{
    using DataAccess;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.SqlServerDB;
    using LqbGrammar.DataTypes;
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class RemoveOrphanedOriginatorCompBeneficiaryId : Migrator<Guid>
    {
        public static void RemoveOrphanedOriginatorCompBeneficiaryIdMigrator(string[] args)
        {
            if (args.Length != 2)
            {
                Console.WriteLine("Usage: ScheduleExecutable RemoveOrphanedOriginatorCompBeneficiaryId BrokerId");
                return;
            } 

            Guid brokerId;

            if (!Guid.TryParse(args[1], out brokerId))
            {
                Console.WriteLine("Invalid input for BrokerId.");
                return;
            }

            var migrator = new RemoveOrphanedOriginatorCompBeneficiaryId(brokerId);
            migrator.Migrate();
        }

        public RemoveOrphanedOriginatorCompBeneficiaryId(Guid brokerId) : base(nameof(RemoveOrphanedOriginatorCompBeneficiaryId))
        {
            this.MigrationHelper = new CustomGUIDMigrationHelper(this.Name, () => GetLoansToRemoveOrphanedOriginatorCompBeneficiaryId(brokerId));
        }

        private IEnumerable<Guid> GetLoansToRemoveOrphanedOriginatorCompBeneficiaryId(Guid brokerId)
        {
            var loanIds = new List<Guid>();

            var query = @"
SELECT sLId
FROM Loan_File_Cache
WHERE sBrokerId = @BrokerId AND
IsValid = 1 AND
IsTemplate = 0 AND 
sLoanFileT = 0";

            SqlParameter[] parameter = { new SqlParameter("@BrokerId", brokerId) };

            using (var conn = DbConnectionInfo.GetConnection(brokerId))
            {
                using (var reader = SqlServerHelper.Select(conn, null, SQLQueryString.Create(query).Value, parameter, TimeoutInSeconds.Default))
                {
                    while (reader.Read())
                    {
                        loanIds.Add((Guid)reader["sLId"]);
                    }
                }
            }

            return loanIds;
        }

        protected override bool MigrationFunction(Guid sLId)
        {
            try
            {
                CPageData loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(sLId, typeof(RemoveOrphanedOriginatorCompBeneficiaryId));
                loan.InitSave(ConstAppDavid.SkipVersionCheck);

                BorrowerClosingCostFee fee = null;

                if (loan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID && loan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid &&
                loan.sGfeIsTPOTransaction)
                {
                    fee = loan.sClosingCostSet.StandaloneLenderOrigCompFee;
                }

                if (loan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.BorrowerPaid)
                {
                    fee = (BorrowerClosingCostFee)loan.sClosingCostSet.FindFeeByTypeId(DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId);
                }

                if (fee == null || fee.BeneficiaryAgentId == Guid.Empty)
                {
                    return true;
                }

                for (int i = 0; i < loan.GetAgentRecordCount(); i++)
                {
                    var agent = loan.GetAgentFields(i);

                    if (agent.RecordId == fee.BeneficiaryAgentId)
                    {
                        return true;
                    }
                }

                fee.BeneficiaryAgentId = Guid.Empty;

                loan.Save();
                return true;
            }
            catch (Exception exc)
            {
                Tools.LogError($"Could not remove orphaned originator compensation beneficiary id for loan: {sLId}.", exc);
                return false;
            }
        }
    }
}
