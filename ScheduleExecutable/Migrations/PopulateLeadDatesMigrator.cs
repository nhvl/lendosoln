﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using DataAccess;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.Gateways;

    /// <summary>
    /// Populates the appropriate dates for files in the lead declined, lead canceled, and lead other statuses. 
    /// </summary>
    public class PopulateLeadDatesMigrator : Migrator<Guid>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PopulateLeadDatesMigrator"/> class.
        /// </summary>
        public PopulateLeadDatesMigrator()
            : base($"PopulateLeadDatesMigrator_{DateTime.Now.ToString("MM_dd_yyyy")}")
        {
            this.MigrationHelper = new CustomGUIDMigrationHelper(this.Name, this.GetAffectedLeads);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PopulateLeadDatesMigrator"/> class.
        /// </summary>
        /// <param name="loansToMigrate">The list of loans to migrate.</param>
        public PopulateLeadDatesMigrator(IEnumerable<Guid> loansToMigrate)
            : base($"PopulateLeadDatesMigrator_{DateTime.Now.ToString("MM_dd_yyyy")}")
        {
            this.MigrationHelper = new CustomGUIDMigrationHelper(this.Name, () => loansToMigrate);
        }

        /// <summary>
        /// Runs the migrator.
        /// </summary>
        /// <param name="args">The command line arguments.</param>
        public static void PopulateLeadDates(string[] args)
        {
            PopulateLeadDatesMigrator migrator;
            if (args.Length == 1)
            {
                migrator = new PopulateLeadDatesMigrator();
            }
            else if (args.Length == 2)
            {
                List<Guid> loanIds = new List<Guid>();
                var lines = TextFileHelper.ReadLines(args[1]);
                foreach (var line in lines)
                {
                    loanIds.Add(Guid.Parse(line));
                }

                migrator = new PopulateLeadDatesMigrator(loanIds);
            }
            else
            {
                Console.WriteLine("Invalid usage.");
                Console.WriteLine("'ScheduleExecutable PopulateLeadDates' => Will run the migration using the query.");
                Console.WriteLine("'ScheduleExecutable PopulateLeadDates File.txt' => Will run the migration on the loan ids found in the file.");
                return;
            }

            migrator.Migrate();
        }

        /// <summary>
        /// The migration function.
        /// If the loan is in one of the three statuses, then it copies the date from the date used before into the proper one.
        /// </summary>
        /// <param name="id">The loan the migrate.</param>
        /// <returns>True if it was saved, false otherwise.</returns>
        protected override bool MigrationFunction(Guid id)
        {
            try
            {
                CPageData loanData = CPageData.CreateUsingSmartDependencyWithSecurityBypass(id, typeof(PopulateLeadDatesMigrator));
                loanData.ByPassFieldSecurityCheck = true;
                loanData.InitSave(ConstAppDavid.SkipVersionCheck);

                E_sStatusT status = loanData.sStatusT;
                if (status == E_sStatusT.Lead_Canceled)
                {
                    loanData.sLeadCanceledD_rep = loanData.sCanceledD_rep;
                }
                else if (status == E_sStatusT.Lead_Declined)
                {
                    // sStatusD returned sRejectedD for Lead_Declined status.
                    loanData.sLeadDeclinedD_rep = loanData.sRejectD_rep;
                }
                else if (status == E_sStatusT.Lead_Other)
                {
                    // sStatusD returned sOpenedD for Lead_Other status.
                    loanData.sLeadOtherD_rep = loanData.sOpenedD_rep;
                }
                else
                {
                    return false;
                }

                loanData.Save();
                return true;
            }
            catch (Exception e)
            {
                Tools.LogError($"Error migrating file {id.ToString()}", e);
                return false;
            }
        }

        /// <summary>
        /// Runs a query to get all files in the Lead_Canceled, Lead_Declined, or Lead_Other statuses.
        /// </summary>
        /// <returns>List of ids for files in those statuses.</returns>
        private List<Guid> GetAffectedLeads()
        {
            string query = @"SELECT
	                                lfc.slid
                                FROM
	                                Loan_File_Cache lfc
                                WHERE
	                                lfc.IsTemplate=0 AND
	                                lfc.IsValid=1 AND
	                                (
	                                 lfc.sStatusT=15 OR -- Lead_Canceled
	                                 lfc.sStatusT=16 OR -- Lead_Declined
	                                 lfc.sStatusT=17	-- Lead_Other
	                                )  
                                order by
	                                lfc.sstatust";

            var loanIds = new List<Guid>();

            Action<IDataReader> readHandler = delegate (IDataReader reader)
            {
                while (reader.Read())
                {
                    loanIds.Add((Guid)reader["slid"]);
                }
            };

            foreach (var connectionInfo in DbConnectionInfo.ListAll())
            {
                DBSelectUtility.ProcessDBData(connectionInfo, query, null, null, readHandler);
            }

            return loanIds;
        }
    }
}
