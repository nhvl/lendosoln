﻿namespace ScheduleExecutable.Migrations
{
    using DataAccess;
    using LendersOffice.Admin;
    using System;

    public class OrigPortalAntiSteeringDisclosureAccessMigrator : Migrator<Guid>
    {
        public OrigPortalAntiSteeringDisclosureAccessMigrator() : base(nameof(OrigPortalAntiSteeringDisclosureAccessMigrator))
        {
            this.MigrationHelper = new AllBrokerMigrationHelper(this.Name);
        }

        public static void OrigPortalAntiSteeringDisclosureAccessRunner(string[] args)
        {
            var migrator = new OrigPortalAntiSteeringDisclosureAccessMigrator();
            migrator.Migrate();
        }

        protected override bool MigrationFunction(Guid brokerId)
        {
            try
            {
                var brokerDb = BrokerDB.RetrieveById(brokerId);

                brokerDb.OrigPortalAntiSteeringDisclosureAccess = OrigPortalAntiSteeringDisclosureAccessType.Never;
                brokerDb.Save();

                return true;
            }
            catch (Exception e)
            {
                Tools.LogError($"Could not migrate the orignator portal anti-steering disclosure access for broker: {brokerId}.", e);
                return false;
            }
        }
    }
}
