﻿namespace ScheduleExecutable.Migrations
{
    using System.Collections.Generic;
    using LendersOffice.ConfigSystem.Operations;

    /// <summary>
    /// Adds workflow operations for requesting re-disclosures and initial closing disclosures
    /// in the TPO portal.
    /// </summary>
    public class Opm457362_AddTpoRedisclosureAndInitialClosingDisclosurePermissionsToSystemConfig : SystemConfigOperationMigrator
    {
        /// <summary>
        /// Returns the new operations to add to the system configuration.
        /// </summary>
        /// <returns>
        /// The new operations.
        /// </returns>
        protected override IEnumerable<OperationAddition> GetNewOperations()
        {
            return new[]
            {
                new OperationAddition(
                    WorkflowOperations.RequestRedisclosureInTpoPortal,
                    defaultValue: true,
                    notes: "OPM 457362",
                    failureMessage: "You do not have permission to request a CoC/Redisclosure on this file."),
                new OperationAddition(
                    WorkflowOperations.RequestInitialClosingDisclosureInTpoPortal,
                    defaultValue: true,
                    notes: "OPM 457362",
                    failureMessage: "You do not have permission to request an initial closing disclosure on this file.")
            };
        }
    }
}
