﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Threading;
    using System.Xml.Linq;
    using DataAccess;
    using LendersOffice.Constants;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Provides an address migration for PML0159 lender contacts.
    /// </summary>
    internal class Opm361597_LenderContactAddressMigrator : Migrator<Guid>
    {
        /// <summary>
        /// Represents the new street address.
        /// </summary>
        private const string Street = "350 Rampart Blvd. Suite 310";

        /// <summary>
        /// Represents the new city.
        /// </summary>
        private const string City = "Las Vegas";

        /// <summary>
        /// Represents the new state.
        /// </summary>
        private const string State = "NV";

        /// <summary>
        /// Represents the new ZIP code.
        /// </summary>
        private const string Zip = "89145";

        /// <summary>
        /// Represents the broker ID for PML0159.
        /// </summary>
        private readonly Guid brokerId = new Guid("B987FF40-139A-4474-B528-A796D75E1130");

        /// <summary>
        /// Prevents a default instance of the <see cref="Opm361597_LenderContactAddressMigrator"/>
        /// class from being created.
        /// </summary>
        private Opm361597_LenderContactAddressMigrator() : base(nameof(Opm361597_LenderContactAddressMigrator))
        {
            this.MigrationHelper = new CustomGUIDMigrationHelper(nameof(Opm361597_LenderContactAddressMigrator), this.IdRetrievalFunction);
        }

        /// <summary>
        /// Gets the allowed statuses for the migration.
        /// </summary>
        /// <value>
        /// The allowed loan statuses for the migration.
        /// </value>
        private IEnumerable<E_sStatusT> AllowedStatuses { get; } = new[]
        {
            E_sStatusT.Loan_Open,
            E_sStatusT.Loan_Registered,
            E_sStatusT.Loan_PreProcessing,
            E_sStatusT.Loan_Processing,
            E_sStatusT.Loan_DocumentCheck,
            E_sStatusT.Loan_SubmittedForPurchaseReview,
            E_sStatusT.Loan_SubmittedForFinalPurchaseReview,
            E_sStatusT.Loan_PreUnderwriting,
            E_sStatusT.Loan_Underwriting,
            E_sStatusT.Loan_Preapproval,
            E_sStatusT.Loan_Approved,
            E_sStatusT.Loan_ConditionReview,
            E_sStatusT.Loan_FinalUnderwriting,
            E_sStatusT.Loan_PreDocQC,
            E_sStatusT.Loan_ClearToClose,
            E_sStatusT.Loan_OnHold,
            E_sStatusT.Loan_Suspended,
            E_sStatusT.Loan_CounterOffer,
            E_sStatusT.Lead_New,
            E_sStatusT.Lead_Canceled,
            E_sStatusT.Lead_Other
        };

        /// <summary>
        /// Runs the migration.
        /// </summary>
        /// <param name="args">
        /// This parameter is not used.
        /// </param>
        public static void Run(string[] args)
        {
            var migrator = new Opm361597_LenderContactAddressMigrator();
            migrator.Migrate();
        }

        /// <summary>
        /// Begins the migration.
        /// </summary>
        protected override void BeginMigration()
        {
            Thread.CurrentPrincipal = SystemUserPrincipal.TaskSystemUser;
        }

        /// <summary>
        /// Migrates the lender contact agents for the specified <paramref name="loanId"/>.
        /// </summary>
        /// <param name="loanId">
        /// The id of the loan to migrate.
        /// </param>
        /// <returns>
        /// True if the migration was successful, false otherwise.
        /// </returns>
        protected override bool MigrationFunction(Guid loanId)
        {
            try
            {
                var data = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(Opm361597_LenderContactAddressMigrator));
                data.InitSave(ConstAppDavid.SkipVersionCheck);
                
                var lenderAgents = data.GetAgentsOfRole(E_AgentRoleT.Lender, E_ReturnOptionIfNotExist.ReturnEmptyObject).Where(agent => agent.IsValid);
                if (!lenderAgents.Any())
                {
                    return true;
                }

                foreach (var agent in lenderAgents)
                {
                    MigrationHelper.WriteToBackup(new
                    {
                        Id = agent.RecordId,
                        Name = agent.AgentName,
                        StreetAddr = agent.StreetAddr,
                        CityStateZip = agent.CityStateZip
                    });

                    agent.StreetAddr = Opm361597_LenderContactAddressMigrator.Street;
                    agent.City = Opm361597_LenderContactAddressMigrator.City;
                    agent.State = Opm361597_LenderContactAddressMigrator.State;
                    agent.Zip = Opm361597_LenderContactAddressMigrator.Zip;
                    agent.Update();
                }

                data.Save();
                return true;
            }
            catch (Exception exc)
            {
                var message = $"Exception encountered while migrating loan {loanId}: {exc}";
                Tools.LogError(message);
                Console.WriteLine(message);
                return false;
            }
        }

        /// <summary>
        /// Obtains the ids of the loans to migrate.
        /// </summary>
        /// <returns>
        /// A list of the loan ids to migrate.
        /// </returns>
        private IEnumerable<Guid> IdRetrievalFunction()
        {
            SqlParameter[] parameters = new[]
            {
                new SqlParameter("@BrokerId", this.brokerId),
                this.GetAllowedStatusesParameter()
            };

            const string Sql = @"
DECLARE @AllowedStatuses TABLE(status int)

INSERT INTO @AllowedStatuses
SELECT T.Item.value('.', 'int') 
FROM @AllowedStatusXml.nodes('root/status') as T(Item)

SELECT slid, sbrokerid, sStatusT
FROM loan_file_cache 
WHERE sbrokerid = @BrokerId
AND IsValid = 1
AND sLoanFileT = 0
AND IsTemplate = 0
AND sstatust IN (SELECT * FROM @AllowedStatuses)
";

            var loanIds = new LinkedList<Guid>();

            Action<IDataReader> readerAction = reader =>
            {
                while (reader.Read())
                {
                    if ((Guid)reader["sbrokerid"] != this.brokerId)
                    {
                        throw new Exception("Error: Unexpected BrokerId");
                    }

                    E_sStatusT status = (E_sStatusT)reader["sStatusT"];
                    if (!this.AllowedStatuses.Contains(status))
                    {
                        throw new UnhandledEnumException(status);
                    }

                    loanIds.AddLast((Guid)reader["slid"]);
                }
            };

            DBSelectUtility.ProcessDBData(this.brokerId, Sql, TimeoutInSeconds.Sixty, parameters, readerAction);

            return loanIds;
        }

        /// <summary>
        /// Obtains the parameter for the list of acceptable statuses.
        /// </summary>
        /// <returns>
        /// The parameter for the list of acceptable statuses.
        /// </returns>
        private SqlParameter GetAllowedStatusesParameter()
        {
            var idElements = this.AllowedStatuses.Select(status => new XElement("status", (int)status));
            var xmlContent = new XElement("root", idElements).ToString(SaveOptions.DisableFormatting);

            var parameter = new SqlParameter("@AllowedStatusXml", xmlContent)
            {
                SqlDbType = SqlDbType.Xml
            };

            return parameter;
        }
    }
}
