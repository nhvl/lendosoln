﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;

    using ConfigSystem.DataAccess;
    using DataAccess;
    using LendersOffice.ConfigSystem;
    using LendersOffice.Constants;

    /// <summary>
    /// Migration for recompiling Workflow byte code for all brokers.
    /// </summary>
    public class RecompileByteCode : Migrator<Guid>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RecompileByteCode"/> class.
        /// </summary>
        public RecompileByteCode()
            : base("RecompileByteCode")
        {
            this.MigrationHelper = new CustomGUIDMigrationHelper(this.Name, this.IDRetrievalFunction);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RecompileByteCode"/> class.
        /// </summary>
        /// <param name="brokerId">The ID of the broker whose byte code should be recompiled.</param>
        public RecompileByteCode(Guid brokerId)
            : base("RecompileByteCode")
        {
            this.MigrationHelper = new CustomGUIDMigrationHelper(this.Name, () => new Guid[] { brokerId });
        }

        /// <summary>
        /// Runs the migration.
        /// </summary>
        /// <param name="args">Command line arguments. Accepts a Customer Code as 2nd argument.</param>
        public static void Run(string[] args)
        {
            RecompileByteCode migrator;
            if (args.Length == 1)
            {
                migrator = new RecompileByteCode();
            }
            else if (args.Length == 2)
            {
                Guid brokerId = Tools.GetBrokerIdByCustomerCode(args[1]);
                migrator = new RecompileByteCode(brokerId);
            }
            else
            {
                Console.WriteLine("Usage: ScheduleExecutable RecompileByteCode OR ScheduleExecutable RecompileByteCode [ConsumerCode]");
                return;
            }

            migrator.Migrate();
        }

        private IEnumerable<Guid> IDRetrievalFunction()
        {
            List<Guid> ids = new List<Guid>();
            ids.Add(ConstAppDavid.SystemBrokerGuid);
            ids.AddRange(Tools.GetAllActiveBrokers());  // NOTE: System broker is not an Active broker.
            return ids;
        }

        protected override bool MigrationFunction(Guid id)
        {
            try
            {
                IConfigRepository repository = ConfigHandler.GetUncachedRepository(id);
                ReleaseConfigData config = repository.LoadActiveRelease(id);
                repository.SaveReleaseConfig(id, config.Configuration, LendersOffice.Security.SystemUserPrincipal.TaskSystemUser.UserId);
            }
            catch (Exception e)
            {
                string msg = $"Could not recompile byte code for brokerId=[{id}].";
                Tools.LogError(msg, e);

                Console.WriteLine(msg);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine();
            }

            return true;
        }
    }
}
