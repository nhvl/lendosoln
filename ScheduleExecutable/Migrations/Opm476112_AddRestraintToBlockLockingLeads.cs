﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;

    using ConfigSystem;
    using DataAccess;
    using LendersOffice.ConfigSystem;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Security;

    class Opm476112_AddRestraintToBlockLockingLeads : Migrator<Guid>
    {
        public Opm476112_AddRestraintToBlockLockingLeads()
            : base(nameof(Opm476112_AddRestraintToBlockLockingLeads))
        {
            this.MigrationHelper = new BrokerMigrationHelper(this.Name);
        }

        public static void Run(string[] args)
        {
            var migrator = new Opm476112_AddRestraintToBlockLockingLeads();
            migrator.Migrate();
        }

        protected override bool MigrationFunction(Guid id)
        {
            WorkflowSystemConfigModel workflow = new WorkflowSystemConfigModel(id, loadDraft: false);
            RestraintSet restraints = workflow.GetFullRestraintSet();

            // Create Restraint for "Request Rate Lock (manual lock)".
            Restraint manualLockRestraint = new Restraint();
            Parameter manualLockParameter = new Parameter("sIsStatusLead", E_FunctionT.Equal, E_ParameterValueType.Bool, false, false);
            manualLockParameter.AddValue("True", "True");
            manualLockRestraint.ParameterSet.AddParameter(manualLockParameter);
            manualLockRestraint.SysOpSet.Add(new SystemOperation(WorkflowOperations.RequestRateLockRequiresManualLock));
            manualLockRestraint.Notes = $"Created automatically by LendingQB.";
            manualLockRestraint.FailureMessage = "Cannot lock Lead files.";
            restraints.AddRestraint(manualLockRestraint);
            
            // Create Restraint for "Request Rate Lock (auto lock)".
            Restraint autoLockRestraint = new Restraint();
            Parameter autoLockParameter = new Parameter("sIsStatusLead", E_FunctionT.Equal, E_ParameterValueType.Bool, false, false);
            autoLockParameter.AddValue("True", "True");
            autoLockRestraint.ParameterSet.AddParameter(autoLockParameter);
            autoLockRestraint.SysOpSet.Add(new SystemOperation(WorkflowOperations.RequestRateLockCausesLock));
            autoLockRestraint.Notes = $"Created automatically by LendingQB.";
            autoLockRestraint.FailureMessage = "Cannot lock Lead files.";
            restraints.AddRestraint(autoLockRestraint);

            workflow.SwitchToDraft();
            workflow.Save();

            try
            {
                WorkflowSystemConfigModel.Publish(id, SystemUserPrincipal.TaskSystemUser);
            }
            catch (ConfigValidationException e)
            {
                Tools.LogError(e.ToString(), e);
                throw;
            }

            return true;
        }
    }
}
