﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.Migration;

    /// <summary>
    /// Migrator that will completely enable multi factor authentication for the provided brokers.
    /// </summary>
    public class EnableMfaOptInBit_Opm266053 : Migrator<Guid>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EnableMfaOptInBit_Opm266053"/> class.
        /// </summary>
        /// <param name="name">The name for the migration.</param>
        /// <param name="brokerIds">The broker ids to migrate.</param>
        public EnableMfaOptInBit_Opm266053(string name, IEnumerable<Guid> brokerIds) :
            base(name)
        {
            this.MigrationHelper = new CustomGUIDMigrationHelper(name, () => brokerIds);
        }

        /// <summary>
        /// Migrations the brokers to fully enable MFA. 
        /// </summary>
        /// <param name="args">The parameters.</param>
        /// <remarks>
        /// Please pass
        /// </remarks>
        public static void EnableMfaOptInBit(string[] args)
        {
            List<Guid> brokerIds;
            if (args.Length == 1)
            {
                brokerIds = Tools.GetAllActiveBrokers().ToList();
            }
            else if (args.Length == 2)
            {
                brokerIds = new List<Guid>();
                foreach (var stringId in TextFileHelper.ReadLines(args[1]))
                {
                    brokerIds.Add(Guid.Parse(stringId));
                }
            }
            else
            {
                Console.WriteLine("Please either pass in a valid file containing a list of broker ids or pass in nothing to migrate all active brokers.");
                return;
            }

            EnableMfaOptInBit_Opm266053 migrator = new EnableMfaOptInBit_Opm266053("EnableMfaOptInBit_Opm266053", brokerIds);
            migrator.Migrate();
        }

        /// <summary>
        /// The migration function. Will remove the AllowUserMfaToBeDisabled temp option as well as toggling the MFA bits to true. 
        /// </summary>
        /// <param name="id">The broker to update.</param>
        /// <returns>True if migrated, false otherwise.</returns>
        protected override bool MigrationFunction(Guid id)
        {
            try
            {
                BrokerDB broker = BrokerDB.RetrieveById(id);

                bool hasChanged = false;

                if (!broker.IsOptInMultiFactorAuthentication)
                {
                    broker.IsOptInMultiFactorAuthentication = true;

                    // These are calculated values but this ensures that the value is saved to the DB by setting the "set" bits.
                    broker.IsEnableMultiFactorAuthentication = true;
                    broker.IsEnableMultiFactorAuthenticationTPO = true;

                    hasChanged = true;
                }

                string tempOption = "<option name=\"AllowUserMfaToBeDisabled\" value=\"true\" />";
                if (broker.TempOptionXmlContent.Value.IndexOf(tempOption, StringComparison.OrdinalIgnoreCase) != -1)
                {
                    broker.TempOptionXmlContent = broker.TempOptionXmlContent.Value.Replace(tempOption, string.Empty);
                    hasChanged = true;
                }

                if (hasChanged)
                {
                    broker.Save();
                    return true;
                }

                return false;
            }
            catch (Exception e)
            {
                Tools.LogError("Error updating broker <" + id.ToString() + "> for EnableMfaOptInBit_Opm266053", e);
            }

            return false;
        }
    }
}
