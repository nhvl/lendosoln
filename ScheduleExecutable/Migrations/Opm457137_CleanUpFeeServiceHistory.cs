﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using DataAccess;
    using DataAccess.FeeService;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.Gateways;

    /// <summary>
    /// Cleans up fee service history so that a fee does not have entries for
    /// both field set and fee removal.
    /// </summary>
    public class Opm457137_CleanUpFeeServiceHistory : Migrator<Guid>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Opm457137_CleanUpFeeServiceHistory"/> class.
        /// </summary>
        /// <param name="loanIds">A list of loan IDs for the loans to be migrated.</param>
        public Opm457137_CleanUpFeeServiceHistory(IEnumerable<Guid> loanIds)
            :base(nameof(Opm457137_CleanUpFeeServiceHistory))
        {
            ////// We don't need to run on templates or QP files (history should be empty on those files).
            ////this.MigrationHelper = new LoanMigrationHelper(this.Name, false, true, true);

            // We're going to run this on a case by case basis rather than try to run it for all loans.
            this.MigrationHelper = new CustomGUIDMigrationHelper(this.Name, () => loanIds);
        }

        /// <summary>
        /// Runs the migrator.
        /// </summary>
        /// <param name="args">Command line arguements.</param>
        public static void Run(string[] args)
        {
            // Get list of loan IDs form file.
            IEnumerable<Guid> loanIds = ParseLoanIdsFromFile();

            // Run the actual migration.
            var migrator = new Opm457137_CleanUpFeeServiceHistory(loanIds);
            migrator.Migrate();
        }

        /// <summary>
        /// Performs the actual migration.
        /// </summary>
        /// <param name="id">The Loan Id.</param>
        /// <returns>True if migration succeeded.</returns>
        protected override bool MigrationFunction(Guid id)
        {
            CPageData dataLoan = new CFullAccessPageData(id, new string [] { "sFeeServiceApplicationHistoryXmlContent" });
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            // Don't update if Fee Service History is blank.
            if (string.IsNullOrWhiteSpace(dataLoan.sFeeServiceApplicationHistoryXmlContent))
            {
                return true;
            }

            FeeServiceApplication.ClosingCostApplicationHistory feeServiceHistory = new FeeServiceApplication.ClosingCostApplicationHistory(dataLoan.sFeeServiceApplicationHistoryXmlContent);

            // Don't need to migrate blank history, or if history is for CCTemplates.
            // NOTE: I'm not going to check broker for valid fee service. We are assuming that this
            // migration won't be run for any broker who does not have a valid fee service file.
            if (feeServiceHistory.IsBlank || !feeServiceHistory.IsCorrectFormat(true))
            {
                return true;
            }

            // Get Fee IDs for all removed fees.
            List<string> feeIds = feeServiceHistory.FeeSetHistory.SelectMany(cond => cond.FieldIds.Where(f => f.EndsWith(":Remove"))).Select(f => f.Substring(0, f.IndexOf(':'))).ToList();

            // For each fee that was removed.
            foreach(string feeId in feeIds)
            {
                // Find all condition hits with entries for this fee.
                IEnumerable<FeeServiceApplication.FeeConditionHit> conds = feeServiceHistory.FeeSetHistory.Where(c => c.FieldIds.Any(f => f.StartsWith(feeId)));

                // We're fine if the remove entry is the only entry in the fee service history.
                if(conds.Count() < 2)
                {
                    continue;
                }

                // If there is more than 1 entry, then we have to remove all but the last.
                FeeServiceApplication.FeeConditionHit last = conds.Last();
                string removeKey = feeId + ":" + E_FeeServiceFeePropertyT.Remove.ToString();
                foreach (FeeServiceApplication.FeeConditionHit cond in conds)
                {
                    // Stop once we've reached remove key.
                    // Any changes after the remove should be reflected in the current closing cost set.
                    if (cond.FieldIds.Contains(removeKey))
                    {
                        // Remove entry if "feeId:Remove" is not the last condition hit in the file.
                        if (cond != last)
                        {
                            cond.FieldIds.RemoveAll(f => f.StartsWith(feeId));
                        }

                        break;
                    }

                    // Remove all entries for this fee id.
                    cond.FieldIds.RemoveAll(f => f.StartsWith(feeId));
                }
            }

            // Clean up empty conditions.
            feeServiceHistory.FeeSetHistory.RemoveAll(p => p.FieldIds.Count == 0);

            dataLoan.sFeeServiceApplicationHistoryXmlContent = feeServiceHistory.ToXml();
            dataLoan.Save();

            return true;
        }

        /// <summary>
        /// Parses a list of loan IDs from file.
        /// </summary>
        /// <returns>A list of loan IDs.</returns>
        private static IEnumerable<Guid> ParseLoanIdsFromFile()
        {
            // Parse out guids
            List<Guid> loanIds = new List<Guid>();
            foreach (string stringId in TextFileHelper.ReadLines("OPM457137.txt"))
            {
                Guid guidId;
                if (!Guid.TryParse(stringId, out guidId))
                {
                    string msg = "OPM457137.txt invalid. File must be a list of loan IDs, separated by new lines.";
                    throw new CBaseException(msg, msg);
                }

                loanIds.Add(guidId);
            }

            return loanIds;
        }
    }
}
