﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading;
    using DataAccess;
    using LendersOffice.Constants;
    using LendersOffice.Security;

    /// <summary>
    /// Migrator to disable the initial CD calculation for all of NFLP's TRID loans.
    /// </summary>
    public class DisableClosingDisclosureInitialCalculationForNflp : Migrator<Guid>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DisableClosingDisclosureInitialCalculationForNflp"/> class.
        /// </summary>
        /// <param name="name">The migration name.</param>
        public DisableClosingDisclosureInitialCalculationForNflp(string name) :
            base(name)
        {
            this.MigrationHelper = new CustomGUIDMigrationHelper(name, ListNflpLoans);
            Thread.CurrentPrincipal = SystemUserPrincipal.TaskSystemUser;
        }

        /// <summary>
        /// Triggers the migration to run.
        /// </summary>
        /// <param name="args">String arguments.</param>
        public static void RunMigration(string[] args)
        {
            var migrator = new DisableClosingDisclosureInitialCalculationForNflp(nameof(DisableClosingDisclosureInitialCalculationForNflp));
            migrator.Migrate();
        }

        /// <summary>
        /// Selects all NFLP TRID loan files that have the option selected to calculate the
        /// initial closing disclosure.
        /// </summary>
        /// <returns>A list of loan IDs to migrate.</returns>
        internal static IEnumerable<Guid> ListNflpLoans()
        {
            List<Guid> loanIds = new List<Guid>();
            string sql = @"
                SELECT lfc.sLId
                FROM LOAN_FILE_CACHE lfc
                    JOIN LOAN_FILE_B lfb on lfc.sLId = lfb.sLId
                WHERE lfc.IsValid = 1
                    AND lfc.sBrokerId = 'c2f3a0f9-eb17-4e66-a951-ff5013f3efde'
                    AND lfb.sCalculateInitialClosingDisclosure = 1";

            Action<IDataReader> readHandler = delegate (IDataReader reader)
            {
                while (reader.Read())
                {
                    loanIds.Add((Guid)reader["sLId"]);
                }
            };

            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                DBSelectUtility.ProcessDBData(connInfo, sql, null, null, readHandler);
            }

            return loanIds;
        }

        /// <summary>
        /// Turns off the initial CD calculation for the given loan file.
        /// </summary>
        /// <param name="loanId">The loan to migrate.</param>
        /// <returns>A boolean indicating whether the migration was successful.</returns>
        protected override bool MigrationFunction(Guid loanId)
        {
            try
            {
                
                var dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(DisableClosingDisclosureInitialCalculationForNflp));
                dataLoan.ByPassFieldSecurityCheck = true;
                dataLoan.AllowSaveWhileQP2Sandboxed = true;
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                dataLoan.sCalculateInitialClosingDisclosure = false;

                dataLoan.Save();
                return true;
            }
            catch (SystemException exc)
            {
                Tools.LogError($"Failed to disable sCalculateInitialClosingDisclosure for loan {loanId}.", exc);
                return false;
            }
            catch (CBaseException exc)
            {
                Tools.LogError($"Failed to disable sCalculateInitialClosingDisclosure for loan {loanId}.", exc);
                return false;
            }
        }
    }
}
