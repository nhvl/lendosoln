﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using DataAccess;

    /// <summary>
    /// Batch closes the tasks for PML0208.
    /// </summary>
    public class CloseTasksForPML0208_Opm338196_Migrator : BatchTaskCloser
    {
        /// <summary>
        /// Brokrer id for PML0208.
        /// </summary>
        private static readonly Guid pml0208BrokerId = new Guid("d352da7a-bfac-45d5-8c21-5f72522e834f");

        /// <summary>
        /// Initializes a new instance of the <see cref="CloseTasksForPML0208_Opm338196_Migrator"/> class.
        /// </summary>
        public CloseTasksForPML0208_Opm338196_Migrator()
            : base($"CloseTasksForPML0208_Opm338196_Migrator_{DateTime.Now.ToString("MM_dd_yyyy")}", pml0208BrokerId)
        {
        }

        /// <summary>
        /// Runs the migrator.
        /// </summary>
        /// <param name="args">Any command line arguments provided.</param>
        public static void CloseTasksForPML0208_Opm338196(string[] args)
        {
            CloseTasksForPML0208_Opm338196_Migrator migrator = new CloseTasksForPML0208_Opm338196_Migrator();
            migrator.Migrate();
        }

        /// <summary>
        /// Gets the task ids for the tasks that should be migrated.
        /// </summary>
        /// <returns>A list of task ids to migrate.</returns>
        /// <remarks>
        /// Requirements for closing a task:
        ///  - Task or condition
        ///  - Isn't closed
        ///  - Must not belong to a template
        ///  - Associated file must be in the following statuses:
        ///     Funded
        ///     Recorded
        ///     Final Docs
        ///     Loan Closed
        ///     Submitted For Purchase Review
        ///     In Purchase Review
        ///     Pre-Purchase Conditions
        ///     Submitted for Final Purchase Review
        ///     In Final Purchase Review
        ///     Clear to Purchase
        ///     Loan Purchased
        ///     Ready for Sale
        ///     Loan Shipped
        ///     Loan Sold
        ///     Counter Offer Approved
        ///     Loan On-Hold
        ///     Loan Canceled
        ///     Loan Suspended
        ///     Loan Denied
        ///     Loan Withdrawn
        ///     Loan Archived
        /// </remarks>
        protected override IEnumerable<string> GetTaskIds()
        {
            string query = @"Select
                            t.TaskId
                        FROM
                            Task t JOIN
                            Loan_file_cache lf_c on t.LoanId=lf_c.sLId
                        WHERE
	                        t.TaskStatus<>2 AND
                            t.BrokerId=@BrokerId AND
                            lf_c.IsTemplate=0 AND
                            (
                              lf_c.sStatusT=6 OR -- Funded
                              lf_c.sStatusT=19 OR -- Recorded
                              lf_c.sStatusT=26 OR -- Final Docs
                              lf_c.sStatusT=11 OR  -- Loan Closed
                              lf_c.sStatusT=40 OR -- Submitted for Purchase Review
                              lf_c.sStatusT=41 OR -- In Purchase Review
                              lf_c.sStatusT=42 OR -- Pre-Purchase Conditions
                              lf_c.sStatusT=43 OR -- Submitted for Final Purchase Review
                              lf_c.sStatusT=44 OR -- In Final Purchase Review
                              lf_c.sStatusT=45 OR -- Clear to Purchase
                              lf_c.sStatusT=46 OR -- Loan Purchased
                              lf_c.sStatusT=39 OR -- Ready for Sale
                              lf_c.sStatusT=20 OR -- Loan Shipped
                              lf_c.sStatusT=27 OR -- Loan Sold (Enum is Loan_LoanPurchased in the data layer)
                              lf_c.sStatusT=47 OR -- Counter Offer Approved 
                              lf_c.sStatusT=7 OR -- Loan On-Hold
                              lf_c.sStatusT=9 OR -- Loan Canceled
                              lf_c.sStatusT=8 OR -- Loan Suspended
                              lf_c.sStatusT=10 OR -- Loan Denied (Enum is Loan_Rejected)
                              lf_c.sStatusT=48 OR -- Loan Withdrawn
                              lf_c.sStatusT=49   -- Loan Archived
                            )";

            List<string> taskIds = new List<string>();
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@BrokerId", pml0208BrokerId),
            };

            Action<IDataReader> readHandler = delegate (IDataReader reader)
            {
                while (reader.Read())
                {
                    taskIds.Add((string)reader["TaskId"]);
                }
            };

            DBSelectUtility.ProcessDBData(pml0208BrokerId, query, null, parameters, readHandler);
            return taskIds;
        }
    }
}