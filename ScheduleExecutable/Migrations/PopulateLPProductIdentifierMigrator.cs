﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Text;
    using DataAccess;
    using LendersOffice.Drivers.Gateways;

    /// <summary>
    /// Migrator that populates the finds loan programs for a given broker and populates the programs' product identifiers.
    /// </summary>
    public class PopulateLPProductIdentifierMigrator : Migrator<string>
    {
        /// <summary>
        /// The number of columns expected in the CSV.
        /// </summary>
        private static readonly int CsvColumnNumber = 2;

        /// <summary>
        /// The broker we're migrating.
        /// </summary>
        private Guid brokerId;

        /// <summary>
        /// Maps program name to the product identifier.
        /// </summary>
        private Dictionary<string, string> programNameToProductIdentifier;

        /// <summary>
        /// The template ids that we processed, since program names aren't unique.
        /// </summary>
        private StringBuilder processedTemplateIdLog = new StringBuilder();

        /// <summary>
        /// Initializes a new instance of the <see cref="PopulateLPProductIdentifierMigrator"/> class.
        /// </summary>
        /// <param name="programNameToProductIdentifier">Mapping from program name to product identifier.</param>
        /// <param name="brokerId">The broker to migrate.</param>
        public PopulateLPProductIdentifierMigrator(Dictionary<string, string> programNameToProductIdentifier, Guid brokerId)
            : base($"PopulateLPProductIdentifier_{DateTime.Now.ToString("MM_dd_yy")}")
        {
            this.brokerId = brokerId;
            this.programNameToProductIdentifier = programNameToProductIdentifier;

            this.MigrationHelper = new CustomStringMigrationHelper(this.Name, () => this.programNameToProductIdentifier.Keys);
        }

        /// <summary>
        /// The migration function.
        /// </summary>
        /// <param name="programName">The program name we're going to look for. Note that this can result in updating multiple programs.</param>
        /// <returns>True if successful, false otherwise.</returns>
        protected override bool MigrationFunction(string programName)
        {
            string productIdentifier = this.programNameToProductIdentifier[programName];

            string selectSql = @"SELECT lLpTemplateId FROM LOAN_PROGRAM_TEMPLATE WHERE BrokerId=@BrokerId AND lLpTemplateNm=@ProgramName";
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@BrokerId", this.brokerId),
                new SqlParameter("@ProgramName", programName)
            };

            List<Guid> matchedTemplateIds = new List<Guid>();
            Action<IDataReader> readerCode = delegate (IDataReader reader)
            {
                while (reader.Read())
                {
                    matchedTemplateIds.Add((Guid)reader["lLpTemplateId"]);
                }
            };

            try
            {
                DBSelectUtility.ProcessDBData(DataSrc.LpeSrc, selectSql, LqbGrammar.DataTypes.TimeoutInSeconds.Default, parameters, readerCode);

                foreach (var templateId in matchedTemplateIds)
                {
                    CLoanProductData data = new CLoanProductData(templateId);
                    data.InitSave();

                    data.ProductIdentifier = productIdentifier;
                    data.ProductIdentifierOverride = true;

                    data.Save();

                    this.processedTemplateIdLog.AppendLine(templateId.ToString());
                }

                return true;
            }
            catch (Exception e)
            {
                string msg = $"Failed to migrate Loan Program(s) with name {programName}";
                Tools.LogError(msg, e);

                return false;
            }
        }

        /// <summary>
        /// Logs the processed tempate ids.
        /// </summary>
        protected override void EndMigration()
        {
            var text = processedTemplateIdLog.ToString();
            var name = this.Name + "_processedTemplateIds";
            TextFileHelper.AppendString(name + ".txt", text);
            processedTemplateIdLog.Length = 0;
        }

        /// <summary>
        /// ScheduleExecutable entry point to the migrator. See remarks for required arguments.
        /// </summary>
        /// <param name="args">The arguments.</param>
        /// <remarks>
        /// This migration requires 2 arguments, each marked by an option.
        /// 
        ///  --brokerId [broker id here]
        ///  This is the guid id of the broker you want to migrate.
        ///  The migrator will also ask if this is the correct broker by giving you the matching name and customer code. Follow the on-screen instructions to proceed.
        ///  
        ///  --csvFilePath [csv file path here]
        ///  This takes in a file path to a CSV file (most likely provided by the SAEs). The CSV file requires 2 columns:
        ///     LoanProgramName, ProductIdentifier.
        ///  Obviously the target program names goes under the LoanProgramName column and the new product identifiers go into the ProductIdentifier column.
        ///  Should error out if there are errors parsing the CSV or if it finds multiple of the same program name.
        /// </remarks>
        public static void Run(string[] args)
        {
            List<string> arguments = args.ToList();

            Guid brokerId;
            int brokerIdOptionIndex = arguments.IndexOf("--brokerId");
            if (brokerIdOptionIndex != -1)
            {
                int brokerIdIndex = brokerIdOptionIndex + 1;
                if (brokerIdIndex < arguments.Count)
                {
                    if (!Guid.TryParse(arguments[brokerIdIndex], out brokerId))
                    {
                        Console.WriteLine($"Invalid broker id {arguments[brokerIdIndex]}");
                        return;
                    }

                    var brokerNameAndCustomerCode = LendersOffice.Admin.BrokerDB.RetrieveNameAndCustomerCodeFromDb(brokerId);
                    if (brokerNameAndCustomerCode == null)
                    {
                        Console.WriteLine($"Invalid broker id {arguments[brokerIdIndex]}");
                        return;
                    }

                    Console.WriteLine($"You have specified broker {brokerId}(Name: {brokerNameAndCustomerCode.Item1} CustomerCode: {brokerNameAndCustomerCode.Item2}) for this migration. Type 'correct' (no quotes) if this is correct:");
                    string response = Console.ReadLine();
                    if (response != "correct")
                    {
                        Console.WriteLine("Migration aborted due to wrong broker.");
                        return;
                    }
                }
                else
                {
                    Console.WriteLine("Please provide a valid broker id after the '--brokerId' option.");
                    return;
                }
            }
            else
            {
                Console.WriteLine("Please provide a valid broker id via a '--brokerId' option");
                return;
            }

            Dictionary<string, string> programNameToProductIdentifier;
            int csvFilePathOptionIndex = arguments.IndexOf("--csvFilePath");
            if (csvFilePathOptionIndex != -1)
            {
                int csvFilePathIndex = csvFilePathOptionIndex + 1;
                if (csvFilePathIndex < arguments.Count)
                {
                    var csvText = System.IO.File.ReadAllText(arguments[csvFilePathIndex]);
                    ILookup<int, string> errors;
                    DataTable dataTable = LendersOffice.Common.TextImport.DelimitedListParser.CsvToDataTable(csvText, hasHeader: true, reqColumns: CsvColumnNumber, numColumns: CsvColumnNumber, errors: out errors);

                    if (errors != null && errors.Any())
                    {
                        foreach (var error in errors)
                        {
                            var lineNum = error.Key;
                            foreach (var errorMessage in error)
                            {
                                Console.WriteLine($"Line {lineNum}: {errorMessage}");
                            }
                        }

                        return;
                    }
                    else
                    {
                        programNameToProductIdentifier = new Dictionary<string, string>();
                        for (int i = 0; i < dataTable.Rows.Count; i++)
                        {
                            var row = dataTable.Rows[i];
                            var programName = row["LoanProgramName"].ToString();
                            var productIdentifier = row["ProductIdentifier"].ToString();

                            if (programNameToProductIdentifier.ContainsKey(programName))
                            {
                                Console.WriteLine($"Multiple instances of LoanProgramName {programName}");
                                return;
                            }

                            programNameToProductIdentifier.Add(programName, productIdentifier);
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Please provide a valid csv file path after the '--csvFilePath' option.");
                    return;
                }
            }
            else
            {
                Console.WriteLine("Please provide a valid csv file path via a '--csvFilePath' option");
                return;
            }

            var migrator = new PopulateLPProductIdentifierMigrator(programNameToProductIdentifier, brokerId);
            migrator.Migrate();
        }
    }
}
