﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data;

    using DataAccess;

    /// <summary>
    /// Migrates loan program templates from using defunct interest only payment options.
    /// </summary>
    public class Opm242351_MigrateLoanProgramPaymentOptions : Migrator<Guid>
    {
        private const string sql = "SELECT lLpTemplateId FROM LOAN_PROGRAM_TEMPLATE WHERE lLpDPmtT = 0 OR lLpQPmtT = 0";

        /// <summary>
        /// Initializes a new instance of the <see cref="Opm242351_MigrateLoanProgramPaymentOptions"/> class.
        /// </summary>
        public Opm242351_MigrateLoanProgramPaymentOptions()
            : base("Opm242351_MigrateLoanProgramPaymentOptions")
        {
            this.MigrationHelper = new CustomGUIDMigrationHelper(this.Name, IDRetrievalFunction);
        }

        /// <summary>
        /// Runs the migration.
        /// </summary>
        /// <param name="args">Command line arguments.</param>
        public static void Run(string[] args)
        {
            Opm242351_MigrateLoanProgramPaymentOptions migrator = new Opm242351_MigrateLoanProgramPaymentOptions();
            migrator.Migrate();
        }

        /// <summary>
        /// Retrieves an enumerable of all loan program template IDs.
        /// </summary>
        /// <returns><see cref="IEnumerable{Guid}"/> of loan program template IDs.</returns>
        private IEnumerable<Guid> IDRetrievalFunction()
        {
            List<Guid> loanProgramTemplateIds = new List<Guid>();

            DataSet ds = new DataSet();
            DBSelectUtility.FillDataSet(DataSrc.LpeSrc, ds, sql, null, null);

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                loanProgramTemplateIds.Add((Guid)row["lLpTemplateId"]);
            }

            return loanProgramTemplateIds;
        }

        protected override bool MigrationFunction(Guid id)
        {
            try
            {
                CLoanProductData productData = new CLoanProductData(id);
                productData.InitSave();

                if (productData.lIOnlyMon <= 0 && productData.lLpDPmtT == E_sLpDPmtT.UserSelection)
                {
                    productData.lLpDPmtT = E_sLpDPmtT.PAndI;
                }
                else if (productData.lIOnlyMon > 0 && productData.lLpDPmtT == E_sLpDPmtT.UserSelection)
                {
                    productData.lLpDPmtT = E_sLpDPmtT.InterestOnly;
                }

                if (productData.lLpQPmtT == E_sLpQPmtT.UserSelection)
                {
                    productData.lLpQPmtT = E_sLpQPmtT.PAndI;
                }

                productData.SuppressEventHandlerCall = true;
                productData.Save();
            }
            catch (Exception e)
            {
                string msg = $"Failed to migrate Loan Program Template. lLpTemplateId=[{id}].";
                Tools.LogError(msg, e);

                return false;
            }

            return true;
        }
    }
}
