﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;
using EDocs;
using LqbGrammar.DataTypes;
using LendersOffice.Drivers.SqlServerDB;
using System.Data;

namespace ScheduleExecutable.Migrations
{
    /// <summary>
    /// Upload non destructive PDF to Amazon. Client must migrate to non destructive eDocs before call this upload.
    /// </summary>
    class UploadPdfToAmazon
    {
        public static void Execute(string[] args)
        {
            if (args.Length < 2)
            {
                Console.WriteLine("Usage: ScheduleExecutable UploadPdfToAmazon [brokerid]");
                return;

            }

            Guid brokerId = new Guid(args[1]);

            Stopwatch totalRunningStopwatch = Stopwatch.StartNew();
            int totalUploaded = 0;

            while (true)
            {
                List<Guid> documentIdList = GetRecentEDocs(brokerId);

                Stopwatch sw = Stopwatch.StartNew();

                foreach (var documentId in documentIdList)
                {
                    AmazonEDocHelper.UploadOriginalPdfToAmazon(brokerId, documentId);
                }
                sw.Stop();
                totalUploaded += documentIdList.Count;

                string debugMesage = string.Format(@"[{0:s}][{1,15:N0}] - Uploaded {2} files in {3:N0} ms. Total uploaded {4:N0}.", DateTime.Now,
                    totalRunningStopwatch.ElapsedMilliseconds, documentIdList.Count, sw.ElapsedMilliseconds, totalUploaded);
                Console.WriteLine(debugMesage);

                if (documentIdList.Count == 0)
                {
                    break; // No more document to process.
                }
            }
        }

        private static List<Guid> GetRecentEDocs(Guid brokerId)
        {
            // Unit tested by copying to test library
            const string sql = "SELECT TOP 10 CreatedDate, DocumentId FROM edocs_document_original WHERE BrokerId=@BrokerId AND IsUploadedAws=0 order by createddate desc";

            SqlParameter[] parameters = { new SqlParameter("@BrokerId", brokerId) };

            List<Guid> documentIdList = new List<Guid>();
            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                while (reader.Read())
                {
                    Guid documentId = (Guid)reader["DocumentId"];
                    documentIdList.Add(documentId);
                }
            };

            DBSelectUtility.ProcessDBData(brokerId, sql, TimeoutInSeconds.Thirty, parameters, readHandler);
            return documentIdList;
        }
    }
}