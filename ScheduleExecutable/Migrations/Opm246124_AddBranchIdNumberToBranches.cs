﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Admin;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Adds a per-broker unique ID to all branches for all active brokers.
    /// </summary>
    public class Opm246124_AddBranchIdNumberToBranches : Migrator<Guid>
    {
        /// <summary>
        /// Prevents a default instance of the <see cref="Opm246124_AddBranchIdNumberToBranches"/>
        /// class from being created.
        /// </summary>
        private Opm246124_AddBranchIdNumberToBranches() : base(nameof(Opm246124_AddBranchIdNumberToBranches))
        {
            this.MigrationHelper = new CustomGUIDMigrationHelper(nameof(Opm246124_AddBranchIdNumberToBranches), GetAllBrokerIds);
        }

        /// <summary>
        /// Executes the migration.
        /// </summary>
        /// <param name="args">
        /// This parameter is not used.
        /// </param>
        public static void Run(string[] args)
        {
            var migrator = new Opm246124_AddBranchIdNumberToBranches();
            migrator.Migrate();
        }

        private IEnumerable<Guid> GetAllBrokerIds()
        {
            var brokerIds = new List<Guid>();
            Action<IDataReader> readBrokerIds = reader =>
            {
                while (reader.Read())
                {
                    brokerIds.Add((Guid)reader["brokerid"]);
                }
            };

            foreach (var connectionInfo in DbConnectionInfo.ListAll())
            {
                DBSelectUtility.ProcessDBData(
                    connectionInfo,
                    "SELECT BrokerId FROM BROKER",
                    TimeoutInSeconds.Thirty,
                    parameters: null,
                    readerCode: readBrokerIds);
            }

            return brokerIds;
        }

        /// <summary>
        /// Migrates the branches for the broker with the specified <paramref name="brokerId"/>.
        /// </summary>
        /// <param name="brokerId">
        /// The ID for the broker.
        /// </param>
        /// <returns>
        /// True if the migration was successful, false otherwise.
        /// </returns>
        protected override bool MigrationFunction(Guid brokerId)
        {
            var branchIds = this.GetBranchIdsForBroker(brokerId);
            if (!branchIds.Any())
            {
                return true;
            }

            var driverFactory = GenericLocator<IStoredProcedureDriverFactory>.Factory;
            var driver = driverFactory.Create(TimeoutInSeconds.Thirty);

            using (var connection = DbConnectionInfo.GetConnection(brokerId))
            {
                try
                {
                    connection.Open();
                    foreach (var branchId in branchIds)
                    {
                        int branchIdNumber = BrokerDB.GetAndUpdateBranchIdNumberCounter(brokerId);

                        SqlParameter[] parameters =
                        {
                            new SqlParameter("@BrokerId", brokerId),
                            new SqlParameter("@BranchId", branchId),
                            new SqlParameter("@BranchIdNumber", branchIdNumber)
                        };

                        driver.ExecuteNonQuery(connection, null, StoredProcedureName.Create("AddBranchIdNumberToBranchForMigrator").Value, parameters);
                    }
                    
                    return true;
                }
                catch (Exception exc)
                {
                    Console.WriteLine("Failed to migrate branches for broker " + brokerId + ": " + exc.Message);
                    Tools.LogError("Failed to migrate branches for broker " + brokerId, exc);

                    return false;
                }
            }
        }

        /// <summary>
        /// Obtains the IDs for the branches of the broker with 
        /// the specified <paramref name="brokerId"/>.
        /// </summary>
        /// <param name="brokerId">
        /// The ID of the broker.
        /// </param>
        /// <returns>
        /// An <see cref="IEnumerable{T}"/> containing the IDs of the branches.
        /// </returns>
        private IEnumerable<Guid> GetBranchIdsForBroker(Guid brokerId)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", brokerId)
            };

            var branchIds = new List<Guid>();

            var queryString = SQLQueryString.Create(@"
SELECT BranchId
FROM BRANCH
WHERE BrokerId = @BrokerId AND BranchIdNumber = 0
ORDER BY BranchNm
");

            var factory = GenericLocator<ISqlDriverFactory>.Factory;
            var driver = factory.Create(TimeoutInSeconds.Thirty);

            using (var connection = DbConnectionInfo.GetConnection(brokerId))
            using (var reader = driver.Select(connection, transaction: null, query: queryString.Value, parameters: parameters))
            {
                while (reader.Read())
                {
                    branchIds.Add((Guid)reader["BranchId"]);
                }
            }

            return branchIds;
        }
    }
}
