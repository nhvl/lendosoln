﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using DataAccess;
using LendersOffice.Common;
using LendersOfficeApp.los.RatePrice;

namespace ScheduleExecutable.Migrations
{
    /// <summary>
    /// Copy PRICE_POLICY.PricePolicyXmlText and RATE_OPTIONS.lRateSheetXmlContent to PRICE_POLICY.RulesProtobufContent and RATE_OPTIONS.RatesProtobufContent.
    /// This is a temporary class. Remove after 6/1/2018.
    /// </summary>
    public class MigratePricePolicyRateOptions
    {
        /// <summary>
        /// Copy PRICE_POLICY.PricePolicyXmlText and RATE_OPTIONS.lRateSheetXmlContent to PRICE_POLICY.RulesProtobufContent and RATE_OPTIONS.RatesProtobufContent.
        /// </summary>
        /// <param name="args"></param>
        public static void Execute(string[] args)
        {
            string cmd = "all";

            if (args.Length == 2)
            {
                // Possible values are pricepolicy or rates.
                cmd = args[1];
            }

            Console.WriteLine("Cmd=" + cmd);
            if (cmd == "all" || cmd == "pricepolicy")
            {
                CopyPricePolicy();
            }

            if (cmd == "all" || cmd == "rates")
            {
                CopyRateOptions();
            }
        }

        private static void CopyPricePolicy()
        {
            string sql = "SELECT PricePolicyId FROM PRICE_POLICY";

            var list = GetIds(sql);

            int count = 0;
            int total = list.Count();
            foreach (var id in list)
            {
                count++;
                CopyPricePolicyImpl(id);
                if (count % 100 == 0)
                {
                    Console.WriteLine(DateTime.Now + " - Price_Policy Processed " + count + " of " + total);
                }
            }
        }

        private static void CopyRateOptions()
        {
            string sql = "SELECT SrcProgId FROM RATE_OPTIONS WHERE RatesProtobufContent IS NULL";

            var list = GetIds(sql);

            int count = 0;
            int total = list.Count();
            foreach (var id in list)
            {
                count++;
                CopyRateOptionsImpl(id);
                if (count % 100 == 0)
                {
                    Console.WriteLine(DateTime.Now + " - Rate_Options Processed " + count + " of " + total);
                }
            }
        }

        private static void CopyRateOptionsImpl(Guid id)
        {
            string selectSql = @"SELECT SrcProgId, lRateSheetXmlContent, ContentKey
FROM RATE_OPTIONS WHERE SrcProgId=@Id";

            Guid srcProgId = Guid.Empty;
            string lRateSheetXmlContent = string.Empty;
            string contentKey = string.Empty;

            Action<IDataReader> readHandler = delegate (IDataReader reader)
            {
                if (reader.Read())
                {
                    srcProgId = (Guid)reader["srcProgId"];
                    lRateSheetXmlContent = (string)reader["lRateSheetXmlContent"];
                    contentKey = (string)reader["ContentKey"];

                }
            };

            SqlParameter[] parameters =
            {
                new SqlParameter("@Id", id)
            };
            DBSelectUtility.ProcessDBData(DataSrc.LpeSrc, selectSql, null, parameters, readHandler);

            var rateOptionList = CLoanProductBase.ConvertRateOption(lRateSheetXmlContent);
            var bytes = SerializationHelper.ProtobufSerialize(rateOptionList);

            string updateSql = "UPDATE RATE_OPTIONS SET RatesProtobufContent=@RatesProtobufContent WHERE SrcProgId=@Id AND ContentKey=@ContentKey";

            SqlParameter[] updateParameters =
            {
                new SqlParameter("@ContentKey", contentKey),
                new SqlParameter("@Id", id),
                new SqlParameter("@RatesProtobufContent", bytes)
            };

            DBUpdateUtility.Update(DataSrc.LpeSrc, updateSql, null, updateParameters);
        }
        private static void CopyPricePolicyImpl(Guid id)
        {
            var pricePolicy = CPricePolicy.Retrieve(id);
            pricePolicy.Save();
        }

        private static IEnumerable<Guid> GetIds(string sql)
        {
            List<Guid> idList = new List<Guid>();
            Action<IDataReader> readHandler = delegate (IDataReader reader)
            {
                while (reader.Read())
                {
                    idList.Add((Guid)reader[0]);
                }

            };

            DBSelectUtility.ProcessDBData(DataSrc.LpeSrc, sql, null, null, readHandler);

            return idList;
        }

    }
}
