﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Text;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Drivers.Gateways;

    public class SetCellPhoneMfaBitMigrator : Migrator<Guid>
    {
        /// <summary>
        /// Dictionary mapping employee id to broker id.
        /// </summary>
        Dictionary<Guid, Guid> employeeIdToBrokerId;

        /// <summary>
        /// Error string builder.
        /// </summary>
        StringBuilder errorBuilder = new StringBuilder();

        /// <summary>
        /// Initializes a new instance of the <see cref="SetCellPhoneMfaBitMigrator"/> class.
        /// This will run a query to pull the employees to edit.
        /// </summary>
        /// <param name="name">The name of the migration.</param>
        public SetCellPhoneMfaBitMigrator(string name)
            : this(name, GetEmployeesWithUserMfaEnabled())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SetCellPhoneMfaBitMigrator"/> class.
        /// This uses the passed in employee ids.
        /// </summary>
        /// <param name="name">The name of the migration.</param>
        /// <param name="employeeIdToBrokerId">The employees to migrate.</param>
        public SetCellPhoneMfaBitMigrator(string name, Dictionary<Guid, Guid> employeeIdToBrokerId)
            : base(name)
        {
            this.employeeIdToBrokerId = employeeIdToBrokerId;
            this.MigrationHelper = new CustomGUIDMigrationHelper(name, () => employeeIdToBrokerId.Keys);
        }

        /// <summary>
        /// Runs the migration to toggle the EnableAuthCodeViaSms user bit.
        /// Usage:
        ///     ScheduleExecutable.exe SetCellPhoneMfaBit => Will run the migration on all employees that have their broker Opt-in MFA bit set to true and their user-level MFA bit set to true.
        ///     ScheduleExecutable.exe SetCellPhoneMfaBit [filePath] => Will run the migration on all employees specified in the file in [filePath]. 
        ///                                                                 The file must be in the form "employeeId,BrokerId". One per line.
        /// </summary>
        /// <param name="args">The command line arguments.</param>
        public static void SetCellPhoneMfaBit(string[] args)
        {
            SetCellPhoneMfaBitMigrator migrator;
            string name = $"SetCellPhoneMfaBit_{DateTime.Now.ToString("MM_dd_yyyy")}";
            if (args.Length == 2)
            {
                Dictionary<Guid, Guid> employeeToBroker = new Dictionary<Guid, Guid>();

                var lines = TextFileHelper.ReadLines(args[1]);
                foreach (string line in lines)
                {
                    string[] parts = line.Split(',');
                    Guid employeeId = Guid.Parse(parts[0]);
                    Guid brokerId = Guid.Parse(parts[1]);

                    employeeToBroker.Add(employeeId, brokerId);
                }

                migrator = new SetCellPhoneMfaBitMigrator(name, employeeToBroker);
            }
            else
            {
                migrator = new SetCellPhoneMfaBitMigrator(name);
            }

            migrator.Migrate();
        }

        /// <summary>
        /// If the employee has MFA enabled, then it sets EnableAuthCodeViaSms.
        /// </summary>
        /// <param name="id">The employee to migrate.</param>
        /// <returns>True if the migration is successful, false otherwise.</returns>
        protected override bool MigrationFunction(Guid id)
        {
            try
            {
                EmployeeDB employee = EmployeeDB.RetrieveById(this.employeeIdToBrokerId[id], id);

                if (employee.EnabledMultiFactorAuthentication)
                {
                    employee.EnableAuthCodeViaSms = true;
                    employee.Save();
                    return true;
                }

                return false;
            }
            catch (Exception e)
            {
                string error = $"Unable to edit employee {id} for {this.Name}.";
                this.errorBuilder.AppendLine(error);
                Tools.LogError(error, e);
            }

            return false;
        }

        protected override void EndMigration()
        {
            errorBuilder.AppendLine();
            TextFileHelper.AppendString(this.Name + "_errorLog.txt", errorBuilder.ToString());
            errorBuilder.Length = 0;
        }

        /// <summary>
        /// Gets the employees to edit. It will pull all employees that have their broker MFA opt in bit set to true AND
        /// their user-level MFA bit set to true.
        /// </summary>
        /// <returns>The dictionary mapping the found employees to broker ids.</returns>
        private static Dictionary<Guid, Guid> GetEmployeesWithUserMfaEnabled()
        {
            string query = @"SELECT 
                                    Employee.EmployeeId,
                                    Broker.BrokerId
                                FROM 
                                    Employee WITH (nolock) JOIN 
                                    Branch WITH (nolock) ON Branch.BranchId = Employee.BranchId JOIN 
                                    Broker WITH (nolock) ON Broker.BrokerId = Branch.BrokerId JOIN 
                                    Broker_User WITH (nolock) ON Broker_User.EmployeeId = Employee.EmployeeId
                                WHERE
	                                Broker.IsOptInMultiFactorAuthentication=1 AND
	                                Broker_User.EnabledMultiFactorAuthentication=1 ";

            var employeeToBroker = new Dictionary<Guid, Guid>();

            foreach (var connectionInfo in DbConnectionInfo.ListAll())
            {
                Action<IDataReader> readerCode = delegate (IDataReader reader)
                {
                    while (reader.Read())
                    {
                        employeeToBroker.Add((Guid)reader["EmployeeId"], (Guid)reader["BrokerId"]);
                    }
                };

                DBSelectUtility.ProcessDBData(connectionInfo, query, null, null, readerCode);
            }

            return employeeToBroker;
        }
    }
}
