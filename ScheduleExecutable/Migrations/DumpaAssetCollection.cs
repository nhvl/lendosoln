﻿namespace ScheduleExecutable.Migrations
{
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Common.TextImport;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.XsltExportReport;
    using LqbGrammar.DataTypes;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml.Linq;

    /// <summary>
    /// This dumps all aAssetCollection for all applications for all loans belonging to a broker.
    /// </summary>
    public class DumpaAssetCollection : Migrator<Guid>
    {
        private string currentFile;

        private XsltExportTransferSettings settings;        

        /// <summary>
        /// Initializes a new instance of the <see cref="DumpaAssetCollection"/> class.
        /// </summary>
        public DumpaAssetCollection(Guid brokerId, XsltExportTransferSettings settings) : base($"DumpaAssetCollection_{brokerId}_{DateTime.Now.ToString("MMddyyyyHHmm")}")
        {
            this.MigrationHelper = new CustomGUIDMigrationHelper(this.Name, () => GetLoansFundedThisMonthForBroker(brokerId));

            this.settings = settings;

            this.currentFile = TempFileUtils.NewTempFilePath();
            TextFileHelper.AppendString(this.currentFile, "LoanId, AppId, aAssetXmlContent\r\n");
        }
        
        public static void DumpaAssetCollectionData(string[] args)
        {
            if (args.Length != 3)
            {
                Console.WriteLine("Usage: ScheduleExecutable DumpaAssetCollection BrokerId ExportName");
                return;
            }

            Guid brokerId;

            if (!Guid.TryParse(args[1], out brokerId))
            {
                Console.WriteLine("Invalid input for BrokerId.");
                return;
            }

            var settings = XsltExportTransferSettings.RetrieveByExportName(args[2], brokerId);

            DumpaAssetCollection migrator = new DumpaAssetCollection(brokerId, settings);

            migrator.Migrate();
        }

        private static string SafeString(XElement el, string attrName)
        {
            XAttribute attr = el.Attribute(attrName);
            if (attr != null)
            {
                return attr.Value;
            }
            return string.Empty;
        }

        protected override void EndMigration()
        {
            var xElementSettings = settings.ToXElement();
            string password = EncryptionHelper.Decrypt(SafeString(xElementSettings, "password"));
            string login = SafeString(xElementSettings, "login");

            WinSCP.Protocol protocol;

            if (settings.TransferOptions.Protocol == FileTransferProtocol.Ftp)
            {
                protocol = WinSCP.Protocol.Ftp;
            }
            else if (settings.TransferOptions.Protocol == FileTransferProtocol.Scp)
            {
                protocol = WinSCP.Protocol.Scp;
            }
            else
            {
                protocol = WinSCP.Protocol.Sftp;
            }
            
            SFTPXsltExport.SFTPXsltExportCallBack.UploadFileUsingWinSCP(settings.HostName, login, password, this.currentFile, settings.DestPath, protocol, settings.Port, settings.TransferOptions.HostKeyFingerprint, settings.TransferOptions.Mode.ToString(), settings.TransferOptions.HostCertificateFingerprint, true);

            File.Delete(this.Name + "_all.txt");
            File.Delete(this.Name + "_backup.txt");
            File.Delete(this.Name + "_processed.txt");
            File.Delete(this.currentFile);
        }

        protected override bool MigrationFunction(Guid id)
        {
            try
            {
                var csvBuilder = new StringBuilder();
                CPageData loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(id, typeof(DumpaAssetCollection));

                loan.InitLoad();
                foreach (CAppData app in loan.Apps)
                {
                    string xml = app.aAssetXmlContent.Value;

                    if (string.IsNullOrWhiteSpace(xml.Replace("<XmlTable />", string.Empty)))
                    {
                        csvBuilder.AppendLine(id + ", " + app.aAppId);
                    }
                    else
                    {                        
                        if (xml.Contains("\r\n"))
                        {
                            xml = xml.Replace("\r\n", string.Empty);
                        }
                        if (xml.Contains("\n"))
                        {
                            xml = xml.Replace("\n", string.Empty);
                        }

                        csvBuilder.AppendLine(id + ", " + app.aAppId + ", " +  DataParsing.sanitizeForCSV(xml));
                    }
                }

                TextFileHelper.AppendString(this.currentFile, csvBuilder.ToString());

                return true;
            }
            catch (Exception exc)
            {
                Tools.LogError($"Could not export aAssetXmlContent for loan: {id}.", exc);
                return false;
            }
        }

        private static IEnumerable<Guid> GetLoansFundedThisMonthForBroker(Guid brokerId)
        {
            string sql = @"
SELECT sLId 
FROM 
    LOAN_FILE_CACHE
WHERE 
    sBrokerId = @BrokerId AND 
    sFundD > DateAdd(month, -1, GetDate()) AND
    IsValid = 1 AND
    IsTemplate = 0 AND
    (sLoanFileT = 0 or sLoanFileT IS null)";

            var loanIds = new LinkedList<Guid>();

            Action<IDataReader> readHandler = delegate (IDataReader reader)
            {
                while (reader.Read())
                {
                    loanIds.AddLast((Guid)reader["sLId"]);
                }
            };

            var connection = DbConnectionInfo.GetConnectionInfo(brokerId);

            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", brokerId)
            };

            DBSelectUtility.ProcessDBData(connection, sql, TimeoutInSeconds.Create(30), parameters, readHandler);

            return loanIds;
        }
    }
}
