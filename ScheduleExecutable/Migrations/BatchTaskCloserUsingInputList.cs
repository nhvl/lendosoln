﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using LendersOffice.Drivers.Gateways;

    /// <summary>
    /// Task Closer for MMI.
    /// </summary>
    public class BatchTaskCloserUsingInputList : BatchTaskCloser
    {
        /// <summary>
        /// Closes the tasks specified in the input list.
        /// </summary>
        /// <param name="args">The args from the command line.</param>
        public static void BatchCloseTasksUsingInputList(string[] args)
        {
            if (args.Length != 4)
            {
                Console.WriteLine("Usage: 'BatchCloseTasksUsingInputList [FilePath with list of task ids] [BrokerId] [OPM case to prefix migration name]'");
                return;
            }

            var taskIds = TextFileHelper.ReadLines(args[1]).ToList();

            BatchTaskCloserUsingInputList closer = new BatchTaskCloserUsingInputList(args[3] + "_BatckTaskCloserUsingInputList", new Guid(args[2]), taskIds);
            closer.Migrate();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BatchTaskCloserUsingInputList"/> class.
        /// </summary>
        /// <param name="name">The name of the migration.</param>
        /// <param name="taskIdsToClose">The task ids to close.</param>
        public BatchTaskCloserUsingInputList(string name, Guid brokerId, List<string> taskIdsToClose)
            : base(name, brokerId, () => taskIdsToClose)
        {
        }

        /// <summary>
        /// Not used.
        /// </summary>
        /// <returns>An empty string.</returns>
        protected override IEnumerable<string> GetTaskIds()
        {
            return new List<string>();
        }
    }
}
