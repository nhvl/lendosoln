﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ConfigSystem;
    using DataAccess;
    using LendersOffice.ConfigSystem;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Constants;
    using LendersOffice.Reports;
    using LendersOffice.Security;

    /// <summary>
    /// Adds the default AddLoanStatusChange operation to the system config. Allows all status changes, always.
    /// </summary>
    class Opm464845_ReplaceOldLoanStatusChangeRules : Migrator<Guid>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Opm464845_ReplaceOldLoanStatusChangeRules"/> class.
        /// </summary>
        /// <param name="name">The name of the migration.</param>
        public Opm464845_ReplaceOldLoanStatusChangeRules(string name)
                : base(name)
            {
            this.MigrationHelper = new BrokerMigrationHelper(name);
        }

        /// <summary>
        /// Removes old rules from system config.
        /// </summary>
        /// <returns></returns>
        public static void MigrateSystemConfig(string[] args)
        {
            WorkflowSystemConfigModel systemConfig = new WorkflowSystemConfigModel(ConstAppDavid.SystemBrokerGuid, loadDraft: false);

            // Remove old rules.
            IEnumerable<Guid> removeConditionIds = systemConfig.GetFullConditionSet()
                .Where(c => c.SysOpSet.SystemOperationNames.Contains(WorkflowOperations.LoanStatusChangeToConditionReview.Id) ||
                    c.SysOpSet.SystemOperationNames.Contains(WorkflowOperations.LoanStatusChangeToDocumentCheck.Id))
                .Select(c => c.Id).ToArray();

            foreach (Guid id in removeConditionIds)
            {
                systemConfig.RemoveCondition(id);
            }

            // Add new rule.
            Parameter alwaysFalse = new Parameter("AlwaysTrue", E_FunctionT.Equal, E_ParameterValueType.Bool, false, false);
            alwaysFalse.AddValue("False", "False");

            Condition condition = new Condition();
            condition.ParameterSet.AddParameter(alwaysFalse);
            condition.SysOpSet.Add(new SystemOperation(WorkflowOperations.LoanStatusChange));
            condition.Notes = $"Created automatically by LendingQB. OPM 464845.";
            condition.FailureMessage = "You do not have permission to change loan status.";
            systemConfig.AddNew(condition);

            // Save and publish.
            systemConfig.SwitchToDraft();
            systemConfig.Save();

            try
            {
                WorkflowSystemConfigModel.Publish(ConstAppDavid.SystemBrokerGuid, SystemUserPrincipal.TaskSystemUser);
            }
            catch (ConfigValidationException e)
            {
                Tools.LogError(e.ToString(), e);
                throw;
            }
        }

        /// <summary>
        /// Adds a rule to the system config that allows loan status to be changed to any value.
        /// </summary>
        public static void MigrateBrokerConfigs(string[] args)
        {

            Opm464845_ReplaceOldLoanStatusChangeRules migrator = new Opm464845_ReplaceOldLoanStatusChangeRules("Opm464845_ReplaceOldLoanStatusChangeRules");
            migrator.Migrate();
        }

        /// <summary>
        /// Migrates a broker config.
        /// </summary>
        /// <param name="id">Broker ID of broker to migrate.</param>
        /// <returns></returns>
        protected override bool MigrationFunction(Guid id)
        {
            WorkflowSystemConfigModel config = new WorkflowSystemConfigModel(id, loadDraft: false);

            bool wasModified = ReplaceOldLoanStatusChangeOps(config.GetFullConditionSet());
            wasModified |= ReplaceOldLoanStatusChangeOps(config.GetFullConstraintSet());

            // Don't save if no changes were made.
            // NOTE: Empty configs throw if you try to save them. So we need this check.
            if (!wasModified)
            {
                return true;
            }

            config.SwitchToDraft();
            config.Save();

            try
            {
                WorkflowSystemConfigModel.Publish(id, SystemUserPrincipal.TaskSystemUser);
            }
            catch (ConfigValidationException e)
            {
                Tools.LogError(e.ToString(), e);
                throw;
            }

            return true;
        }

        /// <summary>
        /// Replaces the old LoanStatusChange workflow ops with the new old.
        /// </summary>
        /// <param name="conditions">Conditions to check and modify if necessary.</param>
        /// <returns>Returns true if any condition was changed. False otherwise.</returns>
        private bool ReplaceOldLoanStatusChangeOps(IEnumerable<AbstractConditionGroup> conditions)
        {
            bool wasModified = false;

            foreach (AbstractConditionGroup condition in conditions)
            {
                bool hasConditionReview = false;
                bool hasDocumentCheck = false;

                if (condition.SysOpSet.SystemOperationNames.Contains(WorkflowOperations.LoanStatusChangeToConditionReview.Id))
                {
                    hasConditionReview = true;
                    condition.SysOpSet.Remove(WorkflowOperations.LoanStatusChangeToConditionReview.Id);
                }

                if (condition.SysOpSet.SystemOperationNames.Contains(WorkflowOperations.LoanStatusChangeToDocumentCheck.Id))
                {
                    hasDocumentCheck = true;
                    condition.SysOpSet.Remove(WorkflowOperations.LoanStatusChangeToDocumentCheck.Id);
                }

                if (hasConditionReview || hasDocumentCheck)
                {
                    SystemOperation LoanStatusChangeOperation = new SystemOperation(WorkflowOperations.LoanStatusChange);

                    if (hasConditionReview)
                    {
                        LoanStatusChangeOperation.AddField(new OperationField("Loan Status (sStatusT) : Condition Review", $"sStatusT:{E_sStatusT.Loan_ConditionReview.ToString("d")}", string.Empty, true, true));
                    }

                    if (hasDocumentCheck)
                    {
                        LoanStatusChangeOperation.AddField(new OperationField("Loan Status (sStatusT) : Document Check", $"sStatusT:{E_sStatusT.Loan_DocumentCheck.ToString("d")}", string.Empty, true, true));
                    }

                    condition.SysOpSet.Add(LoanStatusChangeOperation);

                    wasModified = true;
                }
            }

            return wasModified;
        }
    }
}
