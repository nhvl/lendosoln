﻿namespace ScheduleExecutable.Migrations
{
    using DataAccess;
    using LendersOffice.Drivers.SqlServerDB;
    using LendersOffice.Integration.MortgageInsurance;
    using LqbGrammar.DataTypes;
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml.Linq;

    public class MIApplicationTypeMigrator : Migrator<string>
    {
        private const string updateQuery = @"
UPDATE MORTGAGE_INSURANCE_ORDER_INFO
SET DelegationType = @DelegationType
WHERE sLId = @sLId AND 
VendorId = @VendorId AND 
OrderNumber = @OrderNumber
";

        public MIApplicationTypeMigrator() : base(nameof(MIApplicationTypeMigrator))
        {
            this.MigrationHelper = new CustomStringMigrationHelper(this.Name, GetMigratorStrings);
        }

        private IEnumerable<string> GetMigratorStrings()
        {
            var sqlQuery = @"
SELECT mi.sLId, VendorId, OrderNumber, ResponseXmlContent, sBrokerId
FROM MORTGAGE_INSURANCE_ORDER_INFO mi JOIN Loan_File_cache c ON mi.sLid = c.sLId
WHERE IsQuote = 0 AND 
DelegationType = 0 AND
(ResponseXmlContent LIKE '%MIApplicationType=""Delegated""%' OR ResponseXmlContent LIKE '%MIApplicationType=""Standard""%')";

            var apps = new List<MiApplication>();

            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                using (var conn = connInfo.GetConnection())
                {
                    using (var reader = SqlServerHelper.Select(conn, null, SQLQueryString.Create(sqlQuery).Value, null, TimeoutInSeconds.Default))
                    {
                        while (reader.Read())
                        {
                            apps.Add(new MiApplication()
                            {
                                sLId = (Guid)reader["sLId"],
                                VendorId = (Guid)reader["VendorId"],
                                OrderNumber = reader["OrderNumber"] as string,
                                XmlContent = reader["ResponseXmlContent"] as string,
                                BrokerId = (Guid)reader["sBrokerId"]
                            });
                        }
                    }
                }
            }

            var strList = new List<string>();

            foreach (var app in apps)
            {
                string customStr = $"{app.sLId.ToString()}_{app.VendorId.ToString()}_{app.OrderNumber}_{app.BrokerId}_";

                var doc = XDocument.Parse(app.XmlContent);
                var response = doc.Descendants("MI_RESPONSE").First();
                
                if (response == null)
                {
                    continue;
                }

                var attr = response.Attribute("MIApplicationType");

                if (attr == null)
                {
                    continue;
                }

                if (attr.Value.Equals("Delegated"))
                {
                    customStr += "true";
                }
                else if (attr.Value.Equals("Standard"))
                {
                    customStr += "false";
                }
                else
                {
                    continue;
                }

                strList.Add(customStr);
            }

            return strList;
        } 

        private class MiApplication
        {
            public Guid sLId;
            public Guid VendorId;
            public string OrderNumber;
            public string XmlContent;
            public Guid BrokerId;
        }

        public static void MigrateMIApplicationType(string [] args)
        {
            var migrator = new MIApplicationTypeMigrator();
            migrator.Migrate();
        }

        protected override bool MigrationFunction(string customStr)
        {
            try
            {
                string[] splitStr = customStr.Split('_');

                Guid sLId = new Guid(splitStr[0]);
                Guid vendorId = new Guid(splitStr[1]);
                string orderNumber = splitStr[2];
                Guid brokerId = new Guid(splitStr[3]);
                bool isDelegated = Convert.ToBoolean(splitStr[4]);

                var parameters = new List<SqlParameter>()
                {
                    new SqlParameter("@sLId", sLId),
                    new SqlParameter("@VendorId", vendorId),
                    new SqlParameter("@OrderNumber", orderNumber)
                };

                if (isDelegated)
                {
                    parameters.Add(new SqlParameter("@DelegationType", DelegationType.Delegated));
                }
                else
                {
                    parameters.Add(new SqlParameter("@DelegationType", DelegationType.NonDelegated));
                }

                using (var conn = DbConnectionInfo.GetConnection(brokerId))
                {
                    SqlServerHelper.Update(conn, null, SQLQueryString.Create(updateQuery).Value, parameters, TimeoutInSeconds.Default);
                }

                return true;
            }            
            catch (Exception exc)
            {
                Tools.LogError($"Could not migrate for current settings {customStr}.", exc);
                return false;
            }
        }
    }
}
