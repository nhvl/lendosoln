﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Text;
    using DataAccess;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.Gateways;

    /// <summary>
    /// Adds Ids to the adjustments and prorations collections.
    /// </summary>
    public class AddIdsToAdjustmentsAndProrationsMigrator : Migrator<Guid>
    {
        /// <summary>
        /// String builder to hold any errors.
        /// </summary>
        private StringBuilder errorBuilder = new StringBuilder();

        /// <summary>
        /// Initializes a new instance of the <see cref="AddIdsToAdjustmentsAndProrationsMigrator"/> class.
        /// </summary>
        public AddIdsToAdjustmentsAndProrationsMigrator()
            : base($"AddIdsToAdjustmentsAndProrations_{DateTime.Now.ToString("MM_dd_yyyy")}")
        {
            this.MigrationHelper = new CustomGUIDMigrationHelper(this.Name, GetLoansToMigrate);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AddIdsToAdjustmentsAndProrationsMigrator"/> class.
        /// </summary>
        /// <param name="loansToMigrate">The loan ids to migrate.</param>
        public AddIdsToAdjustmentsAndProrationsMigrator(IEnumerable<Guid> loansToMigrate)
            : base($"AddIdsToAdjustmentsAndProrations_{DateTime.Now.ToString("MM_dd_yyyy")}")
        {
            this.MigrationHelper = new CustomGUIDMigrationHelper(this.Name, () => loansToMigrate);
        }

        /// <summary>
        /// Adds ids to the adjustments and prorations.
        /// 'ScheduleExecutable AddIdsToAdjustmentsAndProrations' => Will get the loan ids via the query.
        /// 'ScheduleExecutable AddIdsToAdjustmentsAndProrations File.txt' => Will use the loan ids listed in File.txt.
        /// </summary>
        /// <param name="args">The command line arguments.</param>
        public static void AddIdsToAdjustmentsAndProrations(string[] args)
        {
            AddIdsToAdjustmentsAndProrationsMigrator migrator;
            if (args.Length == 1)
            {
                migrator = new AddIdsToAdjustmentsAndProrationsMigrator();
            }
            else
            {
                List<Guid> loanIds = new List<Guid>();
                var lines = TextFileHelper.ReadLines(args[1]);
                foreach (var line in lines)
                {
                    loanIds.Add(Guid.Parse(line));
                }

                migrator = new AddIdsToAdjustmentsAndProrationsMigrator(loanIds);
            }

            migrator.Migrate();
        }

        /// <summary>
        /// Runs a query to get the loans to migrate.
        /// This looks for any valid loan that has data in at least one of the two collections and is in TRID2015 mode.
        /// </summary>
        /// <returns>The loans to migrate.</returns>
        private static IEnumerable<Guid> GetLoansToMigrate()
        {
            // We're looking for all valid files that have one of the two collections filled in
            // and are in TRID2015 mode.
            string query = @"select 
	                                lfa.slid
                                from 
	                                loan_file_b lfb join
	                                loan_file_a lfa on lfb.slid=lfa.slid
                                where 
	                                (lfb.sadjustmentlistjson<>'' OR lfb.sprorationlistjson<>'') AND
	                                lfb.sDisclosureRegulationT=2 AND 
	                                lfa.isValid=1";

            var loanIds = new List<Guid>();

            Action<IDataReader> readHandler = delegate (IDataReader reader)
            {
                while (reader.Read())
                {
                    loanIds.Add((Guid)reader["slid"]);
                }
            };

            foreach (var connectionInfo in DbConnectionInfo.ListAll())
            {
                DBSelectUtility.ProcessDBData(connectionInfo, query, null, null, readHandler);
            }

            return loanIds;
        }

        /// <summary>
        /// We only need to load the collections and then save immediately afterwards.
        /// The save itself will go ahead and add the ids to the collections.
        /// </summary>
        /// <param name="id">The id of the loan to migrate.</param>
        /// <returns>True if saved. False otherwise.</returns>
        protected override bool MigrationFunction(Guid id)
        {
            try
            {
                CPageData loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(id, typeof(AddIdsToAdjustmentsAndProrationsMigrator));
                loan.InitSave(ConstAppDavid.SkipVersionCheck);
                loan.ByPassFieldSecurityCheck = true;
                loan.DisableFieldEnforcement();

                if (loan.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID)
                {
                    return false;
                }

                // We only need to make sure the collections are loaded up. Saving will take care of adding the ids if needed.
                var tempAdjustments = loan.sAdjustmentList;
                var tempProrations = loan.sProrationList;

                loan.Save();
                return true;
            }
            catch (Exception e)
            {
                Tools.LogError($"Unable to migrate loan id {id.ToString()}", e);
                errorBuilder.AppendLine(id.ToString());
                return false;
            }
        }

        /// <summary>
        /// Writes any errors to the error log after migration.
        /// </summary>
        protected override void EndMigration()
        {
            errorBuilder.AppendLine();
            TextFileHelper.AppendString(this.Name + "_errorLog.txt", errorBuilder.ToString());
            errorBuilder.Length = 0;
        }
    }
}
