﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.IO;
    using DataAccess;
    using LendersOffice.Audit;

    /// <summary>
    /// FirstTech has a large userbase that still uses the old DU seamless integration, which does not record its runs
    /// to the AUS ordering dashboard. They have asked for a list of loans with these legacy seamless AUS runs so they can
    /// complete their HMDA reporting. Other clients still using the old AUS integrations like NFLP may also want to use this.
    /// </summary>
    /// <remarks>This can be removed after 1 March 2019.</remarks>
    public class FindMissingAusRunsForClient : Migrator<Guid>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FindMissingAusRunsForClient"/> class.
        /// </summary>
        /// <param name="name">The name of the migration.</param>
        /// <param name="customerCode">The customer code for the client on whose behalf this is being run.</param>
        /// <param name="directoryPath">The directory in which the results file will be stored.</param>
        public FindMissingAusRunsForClient(string name, string customerCode, string directoryPath) : base(name)
        {
            this.BrokerId = Tools.GetBrokerIdByCustomerCode(customerCode);
            this.ResultsDirectory = directoryPath;
            this.ResultsFileName = $"{customerCode}_AusRuns.csv";
            this.MigrationHelper = new CustomGUIDMigrationHelper(name, this.RetrieveLoansForAnalysis);
        }

        /// <summary>
        /// Gets the broker ID indicating the client on whose behalf the migration is being run.
        /// </summary>
        protected Guid BrokerId { get; }

        /// <summary>
        /// Gets the directory for the results.
        /// </summary>
        protected string ResultsDirectory { get; }

        /// <summary>
        /// Gets the file name for the results.
        /// </summary>
        protected string ResultsFileName { get; }

        /// <summary>
        /// Gets the full file path for the results.
        /// </summary>
        protected string ResultsFilePath
        {
            get
            {
                if (this.ResultsDirectory.EndsWith(@"\"))
                {
                    return this.ResultsDirectory + this.ResultsFileName;
                }
                else
                {
                    return $@"{this.ResultsDirectory}\{this.ResultsFileName}";
                }
            }
        }

        /// <summary>
        /// Runs the migration.
        /// </summary>
        /// <param name="args">
        /// String arguments:
        /// args[0] is the migration name.
        /// args[1] is the customer code.
        /// args[2] is the directory for storage (ex: "C:\Opm478408\")</param>
        public static void Run(string[] args)
        {
            // The first argument is the name of the migration.
            var customerCode = args[1];
            var directoryPath = args[2];

            if (string.IsNullOrEmpty(directoryPath))
            {
                directoryPath = @"C:\Opm478408\";
            }

            var migrator = new FindMissingAusRunsForClient(nameof(FindMissingAusRunsForClient), customerCode, directoryPath);
            migrator.Migrate();
        }

        /// <summary>
        /// Retrieves a collection of loans associated with the client that have not submitted an AUS order
        /// via the AUS Ordering Dashboard.
        /// </summary>
        /// <returns>A collection of loans to analyze.</returns>
        public IEnumerable<Guid> RetrieveLoansForAnalysis()
        {
            var loanIds = new List<Guid>();

            var parameters = new SqlParameter[]
            {
                new SqlParameter("@BrokerId", this.BrokerId)
            };

            var sql = @"
                SELECT sLId
                FROM LOAN_FILE_CACHE
                WHERE sBrokerId = @BrokerId
                    AND IsValid = 1
                    AND ((sHmdaActionD IS NULL AND sOpenedD > '01/01/2017') OR sHmdaActionD > CONVERT(datetime, '2017-12-31'))
                    AND sLId NOT IN (
                        SELECT DISTINCT LoanId
                        FROM AUS_ORDERS
                        WHERE BrokerId = @BrokerId
                    )";

            Action<IDataReader> readHandler = delegate (IDataReader reader)
            {
                while (reader.Read())
                {
                    loanIds.Add((Guid)reader["sLId"]);
                }
            };

            DBSelectUtility.ProcessDBData(this.BrokerId, sql, null, parameters, readHandler);

            return loanIds;
        }

        /// <summary>
        /// Sets up the file if necessary prior to the migration.
        /// </summary>
        protected override void BeginMigration()
        {
            if (!Directory.Exists(this.ResultsDirectory))
            {
                Directory.CreateDirectory(this.ResultsDirectory);
            }

            if (!File.Exists(this.ResultsFilePath))
            {
                var file = File.Create(this.ResultsFilePath);
                file.Close();

                // Filestreams don't let you write lines of text easily, so just close it out and use the helper method.
                this.WriteLineToResults("Loan Number (sLNm),Primary Borrower Last Name (aBLastNm),Loan Status (sStatusT),HMDA Action Taken (sHmdaActionTakenT),HMDA Action Date (sHmdaActionD),Credit Decision Date (sCreditDecisionD),Most Recent AUS Result (sProd3rdPartyUwResultT),Audit Record Date/Time");
            }
        }

        /// <summary>
        /// Analyzes the audit history of a loan file to see if it has any records from the old seamless DO/DU.
        /// These occurrences are written to a CSV file for the client.
        /// </summary>
        /// <param name="loanId">The ID of the loan being analyzed.</param>
        /// <returns>A boolean indicating whether the analysis was successful.</returns>
        protected override bool MigrationFunction(Guid loanId)
        {
            try
            {
                var dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(FindMissingAusRunsForClient));
                dataLoan.InitLoad();

                var auditRecords = AuditManager.RetrieveAuditList(loanId);
                foreach (var auditRecord in auditRecords)
                {
                    if (!auditRecord.Description.Contains("Update From DO/DU"))
                    {
                        continue;
                    }

                    this.WriteLineToResults($"\"{dataLoan.sLNm}\",\"{dataLoan.GetAppData(0).aBLastNm}\",\"{dataLoan.sStatusT_rep}\",\"{dataLoan.sHmdaActionTakenT.ToString()}\",\"{dataLoan.sHmdaActionD_rep}\",\"{dataLoan.sCreditDecisionD_rep}\",\"{dataLoan.sProd3rdPartyUwResultT_rep}\",\"{auditRecord.TimestampDescription}\"");
                }

                return true;
            }
            catch (Exception exc)
            {
                Tools.LogError($"Failed to analyze AUS runs in audit history for loan {loanId} under client {this.BrokerId}.", exc);
                return false;
            }
        }

        /// <summary>
        /// Writes a line to the results file.
        /// </summary>
        /// <param name="line">The line to write.</param>
        private void WriteLineToResults(string line)
        {
            using (StreamWriter sw = File.AppendText(this.ResultsFilePath))
            {
                sw.WriteLine(line);
            }
        }
    }
}