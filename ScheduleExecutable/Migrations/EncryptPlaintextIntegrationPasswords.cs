﻿namespace ScheduleExecutable.Migrations
{
    using static RowBasedEncryptionGenericTableMigrator;

    public class EncryptPlaintextIntegrationPasswords
    {
        public static void Encrypt_ConsumerPortalUser(string[] args)
        {
            var table = new TableToEncrypt
            {
                TableName = "CONSUMER_PORTAL_USER",
                PrimaryKeyColumns = new[] { new PrimaryKeyColumn { ColumnName = "Id" } },
                Columns = new ColumnToEncrypt[]
                {
                    new UnencryptedColumnToEncrypt
                    {
                        ExistingColumnName = "SecurityPin",
                        TargetColumnName = "EncryptedSecurityPin",
                    },
                },
                SelectStatementForKeys = @"SELECT Id FROM CONSUMER_PORTAL_USER WHERE EncryptionKeyId = '00000000-0000-0000-0000-000000000000'",
                SelectStatementForRecord = @"SELECT EncryptionKeyId,
    Id,
    SecurityPin
FROM CONSUMER_PORTAL_USER
WHERE Id = @Id AND EncryptionKeyId = '00000000-0000-0000-0000-000000000000'",
                UpdateStatement = @"UPDATE CONSUMER_PORTAL_USER
SET EncryptionKeyId = @EncryptionKeyId,
    EncryptedSecurityPin = @EncryptedSecurityPin
WHERE Id = @Id",
            };

            MigrateLenderTable(table);
        }

        public static void Encrypt_CreditReportAccountProxy(string[] args)
        {
            var table = new TableToEncrypt
            {
                TableName = "CREDIT_REPORT_ACCOUNT_PROXY",
                PrimaryKeyColumns = new[] { new PrimaryKeyColumn { ColumnName = "CrAccProxyId" } },
                Columns = new ColumnToEncrypt[]
                {
                    new UnencryptedColumnToEncrypt
                    {
                        ExistingColumnName = "CraPassword",
                        TargetColumnName = "EncryptedCraPassword",
                    },
                    new UnencryptedColumnToEncrypt
                    {
                        ExistingColumnName = "CreditMornetPlusPassword",
                        TargetColumnName = "EncryptedCreditMornetPlusPassword",
                    },
                },
                SelectStatementForKeys = @"SELECT CrAccProxyId FROM CREDIT_REPORT_ACCOUNT_PROXY WHERE EncryptionKeyId = '00000000-0000-0000-0000-000000000000'",
                SelectStatementForRecord = @"SELECT EncryptionKeyId,
    CrAccProxyId,
    CraPassword,
    CreditMornetPlusPassword
FROM CREDIT_REPORT_ACCOUNT_PROXY
WHERE CrAccProxyId = @CrAccProxyId AND EncryptionKeyId = '00000000-0000-0000-0000-000000000000'",
                UpdateStatement = @"UPDATE CREDIT_REPORT_ACCOUNT_PROXY
SET EncryptionKeyId = @EncryptionKeyId,
    EncryptedCraPassword = @EncryptedCraPassword,
    EncryptedCreditMornetPlusPassword = @EncryptedCreditMornetPlusPassword
WHERE CrAccProxyId = @CrAccProxyId",
            };

            MigrateLenderTable(table);
        }

        public static void Encrypt_TitleVendor(string[] args)
        {
            var table = new TableToEncrypt
            {
                TableName = "TITLE_VENDOR",
                PrimaryKeyColumns = new[] { new PrimaryKeyColumn { ColumnName = "TitleVendorId" } },
                Columns = new ColumnToEncrypt[]
                {
                    new UnencryptedColumnToEncrypt
                    {
                        ExistingColumnName = "LQBServicePassword",
                        TargetColumnName = "EncryptedLQBServicePassword",
                    },
                },
                SelectStatementForKeys = @"SELECT TitleVendorId FROM TITLE_VENDOR WHERE EncryptionKeyId = '00000000-0000-0000-0000-000000000000'",
                SelectStatementForRecord = @"SELECT EncryptionKeyId,
    TitleVendorId,
    LQBServicePassword
FROM TITLE_VENDOR
WHERE TitleVendorId = @TitleVendorId AND EncryptionKeyId = '00000000-0000-0000-0000-000000000000'",
                UpdateStatement = @"UPDATE TITLE_VENDOR
SET EncryptionKeyId = @EncryptionKeyId,
    EncryptedLQBServicePassword = @EncryptedLQBServicePassword
WHERE TitleVendorId = @TitleVendorId",
            };

            MigrateTable(DataAccess.DataSrc.LOShare, table);
        }

        public static void Encrypt_GenericFrameworkVendor(string[] args)
        {
            var table = new TableToEncrypt
            {
                TableName = "GENERIC_FRAMEWORK_VENDOR_CONFIG",
                PrimaryKeyColumns = new[] { new PrimaryKeyColumn { ColumnName = "ProviderID" } },
                Columns = new ColumnToEncrypt[]
                {
                    new UnencryptedColumnToEncrypt
                    {
                        ExistingColumnName = "CredentialXML",
                        TargetColumnName = "EncryptedCredentialXML",
                    },
                },
                SelectStatementForKeys = @"SELECT ProviderID FROM GENERIC_FRAMEWORK_VENDOR_CONFIG WHERE EncryptionKeyId = '00000000-0000-0000-0000-000000000000'",
                SelectStatementForRecord = @"SELECT EncryptionKeyId,
    ProviderID,
    CredentialXML
FROM GENERIC_FRAMEWORK_VENDOR_CONFIG
WHERE ProviderID = @ProviderID AND EncryptionKeyId = '00000000-0000-0000-0000-000000000000'",
                UpdateStatement = @"UPDATE GENERIC_FRAMEWORK_VENDOR_CONFIG
SET EncryptionKeyId = @EncryptionKeyId,
    EncryptedCredentialXML = @EncryptedCredentialXML
WHERE ProviderID = @ProviderID",
            };

            MigrateTable(DataAccess.DataSrc.LOShare, table);
        }
    }
}
