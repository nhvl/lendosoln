﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using DataAccess;
    using LendersOffice.Integration.VOXFramework;

    /// <summary>
    /// Case 461139 gives lenders the ability to enable/disable individual VOA options
    /// (Account History and Refresh Period) made available by vendors. At release, 
    /// every available option will be enabled, to match current behavior.
    /// </summary>
    public class Opm461139_EnableAllVendorVoaOptionsForLenderServices : Migrator<Tuple<Guid, int>>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Opm461139_EnableAllVendorVoaOptionsForLenderServices"/> class.
        /// </summary>
        /// <param name="name">The migration name.</param>
        public Opm461139_EnableAllVendorVoaOptionsForLenderServices(string name)
            : base(name)
        {
            this.MigrationHelper = new CustomTupleMigrationHelper<Guid, int>(name, RetrieveLenderServiceKeySet, Guid.Parse, int.Parse);
        }

        /// <summary>
        /// Runs the migration.
        /// </summary>
        /// <param name="args">String arguments.</param>
        public static void Run(string[] args)
        {
            var migrator = new Opm461139_EnableAllVendorVoaOptionsForLenderServices(nameof(Opm461139_EnableAllVendorVoaOptionsForLenderServices));
            migrator.Migrate();
        }

        /// <summary>
        /// Retrieves the IDs for all VOA lender services.
        /// </summary>
        /// <returns>A list of IDs for VOA lender services.</returns>
        private List<Tuple<Guid, int>> RetrieveLenderServiceKeySet()
        {
            var lenderServiceIds = new List<Tuple<Guid, int>>();
            var sql = @"
                SELECT BrokerId, LenderServiceId
                FROM VERIFICATION_LENDER_SERVICE
                WHERE ServiceT = 0
            ";

            Action<IDataReader> readHandler = delegate (IDataReader reader)
            {
                while (reader.Read())
                {
                    var brokerId = (Guid)reader["BrokerId"];
                    var lenderServiceId = (int)reader["LenderServiceId"];
                    var lenderService = new Tuple<Guid, int>(brokerId, lenderServiceId);
                    lenderServiceIds.Add(lenderService);
                }
            };

            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                DBSelectUtility.ProcessDBData(connInfo, sql, null, null, readHandler);
            }

            return lenderServiceIds;
        }

        /// <summary>
        /// Adds a configured VOA option to all VOA lender services for each corresponding option supported by the vendor.
        /// </summary>
        /// <param name="keys">The keys for a lender service.</param>
        /// <returns>A boolean indicating whether the migration was successful.</returns>
        protected override bool MigrationFunction(Tuple<Guid, int> keys)
        {
            var lenderService = VOXLenderService.GetLenderServiceById(keys.Item1, keys.Item2);
            var configuration = VOAOptionConfiguration.Load(lenderService.BrokerId, lenderService.LenderServiceId, lenderService.AssociatedVendorId);

            var vendor = VOXVendor.LoadVendor(lenderService.AssociatedVendorId);
            if (!vendor.Services.ContainsKey(VOXServiceT.VOA_VOD))
            {
                // VOA lender service has an associated vendor with no VOA vendor service...?
                return false;
            }

            bool itemsToSave = false;
            var voaService = (VOAVendorService)vendor.Services[VOXServiceT.VOA_VOD];

            // Account History
            foreach (int optionValue in voaService.AccountHistoryOptions)
            {
                if (lenderService.EnabledForLqbUsers)
                {
                    var option = this.GenerateOption(
                        lenderService.AssociatedVendorId,
                        lenderService.BrokerId,
                        lenderService.LenderServiceId,
                        VOAOptionType.AccountHistory,
                        optionValue,
                        userType: "B",
                        isDefault: false);
                    configuration.AccountHistoryOptions_LqbUsers.Add(option);
                    itemsToSave = true;
                }

                if (lenderService.EnabledForTpoUsers)
                {
                    var option = this.GenerateOption(
                        lenderService.AssociatedVendorId,
                        lenderService.BrokerId,
                        lenderService.LenderServiceId,
                        VOAOptionType.AccountHistory,
                        optionValue,
                        userType: "P",
                        isDefault: false);
                    configuration.AccountHistoryOptions_PmlUsers.Add(option);
                    itemsToSave = true;
                }
            }

            // Refresh Period
            foreach (int optionValue in voaService.RefreshPeriodOptions)
            {
                if (lenderService.EnabledForLqbUsers)
                {
                    var option = this.GenerateOption(
                        lenderService.AssociatedVendorId,
                        lenderService.BrokerId,
                        lenderService.LenderServiceId,
                        VOAOptionType.RefreshPeriod,
                        optionValue,
                        userType: "B",
                        isDefault: false);
                    configuration.RefreshPeriodOptions_LqbUsers.Add(option);
                    itemsToSave = true;
                }

                if (lenderService.EnabledForTpoUsers)
                {
                    var option = this.GenerateOption(
                        lenderService.AssociatedVendorId,
                        lenderService.BrokerId,
                        lenderService.LenderServiceId,
                        VOAOptionType.RefreshPeriod,
                        optionValue,
                        userType: "P",
                        isDefault: false);
                    configuration.RefreshPeriodOptions_PmlUsers.Add(option);
                    itemsToSave = true;
                }
            }

            if (itemsToSave)
            {
                using (var exec = new CStoredProcedureExec(lenderService.BrokerId))
                {
                    try
                    {
                        exec.BeginTransactionForWrite();

                        // We MUST override validation in the save because validation requires a default option be set for each category
                        // and we're leaving it to lenders to set their own defaults.
                        //// TODO: Remove overrideDefaultValidation parameter when this migration is deleted.
                        string errorMessage = string.Empty;
                        if (!configuration.SaveConfiguration(exec, out errorMessage, overrideDefaultValidation: true))
                        {
                            exec.RollbackTransaction();
                            Tools.LogError($"Failed to enable default options for broker {lenderService.BrokerId}: {errorMessage}");
                            return false;
                        }

                        exec.CommitTransaction();
                        return true;
                    }
                    catch (Exception exc)
                    {
                        exec.RollbackTransaction();
                        Tools.LogError("Failed to enable default options for broker {lenderService.BrokerId}", exc);
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Generates a new VOA option.
        /// </summary>
        /// <param name="vendorId">The vendor ID.</param>
        /// <param name="brokerId">The broker ID.</param>
        /// <param name="lenderServiceId">The lender service ID.</param>
        /// <param name="type">The option type.</param>
        /// <param name="value">The option value.</param>
        /// <param name="userType">The user type.</param>
        /// <param name="isDefault">Indicates whether the option is a default option.</param>
        /// <returns>a VOA option.</returns>
        protected VOAOption GenerateOption(int vendorId, Guid brokerId, int lenderServiceId, VOAOptionType type, int value, string userType, bool isDefault)
        {
            var option = new VOAOption();
            option.VendorId = vendorId;
            option.BrokerId = brokerId;
            option.LenderServiceId = lenderServiceId;
            option.OptionType = type;
            option.OptionValue = value;
            option.UserType = userType;
            option.IsDefault = isDefault;
            return option;
        }
    }
}
