﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Text;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Drivers.Gateways;

    /// <summary>
    /// Migration that will disable the user-level MFA bit if the broker-MFA bit is disabled.
    /// </summary>
    public class DisableUserLevelMfaBit : Migrator<Guid>
    {
        /// <summary>
        /// Maps the employee id to the broker id.
        /// </summary>
        Dictionary<Guid, Guid> employeeIdToBrokerId;

        /// <summary>
        /// String builder for error logs.
        /// </summary>
        StringBuilder errorLog = new StringBuilder();

        /// <summary>
        /// Initializes a new instance of the <see cref="DisableUserLevelMfaBit"/> class.
        /// </summary>
        /// <param name="name">The name of the migration.</param>
        public DisableUserLevelMfaBit(string name)
            : this(name, GetAffectedEmployees())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DisableUserLevelMfaBit"/> class.
        /// </summary>
        /// <param name="name">The name of the migration.</param>
        /// <param name="employeeToBroker">Dictionary mapping employees to migrate to their broker id.</param>
        public DisableUserLevelMfaBit(string name, Dictionary<Guid, Guid> employeeToBroker)
            : base(name)
        {
            this.employeeIdToBrokerId = employeeToBroker;
            this.MigrationHelper = new CustomGUIDMigrationHelper(name, () => this.employeeIdToBrokerId.Keys);
        }

        /// <summary>
        /// Runs the migration to disable the user-level MFA bit.
        /// Usage:
        ///     ScheduleExecutable.exe DisableUserLevelMfaBit => Will run the migration on all employees that have their MFA bit set to true but the broker MFA bit set to false.
        ///     ScheduleExecutable.exe DisableUserLevelMfaBit [filePath] => Will run the migration on all employees specified in the file in [filePath]. 
        ///                                                                 The file must be in the form "employeeId,BrokerId". One per line.
        /// </summary>
        /// <param name="args">The string arguments.</param>
        public static void DisableUserLevelMfaBit_Opm311263(string[] args)
        {
            DisableUserLevelMfaBit migrator;
            if (args.Length == 2)
            {
                Dictionary<Guid, Guid> userToBroker = new Dictionary<Guid, Guid>();
                var fileLines = TextFileHelper.ReadLines(args[1]);
                foreach (string line in fileLines)
                {
                    string[] parts = line.Split(new char[] { ',' }, 2);
                    userToBroker.Add(Guid.Parse(parts[0]), Guid.Parse(parts[1])); // Should blow up if not valid guids.
                }

                migrator = new DisableUserLevelMfaBit("DisableUserLevelMfaBit", userToBroker);
            }
            else
            {
                migrator = new DisableUserLevelMfaBit("DisableUserLevelMfaBit");
            }

            migrator.Migrate();
        }

        /// <summary>
        /// Sets EnabledMultiFactorAuthentication to false. This migration bypasses the calculation for EnabledMultiFactorAuthentication
        /// by setting IsForMfaMigration. This will make it so that EmployeeDb.Save will use the private field, which is set in EnabledMultiFactorAuthentication's setter.
        /// </summary>
        /// <param name="id">The employee id of the employee to migrate.</param>
        /// <returns>True if successful migration. False otherwise.</returns>
        protected override bool MigrationFunction(Guid id)
        {
            try
            {
                Guid brokerId;
                if (!this.employeeIdToBrokerId.TryGetValue(id, out brokerId))
                {
                    string errorLog = $"Unable to get broker for employee <{id}>";
                    this.errorLog.AppendLine(errorLog);
                    Tools.LogError(errorLog);
                    return false;
                }

                var employee = EmployeeDB.RetrieveById(brokerId, id);
                employee.EnabledMultiFactorAuthentication = false;
                employee.Save();
                return true;
            }
            catch (Exception e)
            {
                string errorLog = $"Unable to set EnabledMultiFactorAuthentication for employee <{id}>";
                Tools.LogError(errorLog, e);
                this.errorLog.AppendLine(errorLog);
                return false;
            }
        }

        protected override void EndMigration()
        {
            errorLog.AppendLine();
            TextFileHelper.AppendString(this.Name + "_errorLog.txt", errorLog.ToString());
            errorLog.Length = 0;
        }

        /// <summary>
        /// Will pull the employees that need to be migrated.
        /// Looks for employees that have EnabledMultiFactorAuthentication set to true in the DB but have
        /// the appropriate Broker-level MFA bit set to false.
        /// </summary>
        /// <returns></returns>
        private static Dictionary<Guid, Guid> GetAffectedEmployees()
        {
            string query = @"SELECT 
	                                Employee.EmployeeId,
	                                Broker.BrokerId
                                FROM 
	                                Employee WITH (nolock) JOIN 
	                                Branch WITH (nolock) ON Branch.BranchId = Employee.BranchId JOIN 
	                                Broker WITH (nolock) ON Broker.BrokerId = Branch.BrokerId JOIN 
	                                Broker_User WITH (nolock) ON Broker_User.EmployeeId = Employee.EmployeeId JOIN 
	                                All_User WITH (nolock) ON All_User.UserId = Employee.EmployeeUserId
                                WHERE 
	                                Broker.Status > 0 AND 
	                                Broker_User.EnabledMultiFactorAuthentication > 0 AND 
	                                (
		                                ( Broker.IsEnableMultiFactorAuthenticationTPO = 0 AND All_User.Type LIKE 'P' ) OR 
		                                ( Broker.IsEnableMultiFactorAuthentication = 0 AND All_User.Type LIKE 'B' )
	                                )";

            var employeeToBroker = new Dictionary<Guid, Guid>();

            foreach (var connectionInfo in DbConnectionInfo.ListAll())
            {
                Action<IDataReader> readerCode = delegate (IDataReader reader)
                {
                    while (reader.Read())
                    {
                        employeeToBroker.Add((Guid)reader["EmployeeId"], (Guid)reader["BrokerId"]);
                    }
                };

                DBSelectUtility.ProcessDBData(connectionInfo, query, null, null, readerCode);
            }

            return employeeToBroker;
        }
    }
}
