﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;

    using DataAccess;
    using LendersOffice.Admin;

    /// <summary>
    /// Migrator that adds DisableTrid20ForNonTestLoans broker temp option.
    /// </summary>
    class OPM470280_AddDisableTrid20ForNonTestLoansTempOption : Migrator<Guid>
    {
        public OPM470280_AddDisableTrid20ForNonTestLoansTempOption(string name)
                : base(name)
        {
            this.MigrationHelper = new BrokerMigrationHelper(name);
        }

        public OPM470280_AddDisableTrid20ForNonTestLoansTempOption(string name, Guid brokerId)
                : base(name)
        {
            this.MigrationHelper = new CustomGUIDMigrationHelper(name, () => new List<Guid> { brokerId });
        }

        public static void Run(string[] args)
        {
            OPM470280_AddDisableTrid20ForNonTestLoansTempOption migrator;
            if (args.Length == 2)
            {
                Guid broker = Guid.Parse(args[1]);
                migrator = new OPM470280_AddDisableTrid20ForNonTestLoansTempOption("OPM470280_AddDisableTrid20ForNonTestLoansTempOption", broker);
            }
            else
            {
                migrator = new OPM470280_AddDisableTrid20ForNonTestLoansTempOption("OPM470280_AddDisableTrid20ForNonTestLoansTempOption");
            }

            migrator.Migrate();
        }

        protected override bool MigrationFunction(Guid id)
        {
            try
            {
                BrokerDB broker = BrokerDB.RetrieveById(id);

                string tempOption = "<option name=\"DisableTrid20ForNonTestLoans\" value=\"true\" />";
                if (broker.TempOptionXmlContent.Value.IndexOf(tempOption, StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    return false;
                }

                if (broker.TempOptionXmlContent.Value.Contains("</options>"))
                {
                    broker.TempOptionXmlContent = broker.TempOptionXmlContent.Value.Replace("</options>", $"{tempOption}{Environment.NewLine}</options>");
                }
                else
                {
                    broker.TempOptionXmlContent = broker.TempOptionXmlContent.Value + Environment.NewLine + tempOption;
                }

                broker.Save();
                return true;
            }
            catch (Exception e)
            {
                Tools.LogError("Error adding DisableTrid20ForNonTestLoans broker temp option to broker <" + id.ToString() + ">.", e);
            }

            return false;
        }
    }
}
