﻿// <copyright file="DeleteAllContactsMigrator.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//      Author: Jhairo Erazo
//      Date: 12/12/2016
// </summary>
namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.IO;

    using DataAccess;
    using LendersOffice.Admin;

    /// <summary>
    /// Used to delete all contacts for a single Broker.
    /// </summary>
    class DeleteAllContactsMigrator : Migrator<Guid>
    {
        /// <summary>
        /// Broker ID.
        /// </summary>
        private Guid brokerId;

        /// <summary>
        /// Instantiates the migrator.
        /// </summary>
        /// <param name="name">Name used for log files.</param>
        /// <param name="brokerId">Broker ID.</param>
        public DeleteAllContactsMigrator(string name, Guid brokerId)
            :base(name)
        {
            this.brokerId = brokerId;
            this.MigrationHelper = new CustomGUIDMigrationHelper(name, this.IDRetrievalFunction);
        }

        /// <summary>
        /// Deleted all Contact Entried in the Rolodex for the given broker.
        /// </summary>
        /// <param name="args">Either a broker ID or a customer code.</param>
        public static void DeleteAllContacts(string[] args)
        {
            if (args.Length < 2)
            {
                Console.WriteLine("A Broker ID or Customer Code must be passed in as an argument.");
                return;
            }

            // Get brokerId from arguments.
            Guid brokerId;
            if (!Guid.TryParse(args[1], out brokerId))
            {
                // Assume argument is customer code. Use to find Broker ID.
                brokerId = Tools.GetBrokerIdByCustomerCode(args[1]);
            }

            // Make sure this is the right broker
            BrokerDB broker = BrokerDB.RetrieveById(brokerId);

            Console.WriteLine("Are you sure you want to delete all contacts for the following Broker:");
            Console.WriteLine();
            Console.WriteLine($"Broker Name=[{broker.Name}]");
            Console.WriteLine($"Customer Code=[{broker.CustomerCode}]");
            Console.WriteLine($"Broker ID=[{broker.BrokerID}]");
            Console.WriteLine();
            Console.WriteLine("Please enter y or n");

            string input = Console.ReadLine();
            if (!input.Equals("y", StringComparison.OrdinalIgnoreCase))
            {
                return;
            }

            Console.WriteLine();

            string name = $"DeleteAllContacts_{broker.CustomerCode}";
            DeleteAllContactsMigrator migrator = new DeleteAllContactsMigrator(name, brokerId);
            migrator.Migrate();
        }

        /// <summary>
        /// Migration function. Deletes agent from rolodex.
        /// </summary>
        /// <param name="id">An Agent ID.</param>
        /// <returns>Bool indicating if export was successful.</returns>
        protected override bool MigrationFunction(Guid id)
        {
            SqlParameter[] parameters = { new SqlParameter("@RolodexID", id) };
            StoredProcedureHelper.ExecuteNonQuery(this.brokerId, "DeleteRolodex", 0, parameters);

            return true;          
        }

        /// <summary>
        /// Get list of all rolodex contact entries for broker.
        /// </summary>
        /// <returns>An enumerable of agent IDs</returns>
        private IEnumerable<Guid> IDRetrievalFunction()
        {
            List<Guid> ids = new List<Guid>();

            SqlParameter[] parameters = { new SqlParameter("@BrokerID", this.brokerId) };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(this.brokerId, "ListRolodexByBrokerID", parameters))
            {
                while (reader.Read())
                {
                    ids.Add((Guid)reader["AgentId"]);
                }
            }

            return ids;
        }
    }
}
