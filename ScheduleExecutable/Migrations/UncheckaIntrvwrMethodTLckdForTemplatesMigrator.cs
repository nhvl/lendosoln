﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using DataAccess;
    using LendersOffice.Constants;

    /// <summary>
    /// Sets this lockback back to unchecked for templates only.
    /// </summary>
    public class UncheckaIntrvwrMethodTLckdForTemplatesMigrator : Migrator<Guid>
    {
        /// <summary>
        /// Any templates that couldn't be migrated.
        /// </summary>
        private List<string> erroredIds = new List<string>();

        /// <summary>
        /// Initializes a new instance of the <see cref="UncheckaIntrvwrMethodTLckdForTemplatesMigrator"/> class.
        /// </summary>
        public UncheckaIntrvwrMethodTLckdForTemplatesMigrator()
            : base($"UncheckaIntrvwrMethodTLckdForTemplates_{DateTime.Today.ToString("MM_dd_yyyy")}")
        {
            this.MigrationHelper = new CustomGUIDMigrationHelper(this.Name, this.GetTemplates);
        }

        /// <summary>
        /// Runs the migrator.
        /// </summary>
        /// <param name="args">Command line arguments.</param>
        public static void UncheckaIntrvwrMethodTLckdForTemplates(string[] args)
        {
            UncheckaIntrvwrMethodTLckdForTemplatesMigrator migrator = new UncheckaIntrvwrMethodTLckdForTemplatesMigrator();
            migrator.Migrate();
        }

        /// <summary>
        /// Sets aIntrvwrMethodTLckd to false if it is set for templates.
        /// </summary>
        /// <param name="id">The template id.</param>
        /// <returns>True if migrated, false otherwise.</returns>
        protected override bool MigrationFunction(Guid id)
        {
            try
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(id, typeof(UncheckaIntrvwrMethodTLckdForTemplatesMigrator));
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                dataLoan.ByPassFieldSecurityCheck = true;
                dataLoan.DisableFieldEnforcement();

                if (!dataLoan.IsTemplate)
                {
                    return false;
                }

                bool isDirty = false;
                for (int i = 0; i < dataLoan.nApps; i++)
                {
                    CAppData app = dataLoan.GetAppData(i);
                    if (app.aIntrvwrMethodTLckd)
                    {
                        app.aIntrvwrMethodTLckd = false;
                        isDirty = true;
                    }
                }

                if (isDirty)
                {
                    dataLoan.Save();
                    return true;
                }

                return false;
            }
            catch (Exception e)
            {
                Tools.LogError($"Unable to migrate loan id {id.ToString()}", e);
                erroredIds.Add(id.ToString());
                return false;
            }
        }

        /// <summary>
        /// Writes any errors to a log file.
        /// </summary>
        protected override void EndMigration()
        {
            File.WriteAllLines(this.Name + "_errorLog.txt", this.erroredIds);
        }

        /// <summary>
        /// Gets the templates from the DB. Only templates on valid loans on active brokers.
        /// </summary>
        /// <returns>The list of template ids.</returns>
        private IEnumerable<Guid> GetTemplates()
        {
            string query = @"SELECT
                                    c.sLId
                                FROM
                                    Loan_File_Cache c JOIN
                                    Broker b ON c.sBrokerId=b.BrokerId
                                WHERE
                                    c.IsTemplate='1' AND
                                    c.IsValid='1' AND
                                    b.Status='1'";

            LinkedList<Guid> loanIds = new LinkedList<Guid>();
            Action<IDataReader> readHandler = delegate (IDataReader reader)
            {
                while (reader.Read())
                {
                    loanIds.AddLast((Guid)reader["slid"]);
                }
            };

            foreach (var connectionInfo in DbConnectionInfo.ListAll())
            {
                DBSelectUtility.ProcessDBData(connectionInfo, query, null, null, readHandler);
            }

            return loanIds;
        }
    }
}
