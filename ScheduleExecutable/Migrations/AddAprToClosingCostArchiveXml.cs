﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using DataAccess;
using LendersOffice.Constants;
using LqbGrammar.DataTypes;

namespace ScheduleExecutable.Migrations
{
    /// <summary>
    /// This migration is meant to be run prior to the 8/11 release.
    /// </summary>
    public class AddAprToClosingCostArchiveXml : Migrator<Guid>
    {
        public AddAprToClosingCostArchiveXml()
            : base(name: nameof(AddAprToClosingCostArchiveXml))
        {
            this.MigrationHelper = new CustomGUIDMigrationHelper(this.Name, this.GetTridFilesWithArchives);
        }

        public static void Run(string[] args)
        {
            var migrator = new AddAprToClosingCostArchiveXml();
            migrator.Migrate();
        }

        private IEnumerable<Guid> GetTridFilesWithArchives()
        {
            // If we want to filter by sDisclosureRegulationT too, we
            // can join LOAN_FILE_CACHE_4. That will shave ~30k loans
            // off of the number we migrate on prod, but this will be
            // more complete. Excluding invalid brokers results in 
            // a couple thousand fewer files on prod.
            string sql = @"
SELECT b.sLId
FROM LOAN_FILE_B b WITH(NOLOCK) 
    JOIN LOAN_FILE_CACHE lfc WITH(NOLOCK) on b.sLId = lfc.sLId
WHERE sClosingCostArchiveXmlContent <> '' 
    AND IsValid = 1
ORDER BY sStatusD DESC";

            var loanIds = new LinkedList<Guid>();

            Action<IDataReader> readLoanIds = reader =>
            {
                while(reader.Read())
                {
                    loanIds.AddLast((Guid)reader["sLId"]);
                }
            };

            foreach (var connInfo in DbConnectionInfo.ListAll())
            {
                DBSelectUtility.ProcessDBData(connInfo, sql, TimeoutInSeconds.Sixty, null, readLoanIds);
            }

            return loanIds;
        }

        /// <summary>
        /// Migrates LE/CD archives to store the APR in the archive XML.
        /// </summary>
        /// <param name="loanId">The loan id.</param>
        /// <returns>True if the migration completed without errors.</returns>
        protected override bool MigrationFunction(Guid loanId)
        {
            try
            {
                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    loanId,
                    typeof(AddAprToClosingCostArchiveXml));
                // Don't expect QP2 files to have actual archives, but if they have
                // non-blank archive content we will at least check.
                loan.AllowSaveWhileQP2Sandboxed = true;
                loan.InitSave(ConstAppDavid.SkipVersionCheck);

                // GFE Archives will not include sApr, so we only need to migrate if
                // the file has TRID archives.
                if (loan.sClosingCostArchive.Any(archive => archive.IsTridArchive))
                {
                    // Do not enqueue files for compliance integrations
                    // or the mod list.
                    loan.DisableComplianceAutomation = true;
                    loan.DisableLoanModificationTracking = true;
                    loan.MigrateClosingCostArchivesToIncludeApr();

                    // All TRID archives should have an APR value. If not, log warning
                    // to gauge how many files are affected.
                    var badArchives = loan.sClosingCostArchive
                        .Where(archive => archive.IsTridArchive && archive.Apr == null);

                    if (badArchives.Any())
                    {
                        var warning = "APR missing from TRID archive(s), loan: " + loanId.ToString()
                            + " archive id(s) " + string.Join(", ", badArchives.Select(archive => archive.Id.ToString()));
                        Tools.LogInfo(warning);
                    }

                    loan.Save();
                }
            }
            catch (Exception e)
            {
                Tools.LogError("Error migrating file: " + loanId.ToString(), e);
                this.MigrationHelper.WriteToBackup(loanId);
                return false;
            }

            return true;
        }
    }
}
