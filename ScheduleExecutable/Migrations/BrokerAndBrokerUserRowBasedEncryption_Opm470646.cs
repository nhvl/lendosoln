﻿namespace ScheduleExecutable.Migrations
{
    using LqbGrammar.DataTypes;
    using static RowBasedEncryptionGenericTableMigrator;

    public class BrokerAndBrokerUserRowBasedEncryption_Opm470646
    {
        public static void MigrateBrokerTable(string[] args)
        {
            var table = new TableToEncrypt
            {
                TableName = "BROKER",
                PrimaryKeyColumns = new[] { new PrimaryKeyColumn { ColumnName = "BrokerId" } },
                Columns = new ColumnToEncrypt[]
                {
                    new UnencryptedColumnToEncrypt
                    {
                        ExistingColumnName = "CreditMornetPlusPassword",
                        TargetColumnName = "EncryptedCreditMornetPlusPassword",
                    },
                    new UnencryptedColumnToEncrypt
                    {
                        ExistingColumnName = "TempOptionXmlContent",
                        TargetColumnName = "EncryptedTempOptionXmlContent",
                    },
                    new AlreadyEncryptedColumnToEncrypt
                    {
                        ExistingEncryptionKeyId = EncryptionKeyIdentifier.FnmaEarlyCheck,
                        ExistingColumnName = "FnmaEarlyCheckPassword",
                        TargetColumnName = "FnmaEarlyCheckPassword",
                    },
                    new AlreadyEncryptedColumnToEncrypt
                    {
                        ExistingEncryptionKeyId = EncryptionKeyIdentifier.ComplianceEagle,
                        ExistingColumnName = "ComplianceEaglePassword",
                        TargetColumnName = "ComplianceEaglePassword",
                    },
                    new AlreadyEncryptedColumnToEncrypt
                    {
                        ExistingEncryptionKeyId = EncryptionKeyIdentifier.OCR,
                        ExistingColumnName = "OCRPassword",
                        TargetColumnName = "OCRPassword",
                    },
                    new StringEncryptedColumnToEncrypt
                    {
                        ExistingColumnName = "ComplianceEasePassword",
                        TargetColumnName = "EncryptedComplianceEasePassword",
                    },
                },
                SelectStatementForKeys = @"SELECT BrokerId FROM BROKER WHERE EncryptionKeyId = '00000000-0000-0000-0000-000000000000'",
                SelectStatementForRecord = @"SELECT EncryptionKeyId,
    BrokerId,
    CreditMornetPlusPassword,
    TempOptionXmlContent,
    FnmaEarlyCheckPassword,
    ComplianceEaglePassword,
    OCRPassword,
    ComplianceEasePassword
FROM BROKER
WHERE BrokerId = @BrokerId AND EncryptionKeyId = '00000000-0000-0000-0000-000000000000'",
                UpdateStatement = @"UPDATE BROKER
SET
    EncryptionKeyId = @EncryptionKeyId,
    EncryptedCreditMornetPlusPassword = @EncryptedCreditMornetPlusPassword,
    EncryptedTempOptionXmlContent = @EncryptedTempOptionXmlContent,
    FnmaEarlyCheckPassword = @FnmaEarlyCheckPassword,
    ComplianceEaglePassword = @ComplianceEaglePassword,
    OCRPassword = @OCRPassword,
    EncryptedComplianceEasePassword = @EncryptedComplianceEasePassword
WHERE BrokerId = @BrokerId",
            };
            MigrateLenderTable(table);
        }

        public static void MigrateBrokerUserTable(string[] args)
        {
            var table = new TableToEncrypt
            {
                TableName = "BROKER_USER",
                PrimaryKeyColumns = new[] { new PrimaryKeyColumn { ColumnName = "UserId" } },
                Columns = new ColumnToEncrypt[]
                {
                    new UnencryptedColumnToEncrypt
                    {
                        ExistingColumnName = "SecurityQuestion1Answer",
                        TargetColumnName = "SecurityQuestion1EncryptedAnswer",
                    },
                    new UnencryptedColumnToEncrypt
                    {
                        ExistingColumnName = "SecurityQuestion2Answer",
                        TargetColumnName = "SecurityQuestion2EncryptedAnswer",
                    },
                    new UnencryptedColumnToEncrypt
                    {
                        ExistingColumnName = "SecurityQuestion3Answer",
                        TargetColumnName = "SecurityQuestion3EncryptedAnswer",
                    },
                    new StringEncryptedColumnToEncrypt
                    {
                        ExistingColumnName = "DOAutoPassword",
                        TargetColumnName = "EncryptedDOAutoPassword",
                    },
                    new StringEncryptedColumnToEncrypt
                    {
                        ExistingColumnName = "DUAutoPassword",
                        TargetColumnName = "EncryptedDUAutoPassword",
                    },
                    new StringEncryptedColumnToEncrypt
                    {
                        ExistingColumnName = "ComplianceEasePassword",
                        TargetColumnName = "EncryptedComplianceEasePassword",
                    },
                    new StringEncryptedColumnToEncrypt
                    {
                        ExistingColumnName = "LPQPassword",
                        TargetColumnName = "EncryptedLPQPassword",
                    },
                    new AlreadyEncryptedColumnToEncrypt
                    {
                        ExistingEncryptionKeyId = EncryptionKeyIdentifier.ComplianceEagle,
                        ExistingColumnName = "ComplianceEaglePassword",
                        TargetColumnName = "ComplianceEaglePassword",
                    },
                    new StringEncryptedColumnToEncrypt
                    {
                        ExistingColumnName = "DocMagicPassword",
                        TargetColumnName = "EncryptedDocMagicPassword",
                    },
                    new StringEncryptedColumnToEncrypt
                    {
                        ExistingColumnName = "DocuTechPassword",
                        TargetColumnName = "EncryptedDocuTechPassword",
                    },
                    new StringEncryptedColumnToEncrypt
                    {
                        ExistingColumnName = "DrivePassword",
                        TargetColumnName = "EncryptedDrivePassword",
                    },
                    new StringEncryptedColumnToEncrypt
                    {
                        ExistingColumnName = "FloodPassword",
                        TargetColumnName = "EncryptedFloodPassword",
                    },
                },
                SelectStatementForKeys = @"SELECT UserId FROM BROKER_USER WHERE EncryptionKeyId = '00000000-0000-0000-0000-000000000000'",
                SelectStatementForRecord = @"SELECT EncryptionKeyId,
    UserId,
    SecurityQuestion1Answer,
    SecurityQuestion2Answer,
    SecurityQuestion3Answer,
    DOAutoPassword,
    DUAutoPassword,
    ComplianceEasePassword,
    LPQPassword,
    ComplianceEaglePassword,
    DocMagicPassword,
    DocuTechPassword,
    DrivePassword,
    FloodPassword
FROM BROKER_USER
WHERE UserId = @UserId AND EncryptionKeyId = '00000000-0000-0000-0000-000000000000'",
                UpdateStatement = @"UPDATE BROKER_USER
SET
    EncryptionKeyId = @EncryptionKeyId,
    SecurityQuestion1EncryptedAnswer = @SecurityQuestion1EncryptedAnswer,
    SecurityQuestion2EncryptedAnswer = @SecurityQuestion2EncryptedAnswer,
    SecurityQuestion3EncryptedAnswer = @SecurityQuestion3EncryptedAnswer,
    EncryptedDOAutoPassword = @EncryptedDOAutoPassword,
    EncryptedDUAutoPassword = @EncryptedDUAutoPassword,
    EncryptedComplianceEasePassword = @EncryptedComplianceEasePassword,
    EncryptedLPQPassword = @EncryptedLPQPassword,
    ComplianceEaglePassword = @ComplianceEaglePassword,
    EncryptedDocMagicPassword = @EncryptedDocMagicPassword,
    EncryptedDocuTechPassword = @EncryptedDocuTechPassword,
    EncryptedDrivePassword = @EncryptedDrivePassword,
    EncryptedFloodPassword = @EncryptedFloodPassword
WHERE UserId = @UserId",
            };
            MigrateLenderTable(table);
        }
    }
}
