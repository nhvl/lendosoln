﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml;
    using System.Xml.Linq;
    using LendersOffice.Admin;
    using LendersOffice.Common;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "*", Justification = "Temporary while developing.")]
    public class TempOptionAdder
    {
        public static void AddByOption(string[] optionsToAdd)
        {
            if (!optionsToAdd.Any())
            {
                Console.WriteLine("No options to add");
                return;
            }

            AddOptions(optionsToAdd, suiteType: null);
        }

        public static void AddByOptionAndSuiteType(string[] optionsToAdd)
        {
            if (!optionsToAdd.Any())
            {
                Console.WriteLine("No options to add");
                return;
            }
            
            var suiteType = optionsToAdd[0].ToNullableEnum<BrokerSuiteType>(ignoreCase: true);
            if (suiteType == null)
            {
                Console.WriteLine("Invalid broker suite type " + optionsToAdd[0]);
                return;
            }

            AddOptions(optionsToAdd.Skip(1), suiteType);
        }

        private static void AddOptions(IEnumerable<string> optionsToAdd, BrokerSuiteType? suiteType)
        {
            bool foundInvalidOptions = false;
            foreach (var option in optionsToAdd)
            {
                if (!IsValidXmlElement(option))
                {
                    foundInvalidOptions = true;
                    Console.WriteLine("Detected invalid XML: " + option);
                }
            }

            if (foundInvalidOptions)
            {
                Console.WriteLine("Please correct the XML options before trying again.");
                return;
            }

            Console.WriteLine($"Running this will add the following temporary options to {suiteType?.ToString() ?? "all"} active lenders:");
            foreach (var option in optionsToAdd)
            {
                Console.WriteLine("  - " + option);
            }

            Console.Write("Confirm (y/n)? ");
            string response = Console.ReadLine()?.Trim().ToLower();
            if (response == "y" || response == "yes")
            {
                MigrateLenders(optionsToAdd, suiteType);
            }
            else
            {
                Console.WriteLine("Aborting...");
            }
        }

        private static bool IsValidXmlElement(string elementString)
        {
            try
            {
                return elementString != null && XElement.Parse(elementString) != null;
            }
            catch (XmlException)
            {
                return false;
            }
        }

        private static void MigrateLenders(IEnumerable<string> optionsToAdd, BrokerSuiteType? suiteType)
        {
            foreach (Guid brokerId in DataAccess.Tools.GetAllActiveBrokers(suiteType))
            {
                try
                {
                    var broker = LendersOffice.Admin.BrokerDB.RetrieveById(brokerId);
                    string tempOptionXml = broker.TempOptionXmlContent.Value;
                    string optionsToAddBlock = Environment.NewLine + string.Join(Environment.NewLine, optionsToAdd) + Environment.NewLine;

                    int optionsIndex = tempOptionXml.IndexOf("</options>", StringComparison.OrdinalIgnoreCase);
                    int insertionLocation = optionsIndex >= 0 ? optionsIndex : tempOptionXml.Length;
                    string newTempOptionXml = tempOptionXml.Insert(insertionLocation, optionsToAddBlock);

                    broker.TempOptionXmlContent = newTempOptionXml;
                    if (broker.Save())
                    {
                        Console.WriteLine("Added to " + broker.CustomerCode);
                    }
                    else
                    {
                        Console.WriteLine("Unable to add to " + broker.CustomerCode);
                    }
                }
                catch (Exception exc)
                {
                    DataAccess.Tools.LogError("Unable to add temp option(s) to lender " + brokerId, exc);
                    Console.WriteLine("Unable to update " + brokerId + " due to " + exc.GetType().FullName + ".");
                }
            }
        }
    }
}
