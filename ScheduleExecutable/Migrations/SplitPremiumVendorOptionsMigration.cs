﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Integration.MortgageInsurance;

    /// <summary>
    /// A migration to provide the relevant split premium options for all MI vendors.
    /// </summary>
    /// <remarks>Can be removed after February 2019 release (2019.R1).</remarks>
    public class SplitPremiumVendorOptionsMigration : Migrator<Guid>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SplitPremiumVendorOptionsMigration"/> class.
        /// </summary>
        public SplitPremiumVendorOptionsMigration()
            : base(nameof(SplitPremiumVendorOptionsMigration))
        {
            this.MigrationHelper = new CustomGUIDMigrationHelper(this.Name, GetVendorIds);
        }

        /// <summary>
        /// An entry point to run the migration.
        /// </summary>
        /// <param name="args"></param>
        public static void Run(string[] args)
        {
            var migrator = new SplitPremiumVendorOptionsMigration();
            migrator.Migrate();
        }

        /// <summary>
        /// Retrieve the IDs of all MI vendors on this environment.
        /// </summary>
        /// <returns></returns>
        private static IEnumerable<Guid> GetVendorIds()
        {
            var vendors = MortgageInsuranceVendorConfig.ListAllVendors();
            return vendors.Select(v => v.VendorId);
        }

        /// <summary>
        /// Migrates a single MI vendor to configure the relevant split premium options.
        /// </summary>
        /// <param name="vendorId">The MI vendor ID.</param>
        /// <returns>A boolean indicating whether the migration was successful.</returns>
        protected override bool MigrationFunction(Guid vendorId)
        {
            try
            {
                var vendor = MortgageInsuranceVendorConfig.RetrieveById(vendorId);

                vendor.UsesSplitPremiumPlans = false;
                vendor.SplitPremiumOptions.Clear();

                if (vendor.VendorType == E_sMiCompanyNmT.Essent)
                {
                    vendor.UsesSplitPremiumPlans = true;
                    vendor.SplitPremiumOptions.Add(new MISplitPremiumOption(0.50m, "SplitPremium1"));
                    vendor.SplitPremiumOptions.Add(new MISplitPremiumOption(0.75m, "SplitPremium2"));
                    vendor.SplitPremiumOptions.Add(new MISplitPremiumOption(1.00m, "SplitPremium3"));
                    vendor.SplitPremiumOptions.Add(new MISplitPremiumOption(1.25m, "SplitPremium4"));
                    vendor.SplitPremiumOptions.Add(new MISplitPremiumOption(1.50m, "SplitPremium5"));
                    vendor.SplitPremiumOptions.Add(new MISplitPremiumOption(1.75m, "SplitPremium6"));
                    vendor.SplitPremiumOptions.Add(new MISplitPremiumOption(2.00m, "SplitPremium7"));
                    vendor.SplitPremiumOptions.Add(new MISplitPremiumOption(2.25m, "SplitPremium8"));
                }
                else if (vendor.VendorType == E_sMiCompanyNmT.Genworth)
                {
                    vendor.UsesSplitPremiumPlans = true;
                    vendor.SplitPremiumOptions.Add(new MISplitPremiumOption(0.50m, "SplitPremium1"));
                    vendor.SplitPremiumOptions.Add(new MISplitPremiumOption(0.75m, "SplitPremium2"));
                    vendor.SplitPremiumOptions.Add(new MISplitPremiumOption(1.00m, "SplitPremium3"));
                    vendor.SplitPremiumOptions.Add(new MISplitPremiumOption(1.25m, "SplitPremium4"));
                    vendor.SplitPremiumOptions.Add(new MISplitPremiumOption(1.50m, "SplitPremium5"));
                    vendor.SplitPremiumOptions.Add(new MISplitPremiumOption(1.75m, "SplitPremium6"));
                }
                else if (vendor.VendorType == E_sMiCompanyNmT.MGIC)
                {
                    vendor.UsesSplitPremiumPlans = true;
                    vendor.SplitPremiumOptions.Add(new MISplitPremiumOption(0.50m, "SplitPremium"));
                    vendor.SplitPremiumOptions.Add(new MISplitPremiumOption(0.75m, "SplitPremium1"));
                    vendor.SplitPremiumOptions.Add(new MISplitPremiumOption(1.00m, "SplitPremium2"));
                    vendor.SplitPremiumOptions.Add(new MISplitPremiumOption(1.25m, "SplitPremium3"));
                    vendor.SplitPremiumOptions.Add(new MISplitPremiumOption(1.50m, "SplitPremium4"));
                    vendor.SplitPremiumOptions.Add(new MISplitPremiumOption(1.75m, "SplitPremium5"));
                }
                else if (vendor.VendorType == E_sMiCompanyNmT.Radian)
                {
                    vendor.UsesSplitPremiumPlans = true;
                    vendor.SplitPremiumOptions.Add(new MISplitPremiumOption(0.25m, "SplitPremium7"));
                    vendor.SplitPremiumOptions.Add(new MISplitPremiumOption(0.50m, "SplitPremium4"));
                    vendor.SplitPremiumOptions.Add(new MISplitPremiumOption(0.75m, "SplitPremium1"));
                    vendor.SplitPremiumOptions.Add(new MISplitPremiumOption(1.00m, "SplitPremium2"));
                    vendor.SplitPremiumOptions.Add(new MISplitPremiumOption(1.25m, "SplitPremium3"));
                    vendor.SplitPremiumOptions.Add(new MISplitPremiumOption(1.50m, "SplitPremium5"));
                    vendor.SplitPremiumOptions.Add(new MISplitPremiumOption(1.75m, "SplitPremium6"));
                }
                else
                {
                    // Vendor does not support split premiums, no configuration needed.
                }

                vendor.Save();
                return true;
            }
            catch (Exception exc) when (exc is CBaseException || exc is SqlException)
            {
                Tools.LogError($"MI Vendor {vendorId} could not be configured with split premium options.");
                return false;
            }
            catch (NotFoundException)
            {
                Tools.LogError($"MI Vendor {vendorId} could not be configured with split premium options because it does not exist.");
                return false;
            }
        }
    }
}
