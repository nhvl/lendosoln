﻿/// <copyright file="Opm449715_AddOrderInitialDisclosuresInTpoPortalWorkflowOperationToSystemConfig.cs" company="MeridianLink">
///     Copyright (c) MeridianLink. All rights reserved.
/// </copyright>
/// <summary>
/// Author: Michael Leinweaver
/// Date:   2/28/2017
/// </summary>
namespace ScheduleExecutable.Migrations
{
    using System.Collections.Generic;
    using LendersOffice.ConfigSystem.Operations;

    /// <summary>
    /// Adds <see cref="WorkflowOperations.OrderInitialDisclosureInTpoPortal"/> 
    /// to the system workflow configuration.
    /// </summary>
    public class Opm449715_AddOrderInitialDisclosuresInTpoPortalWorkflowOperationToSystemConfig : SystemConfigOperationMigrator
    {
        /// <summary>
        /// Returns the new operations to add to the system configuration.
        /// </summary>
        /// <returns>
        /// The new operations.
        /// </returns>
        protected override IEnumerable<OperationAddition> GetNewOperations()
        {
            return new[]
            {
                new OperationAddition(
                    WorkflowOperations.OrderInitialDisclosureInTpoPortal,
                    defaultValue: false,
                    notes: "OPM 449715",
                    failureMessage: "You do not have permission to order initial disclosures on this file.")
            };
        }
    }
}
