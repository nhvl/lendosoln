﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DataAccess;
    using EDocs;
    using LendersOffice.Admin;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.Migration;
    using LendersOffice.ObjLib.Edocs.AutoSaveDocType;

    /// <summary>
    /// For lenders who currently have EDocument autosave enabled for MI Docs, 
    /// create a new "MORTGAGE INSURANCE QUOTE DOCUMENT" document type and enable MI Quote Doc autosave for the new Policy document type.
    /// </summary>
    class OPM239654_Broker_MIPolicyDocTypeAutosave : Migrator<Guid>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Opm239654_CreateMIPolicyDocTypeForMIAutosaveBrokers"/> class.
        /// </summary>
        /// <param name="name">The migration name.</param>
        public OPM239654_Broker_MIPolicyDocTypeAutosave(string name, IEnumerable<Guid> brokerIds) : base(name)
        {
            this.MigrationHelper = new CustomGUIDMigrationHelper(name, () => brokerIds);
        }

        /// <summary>
        /// Migrates brokers to split the MI doc type into quote and policy doc types, if autosave is enabled.
        /// </summary>
        /// <param name="args">Parameters for the migration. The second parameter, if supplied, should be the name of a file containing a list of brokerIds to migrate.</param>
        public static void MigrateMIDocTypes(string[] args)
        {
            List<Guid> brokerIds;
            if (args.Length == 1)
            {
                brokerIds = Tools.GetAllActiveBrokers().ToList();
            }
            else if (args.Length == 2)
            {
                string idFilePath = args[1];
                brokerIds = TextFileHelper.ReadLines(idFilePath).Select(id => Guid.Parse(id)).ToList();
            }
            else
            {
                Console.WriteLine("Please either pass in a valid file containing a list of broker ids or pass in nothing to migrate all active brokers.");
                return;
            }

            OPM239654_Broker_MIPolicyDocTypeAutosave migrator = new OPM239654_Broker_MIPolicyDocTypeAutosave(nameof(OPM239654_Broker_MIPolicyDocTypeAutosave), brokerIds);
            migrator.Migrate();
        }


        /// <summary>
        /// Creates a "MORTGAGE INSURANCE QUOTE DOCUMENTS" doc type and sets it as the default doc type for the new <see cref="E_AutoSavePage.MortgageInsuranceQuoteDocuments"/> default.
        /// </summary>
        /// <param name="brokerId">The broker to migrate.</param>
        /// <returns>A boolean indicating whether the broker was migrated. False if nothing was done or an error occurred.</returns>
        protected override bool MigrationFunction(Guid brokerId)
        {
            try
            {
                BrokerDB broker = BrokerDB.RetrieveById(brokerId);

                if (AutoSaveDocTypeFactory.IsAutoSaveEnabled(E_AutoSavePage.MortgageInsurancePolicyDocuments, brokerId, E_EnforceFolderPermissions.False))
                {
                    // Need to clear doc type before assigning a new one, to make absolutely sure that this is THE MI Quote doc type. There shouldn't be anything here, though.
                    AutoSaveDocTypeFactory.ClearDocTypesForPageId(E_AutoSavePage.MortgageInsuranceQuoteDocuments, brokerId);

                    DocType miQuoteDocType = EDocumentDocType.GetDocTypeById(brokerId, EDocumentDocType.GetOrCreateDocType(brokerId, "MORTGAGE INSURANCE QUOTE DOCUMENT", "MORTGAGE INSURANCE DOCUMENTS"));
                    miQuoteDocType.DefaultPage = E_AutoSavePage.MortgageInsuranceQuoteDocuments;
                    miQuoteDocType.Save(brokerId);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch(Exception e)
            {
                Tools.LogError($"Error updating broker <{brokerId}> for {nameof(OPM239654_Broker_MIPolicyDocTypeAutosave)}", e);
            }
            return false;
        }
    }
}
