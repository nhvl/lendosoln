﻿namespace ScheduleExecutable.Migrations
{
    using System.Collections.Generic;

    using LendersOffice.ConfigSystem.Operations;

    public class AusAndCreditPrivileges : SystemConfigOperationMigrator
    {
        /// <summary>
        /// Runs the migration.
        /// </summary>
        /// <param name="args">The command line arguments.</param>
        public static void Run(string[] args)
        {
            var privilegeAdder = new AusAndCreditPrivileges();
            privilegeAdder.AddOperations();
        }

        /// <summary>
        /// Gets the operations to add.
        /// </summary>
        /// <returns>The operations to add.</returns>
        protected override IEnumerable<OperationAddition> GetNewOperations()
        {
            return new[]
            {
                new OperationAddition(
                    WorkflowOperations.RunDo,
                    defaultValue: true,
                    notes: "OPM 245347",
                    failureMessage: "You do not have permission to run DO."),
                new OperationAddition(
                    WorkflowOperations.RunDu,
                    defaultValue: true,
                    notes: "OPM 245347",
                    failureMessage: "You do not have permission to run DU."),
                new OperationAddition(
                    WorkflowOperations.RunLp,
                    defaultValue: true,
                    notes: "OPM 245347",
                    failureMessage: "You do not have permission to run LP."),
                new OperationAddition(
                    WorkflowOperations.OrderCredit,
                    defaultValue: true,
                    notes: "OPM 245349",
                    failureMessage: "You do not have permission to order credit.")
            };
        }
    }
}
