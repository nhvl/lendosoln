﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Integration.DocumentVendor;

namespace ScheduleExecutable.Migrations
{
    public class OPM461391_DocVendorPlatforms : Migrator<Guid>
    {
        /// <summary>
        /// Prevents a default instance of the <see cref="OPM461391_DocVendorPlatforms"/> 
        /// class from being created.
        /// </summary>
        /// <param name="name">The name of the migration.</param>
        private OPM461391_DocVendorPlatforms(string name) : base(name)
        {
            this.MigrationHelper = new CustomGUIDMigrationHelper(name, () => DocumentVendorFactory.AvailableVendors().Select(config => config.VendorId).Where(id => id != Guid.Empty));
        }

        /// <summary>
        /// Executes the migration.
        /// </summary>
        /// <param name="args">The command-line arguments to the migration, which are not used.</param>
        public static void Run(string[] args)
        {
            var migrator = new OPM461391_DocVendorPlatforms(nameof(OPM461391_DocVendorPlatforms));
            migrator.Migrate();
        }

        /// <summary>
        /// Migrates a document vendor configuration to set its platform type based on the vendor name.
        /// </summary>
        /// <param name="id">The vendor ID of the vendor.</param>
        /// <returns>
        /// True if the migration was done.
        /// False if nothing was done.
        /// </returns>
        protected override bool MigrationFunction(Guid id)
        {
            VendorConfig config = VendorConfig.Retrieve(id);

            if (config.PlatformType == E_DocumentVendor.UnknownOtherNone)
            {
                config.PlatformType = ComputeVendorType(config.VendorName);
                VendorConfig.Save(config);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Determine the vendor type represented by this vendor config.
        /// </summary>
        /// <param name="vendorName">The name of the vendor.</param>
        /// <returns>An enum indicating which vendor is being used</returns>
        private static E_DocumentVendor ComputeVendorType(string vendorName)
        {
            E_DocumentVendor vendor = E_DocumentVendor.UnknownOtherNone;
            vendor = vendorName.Contains("DocuTech", StringComparison.OrdinalIgnoreCase) ? E_DocumentVendor.DocuTech : vendor;
            vendor = vendorName.Contains("Docs on Demand", StringComparison.OrdinalIgnoreCase) ? E_DocumentVendor.DocsOnDemand : vendor;
            vendor = vendorName.Contains("IDS", StringComparison.OrdinalIgnoreCase) ? E_DocumentVendor.IDS : vendor;
            vendor = vendorName.Contains("DocMagic", StringComparison.OrdinalIgnoreCase) ? E_DocumentVendor.DocMagic : vendor;
            vendor = vendorName.Contains("ClosingXpress", StringComparison.OrdinalIgnoreCase) ? E_DocumentVendor.ClosingXpress : vendor;
            vendor = vendorName.Contains("Signia Documents", StringComparison.OrdinalIgnoreCase) ? E_DocumentVendor.SigniaDocuments : vendor;
            return vendor;
        }
    }
}
