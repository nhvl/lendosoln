﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using DataAccess;
    using LendersOffice.ObjLib.Task;
    using LendersOffice.Security;

    /// <summary>
    /// Base class for batch closing tasks. Ignores resolution block triggers and resolution date setters.
    /// </summary>
    public abstract class BatchTaskCloser : Migrator<string>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BatchTaskCloser"/> class.
        /// </summary>
        /// <param name="name">The name of the migrator.</param>
        public BatchTaskCloser(string name, Guid brokerId)
            : base(name)
        {
            this.BrokerId = brokerId;
            this.MigrationHelper = new CustomStringMigrationHelper(name, this.GetTaskIds);
            Thread.CurrentPrincipal = SystemUserPrincipal.TaskSystemUser;
        }

        /// <summary>
        /// Initializes a new instances of the <see cref="BatchTaskCloser"/> class.
        /// </summary>
        /// <param name="name">The name of the migration.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="getTaskIdsFunction">The functino to get the tasks to close.</param>
        public BatchTaskCloser(string name, Guid brokerId, Func<IEnumerable<string>> getTaskIdsFunction)
            : base(name)
        {
            this.BrokerId = brokerId;
            this.MigrationHelper = new CustomStringMigrationHelper(name, getTaskIdsFunction);
            Thread.CurrentPrincipal = SystemUserPrincipal.TaskSystemUser;
        }

        /// <summary>
        /// Gets the broker id that will be used when retrieving the tasks.
        /// </summary>
        /// <value>The broker id that will be used when retrieving the tasks.</value>
        protected Guid BrokerId { get; private set; }

        /// <summary>
        /// Gets the ids of the tasks that will be closed.
        /// </summary>
        /// <returns>The ids of the tasks that will be closed.</returns>
        protected abstract IEnumerable<string> GetTaskIds();

        /// <summary>
        /// Asks the user to confirm if the passed in broker id is correct.
        /// </summary>
        protected override void BeginMigration()
        {
            string brokerName = LendersOffice.Admin.BrokerDB.RetrieveNameOnlyFromDb(this.BrokerId);
            Console.WriteLine($"You have specified broker {this.BrokerId}(Name: {brokerName}) for this migration. Type 'correct' (no quotes) if this is correct:");
            string response = Console.ReadLine();
            if (response != "correct")
            {
                Environment.Exit(0);
                throw new Exception("This shouldn't get here but if it does, throw an exception and let it blow up.");
            }
        }

        /// <summary>
        /// Closes the task if it is not already closed.
        /// </summary>
        /// <param name="taskId">The id of the task.</param>
        /// <returns>True if the task was closed successfully or was already closed. Otherwise, false.</returns>
        protected override bool MigrationFunction(string taskId)
        {
            try
            {
                var task = Task.RetrieveWithoutPermissionCheck(this.BrokerId, taskId);

                if (task.BrokerId != this.BrokerId)
                {
                    Tools.LogError($"{this.Name}: Passed in broker id ({this.BrokerId}) somehow doesn't match the task brokerId for task <{taskId}>.");
                    return false;
                }

                task.DisableResolutionBlockTrigger = true;
                task.DisableResolutionDateSetter = true;

                // From experience with case 338196, keeping a log of this information will be 
                // useful if the client would like to reactivate the tasks. This data will keep
                // us from having to parse the last assigned user / role out of the audit XML.
                // The owner of the task is not changed when resolving / closing, so we can 
                // just keep track of who it was assigned to. gf 11/11/16, opm 328860
                this.MigrationHelper.WriteToBackup(
                    new
                    {
                        TaskId = task.TaskId,
                        LoanId = task.LoanId,
                        AssignedUserId = task.TaskAssignedUserId,
                        ToBeAssignedRoleId = task.TaskToBeAssignedRoleId,
                    });

                if (task.TaskStatus == E_TaskStatus.Active)
                {
                    task.ResolveAndClose();
                }
                else if (task.TaskStatus == E_TaskStatus.Resolved)
                {
                    task.Close();
                }
            }
            catch (Exception exc)
            {
                Tools.LogError($"{this.Name}: Error occurred when closing task with id {taskId}.", exc);
                return false;
            }

            return true;
        }
    }
}
