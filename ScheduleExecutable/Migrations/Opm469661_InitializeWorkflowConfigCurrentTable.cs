﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Collections.Generic;

    using CommonProjectLib.Database;
    using ConfigSystem;
    using ConfigSystem.DataAccess;
    using DataAccess;
    using LendersOffice.ConfigSystem;
    using LendersOffice.Drivers.SqlServerDB;
    using LqbGrammar.DataTypes;

    public static class Opm469661_InitializeWorkflowConfigCurrentTable
    {
        private static SQLQueryString Query = SQLQueryString.Create(
            @"INSERT INTO WORKFLOW_CONFIG_CURRENT(BrokerId, ConfigId, Version, ConfigXmlKey, CompiledConfig, ModifyingUserId, ReleaseDate)
                VALUES(@BrokerId, @ConfigId, @Version, @ConfigXmlKey, @CompiledConfig, @ModifyingUserId, @ReleaseDate)").Value;

        public static void Run(string[] args)
        {
            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                DBConfigRepository repo = new DBConfigRepository(SqlConnectionManager.Get(connInfo), null, null);
                foreach (ReleaseConfigData config in repo.GetActiveReleases_OLD())
                {
                    try
                    {
                        // Save config XML to FileDB.
                        Guid keyGuid = Guid.NewGuid();
                        string fullKey = $"{keyGuid}_{config.OrganizationId}_{SystemConfig.fileDbKeySuffix}";
                        FileDBTools.WriteData(E_FileDB.Normal, fullKey, config.Configuration.ToString());

                        // Save to current config table.
                        SqlParameter[] parameters = new SqlParameter[]
                        {
                        new SqlParameter("@BrokerId", config.OrganizationId),
                        new SqlParameter("@ConfigId", config.Id),
                        new SqlParameter("@Version", 1),
                        new SqlParameter("@ConfigXmlKey", keyGuid),
                        new SqlParameter("@CompiledConfig", config.CompiledConfiguration),
                        new SqlParameter("@ModifyingUserId", config.UserId),
                        new SqlParameter("@ReleaseDate", config.ReleaseDate)
                        };

                        using (DbConnection conn = connInfo.GetConnection())
                        {
                            SqlServerHelper.InsertNoKey(conn, null, Query, parameters, TimeoutInSeconds.Default);
                        }
                    }
                    catch(Exception e)
                    {
                        string msg = $"[Opm469661_InitializeWorkflowConfigCurrentTable] Could not copy broker config to WORKFLOW_CONFIG_CURRENT. brokerId=[{config.OrganizationId}]. ConfigId=[{config.Id}]";
                        Tools.LogError(msg, e);
                    }
                }
            }   
        }
    }
}
