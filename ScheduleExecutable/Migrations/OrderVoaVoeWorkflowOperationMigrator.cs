﻿namespace ScheduleExecutable.Migrations
{
    using System.Collections.Generic;
    using LendersOffice.ConfigSystem.Operations;

    public class OrderVoaVoeWorkflowOperationMigrator : SystemConfigOperationMigrator
    {
        protected override IEnumerable<OperationAddition> GetNewOperations()
        {
            return new[]
            {
                new OperationAddition(
                    WorkflowOperations.OrderVoa,
                    defaultValue: false,
                    notes: "OPM 453029",
                    failureMessage: "You do not have permission to order VOA/VOD."),
                new OperationAddition(
                    WorkflowOperations.OrderVoe,
                    defaultValue: false,
                    notes: "OPM 450153",
                    failureMessage: "You do not have permission to order VOE/VOI.")
            };
        }
    }
}
