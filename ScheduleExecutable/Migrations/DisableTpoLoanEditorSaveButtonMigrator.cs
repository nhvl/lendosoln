﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using System.Collections.Generic;

    /// <summary>
    /// Migrator for turning on DisableTpoLoanEditorSaveButton Broker temp option.
    /// </summary>
    public class DisableTpoLoanEditorSaveButtonMigrator : Migrator<Guid>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DisableTpoLoanEditorSaveButtonMigrator"/> class.
        /// </summary>
        /// The name of the temp option to add.
        /// </param>
        public DisableTpoLoanEditorSaveButtonMigrator() :
            base(nameof(DisableTpoLoanEditorSaveButtonMigrator))
        {
            this.MigrationHelper = new BrokerMigrationHelper(this.Name);
        }

        /// <summary>
        /// Disable TPO Loan Editor Save buttons.
        /// </summary>
        public static void DisableTpoLoanEditorSaveButton(string[] args)
        {
            if (args.Length > 1)
            {
                Console.WriteLine("Usage: ScheduleExecutable DisableTpoLoanEditorSaveButton");
                return;
            }

            DisableTpoLoanEditorSaveButtonMigrator migrator = new DisableTpoLoanEditorSaveButtonMigrator();

            migrator.Migrate();

            Console.WriteLine("DisableTpoLoanEditorSaveButton migration has finished successfully.");
        }

        /// <summary>
        /// Migrates the broker with the specified <paramref name="id"/>
        /// by adding the temp option.
        /// </summary>
        /// <param name="id">
        /// The <see cref="Guid"/> for the broker.
        /// </param>
        /// <returns>
        /// True if the migration was successful, false otherwise.
        /// </returns>
        protected override bool MigrationFunction(Guid id)
        {
            try
            {
                var broker = BrokerDB.RetrieveById(id);
                var tempOptionXml = broker.TempOptionXmlContent.Value;

                if (tempOptionXml != null && tempOptionXml.Contains("</options>", StringComparison.OrdinalIgnoreCase))
                {
                    tempOptionXml = tempOptionXml.Replace("</options>", "<option name=\"DisableTpoLoanEditorSaveButton\" value=\"true\" ExceptForThisEmployeeGroup=\"\" ExceptForThisOcGroup=\"\" />" + Environment.NewLine + "</options>");
                }
                else
                {
                    tempOptionXml = tempOptionXml + Environment.NewLine + "<option name=\"DisableTpoLoanEditorSaveButton\" value=\"true\" ExceptForThisEmployeeGroup=\"\" ExceptForThisOcGroup=\"\" />";
                }

                broker.TempOptionXmlContent = tempOptionXml;
                broker.Save();
                return true;
            }
            catch (Exception exc)
            {
                Tools.LogError($"Could not migrate broker {id} to add temp option < option name =\"DisableTpoLoanEditorSaveButton\" value=\"true\" ExceptForThisEmployeeGroup=\"\" ExceptForThisOcGroup=\"\" />.", exc);
                return false;
            }
        }
    }
}
