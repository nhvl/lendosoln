﻿/// <copyright file="SetBrokerTempOptionWithOcGroupExclusionMigrator.cs" company="MeridianLink">
///     Copyright (c) MeridianLink. All rights reserved.
/// </copyright>
/// <summary>
/// Author: Michael Leinweaver
/// Date:   10/24/2016
/// </summary>
namespace ScheduleExecutable.Migrations
{
    using System;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;

    /// <summary>
    /// Adds TPO loan navigation temp options to all active brokers.
    /// </summary>
    public class SetBrokerTempOptionWithOcGroupExclusionMigrator : Migrator<Guid>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SetBrokerTempOptionWithOcGroupExclusionMigrator"/> class.
        /// </summary>
        /// <param name="tempOptionName">
        /// The name of the temp option to add.
        /// </param>
        private SetBrokerTempOptionWithOcGroupExclusionMigrator(string tempOptionName) :
            base(nameof(SetBrokerTempOptionWithOcGroupExclusionMigrator) + "_" + tempOptionName)
        {
            this.MigrationHelper = new BrokerMigrationHelper(nameof(SetBrokerTempOptionWithOcGroupExclusionMigrator) + "_" + tempOptionName);
            this.TempOption = $"<option name=\"{tempOptionName}\" OCGroupExclusion=\"\" value=\"true\" />";
        }

        /// <summary>
        /// Gets the name of the temp option to add to all active brokers.
        /// </summary>
        /// <value>
        /// The temp option name.
        /// </value>
        private string TempOption { get; }

        /// <summary>
        /// Runs the migration.
        /// </summary>
        /// <param name="args">
        /// The arguments for the migration.
        /// </param>
        public static void Run(string[] args)
        {
            if (args.Length != 1)
            {
                throw new ArgumentException("Must specify the name of the temp option as the only argument.", nameof(args));
            }

            var migrator = new SetBrokerTempOptionWithOcGroupExclusionMigrator(args[0]);
            migrator.Migrate();
        }

        /// <summary>
        /// Migrates the broker with the specified <paramref name="id"/>
        /// by adding the temp option.
        /// </summary>
        /// <param name="id">
        /// The <see cref="Guid"/> for the broker.
        /// </param>
        /// <returns>
        /// True if the migration was successful, false otherwise.
        /// </returns>
        protected override bool MigrationFunction(Guid id)
        {
            try
            {
                var broker = BrokerDB.RetrieveById(id);
                var tempOptionXml = broker.TempOptionXmlContent.Value;

                if (tempOptionXml.Contains("</options>", StringComparison.OrdinalIgnoreCase))
                {
                    tempOptionXml = tempOptionXml.Replace("</options>", $"{this.TempOption}{Environment.NewLine}</options>");
                }
                else
                {
                    tempOptionXml = tempOptionXml + Environment.NewLine + this.TempOption;
                }

                broker.TempOptionXmlContent = tempOptionXml;
                broker.Save();
                return true;
            }
            catch (Exception exc)
            {
                Tools.LogError($"Could not migrate broker {id} to add temp option {this.TempOption}.", exc);
                return false;
            }
        }
    }
}
