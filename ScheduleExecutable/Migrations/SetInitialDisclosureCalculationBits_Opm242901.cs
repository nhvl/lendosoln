﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using DataAccess;
    using LendersOffice.Constants;

    /// <summary>
    /// Migrator that will set the new bits to automate selection of initial LE/CD on files
    /// where an initial is not already selected.
    /// </summary>
    public class SetInitialDisclosureCalculationBits_Opm242901 : Migrator<Guid>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SetInitialDisclosureCalculationBits_Opm242901"/> class.
        /// </summary>
        /// <param name="name">The migration name.</param>
        public SetInitialDisclosureCalculationBits_Opm242901(string name) :
            base(name)
        {
            this.MigrationHelper = new CustomGUIDMigrationHelper(name, ListLoansForInitialCalculationBitMigration);
        }

        /// <summary>
        /// Triggers the migration to run.
        /// </summary>
        /// <param name="args">String arguments.</param>
        public static void RunMigration(string[] args)
        {
            var migrator = new SetInitialDisclosureCalculationBits_Opm242901("SetInitialDisclosureCalculationBits_Opm242901");
            migrator.Migrate();
        }

        /// <summary>
        /// Selects all valid TRID loan files from active brokers.
        /// </summary>
        /// <returns>A list of loan IDs to migrate.</returns>
        internal static IEnumerable<Guid> ListLoansForInitialCalculationBitMigration()
        {
            List<Guid> loanIds = new List<Guid>();
            string sql = @"
                SELECT lfc.sLId
                FROM LOAN_FILE_CACHE lfc
                    JOIN LOAN_FILE_B lfb on lfc.sLId = lfb.sLId
                WHERE lfc.IsValid = 1
                    AND lfb.sDisclosureRegulationT = 2
                    AND lfc.sBrokerId IN
                        (SELECT BrokerId
                         FROM BROKER
                         WHERE Status = 1)";

            Action<IDataReader> readHandler = delegate (IDataReader reader)
            {
                while (reader.Read())
                {
                    loanIds.Add((Guid)reader["sLId"]);
                }
            };

            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                DBSelectUtility.ProcessDBData(connInfo, sql, null, null, readHandler);
            }

            return loanIds;
        }

        /// <summary>
        /// Sets the loan file to calculate the initial LE and CD, but ONLY if the file does not already have
        /// an initial disclosure of that type. If an initial disclosure already exists, we leave the choice
        /// up to the user.
        /// </summary>
        /// <param name="loanId">The ID of the loan to migrate.</param>
        /// <returns>A boolean indicating whether migration was successful.</returns>
        protected override bool MigrationFunction(Guid loanId)
        {
            try
            {
                var dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(SetInitialDisclosureCalculationBits_Opm242901));
                dataLoan.ByPassFieldSecurityCheck = true;
                dataLoan.AllowSaveWhileQP2Sandboxed = true;
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                bool hasInitialLe = dataLoan.sLoanEstimateDatesInfo.InitialLoanEstimate != null;
                dataLoan.sCalculateInitialLoanEstimate = !hasInitialLe;

                bool hasInitialCd = dataLoan.sClosingDisclosureDatesInfo.InitialClosingDisclosure != null;
                dataLoan.sCalculateInitialClosingDisclosure = !hasInitialCd;

                dataLoan.Save();
                return true;
            }
            catch (SystemException exc)
            {
                Tools.LogError($"Failed to set sCalculateInitialLoanEstimate and sCalculateInitialClosingDisclosure for loan {loanId}.", exc);
                return false;
            }
            catch (ApplicationException exc)
            {
                Tools.LogError($"Failed to set sCalculateInitialLoanEstimate and sCalculateInitialClosingDisclosure for loan {loanId}.", exc);
                return false;
            }
            catch (CBaseException exc)
            {
                Tools.LogError($"Failed to set sCalculateInitialLoanEstimate and sCalculateInitialClosingDisclosure for loan {loanId}.", exc);
                return false;
            }
        }
    }
}
