﻿/// <summary>
/// Settlement migration for iServe.
/// </summary>
namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Text;
    using DataAccess;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.Gateways;

    /// <summary>
    /// Settlement migration for iServe. Doing it as a seperate migration instead of using the tool since their migration is a bit more complicated than what the tool can do in a single pass.
    /// </summary>
    public class OPM235716_iServeSettlementMigrator : Migrator<Guid>
    {
        /// <summary>
        /// Broker Id for PML0170.
        /// </summary>
        Guid pml0170BrokerId = new Guid("ccccf4a3-a076-4e74-a195-9d0e5f2e52eb");

        /// <summary>
        /// Logs any errors.
        /// </summary>
        StringBuilder errorLogger = new StringBuilder();

        /// <summary>
        /// Initializes a new instance of the <see cref="OPM235716_iServeSettlementMigrator"/> class.
        /// </summary>
        /// <param name="name">Name of the migration.</param>
        public OPM235716_iServeSettlementMigrator(string name)
            : base(name)
        {
            this.MigrationHelper = new CustomGUIDMigrationHelper(name, this.GetLoanIdsForMigration);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OPM235716_iServeSettlementMigrator"/> class. 
        /// </summary>
        /// <param name="name">Name of the migration.</param>
        /// <param name="loansToMigrate">The loans to migrate.</param>
        public OPM235716_iServeSettlementMigrator(string name, IEnumerable<Guid> loansToMigrate)
            : base(name)
        {
            this.MigrationHelper = new CustomGUIDMigrationHelper(name, loansToMigrate.ToArray);
        }

        /// <summary>
        /// Usage:
        ///   OPM235716_iServeSettlementMigration => Will run the query to determine which loans to run the migration on.
        ///   OPM235716_iServeSettlementMigration FilePath.txt => Will run the migration on the loan files in FilePath.txt.
        ///     FilePath.txt contains loan ids. One per line.
        /// </summary>
        /// <param name="args">The command line arguments.</param>
        public static void OPM235716_iServeSettlementMigration(string[] args)
        {
            OPM235716_iServeSettlementMigrator migrator = null;

            if (args.Length == 1)
            {
                migrator = new OPM235716_iServeSettlementMigrator("OPM235716_iServeSettlementMigration");
            }
            else if (args.Length == 2)
            {
                List<Guid> loansToMigrate = new List<Guid>();
                var fileContents = TextFileHelper.ReadLines(args[1]);
                foreach (string stringId in fileContents)
                {
                    loansToMigrate.Add(new Guid(stringId));
                }

                migrator = new OPM235716_iServeSettlementMigrator("OPM235716_iServeSettlementMigration", loansToMigrate);
            }

            if (migrator != null)
            {
                migrator.Migrate();
            }
        }

        /// <summary>
        /// The migration function. Performs the Settlement Charge migration for the loan.
        /// </summary>
        /// <param name="id">The loan id of the loan to migrate.</param>
        /// <returns>True if sucessful migration, false otherwise.</returns>
        protected override bool MigrationFunction(Guid id)
        {
            try
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(id, typeof(OPM235716_iServeSettlementMigrator));
                dataLoan.ByPassFieldSecurityCheck = true;
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                if (dataLoan.sUseGFEDataForSCFields)
                {
                    // Already migrated.
                    return true;
                }

                E_CurrentClosingCostSourceT currentCCSource;
                E_LastDisclosedClosingCostSourceT lastDisclosedCCSource;

                if (dataLoan.sSettlementChargesExportSource == E_SettlementChargesExportSource.SETTLEMENT || this.IsAtOrAfterFinalUnderwriting(dataLoan.sStatusT))
                {
                    currentCCSource = E_CurrentClosingCostSourceT.SettlementCharges;
                }
                else if (dataLoan.sSettlementChargesExportSource == E_SettlementChargesExportSource.GFE && !this.IsAtOrAfterFinalUnderwriting(dataLoan.sStatusT))
                {
                    // This else-if check looks like the exact negation of the first if check but going to do it like this in case.
                    currentCCSource = E_CurrentClosingCostSourceT.GFE;
                }
                else
                {
                    errorLogger.AppendLine($"{id.ToString()} could not be migrated.");
                    return false;
                }

                if (dataLoan.sClosingCostMigrationLastDisclosedClosingCostSource == E_LastDisclosedClosingCostSourceT.GFEArchive)
                {
                    lastDisclosedCCSource = E_LastDisclosedClosingCostSourceT.GFEArchive;
                }
                else
                {
                    lastDisclosedCCSource = E_LastDisclosedClosingCostSourceT.GFE;
                }

                dataLoan.MigrateCurrentClosingCostDataToGFE(currentCCSource, lastDisclosedCCSource);

                dataLoan.Save();
                return true;
            }
            catch (Exception e)
            {
                string message = $"{id.ToString()} could not be migrated.";
                errorLogger.AppendLine(message);
                Tools.LogError(message, e);
                return false;
            }
        }

        /// <summary>
        /// Writes the errors to _errorLog.txt.
        /// </summary>
        protected override void EndMigration()
        {
            errorLogger.AppendLine();
            TextFileHelper.AppendString($"{this.Name}_errorLog.txt", errorLogger.ToString());
            errorLogger.Length = 0;
        }
        
        /// <summary>
         /// Gets the loan ids using the select statement.
         /// </summary>
         /// <returns>List of loan ids to migrate.</returns>
        private IEnumerable<Guid> GetLoanIdsForMigration()
        {
            string query = @"
                            SELECT
	                            b.sLId as sLId
                            FROM
	                            LOAN_FILE_CACHE lfc JOIN
	                            LOAN_FILE_B b on lfc.sLId=b.sLId JOIN
	                            LOAN_FILE_C c on b.sLId=c.sLId
                            WHERE
	                            lfc.sBrokerId=@BrokerId AND
	                            lfc.IsValid=1 AND 
	                            lfc.IsTemplate=0 AND
	                            b.sIsHousingExpenseMigrated=0 AND
	                            b.sClosingCostFeeVersionT=0 AND
	                            c.sUseGFEDataForSCFields=0
                           ";

            List<Guid> loanIds = new List<Guid>();

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@BrokerId", pml0170BrokerId)
            };

            Action<IDataReader> readHandler = delegate (IDataReader reader)
            {
                while (reader.Read())
                {
                    loanIds.Add((Guid)reader["slid"]);
                }
            };

            DBSelectUtility.ProcessDBData(pml0170BrokerId, query, timeout: null, parameters: parameters, readerCode: readHandler);
            return loanIds;
        }

        /// <summary>
        /// Checks if the loan status is "after final underwriting" as defined in the case.
        /// </summary>
        /// <param name="status">The status to check.</param>
        /// <returns>True if it is after final underwriting, false otherwise.</returns>
        private bool IsAtOrAfterFinalUnderwriting(E_sStatusT status)
        {
            if (!Enum.IsDefined(typeof(E_sStatusT), status))
            {
                throw new UnhandledEnumException(status);
            }

            switch (status)
            {
                case E_sStatusT.Loan_FinalUnderwriting:
                case E_sStatusT.Loan_PreDocQC:
                case E_sStatusT.Loan_ClearToClose:
                case E_sStatusT.Loan_DocsOrdered:
                case E_sStatusT.Loan_DocsDrawn:
                case E_sStatusT.Loan_Docs:
                case E_sStatusT.Loan_DocsBack:
                case E_sStatusT.Loan_FundingConditions:
                case E_sStatusT.Loan_Funded:
                case E_sStatusT.Loan_Recorded:
                case E_sStatusT.Loan_FinalDocs:
                case E_sStatusT.Loan_SubmittedForPurchaseReview:
                case E_sStatusT.Loan_InPurchaseReview:
                case E_sStatusT.Loan_PrePurchaseConditions:
                case E_sStatusT.Loan_SubmittedForFinalPurchaseReview:
                case E_sStatusT.Loan_InFinalPurchaseReview:
                case E_sStatusT.Loan_ClearToPurchase:
                case E_sStatusT.Loan_Purchased:
                case E_sStatusT.Loan_ReadyForSale:
                case E_sStatusT.Loan_Shipped:
                case E_sStatusT.Loan_InvestorConditions:
                case E_sStatusT.Loan_InvestorConditionsSent:
                case E_sStatusT.Loan_LoanPurchased:
                    return true;
                default:
                    return false;
            }
        }
    }
}
