﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Drivers.SqlServerDB;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Populates the public ID value for the existing originating companies
    /// of all active lenders.
    /// </summary>
    public class Opm472148_PopulateMortgagePoolPublicIds : Migrator<Guid>
    {
        /// <summary>
        /// Prevents a default instance of the <see cref="Opm472148_PopulateMortgagePoolPublicIds"/>
        /// class from being created.
        /// </summary>
        private Opm472148_PopulateMortgagePoolPublicIds()
            : base(nameof(Opm472148_PopulateMortgagePoolPublicIds))
        {
            this.MigrationHelper = new BrokerMigrationHelper(this.Name);
        }

        /// <summary>
        /// Executes the migration.
        /// </summary>
        /// <param name="args">
        /// This parameter is not used.
        /// </param>
        public static void Run(string[] args)
        {
            var migrator = new Opm472148_PopulateMortgagePoolPublicIds();
            migrator.Migrate();
        }

        /// <summary>
        /// Migrates the originating companies for the broker with the specified <paramref name="brokerId"/>.
        /// </summary>
        /// <param name="brokerId">
        /// The ID for the broker.
        /// </param>
        /// <returns>
        /// True if the migration was successful, false otherwise.
        /// </returns>
        protected override bool MigrationFunction(Guid brokerId)
        {
            var poolIds = this.GetPoolIdsByBroker(brokerId);
            if (!poolIds.Any())
            {
                return true;
            }
            
            using (var connection = DbConnectionInfo.GetConnection(brokerId))
            {
                try
                {
                    connection.Open();
                    foreach (var poolId in poolIds)
                    {
                        var indexNumber = BrokerDB.GetAndUpdateOriginatingCompanyIndexNumberCounter(brokerId);

                        SqlParameter[] parameters =
                        {
                            new SqlParameter("@BrokerId", brokerId),
                            new SqlParameter("@PublicId", Guid.NewGuid()),
                            new SqlParameter("@PoolId", poolId)
                        };

                        DBUpdateUtility.Update(connection, null, "UPDATE dbo.MORTGAGE_POOL SET PublicId = @PublicId WHERE BrokerId = @BrokerId AND PoolId = @PoolId", TimeoutInSeconds.Thirty, parameters);
                    }

                    return true;
                }
                catch (Exception exc)
                {
                    Console.WriteLine($"Failed to migrate mortgage pool public IDs for broker {brokerId}: {exc.Message}");
                    Tools.LogError("Failed to migrate mortgage pool public IDs for broker " + brokerId, exc);

                    return false;
                }
            }
        }

        /// <summary>
        /// Obtains the IDs for the mortgage pools of the broker with 
        /// the specified <paramref name="brokerId"/>.
        /// </summary>
        /// <param name="brokerId">
        /// The ID of the broker.
        /// </param>
        /// <returns>
        /// The IDs of the mortgage pools.
        /// </returns>
        private List<long> GetPoolIdsByBroker(Guid brokerId)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", brokerId)
            };

            var poolIds = new List<long>();

            const string Sql = @"
SELECT PoolId
FROM dbo.MORTGAGE_POOL
WHERE BrokerId = @BrokerId AND PublicId IS NULL
ORDER BY PoolId";

            Action<IDataReader> readAction = reader =>
            {
                while (reader.Read())
                {
                    poolIds.Add((long)reader["PoolId"]);
                }
            };

            DBSelectUtility.ProcessDBData(brokerId, Sql, TimeoutInSeconds.Thirty, parameters, readAction);
            return poolIds;
        }
    }
}
