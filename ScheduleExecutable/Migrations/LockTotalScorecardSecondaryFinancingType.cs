﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using DataAccess;
    using LendersOffice.Constants;
    using TotalScoreCard;

    /// <summary>
    /// In case 458943 we are adding the ability to lock the field sTotalScoreSecondaryFinancingSrc.
    /// This migration will ensure that existing loans have the existing field value preserved.
    /// </summary>
    public class LockTotalScorecardSecondaryFinancingType : Migrator<Guid>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LockTotalScorecardSecondaryFinancingType"/> class.
        /// </summary>
        public LockTotalScorecardSecondaryFinancingType()
            : base(nameof(LockTotalScorecardSecondaryFinancingType))
        {
            this.MigrationHelper = new CustomGUIDMigrationHelper(nameof(LockTotalScorecardSecondaryFinancingType), GetLoansWithTotalSecondaryFinancing);
        }

        /// <summary>
        /// Triggers the migration to run.
        /// </summary>
        /// <param name="args">String arguments.</param>
        public static void RunMigration(string[] args)
        {
            var migrator = new LockTotalScorecardSecondaryFinancingType();
            migrator.Migrate();
        }

        /// <summary>
        /// Retrieves GUIDs for migration. We only care about loan files with
        /// sFHASecondaryFinancingIsGov or sFHASecondaryFinancingIsFamily, as
        /// the source calculation based on those will change. Any other values
        /// of the source will stay the same in the new calculation.
        /// </summary>
        /// <returns>A list of loan IDs to migrate.</returns>
        private List<Guid> GetLoansWithTotalSecondaryFinancing()
        {
            string sql = @"
                SELECT lfa.sLId
                FROM LOAN_FILE_A lfa
	                JOIN LOAN_FILE_F lff ON lfa.sLId = lff.sLId
                WHERE lfa.IsValid = 1
	                AND lfa.IsTemplate = 0
	                AND (lff.sFHASecondaryFinancingIsGov = 1
		                OR lff.sFHASecondaryFinancingIsFamily = 1)";

            var loanIds = new List<Guid>();
            Action<IDataReader> readHandler = delegate (IDataReader reader)
            {
                while (reader.Read())
                {
                    loanIds.Add((Guid)reader["sLId"]);
                }
            };

            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                DBSelectUtility.ProcessDBData(connInfo, sql, null, null, readHandler);
            }

            return loanIds;
        }

        /// <summary>
        /// Locks the secondary financing source to the legacy calculation to preserve the value.
        /// </summary>
        /// <param name="loanId">The loan to migrate.</param>
        /// <returns>A boolean indicating whether the migration was successful.</returns>
        protected override bool MigrationFunction(Guid loanId)
        {
            try
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(LockTotalScorecardSecondaryFinancingType));
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                dataLoan.sTotalScoreSecondaryFinancingSrcLckd = true;

                // Use the legacy calculation instead of the current LoanFileFields version.
                if (dataLoan.sFHASecondaryFinancingIsGov)
                {
                    dataLoan.sTotalScoreSecondaryFinancingSrc = E_TotalScorecardSecondaryFinancingSrc.GovernmentNonprofitInstrumentality;
                }
                else if (dataLoan.sFHASecondaryFinancingIsFamily)
                {
                    dataLoan.sTotalScoreSecondaryFinancingSrc = E_TotalScorecardSecondaryFinancingSrc.PrivateOrganizations;
                }

                dataLoan.Save();
                return true;
            }
            catch (Exception exc)
            {
                Tools.LogError($"Unable to migrate loan ID {loanId.ToString()}", exc);
                return false;
            }
        }
    }
}
