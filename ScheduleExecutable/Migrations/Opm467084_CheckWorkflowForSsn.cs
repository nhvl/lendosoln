﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using DataAccess;
    using ConfigSystem;
    using ConfigSystem.DataAccess;
    using LendersOffice.ConfigSystem;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Drivers.Gateways;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Utils;

    public class Opm467084_CheckWorkflowForSsn : Migrator<Guid>
    {
        /// <summary>
        /// The Workflow operations we're interested in.
        /// </summary>
        private static HashSet<string> interestedOps = new HashSet<string>(StringComparer.OrdinalIgnoreCase)
        {
            WorkflowOperations.ReadLoan.Id,
            WorkflowOperations.ReadLoanOrTemplate.Id,
            WorkflowOperations.ReadTemplate.Id,
            WorkflowOperations.WriteLoan.Id,
            WorkflowOperations.WriteLoanOrTemplate.Id,
            WorkflowOperations.WriteTemplate.Id
        };

        /// <summary>
        /// Parameter fields to look for.
        /// </summary>
        private static HashSet<string> ssnFields = new HashSet<string>(StringComparer.OrdinalIgnoreCase)
        {
            "aBSsn",
            "aCSsn",
            "aB4506TSsnTinEin",
            "aC4506TSsnTinEin",
            "aVaServ1Ssn",
            "aVaServ2Ssn",
            "aVaServ3Ssn",
            "aVaServ4Ssn",
            "sHmdaCoApplicantSourceSsn",
            "sCombinedBorSsn",
            "sCombinedCoborSsn",
            "sTax1098PayerSsn"
        };

        /// <summary>
        /// String builder used to build csv file.
        /// </summary>
        private StringBuilder csv;

        /// <summary>
        /// Initializes a new instance of the <see cref="Opm467084_CheckWorkflowForSsn"/> class.
        /// </summary>
        public Opm467084_CheckWorkflowForSsn()
            : base("Opm467084_CheckWorkflowForSsn")
        {
            this.MigrationHelper = new BrokerMigrationHelper(this.Name);

            this.csv = new StringBuilder();
            this.csv.AppendLine("BrokerId,Condition Type,ConditionId,Parameter,Function,Values");
        }

        /// <summary>
        /// Runs the migration.
        /// </summary>
        /// <param name="args">Command line arguments. Accepts a Customer Code as 2nd argument.</param>
        public static void Run(string[] args)
        {
            Opm467084_CheckWorkflowForSsn migrator = new Opm467084_CheckWorkflowForSsn();
            migrator.Migrate();

            TextFileHelper.WriteString("CheckWorkflowForSsn.csv", migrator.csv.ToString(), false);
        }

        protected override bool MigrationFunction(Guid id)
        {
            try
            {
                IConfigRepository repository = ConfigHandler.GetUncachedRepository(id);
                ReleaseConfigData config = repository.LoadActiveRelease(id);

                CheckConditionSet(config.Configuration.Conditions, "Conditions", id);
                CheckConditionSet(config.Configuration.Constraints, "Constraints", id);
            }
            catch (Exception e)
            {
                string msg = $"[Opm467084_CheckWorkflowForSsn] An unexpected error occurred while searching thorough the Workflow Config for brokerId=[{id}].";
                Tools.LogError(msg, e);
            }

            return true;
        }

        private void CheckConditionSet(AbstractSecurityPermissionSet conditionSet, string conditionType, Guid brokerId)
        {
            foreach (AbstractConditionGroup condition in conditionSet)
            {
                if (!condition.SystemOperationNames.Intersect(interestedOps).Any())
                {
                    continue;
                }

                foreach (Parameter param in condition.ParameterSet)
                {
                    if (!ssnFields.Contains(param.Name))
                    {
                        continue;
                    }

                    this.csv.AppendLine($"\"{brokerId}\",\"{conditionType}\",\"{condition.Id}\",\"{param.Name}\",\"{param.FunctionT}\",\"{string.Join(", ", param.ValueDesc.Keys)}\"");
                }
            }
        }
    }
}
