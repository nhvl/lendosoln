﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.CreditReport;
    using LendersOffice.Drivers.Gateways;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Provides a means for querying credit reports to inspect
    /// on-file credit score model names.
    /// </summary>
    public class Opm451697_QueryModelsInStoredCreditReports
    {
        /// <summary>
        /// Runs the credit report query.
        /// </summary>
        /// <param name="args">
        /// The parameter is not used.
        /// </param>
        public static void Run(string[] args)
        {
            var migrator = new Opm451697_QueryModelsInStoredCreditReports();
            migrator.RunQuery();
        }

        /// <summary>
        /// Runs the credit report query.
        /// </summary>
        private void RunQuery()
        {
            foreach (var creditTuple in this.GetCreditReportKeyTuples())
            {
                var fileDbKey = CreditReportFactory.RetrieveFileDBKey(
                    brokerId: creditTuple.Item1, 
                    appId: creditTuple.Item2, 
                    bIsRetrieveLastDeleteCreditReport: false, 
                    reportType:CreditReportTypes.Primary);

                if (fileDbKey == Guid.Empty || !FileDBTools.DoesFileExist(E_FileDB.Normal, fileDbKey.ToString()))
                {
                    continue;
                }

                var creditData = this.GetCreditData(fileDbKey, creditTuple.Item3);

                var modelNames = new
                {
                    BrokerId = creditTuple.Item1,
                    AppId = creditTuple.Item2,
                    LoanId = creditTuple.Item3,

                    BorrowerExperian = creditData.BorrowerScore.ExperianModelName,
                    BorrowerEquifax = creditData.BorrowerScore.EquifaxModelName,
                    BorrowerTransUnion = creditData.BorrowerScore.TransUnionModelName,

                    CoborrowerExperian = creditData.CoborrowerScore.ExperianModelName,
                    CoborrowerEquifax = creditData.CoborrowerScore.EquifaxModelName,
                    CoborrowerTransUnion = creditData.CoborrowerScore.TransUnionModelName,
                };

                this.WriteToQueryFile(modelNames);
            }
        }

        /// <summary>
        /// Gets the tuples used to query credit reports in FileDB.
        /// </summary>
        /// <returns>
        /// A list of (BrokerId, AppId, LoanId) tuples.
        /// </returns>
        private IEnumerable<Tuple<Guid, Guid, Guid>> GetCreditReportKeyTuples()
        {
            var tuples = new LinkedList<Tuple<Guid, Guid, Guid>>();

            const string Sql = @"
SELECT TOP 100000 lfc.sBrokerId, app.aAppId, lfc.sLId
FROM 
	LOAN_FILE_CACHE lfc WITH (NOLOCK)
	JOIN APPLICATION_A app WITH (NOLOCK) ON lfc.sLId = app.sLid
WHERE EXISTS(
	SELECT 1 
	FROM SERVICE_FILE WITH (NOLOCK)
	WHERE	
		Owner = app.aAppId
        AND IsInEffect = 1
		AND ServiceType = 'Credit'
		AND FileType = 'CreditReport')
";

            Action<IDataReader> readerCode = (reader) =>
            {
                while (reader.Read())
                {
                    tuples.AddLast(Tuple.Create(
                        (Guid)reader["sBrokerId"],
                        (Guid)reader["aAppId"],
                        (Guid)reader["sLId"]));
                }
            };

            foreach (var connectionInfo in DbConnectionInfo.ListAll())
            {
                DBSelectUtility.ProcessDBData(connectionInfo, Sql, TimeoutInSeconds.Sixty, parameters: null, readerCode: readerCode);
            }

            return tuples;
        }

        /// <summary>
        /// Gets the credit data proxy.
        /// </summary>
        /// <param name="fileDbKey">
        /// The FileDB key for the credit report.
        /// </param>
        /// <param name="loanId">
        /// The loan ID for the credit report.
        /// </param>
        /// <returns>
        /// The credit data proxy.
        /// </returns>
        private CreditReportProxy GetCreditData(Guid fileDbKey, Guid loanId)
        {
            var debugInfo = new CreditReportDebugInfo(loanId, loginNm: "SYSTEM")
            {
                FileDbKey = fileDbKey.ToString()
            };

            var creditDoc = CreditReportFactory.LoadFromFileDB(fileDbKey.ToString(), debugInfo);
            return CreditReportFactory.ConstructCreditReportFromXmlDoc(creditDoc, debugInfo);
        }

        /// <summary>
        /// Writes the specified <paramref name="data"/> to the query file.
        /// </summary>
        /// <param name="data">
        /// The data to write.
        /// </param>
        private void WriteToQueryFile(object data)
        {
            var serializedData = SerializationHelper.JsonNetAnonymousSerialize(data) + Environment.NewLine;
            TextFileHelper.AppendString(nameof(Opm451697_QueryModelsInStoredCreditReports) + ".txt", serializedData);
        }
    }
}
