﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;
    using static RowBasedEncryptionGenericTableMigrator;

    public class MakeIntegrationEncryptionKeysUnique_Opm472081
    {
        private const string CaseNumber = "OPM472081";

        public static void Migrate_GenericFramework(string[] args)
        {
            var table = new TableToEncrypt
            {
                TableName = "GENERIC_FRAMEWORK_VENDOR_BROKER_CONFIG",
                PrimaryKeyColumns = new[]
                {
                    new PrimaryKeyColumn { ColumnName = "BrokerID" },
                    new PrimaryKeyColumn { ColumnName = "ProviderID" },
                },
                Columns = new ColumnToEncrypt[]
                {
                    new AlreadyEncryptedColumnToEncrypt
                    {
                        ExistingEncryptionKeyId = EncryptionKeyIdentifier.Create("8A65B3E1-75F3-4EC8-B6FD-C47F0372B286").ForceValue(),
                        ExistingColumnName = "CredentialXML",
                        TargetColumnName = "CredentialXML",
                    },
                },
                SelectStatementForKeys = @"SELECT BrokerID, ProviderID FROM GENERIC_FRAMEWORK_VENDOR_BROKER_CONFIG WHERE EncryptionKeyId = '8A65B3E1-75F3-4EC8-B6FD-C47F0372B286'",
                SelectStatementForRecord = @"SELECT EncryptionKeyId,
    BrokerID,
    ProviderID,
    CredentialXML
FROM GENERIC_FRAMEWORK_VENDOR_BROKER_CONFIG
WHERE BrokerID = @BrokerID AND ProviderID = @ProviderID AND EncryptionKeyId = '8A65B3E1-75F3-4EC8-B6FD-C47F0372B286'",
                UpdateStatement = @"UPDATE GENERIC_FRAMEWORK_VENDOR_BROKER_CONFIG
SET EncryptionKeyId = @EncryptionKeyId,
    CredentialXML = @CredentialXML
WHERE BrokerID = @BrokerID AND ProviderID = @ProviderID",
            };

            MigrateLenderTable(table);
        }

        public static void Migrate_Irs4506TUser(string[] args)
        {
            var table = new TableToEncrypt
            {
                TableName = "IRS_4506T_VENDOR_EMPLOYEE_INFO",
                PrimaryKeyColumns = new[]
                {
                    new PrimaryKeyColumn { ColumnName = "UserId" },
                    new PrimaryKeyColumn { ColumnName = "VendorId" },
                },
                Columns = new ColumnToEncrypt[]
                {
                    new AlreadyEncryptedColumnToEncrypt
                    {
                        ExistingEncryptionKeyId = EncryptionKeyIdentifier.Create("03F709FE-0366-4FB4-82F1-D8803E264E73").ForceValue(),
                        ExistingColumnName = "Password",
                        TargetColumnName = "Password",
                    },
                },
                SelectStatementForKeys = @"SELECT UserId, VendorId FROM IRS_4506T_VENDOR_EMPLOYEE_INFO WHERE EncryptionKeyId = '03F709FE-0366-4FB4-82F1-D8803E264E73'",
                SelectStatementForRecord = @"SELECT EncryptionKeyId,
    UserId,
    VendorId,
    Password
FROM IRS_4506T_VENDOR_EMPLOYEE_INFO
WHERE UserId = @UserId AND VendorId = @VendorId AND EncryptionKeyId = '03F709FE-0366-4FB4-82F1-D8803E264E73'",
                UpdateStatement = @"UPDATE IRS_4506T_VENDOR_EMPLOYEE_INFO
SET EncryptionKeyId = @EncryptionKeyId,
    Password = @Password
WHERE UserId = @UserId AND VendorId = @VendorId",
            };

            MigrateLenderTable(table);
        }

        public static void Migrate_MIBranch(string[] args)
        {
            var table = new TableToEncrypt
            {
                TableName = "MORTGAGE_INSURANCE_VENDOR_BRANCH_SETTINGS",
                PrimaryKeyColumns = new[]
                {
                    new PrimaryKeyColumn { ColumnName = "VendorId" },
                    new PrimaryKeyColumn { ColumnName = "BranchId" },
                },
                Columns = new ColumnToEncrypt[]
                {
                    new AlreadyEncryptedColumnToEncrypt
                    {
                        ExistingEncryptionKeyId = EncryptionKeyIdentifier.Create("98D44714-6FE5-4464-AD8F-327838C561D8").ForceValue(),
                        ExistingColumnName = "Password",
                        TargetColumnName = "Password",
                    },
                },
                SelectStatementForKeys = @"SELECT VendorId, BranchId FROM MORTGAGE_INSURANCE_VENDOR_BRANCH_SETTINGS WHERE EncryptionKeyId = '98D44714-6FE5-4464-AD8F-327838C561D8'",
                SelectStatementForRecord = @"SELECT EncryptionKeyId,
    VendorId,
    BranchId,
    Password
FROM MORTGAGE_INSURANCE_VENDOR_BRANCH_SETTINGS
WHERE VendorId = @VendorId AND BranchId = @BranchId AND EncryptionKeyId = '98D44714-6FE5-4464-AD8F-327838C561D8'",
                UpdateStatement = @"UPDATE MORTGAGE_INSURANCE_VENDOR_BRANCH_SETTINGS
SET EncryptionKeyId = @EncryptionKeyId,
    Password = @Password
WHERE VendorId = @VendorId AND BranchId = @BranchId",
            };

            MigrateLenderTable(table);
        }

        public static void Migrate_MIBroker(string[] args)
        {
            var table = new TableToEncrypt
            {
                TableName = "MORTGAGE_INSURANCE_VENDOR_BROKER_SETTINGS",
                PrimaryKeyColumns = new[]
                {
                    new PrimaryKeyColumn { ColumnName = "VendorId" },
                    new PrimaryKeyColumn { ColumnName = "BrokerId" },
                },
                Columns = new ColumnToEncrypt[]
                {
                    new AlreadyEncryptedColumnToEncrypt
                    {
                        ExistingEncryptionKeyId = EncryptionKeyIdentifier.Create("98D44714-6FE5-4464-AD8F-327838C561D8").ForceValue(),
                        ExistingColumnName = "Password",
                        TargetColumnName = "Password",
                    },
                },
                SelectStatementForKeys = @"SELECT VendorId, BrokerId FROM MORTGAGE_INSURANCE_VENDOR_BROKER_SETTINGS WHERE EncryptionKeyId = '98D44714-6FE5-4464-AD8F-327838C561D8'",
                SelectStatementForRecord = @"SELECT EncryptionKeyId,
    VendorId,
    BrokerId,
    Password
FROM MORTGAGE_INSURANCE_VENDOR_BROKER_SETTINGS
WHERE VendorId = @VendorId AND BrokerId = @BrokerId AND EncryptionKeyId = '98D44714-6FE5-4464-AD8F-327838C561D8'",
                UpdateStatement = @"UPDATE MORTGAGE_INSURANCE_VENDOR_BROKER_SETTINGS
SET EncryptionKeyId = @EncryptionKeyId,
    Password = @Password
WHERE VendorId = @VendorId AND BrokerId = @BrokerId",
            };

            MigrateLenderTable(table);
        }

        public static void Migrate_ServiceCredential(string[] args)
        {
            var table = new TableToEncrypt
            {
                TableName = "SERVICE_CREDENTIAL",
                PrimaryKeyColumns = new[]
                {
                    new PrimaryKeyColumn { ColumnName = "Id" },
                },
                Columns = new ColumnToEncrypt[]
                {
                    new AlreadyEncryptedColumnToEncrypt
                    {
                        ExistingEncryptionKeyId = EncryptionKeyIdentifier.Create("8ABD578F-4F3D-4E8B-A38E-CCDD8C2F8854").ForceValue(),
                        ExistingColumnName = "UserPassword",
                        TargetColumnName = "UserPassword",
                    },
                },
                SelectStatementForKeys = @"SELECT Id FROM SERVICE_CREDENTIAL WHERE EncryptionKeyId = '8ABD578F-4F3D-4E8B-A38E-CCDD8C2F8854'",
                SelectStatementForRecord = @"SELECT EncryptionKeyId,
    Id,
    UserPassword
FROM SERVICE_CREDENTIAL
WHERE Id = @Id AND EncryptionKeyId = '8ABD578F-4F3D-4E8B-A38E-CCDD8C2F8854'",
                UpdateStatement = @"UPDATE SERVICE_CREDENTIAL
SET EncryptionKeyId = @EncryptionKeyId,
    UserPassword = @UserPassword
WHERE Id = @Id",
            };

            MigrateLenderTable(table);
        }

        public static void MigrateTransmissionTables(string[] args)
        {
            EncryptionKeyIdentifier requestEncryptionKeyId = EncryptionKeyIdentifier.Create(Guid.Parse("D8A72253-69B1-4D02-BAAF-9D3D9D3FC8C4")).ForceValue();
            EncryptionKeyIdentifier responseEncryptionKeyId = EncryptionKeyIdentifier.Create(Guid.Parse("A705BAB7-9436-4E49-A26E-56625CCE63BA")).ForceValue();
            var verificationTransmissionTable = new TableToEncrypt
            {
                TableName = "VERIFICATION_TRANSMISSION",
                PrimaryKeyColumns = new[]
                {
                    new PrimaryKeyColumn { ColumnName = "TransmissionId" },
                },
                Columns = new ColumnToEncrypt[]
                {
                    new AlreadyEncryptedColumnToEncrypt
                    {
                        ExistingEncryptionKeyId = requestEncryptionKeyId,
                        ExistingColumnName = "RequestCredentialPassword",
                        TargetColumnName = "RequestCredentialPassword",
                    },
                    new AlreadyEncryptedColumnToEncrypt
                    {
                        ExistingEncryptionKeyId = responseEncryptionKeyId,
                        ExistingColumnName = "ResponseCredentialPassword",
                        TargetColumnName = "ResponseCredentialPassword",
                    },
                },
                SelectStatementForKeys = @"SELECT TransmissionId FROM VERIFICATION_TRANSMISSION WHERE EncryptionKeyId = '00000000-0000-0000-0000-000000000000'",
                SelectStatementForRecord = @"SELECT EncryptionKeyId,
    TransmissionId,
    RequestCredentialPassword,
    ResponseCredentialPassword
FROM VERIFICATION_TRANSMISSION
WHERE TransmissionId = @TransmissionId AND EncryptionKeyId = '00000000-0000-0000-0000-000000000000'",
                UpdateStatement = @"UPDATE VERIFICATION_TRANSMISSION
SET EncryptionKeyId = @EncryptionKeyId,
    RequestCredentialPassword = @RequestCredentialPassword,
    ResponseCredentialPassword = @ResponseCredentialPassword
WHERE TransmissionId = @TransmissionId",
            };

            MigrateTable(DataAccess.DataSrc.LOShare, verificationTransmissionTable);

            var titleTransmissionTable = new TableToEncrypt
            {
                TableName = "TITLE_FRAMEWORK_TRANSMISSION_INFO",
                PrimaryKeyColumns = new[]
                {
                    new PrimaryKeyColumn { ColumnName = "TransmissionInfoId" },
                },
                Columns = new ColumnToEncrypt[]
                {
                    new AlreadyEncryptedColumnToEncrypt
                    {
                        ExistingEncryptionKeyId = requestEncryptionKeyId,
                        ExistingColumnName = "RequestCredentialPassword",
                        TargetColumnName = "RequestCredentialPassword",
                    },
                    new AlreadyEncryptedColumnToEncrypt
                    {
                        ExistingEncryptionKeyId = responseEncryptionKeyId,
                        ExistingColumnName = "ResponseCredentialPassword",
                        TargetColumnName = "ResponseCredentialPassword",
                    },
                },
                SelectStatementForKeys = @"SELECT TransmissionInfoId FROM TITLE_FRAMEWORK_TRANSMISSION_INFO WHERE EncryptionKeyId = '00000000-0000-0000-0000-000000000000'",
                SelectStatementForRecord = @"SELECT EncryptionKeyId,
    TransmissionInfoId,
    RequestCredentialPassword,
    ResponseCredentialPassword
FROM TITLE_FRAMEWORK_TRANSMISSION_INFO
WHERE TransmissionInfoId = @TransmissionInfoId AND EncryptionKeyId = '00000000-0000-0000-0000-000000000000'",
                UpdateStatement = @"UPDATE TITLE_FRAMEWORK_TRANSMISSION_INFO
SET EncryptionKeyId = @EncryptionKeyId,
    RequestCredentialPassword = @RequestCredentialPassword,
    ResponseCredentialPassword = @ResponseCredentialPassword
WHERE TransmissionInfoId = @TransmissionInfoId",
            };

            MigrateTable(DataAccess.DataSrc.LOShare, titleTransmissionTable);
        }
    }
}
