﻿namespace ScheduleExecutable.Migrations
{
    using System.Collections.Generic;
    using LendersOffice.ConfigSystem.Operations;

    public class ImportIntoExistingFileAdder : SystemConfigOperationMigrator
    {
        /// <summary>
        /// Runs the migration.
        /// </summary>
        /// <param name="args">The command line arguments.</param>
        public static void Run(string[] args)
        {
            var privilegeAdder = new ImportIntoExistingFileAdder();
            privilegeAdder.AddOperations();
        }

        /// <summary>
        /// Gets the operations to add.
        /// </summary>
        /// <returns>The operations to add.</returns>
        protected override IEnumerable<OperationAddition> GetNewOperations()
        {
            return new[]
            {
                new OperationAddition(
                    WorkflowOperations.ImportIntoExistingFile,
                    defaultValue: false,
                    notes: "OPM 184821",
                    failureMessage: "You do not have permission to import into this file.")
            };
        }
    }
}
