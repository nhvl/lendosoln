﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.ObjLib.Task;
    using LendersOffice.Security;
    using System.Threading;

    /// <summary>
    /// At this point, I'm going to write this to iterate over a list of the task ids that we processed.
    /// This should allow 
    /// </summary>
    public class Opm338196_ReactivateTemplateTasks
    {
        /// <summary>
        /// Broker id for PML0208.
        /// </summary>
        private Guid BrokerId = new Guid("d352da7a-bfac-45d5-8c21-5f72522e834f");
        private IEnumerable<TaskIdUserAssignment> tasksToReactivate;

        private static readonly Dictionary<string, E_RoleT> roleTypeByRoleDescription = new Dictionary<string, E_RoleT>()
        {
            ["Funder"] = E_RoleT.Funder,
            ["Post-Closer"] = E_RoleT.PostCloser,
            ["Processor"] = E_RoleT.Processor,
            ["Loan Officer"] = E_RoleT.LoanOfficer
        };

        private static readonly Dictionary<string, Guid> userIdByUserName = new Dictionary<string, Guid>()
        {
            ["SETUP DEPARTMENT"] = new Guid("418D61A0-73D4-411D-8B79-B923B3604F2E"), //disabled
            ["Mina Soliman"] = new Guid("D941A6FD-8E6F-43EE-93A3-E7AD8365DFD7"),
            ["Haidy Ibrahim "] = new Guid("035594D8-7FDE-4FAA-86BA-52BE4F747869"),
            ["Rachel Solorzano"] = new Guid("1BB0194C-013B-4517-8099-792D8808ADB7"), // disabled
        };

        public struct TaskIdUserAssignment
        {
            public readonly string TaskId;
            public readonly string Assignment;

            public TaskIdUserAssignment(string csv)
            {
                var components = csv.Split(new[] { ',' });
                this.TaskId = components[0];
                this.Assignment = components[1];
            }

            public override string ToString()
            {
                return $"TaskId: {this.TaskId}, Assignment: {this.Assignment}";
            }
        }

        public Opm338196_ReactivateTemplateTasks(string filepath)
        {
            this.tasksToReactivate = TextFileHelper.ReadLines(filepath).Select(line => new TaskIdUserAssignment(line));
            Thread.CurrentPrincipal = SystemUserPrincipal.TaskSystemUser;
        }

        public static void ReactivateTemplateTasks(string[] args)
        {
            var migrator = new Opm338196_ReactivateTemplateTasks(args[1]);
            migrator.ReactivateTemplateTasks();
        }

        public void ReactivateTemplateTasks()
        {
            int i = 0;
            foreach (var task in this.tasksToReactivate)
            {
                if (++i % 50 == 0)
                {
                    Console.WriteLine($"Processing item {i}");
                }

                ReactivateTaskIfLoanTemplate(task);
            }
        }

        public void ReactivateTaskIfLoanTemplate(TaskIdUserAssignment taskInfo)
        {
            try
            {
                var task = Task.RetrieveWithoutPermissionCheck(this.BrokerId, taskInfo.TaskId);

                if (task.BrokerId != this.BrokerId)
                {
                    Tools.LogError($"Opm338196_ReactivateTemplateTasks: Passed in broker id ({this.BrokerId}) somehow doesn't match the task brokerId for task <{taskInfo}>.");
                    return;
                }

                if (task.TaskStatus == E_TaskStatus.Closed)
                {
                    // Then we will either need to assign the role id or the user id.
                    if (roleTypeByRoleDescription.ContainsKey(taskInfo.Assignment))
                    {
                        task.TaskToBeAssignedRoleId = Role.Get(roleTypeByRoleDescription[taskInfo.Assignment]).Id;
                    }
                    else if (userIdByUserName.ContainsKey(taskInfo.Assignment))
                    {
                        task.TaskAssignedUserId = userIdByUserName[taskInfo.Assignment];
                    }
                    else
                    {
                        Tools.LogError($"Opm338196_ReactivateTemplateTasks: Unable to map assignment {taskInfo}.");
                        return;
                    }

                    task.Reactivate();
                }
            }
            catch (Exception exc)
            {
                Tools.LogError($"Opm338196_ReactivateTemplateTasks: Error occurred when reactivating task {taskInfo}.", exc);
            }
        }
    }
}
