﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using DataAccess;
    using LendersOffice.Admin;

    /// <summary>
    /// Sets the AllowDeviceRegistrationViaAuthCodePage broker bit for MFA-enabled brokers.
    /// </summary>
    public class PopulateDeviceRegistrationBrokerBitMigrator : Migrator<Guid>
    {
        /// <summary>
        /// Broker ids of brokers that could not be migrated.
        /// </summary>
        private List<string> unmigratedBrokers = new List<string>();

        /// <summary>
        /// Runs the migration.
        /// </summary>
        /// <param name="args">The command line arguments.</param>
        public static void PopulateDeviceRegistrationBrokerBit(string[] args)
        {
            PopulateDeviceRegistrationBrokerBitMigrator migrator;
            if (args.Length == 1)
            {
                migrator = new PopulateDeviceRegistrationBrokerBitMigrator();
            }
            else
            {
                List<Guid> brokerIds = new List<Guid>();
                foreach (string id in File.ReadAllLines(args[1]))
                {
                    brokerIds.Add(Guid.Parse(id));
                }

                migrator = new PopulateDeviceRegistrationBrokerBitMigrator(brokerIds);
            }

            migrator.Migrate();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PopulateDeviceRegistrationBrokerBitMigrator"/> class.
        /// </summary>
        public PopulateDeviceRegistrationBrokerBitMigrator()
            : base("PopulateDeviceRegistrationBrokerBit")
        {
            this.MigrationHelper = new BrokerMigrationHelper(this.Name);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PopulateDeviceRegistrationBrokerBitMigrator"/> class.
        /// </summary>
        /// <param name="brokerIds">The brokers to be migrated.</param>
        public PopulateDeviceRegistrationBrokerBitMigrator(IEnumerable<Guid> brokerIds)
            : base("PopulateDeviceRegistrationBrokerBit")
        {
            this.MigrationHelper = new CustomGUIDMigrationHelper(this.Name, () => brokerIds);
        }

        /// <summary>
        /// Runs the migration.
        /// </summary>
        /// <param name="id">The broker id.</param>
        /// <returns>True if saved. False otherwise.</returns>
        protected override bool MigrationFunction(Guid id)
        {
            try
            {
                var broker = BrokerDB.RetrieveById(id);
                if (broker.IsOptInMultiFactorAuthentication)
                {
                    broker.AllowDeviceRegistrationViaAuthCodePage = true;
                    broker.Save();
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Tools.LogError($"Unable to migrate broker {id} for migration {this.Name}.", ex);
                unmigratedBrokers.Add(id.ToString());
                return false;
            }
        }

        /// <summary>
        /// Writes out any broker ids that errored during migration to a file.
        /// </summary>
        protected override void EndMigration()
        {
            if (unmigratedBrokers.Count > 0)
            {
                File.WriteAllLines(this.Name + "_errorLog.txt", this.unmigratedBrokers);
            }
        }
    }
}
