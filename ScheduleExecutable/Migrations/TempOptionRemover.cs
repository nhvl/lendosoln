﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Xml;
    using System.Xml.Linq;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;

    public class TempOptionRemover
    {
        private readonly Regex startElement;

        private TempOptionRemover(string name)
        {
            this.Name = name;
            this.startElement = new Regex($@"<option\s+name=""{this.Name}""", RegexOptions.Compiled | RegexOptions.IgnoreCase);
        }

        public string Name { get; }

        public static void RemoveByName(string[] namesToRemove)
        {
            if (!namesToRemove.Any())
            {
                Console.WriteLine("No options to remove");
                return;
            }

            RemoveOptions(namesToRemove, suiteType: null);
        }

        public static void RemoveByNameAndSuiteType(string[] namesToRemove)
        {
            if (!namesToRemove.Any())
            {
                Console.WriteLine("No options to remove");
                return;
            }
            
            var suiteType = namesToRemove[0].ToNullableEnum<BrokerSuiteType>(ignoreCase: true);
            if (suiteType == null)
            {
                Console.WriteLine("Invalid broker suite type " + namesToRemove[0]);
                return;
            }

            RemoveOptions(namesToRemove.Skip(1), suiteType);
        }

        private static void RemoveOptions(IEnumerable<string> namesToRemove, BrokerSuiteType? suiteType)
        {
            char[] invalidXmlAttributeChars = new char[] { '<', '&', '"' }; // For attribute wrapped in double quotes, per https://www.w3.org/TR/REC-xml/#NT-AttValue
            if (namesToRemove.Any(name => name.IndexOfAny(invalidXmlAttributeChars) >= 0))
            {
                Console.WriteLine($"Detected invalid characters in one of the names; [\"{string.Join("\", \"", namesToRemove)}\"] cannot contain ['{string.Join("', '", invalidXmlAttributeChars)}\']");
                return;
            }

            var removers = namesToRemove.Select(name => new TempOptionRemover(name)).ToList();
            Console.WriteLine($"Running this will remove the following temporary options from {suiteType?.ToString() ?? "all"} active lenders:");
            foreach (var remover in removers)
            {
                Console.WriteLine($"  - \"{remover.Name}\"");
            }

            Console.Write("Confirm (y/n)? ");
            string response = Console.ReadLine()?.Trim().ToLower();
            if (response == "y" || response == "yes")
            {
                MigrateLenders(removers, suiteType);
            }
            else
            {
                Console.WriteLine("Aborting...");
            }
        }

        private static void MigrateLenders(IReadOnlyCollection<TempOptionRemover> removers, BrokerSuiteType? suiteType)
        {
            foreach (Guid brokerId in Tools.GetAllActiveBrokers(suiteType))
            {
                try
                {
                    var broker = BrokerDB.RetrieveById(brokerId);
                    string tempOptionXml = broker.TempOptionXmlContent.Value;
                    string newTempOptionXml = tempOptionXml;

                    var completedRemovals = new List<Tuple<TempOptionRemover, string>>(removers.Count);
                    foreach (TempOptionRemover remover in removers)
                    {
                        string possibleElement = remover.FindPossibleElement(tempOptionXml, broker.CustomerCode);
                        if (IsValidXmlElement(possibleElement, broker.CustomerCode))
                        {
                            newTempOptionXml = newTempOptionXml.Replace(possibleElement, string.Empty);
                            completedRemovals.Add(Tuple.Create(remover, possibleElement));
                        }
                    }

                    if (tempOptionXml != newTempOptionXml)
                    {
                        broker.TempOptionXmlContent = newTempOptionXml;

                        if (broker.Save())
                        {
                            Console.WriteLine($"Updated {broker.CustomerCode}:");
                            foreach (var removal in completedRemovals)
                            {
                                Console.WriteLine($"  {removal.Item1.Name} - {removal.Item2}");
                            }
                        }
                        else
                        {
                            Console.WriteLine("Unable to update " + broker.CustomerCode);
                        }
                    }
                }
                catch (Exception exc)
                {
                    DataAccess.Tools.LogError("Unable to remove temp option(s) to lender " + brokerId, exc);
                    Console.WriteLine("Unable to update " + brokerId + " due to " + exc.GetType().FullName + ".");
                }
            }

            Console.WriteLine("Completed migration for all active lenders");
        }

        private static bool IsValidXmlElement(string elementString, string customerCode)
        {
            try
            {
                return elementString != null && XElement.Parse(elementString) != null;
            }
            catch (XmlException)
            {
                Console.WriteLine(customerCode + ": Failed to parse xml element " + elementString);
                return false;
            }
        }

        private string FindPossibleElement(string sourceText, string customerCode)
        {
            var match = this.startElement.Match(sourceText);
            if (!match.Success)
            {
                return null;
            }

            int startIndex = match.Index;
            const string selfClosingElementString = "/>";
            int selfClosingIndex = sourceText.IndexOf(selfClosingElementString, startIndex);
            int closeElementIndex = sourceText.IndexOf('>', startIndex);
            if (selfClosingIndex >= 0 && selfClosingIndex < closeElementIndex)
            {
                // We're hoping the element is self closing, i.e. <option />
                return sourceText.Substring(startIndex, selfClosingIndex + selfClosingElementString.Length - startIndex);
            }
            else if (closeElementIndex >= 0)
            {
                // We're hoping this is an element with a separate close tag, i.e. <option></option>
                const string closeTagString = "</option>";
                int closeTagIndex = sourceText.IndexOf(closeTagString, closeElementIndex);
                if (closeTagIndex >= 0)
                {
                    return sourceText.Substring(startIndex, closeTagIndex + closeTagString.Length - startIndex);
                }
            }

            Console.WriteLine($"{customerCode}: Detected start to item with name \"{this.Name}\", but could not find the element's close tag.");
            return null;
        }
    }
}
