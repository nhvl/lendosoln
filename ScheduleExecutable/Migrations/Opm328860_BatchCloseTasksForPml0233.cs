﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using DataAccess;

    public class Opm328860_BatchCloseTasksForPml0233 : BatchTaskCloser
    {
        private static readonly Guid pml0233BrokerId = new Guid("EAA183AC-6EBD-4072-B4AC-01CB65C50E61");
        private static readonly Guid branchIdFor4400 = new Guid("E8D6F373-CA4B-48AE-A04A-499DFC3A4078");
        ////// ThinhTest2
        ////private static readonly Guid pml0233BrokerId = new Guid("839127ef-72d1-4873-bfdf-18a23416b146");
        ////// 0000001
        ////private static readonly Guid branchIdFor4400 = new Guid("F544FB4E-44D8-4F11-A369-98B79A7E0CD1");

        public Opm328860_BatchCloseTasksForPml0233()
            : base("Opm328860_BatchCloseTasksForPml0233", pml0233BrokerId)
        {
        }

        protected override IEnumerable<string> GetTaskIds()
        {
            var taskIds = new LinkedList<string>();
            var query = @"
                select taskid
                from task t join loan_file_cache c on t.loanid = c.slid
                where t.brokerid = @BrokerId
                    and t.taskiscondition = 0
                    and c.istemplate = 0
                    and taskstatus <> 2
                    and t.taskassigneduserid in
                    (	
                        select employeeuserid
                        from employee
                        where branchid = @BranchId
                    )";
            var parameters = new[]
            {
                new SqlParameter("BrokerId", pml0233BrokerId),
                new SqlParameter("BranchId", branchIdFor4400)
            };

            Action<IDataReader> readerCode = delegate (IDataReader reader)
            {
                while (reader.Read())
                {
                    taskIds.AddLast((string)reader["taskid"]);
                }
            };

            DBSelectUtility.ProcessDBData(pml0233BrokerId, query, null, parameters, readerCode);

            return taskIds;
        }

        public static void CloseTasks(string[] args)
        {
            var migrator = new Opm328860_BatchCloseTasksForPml0233();
            migrator.Migrate();
        }
    }
}
