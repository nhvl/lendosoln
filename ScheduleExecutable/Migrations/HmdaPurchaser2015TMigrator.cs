﻿
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Common.SerializationTypes;
    using LendersOffice.Constants;
    using LqbGrammar.DataTypes;
    using LendersOffice.Drivers.SqlServerDB;
    using System.Data;
using LendersOffice.ObjLib.Rolodex;

namespace ScheduleExecutable.Migrations
{
    class HmdaPurchaser2015TMigrator : Migrator<Guid>
    {
        public HmdaPurchaser2015TMigrator(string name)
            : base(name)
        {
                this.MigrationHelper = new BrokerMigrationHelper(name);
        }

        /// <summary>
        /// Triggers the migration to run.
        /// </summary>
        /// <param name="args">A set of string arguments.</param>
        public static void RunMigration(string[] args)
        {
            var migrator = new HmdaPurchaser2015TMigrator(nameof(HmdaPurchaser2015TMigrator));
            migrator.Migrate();
        }

        protected override bool MigrationFunction(Guid brokerId)
        {
            // Load the investors
            var investors = InvestorRolodexEntry.GetAll(brokerId, null);

            foreach (InvestorRolodexEntry investor in investors)
            {
                HmdaPurchaser2015T value;
                Tools.HmdaPurchaserMapping2004To2015.TryGetValue(investor.HmdaPurchaser2004T, out value);
                investor.HmdaPurchaser2015T = value;
                if(investor.HmdaPurchaser2015T != HmdaPurchaser2015T.LeaveBlank)
                {
                    // For performance reasons, only bother saving the Investor if it's no value is different
                    // from the default.
                    investor.Save();
                }
            }

            return false;
        }
    }
}
