﻿namespace ScheduleExecutable.Migrations
{
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.ObjLib.PMLNavigation;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using static LendersOffice.ObjLib.PMLNavigation.NavigationConfiguration;

    public class GiveAEAndAnyPermissionsToExistingOPSystemPages : Migrator<Guid>
    {
        public GiveAEAndAnyPermissionsToExistingOPSystemPages() : base(nameof(GiveAEAndAnyPermissionsToExistingOPSystemPages))
        {
            this.MigrationHelper = new BrokerMigrationHelper(this.Name);
        }

        public static void Run(string[] args)
        {
            var migrator = new GiveAEAndAnyPermissionsToExistingOPSystemPages();
            migrator.Migrate();
        }

        protected override bool MigrationFunction(Guid brokerId)
        {
            try
            {
                BrokerDB broker = BrokerDB.RetrieveById(brokerId);
                NavigationConfiguration navConfig = broker.GetPmlNavigationConfiguration(null);
                AllowAEAndAnyRoleRecursive(navConfig.RootNode);
                broker.SetPmlNavigationConfiguration(null, navConfig);
                broker.Save();

                return true;
            }
            catch (Exception e)
            {
                string msg = $"[GiveAEAndAnyPermissionsToExistingOPSystemPages] Migration failed for BrokerId=[{brokerId}].";
                Tools.LogError(msg, e);
                return false;
            }
        }

        private void AllowAEAndAnyRoleRecursive(FolderNode node)
        {
            foreach (var childNode in node.Children)
            {
                if (childNode is FolderNode)
                {
                    AllowAEAndAnyRoleRecursive((FolderNode)childNode);
                }
                else if (childNode is PageNode)
                {                    
                    if (SystemNode.BelongsToSystemNode(childNode.Id))
                    {
                        var pageNode = (PageNode)childNode;

                        pageNode.AllowCorrMode = true;
                        pageNode.AllowMiniCorrMode = true;
                        pageNode.AllowBrokerMode = true;
                        pageNode.AllowRetailMode = true;

                        pageNode.AllowLoanOfficer = true;
                        pageNode.AllowProcessor = true;
                        pageNode.AllowSecondary = true;
                        pageNode.AllowPostCloser = true;
                        pageNode.AllowSupervisor = true;
                        pageNode.AllowAccountExecutive = true;
                        pageNode.AllowAny = true;
                    }
                }
            }
        }
    }
}
