﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using DataAccess;
    using LendersOffice.Admin;

    public class PreserveMfaBitSettings_Opm266053 : Migrator<Guid>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PreserveMfaBitSettings_Opm266053"/> class.
        /// </summary>
        /// <param name="name">The name of the migration.</param>
        public PreserveMfaBitSettings_Opm266053(string name)
            : base(name)
        {
            this.MigrationHelper = new BrokerMigrationHelper(name);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PreserveMfaBitSettings_Opm266053"/> class.
        /// </summary>
        /// <param name="name">The name of the migration.</param>
        /// <param name="brokerId">The specific broker to migrate.</param>
        public PreserveMfaBitSettings_Opm266053(string name, Guid brokerId)
            : base(name)
        {
            this.MigrationHelper = new CustomGUIDMigrationHelper(name, () => new Guid[] { brokerId });
        }

        /// <summary>
        /// Migrates brokers to have the opt in MFA bit set to true
        /// and adds the AllowDisableUserMfaBits temp option.
        /// </summary>
        /// <param name="args"></param>
        public static void PreserveMfaBitSettings(string[] args)
        {
            PreserveMfaBitSettings_Opm266053 migrator;
            if (args.Length == 2)
            {
                Guid brokerId = Guid.Parse(args[1]);
                migrator = new PreserveMfaBitSettings_Opm266053("PreserveMfaBitSettings_OPM266053", brokerId);
            }
            else
            {
                migrator = new PreserveMfaBitSettings_Opm266053("PreserveMfaBitSettings_OPM266053");
            }

            migrator.Migrate();
        }

        /// <summary>
        /// The migration function.
        /// This will set the opt in MFA bit to true but also add in the temp bit that allows manually setting the user level MFA bits.
        /// This will preserve the behavior for all users.
        /// </summary>
        /// <param name="id">The broker id to migrate.</param>
        /// <returns>True if the migration is successful. False otherwise.</returns>
        protected override bool MigrationFunction(Guid id)
        {
            try
            {
                BrokerDB broker = BrokerDB.RetrieveById(id);

                bool hasChanged = false;

                string tempOption = "<option name=\"AllowUserMfaToBeDisabled\" value=\"true\" />";
                if (broker.TempOptionXmlContent.Value.IndexOf(tempOption, StringComparison.OrdinalIgnoreCase) == -1)
                {
                    if (broker.TempOptionXmlContent.Value.Contains("</options>"))
                    {
                        broker.TempOptionXmlContent = broker.TempOptionXmlContent.Value.Replace("</options>", $"{tempOption}</options>");
                    }
                    else
                    {
                        broker.TempOptionXmlContent = broker.TempOptionXmlContent + tempOption;
                    }

                    hasChanged = true;
                }

                if (!broker.IsOptInMultiFactorAuthentication)
                {
                    broker.IsOptInMultiFactorAuthentication = true;
                    hasChanged = true;
                }

                if (hasChanged)
                {
                    broker.Save();
                    return true;
                }

                return false;
            }
            catch (Exception e)
            {
                Tools.LogError("Error updating broker <" + id.ToString() + "> for PreserveMfaBitSettings_Opm266053.", e);
            }

            return false;
        }
    }
}
