﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using DataAccess;
    using LendersOffice.QueryProcessor;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Migrates filters in custom reports to replace borrower
    /// SSN fields with indicator fields.
    /// </summary>
    public class Opm468315_MigrateSsnCustomReportFilters : Migrator<Tuple<Guid, Guid>>
    {
        public static void Run(string[] args)
        {
            var migrator = new Opm468315_MigrateSsnCustomReportFilters();
            migrator.Migrate();
        }

        public Opm468315_MigrateSsnCustomReportFilters()
            : base(nameof(Opm468315_MigrateSsnCustomReportFilters))
        {
            this.MigrationHelper = new CustomTupleMigrationHelper<Guid, Guid>(this.Name, this.GetQueryIds, Guid.Parse, Guid.Parse);
        }

        protected override bool MigrationFunction(Tuple<Guid, Guid> brokerIdQueryIdTuple)
        {
            try
            {
                var brokerId = brokerIdQueryIdTuple.Item1;
                var queryId = brokerIdQueryIdTuple.Item2;

                var query = Query.GetQuery(brokerId, queryId);
                var originalQueryXml = query.ToString();

                var wasUpdated = this.MigrateConditions(query.Relates);
                if (wasUpdated)
                {
                    this.MigrationHelper.WriteToBackup(new
                    {
                        BrokerId = brokerId,
                        QueryId = queryId,
                        OriginalXml = originalQueryXml
                    });

                    this.SaveQuery(brokerId, query);
                }

                return true;
            }
            catch (Exception e)
            {
                Tools.LogError($"Failed to migrate query {brokerIdQueryIdTuple.Item2} for broker {brokerIdQueryIdTuple.Item1}", e);
                return false;
            }
        }

        private bool MigrateConditions(Conditions conditions)
        {
            var wasUpdated = false;
            foreach (var condition in conditions)
            {
                if (condition is Conditions)
                {
                    wasUpdated |= MigrateConditions((Conditions)condition);
                }
                else
                {
                    var cond = (Condition)condition;
                    if (string.Equals("aBSsn", cond.Id, StringComparison.OrdinalIgnoreCase))
                    {
                        this.UpdateCondition(cond, isBorrower: true);
                        wasUpdated = true;
                    }
                    else if (string.Equals("aCSsn", cond.Id, StringComparison.OrdinalIgnoreCase))
                    {
                        this.UpdateCondition(cond, isBorrower: false);
                        wasUpdated = true;
                    }
                }
            }

            return wasUpdated;
        }

        private void UpdateCondition(Condition condition, bool isBorrower)
        {
            var isEqualToBlank = condition.Type == E_ConditionType.Eq || condition.Type == E_ConditionType.Bw;
            var checkedValue = isEqualToBlank ? "0" : "1";

            condition.Id = isBorrower ? "aBHasSsn" : "aBHasSpouse";
            condition.Type = E_ConditionType.Eq;
            condition.Argument = new Argument(E_ArgumentType.Const, checkedValue);
        }

        private void SaveQuery(Guid brokerId, Query query)
        {
            SqlParameter[] parameters = {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@QueryId", query.Id),
                new SqlParameter("@XmlContent", query.ToString())
            };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "UpdateReportQuery", 3, parameters);
        }

        private IEnumerable<Tuple<Guid, Guid>> GetQueryIds()
        {
            const string Sql = @"
SELECT r.BrokerId, r.QueryId 
FROM REPORT_QUERY r JOIN Broker b ON r.BrokerId = b.BrokerId
WHERE b.Status = 1";

            var queryIds = new LinkedList<Tuple<Guid, Guid>>();

            Action<IDataReader> readerCode = reader =>
            {
                while (reader.Read())
                {
                    var tuple = Tuple.Create((Guid)reader["BrokerId"], (Guid)reader["QueryId"]);
                    queryIds.AddLast(tuple);
                }
            };

            foreach (var connection in DbConnectionInfo.ListAll())
            {
                DBSelectUtility.ProcessDBData(connection, Sql, TimeoutInSeconds.Thirty, parameters: null, readerCode: readerCode);
            }

            return queryIds;
        }
    }
}
