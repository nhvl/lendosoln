﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.Emailer;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.Email;
    using LendersOffice.Migration;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.Emailer;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Uses the small data layer migration framework to mass migrate loans to the latest system version.
    /// </summary>
    public class SmallDataMigrationBatchRunner : Migrator<Guid>
    {
        /// <summary>
        /// Ignore logging errors for this broker if we run into them.
        /// </summary>
        private readonly HashSet<Guid> ignoreErrorBrokerIds = new HashSet<Guid>()
        {
            new Guid("b8f3b45b-bf88-4739-872e-fab205c660f9")
        };

        /// <summary>
        /// Maps the loan id to broker id. This is to prevent having to call Tools.GetBrokerIdByLoanId a bunch of times.
        /// </summary>
        private Dictionary<Guid, Guid> loanIdsToBroker;

        /// <summary>
        /// Whether we should run these migrations as a system operation.
        /// </summary>
        private bool isSystemMigration = true;

        /// <summary>
        /// The email to send error logs to.
        /// </summary>
        private string targetEmail;

        /// <summary>
        /// String builder for error logging.
        /// </summary>
        private StringBuilder logBuilder;

        /// <summary>
        /// The latest version. Stored here so we don't have to recalc every time.
        /// </summary>
        private LoanVersionT latestVersion;

        /// <summary>
        /// Maps broker id to broker name and customer code.
        /// </summary>
        private Dictionary<Guid, Tuple<string, string>> brokerIdToBrokerNameAndCustomerCode = new Dictionary<Guid, Tuple<string, string>>();

        /// <summary>
        /// Initializes a new instance of the <see cref="SmallDataMigrationBatchRunner"/> class.
        /// </summary>
        /// <param name="name">The name of the migration.</param>
        /// <param name="brokerIds">The brokers to migrate.</param>
        /// <param name="isSystemMigration">Whether we should run these migrations as a system operation.</param>
        /// <param name="isOnlyTemplates">Whether this should only target templates.</param>
        /// <param name="email">The target email</param>
        public SmallDataMigrationBatchRunner(string name, IEnumerable<Guid> brokerIds, bool isSystemMigration, bool? isOnlyTemplates, string email)
            :this(name, MigrationUtils.GetAllLoansWithBrokerIdUsingGivenBrokers(isOnlyTemplates, isValid: true, brokerIds: brokerIds), isSystemMigration, isOnlyTemplates, email)
        {
        }

        /// <summary>
        /// Initializes a instance of the <see cref="SmallDataMigrationBatchRunner"/> class.
        /// </summary>
        /// <param name="name">The name of the migration.</param>
        /// <param name="loanIdsToBrokerId">The loan ids to migrate.</param>
        /// <param name="isSystemMigration">Whether we should run these migrations as a system operation.</param>
        /// <param name="isOnlyTemplates">Whether we only want to target templates with this run. Null = target all, false = target non-templates, true = target templates. Only matters if not passing in list of loans to migrate.</param>
        /// <param name="email">The target email</param>
        public SmallDataMigrationBatchRunner(string name, Dictionary<Guid, Guid> loanIdsToBrokerId, bool isSystemMigration, bool? isOnlyTemplates, string email)
            : base(name)
        {
            this.logBuilder = new StringBuilder();

            if (loanIdsToBrokerId == null)
            {
                Console.WriteLine("About to run MigrationUtils.GetAllLoansWithBrokerId.");
                this.loanIdsToBroker = MigrationUtils.GetAllLoansWithBrokerId(isTemplate: isOnlyTemplates, isValid: true);
                Console.WriteLine("Finished running MigrationUtils.GetAllLoansWithBrokerId.");
            }
            else
            {
                Console.WriteLine("Using the passed in loans list.");
                this.loanIdsToBroker = loanIdsToBrokerId;
            }

            this.latestVersion = LoanDataMigrationUtils.GetLatestVersion();
            this.isSystemMigration = isSystemMigration;
            this.targetEmail = string.IsNullOrEmpty(email) ? ConstStage.EmailForSMFErrorLogs : email;
            this.MigrationHelper = new CustomGUIDMigrationHelper(name, () => this.loanIdsToBroker.Keys);
        }

        /// <summary>
        /// Will attempt to update all loans to the maximum system version. 
        /// When new small data layer migrations are needed for release, all that should be done is to run 'ScheduleExecutable RunSmallDataLayerMigrationOnLoans'.
        /// This update all loans as far as possible.
        /// </summary>
        /// <param name="args">The arguments.</param>
        /// 
        /// <remarks>
        /// If no arguments are provided, then this will migrate every single file. This should really migrate every single loan since the LoanVersionT needs to be updated even if no migration is required for the loan.
        /// This will also treat the migrations as a system migration (sIsSystemBatchMigration == true).
        /// 
        /// These are the parameters that can be provided:
        /// If the migrations should not be considered a system operation (and thus sIsSystemBatchMigration == false for all migrations), pass in the --isSystem option and either 0(false) or 1(true).
        /// ex: RunSmallDataLayerMigrationOnLoans --isSystem 0 => Will set sIsSystemBatchMigration to false for the migrations.
        /// 
        /// To only target specific brokers, use the --brokersFilePath option. This will allow you to pass in a file path to a file that contains a list of broker ids. All files in these brokers will be migrated.
        /// Cannot be used with the --loansFilePath option.
        /// ex: RunSmallDataLayerMigrationOnLoans --brokersFilePath BrokerIds.txt => Will load in broker ids from the BrokerIds.txt file. All files in those brokers will be migrated.
        /// 
        /// If you only want to target templates or non-templates, pass in the --onlyTemplates option and either 0(false) or 1(true).
        /// Not passing in --onlyTemplates will target both templates and non-templates.
        /// Furthermore, if specified to target only templates (--onlyTemplates 1), then the name of the migration will have _Template_ appended to it. Also, extra logging will be done for templates.
        /// This can be used with the --brokersFilePath option.
        /// ex: RunSmallDataLayerMigrationOnLoans --onlyTemplates 0 => Will only migrate non-templates
        ///     RunSmallDataLayerMigrationOnLoans --onlyTemplates 1 => Will only migrate templates
        /// 
        /// The migrator will automatically email the error logs to an email (currently LOsupport@meridianlink.com) after it has finished. To override this, pass in the --email option followed by the target email address.
        /// ex: RunSmallDataLayerMigrationsOnLoan --email you@me.com.
        /// 
        /// For testing purposes, you can provide a file path to a txt file that contains lines in the following format:
        /// [LoanId],[BrokerId]
        /// To pass this in, use the --loansFilePath option and follow that with the filePath. This cannot be used alongside the --brokersFilePath option.
        /// ex: RunSmallDataLayerMigrationOnLoans --loansFilePath MigrationLoans.txt => Will look at the MigratinoLoans.txt file in the same folder as ScheduleExecutable.exe
        /// 
        /// </remarks>
        public static void RunSmallDataLayerMigrationOnLoans(string[] args)
        {
            List<string> arguments = args.ToList();
            if (arguments.Count != 1)
            {
                Dictionary<Guid, Guid> loanIdToBroker = null;
                int filePathOptionIndex = arguments.IndexOf("--loansFilePath");
                if (filePathOptionIndex != -1)
                {
                    int filePathIndex = filePathOptionIndex + 1;
                    if (filePathIndex < arguments.Count)
                    {
                        loanIdToBroker = new Dictionary<Guid, Guid>();
                        var loansToProcess = TextFileHelper.ReadLines(arguments[filePathIndex]); // Should blow up if not a valid file.

                        foreach (string line in loansToProcess)
                        {
                            string[] parts = line.Split(new char[] { ',' }, 2);
                            loanIdToBroker.Add(Guid.Parse(parts[0]), Guid.Parse(parts[1])); // Should blow up if not valid guids.
                        }
                    }
                    else
                    {
                        Console.WriteLine("Please provide a valid file path after the '--loansFilePath' option.");
                        return;
                    }
                }

                HashSet<Guid> brokerIds = null;
                int brokerFilePathOptionIndex = arguments.IndexOf("--brokersFilePath");
                if (filePathOptionIndex != -1 && brokerFilePathOptionIndex != -1)
                {
                    Console.WriteLine("Both --loansFilePath and --brokersFilePath were specified. Please use only one option to select target loans.");
                    return;
                }

                if (brokerFilePathOptionIndex != -1)
                {
                    int brokerIdsFilePathIndex = brokerFilePathOptionIndex + 1;
                    if (brokerIdsFilePathIndex < arguments.Count)
                    {
                        brokerIds = new HashSet<Guid>();
                        var brokerIdsToProcess = TextFileHelper.ReadLines(arguments[brokerIdsFilePathIndex]);

                        foreach (string id in brokerIdsToProcess)
                        {
                            brokerIds.Add(Guid.Parse(id));
                        }
                    }
                    else
                    {
                        Console.WriteLine("Please provide a valid file path after the '--brokersFilePath' option.");
                        return;
                    }
                }


                bool isSystemOperation = true;
                int isSystemOverrideOptionIndex = arguments.IndexOf("--isSystem");
                if (isSystemOverrideOptionIndex != -1)
                {
                    int isSystemArgIndex = isSystemOverrideOptionIndex + 1;
                    if (isSystemArgIndex < arguments.Count)
                    {
                        string isSystemArg = arguments[isSystemArgIndex];
                        if (isSystemArg != "0" && isSystemArg != "1")
                        {
                            Console.WriteLine("Please provide either 0(false) or 1(true) as arguments for the --isSystem option.");
                            return;
                        }
                        else
                        {
                            isSystemOperation = isSystemArg == "1";
                        }
                    }
                    else
                    {
                        Console.WriteLine("Please provide a value indicating whether this should be considered a system migration.");
                        return;
                    }
                }

                bool? onlyTemplates = null;
                int onlyTemplatesIndex = arguments.IndexOf("--onlyTemplates");
                if (onlyTemplatesIndex != -1)
                {
                    int onlyTemplatesArgIndex = onlyTemplatesIndex + 1;
                    if (onlyTemplatesArgIndex < arguments.Count)
                    {
                        string onlyTemplatesArg = arguments[onlyTemplatesArgIndex];
                        if (onlyTemplatesArg != "0" && onlyTemplatesArg != "1")
                        {
                            Console.WriteLine("Please provide either 0(false) or 1(true) as arguments for the --onlyTemplates option.");
                            return;
                        }
                        else
                        {
                            onlyTemplates = onlyTemplatesArg == "1";
                        }
                    }
                    else
                    {
                        Console.WriteLine("Please provide a value indicating whether this migration should only target templates or non-templates. Or remove the --onlyTemplates option to target all files regardless of template status.");
                        return;
                    }
                }

                string targetEmail = null;
                int emailOptionIndex = arguments.IndexOf("--email");
                if (emailOptionIndex != -1)
                {
                    int emailIndex = emailOptionIndex + 1;
                    if (emailIndex < arguments.Count)
                    {
                        string emailAddress = arguments[emailIndex];
                        var email = LqbGrammar.DataTypes.EmailAddress.Create(emailAddress);
                        if (!email.HasValue)
                        {
                            Console.WriteLine("Please provide a valid email address.");
                            return;
                        }

                        targetEmail = emailAddress;
                    }
                    else
                    {
                        Console.WriteLine("Please provide a valid email address if using the --email option.");
                        return;
                    }
                }

                if (onlyTemplates.HasValue && !onlyTemplates.Value && brokerIds == null && loanIdToBroker == null)
                {
                    Console.WriteLine("--onlyTemplates 0 was specified but no specific loans or brokers were passed in. This will migrate all non-templates, which is not allowed yet.");
                    return;
                }

                // There are unexpected arguments in here.
                if (isSystemOverrideOptionIndex == -1 && filePathOptionIndex == -1 && onlyTemplatesIndex == -1 && brokerFilePathOptionIndex == -1)
                {
                    Console.WriteLine("Please provide options to these arguments. Formatting is in the remarks for this method.");
                    return;
                }

                string isTemplateName = onlyTemplates.HasValue && onlyTemplates.Value ? "TemplatesOnly_" : string.Empty;
                Console.WriteLine($"isTemplateName <{isTemplateName}>, isSystemMigration <{isSystemOperation}>");

                SmallDataMigrationBatchRunner runner;
                if (brokerIds != null)
                {
                    Console.WriteLine("Running SmallDataMigrationBatchRunner using specified brokerIds.");
                    runner = new SmallDataMigrationBatchRunner("SmallDataMigrationBatchRunner_" + isTemplateName + DateTime.Now.ToString("M_d_yyyy"), brokerIds, isSystemMigration: isSystemOperation, isOnlyTemplates: onlyTemplates, email: targetEmail);
                }
                else
                {
                    string loanIdToBrokerCount = "null";
                    if (loanIdToBroker != null)
                    {
                        loanIdToBrokerCount = loanIdToBroker.Count.ToString();
                    }

                    Console.WriteLine($"Running SmallDataMigrationBatchRunner without using brokers. loanIdToBroker.Count <{loanIdToBrokerCount}>");
                    runner = new SmallDataMigrationBatchRunner("SmallDataMigrationBatchRunner_" + isTemplateName + DateTime.Now.ToString("M_d_yyyy"), loanIdToBroker, isSystemMigration: isSystemOperation, isOnlyTemplates: onlyTemplates, email: targetEmail);
                }

                runner.Migrate();
                return;
            }

            Console.WriteLine("Running the migrations on all files in the system is disabled for now.");
        }

        /// <summary>
        /// Performs the migrations. 
        /// </summary>
        /// <param name="id">The loan id.</param>
        /// <returns>True if all migrations were successful, false otherwise.</returns>
        protected override bool MigrationFunction(Guid id)
        {
            Guid brokerId = Guid.Empty;
            if (!this.loanIdsToBroker.TryGetValue(id, out brokerId) || brokerId == Guid.Empty)
            {
                this.logBuilder.AppendLine("No broker id found for loan id <" + id.ToString() + ">. Skipping");
                return false;
            }

            try
            {
                LoanDataMigrationsRunner migrationRunner = new LoanDataMigrationsRunner("System", Guid.Empty, id, brokerId, isSystemBatchMigration: this.isSystemMigration);
                LoanVersionT? failedVersion;
                bool success = migrationRunner.ApplyMigrations(this.latestVersion, shouldSaveAtEnd: true, canUpdateToMax: true, failedMigrationNumber: out failedVersion);

                if (!success && !this.ignoreErrorBrokerIds.Contains(brokerId))
                {
                    string loanNum = Tools.GetLoanNameByLoanId(brokerId, id);
                    Tuple<string, string> brokerNameAndCustomerCode = this.GetBrokerNameAndCustomerCode(brokerId);

                    string brokerName = brokerNameAndCustomerCode?.Item1 ?? string.Empty;
                    string customerCode = brokerNameAndCustomerCode?.Item2 ?? string.Empty;

                    // We also don't want to log if the customer code begins with ILS.
                    if (!customerCode.StartsWith("ILS"))
                    {
                        this.logBuilder.AppendLine($"File {loanNum} could not be migrated to version {failedVersion.Value.ToString()}. ID: <{id.ToString()}> CustomerCode: <{customerCode}> BrokerName: <{brokerName}>");
                    }

                    return false;
                }

                return success;
            }
            catch (Exception e) 
            {
                Tuple<string, string> brokerNameAndCustomerCode = this.GetBrokerNameAndCustomerCode(brokerId);
                string customerCode = brokerNameAndCustomerCode?.Item2 ?? string.Empty;
                if (!ignoreErrorBrokerIds.Contains(brokerId) && !customerCode.StartsWith("ILS"))
                {
                    this.logBuilder.AppendLine($"Unknown error during migrations. Please search PB for details. ID: <{id.ToString()}> CustomerCode: <{customerCode}>");
                    Tools.LogError(this.Name + ": Unknown error during migrations. ID: <{id.ToString()}> CustomerCode: <{customerCode}> BrokerId: <{brokerId}>.", e);
                }

                return false;
            }
        }

        /// <summary>
        /// Gets the broker name and customer code for the broker id and stores it for future use.
        /// </summary>
        /// <param name="brokerId">The broker id.</param>
        /// <returns>A tuple. Item 1 has broker name, Item 2 has customer code.</returns>
        private Tuple<string, string> GetBrokerNameAndCustomerCode(Guid brokerId)
        {
            Tuple<string, string> brokerNmAndCc;
            if (this.brokerIdToBrokerNameAndCustomerCode.ContainsKey(brokerId))
            {
                brokerNmAndCc = this.brokerIdToBrokerNameAndCustomerCode[brokerId];
            }
            else
            {
                brokerNmAndCc = BrokerDB.RetrieveNameAndCustomerCodeFromDb(brokerId);
                brokerIdToBrokerNameAndCustomerCode.Add(brokerId, brokerNmAndCc);
            }

            return brokerNmAndCc;
        }

        /// <summary>
        /// Logs any errors to a file and attempts to email it.
        /// </summary>
        protected override void EndMigration()
        {
            logBuilder.AppendLine();
            var errorText = logBuilder.ToString();
            var name = this.Name + "_errorLog";
            TextFileHelper.AppendString(name + ".txt", errorText);
            logBuilder.Length = 0;

            EmailServerName? server = LqbGrammar.DataTypes.EmailServerName.Create(ConstStage.SmtpServer);
            PortNumber? port = LqbGrammar.DataTypes.PortNumber.Create(ConstStage.SmtpServerPortNumber);
            EmailAddress? from = EmailAddress.Create(ConstStage.DefaultDoNotReplyAddress);
            EmailSubject? subject = EmailSubject.Create(name).Value;
            EmailAddress? to = EmailAddress.Create(this.targetEmail).Value;

            if (server == null || from == null || subject == null || to == null || port == null)
            {
                // Just give up. We have the logs in a txt file anyways.
                return;
            }

            EmailTextBody body = new EmailTextBody();
            var package = new EmailPackage(ConstAppDavid.SystemBrokerGuid, from.Value, subject.Value, body, to.Value);
            package.AddAttachment(TypedResource<string>.CreateText(errorText, System.Net.Mime.MediaTypeNames.Text.Plain, name + ".txt"));
            EmailHelper.SendEmail(server.Value, port.Value, package);
        }
    }
}
