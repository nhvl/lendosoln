﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using DataAccess;
    using LqbGrammar.DataTypes;

    public class MigrateVorRecordsToUlad : Migrator<Guid>
    {
        public MigrateVorRecordsToUlad()
            :base(nameof(MigrateVorRecordsToUlad))
        {
            this.MigrationHelper = new CustomGUIDMigrationHelper(nameof(MigrateVorRecordsToUlad), this.IDRetrievalFunction);
        }

        public static void Run(string[] args)
        {
            Console.WriteLine("Are you sure you want to migrate VOR Records?");
            Console.WriteLine("Please enter y or n");

            string input = Console.ReadLine();
            if (!input.Equals("y", StringComparison.OrdinalIgnoreCase))
            {
                return;
            }

            Console.WriteLine();

            var migrator = new MigrateVorRecordsToUlad();
            migrator.Migrate();
        }

        protected override bool MigrationFunction(Guid loanId)
        {
            var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(MigrateVorRecordsToUlad));
            loan.InitSave();

            if (loan.sBorrowerApplicationCollectionT == E_sLqbCollectionT.Legacy)
            {
                // There isn't anything for us to do.
                return true;
            }
            else if (loan.VorRecords.Any())
            {
                var message = "Skipping loan that already has records in VOR_RECORD table: " + loanId.ToString();
                Console.WriteLine(message);
                Tools.LogInfo(message);
                return true;
            }

            try
            {
                loan.MigrateVorRecordsToUladDataLayer();
                loan.Save();
            }
            catch (Exception e)
            {
                Console.WriteLine("Encountered error migrating file: " + loanId.ToString());
                Tools.LogError(e);
                return false;
            }

            return true;
        }

        private IEnumerable<Guid> IDRetrievalFunction()
        {
            string sql = @"
SELECT lf_a.sLId
FROM LOAN_FILE_A lf_a WITH(NOLOCK) 
    JOIN APPLICATION_B app_b WITH(NOLOCK) on lf_a.sLId = app_b.sLId
WHERE 
    IsValid = 1
    AND sBorrowerApplicationCollectionT IS NOT NULL
    AND sBorrowerApplicationCollectionT <> 0
    AND datalength(aVorXmlContent) <> 0";

            var loanIds = new HashSet<Guid>();

            Action<IDataReader> readLoanIds = reader =>
            {
                while(reader.Read())
                {
                    loanIds.Add((Guid)reader["sLId"]);
                }
            };

            foreach (var connInfo in DbConnectionInfo.ListAll())
            {
                DBSelectUtility.ProcessDBData(connInfo, sql, TimeoutInSeconds.Sixty, null, readLoanIds);
            }

            return loanIds;
        }
    }
}
