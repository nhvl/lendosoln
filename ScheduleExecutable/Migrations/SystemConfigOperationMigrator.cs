﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ConfigSystem;
    using DataAccess;
    using LendersOffice.ConfigSystem;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Constants;
    using LendersOffice.Security;

    /// <summary>
    /// Provides a base implementation for adding new operations to the System Config workflow.
    /// </summary>
    public abstract class SystemConfigOperationMigrator
    {
        /// <summary>
        /// The system config broker id.
        /// </summary>
        private Guid systemBrokerId = ConstAppDavid.SystemBrokerGuid;

        /// <summary>
        /// Runs the specified <see cref="SystemConfigOperationMigrator"/> subclass
        /// to add operations to the System Config workflow.
        /// </summary>
        /// <param name="args">The command line arguments.</param>
        public static void AddOperationToSystemConfig(string[] args)
        {
            if (args.Length < 2)
            {
                Console.WriteLine("Usage: ScheduleExecutable AddOperationToSystemConfig {Fully_Specified_Migrator_Type_Name}");
                return;
            }

            var fullyQualifiedMigratorType = args[1];
            var instance = Activator.CreateInstance(Type.GetType(fullyQualifiedMigratorType), args.Skip(2).ToArray()) as SystemConfigOperationMigrator;

            if (instance == null)
            {
                Console.WriteLine($"Unable to locate migrator with the provided type name.");
            }
            else
            {
                instance.AddOperations();
            }
        }

        /// <summary>
        /// Gets the new operations that will be added to the system config.
        /// </summary>
        /// <remarks>
        /// The operations are required to have unique ids and will only
        /// be added if they are not already defined.
        /// </remarks>
        /// <returns>The new operations that will be added to the system config.</returns>
        protected abstract IEnumerable<OperationAddition> GetNewOperations();

        /// <summary>
        /// Adds the operations to the system config and publishes the updated content. 
        /// NOTE: this will wipe out any existing draft for the system config.
        /// </summary>
        internal void AddOperations()
        {
            var workflow = new WorkflowSystemConfigModel(this.systemBrokerId, loadDraft: false);
            ConditionSet privileges = workflow.GetFullConditionSet();

            IEnumerable<OperationAddition> newOperations = this.GetOperationsToAdd(privileges);
            this.VerifyAllOperationIdsUnique(newOperations);
            this.AddOperations(privileges, newOperations);

            workflow.SwitchToDraft(); 
            workflow.Save();
            try
            {
                WorkflowSystemConfigModel.Publish(this.systemBrokerId, SystemUserPrincipal.TaskSystemUser);
            }
            catch (ConfigValidationException e)
            {
                Tools.LogError(e.ToString(), e);
                throw;
            }
        }

        /// <summary>
        /// Gets the operations to add, filtering out those which are already
        /// defined in the system config.
        /// </summary>
        /// <param name="privileges">The privileges condition set from the system config.</param>
        /// <returns>The operations to add.</returns>
        private IEnumerable<OperationAddition> GetOperationsToAdd(ConditionSet privileges)
        {
            return this.GetNewOperations()
                .Where(o => !privileges.Any(p => p.SystemOperationNames.Contains(o.Operation.Id)));
        }

        /// <summary>
        /// Verifies that all operations to be added have a unique id.
        /// </summary>
        /// <param name="operations">The operations to check.</param>
        /// <exception cref="CBaseException">Thrown if multiple operations have the same id.</exception>
        private void VerifyAllOperationIdsUnique(IEnumerable<OperationAddition> operations)
        {
            bool entriesUnique = operations.GroupBy(o => o.Operation.Id).All(g => g.Count() == 1);
            if (!entriesUnique)
            {
                throw CBaseException.GenericException("Cannot add multiple operations with the same ID.");
            }
        }

        /// <summary>
        /// Adds the operations to the condition set.
        /// </summary>
        /// <param name="privileges">The condition set.</param>
        /// <param name="operations">The operations to add.</param>
        private void AddOperations(ConditionSet privileges, IEnumerable<OperationAddition> operations)
        {
            foreach (var operation in operations)
            {
                Condition privilege = this.CreateCondition(operation);
                privileges.AddCondition(privilege);
            }
        }

        /// <summary>
        /// Creates a condition from the operation to add.
        /// </summary>
        /// <param name="newOperation">The operation to convert to a condition.</param>
        /// <returns>A condition.</returns>
        private Condition CreateCondition(OperationAddition newOperation)
        {
            var newPrivilege = new ConfigSystem.Condition();
            var newParameter = this.GetDefaultParameter(newOperation.DefaultValue);
            newPrivilege.ParameterSet.AddParameter(newParameter);
            newPrivilege.SysOpSet.Add(new SystemOperation(newOperation.Operation));
            newPrivilege.Notes = $"Created automatically by LendingQB. {newOperation.Notes}";
            newPrivilege.FailureMessage = newOperation.FailureMessage;
            return newPrivilege;
        }

        /// <summary>
        /// Gets a parameter for the AlwaysTrue field.
        /// </summary>
        /// <param name="defaultValue">
        /// The value to check AlwaysTrue against. This controls the default behavior
        /// for the operation. Passing true will allow the operation to pass checks
        /// by default. Passing false will prevent the operation.
        /// </param>
        /// <returns>The parameter with the specified default value.</returns>
        private Parameter GetDefaultParameter(bool defaultValue)
        {
            var newParameter = new Parameter("AlwaysTrue", E_FunctionT.Equal, E_ParameterValueType.Bool, false, false);
            if (defaultValue)
            {
                newParameter.AddValue("True", "True");
            }
            else
            {
                newParameter.AddValue("False", "False");
            }

            return newParameter;
        }

        /// <summary>
        /// Represents the information needed to add an operation to a workflow config.
        /// </summary>
        protected struct OperationAddition
        {
            /// <summary>
            /// The operation that will be added.
            /// </summary>
            public readonly WorkflowOperation Operation;

            /// <summary>
            /// A value indicating whether the operation should be allowed or 
            /// prevented by default.
            /// </summary>
            public readonly bool DefaultValue;

            /// <summary>
            /// Notes for the rule that will be added.
            /// </summary>
            /// <remarks>A case number can be helpful here.</remarks>
            public readonly string Notes;

            /// <summary>
            /// The message to display if the operation is not allowed.
            /// </summary>
            public readonly string FailureMessage;

            /// <summary>
            /// Initializes a new instance of the <see cref="OperationAddition"/> struct.
            /// </summary>
            /// <param name="operation">The operation to add.</param>
            /// <param name="defaultValue">Whether the operation can be performed by default.</param>
            /// <param name="notes">The notes for the condition.</param>
            /// <param name="failureMessage">The message when the operation is not allowed.</param>
            public OperationAddition(WorkflowOperation operation, bool defaultValue, string notes, string failureMessage)
            {
                this.Operation = operation;
                this.DefaultValue = defaultValue;
                this.Notes = notes;
                this.FailureMessage = failureMessage;
            }
        }
    }

    /*
    /// <summary>
    /// A migration to add the loan status operations from case 236301 to workflow.
    /// </summary>
    /// <remarks>
    /// This migration was already run on production without using this class.
    /// It is included here as an example of how to add an operation to the 
    /// System Config by inheriting from SystemConfigOperationMigrator.
    /// There is code in ScheduleExecutable that would allow this to be run as:
    /// ScheduleExecutable.exe AddOperationToSystemConfig ScheduleExecutable.Migrations.LoanStatusChangePrivileges
    /// </remarks>
    public class LoanStatusChangePrivileges : SystemConfigOperationMigrator
    {
        /// <summary>
        /// Runs the migration. Commented out to prevent re-running, this is just
        /// meant to be an example.
        /// </summary>
        /// <param name="args">The command line arguments.</param>
        public static void Run(string[] args)
        {
            var privilegeAdder = new LoanStatusChangePrivileges();
            privilegeAdder.AddOperations();
        }

        /// <summary>
        /// Gets the operations to add.
        /// </summary>
        /// <returns>The operations to add.</returns>
        protected override IEnumerable<OperationAddition> GetNewOperations()
        {
            return new[]
            {
                new OperationAddition(
                    WorkflowOperations.LoanStatusChangeToConditionReview,
                    defaultValue: false,
                    notes: "OPM 236301",
                    failureMessage: "You do not have permission to force loan status to Condition Review."),
                new OperationAddition(
                    WorkflowOperations.LoanStatusChangeToDocumentCheck,
                    defaultValue: false,
                    notes: "OPM 236301",
                    failureMessage: "You do not have permission to force loan status to Document Chack.")
            };
        }
    }
    */
}
