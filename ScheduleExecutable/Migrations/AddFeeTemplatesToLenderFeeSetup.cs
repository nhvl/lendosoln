﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using LendersOffice.Admin;

    public class AddFeeTemplatesToLenderFeeSetup : Migrator<Guid>
    {
        public AddFeeTemplatesToLenderFeeSetup(MigrationHelper<Guid> migrationHelper)
            : base(migrationHelper.Name)
        {
            this.MigrationHelper = migrationHelper;
        }

        public static void AddFeeTemplatesToFeeSetup(string[] args)
        {
            MigrationHelper<Guid> brokerMigrationHelper = new BrokerMigrationHelper(nameof(AddFeeTemplatesToLenderFeeSetup));
            var migrator = new AddFeeTemplatesToLenderFeeSetup(brokerMigrationHelper);
            migrator.Migrate();
        }

        protected override bool MigrationFunction(Guid id)
        {
            BrokerDB lender = null;
            try
            {
                lender = BrokerDB.RetrieveById(id);
                DataAccess.FeeSetupClosingCostSet closingCostSet = lender.GetUnlinkedClosingCostSet();
                bool feesAdded = false;
                foreach (DataAccess.FeeSetupClosingCostFee fee in DataAccess.DefaultSystemClosingCostFee.FeeTemplate.SystemFeeList)
                {
                    if (closingCostSet.FindFeeByTypeId(fee.ClosingCostFeeTypeId) == null)
                    {
                        closingCostSet.AddOrUpdate(fee);
                        feesAdded = true;
                    }
                }

                if (feesAdded || closingCostSet.Mismo34ImportMappings == null)
                {
                    closingCostSet.SetMismoImportMappings(DataAccess.DefaultSystemClosingCostFee.FeeTemplate.DefaultMismoImportFeeTypeMappings);
                    lender.SetClosingCostSet(closingCostSet, validate: true);
                    lender.Save();
                }

                return true;
            }
            catch (Exception exception)
            {
                DataAccess.Tools.LogWarning($"Unable to apply migration {this.Name} to lender with Broker ID: '{id}'; CustomerCode: '{lender?.CustomerCode}'", exception);
                return false;
            }
        }
    }
}
