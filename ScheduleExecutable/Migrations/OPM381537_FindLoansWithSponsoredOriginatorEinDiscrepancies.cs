﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using DataAccess;
    using LendersOffice.Migration;

    /// <summary>
    /// Determines if there are loans in the system with different values for
    /// the <see cref="CPageBase.sFHASponsoredOriginatorEIN"/> and 
    /// <see cref="CPageBase.sSponsoredOriginatorEIN"/> fields.
    /// </summary>
    public class OPM381537_FindLoansWithSponsoredOriginatorEinDiscrepancies : Migrator<Guid>
    {
        /// <summary>
        /// Prevents a default instance of the <see cref="OPM381537_FindLoansWithSponsoredOriginatorEinDiscrepancies"/>
        /// class from being created.
        /// </summary>
        private OPM381537_FindLoansWithSponsoredOriginatorEinDiscrepancies() : base(nameof(OPM381537_FindLoansWithSponsoredOriginatorEinDiscrepancies))
        {
            this.MigrationHelper = new CustomGUIDMigrationHelper(nameof(OPM381537_FindLoansWithSponsoredOriginatorEinDiscrepancies), this.RetrieveLoanIds);
        }

        /// <summary>
        /// Runs the migration.
        /// </summary>
        /// <param name="args">
        /// This argument is not used.
        /// </param>
        public static void Run(string[] args)
        {
            var migrator = new OPM381537_FindLoansWithSponsoredOriginatorEinDiscrepancies();
            migrator.Migrate();
        }

        /// <summary>
        /// Loads the loan with the specified <paramref name="loanId"/>
        /// to determine if there is a discrepancy between <see cref="CPageBase.sFHASponsoredOriginatorEIN"/>
        /// and <see cref="CPageBase.sSponsoredOriginatorEIN"/>.
        /// </summary>
        /// <param name="loanId">
        /// The id of the loan to load.
        /// </param>
        /// <returns>
        /// True if the check was successful, false otherwise.
        /// </returns>
        protected override bool MigrationFunction(Guid loanId)
        {
            try
            {
                var dataloan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(OPM381537_FindLoansWithSponsoredOriginatorEinDiscrepancies));
                dataloan.InitLoad();

                var hasDiscrepancy = !string.IsNullOrEmpty(dataloan.sFHASponsoredOriginatorEIN) &&
                    !string.IsNullOrEmpty(dataloan.sSponsoredOriginatorEIN) &&
                    !string.Equals(dataloan.sFHASponsoredOriginatorEIN, dataloan.sSponsoredOriginatorEIN, StringComparison.Ordinal);

                if (hasDiscrepancy)
                {
                    this.MigrationHelper.WriteToBackup(new
                    {
                        LoanId = dataloan.sLId,
                        LoanNumber = dataloan.sLNm,
                        sFHASponsoredOriginatorEIN = dataloan.sFHASponsoredOriginatorEIN,
                        sSponsoredOriginatorEIN = dataloan.sSponsoredOriginatorEIN
                    });
                }

                return true;
            }
            catch (Exception exc)
            {
                var message = $"Could not determine if loan {loanId} has FHA sponsored originator EIN discrepancy, skipping: {exc}";

                Console.WriteLine(message);
                Tools.LogError(message);

                return false;
            }
        }

        /// <summary>
        /// Obtains the list of loan ids to check.
        /// </summary>
        /// <returns>
        /// The list of loan ids to check.
        /// </returns>
        private IEnumerable<Guid> RetrieveLoanIds()
        {
            const string Sql = @"
SELECT lfc.sLId
FROM LOAN_FILE_CACHE lfc JOIN LOAN_FILE_F lff ON lfc.sLId = lff.sLId
WHERE lfc.sBrokerId = @BrokerId
AND lfc.IsValid = 1
AND lfc.sLoanFileT = 0
AND lfc.IsTemplate = 0
AND datalength(lff.sFHASponsoredOriginatorEIN) > 0
";

            var loanIds = new LinkedList<Guid>();

            Action<IDataReader> readerCode = reader =>
            {
                while (reader.Read())
                {
                    loanIds.AddLast((Guid)reader["sLId"]);
                }
            };

            foreach (var brokerId in Tools.GetAllActiveBrokers())
            {
                SqlParameter[] parameters =
                {
                    new SqlParameter("@BrokerId", brokerId)
                };

                DBSelectUtility.ProcessDBData(brokerId, Sql, LqbGrammar.DataTypes.TimeoutInSeconds.Sixty, parameters, readerCode);
            }

            return loanIds;
        }
    }
}
