﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;

    public class RowBasedEncryptionGenericTableMigrator
    {
        /// <summary>
        /// Runs the migration on each Lender database.
        /// </summary>
        /// <param name="table">The table to migrate.</param>
        public static void MigrateLenderTable(TableToEncrypt table)
        {
            foreach (var connectionInfo in DataAccess.DbConnectionInfo.ListAll())
            {
                MigrateTable(new DbConnectionInfoReference(connectionInfo), table);
            }
        }

        /// <summary>
        /// Migrates the specified table on the specified database.
        /// </summary>
        /// <param name="source">The data source to connect.</param>
        /// <param name="table">The table to migrate.</param>
        public static void MigrateTable(DataAccess.DataSrc source, TableToEncrypt table)
        {
            MigrateTable(new DbConnectionInfoReference(source), table);
        }

        private static void MigrateTable(DbConnectionInfoReference connectionInfo, TableToEncrypt table)
        {
            string fileName = "BackupForMigration_TABLE_" + table.TableName + "_DB_" + connectionInfo.Id + ".txt";
            var primaryKeysForRows = SelectPrimaryKeysToMigrate(connectionInfo, table);

            ConsoleAndLog($"Found {primaryKeysForRows.Count} rows to migrate on {GetDescription(connectionInfo, table)}.");
            if (primaryKeysForRows.Count == 0)
            {
                return;
            }

            Console.WriteLine($"Encrypted backups will be written to {fileName}");
            Console.WriteLine("The console will output progress at each 10% it reaches.");

            var markerIndices = new HashSet<int>(Enumerable.Range(1, 10).Select(i => primaryKeysForRows.Count * i / 10).Concat(new[] { primaryKeysForRows.Count }));
            int processedCount = 0;
            int errorCount = 0;
            foreach (Dictionary<string, object> primaryKey in primaryKeysForRows)
            {
                try
                {
                    TableRowToEncrypt tableRow = SelectRow(connectionInfo, table, primaryKey);
                    if (tableRow == null)
                    {
                        DataAccess.Tools.LogWarning(
                            "A record was found in the initial selection, but not the detailed selection, suggesting it was migrated externally.  The record is identified by " + GetDescription(connectionInfo, table, primaryKey) + ".");
                        continue;
                    }

                    BackupDataToFile(fileName, tableRow);
                    UpdateRowToBeEncrypted(connectionInfo, table, tableRow);
                }
                catch (Exception e)
                {
                    errorCount += 1;
                    DataAccess.Tools.LogError("Unable to migrate " + GetDescription(connectionInfo, table, primaryKey) + ".", e);
                }

                if (markerIndices.Contains(++processedCount))
                {
                    ConsoleAndLog("Processed " + processedCount + " records of " + primaryKeysForRows.Count + " total. So far, " + errorCount + " had errors.");
                }
            }
        }

        private static List<Dictionary<string, object>> SelectPrimaryKeysToMigrate(DbConnectionInfoReference connectionInfo, TableToEncrypt table)
        {
            var primaryKeyList = new List<Dictionary<string, object>>();
            connectionInfo.RunSelectStatement(
                table.SelectStatementForKeys,
                TimeoutInSeconds.Default,
                null,
                reader =>
                {
                    while (reader.Read())
                    {
                        var primaryKey = new Dictionary<string, object>(table.PrimaryKeyColumns.Count);
                        foreach (var column in table.PrimaryKeyColumns)
                        {
                            primaryKey.Add(column.ColumnName, reader[column.ColumnName]);
                        }

                        primaryKeyList.Add(primaryKey);
                    }
                });

            return primaryKeyList;
        }

        private static TableRowToEncrypt SelectRow(DbConnectionInfoReference connectionInfo, TableToEncrypt table, Dictionary<string, object> primaryKey)
        {
            TableRowToEncrypt tableRowToMigrate = null;
            connectionInfo.RunSelectStatement(
                table.SelectStatementForRecord,
                TimeoutInSeconds.Default,
                primaryKey.Select(kvp => CreateSqlParameter(kvp.Key, kvp.Value)),
                reader =>
                {
                    if (reader.Read())
                    {
                        var columnsToMigrate = new Dictionary<string, Lazy<string>>(table.Columns.Count);
                        foreach (var column in table.Columns)
                        {
                            columnsToMigrate.Add(column.TargetColumnName, column.ReadValue(reader, primaryKey));
                        }

                        tableRowToMigrate = new TableRowToEncrypt
                        {
                            PrimaryKey = primaryKey,
                            ColumnsToMigrate = columnsToMigrate,
                        };
                    }
                });
            return tableRowToMigrate;
        }

        private static void BackupDataToFile(string fileName, TableRowToEncrypt tableRow)
        {
            LendersOffice.Drivers.Gateways.TextFileHelper.OpenForAppend(fileName, textWriter =>
            {
                textWriter.AppendLine(SerializationHelper.JsonNetAnonymousSerialize(tableRow.PrimaryKey));
                textWriter.AppendLine(EncryptionHelper.Encrypt(SerializationHelper.JsonNetAnonymousSerialize(tableRow.ColumnsToMigrate)));
            });
        }

        private static void UpdateRowToBeEncrypted(DbConnectionInfoReference connectionInfo, TableToEncrypt table, TableRowToEncrypt tableRow)
        {
            var parameters = new List<SqlParameter>(tableRow.PrimaryKey.Count + tableRow.ColumnsToMigrate.Count + 1);
            foreach (var primaryKey in tableRow.PrimaryKey)
            {
                parameters.Add(CreateSqlParameter(primaryKey.Key, primaryKey.Value));
            }

            var encryptionKeyId = EncryptionHelper.GenerateNewKey();
            parameters.Add(CreateSqlParameter("@EncryptionKeyId", encryptionKeyId.Value));
            foreach (var columnToMigrate in tableRow.ColumnsToMigrate)
            {
                parameters.Add(CreateSqlParameter(columnToMigrate.Key, EncryptionHelper.EncryptString(encryptionKeyId, columnToMigrate.Value.Value) ?? new byte[0]));
            }

            var rowCount = connectionInfo.RunUpdateStatement(
                table.UpdateStatement,
                TimeoutInSeconds.Default,
                parameters);
            if (rowCount.Value != 1)
            {
                DataAccess.Tools.LogWarning("Unexpected row count of " + rowCount.Value + " when migrating " + GetDescription(connectionInfo, table, tableRow.PrimaryKey));
            }
        }

        private static SqlParameter CreateSqlParameter(string name, object value)
        {
            var parameter = new SqlParameter(name, value);
            if (value is string)
            {
                // This allows us to avoid costly query plans containing things like:
                // CONVERT_IMPLICIT(nvarchar(7),[LoDev].[dbo].[GENERIC_FRAMEWORK_VENDOR_BROKER_CONFIG].[ProviderID],0)=[@ProviderID]
                // Without this line, this would happen, as SqlParameter defaults string values to SqlDbType.NVarChar.  For stored procedures,
                // the parameters are typed, so conversion to the appropriate type happens during initialization instead of for every row.
                // This code is dynamic, however, and so needs to specify the SQL type.
                // If we ever need to handle an actual NVarChar value, we may need to make real logic.
                parameter.SqlDbType = SqlDbType.VarChar;
            }

            return parameter;
        }

        private static void ConsoleAndLog(string msg)
        {
            Console.WriteLine(msg);
            DataAccess.Tools.LogInfo(msg);
        }

        private static string GetDescription(DbConnectionInfoReference connectionInfo, TableToEncrypt table, Dictionary<string, object> primaryKey = null)
        {
            return (primaryKey != null ? "row " + SerializationHelper.JsonNetAnonymousSerialize(primaryKey) + " in " : null)
                + "table " + table.TableName + " in DB " + connectionInfo.Description;
        }

        /// <summary>
        /// This code is not supposed to be called live from any code, but it is handy for generating the SQL statements
        /// from a partially filled out <see cref="TableToEncrypt"/> class.  We don't like dynamic SQL, but we can generate it
        /// during development time and copy it over from here.
        /// </summary>
        /// <param name="table">The partially completed table class.</param>
        /// <returns>The C# statements to set the SQL statements for <paramref name="table"/>.</returns>
        /// <remarks>The obsolete attribute is here to ensure any code that calls this live generates a warning.</remarks>
        [Obsolete]
        public static string GenerateStatements(TableToEncrypt table)
        {
            var primaryKeyColumns = table.PrimaryKeyColumns.Select(c => c.ColumnName);
            var recordWriteColumns = table.Columns.Select(c => c.TargetColumnName);
            string commaNewline = "," + Environment.NewLine + "    ";
            string whereClause = "WHERE " + string.Join(" AND ", table.PrimaryKeyColumns.Select(c => c.ColumnName + " = @" + c.ColumnName));

            return $"{nameof(TableToEncrypt.SelectStatementForKeys)} = @\"SELECT {string.Join(", ", primaryKeyColumns)} FROM {table.TableName}\",{Environment.NewLine}"
                + $@"{nameof(TableToEncrypt.SelectStatementForRecord)} = @""SELECT {
                    string.Join(
                        commaNewline,
                        new[] { "EncryptionKeyId" }.Concat(primaryKeyColumns).Concat(table.Columns.Select(c => c.ExistingColumnName)))}{Environment.NewLine
                }FROM {table.TableName}{Environment.NewLine
                }{whereClause}"",{Environment.NewLine}"
                + $@"{nameof(TableToEncrypt.UpdateStatement)} = @""UPDATE {table.TableName}{Environment.NewLine
                }SET {
                    string.Join(
                        commaNewline,
                        new[] { "EncryptionKeyId" }.Concat(table.Columns.Select(c => c.TargetColumnName)).Select(c => c + " = @" + c))}{Environment.NewLine
                }{whereClause}"",";
        }

        public class TableToEncrypt
        {
            public string TableName { get; set; }

            /// <summary>
            /// A select statement for the columns from <see cref="PrimaryKeyColumns"/> for all rows that need migration.
            /// </summary>
            public string SelectStatementForKeys { get; set; }

            /// <summary>
            /// A select statement for the data from <see cref="Columns"/> for a single row that needs to be migrated.  The
            /// where clause should identify a single row via <see cref="PrimaryKeyColumns"/>. 
            /// </summary>
            public string SelectStatementForRecord { get; set; }

            /// <summary>
            /// The update statement for an individual row.  The updates should include the EncryptionKeyId column and all <see cref="Columns"/>.
            /// The where clause should include <see cref="PrimaryKeyColumns"/>.
            /// </summary>
            public string UpdateStatement { get; set; }

            public IReadOnlyCollection<PrimaryKeyColumn> PrimaryKeyColumns { get; set; }

            public IReadOnlyCollection<ColumnToEncrypt> Columns { get; set; }
        }

        public class PrimaryKeyColumn
        {
            public string ColumnName { get; set; }
        }

        public abstract class ColumnToEncrypt
        {
            /// <summary>
            /// This can be whatever type is necessary, as it's handled in the subclass.
            /// </summary>
            public string ExistingColumnName { get; set; }

            /// <summary>
            /// This is assumed to be varbinary(something), and assumed to have a default of the empty array if no value.
            /// </summary>
            public string TargetColumnName { get; set; }

            public abstract Lazy<string> ReadValue(IDataRecord record, Dictionary<string, object> primaryKey);
        }

        /// <summary>
        /// A column that is already encrypted, but using a hardcoded key.
        /// </summary>
        public class AlreadyEncryptedColumnToEncrypt : ColumnToEncrypt
        {
            public EncryptionKeyIdentifier ExistingEncryptionKeyId { get; set; }

            public override Lazy<string> ReadValue(IDataRecord record, Dictionary<string, object> primaryKey)
            {
                object rawValue = record[this.ExistingColumnName];
                byte[] val = rawValue == DBNull.Value ? null : (byte[])rawValue;
                return new Lazy<string>(() =>
                {
                    try
                    {
                        return EncryptionHelper.DecryptString(this.ExistingEncryptionKeyId, val);
                    }
                    catch (Exception e)
                    {
                        DataAccess.Tools.LogError(
                            "Unable to decrypt existing value for " + this.ExistingColumnName + " on row with key " + SerializationHelper.JsonNetAnonymousSerialize(primaryKey),
                            e);
                        throw;
                    }
                });
            }
        }

        /// <summary>
        /// A column that is not encrypted at all.
        /// </summary>
        public class UnencryptedColumnToEncrypt : ColumnToEncrypt
        {
            public override Lazy<string> ReadValue(IDataRecord record, Dictionary<string, object> primaryKey)
            {
                object rawValue = record[this.ExistingColumnName];
                string val = rawValue == DBNull.Value ? null : (string)rawValue;
                return new Lazy<string>(() => val);
            }
        }

        /// <summary>
        /// A column encrypted using <see cref="EncryptionHelper.Encrypt(string)"/>. 
        /// </summary>
        public class StringEncryptedColumnToEncrypt : ColumnToEncrypt
        {
            public override Lazy<string> ReadValue(IDataRecord record, Dictionary<string, object> primaryKey)
            {
                object rawValue = record[this.ExistingColumnName];
                string val = rawValue == DBNull.Value ? null : (string)rawValue;
                return new Lazy<string>(() =>
                {
                    try
                    {
                        return EncryptionHelper.Decrypt(val);
                    }
                    catch (Exception e)
                    {
                        DataAccess.Tools.LogError(
                            "Unable to decrypt existing value for " + this.ExistingColumnName + " on row with key " + SerializationHelper.JsonNetAnonymousSerialize(primaryKey),
                            e);
                        throw;
                    }
                });
            }
        }

        private class TableRowToEncrypt
        {
            public Dictionary<string, object> PrimaryKey { get; set; }

            public Dictionary<string, Lazy<string>> ColumnsToMigrate { get; set; }
        }

        /// <summary>
        /// Sadly, the <see cref="DataAccess.DbConnectionInfo"/> class only appears to expose instances for the lender databases,
        /// so we have to create this stub for wrapping the calls to simplify our underlying usage.
        /// </summary>
        private class DbConnectionInfoReference
        {
            private readonly Either<DataAccess.DbConnectionInfo, DataAccess.DataSrc> actualDbData;

            public DbConnectionInfoReference(DataAccess.DbConnectionInfo connectionInfo)
            {
                this.actualDbData = Either<DataAccess.DbConnectionInfo, DataAccess.DataSrc>.CreateLeft(connectionInfo);
            }

            public DbConnectionInfoReference(DataAccess.DataSrc source)
            {
                this.actualDbData = Either<DataAccess.DbConnectionInfo, DataAccess.DataSrc>.CreateRight(source);
            }

            public string Description => this.actualDbData.IsLeft ? this.actualDbData.Left.Description : this.actualDbData.Right.ToString();

            public string Id => this.actualDbData.IsLeft ? this.actualDbData.Left.Id.ToString() : this.actualDbData.Right.ToString();

            public void RunSelectStatement(string sql, TimeoutInSeconds? timeout, IEnumerable<SqlParameter> parameters, Action<IDataReader> readerCode)
            {
                if (this.actualDbData.IsLeft)
                {
                    DataAccess.DBSelectUtility.ProcessDBData(
                        this.actualDbData.Left,
                        sql,
                        timeout,
                        parameters,
                        readerCode);
                }
                else
                {
                    DataAccess.DBSelectUtility.ProcessDBData(
                        this.actualDbData.Right,
                        sql,
                        timeout,
                        parameters,
                        readerCode);
                }
            }

            public ModifiedRowCount RunUpdateStatement(string sql, TimeoutInSeconds? timeout, IEnumerable<SqlParameter> parameters)
            {
                if (this.actualDbData.IsLeft)
                {
                    return DataAccess.DBUpdateUtility.Update(
                        this.actualDbData.Left,
                        sql,
                        timeout,
                        parameters);
                }
                else
                {
                    return DataAccess.DBUpdateUtility.Update(
                        this.actualDbData.Right,
                        sql,
                        timeout,
                        parameters);
                }
            }
        }

        private class Either<TLeft, TRight>
        {
            private Either(bool isLeft, TLeft left, TRight right)
            {
                this.IsLeft = isLeft;
                this.Left = left;
                this.Right = right;
            }

            public bool IsLeft { get; }

            public TLeft Left { get; }

            public TRight Right { get; }

            public static Either<TLeft, TRight> CreateLeft(TLeft left) => new Either<TLeft, TRight>(true, left, default(TRight));

            public static Either<TLeft, TRight> CreateRight(TRight right) => new Either<TLeft, TRight>(false, default(TLeft), right);
        }
    }
}
