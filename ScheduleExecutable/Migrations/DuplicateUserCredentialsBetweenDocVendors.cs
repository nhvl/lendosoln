﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using DataAccess;
    using LendersOffice.Integration.DocumentVendor;
    using System.Linq;
    using LendersOffice.Admin;

    /// <summary>
    /// Replicates MMI document vendor user credentials from one DocuTech vendor to another.
    /// </summary>
    /// <remarks>
    /// This is necessary because DocuTech spun up an entire different vendor environment while
    /// transitioning clients from MISMO 2.6 to 3.3, and will likely do so again for 3.4.
    /// This causes issues for MMI specifically because each user has their own credentials, necessitating
    /// copying hundreds of credential sets between DocuTech instances. This will automate that process for MMI.
    /// </remarks>
    public class DuplicateUserCredentialsBetweenDocVendors : Migrator<Guid>
    {
        /// <summary>
        /// The broker ID associated with the users to migrate.
        /// </summary>
        private Guid BrokerId { get; set; }

        /// <summary>
        /// The ID of the document vendor that contains credentials.
        /// </summary>
        private Guid SourceVendorId { get; set; }

        /// <summary>
        /// The ID of the document vendor that needs credentials populated.
        /// </summary>
        private Guid DestinationVendorId { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="DuplicateUserCredentialsBetweenDocVendors"/> class.
        /// </summary>
        public DuplicateUserCredentialsBetweenDocVendors(Guid brokerId, Guid sourceVendorId, Guid destinationVendorId)
            : base(nameof(DuplicateUserCredentialsBetweenDocVendors))
        {
            this.BrokerId = brokerId;
            this.SourceVendorId = sourceVendorId;
            this.DestinationVendorId = destinationVendorId;
            this.ValidateArguments();

            this.MigrationHelper = new CustomGUIDMigrationHelper(nameof(DuplicateUserCredentialsBetweenDocVendors), ListUsersToMigrate);
        }

        /// <summary>
        /// Runs the migration.
        /// </summary>
        /// <param name="args">Console string arguments.</param>
        public static void Run(string[] args)
        {
            Guid brokerId = Guid.Empty;
            Guid sourceVendorId = Guid.Empty;
            Guid destinationVendorId = Guid.Empty;

            try
            {
                // The migration name will be the first argument, so grab the rest starting at index 1.
                brokerId = new Guid(args[1]);
                sourceVendorId = new Guid(args[2]);
                destinationVendorId = new Guid(args[3]);
            }
            catch (SystemException exc) when (exc is ArgumentNullException || exc is FormatException)
            {
                throw new ArgumentException("The arguments must be valid GUIDs. Usage: \"DuplicateUserCredentialsBetweenDocVendors <BrokerId> <SourceVendorId> <DestinationVendorId>\"");
            }

            var brokerDb = BrokerDB.RetrieveById(brokerId);
            Console.WriteLine($"The input broker ID ({brokerId}) corresponds to {brokerDb.Name}");
            Console.WriteLine("Is this correct? (y/n)");
            var inputKey = Console.ReadKey(intercept: true);

            if (inputKey.Key == ConsoleKey.Y)
            {
                Console.WriteLine("Proceeding with migration....");
                var migrator = new DuplicateUserCredentialsBetweenDocVendors(brokerId, sourceVendorId, destinationVendorId);
                migrator.Migrate();
            }
            else
            {
                Console.WriteLine("Aborting migration....");
            }
        }

        /// <summary>
        /// Validates the arguments to ensure that the specified vendors are configured for the specified lender.
        /// </summary>
        public void ValidateArguments()
        {
            var configuredVendors = DocumentVendorBrokerSettings.ListAllForBroker(this.BrokerId);

            if (!configuredVendors.Any(vendor => vendor.VendorId == this.SourceVendorId))
            {
                throw new ArgumentException($"Source vendor ID {this.SourceVendorId} is not configured for broker {this.BrokerId}");
            }

            if (!configuredVendors.Any(vendor => vendor.VendorId == this.DestinationVendorId))
            {
                throw new ArgumentException($"Destination vendor ID {this.DestinationVendorId} is not configured for broker {this.BrokerId}");
            }
        }

        /// <summary>
        /// Retrieves the IDs for users associated with the given broker who have credentials
        /// configured for the source vendor.
        /// </summary>
        /// <returns>A list of user IDs to migrate.</returns>
        public List<Guid> ListUsersToMigrate()
        {
            var userIds = new List<Guid>();
            var sql = $@"
                SELECT UserId
                FROM DOCUMENT_VENDOR_USER_ENABLED_CREDENTIALS
                WHERE VendorId = @SourceVendorId
                    AND BrokerId = @BrokerId;
            ";

            var parameters = new SqlParameter[]
            {
                new SqlParameter("@SourceVendorId", this.SourceVendorId),
                new SqlParameter("@BrokerId", this.BrokerId)
            };

            Action<IDataReader> readHandler = delegate (IDataReader reader)
            {
                while (reader.Read())
                {
                    var userId = (Guid)reader["UserId"];
                    userIds.Add(userId);
                }
            };

            DBSelectUtility.ProcessDBData(this.BrokerId, sql, null, parameters, readHandler);
            return userIds;
        }

        /// <summary>
        /// Migrates the credentials for a single user from the source vendor to the destination vendor.
        /// Does not stomp any existing credentials.
        /// </summary>
        /// <param name="userId">The user ID to migrate.</param>
        /// <returns>A boolean indicating whether the migration was successful.</returns>
        protected override bool MigrationFunction(Guid userId)
        {
            try
            {
                var destinationCredentials = VendorCredentials.GetUserCredentialsOnly(userId, this.BrokerId, this.DestinationVendorId);

                if (!string.IsNullOrEmpty(destinationCredentials.UserName)
                    || !string.IsNullOrEmpty(destinationCredentials.Password)
                    || !string.IsNullOrEmpty(destinationCredentials.CustomerId))
                {
                    // Don't stomp any existing credentials.
                    return true;
                }

                var sourceCredentials = VendorCredentials.GetUserCredentialsOnly(userId, this.BrokerId, this.SourceVendorId);

                destinationCredentials.UserName = sourceCredentials.UserName;
                destinationCredentials.Password = sourceCredentials.Password;
                destinationCredentials.CustomerId = sourceCredentials.CustomerId;

                destinationCredentials.SaveUserCredentials();
                return true;
            }
            catch (Exception exc) when (exc is ArgumentException || exc is SqlException || exc is CBaseException)
            {
                Tools.LogError($"Failed to migrate document vendor credentials for user {userId} from vendor {this.SourceVendorId} to vendor {this.DestinationVendorId}", exc);
                return false;
            }
        }
    }
}