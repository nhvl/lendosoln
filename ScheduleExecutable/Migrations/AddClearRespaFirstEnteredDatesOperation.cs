﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LendersOffice.ConfigSystem.Operations;

namespace ScheduleExecutable.Migrations
{
    public class AddClearRespaFirstEnteredDatesOperation : SystemConfigOperationMigrator
    {
        protected override IEnumerable<OperationAddition> GetNewOperations()
        {
            return new[]
            {
                new OperationAddition(
                    WorkflowOperations.ClearRespaFirstEnteredDates,
                    defaultValue: false,
                    notes: "OPM 365605",
                    failureMessage: "You do not have permission to clear the entered date.")
            };
        }
    }
}
