﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Drivers.Gateways;

    public class EnableBananaSecurityTempOptionMigrator : Migrator<Guid>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EnableBananaSecurityTempOptionMigrator"/> class. Will migrate all brokers.
        /// </summary>
        /// <param name="name">The name of the migratino.</param>
        public EnableBananaSecurityTempOptionMigrator()
            : base($"EnableBananaSecurity_{DateTime.Now.ToString("MM_dd_yyyy")}")
        {
            this.MigrationHelper = new BrokerMigrationHelper(this.Name);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EnableBananaSecurityTempOptionMigrator"/> class. Will migrate using the list of broker ids.
        /// </summary>
        /// <param name="brokerIds"></param>
        public EnableBananaSecurityTempOptionMigrator(IEnumerable<Guid> brokerIds)
            : base($"EnableBananaSecurity_{DateTime.Now.ToString("MM_dd_yyyy")}")
        {
            this.MigrationHelper = new CustomGUIDMigrationHelper(this.Name, () => brokerIds);
        }

        /// <summary>
        /// Adds the UseNewSMSApi temp option.
        /// </summary>
        /// <param name="args"></param>
        public static void EnableBananaSecurity(string[] args)
        {
            EnableBananaSecurityTempOptionMigrator migrator;
            if (args.Length == 1)
            {
                migrator = new EnableBananaSecurityTempOptionMigrator();
            }
            else if (args.Length == 2)
            {
                IEnumerable<Guid> brokerIds = TextFileHelper.ReadLines(args[1]).Select((stringId) => Guid.Parse(stringId));
                migrator = new EnableBananaSecurityTempOptionMigrator(brokerIds);
            }
            else
            {
                Console.WriteLine("Usage: ScheduleExecutable EnableBananaSecurity OR ScheduleExecutable EnableBananaSecurity [FilePath]");
                return;
            }

            migrator.Migrate();
        }

        /// <summary>
        /// The migration function.
        /// Will add the temp option to the broker temp option xml if it doesn't exist.
        /// </summary>
        /// <param name="id">The broker id to migrate.</param>
        /// <returns>True if the migration is successful. False otherwise.</returns>
        protected override bool MigrationFunction(Guid id)
        {
            try
            {
                BrokerDB broker = BrokerDB.RetrieveById(id);

                string tempOption = "<option name=\"UseNewSMSApi\" value=\"true\" />";
                if (broker.TempOptionXmlContent.Value.IndexOf(tempOption, StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    return false;
                }

                if (broker.TempOptionXmlContent.Value.Contains("</options>"))
                {
                    broker.TempOptionXmlContent = broker.TempOptionXmlContent.Value.Replace("</options>", $"{tempOption}</options>");
                }
                else
                {
                    broker.TempOptionXmlContent = broker.TempOptionXmlContent.Value + tempOption;
                }

                broker.Save();
                return true;
            }
            catch (Exception e)
            {
                Tools.LogError("Error updating broker <" + id.ToString() + "> for EnableBananaSecurity.", e);
            }

            return false;
        }
    }
}
