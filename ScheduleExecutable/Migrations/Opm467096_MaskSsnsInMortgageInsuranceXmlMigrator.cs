﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using DataAccess;
    using LendersOffice.Integration.MortgageInsurance;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Migrates the response XML for mortgage insurance orders
    /// to mask the SSN for the borrower element.
    /// </summary>
    public class Opm467096_MaskSsnsInMortgageInsuranceXmlMigrator
    {
        public static void Run(string[] args)
        {
            var migrator = new Opm467096_MaskSsnsInMortgageInsuranceXmlMigrator();
            migrator.Migrate();
        }

        private void Migrate()
        {
            const string UpdateSql = @"
UPDATE MORTGAGE_INSURANCE_ORDER_INFO
SET ResponseXmlContent = @ResponseXmlContent
WHERE sLId = @LoanId AND VendorId = @VendorId AND OrderNumber = @OrderNumber
";

            foreach (var order in this.GetOrders())
            {
                try
                {
                    var maskedResponseXmlContent = MIResponseProvider.MaskSensitiveResponseData(order.ResponseXmlContent);

                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@LoanId", order.LoanId),
                        new SqlParameter("@VendorId", order.VendorId),
                        new SqlParameter("@OrderNumber", order.OrderNumber),
                        new SqlParameter("@ResponseXmlContent", maskedResponseXmlContent)
                    };

                    DBUpdateUtility.Update(order.BrokerId, UpdateSql, TimeoutInSeconds.Thirty, parameters);
                }
                catch (Exception e)
                {
                    Tools.LogError($"[OPM 467096] Unable to migrate MI order. LoanId: {order.LoanId}, VendorId: {order.VendorId}, OrderNumber: {order.OrderNumber}", e);
                }
            }
        }

        private IEnumerable<MortgageInsuranceOrder> GetOrders()
        {
            var brokerIdsByLoanId = new Dictionary<Guid, Guid>();
            var orders = new LinkedList<MortgageInsuranceOrder>();

            Action<IDataReader> readerCode = reader =>
            {
                while (reader.Read())
                {
                    var order = new MortgageInsuranceOrder()
                    {
                        LoanId = (Guid)reader["sLId"],
                        VendorId = (Guid)reader["vendorid"],
                        OrderNumber = (string)reader["ordernumber"],
                        ResponseXmlContent = (string)reader["ResponseXmlContent"]
                    };

                    Guid brokerId;
                    if (!brokerIdsByLoanId.TryGetValue(order.LoanId, out brokerId))
                    {
                        try
                        {
                            brokerId = Tools.GetBrokerIdByLoanID(order.LoanId);
                        }
                        catch (Exception e)
                        {
                            Tools.LogError($"[OPM 467096] Failed to retrieve broker ID for loan {order.LoanId} in MI migration, response XML should be manually updated.", e);
                            continue;
                        }

                        brokerIdsByLoanId.Add(order.LoanId, brokerId);
                    }

                    order.BrokerId = brokerId;
                    orders.AddLast(order);
                }
            };

            const string SelectSql = @"
SELECT sLId, vendorid, ordernumber, ResponseXmlContent
FROM MORTGAGE_INSURANCE_ORDER_INFO
WHERE DATALENGTH(ResponseXmlContent) > 0
";

            foreach (var connection in DbConnectionInfo.ListAll())
            {
                DBSelectUtility.ProcessDBData(connection, SelectSql, TimeoutInSeconds.Sixty, null, readerCode);
            }

            return orders;
        }

        private class MortgageInsuranceOrder
        {
            public Guid BrokerId { get; set; }
            public Guid LoanId { get; set; }
            public Guid VendorId { get; set; }
            public string OrderNumber { get; set; }
            public string ResponseXmlContent { get; set; }
        }
    }
}
