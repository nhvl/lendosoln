﻿namespace ScheduleExecutable.Migrations
{
    using System.Collections.Generic;
    using LendersOffice.ConfigSystem.Operations;

    public class DisclosureEventWorkflowMigrator : SystemConfigOperationMigrator
    {
        /// <summary>
        /// Runs the migration.
        /// </summary>
        /// <param name="args">The command line arguments.</param>
        public static void Run(string[] args)
        {
            var privilegeAdder = new DisclosureEventWorkflowMigrator();
            privilegeAdder.AddOperations();
        }

        /// <summary>
        /// Gets the operations to add.
        /// </summary>
        /// <returns>The operations to add.</returns>
        protected override IEnumerable<OperationAddition> GetNewOperations()
        {
            return new[]
            {
                new OperationAddition(
                    WorkflowOperations.AllowRecordingExternalDisclosureEvents,
                    defaultValue: true,
                    notes: "OPM 444803",
                    failureMessage: "You do not have permission to record external disclosure events.")
            };
        }
    }
}
