﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Drivers.Gateways;

    /// <summary>
    /// Adds the temp option to brokers.
    /// </summary>
    public class UsingLegacyDUCredentialsTempOptionMigrator : Migrator<Guid>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UsingLegacyDUCredentialsTempOptionMigrator"/> class.
        /// </summary>
        public UsingLegacyDUCredentialsTempOptionMigrator()
            : base($"AddUsingLegacyDUCredentialsTempOption_{DateTime.Now.ToString("MM_dd_yyyy")}")
        {
            this.MigrationHelper = new BrokerMigrationHelper(this.Name);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UsingLegacyDUCredentialsTempOptionMigrator"/> class.
        /// </summary>
        /// <param name="brokerIds">The broker to add the temp option to.</param>
        public UsingLegacyDUCredentialsTempOptionMigrator(IEnumerable<Guid> brokerIds)
            : base($"UsingLegacyDUCredentialsTempOption_{DateTime.Now.ToString("MM_dd_yyyy")}")
        {
            this.MigrationHelper = new CustomGUIDMigrationHelper(this.Name, () => brokerIds);
        }

        /// <summary>
        /// Adds the UsingLegacyDUCredentials temp option.
        /// </summary>
        /// <param name="args">Command line arguments.</param>
        public static void AddUsingLegacyDUCredentialsTempOption(string[] args)
        {
            UsingLegacyDUCredentialsTempOptionMigrator migrator;
            if (args.Length == 1)
            {
                migrator = new UsingLegacyDUCredentialsTempOptionMigrator();
            }
            else if (args.Length == 2)
            {
                IEnumerable<Guid> brokerIds = TextFileHelper.ReadLines(args[1]).Select((stringId) => Guid.Parse(stringId));
                migrator = new UsingLegacyDUCredentialsTempOptionMigrator(brokerIds);
            }
            else
            {
                Console.WriteLine("Usage: ScheduleExecutable AddUsingLegacyDUCredentialsTempOption OR ScheduleExecutable AddUsingLegacyDUCredentialsTempOption [FilePath]");
                return;
            }

            migrator.Migrate();
        }

        /// <summary>
        /// The migration function. Adds the temp option if it doesn't already exist.
        /// </summary>
        /// <param name="id">The broker id.</param>
        /// <returns>True if migrated. False otherwise.</returns>
        protected override bool MigrationFunction(Guid id)
        {
            try
            {
                BrokerDB broker = BrokerDB.RetrieveById(id);

                string tempOption = "<option name=\"UsingLegacyDUCredentials\" value=\"true\" />";
                if (broker.TempOptionXmlContent.Value.IndexOf(tempOption, StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    return false;
                }

                if (broker.TempOptionXmlContent.Value.Contains("</options>"))
                {
                    broker.TempOptionXmlContent = broker.TempOptionXmlContent.Value.Replace("</options>", $"{tempOption}</options>");
                }
                else
                {
                    broker.TempOptionXmlContent = broker.TempOptionXmlContent.Value + tempOption;
                }

                broker.Save();
                return true;
            }
            catch (Exception e)
            {
                Tools.LogError("Error updating broker <" + id.ToString() + "> for AddUsingLegacyDUCredentialsTempOption.", e);
            }

            return false;
        }
    }
}
