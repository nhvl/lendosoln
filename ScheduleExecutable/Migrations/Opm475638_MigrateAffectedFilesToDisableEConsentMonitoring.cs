﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.Disclosure.EConsentMonitoring;

    public class Opm475638_MigrateAffectedFilesToDisableEConsentMonitoring : Migrator<DisableEConsentMonitoringAffectedFileData>
    {
        private readonly Guid brokerId;
        private readonly DisableEConsentMonitoringMigrationHelper disableEConsentMonitoringMigrationHelper;

        public Opm475638_MigrateAffectedFilesToDisableEConsentMonitoring(Guid brokerId)
            : base(nameof(Opm475638_MigrateAffectedFilesToDisableEConsentMonitoring))
        {
            this.brokerId = brokerId;
            this.disableEConsentMonitoringMigrationHelper = new DisableEConsentMonitoringMigrationHelper(brokerId);

            this.MigrationHelper = new MigrationHelper<DisableEConsentMonitoringAffectedFileData>(
                nameof(Opm475638_MigrateAffectedFilesToDisableEConsentMonitoring),
                () => this.disableEConsentMonitoringMigrationHelper.GetAffectedFileList(),
                fileDataStr => SerializationHelper.JsonNetDeserialize<DisableEConsentMonitoringAffectedFileData>(fileDataStr),
                fileData => SerializationHelper.JsonNetSerialize(fileData));
        }

        public static void Run(string[] args)
        {
            Guid brokerId;
            if (args.Length != 1 || !Guid.TryParse(args[0], out brokerId))
            {
                Console.WriteLine("Please enter the ID of the lender to migrate as the only parameter to the migration.");
                return;
            }

            var nameCustomCodeTuple = BrokerDB.RetrieveNameAndCustomerCodeFromDb(brokerId);
            Console.WriteLine(
                $"This migration will update affected files and disable e-consent monitoring automation for {nameCustomCodeTuple.Item1} ({nameCustomCodeTuple.Item2}). " +
                "Enter 'yes' without quotes to confirm.");

            var response = Console.ReadLine();
            if (!string.Equals("yes", response, StringComparison.OrdinalIgnoreCase))
            {
                Console.WriteLine("Quitting.");
                return;
            }

            var migrator = new Opm475638_MigrateAffectedFilesToDisableEConsentMonitoring(brokerId);
            migrator.Migrate();
        }

        protected override bool MigrationFunction(DisableEConsentMonitoringAffectedFileData file)
        {
            try
            {
                this.MigrationHelper.WriteToBackup(file);
                this.disableEConsentMonitoringMigrationHelper.MigrateFile(file.sLId);
                return true;
            }
            catch (Exception e)
            {
                Tools.LogError("[OPM 475638] Error encountered during migration.", e);
                throw;
            }
        }

        protected override void EndMigration()
        {
            try
            {
                this.disableEConsentMonitoringMigrationHelper.DisableLenderBit();
                Tools.LogInfo(nameof(Opm475638_MigrateAffectedFilesToDisableEConsentMonitoring), "Updated lender bit, migration complete.");
                Console.WriteLine($"Please email backup file to Michael.");
            }
            catch (Exception e)
            {
                Tools.LogError("[OPM 475638] Error encountered when disabling lender bit.", e);

                // Failure to set the lender bit could leave files updated after
                // this migration in a bad state and is grounds for critical
                // issues. Send an email to the critical inbox in the event
                // an exception occurs.
                EmailUtilities.SendToCritical(
                    "[OPM 475638] Unable to disable lender e-consent automation", 
                    $"Unable to set EnableEConsentMonitoringAutomation = 0 for lender {brokerId}, please update database manually. See stored procedure DisableEConsentMonitoringMigration_DisableLenderBit.");
            }
        }
    }
}
