﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Drivers.SqlServerDB;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Populates the company index number for the existing originating companies
    /// of all active lenders.
    /// </summary>
    public class Opm470266_PopulateIndexNumberForOriginatingCompanies : Migrator<Guid>
    {
        /// <summary>
        /// Prevents a default instance of the <see cref="Opm470266_PopulateIndexNumberForOriginatingCompanies"/>
        /// class from being created.
        /// </summary>
        private Opm470266_PopulateIndexNumberForOriginatingCompanies()
            : base(nameof(Opm470266_PopulateIndexNumberForOriginatingCompanies))
        {
            this.MigrationHelper = new BrokerMigrationHelper(this.Name);
        }

        /// <summary>
        /// Executes the migration.
        /// </summary>
        /// <param name="args">
        /// This parameter is not used.
        /// </param>
        public static void Run(string[] args)
        {
            var migrator = new Opm470266_PopulateIndexNumberForOriginatingCompanies();
            migrator.Migrate();
        }

        /// <summary>
        /// Migrates the originating companies for the broker with the specified <paramref name="brokerId"/>.
        /// </summary>
        /// <param name="brokerId">
        /// The ID for the broker.
        /// </param>
        /// <returns>
        /// True if the migration was successful, false otherwise.
        /// </returns>
        protected override bool MigrationFunction(Guid brokerId)
        {
            var originatingCompanyIds = this.GetOriginatingCompanyIdsForBroker(brokerId);
            if (!originatingCompanyIds.Any())
            {
                return true;
            }
            
            using (var connection = DbConnectionInfo.GetConnection(brokerId))
            {
                try
                {
                    connection.Open();
                    foreach (var originatingCompanyId in originatingCompanyIds)
                    {
                        var indexNumber = BrokerDB.GetAndUpdateOriginatingCompanyIndexNumberCounter(brokerId);

                        SqlParameter[] parameters =
                        {
                            new SqlParameter("@BrokerId", brokerId),
                            new SqlParameter("@PmlBrokerId", originatingCompanyId),
                            new SqlParameter("@IndexNumber", indexNumber)
                        };

                        StoredProcedureDriverHelper.ExecuteNonQuery(
                            connection, 
                            null, 
                            StoredProcedureName.Create("PML_BROKER_AddIndexNumberForMigration").Value, 
                            parameters, 
                            TimeoutInSeconds.Thirty);
                    }

                    return true;
                }
                catch (Exception exc)
                {
                    Console.WriteLine($"Failed to migrate originating companies for broker {brokerId}: {exc.Message}");
                    Tools.LogError("Failed to migrate originating companies for broker " + brokerId, exc);

                    return false;
                }
            }
        }

        /// <summary>
        /// Obtains the IDs for the originating companies of the broker with 
        /// the specified <paramref name="brokerId"/>.
        /// </summary>
        /// <param name="brokerId">
        /// The ID of the broker.
        /// </param>
        /// <returns>
        /// The IDs of the originating companies.
        /// </returns>
        private IEnumerable<Guid> GetOriginatingCompanyIdsForBroker(Guid brokerId)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", brokerId)
            };

            var originatingCompanyIds = new LinkedList<Guid>();

            const string Sql = @"
SELECT PmlBrokerId
FROM PML_BROKER
WHERE BrokerId = @BrokerId AND IndexNumber IS NULL
ORDER BY Name";

            Action<IDataReader> readerCode = reader =>
            {
                while (reader.Read())
                {
                    originatingCompanyIds.AddLast((Guid)reader["PmlBrokerId"]);
                }
            };

            DBSelectUtility.ProcessDBData(brokerId, Sql, TimeoutInSeconds.Thirty, parameters, readerCode);
            return originatingCompanyIds;
        }
    }
}
