﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Xml;
    using System.Xml.Linq;
    using DataAccess;
    using LendersOffice.Drivers.SqlServerDB;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Check all data in legacy XML format to determine if/where data
    /// is not compatible with the new tables.
    /// </summary>
    public sealed class Opm474083_CheckUladCollectionsForMigrability
    {
        private Guid migrationId;

        public Opm474083_CheckUladCollectionsForMigrability()
        {
            this.migrationId = Guid.NewGuid();
        }

        public static void RecordUnmigratedLoanCollections(string[] args)
        {
            Console.WriteLine("Writing the loan identifiers for loan collections that haven't been migrated to the table LOAN_COLLECTION_MIGRATION_LOAN_IDS for future processing.");
            Console.WriteLine("This may take a few minutes...");
            string sql = "INSERT INTO [dbo].[LOAN_COLLECTION_MIGRATION_LOAN_IDS] (LoanId) SELECT sLId As LoanId FROM [dbo].[LOAN_FILE_A] WHERE sBorrowerApplicationCollectionT IS NULL OR sBorrowerApplicationCollectionT = 0;";

            foreach (var connectionInfo in DbConnectionInfo.ListAll())
            {
                DBUpdateUtility.Update(connectionInfo, sql, TimeoutInSeconds.Create(600000), null);
            }

            Console.WriteLine("Done writing the loan identifiers for unmigrated loan collections.");
        }

        public static void MigrateLoanCollections(string[] args)
        {
            int rowCount = 0;
            int.TryParse(args[0], out rowCount);
            string topClause = rowCount <= 0 ? string.Empty : $" TOP {rowCount}";
            if (rowCount <= 0)
            {
                Console.WriteLine("Processing ALL the loan identifiers for loan collections that haven't been migrated from the table LOAN_COLLECTION_MIGRATION_LOAN_IDS.");
            }
            else
            {
                Console.WriteLine($"Processing TOP {rowCount.ToString()} of the loan identifiers for loan collections that haven't been migrated from the table LOAN_COLLECTION_MIGRATION_LOAN_IDS.");
            }

            // Read in a batch of identifiers and delete those rows
            string sql = $"SELECT{topClause} LoanId FROM [dbo].[LOAN_COLLECTION_MIGRATION_LOAN_IDS] ORDER BY RowId; DELETE FROM [dbo].[LOAN_COLLECTION_MIGRATION_LOAN_IDS] WHERE RowId IN (SELECT{topClause} RowId FROM [dbo].[LOAN_COLLECTION_MIGRATION_LOAN_IDS] ORDER BY RowId);";
            foreach (var connectionInfo in DbConnectionInfo.ListAll())
            {
                var identifiers = new List<Guid>();
                Action<IDataReader> readHandler = r =>
                {
                    while (r.Read())
                    {
                        var id = (Guid)r["LoanId"];
                        identifiers.Add(id);
                    }
                };

                Console.WriteLine("Reading the loan identifiers for a database.");
                using (var connection = connectionInfo.GetConnection())
                {
                    DBSelectUtility.ProcessDBData(connection, null, sql, TimeoutInSeconds.Create(60), null, readHandler);
                }

                Console.WriteLine("Migrating the loan collections, this may take several minutes.");
                var migrator = new Opm474083_CheckUladCollectionsForMigrability();
                foreach (Guid loanId in identifiers)
                {
                    migrator.Migrate(loanId);
                }
            }

            Console.WriteLine("Done processing the unmigrated loan collections.");
        }

        public void Migrate(Guid id)
        {
            try
            {
                this.MigrationFunctionImpl(id);
            }
            catch (Exception ex)
            {
                this.WriteData(this.migrationId, "EXCEPTION", id, ex.Message);
            }
        }

        private void MigrationFunctionImpl(Guid id)
        {
            var collectionNames = Opm474083_UladEntityInfo.CollectionNames();
            var loan = this.LoadLoan(id, collectionNames);
            if (loan == null)
            {
                return;
            }

            for (int i=0; i<loan.nApps; ++i)
            {
                var appData = loan.GetAppData(i);
                foreach (var name in collectionNames)
                {
                    var xml = Opm474083_UladEntityInfo.GetCollectionXml(appData, name);
                    if (!string.IsNullOrEmpty(xml))
                    {
                        var uladInfos = Opm474083_UladEntityInfo.GetUladEntityInfo(name);

                        var doc = XDocument.Parse(xml);
                        foreach (var elem in doc.Descendants().Where(d => d.Element("RecordId") != null))
                        {
                            string recordId = elem.Element("RecordId").Value;
                            foreach (var info in uladInfos)
                            {
                                string itemValue = elem.Element(info.XmlElementName)?.Value.Trim();
                                if (!string.IsNullOrEmpty(itemValue) || !info.AllowNull)
                                {
                                    int? len = Opm474083_UladEntityInfo.GetColumnSize(info.SemanticType);
                                    if (len != null && itemValue.Length > len.Value)
                                    {
                                        string message = $"(size_limit, data_size) = ({len.Value.ToString()}, {itemValue.Length.ToString()})";
                                        this.WriteData(loan.sBrokerId, this.migrationId, "LENGTH", id, appData.aAppId, name, Guid.Parse(recordId), info.XmlElementName, info.SemanticType, message);
                                    }

                                    if (!string.IsNullOrEmpty(itemValue) && !Opm474083_UladEntityInfo.CanCreateSemanticType(info.SemanticType, itemValue))
                                    {
                                        int logLen = Math.Min(100, itemValue.Length);
                                        string message = $"Bad Value (possibly truncated) = {itemValue.Substring(0, logLen)}";
                                        this.WriteData(loan.sBrokerId, this.migrationId, "SEMANTIC", id, appData.aAppId, name, Guid.Parse(recordId), info.XmlElementName, info.SemanticType, message);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void WriteData(Guid migrationId, string problemType, Guid loanId, string message)
        {
            Guid brokerId;
            DbConnectionInfo.GetConnectionInfoByLoanId(loanId, out brokerId);
            WriteData(brokerId, migrationId, problemType, loanId, null, null, null, null, null, message);
        }

        private void WriteData(Guid brokerId, Guid migrationId, string problemType, Guid loanId, Guid? appId, string collectionName, Guid? recordId, string fieldName, string semanticType, string message)
        {
            try
            {
                var parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@migId", migrationId));
                parameters.Add(this.CreateStringParameter("@type", problemType));
                parameters.Add(new SqlParameter("@loanId", loanId));
                parameters.Add(this.CreateStringParameter("@msg", message));

                string sql;
                if (appId != null)
                {
                    parameters.Add(new SqlParameter("@appId", appId.Value));
                    parameters.Add(this.CreateStringParameter("@coll", collectionName));
                    parameters.Add(new SqlParameter("@recId", recordId.Value));
                    parameters.Add(this.CreateStringParameter("@field", fieldName));
                    parameters.Add(this.CreateStringParameter("@semType", semanticType));

                    sql = "INSERT INTO [dbo].[LOAN_COLLECTION_MIGRATION_PROBLEMS] (MigrationId, ProblemType, LoanId, LegacyApplicationId, CollectionName, RecordId, FieldName, SemanticType, Message) VALUES (@migId, @type, @loanId, @appId, @coll, @recId, @field, @semType, @msg)";
                }
                else
                {
                    sql = "INSERT INTO [dbo].[LOAN_COLLECTION_MIGRATION_PROBLEMS] (MigrationId, ProblemType, LoanId, Message) VALUES (@migId, @type, @loanId, @msg)";
                }

                var query = SQLQueryString.Create(sql).Value;
                using (var connection = DbConnectionInfo.GetConnection(brokerId))
                {
                    SqlServerHelper.InsertNoKey(connection, null, query, parameters, TimeoutInSeconds.Default);
                }
            }
            catch { } // do nothing
        }

        private SqlParameter CreateStringParameter(string name, string value)
        {
            var param = new SqlParameter();
            param.ParameterName = name;
            param.SqlDbType = SqlDbType.VarChar;
            param.Value = value;
            return param;
        }

        private CPageData LoadLoan(Guid id, string[] collectionNames)
        {
            try
            {
                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypassAndGuaranteedFields(id, this.GetType(), collectionNames);
                loan.AllowLoadWhileQP2Sandboxed = true;
                loan.InitLoad();
                return loan;
            }
            catch
            {
                return null;
            }
        }
    }
}
