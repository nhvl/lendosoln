﻿namespace ScheduleExecutable.Migrations
{
    using DataAccess;
    using LendersOffice.Admin;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;

    /// <summary>
    ///
    /// </summary>
    public class Pml3Migrator : Migrator<Guid>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Pml3Migrator"/> class.
        /// </summary>
        public Pml3Migrator() : base(nameof(Pml3Migrator))
        {
            this.MigrationHelper = new BrokerMigrationHelper(this.Name);
        }

        public static void RunPml3Migrator(string[] args)
        {
            if (args.Length != 1)
            {
                Console.WriteLine("Usage: ScheduleExecutable RunPml3Migrator");
                return;
            }

            Pml3Migrator migrator = new Pml3Migrator();
            migrator.Migrate();
        }

        /// <summary>
        /// Migration function that determines if the job was successful or not.
        /// </summary>
        /// <param name="brokerId">Broker Id.</param>
        /// <returns>True if successful, false otherwise.</returns>
        protected override bool MigrationFunction(Guid brokerId)
        {
            try
            {
                BrokerDB broker = BrokerDB.RetrieveById(brokerId);

                Regex regex = new Regex($"<option name=\"EnablePmlUiResults3\" EmployeeGroup=\"([-a-zA-Z0-9.,;:'\"!$%&?*()]*)\" />");

                if (string.IsNullOrEmpty(broker.TempOptionXmlContent.Value))
                {
                    return true;
                }

                Match matches = regex.Match(broker.TempOptionXmlContent.Value);
                if (!matches.Success || matches.Groups.Count != 2)
                {
                    return false;
                }

                string employeeGroupName = matches.Groups[1].Value;

                broker.TempOptionXmlContent = broker.TempOptionXmlContent.Value.Replace($"<option name=\"EnablePmlUiResults3\" EmployeeGroup=\"{employeeGroupName}\" />", $"<option name=\"EnablePmlUiResults3\" EmployeeGroup=\"{employeeGroupName}\" OCGroup=\"EmptyGroup\" ForcePML3=\"false\" />");
                broker.Save();

                return true;
            }
            catch (Exception e)
            {
                Tools.LogError($"Unable to migrate broker id {brokerId.ToString()}", e);
                return false;
            }
        }
    }
}
