﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.ObjLib.Conversions.ComplianceEase;

    /// <summary>
    /// In case 240591, we are letting lenders customize the statuses that will NOT
    /// trigger ComplianceEase audits. To maintain existing behavior as a starting point,
    /// we want to populate a set of baseline ignored statuses to each broker.
    /// </summary>
    public class SetStatusesIgnoredByComplianceEase : Migrator<Guid>
    {
        /// <summary>
        /// The baseline ignored statuses are pipeline endpoints and statuses where
        /// it is unlikely that any compliance-risk changes will occur.
        /// </summary>
        private static readonly E_sStatusT[] baselineIgnoredStatuses =
        {
            E_sStatusT.Loan_Canceled,
            E_sStatusT.Loan_Rejected,
            E_sStatusT.Loan_Withdrawn,
            E_sStatusT.Loan_Archived,
            E_sStatusT.Loan_LoanPurchased,
            E_sStatusT.Loan_OnHold,
            E_sStatusT.Loan_CounterOffer
        };

        /// <summary>
        /// Initializes a new instance of the <see cref="SetStatusesIgnoredByComplianceEase"/> class.
        /// </summary>
        /// <param name="name">The migration name.</param>
        public SetStatusesIgnoredByComplianceEase(string name) : base(name)
        {
            this.MigrationHelper = new CustomGUIDMigrationHelper(name, RetrieveBrokersWithComplianceEaseConfiguration);
        }

        /// <summary>
        /// Triggers the migration to run.
        /// </summary>
        /// <param name="args">A set of string arguments.</param>
        public static void RunMigration(string[] args)
        {
            var migrator = new SetStatusesIgnoredByComplianceEase(nameof(SetStatusesIgnoredByComplianceEase));
            migrator.Migrate();
        }

        /// <summary>
        /// Retrieves all valid brokers who have a non-empty ComplianceEase config.
        /// </summary>
        /// <returns>A list of IDs corresponding to brokers that should be migrated.</returns>
        public static IEnumerable<Guid> RetrieveBrokersWithComplianceEaseConfiguration()
        {
            var brokerIds = new List<Guid>();
            var sql = @"
                SELECT BrokerId
                FROM BROKER
                WHERE Status = 1
                 AND ComplianceEaseConfigXml != ''";

            Action<IDataReader> readHandler = delegate (IDataReader reader)
            {
                while (reader.Read())
                {
                    brokerIds.Add((Guid)reader["BrokerId"]);
                }
            };

            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                DBSelectUtility.ProcessDBData(connInfo, sql, null, null, readHandler);
            }

            return brokerIds;
        }

        /// <summary>
        /// Sets the baseline ignored statuses for a given broker.
        /// </summary>
        /// <param name="id">The ID of the broker to migrate.</param>
        /// <returns>A boolean indicating whether the migration was successful.</returns>
        /// <remarks>Modeled after the save method in <see cref="ComplianceEaseConfigurationEditor"/></remarks>
        protected override bool MigrationFunction(Guid id)
        {
            try
            {
                BrokerDB broker = BrokerDB.RetrieveById(id);

                var newConfig = new ComplianceEaseExportConfiguration(
                    baselineIgnoredStatuses,
                    broker.ComplianceEaseExportConfig.LenderLicensesByState,
                    broker.ComplianceEaseExportConfig.LendingPolicies,
                    broker.ComplianceEaseExportConfig.DIDMCAExemptions,
                    broker.ComplianceEaseExportConfig.HUDApproval,
                    broker.ComplianceEaseExportConfig.BeginRunningAuditsAt);
                newConfig.sLOrigFPaidTo = broker.ComplianceEaseExportConfig.sLOrigFPaidTo;
                newConfig.sApprFPaidTo = broker.ComplianceEaseExportConfig.sApprFPaidTo;
                newConfig.sCrFPaidTo = broker.ComplianceEaseExportConfig.sCrFPaidTo;
                newConfig.sInspectFPaidTo = broker.ComplianceEaseExportConfig.sInspectFPaidTo;
                newConfig.sMBrokFPaidTo = broker.ComplianceEaseExportConfig.sMBrokFPaidTo;
                newConfig.sProcFPaidTo = broker.ComplianceEaseExportConfig.sProcFPaidTo;
                newConfig.sUwFPaidTo = broker.ComplianceEaseExportConfig.sUwFPaidTo;
                newConfig.sWireFPaidTo = broker.ComplianceEaseExportConfig.sWireFPaidTo;
                newConfig.sTxServFPaidTo = broker.ComplianceEaseExportConfig.sTxServFPaidTo;
                newConfig.sFloodCertificationFPaidTo = broker.ComplianceEaseExportConfig.sFloodCertificationFPaidTo;
                newConfig.sLDiscntPaidTo = broker.ComplianceEaseExportConfig.sLDiscntPaidTo;
                newConfig.sGfeCreditLenderPaidItemFPaidTo = broker.ComplianceEaseExportConfig.sGfeCreditLenderPaidItemFPaidTo;
                newConfig.sGfeLenderCreditFPaidTo = broker.ComplianceEaseExportConfig.sGfeLenderCreditFPaidTo;
                newConfig.sGfeDiscountPointFPaidTo = broker.ComplianceEaseExportConfig.sGfeDiscountPointFPaidTo;
                newConfig.s800RestPaidTo = broker.ComplianceEaseExportConfig.s800RestPaidTo;
                newConfig.sIPiaPaidTo = broker.ComplianceEaseExportConfig.sIPiaPaidTo;
                newConfig.sHazInsPiaPaidTo = broker.ComplianceEaseExportConfig.sHazInsPiaPaidTo;
                newConfig.sMipPiaPaidTo = broker.ComplianceEaseExportConfig.sMipPiaPaidTo;
                newConfig.s900RestPaidTo = broker.ComplianceEaseExportConfig.s900RestPaidTo;
                newConfig.sEscrowFPaidTo = broker.ComplianceEaseExportConfig.sEscrowFPaidTo;
                newConfig.sDocPrepFPaidTo = broker.ComplianceEaseExportConfig.sDocPrepFPaidTo;
                newConfig.sOwnerTitleInsPaidTo = broker.ComplianceEaseExportConfig.sOwnerTitleInsPaidTo;
                newConfig.sTitleInsFPaidTo = broker.ComplianceEaseExportConfig.sTitleInsFPaidTo;
                newConfig.sNotaryFPaidTo = broker.ComplianceEaseExportConfig.sNotaryFPaidTo;
                newConfig.sAttorneyFPaidTo = broker.ComplianceEaseExportConfig.sAttorneyFPaidTo;
                newConfig.s1100RestPaidTo = broker.ComplianceEaseExportConfig.s1100RestPaidTo;
                newConfig.sCountyRtcPaidTo = broker.ComplianceEaseExportConfig.sCountyRtcPaidTo;
                newConfig.sStateRtcPaidTo = broker.ComplianceEaseExportConfig.sStateRtcPaidTo;
                newConfig.sRecFPaidTo = broker.ComplianceEaseExportConfig.sRecFPaidTo;
                newConfig.s1200RestPaidTo = broker.ComplianceEaseExportConfig.s1200RestPaidTo;
                newConfig.sPestInspectFPaidTo = broker.ComplianceEaseExportConfig.sPestInspectFPaidTo;
                newConfig.s1300RestPaidTo = broker.ComplianceEaseExportConfig.s1300RestPaidTo;

                broker.ComplianceEaseExportConfig = newConfig;

                if (broker.Save())
                {
                    return true;
                }

                throw new SystemException();
            }
            catch (SystemException)
            {
                Tools.LogError($"Failed to set ignored ComplianceEase statuses for broker {id.ToString()}.");
            }

            return false;
        }
    }
}
