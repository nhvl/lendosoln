﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using DataAccess;

    /// <summary>
    /// Populates the new Investor Program Identifier field for CCU's loan files.
    /// </summary>
    /// <remarks>Can be removed after 3/1/2019.</remarks>
    public class PopulateInvestorProgramIdForCcu : Migrator<Guid>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PopulateInvestorProgramIdForCcu"/> class.
        /// </summary>
        public PopulateInvestorProgramIdForCcu()
            : base(nameof(PopulateInvestorProgramIdForCcu))
        {
            var brokerId = Tools.GetBrokerIdByCustomerCode("PML0215");
            this.MigrationHelper = new LoansFromSingleBrokerMigrationHelper(nameof(PopulateInvestorProgramIdForCcu), brokerId);
        }

        /// <summary>
        /// Triggers the migration to run.
        /// </summary>
        /// <param name="args">String arguments.</param>
        public static void RunMigration(string[] args)
        {
            var migrator = new PopulateInvestorProgramIdForCcu();
            migrator.Migrate();
        }

        /// <summary>
        /// Sets the investor loan program ID to the value of sU1LockFieldDesc, where CCU is currently storing this data.
        /// </summary>
        /// <param name="loanId">The ID of the loan to migrate.</param>
        /// <returns>A boolean indicating whether the migration was successful.</returns>
        protected override bool MigrationFunction(Guid loanId)
        {
            try
            {
                var dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(PopulateInvestorProgramIdForCcu));
                dataLoan.InitSave();

                bool lockField1DescriptionHasValue = !string.IsNullOrEmpty(dataLoan.sU1LockFieldDesc);
                bool investorProgramIdIsEmpty = string.IsNullOrEmpty(dataLoan.sInvestorLockProgramId);

                if (lockField1DescriptionHasValue && investorProgramIdIsEmpty)
                {
                    dataLoan.sInvestorLockProgramId = dataLoan.sU1LockFieldDesc;
                    dataLoan.sU1LockFieldDesc = string.Empty;
                    dataLoan.Save();
                }

                return true;
            }
            catch (Exception exc) when (exc is SystemException || exc is CBaseException)
            {
                Tools.LogError($"Failed to set sInvestorLockProgramId for CCU loan {loanId}.", exc);
                return false;
            }
        }
    }
}