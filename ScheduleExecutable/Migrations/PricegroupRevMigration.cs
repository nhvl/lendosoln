﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LendersOffice.Migration;
using LendersOffice.ObjLib.PriceGroups;
using DataAccess;
using System.Threading;
using LendersOffice.Security;
using LendersOffice.ObjLib.PriceGroups.Model;

namespace ScheduleExecutable.Migrations
{
    class PricegroupRevMigration
    {
        public static void PopulatePricegroupRev(string[] args)
        {
            AbstractUserPrincipal principal = SystemUserPrincipal.TaskSystemUser;
            Thread.CurrentPrincipal = principal;

            var updateProductList = System.Linq.Enumerable.Empty<LpePriceGroupProduct>();

            int count = 0;
            int errCount = 0;

            foreach (var brokerId in Tools.GetAllActiveBrokers())
            {
                foreach (var priceGroup in PriceGroup.RetrieveAllFromBroker(brokerId))
                {
                    try
                    {
                        priceGroup.Save(principal, updateProductList);
                    }
                    catch( Exception exc)
                    {
                        ++errCount;
                        string errMsg = $"{Environment.NewLine}#error {errCount}: BrokerId {priceGroup.BrokerID}, priceGroupId {priceGroup.ID}: {exc.Message}";
                        Console.WriteLine(errMsg);
                        Tools.LogError(errMsg, exc);
                    }

                    if ((++count % 100) == 0)
                    {
                        Console.WriteLine(DateTime.Now + " - Processed " + count + " price groups, #error " + errCount);
                    }

                }
            }

            Console.WriteLine(DateTime.Now + " - Processed " + count + " price groups, #error " + errCount);
        }
    }
}
