﻿using System;
using System.Collections.Generic;
using System.Data;
using DataAccess;
using LendersOffice.Constants;
using LqbGrammar.DataTypes;

namespace ScheduleExecutable.Migrations
{
    public class Opm242144_UpdateTemplatesToAutoCalcInitAprAndLastDiscApr : Migrator<Guid>
    {
        public Opm242144_UpdateTemplatesToAutoCalcInitAprAndLastDiscApr()
            :base(nameof(Opm242144_UpdateTemplatesToAutoCalcInitAprAndLastDiscApr))
        {
            this.MigrationHelper = new CustomGUIDMigrationHelper(this.Name, GetAllTridLoanTemplateIds);
        }

        public static void Run(string[] args)
        {
            var migrator = new Opm242144_UpdateTemplatesToAutoCalcInitAprAndLastDiscApr();
            migrator.Migrate();
        }

        private IEnumerable<Guid> GetAllTridLoanTemplateIds()
        {
            var sql = @"SELECT lfc.sLId
                        FROM LOAN_FILE_CACHE lfc
                            JOIN LOAN_FILE_CACHE_4 lfc4 on lfc.sLId = lfc4.sLId
                            JOIN BROKER bro on lfc.sBrokerId = bro.BrokerId
                        WHERE 
                            lfc.IsValid = 1 
                            AND lfc.IsTemplate = 1 
                            AND lfc4.sDisclosureRegulationT = 2
                            AND bro.Status = 1";
            var tridTemplateIds = new List<Guid>();
            Action<IDataReader> readTemplateIds = reader =>
            {
                while (reader.Read())
                {
                    tridTemplateIds.Add((Guid)reader["sLId"]);
                }
            };

            foreach (var connection in DbConnectionInfo.ListAll())
            {
                DBSelectUtility.ProcessDBData(
                    connection,
                    sql,
                    TimeoutInSeconds.Thirty,
                    null,
                    readTemplateIds);
            }

            return tridTemplateIds;
        }

        protected override bool MigrationFunction(Guid templateId)
        {
            var tridTemplate = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                templateId,
                typeof(Opm242144_UpdateTemplatesToAutoCalcInitAprAndLastDiscApr));
            tridTemplate.InitSave(ConstAppDavid.SkipVersionCheck);

            tridTemplate.sCalculateInitAprAndLastDiscApr = true;

            tridTemplate.Save();
            return true;
        }
    }
}
