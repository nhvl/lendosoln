﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using DataAccess;
    using LendersOffice.Integration.Irs4506T;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Migrates sensitive data in LQB POST and Equifax 4506-T response XML.
    /// </summary>
    public class Opm467096_MigrateSensitiveDataIn4506TXml
    {
        public static void Run(string[] args)
        {
            var migrator = new Opm467096_MigrateSensitiveDataIn4506TXml();
            migrator.Migrate();
        }

        private void Migrate()
        {
            const string UpdateSql = @"
UPDATE IRS_4506T_ORDER_INFO
SET ResponseXmlContent = @ResponseXmlContent
WHERE TransactionId = @TransactionId";

            foreach (var order in this.GetOrders())
            {
                try
                {
                    string updatedResponseXml = null;

                    var xmlDoc = Tools.CreateXmlDoc(order.ResponseXmlContent);
                    if (xmlDoc.SelectSingleNode("OFX") != null)
                    {
                        var fileDbKey = Irs4506TOrderInfo.CreateFileDbKeyForResponseXml(order.BrokerId, order.TransactionId);
                        FileDBTools.WriteData(E_FileDB.Normal, fileDbKey, order.ResponseXmlContent);

                        // For Equifax 4506-T orders, store the FileDB key in the database
                        // instead of the content.
                        updatedResponseXml = fileDbKey;
                    }
                    else
                    {
                        updatedResponseXml = Irs4506TOrderInfo.MaskSensitiveLqbPostResponseData(xmlDoc);
                    }

                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@TransactionId", order.TransactionId),
                        new SqlParameter("@ResponseXmlContent", updatedResponseXml)
                    };

                    DBUpdateUtility.Update(order.BrokerId, UpdateSql, TimeoutInSeconds.Thirty, parameters);
                }
                catch (Exception e)
                {
                    Tools.LogError($"[OPM 467096] Unable to migrate 4506-T order. LoanId: {order.LoanId}, TransactionId: {order.TransactionId}", e);
                }
            }
        }

        private IEnumerable<Irs4506TOrder> GetOrders()
        {
            var brokerIdsByLoanId = new Dictionary<Guid, Guid>();
            var orders = new LinkedList<Irs4506TOrder>();

            Action<IDataReader> readerCode = reader =>
            {
                while (reader.Read())
                {
                    var order = new Irs4506TOrder()
                    {
                        LoanId = (Guid)reader["sLId"],
                        TransactionId = (Guid)reader["TransactionId"],
                        ResponseXmlContent = (string)reader["ResponseXmlContent"]
                    };

                    Guid brokerId;
                    if (!brokerIdsByLoanId.TryGetValue(order.LoanId, out brokerId))
                    {
                        try
                        {
                            brokerId = Tools.GetBrokerIdByLoanID(order.LoanId);
                        }
                        catch (Exception e)
                        {
                            Tools.LogError($"[OPM 467096] Failed to retrieve broker ID for loan {order.LoanId} in 4506-T migration, response XML should be manually updated.", e);
                            continue;
                        }

                        brokerIdsByLoanId.Add(order.LoanId, brokerId);
                    }

                    order.BrokerId = brokerId;
                    orders.AddLast(order);
                }
            };

            const string SelectSql = @"
SELECT TransactionId, sLId, ResponseXmlContent
FROM IRS_4506T_ORDER_INFO
WHERE DATALENGTH(ResponseXmlContent) > 0";

            foreach (var connection in DbConnectionInfo.ListAll())
            {
                DBSelectUtility.ProcessDBData(connection, SelectSql, TimeoutInSeconds.Sixty, null, readerCode);
            }

            return orders;
        }

        private class Irs4506TOrder
        {
            public Guid BrokerId { get; set; }
            public Guid TransactionId { get; set; }
            public Guid LoanId { get; set; }
            public string ResponseXmlContent { get; set; }
        }
    }
}
