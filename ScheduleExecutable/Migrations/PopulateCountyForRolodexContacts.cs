﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Linq;
    using DataAccess;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Retrieves each agent that does not have a county populated, and attempts to
    /// calculate the county using the zip code.
    /// </summary>
    public class PopulateCountyForRolodexContacts : Migrator<Guid>
    {
        /// <summary>
        /// A cache to hold retrieved counties by zip code.
        /// </summary>
        private Dictionary<string, string> cachedCounties;

        /// <summary>
        /// Gets a set of cached counties by zip code.
        /// </summary>
        /// <value>A set of cached counties by zip code.</value>
        public Dictionary<string, string> CachedCounties
        {
            get
            {
                if (this.cachedCounties == null)
                {
                    this.cachedCounties = new Dictionary<string, string>();
                }

                return this.cachedCounties;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PopulateCountyForRolodexContacts"/> class.
        /// </summary>
        /// <param name="name">The migration name.</param>
        public PopulateCountyForRolodexContacts() : base(nameof(PopulateCountyForRolodexContacts))
        {
            this.MigrationHelper = new BrokerMigrationHelper(nameof(PopulateCountyForRolodexContacts));
        }

        /// <summary>
        /// Triggers the migration to run.
        /// </summary>
        /// <param name="args">String arguments.</param>
        public static void RunMigration(string[] args)
        {
            var migrator = new PopulateCountyForRolodexContacts();
            migrator.Migrate();
        }

        /// <summary>
        /// Attempts to migrate the county field for agents associated with the given broker.
        /// </summary>
        /// <param name="brokerId">The broker to migrate.</param>
        /// <returns>A boolean indicating whether the migration was successful.</returns>
        protected override bool MigrationFunction(Guid brokerId)
        {
            var contactDictionary = this.GetContactsForBroker(brokerId);
            if (!contactDictionary.Any())
            {
                return true;
            }

            var failedAgents = new List<Guid>();

            var driverFactory = GenericLocator<IStoredProcedureDriverFactory>.Factory;
            var driver = driverFactory.Create(TimeoutInSeconds.Thirty);

            foreach (var agentId in contactDictionary.Keys)
            {
                try
                {
                    var county = this.GetCounty(contactDictionary[agentId]);

                    using (var connection = DbConnectionInfo.GetConnection(brokerId))
                    {
                        connection.Open();

                        if (!string.IsNullOrEmpty(county))
                        {
                            SqlParameter[] parameters =
                            {
                                new SqlParameter("@BrokerId", brokerId),
                                new SqlParameter("@AgentId", agentId),
                                new SqlParameter("@AgentCounty", county)
                            };

                            driver.ExecuteNonQuery(connection, null, StoredProcedureName.Create("PopulateAgentCountyForMigrator").Value, parameters);
                        }
                    }
                }
                catch (SystemException)
                {
                    failedAgents.Add(agentId);
                }
                catch (ApplicationException)
                {
                    failedAgents.Add(agentId);
                }
                catch (CBaseException)
                {
                    failedAgents.Add(agentId);
                }
            }

            if (failedAgents.Any())
            {
                var message = $"Failed to migrate the county field for the following agents under broker {brokerId}:{Environment.NewLine}";
                foreach (var agentId in failedAgents)
                {
                    message += Environment.NewLine + agentId;
                }

                Tools.LogError(message);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Retrieve the agents needing migration under a given broker.
        /// </summary>
        /// <param name="brokerId">The broker whose agents are being migrated.</param>
        /// <returns>A dictionary of agent zipcodes keyed by the agent ID.</returns>
        private Dictionary<Guid, string> GetContactsForBroker(Guid brokerId)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", brokerId)
            };

            var contacts = new Dictionary<Guid, string>();

            // Valid agents for migration have no County set (we don't want to overwrite
            // existing data), and a Zip code that can be used to calculate the County.
            var queryString = SQLQueryString.Create(@"
                SELECT AgentId, AgentZip
                FROM AGENT
                WHERE BrokerId = @BrokerId
                    AND AgentCounty = ''
                    AND AgentZip <> ''
            ");

            var factory = GenericLocator<ISqlDriverFactory>.Factory;
            var driver = factory.Create(TimeoutInSeconds.Thirty);

            using (var connection = DbConnectionInfo.GetConnection(brokerId))
            using (var reader = driver.Select(connection, transaction: null, query: queryString.Value, parameters: parameters))
            {
                while (reader.Read())
                {
                    var agentId = (Guid)reader["AgentId"];
                    var zip = (string)reader["AgentZip"];

                    contacts[agentId] = zip;
                }
            }

            return contacts;
        }

        /// <summary>
        /// Attempts to retrieve the county from the cache by zip code. If the zip code
        /// is not cached, it will hit the database instead.
        /// </summary>
        /// <param name="zip">The zip code.</param>
        /// <returns>The county associated with the zip code.</returns>
        private string GetCounty(string zip)
        {
            if (this.CachedCounties.ContainsKey(zip))
            {
                return this.CachedCounties[zip];
            }

            var county = Tools.GetCountyFromZipCode(zip);
            this.CachedCounties[zip] = county;
            return county;
        }
    }
}
