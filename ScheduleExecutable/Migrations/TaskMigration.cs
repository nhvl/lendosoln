﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.OleDb;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Threading;
    using DataAccess;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.Drivers.SqlServerDB;
    using LendersOffice.ObjLib.DatabaseMessageQueue;
    using LendersOffice.ObjLib.Task;
    using LendersOffice.Reminders;
    using LendersOffice.Security;
    using LendersOffice.Admin;
    using LqbGrammar.DataTypes;
    using Microsoft.VisualBasic.FileIO;

    class TaskMigration
    {
        class OldTask
        {
            public Guid DiscLogId;
            public Guid DiscRefObjId;
            public string DiscSubject;
            public int DiscStatus;
            public Guid DiscCreatorUserId;
            public DateTime DiscLastModifyD;
            public DateTime DiscCreatedDate;
            public DateTime DiscDueDate;
            public int? NamingIndex;

        }
        class LoanInfo
        {
            public Guid sLId;
            public string sLNm;
            public E_sStatusT sStatusT;
            public DateTime sEstClosedD;
            public DateTime sRLckdExpiredD;
            public bool IsTemplate;
        }

        class UserInfo
        {
            public Guid UserId;
            public Guid EmployeeId;
            public Guid BranchId;
            public string DisplayName;
            public string LoginName;
            public string UserFirstNm;
            public string UserLastNm;
        }

        private static Guid RoleUnderwriterId = new Guid("E0534F45-2AE1-4A8C-952E-00520F445819");
        public static void Execute(string[] args)
        {
            if (args.Length < 3)
            {
                Console.WriteLine("Usage: ScheduleExecutable TaskMigration [action] [brokerid] [starting_task]");
                Console.WriteLine("    [action]   - ConvertToNew");
                Console.WriteLine("               - Rollback");
                Console.WriteLine("               - ExportConditionChoices");
                Console.WriteLine("    [starting_task] - Required for Rollback action. This will roll back new task that is equal or greater in integer id.");
                return;

            }
            if (args[1] != "ConvertToNew" && args[1] != "ConvertOldTaskToNewTask" && args[1] != "Rollback" && args[1] != "ExportConditionChoices" && args[1] != "FixCateories")
            {
                Console.WriteLine("Usage: ScheduleExecutable TaskMigration [action] [brokerid] [starting_task]");
                Console.WriteLine("    [action]   - ConvertToNew");
                Console.WriteLine("               - Rollback");
                Console.WriteLine("               - ExportConditionChoices");
                Console.WriteLine("               - FixCateories");
                Console.WriteLine("    [starting_task] - Required for Rollback action. This will roll back new task that is equal or greater in integer id.");

                return;

            }

            Guid brokerId = new Guid(args[2]);
            string action = args[1];
            string mapFile = "";
 
            Dictionary<string, string> categoryMap = null;
            if (args.Length > 3)
            {
                mapFile = args[3];
                categoryMap = ParseConditionCategoryMap(mapFile);
            }

            var broker = BrokerDB.RetrieveById(brokerId);
            TaskMigration.GetAccountOwner(brokerId); //this is required so check before running.


            Console.WriteLine($"You are about to {action} for broker {broker.CustomerCode}-{broker.Name}. Are you sure? Enter y.");

            if (Console.ReadKey().KeyChar != 'y')
            {
                return;
            }


            AbstractUserPrincipal principal = SystemUserPrincipal.TaskSystemUser;
            Thread.CurrentPrincipal = principal;
            if (action == "ConvertToNew")
            {
                ConvertToNew(brokerId, categoryMap);
            }
            else if (action == "ConvertOldTaskToNewTask")
            {
                ConvertOldTaskToNewTask(BrokerDB.RetrieveById(brokerId));
            }
            else if (action == "Rollback")
            {

                string startingTask = args[2];
                RollbackNewTask(brokerId, startingTask);
            }
            else if (action == "ExportConditionChoices")
            {
                ExportConditionChoices(brokerId);
            }
            else if (action == "FixCateories")
            {
                FixCateories(brokerId, categoryMap);
            }
            else
            {
                throw new Exception("Invalid action: " + action);
            }
        }

        private static void ExportConditionChoices(Guid brokerId)
        {
            BrokerDB brokerDB = BrokerDB.RetrieveById(brokerId);
            Console.WriteLine("ExportConditionChoices for broker " + brokerDB.Name);

            Dictionary<string, int> conditionCategoryDictionary = ConditionCategory.GetCategories(brokerDB.BrokerID).ToDictionary(o => o.Category.ToLower(), o => o.Id);
            int managedPermissionLevelId = GetManagedTaskPermissionLevelId(brokerId);
            ChoiceList choiceList = brokerDB.ConditionChoices;

            foreach (ChoiceItem choice in choiceList.Choices)
            {
                ConditionChoice conditionChoice = new ConditionChoice(brokerId);
                int categoryId;
                if (conditionCategoryDictionary.TryGetValue(choice.Category.ToLower(), out categoryId) == false)
                {
                    ConditionCategory category = new ConditionCategory(brokerId);
                    category.Category = choice.Category;
                    category.DefaultTaskPermissionLevelId = managedPermissionLevelId;
                    category.Save();

                    categoryId = category.Id;
                    conditionCategoryDictionary.Add(choice.Category.ToLower(), categoryId);
                }
                conditionChoice.CategoryId = conditionCategoryDictionary[choice.Category.ToLower()];
                conditionChoice.ConditionSubject = choice.Description;
                conditionChoice.ToBeAssignedRoleId = new Guid("89572CFA-A3EE-4513-9B25-71A212AD5D09"); // Processor Role
                conditionChoice.ToBeOwnedByRoleId = new Guid("E0534F45-2AE1-4A8C-952E-00520F445819"); // Underwriter Role
                conditionChoice.DueDateFieldName = "sEstCloseD";
                conditionChoice.DueDateAddition = 0;
                conditionChoice.ConditionType = E_ConditionType.Misc;
                conditionChoice.sLT = E_ConditionChoice_sLT.Any;
                conditionChoice.Save(brokerId);
            }

        }

        private static void RollbackNewTask(Guid brokerId, string startingTaskId)
        {
            BrokerDB brokerDB = BrokerDB.RetrieveById(brokerId);
            Console.WriteLine("Rollback for broker " + brokerDB.Name + ", starting id=" + startingTaskId);
            if (brokerDB.HasLenderDefaultFeatures)
            {
                RollbackToOldCondition(brokerDB, startingTaskId);
            }
            else
            {
                RollbackToOldXmlCondition(brokerDB, startingTaskId);
            }
            RollbackNewTaskToOldTask(brokerDB, startingTaskId);
        }


        private static void FixCateories(Guid brokerId, Dictionary<string, string> categoryMap)
        {
            var categories = ConditionCategory.GetCategories(brokerId).OrderBy(p => p.Id);
            int countofMatch = 0;
            var existingCategoryMap = categories.ToDictionary(p => p.Category, StringComparer.OrdinalIgnoreCase);
            foreach (var category in categories)
            {
                string desc = category.Category.Trim();

                if (!categoryMap.ContainsKey(desc))
                {
                    System.Console.WriteLine("Missing category from excel: --{0}--. Not going to modify.", desc);
                    continue;
                }

                if (categoryMap[desc].Trim().Equals(desc.Trim(), StringComparison.OrdinalIgnoreCase))
                {
                    System.Console.WriteLine("Ignoring category {0} because the mapping has it with key =value", desc);
                    continue;
                }

                string dst = categoryMap[desc]; //get the path where this category is suppose to go to
                int dstid = existingCategoryMap[dst].Id;
                int srcid = category.Id;

                var sqlParameters = new List<SqlParameter>()
                {
                    new SqlParameter("@BrokerId", brokerId),
                    new SqlParameter("@CondCategorySrc", srcid),
                    new SqlParameter("@CondCategoryDst", dstid)
                };

                var affectedTaskRowsParameter = new SqlParameter("@AffectedTaskRows", SqlDbType.Int) { Direction = ParameterDirection.Output };
                var affectedCondChoiceRowsParameter = new SqlParameter("@AffectedConditionChoiceRows", SqlDbType.Int) { Direction = ParameterDirection.Output };

                sqlParameters.Add(affectedTaskRowsParameter);
                sqlParameters.Add(affectedCondChoiceRowsParameter);

                StoredProcedureHelper.ExecuteNonQuery(brokerId, "TASK_MigrateCategory", nRetry: 0, parameters: sqlParameters);

                System.Console.WriteLine("Migrated condition ({0}::{1}) to ({2}::{3}) {4} cond, {5} cond choice affected. ", category.Id, category.Category, existingCategoryMap[dst].Id, existingCategoryMap[dst].Category, affectedTaskRowsParameter.Value, affectedCondChoiceRowsParameter.Value);
                ConditionCategory.Delete(brokerId, srcid);
                System.Console.WriteLine("Deleted condition ({0}::{1})", category.Id, category.Category);
                countofMatch += 1;
            }

            System.Console.WriteLine("{0} Condition Categories migrated.", countofMatch);
        }


        #region Rollback Methods
        private static void RollbackToOldCondition(BrokerDB brokerDB, string startingTaskId)
        {
            Console.WriteLine("Rollback Condition to Old Condition");
            Guid sLId = Guid.Empty;

            var listParams = new SqlParameter[] { DbAccessUtils.SqlParameterForVarchar("@start", startingTaskId), new SqlParameter("@brokerid", brokerDB.BrokerID) };
            string sql = "SELECT TaskId, TaskSubject, LoanId, CondCategoryId, TaskStatus, TaskClosedDate, CondInternalNotes, CondIsHidden, TaskCreatedDate, TaskClosedUserFullName FROM TASK WHERE TaskIsCondition=1 AND CondIsDeleted=0 AND  TaskId >= @start AND BrokerId=@brokerid AND Substring(TaskId, 1, 1) <> 'T' ORDER BY LoanId";

            Dictionary<int, string> conditionCategoryDictionary = ConditionCategory.GetCategories(brokerDB.BrokerID).ToDictionary(o => o.Id, o => o.Category);
            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                while (reader.Read())
                {
                    string taskId = (string)reader["TaskId"];
                    try
                    {

                        using (CStoredProcedureExec spExec = new CStoredProcedureExec(brokerDB.BrokerID))
                        {
                            spExec.BeginTransactionForWrite();
                            var oldCondition = new CLoanConditionObsolete(brokerDB.BrokerID);

                            oldCondition.CondCategoryDesc = conditionCategoryDictionary[(int)reader["CondCategoryId"]]; ;
                            oldCondition.CondDesc = (string)reader["TaskSubject"];
                            oldCondition.CondIsHidden = (bool)reader["CondIsHidden"];
                            oldCondition.LoanId = (Guid)reader["LoanId"];
                            oldCondition.CondNotes = (string)reader["CondInternalNotes"];
                            oldCondition.CondStatus = (byte)reader["TaskStatus"] == 0 ? E_CondStatus.Active : E_CondStatus.Done;
                            if (oldCondition.CondStatus == E_CondStatus.Done)
                            {
                                if (reader["TaskClosedDate"] != DBNull.Value)
                                {
                                    oldCondition.CondStatusDate = (DateTime)reader["TaskClosedDate"];
                                    oldCondition.CondCompletedByUserNm = (string)reader["TaskClosedUserFullName"];
                                }
                                if (string.IsNullOrEmpty(oldCondition.CondCompletedByUserNm))
                                {
                                    // 6/30/2011 dd - Old condition does not allow this field to be empty when condition is done.
                                    oldCondition.CondCompletedByUserNm = "Unknown";
                                }
                            }
                            oldCondition.Save(spExec);
                            spExec.CommitTransaction();
                        }

                        Console.WriteLine("Task #: " + taskId + " ROLLBACK SUCCESSFULLY.");
                    }
                    catch (Exception exc)
                    {
                        Console.WriteLine("TaskId:" + taskId + " FAILED. " + exc.Message + Environment.NewLine + exc.StackTrace);
                        throw; // 12/16/2011 dd - Do not proceed.
                    }
                }
            };

            DBSelectUtility.ProcessDBData(brokerDB.BrokerID, sql, null, listParams, readHandler);
        }

        private static void RollbackToOldXmlCondition(BrokerDB brokerDB, string startingTaskId)
        {
            Console.WriteLine("Rollback Condition to Old Xml Condition");

            Guid sLId = Guid.Empty;


            var listParams = new SqlParameter[] { DbAccessUtils.SqlParameterForVarchar("@start", startingTaskId), new SqlParameter("@brokerid", brokerDB.BrokerID) };
            string sql = "SELECT TaskId, TaskSubject, LoanId, CondCategoryId, TaskStatus, TaskClosedDate FROM TASK WHERE TaskIsCondition=1 AND CondIsDeleted=0 AND  TaskId >= @start AND BrokerId=@brokerid AND Substring(TaskId, 1, 1) <> 'T' ORDER BY LoanId";

            CPageData dataLoan = null;
            Dictionary<int, string> conditionCategoryDictionary = ConditionCategory.GetCategories(brokerDB.BrokerID).ToDictionary(o => o.Id, o => o.Category);
            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                while (reader.Read())
                {
                    string taskId = (string)reader["TaskId"];
                    Guid loanId = (Guid)reader["LoanId"];
                    try
                    {
                        if (loanId != sLId)
                        {
                            if (dataLoan != null)
                            {
                                dataLoan.Save();
                            }
                            sLId = loanId;
                            dataLoan = CreateCPageData(sLId);
                            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                        }

                        var oldCondition = dataLoan.GetCondFieldsObsolete(Guid.Empty);
                        oldCondition.CondDesc = (string)reader["TaskSubject"];
                        oldCondition.IsDone = (byte)reader["TaskStatus"] == 0 ? false : true;
                        if ((byte)reader["TaskStatus"] == 2)
                        {
                            if (reader["TaskClosedDate"] != DBNull.Value)
                            {
                                oldCondition.DoneDate = CDateTime.Create((DateTime)reader["TaskClosedDate"]);
                            }
                        }
                        oldCondition.PriorToEventDesc = conditionCategoryDictionary[(int)reader["CondCategoryId"]];

                        oldCondition.Update();
                        Console.WriteLine("Task #" + taskId + " ROLLBACK SUCCESSFULLY");
                    }
                    catch (Exception exc)
                    {
                        Console.WriteLine("TaskId:" + taskId + " FAILED. " + exc.Message + Environment.NewLine + exc.StackTrace);
                        throw;
                    }
                }
            };

            DBSelectUtility.ProcessDBData(brokerDB.BrokerID, sql, null, listParams, readHandler);

            if (dataLoan != null)
            {
                dataLoan.Save();
            }
        }

        private static void RollbackNewTaskToOldTask(BrokerDB brokerDB, string startingTaskId)
        {
            Console.WriteLine("Rollback New Task to Old Task");
            var activeUserList = ListActiveLoUsers(brokerDB.BrokerID);

            Guid accountOwnerId = GetAccountOwner(brokerDB.BrokerID);

            var listParams = new SqlParameter[] { DbAccessUtils.SqlParameterForVarchar("@start", startingTaskId), new SqlParameter("@brokerid", brokerDB.BrokerID) };
            string sql = "SELECT TaskOwnerUserId, TaskCreatedDate, TaskSubject, TaskDueDate, BorrowerNmCached, LoanNumCached, LoanId, TaskStatus FROM TASK WHERE TaskIsCondition=0 AND TaskId >= @start AND BrokerId=@brokerid AND Substring(TaskId, 1, 1) <> 'T'";

            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                while (reader.Read())
                {
                    Guid taskOwnerUserId = (Guid)reader["TaskOwnerUserId"];
                    DateTime taskCreatedDate = (DateTime)reader["TaskCreatedDate"];
                    string taskSubject = (string)reader["TaskSubject"];
                    DateTime taskDueDate = DateTime.MinValue;
                    if (reader["TaskDueDate"] != DBNull.Value)
                    {
                        taskDueDate = (DateTime)reader["TaskDueDate"];
                    }
                    string borrowerNmCached = (string)reader["BorrowerNmCached"];
                    string loanNumCached = (string)reader["LoanNumCached"];
                    Guid loanId = (Guid)reader["LoanId"];
                    byte taskStatus = (byte)reader["TaskStatus"]; // 0 - Active, 1 - Resolved, 2 - Closed
                    try
                    {
                        UserInfo user = null;
                        if (activeUserList.TryGetValue(taskOwnerUserId, out user) == false)
                        {
                            if (activeUserList.TryGetValue(accountOwnerId, out user) == false)
                            {
                                throw new Exception("UserId=" + taskOwnerUserId + " is not an active user");
                            }
                        }
                        CDiscNotif oldTask = new CDiscNotif(brokerDB.BrokerID, Guid.Empty, Guid.Empty);

                        oldTask.DiscCreatorUserId = user.UserId;
                        oldTask.DiscCreatorLoginNm = user.LoginName;
                        oldTask.DiscStatus = taskStatus == 0 ? E_DiscStatus.Active : E_DiscStatus.Done;
                        oldTask.DiscRefObjId = loanId;
                        oldTask.DiscRefObjType = "Loan_File";
                        oldTask.DiscRefObjNm1 = loanNumCached;
                        oldTask.DiscRefObjNm2 = borrowerNmCached;
                        oldTask.DiscSubject = taskSubject;
                        //oldTask.DiscHistory = 
                        if (taskDueDate != DateTime.MinValue)
                        {
                            oldTask.DiscDueDate = taskDueDate;
                        }
                        oldTask.Save(user.DisplayName, false, false, false, DateTime.Now);
                        ParticipantEntry entry = new ParticipantEntry(user.UserFirstNm, user.UserLastNm, user.UserId, false);
                        ArrayList entryList = new ArrayList();
                        entryList.Add(entry);
                        oldTask.Join(user.UserId, entryList);

                    }
                    catch (Exception exc)
                    {
                        Console.Write("   TaskId:" + reader["TaskId"] + " FAILED. " + exc.Message + Environment.NewLine + exc.StackTrace);
                        throw;
                    }
                }
            };

            DBSelectUtility.ProcessDBData(brokerDB.BrokerID, sql, null, listParams, readHandler);
        }
        #endregion
        private static void ConvertToNew(Guid brokerId, Dictionary<string, string> condCategoryMap)
        {
            BrokerDB brokerDB = BrokerDB.RetrieveById(brokerId);
            Console.WriteLine("Migrate to new task/condition for broker " + brokerDB.Name);

            if (brokerDB.HasLenderDefaultFeatures)
            {
                ConvertOldConditionToNewCondition(brokerDB, condCategoryMap);
            }
            else
            {
                ConvertOldXmlConditionToNewCondition(brokerDB, condCategoryMap);
            }


            ConvertOldTaskToNewTask(brokerDB);

        }

        private static void ConvertOldTaskToNewTask(BrokerDB brokerDB)
        {
            Console.WriteLine("Migrate old task to new task");

            // Find Account Owner of BrokerDB.
            Guid accountOwnerUserId = GetAccountOwner(brokerDB.BrokerID);

            // Get Default Task Permission Level. Maybe get General.
            int taskPermissionLevelId = GetGeneralTaskPermissionLevelId(brokerDB.BrokerID);

            Dictionary<Guid, UserInfo> activeUserList = ListActiveLoUsers(brokerDB.BrokerID);

            Dictionary<Guid, LoanInfo> loanDictionary = new Dictionary<Guid, LoanInfo>();

            // Step 1 - List all active tasks belong to active loans.
            var listParams = new SqlParameter[] { new SqlParameter("@brokerid", brokerDB.BrokerID) };
            string sql = "SELECT DiscLogId, DiscRefObjId, DiscSubject, DiscStatus, DiscCreatorUserId, DiscLastModifyD, DiscCreatedDate, DiscDueDate FROM DISCUSSION_LOG WHERE DiscBrokerId=@brokerid AND DiscRefObjId <> '00000000-0000-0000-0000-000000000000' AND IsValid=1 AND DiscIsRefObjValid=1 ORDER BY DiscRefObjId";
            List<OldTask> oldTaskList = new List<OldTask>();

            List<Guid> uniqueLoanIdList = new List<Guid>();

            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                while (reader.Read())
                {
                    OldTask oldTask = new OldTask();
                    oldTask.DiscLogId = (Guid)reader["DiscLogId"];
                    oldTask.DiscRefObjId = (Guid)reader["DiscRefObjId"];
                    oldTask.DiscSubject = (string)reader["DiscSubject"];
                    oldTask.DiscStatus = (int)reader["DiscStatus"]; // 0 - Active 
                    oldTask.DiscCreatorUserId = (Guid)reader["discCreatorUserId"];
                    oldTask.DiscLastModifyD = (DateTime)reader["DiscLastModifyD"];
                    oldTask.DiscCreatedDate = (DateTime)reader["DiscCreatedDate"];
                    if (reader["DiscDueDate"] == DBNull.Value)
                    {
                        oldTask.DiscDueDate = DateTime.MinValue;
                    }
                    else
                    {
                        oldTask.DiscDueDate = (DateTime)reader["DiscDueDate"];
                    }

                    oldTaskList.Add(oldTask);
                }
            };

            DBSelectUtility.ProcessDBData(brokerDB.BrokerID, sql, null, listParams, readHandler);

            foreach (var oldTask in oldTaskList)
            {
                MigrateOldTaskObjectToNewTaskObject(brokerDB, accountOwnerUserId, taskPermissionLevelId, activeUserList, loanDictionary, uniqueLoanIdList, oldTask, new Dictionary<Guid, Guid>());
            }

            Console.WriteLine();
            Console.WriteLine("Migrate Task without loan");
            Console.WriteLine();
            // Step 2 - List all active task does not belong to loan.
            listParams = new SqlParameter[] { new SqlParameter("@brokerid", brokerDB.BrokerID) }; // we have to re-create because a SqlParameter cannot be re-used.
            sql = "SELECT DiscLogId, DiscRefObjId, DiscSubject, DiscStatus, DiscCreatorUserId, DiscLastModifyD, DiscCreatedDate, DiscDueDate FROM DISCUSSION_LOG WHERE DiscBrokerId=@brokerid AND DiscRefObjId = '00000000-0000-0000-0000-000000000000' AND IsValid=1 ORDER BY DiscRefObjId";
            oldTaskList = new List<OldTask>();

            Action<IDataReader> readHandler2 = delegate(IDataReader reader)
            {
                while (reader.Read())
                {
                    OldTask oldTask = new OldTask();
                    oldTask.DiscLogId = (Guid)reader["DiscLogId"];
                    //oldTask.DiscRefObjId = (Guid)reader["DiscRefObjId"];
                    oldTask.DiscRefObjId = Guid.Empty;
                    oldTask.DiscSubject = (string)reader["DiscSubject"];
                    oldTask.DiscStatus = (int)reader["DiscStatus"]; // 0 - Active 
                    oldTask.DiscCreatorUserId = (Guid)reader["discCreatorUserId"];
                    oldTask.DiscLastModifyD = (DateTime)reader["DiscLastModifyD"];
                    oldTask.DiscCreatedDate = (DateTime)reader["DiscCreatedDate"];
                    if (reader["DiscDueDate"] == DBNull.Value)
                    {
                        oldTask.DiscDueDate = DateTime.MinValue;
                    }
                    else
                    {
                        oldTask.DiscDueDate = (DateTime)reader["DiscDueDate"];
                    }
                    oldTaskList.Add(oldTask);
                    oldTask.NamingIndex = oldTaskList.Count;
                }
            };

            DBSelectUtility.ProcessDBData(brokerDB.BrokerID, sql, null, listParams, readHandler2);

            Dictionary<Guid, Guid> userIdAndLoanIdDictionary = new Dictionary<Guid, Guid>();
            foreach (var oldTask in oldTaskList)
            {
                MigrateOldTaskObjectToNewTaskObject(brokerDB, accountOwnerUserId, taskPermissionLevelId, activeUserList,
                    loanDictionary, uniqueLoanIdList, oldTask, userIdAndLoanIdDictionary);

            }
            // Go through a list of loan and update task statistics.
            foreach (var sLId in uniqueLoanIdList)
            {
                TaskUtilities.UpdateTasksDueDateByLoan(brokerDB.BrokerID, sLId);
            }
        }

        private static Guid CreateToDoListLoan(OldTask oldTask, BrokerDB brokerDB, Guid accountOwnerUserId, Dictionary<Guid, UserInfo> activeUserList, Dictionary<Guid, Guid> userIdAndLoanIdDictionary)
        {
            Guid sLId = Guid.Empty;

            if (userIdAndLoanIdDictionary.TryGetValue(oldTask.DiscCreatorUserId, out sLId) == false)
            {
                UserInfo user = null;

                if (activeUserList.TryGetValue(oldTask.DiscCreatorUserId, out user) == false)
                {
                    if (activeUserList.TryGetValue(accountOwnerUserId, out user) == false)
                    {
                        throw new Exception("There is no active user=" + oldTask.DiscCreatorUserId + ", and no active account owner=" + accountOwnerUserId);
                    }
                }
                sLId = Guid.NewGuid();
                int i = 1;
                if (oldTask.NamingIndex.HasValue)
                {
                    i = oldTask.NamingIndex.Value;
                }
                int maxRetries = i + 300;
                for (; i < maxRetries; i++)
                {
                    string sLNm = "TO-DO LIST - " + i;



                    SqlParameter[] parameters = {
                                                    new SqlParameter("@BrokerId", brokerDB.BrokerID),
                                                    DbAccessUtils.SqlParameterForVarchar("@LoanNumber", sLNm)
                                                };
                    using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerDB.BrokerID, "IsLoanNumberExisting", parameters))
                    {
                        if (reader.Read())
                        {
                            Console.WriteLine("IsLoanNumberExisting should never continue.");
                            continue; // Try next loan number.
                        }
                    }

                    SqlParameter outLoanId = new SqlParameter("@LoanId", SqlDbType.UniqueIdentifier);
                    outLoanId.Direction = ParameterDirection.Output;
                    SqlParameter[] createLoanParameters = {
                                                              new SqlParameter("@EmployeeId", user.EmployeeId),
                                                              new SqlParameter("@BranchId", user.BranchId),
                                                              DbAccessUtils.SqlParameterForVarchar("@sLNm", sLNm),
                                                              new SqlParameter("@InLoanId", sLId),
                                                              outLoanId
                                                          };
                    StoredProcedureHelper.ExecuteNonQuery(brokerDB.BrokerID, "CreateBlankLoanFile", 2, createLoanParameters);

                    SqlParameter[] arrParams = new SqlParameter[] { new SqlParameter("@slid", sLId) };

                    string sql = "UPDATE LOAN_FILE_A SET IsValid=1 WHERE sLId=@slid";

                    DBUpdateUtility.Update(brokerDB.BrokerID, sql, null, arrParams);

                    CPageData dataLoan = CreateCPageData(sLId);
                    dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                    dataLoan.sStatusT = E_sStatusT.Loan_Other;
                    dataLoan.sEmployeeLoanRepId = user.EmployeeId;
                    dataLoan.sEmployeeManagerId = user.EmployeeId;
                    dataLoan.sEmployeeProcessorId = user.EmployeeId;
                    dataLoan.sEmployeeCallCenterAgentId = user.EmployeeId;
                    dataLoan.sEmployeeCloserId = user.EmployeeId;
                    dataLoan.sEmployeeLenderAccExecId = user.EmployeeId;
                    dataLoan.sEmployeeLoanOpenerId = user.EmployeeId;
                    dataLoan.sEmployeeLockDeskId = user.EmployeeId;
                    dataLoan.sEmployeeUnderwriterId = user.EmployeeId;
                    dataLoan.sEmployeeShipperId = user.EmployeeId;
                    dataLoan.sEmployeeFunderId = user.EmployeeId;
                    dataLoan.sEmployeePostCloserId = user.EmployeeId;
                    dataLoan.sEmployeeInsuringId = user.EmployeeId;
                    dataLoan.sEmployeeCollateralAgentId = user.EmployeeId;
                    dataLoan.sEmployeeDocDrawerId = user.EmployeeId;
                    dataLoan.sEmployeeCreditAuditorId = user.EmployeeId;
                    dataLoan.sEmployeeDisclosureDeskId = user.EmployeeId;
                    dataLoan.sEmployeeJuniorProcessorId = user.EmployeeId;
                    dataLoan.sEmployeeJuniorUnderwriterId = user.EmployeeId;
                    dataLoan.sEmployeeLegalAuditorId = user.EmployeeId;
                    dataLoan.sEmployeeLoanOfficerAssistantId = user.EmployeeId;
                    dataLoan.sEmployeePurchaserId = user.EmployeeId;
                    dataLoan.sEmployeeQCComplianceId = user.EmployeeId;
                    dataLoan.sEmployeeSecondaryId = user.EmployeeId;
                    dataLoan.sEmployeeServicingId = user.EmployeeId;

                    dataLoan.Save();
                    break;
                }
                userIdAndLoanIdDictionary.Add(oldTask.DiscCreatorUserId, sLId);
            }

            return sLId;
        }
        private static void MigrateOldTaskObjectToNewTaskObject(BrokerDB brokerDB, Guid accountOwnerUserId, int taskPermissionLevelId, Dictionary<Guid, UserInfo> activeUserList, Dictionary<Guid, LoanInfo> loanDictionary, List<Guid> uniqueLoanIdList, OldTask oldTask, Dictionary<Guid, Guid> userIdAndLoanIdDictionary)
        {
            try
            {
                if (oldTask.DiscRefObjId == Guid.Empty)
                {
                    oldTask.DiscRefObjId = CreateToDoListLoan(oldTask, brokerDB, accountOwnerUserId, activeUserList, userIdAndLoanIdDictionary);
                }

                Task newTask = Task.CreateWithoutPermissionCheck(oldTask.DiscRefObjId, brokerDB.BrokerID);
                newTask.TaskIsCondition = false;
                if (string.IsNullOrEmpty(oldTask.DiscSubject))
                {
                    newTask.TaskSubject = "<blank>";
                }
                else
                {
                    newTask.TaskSubject = oldTask.DiscSubject;
                }
                string oldTaskHistory = string.Empty;
                try
                {
                    CFileDBFields fileDBFields = new CFileDBFields(oldTask.DiscLogId);
                    oldTaskHistory = fileDBFields.Load("DiscussionLog");
                }
                catch { }
                newTask.Comments = oldTaskHistory;
                newTask.TaskPermissionLevelId = taskPermissionLevelId;
                LoanInfo loanInfo = GetLoanInfo(brokerDB.BrokerID, oldTask.DiscRefObjId, loanDictionary);

                if (uniqueLoanIdList.Contains(oldTask.DiscRefObjId) == false)
                {
                    uniqueLoanIdList.Add(oldTask.DiscRefObjId);
                }
                if (activeUserList.ContainsKey(oldTask.DiscCreatorUserId))
                {
                    newTask.TaskOwnerUserId = oldTask.DiscCreatorUserId;
                    newTask.TaskAssignedUserId = oldTask.DiscCreatorUserId;
                }
                else
                {
                    // Assign to account owner.
                    newTask.TaskOwnerUserId = accountOwnerUserId;
                    newTask.TaskAssignedUserId = accountOwnerUserId;
                }
                if (oldTask.DiscStatus == 0)
                {
                    if (oldTask.DiscLastModifyD < DateTime.Now.AddMonths(-6))
                    {
                        newTask.TaskDueDate = oldTask.DiscLastModifyD;
                        newTask.TaskStatus = E_TaskStatus.Closed;
                    }
                    else if (oldTask.DiscLastModifyD == oldTask.DiscCreatedDate && oldTask.DiscLastModifyD < DateTime.Now.AddMonths(-3))
                    {
                        newTask.TaskDueDate = oldTask.DiscLastModifyD;
                        newTask.TaskStatus = E_TaskStatus.Closed;
                    }
                    else
                    {
                        E_sStatusT sStatusT = GetLoanStatus(brokerDB.BrokerID, oldTask.DiscRefObjId, loanDictionary);
                        switch (sStatusT)
                        {
                            //case E_sStatusT.Loan_Closed:
                            case E_sStatusT.Lead_Canceled:
                            case E_sStatusT.Loan_Canceled:
                            case E_sStatusT.Loan_Rejected:
                            case E_sStatusT.Lead_Declined:
                            //case E_sStatusT.Loan_Recorded:
                            //case E_sStatusT.Loan_Shipped:
                            case E_sStatusT.Loan_LoanPurchased:
                            case E_sStatusT.Loan_Withdrawn:
                            case E_sStatusT.Loan_Archived:
                            case E_sStatusT.Loan_Purchased:
                                newTask.TaskDueDate = oldTask.DiscLastModifyD;
                                newTask.TaskStatus = E_TaskStatus.Closed;
                                break;
                            default:
                                newTask.TaskStatus = E_TaskStatus.Active;
                                break;
                        }
                    }
                }
                else
                {
                    newTask.TaskDueDate = oldTask.DiscLastModifyD;
                    newTask.TaskStatus = E_TaskStatus.Closed;
                }

                if (oldTask.DiscDueDate == DateTime.MinValue)
                {
                    if (newTask.TaskStatus == E_TaskStatus.Active)
                    {
                        if (loanInfo.sEstClosedD != DateTime.MinValue)
                        {
                            newTask.TaskDueDate = loanInfo.sEstClosedD;
                        }
                        else if (loanInfo.sRLckdExpiredD != DateTime.MinValue)
                        {
                            newTask.TaskDueDate = loanInfo.sRLckdExpiredD;
                        }
                    }
                }
                else
                {
                    newTask.TaskDueDate = oldTask.DiscDueDate;
                }

                if (newTask.TaskStatus == E_TaskStatus.Closed && loanInfo.IsTemplate == true)
                {
                    Console.WriteLine("sLNm:" + loanInfo.sLNm + ", Task is closed and loan is template. Skip");
                }
                else
                {

                    // 12/16/2011 dd - Each task MUST HAVE a due date. Therefore if task is missing due date then I will
                    // set it to old task last modify d.
                    if (newTask.TaskDueDate.HasValue == false)
                    {
                        newTask.TaskDueDate = oldTask.DiscLastModifyD;
                    }

                    // Will update statistics at the end.
                    RunActionWithRetry(() => newTask.Save(false), 3);

                    List<Guid> participantUserList = ListActiveParticipants(brokerDB.BrokerID, oldTask.DiscLogId);
                    foreach (var userid in participantUserList)
                    {
                        if (activeUserList.ContainsKey(userid))
                        {
                            RunActionWithRetry(() => TaskSubscription.Subscribe(brokerDB.BrokerID, newTask.TaskId, userid), 3);
                        }
                    }
                    Console.WriteLine("sLNm:" + loanInfo.sLNm + ", Task #:" + newTask.TaskId + " created successfully.");

                }

            }
            catch (Exception exc)
            {
                Console.WriteLine("OldTask: " + oldTask.DiscLogId + " failed. " + Environment.NewLine + exc + Environment.NewLine + exc.StackTrace);
                Tools.LogError("OldTask: " + oldTask.DiscLogId + " failed", exc);
                throw;

            }
        }

        private static List<Guid> ListActiveParticipants(Guid brokerId, Guid discLogId)
        {
            // Unit tested by copying to the test module
            List<Guid> userList = new List<Guid>();
            string sql = "SELECT NotifReceiverUserId FROM DISCUSSION_NOTIFICATION WHERE NotifIsValid=1 AND DiscLogId=@disc";
            var listParams = new SqlParameter[] { new SqlParameter("@disc", discLogId) };

            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                while (reader.Read())
                {
                    userList.Add((Guid)reader["NotifReceiverUserId"]);
                }
            };

            DBSelectUtility.ProcessDBData(brokerId, sql, null, listParams, readHandler);

            return userList;
        }

        private static E_sStatusT GetLoanStatus(Guid brokerId, Guid sLId, Dictionary<Guid, LoanInfo> dictionary)
        {
            LoanInfo loanInfo = GetLoanInfo(brokerId, sLId, dictionary);
            return loanInfo.sStatusT;
        }

        private static LoanInfo GetLoanInfo(Guid brokerId, Guid sLId, Dictionary<Guid, LoanInfo> dictionary)
        {
            // Unit tested by copying to the testing library
            LoanInfo loanInfo;
            if (dictionary.TryGetValue(sLId, out loanInfo) == false)
            {
                string sql = "SELECT sStatusT, sEstCloseD,sRLckdExpiredD, sLNm, IsTemplate FROM LOAN_FILE_CACHE WHERE IsValid=1 AND sLId=@slid";
                var listParams = new SqlParameter[] { new SqlParameter("@slid", sLId) };

                Action<IDataReader> readHandler = delegate(IDataReader reader)
                {
                    loanInfo = new LoanInfo();
                    loanInfo.sLId = sLId;
                    if (reader.Read())
                    {
                        loanInfo.sStatusT = (E_sStatusT)reader["sStatusT"];
                        loanInfo.sLNm = (string)reader["sLNm"];
                        if (reader["sEstCloseD"] != DBNull.Value)
                        {
                            loanInfo.sEstClosedD = (DateTime)reader["sEstCloseD"];
                        }
                        else
                        {
                            loanInfo.sEstClosedD = DateTime.MinValue;
                        }
                        if (reader["sRLckdExpiredD"] != DBNull.Value)
                        {
                            loanInfo.sRLckdExpiredD = (DateTime)reader["sRLckdExpiredD"];
                        }
                        else
                        {
                            loanInfo.sRLckdExpiredD = DateTime.MinValue;
                        }
                        loanInfo.IsTemplate = (bool)reader["IsTemplate"];
                    }
                    else
                    {
                        loanInfo.sStatusT = E_sStatusT.Loan_Canceled;
                        loanInfo.sEstClosedD = DateTime.MinValue;
                        loanInfo.sRLckdExpiredD = DateTime.MinValue;
                        loanInfo.IsTemplate = true;
                    }
                };

                DBSelectUtility.ProcessDBData(brokerId, sql, null, listParams, readHandler);

                dictionary.Add(sLId, loanInfo);
            }
            return loanInfo;
        }

        private static Dictionary<Guid, UserInfo> ListActiveLoUsers(Guid brokerId)
        {
            // Unit tested by copying to the test module
            string sql = "SELECT UserId, BranchId, EmployeeId, LoginNm, UserFirstNm, UserLastNm FROM VIEW_ACTIVE_LO_USER WHERE BrokerId=@brokerid";
            var listParams = new SqlParameter[] { new SqlParameter("@brokerid", brokerId) };

            Dictionary<Guid, UserInfo> list = new Dictionary<Guid, UserInfo>();
            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                while (reader.Read())
                {
                    UserInfo user = new UserInfo();
                    user.UserId = (Guid)reader["UserId"];
                    user.EmployeeId = (Guid)reader["EmployeeId"];
                    user.BranchId = (Guid)reader["BranchId"];
                    user.LoginName = (string)reader["LoginNm"];
                    user.DisplayName = (string)reader["UserFirstNm"] + " " + (string)reader["UserLastNm"];
                    user.UserFirstNm = (string)reader["UserFirstNm"];
                    user.UserLastNm = (string)reader["UserLastNm"];
                    list.Add(user.UserId, user);
                }
            };

            DBSelectUtility.ProcessDBData(brokerId, sql, null, listParams, readHandler);

            return list;
        }

        private static Guid GetAccountOwner(Guid brokerId)
        {
            // Unit tested by copying to the test library
            string sql = "SELECT UserId FROM VIEW_ACTIVE_LO_USER WHERE IsAccountOwner=1 AND BrokerId=@brokerid";
            var listParams = new SqlParameter[] { new SqlParameter("@brokerid", brokerId) };

            Guid userID = Guid.Empty;
            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                if (reader.Read())
                {
                    userID = (Guid)reader["UserId"];
                }
            };

            DBSelectUtility.ProcessDBData(brokerId, sql, null, listParams, readHandler);

            if (userID != Guid.Empty) return userID;

            throw new Exception("There is no active account owner");
        }

        private static int GetGeneralTaskPermissionLevelId(Guid brokerId)
        {
            // Unit tested by copying to the tesing module
            string sql = "SELECT TaskPermissionLevelId FROM TASK_PERMISSION_LEVEL WHERE Name='General' AND BrokerId=@brokerid";
            var listParams = new SqlParameter[] { new SqlParameter("@brokerid", brokerId) };

            int? levelID = null;
            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                if (reader.Read())
                {
                    levelID = (int)reader["TaskPermissionLevelId"];
                }
            };

            DBSelectUtility.ProcessDBData(brokerId, sql, null, listParams, readHandler);

            if (levelID != null) return levelID.Value;

            throw new Exception("There is no 'General' task permission level id");
        }

        private static int GetManagedTaskPermissionLevelId(Guid brokerId)
        {
            // Unit tested by copying to the test library
            string sql = "SELECT TaskPermissionLevelId FROM TASK_PERMISSION_LEVEL WHERE Name='Managed' AND BrokerId=@brokerid";
            var listParams = new SqlParameter[] { new SqlParameter("@brokerid", brokerId) };

            int? levelID = null;
            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                if (reader.Read())
                {
                    levelID = (int)reader["TaskPermissionLevelId"];
                }
            };

            DBSelectUtility.ProcessDBData(brokerId, sql, null, listParams, readHandler);

            if (levelID != null) return levelID.Value;

            throw new Exception("There is no 'Managed' task permission level id");
        }

        private static List<Guid> ListActiveLoans(Guid brokerId)
        {
            // Unit tested by copying to the test library
            // 2/23/2015 dd - We only perform task migration of actual loans. We are not perform migration of sandbox or quicker pricer 2.0.
            string sql = "SELECT sLId FROM LOAN_FILE_CACHE WITH(NOLOCK) WHERE sBrokerId=@brokerid AND isvalid=1 AND (sLoanFileT IS NULL OR sLoanFileT=0)";
            var listParams = new SqlParameter[] { new SqlParameter("@brokerid", brokerId) };

            List<Guid> list = new List<Guid>();
            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                while (reader.Read())
                {
                    list.Add((Guid)reader["sLId"]);
                }
            };

            DBSelectUtility.ProcessDBData(brokerId, sql, null, listParams, readHandler);

            return list;
        }

        private static ConditionCategory GetCategory(Guid brokerId, Dictionary<string, ConditionCategory> dictionary, string category, int permissionLevelId)
        {
            category = category.Trim();
            ConditionCategory conditionCategory = null;

            if (dictionary.TryGetValue(category, out conditionCategory) == false)
            {
                foreach (var o in ConditionCategory.GetCategories(brokerId))
                {

                    if (o.Category.Trim().Equals(category, StringComparison.OrdinalIgnoreCase))
                    {
                        conditionCategory = o;
                    }
                    if (dictionary.ContainsKey(o.Category.Trim()) == false)
                    {
                        dictionary.Add(o.Category.Trim(), o);
                    }

                }
                if (conditionCategory == null)
                {
                    conditionCategory = new ConditionCategory(brokerId);
                    conditionCategory.Category = category;
                    conditionCategory.DefaultTaskPermissionLevelId = permissionLevelId;
                    conditionCategory.Save();
                    Console.WriteLine("Creating Condition Category: " + category);
                    dictionary.Add(category, conditionCategory);
                }

            }




            return conditionCategory;
        }

        private static Guid GetUserIdByEmployeeFullName(Dictionary<string, Guid> dictionary, Guid brokerId, string employeeName)
        {
            // Unit tested by copying to the test library
            Guid userId = Guid.Empty;

            if (dictionary.TryGetValue(employeeName, out userId) == false)
            {
                string sql = "SELECT UserId FROM VIEW_ACTIVE_LO_USER WHERE BrokerId = @BrokerId AND UserFirstNm + ' ' + UserLastNm = @EmployeeName";

                SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId),
                                            new SqlParameter("@EmployeeName", employeeName)
                                        };

                Action<IDataReader> readHandler = delegate(IDataReader reader)
                {
                    if (reader.Read())
                    {
                        userId = (Guid)reader["UserId"];
                    }
                };

                DBSelectUtility.ProcessDBData(brokerId, sql, null, parameters, readHandler);

                dictionary.Add(employeeName, userId);
            }

            return userId;
        }

        private static Guid GetUserIdByEmployeeId(Dictionary<Guid, Guid> dictionary, Guid brokerId, Guid employeeId)
        {
            Guid userId = Guid.Empty;

            if (dictionary.TryGetValue(employeeId, out userId) == false)
            {
                EmployeeDB employee = new EmployeeDB(employeeId, brokerId);
                employee.Retrieve();
                userId = employee.UserID;
                dictionary.Add(employeeId, userId);
            }
            return userId;



        }
        private static void ConvertOldXmlConditionToNewCondition(BrokerDB brokerDB, Dictionary<string, string> map)
        {
            int permissionLevelId = -1;
            foreach (PermissionLevel permissionLevel in PermissionLevel.RetrieveAll(brokerDB.BrokerID))
            {
                // If the default Managed permission doesn't exist, then look for the default Condition: UW permission
                if (permissionLevel.Name == "Managed" || permissionLevel.Name.Equals("Condition: UW", StringComparison.OrdinalIgnoreCase))
                {
                    permissionLevelId = permissionLevel.Id;
                    break;
                }
            }
            if (permissionLevelId == -1)
            {
                throw new Exception("Need to setup default Managed or Condition: UW Permission Level");
            }

            List<Guid> activeLoanList = ListActiveLoans(brokerDB.BrokerID);
            Console.WriteLine("There are " + activeLoanList.Count + " loan");
            Console.WriteLine();
            Dictionary<string, ConditionCategory> conditionCategoryDictionary = new Dictionary<string, ConditionCategory>(StringComparer.OrdinalIgnoreCase);
            Dictionary<Guid, Guid> employeeUserIdDictionary = new Dictionary<Guid, Guid>();

            List<KeyValuePair<Guid, string>> errorLoanList = new List<KeyValuePair<Guid, string>>();
            List<Guid> uniqueLoanList = new List<Guid>();
            foreach (var sLId in activeLoanList)
            {
                string sLNm = string.Empty;
                try
                {

                    CPageData dataLoan = CreateCPageData(sLId);
                    dataLoan.InitLoad();
                    sLNm = dataLoan.sLNm;
                    int count = dataLoan.GetCondRecordCount();
                    Guid assignedUnderwriter = dataLoan.sEmployeeUnderwriterId;

                    for (int i = 0; i < count; i++)
                    {
                        CCondFieldsObsolete condition = dataLoan.GetCondFieldsObsolete(i);
                        if (dataLoan.IsTemplate == true && condition.IsDone)
                        {
                            continue; // Skip
                        }
                        if (string.IsNullOrEmpty(condition.CondDesc))
                        {
                            continue; // skip
                        }
                        Task task = Task.CreateWithoutPermissionCheck(sLId, brokerDB.BrokerID);
                        task.TaskIsCondition = true;


                        if (map != null && map.ContainsKey(condition.PriorToEventDesc))
                        {
                            task.CondCategoryId = GetCategory(brokerDB.BrokerID, conditionCategoryDictionary, map[condition.PriorToEventDesc], permissionLevelId).Id;
                        }
                        else
                        {
                            Console.WriteLine("Did not find condition category : " + condition.PriorToEventDesc);
                            task.CondCategoryId = GetCategory(brokerDB.BrokerID, conditionCategoryDictionary, condition.PriorToEventDesc, permissionLevelId).Id;
                        }

                        //task.CondCategoryId = GetCategory(brokerDB.BrokerID, conditionCategoryDictionary, condition.PriorToEventDesc, permissionLevelId).Id;
                        task.TaskSubject = condition.CondDesc;
                        task.TaskStatus = condition.IsDone ? E_TaskStatus.Closed : E_TaskStatus.Active;
                        task.TaskDueDateLocked = false;
                        task.TaskDueDateCalcField = "sEstCloseD";
                        task.TaskDueDateCalcDays = 0;
                        task.TaskPermissionLevelId = permissionLevelId;

                        if (assignedUnderwriter == Guid.Empty)
                        {
                            task.TaskToBeAssignedRoleId = RoleUnderwriterId;
                            task.TaskToBeOwnerRoleId = RoleUnderwriterId;
                        }
                        else
                        {
                            Guid userId = GetUserIdByEmployeeId(employeeUserIdDictionary, brokerDB.BrokerID, assignedUnderwriter);

                            if (userId == Guid.Empty)
                            {

                                task.TaskToBeAssignedRoleId = RoleUnderwriterId;
                                task.TaskToBeOwnerRoleId = RoleUnderwriterId;

                            }
                            else
                            {
                                task.TaskAssignedUserId = userId;
                                task.TaskOwnerUserId = userId;
                            }

                        }
                        task.Save(false);



                    }

                    if (count > 0)
                    {
                        uniqueLoanList.Add(sLId);
                        Console.WriteLine(sLId + " - " + sLNm + " - SUCCESSFUL - Condition Count=" + count);

                    }
                }
                catch (Exception exc)
                {
                    Console.WriteLine(sLId + " - " + sLNm + " - ERROR - " + exc);
                    Console.WriteLine(exc.StackTrace);
                    errorLoanList.Add(new KeyValuePair<Guid, string>(sLId, sLNm));
                    Tools.LogError(sLId + " - " + sLNm + " - ERROR - ", exc);
                    throw;
                }

            }
            if (errorLoanList.Count > 0)
            {
                Console.WriteLine();
                Console.WriteLine("There are " + errorLoanList.Count + " loans with errors");
                foreach (var o in errorLoanList)
                {
                    Console.WriteLine("    " + o.Key + " - " + o.Value);
                }
            }
            // Go through a list of loan and update task statistics.
            foreach (var sLId in uniqueLoanList)
            {
                TaskUtilities.UpdateTasksDueDateByLoan(brokerDB.BrokerID, sLId);
            }
        }
        private static CPageData CreateCPageData(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependencyWithSecurityBypass(sLId, typeof(TaskMigration));
        }
        private static void ConvertOldConditionToNewCondition(BrokerDB brokerDB, Dictionary<string, string> map)
        {

            int permissionLevelId = -1;
            foreach (PermissionLevel permissionLevel in PermissionLevel.RetrieveAll(brokerDB.BrokerID))
            {
                if (permissionLevel.Name == "Managed" || permissionLevel.Name.Equals("Condition: UW", StringComparison.OrdinalIgnoreCase))
                {
                    permissionLevelId = permissionLevel.Id;
                    break;
                }
            }
            if (permissionLevelId == -1)
            {
                throw new Exception("Need to setup default Managed or Condition: UW Permission Level");
            }

            List<Guid> activeLoanList = ListActiveLoans(brokerDB.BrokerID);
            Console.WriteLine("There are " + activeLoanList.Count + " loan");
            Console.WriteLine();
            Dictionary<string, ConditionCategory> conditionCategoryDictionary = new Dictionary<string, ConditionCategory>(StringComparer.OrdinalIgnoreCase);
            Dictionary<Guid, Guid> employeeUserIdDictionary = new Dictionary<Guid, Guid>();
            Dictionary<string, Guid> employeeNameUserIdDictionary = new Dictionary<string, Guid>(StringComparer.OrdinalIgnoreCase);
            List<KeyValuePair<Guid, string>> errorLoanList = new List<KeyValuePair<Guid, string>>();
            List<Guid> uniqueLoanList = new List<Guid>();
            int currentCount = 0;
            foreach (var sLId in activeLoanList)
            {
                Console.WriteLine("Process " + currentCount + " out of " + activeLoanList.Count + " loans.");
                currentCount++;
                string sLNm = string.Empty;
                try
                {
                    CPageData dataLoan = CreateCPageData(sLId);
                    dataLoan.InitLoad();
                    sLNm = dataLoan.sLNm;
                    Guid assignedUnderwriter = dataLoan.sEmployeeUnderwriterId;

                    int count = 0;
                    LoanConditionSetObsolete loanConditionSet = new LoanConditionSetObsolete();
                    loanConditionSet.Retrieve(sLId, true, true, false);
                    List<KeyValuePair<int, string>> newTaskList = new List<KeyValuePair<int, string>>();
                    foreach (CLoanConditionObsolete condition in loanConditionSet)
                    {
                        if (dataLoan.IsTemplate == true && condition.CondStatus == E_CondStatus.Done)
                        {
                            continue; // Skip complete condition for template.
                        }
                        Task task = Task.CreateWithoutPermissionCheck(sLId, brokerDB.BrokerID);
                        task.TaskIsCondition = true;

                        if (map != null && map.ContainsKey(condition.CondCategoryDesc))
                        {
                            task.CondCategoryId = GetCategory(brokerDB.BrokerID, conditionCategoryDictionary, map[condition.CondCategoryDesc], permissionLevelId).Id;
                        }
                        else
                        {
                            Console.WriteLine("Did not find condition category : " + condition.CondCategoryDesc);
                            task.CondCategoryId = GetCategory(brokerDB.BrokerID, conditionCategoryDictionary, condition.CondCategoryDesc, permissionLevelId).Id;
                        }

                        if (string.IsNullOrEmpty(condition.CondDesc))
                        {
                            task.TaskSubject = "<blank>";
                        }
                        else
                        {
                            task.TaskSubject = condition.CondDesc;

                        }
                        task.CondIsHidden = condition.CondIsHidden;
                        task.CondInternalNotes = condition.CondNotes;
                        task.TaskDueDateLocked = false;
                        task.TaskDueDateCalcField = "sEstCloseD";
                        task.TaskDueDateCalcDays = 0;
                        task.TaskPermissionLevelId = permissionLevelId;
                        task.TaskStatus = condition.CondStatus == E_CondStatus.Done ? E_TaskStatus.Closed : E_TaskStatus.Active;

                        if (task.TaskStatus == E_TaskStatus.Closed)
                        {
                            //task.Close(condition.CondStatusDate, condition.CondCompletedByUserNm);
                            // 8/30/2011 dd - Copy Closed Date and Closed By over.
                            //task.ClosingUserFullName = condition.CondCompletedByUserNm;
                            //task.TaskClosedDate = condition.CondStatusDate;
                            task.TaskClosedUserId = GetUserIdByEmployeeFullName(employeeNameUserIdDictionary, brokerDB.BrokerID, condition.CondCompletedByUserNm);
                        }

                        if (assignedUnderwriter == Guid.Empty)
                        {
                            task.TaskToBeAssignedRoleId = RoleUnderwriterId;
                            task.TaskToBeOwnerRoleId = RoleUnderwriterId;
                        }
                        else
                        {
                            Guid userId = GetUserIdByEmployeeId(employeeUserIdDictionary, brokerDB.BrokerID, assignedUnderwriter);

                            if (userId == Guid.Empty)
                            {
                                task.TaskToBeAssignedRoleId = RoleUnderwriterId;
                                task.TaskToBeOwnerRoleId = RoleUnderwriterId;

                            }
                            else
                            {
                                task.TaskAssignedUserId = userId;
                                task.TaskOwnerUserId = userId;
                            }
                        }
                        task.Save(false);
                        if (task.TaskStatus == E_TaskStatus.Closed)
                        {
                            task = Task.RetrieveWithoutPermissionCheck(brokerDB.BrokerID, task.TaskId);
                            // 8/30/2011 dd - This will trigger the audit entry.
                            task.Close(brokerDB.BrokerID, condition.CondStatusDate, condition.CondCompletedByUserNm);
                        }

                        newTaskList.Add(new KeyValuePair<int, string>(condition.CondOrderedPosition, task.TaskId));

                        count++;
                    }
                    if (count > 0)
                    {

                        ConditionOrderManager.StoreUserSort(sLId, newTaskList.OrderBy(o => o.Key).Select(o => o.Value));
                        uniqueLoanList.Add(sLId);
                        Console.WriteLine(sLId + " - " + sLNm + " - SUCCESSFUL - Condition Count=" + count);

                    }

                    #region Migrate Delete Conditions
                    count = 0;
                    loanConditionSet = new LoanConditionSetObsolete();
                    loanConditionSet.Retrieve(sLId, true, true, true);
                    //List<KeyValuePair<int, string>> newTaskList = new List<KeyValuePair<int, string>>();
                    foreach (CLoanConditionObsolete condition in loanConditionSet)
                    {

                        Task task = Task.CreateWithoutPermissionCheck(sLId, brokerDB.BrokerID);
                        task.TaskIsCondition = true;

                        task.CondCategoryId = GetCategory(brokerDB.BrokerID, conditionCategoryDictionary, condition.CondCategoryDesc, permissionLevelId).Id;

                        if (string.IsNullOrEmpty(condition.CondDesc))
                        {
                            task.TaskSubject = "<blank>";
                        }
                        else
                        {
                            task.TaskSubject = condition.CondDesc;

                        }
                        task.CondIsHidden = condition.CondIsHidden;
                        task.CondInternalNotes = condition.CondNotes;
                        task.TaskDueDateLocked = false;
                        task.TaskDueDateCalcField = "sEstCloseD";
                        task.TaskDueDateCalcDays = 0;
                        task.TaskPermissionLevelId = permissionLevelId;
                        task.TaskStatus = condition.CondStatus == E_CondStatus.Done ? E_TaskStatus.Closed : E_TaskStatus.Active;

                        if (task.TaskStatus == E_TaskStatus.Closed)
                        {
                            //task.Close(condition.CondStatusDate, condition.CondCompletedByUserNm);
                            // 8/30/2011 dd - Copy Closed Date and Closed By over.
                            //task.ClosingUserFullName = condition.CondCompletedByUserNm;
                            //task.TaskClosedDate = condition.CondStatusDate;
                            task.TaskClosedUserId = GetUserIdByEmployeeFullName(employeeNameUserIdDictionary, brokerDB.BrokerID, condition.CondCompletedByUserNm);
                        }

                        if (assignedUnderwriter == Guid.Empty)
                        {
                            task.TaskToBeAssignedRoleId = RoleUnderwriterId;
                            task.TaskToBeOwnerRoleId = RoleUnderwriterId;
                        }
                        else
                        {
                            Guid userId = GetUserIdByEmployeeId(employeeUserIdDictionary, brokerDB.BrokerID, assignedUnderwriter);

                            if (userId == Guid.Empty)
                            {
                                task.TaskToBeAssignedRoleId = RoleUnderwriterId;
                                task.TaskToBeOwnerRoleId = RoleUnderwriterId;

                            }
                            else
                            {
                                task.TaskAssignedUserId = userId;
                                task.TaskOwnerUserId = userId;
                            }
                        }



                        RunActionWithRetry(() => task.Save(false), 3);

                        task = Task.RetrieveWithoutPermissionCheck(brokerDB.BrokerID, task.TaskId);
                        // 8/30/2011 dd - This will trigger the audit entry.

                        RunActionWithRetry(() => task.Delete(condition.CondDeleteByUserNm, false), 3);

                        count++;
                    }
                    if (count > 0)
                    {

                        Console.WriteLine(sLId + " - " + sLNm + " - SUCCESSFUL - Delete Condition Count=" + count);

                    }
                    #endregion

                }
                catch (Exception exc)
                {
                    Console.WriteLine(sLId + " - " + sLNm + " - ERROR - " + exc);
                    Console.WriteLine(exc.StackTrace);
                    Tools.LogError(sLId + " - " + sLNm + " - ERROR - " + exc);

                    errorLoanList.Add(new KeyValuePair<Guid, string>(sLId, sLNm));
                    throw;
                }
            }
            if (errorLoanList.Count > 0)
            {
                Console.WriteLine();

                Console.WriteLine("There are " + errorLoanList.Count + " loans with errors");
                foreach (var o in errorLoanList)
                {
                    Console.WriteLine("    " + o.Key + " - " + o.Value);
                }
            }
            Console.WriteLine("Starting to update task statistics");
            int idx = 0;
            // Go through a list of loan and update task statistics.
            foreach (var sLId in uniqueLoanList)
            {
                if (idx != 0 && (idx % 10 == 0))
                {
                    Console.WriteLine("Updated Task Statistics Count=" + idx);
                }

                RunActionWithRetry(() => TaskUtilities.UpdateTasksDueDateByLoan(brokerDB.BrokerID, sLId), 3);
                idx++;
            }
        }

        public static void RunActionWithRetry(Action action, int retries)
        {
            if (retries <= 0)
            {
                retries = 3;
            }

            for (int i = 0; i < retries; i++)
            {
                try
                {
                    action();
                    return;
                }
                catch (Exception)
                {
                    if (i + 1 == retries)
                    {
                        throw;
                    }

                    Thread.Sleep(TimeSpan.FromSeconds(60));
                }
            }

            throw new NotImplementedException("Should not have reached this far.");

        }

        public static Dictionary<string, string> ParseConditionCategoryMap(string path)
        {
            if (!FileOperationHelper.Exists(path))
                return null;


            Dictionary<string, string> conditionCategoryMap = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
            HashSet<string> mapItems = new HashSet<string>();
            using (TextFieldParser parser = new TextFieldParser(path))
            {
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(",");
                if ( !parser.EndOfData )
                {
                    string[] fields = parser.ReadFields();
                    if (!fields[0].Equals("CondCategoryDesc") || !fields[1].Equals("Map"))
                    {
                        throw new CBaseException($"Invalid mapping file {fields[0]} {fields[1]}", "Bad File");
                    }
                }
                while (!parser.EndOfData)
                {
                    //Processing row
                    string[] fields = parser.ReadFields();
                    try
                    {
                        conditionCategoryMap.Add(fields[0].Trim(), fields[1].Trim());
                        mapItems.Add(fields[1]);

                    }
                    catch (ArgumentException)
                    {
                        Console.WriteLine($"Duplicate category found in excel ignoring {fields[0]}.");
                    }
                    }
            }

            foreach(var newCateogry in mapItems)
            {
                Console.WriteLine($"New Category: {newCateogry}");
            }
            
            Console.WriteLine("{0} Mappings found.", conditionCategoryMap.Count);
            return conditionCategoryMap;
        }

    }
}