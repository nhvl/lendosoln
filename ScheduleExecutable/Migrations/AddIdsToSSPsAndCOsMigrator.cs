﻿namespace ScheduleExecutable.Migrations
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Linq;
    using System.Text;
    using DataAccess;
    using LendersOffice.Constants;

    /// <summary>
    /// Adds ids to the SSPs and counseling organizations.
    /// </summary>
    public class AddIdsToSSPsAndCOsMigrator : Migrator<Guid>
    {
        /// <summary>
        /// Ids that errored while migrating.
        /// </summary>
        private List<string> erroredIds = new List<string>();

        /// <summary>
        /// Initializes a new instance of the <see cref="AddIdsToSSPsAndCOsMigrator"/> class.
        /// </summary>
        public AddIdsToSSPsAndCOsMigrator()
            : this(RetrieveLoansToMigrateFromDb())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AddIdsToSSPsAndCOsMigrator"/> class.
        /// </summary>
        /// <param name="loansToMigrate">The loans to migrate.</param>
        public AddIdsToSSPsAndCOsMigrator(IEnumerable<Guid> loansToMigrate)
            : base($"AddIdsToSSPsAndCOsMigrator_{DateTime.Today.ToString("MM_dd_yyyy")}")
        {
            this.MigrationHelper = new CustomGUIDMigrationHelper(this.Name, () => loansToMigrate);
        }

        /// <summary>
        /// Runs the migration to add the IDs
        /// </summary>
        /// <param name="args">Arguments from the command line.</param>
        public static void AddIdsToSSPsAndCOs(string[] args)
        {
            AddIdsToSSPsAndCOsMigrator migrator;
            if (args.Length == 1)
            {
                migrator = new AddIdsToSSPsAndCOsMigrator();
            }
            else
            {
                List<Guid> loanIds = new List<Guid>();
                foreach (string id in File.ReadAllLines(args[1]))
                {
                    loanIds.Add(Guid.Parse(id));
                }

                migrator = new AddIdsToSSPsAndCOsMigrator(loanIds);
            }

            migrator.Migrate();
        }

        /// <summary>
        /// Adds Ids to the various collections.
        /// </summary>
        /// <param name="id">The loan to modify.</param>
        /// <returns>True if successful, false otherwise.</returns>
        protected override bool MigrationFunction(Guid id)
        {
            try
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(id, typeof(AddIdsToSSPsAndCOsMigrator));
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                dataLoan.ByPassFieldSecurityCheck = true;
                dataLoan.DisableFieldEnforcement();

                bool isDirty = false;

                var titleSsp = dataLoan.sTitleSettlementServiceProviders;
                if (titleSsp.Any())
                {
                    dataLoan.sTitleSettlementServiceProviders = titleSsp;
                    isDirty = true;
                }

                var escrowSSP = dataLoan.sEscrowSettlementServiceProviders;
                if (escrowSSP.Any())
                {
                    dataLoan.sEscrowSettlementServiceProviders = escrowSSP;
                    isDirty = true;
                }

                var surveySSP = dataLoan.sSurveySettlementServiceProviders;
                if (surveySSP.Any())
                {
                    dataLoan.sSurveySettlementServiceProviders = surveySSP;
                    isDirty = true;
                }

                var pestSSP = dataLoan.sPestInspectionSettlementServiceProviders;
                if (pestSSP.Any())
                {
                    dataLoan.sPestInspectionSettlementServiceProviders = pestSSP;
                    isDirty = true;
                }

                var miscSSP = dataLoan.sMiscSettlementServiceProviders;
                if (miscSSP.Any())
                {
                    dataLoan.sMiscSettlementServiceProviders = miscSSP;
                    isDirty = true;
                }

                var availSSP = dataLoan.sAvailableSettlementServiceProviders;
                if (availSSP.GetAllProviders().Any())
                {
                    dataLoan.sAvailableSettlementServiceProviders = availSSP;
                    isDirty = true;
                }

                var counselingOrgs = dataLoan.sHomeownerCounselingOrganizationCollection;
                if (counselingOrgs.Any())
                {
                    dataLoan.sHomeownerCounselingOrganizationCollection = counselingOrgs;
                    isDirty = true;
                }

                if (isDirty)
                {
                    dataLoan.Save();
                }

                return true;
            }
            catch (Exception e)
            {
                Tools.LogError($"Unable to migrate loan id {id.ToString()}", e);
                erroredIds.Add(id.ToString());
                return false;
            }
        }

        /// <summary>
        /// Logs any errors to an error file.
        /// </summary>
        protected override void EndMigration()
        {
            File.WriteAllLines(this.Name + "_errorLog.txt", this.erroredIds);
        }

        /// <summary>
        /// Gets the loans from the DB to migrate.
        /// </summary>
        /// <returns>The list of loan ids.</returns>
        private static IEnumerable<Guid> RetrieveLoansToMigrateFromDb()
        {
            string query = @"SELECT b.sLId
                            FROM
	                            LOAN_FILE_B b JOIN
	                            LOAN_FILE_C c ON b.sLId=c.sLId JOIN
	                            LOAN_FILE_CACHE lfc ON b.sLId=lfc.sLId
                            WHERE
	                            lfc.IsValid=1 AND 
	                            (sTitleSettlementServiceProviderXml<>'' OR
	                             sEscrowSettlementServiceProviderXml<>'' OR
	                             sSurveySettlementServiceProviderXml<>'' OR
	                             sPestInspectionSettlementServiceProviderXml<>'' OR
	                             sMiscSettlementServiceProviderXml<>'' OR
	                             sAvailableSettlementServiceProvidersJson<>'' OR
	                             sHomeownerCounselingOrganizationXmlContent<>'')";

            LinkedList<Guid> loanIds = new LinkedList<Guid>();
            Action<IDataReader> readHandler = delegate (IDataReader reader)
            {
                while (reader.Read())
                {
                    loanIds.AddLast((Guid)reader["slid"]);
                }
            };

            foreach (var connectionInfo in DbConnectionInfo.ListAll())
            {
                DBSelectUtility.ProcessDBData(connectionInfo, query, null, null, readHandler);
            }

            return loanIds;
        }
    }
}
