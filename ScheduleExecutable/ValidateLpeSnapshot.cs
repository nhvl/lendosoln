﻿namespace ScheduleExecutable
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using DataAccess;
    using LendersOfficeApp.los.RatePrice;

    internal class ValidateLpeSnapshot
    {
        string m_snapshotInfo = "";
        DataSrc m_dataSrc;
        DateTime m_versionD;
        PolicyRelationCenter m_policyRelationDb;
        Hashtable m_rateSheetIds = new Hashtable();
        Hashtable m_enableBorkers = new Hashtable();
        ArrayList m_programs = new ArrayList();

        bool m_validatePolicyRelationShip = true;

        void Init(DataSrc dataSrc)
        {
            m_dataSrc = dataSrc;
            m_snapshotInfo = dataSrc.ToString();
            m_versionD = LpeTools.GetLpeDataLastModified(dataSrc);

            m_snapshotInfo = string.Format("{0}  {1} {2}", dataSrc,
                m_versionD.ToShortTimeString(), m_versionD.ToShortDateString());

            m_policyRelationDb = null;
            m_rateSheetIds.Clear();
            m_enableBorkers.Clear();
            m_programs.Clear();

            LpeTools.ListLpeEnabledBrokers(m_enableBorkers);
        }

        public bool Validate(DataSrc dataSrc, bool validatePolicyRelationShip)
        {
            m_validatePolicyRelationShip = validatePolicyRelationShip;
            m_snapshotInfo = dataSrc.ToString();
            try
            {
                Init(dataSrc);
                CreatePolicyRelationCenter();
                LoadRateSheetIds();
                LoadProgramIds();

                Tools.LogRegTest(string.Format("<LpeRelease_Validate> the snapshot {0} has {1} programs, {2} rateSheets, {3} policyies. ValidatePolicyRelationShip = {4}",
                    m_snapshotInfo, m_programs.Count.ToString("N0"), m_rateSheetIds.Count.ToString("N0"), m_policyRelationDb.Count.ToString("N0"), m_validatePolicyRelationShip));

                ArrayList ids = new ArrayList();
                const int blockSize = 200;
                for (int i = 0; i < m_programs.Count; i++)
                {
                    ids.Add(m_programs[i]);
                    if (ids.Count == blockSize)
                    {
                        ValidateLoadPrograms(ids);
                        ids.Clear();
                    }
                }

                if (ids.Count > 0)
                    ValidateLoadPrograms(ids);

                Tools.LogRegTest(string.Format("<LpeRelease_Validate> {0} : success", m_snapshotInfo));

                return true;
            }
            catch (Exception exc)
            {
                Tools.LogError("<LpeRelease_Error> " + m_snapshotInfo, exc);
                return false;
            }
        }

        void LoadProgramIds()
        {
            Tools.LogRegTest(string.Format("<LpeRelease_Validate> {0} : LoadProgramIds", m_snapshotInfo));

            // 11/21/2007 ThienNguyen - Reviewed and safe
            string sql = "SELECT lLpTemplateId\n" +
                "    FROM Loan_Program_Template\n" +
                "    WHERE IsMaster = 0 and IsEnabled = 1 and IsLpe = 1  "
                + " and BrokerId " + DbTools.InClauseForSql(m_enableBorkers.Keys);

            CDataSet ds = new CDataSet(60);
            ds.LoadWithSqlQueryDangerously(m_dataSrc, sql);

            m_programs.Clear();
            foreach (DataRow row in ds.Tables[0].Rows)
                m_programs.Add(row["lLpTemplateId"]);
        }

        void ValidateLoadPrograms(ArrayList prgIds)
        {
            // 11/21/2007 ThienNguyen - Reviewed and safe
            // get loan programs
            string sql = "SELECT lLpTemplateId, SrcRateoptionsProgId\n" +
                "    FROM Loan_Program_Template\n" +
                "    WHERE IsMaster = 0 and IsEnabled = 1 and IsLpe = 1  and lLpTemplateId "
                + DbTools.InClauseForSql(prgIds);

            CDataSet ds = new CDataSet(60);
            ds.LoadWithSqlQueryDangerously(m_dataSrc, sql);

            ArrayList programs = new ArrayList();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                if (m_rateSheetIds.Contains(row["SrcRateoptionsProgId"]) == false)
                {
                    throw new InvalidLpeSnapshotException(
                        String.Format("LpeRelease_Validate> {2} : loanProgram {0} contains a invalid  SrcRateoptionsProgId = {1}",
                        row["lLpTemplateId"], row["SrcRateoptionsProgId"], m_snapshotInfo));
                }
                programs.Add(row["lLpTemplateId"]);
            }
            ds = null;

            if (m_validatePolicyRelationShip == false)
                return;

            // 11/21/2007 ThienNguyen - Reviewed and safe
            // get policies
            string directlyAssociatedPoliciesSql =
                "\nSELECT pa.ProductId, p.PricePolicyId, p.PricePolicyDescription, p.MutualExclusiveExecSortedId, lLpTemplateNm " +
                "FROM ( Product_Price_Association pa JOIN Price_Policy p ON pa.PricePolicyID = p.PricePolicyId )              " +
                "   join loan_program_template product on pa.productId = product.lLpTemplateId                                " +
                "WHERE                               \n" +
                "           product.IsMaster = 0     \n" +
                "   and     product.IsEnabled = 1    \n" +
                "   and     product.IsLpe = 1        \n" +
                "   and     pa.AssocOverrideTri != 2 \n" +
                "   and ( pa.InheritVal = 1 or pa.AssocOverrideTri = 1 or pa.InheritValFromBaseProd = 1 ) \n" +
                "   and product.lLpTemplateId " + DbTools.InClauseForSql(programs); ;

            ds = new CDataSet(60);
            ds.LoadWithSqlQueryDangerously(m_dataSrc, directlyAssociatedPoliciesSql);

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                Guid productIdKey = (Guid)row["ProductId"];
                Guid policyId = (Guid)row["PricePolicyID"];

                if (m_policyRelationDb.Exist(policyId) == false)
                    throw new InvalidLpeSnapshotException(string.Format("LpeRelease> {2} : Loan Program {0} can not associate with the invalid policy {1}"
                        , productIdKey, policyId, m_snapshotInfo));

                ArrayList parents = m_policyRelationDb.GetAncestors(policyId);

                if (parents == null)
                    throw new InvalidLpeSnapshotException(string.Format("LpeRelease> {2} : Loan Program {0} can not associate with the invalid policy {1}"
                        , productIdKey, policyId, m_snapshotInfo));


                foreach (Guid ancestorPolicyId in parents)
                    if (m_policyRelationDb.Exist(ancestorPolicyId) == false)
                        throw new InvalidLpeSnapshotException(string.Format("<LpeRelease> {0} : policy {1} for Loan Program {2} has a invalid ancestor {3}.",
                            m_snapshotInfo, policyId, productIdKey, ancestorPolicyId));
            }

        }

        void LoadRateSheetIds()
        {
            CDataSet ds = new CDataSet();

            // 11/21/2007 ThienNguyen - Reviewed and safe
            ds.LoadWithSqlQueryDangerously(m_dataSrc, "select SrcProgId from Rate_Options");

            m_rateSheetIds.Clear();
            foreach (DataRow row in ds.Tables[0].Rows)
                if (row["SrcProgId"] != DBNull.Value)
                    m_rateSheetIds[row["SrcProgId"]] = null;
        }


        void CreatePolicyRelationCenter()
        {
            Tools.LogRegTest(string.Format("<LpeRelease_Validate> {0} : CreatePolicyRelationCenter", m_snapshotInfo));

            CDataSet ds = new CDataSet(60);

            // 11/21/2007 ThienNguyen - Reviewed and safe
            ds.LoadWithSqlQueryDangerously(m_dataSrc, "select PricePolicyId, ParentPolicyId, MutualExclusiveExecSortedId from price_policy");

            CRowHashtable rowsPolicyRelation = new CRowHashtable(ds.Tables[0].Rows, "PricePolicyId");
            m_policyRelationDb = new PolicyRelationCenter(rowsPolicyRelation);

        }

        private class InvalidLpeSnapshotException : CBaseException
        {
            public override string EmailSubjectCode
            {
                get { return "InvalidLpeSnapshotException"; }
            }

            public InvalidLpeSnapshotException(string message)
                : base("Invalid LPE snapshot", message)
            {

            }

        }

    }
}
