﻿namespace ScheduleExecutable.Integrations.Cenlar
{
    using System;
    using DataAccess;
    using LendersOffice.Integration.Cenlar;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// A service that runs on a regular schedule to transmit clients' loans to Cenlar.
    /// </summary>
    public static class RunAutomatedCenlarTransmissions
    {
        /// <summary>
        /// Iterates through each lender configured to send automatic transmissions
        /// to Cenlar and sends a transmission on their behalf.
        /// </summary>
        /// <param name="args">A set of executable string arguments.</param>
        public static void Execute(string[] args)
        {
            var lenders = LightCenlarLender.LoadAll(filterForAutomatedTransmissions: true);

            foreach (var lender in lenders)
            {
                try
                {
                    var requestData = new CenlarRequestData(
                        lender.BrokerId,
                        CenlarActionT.SendLoanFiles,
                        CenlarEnvironmentT.Production,
                        automatedTransmission: true);

                    var requestHandler = new CenlarRequestHandler(requestData);
                    requestHandler.SubmitRequest();
                }
                catch (Exception exc) when (exc is DeveloperException || exc is CBaseException)
                {
                    Tools.LogError(exc);
                    continue;
                }
            }
        }
    }
}
