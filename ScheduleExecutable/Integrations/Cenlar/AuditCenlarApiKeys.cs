﻿namespace ScheduleExecutable.Integrations.Cenlar
{
    using System;
    using DataAccess;
    using LendersOffice.Integration.Cenlar;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// A service that runs daily to monitor lenders' API keys and regenerate
    /// them once they near expiry.
    /// </summary>
    public static class AuditCenlarApiKeys
    {
        /// <summary>
        /// The API Key expires every month. We will regenerate slightly more
        /// frequently after 3 weeks to account for any process outage.
        /// </summary>
        private const int DaysUntilStale = 21;

        /// <summary>
        /// Iterates through each lender using the Cenlar integration and
        /// audits their API keys.
        /// </summary>
        /// <param name="args">A set of executable string arguments.</param>
        public static void Execute(string[] args)
        {
            var lenders = LightCenlarLender.LoadAll();

            foreach (var lender in lenders)
            {
                try
                {
                    AuditApiKey(lender, CenlarEnvironmentT.Beta);
                    AuditApiKey(lender, CenlarEnvironmentT.Production);
                }
                catch (Exception exc) when (exc is DeveloperException || exc is CBaseException)
                {
                    Tools.LogError(exc);
                    continue;
                }
            }
        }

        /// <summary>
        /// Audits the lender's API key for the given Cenlar environment,
        /// regenerating it if necessary.
        /// </summary>
        /// <param name="lender">The lender to audit.</param>
        /// <param name="environment">The environment for which to audit credentials.</param>
        private static void AuditApiKey(LightCenlarLender lender, CenlarEnvironmentT environment)
        {
            var credentials = CenlarCredentialSet.Retrieve(lender.BrokerId, environment);

            if (credentials == null || !credentials.IsValid)
            {
                return;
            }

            if (IsApiKeyStale(credentials.LastApiKeyReset))
            {
                var requestData = new CenlarRequestData(lender.BrokerId, CenlarActionT.UpdateApiKey, environment);
                var requestHandler = new CenlarRequestHandler(requestData);
                requestHandler.SubmitRequest();
            }
        }

        /// <summary>
        /// Determines whether the API key is old enough to be regenerated.
        /// </summary>
        /// <param name="lastUpdated">
        /// The date on which the API key was last generated.
        /// </param>
        /// <returns>
        /// A boolean indicating whether the API key should be regenerated.
        /// </returns>
        private static bool IsApiKeyStale(DateTime lastUpdated)
        {
            if (lastUpdated == default(DateTime))
            {
                return false;
            }

            var daysSinceLastUpdate = (DateTime.Now - lastUpdated).Days;
            return daysSinceLastUpdate >= DaysUntilStale;
        }
    }
}
