﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleExecutable
{
    // opm 457935: Generic alive external site check.
    // Note: webping has same feature but its input data gets from file "CalcSiteURLs.txt". 
    class HealthCheck
    {
        public static void Execute(string[] args)
        {
            if (args.Length != 2)
            {
                Console.WriteLine("Usage: ScheduleExecutable HealthCheck url");
                return;
            }

            string url = args[1];

            WebClient client = new WebClient();
            var data = client.DownloadString(url);

            Console.WriteLine($"Downloaded {data.Length} characters from {url}");
        }
    }
}
