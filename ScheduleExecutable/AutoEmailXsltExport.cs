﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.XsltExportReport;
using System.Xml.Linq;
using LendersOffice.Common;
using DataAccess;

namespace ScheduleExecutable
{
    class AutoEmailXsltExport
    {
        /// <summary>
        /// Execute the Batch Xslt Export and then email the file as an attachment.
        /// </summary>
        /// <param name="args"></param>
        public static void Execute(string[] args)
        {

            string exportName = args[1];
            
            // 6/25/2013 dd - Depend on usage, may want to store the settings in database.
            string settingsFile = "AutoEmailXsltExportSettings.xml";

            XDocument xdoc = XDocument.Load(settingsFile);

            foreach (XElement el in xdoc.Root.Elements("item"))
            {
                string id = SafeString(el, "id");
                if (id.Equals(exportName, StringComparison.OrdinalIgnoreCase) == false)
                {
                    continue;
                }

                string mapName = SafeString(el, "map_name");
                Guid userId = SafeGuid(el, "userid");
                Guid reportId = SafeGuid(el, "reportid");
                string emailFrom = SafeString(el, "email_from");
                string emailTo = SafeString(el, "email_to");
                string emailSubject = SafeString(el, "email_subject");
                string emailMessage = SafeString(el, "email_message");

                List<KeyValuePair<string, string>> parameterList = new List<KeyValuePair<string, string>>();
                parameterList.Add(new KeyValuePair<string, string>("From", emailFrom));
                parameterList.Add(new KeyValuePair<string, string>("To", emailTo));
                parameterList.Add(new KeyValuePair<string, string>("Subject", emailSubject));
                parameterList.Add(new KeyValuePair<string, string>("Message", emailMessage));

                EmailXsltExportCallBack callBack = new EmailXsltExportCallBack();

                Guid brokerId = Guid.Empty;

                DbConnectionInfo.GetConnectionInfoByUserId(userId, out brokerId);

                var xsltRequest = XsltExportRequest.CreateFromCustomReport(brokerId, userId, reportId, mapName, callBack, parameterList);

                XsltExportResult result = XsltExport.ExecuteSynchronously(xsltRequest);

                return;

            }

            throw new NotFoundException("[" + exportName + "] is not found in " + settingsFile);
        }

        private static string SafeString(XElement el, string attrName)
        {
            XAttribute attr = el.Attribute(attrName);
            if (attr != null)
            {
                return attr.Value;
            }
            return string.Empty;
        }
        private static Guid SafeGuid(XElement el, string attrName)
        {
            string v = SafeString(el, attrName);

            if (string.IsNullOrEmpty(v) == false)
            {
                try
                {
                    return new Guid(v);
                }
                catch (FormatException)
                {
                }
            }
            return Guid.Empty;
        }

    }
}
