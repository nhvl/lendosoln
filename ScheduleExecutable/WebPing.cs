﻿// <copyright file="WebPing.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: David Dao
//    Date:   8/21/2014 5:08:51 PM 
// </summary>
namespace ScheduleExecutable
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using DataAccess;
    using LendersOffice.Drivers.Gateways;

    /// <summary>
    ///
    /// </summary>
    class WebPing
    {
        public static void Execute(string[] args)
        {
            string fileName = "CalcSiteURLs.txt";

            if (FileOperationHelper.Exists(fileName) == false)
            {
                Tools.LogError("WebPing: Unable to find file " + fileName + ".");
                return;
            }

            var sites = TextFileHelper.ReadLines(fileName);

            StringBuilder sb = new StringBuilder();

            foreach (var url in sites)
            {
                if (string.IsNullOrEmpty(url))
                {
                    continue;
                }
                try
                {
                    WebRequest request = WebRequest.Create(url);
                    request.Credentials = CredentialCache.DefaultCredentials;
                    using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                    {
                        byte[] buffer = new byte[100];
                        using (Stream reader = response.GetResponseStream())
                        {
                            int count = reader.Read(buffer, 0, buffer.Length);
                            if (count > 0)
                            {
                                sb.AppendLine("WebPing: " + url + " successfully.");
                            }
                            else
                            {
                                sb.AppendLine("WebPing: " + url + " return 0 byte.");
                            }
                        }
                    }
                }
                catch (WebException exc)
                {
                    Tools.LogError("WebPing: Unable to connect " + url, exc);
                }
            }

            Tools.LogInfo("WebPing", sb.ToString());


        }
    }
}