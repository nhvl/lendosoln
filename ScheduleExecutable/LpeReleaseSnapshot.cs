﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Toolbox.Distributed;
using DataAccess;
using LendersOffice.Constants;
using LendersOfficeApp.los.RatePrice;

namespace ScheduleExecutable
{
    class LpeReleaseSnapshot
    {
        const int ACTION_Normal = 0;
        const int ACTION_SmokeTest = 1;
        const int ACTION_SleepAndExit = 2;      // for testing
        const int ACTION_SleepAndContinue = 3;  // for testing

        public static void RelaseSnapshot(string[] args)
        {
            int action = ACTION_Normal;
            if (args.Length > 1)
            {
                string option = args[1];

                if (option.Equals("snapshot", StringComparison.OrdinalIgnoreCase))
                {
                    action = ACTION_Normal;
                }
                else if (option.Equals("smoke", StringComparison.OrdinalIgnoreCase))
                {
                    action = ACTION_SmokeTest;
                }
                else if (option.Equals("sleepExit", StringComparison.OrdinalIgnoreCase))
                {
                    action = ACTION_SleepAndExit;
                }
                else if (option.Equals("sleepContinue", StringComparison.OrdinalIgnoreCase))
                {
                    action = ACTION_SleepAndContinue;
                }
                else
                {
                    throw new ArgumentException("Error: unknow argument " + option);
                }
            }

            Execute(action);
        }


        static void Execute(int action)
        {
            bool relealseForSmokeTest = (action == ACTION_SmokeTest);

            bool createdNew;
            using (Mutex mutex = CreateMutex(lpeMutexName, out createdNew))
            {
                if (!createdNew)
                {
                    if (relealseForSmokeTest)
                    {
                        Toolbox.RatePrice.Invalidate();

                        // Todo: Process Synchronization
                        if (!File.Exists(SmokeTestSignalFile))
                        {
                            File.CreateText(SmokeTestSignalFile);
                            Tools.LogRegTest($"Create signal file {SmokeTestSignalFile}.");
                        }
                    }

                    Console.WriteLine("Exit because other instance is running.");
                    return;
                }

                if (action == ACTION_SleepAndExit || action == ACTION_SleepAndContinue) // sleep
                {
                    Console.Write("\nPress any key to terminate application.");
                    Console.ReadLine();
                    if (action == ACTION_SleepAndExit)
                    {
                        return;
                    }
                }


                if (relealseForSmokeTest)
                {
                    Toolbox.RatePrice.Invalidate();
                    Tools.LogRegTest("<SnapshotSmokeTest> Just called the UpdatePriceEngineVersionDate stored procedure and will sleep for 1 minute to ensure the data is old enough.");
                    Thread.Sleep(60 * 1000);
                }

                var lpeReleaseSnapshot = new LpeReleaseSnapshot(relealseForSmokeTest);
                lpeReleaseSnapshot.Run();
            }
        }

        private LpeReleaseSnapshot(bool relealseForSmokeTest)
        {
            RequestSmokeTest = relealseForSmokeTest;
            Init();
        }

        void Init()
        {
            try
            {
                // load config 
                s_dropOutOfDateSsAfterXMinutes = ReadConfigVar("DestroySnapshotPoint", 7);                   // destroyPoint
                s_detectLpeSrcDataChangeAfterServiceXMinutes = ReadConfigVar("CreateSnapshotPoint", 23);     // createPoint
                s_takeSsAfterServiceXMinutes = ReadConfigVar("ReleaseSnapshotPoint", 25);                    // releasePoint
                s_waitThresholdInMinutes = ReadConfigVar("WaitThresholdInMinutes", 1); // the application will terninate if wait time >= s_waitThresholdInMinutes

                // get release snapshot information
                m_latestRelease = CLpeReleaseInfo.GetLatestReleaseInfo();
                if (m_latestRelease == null)
                {
                    string errMsg = "<LpeRelease_Error>: Unexpected situation. Expected to have a release version declared at here";
                    Tools.LogErrorWithCriticalTracking(errMsg);
                    throw new Exception(errMsg);
                }

                m_inServiceSnapshotId = m_latestRelease.SnapshotId;
                m_ssId = (m_latestRelease.SnapshotId != ConstAppThinh.LpeSnapshot1Id) ? ConstAppThinh.LpeSnapshot1Id : ConstAppThinh.LpeSnapshot2Id;
                m_ssName = CLpeReleaseInfo.GetSnapshotNameFrom(m_ssId);
                m_ssDataSrc = CLpeReleaseInfo.GetDataSrcFrom(m_ssId);

                if(!RequestSmokeTest)
                {
                    // Todo: Process Synchronization
                    if (File.Exists(SmokeTestSignalFile))
                    {
                        File.Delete(SmokeTestSignalFile);
                        RequestSmokeTest = true;
                        Tools.LogRegTest("<LpeRelease> detect smoke test signal.");
                    }

                }

            } // try
            catch (Exception ex)
            {
                Tools.LogError("<LpeRelease_Exception> happened. The program terminated.", ex);
                throw;
            }
        }

        void Run()
        {
            if(!CanDropOldSnapshot()) // Make sure that LATEST SNAPSHOT services at least 7 minutes
            {
                return;
            }

            DropOldSnapshot();

            if(!CanCreateSnapshot())
            {
                return;
            }

            CreateSnapshot(m_ssId, m_ssName);

            #region Waiting for the current release (of other dataset) to be at least 25 min
            double minutesAge = m_latestRelease.ReleaseAge.TotalMinutes; // getting the new age
            double minutesAgeSpecified = s_takeSsAfterServiceXMinutes;
            double nWait = (minutesAgeSpecified - minutesAge);

            if (nWait > 0)
            {
                Report($"<LpeRelease> {m_ssId}: Going to sleep {nWait:N1} minutes before release new snapshot.");
                MySleep(nWait);
            }
            #endregion // Waiting for the current release (of other dataset) to be at least 25 min

            DeclareNewVersion(m_ssId, m_ssName);
            if (RequestSmokeTest)
            {
                // Todo: Process Synchronization
                File.Delete(SmokeTestSignalFile);

                Tools.LogRegTest("<SnapshotSmokeTest> successfully create snapshot " + m_ssId);
                RequestSmokeTest = false;
            }

        }

        static private string lpeMutexName = @"Global\abecb3c3-7ee7-41e5-9845-62aa8f63a873";
        static private double s_dropOutOfDateSsAfterXMinutes = 7;
        static private double s_detectLpeSrcDataChangeAfterServiceXMinutes = 23;
        static private double s_takeSsAfterServiceXMinutes = 25;
        static private double s_waitThresholdInMinutes = 1; // the application will terninate if wait time >= s_waitThresholdInMinutes
        static string SmokeTestSignalFile = "CreateSnapshotForSmokeTest";


        private CLpeReleaseInfo m_latestRelease = null;
        private string m_inServiceSnapshotId;
        private string m_ssId; //out of date snapshot
        private string m_ssName;
        private DataSrc m_ssDataSrc;
        private bool RequestSmokeTest;

        const int s_reportIntervalInSeconds = 90;
        DateTime m_reportTime = DateTime.Now.AddSeconds(s_reportIntervalInSeconds);

        void MySleep(double minutesTimeout)
        {
            const double MilisecondsPerMinute = 60 * 1000;
            if (minutesTimeout <= 0)
                return;


            string snapshotId = m_ssId;
            DateTime reportTime = DateTime.Now.AddMinutes(5);
            DateTime target = DateTime.Now.AddMinutes(minutesTimeout);

            while (true)
            {
                int remainMilliseconds = (int)(target - DateTime.Now).TotalMilliseconds;
                if (remainMilliseconds <= 0)
                    return;

                if (RequestSmokeTest)
                {
                    Tools.LogRegTest(string.Format("<SnapshotSmokeTest> Unsafe Release Snapshot Process *** {0}  doesn't need to sleep {1} minutes.",
                                      snapshotId, (remainMilliseconds / MilisecondsPerMinute).ToString("N2")));
                    return;
                }

                if (reportTime < DateTime.Now)
                {
                    WriteLogIntoDb(snapshotId, string.Format("<LpeRelease> {0} SleepCountDown : {1} minutes.", snapshotId,
                                                     (1.0*remainMilliseconds / MilisecondsPerMinute).ToString("N2")));
                    reportTime = DateTime.Now;
                }

                remainMilliseconds = (int)(target - DateTime.Now).TotalMilliseconds;
                if (remainMilliseconds <= 0)
                {
                    return;
                }

                if (remainMilliseconds > 20 * 1000)
                    remainMilliseconds = 20 * 1000;

                Thread.Sleep(remainMilliseconds); // Sleep 20 second
            }
        }

        bool CanDropOldSnapshot() // Make sure current SNAPSHOT services at least 5 minutes
        {
            const double MilisecondsPerMinute = 60 * 1000;

            while (true)
            {
                // Basically this is ( 5 Min - Release Age )
                double nSleep = s_dropOutOfDateSsAfterXMinutes - m_latestRelease.ReleaseAge.TotalMinutes;

                if (nSleep <= 0)
                {
                    return true;
                }

                if (RequestSmokeTest)
                {
                    int numberOfSecondsNeeded = (int)StoredProcedureHelper.ExecuteScalar(DataSrc.LpeSrc, "DataOldEnough");
                    if (numberOfSecondsNeeded <= 0)
                    {
                        Tools.LogRegTest($"<SnapshotSmokeTest> Unsafe Release Snapshot Process *** don't need to sleep {nSleep:N2} minutes.");
                        return true;
                    }

                    Report($"<LpeRelease> {m_ssId} : someone requested to create snapshot ASAP but data is dirty in db in the last 60 seconds", false);
                    Thread.Sleep(1000 * numberOfSecondsNeeded);
                    continue;
                }

                if (nSleep >= s_waitThresholdInMinutes)
                {
                    Report($"<LpeRelease> {m_ssId} : wait {nSleep:N2} minutes before drop this out of date snapshot.", false);
                    return false; // next lpeRealse launch will drop it.
                }

                Report($"<LpeRelease> {m_ssId} SleepCountDown: {nSleep:N2} minutes.", false);

                int remainMilliseconds = (int)(nSleep * MilisecondsPerMinute);
                if (remainMilliseconds > 20 * 1000)
                {
                    remainMilliseconds = 20 * 1000;
                }

                Thread.Sleep(remainMilliseconds); // Sleep 20 second
            }
        }

        bool CanCreateSnapshot() // Make sure current SNAPSHOT services at least 10 minutes
        {
            DateTime ssDataModD = m_latestRelease.DataLastModifiedD;

            if (LpeTools.GetLpeDataLastModified(DataSrc.LpeSrc) == ssDataModD) 
            {
                // data is not modified.
                Report($"<LpeRelease> both latest release and src data have same last modifed date {ssDataModD.ToLongTimeString()} on {ssDataModD.ToShortDateString()}. Terminate program.");
                return false;
            }

            double minutesAge = m_latestRelease.ReleaseAge.TotalMinutes;
            double minutesAgeSpecified = s_detectLpeSrcDataChangeAfterServiceXMinutes;
            double nSleep = minutesAgeSpecified - minutesAge;

            if (nSleep <= 0)
            {
                return true;
            }

            bool forceReport = (nSleep >= s_waitThresholdInMinutes);
            Report($"<LpeRelease> {m_ssId}: the other dataset's been in service for less than {minutesAgeSpecified} minutes.  Need to wait {nSleep:N1} minutes more", forceReport);

            if (!RequestSmokeTest && nSleep >= s_waitThresholdInMinutes)
            {
                return false;
            }

            MySleep(nSleep);
            return true;
        }

        private void DropOldSnapshot()
        {
            Tools.LogRegTest(string.Format("<LpeRelease> {0}: dropping snapshot with name {1}", m_ssId, m_ssName));

            for (int i = 0; i < 3; ++i)
            {
                try
                {
                    StoredProcedureHelper.ExecuteNonQuery(DataSrc.LpeSrc, "DropDbSnapshot", 1,
                        new SqlParameter[] { new SqlParameter("@SnapshotName", m_ssName) });

                    // Verifying the drop, if it has been dropped successfully, it should be no longer accessible, we should get an SqlException when trying to get some data.
                    try
                    {
                        DateTime lastModD = LpeTools.GetLpeDataLastModified(m_ssDataSrc);
                        string errorMsg = string.Format("{0}: Drop-verification shows that snapshot {1} didn't get dropped. It's still accessible. Its DataLastModifiedD is {2} on {3}",
                            lastModD.ToLongTimeString(), lastModD.ToLongDateString());
                        throw new Exception(errorMsg);
                    }
                    catch (Exception ex)
                    {
                        if (ex is SqlException)
                        {
                            //Tools.LogRegTest( string.Format( "{0}: Verified that snapshot name {1} had been dropped.", m_ssId, m_ssName ) );
                            Report(string.Format("<LpeRelease> {0}: Verified that snapshot name {1} had been dropped.", m_ssId, m_ssName));
                            return;
                        }
                        throw ex;
                    }
                }
                catch (Exception ex)
                {
                    Tools.LogWarning(string.Format("<LpeRelease_Exception> {0}: exception encountered during dropping snapshot {1}. Sleep for 5 seconds and retry", m_ssId, m_ssName), ex);
                    Thread.Sleep(5000);
                }
            }

            string errMsg = String.Format("<LpeRelease_Error> {0}: Critial error encountered. Failed to drop snapshot {1}", m_ssId, m_ssName);
            Tools.LogErrorWithCriticalTracking(errMsg);
            WriteLogIntoDb(m_ssId, errMsg);

            throw new Exception(errMsg);
        }

        private static void CreateSnapshot(string ssId, string ssName)
        {
            bool force = true;
            DateTime reportTime = DateTime.Now;
            while (true)
            {
                try
                {
                    // Wait for the singleton
                    using (CSingletonDistributed singleton = CSingletonDistributed.RequestSingleton(CSingletonDistributed.SingletonObjectT.LpeDataAccessTicket, ssId, 30))
                    {
                        DateTime singletonStartD = DateTime.Now;

                        while (true)
                        {
                            //Tools.LogRegTest(string.Format("<LpeRelease> {0}: Attempting to create snapshot {1} now", ssId, ssName));
                            Report(ssId, string.Format("<LpeRelease> {0}: Attempting to create snapshot {1} now", ssId, ssName), force, ref reportTime);
                            force = false;

                            // Create snapshot  within the same serializable transaction if it found a 30 seconds idle in data.
                            object obj = StoredProcedureHelper.ExecuteScalar(DataSrc.LpeSrc, "CreateLpeSsIfDataOldEnough",
                                new SqlParameter[] { new SqlParameter("@SnapshotName", ssName) });
                            if (obj == null || obj == DBNull.Value)
                            {
                                String strangeErrMsg = string.Format("<LpeRelease_Error> {0}: Strange thing happens in CController code, CreateLpeSsIfDataOldEnough store proc returns null", ssId);
                                Tools.LogError(strangeErrMsg);
                                WriteLogIntoDb(ssId, strangeErrMsg);
                                Thread.Sleep(60000); // sleeping for 60 seconds
                                continue;
                            }

                            int numberOfSecondsNeeded = (int)obj;

                            if (numberOfSecondsNeeded < 0)
                                throw new Exception(string.Format("{0}: Creating snapshot ran into error", ssId));
                            else if (numberOfSecondsNeeded > 0)
                            {
                                Report(ssId, string.Format("<LpeRelease> {0}: Data is dirty in db in the last 60 seconds, ss wasn't created. Need to sleep for {1} seconds.", ssId, numberOfSecondsNeeded.ToString()), force, ref reportTime);
                                Thread.Sleep(1000 * numberOfSecondsNeeded);

                                if (singletonStartD.AddMinutes(5).CompareTo(DateTime.Now) < 0)
                                {
                                    //singletonStartD = DateTime.MaxValue; // to avoid sending too many emails. Singleton feeding code already shows the total time.
                                    //Tools.LogErrorAndSendMail( string.Format( "{0}: this controller is holding singleton for more than 30 min.", ssId ) );
                                    throw new HoldSingletonToolongException(string.Format("{0}: this controller is holding singleton for more than 5 min.", ssId));
                                }
                                continue;
                            }

                            try
                            {
                                Thread.Sleep(10 * 1000); // sleep for new created snapshot is stable                                

                                Report(ssId, string.Format("<LpeRelease> {0}: try to read some data from {1}", ssId, ssName), true /*force*/, ref reportTime);
                                ReadSomeDataFromSnapshot(CLpeReleaseInfo.GetDataSrcFrom(ssId));
                                break;
                            }
                            catch (Exception ex)
                            {
                                Tools.LogError(string.Format("<LpeRelease_Error> {0}: Although CreateLpeSsIfDataOldEnough was executed without error, the verfication process failed with an exception error. Sleeping for 10 seconds and try again.\r\nError:{1}\r\nStackTrace:{2}",
                                    ssId, ex.Message, ex.StackTrace));
                                WriteLogIntoDb(ssId, "Although CreateLpeSsIfDataOldEnough was executed without error, the verfication process failed with an exception error.");
                                Thread.Sleep(10 * 1000);
                            }
                        } // while( true )
                    } // using


                    // at here, we successly create new snapshot

                    Report(ssId, string.Format("<LpeRelease_Validate> {0}: try to load data from {1} now", ssId, ssName), true /*force*/, ref reportTime);

                    // OPM 11141 
                    ValidateLpeSnapshot validateSnapshotObj = new ValidateLpeSnapshot();
                    bool validatePolicyRel = false; // validatePolicyRel = RequestSmokeTest; 04/09/2008 access db too slow. Give up policy realtionship's validation.
                    validateSnapshotObj.Validate(CLpeReleaseInfo.GetDataSrcFrom(ssId), validatePolicyRel); // InvalidLpeSnapshotException can happen

                    DateTime dataModD = LpeTools.GetLpeDataLastModified(CLpeReleaseInfo.GetDataSrcFrom(ssId));
                    Tools.LogRegTest(string.Format("<LpeRelease> {0}: Snapshot {1} has been created and verified. Its data-last-modified-date is {2} on {3}{4}",
                                        ssId, ssName, dataModD.ToLongTimeString(), dataModD.ToShortDateString(),
                                        (validatePolicyRel == false ? ". Sleep 10 seconds" : ""))
                                    );
                    if (validatePolicyRel == false)
                        Thread.Sleep(10 * 1000);

                    return; // snapshot has been created

                } // try
                catch (Exception ex)
                {
                    if (ex is CLpeLoadingDeniedException)
                    {
                        Tools.LogRegTest(string.Format("<LpeRelease_Exception> {0} Attempt to create snapshot failed. Will try again in 10 seconds. Error: {1} ", ssId, ex.Message));
                        Thread.Sleep(10 * 1000);
                    }
                    else if (ex is HoldSingletonToolongException)
                    {
                        Tools.LogRegTest(string.Format("<LpeRelease_Exception> {0} Attempt to create snapshot failed. Will try again in 120 seconds. Error: {1} ", ssId, ex.Message));
                        Thread.Sleep(120 * 1000);
                    }
                    else
                    {
                        Tools.LogErrorWithCriticalTracking(string.Format("<LpeRelease_Exception> {0} Attempt to create snapshot failed. Will try again in 1 min. ", ssId), ex);
                        Thread.Sleep(60000);
                    }
                }
            } // while( true )
        }

        void Report(string msg)
        {
            Report(msg, true);
        }

        void Report(string msg, bool force)
        {
            Report(m_ssId, msg, force, ref m_reportTime);
        }


        static void Report(string ssId, string msg, bool force, ref DateTime reportTime)
        {
            Tools.LogRegTest(msg);
            if (force == false && DateTime.Now < reportTime)
                return;

            if (WriteLogIntoDb(ssId, msg))
                reportTime = DateTime.Now.AddSeconds(s_reportIntervalInSeconds);
        }

        static bool WriteLogIntoDb(string snapshotId, string msg)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@SnapshotId", snapshotId),
                                            new SqlParameter("@Msg", msg)
                                        };
            try
            {
                StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOTransient, "LpeReleaseLog_InsertMsg", 3, parameters);
                return true;
            }
            catch
            {
                return false;
            }
        }

        private static void DeclareNewVersion(string ssId, string ssName)
        {
            DateTime reportTime = DateTime.Now;
            while (true)
            {
                try
                {
                    Tools.LogRegTest(string.Format("<LpeRelease> {0}: declaring new version now", ssId));
                    SqlParameter[] pars = new SqlParameter[] {
                                                                 new SqlParameter( "@SnapshotId", ssId ),
                                                                 new SqlParameter( "@SnapshotName", ssName ),
                                                                 new SqlParameter( "@DataLastModifiedD", LpeTools.GetLpeDataLastModified( CLpeReleaseInfo.GetDataSrcFrom( ssId ) ) )
                                                             };
                    StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "DeclareNewLpeVersion", 3, pars);
                    Tools.LogRegTest(string.Format("<LpeRelease> {0}: done declaring new version", ssId));
                    WriteLogIntoDb(ssId, string.Format("{0}: done declaring new version", ssId));

                    // 12/31/2013 dd - Create a file based snapshot.
                    // 3/19/2014 dd - Temporary turn off snapshot
                    if (ConstStage.HostingServer == "CMG")
                    {
                        // 3/19/2014 dd - Only turn on this capability for CMG right now.
                        LendersOffice.RatePrice.FileBasedPricing.FileBasedPricingUtilities.CreateSnapshot();
                    }

                    return;
                }
                catch (Exception ex)
                {
                    string errMsg = string.Format("{0}: Failed to declare new version of {1}. Sleeping for 30 seconds and will try again", ssId, ssName);
                    Tools.LogErrorWithCriticalTracking(errMsg, ex);
                    if (DateTime.Now >= reportTime)
                    {
                        WriteLogIntoDb(ssId, errMsg);
                        reportTime = DateTime.Now.AddMinutes(2);
                    }
                    Thread.Sleep(30000);
                }
            }
        }

        private class HoldSingletonToolongException : CBaseException
        {
            public HoldSingletonToolongException(string errDevMsg) : base("Hold CSingletonDistributed too long", errDevMsg) { }
        }

        static string m_readSomeDataQuery = "SELECT top 2 * FROM LOAN_PRODUCT_FOLDER ; " +
            "SELECT top 2 * FROM LOAN_PROGRAM_TEMPLATE ; " +
            "SELECT top 2 * FROM PRICE_POLICY ; " +
            "SELECT top 2 * FROM PRODUCT_PRICE_ASSOCIATION ; " +
            "SELECT top 2 * FROM RATE_OPTIONS ; ";

        private static void ReadSomeDataFromSnapshot(DataSrc dataSrc)
        {
            LpeTools.GetLpeDataLastModified(dataSrc);
            DataSet ds = new DataSet();
            DBSelectUtility.FillDataSet(dataSrc, ds, m_readSomeDataQuery, null, null);
        }

        // helper methods
        static Mutex CreateMutex(string mutexName, out bool createdNew)
        {
            var allowEveryoneRule =
                    new MutexAccessRule(
                        new SecurityIdentifier(WellKnownSidType.WorldSid, null),
                        MutexRights.FullControl, AccessControlType.Allow);
            var securitySettings = new MutexSecurity();
            securitySettings.AddAccessRule(allowEveryoneRule);

            return new Mutex(false, mutexName, out createdNew, securitySettings);
        }

        double ReadConfigVar(string id, double defVal)
        {
            string strVal = ConfigurationManager.AppSettings[id];
            
            if (string.IsNullOrEmpty(strVal))
            {
                Tools.LogRegTest($"<LpeRelease_Warning> the config file doesn't contain the key {id}. Use the defaul value {defVal}");
                return defVal;
            }
            return double.Parse(strVal);

        }

    }
}
