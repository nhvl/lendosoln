﻿namespace ScheduleExecutable
{
    using System;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.Threading;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Security;
    using LqbGrammar.Drivers.SecurityEventLogging;

    public class LoanCreationStressTestClass
    {
        private AbstractUserPrincipal Login(string loginName, string password)
        {
            PrincipalFactory.E_LoginProblem loginProblem = PrincipalFactory.E_LoginProblem.None;

            AbstractUserPrincipal principal = PrincipalFactory.CreateWithFailureType(loginName, password, out loginProblem, true, false, LoginSource.Website);

            if (null == principal)
            {
                throw new CBaseException(ErrorMessages.GenericAccessDenied, "Invalid username/password");
            }

            return principal;

        }

        public static void Execute(string[] args)
        {
            int numOfThread = 20;
            Thread[] list = new Thread[numOfThread];

            LoanCreationStressTestClass[] objList = new LoanCreationStressTestClass[numOfThread];


            for (int i = 0; i < numOfThread; i++)
            {
                objList[i] = new LoanCreationStressTestClass();
                list[i] = new Thread(new ThreadStart(objList[i].SingleTest));

            }

            for (int i = 0; i < numOfThread; i++)
            {
                list[i].Start();
            }
            for (int i = 0; i < numOfThread; i++)
            {
                list[i].Join();
            }
            for (int i = 0; i < numOfThread; i++)
            {
                Console.WriteLine(i + " - Avg Time: " + (objList[i].TotalDuration / objList[i].Count) + "ms. Error Count:" + objList[i].ErrorCount);
            }


            Console.WriteLine("Finish");
        }

        public int ErrorCount { get; private set; }

        public int Count { get; private set; }
        public long TotalDuration { get; private set; }
        public void SingleTest()
        {
            AbstractUserPrincipal principal = Login("lotest001", "lodblodb2");


            Guid templateId = Tools.GetLoanIdByLoanName(principal.BrokerId, "template1");

            for (int i = 0; i < 20; i++)
            {
                Guid sLId = Guid.Empty;

                Stopwatch sw = Stopwatch.StartNew();

                try
                {
                    CLoanFileCreator fileCreator = CLoanFileCreator.GetCreator(principal, LendersOffice.Audit.E_LoanCreationSource.FromTemplate);
                    sLId = fileCreator.CreateLoanFromTemplate(templateId: templateId);


                    CPageData dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(LoanCreationStressTestClass));
                    dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                    dataLoan.sNoteIR_rep = "7.00%";
                    CAppData dataApp = dataLoan.GetAppData(0);
                    dataApp.aBFirstNm = "First Name";

                    dataLoan.Save();
                }
                catch (SqlException)
                {
                    ErrorCount++;
                }
                catch (CBaseException)
                {
                    ErrorCount++;
                }
                finally
                {
                    if (sLId != Guid.Empty)
                    {
                        Tools.DeclareLoanFileInvalid(principal, sLId, false, false);
                    }
                }
                sw.Stop();
                Count++;
                TotalDuration += sw.ElapsedMilliseconds;
            }
        }
    }
}
