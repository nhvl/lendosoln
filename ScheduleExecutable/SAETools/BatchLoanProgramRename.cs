﻿namespace ScheduleExecutable.SAETools
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using ClosedXML.Excel;
    using DataAccess;
    using LendersOffice.Drivers.Gateways;

    class BatchLoanProgramRename
    {
        public static void Execute(string[] args)
        {
            string path = args[1];

            string[] excelFileList = Directory.GetFiles(path, "*.xlsx");

            if (excelFileList.Length > 0)
            {
                foreach (string excelFile in excelFileList)
                {
                    string errorMessage = string.Empty;
                    if (ProcessExcel(excelFile, out errorMessage))
                    {

                        FileOperationHelper.Move(excelFile, excelFile.Replace(path, path + @"\processed"));
                    }
                }

            }
        }

        private static bool ProcessExcel(string excelFile, out string errorMessage)
        {
            errorMessage = string.Empty;
            try
            {



                XLWorkbook workbook = new XLWorkbook(excelFile);

                IXLWorksheet worksheet = workbook.Worksheet("Main");

                if (worksheet == null)
                {
                    errorMessage = "Unable to locate worksheet name'MAIN'  in [" + excelFile + "]";
                    return false;
                }

                int lastRow = worksheet.LastRowUsed().RowNumber();
                for (int row = 2; row <= lastRow; row++)
                {
                    string colA = worksheet.Cell(row, 1).GetString();
                    string colB = worksheet.Cell(row, 2).GetString();
                    string colC = worksheet.Cell(row, 3).GetString();

                    if (string.IsNullOrEmpty(colA) || string.IsNullOrEmpty(colB) || string.IsNullOrEmpty(colC))
                    {
                        continue; // Skip row
                    }

                    string status = string.Empty;

                    Guid lLpTemplateId = Guid.Empty;

                    if (string.IsNullOrEmpty(colA) == false)
                    {
                        try
                        {
                            lLpTemplateId = new Guid(colA);
                        }
                        catch (FormatException)
                        { }
                    }

                    if (lLpTemplateId == Guid.Empty)
                    {
                        status = "SKIP. lLpTemplateId is invalid or empty.";
                    }
                    else if (string.IsNullOrEmpty(colB))
                    {
                        status = "SKIP. Old Name cannot be empty.";
                    }
                    else if (string.IsNullOrEmpty(colC))
                    {
                        status = "SKIP. New name cannot be empty.";
                    }
                    else if (colB.Equals(colC, StringComparison.OrdinalIgnoreCase))
                    {
                        status = "SKIP. New name is same as old name.";
                    }
                    else
                    {
                        status = Update(lLpTemplateId, colB, colC);
                    }
                    worksheet.Cell(row, 4).SetValue(status);
                }
                workbook.Save();
                return true;
            }
            catch (IOException)
            {
                return false; // Someone has Excel file open.
            }
        }

        private static string Update(Guid lLpTemplateId, string oldName, string newName)
        {
            try
            {
                CLoanProductData data = new CLoanProductData(lLpTemplateId);
                data.InitSave();
                if (data.IsMaster)
                {
                    return "SKIP. Cannot update Master program.";
                }
                if (data.IsMasterPriceGroup)
                {
                    return "SKIP. Cannot update Master Price Group program";
                }
                if (data.lLpTemplateNm.Equals(oldName, StringComparison.OrdinalIgnoreCase) == false)
                {
                    return "SKIP. Old name does not match.";
                }

                if (data.lLpTemplateNm.Equals(newName, StringComparison.OrdinalIgnoreCase) == true)
                {
                    return "SKIP. New name is same as old name.";
                }

                data.lLpTemplateNmOverrideBit = true;
                data.lLpTemplateNm = newName;

                data.Save();
                return "OK";
            }
            catch (IndexOutOfRangeException)
            {
                return "SKIP. Could not find record.";
            }
        }
    }
}
