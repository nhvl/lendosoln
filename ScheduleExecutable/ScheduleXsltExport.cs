﻿namespace ScheduleExecutable
{
    using System;
    using System.Collections.Generic;
    using LendersOffice.XsltExportReport;

    class ScheduleXsltExport
    {
        public static void Execute(string[] args)
        {
            if (args.Length != 4)
            {
                Console.WriteLine("Usage: ScheduleExecutable ScheduleXsltExport [ftp|email] customerCode exportName");
                return;
            }

            string action = args[1];
            string customerCode = args[2];
            string exportName = args[3];

            bool isFtp = action.Equals("FTP", StringComparison.OrdinalIgnoreCase);
            if (!isFtp && !action.Equals("Email", StringComparison.OrdinalIgnoreCase))
            {
                throw new ArgumentException("ScheduleXsltExport error: action should be FTP or Email");
            }

            var settings = XsltExportTransferSettings.RetrieveByExportName(exportName, customerCode);
            if (settings == null)
            {
                throw new Exception($"XsltExportTransferSettings.RetrieveByExportName('{exportName}', '{customerCode}') error.");
            }

            if (isFtp)
            {
                SFtpXsltExport(settings);
            }
            else
            {
                EmailXsltExport(settings);
            }
        }

        private static void EmailXsltExport(XsltExportTransferSettings settings)
        {
            if(!settings.IsSupportedEmail() )
            {
                throw new Exception("The email settings is not valid.");
            }

            List<KeyValuePair<string, string>> parameterList = new List<KeyValuePair<string, string>>();
            parameterList.Add(new KeyValuePair<string, string>("From", settings.From));
            parameterList.Add(new KeyValuePair<string, string>("To", settings.To));
            parameterList.Add(new KeyValuePair<string, string>("Subject", settings.Subject));
            parameterList.Add(new KeyValuePair<string, string>("Message", settings.Message));

            EmailXsltExportCallBack callBack = new EmailXsltExportCallBack();

            var xsltRequest = XsltExportRequest.CreateFromCustomReport(settings.BrokerId, settings.UserId, settings.ReportId, settings.MapName, callBack, parameterList);

            XsltExportResult result = XsltExport.ExecuteSynchronously(xsltRequest);

        }

        private static void SFtpXsltExport(XsltExportTransferSettings settings)
        {
            if (!settings.IsSupportedFtp())
            {
                throw new Exception("The sFtp setting is not valid.");
            }

            // opm 468014 2018/02/02 with current code, the database table contains double encrypted password, 
            // TransferOptions.Password contains the encrypted password => we need to decrypt.

            SFTPXsltExport.SFTPXsltExport.ExportFromXElement(settings.ToXElement(), settings.ExportName, settings.BrokerId);
        }
    }
}
