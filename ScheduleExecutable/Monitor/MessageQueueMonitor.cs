﻿namespace ScheduleExecutable.Monitor
{
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.BackgroundJobs;
    using LendersOffice.ObjLib.DatabaseMessageQueue;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class MessageQueueMonitor
    {
        public static void EnqueueDBMessageQueueStats(string[] args)
        {
            EnqueueDBMQStats();
            EnqueueBackgroundJobStats();
            EnqueueLoanFileCacheErrorStats();
        }

        private static void Enqueue(DateTime ut, DBMessageQueueStatistic stats)
        {
            string indexName = $"ml_lqb_monitor-{ut.Year}.{ut.DayOfYear / 7}/monitor";
            LendersOffice.Drivers.Gateways.MessageQueueHelper.SendJSON(ConstStage.MSMQ_ElasticSearch, indexName, stats);
        }

        private static void EnqueueBackgroundJobStats()
        {
            var jobs = new List<Tuple<ProcessingStatus, JobType, int, DateTime>>();
            DateTime timeFetched = DateTime.Now;
            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "BACKGROUND_JOBS_GetStatus"))
            {
                while (reader.Read())
                {
                    ProcessingStatus pstat = (ProcessingStatus)reader["ProcessingStatus"];
                    JobType jobType = (JobType)reader["JobType"];
                    int numbJobs = (int)reader["NumJobs"];
                    DateTime dt = (DateTime)reader["OldestJobDateCreated"];
                    jobs.Add(Tuple.Create(pstat, jobType, numbJobs, dt));
                }
            }

            HashSet<string> seen = new HashSet<string>();
            foreach (var entry in jobs)
            {
                int seconds = Convert.ToInt32(timeFetched.Subtract(entry.Item4).TotalSeconds);
                string name = $"{entry.Item2}_{entry.Item1}";
                var stat = new DBMessageQueueStatistic(-1, name, entry.Item3, seconds, ConstStage.EnvironmentName, timeFetched);
                seen.Add(name);
                Enqueue(timeFetched.ToUniversalTime(), stat);
            }

            foreach (var jt in Enum.GetValues(typeof(JobType)))
            {
                foreach (var st in Enum.GetValues(typeof(ProcessingStatus)))
                {
                    string name = $"{jt}_{st}";
                    if (seen.Contains(name))
                    {
                        continue;
                    }

                    var s = new DBMessageQueueStatistic(-1, name, 0, 0, ConstStage.EnvironmentName, timeFetched);
                    Enqueue(timeFetched.ToUniversalTime(), s);
                }
            }
        }

        private static void EnqueueDBMQStats()
        {
            var stats = DBMessageQueue.GetQueueCounts();

            foreach (var stat in stats)
            {
                DateTime dt;
                if (DateTime.TryParse(stat.Timestamp, out dt) == false)
                {
                    Tools.LogError($"Unable to parse timestamp. Value=[{stat.Timestamp}]");
                    return;
                }

                dt = dt.ToUniversalTime();
                Enqueue(dt, stat);
            }
        }

        private static void EnqueueLoanFileCacheErrorStats()
        {
            var date = DateTime.Now;
            foreach (var con in DbConnectionInfo.ListAll())
            {
                int count = 0;
                int elapsedSeconds = 0;

                using (var reader = StoredProcedureHelper.ExecuteReader(con, "LOAN_CACHE_UPDATE_REQUEST_GetMaxAttemptEntries", null, null))
                {
                    if (reader.Read())
                    {
                        count = (int)reader["Count"];
                                             
                        try
                        {
                            if (count > 0)
                            {
                                DateTime minDate = (DateTime)reader["FirstOccurD"];
                                elapsedSeconds = Convert.ToInt32(date.Subtract(minDate).TotalSeconds);
                            }
                        }
                        catch (OverflowException)
                        {
                            elapsedSeconds = int.MaxValue;
                        };
                    }
                }

                var s = new DBMessageQueueStatistic(-1, $"LoanCacheError_{con.InitialCatalog}", count, elapsedSeconds, ConstStage.EnvironmentName, date);
                Enqueue(date.ToUniversalTime(), s);
            }
        }
    }
}
