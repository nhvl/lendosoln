﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using DataAccess;
using LendersOffice.Constants;

namespace ScheduleExecutable
{
    class CheckExpiredPricing
    {
        private static bool useNewEmailer = false;

        public static void Check(string[] args)
        {

            // 12/9/2010 dd - OPM 59724 - Setup a script that notify SAE via e-mail if pricing is expired for more than one day.
            DateTime dt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);

            SqlParameter[] parameters = {
                                            new SqlParameter("@FileVersionDateTime", dt)
                                        };
            bool hasExpired = false;
            StringBuilder sb = new StringBuilder();
            List<string> customLenderProductList = new List<string>();
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.RateSheet, "RS_ListInvestorXlsFileWithFileVersionDateLaterThan", parameters))
            {
                while (reader.Read())
                {
                    hasExpired = true;
                    string xlsFileName = (string)reader["InvestorXlsFileName"];
                    sb.AppendLine(string.Format("{0} - {1}. FileVersionDateTime={2}", reader["InvestorXlsFileId"], xlsFileName, reader["FileVersionDateTime"]));
                    if (Regex.IsMatch(xlsFileName, "^PML[0-9]+_CUSTOM"))
                    {
                        // 7/29/2011 dd - From eOPM 303414 if the investor xls file name has the following format PML####_CUSTOM then
                        // we will send out reminder.
                        customLenderProductList.Add(xlsFileName);
                    }

                }
            }
            string recipientAddress = ConfigurationManager.AppSettings["RecipientAddress"];
            if (hasExpired)
            {
                // 12/10/2010 dd - Multiple recipients are separating by comma.

                // Send Email
                // 12/10/2010 dd - There is no quick and easy way to use the email client in LendersOfficeLib. Therefore
                // I use the SmtpClient code directly to email.
                SmtpClient client = new SmtpClient(ConstStage.SmtpServer, ConstStage.SmtpServerPortNumber);
                client.Send(ConstApp.DefaultErrorEmailFromAddress, recipientAddress, "[INVESTOR_PRICING_WARNING] - " + DateTime.Now, sb.ToString());

                // 7/29/2011 dd - Implement eOPM 303414
                List<string> reminderList = new List<string>();
                foreach (var customProduct in customLenderProductList)
                {
                    GetEmailSender(reminderList, customProduct);
                }

                if (useNewEmailer)
                {
                    CheckNEW(recipientAddress, sb.ToString(), reminderList);
                }
                else
                {
                    CheckOLD(recipientAddress, sb.ToString(), reminderList);
                }
            }
        }

        private static void CheckOLD(string recipientAddress, string firstMessage, List<string> reminderList)
        {
            // Send Email

            // 12/10/2010 dd - There is no quick and easy way to use the email client in LendersOfficeLib. Therefore
            // I use the SmtpClient code directly to email.
            SmtpClient client = new SmtpClient(ConstStage.SmtpServer, ConstStage.SmtpServerPortNumber);
            client.Send(ConstApp.DefaultErrorEmailFromAddress, recipientAddress, "[INVESTOR_PRICING_WARNING] - " + DateTime.Now, firstMessage);

            var from = LqbGrammar.DataTypes.EmailAddress.BadEmail;

            string fromEmail = "support@pricemyloan.com";
            string emailMessage = GetMessage(fromEmail);

            foreach (var sender in reminderList)
            {
                try
                {
                    MailMessage msg = new MailMessage();
                    msg.From = new MailAddress("support@pricemyloan.com");
                    msg.To.Add(new MailAddress(sender));
                    msg.To.Add(new MailAddress("support@pricemyloan.com"));
                    msg.Subject = "Ratesheet Reminder";
                    msg.Body = emailMessage;
                    client.Send(msg);
                }
                catch (SmtpFailedRecipientsException)
                {
                    client.Send(ConstApp.DefaultErrorEmailFromAddress, recipientAddress, "[ERROR] - Unable to send auto ratesheet reminder to " + sender, "Unable to send auto ratesheet reminder to " + sender);
                }
            }
        }

        private static void CheckNEW(string recipientAddress, string firstMessage, List<string> reminderList)
        {
            // Send Email

            LqbGrammar.DataTypes.EmailServerName? server = LqbGrammar.DataTypes.EmailServerName.Create(ConstStage.SmtpServer);
            if (server == null)
            {
                throw new LqbGrammar.Exceptions.DeveloperException(LqbGrammar.DataTypes.ErrorMessage.BadConfiguration);
            }

            LqbGrammar.DataTypes.PortNumber? port = LqbGrammar.DataTypes.PortNumber.Create(ConstStage.SmtpServerPortNumber);
            if (port == null)
            {
                throw new LqbGrammar.Exceptions.DeveloperException(LqbGrammar.DataTypes.ErrorMessage.BadConfiguration);
            }

            LqbGrammar.DataTypes.EmailAddress? fromDef = LqbGrammar.DataTypes.EmailAddress.Create(ConstApp.DefaultErrorEmailFromAddress); // this is mclerrors@meridianlink.com!  should be something like lqberrors@lendingqb.com instead

            var listRecipients = LqbGrammar.DataTypes.EmailAddress.ParsePotentialList(recipientAddress);

            LqbGrammar.DataTypes.EmailSubject? subject = LqbGrammar.DataTypes.EmailSubject.Create("[INVESTOR_PRICING_WARNING] - " + DateTime.Now);
            if (subject == null)
            {
                throw new LqbGrammar.Exceptions.DeveloperException(LqbGrammar.DataTypes.ErrorMessage.SystemError);
            }

            var body = new LqbGrammar.Drivers.Emailer.EmailTextBody();
            body.AppendText(firstMessage);

            var emailPackage = new LqbGrammar.Drivers.Emailer.EmailPackage(ConstAppDavid.SystemBrokerGuid, fromDef.Value, subject.Value, body, listRecipients.ToArray());

            LendersOffice.Drivers.Emailer.EmailHelper.SendEmail(server.Value, port.Value, emailPackage);

            var from = LqbGrammar.DataTypes.EmailAddress.BadEmail;

            subject = LqbGrammar.DataTypes.EmailSubject.Create("Ratesheet Reminder");
            from = LqbGrammar.DataTypes.EmailAddress.PriceMyLoanSupport;

            string emailMessage = GetMessage(from.ToString());

            body = new LqbGrammar.Drivers.Emailer.EmailTextBody();
            body.AppendText(emailMessage);

            foreach (var sender in reminderList)
            {
                try
                {
                    emailPackage = new LqbGrammar.Drivers.Emailer.EmailPackage(ConstAppDavid.SystemBrokerGuid, from, subject.Value, body, CreateEmailAddress(sender), LqbGrammar.DataTypes.EmailAddress.PriceMyLoanSupport);
                    LendersOffice.Drivers.Emailer.EmailHelper.SendEmail(server.Value, port.Value, emailPackage);
                }
                catch (LqbGrammar.Exceptions.LqbException ex) when (ex.InnerException is System.Net.Mail.SmtpFailedRecipientsException)
                {
                    LqbGrammar.DataTypes.EmailSubject? errSub = LqbGrammar.DataTypes.EmailSubject.Create("[ERROR] - Unable to send auto ratesheet reminder to " + sender);

                    var errBody = new LqbGrammar.Drivers.Emailer.EmailTextBody();
                    errBody.AppendText("Unable to send auto ratesheet reminder to " + sender);

                    emailPackage = new LqbGrammar.Drivers.Emailer.EmailPackage(ConstAppDavid.SystemBrokerGuid, fromDef.Value, errSub.Value, errBody, listRecipients.ToArray());
                    LendersOffice.Drivers.Emailer.EmailHelper.SendEmail(server.Value, port.Value, emailPackage);
                }
            }
        }

        private static string GetMessage(string fromEmail)
        {
            // 8/1/2011 dd - The content is specify in eOPM 303414.
            return string.Format(@"Hi,

We have not received your custom product ratesheets at our ""pmlrates@meridianlink.com"". Could you please verify and forward any missing ratesheets to that mailbox if needed?

[WARNING: THIS MESSAGE WAS AUTOMATICALLY GENERATED, FOR ANY INQUIRIES, PLEASE CONTACT {0}]

Sincerely,
--
PML Support Team - NEWSMTP
{1}", fromEmail.ToUpper(), fromEmail);
        }

        private static LqbGrammar.DataTypes.EmailAddress CreateEmailAddress(string address)
        {
            LqbGrammar.DataTypes.EmailAddress? email = LqbGrammar.DataTypes.EmailAddress.Create(address);
            if (email != null)
            {
                return email.Value;
            }
            else
            {
                throw new LqbGrammar.Exceptions.DeveloperException(LqbGrammar.DataTypes.ErrorMessage.SystemError);
            }
        }
        private static void GetEmailSender(List<string> reminderList, string customProduct)
        {
            // Cannot unit test as this is within application code...I will copy/paste to the testing library

            string query = "SELECT Sender FROM EMAIL_DOWNLOAD_LIST WHERE TargetFileName = @TargetFileName";
            SqlParameter[] customProductParameters = { DbAccessUtils.SqlParameterForVarchar("@TargetFileName", customProduct) };

            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                if (reader.Read())
                {
                    // NOTE: in LoDevRateSheet we have multiple entries for the same TargetFileName
                    //       which can result in multiple entries with the same value...
                    //       We probably want a set of distinct values but I would need confirmation
                    //       prior to making that change.
                    reminderList.Add((string)reader["Sender"]);
                }
            };

            DBSelectUtility.ProcessDBData(DataSrc.RateSheet, query, null, customProductParameters, readHandler);
        }
    }
}
