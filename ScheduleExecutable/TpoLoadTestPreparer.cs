﻿namespace ScheduleExecutable.TpoLoadTest
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.Task;
    using LendersOffice.Security;
    using LendersOffice.Common.TextImport;
    using System.Data;
    using System.IO;

    internal class Preparer
    {
        internal static void ExecuteFromCsv(string[] args)
        {
            if(args.Length != 2)
            {
                WriteCsvUsage();
                return;
            }
            int successCount = 0;
            int totalRows = -1;
            try
            {
                var baseErrorsDirectory = Directory.CreateDirectory("tpo_load_test_setup_errors").FullName;
                string parserErrorsFileName = Path.Combine(baseErrorsDirectory, "parseErrors.txt");
                string badBrokerIdsFileName = Path.Combine(baseErrorsDirectory, "badbrokerids.txt");
                string badUserIdsFileName = Path.Combine(baseErrorsDirectory, "baduserids.txt");
                var inputCsvFileName = args[1];

                // create csv with rowid, brokerid, userid, username, pw
                // read it in with headers from the DataParsing.FileToDataTable
                // create a csv output file where the results wil go
                // for each input row create an output entry in the csv, or create a file like tpo_rowid.error with the error messages.

                ILookup<int, string> parseErrors;
                var table = DataParsing.FileToDataTable(inputCsvFileName, true, string.Empty, out parseErrors);

                if (parseErrors.Any())
                {
                    using (var fs = File.OpenWrite(parserErrorsFileName))
                    using (var tw = new StreamWriter(fs))
                    {
                        foreach (IGrouping<int, string> parseError in parseErrors)
                        {
                            var errorMessage = "line " + parseError.Key + string.Join(";", parseError.SelectMany(s => s));
                            tw.WriteLine(errorMessage);
                        }
                    }
                    Console.WriteLine($"Has errors parsing inputted csv.  Check {parserErrorsFileName}.");
                }

                var rowIdColumn = table.Columns["RowId"];
                var brokerIdColumn = table.Columns["BrokerId"];
                var userIdColumn = table.Columns["UserId"];
                var userNameColumn = table.Columns["UserName"];
                var passwordColumn = table.Columns["Password"];

                var outputCsvFileName = "tpoloadtestinfo-XXX.csv";
                var remainingCsvFileName = "tpoloadtestinfo-remaining.csv";
                totalRows = table.Rows.Count;

                File.WriteAllText(outputCsvFileName, string.Empty);
                File.WriteAllText(remainingCsvFileName, string.Empty);
                
                // I'm using append text instead of a TextWriter since the process can run long and I want to be able to peek.
                File.AppendAllText(outputCsvFileName, 
                    "RowId,BrokerId,UserId,UserName,Password" + Environment.NewLine);

                foreach (DataRow row in table.Rows)
                {
                    var rowId = row[rowIdColumn].ToString();

                    var userName = row[userNameColumn].ToString();
                    var password = row[passwordColumn].ToString();

                    Guid brokerId;
                    {
                        var brokerIdString = row[brokerIdColumn].ToString();
                        if (!Guid.TryParse(brokerIdString, out brokerId))
                        {
                            File.AppendAllText(badBrokerIdsFileName, $"rowid:{rowId}, brokerId:{brokerId}");
                            File.AppendAllText(remainingCsvFileName, 
                                string.Join(",", new string[] { rowId, row[brokerIdColumn].ToString(), row[userIdColumn].ToString(), userName, password }) + Environment.NewLine);
                            continue;
                        }
                    }

                    Guid userId;
                    {
                        var userIdString = row[userIdColumn].ToString();
                        if (!Guid.TryParse(userIdString, out userId))
                        {
                            File.AppendAllText(badUserIdsFileName, $"rowid:{rowId}, brokerId:{brokerId}" + Environment.NewLine);
                            File.AppendAllText(remainingCsvFileName, 
                                string.Join(",", new string[] { rowId, row[brokerIdColumn].ToString(), row[userIdColumn].ToString(), userName, password }) + Environment.NewLine);
                            continue;
                        }
                    }

                    var options = new PreparationOptions()
                    {
                        BrokerId = brokerId,
                        UserId = userId,
                        UserOptions = new UserCreationOptions()
                        {
                            UserName = userName,
                            Password = password
                        }
                    };

                    var result = CreatePUserAndLoan(options);
                    if (result.ErrorMessages.Any())
                    {
                        var errorFileName = Path.Combine(baseErrorsDirectory, "tpo" + rowId + ".error.txt");
                        Console.WriteLine($"row {rowId} failed.  Check {errorFileName}");
                        File.WriteAllLines(errorFileName, result.ErrorMessages);
                        File.AppendAllText(
                            remainingCsvFileName, 
                            string.Join(",", new string[] { rowId, row[brokerIdColumn].ToString(), row[userIdColumn].ToString(), userName, password }) + Environment.NewLine);
                    }
                    else
                    {
                        // username,pw,lenderpmlsiteid,loanid,appid,qpanonymousid,employeeid

                        var line = string.Join(",", new string[]
                        {
                            options.UserOptions.UserName,
                            options.UserOptions.Password,
                            result.LenderPmlSiteId.ToString(),
                            result.CreatedLoanResult.CreatedLoanId.ToString(),
                            result.CreatedLoanResult.CreatedAppId.ToString(),
                            Guid.Empty.ToString(),//result.CreatedUserResult.CreatedUserAnonymousQpId.ToString(),
                            result.CreatedUserResult.CreatedEmployeeId.ToString()
                        });

                        File.AppendAllText(outputCsvFileName, line + Environment.NewLine);
                        successCount++;
                        Console.WriteLine($"row {rowId} succeeded.");
                    }

                }
            }
            catch(Exception e)
            {
                Tools.LogError(e);
                Console.WriteLine(e);
                File.WriteAllText("tpoloadtestsetup.error.txt", e.Message);
            }

            Console.WriteLine($"All done.  {successCount} / {totalRows} succeeded.");
        }

        internal static void ExecuteOnce(string[] args)
        {
            if (args.Length != 5)
            {
                WriteOnetimeUsage();
                return;
            }

            Guid brokerId;
            {
                var brokerIdString = args[1];
                if (!Guid.TryParse(brokerIdString, out brokerId))
                {
                    Console.WriteLine($"args[1] '{args[1]}' is not a valid GUID.");
                    WriteOnetimeUsage();
                    return;
                }
            }

            Guid userId;
            {
                var userIdString = args[2];
                if (!Guid.TryParse(userIdString, out userId))
                {
                    Console.WriteLine($"args[2] '{args[2]}' is not a valid GUID.");
                    WriteOnetimeUsage();
                    return;
                }
            }

            string userName = args[3];
            string password = args[4];
            var options = new PreparationOptions()
            {
                BrokerId = brokerId,
                UserId = userId,
                UserOptions = new UserCreationOptions()
                {
                    UserName = userName,
                    Password = password
                }
            };

            var result = CreatePUserAndLoan(options);
            SerializationHelper.JsonSerializeToStream(Console.Out, result);
        }

        public class PreparationResult
        {
            private List<string> errorMessages = new List<string>();
            public List<string> ErrorMessages
            {
                get
                {
                    return 
                        this.errorMessages
                        .Union(CreatedUserResult?.ErrorMessages ?? new List<string>())
                        .Union(CreatedLoanResult?.ErrorMessages ?? new List<string>())
                        .ToList();
                }
            }

            public void AddErrorMessage(string message)
            {
                this.errorMessages.Add(message);
            }

            public Guid LenderPmlSiteId { get; set; }

            public CreatedUserResult CreatedUserResult { get; set; }
            public CreatedLoanResult CreatedLoanResult { get; set; }
        }

        public class CreatedUserResult
        {
            public List<string> ErrorMessages { get; set; } = new List<string>();
            public Guid CreatedUserId { get; set; }
            public Guid CreatedEmployeeId { get; set; }
            // public Guid CreatedUserAnonymousQpId { get; set; }
        }

        public class CreatedLoanResult
        {
            public List<string> ErrorMessages { get; set; } = new List<string>();
            public Guid CreatedLoanId { get; set; }
            public Guid CreatedAppId { get; set; }
        }

        public class PreparationOptions
        {
            public Guid BrokerId { get; set; }
            public Guid UserId { get; set; }
            public UserCreationOptions UserOptions { get; set; }
            //public LoanCreationOptions LoanOptions { get; set; }

            //public string UserName { get; set; }
            //public string Password { get; set; }

            //public Guid pUserId { get; set; }
            //public Guid pEmployeeId { get; set; }
        }

        public class UserCreationOptions
        {
            public string UserName { get; set; }
            public string Password { get; set; }
        }

        public class LoanCreationOptions
        {
            public Guid TpoUserId { get; set; }
            public Guid TpoEmployeeId { get; set; }
        }

        private static PreparationResult CreatePUserAndLoan(PreparationOptions options)
        {
            var result = new PreparationResult();
            try
            {
                var principal = (BrokerUserPrincipal)PrincipalFactory.Create(options.BrokerId, options.UserId, "B", true, false);
                if ((!principal.HasPermission(Permission.CanAdministrateExternalUsers)) || principal.HasFeatures(E_BrokerFeatureT.PriceMyLoan) == false)
                {
                    result.AddErrorMessage("Permission denied.  You lack authorization to invoke this command.");
                    return result;
                }

                var broker = principal.BrokerDB;

                if (!broker.Name.StartsWith("LOAD"))
                {
                    result.AddErrorMessage($"You cannot create P users at non load test brokers.  Your broker {broker.Name} does not start with 'LOAD'");
                    return result;
                }

                // don't actually need quick pricer just yet.
                //if (!broker.IsQuickPricerEnable)
                //{
                //    result.AddErrorMessage($"broker with customer code {broker.CustomerCode} does not have QuickPricer enabled.");
                //}

                result.LenderPmlSiteId = broker.PmlSiteID;

                result.CreatedUserResult = CreatePUser(principal, options);

                if (result.ErrorMessages.Any())
                {
                    return result;
                }

                var loanCreationOptions = new LoanCreationOptions()
                {
                    TpoUserId = result.CreatedUserResult.CreatedUserId,
                    TpoEmployeeId = result.CreatedUserResult.CreatedEmployeeId
                };

                result.CreatedLoanResult = CreateLoan(principal, loanCreationOptions);
            }
            catch(Exception e)
            {
                // tdsk: add this to exception list if needed.
                result.AddErrorMessage(e.Message);
                Tools.LogError(e);
            }

            return result;
        }

        private static CreatedUserResult CreatePUser(BrokerUserPrincipal principal, PreparationOptions options)
        {
            var result = new CreatedUserResult();

            var broker = principal.BrokerDB;
            var pmlBroker = PmlBroker.ListAllActivePmlBrokerByBrokerId(principal.BrokerId).First();

            var pUser = new EmployeeDB();

            pUser.EnableAuthCodeViaSms = false;
            pUser.EnabledMultiFactorAuthentication = false;
            pUser.EnabledClientDigitalCertificateInstall = false;

            //ClientScript.RegisterHiddenField("UsersCurrentOc", m_CompanyChoice.Data);
            //ClientScript.RegisterHiddenField("UsersCurrentOcName", m_CompanyChoice.Text);
            pUser.FirstName = "TestTpoUser";
            //pUser.MiddleName = ;
            pUser.LastName = "@" + broker.CustomerCode;
            //pUser.NmOnLoanDocs = pUser.FirstName + " " + pUser.LastName;
            pUser.NmOnLoanDocsLckd = false;

            //pUser.PmlExternalManagerEmployeeId = new Guid(m_SupervisorChoice.Data);

            //pUser.Address.StreetAddress = m_Address.Text.TrimWhitespaceAndBOM();
            //pUser.Address.City = m_City.Text.TrimWhitespaceAndBOM();
            //pUser.Address.State = m_State.Value.TrimWhitespaceAndBOM();
            //pUser.Address.Zipcode = m_Zipcode.Text.TrimWhitespaceAndBOM();
            pUser.Phone = "(123) 123-1231";
            pUser.PrivateCellPhone = pUser.Phone;
            //pUser.IsCellphoneForMultiFactorOnly = this.IsCellphoneForMultiFactorOnly.Checked;
            //pUser.Pager = m_Pager.Text.TrimWhitespaceAndBOM();
            //pUser.Fax = m_Fax.Text.TrimWhitespaceAndBOM();
            pUser.Email = "donotreply@lendinqgb.com"; //m_Email.Text.TrimWhitespaceAndBOM();
            //pUser.NotesByEmployer = m_Notes.Text;
            pUser.TaskRelatedEmailOptionT = E_TaskRelatedEmailOptionT.DontReceiveEmail;

            pUser.PasswordExpirationD = DateTime.MaxValue;

            pUser.BrokerID = principal.BrokerId;
            pUser.IsSharable = true;
            pUser.UserType = 'P';

            var originalEmployeePmlBrokerId = pUser.PmlBrokerId;

            pUser.PmlBrokerId = pmlBroker.PmlBrokerId;
            pUser.UseOriginatingCompanyBranchId = true;

            var hasChangedOriginatingCompanies = originalEmployeePmlBrokerId != pUser.PmlBrokerId;

            
            // tdsk: fill this in.
            /*this.brokerRelationships.ApplySettingsToEmployee(pUser, hasChangedOriginatingCompanies);
            this.miniCorrespondentRelationships.ApplySettingsToEmployee(pUser, hasChangedOriginatingCompanies);
            this.correspondentRelationships.ApplySettingsToEmployee(pUser, hasChangedOriginatingCompanies);*/

            pUser.PopulatePMLPermissionsT = EmployeePopulationMethodT.IndividualUser;

            pUser.AccessiblePmlNavigationLinksId = new Guid[0];
            //pUser.MustLogInFromTheseIpAddresses = m_ip_parsed.Value;
            pUser.EnabledIpRestriction = false;

            //if (broker.IsPmlDoEnabled)
            //{
            //    // pUser.DOAutoLoginName = string.Empty;
            //}
            //if (broker.IsPmlDuEnabled && broker.UsingLegacyDUCredentials)
            //{
            //    //pUser.DUAutoLoginName = string.Empty;
            //}

            pUser.IsPmlManager = false;

            pUser.PmlLevelAccess = E_PmlLoanLevelAccess.Individual;

            pUser.LoginName = options.UserOptions.UserName;
            pUser.Password = options.UserOptions.Password;

            pUser.AllowLogin = true;
            pUser.IsActive = true;

            pUser.Roles = CEmployeeFields.s_LoanRepRoleId.ToString();

            // don't need quickpricer just yet.
            // pUser.ActivateQuickPricerAccessForThisUser();

            // result.CreatedUserAnonymousQpId = pUser.AnymousQuickPricerAccessId;

            try
            {
                pUser.PasswordExpirationD = SmallDateTime.MaxValue;
                pUser.PasswordExpirationPeriod = 0;
            }
            catch (CBaseException e)
            {
                Tools.LogError(e);
                result.ErrorMessages.Add("Failed to update user's password expirations.");
                return result;
            }

            //pUser.LosIdentifier = LicensesPanel.LosIdentifier;
            //pUser.LicenseInformationList = LicensesPanel.LicenseList; //opm 30840

            // opm 64436
            //pUser.CommissionPointOfLoanAmount = m_losConvert.ToRate(m_CommissionPointOfLoanAmount.Text);
            //pUser.CommissionMinBase = m_losConvert.ToMoney(m_CommissionMinBase.Text);
            //pUser.CommissionPointOfGrossProfit = m_losConvert.ToRate(m_CommissionPointOfGrossProfit.Text);

            pUser.OriginatorCompensationSetLevelT = E_OrigiantorCompenationLevelT.OriginatingCompany;//Convert.ToInt32(m_ddlOrigiantorCompenationLevelT.SelectedValue);
            //pUser.OriginatorCompensationIsOnlyPaidForFirstLienOfCombo = m_OriginatorCompensationIsOnlyPaidForFirstLienOfCombo.Checked;
            //pUser.OriginatorCompensationPercent = m_losConvert.ToRate(m_OriginatorCompensationPercent.Text);
            //pUser.OriginatorCompensationMinAmount = m_losConvert.ToMoney(m_OriginatorCompensationMinAmount.Text);
            //pUser.OriginatorCompensationMaxAmount = m_losConvert.ToMoney(m_OriginatorCompensationMaxAmount.Text);
            //pUser.OriginatorCompensationFixedAmount = m_losConvert.ToMoney(m_OriginatorCompensationFixedAmount.Text);
            //pUser.OriginatorCompensationNotes = m_OriginatorCompensationNotes.Text;
            //pUser.OriginatorCompensationBaseT = (E_PercentBaseT)Convert.ToInt32(m_OriginatorCompensationBaseT.SelectedValue);

            if (broker.IsCustomPricingPolicyFieldEnabled)
            {
                pUser.CustomPricingPolicyFieldValueSource = E_PricingPolicyFieldValueSource.IndividualUser;
                // SetCustomLOPricingPolicyFields(pUser);
            }

            pUser.WhoDoneIt = principal.UserId;
            if (pUser.Save() == false)
            {
                result.ErrorMessages.Add("Failed to save user.");
                return result;
            }

            try
            {
                BrokerUserPermissions buP = new BrokerUserPermissions(pUser.BrokerID, pUser.ID);
                buP.SetPermission(Permission.CanApplyForIneligibleLoanPrograms, false);
                buP.SetPermission(Permission.CanRunPricingEngineWithoutCreditReport, false);
                buP.SetPermission(Permission.CanSubmitWithoutCreditReport, false);
                buP.SetPermission(Permission.CanAccessTotalScorecard, false);
                buP.SetPermission(Permission.AllowOrder4506T, false);

                buP.SetPermission(Permission.AllowCreatingCorrChannelLoans, false);
                buP.SetPermission(Permission.AllowCreatingMiniCorrChannelLoans, false);
                buP.SetPermission(Permission.AllowCreatingWholesaleChannelLoans, false);

                buP.SetPermission(Permission.AllowViewingCorrChannelLoans, false);
                buP.SetPermission(Permission.AllowViewingMiniCorrChannelLoans, false);
                buP.SetPermission(Permission.AllowViewingWholesaleChannelLoans, true);

                buP.Update();
            }
            catch (Exception)
            {
                result.ErrorMessages.Add("Unable to update user's permissions.");
            }

            result.CreatedUserId = pUser.UserID;
            result.CreatedEmployeeId = pUser.ID;

            var questionIds = SecurityQuestion.ListAllSecurityQuestions(false).Take(3).Select(q => q.QuestionId).ToArray();
            BrokerUserDB.SaveSecurityQuestionsAnswers(principal.BrokerId, pUser.UserID, questionIds[0], "asdf", questionIds[1], "fdsa", questionIds[2], "afafa"); // tdsk: do away with these hardcodes.
            return result;
        }

        private static CreatedLoanResult CreateLoan(BrokerUserPrincipal principal, LoanCreationOptions loanOptions)
        {
            var result = new CreatedLoanResult();

            var creator = CLoanFileCreator.GetCreator(principal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
            var loanId = creator.CreateBlankLoanFile();
            creator.CommitFileCreation(false);

            result.CreatedLoanId = loanId;

            var loanData = CPageData.CreatePmlPageDataUsingSmartDependency(loanId, typeof(Preparer));
            loanData.InitSave(ConstAppDavid.SkipVersionCheck);

            var appData = loanData.GetAppData(0);
            appData.aBFirstNm = "TPO";
            appData.aBLastNm = "Test";
            appData.aBSsn = "123-12-1231";
            appData.aBTotIPe_rep = "$10,000.00";

            result.CreatedAppId = appData.aAppId;
            loanData.sEmployeeLoanRepId = loanOptions.TpoEmployeeId;
            
            // make loan a wholesale loan
            loanData.sIsBranchActAsOriginatorForFileTri = E_TriState.No;
            loanData.sIsBranchActAsLenderForFileTri = E_TriState.Yes;

            // set some pricing options
            loanData.sProdIncludeFHATotalProc = true;
            loanData.sProdIncludeUSDARuralProc = true;
            loanData.sProdIncludeNormalProc = true;

            loanData.sProdFilterFinMethFixed = true;
            loanData.sProdFilterFinMeth3YrsArm = true;
            loanData.sProdFilterFinMeth3YrsArm = true;
            loanData.sProdFilterFinMeth5YrsArm = true;
            loanData.sProdFilterFinMeth7YrsArm = true;
            loanData.sProdFilterFinMeth10YrsArm = true;
            loanData.sProdFilterFinMethOther = true;

            loanData.sProdFilterDue15Yrs = true;
            loanData.sProdFilterDue30Yrs = true;

            loanData.sSpAddr = "123 sumvar rd";
            loanData.sSpZip = "90210";
            loanData.sSpState = "CA";
            loanData.sSpCounty = "Los Angeles";
            loanData.sSpCity = "Beverly Hills";

            loanData.sApprVal_rep = "$123,123.00";
            loanData.sHouseValPe_rep = "$100,000.00";
            loanData.Save();

            var createdCondition = CreateCondition(principal, loanOptions, loanId);
            if(!createdCondition)
            {
                result.ErrorMessages.Add("Could not create condition.");
            }
            return result;
        }

        private static bool CreateCondition(BrokerUserPrincipal principal, LoanCreationOptions loanOptions, Guid loanId)
        {
            Task task;
            task = Task.Create(loanId, principal.BrokerId);

            task.SetDueDate(DateTime.Now.AddDays(7));            
            
            task.TaskCreatedByUserId = principal.UserId;

            task.TaskSubject = "TPO Load Test";
            task.TaskOwnerUserId = principal.UserId;
            
            task.TaskAssignedUserId = loanOptions.TpoUserId;
            task.TaskFollowUpDate_rep = string.Empty;
            task.Comments = "Solely for TPO load test.";

            task.MakeCondition();
            var category = ConditionCategory.GetCategories(principal.BrokerId).First();
            task.CondCategoryId = category.Id;

            task.CondRequiredDocTypeId = null;

            task.TaskPermissionLevelId = category.DefaultTaskPermissionLevelId;
            //var permissionLevel = task.GetPermissionLevelFor(principal);
            //if (permissionLevel == E_UserTaskPermissionLevel.Manage)
            //{
            //    task.ResolutionBlockTriggerName = ResolutionBlockTriggerName.Value;
            //    task.ResolutionDateSetterFieldId = ResolutionDateSetterFieldId.Value;
            //}

            if (task.Save(true))
            {
                return true;
            }

            return false;
        }

        private static string GetOnetimeUsage()
        {
            var usage = @"usage: ScheduledExecutable PrepareOneTpoLoadTestUserAndLoan BrokerId UserIdOfAdminUser UserNameOfPUserToCreate PasswordForThatUser";
            return usage;
        }

        private static void WriteOnetimeUsage()
        {
            Console.WriteLine(GetOnetimeUsage());
        }

        private static string GetCsvUsage()
        {
            var usage = @"usage: ScheduledExecutable PrepareTpoLoadTestUsersAndLoansFromCsv csvFileName 
(where csvFileName has columns: 
BrokerId UserIdOfAdminUser UserNameOfPUserToCreate PasswordForThatUser
)";
            return usage;
        }

        private static void WriteCsvUsage()
        {
            Console.WriteLine(GetCsvUsage());
        }
    }
}
