﻿# ScheduleExecutable

## Summary

The executable contains 2 sets of items. One set is scripts/migrations that need to be run after a release/update. The second set is common
scripts that should be run all the time. For example, if we need to migrate brokers and it can only be performed via code then a script would be added
to program.cs. Another example is the Temp Folder Cleanup, this is scheduled to run on all servers every 15 minutes.
