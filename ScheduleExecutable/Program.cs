namespace ScheduleExecutable
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Web.Script.Serialization;
    using System.Xml;
    using System.Xml.Linq;
    using ConfigSystem;
    using ConfigSystem.DataAccess;
    using DataAccess;
    using EDocs;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Common.TextImport;
    using LendersOffice.ConfigSystem;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.HttpModule;
    using LendersOffice.LOAdmin.Manage;
    using LendersOffice.Migration;
    using LendersOffice.ObjLib.DatabaseMessageQueue;
    using LendersOffice.ObjLib.Disclosure;
    using LendersOffice.ObjLib.FieldInfoCache;
    using LendersOffice.ObjLib.LoanFileCache;
    using LendersOffice.ObjLib.MortgagePool;
    using LendersOffice.ObjLib.PriceGroups;
    using LendersOffice.ObjLib.Security;
    using LendersOffice.ObjLib.SequentialNumbering;
    using LendersOffice.ObjLib.Task;
    using LendersOffice.Reports;
    using LendersOffice.Security;
    using LendersOffice.XsltExportReport;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;
    using Pricing;
    using ScheduleExecutable.Integrations.Cenlar;
    using ScheduleExecutable.Migrations;
    using ScheduleExecutable.Nightly;
    using Monitor;
    using System.Messaging;

    class Program
    {
        private static Dictionary<string, Action<string[]>> MethodsTable = new Dictionary<string, Action<string[]>>(StringComparer.OrdinalIgnoreCase)
        {
            // Permanent Tasks
            { "SplitFile", Program.SplitFile}
            , { "TaskNightly", Program.TaskNightly }
            , { "TaskTriggerNightly", Program.TaskTriggerNightly}
            , { "EDocsPNGCleanup", Program.EDocsPNGCleanup }
            , { "CredcoCleanup", ScheduleExecutable.Cleanup.CredcoCleanup.Execute}
            , { "ExpiredCacheCleanup", ScheduleExecutable.Cleanup.ExpiredCacheCleanup.Execute}
            , { "CleanupExpireLpeRequestAndResult", Program.CleanupExpireLpeRequestAndResult} // Use to run in same process as MoveAudiTrailToFileDB
            , { "MoveAuditTrailToFileDB", Program.MoveAuditTrailToFileDB}
            , { "WebPing", WebPing.Execute}
            , { "CheckExpiredPricing", CheckExpiredPricing.Check}
            , { "UpdateLON", LendersOffice.Lon.UpdateLONMainClass.Execute}
            , { "DisclosureNightly", Program.DisclosureNightly}
            , { "DisclosureNightly_ProcessLoansForEConsentExpiration", Program.DisclosureNightly_ProcessLoansForEConsentExpiration}
            , { "LockPolicyObservedHolidayClosure", LockPolicyObservedHolidayClosure.RunNightlyProcess }
            , { "LockPolicyObservedHolidayClosure_RunUpdateForDates", LockPolicyObservedHolidayClosure.RunUpdateForDates }
            , { "LockPolicyObservedHolidayClosure_MigrateOldHolidayDates", LockPolicyObservedHolidayClosure.MigrateOldHolidayDates }
            , { "TestXsltExport", TestXsltExport.Execute}
            , { "SequentialNumberingNightly", Program.SequentialNumberingNightly}
            , { "CheckFirstTechLoanReserveList", Nightly.CheckFirstTechLoanReserveList.Execute}
            , { "AutoEmailXsltExport", AutoEmailXsltExport.Execute}
            , { "TemplateEmailXsltExport", TemplateEmailXsltExport.Execute}
            , { "ExecuteMemberNotificationForPML0223", TemplateEmailXsltExport.ExecuteMemberNotificationForPML0223}
            , { "DownloadFFIECYieldTable", DownloadFFIECYieldTable.Execute}
            , { "SFTPXsltExport", ScheduleExecutable.SFTPXsltExport.SFTPXsltExport.Execute}
            , { "ScheduleXsltExport", ScheduleExecutable.ScheduleXsltExport.Execute}
            , { "ExportLpeSnapshot", ExportLpeSnapshot.Execute}
            , { "CreateFileSnapshot", ExportLpeSnapshot.CreateSnapshot}
            , { "LoanCreationStressTestClass", LoanCreationStressTestClass.Execute}
            , { "BatchLoanProgramRename", ScheduleExecutable.SAETools.BatchLoanProgramRename.Execute}
            , { "LoanInspection", LoanInspection.Execute}
            , { "ScheduleCustomReport", LendersOffice.QueryProcessor.ScheduleCustomReport.Execute}
            , { "TaskMigration", Migrations.TaskMigration.Execute} // Perform task migration from old version to new task system. This will be need until ALL of our user move to new task system.
            , { "QueuePdfConverter", EDocs.Runnable.QueuePdfConverter.Execute} // 5/28/2015 dd - Run QueuePdfConverter as Executable
            , { "PdfRasterizerProcessor", LendersOffice.ObjLib.Edocs.Runnables.PdfRasterizerProcessor.Execute} // 6/23/2015 dd - Run Pdf Rasterizer for NonDestructive eDocs.
            , { "EDocsCopyQueueProcessor", EDocs.Runnable.EDocsCopyQueueProcessor.Execute} // 7/29/2015 - dd - Run EDocsCopyQueueProcessor to test.
            , { "EDocsMigration", EDocsMigration.ExecuteMigration} // 8/4/2015 - dd - Convert legacy edocs to non destructive edocs.
            , { "LpeEmailDownload", LpeUpdateDownloadProcess.EmailDownloadProcess.Execute } // 01/21/2016 - dd - Download investor emails in pmlrates inbox.
            , { "ExportCustomReportsSchema", Program.ExportCustomReportsSchema }
            , { "SessionCleanup", ScheduleExecutable.Cleanup.SessionCleanup.Execute}
            , { "AddOperationToSystemConfig", SystemConfigOperationMigrator.AddOperationToSystemConfig }
            , { "ExportAllLoansAndEdocs", ExportAllLoansAndEdocsMigrator.ExportAllLoansAndEdocs }
            , { "DeleteAllContacts", DeleteAllContactsMigrator.DeleteAllContacts }
            , { "RatePriceMoveFromLocalToPersistent" , LendersOfficeApp.los.RatePrice.Utils.MoveFromLocalToPersistent} // 12/20/2016 - dd - Move rate options from LpeUpdate to persistent storage.
            , { nameof(CreateFieldInfoCache), Program.CreateFieldInfoCache }
            , { "AutoPriceEligibilityProcess", AutoPriceEligibilityProcess.Execute }
            , {"SyncRatesheetTables", SyncRatesheetTables.Execute } // 8/24/2017 - dd - Sync Ratesheets table on production to other environment.
            , { "HealthCheck", HealthCheck.Execute }
            , { "RecompileByteCode", RecompileByteCode.Run }
            , { "LpeReleaseSnapshot", LpeReleaseSnapshot.RelaseSnapshot}
            , { "GenerateDailyIISReport", LendersOffice.ElasticSearch.GenerateDailyIISReport.Execute}

            , { "SyncPricingSnapshot", SyncPricingSnapshot.Execute } // 2/28/2018 - dd - Export pricing snapshot to other environment. 
            , { "SnapshotSplit", FilebasedSnapshotSplit.Execute } // 9/24/2018 - Split filebased snapshot by lenders.
            , { "SubsnapshotVerify", SubsnapshotVerify.Execute } // 9/26/2018 - Subsnapshot verify

            // Recurring Tasks
            , { "UploadPdfToAmazon", UploadPdfToAmazon.Execute } // 5/14/2016 - dd - Upload PDF to Amazon.
            , { "PrepareOneTpoLoadTestUserAndLoan", TpoLoadTest.Preparer.ExecuteOnce } // 1/12/2018 - sk - One time executable for creating tpo load test P users and loans for opm 464739.
            , { "PrepareTpoLoadTestUsersAndLoansFromCsv", TpoLoadTest.Preparer.ExecuteFromCsv } // 1/12/2018 - sk - One time executable for creating tpo load test P users and loans for opm 464739.
            , { "RunSmallDataLayerMigrationOnLoans", SmallDataMigrationBatchRunner.RunSmallDataLayerMigrationOnLoans }
            , { "EnqueueEdocsMigration", Program.EnqueueEdocsMigration} // 9/8/2015 - dd - Query a list of old edocs and put in queue for convert to non destructive edocs.
            , { "MigrateTaskFromProcessorToUnderwriter", Program.MigrateTaskFromProcessorToUnderwriter }
            , { "MigrationLoanFileCache", Program.MigrationLoanFileCache}
            , { "AllBrokersDisableTPOServices", Program.AllBrokersDisableTPOServices}
            , { "MaskBrokerLoans", Program.MaskBrokerLoans}
            , { "RemoveOrphanedOriginatorCompBeneficiaryId", RemoveOrphanedOriginatorCompBeneficiaryId.RemoveOrphanedOriginatorCompBeneficiaryIdMigrator }
            , { "OrigPortalAntiSteeringDisclosureAccessMigrator", OrigPortalAntiSteeringDisclosureAccessMigrator.OrigPortalAntiSteeringDisclosureAccessRunner }
            , { "DocumentImageFix", Program.DocumentImageFix }
            , { "MigratesSpState", Program.Migratesspstatepe }
            , { "MonitorQueues", Program.MonitorQueues }
            , { "ExportBrokerEdocs", Program.ExportBrokerEdocs}
            , { "UpdateAllowCreateWholesaleLoanPermission", Program.UpdateAllowCreateWholesaleLoanPermission}
            , { "MigrateSettlementCharges", Program.MigrateSettlementCharges }
            , { "AddToPredefinedAdjustmentTable", PredefinedAdjustmentsMigrator.AddToPredefinedAdjustmentTable }
            // MigrateCCArchiveLoanDataToFileDB is meant to be run on 11/6, and it should be fine to remove the method
            // after it is run. The migration code is in the MigrateCCArchiveLoanDataToFileDB.cs file. If you are going
            // to reuse the migration code you may need to update the query. I don't think there will be issues if the
            // migration runs multiple times for a single file, but you should verify this independently before doing so.
            , { "MigrateCCArchiveLoanDataToFileDB", Program.MigrateCCArchiveLoanDataToFileDB }
            , { "ExportLicenseInfoForBrokerAndType", Program.ExportLicenseInfo }
            , { "UpdateLicenseInfoForLoanOfficer", Program.UpdateLicenseInfo }
            , { "UploadMMIBranches", Program.UploadMMIBranches }
            , { "RunEDocNotification", Program.RunEDocNotification }
            , { "UploadMMIPools", Program.UploadMMIPools }
            , { "BatchDisableTaskNotificationsForPmlUsers", Program.BatchDisableTaskNotificationsForPmlUsers }
            , { "TrackAllLoans", QueueUpAllLoans }
            , { "BatchCloseTasksUsingInputList", BatchTaskCloserUsingInputList.BatchCloseTasksUsingInputList }
            , { "AddActionMessagesToLoanUpdaterQueue", Program.AddActionMessagesToLoanUpdaterQueue }
            , { nameof(Program.RunXsltExportProcessor), Program.RunXsltExportProcessor }
            , { nameof(Program.RunSingleAsyncBatchExport), Program.RunSingleAsyncBatchExport}
            , { "RunPricing", Pricing.StandaloneRunner.Execute }
            , { nameof(RunAutomatedCenlarTransmissions), RunAutomatedCenlarTransmissions.Execute }
            , { nameof(AuditCenlarApiKeys), AuditCenlarApiKeys.Execute }
            , { nameof(PopulateLPProductIdentifierMigrator), PopulateLPProductIdentifierMigrator.Run }

            // Temporary operations
            , { "BatchEnableOcPricingEnginePermissionsForPml0176", Program.BatchEnableOcPricingEnginePermissionsForPml0176 }
            , { "OPM243181_GenerateNewLoanNumbers", Program.OPM243181_GenerateNewLoanNumbers }
            , { "MigrateCreateLeadsPermission", MigrateCreateLeadsPermission }
            , { "Opm338196_ReactivateTemplateTasks", Opm338196_ReactivateTemplateTasks.ReactivateTemplateTasks }
            , { "EnableMfaOptInBit_Opm266053", EnableMfaOptInBit_Opm266053.EnableMfaOptInBit }
            , { "PreserveMfaBitSettings_Opm266053", PreserveMfaBitSettings_Opm266053.PreserveMfaBitSettings }
            , { "AddNewMFATempOption", AddNewMFATempOptionMigrator.AddNewMFATempOption }
            , { "DisableUserLevelMfaBit", DisableUserLevelMfaBit.DisableUserLevelMfaBit_Opm311263 }
            , { nameof(SetBitToEnableComplianceEase_Opm261777), SetBitToEnableComplianceEase_Opm261777.RunMigration }
            , { "SetCellPhoneMfaBit", SetCellPhoneMfaBitMigrator.SetCellPhoneMfaBit }
            , { nameof(SetInitialDisclosureCalculationBits_Opm242901), SetInitialDisclosureCalculationBits_Opm242901.RunMigration }
            , { nameof(OPM239654_Broker_MIPolicyDocTypeAutosave), OPM239654_Broker_MIPolicyDocTypeAutosave.MigrateMIDocTypes}
            , { "Opm328860_BatchCloseTasksForPml0233", Opm328860_BatchCloseTasksForPml0233.CloseTasks }
            , { "MigratePricePolicyRateOptions", MigratePricePolicyRateOptions.Execute } // dd - Remove after 01/01/2017.
            , { "AddIdsToAdjustmentsAndProrations", AddIdsToAdjustmentsAndProrationsMigrator.AddIdsToAdjustmentsAndProrations }
            , { nameof(Opm281099_PopulateFileDbEncryptionKeys), Opm281099_PopulateFileDbEncryptionKeys.Execute }
            , { nameof(SetBrokerTempOptionWithOcGroupExclusionMigrator), SetBrokerTempOptionWithOcGroupExclusionMigrator.Run }
            , { "OPM235716_iServeSettlementMigration", OPM235716_iServeSettlementMigrator.OPM235716_iServeSettlementMigration }
            , { "CloseTasksForPML0208_Opm338196", CloseTasksForPML0208_Opm338196_Migrator.CloseTasksForPML0208_Opm338196 }
            , { "PopulateLeadDates", PopulateLeadDatesMigrator.PopulateLeadDates }
            , { "PopulatePricegroupRev", PricegroupRevMigration.PopulatePricegroupRev }
            , { "EnableBananaSecurity", EnableBananaSecurityTempOptionMigrator.EnableBananaSecurity }
            , { "DisableClosingDisclosureInitialCalculationForNflp", DisableClosingDisclosureInitialCalculationForNflp.RunMigration }
            , { "AddIdsToSSPsAndCOs", AddIdsToSSPsAndCOsMigrator.AddIdsToSSPsAndCOs }
            , { nameof(PopulateCountyForRolodexContacts), PopulateCountyForRolodexContacts.RunMigration }
            , { "DisclosureEventWorkflowMigrator", DisclosureEventWorkflowMigrator.Run }
            , { "PopulateDeviceRegistrationBrokerBit", PopulateDeviceRegistrationBrokerBitMigrator.PopulateDeviceRegistrationBrokerBit }
            , { "UncheckaIntrvwrMethodTLckdForTemplates", UncheckaIntrvwrMethodTLckdForTemplatesMigrator.UncheckaIntrvwrMethodTLckdForTemplates }
            , { nameof(SetStatusesIgnoredByComplianceEase), SetStatusesIgnoredByComplianceEase.RunMigration }
            , { nameof(Program.ListTaskInLoans), Program.ListTaskInLoans }
            , { "InvestorHmdaPurchaserT", HmdaPurchaser2015TMigrator.RunMigration }
            , { nameof(LockTotalScorecardSecondaryFinancingType), LockTotalScorecardSecondaryFinancingType.RunMigration }
            , { "AddUsingLegacyDUCredentialsTempOption", UsingLegacyDUCredentialsTempOptionMigrator.AddUsingLegacyDUCredentialsTempOption }
            , { "Opm457137_CleanUpFeeServiceHistory", Opm457137_CleanUpFeeServiceHistory.Run }
            , { nameof(Opm461275_ClearUnusedEdocClassification), Program.Opm461275_ClearUnusedEdocClassification }
            , { nameof(OPM461391_DocVendorPlatforms), OPM461391_DocVendorPlatforms.Run }
            , { "ClearCreditInfo", ClearCreditInfo }
            , { "QuickPricer2LoansCleanupProcessAbandoned", QuickPricer2LoansCleanupProcessAbandoned}
            , { "ImportGSEFile", ImportGSEFile }
            , { "Opm464845_ReplaceOldLoanStatusChangeRules_system", Opm464845_ReplaceOldLoanStatusChangeRules.MigrateSystemConfig }
            , { "Opm464845_ReplaceOldLoanStatusChangeRules_broker", Opm464845_ReplaceOldLoanStatusChangeRules.MigrateBrokerConfigs }
            , { "DisableTpoLoanEditorSaveButton",  DisableTpoLoanEditorSaveButtonMigrator.DisableTpoLoanEditorSaveButton }
            , { "Opm467084_CheckWorkflowForSsn", Opm467084_CheckWorkflowForSsn.Run }
            , { "DumpaAssetCollection", DumpaAssetCollection.DumpaAssetCollectionData }
            , { "MigrateMIApplicationType", MIApplicationTypeMigrator.MigrateMIApplicationType }
            , { "CustomFavoriteFolderMigration", CustomFavoriteFolderMigration.RunCustomFavoriteFolderMigration }
            , { "RunTouchesMigrator", TouchesMigrator.RunTouchesMigrator }
            , { "MigrationHistoryAudit", LendersOffice.Audit.AuditManager.MigrationHistoryAudit}
            , { "RunPml3Migrator", Pml3Migrator.RunPml3Migrator }
            , { "Opm468394_AddLeadsToRetailPortal", Opm468394_AddLeadsToRetailPortal.Run }
            , { "GiveAEAndAnyPermissionsToExistingOPSystemPages", GiveAEAndAnyPermissionsToExistingOPSystemPages.Run }
            , { "AddTrid2017PrincipalReductionAdjustmentsToBrokers", PredefinedAdjustmentsMigrator.AddTrid2017PrincipalReductionAdjustmentsToBrokers }
            , { "Opm469661_InitializeWorkflowConfigCurrentTable", Opm469661_InitializeWorkflowConfigCurrentTable.Run }
            , { "OPM470280_AddDisableTrid20ForNonTestLoansTempOption", OPM470280_AddDisableTrid20ForNonTestLoansTempOption.Run }
            , { nameof(Opm461139_EnableAllVendorVoaOptionsForLenderServices), Opm461139_EnableAllVendorVoaOptionsForLenderServices.Run }
            , { "EnqueueDBMessageQueueStats", MessageQueueMonitor.EnqueueDBMessageQueueStats }
            , { "Opm242351_MigrateLoanProgramPaymentOptions", Opm242351_MigrateLoanProgramPaymentOptions.Run }
            , { "RunLoanFileCacheUpdater", RunLoanFileCacheUpdater }
            , { "QAllLoansForCacheUpdate", QAllLoansForCacheUpdate}
            , { nameof(DuplicateUserCredentialsBetweenDocVendors), DuplicateUserCredentialsBetweenDocVendors.Run }
            , { nameof(Opm476112_AddRestraintToBlockLockingLeads), Opm476112_AddRestraintToBlockLockingLeads.Run }
            , { nameof(PopulateInvestorProgramIdForCcu), PopulateInvestorProgramIdForCcu.RunMigration }
            , { nameof(FindMissingAusRunsForClient), FindMissingAusRunsForClient.Run }
        };


        private static string arg2 = null;
        private static string arg3 = null;
        private static string[] m_args;


        private static void CreateFieldInfoCache(string[] args)
        {
            var manager = new FieldInfoCacheManager();
            manager.CreateCacheResource();
        }

        /// <summary>
        /// Add a way to run the async Batch Export from Continuous. 
        /// </summary>
        /// <param name="args">No arguments required.</param>
        private static void RunXsltExportProcessor(string[] args)
        {
            XsltExportProcessor processor = new XsltExportProcessor();
            processor.Run();
        }

        public static void QAllLoansForCacheUpdate(string[] args)
        {
            Console.WriteLine("This script will enqueue all valid loan files for cache migration of fields:");
            Console.WriteLine(ConstStage.LoanUpdaterFieldsForMigration);
            Console.WriteLine(ConstStage.LoanFileCacheQueueConnectionString);
            Console.WriteLine("Please update the stage constant 20 mintues prior to running this. Please enter 'yes' to continue.");

            if (!Console.ReadLine().Equals("yes", StringComparison.OrdinalIgnoreCase))
            {
                Console.WriteLine("Quitting.");
                return;
            }

            Console.WriteLine("Gathering all active loans. May take a few minutes.");
            string sql = "SELECT c.sLId, c.sBrokerId FROM LOAN_FILE_CACHE c WITH(NOLOCK) JOIN BROKER b WITH(NOLOCK) ON c.sBrokerId=b.BrokerId  WHERE b.Status= 1 and c.isvalid = 1";

            LinkedList<Tuple<Guid, Guid>> loans = new LinkedList<Tuple<Guid, Guid>>();


            foreach (var connection in DbConnectionInfo.ListAll())
            {
                Console.WriteLine($"Gathering loans from database {connection.Code}");
                DBSelectUtility.ProcessDBData(connection, sql, TimeoutInSeconds.Create(300), null, r =>
                {
                    while (r.Read())
                    {
                        Guid brokerid = (Guid)r["sBrokerId"];
                        Guid loanid = (Guid)r["sLId"];
                        loans.AddLast(Tuple.Create(brokerid, loanid));
                    }
                });
            }

            Console.WriteLine($"Gathered {loans.Count} loans. Begining to enqueue.");

            int count = 0;

            foreach (var loan in loans)
            {
                XDocument msg = new XDocument(
                        new XElement("l",
                           new XAttribute("s", loan.Item2.ToString()),
                           new XAttribute("b", loan.Item1.ToString())));

                while (true)
                {
                    try
                    {
                        LendersOffice.Drivers.Gateways.MessageQueueHelper.SendXML(ConstStage.LoanFileCacheQueueConnectionString, null, msg);
                        break;
                    }
                    catch (MessageQueueException ee)
                    {
                        if (ee.ErrorCode == unchecked((int)0x80004005))
                        {
                            Console.WriteLine("Ran into an error queuing msgs. Going to sleep for 1 min");
                            Thread.Sleep(60000);
                        }
                        else
                        {
                            throw;
                        }
                    }
                }

                count++;

                if (count % 50000 == 0)
                {
                    Console.WriteLine($"Sent {count} loans to queue.");
                }

            }
            Console.WriteLine($"Sent a total of {count} loans to queue. Complete");
        }

        public static void RunLoanFileCacheUpdater(string[] args)
        {
            if (args.Length <= 2)
            {
                Console.WriteLine("Error. Invalid arguments. Need loanlist.txt and field.txt");
                return;
            }

            string[] loanIdsFromFile = File.ReadAllLines(args[1]);
            string[] fields = File.ReadAllLines(args[2]);

            int i = 1;
            Console.WriteLine($"Found {loanIdsFromFile.Length} lines in the file.");

            foreach(var loan in loanIdsFromFile)
            {
                if (string.IsNullOrEmpty(loan))
                {
                    continue;
                }

                Guid loanId; 

                if (Guid.TryParse(loan, out loanId))
                {
                    Tools.UpdateCacheTable(loanId, fields);
                }
                i++;
                Console.Clear();
                Console.WriteLine($"Converted {i} out of {loanIdsFromFile.Length}.");
            }
        }


        public static void ImportGSEFile(string[] args)
        {
            if (args.Length <= 1)
            {
                Console.WriteLine("Please specify loan limit file. Requires a heading row.");
                return;
            }
            var dataTable = LendersOffice.ObjLib.ExcelHelpers.ClosedXMLHelper.GenerateDataTable(args[1], 1);

            var limits = new LinkedList<LendersOfficeApp.ObjLib.LoanLimitsData.LoanLimitEntry>();

            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                DataRow row = dataTable.Rows[i];

                var entry = new LendersOfficeApp.ObjLib.LoanLimitsData.LoanLimitEntry();
                entry.CountyCode = row["CountyCode"].ToString();
                entry.State = row["State"].ToString();
                entry.Limit1Units = row["LoanLimit1"].ToString();
                entry.Limit2Units = row["LoanLimit2"].ToString();
                entry.Limit3Units = row["LoanLimit3"].ToString();
                entry.Limit4Units = row["LoanLimit4"].ToString();
                entry.SoaCode = row["SoaCode"].ToString();
                entry.LastRevised = row["LastRevised"].ToString();
                limits.AddLast(entry);
                if (entry.SoaCode != "GSE")
                {
                    throw new ArgumentException("Error wrong type.");
                }
            }

            Console.WriteLine(limits.Count);
            var updater = new LendersOfficeApp.ObjLib.LoanLimitsData.GSELoanLimitDatabaseUpdater(new DateTime(2019, 1, 1));
            updater.Run(limits);
        }

        /// <summary>
        /// Turns a list of loans into task ids. 
        /// </summary>
        /// <param name="args"></param>
        private static void ListTaskInLoans(string[] args)
        {
            if (args.Length < 3)
            {
                Console.WriteLine("Broker Id Is Required and so is the filename with list of loans.");
                return;
            }

            Guid brokerId;
            if (!Guid.TryParse(args[1], out brokerId))
            {
                Console.WriteLine("Broker id not parsed.");
                return;
            }


            if (!File.Exists(args[2]))
            {
                Console.WriteLine($"File not found {args[2]}.");
                return;
            }

            string[] lines = File.ReadAllLines(args[2]);

            BrokerDB db = BrokerDB.RetrieveById(brokerId);

            Console.Write($"Looking up loans for {db.Name} - {db.CustomerCode}. Continue? ");

            string answer = Console.ReadLine();
            if (answer != "Yes")
            {
                Console.WriteLine("Exiting.");
                return;
            }

            string taskFile = $"ListTaskInLoans_TaskIds_{db.CustomerCode}.txt";
            string taskError = $"ListTaskInLoans_Error_{db.CustomerCode}.txt";

            if (File.Exists(taskFile))
            {
                Console.WriteLine($"deleting file {taskFile}.");
                File.Delete(taskFile);
            }

            if (File.Exists(taskError))
            {
                Console.WriteLine($"deleting file {taskError}.");
                File.Delete(taskError);
            }

            foreach (string sLNm in lines)
            {
                Guid sLId = Tools.GetLoanIdByLoanName(db.BrokerID, sLNm);

                if (sLId == Guid.Empty)
                {
                    File.AppendAllText(taskError, $"{sLNm}{Environment.NewLine}");
                    continue;
                }

                var lqbTasks = Task.GetTasksInLoan(db.BrokerID, sLId);

                foreach (var lqbTask in lqbTasks)
                {
                    if (lqbTask.TaskStatus != E_TaskStatus.Resolved)
                    {
                        File.AppendAllText(taskFile, $"{lqbTask.TaskId},{sLNm},{sLId}{Environment.NewLine}");
                    }
                }
            }
        }


        private static void QuickPricer2LoansCleanupProcessAbandoned(string[] args)
        {
            if (args.Length < 2)
            {
                Console.WriteLine("Error. Expect ScheduleExecutable.exe QuickPricer2LoansCleanupProcessAbandoned 1/1/2018 brokerpath.txt");
                return;
            }
            string[] lines = File.ReadAllLines(args[2]);
            DateTime dt = DateTime.Parse(args[1]);

            var p = new LendersOffice.ObjLib.QuickPricer.QuickPricer2LoansCleanupProcess();
            foreach (var line in lines)
            {
                Guid brokerId = Guid.Parse(line);
                {
                    Console.WriteLine($"Running broker {brokerId}. Deleting loans older than {dt.ToShortDateString()}");
                    p.CleanupAbandonedLoans(brokerId, dt);
                }
            }
        }


        public static void ClearCreditInfo(string[] args)
        {
            Console.WriteLine("ClearCreditInfo Start...");

            foreach( var item in DbConnectionInfo.ListAll())
            {
                CreateCreditInfoOnDb(item);
            }
        }

        public static void CreateCreditInfoOnDb(DbConnectionInfo src)
        {
            Console.WriteLine($"Loading affected appraisal orders on {src.InitialCatalog}");

            List<Guid> records = new List<Guid>();
            string gatherSql = "SELECT AppraisalOrderId FROM APPRAISAL_ORDER_INFO where AppraisalOrderXmlContent like '%ccinfo%'";
         
            DBSelectUtility.ProcessDBData(src, gatherSql, TimeoutInSeconds.Sixty, null, p => {
                while (p.Read())
                {
                    records.Add((Guid)p["AppraisalOrderId"]);
                }
            });

            Console.WriteLine($"Loaded {records.Count} from {src.InitialCatalog}.");
            int i = 0; 
            foreach(var appraisalRecordId in records)
            {
                i++;

                UpdateAppraisalRecord(src, appraisalRecordId);
                Console.WriteLine($"Running {i}/{records.Count}");
            }
        }

        private static void UpdateAppraisalRecord(DbConnectionInfo src, Guid appraisalRecordId)
        {
            string orderSql = "select AppraisalOrderId, AppraisalOrderXmlContent FROM APPRAISAL_ORDER_INFO where AppraisalOrderId = @orderId";
            string updateSql = "update top(1) APPRAISAL_ORDER_INFO SET AppraisalOrderXmlContent = @xmlContent WHERE AppraisalOrderId = @orderId";

            string xml = null;
            DBSelectUtility.ProcessDBData(src, orderSql, TimeoutInSeconds.Sixty, new SqlParameter[] { new SqlParameter("@orderId", appraisalRecordId) }, p => {
                if (!p.Read())
                {
                    throw new ArgumentException($"Could not load order {appraisalRecordId}");
                }

                Guid id = (Guid)p["AppraisalOrderId"];

                if (id != appraisalRecordId)
                {
                    throw new ArgumentException($"Sanity Check Failure {id} {appraisalRecordId}.");
                }

                xml = (string)p["AppraisalOrderXmlContent"];
            });

            if (string.IsNullOrEmpty(xml))
            {
                Console.WriteLine($"Could not load order {appraisalRecordId}.");
                return;
            }

            // For some reason xmlDocument chokes on the xml pre declaration. 
            var xmlStartIndex = xml.IndexOf("<AppraisalOrder");

            if (xmlStartIndex < 0)
            {
                Console.WriteLine($"Could not find appraisalOrder tag {appraisalRecordId}.");
                return;
            }

            xml = xml.Substring(xmlStartIndex);

            var doc = new System.Xml.XmlDocument();
            try
            {
                doc.LoadXml(xml);
            }
            catch (XmlException e)
            {
                Console.WriteLine($"Error {appraisalRecordId}  {e}");
                return;
            }

            XmlNode badNode = doc.SelectSingleNode("//CCInfo");

            if (badNode == null)
            {
                Console.WriteLine($"Could not find node to delete for order {appraisalRecordId}.");
                return;
            }

            badNode.ParentNode.RemoveChild(badNode);


            using (var sw = new StringWriter8())
            {
                using (var w = XmlWriter.Create(sw))
                {
                    doc.WriteTo(w);
                    w.Flush();
                }
                xml = sw.GetStringBuilder().ToString();
            }

            var count = DBUpdateUtility.Update(src, updateSql, null, new SqlParameter[] {
                    new SqlParameter("@xmlContent", xml),
                    new SqlParameter("@orderId", appraisalRecordId)
                });

            if (count.Value != 1)
            {
                throw new ArgumentException($"failed to update order {appraisalRecordId} got back count {count.Value}");
            }
        }
        

        /// <summary>
        /// Deletes loan cache update requests that are no longer associated with
        /// a loan id -- this can occur when a loan is deleted from the database.
        /// This will not affect records associated with invalid (ie !loan.IsValid)
        /// loan files.
        /// This should be scheduled at a regular interval. Do not delete.
        /// </summary>
        /// <param name="args">The command line arguments.</param>
        public static void DeleteInvalidCacheRequests(string[] args)
        {
            int total = LoanCacheUpdateRequest.DeleteInvalidRequests();
            Tools.LogInfo($"Deleted {total} invalid records.");
        }

        /// <summary>
        /// Reruns all failed cache update requests. The class used here can be
        /// invoked through continuous service. I think it may be helpful to 
        /// perform the first few runs through ScheduleExecutable since there 
        /// will be a much bigger backlog. gf opm 449969.
        /// </summary>
        public static void RerunAllCacheUpdateRequests(string[] args)
        {
            var updater = new FailedCacheUpdateRunner();
            updater.Run();
        }

        /// <summary>
        /// Loads up a single uncompleted request and runs it immediately bypassing the queue.
        /// </summary>
        /// <param name="args">The BrokerId and report id are needed.</param>
        private static void RunSingleAsyncBatchExport(string[] args)
        {
            if (args.Length != 4)
            {
                Console.WriteLine("Please pass in the brokerid userid, and batch export id.");
                return;
            }

            Guid brokerId;
            Guid userId; 

            if (!Guid.TryParse(args[1], out brokerId))
            {
                Console.WriteLine("Broker id not parsed.");
                return;
            }
            if (!Guid.TryParse(args[2], out userId))
            {
                Console.WriteLine("Could not parse userId");
                return;
            }


            XsltExportResultStatusItem item = XsltExportResultStatus.GetResult(brokerId, userId, args[3]);
            if (item == null || item.Status == E_XsltExportResultStatusT.Complete)
            {
                Console.WriteLine("Item is null or completed.");
                return;
            }

            XsltExportRequest req = XsltExportRequest.Parse(item.RequestXml);
            XsltExportWorker worker = new XsltExportWorker(req);
            worker.Execute();
        } 

        /// <summary>
        /// Adds messages to the LoanUpdater queue.
        /// </summary>
        /// <param name="args">
        /// Needs 2 arguments:
        ///     LoanUpdaterAction => The action the LoanUpdater will perform when it receives the message. See <see cref="LoanUpdaterAction"/> for values.
        ///     FilePath => The file that has all the info needed to make the message. It's up to you to define a format and parse this file for the values.
        /// </param>
        private static void AddActionMessagesToLoanUpdaterQueue(string[] args)
        {
            if (args.Length != 3)
            {
                Console.WriteLine("Usage: ScheduleExecutable QueueLoansForLOLicenseUpdate [LoanUpdaterAction] [FilePath]");
                return;
            }

            LoanUpdaterAction action;
            if (!Enum.TryParse<LoanUpdaterAction>(args[1], out action))
            {
                Console.WriteLine("Invalid action specified.");
                return;
            }

            switch (action)
            {
                case LoanUpdaterAction.UpdateLicenseInfoForLoanOfficer:
                    {
                        IEnumerable<Guid> loanIds = TextFileHelper.ReadLines(args[2]).Select((stringId) => Guid.Parse(stringId));

                        Console.WriteLine($"Beginning action: UpdateLicenseInfoForLoanOfficer. Updating {loanIds.Count()} loans.");
                        LoanUpdater.QueueLoansForLOLicenseUpdate(loanIds, null);
                        Console.WriteLine($"Finished performing action: UpdateLicenseInfoForLoanOfficer");
                    }
                    break;
                default:
                    throw new UnhandledEnumException(action);
            }
        }

        private static void Opm461275_ClearUnusedEdocClassification(string[] args)
        {
            const string SelectSql = @"
select CustomerCode, DocTypeName, ClassificationId,
CASE ClassificationId
WHEN 0 THEN 'None'
WHEN 1 THEN '_203KHomeownerAcknowledgement'
WHEN 2 THEN '_203KInitialDrawRequest'
WHEN 3 THEN '_203KMaximumMortgageWorksheet'
WHEN 4 THEN '_203KRehabilitationAgreement'
WHEN 5 THEN 'AbstractNoticeAgreement'
WHEN 6 THEN 'AcknowledgementOfNoticeOfRightToCancel'
WHEN 7 THEN 'AffiliatedBusinessArrangementDisclosure'
WHEN 8 THEN 'AirportNoisePollutionAgreement'
WHEN 9 THEN 'AmendatoryClause'
WHEN 10 THEN 'AmortizationSchedule'
WHEN 11 THEN 'AppraisalRecertificationForm'
WHEN 12 THEN 'AppraisalReport'
WHEN 13 THEN 'ApprovalLetter'
WHEN 14 THEN 'ArticlesOfIncorporation'
WHEN 15 THEN 'AssignmentOfMortgage'
WHEN 16 THEN 'AssignmentOfRents'
WHEN 17 THEN 'AssignmentOfTrade'
WHEN 18 THEN 'AssumptionAgreement'
WHEN 19 THEN 'AssuranceOfCompletion'
WHEN 20 THEN 'AttorneyInFactAffidavit'
WHEN 21 THEN 'AutomatedClearingHouseDebitForm'
WHEN 22 THEN 'AutomatedUnderwritingFeedback'
WHEN 23 THEN 'AutomatedValueModelFeedback'
WHEN 24 THEN 'BaileeLetter'
WHEN 25 THEN 'BalloonRefinanceDisclosure'
WHEN 26 THEN 'BankDepositSlip'
WHEN 27 THEN 'BankStatement'
WHEN 28 THEN 'BankruptcyDischargeNotice'
WHEN 29 THEN 'Bid'
WHEN 30 THEN 'BirthCertificate'
WHEN 31 THEN 'BondCertificate'
WHEN 32 THEN 'BorrowerAcknowledgmentOfPropertyCondition'
WHEN 33 THEN 'BorrowerCorrespondence'
WHEN 34 THEN 'BorrowersCertification'
WHEN 35 THEN 'BrokerDisclosureStatement'
WHEN 36 THEN 'BrokerPriceOpinion'
WHEN 37 THEN 'BuildersCertification'
WHEN 38 THEN 'BuildingPermit'
WHEN 39 THEN 'BusinessLicense'
WHEN 40 THEN 'BuydownAgreement'
WHEN 41 THEN 'BuyingYourHomeSettlementCostsAndHelpfulInformation'
WHEN 42 THEN 'CABOCertification'
WHEN 43 THEN 'CancellationofListing'
WHEN 44 THEN 'Check'
WHEN 45 THEN 'Checklist'
WHEN 46 THEN 'ChildSupportVerification'
WHEN 47 THEN 'CloseLineOfCreditRequest'
WHEN 48 THEN 'ClosingInstructions'
WHEN 49 THEN 'ClosingProtectionLetter'
WHEN 50 THEN 'ComplianceAgreement'
WHEN 51 THEN 'ComplianceInspectionReport'
WHEN 52 THEN 'ConditionalCommitment'
WHEN 53 THEN 'CondominiumOccupancyCertificate'
WHEN 54 THEN 'ConservatorAndGuardianshipAgreement'
WHEN 55 THEN 'ConsolidationExtensionModificationAgreement'
WHEN 56 THEN 'ConstructionCostBreakdown'
WHEN 57 THEN 'ConsumerHandbookonAdjustableRateMortgages'
WHEN 58 THEN 'ConveyanceDeed'
WHEN 59 THEN 'CooperativeAssignmentOfProprietaryLease'
WHEN 60 THEN 'CooperativeBylaws'
WHEN 61 THEN 'CooperativeOperatingBudget'
WHEN 62 THEN 'CooperativeProprietaryLease'
WHEN 63 THEN 'CooperativeRecognitionAgreement'
WHEN 64 THEN 'CooperativeStockCertificate'
WHEN 65 THEN 'CooperativeStockPower'
WHEN 66 THEN 'CosignerNotice'
WHEN 67 THEN 'CounselingCertification'
WHEN 68 THEN 'CreditAlertInteractiveVoiceResponseSystem'
WHEN 69 THEN 'CreditCardAuthorization'
WHEN 70 THEN 'CreditInsuranceAgreement'
WHEN 71 THEN 'CreditReport'
WHEN 72 THEN 'DeathCertificate'
WHEN 73 THEN 'DivorceDecree'
WHEN 74 THEN 'ElectronicFundsTransfer'
WHEN 75 THEN 'EnergyEfficientMortgageWorksheet'
WHEN 76 THEN 'EqualCreditOpportunityActForm'
WHEN 77 THEN 'EscrowAgreement'
WHEN 78 THEN 'EscrowForCompletionAgreement'
WHEN 79 THEN 'EscrowForCompletionLetter'
WHEN 80 THEN 'EscrowWaiver'
WHEN 81 THEN 'EstimateOfClosingCostsPaidToThirdParty'
WHEN 82 THEN 'EstoppelAgreement'
WHEN 83 THEN 'FACTACreditScoreDisclosure'
WHEN 84 THEN 'FederalApplicationInsuranceDisclosure'
WHEN 85 THEN 'FederalSaleofInsuranceDisclosure'
WHEN 86 THEN 'FHAFiveDayWaiver'
WHEN 87 THEN 'FHALimitedDenialOfParticipationGeneralServicesAdministrationChecklist'
WHEN 88 THEN 'FHAMIPNettingAuthorization'
WHEN 89 THEN 'FHAMortgageCreditAnalysisWorksheet'
WHEN 90 THEN 'FHAReferralChecklist'
WHEN 91 THEN 'FHARefinanceMaximumMortgageWorksheet'
WHEN 92 THEN 'FinancialStatement'
WHEN 93 THEN 'FloodHazardNotice'
WHEN 94 THEN 'FloodInsuranceAgreement'
WHEN 95 THEN 'ForYourProtectionHomeInspection'
WHEN 96 THEN 'FreddieMacOwnedStreamlineRefinanceChecklist'
WHEN 97 THEN 'FundingTransmittal'
WHEN 98 THEN 'GeneralLoanAcknowledgement'
WHEN 99 THEN 'GiftLetter'
WHEN 100 THEN 'GoodFaithEstimate'
WHEN 101 THEN 'GoodFaithEstimateFinal'
WHEN 102 THEN 'GroupSavingsAgreement'
WHEN 103 THEN 'HighCostWorksheet'
WHEN 104 THEN 'HoldHarmlessAgreement'
WHEN 105 THEN 'HomeBuyerEducationCertification'
WHEN 106 THEN 'HomeEquityConversionMortgageAntiChurningDisclosure'
WHEN 107 THEN 'HomeEquityConversionMortgageChoiceOfInsuranceOptionsForm'
WHEN 108 THEN 'HomeEquityConversionMortgageCounselingWaiverQualification'
WHEN 109 THEN 'HomeEquityConversionMortgageExtension'
WHEN 110 THEN 'HomeEquityConversionMortgageFaceToFaceCertification'
WHEN 111 THEN 'HomeEquityConversionMortgageLoanSubmissionSchedule'
WHEN 112 THEN 'HomeEquityConversionMortgageNearestLivingRelative'
WHEN 113 THEN 'HomeEquityConversionMortgageNoticeToBorrower'
WHEN 114 THEN 'HomeEquityConversionMortgagePaymentPlan'
WHEN 115 THEN 'HomeEquityLineFreezeLetter'
WHEN 116 THEN 'HomeownersAssociationCertification'
WHEN 117 THEN 'IdentityTheftDisclosure'
WHEN 118 THEN 'IncompleteApplicationNotice'
WHEN 119 THEN 'IndividualDevelopmentAccountStatement'
WHEN 120 THEN 'IRS1098'
WHEN 121 THEN 'IRS1099MISC'
WHEN 122 THEN 'IRSW2'
WHEN 123 THEN 'IRSW8'
WHEN 124 THEN 'IRSW9'
WHEN 125 THEN 'ItemizationOfAmountFinanced'
WHEN 126 THEN 'LandLeaseholdAgreement'
WHEN 127 THEN 'LastWillAndTestament'
WHEN 128 THEN 'LenderCorrespondence'
WHEN 129 THEN 'LineItemBudget'
WHEN 130 THEN 'LoanApplication'
WHEN 131 THEN 'LoanApplicationFinal'
WHEN 132 THEN 'LoanClosingNotice'
WHEN 133 THEN 'LoanPayoffRequest'
WHEN 134 THEN 'LoanStatement'
WHEN 135 THEN 'MarriageCertificate'
WHEN 136 THEN 'MilitaryDischargePapers'
WHEN 137 THEN 'MortgageInsuranceCertificate'
WHEN 138 THEN 'MortgageInsuranceConditionalCommitment'
WHEN 139 THEN 'MortgageInsuranceModification'
WHEN 140 THEN 'NameAffidavit'
WHEN 141 THEN 'NonDiplomatVerification'
WHEN 142 THEN 'Note'
WHEN 143 THEN 'NoteAddendum'
WHEN 144 THEN 'NoteAllonge'
WHEN 145 THEN 'NoteAndSecurityInstrumentModification'
WHEN 146 THEN 'NoteModification'
WHEN 147 THEN 'NoticeOfActionTaken'
WHEN 148 THEN 'NoticeOfCompletion'
WHEN 149 THEN 'NoticeOfRightToCancel'
WHEN 150 THEN 'NoticeOfRightToCopyOfAppraisalReport'
WHEN 151 THEN 'NoticeToHomebuyer'
WHEN 152 THEN 'NoticeToLender'
WHEN 153 THEN 'OccupancyAgreement'
WHEN 154 THEN 'OccupancyCertification'
WHEN 155 THEN 'PackageLenderClosingDocuments'
WHEN 156 THEN 'PackageSignedClosingDocuments'
WHEN 157 THEN 'PartnershipAgreement'
WHEN 158 THEN 'PayStub'
WHEN 159 THEN 'PaymentHistory'
WHEN 160 THEN 'PaymentLetter'
WHEN 161 THEN 'PayoffStatement'
WHEN 162 THEN 'PermanentResidentID'
WHEN 163 THEN 'PersonalIdentification'
WHEN 164 THEN 'PersonalPropertyAppraisalReport'
WHEN 165 THEN 'PowerOfAttorney'
WHEN 166 THEN 'PreApplicationDisclosureNotice'
WHEN 167 THEN 'PrepaymentChargeOptionNotification'
WHEN 168 THEN 'PrequalificationLetter'
WHEN 169 THEN 'PrivacyDisclosure'
WHEN 170 THEN 'PropertyInspectionReport'
WHEN 171 THEN 'PropertyInsuranceBinder'
WHEN 172 THEN 'PropertyInsuranceDeclarationsPage'
WHEN 173 THEN 'PropertyInsurancePolicy'
WHEN 174 THEN 'PurchaseAgreement'
WHEN 175 THEN 'RateLockAgreement'
WHEN 176 THEN 'ReaffirmationAgreement'
WHEN 177 THEN 'Receipt'
WHEN 178 THEN 'RelocationBenefitsPackage'
WHEN 179 THEN 'RelocationBuyoutAgreement'
WHEN 180 THEN 'RentalAgreement'
WHEN 181 THEN 'RequestForCopyOfTaxReturn'
WHEN 182 THEN 'RequestForNoticeOfDefault'
WHEN 183 THEN 'RoadMaintenanceAgreement'
WHEN 184 THEN 'SatisfactionOfJudgment'
WHEN 185 THEN 'SatisfactionOfMortgage'
WHEN 186 THEN 'Section32DisclosureForm'
WHEN 187 THEN 'SecurityInstrument'
WHEN 188 THEN 'SecurityInstrumentModification'
WHEN 189 THEN 'SecurityInstrumentRider'
WHEN 190 THEN 'ServicingDisclosureStatement'
WHEN 191 THEN 'ServicingTransferStatement'
WHEN 192 THEN 'SettlementStatement'
WHEN 193 THEN 'SettlementStatementInitial'
WHEN 194 THEN 'SocialSecurityAwardLetter'
WHEN 195 THEN 'StandardFloodHazardDetermination'
WHEN 196 THEN 'StatementOfBorrowerBenefit'
WHEN 197 THEN 'StockCertificate'
WHEN 198 THEN 'SubordinationAgreement'
WHEN 199 THEN 'SubsidyAgreement'
WHEN 200 THEN 'Survey'
WHEN 201 THEN 'SurveyAffidavit'
WHEN 202 THEN 'TaxCertificate'
WHEN 203 THEN 'TaxReturn'
WHEN 204 THEN 'TenYearWarranty'
WHEN 205 THEN 'TitleAbstract'
WHEN 206 THEN 'TitleCommitment'
WHEN 207 THEN 'TitleInsuranceEndorsement'
WHEN 208 THEN 'TitleInsurancePolicy'
WHEN 209 THEN 'TrustAgreement'
WHEN 210 THEN 'TruthInLendingDisclosure'
WHEN 211 THEN 'TruthInLendingDisclosureFinal'
WHEN 212 THEN 'UCC1Statement'
WHEN 213 THEN 'UCC3Statement'
WHEN 214 THEN 'UnderwritingTransmittal'
WHEN 215 THEN 'UtilityBill'
WHEN 216 THEN 'VACertificateOfEligibility'
WHEN 217 THEN 'VACertificateOfReasonableValue'
WHEN 218 THEN 'VACollectionPolicyNotice'
WHEN 219 THEN 'VAForeclosureBidLetter'
WHEN 220 THEN 'VAFundingFeeNotice'
WHEN 221 THEN 'VAInterestRateReductionRefinancingLoanWorksheet'
WHEN 222 THEN 'VALoanAnalysis'
WHEN 223 THEN 'VAReportAndCertificationOfLoanDisbursement'
WHEN 224 THEN 'VARequestForCertificationOfEligibilityForHomeLoanBenefit'
WHEN 225 THEN 'VAVerificationOfBenefitRelatedIndebtedness'
WHEN 226 THEN 'VerificationOfCredit'
WHEN 227 THEN 'VerificationOfDependentCare'
WHEN 228 THEN 'VerificationOfDeposit'
WHEN 229 THEN 'VerificationOfEmployment'
WHEN 230 THEN 'VerificationOfMortgageOrRent'
WHEN 231 THEN 'VerificationOfSecurities'
WHEN 232 THEN 'VolunteerEscrowPrepaymentDesignation'
WHEN 233 THEN 'WireInstructions'
WHEN 234 THEN 'WireTransferAuthorization'
WHEN 235 THEN 'WireTransferConfirmation'
WHEN 236 THEN 'Worksheet'
WHEN 237 THEN 'Other'
WHEN 238 THEN '_203KConsultantReport'
END as ClassificationType
from EDOCS_DOCUMENT_TYPE e with(nolock) join broker b with(nolock) on e.brokerid=b.brokerid
where b.Status = 1 and e.ClassificationId <> 0
order by CustomerCode
";

            const string UpdateSql = @"
UPDATE EDOCS_DOCUMENT_TYPE
SET ClassificationId = 0
from EDOCS_DOCUMENT_TYPE e with(nolock) join broker b with(nolock) on e.brokerid=b.brokerid
where b.Status = 1 and e.ClassificationId <> 0
";

            var backupList = new List<object>();

            Action<IDataReader> readerCode = reader =>
            {
                while (reader.Read())
                {
                    backupList.Add(new
                    {
                        CustomerCode = reader.SafeString("CustomerCode"),
                        DocTypeName = reader.SafeString("DocTypeName"),
                        ClassificationId = reader.SafeString("ClassificationId"),
                        ClassificationType = reader.SafeString("ClassificationType")
                    });
                }
            };

            var backupPath = LocalFilePath.Create(nameof(Opm461275_ClearUnusedEdocClassification) + "_backup.txt").Value;

            TextFileHelper.WriteString(
                backupPath.Value,
                string.Join(Environment.NewLine, backupList.Select(backup => SerializationHelper.JsonNetAnonymousSerialize(backup))),
                writeBOM: false);

            foreach (var connection in DbConnectionInfo.ListAll())
            {
                DBSelectUtility.ProcessDBData(connection, SelectSql, TimeoutInSeconds.Sixty, parameters: null, readerCode: readerCode);
                DBUpdateUtility.Update(connection, UpdateSql, TimeoutInSeconds.Sixty, parameters: null);
            }
        }

        // More general migrations and operations
        public class AddNewMFATempOptionMigrator : Migrator<Guid>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="AddNewMFATempOption"/> class.
            /// </summary>
            /// <param name="name">The name of the migratino.</param>
            public AddNewMFATempOptionMigrator(string name)
                : base(name)
            {
                this.MigrationHelper = new BrokerMigrationHelper(name);
            }

            public AddNewMFATempOptionMigrator(string name, Guid brokerId)
                : base(name)
            {
                this.MigrationHelper = new CustomGUIDMigrationHelper(name, () => new List<Guid> { brokerId });
            }

            /// <summary>
            /// Adds the AllowDisableUserMfaBits temp option.
            /// </summary>
            /// <param name="args"></param>
            public static void AddNewMFATempOption(string[] args)
            {
                AddNewMFATempOptionMigrator migrator;
                if (args.Length == 2)
                {
                    Guid broker = Guid.Parse(args[1]);
                    migrator = new AddNewMFATempOptionMigrator("AddNewMFATempOption", broker);
                }
                else
                {
                    migrator = new AddNewMFATempOptionMigrator("AddNewMFATempOption");
                }

                migrator.Migrate();
            }

            /// <summary>
            /// The migration function.
            /// This will set the opt in MFA bit to true but also add in the temp bit that allows manually setting the user level MFA bits.
            /// This will preserve the behavior for all users.
            /// </summary>
            /// <param name="id">The broker id to migrate.</param>
            /// <returns>True if the migration is successful. False otherwise.</returns>
            protected override bool MigrationFunction(Guid id)
            {
                try
                {
                    BrokerDB broker = BrokerDB.RetrieveById(id);

                    string tempOption = "<option name=\"AllowUserMfaToBeDisabled\" value=\"true\" />";
                    if (broker.TempOptionXmlContent.Value.IndexOf(tempOption, StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        return false;
                    }

                    if (broker.TempOptionXmlContent.Value.Contains("</options>"))
                    {
                        broker.TempOptionXmlContent = broker.TempOptionXmlContent.Value.Replace("</options>", $"{tempOption}</options>");
                    }
                    else
                    {
                        broker.TempOptionXmlContent = broker.TempOptionXmlContent.Value + tempOption;
                    }

                    broker.Save();
                    return true;
                }
                catch (Exception e)
                {
                    Tools.LogError("Error updating broker <" + id.ToString() + "> for AddNewMFATempOption.", e);
                }

                return false;
            }
        }

        private static void QueueUpAllLoans(string[] args)
        {
            if (arg2.Length < 3)
            {
                Console.WriteLine("Need to provide brokerid and app code.");
                return;
            }

            Guid brokerId = new Guid(args[1]);
            Guid appCode = new Guid(args[2]);

            BrokerDB db = BrokerDB.RetrieveById(brokerId);
            var appCodes = AppCode.Retrieve(brokerId);
            AppCode code = appCodes.FirstOrDefault(p => p.Id == appCode);

            if (code == null)
            {
                Console.WriteLine("Appcode {0} is not in {1} appcode list.", appCode, db.CustomerCode);
                return;
            }

            Console.WriteLine("Will enqueue all loans for customer {0} {1} on app code {2} {3}. Enter YES to continue.", db.CustomerCode, db.Name, code.Id, code.Name);
            string line = Console.ReadLine();
            if (line.Trim() != "YES")
            {
                Console.WriteLine("Quitting.");
                return;
            }

            var loans = RetrieveCachedLoanFiles(brokerId);

            Console.WriteLine("Found {0} loans. Hit enter to continue.", loans.Count);
            Console.ReadLine();

            int count = 0;
            foreach (var item in loans)
            {
                count++;
                SqlParameter[] parameters =
                {
                    new SqlParameter("@BrokerId", brokerId),
                    new SqlParameter("@AppCode", code.Id),
                    new SqlParameter("@LoanName", item.Item1),
                    new SqlParameter("@LoanId", item.Item2)
                };

                StoredProcedureHelper.ExecuteNonQuery(brokerId, "TrackIntegrationModifiedFile_SpecificAppCode", 1, parameters);
                Thread.Sleep(100);
                Console.Write("\r{0}/{1} sLId = {2}", count, loans.Count, item.Item2);
            }

        }

        private static LinkedList<Tuple<string, Guid>> RetrieveCachedLoanFiles(Guid brokerId)
        {
            // Unit tested by copying to test library
            string sql = @"
select slid, slnm, sbrokerid  
from loan_file_cache 
where sbrokerid = @brokerid and isvalid = 1 and sLoanFileT = 0 and IsTemplate = 0 
";
            var listParams = new SqlParameter[] { new SqlParameter("@BrokerId", brokerId) };

            var loans = new LinkedList<Tuple<string, Guid>>();
            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                while (reader.Read())
                {
                    Guid resultBrokerId = (Guid)reader["sbrokerid"];

                    if (resultBrokerId != brokerId)
                    {
                        Console.WriteLine("Error in app cross broker.");
                        return;
                    }

                    loans.AddLast(Tuple.Create((string)reader["slnm"], (Guid)reader["slid"]));
                }
            };

            DBSelectUtility.ProcessDBData(brokerId, sql, null, listParams, readHandler);
            return loans;
        }

        private static void RunEDocNotification(string[] args)
        {
            EDocNotificationQueueProcessor proc = new EDocNotificationQueueProcessor();
            proc.Run();
        }

        private static void ExportCustomReportsSchema(string[] args)
        {
            if (args.Length < 2)
            {
                throw new InvalidOperationException(
                    "Must specify output path. Expected usage: " +
                    "ScheduleExecutable.exe ExportCustomReportsSchema [OUTPUT_PATH]");
            }

            var outputPath = args[1];

            XDocument schema = LoanReporting.Schema.ExportToXDoc();
            schema.Save(outputPath);
        }

        private static void BatchDisableTaskNotificationsForPmlUsers(string[] args)
        {
            Guid brokerId = Guid.Empty;

            if (args.Length != 2)
            {
                throw new InvalidOperationException(
                    "You must specify the id of the broker for the batch disable task notification migration.");
            }
            else if (!Guid.TryParse(args[1], out brokerId))
            {
                throw new InvalidOperationException(
                    $"The specified broker id {args[1]} is not a valid GUID.");
            }

            var originatingCompanyMigrator = new DisableOriginatingCompanyTaskEmailMigrator(
                "DisableTaskNotificationsAtOcLevel_" + brokerId.ToString("N"),
                brokerId);

            originatingCompanyMigrator.Migrate();

            var userMigrator = new DisablePmlUserTaskEmailMigrator(
                "DisableTaskNotificationsForPmlUsers_" + brokerId.ToString("N"),
                brokerId);

            userMigrator.Migrate();
        }

        private static void UploadMMIPools(string[] args)
        {
            Guid brokerId = new Guid(args[1]);
            List<MortgagePool> pools = MortgagePool.FindMortgagePools(brokerId);

            DataTable dt = new DataTable();
            var columns = new[]
            {
                new { Header = "PoolId", ColumnType = typeof(long), Getter = new Func<MortgagePool, object>((MortgagePool pool) => pool.PoolId) },
                new { Header = "InvestorName", ColumnType = typeof(string), Getter = new Func<MortgagePool, object>((MortgagePool pool) => pool.InvestorName) },
                new { Header = "PoolSettlementDate", ColumnType = typeof(string), Getter = new Func<MortgagePool, object>((MortgagePool pool) => pool.SettlementD_rep) },
                new { Header = "CommitmentNumber", ColumnType = typeof(string), Getter = new Func<MortgagePool, object>((MortgagePool pool) => pool.CommitmentNum) },
                new { Header = "CommitmentDate", ColumnType = typeof(string), Getter = new Func<MortgagePool, object>((MortgagePool pool) => pool.CommitmentDate_rep)},
                new { Header = "CommitmentExpires", ColumnType = typeof(string), Getter = new Func<MortgagePool, object>((MortgagePool pool) => pool.CommitmentExpires_rep)},
                new { Header = "SecurityRate", ColumnType = typeof(string), Getter = new Func<MortgagePool, object>((MortgagePool pool) => pool.SecurityR_rep)},
            };

            foreach (var column in columns)
            {
                dt.Columns.Add(new DataColumn(column.Header, column.ColumnType));
            }

            foreach (var pool in pools)
            {
                DataRow row = dt.NewRow();
                foreach (var column in columns)
                {
                    row[column.Header] = column.Getter.Invoke(pool);
                }
                dt.Rows.Add(row);
            }

            string csv = dt.ToCSV(true);

            string file = TempFileUtils.NewTempFilePath();
            TextFileHelper.WriteString(file, csv, false);
            Console.WriteLine(file);

            SFTPXsltExport.SFTPXsltExportCallBack.UploadFile("ftp.mimutual.com", "ftp.mimutual.com|lqb_ftp", args[2], file, "/lqb_ftp/pools.csv", string.Empty);
        }

        private static void UploadMMIBranches(string[] args)
        {

            Guid brokerId = new Guid(args[1]);
            Dictionary<Guid, PriceGroup> priceGroupsById = PriceGroup.RetrieveAllFromBroker(brokerId).ToDictionary(p => p.ID);
            IEnumerable<BranchDB> branches = BranchDB.GetBranchObjects(brokerId);

            Func<BranchDB, Object> groupMembership = (BranchDB db) =>
            {
                StringBuilder sb = new StringBuilder();
                foreach (var g in GroupDB.ListInclusiveGroupForBranch(brokerId, db.BranchID))
                {
                    sb.Append(g.Name);
                    sb.Append(";");
                }
                if (sb.Length > 0)
                {
                    sb.Remove(sb.Length - 1, 1);
                }
                return sb.ToString();
            };

            Func<BranchDB, Object> licenseInfo = (BranchDB db) =>
            {
                LicenseInfoList li = LicenseInfoList.ToObject(db.LicenseXmlContent);
                StringBuilder sb = new StringBuilder();

                foreach (LicenseInfo info in li.List)
                {
                    sb.Append(info.State);
                    sb.Append(";");
                    sb.Append(info.ExpD);
                    sb.Append(";");
                    sb.Append(info.License);
                    sb.Append("|");
                }

                if (sb.Length > 0)
                {
                    sb.Remove(sb.Length - 1, 1);
                }

                return sb.ToString();
            };

            Func<BranchDB, Object> priceGroupName = (BranchDB db) =>
            {
                PriceGroup pricegroup = null;

                priceGroupsById.TryGetValue(db.BranchLpePriceGroupIdDefault, out pricegroup);

                return pricegroup == null ? "" : pricegroup.Name;
            };

            DataTable dt = new DataTable();
            var columns = new[]
            {
                new { Header = "BranchId", ColumnType = typeof(Guid), Getter = new Func<BranchDB, object>((BranchDB db) => db.BranchID) },
                new { Header = "DisplayName", ColumnType = typeof(string), Getter = new Func<BranchDB, object>((BranchDB db) => db.DisplayNm) },
                new { Header = "BranchName", ColumnType = typeof(string), Getter = new Func<BranchDB, object>((BranchDB db)  => db.Name) },
                new { Header = "BranchCode", ColumnType = typeof(string), Getter = new Func<BranchDB, object>(db => db.BranchCode) },
                new { Header = "BranchChannelType", ColumnType = typeof(string) , Getter = new Func<BranchDB, object>(db => db.BranchChannelT.ToString()) },
                new { Header = "Address1", ColumnType = typeof(string) ,Getter = new Func<BranchDB, object>(db => db.Address.StreetAddress) },
                new { Header = "City", ColumnType = typeof(string) ,Getter = new Func<BranchDB, object>(db => db.Address.City) },
                new { Header = "State", ColumnType = typeof(string),Getter = new Func<BranchDB, object>(db => db.Address.State) },
                new { Header = "ZIP", ColumnType = typeof(string) ,Getter = new Func<BranchDB, object>(db => db.Address.Zipcode) },
                new { Header = "Phone", ColumnType = typeof(string) ,Getter = new Func<BranchDB, object>(db => db.Phone) },
                new { Header = "Fax", ColumnType = typeof(string) ,Getter = new Func<BranchDB, object>(db => db.Fax) },
                new { Header = "DefaultPriceGroup", ColumnType = typeof(string) ,Getter = priceGroupName },
                new { Header = "NMLSId", ColumnType = typeof(string) ,Getter = new Func<BranchDB, object>(db => db.LosIdentifier) },
                new { Header = "BranchGroups", ColumnType = typeof(string) ,Getter = groupMembership},
                new { Header = "LicensingInfo", ColumnType = typeof(string) ,Getter = licenseInfo},
                new { Header = "Division", ColumnType = typeof(string) ,Getter = new Func<BranchDB, object>(db => db.Division) },
                new { Header = "BranchStatus", ColumnType = typeof(string) ,Getter = new Func<BranchDB, object>(db => db.Status.ToString()) },
            };

            foreach (var column in columns)
            {
                dt.Columns.Add(new DataColumn(column.Header, column.ColumnType));
            }

            foreach (var branch in branches)
            {
                DataRow row = dt.NewRow();
                foreach (var column in columns)
                {
                    row[column.Header] = column.Getter.Invoke(branch);
                }
                dt.Rows.Add(row);
            }

            string csv = dt.ToCSV(true);

            string file = TempFileUtils.NewTempFilePath();
            TextFileHelper.WriteString(file, csv, false);
            Console.WriteLine(file);

            SFTPXsltExport.SFTPXsltExportCallBack.UploadFile("ftp.mimutual.com", "ftp.mimutual.com|lqb_ftp", args[2], file,  "/lqb_ftp/branch.csv", string.Empty);
        }

		private static void UpdateLicenseInfo(string[] args)
        {
            string[] lines = TextFileHelper.ReadLines(args[1]).ToArray();

            var loanIds = from p in lines
                          select new Guid(p.Trim());

            Console.WriteLine("Will update {0} loans.", lines.Length);

            int num = 0;

            foreach (Guid sLId in loanIds)
            {
                CPageData pageData = new NotEnforceAccessControlPageData(sLId, new string[] { "sf_UpdateLicenseInfoForLoanOfficer" });
                pageData.DisableLoanModificationTracking = true;
                pageData.InitSave(ConstAppDavid.SkipVersionCheck);
                try
                {
                    if (pageData.UpdateLicenseInfoForLoanOfficer())
                    {
                        pageData.Save();
                    }
                }
                catch (Exception)
                {
                    TextFileHelper.AppendString("UpdateLicenseInfoForLoanOfficer_error.txt", sLId.ToString() + Environment.NewLine);
                }
                num++;
                ClearCurrentConsoleLine();
                Console.WriteLine("Loan {0} out of {1} Last Updated {2}", num, lines.Length, sLId);
            }
        }

        private static void ExportLicenseInfo(string[] args)
        {
            if (arg2.Length < 3)
            {
                return;
            }

            Guid brokerId;

            if (!Guid.TryParse(args[1], out brokerId))
            {
                Console.WriteLine("Could not get brokerid.");
                return;
            }

            if (args[2].Length != 1 && (args[2] != "b" && args[2] != "p"))
            {
                Console.WriteLine("USer Type can only be b or p.");
                return;
            }

            List<Tuple<Guid, string, string>> rows = RetrieveEmployeeLicenseContent(args[2][0], brokerId);

            String file = string.Format("LicenseExport_{0}_{1}.csv", brokerId, args[2]);
            StringBuilder sb = new StringBuilder("EmployeeId,Login Name,License #, State, Expiration Date\n");
            foreach (var row in rows)
            {
                try
                {
                    LicenseInfoList list = LicenseInfoList.ToObject(row.Item3);

                    foreach (LicenseInfo license in list)
                    {
                        sb.AppendFormat("{0},{1},{2},{3},{4}\n", row.Item1, row.Item2, license.License, license.State, license.ExpD);
                    }

                }
                catch (Exception)
                {
                    Console.Error.WriteLine("Could not parse xml for {0}", row.Item2);
                }
            }

            if (FileOperationHelper.Exists(file))
            {
                FileOperationHelper.Delete(file);
            }

            TextFileHelper.WriteString(file, sb.ToString(), false);

        }

        private static List<Tuple<Guid, string, string>> RetrieveEmployeeLicenseContent(char userType, Guid brokerId)
        {
            // Unit tested by copying to the test library
            string sql = @"
select e.employeeid, loginnm, licensexmlcontent
    from view_employee_role ver 
        join EMPLOYEE e on e.employeeid = ver.employeeid 
        join broker_user bu on bu.employeeid = e.employeeid 
        join all_user au on au.userid = bu.userid 
    where roleid = '6DFC9B0E-11E3-48C1-B410-A1B0B5363B69' and brokerid = @brokerid and type = @usertype and e.isactive = 1";

            SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@brokerid", brokerId), DbAccessUtils.SqlParameterForSingleChar("@usertype", userType) };

            List<Tuple<Guid, string, string>> rows = new List<Tuple<Guid, string, string>>();
            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                while (reader.Read())
                {
                    rows.Add(Tuple.Create((Guid)reader["employeeid"], (string)reader["loginnm"], (string)reader["licensexmlcontent"]));
                }
            };

            var conInfo = DbConnectionInfo.GetConnectionInfo(brokerId);
            DBSelectUtility.ProcessDBData(conInfo, sql, null, parameters, readHandler);
            return rows;
        }

		private static void MigrateCCArchiveLoanDataToFileDB(string[] args)
        {
            var migrator = new MigrateCCArchiveLoanDataToFileDB("MigrateClosingCostArchiveContentToFileDB");
            migrator.Migrate();
        }

        /// <summary>
        /// 9/8/2015 - dd
        /// Read a list of BrokerId, DocumentId, FileDBKey_CurrentRevision from text file and put into migratin queue.
        /// Remove this method when all brokers are convert to new edocs.
        /// </summary>
        /// <param name="args"></param>
        private static void EnqueueEdocsMigration(string[] args)
        {
            List<EDocsMigrationRequestItem> queueList = new List<EDocsMigrationRequestItem>();

            string[] lines = TextFileHelper.ReadLines("EdocsMigration.txt").ToArray();

            foreach (string line in lines)
            {
                if (string.IsNullOrEmpty(line))
                {
                    continue;
                }
                string[] parts = line.Split(',');

                EDocsMigrationRequestItem item = new EDocsMigrationRequestItem();
                item.BrokerId = new Guid(parts[0]);
                item.DocumentId = new Guid(parts[1]);
                item.FileDBKey_CurrentRevision = new Guid(parts[2]);

                queueList.Add(item);

            }

            Console.WriteLine(queueList.Count + " edocs to migrate.");
            foreach (var item in queueList)
            {
                EDocument.UpdateImageStatus(item.BrokerId, item.DocumentId, E_ImageStatus.NoCachedImagesNotInQueue);
                LendersOffice.Drivers.Gateways.MessageQueueHelper.SendJSON(ConstStage.MSMQ_EdocsNightlyMigration, null, item);
            }
        }

        public static void UpdateAllowCreateWholesaleLoanPermission(string[] args)
        {
            if (args.Length != 2)
            {
                Console.WriteLine("Expected usage: ScheduleExecutable.exe UpdateAllowCreatingWholesaleLoanPermission [BROKERID]");
                return;
            }

            Guid brokerID;

            try
            {
                brokerID = new Guid(args[1]);
            }
            catch (FormatException)
            {
                Console.WriteLine("Invalid Guid supplied as broker id.");
                return;
            }
            catch (OverflowException)
            {
                Console.WriteLine("Invalid Guid supplied as broker id.");
                return;
            }

            var migrator = new AllowCreatingWholesaleLoansPermissionMigrator(
                string.Format("UpdateAllowCreateWholesaleLoanPermission_{0}", brokerID),
                brokerID);
            migrator.Migrate();
        }

		#region MigrateSettlementCharges
		/// <summary>
		/// Runs settlement charges migration with current closing cost source set to GFE
		/// and last disclosed closing cost source set to GFE archive.
		/// </summary>
		/// <param name="args">String arguments.</param>
		/// <remarks>This migration will need to be kept around at least until we get rid of
		/// the Settlement Charges page. As other brokers are likely to request this kind of
		/// migration, this function was designed to take in a text file with a broker's
		/// customer code as the first line followed by a list of loan names separated by
		/// lines. See OPM 212209 for more detail.</remarks>
		/// <remarks>
		/// Either:
		/// MigrateSettlementCharges 
		/// OR
		/// MigrateSettlementCharges [E_CurrentClosingCostSourceT] [E_LastDisclosedClosingCostSourceT]
		/// </remarks>
		public static void MigrateSettlementCharges(string[] args)
		{
			if (args.Length != 1 && args.Length != 3)
			{
				Console.WriteLine("Either pass in no arguments or 2 arguments to choose settings for the migration. Proper formatting in remarks in code.");
				return;
			}

            E_CurrentClosingCostSourceT passedInCurrentCCSource = E_CurrentClosingCostSourceT.GFE;
            E_LastDisclosedClosingCostSourceT passedInLastDisclosedCCSource = E_LastDisclosedClosingCostSourceT.GFEArchive;
            if (args.Length == 3)
			{
                // We'll let this error out if it can't parse correctly.
                passedInCurrentCCSource = (E_CurrentClosingCostSourceT)Enum.Parse(typeof(E_CurrentClosingCostSourceT), args[1]);
                passedInLastDisclosedCCSource = (E_LastDisclosedClosingCostSourceT)Enum.Parse(typeof(E_LastDisclosedClosingCostSourceT), args[2]);
            }

			// Read in file.
			string[] file = TextFileHelper.ReadLines("MigrateSettlementCharges.txt").ToArray();

			// Get broker customer code from first line of file.
			if (String.IsNullOrEmpty(file[0].Trim()))
			{
				string msg = "MigrateSettlementCharges. First line of text file should be a valid broker customer code.";
				throw new CBaseException(msg, msg);
			}

			Guid brokerId;
			try
			{
				brokerId = Tools.GetBrokerIdByCustomerCode(file[0].Trim());
			}
			catch (NotFoundException e)
			{
				string msg = "MigrateSettlementCharges. Failed to find broker with customer code = " + file[0].Trim() + ".";
				throw new CBaseException(msg, e);
			}

			// Read in loan names from rest of file (one loan per line).
			foreach (string loanName in file.Skip(1))
			{
				// Skip blank lines.
				if (String.IsNullOrEmpty(loanName.Trim()))
				{
					continue;
				}

				Guid sLId = Tools.GetLoanIdByLoanName(brokerId, loanName);

				// Log if loan not found.
				if (sLId == Guid.Empty)
				{
					TextFileHelper.AppendString("MigrateSettlementCharges_log.txt", loanName + " was not found." + Environment.NewLine);
					continue;
				}

				// Migrate found loan.
				CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(sLId, typeof(Program));
				dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

				try
				{
                    E_CurrentClosingCostSourceT currentCCSource = passedInCurrentCCSource;
                    E_LastDisclosedClosingCostSourceT lastDisclosedCCSource = passedInLastDisclosedCCSource;

                    if (dataLoan.sClosingCostMigrationLastDisclosedClosingCostSource == E_LastDisclosedClosingCostSourceT.GFEArchive)
					{
						// The Settlement Charges Migration page doesn't seem to allow choosing an option besides GFEArchive if this loan field is set to GFEArchive.
						// We'll force that here as well.
						lastDisclosedCCSource = E_LastDisclosedClosingCostSourceT.GFEArchive;
					}

					dataLoan.MigrateCurrentClosingCostDataToGFE(currentCCSource, lastDisclosedCCSource);
				}
				catch (Exception e)
				{
					// Log if loan could not be migrated. Either the broker is set up wrong or the loan is already migrated.
					TextFileHelper.AppendString("MigrateSettlementCharges_log.txt", loanName + " could not be migrated. It may have already been migrated." + Environment.NewLine);
					Tools.LogWarning("MigrateSettlementCharges. Failed to migrate loan with loanName=" + loanName, e);  // PB Log of actual error.
					continue;
				}

				try
				{
					dataLoan.Save();
				}
				catch (Exception e)
				{
					TextFileHelper.AppendString("MigrateSettlementCharges_log.txt", loanName + " could not be saved." + Environment.NewLine);
					Tools.LogWarning("MigrateSettlementCharges. Failed to save migrated loan with loanName=" + loanName, e);  // PB Log of actual error.
					continue;
				}
			}
		}

        #endregion

        private static void Migratesspstatepe(string[] args)
        {
            Guid userId;
            if (string.IsNullOrEmpty(arg2))
            {
                System.Console.WriteLine("Specify loadmin user id to modify workflows as.");
                return;
            }
            userId = new Guid(arg2);

            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                IConfigRepository repo = ConfigHandler.GetUncachedRepository(connInfo);
                List<ReleaseConfigData> configs = repo.GetActiveReleases().ToList();


                foreach (var config in configs)
                {
                    bool update = false;
                    var configObj = new AbstractSecurityPermissionSet[] { config.Configuration.Conditions, config.Configuration.Constraints };

                    Tools.LogInfo("Migration for broker id : " + config.OrganizationId);
                    Tools.LogInfo(config.Configuration.ToString());

                    foreach (var con in configObj)
                    {
                        foreach (var condition in con)
                        {
                            foreach (var parameter in condition.ParameterSet)
                            {
                                if (parameter.Name.Equals("sspstate", StringComparison.OrdinalIgnoreCase))
                                {
                                    if (parameter.FunctionT == ConfigSystem.E_FunctionT.IsNonblankStr && parameter.Values.First() == "True")
                                    {
                                        parameter.FunctionT = ConfigSystem.E_FunctionT.In;
                                        parameter.ClearValues();
                                        foreach (var state in StateInfo.Instance.GetAllStates())
                                        {
                                            parameter.AddValue(state, state);
                                        }
                                        update = true;
                                    }
                                    else
                                    {
                                        Tools.LogWarning("unhandled situation.");
                                    }
                                }
                            }
                        }
                    }


                    foreach (var customVariable in config.Configuration.CustomVariables)
                    {
                        foreach (var parameter in customVariable.ParameterSet)
                        {
                            if (parameter.Name.Equals("sspstate", StringComparison.OrdinalIgnoreCase))
                            {
                                if (parameter.FunctionT == ConfigSystem.E_FunctionT.IsNonblankStr && parameter.Values.First() == "True")
                                {
                                    parameter.FunctionT = ConfigSystem.E_FunctionT.In;
                                    parameter.ClearValues();
                                    foreach (var state in StateInfo.Instance.GetAllStates())
                                    {
                                        parameter.AddValue(state, state);
                                    }
                                    update = true;
                                }
                                else
                                {
                                    Tools.LogWarning("unhandled situation.");
                                }
                            }
                        }
                    }

                    if (update)
                    {
                        Tools.LogInfo(config.Configuration.ToString());
                        repo.SaveReleaseConfig(config.OrganizationId, config.Configuration, userId);
                    }
                    else
                    {
                        Tools.LogInfo("Did not update.");
                    }
                }
            }
        }

        private static void MonitorQueues(string[] args)
        {

            string value = ConfigurationManager.AppSettings["QueueStatusInfo"];
            if (string.IsNullOrEmpty(value))
            {

                System.Console.WriteLine("Did not find  QueueStatusInfo in appsettings.");
                return;
            }

            JavaScriptSerializer srl = new JavaScriptSerializer();
            List<QuueueStatusInfo> queuInfo = srl.Deserialize<List<QuueueStatusInfo>>(value);
            StringBuilder errorMessage = new StringBuilder();

            foreach (QuueueStatusInfo info in queuInfo)
            {
                DBMessageQueue q = new DBMessageQueue(new DBQueue(info.Id, info.Name));
                DBMessage[] msgs = q.PeekN(1);
                if (msgs.Length == 0)
                {
                    continue;
                }

                var minutes = DateTime.Now.Subtract(msgs[0].InsertionTime).TotalMinutes;

                if (minutes > info.MinuteCutoff)
                {
                    errorMessage.AppendFormat("Queue {0} Id: {1} has cutoff time of {2} minutes but oldest message is {3} minutes old.\n", info.Name, info.Id, info.MinuteCutoff, minutes);
                }
            }

            if (errorMessage.Length > 0)
            {
                Tools.LogErrorWithCriticalTracking("QUEUE CHECK FAILURES" + errorMessage.ToString());
            }
        }

        private static void EDocsPNGCleanup(string[] args)
        {
            EDocsPNGCleanupProcessor processor = new EDocsPNGCleanupProcessor();
            processor.Run();

        }

        private static void TaskTriggerNightly(string[] args)
        {
            TaskTriggerTemplateNightlyProcessor.PopulateLoansNeedToBeRun();
            TaskTriggerTemplateNightlyProcessor processor = new TaskTriggerTemplateNightlyProcessor();

            processor.Run();
        }

        private static void TaskNightly(string[] args)
        {
            NightlyTaskRunnerProcessor.PopulateLoansNeedToBeRun();

            NightlyTaskRunnerProcessor processor = new NightlyTaskRunnerProcessor();
            processor.Run();

        }

        private static void DisclosureNightly(string[] args)
        {
            var runner = new NightlyDisclosureRunner();
            runner.Run();
        }

        private static void DisclosureNightly_ProcessLoansForEConsentExpiration(string[] args)
        {
            bool generateCriticalCaseOnFailure = false;
            NightlyDisclosureRunner.ProcessEConsentExpiration(generateCriticalCaseOnFailure, logger: null);
        }

        private static void SequentialNumberingNightly(string[] args)
        {
            var runner = new SequentialNumberingRunner();
            runner.Run();
        }

        private static void CleanupExpireLpeRequestAndResult(string[] args)
        {
            // 9/13/2011 dd - Note: Use to run in the same executable as MoveAuditTrailToFileDB
            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOTransient, "LPE_DeleteExpiredResults_0", 4);
            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOTransient, "LPE_DeleteExpiredResults_1", 4);
            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOTransient, "LPE_DeleteExpiredResults_2", 4);
            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOTransient, "LPE_DeleteExpiredResults_3", 4);
            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOTransient, "LPE_DeleteExpiredResults_4", 4);
            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOTransient, "LPE_DeleteExpiredResults_5", 4);
            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOTransient, "LPE_DeleteExpiredResults_6", 4);
            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOTransient, "LPE_DeleteExpiredResults_7", 4);
            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOTransient, "LPE_DeleteExpiredResults_8", 4);
            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOTransient, "LPE_DeleteExpiredResults_9", 4);

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOTransient, "LPE_RemoveExpiredRequestMessages", 4);
        }

        private static void MoveAuditTrailToFileDB(string[] args)
        {
            // 10/7/2005 dd - Temporary place to move daily audit to FileDB
            List<Guid> list = new List<Guid>();
            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connInfo, "ListLoanIdInAuditTrail", null))
                {
                    while (reader.Read())
                    {
                        list.Add((Guid)reader["LoanId"]);
                    }
                }
            }
            foreach (Guid id in list)
            {
                LendersOffice.Audit.AuditManager.MoveToPermanentStorage(id);
            }
        }

        /// <summary>
        /// This method will mask all the sensitive information out. Since this is
        /// a massive data change operation. You will be prompt for broker id manually. THerefore this method
        /// is not intend to execute on daily basis.
        /// </summary>
        private static void MaskBrokerLoans(string[] args)
        {
            Console.Write("Enter BrokerId: ");
            Guid brokerId = Guid.Empty;
            try
            {
                brokerId = new Guid(Console.ReadLine());
            }
            catch (FormatException)
            {
                Console.WriteLine("Invalid GUID");
                return;
            }
            if (brokerId == Guid.Empty)
            {
                Console.WriteLine("Invalid GUID");
                return;

            }

            List<Guid> loanIdList = new List<Guid>();
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId)
                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "ListLoansByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    loanIdList.Add((Guid)reader["LoanId"]);
                }
            }

			for (int i = 0; i < loanIdList.Count; i++)
			{
				LoanMasking.MaskLoan(loanIdList[i]);
				Console.WriteLine("Finish " + i + " out of " + loanIdList.Count + " - " + loanIdList[i]);
			}

		}

        private static void DocumentImageFix(string[] args)
        {


            foreach (string line in TextFileHelper.ReadLines("docids.txt"))
            {
                if (line.Trim().Length == 0)
                {
                    break;
                }
                Guid docId = new Guid(line.Trim());
                EDocument doc = EDocument.GetDocumentByIdObsolete(docId);
                string path = doc.GetPDFTempFile_Current();
                FileOperationHelper.Copy(path, TempFileUtils.Name2Path(docId.ToString() + "_backup.pdf"), false);
                string newPath = TempFileUtils.NewTempFilePath();
                using (PDFPageIDManager manager = new PDFPageIDManager(path))
                {
                    manager.DeletePageIds();
                    manager.Save(newPath);
                }
                FileDBTools.WriteFile(E_FileDB.EDMS, doc.PdfFileDBKeyCurrentRevision, newPath);
                doc.RegenerateImages();

                Console.WriteLine("Image ids deleted on " + docId);
            }

            Console.Write("Done With Image Migration");
        }

        private static void MigrateCreateLeadsPermission(string[] args)
        {
            UpdateDefaultPermission(
                E_RoleT.CallCenterAgent,
                isOn: true,
                permissions: Permission.AllowCreatingNewLeadFiles);

            UpdateDefaultPermission(
                E_RoleT.Manager,
                isOn: true,
                permissions: Permission.AllowCreatingNewLeadFiles);

            var permissionGrantingSituation = new NewPermissionGrantingSituation
            {
                ApplicableUserTypes = new List<char> { 'B' },
                SufficientRoles = new List<E_RoleT> { E_RoleT.CallCenterAgent, E_RoleT.Manager },
                PermissionsToGrant = new List<Permission> { Permission.AllowCreatingNewLeadFiles },
                BrokerRequirements = (broker) => broker.Status == 1
            };

            SetUserPermissions(
                permissionGrantingSituations: new[] { permissionGrantingSituation },
                brokerFilter: new HashSet<Guid>(Tools.GetAllActiveBrokers()),
                employeeFilter: new HashSet<Guid>());
        }

        private static void AllBrokersDisableTPOServices(string[] args)
        {
            List<Guid> brokerIds = (List<Guid>)Tools.GetAllActiveBrokers();

            foreach (Guid id in brokerIds)
            {
                BrokerDB broker = BrokerDB.RetrieveById(id);

                string tempOptionXml = broker.TempOptionXmlContent.Value;

                if (!tempOptionXml.Contains("<option name=\"HideServicesDashboardInTPOPortal\" value=\"true\" />"))
                {
                    if (tempOptionXml.Contains("</options>"))
                    {
                        tempOptionXml = tempOptionXml.Replace("</options>", "<option name=\"HideServicesDashboardInTPOPortal\" value=\"true\" /> </options>");
                    }
                    else
                    {
                        tempOptionXml += "<option name=\"HideServicesDashboardInTPOPortal\" value=\"true\" />";
                    }

                    broker.TempOptionXmlContent = tempOptionXml;
                    broker.Save();
                }
            }
        }

        private static void InitializeApplicationFramework()
        {
            IExceptionHandlerFactory[] arrHandlers = ExceptionHandlerUtil.GetCoreExceptionHandlerFactories();
            using (IApplicationInitialize iAppInit = LqbApplication.CreateInitializer(arrHandlers))
            {
                const string AppName = "ScheduleExecutable";
                iAppInit.SetName(AppName);

                Adapter.ApplicationInitializer.RegisterFactories(AppName, iAppInit);
                LendersOffice.ApplicationInitializer.RegisterFactories(AppName, iAppInit);
            }
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Working directory is : {0}", Directory.GetCurrentDirectory());

            InitializeApplicationFramework();

            m_args = args;
            if (args.Length < 1)
            {
                PrintUsage();
                return;
            }

            string cmd = args[0];

            if (args.Length > 1)
            {
                arg2 = args[1];
            }
            if (args.Length > 2)
            {
                arg3 = args[2];
            }

            Action<string[]> action = null;

            if (!MethodsTable.TryGetValue(cmd, out action))
            {
                var actionViaReflection = CreateActionFromName(cmd);
                if (actionViaReflection != null)
                {
                    action = arguments => actionViaReflection(arguments.Skip(1).ToArray()); // remove the command, since the downstream method doesn't want it 99% of the time.
                }
            }

            if (action != null)
            {
                Stopwatch sw = Stopwatch.StartNew();

                // 10/29/2015 - dd - These tasks run every long time. Therefore we do not want to log the SQL queries and FileDB performance info.
                string[] excludeLogList = {
                                              "TaskNightly",
                                              "DisclosureNightly",
                                              "TaskTriggerNightly",
                                              "EDocsMigration"
                                          };
                bool isEnableLog = excludeLogList.Contains(cmd, StringComparer.OrdinalIgnoreCase) == false;

                if (isEnableLog)
                {
                    PerformanceMonitorItem.CreateNewForThread();
                }
                Tools.SetupServicePointManager();
                Tools.SetupServicePointCallback(); // Bypass Known test SSL.
                string serviceMonitorLogKey = GetServiceMonitorLogKey(args);

                var threadId = Thread.CurrentThread?.ManagedThreadId ?? 0; // Unlikely for the current thread to be null, but just in case
                var monitorLog = new ServiceExecutionLogging.Logger(serviceMonitorLogKey);

                bool enableServiceMonitorLog = monitorLog.IsImproperlyConfigured() == false;

                var monitorStartLog = new ServiceExecutionLogging.Logger(serviceMonitorLogKey + "::STARTED");
                bool enableServiceMonitorStartLog = monitorStartLog.IsImproperlyConfigured() == false;

                if (enableServiceMonitorStartLog)
                {
                    // 3/14/2016 - dd - For some long processing task, we want to be able to detect if the task did not
                    // start within certain time interval. The monitor framework can only detect anomaly using the StatusType.OK.
                    // Therefore as a work around, we have to record a separate task to indicate the task started.
                    monitorStartLog.Record(ServiceExecutionLogging.Logger.StatusType.OK, $"{ConstAppDavid.ServerName} - Thread ID {threadId} - Started.");
                }

                try
                {
                    if (enableServiceMonitorLog)
                    {
                        monitorLog.Record(ServiceExecutionLogging.Logger.StatusType.STARTED, $"{ConstAppDavid.ServerName} - Thread ID {threadId} - Started.");
                    }

                    action(args);

                    if (enableServiceMonitorLog)
                    {
                        monitorLog.Record(ServiceExecutionLogging.Logger.StatusType.OK, $"{ConstAppDavid.ServerName} - Thread ID {threadId} - Executed in {sw.ElapsedMilliseconds.ToString("0,0")}ms.");
                    }
                }

                catch (Exception exc)
                {
                    string msg = "ScheduleExecutable." + cmd + ":: " + exc.Message + Environment.NewLine + exc.StackTrace;
                    Console.WriteLine(msg);
                    Tools.LogError(msg);

                    if (enableServiceMonitorLog)
                    {
                        monitorLog.Record(ServiceExecutionLogging.Logger.StatusType.FAILED, $"{ConstAppDavid.ServerName} - Thread ID {threadId} - {exc.Message}");
                    }

                    throw;
                }
                finally
                {
                    if (isEnableLog)
                    {
                        PerformanceMonitorItem.LogCurrent("ScheduleExecutable-" + cmd);
                    }
                }

                sw.Stop();
                Tools.LogInfo("Executed " + cmd + " in " + sw.ElapsedMilliseconds + " ms.");
            }
            else
            {
                PrintUsage();
            }

        }

        /// <summary>
        /// Constructions an action that can be performed from the type and method name.
        /// </summary>
        /// <example>Pass <c>"ScheduleExecutable.Program::Main"</c> to receive an action representing the function.</example>
        /// <param name="name">The type and method name, joined by a double colon.</param>
        /// <returns>The action found for the specified name, or null if none was found.</returns>
        public static Action<string[]> CreateActionFromName(string name)
        {
            string[] classAndMethod = name.Split(new[] { "::" }, 2, StringSplitOptions.RemoveEmptyEntries);
            if (classAndMethod.Length == 2)
            {
                var executingAssembly = typeof(Program).Assembly;
                var method = executingAssembly.GetType(classAndMethod[0])?.GetMethod(classAndMethod[1], System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public);
                if (method != null)
                {
                    return (string[] args) => method.Invoke(null, new object[] { args });
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the generic default logger name for service monitor framework. The logger name will be just all the arguments concat by space.
        /// </summary>
        /// <param name="args">A list of user arguments.</param>
        /// <returns>A string of all user arguments.</returns>
        private static string GetServiceMonitorLogKey(string[] args)
        {
            if (null == args)
            {
                return string.Empty;
            }

            return string.Join(" ", args);
        }

        private static void PrintUsage()
        {
            Console.WriteLine("Usage: ScheduleExecutable [action]");
            foreach (var key in MethodsTable.Keys.OrderBy(o => o))
            {
                Console.WriteLine("      [action] - " + key);
            }
        }

        public class OriginatorCompFixClass
        {
            public string OriginatorCompensationPercent { get; set; }
            public string OriginatorCompensationBaseT { get; set; }
            public string OriginatorCompensationMinAmount { get; set; }
            public string OriginatorCompensationMaxAmount { get; set; }
            public string OriginatorCompensationAuditXML { get; set; }
            public string OriginatorCompensationFixedAmount { get; set; }
            public Guid UserId { get; set; }
            public Guid employeeid { get; set; }
            public Guid BrokerId { get; set; }

            public OriginatorCompFixClass(DbDataReader reader)
            {
                OriginatorCompensationPercent = reader["OriginatorCompensationPercent"].ToString();
                OriginatorCompensationBaseT = reader["OriginatorCompensationBaseT"].ToString();
                OriginatorCompensationMinAmount = reader["OriginatorCompensationMinAmount"].ToString();
                OriginatorCompensationMaxAmount = reader["OriginatorCompensationMaxAmount"].ToString();
                OriginatorCompensationAuditXML = reader["OriginatorCompensationAuditXML"].ToString();
                OriginatorCompensationFixedAmount = reader["OriginatorCompensationFixedAmount"].ToString();
                UserId = (Guid)reader["UserId"];
                employeeid = (Guid)reader["employeeid"];
                BrokerId = (Guid)reader["BrokerId"];
            }
        }

		private class NewPermissionGrantingSituation
        {
            public List<Permission> PermissionsToGrant = new List<Permission>();
            public List<Permission> PermissionsToDeny = new List<Permission>();
            public List<Permission> RequiredPermissions = new List<Permission>();
            public List<E_RoleT> RequiredRoles = new List<E_RoleT>();
            public List<E_RoleT> SufficientRoles = new List<E_RoleT>();
            public List<Char> ApplicableUserTypes = new List<Char>();
            public Predicate<BrokerDB> BrokerRequirements = (b) => false;
        }

        #region <User Permissions and Default Broker Permission>

        /// <summary>
        /// Assumes that you've already filtered the list of permissions down to those that apply to the broker.
        /// </summary>
        private static void UpdateEmployeePermissions(Guid brokerID, Guid employeeID, IEnumerable<NewPermissionGrantingSituation> permissionGrantingSituationsForCurrentBroker)
        {
            try
            {
                EmployeeDB empDb = new EmployeeDB(employeeID, brokerID);
                empDb.Retrieve();

                BrokerUserPermissions userPerms = new BrokerUserPermissions(brokerID, employeeID);
                bool updatedPerms = false;
                foreach (var permissionGrantingSituation in permissionGrantingSituationsForCurrentBroker)
                {
                    var userTypes = permissionGrantingSituation.ApplicableUserTypes;

                    if (false == userTypes.Contains(empDb.UserType))
                    {
                        continue;
                    }

                    var employeeRoles = new EmployeeRoles(brokerID, employeeID);


                    if (permissionGrantingSituation.SufficientRoles.Count() > 0)
                    {
                        var hasASufficientRole = false;
                        foreach (var role in permissionGrantingSituation.SufficientRoles)
                        {
                            if (employeeRoles.IsInRole(role))
                            {
                                hasASufficientRole = true;
                                break;
                            }
                        }
                        if (false == hasASufficientRole)
                        {
                            continue;
                        }
                    }
                    else
                    {
                        var doesntHaveRequiredRoles = false;
                        foreach (var role in permissionGrantingSituation.RequiredRoles)
                        {
                            if (false == employeeRoles.IsInRole(role))
                            {
                                doesntHaveRequiredRoles = true;
                                break;
                            }
                        }
                        if (doesntHaveRequiredRoles)
                        {
                            continue;
                        }
                    }

                    bool hasAllRequiredPerms = true;
                    foreach (var requiredPermission in permissionGrantingSituation.RequiredPermissions)
                    {
                        if (false == userPerms.HasPermission(requiredPermission))
                        {
                            hasAllRequiredPerms = false;
                            break;
                        }
                    }
                    if (false == hasAllRequiredPerms)
                    {
                        continue;
                    }

                    foreach (Permission p in permissionGrantingSituation.PermissionsToGrant)
                    {
                        userPerms.SetPermission(p, true);
                    }
                    foreach (Permission p in permissionGrantingSituation.PermissionsToDeny)
                    {
                        userPerms.SetPermission(p, false);
                    }
                    updatedPerms = true;
                }
                if (updatedPerms)
                {
                    userPerms.Update();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Failed to update user permissions of employee id: " + employeeID + ". Check logs.");
                Tools.LogError("Failed to update user permissions of employee id: " + employeeID, e);
            }
        }

        private static void CheckTheSituation(NewPermissionGrantingSituation theSituation)
        {
            if (theSituation.RequiredRoles.Count > 0 && theSituation.SufficientRoles.Count > 0)
            {
                throw new Exception("cannot resolve the situation with both required roles and sufficient roles.");
            }

            if (theSituation.ApplicableUserTypes.Count() == 0)
            {
                throw new Exception("cannot have no applicable user types.");
            }

            if (theSituation.PermissionsToGrant.Count() == 0 && theSituation.PermissionsToDeny.Count() == 0)
            {
                throw new Exception("cannot grant and deny zero permissions.");
            }
        }

        private static void SetUserPermissions(IEnumerable<NewPermissionGrantingSituation> permissionGrantingSituations, HashSet<Guid> brokerFilter, HashSet<Guid> employeeFilter)
        {
            foreach (var situation in permissionGrantingSituations)
            {
                CheckTheSituation(situation);
            }

            var brokerIDs = GetBrokerIds();
            var brokerCount = 0;
            foreach (var brokerID in brokerIDs)
            {
                brokerCount++;
                if (brokerFilter.Count() > 0)
                {
                    if (false == brokerFilter.Contains(brokerID))
                    {
                        continue;
                    }
                }
                Console.WriteLine("starting " + brokerCount + " of " + brokerIDs.Count() + " brokers.");
                SetUserPermissionsForBroker(permissionGrantingSituations, brokerID, employeeFilter);
            }
        }

        private static void SetUserPermissionsForBroker(IEnumerable<NewPermissionGrantingSituation> permissionGrantingSituations, Guid brokerID, HashSet<Guid> employeeFilter)
        {
            BrokerDB broker;
            try
            {
                broker = BrokerDB.RetrieveById(brokerID);
            }
            catch (Exception e)
            {
                Console.WriteLine("Failed migrating broker " + brokerID + " because of retrieval.");
                Tools.LogError("Failed migrating and retrieving broker " + brokerID, e);
                return;
            }
            try
            {
                var permissionGrantingSituationsForCurrentBroker = new List<NewPermissionGrantingSituation>();

                Console.WriteLine("migrating broker " + broker.AsSimpleStringForLog());

                foreach (var permissionGrantingSituation in permissionGrantingSituations)
                {
                    try
                    {
                        if (permissionGrantingSituation.BrokerRequirements(broker))
                        {
                            permissionGrantingSituationsForCurrentBroker.Add(permissionGrantingSituation);
                        }
                        else
                        {
                            Console.WriteLine("skipping permissions " + string.Join(", ", permissionGrantingSituation.PermissionsToGrant.Select(p => p.ToString("g")).ToArray()));
                        }
                    }
                    catch (Exception brokerRequirementError)
                    {
                        string msg = "**skipping permissions due to errored broker " + broker.AsSimpleStringForLog() + " requirement " + string.Join(", ", permissionGrantingSituation.PermissionsToGrant.Select(p => p.ToString("g")).ToArray());
                        Console.WriteLine(msg);
                        Tools.LogError(msg, brokerRequirementError);
                    }
                }

                if (permissionGrantingSituationsForCurrentBroker.Count() == 0)
                {
                    return;
                }

                var sqlParameters = new SqlParameter[]
                {
                    new SqlParameter("@brokerID", brokerID)
                };

                var employeeIDs = new HashSet<Guid>();
                using (var sR = StoredProcedureHelper.ExecuteReader(brokerID, "INTERNAL_USE_ONLY_ListEmployeesInRoleId", sqlParameters))
                {
                    while (sR.Read() == true)
                    {
                        Guid BrokerId = (Guid)sR["BrokerId"];
                        if (brokerID != BrokerId)
                        {
                            Tools.LogError("INTERNAL_USE_ONLY_ListEmployeesInRoleId returned employee with brokerid " + BrokerId + " which doesn't match the brokerID parameter supplied of " + brokerID);
                            continue;
                        }

                        Guid EmployeeId = (Guid)sR["EmployeeId"];

                        if (false == employeeIDs.Contains(EmployeeId))
                        {
                            employeeIDs.Add(EmployeeId);
                        }
                    }
                }

                var userCount = 0;
                foreach (var employeeID in employeeIDs)
                {
                    userCount++;
                    if (userCount % 20 == 1)
                    {
                        Console.WriteLine("considering user " + userCount + " at broker.");
                    }

                    if (employeeFilter.Count() > 0)
                    {
                        if (false == employeeFilter.Contains(employeeID))
                        {
                            continue;
                        }
                    }

                    UpdateEmployeePermissions(brokerID, employeeID, permissionGrantingSituationsForCurrentBroker);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Failed migrating broker " + broker.AsSimpleStringForLog() + ", check logs.");
                Tools.LogError("Failed migrating broker " + broker.AsSimpleStringForLog(), e);
            }
        }

        public static void UpdateDefaultPermission(E_RoleT roleType, bool isOn, params Permission[] permissions)
        {

            Guid roleId = Role.Get(roleType).Id;

            List<Guid> brokerIds = GetBrokerIds();

            BrokerUserPermissions indPerm;

            System.Console.Write("Beginning to set default permissions for role id : " + roleId.ToString());
            foreach (Guid brokerId in brokerIds)
            {
                using (CStoredProcedureExec spEx = new CStoredProcedureExec(brokerId))
                {
                    spEx.BeginTransactionForWrite();
                    try
                    {
                        indPerm = new BrokerUserPermissions();
                        indPerm.RetrieveRolePermissions(brokerId, roleId);
                        foreach (Permission perm in permissions)
                        {
                            indPerm.SetPermission(perm, isOn);
                        }
                        indPerm.UpdateRolePermissions(spEx, brokerId, roleId);
                        spEx.CommitTransaction();
                    }
                    catch (Exception e)
                    {
                        Tools.LogError(e);
                        spEx.RollbackTransaction();
                        throw;
                    }
                }
            }
            System.Console.WriteLine("Default Role permissions have been set for role id : " + roleId.ToString() + " to " + (isOn ? "on." : "off."));
        }
        #endregion

        public static void ExportBrokerEdocs(string[] args)
        {
            System.Diagnostics.Debugger.Launch();
            //stockton lf loans
            EDocBrokerExport brokerExport = new EDocBrokerExport(new Guid("7dfe6d72-ff3d-4c9b-a1d2-132a26d985eb"));
            brokerExport.Export();
        }

        private static void SplitFile(string[] args)
        {
            string file = arg2;
            int fileCount = int.Parse(arg3);

            string[] ss = TextFileHelper.ReadLines(arg2).ToArray();
            int cycle = 1;
            int chunkSize = (int)Math.Ceiling(((double)ss.Length / fileCount));

            var chunk = ss.Take(chunkSize);
            var rem = ss.Skip(chunkSize);

            while (chunk.Take(1).Count() > 0)
            {
                string filename = file + cycle;

                using (StreamWriter sw = new StreamWriter(filename))
                {
                    foreach (string line in chunk)
                    {
                        sw.WriteLine(line);
                    }
                }
                chunk = rem.Take(chunkSize);
                rem = rem.Skip(chunkSize);
                cycle++;
            }
        }

        private static void MigrationLoanFileCache(string[] args)
        {
            // 12/10/2012 dd - Brief instruction
            //
            // Put a list of fields to update in loan file cache in the file 'MigrationLoanFileList.fields.txt'
            //
            // If you are running the cache for first time make sure these two files DO NOT existed
            //      MigrationLoanFileList.txt
            //      MigrationLoanFileList.processed.txt
            //
            // If the process get interrupt, you can just resume the migration and the executable will pick
            // up where it left off.
            //
            // Usage: ScheduleExecutable MigrationLoanFileCache
            //
            Console.WriteLine("Notes: Search 'MigrationLoanFileCache' in PB to check for status.");

            string file_loan_list = "MigrationLoanFileList.txt";
            string file_loan_process_list = "MigrationLoanFileList.processed.txt";
            string file_field_to_migrate = "MigrationLoanFileList.fields.txt";

            List<string> sLIdList = new List<string>();
            List<string> processedList = new List<string>();

            string fieldsMigrate = string.Empty;
            if (!FileOperationHelper.Exists(file_loan_list))
            {
                Console.WriteLine("Build a list of active loan files");
                // 12/10/2012 dd - May want to put the sql in stored procedure. The reason I use sql query
                // is because this is a hotfix.

                StringBuilder sb = new StringBuilder();
                RetrieveLoanFileCache(sLIdList, sb);
                TextFileHelper.WriteString(file_loan_list, sb.ToString(), false);
                FileOperationHelper.Delete(file_loan_process_list);
            }
            else
            {
                Console.WriteLine("Retrieve a list of active loan files");
                sLIdList = TextFileHelper.ReadLines(file_loan_list).ToList();
            }

            if (FileOperationHelper.Exists(file_loan_process_list))
            {
                Console.WriteLine("Load processed loan file list");
                processedList = TextFileHelper.ReadLines(file_loan_process_list).ToList();
            }

            if (FileOperationHelper.Exists(file_field_to_migrate))
            {
                fieldsMigrate = TextFileHelper.ReadFile(file_field_to_migrate);
            }

            int currentCount = 0;
            int totalCount = sLIdList.Count;

            ArrayList dirtyFields = null;
            foreach (string sField in fieldsMigrate.Split(',', ';'))
            {
                if (sField.Trim() != "")
                {
                    if (null == dirtyFields)
                    {
                        dirtyFields = new ArrayList();
                    }
                    dirtyFields.Add(sField.Trim());
                }
            }

            Action<TextFileHelper.LqbTextWriter> writeHandler = delegate(TextFileHelper.LqbTextWriter processedStreamWriter)
            {
                foreach (string id in sLIdList)
                {
                    currentCount++;
                    if (processedList.Contains(id))
                    {
                        continue;
                    }

                    Guid sLId = new Guid(id);

                    Tools.UpdateCacheTable(sLId, dirtyFields);

                    processedStreamWriter.AppendLine(id);
                    if (currentCount % 200 == 0)
                    {
                        processedStreamWriter.Flush();
                        Tools.LogInfo("MigrationLoanFileCache: " + currentCount + " out of " + totalCount);
                    }

                }
            };

            TextFileHelper.OpenForAppend(file_loan_process_list, writeHandler);

            // Clean up
            FileOperationHelper.Delete(file_loan_list);
            FileOperationHelper.Delete(file_loan_process_list);

        }

        private static void RetrieveLoanFileCache(List<string> sLIdList, StringBuilder sb)
        {
            // Unit tested by copying to test library
            string sql = "SELECT c.sLId FROM LOAN_FILE_CACHE c WITH(NOLOCK) JOIN BROKER b WITH(NOLOCK) ON c.sBrokerId=b.BrokerId WHERE c.IsValid=1 AND b.Status=1 order by sopenedd desc";
            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                while (reader.Read())
                {
                    sLIdList.Add(reader["sLId"].ToString());
                    sb.AppendLine(reader["sLId"].ToString());
                }
            };

            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                DBSelectUtility.ProcessDBData(connInfo, sql, null, null, readHandler);
            }
        }

        private static void AddRegisterOpsToWorkflow(string[] args)
        {
            string xml = @"<Condition id=""@Id""><ParameterSet><Parameter name=""AlwaysTrue"" function=""Equal"" type=""Bool""><Value desc=""True"">True</Value></Parameter></ParameterSet><ApplicableSystemOperationSet><SystemOperation>RegisterLoan</SystemOperation></ApplicableSystemOperationSet><Notes><![CDATA[]]></Notes></Condition>";

            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                IConfigRepository repo = ConfigHandler.GetUncachedRepository(connInfo);
                foreach (var config in repo.GetActiveReleases())
                {
                    bool foundNewOp = false;
                    foreach (var p in config.Configuration.Conditions)
                    {
                        foreach (string op in p.SysOpSet.SystemOperationNames)
                        {
                            if (op.Equals(WorkflowOperations.RegisterLoan.Id))
                            {
                                foundNewOp = true;
                                break;
                            }
                        }
                    }

                    if (foundNewOp)
                    {
                        continue;
                    }

                    Guid newId = Guid.NewGuid();
                    string conditionXml = xml.Replace("@Id", newId.ToString());
                    XElement cond = XElement.Parse(conditionXml);
                    config.Configuration.Conditions.AddCondition(new ConfigSystem.Condition(cond));
                    repo.SaveReleaseConfig(config.OrganizationId, config.Configuration, Guid.Empty);
                }
            }
        }

        private static void AddDeleteAppToWorkflow(string[] args)
        {
            string[] deleteAppConfig = new string[] { 
                @"<Condition id=""@Id""><ParameterSet><Parameter name=""Scope"" function=""In"" type=""Int""><Value desc=""In Scope"">1</Value></Parameter><Parameter name=""Role"" function=""In"" type=""Int""><Value desc=""Accountant"">0</Value><Value desc=""Administrator"">1</Value><Value desc=""Call Center Agent"">2</Value><Value desc=""Closer"">3</Value><Value desc=""Consumer"">4</Value><Value desc=""Funder"">5</Value><Value desc=""Lender Account Executive"">6</Value><Value desc=""Loan Officer"">7</Value><Value desc=""Loan Opener"">8</Value><Value desc=""Lock Desk"">9</Value><Value desc=""Manager"">10</Value><Value desc=""Processor"">11</Value><Value desc=""Real Estate Agent"">12</Value><Value desc=""Shipper"">13</Value><Value desc=""Underwriter"">14</Value><Value desc=""Post-Closer"">15</Value><Value desc=""Insuring"">16</Value><Value desc=""Collateral Agent"">17</Value><Value desc=""Doc Drawer"">18</Value></Parameter></ParameterSet><ApplicableSystemOperationSet><SystemOperation>DeleteApplication</SystemOperation></ApplicableSystemOperationSet><Notes><![CDATA[iOPM 125065]]></Notes></Condition>",
                @"<Condition id=""@Id""><ParameterSet><Parameter name=""Role"" function=""In"" type=""Int""><Value desc=""Loan Officer (PML)"">50</Value><Value desc=""Broker Processor (PML)"">51</Value><Value desc=""Administrator (PML)"">52</Value></Parameter><Parameter name=""IsPmlExternalAssigned"" function=""Equal"" type=""Bool""><Value desc=""True"">True</Value></Parameter></ParameterSet><ApplicableSystemOperationSet><SystemOperation>DeleteApplication</SystemOperation></ApplicableSystemOperationSet><Notes><![CDATA[iOPM 125065]]></Notes></Condition>",
                @"<Condition id=""@Id""><ParameterSet><Parameter name=""Role"" function=""In"" type=""Int""><Value desc=""Loan Officer (PML)"">50</Value><Value desc=""Broker Processor (PML)"">51</Value><Value desc=""Administrator (PML)"">52</Value></Parameter><Parameter name=""IsPmlAssigned"" function=""Equal"" type=""Bool""><Value desc=""True"">True</Value></Parameter></ParameterSet><ApplicableSystemOperationSet><SystemOperation>DeleteApplication</SystemOperation></ApplicableSystemOperationSet><Notes><![CDATA[iOPM 125065]]></Notes></Condition>"
            };

            System.Diagnostics.Debugger.Launch();
            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                IConfigRepository repo = ConfigHandler.GetUncachedRepository(connInfo);
                foreach (var config in repo.GetActiveReleases())
                {
                    if (args.Any(p => p.Equals("delete")))
                    {
                        System.Console.WriteLine("Removing any existing ones first.");
                        List<Guid> ids = new List<Guid>();
                        foreach (var p in config.Configuration.Conditions.Where(x => x.SysOpSet.SystemOperationNames.Any(y => y.Equals(WorkflowOperations.DeleteApp.Id))))
                        {
                            ids.Add(p.Id);
                        }

                        foreach (var p in ids)
                        {
                            config.Configuration.Conditions.Remove(p);
                        }
                    }

                    Console.WriteLine("Inspecting " + config.OrganizationId);

                    bool foundNewOp = false;
                    foreach (var p in config.Configuration.Conditions)
                    {
                        foreach (string op in p.SysOpSet.SystemOperationNames)
                        {
                            if (op.Equals(WorkflowOperations.DeleteApp.Id))
                            {
                                foundNewOp = true;
                                break;
                            }
                        }
                    }

                    if (foundNewOp)
                    {
                        Console.WriteLine("Found op in  " + config.OrganizationId);
                        continue;
                    }
                    Console.WriteLine("Adding Op to   " + config.OrganizationId);

                    foreach (string rule in deleteAppConfig)
                    {
                        Guid newId = Guid.NewGuid();
                        string conditionXml = rule.Replace("@Id", newId.ToString());
                        XElement cond = XElement.Parse(conditionXml);
                        config.Configuration.Conditions.AddCondition(new ConfigSystem.Condition(cond));
                    }

                    repo.SaveReleaseConfig(config.OrganizationId, config.Configuration, Guid.Empty);
                }
            }
        }

        private static List<Guid> GetAllLoans(Guid brokerId)
        {

            var loanIdList = new List<Guid>();
            // Should we only migrate valid loans?
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerID", brokerId),
                                            new SqlParameter("@IsTemplate", false),
                                            new SqlParameter("@IsValid", true)
                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "ListLoansByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    loanIdList.Add((Guid)reader["LoanId"]);
                }
            }
            return loanIdList;
        }

        public static void MigrateTaskFromProcessorToUnderwriter(string[] args)
        {
            Thread.CurrentPrincipal = SystemUserPrincipal.TaskSystemUser;

            if (string.IsNullOrEmpty(arg2))
            {
                System.Console.WriteLine("Pass in brokerid.");
                return;
            }

            Guid brokerId = new Guid(arg2);
            Guid processorRoleId = CEmployeeFields.s_ProcessorRoleId;
            Guid underwriterRoleId = CEmployeeFields.s_UnderwriterRoleId;



            Dictionary<Guid, Guid> employeeIdToUserIdMap = new Dictionary<Guid, Guid>();
            List<ConditionCategory> categories = ConditionCategory.GetCategories(brokerId).ToList();


            foreach (Guid loanId in GetAllLoans(brokerId))
            {
                LoanAssignmentContactTable loanAssignmentTable = new LoanAssignmentContactTable(brokerId, loanId);
                var underwriter = loanAssignmentTable.FindByRoleT(E_RoleT.Underwriter);
                var processor = loanAssignmentTable.FindByRoleT(E_RoleT.Processor);
                //underwriter and processor are same so nothing to be done
                if (processor != null && underwriter != null && processor.EmployeeId == underwriter.EmployeeId)
                {
                    Console.WriteLine("Skipping " + loanId);
                    continue;
                }

				bool hasUnderwriterAssigned = underwriter != null;
                bool hasProcessorAssigned = processor != null;

                Guid assignedUnderwriterUserId = Guid.Empty;
                Guid assignedProcessorUserId = Guid.Empty;

				if (hasUnderwriterAssigned)   //get the underwriters user id
				{
					if (false == employeeIdToUserIdMap.TryGetValue(underwriter.EmployeeId, out assignedUnderwriterUserId))
					{
						EmployeeDB db = new EmployeeDB(underwriter.EmployeeId, brokerId);
						db.Retrieve();

						employeeIdToUserIdMap.Add(underwriter.EmployeeId, db.UserID);
						assignedUnderwriterUserId = db.UserID;
					}
				}

				if (hasProcessorAssigned) //get the processors user id
				{
					if (false == employeeIdToUserIdMap.TryGetValue(processor.EmployeeId, out assignedProcessorUserId))
					{
						EmployeeDB db = new EmployeeDB(processor.EmployeeId, brokerId);
						db.Retrieve();

						employeeIdToUserIdMap.Add(processor.EmployeeId, db.UserID);
						assignedProcessorUserId = db.UserID;
					}
				}

				Console.WriteLine("Begin " + loanId + " HasUnderwriter " + hasUnderwriterAssigned.ToString() + " HasProcessor " + hasProcessorAssigned.ToString());

				Dictionary<string, Task> taskList = new Dictionary<string, Task>(StringComparer.CurrentCultureIgnoreCase);
				List<Task> conditionsToCheck = Task.GetActiveConditionsByLoanId(brokerId, loanId, true, categories);
				List<Task> conditionsAffected = new List<Task>();

				foreach (Task task in conditionsToCheck)
				{
					if (task.TaskStatus != E_TaskStatus.Active)
					{
						continue;
					}
					task.EnforcePermissions = false;
					if (task.TaskOwnerUserId == Guid.Empty && task.TaskToBeOwnerRoleId == processorRoleId)
					{
						if (hasUnderwriterAssigned)
						{
							task.TaskOwnerUserId = assignedUnderwriterUserId;
						}
						//reassign task to underwriter role 
						else
						{
							task.TaskToBeOwnerRoleId = underwriterRoleId;
						}
						conditionsAffected.Add(task);
					}
					else if (hasProcessorAssigned && task.TaskOwnerUserId == assignedProcessorUserId)
					{
						if (hasUnderwriterAssigned)
						{
							task.TaskOwnerUserId = assignedUnderwriterUserId;
						}
						else
						{
							task.TaskToBeOwnerRoleId = underwriterRoleId;
						}

						conditionsAffected.Add(task);
					}
				}

				if (conditionsAffected.Count > 0)
				{
					Console.WriteLine("Saving changes to " + conditionsAffected.Count + " conditions");
					for (int x = 0; x < conditionsAffected.Count - 1; x++)
					{
						Task t = conditionsAffected[x];
						t.Save(false);
					}

					conditionsAffected[conditionsAffected.Count - 1].Save(true);
					Console.WriteLine("Finish saving changes to loan " + loanId);
				}
				else
				{
					Console.WriteLine("no Changes made to loan");
				}
			}

		}

        private static void BatchEnableOcPricingEnginePermissionsForPml0176(string[] args)
        {
            var migrator = new BatchEnableOcPricingEnginePermissionsMigrator(
                "BatchEnableOcPricingEnginePermissionsForPml0176");

            migrator.Migrate();
        }

        protected class BatchEnableOcPricingEnginePermissionsMigrator : Migrator<Guid>
        {
            // PML0176
            private readonly Guid brokerId = new Guid("79991CF9-E793-4609-A2EA-E63995947A27");

            public BatchEnableOcPricingEnginePermissionsMigrator(string name)
                : base(name)
            {
                this.MigrationHelper = new CustomGUIDMigrationHelper(name, IDRetrievalFunction);
            }

            protected IEnumerable<Guid> IDRetrievalFunction()
            {
                var pmlBrokerIDs = new LinkedList<Guid>();

                var parameters = new SqlParameter[]
				{
                    new SqlParameter("@BrokerId", this.brokerId)
                };

                using (var reader = StoredProcedureHelper.ExecuteReader(
                    this.brokerId,
                    "ListAllPmlBrokerByBrokerId",
                    parameters))
                {
                    while (reader.Read())
                    {
                        pmlBrokerIDs.AddLast((Guid)reader["PmlBrokerId"]);
                    }
                }

                return pmlBrokerIDs;
            }

            protected override bool MigrationFunction(Guid pmlBrokerId)
            {
                var permissions = PmlBrokerPermission.Retrieve(this.brokerId, pmlBrokerId);

                if (!permissions.CanApplyForIneligibleLoanPrograms ||
                    !permissions.CanRunPricingEngineWithoutCreditReport ||
                    !permissions.CanSubmitWithoutCreditReport)
                {
                    MigrationHelper.WriteToBackup(new
                    {
                        PmlBrokerId = pmlBrokerId,
                        BrokerId = this.brokerId,
                        CanApplyForIneligibleLoanPrograms = permissions.CanApplyForIneligibleLoanPrograms,
                        CanRunPricingEngineWithoutCreditReport = permissions.CanRunPricingEngineWithoutCreditReport,
                        CanSubmitWithoutCreditReport = permissions.CanSubmitWithoutCreditReport
                    });

                    permissions.SetPermission(Permission.CanApplyForIneligibleLoanPrograms, true);
                    permissions.SetPermission(Permission.CanRunPricingEngineWithoutCreditReport, true);
                    permissions.SetPermission(Permission.CanSubmitWithoutCreditReport, true);

                    permissions.Save();
                }

                return true;
            }
        }

        /// <summary>
        /// Reads brokerId and a list of sLId from a file (1 ID per line).
        /// Generates a new LoanId for each loan file using the loan counter (no lead numbering).
        /// </summary>
        /// <param name="args">The arguments.</param>
        public static void OPM243181_GenerateNewLoanNumbers(string[] args)
        {
            using (StreamWriter sw = new StreamWriter("OPM243181_GenerateNewLoanNumbers_" + DateTime.Now.ToString("yyyyMMddHHmm") + ".txt"))
            {
                try
                {
                    string formatErrorMsg = "OPM243181.txt invalid. File must be a broker ID followed by a list of loan IDs, separated by new lines.";

                    // Parse out guids
                    List<Guid> ids = new List<Guid>();
                    foreach (string stringId in TextFileHelper.ReadLines("OPM243181.txt"))
                    {
                        Guid guidId;
                        if (!Guid.TryParse(stringId, out guidId))
                        {
                            throw new CBaseException(formatErrorMsg, formatErrorMsg);
                        }

                        ids.Add(guidId);
                    }

                    if (ids.Count < 2)
                    {
                        throw new CBaseException(formatErrorMsg, formatErrorMsg);
                    }

                    // First ID is broker. Check if valid broker.
                    Guid brokerId = ids[0];
                    try
                    {
                        BrokerDB brokerDb = BrokerDB.RetrieveById(brokerId);
                    }
                    catch (BrokerDBNotFoundException)
                    {
                        sw.WriteLine("Broker ID invalid. First ID in file must be a valid Broker ID. Bad ID = " + brokerId);
                        throw;
                    }

                    sw.WriteLine("Valid BrokerId found. BrokerId = " + brokerId);
                    Console.WriteLine("Valid BrokerId found. BrokerId = " + brokerId);

                    // Go through list of loans and rename them.
                    for (int i = 1; i < ids.Count; i++)
                    {
                        Guid loanId = ids[i];
                        try
                        {
                            CPageData dataLoan = new CFullAccessPageData(loanId, new string[] { "sLNm", "sBranchId" });
                            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                            CLoanFileNamer loanNamer = new CLoanFileNamer();
                            string newLoanName = "";
                            string sMersMin = string.Empty;
                            string sRefNm = string.Empty;
                            do
                            {
                                newLoanName = loanNamer.GetLoanAutoName(brokerId, dataLoan.sBranchId, false, false, false/* enableMersGeneration*/, out sMersMin, out sRefNm);
                            }
                            while (CLoanFileNamer.CheckIfLoanNameExists(brokerId, loanId, newLoanName));

                            dataLoan.SetsLNmWithPermissionBypass(newLoanName);
                            dataLoan.Save();

                            string msg = "Generated new loan number for loan with loanId = " + loanId;
                            sw.WriteLine(msg);
                            Console.WriteLine(msg);
                        }
                        catch (Exception ex)
                        {
                            string msg = "Failed to generate new loan number for loan with loanId = " + loanId;
                            sw.WriteLine(msg);
                            sw.WriteLine(ex.ToString());

                            Console.WriteLine(msg);
                            Console.WriteLine(ex);

                            continue; // Don't fail entire process just because 1 loan fails.
                        }
                    }
                }
                catch (Exception e)
                {
                    sw.WriteLine(e.ToString());
                    Console.WriteLine(e);
                }
            }
        }

        public static void ClearCurrentConsoleLine()
        {
            int currentLineCursor = Console.CursorTop;
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write(new string(' ', Console.WindowWidth));
            Console.SetCursorPosition(0, currentLineCursor);
        }

        public static List<Guid> GetBrokerIds()
        {
            List<Guid> brokerIds = new List<Guid>();

            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {

                using (IDataReader sR = StoredProcedureHelper.ExecuteReader(connInfo, "ListBrokers", null))
                {
                    while (sR.Read())
                    {
                        brokerIds.Add((Guid)sR["BrokerID"]);
                    }
                }
            }

            return brokerIds;
        }

        // Migrators
        protected class DisablePmlUserTaskEmailMigrator : Migrator<Guid>
        {
            protected readonly Guid brokerID;

            public DisablePmlUserTaskEmailMigrator(string name, Guid brokerID)
                : base(name)
            {
                this.brokerID = brokerID;
                this.MigrationHelper = new ActivePmlUsersFromSingleBrokerMigrationHelper(name, brokerID);
            }

            protected override bool MigrationFunction(Guid employeeID)
            {
                var employee = EmployeeDB.RetrieveById(this.brokerID, employeeID);

                // We skip the migration if the employee is not a P-user or if the employee
                // is not populating permissions from their OC and already has task emails
                // disabled. We want to migrate when the user populates from their OC so that 
                // both the value in the database and the value we calculate for the 
                // TaskRelatedEmailOptionT property are synced. 
                if (char.ToLower(employee.UserType) != 'p' ||
                    (employee.PopulatePMLPermissionsT == EmployeePopulationMethodT.IndividualUser &&
                    employee.TaskRelatedEmailOptionT == E_TaskRelatedEmailOptionT.DontReceiveEmail))
                {
                    return false;
                }

                MigrationHelper.WriteToBackup(new
                {
                    EmployeeID = employee.ID,
                    TaskRelatedEmailOptionT = (int)employee.TaskRelatedEmailOptionT
                });

                employee.TaskRelatedEmailOptionT = E_TaskRelatedEmailOptionT.DontReceiveEmail;
                employee.Save();

                return true;
            }
        }

        protected class DisableOriginatingCompanyTaskEmailMigrator : Migrator<Guid>
        {
            protected readonly Guid brokerId;

            public DisableOriginatingCompanyTaskEmailMigrator(string name, Guid brokerId)
                : base(name)
            {
                this.brokerId = brokerId;
                this.MigrationHelper = new CustomGUIDMigrationHelper(name, IDRetrievalFunction);
            }

            protected IEnumerable<Guid> IDRetrievalFunction()
            {
                var pmlBrokerIDs = new List<Guid>();

                var parameters = new SqlParameter[]
                {
                    new SqlParameter("@BrokerId", this.brokerId)
                };

                using (var reader = StoredProcedureHelper.ExecuteReader(
                    this.brokerId,
                    "ListAllPmlBrokerByBrokerId",
                    parameters))
                {
                    while (reader.Read())
                    {
                        pmlBrokerIDs.Add((Guid)reader["PmlBrokerId"]);
                    }
                }

                return pmlBrokerIDs;
            }

			protected override bool MigrationFunction(Guid pmlBrokerId)
			{
				var permissions = PmlBrokerPermission.Retrieve(this.brokerId, pmlBrokerId);

				if (permissions.TaskEmailOption != E_TaskRelatedEmailOptionT.DontReceiveEmail)
				{
					MigrationHelper.WriteToBackup(new
					{
						PmlBrokerId = pmlBrokerId,
						BrokerId = this.brokerId,
						TaskOption = permissions.TaskEmailOption
					});

					permissions.TaskEmailOption = E_TaskRelatedEmailOptionT.DontReceiveEmail;
					permissions.Save();
				}

				return true;
			}
        }

		public class AllowCreatingWholesaleLoansPermissionMigrator : Migrator<Guid>
		{
			Guid brokerID;

			public AllowCreatingWholesaleLoansPermissionMigrator(string name, Guid brokerID)
				: base(name)
			{
				this.brokerID = brokerID;
				this.MigrationHelper = new ActivePmlUsersFromSingleBrokerMigrationHelper(
					name,
					this.brokerID);
			}

			protected override bool MigrationFunction(Guid employeeID)
			{
				var brokerUserPermissions = new BrokerUserPermissions(
					this.brokerID,
					employeeID);

				if (brokerUserPermissions.HasPermission(Permission.AllowCreatingWholesaleChannelLoans))
				{
					return true;
				}

				var backup = new
				{
					BrokerID = this.brokerID,
					EmployeeID = employeeID,
					AllowCreatingWholesaleChannelLoans = brokerUserPermissions.HasPermission(Permission.AllowCreatingWholesaleChannelLoans)
				};

				this.MigrationHelper.WriteToBackup(backup);

				brokerUserPermissions.SetPermission(Permission.AllowCreatingWholesaleChannelLoans);

				brokerUserPermissions.Update();

				return true;
			}
		}
    }

    /// <summary>
    /// Subclasses of the Migrator class will implement a MigrationFunction that performs the actual migration.
    /// So, to create a migration:
    ///     1. Create a subclass of Migrator
    ///     2. Define a MigrationHelper to use in the constructor of the subclass.
    ///         The MigrationHelper determines which GUIDs to use.
    ///         It also abstracts away stopping and resuming migrations.
    ///     3. Override the MigrationFunction.
    ///         The MigrationFunction determines what to do with each item.
    ///         WARNING: the MigrationFunction should be idempotent.
    ///     4. Instantiate your new subclass and call its Migrate() method.
    /// 
    /// Call Migrate() to do the migration.
    /// 
    /// Before you attempt to refactor this to be more functional,
    ///     let me tell you,
    ///     it is a fool's errand.
    /// The SmartDependency CPageData constructor will not allow you to pass around functions willy-nilly.
    /// </summary>
    public abstract class Migrator<T>
    {
        public string Name;
        public MigrationHelper<T> MigrationHelper;

        public Migrator(string name)
        {
            this.Name = name;
        }

        protected virtual void BeginMigration() { }

        protected abstract bool MigrationFunction(T id);

        protected virtual void EndMigration() { }
        public void Migrate()
        {
            this.BeginMigration();
            this.MigrationHelper.Migrate(this.MigrationFunction);
            this.EndMigration();
        }

    }

    /// <summary>
    /// Helps with stopping and resuming migrations. Make sure to append processed loan guids to the processed loan file.
    /// Gets all brokers and loans.
    /// 
    /// This will create several files:
    ///     {name}_all.txt - Contains the list of guids to process
    ///     {name}_processed.txt - Contains the list of guids that have been processed
    ///     
    /// You can create these files manually in order to narrow the number of guids to process.
    /// 
    /// T must be serializable and deserializable.
    /// </summary>
    public class MigrationHelper<T>
    {
        /// <summary>
        /// Called by GetAllIds. This needs to be set to determine where IDs come from.
        /// </summary>
        public Func<IEnumerable<T>> IDRetrievalFunction;
        public Func<string, JavaScriptSerializer, T> ParseItemFunction;
        public Func<T, JavaScriptSerializer, string> SerializeItemFunction;
        public List<T> AllItems;

        public HashSet<T> ProcessedItems;

        public string AllFile;
        public string ProcessedFile;
        public string BackupFile;
        public TextFileHelper.LqbTextWriter BackupFileWriter;

        public string Name;

        protected JavaScriptSerializer JSONSerializer = new JavaScriptSerializer();

        public MigrationHelper(Func<string, T> parseItemFunction, Func<T, string> serializeItemFunction)
        {
            this.ParseItemFunction = (s, serializer) => parseItemFunction(s); // just throw away the serializer in this case, we don't need it
            this.SerializeItemFunction = (item, serializer) => serializeItemFunction(item);
        }

        public MigrationHelper(Func<string, JavaScriptSerializer, T> parseItemFunction, Func<T, JavaScriptSerializer, string> serializeItemFunction)
        {
            this.ParseItemFunction = parseItemFunction;
            this.SerializeItemFunction = serializeItemFunction;
        }

        public MigrationHelper(string name, Func<IEnumerable<T>> idRetrievalFunction, Func<string, T> parseItemFunction, Func<T, string> serializeItemFunction)
            : this(parseItemFunction, serializeItemFunction)
        {
            this.IDRetrievalFunction = idRetrievalFunction;
            this.Init(name);
        }

        /// <summary>
        /// Determines how often we write to the console during the Migrate method.
        /// A ConsoleWritePeriod of N means that we write to the console every N iterations.
        /// </summary>
        public uint ConsoleWritePeriod = 100;

        public IEnumerable<T> GetAllIds()
        {
            return IDRetrievalFunction();
        }

        protected void Init(string name)
        {
            this.Name = name;

            // Create a big list of all active items from active brokers, or grab it from a file if possible
            this.AllFile = name + "_" + "all.txt";
            if (FileOperationHelper.Exists(this.AllFile))
            {
                this.AllItems = new List<T>();
                foreach (string line in TextFileHelper.ReadLines(this.AllFile))
                {
                    try
                    {
                        T item = ParseItemFunction(line, JSONSerializer);
                        this.AllItems.Add(item);
                    }
                    catch (FormatException)
                    {
                        continue; // We didn't finish writing the guid
                    }
                }
            }
            else
            {
                this.AllItems = this.GetAllIds().ToList();
                // Create the file
                Action<TextFileHelper.LqbTextWriter> writeHandler = delegate(TextFileHelper.LqbTextWriter fileWriter)
                {
                    foreach (T item in this.AllItems)
                    {
                        string serializedItem = SerializeItemFunction(item, JSONSerializer);
                        fileWriter.AppendLine(serializedItem);
                    }
                };

                TextFileHelper.OpenForAppend(this.AllFile, writeHandler);
            }

            // A list of loans that we've completed
            this.ProcessedFile = name + "_" + "processed.txt";
            this.ProcessedItems = new HashSet<T>();
            if (FileOperationHelper.Exists(this.ProcessedFile))
            {
                foreach (string line in TextFileHelper.ReadLines(this.ProcessedFile))
                {
                    try
                    {
                        T item = ParseItemFunction(line, JSONSerializer);
                        this.ProcessedItems.Add(item);
                    }
                    catch (FormatException)
                    {
                        continue; // We didn't finish writing the guid, so we'll pretend the operation was unfinished for this GUID
                    }
                }
            }

            // We might want to back up some stuff
            this.BackupFile = name + "_" + "backup.txt";
        }

        public void WriteToBackup(object obj)
        {
            string serialized = JSONSerializer.Serialize(obj);
            try
            {
                BackupFileWriter.AppendLine(serialized);
            }
            catch (NullReferenceException) // Could happen when you call WriteToBackup outside of the Migrate function
            {
                TextFileHelper.AppendString(this.BackupFile, serialized); // If BackupFile can't be opened, we can't do anything
            }
        }

        private long processedContains = 0;
        private long migrate = 0;
        private long processedAdd = 0;
        private long serialize = 0;

        public void Migrate(Func<T, bool> migrationFunction)
        {
            Tools.LogInfo(this.Name + "::Begin");
            Console.WriteLine(this.Name + "::Begin");

            Console.WriteLine(
                    string.Format("{0}::{1} items completed out of {2}",
                        this.Name,
                        this.ProcessedItems.Count.ToString(),
                        this.AllItems.Count.ToString()));
            var start = DateTime.Now;
            int initProcessed = ProcessedItems.Count;

            Action<TextFileHelper.LqbTextWriter> processedHandler = delegate(TextFileHelper.LqbTextWriter processedItemsFileWriter)
            {
                Action<TextFileHelper.LqbTextWriter> backupHandler = delegate(TextFileHelper.LqbTextWriter backupFileWriter)
                {
                    this.BackupFileWriter = backupFileWriter;
                    this.BackupFileWriter.AutoFlush = true;

                    processedItemsFileWriter.AutoFlush = true;

                    var sw = new Stopwatch();
                    foreach (var id in this.AllItems)
                    {
                        // Skip this item if we've already processed it
                        sw.Reset();
                        sw.Start();
                        if (this.ProcessedItems.Contains(id))
                        {
                            continue;
                        }
                        processedContains += sw.ElapsedMilliseconds;

                        // Call the given migration function on the id
                        sw.Reset();
                        sw.Start();
                        migrationFunction(id);
                        migrate += sw.ElapsedMilliseconds;


                        // Mark this item as processed
                        sw.Reset();
                        sw.Start();
                        this.ProcessedItems.Add(id);
                        processedAdd += sw.ElapsedMilliseconds;

                        sw.Reset();
                        sw.Start();
                        string serializedItem = this.SerializeItemFunction(id, JSONSerializer);
                        processedItemsFileWriter.AppendLine(serializedItem);
                        serialize += sw.ElapsedMilliseconds;

                        // Log progress to console -- but not too often
                        if ((this.ProcessedItems.Count % this.ConsoleWritePeriod) == 0)
                        {
                            var runtime = DateTime.Now - start;
                            var perItem = (this.ProcessedItems.Count - initProcessed) / runtime.TotalSeconds;
                            var estimate = DateTime.Now.AddSeconds((1 / perItem) * (AllItems.Count - ProcessedItems.Count));
                            Console.WriteLine(
                                    string.Format("{0}::{1} items completed out of {2}, {3} items per second, {4} estimated time of completion",
                                        this.Name,
                                        this.ProcessedItems.Count.ToString(),
                                        this.AllItems.Count.ToString(),
                                        perItem,
                                        estimate));

                            Console.WriteLine(string.Format(@"
Processed Contains: {0}
           Migrate: {1}
     Processed Add: {2}
         Serialize: {3}", processedContains, migrate, processedAdd, serialize));
                            processedAdd = processedContains = migrate = serialize = 0;
                        }
                    }
                };

                TextFileHelper.OpenForAppend(this.BackupFile, backupHandler);
            };

            TextFileHelper.OpenForAppend(this.ProcessedFile, processedHandler);

            Tools.LogInfo(this.Name + "::End");
            Console.WriteLine(this.Name + "::End");
            return;
        }
    }

    public class StringMigrationHelper : MigrationHelper<string>
    {
        public StringMigrationHelper()
            : base((line) => line, (line) => line)
        {
        }
    }

    public class CustomStringMigrationHelper : StringMigrationHelper
    {
        public CustomStringMigrationHelper(
            string name,
            Func<IEnumerable<string>> IDRetrievalFunction)
        {
            this.IDRetrievalFunction = IDRetrievalFunction;
            this.Init(name);
        }
    }

    public class GUIDMigrationHelper : MigrationHelper<Guid>
    {
        public GUIDMigrationHelper()
            : base(
                (line) => new Guid(line),
                (guid) => guid.ToString()
            ) { }
    }

    public class CustomGUIDMigrationHelper : GUIDMigrationHelper
    {
        public CustomGUIDMigrationHelper(string name, Func<IEnumerable<Guid>> IDRetrievalFunction)
        {
            this.IDRetrievalFunction = IDRetrievalFunction;
            this.Init(name);
        }
    }

    public class LongMigrationHelper : MigrationHelper<long>
    {
        public LongMigrationHelper() : base(
            (line) => long.Parse(line),
            (id) => id.ToString())
        {
        }
    }

    public class CustomLongMigrationHelper : LongMigrationHelper
    {
        public CustomLongMigrationHelper(string name, Func<IEnumerable<long>> idRetrievalFunction)
        {
            this.IDRetrievalFunction = idRetrievalFunction;
            this.Init(name);
        }
    }

    public class IntMigrationHelper : MigrationHelper<int>
    {
        public IntMigrationHelper() :
            base((line) => int.Parse(line),
                (id) => id.ToString())
        {
        }
    }

    public class CustomIntMigrationHelper : IntMigrationHelper
    {
        public CustomIntMigrationHelper(string name, Func<IEnumerable<int>> idRetrievalFunction)
        {
            this.IDRetrievalFunction = idRetrievalFunction;
            this.Init(name);
        }
    }

    public class CustomTupleMigrationHelper<T1, T2> : MigrationHelper<Tuple<T1, T2>>
    {
        public CustomTupleMigrationHelper(
            string name,
            Func<IEnumerable<Tuple<T1, T2>>> idRetrievalFunction,
            Func<string, T1> parseFirstType,
            Func<string, T2> parseSecondType)
            : base((s) => DeserializeTuple(s, parseFirstType, parseSecondType), SerializeTuple)
        {
            this.IDRetrievalFunction = idRetrievalFunction;
            this.Init(name);
        }

        private static string SerializeTuple(Tuple<T1, T2> tuple) => tuple.Item1 + "," + tuple.Item2;

        private static Tuple<T1, T2> DeserializeTuple(string input, Func<string, T1> parseFirstType, Func<string, T2> parseSecondType)
        {
            var parts = input.Split(',');

            var firstItem = parseFirstType(parts[0]);
            var secondItem = parseSecondType(parts[1]);
            return Tuple.Create(firstItem, secondItem);
        }
    }

    public class LoanMigrationHelper : GUIDMigrationHelper
    {
        public LoanMigrationHelper(string name)
        {
            this.IDRetrievalFunction = () => MigrationUtils.GetAllLoans(null, true, false);
            this.Init(name);
        }

        public LoanMigrationHelper(string name, bool? isTemplate, bool? isValid, bool excludeQp)
        {
            this.IDRetrievalFunction = () => MigrationUtils.GetAllLoans(isTemplate, isValid, excludeQp);
            this.Init(name);
        }
    }

    public class BrokerMigrationHelper : GUIDMigrationHelper
    {
        public BrokerMigrationHelper(string name)
        {
            this.IDRetrievalFunction = () => Tools.GetAllActiveBrokers();
            this.Init(name);
        }
    }

    /// <summary>
    /// Unlike <see cref="BrokerMigrationHelper"/>, this one migrates all brokers regardless of if they are active or not.
    /// </summary>
    public class AllBrokerMigrationHelper : GUIDMigrationHelper
    {
        public AllBrokerMigrationHelper(string name)
        {
            this.IDRetrievalFunction = () => MigrationUtils.GetAllActiveAndInactiveBrokerIds();
            this.Init(name);
        }
    }

    public class LoansFromSingleBrokerMigrationHelper : GUIDMigrationHelper
    {
        public Guid BrokerID;
        public LoansFromSingleBrokerMigrationHelper(string name, Guid brokerId)
        {
            this.BrokerID = brokerId;
            this.IDRetrievalFunction = () => MigrationUtils.GetAllLoansUsingGivenBrokers(false, true, false, new Guid[] { this.BrokerID });
            this.Init(name);
        }
    }

    public class LoansAndTemplatesFromSingleBrokerMigrationHelper : GUIDMigrationHelper
    {
        public Guid BrokerID;
        public LoansAndTemplatesFromSingleBrokerMigrationHelper(string name, Guid brokerId)
        {
            this.BrokerID = brokerId;
            this.IDRetrievalFunction = () => MigrationUtils.GetAllLoansUsingGivenBrokers(null, true, false, new Guid[] { this.BrokerID });
            this.Init(name);
        }
    }

    public class TemplatesFromSingleBrokerMigrationHelper : GUIDMigrationHelper
    {
        public Guid BrokerID;
        public TemplatesFromSingleBrokerMigrationHelper(string name, Guid brokerId)
        {
            this.BrokerID = brokerId;
            this.IDRetrievalFunction = () => MigrationUtils.GetAllLoansUsingGivenBrokers(true, true, false, new Guid[] { this.BrokerID });
            this.Init(name);
        }
    }

    public class CCTemplatesFromSingleBrokerMigrationHelper : GUIDMigrationHelper
    {
        public Guid BrokerID;
        public CCTemplatesFromSingleBrokerMigrationHelper(string name, Guid brokerId)
        {
            this.BrokerID = brokerId;
            this.IDRetrievalFunction = () => MigrationUtils.GetAllCCTemplatesUsingGivenBrokers(new Guid[] { this.BrokerID });
            this.Init(name);
        }
    }

    public class ActivePmlUsersFromSingleBrokerMigrationHelper : GUIDMigrationHelper
    {
        public Guid BrokerID;
        public ActivePmlUsersFromSingleBrokerMigrationHelper(string name, Guid brokerId)
        {
            this.BrokerID = brokerId;
            this.IDRetrievalFunction = () => MigrationUtils.GetAllActivePmlUsersUsingGivenBrokers(new Guid[] { this.BrokerID });
            this.Init(name);
        }
    }

    public class LoansFromSingleBranchMigrationHelper : GUIDMigrationHelper
    {
        public Guid BrokerID;
        public Guid BranchID;
        public LoansFromSingleBranchMigrationHelper(string name, Guid brokerId, Guid branchId)
        {
            this.BrokerID = brokerId;
            this.BranchID = branchId;
            this.IDRetrievalFunction = () => MigrationUtils.GetAllLoansUsingGivenBranches(null, true, this.BrokerID, new Guid[] { this.BranchID });
            this.Init(name);
        }
    }

    public class QueriesFromSingleBrokerMigrationHelper : GUIDMigrationHelper
    {
        public Guid BrokerID;
        public QueriesFromSingleBrokerMigrationHelper(string name, Guid brokerID)
        {
            this.BrokerID = brokerID;
            this.IDRetrievalFunction = () => MigrationUtils.GetAllQueryIDsFromBroker(brokerID);
            this.Init(name);
        }
    }

    class LoanMasking
    {
        public static void MaskLoan(Guid sLId)
        {
            // For each loan, we get 3 separate views of the
            // data -- each is needed to save masked updates
            // to the loan.
            CPageData loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(sLId, typeof(LoanMasking));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            // Manipulate the masked fields so that sensitive info
            // is no longer accessible.

            for (int i = 0, n = loan.nApps; i < n; ++i)
            {
                // Get next application.

                CAppData data = loan.GetAppData(i);

                if (data == null)
                {
                    continue;
                }
                #region Mask Borrower Information
                // Update social security numbers.
                data.aBFirstNm = "John";
                data.aBLastNm = "Doe";

                data.aCFirstNm = "Mary";
                data.aCLastNm = "Doe";
                if (data.aBSsn != null && data.aBSsn.Length > 0)
                {
                    data.aBSsn = "123-45-6789";
                }

                if (data.aCSsn != null && data.aCSsn.Length > 0)
                {
                    data.aCSsn = "987-65-4321";
                }

                // Update borrowers' personal info.

                if (data.aBAddr != null && data.aBAddr.Length > 0)
                {
                    data.aBAddr = "1234 Any Street";
                }

                if (data.aBAddrMail != null && data.aBAddrMail.Length > 0)
                {
                    data.aBAddrMail = "1234 Any Street";
                }

                if (data.aBBusPhone != null && data.aBBusPhone.Length > 0)
                {
                    data.aBBusPhone = "(714) 999-9999";
                }

                if (data.aBHPhone != null && data.aBHPhone.Length > 0)
                {
                    data.aBHPhone = "(714) 999-9999";
                }

                if (data.aBCellPhone != null && data.aBCellPhone.Length > 0)
                {
                    data.aBCellPhone = "(714) 999-9999";
                }

                if (data.aBEmail != null && data.aBEmail.Length > 0)
                {
                    data.aBEmail = "any@anywhere.com";
                }

                if (data.aCAddr != null && data.aCAddr.Length > 0)
                {
                    data.aCAddr = "1234 Any Street";
                }

                if (data.aCAddrMail != null && data.aCAddrMail.Length > 0)
                {
                    data.aCAddrMail = "1234 Any Street";
                }

                if (data.aCBusPhone != null && data.aCBusPhone.Length > 0)
                {
                    data.aCBusPhone = "(714) 000-0000";
                }

                if (data.aCHPhone != null && data.aCHPhone.Length > 0)
                {
                    data.aCHPhone = "(714) 000-0000";
                }

                if (data.aCCellPhone != null && data.aCCellPhone.Length > 0)
                {
                    data.aCCellPhone = "(714) 000-0000";
                }

                if (data.aCEmail != null && data.aCEmail.Length > 0)
                {
                    data.aCEmail = "any@anywhere.com";
                }
                #endregion
                #region Mask Employment Record
                // Update individual employment records.

                IEmpCollection bemps = data.aBEmpCollection;

                foreach (IEmploymentRecord emp in bemps.GetSubcollection(true, E_EmpGroupT.All))
                {
                    if (emp.EmplrAddr != null && emp.EmplrAddr.Length > 0)
                    {
                        emp.EmplrAddr = "1234 Any Street";
                    }

                    if (emp.EmplrNm != null && emp.EmplrNm.Length > 0)
                    {
                        emp.EmplrNm = "Company Name";
                    }
                }

                // Update individual employment records.

                IEmpCollection cemps = data.aCEmpCollection;

                foreach (IEmploymentRecord emp in cemps.GetSubcollection(true, E_EmpGroupT.All))
                {
                    if (emp.EmplrAddr != null && emp.EmplrAddr.Length > 0)
                    {
                        emp.EmplrAddr = "1234 Any Street";
                    }

                    if (emp.EmplrNm != null && emp.EmplrNm.Length > 0)
                    {
                        emp.EmplrNm = "Company Name";
                    }
                }
                #endregion
                #region Mask Asset
                IAssetCollection assets = data.aAssetCollection;

                foreach (var item in assets.GetSubcollection(true, E_AssetGroupT.Regular))
                {
                    var asset = (IAssetRegular)item;
                    asset.AccNm = "Account Name";
                    asset.AccNum = "Account ####";

                    if (asset.StAddr != null && asset.StAddr.Length > 0)
                    {
                        asset.StAddr = "1234 Any Street";
                    }

                    if (asset.ComNm != null && asset.ComNm.Length > 0)
                    {
                        asset.ComNm = "Any Company";
                    }
                }
                #endregion
                #region Mask Liabilities
                // Update individual debt records.

                ILiaCollection debts = data.aLiaCollection;

                foreach (ILiabilityRegular debt in debts.GetSubcollection(true, E_DebtGroupT.Regular))
                {
                    debt.AccNm = "Account Name";
                    debt.AccNum = "Account ####";

                    if (debt.ComAddr != null && debt.ComAddr.Length > 0)
                    {
                        debt.ComAddr = "1234 Any Street";
                    }

                    if (debt.ComNm != null && debt.ComNm.Length > 0)
                    {
                        debt.ComNm = "Any Company";
                    }
                }
                #endregion
                #region Mask REO
                // Update individual reo records.

                var reos = data.aReCollection;

                foreach (var item in reos.GetSubcollection(true, E_ReoGroupT.All))
                {
                    var reo = (IRealEstateOwned)item;
                    if (reo.Addr != null && reo.Addr.Length > 0)
                    {
                        reo.Addr = "1234 Any Street";
                    }
                }
                #endregion
            }

            // Update the property addresses to hide the
            // location details.

            if (loan.sSpAddr != null && loan.sSpAddr.Length > 0)
            {
                loan.sSpAddr = "1234 Any Street";
            }




            // Commit this loan to the database.

            loan.Save();
            Thread.Sleep(500);
        }
    }

    public class QuueueStatusInfo
    {
        public int Id { get; set; }
        public int MinuteCutoff { get; set; }
        public string Name { get; set; }
    }

}
