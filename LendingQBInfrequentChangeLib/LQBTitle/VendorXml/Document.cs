// Generated by CodeMonkey on 5/5/2013 1:13:48 PM
using System;
using System.Collections.Generic;
using System.Xml;

namespace LQBTitle.VendorXml
{
    public class Document : AbstractXmlSerializable
    {
        #region Private Member Variables
        private E_DocumentFormat m_Format = E_DocumentFormat.PDF;
        private string m_Name = string.Empty;
        private string m_MismoDocClass = string.Empty;
        private string m_innerText = string.Empty;
        #endregion

        #region Public Properties
        public E_DocumentFormat Format
        {
            get { return m_Format; }
            set { m_Format = value; }
        }
        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }
        public string MismoDocClass
        {
            get { return m_MismoDocClass; }
            set { m_MismoDocClass = value; }
        }
        public string InnerText
        {
            get { return m_innerText; }
            set { m_innerText = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "Document")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_Format = (E_DocumentFormat) ReadAttribute(reader, "Format", EnumMappings.E_DocumentFormat);
            m_Name = ReadAttribute(reader, "Name");
            m_MismoDocClass = ReadAttribute(reader, "MISMODocClass");
            #endregion

            if (reader.IsEmptyElement) return;
            m_innerText = reader.ReadString();
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("Document");
            WriteAttribute(writer, "Format", EnumMappings.E_DocumentFormat[(int) m_Format]);
            WriteAttribute(writer, "Name", m_Name);
            WriteAttribute(writer, "MISMODocClass", m_MismoDocClass);
            WriteString(writer, m_innerText);
            writer.WriteEndElement();
        }
    }

    public enum E_DocumentFormat
    {
        Undefined
        , PDF
    }

    static partial class EnumMappings
    {
        public static readonly string[] E_DocumentFormat = {
                        ""
                         , "PDF"
                         };
    }
}
