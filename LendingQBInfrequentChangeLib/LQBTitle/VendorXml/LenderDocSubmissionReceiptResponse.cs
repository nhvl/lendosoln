// Generated by CodeMonkey on 5/5/2013 1:13:48 PM
using System;
using System.Collections.Generic;
using System.Xml;

namespace LQBTitle.VendorXml
{
    public class LenderDocSubmissionReceiptResponse : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_DocumentName = string.Empty;
        #endregion

        #region Public Properties
        public string DocumentName
        {
            get { return m_DocumentName; }
            set { m_DocumentName = value; }
        }
        #endregion

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "LenderDocSubmissionReceiptResponse")
            {
                reader.Read();
                if (reader.EOF) return;
            }
            #region Read Attributes
            m_DocumentName = ReadAttribute(reader, "DocumentName");
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "LenderDocSubmissionReceiptResponse") return;

            }
        }
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("LenderDocSubmissionReceiptResponse");
            WriteAttribute(writer, "DocumentName", m_DocumentName);
            writer.WriteEndElement();
        }
    }
}
