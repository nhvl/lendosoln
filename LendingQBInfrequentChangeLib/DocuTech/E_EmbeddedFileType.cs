using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocuTech
{
    public enum E_EmbeddedFileType
    {
        Undefined = 0,
        Pdf,
        Xml,
        XHtml
    }
}
