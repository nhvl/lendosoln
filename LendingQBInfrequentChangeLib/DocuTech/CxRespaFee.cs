// Generated by CodeMonkey on 10/25/2010 6:38:29 PM
using System;
using System.Collections.Generic;
using System.Xml;

namespace DocuTech
{
    public sealed class CxRespaFee : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "CX_RESPA_FEE"; }
        }

        public string SpecifiedHudLineNumber;
        public string PremiumYears;
        public string PlusAmount;
        public string FeeDescription;
        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "_SpecifiedHUDLineNumber", out SpecifiedHudLineNumber);
            ReadAttribute(reader, "PremiumYears", out PremiumYears);
            ReadAttribute(reader, "PlusAmount", out PlusAmount);
            ReadAttribute(reader, "FeeDescription", out FeeDescription);
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "_SpecifiedHUDLineNumber", SpecifiedHudLineNumber);
            WriteAttribute(writer, "PremiumYears", PremiumYears);
            WriteAttribute(writer, "PlusAmount", PlusAmount);
            WriteAttribute(writer, "FeeDescription", FeeDescription);
        }
    }
}
