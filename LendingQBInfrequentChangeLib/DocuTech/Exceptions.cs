﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace DocuTech
{
    public sealed class Exceptions : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "EXCEPTIONS"; }
        }

        private List<Key> m_keyList;
        public List<Key> KeyList
        {
            get
            {
                if (m_keyList == null)
                {
                    m_keyList = new List<Key>();
                }
                return m_keyList;
            }
            set { m_keyList = value; }
        }

        private List<GeneralException> m_generalExceptionList;
        public List<GeneralException> GeneralExceptionList
        {
            get
            {
                if (m_generalExceptionList == null)
                {
                    m_generalExceptionList = new List<GeneralException>();
                }
                return m_generalExceptionList;
            }
            set { m_generalExceptionList = value; }
        }
        private List<AuthenticationException> m_authenticationExceptionList;
        public List<AuthenticationException> AuthenticationExceptionList
        {
            get
            {
                if (m_authenticationExceptionList == null)
                {
                    m_authenticationExceptionList = new List<AuthenticationException>();
                }
                return m_authenticationExceptionList;
            }
            set { m_authenticationExceptionList = value; }
        }

        private List<SchemaValidationException> m_schemaValidationExceptionList;
        public List<SchemaValidationException> SchemaValidationExceptionList
        {
            get
            {
                if (m_schemaValidationExceptionList == null)
                {
                    m_schemaValidationExceptionList = new List<SchemaValidationException>();
                }
                return m_schemaValidationExceptionList;
            }
            set { m_schemaValidationExceptionList = value; }
        }
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "KEY": ReadElement(reader, KeyList); break;
                case "GENERAL_EXCEPTION": ReadElement(reader, GeneralExceptionList); break;
                case "AUTHENTICATION_EXCEPTION": ReadElement(reader, AuthenticationExceptionList); break;
                case "SCHEMA_VALIDATION_EXCEPTION": ReadElement(reader, SchemaValidationExceptionList); break;
            }
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, m_keyList);
            WriteElement(writer, m_generalExceptionList);
            WriteElement(writer, m_authenticationExceptionList);
            WriteElement(writer, m_schemaValidationExceptionList);
        }
    }
}
