// Generated by CodeMonkey on 10/22/2010 6:17:54 PM
using System;
using System.Collections.Generic;
using System.Xml;

namespace DocuTech
{
    public sealed class AssociatedDocument : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "_ASSOCIATED_DOCUMENT"; }
        }

        public E_AssociatedDocumentType Type;
        public string TypeOtherDescription;
        public string BookNumber;
        public E_AssociatedDocumentBookType BookType;
        public string BookTypeOtherDescription;
        public string Number;
        public string TitleDescription;
        public string InstrumentNumber;
        public string PageNumber;
        public string VolumeNumber;
        public string RecordingDate;
        public string RecordingJurisdictionName;
        public string CountyOfRecordationName;
        public string OfficeOfRecordationName;
        public string StateOfRecordationName;
        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "_Type", out Type);
            ReadAttribute(reader, "_TypeOtherDescription", out TypeOtherDescription);
            ReadAttribute(reader, "_BookNumber", out BookNumber);
            ReadAttribute(reader, "_BookType", out BookType);
            ReadAttribute(reader, "_BookTypeOtherDescription", out BookTypeOtherDescription);
            ReadAttribute(reader, "_Number", out Number);
            ReadAttribute(reader, "_TitleDescription", out TitleDescription);
            ReadAttribute(reader, "_InstrumentNumber", out InstrumentNumber);
            ReadAttribute(reader, "_PageNumber", out PageNumber);
            ReadAttribute(reader, "_VolumeNumber", out VolumeNumber);
            ReadAttribute(reader, "_RecordingDate", out RecordingDate);
            ReadAttribute(reader, "_RecordingJurisdictionName", out RecordingJurisdictionName);
            ReadAttribute(reader, "_CountyOfRecordationName", out CountyOfRecordationName);
            ReadAttribute(reader, "_OfficeOfRecordationName", out OfficeOfRecordationName);
            ReadAttribute(reader, "_StateOfRecordationName", out StateOfRecordationName);
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "_Type", Type);
            WriteAttribute(writer, "_TypeOtherDescription", TypeOtherDescription);
            WriteAttribute(writer, "_BookNumber", BookNumber);
            WriteAttribute(writer, "_BookType", BookType);
            WriteAttribute(writer, "_BookTypeOtherDescription", BookTypeOtherDescription);
            WriteAttribute(writer, "_Number", Number);
            WriteAttribute(writer, "_TitleDescription", TitleDescription);
            WriteAttribute(writer, "_InstrumentNumber", InstrumentNumber);
            WriteAttribute(writer, "_PageNumber", PageNumber);
            WriteAttribute(writer, "_VolumeNumber", VolumeNumber);
            WriteAttribute(writer, "_RecordingDate", RecordingDate);
            WriteAttribute(writer, "_RecordingJurisdictionName", RecordingJurisdictionName);
            WriteAttribute(writer, "_CountyOfRecordationName", CountyOfRecordationName);
            WriteAttribute(writer, "_OfficeOfRecordationName", OfficeOfRecordationName);
            WriteAttribute(writer, "_StateOfRecordationName", StateOfRecordationName);
        }
    }
}
