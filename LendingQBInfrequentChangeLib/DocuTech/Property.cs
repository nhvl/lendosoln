// Generated by CodeMonkey on 10/22/2010 6:17:53 PM
using System;
using System.Collections.Generic;
using System.Xml;

namespace DocuTech
{
    public sealed class Property : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "PROPERTY"; }
        }

        public string StreetAddress;
        public string StreetAddress2;
        public string City;
        public string State;
        public string PostalCode;
        public string AssessorsParcelIdentifier;
        public string AssessorsSecondParcelIdentifier;
        public E_PropertyBuildingStatusType BuildingStatusType;
        public string FinancedNumberOfUnits;
        public string StructureBuiltYear;
        public string AcquiredDate;
        public E_YesNoIndicator PlannedUnitDevelopmentIndicator;
        public string AcreageNumber;
        public string Country;
        public string County;
        private List<LegalDescription> m_LegalDescriptionList;
        public List<LegalDescription> LegalDescriptionList
        {
            get
            {
                if (null == m_LegalDescriptionList)
                {
                    m_LegalDescriptionList = new List<LegalDescription>();
                }
                return m_LegalDescriptionList;
            }
        }

        private ParsedStreetAddress m_ParsedStreetAddress;
        public ParsedStreetAddress ParsedStreetAddress
        {
            get
            {
                if (null == m_ParsedStreetAddress)
                {
                    m_ParsedStreetAddress = new ParsedStreetAddress();
                }
                return m_ParsedStreetAddress;
            }
            set { m_ParsedStreetAddress = value; } 
        }

        private List<Valuation> m_ValuationList;
        public List<Valuation> ValuationList
        {
            get
            {
                if (null == m_ValuationList)
                {
                    m_ValuationList = new List<Valuation>();
                }
                return m_ValuationList;
            }
        }

        private Details m_Details;
        public Details Details
        {
            get
            {
                if (null == m_Details)
                {
                    m_Details = new Details();
                }
                return m_Details;
            }
            set { m_Details = value; } 
        }

        private HomeownersAssociation m_HomeownersAssociation;
        public HomeownersAssociation HomeownersAssociation
        {
            get
            {
                if (null == m_HomeownersAssociation)
                {
                    m_HomeownersAssociation = new HomeownersAssociation();
                }
                return m_HomeownersAssociation;
            }
            set { m_HomeownersAssociation = value; } 
        }

        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "_StreetAddress", out StreetAddress);
            ReadAttribute(reader, "_StreetAddress2", out StreetAddress2);
            ReadAttribute(reader, "_City", out City);
            ReadAttribute(reader, "_State", out State);
            ReadAttribute(reader, "_PostalCode", out PostalCode);
            ReadAttribute(reader, "AssessorsParcelIdentifier", out AssessorsParcelIdentifier);
            ReadAttribute(reader, "AssessorsSecondParcelIdentifier", out AssessorsSecondParcelIdentifier);
            ReadAttribute(reader, "BuildingStatusType", out BuildingStatusType);
            ReadAttribute(reader, "_FinancedNumberOfUnits", out FinancedNumberOfUnits);
            ReadAttribute(reader, "_StructureBuiltYear", out StructureBuiltYear);
            ReadAttribute(reader, "_AcquiredDate", out AcquiredDate);
            ReadAttribute(reader, "PlannedUnitDevelopmentIndicator", out PlannedUnitDevelopmentIndicator);
            ReadAttribute(reader, "_AcreageNumber", out AcreageNumber);
            ReadAttribute(reader, "_Country", out Country);
            ReadAttribute(reader, "_County", out County);
        }
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "_LEGAL_DESCRIPTION": ReadElement(reader, LegalDescriptionList); break;
                case "PARSED_STREET_ADDRESS": ReadElement(reader, ParsedStreetAddress); break;
                case "_VALUATION": ReadElement(reader, ValuationList); break;
                case "_DETAILS": ReadElement(reader, Details); break;
                case "HOMEOWNERS_ASSOCIATION": ReadElement(reader, HomeownersAssociation); break;
            }
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "_StreetAddress", StreetAddress);
            WriteAttribute(writer, "_StreetAddress2", StreetAddress2);
            WriteAttribute(writer, "_City", City);
            WriteAttribute(writer, "_State", State);
            WriteAttribute(writer, "_PostalCode", PostalCode);
            WriteAttribute(writer, "AssessorsParcelIdentifier", AssessorsParcelIdentifier);
            WriteAttribute(writer, "AssessorsSecondParcelIdentifier", AssessorsSecondParcelIdentifier);
            WriteAttribute(writer, "BuildingStatusType", BuildingStatusType);
            WriteAttribute(writer, "_FinancedNumberOfUnits", FinancedNumberOfUnits);
            WriteAttribute(writer, "_StructureBuiltYear", StructureBuiltYear);
            WriteAttribute(writer, "_AcquiredDate", AcquiredDate);
            WriteAttribute(writer, "PlannedUnitDevelopmentIndicator", PlannedUnitDevelopmentIndicator);
            WriteAttribute(writer, "_AcreageNumber", AcreageNumber);
            WriteAttribute(writer, "_Country", Country);
            WriteAttribute(writer, "_County", County);
            WriteElement(writer, m_LegalDescriptionList);
            WriteElement(writer, m_ParsedStreetAddress);
            WriteElement(writer, m_ValuationList);
            WriteElement(writer, m_Details);
            WriteElement(writer, m_HomeownersAssociation);
        }
    }
}
