// Generated by CodeMonkey on 10/22/2010 6:17:53 PM
using System;
using System.Collections.Generic;
using System.Xml;

namespace DocuTech
{
    public sealed class LoanFeatures : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "LOAN_FEATURES"; }
        }

        public E_YesNoIndicator AssumabilityIndicator;
        public E_YesNoIndicator BalloonIndicator;
        public string BalloonLoanMaturityTermMonths;
        public E_YesNoIndicator BuydownTemporarySubsidyIndicator;
        public E_YesNoIndicator CounselingConfirmationIndicator;
        public E_LoanFeaturesDownPaymentOptionType DownPaymentOptionType;
        public E_YesNoIndicator EscrowWaiverIndicator;
        public string FnmProductPlanIdentifier;
        public string FreOfferingIdentifier;
        public E_LoanFeaturesGseProjectClassificationType GseProjectClassificationType;
        public E_LoanFeaturesGsePropertyType GsePropertyType;
        public string HelocMaximumBalanceAmount;
        public string HelocInitialAdvanceAmount;
        public string InterestOnlyTerm;
        public E_YesNoIndicator LenderSelfInsuredIndicator;
        public E_LoanFeaturesLienPriorityType LienPriorityType;
        public E_LoanFeaturesLoanClosingStatusType LoanClosingStatusType;
        public E_LoanFeaturesLoanDocumentationType LoanDocumentationType;
        public E_LoanFeaturesLoanRepaymentType LoanRepaymentType;
        public string LoanScheduledClosingDate;
        public E_LoanFeaturesMiCertificationStatusType MiCertificationStatusType;
        public E_LoanFeaturesMiCompanyNameType MiCompanyNameType;
        public string MiCoveragePercent;
        public E_LoanFeaturesNameDocumentsDrawnInType NameDocumentsDrawnInType;
        public string NegativeAmortizationLimitPercent;
        public E_LoanFeaturesPaymentFrequencyType PaymentFrequencyType;
        public E_YesNoIndicator PrepaymentPenaltyIndicator;
        public E_LoanFeaturesFullPrepaymentPenaltyOptionType FullPrepaymentPenaltyOptionType;
        public string PrepaymentPenaltyTermMonths;
        public E_YesNoIndicator PrepaymentRestrictionIndicator;
        public string ProductDescription;
        public string ProductName;
        public string ScheduledFirstPaymentDate;
        public E_LoanFeaturesServicingTransferStatusType ServicingTransferStatusType;
        public E_YesNoIndicator ConformingIndicator;
        public E_YesNoIndicator RequiredDepositIndicator;
        public E_YesNoIndicator DemandFeatureIndicator;
        public string EstimatedPrepaidDays;
        public E_LoanFeaturesEstimatedPrepaidDaysPaidByType EstimatedPrepaidDaysPaidByType;
        public string EstimatedPrepaidDaysPaidByOtherTypeDescription;
        public E_YesNoIndicator PrepaymentFinanceChargeRefundableIndicator;
        public string GraduatedPaymentMultiplierFactor;
        public string LoanMaturityDate;
        public string LoanOriginalMaturityTermMonths;
        public string PaymentFrequencyTypeOtherDescription;
        public string OriginalPrincipalAndInterestPaymentAmount;
        public E_YesNoIndicator TimelyPaymentRateReductionIndicator;
        public string TimelyPaymentRateReductionPercent;
        public E_LoanFeaturesCounselingConfirmationType CounselingConfirmationType;
        public string InitialPaymentRatePercent;
        public E_YesNoIndicator RefundableApplicationFeeIndicator;
        public string OriginalBalloonTermMonths;
        public string GrowingEquityLoanPayoffYearsCount;
        public E_YesNoIndicator ConditionsToAssumabilityIndicator;
        private LateCharge m_LateCharge;
        public LateCharge LateCharge
        {
            get
            {
                if (null == m_LateCharge)
                {
                    m_LateCharge = new LateCharge();
                }
                return m_LateCharge;
            }
            set { m_LateCharge = value; } 
        }

        private NotePayTo m_NotePayTo;
        public NotePayTo NotePayTo
        {
            get
            {
                if (null == m_NotePayTo)
                {
                    m_NotePayTo = new NotePayTo();
                }
                return m_NotePayTo;
            }
            set { m_NotePayTo = value; } 
        }

        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "AssumabilityIndicator", out AssumabilityIndicator);
            ReadAttribute(reader, "BalloonIndicator", out BalloonIndicator);
            ReadAttribute(reader, "BalloonLoanMaturityTermMonths", out BalloonLoanMaturityTermMonths);
            ReadAttribute(reader, "BuydownTemporarySubsidyIndicator", out BuydownTemporarySubsidyIndicator);
            ReadAttribute(reader, "CounselingConfirmationIndicator", out CounselingConfirmationIndicator);
            ReadAttribute(reader, "DownPaymentOptionType", out DownPaymentOptionType);
            ReadAttribute(reader, "EscrowWaiverIndicator", out EscrowWaiverIndicator);
            ReadAttribute(reader, "FNMProductPlanIdentifier", out FnmProductPlanIdentifier);
            ReadAttribute(reader, "FREOfferingIdentifier", out FreOfferingIdentifier);
            ReadAttribute(reader, "GSEProjectClassificationType", out GseProjectClassificationType);
            ReadAttribute(reader, "GSEPropertyType", out GsePropertyType);
            ReadAttribute(reader, "HELOCMaximumBalanceAmount", out HelocMaximumBalanceAmount);
            ReadAttribute(reader, "HELOCInitialAdvanceAmount", out HelocInitialAdvanceAmount);
            ReadAttribute(reader, "InterestOnlyTerm", out InterestOnlyTerm);
            ReadAttribute(reader, "LenderSelfInsuredIndicator", out LenderSelfInsuredIndicator);
            ReadAttribute(reader, "LienPriorityType", out LienPriorityType);
            ReadAttribute(reader, "LoanClosingStatusType", out LoanClosingStatusType);
            ReadAttribute(reader, "LoanDocumentationType", out LoanDocumentationType);
            ReadAttribute(reader, "LoanRepaymentType", out LoanRepaymentType);
            ReadAttribute(reader, "LoanScheduledClosingDate", out LoanScheduledClosingDate);
            ReadAttribute(reader, "MICertificationStatusType", out MiCertificationStatusType);
            ReadAttribute(reader, "MICompanyNameType", out MiCompanyNameType);
            ReadAttribute(reader, "MICoveragePercent", out MiCoveragePercent);
            ReadAttribute(reader, "NameDocumentsDrawnInType", out NameDocumentsDrawnInType);
            ReadAttribute(reader, "NegativeAmortizationLimitPercent", out NegativeAmortizationLimitPercent);
            ReadAttribute(reader, "PaymentFrequencyType", out PaymentFrequencyType);
            ReadAttribute(reader, "PrepaymentPenaltyIndicator", out PrepaymentPenaltyIndicator);
            ReadAttribute(reader, "FullPrepaymentPenaltyOptionType", out FullPrepaymentPenaltyOptionType);
            ReadAttribute(reader, "PrepaymentPenaltyTermMonths", out PrepaymentPenaltyTermMonths);
            ReadAttribute(reader, "PrepaymentRestrictionIndicator", out PrepaymentRestrictionIndicator);
            ReadAttribute(reader, "ProductDescription", out ProductDescription);
            ReadAttribute(reader, "ProductName", out ProductName);
            ReadAttribute(reader, "ScheduledFirstPaymentDate", out ScheduledFirstPaymentDate);
            ReadAttribute(reader, "ServicingTransferStatusType", out ServicingTransferStatusType);
            ReadAttribute(reader, "ConformingIndicator", out ConformingIndicator);
            ReadAttribute(reader, "RequiredDepositIndicator", out RequiredDepositIndicator);
            ReadAttribute(reader, "DemandFeatureIndicator", out DemandFeatureIndicator);
            ReadAttribute(reader, "EstimatedPrepaidDays", out EstimatedPrepaidDays);
            ReadAttribute(reader, "EstimatedPrepaidDaysPaidByType", out EstimatedPrepaidDaysPaidByType);
            ReadAttribute(reader, "EstimatedPrepaidDaysPaidByOtherTypeDescription", out EstimatedPrepaidDaysPaidByOtherTypeDescription);
            ReadAttribute(reader, "PrepaymentFinanceChargeRefundableIndicator", out PrepaymentFinanceChargeRefundableIndicator);
            ReadAttribute(reader, "GraduatedPaymentMultiplierFactor", out GraduatedPaymentMultiplierFactor);
            ReadAttribute(reader, "LoanMaturityDate", out LoanMaturityDate);
            ReadAttribute(reader, "LoanOriginalMaturityTermMonths", out LoanOriginalMaturityTermMonths);
            ReadAttribute(reader, "PaymentFrequencyTypeOtherDescription", out PaymentFrequencyTypeOtherDescription);
            ReadAttribute(reader, "OriginalPrincipalAndInterestPaymentAmount", out OriginalPrincipalAndInterestPaymentAmount);
            ReadAttribute(reader, "TimelyPaymentRateReductionIndicator", out TimelyPaymentRateReductionIndicator);
            ReadAttribute(reader, "TimelyPaymentRateReductionPercent", out TimelyPaymentRateReductionPercent);
            ReadAttribute(reader, "CounselingConfirmationType", out CounselingConfirmationType);
            ReadAttribute(reader, "InitialPaymentRatePercent", out InitialPaymentRatePercent);
            ReadAttribute(reader, "RefundableApplicationFeeIndicator", out RefundableApplicationFeeIndicator);
            ReadAttribute(reader, "OriginalBalloonTermMonths", out OriginalBalloonTermMonths);
            ReadAttribute(reader, "GrowingEquityLoanPayoffYearsCount", out GrowingEquityLoanPayoffYearsCount);
            ReadAttribute(reader, "ConditionsToAssumabilityIndicator", out ConditionsToAssumabilityIndicator);
        }
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "LATE_CHARGE": ReadElement(reader, LateCharge); break;
                case "NOTE_PAY_TO": ReadElement(reader, NotePayTo); break;
            }
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "AssumabilityIndicator", AssumabilityIndicator);
            WriteAttribute(writer, "BalloonIndicator", BalloonIndicator);
            WriteAttribute(writer, "BalloonLoanMaturityTermMonths", BalloonLoanMaturityTermMonths);
            WriteAttribute(writer, "BuydownTemporarySubsidyIndicator", BuydownTemporarySubsidyIndicator);
            WriteAttribute(writer, "CounselingConfirmationIndicator", CounselingConfirmationIndicator);
            WriteAttribute(writer, "DownPaymentOptionType", DownPaymentOptionType);
            WriteAttribute(writer, "EscrowWaiverIndicator", EscrowWaiverIndicator);
            WriteAttribute(writer, "FNMProductPlanIdentifier", FnmProductPlanIdentifier);
            WriteAttribute(writer, "FREOfferingIdentifier", FreOfferingIdentifier);
            WriteAttribute(writer, "GSEProjectClassificationType", GseProjectClassificationType);
            WriteAttribute(writer, "GSEPropertyType", GsePropertyType);
            WriteAttribute(writer, "HELOCMaximumBalanceAmount", HelocMaximumBalanceAmount);
            WriteAttribute(writer, "HELOCInitialAdvanceAmount", HelocInitialAdvanceAmount);
            WriteAttribute(writer, "InterestOnlyTerm", InterestOnlyTerm);
            WriteAttribute(writer, "LenderSelfInsuredIndicator", LenderSelfInsuredIndicator);
            WriteAttribute(writer, "LienPriorityType", LienPriorityType);
            WriteAttribute(writer, "LoanClosingStatusType", LoanClosingStatusType);
            WriteAttribute(writer, "LoanDocumentationType", LoanDocumentationType);
            WriteAttribute(writer, "LoanRepaymentType", LoanRepaymentType);
            WriteAttribute(writer, "LoanScheduledClosingDate", LoanScheduledClosingDate);
            WriteAttribute(writer, "MICertificationStatusType", MiCertificationStatusType);
            WriteAttribute(writer, "MICompanyNameType", MiCompanyNameType);
            WriteAttribute(writer, "MICoveragePercent", MiCoveragePercent);
            WriteAttribute(writer, "NameDocumentsDrawnInType", NameDocumentsDrawnInType);
            WriteAttribute(writer, "NegativeAmortizationLimitPercent", NegativeAmortizationLimitPercent);
            WriteAttribute(writer, "PaymentFrequencyType", PaymentFrequencyType);
            WriteAttribute(writer, "PrepaymentPenaltyIndicator", PrepaymentPenaltyIndicator);
            WriteAttribute(writer, "FullPrepaymentPenaltyOptionType", FullPrepaymentPenaltyOptionType);
            WriteAttribute(writer, "PrepaymentPenaltyTermMonths", PrepaymentPenaltyTermMonths);
            WriteAttribute(writer, "PrepaymentRestrictionIndicator", PrepaymentRestrictionIndicator);
            WriteAttribute(writer, "ProductDescription", ProductDescription);
            WriteAttribute(writer, "ProductName", ProductName);
            WriteAttribute(writer, "ScheduledFirstPaymentDate", ScheduledFirstPaymentDate);
            WriteAttribute(writer, "ServicingTransferStatusType", ServicingTransferStatusType);
            WriteAttribute(writer, "ConformingIndicator", ConformingIndicator);
            WriteAttribute(writer, "RequiredDepositIndicator", RequiredDepositIndicator);
            WriteAttribute(writer, "DemandFeatureIndicator", DemandFeatureIndicator);
            WriteAttribute(writer, "EstimatedPrepaidDays", EstimatedPrepaidDays);
            WriteAttribute(writer, "EstimatedPrepaidDaysPaidByType", EstimatedPrepaidDaysPaidByType);
            WriteAttribute(writer, "EstimatedPrepaidDaysPaidByOtherTypeDescription", EstimatedPrepaidDaysPaidByOtherTypeDescription);
            WriteAttribute(writer, "PrepaymentFinanceChargeRefundableIndicator", PrepaymentFinanceChargeRefundableIndicator);
            WriteAttribute(writer, "GraduatedPaymentMultiplierFactor", GraduatedPaymentMultiplierFactor);
            WriteAttribute(writer, "LoanMaturityDate", LoanMaturityDate);
            WriteAttribute(writer, "LoanOriginalMaturityTermMonths", LoanOriginalMaturityTermMonths);
            WriteAttribute(writer, "PaymentFrequencyTypeOtherDescription", PaymentFrequencyTypeOtherDescription);
            WriteAttribute(writer, "OriginalPrincipalAndInterestPaymentAmount", OriginalPrincipalAndInterestPaymentAmount);
            WriteAttribute(writer, "TimelyPaymentRateReductionIndicator", TimelyPaymentRateReductionIndicator);
            WriteAttribute(writer, "TimelyPaymentRateReductionPercent", TimelyPaymentRateReductionPercent);
            WriteAttribute(writer, "CounselingConfirmationType", CounselingConfirmationType);
            WriteAttribute(writer, "InitialPaymentRatePercent", InitialPaymentRatePercent);
            WriteAttribute(writer, "RefundableApplicationFeeIndicator", RefundableApplicationFeeIndicator);
            WriteAttribute(writer, "OriginalBalloonTermMonths", OriginalBalloonTermMonths);
            WriteAttribute(writer, "GrowingEquityLoanPayoffYearsCount", GrowingEquityLoanPayoffYearsCount);
            WriteAttribute(writer, "ConditionsToAssumabilityIndicator", ConditionsToAssumabilityIndicator);
            WriteElement(writer, m_LateCharge);
            WriteElement(writer, m_NotePayTo);
        }
    }
}
