// Generated by CodeMonkey on 10/25/2010 6:38:29 PM
using System;
using System.Collections.Generic;
using System.Xml;

namespace DocuTech
{
    public sealed class CxDocTrigger : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "CX_DOC_TRIGGER"; }
        }

        public E_CxDocTrigger_4506 _4506;
        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "_4506", out _4506);
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "_4506", _4506);
        }
    }
}
