using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocuTech
{
    public class EMortgagePackage : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "EMORTGAGE_PACKAGE"; }
        }

        public string Id;
        public string PostId;
        public string MISMOVersionIdentifier = "1.02";

        private List<Key> m_keyList;
        public List<Key> KeyList
        {
            get
            {
                if (m_keyList == null)
                {
                    m_keyList = new List<Key>();
                }
                return m_keyList;
            }
        }

        private EmbeddedFile m_embeddedFile;
        public EmbeddedFile EmbeddedFile
        {
            get
            {
                if (m_embeddedFile == null)
                {
                    m_embeddedFile = new EmbeddedFile();
                }
                return m_embeddedFile;
            }
            set { m_embeddedFile = value; } 
        }

        private SmartDocument m_smartDocument;
        public SmartDocument SmartDocument
        {
            get
            {
                if (m_smartDocument == null)
                {
                    m_smartDocument = new SmartDocument();
                }
                return m_smartDocument;
            }
            set { m_smartDocument = value; } 
        }


        protected override void ReadElement(System.Xml.XmlReader reader)
        {
            switch (reader.Name)
            {
                case "KEY": ReadElement(reader, KeyList); break;
                case "EMBEDDED_FILE": ReadElement(reader, EmbeddedFile); break;
                case "SMART_DOCUMENT": ReadElement(reader, SmartDocument); break;
            }
        }

        protected override void ReadAttributes(System.Xml.XmlReader reader)
        {
            ReadAttribute(reader, "_ID", out Id);
            ReadAttribute(reader, "_PostID", out PostId);
            ReadAttribute(reader, "MISMOVersionIdentifier", out MISMOVersionIdentifier);
        }

        protected override void WriteXmlImpl(System.Xml.XmlWriter writer)
        {
            WriteAttribute(writer, "_ID", Id);
            WriteAttribute(writer, "_PostID", PostId);
            WriteAttribute(writer, "MISMOVersionIdentifier", MISMOVersionIdentifier);
            WriteElement(writer, m_keyList);
            WriteElement(writer, m_embeddedFile);
            WriteElement(writer, m_smartDocument);
        }

    }
}
