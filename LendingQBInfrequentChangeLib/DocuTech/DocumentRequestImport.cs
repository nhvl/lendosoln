using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace DocuTech
{
    public sealed class DocumentRequestImport : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "DOCUMENT_REQUEST_IMPORT"; }
        }
        public string Version = "2.0";
        public string PopulatingSystemName;
        public string PopulatingSystemVersion;
        public string PopulatingSystemLightsOn;

        private User m_user;
        public User User
        {
            get
            {
                if (m_user == null)
                {
                    m_user = new User();
                }
                return m_user;
            }
            set { m_user = value; } 
        }
        private EMortgagePackage m_emortgagePackage;
        public EMortgagePackage EMortgagePackage
        {
            get
            {
                if (m_emortgagePackage == null)
                {
                    m_emortgagePackage = new EMortgagePackage();
                }
                return m_emortgagePackage;
            }
            set { m_emortgagePackage = value; } 
        }

        private RequestOptions m_requestOptions;
        public RequestOptions RequestOptions
        {
            get
            {
                if (m_requestOptions == null)
                {
                    m_requestOptions = new RequestOptions();
                }
                return m_requestOptions;
            }
            set { m_requestOptions = value; } 
        }
        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "_Version", out Version);
            ReadAttribute(reader, "_PopulatingSystemName", out PopulatingSystemName);
            ReadAttribute(reader, "_PopulatingSystemVersion", out PopulatingSystemVersion);
            ReadAttribute(reader, "_PopulatingSystemLightsOn", out PopulatingSystemLightsOn);
        }
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "USER": ReadElement(reader, User); break;
                case "EMORTGAGE_PACKAGE": ReadElement(reader, EMortgagePackage); break;
                case "REQUEST_OPTIONS": ReadElement(reader, RequestOptions); break;
            }
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {

            WriteAttribute(writer, "_Version", Version);
            WriteAttribute(writer, "_PopulatingSystemName", PopulatingSystemName);
            WriteAttribute(writer, "_PopulatingSystemVersion", PopulatingSystemVersion);
            WriteAttribute(writer, "_PopulatingSystemLightsOn", PopulatingSystemLightsOn);

            WriteElement(writer, m_user);
            WriteElement(writer, m_emortgagePackage);
            WriteElement(writer, m_requestOptions);
        }

    }
}
