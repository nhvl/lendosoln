// Generated by CodeMonkey on 10/25/2010 6:38:28 PM
namespace DocuTech
{
    public enum E_CxLoanHelocProgram
    {
        Undefined = 0,
        AmericanSterlingHeloc,
        BarringtonHeloc,
        _Cal_State_9_HELOC,
        CalBayHeloc,
        ChaseHeloc,
        CountrywideFlexSaverHeloc,
        CountrywideHeloc,
        CountrywidePreferredHeloc,
        CountrywideSubprimeHeloc,
        CreditSuisseHeloc,
        CunaMutualMortgageHeloc,
        ETradeBankHeloc,
        FirstAdvantageMortgageHeloc,
        _FirstMac_C_X__First_Lien,
        _FirstMac_C_X__Second_Lien,
        FirstMacXHeloc,
        FirstMacXPlusHeloc,
        GMACHeloc,
        GreenPointHeloc,
        HorizonCreditUnionHeloc,
        ImpacHeloc,
        IndymacHeloc,
        KaipermFederalCreditUnionHeloc,
        LimeHeloc,
        MITLending_AND_MortgageITHeloc,
        RFCHeloc,
        SovereignHeloc,
        TranslandHeloc,
        VenturaFederalCountyCreditUnionHeloc,
        WellsFargoHeloc,
        WinterGroupHeloc,
    }
}
