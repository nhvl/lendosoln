using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace DocuTech
{
    public sealed class UserData : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "USER_DATA"; }
        }

        private List<Data> m_dataList;

        public List<Data> DataList
        {
            get
            {
                if (null == m_dataList)
                {
                    m_dataList = new List<Data>();
                }
                return m_dataList;
            }
        }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "DATA": ReadElement(reader, DataList); break;
            }
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, m_dataList);
        }
    }
}
