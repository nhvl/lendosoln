using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace DocuTech
{
    public sealed class RequestOptions : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "REQUEST_OPTIONS"; }
        }

        public string IncludeXmlSignatureMetaFile;

        private List<Documents> m_documentsList;
        public List<Documents> DocumentsList
        {
            get
            {
                if (m_documentsList == null)
                {
                    m_documentsList = new List<Documents>();
                }
                return m_documentsList;
            }
        }

        private List<DocumentFormat> m_documentFormatList;
        public List<DocumentFormat> DocumentFormatList
        {
            get
            {
                if (m_documentFormatList == null)
                {
                    m_documentFormatList = new List<DocumentFormat>();
                }
                return m_documentFormatList;
            }
        }

        private DeliveryFormat m_deliveryFormat;
        public DeliveryFormat DeliveryFormat
        {
            get
            {
                if (m_deliveryFormat == null)
                {
                    m_deliveryFormat = new DeliveryFormat();
                }
                return m_deliveryFormat;
            }
            set { m_deliveryFormat = value; } 
        }

        private DataChecks m_dataChecks;
        public DataChecks DataChecks
        {
            get
            {
                if (m_dataChecks == null)
                {
                    m_dataChecks = new DataChecks();
                }
                return m_dataChecks;
            }
            set { m_dataChecks = value; } 
        }


        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "_IncludeXmlSignatureMetaFile", out IncludeXmlSignatureMetaFile);
        }
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "DOCUMENTS": ReadElement(reader, DocumentsList); break;
                case "DELIVERY_FORMAT": ReadElement(reader, DeliveryFormat); break;
                case "DOCUMENT_FORMAT": ReadElement(reader, DocumentFormatList); break;
                case "DATA_CHECKS": ReadElement(reader, DataChecks); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "_IncludeXmlSignatureMetaFile", IncludeXmlSignatureMetaFile);
            WriteElement(writer, m_documentsList);
            WriteElement(writer, m_deliveryFormat);
            WriteElement(writer, m_documentFormatList);
            WriteElement(writer, m_dataChecks);
        }
    }
}
