// Generated by CodeMonkey on 10/25/2010 6:38:29 PM
using System;
using System.Collections.Generic;
using System.Xml;

namespace DocuTech
{
    public sealed class CxNonPurchasingSpouse : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "CX_NON_PURCHASING_SPOUSE"; }
        }

        public string UnparsedName;
        public string FirstName;
        public string MiddleName;
        public string LastName;
        public string Suffix;
        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "_UnparsedName", out UnparsedName);
            ReadAttribute(reader, "_FirstName", out FirstName);
            ReadAttribute(reader, "_MiddleName", out MiddleName);
            ReadAttribute(reader, "_LastName", out LastName);
            ReadAttribute(reader, "_Suffix", out Suffix);
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "_UnparsedName", UnparsedName);
            WriteAttribute(writer, "_FirstName", FirstName);
            WriteAttribute(writer, "_MiddleName", MiddleName);
            WriteAttribute(writer, "_LastName", LastName);
            WriteAttribute(writer, "_Suffix", Suffix);
        }
    }
}
