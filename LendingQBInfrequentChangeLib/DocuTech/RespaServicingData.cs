// Generated by CodeMonkey on 10/22/2010 6:17:54 PM
using System;
using System.Collections.Generic;
using System.Xml;

namespace DocuTech
{
    public sealed class RespaServicingData : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "RESPA_SERVICING_DATA"; }
        }

        public E_YesNoIndicator AreAbleToServiceIndicator;
        public E_YesNoIndicator AssignSellOrTransferSomeServicingIndicator;
        public E_YesNoIndicator DoNotServiceIndicator;
        public E_YesNoIndicator ExpectToAssignSellOrTransferPercentOfServicingIndicator;
        public string ExpectToAssignSellOrTransferPercent;
        public E_YesNoIndicator ExpectToRetainAllServicingIndicator;
        public E_YesNoIndicator ExpectToSellAllServicingIndicator;
        public string FirstTransferYear;
        public string FirstTransferYearValue;
        public E_YesNoIndicator HaveNotDecidedToServiceIndicator;
        public E_YesNoIndicator HaveNotServicedInPastThreeYearsIndicator;
        public E_YesNoIndicator HavePreviouslyAssignedServicingIndicator;
        public E_YesNoIndicator MayAssignServicingIndicator;
        public E_YesNoIndicator PresentlyIntendToAssignSellOrTransferServicingIndicator;
        public string SecondTransferYear;
        public string SecondTransferYearValue;
        public string ThirdTransferYear;
        public string ThirdTransferYearValue;
        public E_YesNoIndicator ThisEstimateIncludesAssignmentsSalesOrTransfersIndicator;
        public E_YesNoIndicator ThisInformationDoesIncludeAssignmentsSalesOrTransfersIndicator;
        public E_YesNoIndicator ThisIsOurRecordOfTransferringServicingIndicator;
        public string TwelveMonthPeriodTransferPercent;
        public E_YesNoIndicator WillNotServiceIndicator;
        public E_YesNoIndicator WillServiceIndicator;
        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "_AreAbleToServiceIndicator", out AreAbleToServiceIndicator);
            ReadAttribute(reader, "_AssignSellOrTransferSomeServicingIndicator", out AssignSellOrTransferSomeServicingIndicator);
            ReadAttribute(reader, "_DoNotServiceIndicator", out DoNotServiceIndicator);
            ReadAttribute(reader, "_ExpectToAssignSellOrTransferPercentOfServicingIndicator", out ExpectToAssignSellOrTransferPercentOfServicingIndicator);
            ReadAttribute(reader, "_ExpectToAssignSellOrTransferPercent", out ExpectToAssignSellOrTransferPercent);
            ReadAttribute(reader, "_ExpectToRetainAllServicingIndicator", out ExpectToRetainAllServicingIndicator);
            ReadAttribute(reader, "_ExpectToSellAllServicingIndicator", out ExpectToSellAllServicingIndicator);
            ReadAttribute(reader, "_FirstTransferYear", out FirstTransferYear);
            ReadAttribute(reader, "_FirstTransferYearValue", out FirstTransferYearValue);
            ReadAttribute(reader, "_HaveNotDecidedToServiceIndicator", out HaveNotDecidedToServiceIndicator);
            ReadAttribute(reader, "_HaveNotServicedInPastThreeYearsIndicator", out HaveNotServicedInPastThreeYearsIndicator);
            ReadAttribute(reader, "_HavePreviouslyAssignedServicingIndicator", out HavePreviouslyAssignedServicingIndicator);
            ReadAttribute(reader, "_MayAssignServicingIndicator", out MayAssignServicingIndicator);
            ReadAttribute(reader, "_PresentlyIntendToAssignSellOrTransferServicingIndicator", out PresentlyIntendToAssignSellOrTransferServicingIndicator);
            ReadAttribute(reader, "_SecondTransferYear", out SecondTransferYear);
            ReadAttribute(reader, "_SecondTransferYearValue", out SecondTransferYearValue);
            ReadAttribute(reader, "_ThirdTransferYear", out ThirdTransferYear);
            ReadAttribute(reader, "_ThirdTransferYearValue", out ThirdTransferYearValue);
            ReadAttribute(reader, "_ThisEstimateIncludesAssignmentsSalesOrTransfersIndicator", out ThisEstimateIncludesAssignmentsSalesOrTransfersIndicator);
            ReadAttribute(reader, "_ThisInformationDoesIncludeAssignmentsSalesOrTransfersIndicator", out ThisInformationDoesIncludeAssignmentsSalesOrTransfersIndicator);
            ReadAttribute(reader, "_ThisIsOurRecordOfTransferringServicingIndicator", out ThisIsOurRecordOfTransferringServicingIndicator);
            ReadAttribute(reader, "_TwelveMonthPeriodTransferPercent", out TwelveMonthPeriodTransferPercent);
            ReadAttribute(reader, "_WillNotServiceIndicator", out WillNotServiceIndicator);
            ReadAttribute(reader, "_WillServiceIndicator", out WillServiceIndicator);
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "_AreAbleToServiceIndicator", AreAbleToServiceIndicator);
            WriteAttribute(writer, "_AssignSellOrTransferSomeServicingIndicator", AssignSellOrTransferSomeServicingIndicator);
            WriteAttribute(writer, "_DoNotServiceIndicator", DoNotServiceIndicator);
            WriteAttribute(writer, "_ExpectToAssignSellOrTransferPercentOfServicingIndicator", ExpectToAssignSellOrTransferPercentOfServicingIndicator);
            WriteAttribute(writer, "_ExpectToAssignSellOrTransferPercent", ExpectToAssignSellOrTransferPercent);
            WriteAttribute(writer, "_ExpectToRetainAllServicingIndicator", ExpectToRetainAllServicingIndicator);
            WriteAttribute(writer, "_ExpectToSellAllServicingIndicator", ExpectToSellAllServicingIndicator);
            WriteAttribute(writer, "_FirstTransferYear", FirstTransferYear);
            WriteAttribute(writer, "_FirstTransferYearValue", FirstTransferYearValue);
            WriteAttribute(writer, "_HaveNotDecidedToServiceIndicator", HaveNotDecidedToServiceIndicator);
            WriteAttribute(writer, "_HaveNotServicedInPastThreeYearsIndicator", HaveNotServicedInPastThreeYearsIndicator);
            WriteAttribute(writer, "_HavePreviouslyAssignedServicingIndicator", HavePreviouslyAssignedServicingIndicator);
            WriteAttribute(writer, "_MayAssignServicingIndicator", MayAssignServicingIndicator);
            WriteAttribute(writer, "_PresentlyIntendToAssignSellOrTransferServicingIndicator", PresentlyIntendToAssignSellOrTransferServicingIndicator);
            WriteAttribute(writer, "_SecondTransferYear", SecondTransferYear);
            WriteAttribute(writer, "_SecondTransferYearValue", SecondTransferYearValue);
            WriteAttribute(writer, "_ThirdTransferYear", ThirdTransferYear);
            WriteAttribute(writer, "_ThirdTransferYearValue", ThirdTransferYearValue);
            WriteAttribute(writer, "_ThisEstimateIncludesAssignmentsSalesOrTransfersIndicator", ThisEstimateIncludesAssignmentsSalesOrTransfersIndicator);
            WriteAttribute(writer, "_ThisInformationDoesIncludeAssignmentsSalesOrTransfersIndicator", ThisInformationDoesIncludeAssignmentsSalesOrTransfersIndicator);
            WriteAttribute(writer, "_ThisIsOurRecordOfTransferringServicingIndicator", ThisIsOurRecordOfTransferringServicingIndicator);
            WriteAttribute(writer, "_TwelveMonthPeriodTransferPercent", TwelveMonthPeriodTransferPercent);
            WriteAttribute(writer, "_WillNotServiceIndicator", WillNotServiceIndicator);
            WriteAttribute(writer, "_WillServiceIndicator", WillServiceIndicator);
        }
    }
}
