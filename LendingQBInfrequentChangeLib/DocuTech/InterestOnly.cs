// Generated by CodeMonkey on 10/22/2010 6:17:53 PM
using System;
using System.Collections.Generic;
using System.Xml;

namespace DocuTech
{
    public sealed class InterestOnly : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "INTEREST_ONLY"; }
        }

        public string MonthlyPaymentAmount;
        public string TermMonthsCount;
        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "_MonthlyPaymentAmount", out MonthlyPaymentAmount);
            ReadAttribute(reader, "_TermMonthsCount", out TermMonthsCount);
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "_MonthlyPaymentAmount", MonthlyPaymentAmount);
            WriteAttribute(writer, "_TermMonthsCount", TermMonthsCount);
        }
    }
}
