using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace DocuTech
{
    public sealed class SmartDocumentDataMain : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "MAIN"; }
        }

        private Loan m_loan;
        public Loan Loan
        {
            get
            {
                if (m_loan == null)
                {
                    m_loan = new Loan();
                }
                return m_loan;
            }
            set { m_loan = value; } 
        }
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "LOAN": ReadElement(reader, Loan); break;
            }
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, m_loan);
        }
    }
}
