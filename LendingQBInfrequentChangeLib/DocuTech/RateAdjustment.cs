// Generated by CodeMonkey on 10/22/2010 6:17:53 PM
using System;
using System.Collections.Generic;
using System.Xml;

namespace DocuTech
{
    public sealed class RateAdjustment : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "RATE_ADJUSTMENT"; }
        }

        public string FirstRateAdjustmentMonths;
        public E_RateAdjustmentCalculationType CalculationType;
        public string DurationMonths;
        public string Percent;
        public string PeriodNumber;
        public string SubsequentCapPercent;
        public string SubsequentRateAdjustmentMonths;
        public string FirstChangeCapRate;
        public string FirstChangeFloorPercent;
        public string FirstChangeFloorRate;
        public string FirstRateAdjustmentDate;
        public string InitialCapPercent;
        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "FirstRateAdjustmentMonths", out FirstRateAdjustmentMonths);
            ReadAttribute(reader, "_CalculationType", out CalculationType);
            ReadAttribute(reader, "_DurationMonths", out DurationMonths);
            ReadAttribute(reader, "_Percent", out Percent);
            ReadAttribute(reader, "_PeriodNumber", out PeriodNumber);
            ReadAttribute(reader, "_SubsequentCapPercent", out SubsequentCapPercent);
            ReadAttribute(reader, "SubsequentRateAdjustmentMonths", out SubsequentRateAdjustmentMonths);
            ReadAttribute(reader, "_FirstChangeCapRate", out FirstChangeCapRate);
            ReadAttribute(reader, "_FirstChangeFloorPercent", out FirstChangeFloorPercent);
            ReadAttribute(reader, "_FirstChangeFloorRate", out FirstChangeFloorRate);
            ReadAttribute(reader, "FirstRateAdjustmentDate", out FirstRateAdjustmentDate);
            ReadAttribute(reader, "_InitialCapPercent", out InitialCapPercent);
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "FirstRateAdjustmentMonths", FirstRateAdjustmentMonths);
            WriteAttribute(writer, "_CalculationType", CalculationType);
            WriteAttribute(writer, "_DurationMonths", DurationMonths);
            WriteAttribute(writer, "_Percent", Percent);
            WriteAttribute(writer, "_PeriodNumber", PeriodNumber);
            WriteAttribute(writer, "_SubsequentCapPercent", SubsequentCapPercent);
            WriteAttribute(writer, "SubsequentRateAdjustmentMonths", SubsequentRateAdjustmentMonths);
            WriteAttribute(writer, "_FirstChangeCapRate", FirstChangeCapRate);
            WriteAttribute(writer, "_FirstChangeFloorPercent", FirstChangeFloorPercent);
            WriteAttribute(writer, "_FirstChangeFloorRate", FirstChangeFloorRate);
            WriteAttribute(writer, "FirstRateAdjustmentDate", FirstRateAdjustmentDate);
            WriteAttribute(writer, "_InitialCapPercent", InitialCapPercent);
        }
    }
}
