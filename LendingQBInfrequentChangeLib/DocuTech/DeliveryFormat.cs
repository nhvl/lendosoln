using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace DocuTech
{
    public sealed class DeliveryFormat : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "DELIVERY_FORMAT"; }
        }

        public E_DeliveryFormatBundlingOptions BundlingOption = E_DeliveryFormatBundlingOptions.Individual;
        public E_DeliveryFormatCompressionType CompressionType;

        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "_BundlingOption", out BundlingOption);
            ReadAttribute(reader, "_CompressionType", out CompressionType);
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "_BundlingOption", BundlingOption);
            WriteAttribute(writer, "_CompressionType", CompressionType);
        }
    }
}
