using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocuTech
{
    public enum E_TransmittalDataDtcMortgageOriginatorType
    {
        Undefined = 0,
        Seller,
        Broker,
        Correspondent
    }
}
