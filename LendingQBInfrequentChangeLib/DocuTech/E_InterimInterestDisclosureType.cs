﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocuTech
{
    public enum E_InterimInterestDisclosureType
    {
        Undefined = 0,
        GFE = 1,
        HUD1 = 2,
        Other = 3,
    }
}
