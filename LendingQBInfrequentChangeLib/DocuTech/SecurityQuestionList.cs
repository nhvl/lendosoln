using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace DocuTech
{
    public sealed class SecurityQuestionList : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "SECURITY_QUESTION_LIST"; }
        }
        private List<Question> m_questionList;
        public List<Question> QuestionList
        {
            get
            {
                if (m_questionList == null)
                {
                    m_questionList = new List<Question>();
                }
                return m_questionList;
            }
        }
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "QUESTION": ReadElement(reader, QuestionList); break;
            }
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, m_questionList);
        }
    }
}
