﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace DocuTech
{
    public sealed class SchemaValidationException : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "SCHEMA_VALIDATION_EXCEPTION"; }
        }

        public string Id;
        public string Description;
        public string Violation;

        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "_ID", out Id);
            ReadAttribute(reader, "_Description", out Description);
            ReadAttribute(reader, "_Violation", out Violation);
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "_ID", Id);
            WriteAttribute(writer, "_Description", Description);
            WriteAttribute(writer, "_Violation", Violation);
        }
    }
}
