// Generated by CodeMonkey on 10/22/2010 6:17:53 PM
using System;
using System.Collections.Generic;
using System.Xml;

namespace DocuTech
{
    public sealed class InterviewerInformation : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "INTERVIEWER_INFORMATION"; }
        }

        public string InterviewersEmployerStreetAddress;
        public string InterviewersEmployerCity;
        public string InterviewersEmployerState;
        public string InterviewersEmployerPostalCode;
        public string InterviewersTelephoneNumber;
        public E_InterviewerInformationApplicationTakenMethodType ApplicationTakenMethodType;
        public string InterviewerApplicationSignedDate;
        public string InterviewersEmployerName;
        public string InterviewersName;
        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "InterviewersEmployerStreetAddress", out InterviewersEmployerStreetAddress);
            ReadAttribute(reader, "InterviewersEmployerCity", out InterviewersEmployerCity);
            ReadAttribute(reader, "InterviewersEmployerState", out InterviewersEmployerState);
            ReadAttribute(reader, "InterviewersEmployerPostalCode", out InterviewersEmployerPostalCode);
            ReadAttribute(reader, "InterviewersTelephoneNumber", out InterviewersTelephoneNumber);
            ReadAttribute(reader, "ApplicationTakenMethodType", out ApplicationTakenMethodType);
            ReadAttribute(reader, "InterviewerApplicationSignedDate", out InterviewerApplicationSignedDate);
            ReadAttribute(reader, "InterviewersEmployerName", out InterviewersEmployerName);
            ReadAttribute(reader, "InterviewersName", out InterviewersName);
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "InterviewersEmployerStreetAddress", InterviewersEmployerStreetAddress);
            WriteAttribute(writer, "InterviewersEmployerCity", InterviewersEmployerCity);
            WriteAttribute(writer, "InterviewersEmployerState", InterviewersEmployerState);
            WriteAttribute(writer, "InterviewersEmployerPostalCode", InterviewersEmployerPostalCode);
            WriteAttribute(writer, "InterviewersTelephoneNumber", InterviewersTelephoneNumber);
            WriteAttribute(writer, "ApplicationTakenMethodType", ApplicationTakenMethodType);
            WriteAttribute(writer, "InterviewerApplicationSignedDate", InterviewerApplicationSignedDate);
            WriteAttribute(writer, "InterviewersEmployerName", InterviewersEmployerName);
            WriteAttribute(writer, "InterviewersName", InterviewersName);
        }
    }
}
