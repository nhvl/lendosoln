using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace DocuTech
{
    public sealed class DocumentFormat : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "DOCUMENT_FORMAT"; }
        }

        private SmartDocType m_smartDocType;
        public SmartDocType SmartDocType
        {
            get
            {
                if (m_smartDocType == null)
                {
                    m_smartDocType = new SmartDocType();
                }
                return m_smartDocType;
            }
            set { m_smartDocType = value; } 
        }

        public E_DocumentFormatType Type;

        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "_Type", out Type);
        }
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "SMART_DOC_TYPE": ReadElement(reader, SmartDocType); break;
            }
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "_Type", Type);
            WriteElement(writer, m_smartDocType);
        }

    }
}
