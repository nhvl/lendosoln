// Generated by CodeMonkey on 10/22/2010 6:17:52 PM
namespace DocuTech
{
    public enum E_TransmittalDataCaseStateType
    {
        Undefined = 0,
        Application,
        FinalDisposition,
        PostClosingQualityControl,
        Prequalification,
        Underwriting,
    }
}
