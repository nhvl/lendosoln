using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace DocuTech
{
    public sealed class SmartDocument : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "SMART_DOCUMENT"; }
        }

        public string MISMOVersionIdentifier = "1.02";
        public string PopulatingSystemDocumentIdentifier;
        public string PackageIdentifier;
        public string DTC_PopulatingSystemIdentifier;

        private SmartDocumentData m_data;
        public SmartDocumentData Data
        {
            get
            {
                if (m_data == null)
                {
                    m_data = new SmartDocumentData();
                }
                return m_data;
            }
            set { m_data = value; } 
        }
        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "MISMOVersionIdentifier", out MISMOVersionIdentifier);
            ReadAttribute(reader, "PopulatingSystemDocumentIdentifier", out PopulatingSystemDocumentIdentifier);
            ReadAttribute(reader, "PackageIdentifier", out PackageIdentifier);
            ReadAttribute(reader, "DTC_PopulatingSystemIdentifier", out DTC_PopulatingSystemIdentifier);
        }
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "DATA": ReadElement(reader, Data); break;
            }
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "MISMOVersionIdentifier", MISMOVersionIdentifier);
            WriteAttribute(writer, "PopulatingSystemDocumentIdentifier", PopulatingSystemDocumentIdentifier);
            WriteAttribute(writer, "PackageIdentifier", PackageIdentifier);
            WriteAttribute(writer, "DTC_PopulatingSystemIdentifier", DTC_PopulatingSystemIdentifier);
            WriteElement(writer, m_data);
        }
    }
}
