// Generated by CodeMonkey on 10/25/2010 6:38:29 PM
using System;
using System.Collections.Generic;
using System.Xml;

namespace DocuTech
{
    public sealed class CxAgent : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "CX_AGENT"; }
        }

        public E_CxAgentType Type;
        public string UnparsedName;
        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "_Type", out Type);
            ReadAttribute(reader, "_UnparsedName", out UnparsedName);
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "_Type", Type);
            WriteAttribute(writer, "_UnparsedName", UnparsedName);
        }
    }
}
