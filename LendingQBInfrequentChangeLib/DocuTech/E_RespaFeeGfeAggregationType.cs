﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocuTech
{
    public enum E_RespaFeeGfeAggregationType
    {
        Undefined = 0,
        ChosenInterestRateCreditOrCharge = 1,
        CombinedOurOriginationAndInterestRateCreditOrCharge = 2,
        GovernmentRecordingCharges = 3,
        None = 4,
        OurOriginationCharge = 5,
        OwnersTitleInsurance = 6,
        RequiredServicesLenderSelected = 7,
        RequiredServicesYouCanShopFor = 8,
        TitleServices = 9,
        TransferTaxes = 10,
    }
}
