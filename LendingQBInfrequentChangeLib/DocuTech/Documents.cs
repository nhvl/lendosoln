using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace DocuTech
{
    public class Documents : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "DOCUMENTS"; }
        }

        private List<Document> m_documentList;
        public List<Document> DocumentList
        {
            get
            {
                if (m_documentList == null)
                {
                    m_documentList = new List<Document>();
                }
                return m_documentList;
            }
        }

        public E_DocumentsPackageType PackageType;

        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "_PackageType", out PackageType);
        }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "DOCUMENT": ReadElement(reader, DocumentList); break;
            }
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "_PackageType", PackageType);
            WriteElement(writer, m_documentList);
        }
    }
}
