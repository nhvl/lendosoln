using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace DocuTech
{
    public sealed class DataChecks : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "DATA_CHECKS"; }
        }

        public string MissingFieldCheck;
        public string IgnoreOptionalFields;
        public string BreakOnException;

        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "_MissingFieldCheck", out MissingFieldCheck);
            ReadAttribute(reader, "_IgnoreOptionalFields", out IgnoreOptionalFields);
            ReadAttribute(reader, "_BreakOnException", out BreakOnException);
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "_MissingFieldCheck", MissingFieldCheck);
            WriteAttribute(writer, "_IgnoreOptionalFields", IgnoreOptionalFields);
            WriteAttribute(writer, "_BreakOnException", BreakOnException);
        }
    }
}
