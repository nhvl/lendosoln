// Generated by CodeMonkey on 10/22/2010 6:17:53 PM
using System;
using System.Collections.Generic;
using System.Xml;

namespace DocuTech
{
    public sealed class Buydown : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "BUYDOWN"; }
        }

        public E_BuydownBaseDateType BaseDateType;
        public string ChangeFrequencyMonths;
        public E_BuydownContributorType ContributorType;
        public string ContributorTypeOtherDescription;
        public string DurationMonths;
        public string IncreaseRatePercent;
        public E_YesNoIndicator LenderFundingIndicator;
        public string OriginalBalanceAmount;
        public E_YesNoIndicator PermanentIndicator;
        public E_BuydownSubsidyCalculationType SubsidyCalculationType;
        public string TotalSubsidyAmount;
        private List<Contributor> m_ContributorList;
        public List<Contributor> ContributorList
        {
            get
            {
                if (null == m_ContributorList)
                {
                    m_ContributorList = new List<Contributor>();
                }
                return m_ContributorList;
            }
        }

        private List<SubsidySchedule> m_SubsidyScheduleList;
        public List<SubsidySchedule> SubsidyScheduleList
        {
            get
            {
                if (null == m_SubsidyScheduleList)
                {
                    m_SubsidyScheduleList = new List<SubsidySchedule>();
                }
                return m_SubsidyScheduleList;
            }
        }

        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "_BaseDateType", out BaseDateType);
            ReadAttribute(reader, "_ChangeFrequencyMonths", out ChangeFrequencyMonths);
            ReadAttribute(reader, "_ContributorType", out ContributorType);
            ReadAttribute(reader, "_ContributorTypeOtherDescription", out ContributorTypeOtherDescription);
            ReadAttribute(reader, "_DurationMonths", out DurationMonths);
            ReadAttribute(reader, "_IncreaseRatePercent", out IncreaseRatePercent);
            ReadAttribute(reader, "_LenderFundingIndicator", out LenderFundingIndicator);
            ReadAttribute(reader, "_OriginalBalanceAmount", out OriginalBalanceAmount);
            ReadAttribute(reader, "_PermanentIndicator", out PermanentIndicator);
            ReadAttribute(reader, "_SubsidyCalculationType", out SubsidyCalculationType);
            ReadAttribute(reader, "_TotalSubsidyAmount", out TotalSubsidyAmount);
        }
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "_CONTRIBUTOR": ReadElement(reader, ContributorList); break;
                case "_SUBSIDY_SCHEDULE": ReadElement(reader, SubsidyScheduleList); break;
            }
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "_BaseDateType", BaseDateType);
            WriteAttribute(writer, "_ChangeFrequencyMonths", ChangeFrequencyMonths);
            WriteAttribute(writer, "_ContributorType", ContributorType);
            WriteAttribute(writer, "_ContributorTypeOtherDescription", ContributorTypeOtherDescription);
            WriteAttribute(writer, "_DurationMonths", DurationMonths);
            WriteAttribute(writer, "_IncreaseRatePercent", IncreaseRatePercent);
            WriteAttribute(writer, "_LenderFundingIndicator", LenderFundingIndicator);
            WriteAttribute(writer, "_OriginalBalanceAmount", OriginalBalanceAmount);
            WriteAttribute(writer, "_PermanentIndicator", PermanentIndicator);
            WriteAttribute(writer, "_SubsidyCalculationType", SubsidyCalculationType);
            WriteAttribute(writer, "_TotalSubsidyAmount", TotalSubsidyAmount);
            WriteElement(writer, m_ContributorList);
            WriteElement(writer, m_SubsidyScheduleList);
        }
    }
}
