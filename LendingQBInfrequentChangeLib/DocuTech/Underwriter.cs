// Generated by CodeMonkey on 10/25/2010 6:38:29 PM
using System;
using System.Collections.Generic;
using System.Xml;

namespace DocuTech
{
    public sealed class Underwriter : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "UNDERWRITER"; }
        }

        private UnderwriterContactDetail m_ContactDetail;
        public UnderwriterContactDetail ContactDetail
        {
            get
            {
                if (null == m_ContactDetail)
                {
                    m_ContactDetail = new UnderwriterContactDetail();
                }
                return m_ContactDetail;
            }
            set { m_ContactDetail = value; } 
        }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "CONTACT_DETAIL": ReadElement(reader, ContactDetail); break;
            }
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, m_ContactDetail);
        }
    }
}
