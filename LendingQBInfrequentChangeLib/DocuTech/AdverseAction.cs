// Generated by CodeMonkey on 10/25/2010 6:38:29 PM
using System;
using System.Collections.Generic;
using System.Xml;

namespace DocuTech
{
    public sealed class AdverseAction : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "ADVERSE_ACTION"; }
        }

        public string CreditAgencyPhoneTollFree;
        public E_YesNoIndicator BasedOnAgencyIndicator;
        public E_YesNoIndicator BasedOnOtherIndicator;
        public string MailedDate;
        public E_YesNoIndicator MailingstatusIndicator;
        private Credit m_Credit;
        public Credit Credit
        {
            get
            {
                if (null == m_Credit)
                {
                    m_Credit = new Credit();
                }
                return m_Credit;
            }
            set { m_Credit = value; } 
        }

        private EmploymentStatus m_EmploymentStatus;
        public EmploymentStatus EmploymentStatus
        {
            get
            {
                if (null == m_EmploymentStatus)
                {
                    m_EmploymentStatus = new EmploymentStatus();
                }
                return m_EmploymentStatus;
            }
            set { m_EmploymentStatus = value; } 
        }

        private Income m_Income;
        public Income Income
        {
            get
            {
                if (null == m_Income)
                {
                    m_Income = new Income();
                }
                return m_Income;
            }
            set { m_Income = value; } 
        }

        private Other m_Other;
        public Other Other
        {
            get
            {
                if (null == m_Other)
                {
                    m_Other = new Other();
                }
                return m_Other;
            }
            set { m_Other = value; } 
        }

        private Residency m_Residency;
        public Residency Residency
        {
            get
            {
                if (null == m_Residency)
                {
                    m_Residency = new Residency();
                }
                return m_Residency;
            }
            set { m_Residency = value; } 
        }

        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "CreditAgencyPhoneTollFree", out CreditAgencyPhoneTollFree);
            ReadAttribute(reader, "BasedOnAgencyIndicator", out BasedOnAgencyIndicator);
            ReadAttribute(reader, "BasedOnOtherIndicator", out BasedOnOtherIndicator);
            ReadAttribute(reader, "MailedDate", out MailedDate);
            ReadAttribute(reader, "MailingstatusIndicator", out MailingstatusIndicator);
        }
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "CREDIT": ReadElement(reader, Credit); break;
                case "EMPLOYMENT_STATUS": ReadElement(reader, EmploymentStatus); break;
                case "INCOME": ReadElement(reader, Income); break;
                case "OTHER": ReadElement(reader, Other); break;
                case "RESIDENCY": ReadElement(reader, Residency); break;
            }
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "CreditAgencyPhoneTollFree", CreditAgencyPhoneTollFree);
            WriteAttribute(writer, "BasedOnAgencyIndicator", BasedOnAgencyIndicator);
            WriteAttribute(writer, "BasedOnOtherIndicator", BasedOnOtherIndicator);
            WriteAttribute(writer, "MailedDate", MailedDate);
            WriteAttribute(writer, "MailingstatusIndicator", MailingstatusIndicator);
            WriteElement(writer, m_Credit);
            WriteElement(writer, m_EmploymentStatus);
            WriteElement(writer, m_Income);
            WriteElement(writer, m_Other);
            WriteElement(writer, m_Residency);
        }
    }
}
