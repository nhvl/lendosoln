﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocuTech
{
    public enum E_GfeDetailGfeCreditOrChargeForChosenInterestRateType
    {
        Undefined = 0,
        BorrowerCharge = 1,
        BorrowerCredit = 2,
        CreditOrChargeIncludedInOriginationCharge = 3,
        Other = 4,
    }
}
