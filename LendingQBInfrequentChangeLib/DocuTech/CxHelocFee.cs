// Generated by CodeMonkey on 10/25/2010 6:38:29 PM
using System;
using System.Collections.Generic;
using System.Xml;

namespace DocuTech
{
    public sealed class CxHelocFee : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "CX_HELOC_FEE"; }
        }

        public E_CxHelocFeeType Type;
        public string SequenceIdentifier;
        public string Description;
        public string Amount;
        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "_Type", out Type);
            ReadAttribute(reader, "_SequenceIdentifier", out SequenceIdentifier);
            ReadAttribute(reader, "_Description", out Description);
            ReadAttribute(reader, "_Amount", out Amount);
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "_Type", Type);
            WriteAttribute(writer, "_SequenceIdentifier", SequenceIdentifier);
            WriteAttribute(writer, "_Description", Description);
            WriteAttribute(writer, "_Amount", Amount);
        }
    }
}
