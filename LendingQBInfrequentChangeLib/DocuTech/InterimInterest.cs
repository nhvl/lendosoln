// Generated by CodeMonkey on 10/22/2010 6:17:54 PM
using System;
using System.Collections.Generic;
using System.Xml;

namespace DocuTech
{
    public sealed class InterimInterest : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "INTERIM_INTEREST"; }
        }

        public string PaidFromDate;
        public string PaidNumberOfDays;
        public string PaidThroughDate;
        public E_InterimInterestPerDiemCalculationMethodType PerDiemCalculationMethodType;
        public E_InterimInterestPerDiemPaymentOptionType PerDiemPaymentOptionType;
        public string SinglePerDiemAmount;
        public string TotalPerDiemAmount;
        public E_InterimInterestDisclosureType DisclosureType;

        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "_PaidFromDate", out PaidFromDate);
            ReadAttribute(reader, "_PaidNumberOfDays", out PaidNumberOfDays);
            ReadAttribute(reader, "_PaidThroughDate", out PaidThroughDate);
            ReadAttribute(reader, "_PerDiemCalculationMethodType", out PerDiemCalculationMethodType);
            ReadAttribute(reader, "_PerDiemPaymentOptionType", out PerDiemPaymentOptionType);
            ReadAttribute(reader, "_SinglePerDiemAmount", out SinglePerDiemAmount);
            ReadAttribute(reader, "_TotalPerDiemAmount", out TotalPerDiemAmount);
            ReadAttribute(reader, "_DisclosureType", out DisclosureType);
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "_PaidFromDate", PaidFromDate);
            WriteAttribute(writer, "_PaidNumberOfDays", PaidNumberOfDays);
            WriteAttribute(writer, "_PaidThroughDate", PaidThroughDate);
            WriteAttribute(writer, "_PerDiemCalculationMethodType", PerDiemCalculationMethodType);
            WriteAttribute(writer, "_PerDiemPaymentOptionType", PerDiemPaymentOptionType);
            WriteAttribute(writer, "_SinglePerDiemAmount", SinglePerDiemAmount);
            WriteAttribute(writer, "_TotalPerDiemAmount", TotalPerDiemAmount);
            WriteAttribute(writer, "_DisclosureType", DisclosureType);
        }
    }
}
