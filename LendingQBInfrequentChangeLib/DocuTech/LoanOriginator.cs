﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocuTech
{
    public sealed class LoanOriginator : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "LOAN_ORIGINATOR"; }
        }
        public string LicenseNumberIdentifier;
        public string NationwideMortgageLicensingSystemAssignedIdentifier;

        protected override void ReadAttributes(System.Xml.XmlReader reader)
        {
            ReadAttribute(reader, "_LicenseNumberIdentifier", out LicenseNumberIdentifier);
            ReadAttribute(reader, "_NationwideMortgageLicensingSystemAssignedIdentifier", out NationwideMortgageLicensingSystemAssignedIdentifier);
        }
        protected override void WriteXmlImpl(System.Xml.XmlWriter writer)
        {
            WriteAttribute(writer, "_LicenseNumberIdentifier", LicenseNumberIdentifier);
            WriteAttribute(writer, "_NationwideMortgageLicensingSystemAssignedIdentifier", NationwideMortgageLicensingSystemAssignedIdentifier);
        }
    }
}
