using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace DocuTech
{
    public sealed class SmartDocType : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "SMART_DOC_TYPE"; }
        }
        public string Level;

        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "_Level", out Level);
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "_Level", Level);
        }
    }
}
