// Generated by CodeMonkey on 10/25/2010 6:38:29 PM
using System;
using System.Collections.Generic;
using System.Xml;

namespace DocuTech
{
    public sealed class CxTitle : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "CX_TITLE"; }
        }

        public E_YesNoIndicator ComprehensiveEndorsementIndicator;
        public string OtherEndorsementDescription;
        public E_YesNoIndicator ThreeRandFiveEndorsementIndicator;
        public E_YesNoIndicator SixArmEndorsementIndicator;
        public E_YesNoIndicator FourCondoEndorsementIndicator;
        public E_YesNoIndicator NegativeAmortizationEndorsementIndicator;
        public E_YesNoIndicator PlannedUnitDevelopmentEndorsementIndicator;
        public E_YesNoIndicator EightOneEnvironmentEndorsementIndicator;
        public E_YesNoIndicator LocationEndorsementIndicator;
        public E_YesNoIndicator OtherEndorsementIndicator;
        public string UnparsedAddress;
        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "ComprehensiveEndorsementIndicator", out ComprehensiveEndorsementIndicator);
            ReadAttribute(reader, "OtherEndorsementDescription", out OtherEndorsementDescription);
            ReadAttribute(reader, "ThreeRandFiveEndorsementIndicator", out ThreeRandFiveEndorsementIndicator);
            ReadAttribute(reader, "SixARMEndorsementIndicator", out SixArmEndorsementIndicator);
            ReadAttribute(reader, "FourCondoEndorsementIndicator", out FourCondoEndorsementIndicator);
            ReadAttribute(reader, "NegativeAmortizationEndorsementIndicator", out NegativeAmortizationEndorsementIndicator);
            ReadAttribute(reader, "PlannedUnitDevelopmentEndorsementIndicator", out PlannedUnitDevelopmentEndorsementIndicator);
            ReadAttribute(reader, "EightOneEnvironmentEndorsementIndicator", out EightOneEnvironmentEndorsementIndicator);
            ReadAttribute(reader, "LocationEndorsementIndicator", out LocationEndorsementIndicator);
            ReadAttribute(reader, "OtherEndorsementIndicator", out OtherEndorsementIndicator);
            ReadAttribute(reader, "_UnparsedAddress", out UnparsedAddress);
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "ComprehensiveEndorsementIndicator", ComprehensiveEndorsementIndicator);
            WriteAttribute(writer, "OtherEndorsementDescription", OtherEndorsementDescription);
            WriteAttribute(writer, "ThreeRandFiveEndorsementIndicator", ThreeRandFiveEndorsementIndicator);
            WriteAttribute(writer, "SixARMEndorsementIndicator", SixArmEndorsementIndicator);
            WriteAttribute(writer, "FourCondoEndorsementIndicator", FourCondoEndorsementIndicator);
            WriteAttribute(writer, "NegativeAmortizationEndorsementIndicator", NegativeAmortizationEndorsementIndicator);
            WriteAttribute(writer, "PlannedUnitDevelopmentEndorsementIndicator", PlannedUnitDevelopmentEndorsementIndicator);
            WriteAttribute(writer, "EightOneEnvironmentEndorsementIndicator", EightOneEnvironmentEndorsementIndicator);
            WriteAttribute(writer, "LocationEndorsementIndicator", LocationEndorsementIndicator);
            WriteAttribute(writer, "OtherEndorsementIndicator", OtherEndorsementIndicator);
            WriteAttribute(writer, "_UnparsedAddress", UnparsedAddress);
        }
    }
}
