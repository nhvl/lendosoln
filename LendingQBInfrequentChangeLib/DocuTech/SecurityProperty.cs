// Generated by CodeMonkey on 10/25/2010 6:38:29 PM
using System;
using System.Collections.Generic;
using System.Xml;

namespace DocuTech
{
    public sealed class SecurityProperty : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "SECURITY_PROPERTY"; }
        }

        public string SequenceIdentifier;
        public string SecurityStreetName;
        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "_SequenceIdentifier", out SequenceIdentifier);
            ReadAttribute(reader, "SecurityStreetName", out SecurityStreetName);
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "_SequenceIdentifier", SequenceIdentifier);
            WriteAttribute(writer, "SecurityStreetName", SecurityStreetName);
        }
    }
}
