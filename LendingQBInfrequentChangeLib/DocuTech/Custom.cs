using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace DocuTech
{
    public sealed class Custom : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "CUSTOM"; }
        }

        private UserData m_userData;
        public UserData UserData
        {
            get
            {
                if (m_userData == null)
                {
                    m_userData = new UserData();
                }
                return m_userData;
            }
            set { m_userData = value; } 
        }

        private ConformX m_conformX;
        public ConformX ConformX
        {
            get
            {
                if (m_conformX == null)
                {
                    m_conformX = new ConformX();
                }
                return m_conformX;
            }
            set { m_conformX = value; } 
        }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "USER_DATA": ReadElement(reader, UserData); break;
                case "CONFORMX": ReadElement(reader, ConformX); break;
            }
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, m_userData);
            WriteElement(writer, m_conformX);
        }
    }
}
