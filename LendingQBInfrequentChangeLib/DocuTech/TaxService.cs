// Generated by CodeMonkey on 10/25/2010 6:38:29 PM
using System;
using System.Collections.Generic;
using System.Xml;

namespace DocuTech
{
    public sealed class TaxService : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "TAX_SERVICE"; }
        }

        public string TaxServiceVendorBankCode;
        public string TaxServiceVendorCity;
        public string TaxServiceVendorContactFirstName;
        public string TaxServiceVendorContactLastName;
        public string TaxServiceVendorContactMiddleName;
        public string TaxServiceVendorContactSuffix;
        public string TaxServiceVendorName;
        public string TaxServiceVendorPhone;
        public string TaxServiceVendorState;
        public string TaxServiceVendorStreet;
        public string TaxServiceVendorZip;
        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "TaxServiceVendorBankCode", out TaxServiceVendorBankCode);
            ReadAttribute(reader, "TaxServiceVendorCity", out TaxServiceVendorCity);
            ReadAttribute(reader, "TaxServiceVendorContactFirstName", out TaxServiceVendorContactFirstName);
            ReadAttribute(reader, "TaxServiceVendorContactLastName", out TaxServiceVendorContactLastName);
            ReadAttribute(reader, "TaxServiceVendorContactMiddleName", out TaxServiceVendorContactMiddleName);
            ReadAttribute(reader, "TaxServiceVendorContactSuffix", out TaxServiceVendorContactSuffix);
            ReadAttribute(reader, "TaxServiceVendorName", out TaxServiceVendorName);
            ReadAttribute(reader, "TaxServiceVendorPhone", out TaxServiceVendorPhone);
            ReadAttribute(reader, "TaxServiceVendorState", out TaxServiceVendorState);
            ReadAttribute(reader, "TaxServiceVendorStreet", out TaxServiceVendorStreet);
            ReadAttribute(reader, "TaxServiceVendorZip", out TaxServiceVendorZip);
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "TaxServiceVendorBankCode", TaxServiceVendorBankCode);
            WriteAttribute(writer, "TaxServiceVendorCity", TaxServiceVendorCity);
            WriteAttribute(writer, "TaxServiceVendorContactFirstName", TaxServiceVendorContactFirstName);
            WriteAttribute(writer, "TaxServiceVendorContactLastName", TaxServiceVendorContactLastName);
            WriteAttribute(writer, "TaxServiceVendorContactMiddleName", TaxServiceVendorContactMiddleName);
            WriteAttribute(writer, "TaxServiceVendorContactSuffix", TaxServiceVendorContactSuffix);
            WriteAttribute(writer, "TaxServiceVendorName", TaxServiceVendorName);
            WriteAttribute(writer, "TaxServiceVendorPhone", TaxServiceVendorPhone);
            WriteAttribute(writer, "TaxServiceVendorState", TaxServiceVendorState);
            WriteAttribute(writer, "TaxServiceVendorStreet", TaxServiceVendorStreet);
            WriteAttribute(writer, "TaxServiceVendorZip", TaxServiceVendorZip);
        }
    }
}
