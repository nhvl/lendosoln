// Generated by CodeMonkey on 10/22/2010 6:17:54 PM
using System;
using System.Collections.Generic;
using System.Xml;

namespace DocuTech
{
    public sealed class Summary : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "SUMMARY"; }
        }

        public string Amount;
        public E_SummaryAmountType AmountType;
        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "_Amount", out Amount);
            ReadAttribute(reader, "_AmountType", out AmountType);
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "_Amount", Amount);
            WriteAttribute(writer, "_AmountType", AmountType);
        }
    }
}
