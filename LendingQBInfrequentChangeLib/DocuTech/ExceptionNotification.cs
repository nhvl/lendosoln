using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace DocuTech
{
    public sealed class ExceptionNotification : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "EXCEPTION_NOTIFICATION"; }
        }
        private Emails m_emails;
        public Emails Emails
        {
            get
            {
                if (m_emails == null)
                {
                    m_emails = new Emails();
                }
                return m_emails;
            }
            set { m_emails = value; } 
        }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "EMAILS": ReadElement(reader, Emails); break;
            }
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, m_emails);
        }
    }


}
