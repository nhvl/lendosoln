// Generated by CodeMonkey on 10/22/2010 6:17:53 PM
using System;
using System.Collections.Generic;
using System.Xml;

namespace DocuTech
{
    public sealed class EscrowAccountSummary : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "ESCROW_ACCOUNT_SUMMARY"; }
        }

        public string EscrowAggregateAccountingAdjustmentAmount;
        public string EscrowCushionNumberOfMonthsCount;
        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "EscrowAggregateAccountingAdjustmentAmount", out EscrowAggregateAccountingAdjustmentAmount);
            ReadAttribute(reader, "EscrowCushionNumberOfMonthsCount", out EscrowCushionNumberOfMonthsCount);
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "EscrowAggregateAccountingAdjustmentAmount", EscrowAggregateAccountingAdjustmentAmount);
            WriteAttribute(writer, "EscrowCushionNumberOfMonthsCount", EscrowCushionNumberOfMonthsCount);
        }
    }
}
