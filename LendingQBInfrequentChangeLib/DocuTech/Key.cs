using System;
using System.Collections.Generic;
using System.Xml;

namespace DocuTech
{
    public sealed class Key : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "KEY"; }
        }

        public string Name;
        public string Value;

        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "_Name", out Name);
            ReadAttribute(reader, "_Value", out Value);
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "_Name", Name);
            WriteAttribute(writer, "_Value", Value);
        }
    }
}
