using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace DocuTech
{
    public sealed class Data : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "DATA"; }
        }

        public string Id;
        public string Value;

        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "_ID", out Id);
            ReadAttribute(reader, "_Value", out Value);
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "_ID", Id);
            WriteAttribute(writer, "_Value", Value);
        }
    }
}
