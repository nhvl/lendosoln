﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocuTech
{
    public sealed class Los : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "LOS"; }
        }

        public string PopulatingSystemRefId;

        protected override void ReadAttributes(System.Xml.XmlReader reader)
        {
            ReadAttribute(reader, "PopulatingSystemRefID", out PopulatingSystemRefId);
        }
        protected override void WriteXmlImpl(System.Xml.XmlWriter writer)
        {
            WriteAttribute(writer, "PopulatingSystemRefID", PopulatingSystemRefId);
        }
    }
}
