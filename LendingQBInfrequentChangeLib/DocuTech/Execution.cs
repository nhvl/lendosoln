// Generated by CodeMonkey on 10/22/2010 6:17:54 PM
using System;
using System.Collections.Generic;
using System.Xml;

namespace DocuTech
{
    public sealed class Execution : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "EXECUTION"; }
        }

        public string City;
        public string State;
        public string County;
        public string Date;
        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "_City", out City);
            ReadAttribute(reader, "_State", out State);
            ReadAttribute(reader, "_County", out County);
            ReadAttribute(reader, "_Date", out Date);
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "_City", City);
            WriteAttribute(writer, "_State", State);
            WriteAttribute(writer, "_County", County);
            WriteAttribute(writer, "_Date", Date);
        }
    }
}
