using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace DocuTech
{
    public sealed class EmbeddedFile : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "EMBEDDED_FILE"; }
        }

        public string Document;
        public string Id;
        public E_EmbeddedFileType Type;
        public string Version;
        public string Name;
        public E_EmbeddedFileEncodingType EncodingType;
        public string Description;
        public E_EmbeddedFileMimeType MimeType;

        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "_ID", out Id);
            ReadAttribute(reader, "_Type", out Type);
            ReadAttribute(reader, "_Version", out Version);
            ReadAttribute(reader, "_Name", out Name);
            ReadAttribute(reader, "_EncodingType", out EncodingType);
            ReadAttribute(reader, "_Description", out Description);
            ReadAttribute(reader, "MIMEType", out MimeType);
        }
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "DOCUMENT": Document = ReadElementString(reader); break;
            }
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "_ID", Id);
            WriteAttribute(writer, "_Type", Type);
            WriteAttribute(writer, "_Version", Version);
            WriteAttribute(writer, "_Name", Name);
            WriteAttribute(writer, "_EncodingType", EncodingType);
            WriteAttribute(writer, "_Description", Description);
            WriteAttribute(writer, "MIMEType", MimeType);
            WriteElementString(writer, "DOCUMENT", Document);
        }
    }
}
