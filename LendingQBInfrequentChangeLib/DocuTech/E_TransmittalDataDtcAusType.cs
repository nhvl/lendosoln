using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocuTech
{
    public enum E_TransmittalDataDtcAusType
    {
        Undefined = 0,
        Other,
        DU,
        LP
    }
}
