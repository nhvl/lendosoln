using System;
using System.Collections.Generic;
using System.Xml;

namespace DocuTech
{
    public sealed class Question : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "QUESTION"; }
        }
        public string Id;
        public string Desc;
        public string Value;

        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "_Id", out Id);
            ReadAttribute(reader, "_Desc", out Desc);
            ReadAttribute(reader, "_Value", out Value);
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "_Id", Id);
            WriteAttribute(writer, "_Desc", Desc);
            WriteAttribute(writer, "_Value", Value);
        }
    }
}
