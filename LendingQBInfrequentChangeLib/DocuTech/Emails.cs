using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace DocuTech
{
    public sealed class Emails : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "EMAILS"; }
        }

        public string Subject;
        public string Body;

        private List<Email> m_emailList;
        public List<Email> EmailList
        {
            get
            {
                if (m_emailList == null)
                {
                    m_emailList = new List<Email>();
                }
                return m_emailList;
            }
        }

        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "_Subject", out Subject);
            ReadAttribute(reader, "_Body", out Body);
        }
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "EMAIL": ReadElement(reader, EmailList); break;
            }
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "_Subject", Subject);
            WriteAttribute(writer, "_Body", Body);
            WriteElement(writer, m_emailList);
        }
    }
}
