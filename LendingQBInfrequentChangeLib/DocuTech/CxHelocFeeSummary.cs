// Generated by CodeMonkey on 10/25/2010 6:38:29 PM
using System;
using System.Collections.Generic;
using System.Xml;

namespace DocuTech
{
    public sealed class CxHelocFeeSummary : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "CX_HELOC_FEE_SUMMARY"; }
        }

        public string BorrowerOtherFeePaidAmount;
        public string BrokerFinanceChargesAmount;
        public string LenderOtherFeePaidAmount;
        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "BorrowerOtherFeePaidAmount", out BorrowerOtherFeePaidAmount);
            ReadAttribute(reader, "BrokerFinanceChargesAmount", out BrokerFinanceChargesAmount);
            ReadAttribute(reader, "LenderOtherFeePaidAmount", out LenderOtherFeePaidAmount);
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "BorrowerOtherFeePaidAmount", BorrowerOtherFeePaidAmount);
            WriteAttribute(writer, "BrokerFinanceChargesAmount", BrokerFinanceChargesAmount);
            WriteAttribute(writer, "LenderOtherFeePaidAmount", LenderOtherFeePaidAmount);
        }
    }
}
