// Generated by CodeMonkey on 10/22/2010 6:17:52 PM
namespace DocuTech
{
    public enum E_DownPaymentType
    {
        Undefined = 0,
        BridgeLoan,
        CashOnHand,
        CheckingSavings,
        DepositOnSalesContract,
        EquityOnPendingSale,
        EquityOnSoldProperty,
        EquityOnSubjectProperty,
        GiftFunds,
        LifeInsuranceCashValue,
        LotEquity,
        OtherTypeOfDownPayment,
        RentWithOptionToPurchase,
        RetirementFunds,
        SaleOfChattel,
        SecuredBorrowedFunds,
        StocksAndBonds,
        SweatEquity,
        TradeEquity,
        TrustFunds,
        UnsecuredBorrowedFunds,
    }
}
