using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocuTech
{
    public enum E_EmbeddedFileMimeType
    {
        Undefined = 0,
        Image_Jpeg,
        Application_Pdf,
        Text_Plain,
        Text_Xml,
        Application_Zip,
        Text_Html
    }
}
