using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace DocuTech
{
    public sealed class Document : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "DOCUMENT"; }
        }

        public string Id;
        public string Name;
        public string Blank;
        public string Sequence;
        public string Copies;

        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "_ID", out Id);
            ReadAttribute(reader, "_Name", out Name);
            ReadAttribute(reader, "_Blank", out Blank);
            ReadAttribute(reader, "_Sequence", out Sequence);
            ReadAttribute(reader, "_Copies", out Copies);
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "_ID", Id);
            WriteAttribute(writer, "_Name", Name);
            WriteAttribute(writer, "_Blank", Blank);
            WriteAttribute(writer, "_Sequence", Sequence);
            WriteAttribute(writer, "_Copies", Copies);
        }
    }
}
