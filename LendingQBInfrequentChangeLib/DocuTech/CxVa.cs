// Generated by CodeMonkey on 10/25/2010 6:38:29 PM
using System;
using System.Collections.Generic;
using System.Xml;

namespace DocuTech
{
    public sealed class CxVa : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "CX_VA"; }
        }

        public string LandAcquiredInSeparateTransactionDate;
        public string AdditionalSecurityDescription;
        public string ApproximateAnnualSpecialAssessmentPaymentAmount;
        public E_YesNoIndicator CompleteWhenAuthorizedByCertificateIndicator;
        public E_CxVaContractPriceExceedsCrv ContractPriceExceedsCrv;
        public E_CxVaInsuranceType InsuranceType;
        public string AnnualMaintainanceAssessment;
        public string NonRealtyAcquiredWithLoanProceedsDescription;
        public E_YesNoIndicator CertificationOfEligibilityIndicator;
        public E_YesNoIndicator VeteranIntendsToOccupyHomeIndicator;
        public E_YesNoIndicator VeteranOnActiveDutySpouseWillOccupyIndicator;
        public E_YesNoIndicator VeteranPreviouslyOccupiedPropertyIndicator;
        public E_YesNoIndicator VeteranOnActiveDutySpousePreviouslyOccupiedPropertyIndicator;
        public E_CxVaProcedureType ProcedureType;
        public string TotalUnpaidSpecailAssessments;
        public E_CxVaVestingType VestingType;
        public string VestingTypeOtherDescription;
        public E_CxVaLoanProceedsWithholdingDepositedInAccountType LoanProceedsWithholdingDepositedInAccountType;
        public string LoanProceedsWithholdingAmount;
        public string ApprovedUnderwriter;
        public E_YesNoIndicator BorrowerAwareContractPriceExceedsCrv;
        public string EstateInPropertyOtherDescription;
        public E_CxVaLienType LienType;
        public string OtherLienTypeDescription;
        public string PurchasePriceOfLandAcquiredInSeparate;
        public string SocialSecurityNumber;
        public string VeteranFirstName;
        public string VeteranGender;
        public string VeteranLastName;
        public string VeteranMiddleName;
        public E_CxVaVetEthnicity VetEthnicity;
        public E_CxVaCoborrowerEthnicity CoborrowerEthnicity;
        private List<CxVaAgent> m_CxVaAgentList;
        public List<CxVaAgent> CxVaAgentList
        {
            get
            {
                if (null == m_CxVaAgentList)
                {
                    m_CxVaAgentList = new List<CxVaAgent>();
                }
                return m_CxVaAgentList;
            }
        }

        private CxVaRelative m_CxVaRelative;
        public CxVaRelative CxVaRelative
        {
            get
            {
                if (null == m_CxVaRelative)
                {
                    m_CxVaRelative = new CxVaRelative();
                }
                return m_CxVaRelative;
            }
            set { m_CxVaRelative = value; } 
        }

        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "LandAcquiredInSeparateTransactionDate", out LandAcquiredInSeparateTransactionDate);
            ReadAttribute(reader, "AdditionalSecurityDescription", out AdditionalSecurityDescription);
            ReadAttribute(reader, "ApproximateAnnualSpecialAssessmentPaymentAmount", out ApproximateAnnualSpecialAssessmentPaymentAmount);
            ReadAttribute(reader, "CompleteWhenAuthorizedByCertificateIndicator", out CompleteWhenAuthorizedByCertificateIndicator);
            ReadAttribute(reader, "ContractPriceExceedsCRV", out ContractPriceExceedsCrv);
            ReadAttribute(reader, "InsuranceType", out InsuranceType);
            ReadAttribute(reader, "AnnualMaintainanceAssessment", out AnnualMaintainanceAssessment);
            ReadAttribute(reader, "NonRealtyAcquiredWithLoanProceedsDescription", out NonRealtyAcquiredWithLoanProceedsDescription);
            ReadAttribute(reader, "CertificationOfEligibilityIndicator", out CertificationOfEligibilityIndicator);
            ReadAttribute(reader, "VeteranIntendsToOccupyHomeIndicator", out VeteranIntendsToOccupyHomeIndicator);
            ReadAttribute(reader, "VeteranOnActiveDutySpouseWillOccupyIndicator", out VeteranOnActiveDutySpouseWillOccupyIndicator);
            ReadAttribute(reader, "VeteranPreviouslyOccupiedPropertyIndicator", out VeteranPreviouslyOccupiedPropertyIndicator);
            ReadAttribute(reader, "VeteranOnActiveDutySpousePreviouslyOccupiedPropertyIndicator", out VeteranOnActiveDutySpousePreviouslyOccupiedPropertyIndicator);
            ReadAttribute(reader, "ProcedureType", out ProcedureType);
            ReadAttribute(reader, "TotalUnpaidSpecailAssessments", out TotalUnpaidSpecailAssessments);
            ReadAttribute(reader, "VestingType", out VestingType);
            ReadAttribute(reader, "VestingTypeOtherDescription", out VestingTypeOtherDescription);
            ReadAttribute(reader, "LoanProceedsWithholdingDepositedInAccountType", out LoanProceedsWithholdingDepositedInAccountType);
            ReadAttribute(reader, "LoanProceedsWithholdingAmount", out LoanProceedsWithholdingAmount);
            ReadAttribute(reader, "ApprovedUnderwriter", out ApprovedUnderwriter);
            ReadAttribute(reader, "BorrowerAwareContractPriceExceedsCRV", out BorrowerAwareContractPriceExceedsCrv);
            ReadAttribute(reader, "EstateInPropertyOtherDescription", out EstateInPropertyOtherDescription);
            ReadAttribute(reader, "LienType", out LienType);
            ReadAttribute(reader, "OtherLienTypeDescription", out OtherLienTypeDescription);
            ReadAttribute(reader, "PurchasePriceOfLandAcquiredInSeparate", out PurchasePriceOfLandAcquiredInSeparate);
            ReadAttribute(reader, "SocialSecurityNumber", out SocialSecurityNumber);
            ReadAttribute(reader, "VeteranFirstName", out VeteranFirstName);
            ReadAttribute(reader, "VeteranGender", out VeteranGender);
            ReadAttribute(reader, "VeteranLastName", out VeteranLastName);
            ReadAttribute(reader, "VeteranMiddleName", out VeteranMiddleName);
            ReadAttribute(reader, "VetEthnicity", out VetEthnicity);
            ReadAttribute(reader, "CoborrowerEthnicity", out CoborrowerEthnicity);
        }
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "CX_VA_AGENT": ReadElement(reader, CxVaAgentList); break;
                case "CX_VA_RELATIVE": ReadElement(reader, CxVaRelative); break;
            }
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "LandAcquiredInSeparateTransactionDate", LandAcquiredInSeparateTransactionDate);
            WriteAttribute(writer, "AdditionalSecurityDescription", AdditionalSecurityDescription);
            WriteAttribute(writer, "ApproximateAnnualSpecialAssessmentPaymentAmount", ApproximateAnnualSpecialAssessmentPaymentAmount);
            WriteAttribute(writer, "CompleteWhenAuthorizedByCertificateIndicator", CompleteWhenAuthorizedByCertificateIndicator);
            WriteAttribute(writer, "ContractPriceExceedsCRV", ContractPriceExceedsCrv);
            WriteAttribute(writer, "InsuranceType", InsuranceType);
            WriteAttribute(writer, "AnnualMaintainanceAssessment", AnnualMaintainanceAssessment);
            WriteAttribute(writer, "NonRealtyAcquiredWithLoanProceedsDescription", NonRealtyAcquiredWithLoanProceedsDescription);
            WriteAttribute(writer, "CertificationOfEligibilityIndicator", CertificationOfEligibilityIndicator);
            WriteAttribute(writer, "VeteranIntendsToOccupyHomeIndicator", VeteranIntendsToOccupyHomeIndicator);
            WriteAttribute(writer, "VeteranOnActiveDutySpouseWillOccupyIndicator", VeteranOnActiveDutySpouseWillOccupyIndicator);
            WriteAttribute(writer, "VeteranPreviouslyOccupiedPropertyIndicator", VeteranPreviouslyOccupiedPropertyIndicator);
            WriteAttribute(writer, "VeteranOnActiveDutySpousePreviouslyOccupiedPropertyIndicator", VeteranOnActiveDutySpousePreviouslyOccupiedPropertyIndicator);
            WriteAttribute(writer, "ProcedureType", ProcedureType);
            WriteAttribute(writer, "TotalUnpaidSpecailAssessments", TotalUnpaidSpecailAssessments);
            WriteAttribute(writer, "VestingType", VestingType);
            WriteAttribute(writer, "VestingTypeOtherDescription", VestingTypeOtherDescription);
            WriteAttribute(writer, "LoanProceedsWithholdingDepositedInAccountType", LoanProceedsWithholdingDepositedInAccountType);
            WriteAttribute(writer, "LoanProceedsWithholdingAmount", LoanProceedsWithholdingAmount);
            WriteAttribute(writer, "ApprovedUnderwriter", ApprovedUnderwriter);
            WriteAttribute(writer, "BorrowerAwareContractPriceExceedsCRV", BorrowerAwareContractPriceExceedsCrv);
            WriteAttribute(writer, "EstateInPropertyOtherDescription", EstateInPropertyOtherDescription);
            WriteAttribute(writer, "LienType", LienType);
            WriteAttribute(writer, "OtherLienTypeDescription", OtherLienTypeDescription);
            WriteAttribute(writer, "PurchasePriceOfLandAcquiredInSeparate", PurchasePriceOfLandAcquiredInSeparate);
            WriteAttribute(writer, "SocialSecurityNumber", SocialSecurityNumber);
            WriteAttribute(writer, "VeteranFirstName", VeteranFirstName);
            WriteAttribute(writer, "VeteranGender", VeteranGender);
            WriteAttribute(writer, "VeteranLastName", VeteranLastName);
            WriteAttribute(writer, "VeteranMiddleName", VeteranMiddleName);
            WriteAttribute(writer, "VetEthnicity", VetEthnicity);
            WriteAttribute(writer, "CoborrowerEthnicity", CoborrowerEthnicity);
            WriteElement(writer, m_CxVaAgentList);
            WriteElement(writer, m_CxVaRelative);
        }
    }
}
