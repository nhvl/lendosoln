﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace DocuTech
{
    public sealed class GeneralException : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "GENERAL_EXCEPTION"; }
        }

        public string Id;
        public string Description;

        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "_ID", out Id);
            ReadAttribute(reader, "_Description", out Description);
        }

        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "_ID", Id);
            WriteAttribute(writer, "_Description", Description);
        }

    }
}
