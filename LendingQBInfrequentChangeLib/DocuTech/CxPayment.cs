// Generated by CodeMonkey on 10/25/2010 6:38:29 PM
using System;
using System.Collections.Generic;
using System.Xml;

namespace DocuTech
{
    public sealed class CxPayment : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "CX_PAYMENT"; }
        }

        public string TotalAmountIncludingEscrow;
        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "TotalAmountIncludingEscrow", out TotalAmountIncludingEscrow);
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "TotalAmountIncludingEscrow", TotalAmountIncludingEscrow);
        }
    }
}
