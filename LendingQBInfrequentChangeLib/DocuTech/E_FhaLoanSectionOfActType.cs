// Generated by CodeMonkey on 10/22/2010 6:17:53 PM
namespace DocuTech
{
    public enum E_FhaLoanSectionOfActType
    {
        Undefined = 0,
        _203B,
        _203B251,
        _203B2,
        _203K,
        _203K251,
        _221D2,
        _221D2251,
        _234C,
        _234C251,
    }
}
