﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace DocuTech
{
    public sealed class GfeDetail : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "GFE_DETAIL"; }
        }

        public string GfeDisclosureDate;
        public string GfeInterestRateAvailableThroughDate;
        public string GfeSettlementChargesAvailableThroughDate;
        public string GfeRateLockPeriodDaysCount;
        public string GfeRateLockMinimumDaysPriorToSettlementCount;
        public E_GfeDetailGfeCreditOrChargeForChosenInterestRateType GfeCreditOrChargeForChosenInterestRateType;

        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "GFEDisclosureDate", out GfeDisclosureDate);
            ReadAttribute(reader, "GFEInterestRateAvailableThroughDate", out GfeInterestRateAvailableThroughDate);
            ReadAttribute(reader, "GFESettlementChargesAvailableThroughDate", out GfeSettlementChargesAvailableThroughDate);
            ReadAttribute(reader, "GFERateLockPeriodDaysCount", out GfeRateLockPeriodDaysCount);
            ReadAttribute(reader, "GFERateLockMinimumDaysPriorToSettlementCount", out GfeRateLockMinimumDaysPriorToSettlementCount);
            ReadAttribute(reader, "GFECreditOrChargeForChosenInterestRateType", out GfeCreditOrChargeForChosenInterestRateType);
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "GFEDisclosureDate", GfeDisclosureDate);
            WriteAttribute(writer, "GFEInterestRateAvailableThroughDate", GfeInterestRateAvailableThroughDate);
            WriteAttribute(writer, "GFESettlementChargesAvailableThroughDate", GfeSettlementChargesAvailableThroughDate);
            WriteAttribute(writer, "GFERateLockPeriodDaysCount", GfeRateLockPeriodDaysCount);
            WriteAttribute(writer, "GFERateLockMinimumDaysPriorToSettlementCount", GfeRateLockMinimumDaysPriorToSettlementCount);
            WriteAttribute(writer, "GFECreditOrChargeForChosenInterestRateType", GfeCreditOrChargeForChosenInterestRateType);
            
        }

    }
}
