// Generated by CodeMonkey on 10/25/2010 6:38:29 PM
using System;
using System.Collections.Generic;
using System.Xml;

namespace DocuTech
{
    public sealed class CxEscrowItem : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "CX_ESCROW_ITEM"; }
        }

        public E_CxEscrowItemItemType ItemType;
        public string PolicyCoverageAmount;
        public string AmountLastPaid;
        public string BuyerExpense;
        public string DelinquentDate;
        public E_CxEscrowItemPeriodicPaymentType PeriodicPaymentType;
        public E_CxEscrowItemNonBuyerExpensePaidBy NonBuyerExpensePaidBy;
        public string PolicyNumber;
        public E_YesNoIndicator PrepaidFinanceChargeIndicator;
        public string NonBuyerExpense;
        public string DateLastPaid;
        public string Phone;
        public string SellerPaysFromDate;
        public string SellerPaysToDate;
        public string FullAddress;
        public string EffectiveDate;
        public string DtcOtherEscrowGroupNumber;
        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "_ItemType", out ItemType);
            ReadAttribute(reader, "PolicyCoverageAmount", out PolicyCoverageAmount);
            ReadAttribute(reader, "AmountLastPaid", out AmountLastPaid);
            ReadAttribute(reader, "BuyerExpense", out BuyerExpense);
            ReadAttribute(reader, "DelinquentDate", out DelinquentDate);
            ReadAttribute(reader, "PeriodicPaymentType", out PeriodicPaymentType);
            ReadAttribute(reader, "NonBuyerExpensePaidBy", out NonBuyerExpensePaidBy);
            ReadAttribute(reader, "PolicyNumber", out PolicyNumber);
            ReadAttribute(reader, "PrepaidFinanceChargeIndicator", out PrepaidFinanceChargeIndicator);
            ReadAttribute(reader, "NonBuyerExpense", out NonBuyerExpense);
            ReadAttribute(reader, "DateLastPaid", out DateLastPaid);
            ReadAttribute(reader, "_Phone", out Phone);
            ReadAttribute(reader, "SellerPaysFromDate", out SellerPaysFromDate);
            ReadAttribute(reader, "SellerPaysToDate", out SellerPaysToDate);
            ReadAttribute(reader, "FullAddress", out FullAddress);
            ReadAttribute(reader, "EffectiveDate", out EffectiveDate);
            ReadAttribute(reader, "DTC_OtherEscrowGroupNumber", out DtcOtherEscrowGroupNumber);
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "_ItemType", ItemType);
            WriteAttribute(writer, "PolicyCoverageAmount", PolicyCoverageAmount);
            WriteAttribute(writer, "AmountLastPaid", AmountLastPaid);
            WriteAttribute(writer, "BuyerExpense", BuyerExpense);
            WriteAttribute(writer, "DelinquentDate", DelinquentDate);
            WriteAttribute(writer, "PeriodicPaymentType", PeriodicPaymentType);
            WriteAttribute(writer, "NonBuyerExpensePaidBy", NonBuyerExpensePaidBy);
            WriteAttribute(writer, "PolicyNumber", PolicyNumber);
            WriteAttribute(writer, "PrepaidFinanceChargeIndicator", PrepaidFinanceChargeIndicator);
            WriteAttribute(writer, "NonBuyerExpense", NonBuyerExpense);
            WriteAttribute(writer, "DateLastPaid", DateLastPaid);
            WriteAttribute(writer, "_Phone", Phone);
            WriteAttribute(writer, "SellerPaysFromDate", SellerPaysFromDate);
            WriteAttribute(writer, "SellerPaysToDate", SellerPaysToDate);
            WriteAttribute(writer, "FullAddress", FullAddress);
            WriteAttribute(writer, "EffectiveDate", EffectiveDate);
            WriteAttribute(writer, "DTC_OtherEscrowGroupNumber", DtcOtherEscrowGroupNumber);
        }
    }
}
