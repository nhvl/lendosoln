using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocuTech
{
    public enum E_DocumentFormatType
    {
        Undefined = 0,
        Pdf,
        xHtml,
        SmartDoc
    }
}
