﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;

namespace DocuTech
{
    public enum E_YesNoIndicator
    {
        Undefined = 0,
        Y,
        N
    }
    public abstract class AbstractXmlSerializable : IXmlSerializable
    {
        public abstract string XmlElementName { get; }

        protected string ReadElementString(XmlReader reader)
        {
            string str = reader.ReadString();
            if (str == null)
            {
                str = string.Empty;
            }
            return str;
        }

        private string ReadAttribute(XmlReader reader, string attrName)
        {
            string value = reader.GetAttribute(attrName);
            if (string.IsNullOrEmpty(value))
            {
                return string.Empty;
            }
            return value;
        }

        protected void WriteAttribute(XmlWriter writer, string attrName, string value)
        {
            if (string.IsNullOrEmpty(value) == false)
            {
                writer.WriteAttributeString(attrName, value);
            }
        }

        protected void ReadAttribute(XmlReader reader, string attrName, out string result)
        {
            result = ReadAttribute(reader, attrName);
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_YesNoIndicator result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Y")
            {
                result = E_YesNoIndicator.Y;
            }
            else if (s == "N")
            {
                result = E_YesNoIndicator.N;
            }
            else
            {
                result = E_YesNoIndicator.Undefined;
            }
            
        }

        protected void WriteAttribute(XmlWriter writer, string attrName, E_YesNoIndicator value)
        {
            switch (value)
            {
                case E_YesNoIndicator.Y:
                    writer.WriteAttributeString(attrName, "Y");
                    break;
                case E_YesNoIndicator.N:
                    writer.WriteAttributeString(attrName, "N");
                    break;
            }
        }

        protected void WriteElementString(XmlWriter writer, string name, string value)
        {
            if (string.IsNullOrEmpty(value) == false)
            {
                writer.WriteElementString(name, value);
            }
        }

        protected void WriteString(XmlWriter writer, string value)
        {
            if (string.IsNullOrEmpty(value) == false)
            {
                writer.WriteString(value);
            }
        }
        protected void WriteElement(XmlWriter writer, IXmlSerializable el)
        {
            if (null != el)
            {
                el.WriteXml(writer);
            }
        }
        protected void WriteElement<T>(XmlWriter writer, List<T> list) where T : IXmlSerializable
        {
            if (null == list)
                return;

            foreach (IXmlSerializable o in list)
            {
                WriteElement(writer, o);
            }
        }

        protected void ReadElement(XmlReader reader, IXmlSerializable el)
        {
            if (null == el)
            {
                return;
            }
            el.ReadXml(reader);
        }
        protected void ReadElement<T>(XmlReader reader, List<T> list) where T : IXmlSerializable, new()
        {
            if (null == list)
                return;

            T _o = new T();
            _o.ReadXml(reader);
            list.Add(_o);
        }

        #region Read/Write Enum Values
        protected void ReadAttribute(XmlReader reader, string attrName, out E_ContactPointRoleType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Home")
            {
                result = E_ContactPointRoleType.Home;
            }
            else if (s == "Mobile")
            {
                result = E_ContactPointRoleType.Mobile;
            }
            else if (s == "Work")
            {
                result = E_ContactPointRoleType.Work;
            }
            else
            {
                result = E_ContactPointRoleType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_ContactPointType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Email")
            {
                result = E_ContactPointType.Email;
            }
            else if (s == "Fax")
            {
                result = E_ContactPointType.Fax;
            }
            else if (s == "Phone")
            {
                result = E_ContactPointType.Phone;
            }
            else if (s == "Other")
            {
                result = E_ContactPointType.Other;
            }
            else
            {
                result = E_ContactPointType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_MortgageScoreType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "FraudFilterScore")
            {
                result = E_MortgageScoreType.FraudFilterScore;
            }
            else if (s == "GE_IQScore")
            {
                result = E_MortgageScoreType.GE_IQScore;
            }
            else if (s == "Other")
            {
                result = E_MortgageScoreType.Other;
            }
            else if (s == "PMIAuraAQIScore")
            {
                result = E_MortgageScoreType.PMIAuraAQIScore;
            }
            else if (s == "UGIAccuscore")
            {
                result = E_MortgageScoreType.UGIAccuscore;
            }
            else
            {
                result = E_MortgageScoreType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_TransmittalDataCaseStateType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Application")
            {
                result = E_TransmittalDataCaseStateType.Application;
            }
            else if (s == "FinalDisposition")
            {
                result = E_TransmittalDataCaseStateType.FinalDisposition;
            }
            else if (s == "PostClosingQualityControl")
            {
                result = E_TransmittalDataCaseStateType.PostClosingQualityControl;
            }
            else if (s == "Prequalification")
            {
                result = E_TransmittalDataCaseStateType.Prequalification;
            }
            else if (s == "Underwriting")
            {
                result = E_TransmittalDataCaseStateType.Underwriting;
            }
            else
            {
                result = E_TransmittalDataCaseStateType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_TransmittalDataCurrentFirstMortgageHolderType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "FNM")
            {
                result = E_TransmittalDataCurrentFirstMortgageHolderType.FNM;
            }
            else if (s == "FRE")
            {
                result = E_TransmittalDataCurrentFirstMortgageHolderType.FRE;
            }
            else if (s == "Other")
            {
                result = E_TransmittalDataCurrentFirstMortgageHolderType.Other;
            }
            else if (s == "Unknown")
            {
                result = E_TransmittalDataCurrentFirstMortgageHolderType.Unknown;
            }
            else
            {
                result = E_TransmittalDataCurrentFirstMortgageHolderType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_TransmittalDataRateLockType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "BestEfforts")
            {
                result = E_TransmittalDataRateLockType.BestEfforts;
            }
            else if (s == "Mandatory")
            {
                result = E_TransmittalDataRateLockType.Mandatory;
            }
            else
            {
                result = E_TransmittalDataRateLockType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_AffordableLendingFnmCommunityLendingProductType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "CommunityHomeBuyerProgram")
            {
                result = E_AffordableLendingFnmCommunityLendingProductType.CommunityHomeBuyerProgram;
            }
            else if (s == "Fannie97")
            {
                result = E_AffordableLendingFnmCommunityLendingProductType.Fannie97;
            }
            else if (s == "Fannie32")
            {
                result = E_AffordableLendingFnmCommunityLendingProductType.Fannie32;
            }
            else if (s == "MyCommunityMortgage")
            {
                result = E_AffordableLendingFnmCommunityLendingProductType.MyCommunityMortgage;
            }
            else if (s == "Other")
            {
                result = E_AffordableLendingFnmCommunityLendingProductType.Other;
            }
            else
            {
                result = E_AffordableLendingFnmCommunityLendingProductType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_AssetType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Automobile")
            {
                result = E_AssetType.Automobile;
            }
            else if (s == "Bond")
            {
                result = E_AssetType.Bond;
            }
            else if (s == "BridgeLoanNotDeposited")
            {
                result = E_AssetType.BridgeLoanNotDeposited;
            }
            else if (s == "CashOnHand")
            {
                result = E_AssetType.CashOnHand;
            }
            else if (s == "CertificateOfDepositTimeDeposit")
            {
                result = E_AssetType.CertificateOfDepositTimeDeposit;
            }
            else if (s == "CheckingAccount")
            {
                result = E_AssetType.CheckingAccount;
            }
            else if (s == "EarnestMoneyCashDepositTowardPurchase")
            {
                result = E_AssetType.EarnestMoneyCashDepositTowardPurchase;
            }
            else if (s == "GiftsTotal")
            {
                result = E_AssetType.GiftsTotal;
            }
            else if (s == "GiftsNotDeposited")
            {
                result = E_AssetType.GiftsNotDeposited;
            }
            else if (s == "LifeInsurance")
            {
                result = E_AssetType.LifeInsurance;
            }
            else if (s == "MoneyMarketFund")
            {
                result = E_AssetType.MoneyMarketFund;
            }
            else if (s == "MutualFund")
            {
                result = E_AssetType.MutualFund;
            }
            else if (s == "NetWorthOfBusinessOwned")
            {
                result = E_AssetType.NetWorthOfBusinessOwned;
            }
            else if (s == "OtherLiquidAssets")
            {
                result = E_AssetType.OtherLiquidAssets;
            }
            else if (s == "OtherNonLiquidAssets")
            {
                result = E_AssetType.OtherNonLiquidAssets;
            }
            else if (s == "PendingNetSaleProceedsFromRealEstateAssets")
            {
                result = E_AssetType.PendingNetSaleProceedsFromRealEstateAssets;
            }
            else if (s == "RelocationMoney")
            {
                result = E_AssetType.RelocationMoney;
            }
            else if (s == "RetirementFund")
            {
                result = E_AssetType.RetirementFund;
            }
            else if (s == "SaleOtherAssets")
            {
                result = E_AssetType.SaleOtherAssets;
            }
            else if (s == "SavingsAccount")
            {
                result = E_AssetType.SavingsAccount;
            }
            else if (s == "SecuredBorrowedFundsNotDeposited")
            {
                result = E_AssetType.SecuredBorrowedFundsNotDeposited;
            }
            else if (s == "Stock")
            {
                result = E_AssetType.Stock;
            }
            else if (s == "TrustAccount")
            {
                result = E_AssetType.TrustAccount;
            }
            else
            {
                result = E_AssetType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_DownPaymentType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "BridgeLoan")
            {
                result = E_DownPaymentType.BridgeLoan;
            }
            else if (s == "CashOnHand")
            {
                result = E_DownPaymentType.CashOnHand;
            }
            else if (s == "CheckingSavings")
            {
                result = E_DownPaymentType.CheckingSavings;
            }
            else if (s == "DepositOnSalesContract")
            {
                result = E_DownPaymentType.DepositOnSalesContract;
            }
            else if (s == "EquityOnPendingSale")
            {
                result = E_DownPaymentType.EquityOnPendingSale;
            }
            else if (s == "EquityOnSoldProperty")
            {
                result = E_DownPaymentType.EquityOnSoldProperty;
            }
            else if (s == "EquityOnSubjectProperty")
            {
                result = E_DownPaymentType.EquityOnSubjectProperty;
            }
            else if (s == "GiftFunds")
            {
                result = E_DownPaymentType.GiftFunds;
            }
            else if (s == "LifeInsuranceCashValue")
            {
                result = E_DownPaymentType.LifeInsuranceCashValue;
            }
            else if (s == "LotEquity")
            {
                result = E_DownPaymentType.LotEquity;
            }
            else if (s == "OtherTypeOfDownPayment")
            {
                result = E_DownPaymentType.OtherTypeOfDownPayment;
            }
            else if (s == "RentWithOptionToPurchase")
            {
                result = E_DownPaymentType.RentWithOptionToPurchase;
            }
            else if (s == "RetirementFunds")
            {
                result = E_DownPaymentType.RetirementFunds;
            }
            else if (s == "SaleOfChattel")
            {
                result = E_DownPaymentType.SaleOfChattel;
            }
            else if (s == "SecuredBorrowedFunds")
            {
                result = E_DownPaymentType.SecuredBorrowedFunds;
            }
            else if (s == "StocksAndBonds")
            {
                result = E_DownPaymentType.StocksAndBonds;
            }
            else if (s == "SweatEquity")
            {
                result = E_DownPaymentType.SweatEquity;
            }
            else if (s == "TradeEquity")
            {
                result = E_DownPaymentType.TradeEquity;
            }
            else if (s == "TrustFunds")
            {
                result = E_DownPaymentType.TrustFunds;
            }
            else if (s == "UnsecuredBorrowedFunds")
            {
                result = E_DownPaymentType.UnsecuredBorrowedFunds;
            }
            else
            {
                result = E_DownPaymentType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_EscrowItemType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Assessment")
            {
                result = E_EscrowItemType.Assessment;
            }
            else if (s == "CityPropertyTax")
            {
                result = E_EscrowItemType.CityPropertyTax;
            }
            else if (s == "CountyPropertyTax")
            {
                result = E_EscrowItemType.CountyPropertyTax;
            }
            else if (s == "EarthquakeInsurance")
            {
                result = E_EscrowItemType.EarthquakeInsurance;
            }
            else if (s == "FloodInsurance")
            {
                result = E_EscrowItemType.FloodInsurance;
            }
            else if (s == "HazardInsurance")
            {
                result = E_EscrowItemType.HazardInsurance;
            }
            else if (s == "Other")
            {
                result = E_EscrowItemType.Other;
            }
            else if (s == "SchoolPropertyTax")
            {
                result = E_EscrowItemType.SchoolPropertyTax;
            }
            else if (s == "TownPropertyTax")
            {
                result = E_EscrowItemType.TownPropertyTax;
            }
            else if (s == "VillagePropertyTax")
            {
                result = E_EscrowItemType.VillagePropertyTax;
            }
            else if (s == "WindstormInsurance")
            {
                result = E_EscrowItemType.WindstormInsurance;
            }
            else if (s == "DTC_MortgageInsurance")
            {
                result = E_EscrowItemType.DTC_MortgageInsurance;
            }
            else
            {
                result = E_EscrowItemType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_EscrowMonthlyPaymentRoundingType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Up")
            {
                result = E_EscrowMonthlyPaymentRoundingType.Up;
            }
            else if (s == "Down")
            {
                result = E_EscrowMonthlyPaymentRoundingType.Down;
            }
            else if (s == "None")
            {
                result = E_EscrowMonthlyPaymentRoundingType.None;
            }
            else
            {
                result = E_EscrowMonthlyPaymentRoundingType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_EscrowPaidByType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Buyer")
            {
                result = E_EscrowPaidByType.Buyer;
            }
            else if (s == "LenderPremium")
            {
                result = E_EscrowPaidByType.LenderPremium;
            }
            else if (s == "Seller")
            {
                result = E_EscrowPaidByType.Seller;
            }
            else
            {
                result = E_EscrowPaidByType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_EscrowPaymentFrequencyType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Annual")
            {
                result = E_EscrowPaymentFrequencyType.Annual;
            }
            else if (s == "Monthly")
            {
                result = E_EscrowPaymentFrequencyType.Monthly;
            }
            else if (s == "Quarterly")
            {
                result = E_EscrowPaymentFrequencyType.Quarterly;
            }
            else if (s == "SemiAnnual")
            {
                result = E_EscrowPaymentFrequencyType.SemiAnnual;
            }
            else if (s == "Unequal")
            {
                result = E_EscrowPaymentFrequencyType.Unequal;
            }
            else if (s == "Other")
            {
                result = E_EscrowPaymentFrequencyType.Other;
            }
            else
            {
                result = E_EscrowPaymentFrequencyType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_EscrowPremiumPaidByType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Buyer")
            {
                result = E_EscrowPremiumPaidByType.Buyer;
            }
            else if (s == "Lender")
            {
                result = E_EscrowPremiumPaidByType.Lender;
            }
            else if (s == "Seller")
            {
                result = E_EscrowPremiumPaidByType.Seller;
            }
            else
            {
                result = E_EscrowPremiumPaidByType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_EscrowPremiumPaymentType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "CollectedAtClosing")
            {
                result = E_EscrowPremiumPaymentType.CollectedAtClosing;
            }
            else if (s == "CollectAtClosing")
            {
                result = E_EscrowPremiumPaymentType.CollectAtClosing;
            }
            else if (s == "PaidOutsideOfClosing")
            {
                result = E_EscrowPremiumPaymentType.PaidOutsideOfClosing;
            }
            else if (s == "Waived")
            {
                result = E_EscrowPremiumPaymentType.Waived;
            }
            else
            {
                result = E_EscrowPremiumPaymentType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_NonPersonEntityDetailOrganizationType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Corporation")
            {
                result = E_NonPersonEntityDetailOrganizationType.Corporation;
            }
            else if (s == "LimitedLiabilityCompany")
            {
                result = E_NonPersonEntityDetailOrganizationType.LimitedLiabilityCompany;
            }
            else if (s == "Partnership")
            {
                result = E_NonPersonEntityDetailOrganizationType.Partnership;
            }
            else if (s == "SoleProprietorship")
            {
                result = E_NonPersonEntityDetailOrganizationType.SoleProprietorship;
            }
            else if (s == "Other")
            {
                result = E_NonPersonEntityDetailOrganizationType.Other;
            }
            else
            {
                result = E_NonPersonEntityDetailOrganizationType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_FhaLoanFhaAlimonyLiabilityTreatmentType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "AdditionToDebt")
            {
                result = E_FhaLoanFhaAlimonyLiabilityTreatmentType.AdditionToDebt;
            }
            else if (s == "ReductionToIncome")
            {
                result = E_FhaLoanFhaAlimonyLiabilityTreatmentType.ReductionToIncome;
            }
            else
            {
                result = E_FhaLoanFhaAlimonyLiabilityTreatmentType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_FhaLoanSectionOfActType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "203B")
            {
                result = E_FhaLoanSectionOfActType._203B;
            }
            else if (s == "203B251")
            {
                result = E_FhaLoanSectionOfActType._203B251;
            }
            else if (s == "203B2")
            {
                result = E_FhaLoanSectionOfActType._203B2;
            }
            else if (s == "203K")
            {
                result = E_FhaLoanSectionOfActType._203K;
            }
            else if (s == "203K251")
            {
                result = E_FhaLoanSectionOfActType._203K251;
            }
            else if (s == "221D2")
            {
                result = E_FhaLoanSectionOfActType._221D2;
            }
            else if (s == "221D2251")
            {
                result = E_FhaLoanSectionOfActType._221D2251;
            }
            else if (s == "234C")
            {
                result = E_FhaLoanSectionOfActType._234C;
            }
            else if (s == "234C251")
            {
                result = E_FhaLoanSectionOfActType._234C251;
            }
            else
            {
                result = E_FhaLoanSectionOfActType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_FhaVaLoanGovernmentRefinanceType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "FullDocumentation")
            {
                result = E_FhaVaLoanGovernmentRefinanceType.FullDocumentation;
            }
            else if (s == "InterestRateReductionRefinanceLoan")
            {
                result = E_FhaVaLoanGovernmentRefinanceType.InterestRateReductionRefinanceLoan;
            }
            else if (s == "StreamlineWithAppraisal")
            {
                result = E_FhaVaLoanGovernmentRefinanceType.StreamlineWithAppraisal;
            }
            else if (s == "StreamlineWithoutAppraisal")
            {
                result = E_FhaVaLoanGovernmentRefinanceType.StreamlineWithoutAppraisal;
            }
            else
            {
                result = E_FhaVaLoanGovernmentRefinanceType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_GovernmentReportingHmdaPurposeOfLoanType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "HomeImprovement")
            {
                result = E_GovernmentReportingHmdaPurposeOfLoanType.HomeImprovement;
            }
            else if (s == "HomePurchase")
            {
                result = E_GovernmentReportingHmdaPurposeOfLoanType.HomePurchase;
            }
            else if (s == "Refinancing")
            {
                result = E_GovernmentReportingHmdaPurposeOfLoanType.Refinancing;
            }
            else
            {
                result = E_GovernmentReportingHmdaPurposeOfLoanType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_GovernmentReportingHmdaPreapprovalType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "PreapprovalWasRequested")
            {
                result = E_GovernmentReportingHmdaPreapprovalType.PreapprovalWasRequested;
            }
            else if (s == "PreapprovalWasNotRequested")
            {
                result = E_GovernmentReportingHmdaPreapprovalType.PreapprovalWasNotRequested;
            }
            else if (s == "NotApplicable")
            {
                result = E_GovernmentReportingHmdaPreapprovalType.NotApplicable;
            }
            else
            {
                result = E_GovernmentReportingHmdaPreapprovalType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_InterviewerInformationApplicationTakenMethodType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "FaceToFace")
            {
                result = E_InterviewerInformationApplicationTakenMethodType.FaceToFace;
            }
            else if (s == "Mail")
            {
                result = E_InterviewerInformationApplicationTakenMethodType.Mail;
            }
            else if (s == "Telephone")
            {
                result = E_InterviewerInformationApplicationTakenMethodType.Telephone;
            }
            else if (s == "Internet")
            {
                result = E_InterviewerInformationApplicationTakenMethodType.Internet;
            }
            else
            {
                result = E_InterviewerInformationApplicationTakenMethodType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_LiabilityType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Alimony")
            {
                result = E_LiabilityType.Alimony;
            }
            else if (s == "ChildCare")
            {
                result = E_LiabilityType.ChildCare;
            }
            else if (s == "ChildSupport")
            {
                result = E_LiabilityType.ChildSupport;
            }
            else if (s == "CollectionsJudgementsAndLiens")
            {
                result = E_LiabilityType.CollectionsJudgementsAndLiens;
            }
            else if (s == "HELOC")
            {
                result = E_LiabilityType.HELOC;
            }
            else if (s == "Installment")
            {
                result = E_LiabilityType.Installment;
            }
            else if (s == "JobRelatedExpenses")
            {
                result = E_LiabilityType.JobRelatedExpenses;
            }
            else if (s == "LeasePayments")
            {
                result = E_LiabilityType.LeasePayments;
            }
            else if (s == "MortgageLoan")
            {
                result = E_LiabilityType.MortgageLoan;
            }
            else if (s == "Open30DayChargeAccount")
            {
                result = E_LiabilityType.Open30DayChargeAccount;
            }
            else if (s == "OtherLiability")
            {
                result = E_LiabilityType.OtherLiability;
            }
            else if (s == "Revolving")
            {
                result = E_LiabilityType.Revolving;
            }
            else if (s == "SeparateMaintenanceExpense")
            {
                result = E_LiabilityType.SeparateMaintenanceExpense;
            }
            else if (s == "OtherExpense")
            {
                result = E_LiabilityType.OtherExpense;
            }
            else if (s == "Taxes")
            {
                result = E_LiabilityType.Taxes;
            }
            else
            {
                result = E_LiabilityType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_ArmIndexType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "EleventhDistrictCostOfFunds")
            {
                result = E_ArmIndexType.EleventhDistrictCostOfFunds;
            }
            else if (s == "OneYearTreasury")
            {
                result = E_ArmIndexType.OneYearTreasury;
            }
            else if (s == "ThreeYearTreasury")
            {
                result = E_ArmIndexType.ThreeYearTreasury;
            }
            else if (s == "SixMonthTreasury")
            {
                result = E_ArmIndexType.SixMonthTreasury;
            }
            else if (s == "DailyCertificateOfDepositRate")
            {
                result = E_ArmIndexType.DailyCertificateOfDepositRate;
            }
            else if (s == "FNM60DayRequiredNetYield")
            {
                result = E_ArmIndexType.FNM60DayRequiredNetYield;
            }
            else if (s == "FNM_LIBOR")
            {
                result = E_ArmIndexType.FNM_LIBOR;
            }
            else if (s == "FederalCostOfFunds")
            {
                result = E_ArmIndexType.FederalCostOfFunds;
            }
            else if (s == "FRE60DayRequiredNetYield")
            {
                result = E_ArmIndexType.FRE60DayRequiredNetYield;
            }
            else if (s == "FRE_LIBOR")
            {
                result = E_ArmIndexType.FRE_LIBOR;
            }
            else if (s == "LIBOR")
            {
                result = E_ArmIndexType.LIBOR;
            }
            else if (s == "MonthlyAverageConstantMaturingTreasury")
            {
                result = E_ArmIndexType.MonthlyAverageConstantMaturingTreasury;
            }
            else if (s == "NationalAverageContractRateFHLBB")
            {
                result = E_ArmIndexType.NationalAverageContractRateFHLBB;
            }
            else if (s == "NationalMonthlyMedianCostOfFunds")
            {
                result = E_ArmIndexType.NationalMonthlyMedianCostOfFunds;
            }
            else if (s == "Other")
            {
                result = E_ArmIndexType.Other;
            }
            else if (s == "TreasuryBillDailyValue")
            {
                result = E_ArmIndexType.TreasuryBillDailyValue;
            }
            else if (s == "WallStreetJournalLIBOR")
            {
                result = E_ArmIndexType.WallStreetJournalLIBOR;
            }
            else if (s == "WeeklyAverageCertificateOfDepositRate")
            {
                result = E_ArmIndexType.WeeklyAverageCertificateOfDepositRate;
            }
            else if (s == "WeeklyAverageConstantMaturingTreasury")
            {
                result = E_ArmIndexType.WeeklyAverageConstantMaturingTreasury;
            }
            else if (s == "WeeklyAveragePrimeRate")
            {
                result = E_ArmIndexType.WeeklyAveragePrimeRate;
            }
            else if (s == "WeeklyAverageSecondaryMarketTreasuryBillInvestmentYield")
            {
                result = E_ArmIndexType.WeeklyAverageSecondaryMarketTreasuryBillInvestmentYield;
            }
            else if (s == "WeeklyAverageTreasuryAuctionAverageBondDiscountYield")
            {
                result = E_ArmIndexType.WeeklyAverageTreasuryAuctionAverageBondDiscountYield;
            }
            else if (s == "WeeklyAverageTreasuryAuctionAverageInvestmentYield")
            {
                result = E_ArmIndexType.WeeklyAverageTreasuryAuctionAverageInvestmentYield;
            }
            else
            {
                result = E_ArmIndexType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_ArmInterestRateRoundingType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Up")
            {
                result = E_ArmInterestRateRoundingType.Up;
            }
            else if (s == "Down")
            {
                result = E_ArmInterestRateRoundingType.Down;
            }
            else if (s == "Nearest")
            {
                result = E_ArmInterestRateRoundingType.Nearest;
            }
            else if (s == "None")
            {
                result = E_ArmInterestRateRoundingType.None;
            }
            else
            {
                result = E_ArmInterestRateRoundingType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_BuydownBaseDateType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "NoteDate")
            {
                result = E_BuydownBaseDateType.NoteDate;
            }
            else if (s == "FirstPaymentDate")
            {
                result = E_BuydownBaseDateType.FirstPaymentDate;
            }
            else if (s == "LastPaymentDate")
            {
                result = E_BuydownBaseDateType.LastPaymentDate;
            }
            else
            {
                result = E_BuydownBaseDateType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_BuydownContributorType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Borrower")
            {
                result = E_BuydownContributorType.Borrower;
            }
            else if (s == "Builder")
            {
                result = E_BuydownContributorType.Builder;
            }
            else if (s == "Employer")
            {
                result = E_BuydownContributorType.Employer;
            }
            else if (s == "LenderPremiumFinanced")
            {
                result = E_BuydownContributorType.LenderPremiumFinanced;
            }
            else if (s == "NonparentRelative")
            {
                result = E_BuydownContributorType.NonparentRelative;
            }
            else if (s == "Parent")
            {
                result = E_BuydownContributorType.Parent;
            }
            else if (s == "Seller")
            {
                result = E_BuydownContributorType.Seller;
            }
            else if (s == "Unassigned")
            {
                result = E_BuydownContributorType.Unassigned;
            }
            else if (s == "UnrelatedFriend")
            {
                result = E_BuydownContributorType.UnrelatedFriend;
            }
            else if (s == "Other")
            {
                result = E_BuydownContributorType.Other;
            }
            else
            {
                result = E_BuydownContributorType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_BuydownSubsidyCalculationType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "DecliningLoanBalance")
            {
                result = E_BuydownSubsidyCalculationType.DecliningLoanBalance;
            }
            else if (s == "OriginalLoanAmount")
            {
                result = E_BuydownSubsidyCalculationType.OriginalLoanAmount;
            }
            else
            {
                result = E_BuydownSubsidyCalculationType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_ContributorRoleType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Borrower")
            {
                result = E_ContributorRoleType.Borrower;
            }
            else if (s == "Builder")
            {
                result = E_ContributorRoleType.Builder;
            }
            else if (s == "Employer")
            {
                result = E_ContributorRoleType.Employer;
            }
            else if (s == "LenderPremiumFinanced")
            {
                result = E_ContributorRoleType.LenderPremiumFinanced;
            }
            else if (s == "NonparentRelative")
            {
                result = E_ContributorRoleType.NonparentRelative;
            }
            else if (s == "Parent")
            {
                result = E_ContributorRoleType.Parent;
            }
            else if (s == "Seller")
            {
                result = E_ContributorRoleType.Seller;
            }
            else if (s == "Unassigned")
            {
                result = E_ContributorRoleType.Unassigned;
            }
            else if (s == "UnrelatedFriend")
            {
                result = E_ContributorRoleType.UnrelatedFriend;
            }
            else if (s == "Other")
            {
                result = E_ContributorRoleType.Other;
            }
            else
            {
                result = E_ContributorRoleType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_LoanFeaturesDownPaymentOptionType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "ThreeTwoOption")
            {
                result = E_LoanFeaturesDownPaymentOptionType.ThreeTwoOption;
            }
            else if (s == "FivePercentOption")
            {
                result = E_LoanFeaturesDownPaymentOptionType.FivePercentOption;
            }
            else if (s == "FNM97Option")
            {
                result = E_LoanFeaturesDownPaymentOptionType.FNM97Option;
            }
            else
            {
                result = E_LoanFeaturesDownPaymentOptionType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_LoanFeaturesGseProjectClassificationType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "A_IIICondominium")
            {
                result = E_LoanFeaturesGseProjectClassificationType.A_IIICondominium;
            }
            else if (s == "ApprovedFHA_VACondominiumProjectOrSpotLoan")
            {
                result = E_LoanFeaturesGseProjectClassificationType.ApprovedFHA_VACondominiumProjectOrSpotLoan;
            }
            else if (s == "B_IICondominium")
            {
                result = E_LoanFeaturesGseProjectClassificationType.B_IICondominium;
            }
            else if (s == "C_ICondominium")
            {
                result = E_LoanFeaturesGseProjectClassificationType.C_ICondominium;
            }
            else if (s == "OneCooperative")
            {
                result = E_LoanFeaturesGseProjectClassificationType.OneCooperative;
            }
            else if (s == "TwoCooperative")
            {
                result = E_LoanFeaturesGseProjectClassificationType.TwoCooperative;
            }
            else if (s == "E_PUD")
            {
                result = E_LoanFeaturesGseProjectClassificationType.E_PUD;
            }
            else if (s == "F_PUD")
            {
                result = E_LoanFeaturesGseProjectClassificationType.F_PUD;
            }
            else if (s == "III_PUD")
            {
                result = E_LoanFeaturesGseProjectClassificationType.III_PUD;
            }
            else
            {
                result = E_LoanFeaturesGseProjectClassificationType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_LoanFeaturesGsePropertyType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Attached")
            {
                result = E_LoanFeaturesGsePropertyType.Attached;
            }
            else if (s == "Condominium")
            {
                result = E_LoanFeaturesGsePropertyType.Condominium;
            }
            else if (s == "Cooperative")
            {
                result = E_LoanFeaturesGsePropertyType.Cooperative;
            }
            else if (s == "Detached")
            {
                result = E_LoanFeaturesGsePropertyType.Detached;
            }
            else if (s == "HighRiseCondominium")
            {
                result = E_LoanFeaturesGsePropertyType.HighRiseCondominium;
            }
            else if (s == "ManufacturedHousing")
            {
                result = E_LoanFeaturesGsePropertyType.ManufacturedHousing;
            }
            else if (s == "Modular")
            {
                result = E_LoanFeaturesGsePropertyType.Modular;
            }
            else if (s == "PUD")
            {
                result = E_LoanFeaturesGsePropertyType.PUD;
            }
            else if (s == "ManufacturedHousingSingleWide")
            {
                result = E_LoanFeaturesGsePropertyType.ManufacturedHousingSingleWide;
            }
            else if (s == "ManufacturedHousingDoubleWide")
            {
                result = E_LoanFeaturesGsePropertyType.ManufacturedHousingDoubleWide;
            }
            else if (s == "DetachedCondominium")
            {
                result = E_LoanFeaturesGsePropertyType.DetachedCondominium;
            }
            else if (s == "ManufacturedHomeCondominium")
            {
                result = E_LoanFeaturesGsePropertyType.ManufacturedHomeCondominium;
            }
            else if (s == "ManufacturedHousingMultiWide")
            {
                result = E_LoanFeaturesGsePropertyType.ManufacturedHousingMultiWide;
            }
            else if (s == "ManufacturedHomeCondominiumOrPUDOrCooperative")
            {
                result = E_LoanFeaturesGsePropertyType.ManufacturedHomeCondominiumOrPUDOrCooperative;
            }
            else
            {
                result = E_LoanFeaturesGsePropertyType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_LoanFeaturesLienPriorityType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "FirstLien")
            {
                result = E_LoanFeaturesLienPriorityType.FirstLien;
            }
            else if (s == "Other")
            {
                result = E_LoanFeaturesLienPriorityType.Other;
            }
            else if (s == "SecondLien")
            {
                result = E_LoanFeaturesLienPriorityType.SecondLien;
            }
            else
            {
                result = E_LoanFeaturesLienPriorityType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_LoanFeaturesLoanClosingStatusType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Closed")
            {
                result = E_LoanFeaturesLoanClosingStatusType.Closed;
            }
            else if (s == "TableFunded")
            {
                result = E_LoanFeaturesLoanClosingStatusType.TableFunded;
            }
            else
            {
                result = E_LoanFeaturesLoanClosingStatusType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_LoanFeaturesLoanDocumentationType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Alternative")
            {
                result = E_LoanFeaturesLoanDocumentationType.Alternative;
            }
            else if (s == "FullDocumentation")
            {
                result = E_LoanFeaturesLoanDocumentationType.FullDocumentation;
            }
            else if (s == "NoDepositVerification")
            {
                result = E_LoanFeaturesLoanDocumentationType.NoDepositVerification;
            }
            else if (s == "NoDepositVerificationEmploymentVerificationOrIncomeVerification")
            {
                result = E_LoanFeaturesLoanDocumentationType.NoDepositVerificationEmploymentVerificationOrIncomeVerification;
            }
            else if (s == "NoDocumentation")
            {
                result = E_LoanFeaturesLoanDocumentationType.NoDocumentation;
            }
            else if (s == "NoEmploymentVerificationOrIncomeVerification")
            {
                result = E_LoanFeaturesLoanDocumentationType.NoEmploymentVerificationOrIncomeVerification;
            }
            else if (s == "Reduced")
            {
                result = E_LoanFeaturesLoanDocumentationType.Reduced;
            }
            else if (s == "StreamlineRefinance")
            {
                result = E_LoanFeaturesLoanDocumentationType.StreamlineRefinance;
            }
            else if (s == "NoRatio")
            {
                result = E_LoanFeaturesLoanDocumentationType.NoRatio;
            }
            else if (s == "NoIncomeNoEmploymentNoAssetsOn1003")
            {
                result = E_LoanFeaturesLoanDocumentationType.NoIncomeNoEmploymentNoAssetsOn1003;
            }
            else if (s == "NoIncomeOn1003")
            {
                result = E_LoanFeaturesLoanDocumentationType.NoIncomeOn1003;
            }
            else if (s == "NoVerificationOfStatedIncomeEmploymentOrAssets")
            {
                result = E_LoanFeaturesLoanDocumentationType.NoVerificationOfStatedIncomeEmploymentOrAssets;
            }
            else if (s == "NoVerificationOfStatedIncomeOrAssets")
            {
                result = E_LoanFeaturesLoanDocumentationType.NoVerificationOfStatedIncomeOrAssets;
            }
            else if (s == "NoVerificationOfStatedAssets")
            {
                result = E_LoanFeaturesLoanDocumentationType.NoVerificationOfStatedAssets;
            }
            else if (s == "NoVerificationOfStatedIncomeOrEmployment")
            {
                result = E_LoanFeaturesLoanDocumentationType.NoVerificationOfStatedIncomeOrEmployment;
            }
            else if (s == "NoVerificationOfStatedIncome")
            {
                result = E_LoanFeaturesLoanDocumentationType.NoVerificationOfStatedIncome;
            }
            else if (s == "VerbalVerificationOfEmployment")
            {
                result = E_LoanFeaturesLoanDocumentationType.VerbalVerificationOfEmployment;
            }
            else if (s == "OnePaystub")
            {
                result = E_LoanFeaturesLoanDocumentationType.OnePaystub;
            }
            else if (s == "OnePaystubAndVerbalVerificationOfEmployment")
            {
                result = E_LoanFeaturesLoanDocumentationType.OnePaystubAndVerbalVerificationOfEmployment;
            }
            else if (s == "OnePaystubAndOneW2AndVerbalVerificationOfEmploymentOrOneYear1040")
            {
                result = E_LoanFeaturesLoanDocumentationType.OnePaystubAndOneW2AndVerbalVerificationOfEmploymentOrOneYear1040;
            }
            else
            {
                result = E_LoanFeaturesLoanDocumentationType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_LoanFeaturesLoanRepaymentType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "InterestOnly")
            {
                result = E_LoanFeaturesLoanRepaymentType.InterestOnly;
            }
            else if (s == "NoNegativeAmortization")
            {
                result = E_LoanFeaturesLoanRepaymentType.NoNegativeAmortization;
            }
            else if (s == "PotentialNegativeAmortization")
            {
                result = E_LoanFeaturesLoanRepaymentType.PotentialNegativeAmortization;
            }
            else if (s == "ScheduledAmortization")
            {
                result = E_LoanFeaturesLoanRepaymentType.ScheduledAmortization;
            }
            else if (s == "ScheduledNegativeAmortization")
            {
                result = E_LoanFeaturesLoanRepaymentType.ScheduledNegativeAmortization;
            }
            else
            {
                result = E_LoanFeaturesLoanRepaymentType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_LoanFeaturesMiCertificationStatusType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "LenderToObtain")
            {
                result = E_LoanFeaturesMiCertificationStatusType.LenderToObtain;
            }
            else if (s == "SellerOfLoanToObtain")
            {
                result = E_LoanFeaturesMiCertificationStatusType.SellerOfLoanToObtain;
            }
            else
            {
                result = E_LoanFeaturesMiCertificationStatusType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_LoanFeaturesMiCompanyNameType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "AmerinGuaranteeCorporation")
            {
                result = E_LoanFeaturesMiCompanyNameType.AmerinGuaranteeCorporation;
            }
            else if (s == "CMG_MICompany")
            {
                result = E_LoanFeaturesMiCompanyNameType.CMG_MICompany;
            }
            else if (s == "CommonwealthMortgageAssuranceCompany")
            {
                result = E_LoanFeaturesMiCompanyNameType.CommonwealthMortgageAssuranceCompany;
            }
            else if (s == "GECapitalMICorporation")
            {
                result = E_LoanFeaturesMiCompanyNameType.GECapitalMICorporation;
            }
            else if (s == "MortgageGuarantyInsuranceCorporation")
            {
                result = E_LoanFeaturesMiCompanyNameType.MortgageGuarantyInsuranceCorporation;
            }
            else if (s == "PMI_MICorporation")
            {
                result = E_LoanFeaturesMiCompanyNameType.PMI_MICorporation;
            }
            else if (s == "RadianGuarantyIncorporated")
            {
                result = E_LoanFeaturesMiCompanyNameType.RadianGuarantyIncorporated;
            }
            else if (s == "RepublicMICompany")
            {
                result = E_LoanFeaturesMiCompanyNameType.RepublicMICompany;
            }
            else if (s == "TriadGuarantyInsuranceCorporation")
            {
                result = E_LoanFeaturesMiCompanyNameType.TriadGuarantyInsuranceCorporation;
            }
            else if (s == "UnitedGuarantyCorporation")
            {
                result = E_LoanFeaturesMiCompanyNameType.UnitedGuarantyCorporation;
            }
            else
            {
                result = E_LoanFeaturesMiCompanyNameType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_LoanFeaturesNameDocumentsDrawnInType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Broker")
            {
                result = E_LoanFeaturesNameDocumentsDrawnInType.Broker;
            }
            else if (s == "Lender")
            {
                result = E_LoanFeaturesNameDocumentsDrawnInType.Lender;
            }
            else if (s == "Investor")
            {
                result = E_LoanFeaturesNameDocumentsDrawnInType.Investor;
            }
            else
            {
                result = E_LoanFeaturesNameDocumentsDrawnInType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_LoanFeaturesPaymentFrequencyType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Annual")
            {
                result = E_LoanFeaturesPaymentFrequencyType.Annual;
            }
            else if (s == "AtMaturity")
            {
                result = E_LoanFeaturesPaymentFrequencyType.AtMaturity;
            }
            else if (s == "Biweekly")
            {
                result = E_LoanFeaturesPaymentFrequencyType.Biweekly;
            }
            else if (s == "Monthly")
            {
                result = E_LoanFeaturesPaymentFrequencyType.Monthly;
            }
            else if (s == "Other")
            {
                result = E_LoanFeaturesPaymentFrequencyType.Other;
            }
            else if (s == "Quarterly")
            {
                result = E_LoanFeaturesPaymentFrequencyType.Quarterly;
            }
            else if (s == "Semiannual")
            {
                result = E_LoanFeaturesPaymentFrequencyType.Semiannual;
            }
            else if (s == "Semimonthly")
            {
                result = E_LoanFeaturesPaymentFrequencyType.Semimonthly;
            }
            else if (s == "Weekly")
            {
                result = E_LoanFeaturesPaymentFrequencyType.Weekly;
            }
            else
            {
                result = E_LoanFeaturesPaymentFrequencyType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_LoanFeaturesFullPrepaymentPenaltyOptionType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Hard")
            {
                result = E_LoanFeaturesFullPrepaymentPenaltyOptionType.Hard;
            }
            else if (s == "Soft")
            {
                result = E_LoanFeaturesFullPrepaymentPenaltyOptionType.Soft;
            }
            else
            {
                result = E_LoanFeaturesFullPrepaymentPenaltyOptionType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_LoanFeaturesServicingTransferStatusType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Retained")
            {
                result = E_LoanFeaturesServicingTransferStatusType.Retained;
            }
            else if (s == "Released")
            {
                result = E_LoanFeaturesServicingTransferStatusType.Released;
            }
            else
            {
                result = E_LoanFeaturesServicingTransferStatusType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_LoanFeaturesEstimatedPrepaidDaysPaidByType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Buyer")
            {
                result = E_LoanFeaturesEstimatedPrepaidDaysPaidByType.Buyer;
            }
            else if (s == "Lender")
            {
                result = E_LoanFeaturesEstimatedPrepaidDaysPaidByType.Lender;
            }
            else if (s == "Other")
            {
                result = E_LoanFeaturesEstimatedPrepaidDaysPaidByType.Other;
            }
            else if (s == "Seller")
            {
                result = E_LoanFeaturesEstimatedPrepaidDaysPaidByType.Seller;
            }
            else
            {
                result = E_LoanFeaturesEstimatedPrepaidDaysPaidByType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_LoanFeaturesCounselingConfirmationType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "AmericanHomeownerEducationInstituteApprovedCounseling")
            {
                result = E_LoanFeaturesCounselingConfirmationType.AmericanHomeownerEducationInstituteApprovedCounseling;
            }
            else if (s == "LenderTrainedCounseling")
            {
                result = E_LoanFeaturesCounselingConfirmationType.LenderTrainedCounseling;
            }
            else if (s == "NoBorrowerCounseling")
            {
                result = E_LoanFeaturesCounselingConfirmationType.NoBorrowerCounseling;
            }
            else if (s == "ThirdPartyCounseling")
            {
                result = E_LoanFeaturesCounselingConfirmationType.ThirdPartyCounseling;
            }
            else
            {
                result = E_LoanFeaturesCounselingConfirmationType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_LateChargeType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "FlatDollarAmount")
            {
                result = E_LateChargeType.FlatDollarAmount;
            }
            else if (s == "NoLateCharges")
            {
                result = E_LateChargeType.NoLateCharges;
            }
            else if (s == "PercentOfPrincipalAndInterest")
            {
                result = E_LateChargeType.PercentOfPrincipalAndInterest;
            }
            else if (s == "PercentageOfDelinquentInterest")
            {
                result = E_LateChargeType.PercentageOfDelinquentInterest;
            }
            else if (s == "PercentageOfNetPayment")
            {
                result = E_LateChargeType.PercentageOfNetPayment;
            }
            else if (s == "PercentageOfPrinicipalBalance")
            {
                result = E_LateChargeType.PercentageOfPrinicipalBalance;
            }
            else if (s == "PercentageOfTotalPayment")
            {
                result = E_LateChargeType.PercentageOfTotalPayment;
            }
            else if (s == "PercentageOfPrincipalBalance")
            {
                result = E_LateChargeType.PercentageOfPrincipalBalance;
            }
            else
            {
                result = E_LateChargeType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_PaymentAdjustmentCalculationType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "AddFixedDollarAmountToTheCurrentPayment")
            {
                result = E_PaymentAdjustmentCalculationType.AddFixedDollarAmountToTheCurrentPayment;
            }
            else if (s == "AddPercentToCurrentPaymentAmount")
            {
                result = E_PaymentAdjustmentCalculationType.AddPercentToCurrentPaymentAmount;
            }
            else if (s == "AddPercentToEffectivePaymentRate")
            {
                result = E_PaymentAdjustmentCalculationType.AddPercentToEffectivePaymentRate;
            }
            else
            {
                result = E_PaymentAdjustmentCalculationType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_RateAdjustmentCalculationType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "AddPercentToCurrentRate")
            {
                result = E_RateAdjustmentCalculationType.AddPercentToCurrentRate;
            }
            else if (s == "AddPercentToOriginalRate")
            {
                result = E_RateAdjustmentCalculationType.AddPercentToOriginalRate;
            }
            else if (s == "IndexPlusMargin")
            {
                result = E_RateAdjustmentCalculationType.IndexPlusMargin;
            }
            else
            {
                result = E_RateAdjustmentCalculationType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_HelocDailyPeriodicInterestRateCalculationType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "360")
            {
                result = E_HelocDailyPeriodicInterestRateCalculationType._360;
            }
            else if (s == "365")
            {
                result = E_HelocDailyPeriodicInterestRateCalculationType._365;
            }
            else
            {
                result = E_HelocDailyPeriodicInterestRateCalculationType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_LoanPurposeType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "ConstructionOnly")
            {
                result = E_LoanPurposeType.ConstructionOnly;
            }
            else if (s == "ConstructionToPermanent")
            {
                result = E_LoanPurposeType.ConstructionToPermanent;
            }
            else if (s == "Other")
            {
                result = E_LoanPurposeType.Other;
            }
            else if (s == "Purchase")
            {
                result = E_LoanPurposeType.Purchase;
            }
            else if (s == "Refinance")
            {
                result = E_LoanPurposeType.Refinance;
            }
            else if (s == "Unknown")
            {
                result = E_LoanPurposeType.Unknown;
            }
            else
            {
                result = E_LoanPurposeType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_LoanPurposePropertyRightsType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "FeeSimple")
            {
                result = E_LoanPurposePropertyRightsType.FeeSimple;
            }
            else if (s == "Leasehold")
            {
                result = E_LoanPurposePropertyRightsType.Leasehold;
            }
            else
            {
                result = E_LoanPurposePropertyRightsType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_LoanPurposePropertyUsageType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Investment")
            {
                result = E_LoanPurposePropertyUsageType.Investment;
            }
            else if (s == "Investor")
            {
                result = E_LoanPurposePropertyUsageType.Investor;
            }
            else if (s == "PrimaryResidence")
            {
                result = E_LoanPurposePropertyUsageType.PrimaryResidence;
            }
            else if (s == "SecondHome")
            {
                result = E_LoanPurposePropertyUsageType.SecondHome;
            }
            else
            {
                result = E_LoanPurposePropertyUsageType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_ConstructionRefinanceDataGseRefinancePurposeType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "CashOutDebtConsolidation")
            {
                result = E_ConstructionRefinanceDataGseRefinancePurposeType.CashOutDebtConsolidation;
            }
            else if (s == "CashOutHomeImprovement")
            {
                result = E_ConstructionRefinanceDataGseRefinancePurposeType.CashOutHomeImprovement;
            }
            else if (s == "CashOutLimited")
            {
                result = E_ConstructionRefinanceDataGseRefinancePurposeType.CashOutLimited;
            }
            else if (s == "CashOutOther")
            {
                result = E_ConstructionRefinanceDataGseRefinancePurposeType.CashOutOther;
            }
            else if (s == "NoCashOutFHAStreamlinedRefinance")
            {
                result = E_ConstructionRefinanceDataGseRefinancePurposeType.NoCashOutFHAStreamlinedRefinance;
            }
            else if (s == "NoCashOutFREOwnedRefinance")
            {
                result = E_ConstructionRefinanceDataGseRefinancePurposeType.NoCashOutFREOwnedRefinance;
            }
            else if (s == "NoCashOutOther")
            {
                result = E_ConstructionRefinanceDataGseRefinancePurposeType.NoCashOutOther;
            }
            else if (s == "NoCashOutStreamlinedRefinance")
            {
                result = E_ConstructionRefinanceDataGseRefinancePurposeType.NoCashOutStreamlinedRefinance;
            }
            else if (s == "ChangeInRateTerm")
            {
                result = E_ConstructionRefinanceDataGseRefinancePurposeType.ChangeInRateTerm;
            }
            else
            {
                result = E_ConstructionRefinanceDataGseRefinancePurposeType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_ConstructionRefinanceDataRefinanceImprovementsType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Made")
            {
                result = E_ConstructionRefinanceDataRefinanceImprovementsType.Made;
            }
            else if (s == "ToBeMade")
            {
                result = E_ConstructionRefinanceDataRefinanceImprovementsType.ToBeMade;
            }
            else if (s == "Unknown")
            {
                result = E_ConstructionRefinanceDataRefinanceImprovementsType.Unknown;
            }
            else
            {
                result = E_ConstructionRefinanceDataRefinanceImprovementsType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_ConstructionRefinanceDataConstructionPurposeType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "ConstructionOnly")
            {
                result = E_ConstructionRefinanceDataConstructionPurposeType.ConstructionOnly;
            }
            else if (s == "ConstructionToPermanent")
            {
                result = E_ConstructionRefinanceDataConstructionPurposeType.ConstructionToPermanent;
            }
            else
            {
                result = E_ConstructionRefinanceDataConstructionPurposeType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_MersMersRegistrationStatusType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Deactivated")
            {
                result = E_MersMersRegistrationStatusType.Deactivated;
            }
            else if (s == "NotRegistered")
            {
                result = E_MersMersRegistrationStatusType.NotRegistered;
            }
            else if (s == "PreRegistered")
            {
                result = E_MersMersRegistrationStatusType.PreRegistered;
            }
            else if (s == "Registered")
            {
                result = E_MersMersRegistrationStatusType.Registered;
            }
            else if (s == "Validated")
            {
                result = E_MersMersRegistrationStatusType.Validated;
            }
            else if (s == "Other")
            {
                result = E_MersMersRegistrationStatusType.Other;
            }
            else
            {
                result = E_MersMersRegistrationStatusType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_MiDataMiDurationType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Annual")
            {
                result = E_MiDataMiDurationType.Annual;
            }
            else if (s == "NotApplicable")
            {
                result = E_MiDataMiDurationType.NotApplicable;
            }
            else if (s == "PeriodicMonthly")
            {
                result = E_MiDataMiDurationType.PeriodicMonthly;
            }
            else if (s == "SingleLifeOfLoan")
            {
                result = E_MiDataMiDurationType.SingleLifeOfLoan;
            }
            else if (s == "SingleSpecific")
            {
                result = E_MiDataMiDurationType.SingleSpecific;
            }
            else
            {
                result = E_MiDataMiDurationType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_MiDataMiInitialPremiumAtClosingType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Deferred")
            {
                result = E_MiDataMiInitialPremiumAtClosingType.Deferred;
            }
            else if (s == "Prepaid")
            {
                result = E_MiDataMiInitialPremiumAtClosingType.Prepaid;
            }
            else
            {
                result = E_MiDataMiInitialPremiumAtClosingType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_MiDataMiLtvCutoffType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "AppraisedValue")
            {
                result = E_MiDataMiLtvCutoffType.AppraisedValue;
            }
            else if (s == "SalesPrice")
            {
                result = E_MiDataMiLtvCutoffType.SalesPrice;
            }
            else
            {
                result = E_MiDataMiLtvCutoffType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_MiDataMiPremiumPaymentType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "BorrowerPaid")
            {
                result = E_MiDataMiPremiumPaymentType.BorrowerPaid;
            }
            else if (s == "BothBorrowerAndLenderPaid")
            {
                result = E_MiDataMiPremiumPaymentType.BothBorrowerAndLenderPaid;
            }
            else if (s == "Financed")
            {
                result = E_MiDataMiPremiumPaymentType.Financed;
            }
            else if (s == "LenderPaid")
            {
                result = E_MiDataMiPremiumPaymentType.LenderPaid;
            }
            else if (s == "PaidFromEscrow")
            {
                result = E_MiDataMiPremiumPaymentType.PaidFromEscrow;
            }
            else if (s == "Prepaid")
            {
                result = E_MiDataMiPremiumPaymentType.Prepaid;
            }
            else
            {
                result = E_MiDataMiPremiumPaymentType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_MiDataMiPremiumRefundableType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "NotRefundable")
            {
                result = E_MiDataMiPremiumRefundableType.NotRefundable;
            }
            else if (s == "Refundable")
            {
                result = E_MiDataMiPremiumRefundableType.Refundable;
            }
            else if (s == "RefundableWithLimits")
            {
                result = E_MiDataMiPremiumRefundableType.RefundableWithLimits;
            }
            else
            {
                result = E_MiDataMiPremiumRefundableType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_MiDataMiRenewalCalculationType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Constant")
            {
                result = E_MiDataMiRenewalCalculationType.Constant;
            }
            else if (s == "Declining")
            {
                result = E_MiDataMiRenewalCalculationType.Declining;
            }
            else if (s == "NoRenewals")
            {
                result = E_MiDataMiRenewalCalculationType.NoRenewals;
            }
            else if (s == "NotApplicable")
            {
                result = E_MiDataMiRenewalCalculationType.NotApplicable;
            }
            else
            {
                result = E_MiDataMiRenewalCalculationType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_MiDataMiSourceType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "FHA")
            {
                result = E_MiDataMiSourceType.FHA;
            }
            else if (s == "PMI")
            {
                result = E_MiDataMiSourceType.PMI;
            }
            else
            {
                result = E_MiDataMiSourceType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_MiPremiumTaxCodeType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "AllTaxes")
            {
                result = E_MiPremiumTaxCodeType.AllTaxes;
            }
            else if (s == "County")
            {
                result = E_MiPremiumTaxCodeType.County;
            }
            else if (s == "Municipal")
            {
                result = E_MiPremiumTaxCodeType.Municipal;
            }
            else if (s == "State")
            {
                result = E_MiPremiumTaxCodeType.State;
            }
            else
            {
                result = E_MiPremiumTaxCodeType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_MiRenewalPremiumMonthlyPaymentRoundingType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Up")
            {
                result = E_MiRenewalPremiumMonthlyPaymentRoundingType.Up;
            }
            else if (s == "Down")
            {
                result = E_MiRenewalPremiumMonthlyPaymentRoundingType.Down;
            }
            else if (s == "None")
            {
                result = E_MiRenewalPremiumMonthlyPaymentRoundingType.None;
            }
            else
            {
                result = E_MiRenewalPremiumMonthlyPaymentRoundingType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_MiRenewalPremiumSequence result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "First")
            {
                result = E_MiRenewalPremiumSequence.First;
            }
            else if (s == "Second")
            {
                result = E_MiRenewalPremiumSequence.Second;
            }
            else if (s == "Third")
            {
                result = E_MiRenewalPremiumSequence.Third;
            }
            else if (s == "Fourth")
            {
                result = E_MiRenewalPremiumSequence.Fourth;
            }
            else if (s == "Fifth")
            {
                result = E_MiRenewalPremiumSequence.Fifth;
            }
            else
            {
                result = E_MiRenewalPremiumSequence.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_MortgageTermsLoanAmortizationType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "AdjustableRate")
            {
                result = E_MortgageTermsLoanAmortizationType.AdjustableRate;
            }
            else if (s == "Fixed")
            {
                result = E_MortgageTermsLoanAmortizationType.Fixed;
            }
            else if (s == "GraduatedPaymentMortgage")
            {
                result = E_MortgageTermsLoanAmortizationType.GraduatedPaymentMortgage;
            }
            else if (s == "GrowingEquityMortgage")
            {
                result = E_MortgageTermsLoanAmortizationType.GrowingEquityMortgage;
            }
            else if (s == "OtherAmortizationType")
            {
                result = E_MortgageTermsLoanAmortizationType.OtherAmortizationType;
            }
            else
            {
                result = E_MortgageTermsLoanAmortizationType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_MortgageTermsMortgageType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Conventional")
            {
                result = E_MortgageTermsMortgageType.Conventional;
            }
            else if (s == "FarmersHomeAdministration")
            {
                result = E_MortgageTermsMortgageType.FarmersHomeAdministration;
            }
            else if (s == "FHA")
            {
                result = E_MortgageTermsMortgageType.FHA;
            }
            else if (s == "Other")
            {
                result = E_MortgageTermsMortgageType.Other;
            }
            else if (s == "VA")
            {
                result = E_MortgageTermsMortgageType.VA;
            }
            else if (s == "HELOC")
            {
                result = E_MortgageTermsMortgageType.HELOC;
            }
            else
            {
                result = E_MortgageTermsMortgageType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_PropertyBuildingStatusType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Existing")
            {
                result = E_PropertyBuildingStatusType.Existing;
            }
            else if (s == "Proposed")
            {
                result = E_PropertyBuildingStatusType.Proposed;
            }
            else if (s == "SubjectToAlterationImprovementRepairAndRehabilitation")
            {
                result = E_PropertyBuildingStatusType.SubjectToAlterationImprovementRepairAndRehabilitation;
            }
            else if (s == "SubstantiallyRehabilitated")
            {
                result = E_PropertyBuildingStatusType.SubstantiallyRehabilitated;
            }
            else if (s == "UnderConstruction")
            {
                result = E_PropertyBuildingStatusType.UnderConstruction;
            }
            else
            {
                result = E_PropertyBuildingStatusType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_LegalDescriptionType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "MetesAndBounds")
            {
                result = E_LegalDescriptionType.MetesAndBounds;
            }
            else if (s == "Other")
            {
                result = E_LegalDescriptionType.Other;
            }
            else
            {
                result = E_LegalDescriptionType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_ValuationMethodType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "FNM1004")
            {
                result = E_ValuationMethodType.FNM1004;
            }
            else if (s == "EmployeeRelocationCouncil2001")
            {
                result = E_ValuationMethodType.EmployeeRelocationCouncil2001;
            }
            else if (s == "FNM1073")
            {
                result = E_ValuationMethodType.FNM1073;
            }
            else if (s == "FNM1025")
            {
                result = E_ValuationMethodType.FNM1025;
            }
            else if (s == "FNM2055Exterior")
            {
                result = E_ValuationMethodType.FNM2055Exterior;
            }
            else if (s == "FNM2065")
            {
                result = E_ValuationMethodType.FNM2065;
            }
            else if (s == "FRE2070Interior")
            {
                result = E_ValuationMethodType.FRE2070Interior;
            }
            else if (s == "FRE2070Exterior")
            {
                result = E_ValuationMethodType.FRE2070Exterior;
            }
            else if (s == "FNM2075")
            {
                result = E_ValuationMethodType.FNM2075;
            }
            else if (s == "BrokerPriceOpinion")
            {
                result = E_ValuationMethodType.BrokerPriceOpinion;
            }
            else if (s == "AutomatedValuationModel")
            {
                result = E_ValuationMethodType.AutomatedValuationModel;
            }
            else if (s == "TaxValuation")
            {
                result = E_ValuationMethodType.TaxValuation;
            }
            else if (s == "DriveBy")
            {
                result = E_ValuationMethodType.DriveBy;
            }
            else if (s == "FullAppraisal")
            {
                result = E_ValuationMethodType.FullAppraisal;
            }
            else if (s == "None")
            {
                result = E_ValuationMethodType.None;
            }
            else if (s == "FNM2055InteriorAndExterior")
            {
                result = E_ValuationMethodType.FNM2055InteriorAndExterior;
            }
            else if (s == "FNM2095Exterior")
            {
                result = E_ValuationMethodType.FNM2095Exterior;
            }
            else if (s == "FNM2095InteriorAndExterior")
            {
                result = E_ValuationMethodType.FNM2095InteriorAndExterior;
            }
            else if (s == "PriorAppraisalUsed")
            {
                result = E_ValuationMethodType.PriorAppraisalUsed;
            }
            else if (s == "Form261805")
            {
                result = E_ValuationMethodType.Form261805;
            }
            else if (s == "Form268712")
            {
                result = E_ValuationMethodType.Form268712;
            }
            else if (s == "Other")
            {
                result = E_ValuationMethodType.Other;
            }
            else
            {
                result = E_ValuationMethodType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_DetailsRecordingJurisdictionType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "County")
            {
                result = E_DetailsRecordingJurisdictionType.County;
            }
            else if (s == "Parish")
            {
                result = E_DetailsRecordingJurisdictionType.Parish;
            }
            else if (s == "Other")
            {
                result = E_DetailsRecordingJurisdictionType.Other;
            }
            else
            {
                result = E_DetailsRecordingJurisdictionType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_ProposedHousingExpenseHousingExpenseType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "FirstMortgagePrincipalAndInterest")
            {
                result = E_ProposedHousingExpenseHousingExpenseType.FirstMortgagePrincipalAndInterest;
            }
            else if (s == "GroundRent")
            {
                result = E_ProposedHousingExpenseHousingExpenseType.GroundRent;
            }
            else if (s == "HazardInsurance")
            {
                result = E_ProposedHousingExpenseHousingExpenseType.HazardInsurance;
            }
            else if (s == "HomeownersAssociationDuesAndCondominiumFees")
            {
                result = E_ProposedHousingExpenseHousingExpenseType.HomeownersAssociationDuesAndCondominiumFees;
            }
            else if (s == "MI")
            {
                result = E_ProposedHousingExpenseHousingExpenseType.MI;
            }
            else if (s == "OtherHousingExpense")
            {
                result = E_ProposedHousingExpenseHousingExpenseType.OtherHousingExpense;
            }
            else if (s == "OtherMortgageLoanPrincipalAndInterest")
            {
                result = E_ProposedHousingExpenseHousingExpenseType.OtherMortgageLoanPrincipalAndInterest;
            }
            else if (s == "RealEstateTax")
            {
                result = E_ProposedHousingExpenseHousingExpenseType.RealEstateTax;
            }
            else
            {
                result = E_ProposedHousingExpenseHousingExpenseType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_ReoPropertyDispositionStatusType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "PendingSale")
            {
                result = E_ReoPropertyDispositionStatusType.PendingSale;
            }
            else if (s == "RetainForRental")
            {
                result = E_ReoPropertyDispositionStatusType.RetainForRental;
            }
            else if (s == "RetainForPrimaryOrSecondaryResidence")
            {
                result = E_ReoPropertyDispositionStatusType.RetainForPrimaryOrSecondaryResidence;
            }
            else if (s == "Sold")
            {
                result = E_ReoPropertyDispositionStatusType.Sold;
            }
            else
            {
                result = E_ReoPropertyDispositionStatusType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_ReoPropertyGsePropertyType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "SingleFamily")
            {
                result = E_ReoPropertyGsePropertyType.SingleFamily;
            }
            else if (s == "Condominium")
            {
                result = E_ReoPropertyGsePropertyType.Condominium;
            }
            else if (s == "Townhouse")
            {
                result = E_ReoPropertyGsePropertyType.Townhouse;
            }
            else if (s == "Cooperative")
            {
                result = E_ReoPropertyGsePropertyType.Cooperative;
            }
            else if (s == "TwoToFourUnitProperty")
            {
                result = E_ReoPropertyGsePropertyType.TwoToFourUnitProperty;
            }
            else if (s == "MultifamilyMoreThanFourUnits")
            {
                result = E_ReoPropertyGsePropertyType.MultifamilyMoreThanFourUnits;
            }
            else if (s == "ManufacturedMobileHome")
            {
                result = E_ReoPropertyGsePropertyType.ManufacturedMobileHome;
            }
            else if (s == "CommercialNonResidential")
            {
                result = E_ReoPropertyGsePropertyType.CommercialNonResidential;
            }
            else if (s == "MixedUseResidential")
            {
                result = E_ReoPropertyGsePropertyType.MixedUseResidential;
            }
            else if (s == "Farm")
            {
                result = E_ReoPropertyGsePropertyType.Farm;
            }
            else if (s == "HomeAndBusinessCombined")
            {
                result = E_ReoPropertyGsePropertyType.HomeAndBusinessCombined;
            }
            else if (s == "Land")
            {
                result = E_ReoPropertyGsePropertyType.Land;
            }
            else
            {
                result = E_ReoPropertyGsePropertyType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_RespaFeeRespaSectionClassificationType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "100:GrossAmountDueFromBorrower")
            {
                result = E_RespaFeeRespaSectionClassificationType._100_GrossAmountDueFromBorrower;
            }
            else if (s == "200:AmountsPaidByOrInBehalfOfBorrower")
            {
                result = E_RespaFeeRespaSectionClassificationType._200_AmountsPaidByOrInBehalfOfBorrower;
            }
            else if (s == "300:CashAtSettlementFromToBorrower")
            {
                result = E_RespaFeeRespaSectionClassificationType._300_CashAtSettlementFromToBorrower;
            }
            else if (s == "400:GrossAmountDueToSeller")
            {
                result = E_RespaFeeRespaSectionClassificationType._400_GrossAmountDueToSeller;
            }
            else if (s == "500:ReductionsInAmountDueSeller")
            {
                result = E_RespaFeeRespaSectionClassificationType._500_ReductionsInAmountDueSeller;
            }
            else if (s == "600:CashAtSettlementToFromSeller")
            {
                result = E_RespaFeeRespaSectionClassificationType._600_CashAtSettlementToFromSeller;
            }
            else if (s == "700:DivisionOfCommissions")
            {
                result = E_RespaFeeRespaSectionClassificationType._700_DivisionOfCommissions;
            }
            else if (s == "800:LoanFees")
            {
                result = E_RespaFeeRespaSectionClassificationType._800_LoanFees;
            }
            else if (s == "900:LenderRequiredPaidInAdvance")
            {
                result = E_RespaFeeRespaSectionClassificationType._900_LenderRequiredPaidInAdvance;
            }
            else if (s == "1000:ReservesDepositedWithLender")
            {
                result = E_RespaFeeRespaSectionClassificationType._1000_ReservesDepositedWithLender;
            }
            else if (s == "1100:TitleCharges")
            {
                result = E_RespaFeeRespaSectionClassificationType._1100_TitleCharges;
            }
            else if (s == "1200:RecordingAndTransferCharges")
            {
                result = E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges;
            }
            else if (s == "1300:AdditionalSettlementCharges")
            {
                result = E_RespaFeeRespaSectionClassificationType._1300_AdditionalSettlementCharges;
            }
            else
            {
                result = E_RespaFeeRespaSectionClassificationType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_RespaFeeResponsiblePartyType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Buyer")
            {
                result = E_RespaFeeResponsiblePartyType.Buyer;
            }
            else if (s == "Seller")
            {
                result = E_RespaFeeResponsiblePartyType.Seller;
            }
            else
            {
                result = E_RespaFeeResponsiblePartyType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_RespaFeeType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "203KDiscountOnRepairs")
            {
                result = E_RespaFeeType._203KDiscountOnRepairs;
            }
            else if (s == "203KPermits")
            {
                result = E_RespaFeeType._203KPermits;
            }
            else if (s == "203KArchitecturalAndEngineeringFee")
            {
                result = E_RespaFeeType._203KArchitecturalAndEngineeringFee;
            }
            else if (s == "203KInspectionFee")
            {
                result = E_RespaFeeType._203KInspectionFee;
            }
            else if (s == "203KSupplementalOriginationFee")
            {
                result = E_RespaFeeType._203KSupplementalOriginationFee;
            }
            else if (s == "203KConsultantFee")
            {
                result = E_RespaFeeType._203KConsultantFee;
            }
            else if (s == "203KTitleUpdate")
            {
                result = E_RespaFeeType._203KTitleUpdate;
            }
            else if (s == "AbstractOrTitleSearchFee")
            {
                result = E_RespaFeeType.AbstractOrTitleSearchFee;
            }
            else if (s == "AmortizationFee")
            {
                result = E_RespaFeeType.AmortizationFee;
            }
            else if (s == "ApplicationFee")
            {
                result = E_RespaFeeType.ApplicationFee;
            }
            else if (s == "AppraisalFee")
            {
                result = E_RespaFeeType.AppraisalFee;
            }
            else if (s == "AssignmentFee")
            {
                result = E_RespaFeeType.AssignmentFee;
            }
            else if (s == "AssignmentRecordingFee")
            {
                result = E_RespaFeeType.AssignmentRecordingFee;
            }
            else if (s == "AssumptionFee")
            {
                result = E_RespaFeeType.AssumptionFee;
            }
            else if (s == "AttorneyFee")
            {
                result = E_RespaFeeType.AttorneyFee;
            }
            else if (s == "BondReviewFee")
            {
                result = E_RespaFeeType.BondReviewFee;
            }
            else if (s == "CityCountyDeedTaxStampFee")
            {
                result = E_RespaFeeType.CityCountyDeedTaxStampFee;
            }
            else if (s == "CityCountyMortgageTaxStampFee")
            {
                result = E_RespaFeeType.CityCountyMortgageTaxStampFee;
            }
            else if (s == "CLOAccessFee")
            {
                result = E_RespaFeeType.CLOAccessFee;
            }
            else if (s == "CommitmentFee")
            {
                result = E_RespaFeeType.CommitmentFee;
            }
            else if (s == "CopyFaxFee")
            {
                result = E_RespaFeeType.CopyFaxFee;
            }
            else if (s == "CourierFee")
            {
                result = E_RespaFeeType.CourierFee;
            }
            else if (s == "CreditReportFee")
            {
                result = E_RespaFeeType.CreditReportFee;
            }
            else if (s == "DeedRecordingFee")
            {
                result = E_RespaFeeType.DeedRecordingFee;
            }
            else if (s == "DocumentPreparationFee")
            {
                result = E_RespaFeeType.DocumentPreparationFee;
            }
            else if (s == "DocumentaryStampFee")
            {
                result = E_RespaFeeType.DocumentaryStampFee;
            }
            else if (s == "EscrowWaiverFee")
            {
                result = E_RespaFeeType.EscrowWaiverFee;
            }
            else if (s == "FloodCertification")
            {
                result = E_RespaFeeType.FloodCertification;
            }
            else if (s == "GeneralCounselFee")
            {
                result = E_RespaFeeType.GeneralCounselFee;
            }
            else if (s == "InspectionFee")
            {
                result = E_RespaFeeType.InspectionFee;
            }
            else if (s == "LoanDiscountPoints")
            {
                result = E_RespaFeeType.LoanDiscountPoints;
            }
            else if (s == "LoanOriginationFee")
            {
                result = E_RespaFeeType.LoanOriginationFee;
            }
            else if (s == "ModificationFee")
            {
                result = E_RespaFeeType.ModificationFee;
            }
            else if (s == "MortgageBrokerFee")
            {
                result = E_RespaFeeType.MortgageBrokerFee;
            }
            else if (s == "MortgageRecordingFee")
            {
                result = E_RespaFeeType.MortgageRecordingFee;
            }
            else if (s == "MunicipalLienCertificateFee")
            {
                result = E_RespaFeeType.MunicipalLienCertificateFee;
            }
            else if (s == "MunicipalLienCertificateRecordingFee")
            {
                result = E_RespaFeeType.MunicipalLienCertificateRecordingFee;
            }
            else if (s == "NewLoanAdministrationFee")
            {
                result = E_RespaFeeType.NewLoanAdministrationFee;
            }
            else if (s == "NotaryFee")
            {
                result = E_RespaFeeType.NotaryFee;
            }
            else if (s == "Other")
            {
                result = E_RespaFeeType.Other;
            }
            else if (s == "PestInspectionFee")
            {
                result = E_RespaFeeType.PestInspectionFee;
            }
            else if (s == "ProcessingFee")
            {
                result = E_RespaFeeType.ProcessingFee;
            }
            else if (s == "RedrawFee")
            {
                result = E_RespaFeeType.RedrawFee;
            }
            else if (s == "RealEstateCommission")
            {
                result = E_RespaFeeType.RealEstateCommission;
            }
            else if (s == "ReinspectionFee")
            {
                result = E_RespaFeeType.ReinspectionFee;
            }
            else if (s == "ReleaseRecordingFee")
            {
                result = E_RespaFeeType.ReleaseRecordingFee;
            }
            else if (s == "RuralHousingFee")
            {
                result = E_RespaFeeType.RuralHousingFee;
            }
            else if (s == "SettlementOrClosingFee")
            {
                result = E_RespaFeeType.SettlementOrClosingFee;
            }
            else if (s == "StateDeedTaxStampFee")
            {
                result = E_RespaFeeType.StateDeedTaxStampFee;
            }
            else if (s == "StateMortgageTaxStampFee")
            {
                result = E_RespaFeeType.StateMortgageTaxStampFee;
            }
            else if (s == "SurveyFee")
            {
                result = E_RespaFeeType.SurveyFee;
            }
            else if (s == "TaxRelatedServiceFee")
            {
                result = E_RespaFeeType.TaxRelatedServiceFee;
            }
            else if (s == "TitleExaminationFee")
            {
                result = E_RespaFeeType.TitleExaminationFee;
            }
            else if (s == "TitleInsuranceBinderFee")
            {
                result = E_RespaFeeType.TitleInsuranceBinderFee;
            }
            else if (s == "TitleInsuranceFee")
            {
                result = E_RespaFeeType.TitleInsuranceFee;
            }
            else if (s == "UnderwritingFee")
            {
                result = E_RespaFeeType.UnderwritingFee;
            }
            else
            {
                result = E_RespaFeeType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_RespaFeePaidToType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Broker")
            {
                result = E_RespaFeePaidToType.Broker;
            }
            else if (s == "Lender")
            {
                result = E_RespaFeePaidToType.Lender;
            }
            else if (s == "Investor")
            {
                result = E_RespaFeePaidToType.Investor;
            }
            else if (s == "Other")
            {
                result = E_RespaFeePaidToType.Other;
            }
            else
            {
                result = E_RespaFeePaidToType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_PaymentCollectedByType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Broker")
            {
                result = E_PaymentCollectedByType.Broker;
            }
            else if (s == "Lender")
            {
                result = E_PaymentCollectedByType.Lender;
            }
            else if (s == "Investor")
            {
                result = E_PaymentCollectedByType.Investor;
            }
            else
            {
                result = E_PaymentCollectedByType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_PaymentPaidByType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Buyer")
            {
                result = E_PaymentPaidByType.Buyer;
            }
            else if (s == "Lender")
            {
                result = E_PaymentPaidByType.Lender;
            }
            else if (s == "Seller")
            {
                result = E_PaymentPaidByType.Seller;
            }
            else if (s == "ThirdParty")
            {
                result = E_PaymentPaidByType.ThirdParty;
            }
            else
            {
                result = E_PaymentPaidByType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_PaymentProcessType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Closing")
            {
                result = E_PaymentProcessType.Closing;
            }
            else if (s == "Processing")
            {
                result = E_PaymentProcessType.Processing;
            }
            else
            {
                result = E_PaymentProcessType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_TitleHolderLandTrustType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "IllinoisLandTrust")
            {
                result = E_TitleHolderLandTrustType.IllinoisLandTrust;
            }
            else
            {
                result = E_TitleHolderLandTrustType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_PurchaseCreditSourceType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "BorrowerPaidOutsideClosing")
            {
                result = E_PurchaseCreditSourceType.BorrowerPaidOutsideClosing;
            }
            else if (s == "PropertySeller")
            {
                result = E_PurchaseCreditSourceType.PropertySeller;
            }
            else if (s == "Lender")
            {
                result = E_PurchaseCreditSourceType.Lender;
            }
            else if (s == "NonParentRelative")
            {
                result = E_PurchaseCreditSourceType.NonParentRelative;
            }
            else
            {
                result = E_PurchaseCreditSourceType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_PurchaseCreditType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "EarnestMoney")
            {
                result = E_PurchaseCreditType.EarnestMoney;
            }
            else if (s == "RelocationFunds")
            {
                result = E_PurchaseCreditType.RelocationFunds;
            }
            else if (s == "EmployerAssistedHousing")
            {
                result = E_PurchaseCreditType.EmployerAssistedHousing;
            }
            else if (s == "LeasePurchaseFund")
            {
                result = E_PurchaseCreditType.LeasePurchaseFund;
            }
            else if (s == "Other")
            {
                result = E_PurchaseCreditType.Other;
            }
            else
            {
                result = E_PurchaseCreditType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_BorrowerPrintPositionType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Borrower")
            {
                result = E_BorrowerPrintPositionType.Borrower;
            }
            else if (s == "CoBorrower")
            {
                result = E_BorrowerPrintPositionType.CoBorrower;
            }
            else
            {
                result = E_BorrowerPrintPositionType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_BorrowerJointAssetLiabilityReportingType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Jointly")
            {
                result = E_BorrowerJointAssetLiabilityReportingType.Jointly;
            }
            else if (s == "NotJointly")
            {
                result = E_BorrowerJointAssetLiabilityReportingType.NotJointly;
            }
            else
            {
                result = E_BorrowerJointAssetLiabilityReportingType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_BorrowerMaritalStatusType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Married")
            {
                result = E_BorrowerMaritalStatusType.Married;
            }
            else if (s == "NotProvided")
            {
                result = E_BorrowerMaritalStatusType.NotProvided;
            }
            else if (s == "Separated")
            {
                result = E_BorrowerMaritalStatusType.Separated;
            }
            else if (s == "Unknown")
            {
                result = E_BorrowerMaritalStatusType.Unknown;
            }
            else if (s == "Unmarried")
            {
                result = E_BorrowerMaritalStatusType.Unmarried;
            }
            else
            {
                result = E_BorrowerMaritalStatusType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_BorrowerRelationshipTitleType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "AMarriedMan")
            {
                result = E_BorrowerRelationshipTitleType.AMarriedMan;
            }
            else if (s == "AMarriedPerson")
            {
                result = E_BorrowerRelationshipTitleType.AMarriedPerson;
            }
            else if (s == "AMarriedWoman")
            {
                result = E_BorrowerRelationshipTitleType.AMarriedWoman;
            }
            else if (s == "AnUnmarriedMan")
            {
                result = E_BorrowerRelationshipTitleType.AnUnmarriedMan;
            }
            else if (s == "AnUnmarriedPerson")
            {
                result = E_BorrowerRelationshipTitleType.AnUnmarriedPerson;
            }
            else if (s == "AnUnmarriedWoman")
            {
                result = E_BorrowerRelationshipTitleType.AnUnmarriedWoman;
            }
            else if (s == "ASingleMan")
            {
                result = E_BorrowerRelationshipTitleType.ASingleMan;
            }
            else if (s == "ASinglePerson")
            {
                result = E_BorrowerRelationshipTitleType.ASinglePerson;
            }
            else if (s == "ASingleWoman")
            {
                result = E_BorrowerRelationshipTitleType.ASingleWoman;
            }
            else if (s == "AWidow")
            {
                result = E_BorrowerRelationshipTitleType.AWidow;
            }
            else if (s == "AWidower")
            {
                result = E_BorrowerRelationshipTitleType.AWidower;
            }
            else if (s == "HerHusband")
            {
                result = E_BorrowerRelationshipTitleType.HerHusband;
            }
            else if (s == "HisWife")
            {
                result = E_BorrowerRelationshipTitleType.HisWife;
            }
            else if (s == "HusbandAndWife")
            {
                result = E_BorrowerRelationshipTitleType.HusbandAndWife;
            }
            else if (s == "NotApplicable")
            {
                result = E_BorrowerRelationshipTitleType.NotApplicable;
            }
            else if (s == "Other")
            {
                result = E_BorrowerRelationshipTitleType.Other;
            }
            else if (s == "WifeAndHusband")
            {
                result = E_BorrowerRelationshipTitleType.WifeAndHusband;
            }
            else
            {
                result = E_BorrowerRelationshipTitleType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_AliasType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "FormerlyKnownAs")
            {
                result = E_AliasType.FormerlyKnownAs;
            }
            else if (s == "NowKnownAs")
            {
                result = E_AliasType.NowKnownAs;
            }
            else if (s == "AlsoKnownAs")
            {
                result = E_AliasType.AlsoKnownAs;
            }
            else if (s == "Other")
            {
                result = E_AliasType.Other;
            }
            else
            {
                result = E_AliasType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_ResidenceBorrowerResidencyBasisType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "LivingRentFree")
            {
                result = E_ResidenceBorrowerResidencyBasisType.LivingRentFree;
            }
            else if (s == "Own")
            {
                result = E_ResidenceBorrowerResidencyBasisType.Own;
            }
            else if (s == "Rent")
            {
                result = E_ResidenceBorrowerResidencyBasisType.Rent;
            }
            else
            {
                result = E_ResidenceBorrowerResidencyBasisType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_ResidenceBorrowerResidencyType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Current")
            {
                result = E_ResidenceBorrowerResidencyType.Current;
            }
            else if (s == "Prior")
            {
                result = E_ResidenceBorrowerResidencyType.Prior;
            }
            else
            {
                result = E_ResidenceBorrowerResidencyType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CurrentIncomeIncomeType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "AlimonyChildSupport")
            {
                result = E_CurrentIncomeIncomeType.AlimonyChildSupport;
            }
            else if (s == "AutomobileExpenseAccount")
            {
                result = E_CurrentIncomeIncomeType.AutomobileExpenseAccount;
            }
            else if (s == "Base")
            {
                result = E_CurrentIncomeIncomeType.Base;
            }
            else if (s == "Bonus")
            {
                result = E_CurrentIncomeIncomeType.Bonus;
            }
            else if (s == "Commissions")
            {
                result = E_CurrentIncomeIncomeType.Commissions;
            }
            else if (s == "DividendsInterest")
            {
                result = E_CurrentIncomeIncomeType.DividendsInterest;
            }
            else if (s == "FosterCare")
            {
                result = E_CurrentIncomeIncomeType.FosterCare;
            }
            else if (s == "NetRentalIncome")
            {
                result = E_CurrentIncomeIncomeType.NetRentalIncome;
            }
            else if (s == "NotesReceivableInstallment")
            {
                result = E_CurrentIncomeIncomeType.NotesReceivableInstallment;
            }
            else if (s == "OtherTypesOfIncome")
            {
                result = E_CurrentIncomeIncomeType.OtherTypesOfIncome;
            }
            else if (s == "Overtime")
            {
                result = E_CurrentIncomeIncomeType.Overtime;
            }
            else if (s == "Pension")
            {
                result = E_CurrentIncomeIncomeType.Pension;
            }
            else if (s == "SocialSecurity")
            {
                result = E_CurrentIncomeIncomeType.SocialSecurity;
            }
            else if (s == "SubjectPropertyNetCashFlow")
            {
                result = E_CurrentIncomeIncomeType.SubjectPropertyNetCashFlow;
            }
            else if (s == "Trust")
            {
                result = E_CurrentIncomeIncomeType.Trust;
            }
            else if (s == "Unemployment")
            {
                result = E_CurrentIncomeIncomeType.Unemployment;
            }
            else if (s == "PublicAssistance")
            {
                result = E_CurrentIncomeIncomeType.PublicAssistance;
            }
            else if (s == "VABenefitsNonEducational")
            {
                result = E_CurrentIncomeIncomeType.VABenefitsNonEducational;
            }
            else if (s == "MortgageDifferential")
            {
                result = E_CurrentIncomeIncomeType.MortgageDifferential;
            }
            else if (s == "MilitaryBasePay")
            {
                result = E_CurrentIncomeIncomeType.MilitaryBasePay;
            }
            else if (s == "MilitaryRationsAllowance")
            {
                result = E_CurrentIncomeIncomeType.MilitaryRationsAllowance;
            }
            else if (s == "MilitaryFlightPay")
            {
                result = E_CurrentIncomeIncomeType.MilitaryFlightPay;
            }
            else if (s == "MilitaryHazardPay")
            {
                result = E_CurrentIncomeIncomeType.MilitaryHazardPay;
            }
            else if (s == "MilitaryClothesAllowance")
            {
                result = E_CurrentIncomeIncomeType.MilitaryClothesAllowance;
            }
            else if (s == "MilitaryQuartersAllowance")
            {
                result = E_CurrentIncomeIncomeType.MilitaryQuartersAllowance;
            }
            else if (s == "MilitaryPropPay")
            {
                result = E_CurrentIncomeIncomeType.MilitaryPropPay;
            }
            else if (s == "MilitaryOverseasPay")
            {
                result = E_CurrentIncomeIncomeType.MilitaryOverseasPay;
            }
            else if (s == "MilitaryCombatPay")
            {
                result = E_CurrentIncomeIncomeType.MilitaryCombatPay;
            }
            else if (s == "MilitaryVariableHousingAllowance")
            {
                result = E_CurrentIncomeIncomeType.MilitaryVariableHousingAllowance;
            }
            else
            {
                result = E_CurrentIncomeIncomeType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_DeclarationCitizenshipResidencyType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "USCitizen")
            {
                result = E_DeclarationCitizenshipResidencyType.USCitizen;
            }
            else if (s == "PermanentResidentAlien")
            {
                result = E_DeclarationCitizenshipResidencyType.PermanentResidentAlien;
            }
            else if (s == "NonPermanentResidentAlien")
            {
                result = E_DeclarationCitizenshipResidencyType.NonPermanentResidentAlien;
            }
            else if (s == "NonResidentAlien")
            {
                result = E_DeclarationCitizenshipResidencyType.NonResidentAlien;
            }
            else if (s == "Unknown")
            {
                result = E_DeclarationCitizenshipResidencyType.Unknown;
            }
            else
            {
                result = E_DeclarationCitizenshipResidencyType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_DeclarationHomeownerPastThreeYearsType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Yes")
            {
                result = E_DeclarationHomeownerPastThreeYearsType.Yes;
            }
            else if (s == "No")
            {
                result = E_DeclarationHomeownerPastThreeYearsType.No;
            }
            else if (s == "Unknown")
            {
                result = E_DeclarationHomeownerPastThreeYearsType.Unknown;
            }
            else
            {
                result = E_DeclarationHomeownerPastThreeYearsType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_DeclarationIntentToOccupyType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Yes")
            {
                result = E_DeclarationIntentToOccupyType.Yes;
            }
            else if (s == "No")
            {
                result = E_DeclarationIntentToOccupyType.No;
            }
            else if (s == "Unknown")
            {
                result = E_DeclarationIntentToOccupyType.Unknown;
            }
            else
            {
                result = E_DeclarationIntentToOccupyType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_DeclarationPriorPropertyTitleType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Sole")
            {
                result = E_DeclarationPriorPropertyTitleType.Sole;
            }
            else if (s == "JointWithSpouse")
            {
                result = E_DeclarationPriorPropertyTitleType.JointWithSpouse;
            }
            else if (s == "JointWithOtherThanSpouse")
            {
                result = E_DeclarationPriorPropertyTitleType.JointWithOtherThanSpouse;
            }
            else
            {
                result = E_DeclarationPriorPropertyTitleType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_DeclarationPriorPropertyUsageType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Investment")
            {
                result = E_DeclarationPriorPropertyUsageType.Investment;
            }
            else if (s == "PrimaryResidence")
            {
                result = E_DeclarationPriorPropertyUsageType.PrimaryResidence;
            }
            else if (s == "SecondaryResidence")
            {
                result = E_DeclarationPriorPropertyUsageType.SecondaryResidence;
            }
            else
            {
                result = E_DeclarationPriorPropertyUsageType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_ExplanationType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "AlimonyChildSupport")
            {
                result = E_ExplanationType.AlimonyChildSupport;
            }
            else if (s == "BorrowedDownPayment")
            {
                result = E_ExplanationType.BorrowedDownPayment;
            }
            else if (s == "CoMakerEndorserOnNote")
            {
                result = E_ExplanationType.CoMakerEndorserOnNote;
            }
            else if (s == "DeclaredBankruptcyPastSevenYears")
            {
                result = E_ExplanationType.DeclaredBankruptcyPastSevenYears;
            }
            else if (s == "DelinquencyOrDefault")
            {
                result = E_ExplanationType.DelinquencyOrDefault;
            }
            else if (s == "DirectIndirectForeclosedPropertyPastSevenYears")
            {
                result = E_ExplanationType.DirectIndirectForeclosedPropertyPastSevenYears;
            }
            else if (s == "ObligatedOnLoanForeclosedOrDeedInLieuOfJudgement")
            {
                result = E_ExplanationType.ObligatedOnLoanForeclosedOrDeedInLieuOfJudgement;
            }
            else if (s == "OutstandingJudgments")
            {
                result = E_ExplanationType.OutstandingJudgments;
            }
            else if (s == "PartyToLawsuit")
            {
                result = E_ExplanationType.PartyToLawsuit;
            }
            else
            {
                result = E_ExplanationType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_FhaVaBorrowerFnmCreditReportScoreType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "CreditQuote")
            {
                result = E_FhaVaBorrowerFnmCreditReportScoreType.CreditQuote;
            }
            else if (s == "FICO")
            {
                result = E_FhaVaBorrowerFnmCreditReportScoreType.FICO;
            }
            else
            {
                result = E_FhaVaBorrowerFnmCreditReportScoreType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_FhaVaBorrowerCertificationSalesPriceExceedsAppraisedValueType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "A")
            {
                result = E_FhaVaBorrowerCertificationSalesPriceExceedsAppraisedValueType.A;
            }
            else if (s == "B")
            {
                result = E_FhaVaBorrowerCertificationSalesPriceExceedsAppraisedValueType.B;
            }
            else
            {
                result = E_FhaVaBorrowerCertificationSalesPriceExceedsAppraisedValueType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_GovernmentMonitoringHmdaEthnicityType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "HispanicOrLatino")
            {
                result = E_GovernmentMonitoringHmdaEthnicityType.HispanicOrLatino;
            }
            else if (s == "NotHispanicOrLatino")
            {
                result = E_GovernmentMonitoringHmdaEthnicityType.NotHispanicOrLatino;
            }
            else if (s == "InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication")
            {
                result = E_GovernmentMonitoringHmdaEthnicityType.InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication;
            }
            else if (s == "NotApplicable")
            {
                result = E_GovernmentMonitoringHmdaEthnicityType.NotApplicable;
            }
            else
            {
                result = E_GovernmentMonitoringHmdaEthnicityType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_GovernmentMonitoringGenderType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Female")
            {
                result = E_GovernmentMonitoringGenderType.Female;
            }
            else if (s == "InformationNotProvidedUnknown")
            {
                result = E_GovernmentMonitoringGenderType.InformationNotProvidedUnknown;
            }
            else if (s == "Male")
            {
                result = E_GovernmentMonitoringGenderType.Male;
            }
            else if (s == "NotApplicable")
            {
                result = E_GovernmentMonitoringGenderType.NotApplicable;
            }
            else
            {
                result = E_GovernmentMonitoringGenderType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_GovernmentMonitoringRaceNationalOriginType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "AmericanIndianOrAlaskanNative")
            {
                result = E_GovernmentMonitoringRaceNationalOriginType.AmericanIndianOrAlaskanNative;
            }
            else if (s == "AsianOrPacificIslander")
            {
                result = E_GovernmentMonitoringRaceNationalOriginType.AsianOrPacificIslander;
            }
            else if (s == "BlackNotOfHispanicOrigin")
            {
                result = E_GovernmentMonitoringRaceNationalOriginType.BlackNotOfHispanicOrigin;
            }
            else if (s == "Hispanic")
            {
                result = E_GovernmentMonitoringRaceNationalOriginType.Hispanic;
            }
            else if (s == "InformationNotProvided")
            {
                result = E_GovernmentMonitoringRaceNationalOriginType.InformationNotProvided;
            }
            else if (s == "Other")
            {
                result = E_GovernmentMonitoringRaceNationalOriginType.Other;
            }
            else if (s == "WhiteNotOfHispanicOrigin")
            {
                result = E_GovernmentMonitoringRaceNationalOriginType.WhiteNotOfHispanicOrigin;
            }
            else
            {
                result = E_GovernmentMonitoringRaceNationalOriginType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_HmdaRaceType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "AmericanIndianOrAlaskaNative")
            {
                result = E_HmdaRaceType.AmericanIndianOrAlaskaNative;
            }
            else if (s == "Asian")
            {
                result = E_HmdaRaceType.Asian;
            }
            else if (s == "BlackOrAfricanAmerican")
            {
                result = E_HmdaRaceType.BlackOrAfricanAmerican;
            }
            else if (s == "NativeHawaiianOrOtherPacificIslander")
            {
                result = E_HmdaRaceType.NativeHawaiianOrOtherPacificIslander;
            }
            else if (s == "White")
            {
                result = E_HmdaRaceType.White;
            }
            else if (s == "InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication")
            {
                result = E_HmdaRaceType.InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication;
            }
            else if (s == "NotApplicable")
            {
                result = E_HmdaRaceType.NotApplicable;
            }
            else
            {
                result = E_HmdaRaceType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_PresentHousingExpenseHousingExpenseType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "FirstMortgagePrincipalAndInterest")
            {
                result = E_PresentHousingExpenseHousingExpenseType.FirstMortgagePrincipalAndInterest;
            }
            else if (s == "HazardInsurance")
            {
                result = E_PresentHousingExpenseHousingExpenseType.HazardInsurance;
            }
            else if (s == "HomeownersAssociationDuesAndCondominiumFees")
            {
                result = E_PresentHousingExpenseHousingExpenseType.HomeownersAssociationDuesAndCondominiumFees;
            }
            else if (s == "MI")
            {
                result = E_PresentHousingExpenseHousingExpenseType.MI;
            }
            else if (s == "OtherHousingExpense")
            {
                result = E_PresentHousingExpenseHousingExpenseType.OtherHousingExpense;
            }
            else if (s == "OtherMortgageLoanPrincipalAndInterest")
            {
                result = E_PresentHousingExpenseHousingExpenseType.OtherMortgageLoanPrincipalAndInterest;
            }
            else if (s == "RealEstateTax")
            {
                result = E_PresentHousingExpenseHousingExpenseType.RealEstateTax;
            }
            else if (s == "Rent")
            {
                result = E_PresentHousingExpenseHousingExpenseType.Rent;
            }
            else
            {
                result = E_PresentHousingExpenseHousingExpenseType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_SummaryAmountType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "TotalMonthlyIncomeNotIncludingNetRentalIncome")
            {
                result = E_SummaryAmountType.TotalMonthlyIncomeNotIncludingNetRentalIncome;
            }
            else if (s == "SubtotalLiabilitiesPaidByClosingNotIncludingSubjectPropertyLiensBalance")
            {
                result = E_SummaryAmountType.SubtotalLiabilitiesPaidByClosingNotIncludingSubjectPropertyLiensBalance;
            }
            else if (s == "TotalPresentHousingExpense")
            {
                result = E_SummaryAmountType.TotalPresentHousingExpense;
            }
            else if (s == "TotalLiabilitesBalance")
            {
                result = E_SummaryAmountType.TotalLiabilitesBalance;
            }
            else if (s == "SubtotalLiabilitesMonthlyPayment")
            {
                result = E_SummaryAmountType.SubtotalLiabilitesMonthlyPayment;
            }
            else if (s == "SubtotalOmittedLiabilitesBalance")
            {
                result = E_SummaryAmountType.SubtotalOmittedLiabilitesBalance;
            }
            else if (s == "SubtotalOmittedLiabilitiesMonthlyPayment")
            {
                result = E_SummaryAmountType.SubtotalOmittedLiabilitiesMonthlyPayment;
            }
            else if (s == "SubtotalLiabilitiesPaidByClosingNotIncludingSubjectPropertyLiensMonthlyPayment")
            {
                result = E_SummaryAmountType.SubtotalLiabilitiesPaidByClosingNotIncludingSubjectPropertyLiensMonthlyPayment;
            }
            else if (s == "SubtotalResubordinatedLiabilitesMonthlyPaymentForSubjectProperty")
            {
                result = E_SummaryAmountType.SubtotalResubordinatedLiabilitesMonthlyPaymentForSubjectProperty;
            }
            else if (s == "SubtotalSubjectPropertyLiensPaidByClosingBalance")
            {
                result = E_SummaryAmountType.SubtotalSubjectPropertyLiensPaidByClosingBalance;
            }
            else if (s == "SubtotalSubjectPropertyLiensPaidByClosingMonthlyPayment")
            {
                result = E_SummaryAmountType.SubtotalSubjectPropertyLiensPaidByClosingMonthlyPayment;
            }
            else if (s == "SubtotalLiabilitiesForRentalPropertyBalance")
            {
                result = E_SummaryAmountType.SubtotalLiabilitiesForRentalPropertyBalance;
            }
            else if (s == "SubtotalLiabilitiesForRentalPropertyMonthlyPayment")
            {
                result = E_SummaryAmountType.SubtotalLiabilitiesForRentalPropertyMonthlyPayment;
            }
            else if (s == "SubtotalResubordinatedLiabilitiesBalanceForSubjectProperty")
            {
                result = E_SummaryAmountType.SubtotalResubordinatedLiabilitiesBalanceForSubjectProperty;
            }
            else if (s == "SubtotalLiquidAssetsNotIncludingGift")
            {
                result = E_SummaryAmountType.SubtotalLiquidAssetsNotIncludingGift;
            }
            else if (s == "SubtotalNonLiquidAssets")
            {
                result = E_SummaryAmountType.SubtotalNonLiquidAssets;
            }
            else if (s == "TotalLiabilitiesBalance")
            {
                result = E_SummaryAmountType.TotalLiabilitiesBalance;
            }
            else if (s == "SubtotalLiabilitiesMonthlyPayment")
            {
                result = E_SummaryAmountType.SubtotalLiabilitiesMonthlyPayment;
            }
            else if (s == "SubtotalOmittedLiabilitiesBalance")
            {
                result = E_SummaryAmountType.SubtotalOmittedLiabilitiesBalance;
            }
            else if (s == "SubtotalResubordinatedLiabilitiesMonthlyPaymentForSubjectProperty")
            {
                result = E_SummaryAmountType.SubtotalResubordinatedLiabilitiesMonthlyPaymentForSubjectProperty;
            }
            else if (s == "UndrawnHELOC")
            {
                result = E_SummaryAmountType.UndrawnHELOC;
            }
            else
            {
                result = E_SummaryAmountType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_VaBorrowerCertificationOccupancyType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "A")
            {
                result = E_VaBorrowerCertificationOccupancyType.A;
            }
            else if (s == "B")
            {
                result = E_VaBorrowerCertificationOccupancyType.B;
            }
            else if (s == "C")
            {
                result = E_VaBorrowerCertificationOccupancyType.C;
            }
            else if (s == "D")
            {
                result = E_VaBorrowerCertificationOccupancyType.D;
            }
            else
            {
                result = E_VaBorrowerCertificationOccupancyType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_ClosingAgentType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Attorney")
            {
                result = E_ClosingAgentType.Attorney;
            }
            else if (s == "ClosingAgent")
            {
                result = E_ClosingAgentType.ClosingAgent;
            }
            else if (s == "EscrowCompany")
            {
                result = E_ClosingAgentType.EscrowCompany;
            }
            else if (s == "Other")
            {
                result = E_ClosingAgentType.Other;
            }
            else if (s == "SettlementAgent")
            {
                result = E_ClosingAgentType.SettlementAgent;
            }
            else if (s == "TitleCompany")
            {
                result = E_ClosingAgentType.TitleCompany;
            }
            else
            {
                result = E_ClosingAgentType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CompensationPaidByType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Lender")
            {
                result = E_CompensationPaidByType.Lender;
            }
            else if (s == "Investor")
            {
                result = E_CompensationPaidByType.Investor;
            }
            else
            {
                result = E_CompensationPaidByType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CompensationPaidToType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Broker")
            {
                result = E_CompensationPaidToType.Broker;
            }
            else if (s == "Lender")
            {
                result = E_CompensationPaidToType.Lender;
            }
            else
            {
                result = E_CompensationPaidToType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CompensationType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "ServiceReleasePremium")
            {
                result = E_CompensationType.ServiceReleasePremium;
            }
            else if (s == "YieldSpreadDifferential")
            {
                result = E_CompensationType.YieldSpreadDifferential;
            }
            else if (s == "BrokerCompensation")
            {
                result = E_CompensationType.BrokerCompensation;
            }
            else if (s == "LenderCompensation")
            {
                result = E_CompensationType.LenderCompensation;
            }
            else if (s == "Rebate")
            {
                result = E_CompensationType.Rebate;
            }
            else if (s == "Other")
            {
                result = E_CompensationType.Other;
            }
            else
            {
                result = E_CompensationType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_EscrowAccountActivityPaymentDescriptionType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Assessment")
            {
                result = E_EscrowAccountActivityPaymentDescriptionType.Assessment;
            }
            else if (s == "CityPropertyTax")
            {
                result = E_EscrowAccountActivityPaymentDescriptionType.CityPropertyTax;
            }
            else if (s == "CountyPropertyTax")
            {
                result = E_EscrowAccountActivityPaymentDescriptionType.CountyPropertyTax;
            }
            else if (s == "EarthquakeInsurance")
            {
                result = E_EscrowAccountActivityPaymentDescriptionType.EarthquakeInsurance;
            }
            else if (s == "FloodInsurance")
            {
                result = E_EscrowAccountActivityPaymentDescriptionType.FloodInsurance;
            }
            else if (s == "HazardInsurance")
            {
                result = E_EscrowAccountActivityPaymentDescriptionType.HazardInsurance;
            }
            else if (s == "Other")
            {
                result = E_EscrowAccountActivityPaymentDescriptionType.Other;
            }
            else if (s == "SchoolPropertyTax")
            {
                result = E_EscrowAccountActivityPaymentDescriptionType.SchoolPropertyTax;
            }
            else if (s == "TownPropertyTax")
            {
                result = E_EscrowAccountActivityPaymentDescriptionType.TownPropertyTax;
            }
            else if (s == "VillagePropertyTax")
            {
                result = E_EscrowAccountActivityPaymentDescriptionType.VillagePropertyTax;
            }
            else if (s == "Windstorm")
            {
                result = E_EscrowAccountActivityPaymentDescriptionType.Windstorm;
            }
            else if (s == "MortgageInsurance")
            {
                result = E_EscrowAccountActivityPaymentDescriptionType.MortgageInsurance;
            }
            else
            {
                result = E_EscrowAccountActivityPaymentDescriptionType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_LoanDetailsDocumentOrderClassificationType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Preliminary")
            {
                result = E_LoanDetailsDocumentOrderClassificationType.Preliminary;
            }
            else if (s == "Final")
            {
                result = E_LoanDetailsDocumentOrderClassificationType.Final;
            }
            else
            {
                result = E_LoanDetailsDocumentOrderClassificationType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_InterimInterestPerDiemCalculationMethodType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "360")
            {
                result = E_InterimInterestPerDiemCalculationMethodType._360;
            }
            else if (s == "365")
            {
                result = E_InterimInterestPerDiemCalculationMethodType._365;
            }
            else
            {
                result = E_InterimInterestPerDiemCalculationMethodType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_InterimInterestPerDiemPaymentOptionType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Closing")
            {
                result = E_InterimInterestPerDiemPaymentOptionType.Closing;
            }
            else if (s == "FirstPayment")
            {
                result = E_InterimInterestPerDiemPaymentOptionType.FirstPayment;
            }
            else
            {
                result = E_InterimInterestPerDiemPaymentOptionType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_LossPayeeType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Investor")
            {
                result = E_LossPayeeType.Investor;
            }
            else if (s == "Lender")
            {
                result = E_LossPayeeType.Lender;
            }
            else if (s == "Other")
            {
                result = E_LossPayeeType.Other;
            }
            else
            {
                result = E_LossPayeeType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_RecordableDocumentType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "All")
            {
                result = E_RecordableDocumentType.All;
            }
            else if (s == "AssignmentOfMortgage")
            {
                result = E_RecordableDocumentType.AssignmentOfMortgage;
            }
            else if (s == "DeedOfTrust")
            {
                result = E_RecordableDocumentType.DeedOfTrust;
            }
            else if (s == "Mortgage")
            {
                result = E_RecordableDocumentType.Mortgage;
            }
            else if (s == "Other")
            {
                result = E_RecordableDocumentType.Other;
            }
            else if (s == "QuitClaimDeed")
            {
                result = E_RecordableDocumentType.QuitClaimDeed;
            }
            else if (s == "ReleaseOfLien")
            {
                result = E_RecordableDocumentType.ReleaseOfLien;
            }
            else if (s == "SecurityInstrument")
            {
                result = E_RecordableDocumentType.SecurityInstrument;
            }
            else if (s == "SignatureAffidavit")
            {
                result = E_RecordableDocumentType.SignatureAffidavit;
            }
            else if (s == "WarrantyDeed")
            {
                result = E_RecordableDocumentType.WarrantyDeed;
            }
            else
            {
                result = E_RecordableDocumentType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_SignerIdentificationType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "PersonallyKnown")
            {
                result = E_SignerIdentificationType.PersonallyKnown;
            }
            else if (s == "ProvidedIdentification")
            {
                result = E_SignerIdentificationType.ProvidedIdentification;
            }
            else
            {
                result = E_SignerIdentificationType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_PreparedByElectronicRoutingMethodType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "EMAIL")
            {
                result = E_PreparedByElectronicRoutingMethodType.EMAIL;
            }
            else if (s == "FTP")
            {
                result = E_PreparedByElectronicRoutingMethodType.FTP;
            }
            else if (s == "HTTP")
            {
                result = E_PreparedByElectronicRoutingMethodType.HTTP;
            }
            else if (s == "Other")
            {
                result = E_PreparedByElectronicRoutingMethodType.Other;
            }
            else if (s == "URI")
            {
                result = E_PreparedByElectronicRoutingMethodType.URI;
            }
            else if (s == "URL")
            {
                result = E_PreparedByElectronicRoutingMethodType.URL;
            }
            else
            {
                result = E_PreparedByElectronicRoutingMethodType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_AssociatedDocumentType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "All")
            {
                result = E_AssociatedDocumentType.All;
            }
            else if (s == "AssignmentOfMortgage")
            {
                result = E_AssociatedDocumentType.AssignmentOfMortgage;
            }
            else if (s == "DeedOfTrust")
            {
                result = E_AssociatedDocumentType.DeedOfTrust;
            }
            else if (s == "Mortgage")
            {
                result = E_AssociatedDocumentType.Mortgage;
            }
            else if (s == "Other")
            {
                result = E_AssociatedDocumentType.Other;
            }
            else if (s == "QuitClaimDeed")
            {
                result = E_AssociatedDocumentType.QuitClaimDeed;
            }
            else if (s == "ReleaseOfLien")
            {
                result = E_AssociatedDocumentType.ReleaseOfLien;
            }
            else if (s == "SecurityInstrument")
            {
                result = E_AssociatedDocumentType.SecurityInstrument;
            }
            else if (s == "SignatureAffidavit")
            {
                result = E_AssociatedDocumentType.SignatureAffidavit;
            }
            else if (s == "WarrantyDeed")
            {
                result = E_AssociatedDocumentType.WarrantyDeed;
            }
            else
            {
                result = E_AssociatedDocumentType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_AssociatedDocumentBookType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Deed")
            {
                result = E_AssociatedDocumentBookType.Deed;
            }
            else if (s == "Maps")
            {
                result = E_AssociatedDocumentBookType.Maps;
            }
            else if (s == "Mortgage")
            {
                result = E_AssociatedDocumentBookType.Mortgage;
            }
            else if (s == "Other")
            {
                result = E_AssociatedDocumentBookType.Other;
            }
            else if (s == "Plat")
            {
                result = E_AssociatedDocumentBookType.Plat;
            }
            else
            {
                result = E_AssociatedDocumentBookType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_ReturnToElectronicRoutingMethodType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "EMAIL")
            {
                result = E_ReturnToElectronicRoutingMethodType.EMAIL;
            }
            else if (s == "FTP")
            {
                result = E_ReturnToElectronicRoutingMethodType.FTP;
            }
            else if (s == "HTTP")
            {
                result = E_ReturnToElectronicRoutingMethodType.HTTP;
            }
            else if (s == "Other")
            {
                result = E_ReturnToElectronicRoutingMethodType.Other;
            }
            else if (s == "URI")
            {
                result = E_ReturnToElectronicRoutingMethodType.URI;
            }
            else if (s == "URL")
            {
                result = E_ReturnToElectronicRoutingMethodType.URL;
            }
            else
            {
                result = E_ReturnToElectronicRoutingMethodType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_TrusteeType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Primary")
            {
                result = E_TrusteeType.Primary;
            }
            else if (s == "Secondary")
            {
                result = E_TrusteeType.Secondary;
            }
            else if (s == "Other")
            {
                result = E_TrusteeType.Other;
            }
            else
            {
                result = E_TrusteeType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_RespaHudDetailLineItemPaidByType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Buyer")
            {
                result = E_RespaHudDetailLineItemPaidByType.Buyer;
            }
            else if (s == "LenderPremium")
            {
                result = E_RespaHudDetailLineItemPaidByType.LenderPremium;
            }
            else if (s == "Seller")
            {
                result = E_RespaHudDetailLineItemPaidByType.Seller;
            }
            else
            {
                result = E_RespaHudDetailLineItemPaidByType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_TotalFeesPaidToType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Broker")
            {
                result = E_TotalFeesPaidToType.Broker;
            }
            else if (s == "Lender")
            {
                result = E_TotalFeesPaidToType.Lender;
            }
            else if (s == "Investor")
            {
                result = E_TotalFeesPaidToType.Investor;
            }
            else if (s == "Other")
            {
                result = E_TotalFeesPaidToType.Other;
            }
            else
            {
                result = E_TotalFeesPaidToType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_TotalFeesPaidByType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Broker")
            {
                result = E_TotalFeesPaidByType.Broker;
            }
            else if (s == "Buyer")
            {
                result = E_TotalFeesPaidByType.Buyer;
            }
            else if (s == "Investor")
            {
                result = E_TotalFeesPaidByType.Investor;
            }
            else if (s == "Lender")
            {
                result = E_TotalFeesPaidByType.Lender;
            }
            else if (s == "Seller")
            {
                result = E_TotalFeesPaidByType.Seller;
            }
            else if (s == "Other")
            {
                result = E_TotalFeesPaidByType.Other;
            }
            else
            {
                result = E_TotalFeesPaidByType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_ServicerType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Lender")
            {
                result = E_ServicerType.Lender;
            }
            else if (s == "Investor")
            {
                result = E_ServicerType.Investor;
            }
            else if (s == "Present")
            {
                result = E_ServicerType.Present;
            }
            else if (s == "New")
            {
                result = E_ServicerType.New;
            }
            else if (s == "Other")
            {
                result = E_ServicerType.Other;
            }
            else
            {
                result = E_ServicerType.Undefined;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_ContactPointRoleType value)
        {
            switch (value)
            {
                case E_ContactPointRoleType.Home:
                    writer.WriteAttributeString(attrName, "Home");
                    break;
                case E_ContactPointRoleType.Mobile:
                    writer.WriteAttributeString(attrName, "Mobile");
                    break;
                case E_ContactPointRoleType.Work:
                    writer.WriteAttributeString(attrName, "Work");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_ContactPointType value)
        {
            switch (value)
            {
                case E_ContactPointType.Email:
                    writer.WriteAttributeString(attrName, "Email");
                    break;
                case E_ContactPointType.Fax:
                    writer.WriteAttributeString(attrName, "Fax");
                    break;
                case E_ContactPointType.Phone:
                    writer.WriteAttributeString(attrName, "Phone");
                    break;
                case E_ContactPointType.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_MortgageScoreType value)
        {
            switch (value)
            {
                case E_MortgageScoreType.FraudFilterScore:
                    writer.WriteAttributeString(attrName, "FraudFilterScore");
                    break;
                case E_MortgageScoreType.GE_IQScore:
                    writer.WriteAttributeString(attrName, "GE_IQScore");
                    break;
                case E_MortgageScoreType.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
                case E_MortgageScoreType.PMIAuraAQIScore:
                    writer.WriteAttributeString(attrName, "PMIAuraAQIScore");
                    break;
                case E_MortgageScoreType.UGIAccuscore:
                    writer.WriteAttributeString(attrName, "UGIAccuscore");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_TransmittalDataCaseStateType value)
        {
            switch (value)
            {
                case E_TransmittalDataCaseStateType.Application:
                    writer.WriteAttributeString(attrName, "Application");
                    break;
                case E_TransmittalDataCaseStateType.FinalDisposition:
                    writer.WriteAttributeString(attrName, "FinalDisposition");
                    break;
                case E_TransmittalDataCaseStateType.PostClosingQualityControl:
                    writer.WriteAttributeString(attrName, "PostClosingQualityControl");
                    break;
                case E_TransmittalDataCaseStateType.Prequalification:
                    writer.WriteAttributeString(attrName, "Prequalification");
                    break;
                case E_TransmittalDataCaseStateType.Underwriting:
                    writer.WriteAttributeString(attrName, "Underwriting");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_TransmittalDataCurrentFirstMortgageHolderType value)
        {
            switch (value)
            {
                case E_TransmittalDataCurrentFirstMortgageHolderType.FNM:
                    writer.WriteAttributeString(attrName, "FNM");
                    break;
                case E_TransmittalDataCurrentFirstMortgageHolderType.FRE:
                    writer.WriteAttributeString(attrName, "FRE");
                    break;
                case E_TransmittalDataCurrentFirstMortgageHolderType.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
                case E_TransmittalDataCurrentFirstMortgageHolderType.Unknown:
                    writer.WriteAttributeString(attrName, "Unknown");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_TransmittalDataRateLockType value)
        {
            switch (value)
            {
                case E_TransmittalDataRateLockType.BestEfforts:
                    writer.WriteAttributeString(attrName, "BestEfforts");
                    break;
                case E_TransmittalDataRateLockType.Mandatory:
                    writer.WriteAttributeString(attrName, "Mandatory");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_AffordableLendingFnmCommunityLendingProductType value)
        {
            switch (value)
            {
                case E_AffordableLendingFnmCommunityLendingProductType.CommunityHomeBuyerProgram:
                    writer.WriteAttributeString(attrName, "CommunityHomeBuyerProgram");
                    break;
                case E_AffordableLendingFnmCommunityLendingProductType.Fannie97:
                    writer.WriteAttributeString(attrName, "Fannie97");
                    break;
                case E_AffordableLendingFnmCommunityLendingProductType.Fannie32:
                    writer.WriteAttributeString(attrName, "Fannie32");
                    break;
                case E_AffordableLendingFnmCommunityLendingProductType.MyCommunityMortgage:
                    writer.WriteAttributeString(attrName, "MyCommunityMortgage");
                    break;
                case E_AffordableLendingFnmCommunityLendingProductType.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_AssetType value)
        {
            switch (value)
            {
                case E_AssetType.Automobile:
                    writer.WriteAttributeString(attrName, "Automobile");
                    break;
                case E_AssetType.Bond:
                    writer.WriteAttributeString(attrName, "Bond");
                    break;
                case E_AssetType.BridgeLoanNotDeposited:
                    writer.WriteAttributeString(attrName, "BridgeLoanNotDeposited");
                    break;
                case E_AssetType.CashOnHand:
                    writer.WriteAttributeString(attrName, "CashOnHand");
                    break;
                case E_AssetType.CertificateOfDepositTimeDeposit:
                    writer.WriteAttributeString(attrName, "CertificateOfDepositTimeDeposit");
                    break;
                case E_AssetType.CheckingAccount:
                    writer.WriteAttributeString(attrName, "CheckingAccount");
                    break;
                case E_AssetType.EarnestMoneyCashDepositTowardPurchase:
                    writer.WriteAttributeString(attrName, "EarnestMoneyCashDepositTowardPurchase");
                    break;
                case E_AssetType.GiftsTotal:
                    writer.WriteAttributeString(attrName, "GiftsTotal");
                    break;
                case E_AssetType.GiftsNotDeposited:
                    writer.WriteAttributeString(attrName, "GiftsNotDeposited");
                    break;
                case E_AssetType.LifeInsurance:
                    writer.WriteAttributeString(attrName, "LifeInsurance");
                    break;
                case E_AssetType.MoneyMarketFund:
                    writer.WriteAttributeString(attrName, "MoneyMarketFund");
                    break;
                case E_AssetType.MutualFund:
                    writer.WriteAttributeString(attrName, "MutualFund");
                    break;
                case E_AssetType.NetWorthOfBusinessOwned:
                    writer.WriteAttributeString(attrName, "NetWorthOfBusinessOwned");
                    break;
                case E_AssetType.OtherLiquidAssets:
                    writer.WriteAttributeString(attrName, "OtherLiquidAssets");
                    break;
                case E_AssetType.OtherNonLiquidAssets:
                    writer.WriteAttributeString(attrName, "OtherNonLiquidAssets");
                    break;
                case E_AssetType.PendingNetSaleProceedsFromRealEstateAssets:
                    writer.WriteAttributeString(attrName, "PendingNetSaleProceedsFromRealEstateAssets");
                    break;
                case E_AssetType.RelocationMoney:
                    writer.WriteAttributeString(attrName, "RelocationMoney");
                    break;
                case E_AssetType.RetirementFund:
                    writer.WriteAttributeString(attrName, "RetirementFund");
                    break;
                case E_AssetType.SaleOtherAssets:
                    writer.WriteAttributeString(attrName, "SaleOtherAssets");
                    break;
                case E_AssetType.SavingsAccount:
                    writer.WriteAttributeString(attrName, "SavingsAccount");
                    break;
                case E_AssetType.SecuredBorrowedFundsNotDeposited:
                    writer.WriteAttributeString(attrName, "SecuredBorrowedFundsNotDeposited");
                    break;
                case E_AssetType.Stock:
                    writer.WriteAttributeString(attrName, "Stock");
                    break;
                case E_AssetType.TrustAccount:
                    writer.WriteAttributeString(attrName, "TrustAccount");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_DownPaymentType value)
        {
            switch (value)
            {
                case E_DownPaymentType.BridgeLoan:
                    writer.WriteAttributeString(attrName, "BridgeLoan");
                    break;
                case E_DownPaymentType.CashOnHand:
                    writer.WriteAttributeString(attrName, "CashOnHand");
                    break;
                case E_DownPaymentType.CheckingSavings:
                    writer.WriteAttributeString(attrName, "CheckingSavings");
                    break;
                case E_DownPaymentType.DepositOnSalesContract:
                    writer.WriteAttributeString(attrName, "DepositOnSalesContract");
                    break;
                case E_DownPaymentType.EquityOnPendingSale:
                    writer.WriteAttributeString(attrName, "EquityOnPendingSale");
                    break;
                case E_DownPaymentType.EquityOnSoldProperty:
                    writer.WriteAttributeString(attrName, "EquityOnSoldProperty");
                    break;
                case E_DownPaymentType.EquityOnSubjectProperty:
                    writer.WriteAttributeString(attrName, "EquityOnSubjectProperty");
                    break;
                case E_DownPaymentType.GiftFunds:
                    writer.WriteAttributeString(attrName, "GiftFunds");
                    break;
                case E_DownPaymentType.LifeInsuranceCashValue:
                    writer.WriteAttributeString(attrName, "LifeInsuranceCashValue");
                    break;
                case E_DownPaymentType.LotEquity:
                    writer.WriteAttributeString(attrName, "LotEquity");
                    break;
                case E_DownPaymentType.OtherTypeOfDownPayment:
                    writer.WriteAttributeString(attrName, "OtherTypeOfDownPayment");
                    break;
                case E_DownPaymentType.RentWithOptionToPurchase:
                    writer.WriteAttributeString(attrName, "RentWithOptionToPurchase");
                    break;
                case E_DownPaymentType.RetirementFunds:
                    writer.WriteAttributeString(attrName, "RetirementFunds");
                    break;
                case E_DownPaymentType.SaleOfChattel:
                    writer.WriteAttributeString(attrName, "SaleOfChattel");
                    break;
                case E_DownPaymentType.SecuredBorrowedFunds:
                    writer.WriteAttributeString(attrName, "SecuredBorrowedFunds");
                    break;
                case E_DownPaymentType.StocksAndBonds:
                    writer.WriteAttributeString(attrName, "StocksAndBonds");
                    break;
                case E_DownPaymentType.SweatEquity:
                    writer.WriteAttributeString(attrName, "SweatEquity");
                    break;
                case E_DownPaymentType.TradeEquity:
                    writer.WriteAttributeString(attrName, "TradeEquity");
                    break;
                case E_DownPaymentType.TrustFunds:
                    writer.WriteAttributeString(attrName, "TrustFunds");
                    break;
                case E_DownPaymentType.UnsecuredBorrowedFunds:
                    writer.WriteAttributeString(attrName, "UnsecuredBorrowedFunds");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_EscrowItemType value)
        {
            switch (value)
            {
                case E_EscrowItemType.Assessment:
                    writer.WriteAttributeString(attrName, "Assessment");
                    break;
                case E_EscrowItemType.CityPropertyTax:
                    writer.WriteAttributeString(attrName, "CityPropertyTax");
                    break;
                case E_EscrowItemType.CountyPropertyTax:
                    writer.WriteAttributeString(attrName, "CountyPropertyTax");
                    break;
                case E_EscrowItemType.EarthquakeInsurance:
                    writer.WriteAttributeString(attrName, "EarthquakeInsurance");
                    break;
                case E_EscrowItemType.FloodInsurance:
                    writer.WriteAttributeString(attrName, "FloodInsurance");
                    break;
                case E_EscrowItemType.HazardInsurance:
                    writer.WriteAttributeString(attrName, "HazardInsurance");
                    break;
                case E_EscrowItemType.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
                case E_EscrowItemType.SchoolPropertyTax:
                    writer.WriteAttributeString(attrName, "SchoolPropertyTax");
                    break;
                case E_EscrowItemType.TownPropertyTax:
                    writer.WriteAttributeString(attrName, "TownPropertyTax");
                    break;
                case E_EscrowItemType.VillagePropertyTax:
                    writer.WriteAttributeString(attrName, "VillagePropertyTax");
                    break;
                case E_EscrowItemType.WindstormInsurance:
                    writer.WriteAttributeString(attrName, "WindstormInsurance");
                    break;
                case E_EscrowItemType.DTC_MortgageInsurance:
                    writer.WriteAttributeString(attrName, "DTC_MortgageInsurance");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_EscrowMonthlyPaymentRoundingType value)
        {
            switch (value)
            {
                case E_EscrowMonthlyPaymentRoundingType.Up:
                    writer.WriteAttributeString(attrName, "Up");
                    break;
                case E_EscrowMonthlyPaymentRoundingType.Down:
                    writer.WriteAttributeString(attrName, "Down");
                    break;
                case E_EscrowMonthlyPaymentRoundingType.None:
                    writer.WriteAttributeString(attrName, "None");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_EscrowPaidByType value)
        {
            switch (value)
            {
                case E_EscrowPaidByType.Buyer:
                    writer.WriteAttributeString(attrName, "Buyer");
                    break;
                case E_EscrowPaidByType.LenderPremium:
                    writer.WriteAttributeString(attrName, "LenderPremium");
                    break;
                case E_EscrowPaidByType.Seller:
                    writer.WriteAttributeString(attrName, "Seller");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_EscrowPaymentFrequencyType value)
        {
            switch (value)
            {
                case E_EscrowPaymentFrequencyType.Annual:
                    writer.WriteAttributeString(attrName, "Annual");
                    break;
                case E_EscrowPaymentFrequencyType.Monthly:
                    writer.WriteAttributeString(attrName, "Monthly");
                    break;
                case E_EscrowPaymentFrequencyType.Quarterly:
                    writer.WriteAttributeString(attrName, "Quarterly");
                    break;
                case E_EscrowPaymentFrequencyType.SemiAnnual:
                    writer.WriteAttributeString(attrName, "SemiAnnual");
                    break;
                case E_EscrowPaymentFrequencyType.Unequal:
                    writer.WriteAttributeString(attrName, "Unequal");
                    break;
                case E_EscrowPaymentFrequencyType.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_EscrowPremiumPaidByType value)
        {
            switch (value)
            {
                case E_EscrowPremiumPaidByType.Buyer:
                    writer.WriteAttributeString(attrName, "Buyer");
                    break;
                case E_EscrowPremiumPaidByType.Lender:
                    writer.WriteAttributeString(attrName, "Lender");
                    break;
                case E_EscrowPremiumPaidByType.Seller:
                    writer.WriteAttributeString(attrName, "Seller");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_EscrowPremiumPaymentType value)
        {
            switch (value)
            {
                case E_EscrowPremiumPaymentType.CollectedAtClosing:
                    writer.WriteAttributeString(attrName, "CollectedAtClosing");
                    break;
                case E_EscrowPremiumPaymentType.CollectAtClosing:
                    writer.WriteAttributeString(attrName, "CollectAtClosing");
                    break;
                case E_EscrowPremiumPaymentType.PaidOutsideOfClosing:
                    writer.WriteAttributeString(attrName, "PaidOutsideOfClosing");
                    break;
                case E_EscrowPremiumPaymentType.Waived:
                    writer.WriteAttributeString(attrName, "Waived");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_NonPersonEntityDetailOrganizationType value)
        {
            switch (value)
            {
                case E_NonPersonEntityDetailOrganizationType.Corporation:
                    writer.WriteAttributeString(attrName, "Corporation");
                    break;
                case E_NonPersonEntityDetailOrganizationType.LimitedLiabilityCompany:
                    writer.WriteAttributeString(attrName, "LimitedLiabilityCompany");
                    break;
                case E_NonPersonEntityDetailOrganizationType.Partnership:
                    writer.WriteAttributeString(attrName, "Partnership");
                    break;
                case E_NonPersonEntityDetailOrganizationType.SoleProprietorship:
                    writer.WriteAttributeString(attrName, "SoleProprietorship");
                    break;
                case E_NonPersonEntityDetailOrganizationType.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_FhaLoanFhaAlimonyLiabilityTreatmentType value)
        {
            switch (value)
            {
                case E_FhaLoanFhaAlimonyLiabilityTreatmentType.AdditionToDebt:
                    writer.WriteAttributeString(attrName, "AdditionToDebt");
                    break;
                case E_FhaLoanFhaAlimonyLiabilityTreatmentType.ReductionToIncome:
                    writer.WriteAttributeString(attrName, "ReductionToIncome");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_FhaLoanSectionOfActType value)
        {
            switch (value)
            {
                case E_FhaLoanSectionOfActType._203B:
                    writer.WriteAttributeString(attrName, "203B");
                    break;
                case E_FhaLoanSectionOfActType._203B251:
                    writer.WriteAttributeString(attrName, "203B251");
                    break;
                case E_FhaLoanSectionOfActType._203B2:
                    writer.WriteAttributeString(attrName, "203B2");
                    break;
                case E_FhaLoanSectionOfActType._203K:
                    writer.WriteAttributeString(attrName, "203K");
                    break;
                case E_FhaLoanSectionOfActType._203K251:
                    writer.WriteAttributeString(attrName, "203K251");
                    break;
                case E_FhaLoanSectionOfActType._221D2:
                    writer.WriteAttributeString(attrName, "221D2");
                    break;
                case E_FhaLoanSectionOfActType._221D2251:
                    writer.WriteAttributeString(attrName, "221D2251");
                    break;
                case E_FhaLoanSectionOfActType._234C:
                    writer.WriteAttributeString(attrName, "234C");
                    break;
                case E_FhaLoanSectionOfActType._234C251:
                    writer.WriteAttributeString(attrName, "234C251");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_FhaVaLoanGovernmentRefinanceType value)
        {
            switch (value)
            {
                case E_FhaVaLoanGovernmentRefinanceType.FullDocumentation:
                    writer.WriteAttributeString(attrName, "FullDocumentation");
                    break;
                case E_FhaVaLoanGovernmentRefinanceType.InterestRateReductionRefinanceLoan:
                    writer.WriteAttributeString(attrName, "InterestRateReductionRefinanceLoan");
                    break;
                case E_FhaVaLoanGovernmentRefinanceType.StreamlineWithAppraisal:
                    writer.WriteAttributeString(attrName, "StreamlineWithAppraisal");
                    break;
                case E_FhaVaLoanGovernmentRefinanceType.StreamlineWithoutAppraisal:
                    writer.WriteAttributeString(attrName, "StreamlineWithoutAppraisal");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_GovernmentReportingHmdaPurposeOfLoanType value)
        {
            switch (value)
            {
                case E_GovernmentReportingHmdaPurposeOfLoanType.HomeImprovement:
                    writer.WriteAttributeString(attrName, "HomeImprovement");
                    break;
                case E_GovernmentReportingHmdaPurposeOfLoanType.HomePurchase:
                    writer.WriteAttributeString(attrName, "HomePurchase");
                    break;
                case E_GovernmentReportingHmdaPurposeOfLoanType.Refinancing:
                    writer.WriteAttributeString(attrName, "Refinancing");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_GovernmentReportingHmdaPreapprovalType value)
        {
            switch (value)
            {
                case E_GovernmentReportingHmdaPreapprovalType.PreapprovalWasRequested:
                    writer.WriteAttributeString(attrName, "PreapprovalWasRequested");
                    break;
                case E_GovernmentReportingHmdaPreapprovalType.PreapprovalWasNotRequested:
                    writer.WriteAttributeString(attrName, "PreapprovalWasNotRequested");
                    break;
                case E_GovernmentReportingHmdaPreapprovalType.NotApplicable:
                    writer.WriteAttributeString(attrName, "NotApplicable");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_InterviewerInformationApplicationTakenMethodType value)
        {
            switch (value)
            {
                case E_InterviewerInformationApplicationTakenMethodType.FaceToFace:
                    writer.WriteAttributeString(attrName, "FaceToFace");
                    break;
                case E_InterviewerInformationApplicationTakenMethodType.Mail:
                    writer.WriteAttributeString(attrName, "Mail");
                    break;
                case E_InterviewerInformationApplicationTakenMethodType.Telephone:
                    writer.WriteAttributeString(attrName, "Telephone");
                    break;
                case E_InterviewerInformationApplicationTakenMethodType.Internet:
                    writer.WriteAttributeString(attrName, "Internet");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_LiabilityType value)
        {
            switch (value)
            {
                case E_LiabilityType.Alimony:
                    writer.WriteAttributeString(attrName, "Alimony");
                    break;
                case E_LiabilityType.ChildCare:
                    writer.WriteAttributeString(attrName, "ChildCare");
                    break;
                case E_LiabilityType.ChildSupport:
                    writer.WriteAttributeString(attrName, "ChildSupport");
                    break;
                case E_LiabilityType.CollectionsJudgementsAndLiens:
                    writer.WriteAttributeString(attrName, "CollectionsJudgementsAndLiens");
                    break;
                case E_LiabilityType.HELOC:
                    writer.WriteAttributeString(attrName, "HELOC");
                    break;
                case E_LiabilityType.Installment:
                    writer.WriteAttributeString(attrName, "Installment");
                    break;
                case E_LiabilityType.JobRelatedExpenses:
                    writer.WriteAttributeString(attrName, "JobRelatedExpenses");
                    break;
                case E_LiabilityType.LeasePayments:
                    writer.WriteAttributeString(attrName, "LeasePayments");
                    break;
                case E_LiabilityType.MortgageLoan:
                    writer.WriteAttributeString(attrName, "MortgageLoan");
                    break;
                case E_LiabilityType.Open30DayChargeAccount:
                    writer.WriteAttributeString(attrName, "Open30DayChargeAccount");
                    break;
                case E_LiabilityType.OtherLiability:
                    writer.WriteAttributeString(attrName, "OtherLiability");
                    break;
                case E_LiabilityType.Revolving:
                    writer.WriteAttributeString(attrName, "Revolving");
                    break;
                case E_LiabilityType.SeparateMaintenanceExpense:
                    writer.WriteAttributeString(attrName, "SeparateMaintenanceExpense");
                    break;
                case E_LiabilityType.OtherExpense:
                    writer.WriteAttributeString(attrName, "OtherExpense");
                    break;
                case E_LiabilityType.Taxes:
                    writer.WriteAttributeString(attrName, "Taxes");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_ArmIndexType value)
        {
            switch (value)
            {
                case E_ArmIndexType.EleventhDistrictCostOfFunds:
                    writer.WriteAttributeString(attrName, "EleventhDistrictCostOfFunds");
                    break;
                case E_ArmIndexType.OneYearTreasury:
                    writer.WriteAttributeString(attrName, "OneYearTreasury");
                    break;
                case E_ArmIndexType.ThreeYearTreasury:
                    writer.WriteAttributeString(attrName, "ThreeYearTreasury");
                    break;
                case E_ArmIndexType.SixMonthTreasury:
                    writer.WriteAttributeString(attrName, "SixMonthTreasury");
                    break;
                case E_ArmIndexType.DailyCertificateOfDepositRate:
                    writer.WriteAttributeString(attrName, "DailyCertificateOfDepositRate");
                    break;
                case E_ArmIndexType.FNM60DayRequiredNetYield:
                    writer.WriteAttributeString(attrName, "FNM60DayRequiredNetYield");
                    break;
                case E_ArmIndexType.FNM_LIBOR:
                    writer.WriteAttributeString(attrName, "FNM_LIBOR");
                    break;
                case E_ArmIndexType.FederalCostOfFunds:
                    writer.WriteAttributeString(attrName, "FederalCostOfFunds");
                    break;
                case E_ArmIndexType.FRE60DayRequiredNetYield:
                    writer.WriteAttributeString(attrName, "FRE60DayRequiredNetYield");
                    break;
                case E_ArmIndexType.FRE_LIBOR:
                    writer.WriteAttributeString(attrName, "FRE_LIBOR");
                    break;
                case E_ArmIndexType.LIBOR:
                    writer.WriteAttributeString(attrName, "LIBOR");
                    break;
                case E_ArmIndexType.MonthlyAverageConstantMaturingTreasury:
                    writer.WriteAttributeString(attrName, "MonthlyAverageConstantMaturingTreasury");
                    break;
                case E_ArmIndexType.NationalAverageContractRateFHLBB:
                    writer.WriteAttributeString(attrName, "NationalAverageContractRateFHLBB");
                    break;
                case E_ArmIndexType.NationalMonthlyMedianCostOfFunds:
                    writer.WriteAttributeString(attrName, "NationalMonthlyMedianCostOfFunds");
                    break;
                case E_ArmIndexType.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
                case E_ArmIndexType.TreasuryBillDailyValue:
                    writer.WriteAttributeString(attrName, "TreasuryBillDailyValue");
                    break;
                case E_ArmIndexType.WallStreetJournalLIBOR:
                    writer.WriteAttributeString(attrName, "WallStreetJournalLIBOR");
                    break;
                case E_ArmIndexType.WeeklyAverageCertificateOfDepositRate:
                    writer.WriteAttributeString(attrName, "WeeklyAverageCertificateOfDepositRate");
                    break;
                case E_ArmIndexType.WeeklyAverageConstantMaturingTreasury:
                    writer.WriteAttributeString(attrName, "WeeklyAverageConstantMaturingTreasury");
                    break;
                case E_ArmIndexType.WeeklyAveragePrimeRate:
                    writer.WriteAttributeString(attrName, "WeeklyAveragePrimeRate");
                    break;
                case E_ArmIndexType.WeeklyAverageSecondaryMarketTreasuryBillInvestmentYield:
                    writer.WriteAttributeString(attrName, "WeeklyAverageSecondaryMarketTreasuryBillInvestmentYield");
                    break;
                case E_ArmIndexType.WeeklyAverageTreasuryAuctionAverageBondDiscountYield:
                    writer.WriteAttributeString(attrName, "WeeklyAverageTreasuryAuctionAverageBondDiscountYield");
                    break;
                case E_ArmIndexType.WeeklyAverageTreasuryAuctionAverageInvestmentYield:
                    writer.WriteAttributeString(attrName, "WeeklyAverageTreasuryAuctionAverageInvestmentYield");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_ArmInterestRateRoundingType value)
        {
            switch (value)
            {
                case E_ArmInterestRateRoundingType.Up:
                    writer.WriteAttributeString(attrName, "Up");
                    break;
                case E_ArmInterestRateRoundingType.Down:
                    writer.WriteAttributeString(attrName, "Down");
                    break;
                case E_ArmInterestRateRoundingType.Nearest:
                    writer.WriteAttributeString(attrName, "Nearest");
                    break;
                case E_ArmInterestRateRoundingType.None:
                    writer.WriteAttributeString(attrName, "None");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_BuydownBaseDateType value)
        {
            switch (value)
            {
                case E_BuydownBaseDateType.NoteDate:
                    writer.WriteAttributeString(attrName, "NoteDate");
                    break;
                case E_BuydownBaseDateType.FirstPaymentDate:
                    writer.WriteAttributeString(attrName, "FirstPaymentDate");
                    break;
                case E_BuydownBaseDateType.LastPaymentDate:
                    writer.WriteAttributeString(attrName, "LastPaymentDate");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_BuydownContributorType value)
        {
            switch (value)
            {
                case E_BuydownContributorType.Borrower:
                    writer.WriteAttributeString(attrName, "Borrower");
                    break;
                case E_BuydownContributorType.Builder:
                    writer.WriteAttributeString(attrName, "Builder");
                    break;
                case E_BuydownContributorType.Employer:
                    writer.WriteAttributeString(attrName, "Employer");
                    break;
                case E_BuydownContributorType.LenderPremiumFinanced:
                    writer.WriteAttributeString(attrName, "LenderPremiumFinanced");
                    break;
                case E_BuydownContributorType.NonparentRelative:
                    writer.WriteAttributeString(attrName, "NonparentRelative");
                    break;
                case E_BuydownContributorType.Parent:
                    writer.WriteAttributeString(attrName, "Parent");
                    break;
                case E_BuydownContributorType.Seller:
                    writer.WriteAttributeString(attrName, "Seller");
                    break;
                case E_BuydownContributorType.Unassigned:
                    writer.WriteAttributeString(attrName, "Unassigned");
                    break;
                case E_BuydownContributorType.UnrelatedFriend:
                    writer.WriteAttributeString(attrName, "UnrelatedFriend");
                    break;
                case E_BuydownContributorType.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_BuydownSubsidyCalculationType value)
        {
            switch (value)
            {
                case E_BuydownSubsidyCalculationType.DecliningLoanBalance:
                    writer.WriteAttributeString(attrName, "DecliningLoanBalance");
                    break;
                case E_BuydownSubsidyCalculationType.OriginalLoanAmount:
                    writer.WriteAttributeString(attrName, "OriginalLoanAmount");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_ContributorRoleType value)
        {
            switch (value)
            {
                case E_ContributorRoleType.Borrower:
                    writer.WriteAttributeString(attrName, "Borrower");
                    break;
                case E_ContributorRoleType.Builder:
                    writer.WriteAttributeString(attrName, "Builder");
                    break;
                case E_ContributorRoleType.Employer:
                    writer.WriteAttributeString(attrName, "Employer");
                    break;
                case E_ContributorRoleType.LenderPremiumFinanced:
                    writer.WriteAttributeString(attrName, "LenderPremiumFinanced");
                    break;
                case E_ContributorRoleType.NonparentRelative:
                    writer.WriteAttributeString(attrName, "NonparentRelative");
                    break;
                case E_ContributorRoleType.Parent:
                    writer.WriteAttributeString(attrName, "Parent");
                    break;
                case E_ContributorRoleType.Seller:
                    writer.WriteAttributeString(attrName, "Seller");
                    break;
                case E_ContributorRoleType.Unassigned:
                    writer.WriteAttributeString(attrName, "Unassigned");
                    break;
                case E_ContributorRoleType.UnrelatedFriend:
                    writer.WriteAttributeString(attrName, "UnrelatedFriend");
                    break;
                case E_ContributorRoleType.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_LoanFeaturesDownPaymentOptionType value)
        {
            switch (value)
            {
                case E_LoanFeaturesDownPaymentOptionType.ThreeTwoOption:
                    writer.WriteAttributeString(attrName, "ThreeTwoOption");
                    break;
                case E_LoanFeaturesDownPaymentOptionType.FivePercentOption:
                    writer.WriteAttributeString(attrName, "FivePercentOption");
                    break;
                case E_LoanFeaturesDownPaymentOptionType.FNM97Option:
                    writer.WriteAttributeString(attrName, "FNM97Option");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_LoanFeaturesGseProjectClassificationType value)
        {
            switch (value)
            {
                case E_LoanFeaturesGseProjectClassificationType.A_IIICondominium:
                    writer.WriteAttributeString(attrName, "A_IIICondominium");
                    break;
                case E_LoanFeaturesGseProjectClassificationType.ApprovedFHA_VACondominiumProjectOrSpotLoan:
                    writer.WriteAttributeString(attrName, "ApprovedFHA_VACondominiumProjectOrSpotLoan");
                    break;
                case E_LoanFeaturesGseProjectClassificationType.B_IICondominium:
                    writer.WriteAttributeString(attrName, "B_IICondominium");
                    break;
                case E_LoanFeaturesGseProjectClassificationType.C_ICondominium:
                    writer.WriteAttributeString(attrName, "C_ICondominium");
                    break;
                case E_LoanFeaturesGseProjectClassificationType.OneCooperative:
                    writer.WriteAttributeString(attrName, "OneCooperative");
                    break;
                case E_LoanFeaturesGseProjectClassificationType.TwoCooperative:
                    writer.WriteAttributeString(attrName, "TwoCooperative");
                    break;
                case E_LoanFeaturesGseProjectClassificationType.E_PUD:
                    writer.WriteAttributeString(attrName, "E_PUD");
                    break;
                case E_LoanFeaturesGseProjectClassificationType.F_PUD:
                    writer.WriteAttributeString(attrName, "F_PUD");
                    break;
                case E_LoanFeaturesGseProjectClassificationType.III_PUD:
                    writer.WriteAttributeString(attrName, "III_PUD");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_LoanFeaturesGsePropertyType value)
        {
            switch (value)
            {
                case E_LoanFeaturesGsePropertyType.Attached:
                    writer.WriteAttributeString(attrName, "Attached");
                    break;
                case E_LoanFeaturesGsePropertyType.Condominium:
                    writer.WriteAttributeString(attrName, "Condominium");
                    break;
                case E_LoanFeaturesGsePropertyType.Cooperative:
                    writer.WriteAttributeString(attrName, "Cooperative");
                    break;
                case E_LoanFeaturesGsePropertyType.Detached:
                    writer.WriteAttributeString(attrName, "Detached");
                    break;
                case E_LoanFeaturesGsePropertyType.HighRiseCondominium:
                    writer.WriteAttributeString(attrName, "HighRiseCondominium");
                    break;
                case E_LoanFeaturesGsePropertyType.ManufacturedHousing:
                    writer.WriteAttributeString(attrName, "ManufacturedHousing");
                    break;
                case E_LoanFeaturesGsePropertyType.Modular:
                    writer.WriteAttributeString(attrName, "Modular");
                    break;
                case E_LoanFeaturesGsePropertyType.PUD:
                    writer.WriteAttributeString(attrName, "PUD");
                    break;
                case E_LoanFeaturesGsePropertyType.ManufacturedHousingSingleWide:
                    writer.WriteAttributeString(attrName, "ManufacturedHousingSingleWide");
                    break;
                case E_LoanFeaturesGsePropertyType.ManufacturedHousingDoubleWide:
                    writer.WriteAttributeString(attrName, "ManufacturedHousingDoubleWide");
                    break;
                case E_LoanFeaturesGsePropertyType.DetachedCondominium:
                    writer.WriteAttributeString(attrName, "DetachedCondominium");
                    break;
                case E_LoanFeaturesGsePropertyType.ManufacturedHomeCondominium:
                    writer.WriteAttributeString(attrName, "ManufacturedHomeCondominium");
                    break;
                case E_LoanFeaturesGsePropertyType.ManufacturedHousingMultiWide:
                    writer.WriteAttributeString(attrName, "ManufacturedHousingMultiWide");
                    break;
                case E_LoanFeaturesGsePropertyType.ManufacturedHomeCondominiumOrPUDOrCooperative:
                    writer.WriteAttributeString(attrName, "ManufacturedHomeCondominiumOrPUDOrCooperative");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_LoanFeaturesLienPriorityType value)
        {
            switch (value)
            {
                case E_LoanFeaturesLienPriorityType.FirstLien:
                    writer.WriteAttributeString(attrName, "FirstLien");
                    break;
                case E_LoanFeaturesLienPriorityType.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
                case E_LoanFeaturesLienPriorityType.SecondLien:
                    writer.WriteAttributeString(attrName, "SecondLien");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_LoanFeaturesLoanClosingStatusType value)
        {
            switch (value)
            {
                case E_LoanFeaturesLoanClosingStatusType.Closed:
                    writer.WriteAttributeString(attrName, "Closed");
                    break;
                case E_LoanFeaturesLoanClosingStatusType.TableFunded:
                    writer.WriteAttributeString(attrName, "TableFunded");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_LoanFeaturesLoanDocumentationType value)
        {
            switch (value)
            {
                case E_LoanFeaturesLoanDocumentationType.Alternative:
                    writer.WriteAttributeString(attrName, "Alternative");
                    break;
                case E_LoanFeaturesLoanDocumentationType.FullDocumentation:
                    writer.WriteAttributeString(attrName, "FullDocumentation");
                    break;
                case E_LoanFeaturesLoanDocumentationType.NoDepositVerification:
                    writer.WriteAttributeString(attrName, "NoDepositVerification");
                    break;
                case E_LoanFeaturesLoanDocumentationType.NoDepositVerificationEmploymentVerificationOrIncomeVerification:
                    writer.WriteAttributeString(attrName, "NoDepositVerificationEmploymentVerificationOrIncomeVerification");
                    break;
                case E_LoanFeaturesLoanDocumentationType.NoDocumentation:
                    writer.WriteAttributeString(attrName, "NoDocumentation");
                    break;
                case E_LoanFeaturesLoanDocumentationType.NoEmploymentVerificationOrIncomeVerification:
                    writer.WriteAttributeString(attrName, "NoEmploymentVerificationOrIncomeVerification");
                    break;
                case E_LoanFeaturesLoanDocumentationType.Reduced:
                    writer.WriteAttributeString(attrName, "Reduced");
                    break;
                case E_LoanFeaturesLoanDocumentationType.StreamlineRefinance:
                    writer.WriteAttributeString(attrName, "StreamlineRefinance");
                    break;
                case E_LoanFeaturesLoanDocumentationType.NoRatio:
                    writer.WriteAttributeString(attrName, "NoRatio");
                    break;
                case E_LoanFeaturesLoanDocumentationType.NoIncomeNoEmploymentNoAssetsOn1003:
                    writer.WriteAttributeString(attrName, "NoIncomeNoEmploymentNoAssetsOn1003");
                    break;
                case E_LoanFeaturesLoanDocumentationType.NoIncomeOn1003:
                    writer.WriteAttributeString(attrName, "NoIncomeOn1003");
                    break;
                case E_LoanFeaturesLoanDocumentationType.NoVerificationOfStatedIncomeEmploymentOrAssets:
                    writer.WriteAttributeString(attrName, "NoVerificationOfStatedIncomeEmploymentOrAssets");
                    break;
                case E_LoanFeaturesLoanDocumentationType.NoVerificationOfStatedIncomeOrAssets:
                    writer.WriteAttributeString(attrName, "NoVerificationOfStatedIncomeOrAssets");
                    break;
                case E_LoanFeaturesLoanDocumentationType.NoVerificationOfStatedAssets:
                    writer.WriteAttributeString(attrName, "NoVerificationOfStatedAssets");
                    break;
                case E_LoanFeaturesLoanDocumentationType.NoVerificationOfStatedIncomeOrEmployment:
                    writer.WriteAttributeString(attrName, "NoVerificationOfStatedIncomeOrEmployment");
                    break;
                case E_LoanFeaturesLoanDocumentationType.NoVerificationOfStatedIncome:
                    writer.WriteAttributeString(attrName, "NoVerificationOfStatedIncome");
                    break;
                case E_LoanFeaturesLoanDocumentationType.VerbalVerificationOfEmployment:
                    writer.WriteAttributeString(attrName, "VerbalVerificationOfEmployment");
                    break;
                case E_LoanFeaturesLoanDocumentationType.OnePaystub:
                    writer.WriteAttributeString(attrName, "OnePaystub");
                    break;
                case E_LoanFeaturesLoanDocumentationType.OnePaystubAndVerbalVerificationOfEmployment:
                    writer.WriteAttributeString(attrName, "OnePaystubAndVerbalVerificationOfEmployment");
                    break;
                case E_LoanFeaturesLoanDocumentationType.OnePaystubAndOneW2AndVerbalVerificationOfEmploymentOrOneYear1040:
                    writer.WriteAttributeString(attrName, "OnePaystubAndOneW2AndVerbalVerificationOfEmploymentOrOneYear1040");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_LoanFeaturesLoanRepaymentType value)
        {
            switch (value)
            {
                case E_LoanFeaturesLoanRepaymentType.InterestOnly:
                    writer.WriteAttributeString(attrName, "InterestOnly");
                    break;
                case E_LoanFeaturesLoanRepaymentType.NoNegativeAmortization:
                    writer.WriteAttributeString(attrName, "NoNegativeAmortization");
                    break;
                case E_LoanFeaturesLoanRepaymentType.PotentialNegativeAmortization:
                    writer.WriteAttributeString(attrName, "PotentialNegativeAmortization");
                    break;
                case E_LoanFeaturesLoanRepaymentType.ScheduledAmortization:
                    writer.WriteAttributeString(attrName, "ScheduledAmortization");
                    break;
                case E_LoanFeaturesLoanRepaymentType.ScheduledNegativeAmortization:
                    writer.WriteAttributeString(attrName, "ScheduledNegativeAmortization");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_LoanFeaturesMiCertificationStatusType value)
        {
            switch (value)
            {
                case E_LoanFeaturesMiCertificationStatusType.LenderToObtain:
                    writer.WriteAttributeString(attrName, "LenderToObtain");
                    break;
                case E_LoanFeaturesMiCertificationStatusType.SellerOfLoanToObtain:
                    writer.WriteAttributeString(attrName, "SellerOfLoanToObtain");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_LoanFeaturesMiCompanyNameType value)
        {
            switch (value)
            {
                case E_LoanFeaturesMiCompanyNameType.AmerinGuaranteeCorporation:
                    writer.WriteAttributeString(attrName, "AmerinGuaranteeCorporation");
                    break;
                case E_LoanFeaturesMiCompanyNameType.CMG_MICompany:
                    writer.WriteAttributeString(attrName, "CMG_MICompany");
                    break;
                case E_LoanFeaturesMiCompanyNameType.CommonwealthMortgageAssuranceCompany:
                    writer.WriteAttributeString(attrName, "CommonwealthMortgageAssuranceCompany");
                    break;
                case E_LoanFeaturesMiCompanyNameType.GECapitalMICorporation:
                    writer.WriteAttributeString(attrName, "GECapitalMICorporation");
                    break;
                case E_LoanFeaturesMiCompanyNameType.MortgageGuarantyInsuranceCorporation:
                    writer.WriteAttributeString(attrName, "MortgageGuarantyInsuranceCorporation");
                    break;
                case E_LoanFeaturesMiCompanyNameType.PMI_MICorporation:
                    writer.WriteAttributeString(attrName, "PMI_MICorporation");
                    break;
                case E_LoanFeaturesMiCompanyNameType.RadianGuarantyIncorporated:
                    writer.WriteAttributeString(attrName, "RadianGuarantyIncorporated");
                    break;
                case E_LoanFeaturesMiCompanyNameType.RepublicMICompany:
                    writer.WriteAttributeString(attrName, "RepublicMICompany");
                    break;
                case E_LoanFeaturesMiCompanyNameType.TriadGuarantyInsuranceCorporation:
                    writer.WriteAttributeString(attrName, "TriadGuarantyInsuranceCorporation");
                    break;
                case E_LoanFeaturesMiCompanyNameType.UnitedGuarantyCorporation:
                    writer.WriteAttributeString(attrName, "UnitedGuarantyCorporation");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_LoanFeaturesNameDocumentsDrawnInType value)
        {
            switch (value)
            {
                case E_LoanFeaturesNameDocumentsDrawnInType.Broker:
                    writer.WriteAttributeString(attrName, "Broker");
                    break;
                case E_LoanFeaturesNameDocumentsDrawnInType.Lender:
                    writer.WriteAttributeString(attrName, "Lender");
                    break;
                case E_LoanFeaturesNameDocumentsDrawnInType.Investor:
                    writer.WriteAttributeString(attrName, "Investor");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_LoanFeaturesPaymentFrequencyType value)
        {
            switch (value)
            {
                case E_LoanFeaturesPaymentFrequencyType.Annual:
                    writer.WriteAttributeString(attrName, "Annual");
                    break;
                case E_LoanFeaturesPaymentFrequencyType.AtMaturity:
                    writer.WriteAttributeString(attrName, "AtMaturity");
                    break;
                case E_LoanFeaturesPaymentFrequencyType.Biweekly:
                    writer.WriteAttributeString(attrName, "Biweekly");
                    break;
                case E_LoanFeaturesPaymentFrequencyType.Monthly:
                    writer.WriteAttributeString(attrName, "Monthly");
                    break;
                case E_LoanFeaturesPaymentFrequencyType.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
                case E_LoanFeaturesPaymentFrequencyType.Quarterly:
                    writer.WriteAttributeString(attrName, "Quarterly");
                    break;
                case E_LoanFeaturesPaymentFrequencyType.Semiannual:
                    writer.WriteAttributeString(attrName, "Semiannual");
                    break;
                case E_LoanFeaturesPaymentFrequencyType.Semimonthly:
                    writer.WriteAttributeString(attrName, "Semimonthly");
                    break;
                case E_LoanFeaturesPaymentFrequencyType.Weekly:
                    writer.WriteAttributeString(attrName, "Weekly");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_LoanFeaturesFullPrepaymentPenaltyOptionType value)
        {
            switch (value)
            {
                case E_LoanFeaturesFullPrepaymentPenaltyOptionType.Hard:
                    writer.WriteAttributeString(attrName, "Hard");
                    break;
                case E_LoanFeaturesFullPrepaymentPenaltyOptionType.Soft:
                    writer.WriteAttributeString(attrName, "Soft");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_LoanFeaturesServicingTransferStatusType value)
        {
            switch (value)
            {
                case E_LoanFeaturesServicingTransferStatusType.Retained:
                    writer.WriteAttributeString(attrName, "Retained");
                    break;
                case E_LoanFeaturesServicingTransferStatusType.Released:
                    writer.WriteAttributeString(attrName, "Released");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_LoanFeaturesEstimatedPrepaidDaysPaidByType value)
        {
            switch (value)
            {
                case E_LoanFeaturesEstimatedPrepaidDaysPaidByType.Buyer:
                    writer.WriteAttributeString(attrName, "Buyer");
                    break;
                case E_LoanFeaturesEstimatedPrepaidDaysPaidByType.Lender:
                    writer.WriteAttributeString(attrName, "Lender");
                    break;
                case E_LoanFeaturesEstimatedPrepaidDaysPaidByType.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
                case E_LoanFeaturesEstimatedPrepaidDaysPaidByType.Seller:
                    writer.WriteAttributeString(attrName, "Seller");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_LoanFeaturesCounselingConfirmationType value)
        {
            switch (value)
            {
                case E_LoanFeaturesCounselingConfirmationType.AmericanHomeownerEducationInstituteApprovedCounseling:
                    writer.WriteAttributeString(attrName, "AmericanHomeownerEducationInstituteApprovedCounseling");
                    break;
                case E_LoanFeaturesCounselingConfirmationType.LenderTrainedCounseling:
                    writer.WriteAttributeString(attrName, "LenderTrainedCounseling");
                    break;
                case E_LoanFeaturesCounselingConfirmationType.NoBorrowerCounseling:
                    writer.WriteAttributeString(attrName, "NoBorrowerCounseling");
                    break;
                case E_LoanFeaturesCounselingConfirmationType.ThirdPartyCounseling:
                    writer.WriteAttributeString(attrName, "ThirdPartyCounseling");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_LateChargeType value)
        {
            switch (value)
            {
                case E_LateChargeType.FlatDollarAmount:
                    writer.WriteAttributeString(attrName, "FlatDollarAmount");
                    break;
                case E_LateChargeType.NoLateCharges:
                    writer.WriteAttributeString(attrName, "NoLateCharges");
                    break;
                case E_LateChargeType.PercentOfPrincipalAndInterest:
                    writer.WriteAttributeString(attrName, "PercentOfPrincipalAndInterest");
                    break;
                case E_LateChargeType.PercentageOfDelinquentInterest:
                    writer.WriteAttributeString(attrName, "PercentageOfDelinquentInterest");
                    break;
                case E_LateChargeType.PercentageOfNetPayment:
                    writer.WriteAttributeString(attrName, "PercentageOfNetPayment");
                    break;
                case E_LateChargeType.PercentageOfPrinicipalBalance:
                    writer.WriteAttributeString(attrName, "PercentageOfPrinicipalBalance");
                    break;
                case E_LateChargeType.PercentageOfTotalPayment:
                    writer.WriteAttributeString(attrName, "PercentageOfTotalPayment");
                    break;
                case E_LateChargeType.PercentageOfPrincipalBalance:
                    writer.WriteAttributeString(attrName, "PercentageOfPrincipalBalance");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_PaymentAdjustmentCalculationType value)
        {
            switch (value)
            {
                case E_PaymentAdjustmentCalculationType.AddFixedDollarAmountToTheCurrentPayment:
                    writer.WriteAttributeString(attrName, "AddFixedDollarAmountToTheCurrentPayment");
                    break;
                case E_PaymentAdjustmentCalculationType.AddPercentToCurrentPaymentAmount:
                    writer.WriteAttributeString(attrName, "AddPercentToCurrentPaymentAmount");
                    break;
                case E_PaymentAdjustmentCalculationType.AddPercentToEffectivePaymentRate:
                    writer.WriteAttributeString(attrName, "AddPercentToEffectivePaymentRate");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_RateAdjustmentCalculationType value)
        {
            switch (value)
            {
                case E_RateAdjustmentCalculationType.AddPercentToCurrentRate:
                    writer.WriteAttributeString(attrName, "AddPercentToCurrentRate");
                    break;
                case E_RateAdjustmentCalculationType.AddPercentToOriginalRate:
                    writer.WriteAttributeString(attrName, "AddPercentToOriginalRate");
                    break;
                case E_RateAdjustmentCalculationType.IndexPlusMargin:
                    writer.WriteAttributeString(attrName, "IndexPlusMargin");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_HelocDailyPeriodicInterestRateCalculationType value)
        {
            switch (value)
            {
                case E_HelocDailyPeriodicInterestRateCalculationType._360:
                    writer.WriteAttributeString(attrName, "360");
                    break;
                case E_HelocDailyPeriodicInterestRateCalculationType._365:
                    writer.WriteAttributeString(attrName, "365");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_LoanPurposeType value)
        {
            switch (value)
            {
                case E_LoanPurposeType.ConstructionOnly:
                    writer.WriteAttributeString(attrName, "ConstructionOnly");
                    break;
                case E_LoanPurposeType.ConstructionToPermanent:
                    writer.WriteAttributeString(attrName, "ConstructionToPermanent");
                    break;
                case E_LoanPurposeType.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
                case E_LoanPurposeType.Purchase:
                    writer.WriteAttributeString(attrName, "Purchase");
                    break;
                case E_LoanPurposeType.Refinance:
                    writer.WriteAttributeString(attrName, "Refinance");
                    break;
                case E_LoanPurposeType.Unknown:
                    writer.WriteAttributeString(attrName, "Unknown");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_LoanPurposePropertyRightsType value)
        {
            switch (value)
            {
                case E_LoanPurposePropertyRightsType.FeeSimple:
                    writer.WriteAttributeString(attrName, "FeeSimple");
                    break;
                case E_LoanPurposePropertyRightsType.Leasehold:
                    writer.WriteAttributeString(attrName, "Leasehold");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_LoanPurposePropertyUsageType value)
        {
            switch (value)
            {
                case E_LoanPurposePropertyUsageType.Investment:
                    writer.WriteAttributeString(attrName, "Investment");
                    break;
                case E_LoanPurposePropertyUsageType.Investor:
                    writer.WriteAttributeString(attrName, "Investor");
                    break;
                case E_LoanPurposePropertyUsageType.PrimaryResidence:
                    writer.WriteAttributeString(attrName, "PrimaryResidence");
                    break;
                case E_LoanPurposePropertyUsageType.SecondHome:
                    writer.WriteAttributeString(attrName, "SecondHome");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_ConstructionRefinanceDataGseRefinancePurposeType value)
        {
            switch (value)
            {
                case E_ConstructionRefinanceDataGseRefinancePurposeType.CashOutDebtConsolidation:
                    writer.WriteAttributeString(attrName, "CashOutDebtConsolidation");
                    break;
                case E_ConstructionRefinanceDataGseRefinancePurposeType.CashOutHomeImprovement:
                    writer.WriteAttributeString(attrName, "CashOutHomeImprovement");
                    break;
                case E_ConstructionRefinanceDataGseRefinancePurposeType.CashOutLimited:
                    writer.WriteAttributeString(attrName, "CashOutLimited");
                    break;
                case E_ConstructionRefinanceDataGseRefinancePurposeType.CashOutOther:
                    writer.WriteAttributeString(attrName, "CashOutOther");
                    break;
                case E_ConstructionRefinanceDataGseRefinancePurposeType.NoCashOutFHAStreamlinedRefinance:
                    writer.WriteAttributeString(attrName, "NoCashOutFHAStreamlinedRefinance");
                    break;
                case E_ConstructionRefinanceDataGseRefinancePurposeType.NoCashOutFREOwnedRefinance:
                    writer.WriteAttributeString(attrName, "NoCashOutFREOwnedRefinance");
                    break;
                case E_ConstructionRefinanceDataGseRefinancePurposeType.NoCashOutOther:
                    writer.WriteAttributeString(attrName, "NoCashOutOther");
                    break;
                case E_ConstructionRefinanceDataGseRefinancePurposeType.NoCashOutStreamlinedRefinance:
                    writer.WriteAttributeString(attrName, "NoCashOutStreamlinedRefinance");
                    break;
                case E_ConstructionRefinanceDataGseRefinancePurposeType.ChangeInRateTerm:
                    writer.WriteAttributeString(attrName, "ChangeInRateTerm");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_ConstructionRefinanceDataRefinanceImprovementsType value)
        {
            switch (value)
            {
                case E_ConstructionRefinanceDataRefinanceImprovementsType.Made:
                    writer.WriteAttributeString(attrName, "Made");
                    break;
                case E_ConstructionRefinanceDataRefinanceImprovementsType.ToBeMade:
                    writer.WriteAttributeString(attrName, "ToBeMade");
                    break;
                case E_ConstructionRefinanceDataRefinanceImprovementsType.Unknown:
                    writer.WriteAttributeString(attrName, "Unknown");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_ConstructionRefinanceDataConstructionPurposeType value)
        {
            switch (value)
            {
                case E_ConstructionRefinanceDataConstructionPurposeType.ConstructionOnly:
                    writer.WriteAttributeString(attrName, "ConstructionOnly");
                    break;
                case E_ConstructionRefinanceDataConstructionPurposeType.ConstructionToPermanent:
                    writer.WriteAttributeString(attrName, "ConstructionToPermanent");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_MersMersRegistrationStatusType value)
        {
            switch (value)
            {
                case E_MersMersRegistrationStatusType.Deactivated:
                    writer.WriteAttributeString(attrName, "Deactivated");
                    break;
                case E_MersMersRegistrationStatusType.NotRegistered:
                    writer.WriteAttributeString(attrName, "NotRegistered");
                    break;
                case E_MersMersRegistrationStatusType.PreRegistered:
                    writer.WriteAttributeString(attrName, "PreRegistered");
                    break;
                case E_MersMersRegistrationStatusType.Registered:
                    writer.WriteAttributeString(attrName, "Registered");
                    break;
                case E_MersMersRegistrationStatusType.Validated:
                    writer.WriteAttributeString(attrName, "Validated");
                    break;
                case E_MersMersRegistrationStatusType.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_MiDataMiDurationType value)
        {
            switch (value)
            {
                case E_MiDataMiDurationType.Annual:
                    writer.WriteAttributeString(attrName, "Annual");
                    break;
                case E_MiDataMiDurationType.NotApplicable:
                    writer.WriteAttributeString(attrName, "NotApplicable");
                    break;
                case E_MiDataMiDurationType.PeriodicMonthly:
                    writer.WriteAttributeString(attrName, "PeriodicMonthly");
                    break;
                case E_MiDataMiDurationType.SingleLifeOfLoan:
                    writer.WriteAttributeString(attrName, "SingleLifeOfLoan");
                    break;
                case E_MiDataMiDurationType.SingleSpecific:
                    writer.WriteAttributeString(attrName, "SingleSpecific");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_MiDataMiInitialPremiumAtClosingType value)
        {
            switch (value)
            {
                case E_MiDataMiInitialPremiumAtClosingType.Deferred:
                    writer.WriteAttributeString(attrName, "Deferred");
                    break;
                case E_MiDataMiInitialPremiumAtClosingType.Prepaid:
                    writer.WriteAttributeString(attrName, "Prepaid");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_MiDataMiLtvCutoffType value)
        {
            switch (value)
            {
                case E_MiDataMiLtvCutoffType.AppraisedValue:
                    writer.WriteAttributeString(attrName, "AppraisedValue");
                    break;
                case E_MiDataMiLtvCutoffType.SalesPrice:
                    writer.WriteAttributeString(attrName, "SalesPrice");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_MiDataMiPremiumPaymentType value)
        {
            switch (value)
            {
                case E_MiDataMiPremiumPaymentType.BorrowerPaid:
                    writer.WriteAttributeString(attrName, "BorrowerPaid");
                    break;
                case E_MiDataMiPremiumPaymentType.BothBorrowerAndLenderPaid:
                    writer.WriteAttributeString(attrName, "BothBorrowerAndLenderPaid");
                    break;
                case E_MiDataMiPremiumPaymentType.Financed:
                    writer.WriteAttributeString(attrName, "Financed");
                    break;
                case E_MiDataMiPremiumPaymentType.LenderPaid:
                    writer.WriteAttributeString(attrName, "LenderPaid");
                    break;
                case E_MiDataMiPremiumPaymentType.PaidFromEscrow:
                    writer.WriteAttributeString(attrName, "PaidFromEscrow");
                    break;
                case E_MiDataMiPremiumPaymentType.Prepaid:
                    writer.WriteAttributeString(attrName, "Prepaid");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_MiDataMiPremiumRefundableType value)
        {
            switch (value)
            {
                case E_MiDataMiPremiumRefundableType.NotRefundable:
                    writer.WriteAttributeString(attrName, "NotRefundable");
                    break;
                case E_MiDataMiPremiumRefundableType.Refundable:
                    writer.WriteAttributeString(attrName, "Refundable");
                    break;
                case E_MiDataMiPremiumRefundableType.RefundableWithLimits:
                    writer.WriteAttributeString(attrName, "RefundableWithLimits");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_MiDataMiRenewalCalculationType value)
        {
            switch (value)
            {
                case E_MiDataMiRenewalCalculationType.Constant:
                    writer.WriteAttributeString(attrName, "Constant");
                    break;
                case E_MiDataMiRenewalCalculationType.Declining:
                    writer.WriteAttributeString(attrName, "Declining");
                    break;
                case E_MiDataMiRenewalCalculationType.NoRenewals:
                    writer.WriteAttributeString(attrName, "NoRenewals");
                    break;
                case E_MiDataMiRenewalCalculationType.NotApplicable:
                    writer.WriteAttributeString(attrName, "NotApplicable");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_MiDataMiSourceType value)
        {
            switch (value)
            {
                case E_MiDataMiSourceType.FHA:
                    writer.WriteAttributeString(attrName, "FHA");
                    break;
                case E_MiDataMiSourceType.PMI:
                    writer.WriteAttributeString(attrName, "PMI");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_MiPremiumTaxCodeType value)
        {
            switch (value)
            {
                case E_MiPremiumTaxCodeType.AllTaxes:
                    writer.WriteAttributeString(attrName, "AllTaxes");
                    break;
                case E_MiPremiumTaxCodeType.County:
                    writer.WriteAttributeString(attrName, "County");
                    break;
                case E_MiPremiumTaxCodeType.Municipal:
                    writer.WriteAttributeString(attrName, "Municipal");
                    break;
                case E_MiPremiumTaxCodeType.State:
                    writer.WriteAttributeString(attrName, "State");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_MiRenewalPremiumMonthlyPaymentRoundingType value)
        {
            switch (value)
            {
                case E_MiRenewalPremiumMonthlyPaymentRoundingType.Up:
                    writer.WriteAttributeString(attrName, "Up");
                    break;
                case E_MiRenewalPremiumMonthlyPaymentRoundingType.Down:
                    writer.WriteAttributeString(attrName, "Down");
                    break;
                case E_MiRenewalPremiumMonthlyPaymentRoundingType.None:
                    writer.WriteAttributeString(attrName, "None");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_MiRenewalPremiumSequence value)
        {
            switch (value)
            {
                case E_MiRenewalPremiumSequence.First:
                    writer.WriteAttributeString(attrName, "First");
                    break;
                case E_MiRenewalPremiumSequence.Second:
                    writer.WriteAttributeString(attrName, "Second");
                    break;
                case E_MiRenewalPremiumSequence.Third:
                    writer.WriteAttributeString(attrName, "Third");
                    break;
                case E_MiRenewalPremiumSequence.Fourth:
                    writer.WriteAttributeString(attrName, "Fourth");
                    break;
                case E_MiRenewalPremiumSequence.Fifth:
                    writer.WriteAttributeString(attrName, "Fifth");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_MortgageTermsLoanAmortizationType value)
        {
            switch (value)
            {
                case E_MortgageTermsLoanAmortizationType.AdjustableRate:
                    writer.WriteAttributeString(attrName, "AdjustableRate");
                    break;
                case E_MortgageTermsLoanAmortizationType.Fixed:
                    writer.WriteAttributeString(attrName, "Fixed");
                    break;
                case E_MortgageTermsLoanAmortizationType.GraduatedPaymentMortgage:
                    writer.WriteAttributeString(attrName, "GraduatedPaymentMortgage");
                    break;
                case E_MortgageTermsLoanAmortizationType.GrowingEquityMortgage:
                    writer.WriteAttributeString(attrName, "GrowingEquityMortgage");
                    break;
                case E_MortgageTermsLoanAmortizationType.OtherAmortizationType:
                    writer.WriteAttributeString(attrName, "OtherAmortizationType");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_MortgageTermsMortgageType value)
        {
            switch (value)
            {
                case E_MortgageTermsMortgageType.Conventional:
                    writer.WriteAttributeString(attrName, "Conventional");
                    break;
                case E_MortgageTermsMortgageType.FarmersHomeAdministration:
                    writer.WriteAttributeString(attrName, "FarmersHomeAdministration");
                    break;
                case E_MortgageTermsMortgageType.FHA:
                    writer.WriteAttributeString(attrName, "FHA");
                    break;
                case E_MortgageTermsMortgageType.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
                case E_MortgageTermsMortgageType.VA:
                    writer.WriteAttributeString(attrName, "VA");
                    break;
                case E_MortgageTermsMortgageType.HELOC:
                    writer.WriteAttributeString(attrName, "HELOC");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_PropertyBuildingStatusType value)
        {
            switch (value)
            {
                case E_PropertyBuildingStatusType.Existing:
                    writer.WriteAttributeString(attrName, "Existing");
                    break;
                case E_PropertyBuildingStatusType.Proposed:
                    writer.WriteAttributeString(attrName, "Proposed");
                    break;
                case E_PropertyBuildingStatusType.SubjectToAlterationImprovementRepairAndRehabilitation:
                    writer.WriteAttributeString(attrName, "SubjectToAlterationImprovementRepairAndRehabilitation");
                    break;
                case E_PropertyBuildingStatusType.SubstantiallyRehabilitated:
                    writer.WriteAttributeString(attrName, "SubstantiallyRehabilitated");
                    break;
                case E_PropertyBuildingStatusType.UnderConstruction:
                    writer.WriteAttributeString(attrName, "UnderConstruction");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_LegalDescriptionType value)
        {
            switch (value)
            {
                case E_LegalDescriptionType.MetesAndBounds:
                    writer.WriteAttributeString(attrName, "MetesAndBounds");
                    break;
                case E_LegalDescriptionType.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_ValuationMethodType value)
        {
            switch (value)
            {
                case E_ValuationMethodType.FNM1004:
                    writer.WriteAttributeString(attrName, "FNM1004");
                    break;
                case E_ValuationMethodType.EmployeeRelocationCouncil2001:
                    writer.WriteAttributeString(attrName, "EmployeeRelocationCouncil2001");
                    break;
                case E_ValuationMethodType.FNM1073:
                    writer.WriteAttributeString(attrName, "FNM1073");
                    break;
                case E_ValuationMethodType.FNM1025:
                    writer.WriteAttributeString(attrName, "FNM1025");
                    break;
                case E_ValuationMethodType.FNM2055Exterior:
                    writer.WriteAttributeString(attrName, "FNM2055Exterior");
                    break;
                case E_ValuationMethodType.FNM2065:
                    writer.WriteAttributeString(attrName, "FNM2065");
                    break;
                case E_ValuationMethodType.FRE2070Interior:
                    writer.WriteAttributeString(attrName, "FRE2070Interior");
                    break;
                case E_ValuationMethodType.FRE2070Exterior:
                    writer.WriteAttributeString(attrName, "FRE2070Exterior");
                    break;
                case E_ValuationMethodType.FNM2075:
                    writer.WriteAttributeString(attrName, "FNM2075");
                    break;
                case E_ValuationMethodType.BrokerPriceOpinion:
                    writer.WriteAttributeString(attrName, "BrokerPriceOpinion");
                    break;
                case E_ValuationMethodType.AutomatedValuationModel:
                    writer.WriteAttributeString(attrName, "AutomatedValuationModel");
                    break;
                case E_ValuationMethodType.TaxValuation:
                    writer.WriteAttributeString(attrName, "TaxValuation");
                    break;
                case E_ValuationMethodType.DriveBy:
                    writer.WriteAttributeString(attrName, "DriveBy");
                    break;
                case E_ValuationMethodType.FullAppraisal:
                    writer.WriteAttributeString(attrName, "FullAppraisal");
                    break;
                case E_ValuationMethodType.None:
                    writer.WriteAttributeString(attrName, "None");
                    break;
                case E_ValuationMethodType.FNM2055InteriorAndExterior:
                    writer.WriteAttributeString(attrName, "FNM2055InteriorAndExterior");
                    break;
                case E_ValuationMethodType.FNM2095Exterior:
                    writer.WriteAttributeString(attrName, "FNM2095Exterior");
                    break;
                case E_ValuationMethodType.FNM2095InteriorAndExterior:
                    writer.WriteAttributeString(attrName, "FNM2095InteriorAndExterior");
                    break;
                case E_ValuationMethodType.PriorAppraisalUsed:
                    writer.WriteAttributeString(attrName, "PriorAppraisalUsed");
                    break;
                case E_ValuationMethodType.Form261805:
                    writer.WriteAttributeString(attrName, "Form261805");
                    break;
                case E_ValuationMethodType.Form268712:
                    writer.WriteAttributeString(attrName, "Form268712");
                    break;
                case E_ValuationMethodType.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_DetailsRecordingJurisdictionType value)
        {
            switch (value)
            {
                case E_DetailsRecordingJurisdictionType.County:
                    writer.WriteAttributeString(attrName, "County");
                    break;
                case E_DetailsRecordingJurisdictionType.Parish:
                    writer.WriteAttributeString(attrName, "Parish");
                    break;
                case E_DetailsRecordingJurisdictionType.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_ProposedHousingExpenseHousingExpenseType value)
        {
            switch (value)
            {
                case E_ProposedHousingExpenseHousingExpenseType.FirstMortgagePrincipalAndInterest:
                    writer.WriteAttributeString(attrName, "FirstMortgagePrincipalAndInterest");
                    break;
                case E_ProposedHousingExpenseHousingExpenseType.GroundRent:
                    writer.WriteAttributeString(attrName, "GroundRent");
                    break;
                case E_ProposedHousingExpenseHousingExpenseType.HazardInsurance:
                    writer.WriteAttributeString(attrName, "HazardInsurance");
                    break;
                case E_ProposedHousingExpenseHousingExpenseType.HomeownersAssociationDuesAndCondominiumFees:
                    writer.WriteAttributeString(attrName, "HomeownersAssociationDuesAndCondominiumFees");
                    break;
                case E_ProposedHousingExpenseHousingExpenseType.MI:
                    writer.WriteAttributeString(attrName, "MI");
                    break;
                case E_ProposedHousingExpenseHousingExpenseType.OtherHousingExpense:
                    writer.WriteAttributeString(attrName, "OtherHousingExpense");
                    break;
                case E_ProposedHousingExpenseHousingExpenseType.OtherMortgageLoanPrincipalAndInterest:
                    writer.WriteAttributeString(attrName, "OtherMortgageLoanPrincipalAndInterest");
                    break;
                case E_ProposedHousingExpenseHousingExpenseType.RealEstateTax:
                    writer.WriteAttributeString(attrName, "RealEstateTax");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_ReoPropertyDispositionStatusType value)
        {
            switch (value)
            {
                case E_ReoPropertyDispositionStatusType.PendingSale:
                    writer.WriteAttributeString(attrName, "PendingSale");
                    break;
                case E_ReoPropertyDispositionStatusType.RetainForRental:
                    writer.WriteAttributeString(attrName, "RetainForRental");
                    break;
                case E_ReoPropertyDispositionStatusType.RetainForPrimaryOrSecondaryResidence:
                    writer.WriteAttributeString(attrName, "RetainForPrimaryOrSecondaryResidence");
                    break;
                case E_ReoPropertyDispositionStatusType.Sold:
                    writer.WriteAttributeString(attrName, "Sold");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_ReoPropertyGsePropertyType value)
        {
            switch (value)
            {
                case E_ReoPropertyGsePropertyType.SingleFamily:
                    writer.WriteAttributeString(attrName, "SingleFamily");
                    break;
                case E_ReoPropertyGsePropertyType.Condominium:
                    writer.WriteAttributeString(attrName, "Condominium");
                    break;
                case E_ReoPropertyGsePropertyType.Townhouse:
                    writer.WriteAttributeString(attrName, "Townhouse");
                    break;
                case E_ReoPropertyGsePropertyType.Cooperative:
                    writer.WriteAttributeString(attrName, "Cooperative");
                    break;
                case E_ReoPropertyGsePropertyType.TwoToFourUnitProperty:
                    writer.WriteAttributeString(attrName, "TwoToFourUnitProperty");
                    break;
                case E_ReoPropertyGsePropertyType.MultifamilyMoreThanFourUnits:
                    writer.WriteAttributeString(attrName, "MultifamilyMoreThanFourUnits");
                    break;
                case E_ReoPropertyGsePropertyType.ManufacturedMobileHome:
                    writer.WriteAttributeString(attrName, "ManufacturedMobileHome");
                    break;
                case E_ReoPropertyGsePropertyType.CommercialNonResidential:
                    writer.WriteAttributeString(attrName, "CommercialNonResidential");
                    break;
                case E_ReoPropertyGsePropertyType.MixedUseResidential:
                    writer.WriteAttributeString(attrName, "MixedUseResidential");
                    break;
                case E_ReoPropertyGsePropertyType.Farm:
                    writer.WriteAttributeString(attrName, "Farm");
                    break;
                case E_ReoPropertyGsePropertyType.HomeAndBusinessCombined:
                    writer.WriteAttributeString(attrName, "HomeAndBusinessCombined");
                    break;
                case E_ReoPropertyGsePropertyType.Land:
                    writer.WriteAttributeString(attrName, "Land");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_RespaFeeRespaSectionClassificationType value)
        {
            switch (value)
            {
                case E_RespaFeeRespaSectionClassificationType._100_GrossAmountDueFromBorrower:
                    writer.WriteAttributeString(attrName, "100:GrossAmountDueFromBorrower");
                    break;
                case E_RespaFeeRespaSectionClassificationType._200_AmountsPaidByOrInBehalfOfBorrower:
                    writer.WriteAttributeString(attrName, "200:AmountsPaidByOrInBehalfOfBorrower");
                    break;
                case E_RespaFeeRespaSectionClassificationType._300_CashAtSettlementFromToBorrower:
                    writer.WriteAttributeString(attrName, "300:CashAtSettlementFromToBorrower");
                    break;
                case E_RespaFeeRespaSectionClassificationType._400_GrossAmountDueToSeller:
                    writer.WriteAttributeString(attrName, "400:GrossAmountDueToSeller");
                    break;
                case E_RespaFeeRespaSectionClassificationType._500_ReductionsInAmountDueSeller:
                    writer.WriteAttributeString(attrName, "500:ReductionsInAmountDueSeller");
                    break;
                case E_RespaFeeRespaSectionClassificationType._600_CashAtSettlementToFromSeller:
                    writer.WriteAttributeString(attrName, "600:CashAtSettlementToFromSeller");
                    break;
                case E_RespaFeeRespaSectionClassificationType._700_DivisionOfCommissions:
                    writer.WriteAttributeString(attrName, "700:DivisionOfCommissions");
                    break;
                case E_RespaFeeRespaSectionClassificationType._800_LoanFees:
                    writer.WriteAttributeString(attrName, "800:LoanFees");
                    break;
                case E_RespaFeeRespaSectionClassificationType._900_LenderRequiredPaidInAdvance:
                    writer.WriteAttributeString(attrName, "900:LenderRequiredPaidInAdvance");
                    break;
                case E_RespaFeeRespaSectionClassificationType._1000_ReservesDepositedWithLender:
                    writer.WriteAttributeString(attrName, "1000:ReservesDepositedWithLender");
                    break;
                case E_RespaFeeRespaSectionClassificationType._1100_TitleCharges:
                    writer.WriteAttributeString(attrName, "1100:TitleCharges");
                    break;
                case E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges:
                    writer.WriteAttributeString(attrName, "1200:RecordingAndTransferCharges");
                    break;
                case E_RespaFeeRespaSectionClassificationType._1300_AdditionalSettlementCharges:
                    writer.WriteAttributeString(attrName, "1300:AdditionalSettlementCharges");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_RespaFeeResponsiblePartyType value)
        {
            switch (value)
            {
                case E_RespaFeeResponsiblePartyType.Buyer:
                    writer.WriteAttributeString(attrName, "Buyer");
                    break;
                case E_RespaFeeResponsiblePartyType.Seller:
                    writer.WriteAttributeString(attrName, "Seller");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_RespaFeeType value)
        {
            switch (value)
            {
                case E_RespaFeeType._203KDiscountOnRepairs:
                    writer.WriteAttributeString(attrName, "203KDiscountOnRepairs");
                    break;
                case E_RespaFeeType._203KPermits:
                    writer.WriteAttributeString(attrName, "203KPermits");
                    break;
                case E_RespaFeeType._203KArchitecturalAndEngineeringFee:
                    writer.WriteAttributeString(attrName, "203KArchitecturalAndEngineeringFee");
                    break;
                case E_RespaFeeType._203KInspectionFee:
                    writer.WriteAttributeString(attrName, "203KInspectionFee");
                    break;
                case E_RespaFeeType._203KSupplementalOriginationFee:
                    writer.WriteAttributeString(attrName, "203KSupplementalOriginationFee");
                    break;
                case E_RespaFeeType._203KConsultantFee:
                    writer.WriteAttributeString(attrName, "203KConsultantFee");
                    break;
                case E_RespaFeeType._203KTitleUpdate:
                    writer.WriteAttributeString(attrName, "203KTitleUpdate");
                    break;
                case E_RespaFeeType.AbstractOrTitleSearchFee:
                    writer.WriteAttributeString(attrName, "AbstractOrTitleSearchFee");
                    break;
                case E_RespaFeeType.AmortizationFee:
                    writer.WriteAttributeString(attrName, "AmortizationFee");
                    break;
                case E_RespaFeeType.ApplicationFee:
                    writer.WriteAttributeString(attrName, "ApplicationFee");
                    break;
                case E_RespaFeeType.AppraisalFee:
                    writer.WriteAttributeString(attrName, "AppraisalFee");
                    break;
                case E_RespaFeeType.AssignmentFee:
                    writer.WriteAttributeString(attrName, "AssignmentFee");
                    break;
                case E_RespaFeeType.AssignmentRecordingFee:
                    writer.WriteAttributeString(attrName, "AssignmentRecordingFee");
                    break;
                case E_RespaFeeType.AssumptionFee:
                    writer.WriteAttributeString(attrName, "AssumptionFee");
                    break;
                case E_RespaFeeType.AttorneyFee:
                    writer.WriteAttributeString(attrName, "AttorneyFee");
                    break;
                case E_RespaFeeType.BondReviewFee:
                    writer.WriteAttributeString(attrName, "BondReviewFee");
                    break;
                case E_RespaFeeType.CityCountyDeedTaxStampFee:
                    writer.WriteAttributeString(attrName, "CityCountyDeedTaxStampFee");
                    break;
                case E_RespaFeeType.CityCountyMortgageTaxStampFee:
                    writer.WriteAttributeString(attrName, "CityCountyMortgageTaxStampFee");
                    break;
                case E_RespaFeeType.CLOAccessFee:
                    writer.WriteAttributeString(attrName, "CLOAccessFee");
                    break;
                case E_RespaFeeType.CommitmentFee:
                    writer.WriteAttributeString(attrName, "CommitmentFee");
                    break;
                case E_RespaFeeType.CopyFaxFee:
                    writer.WriteAttributeString(attrName, "CopyFaxFee");
                    break;
                case E_RespaFeeType.CourierFee:
                    writer.WriteAttributeString(attrName, "CourierFee");
                    break;
                case E_RespaFeeType.CreditReportFee:
                    writer.WriteAttributeString(attrName, "CreditReportFee");
                    break;
                case E_RespaFeeType.DeedRecordingFee:
                    writer.WriteAttributeString(attrName, "DeedRecordingFee");
                    break;
                case E_RespaFeeType.DocumentPreparationFee:
                    writer.WriteAttributeString(attrName, "DocumentPreparationFee");
                    break;
                case E_RespaFeeType.DocumentaryStampFee:
                    writer.WriteAttributeString(attrName, "DocumentaryStampFee");
                    break;
                case E_RespaFeeType.EscrowWaiverFee:
                    writer.WriteAttributeString(attrName, "EscrowWaiverFee");
                    break;
                case E_RespaFeeType.FloodCertification:
                    writer.WriteAttributeString(attrName, "FloodCertification");
                    break;
                case E_RespaFeeType.GeneralCounselFee:
                    writer.WriteAttributeString(attrName, "GeneralCounselFee");
                    break;
                case E_RespaFeeType.InspectionFee:
                    writer.WriteAttributeString(attrName, "InspectionFee");
                    break;
                case E_RespaFeeType.LoanDiscountPoints:
                    writer.WriteAttributeString(attrName, "LoanDiscountPoints");
                    break;
                case E_RespaFeeType.LoanOriginationFee:
                    writer.WriteAttributeString(attrName, "LoanOriginationFee");
                    break;
                case E_RespaFeeType.ModificationFee:
                    writer.WriteAttributeString(attrName, "ModificationFee");
                    break;
                case E_RespaFeeType.MortgageBrokerFee:
                    writer.WriteAttributeString(attrName, "MortgageBrokerFee");
                    break;
                case E_RespaFeeType.MortgageRecordingFee:
                    writer.WriteAttributeString(attrName, "MortgageRecordingFee");
                    break;
                case E_RespaFeeType.MunicipalLienCertificateFee:
                    writer.WriteAttributeString(attrName, "MunicipalLienCertificateFee");
                    break;
                case E_RespaFeeType.MunicipalLienCertificateRecordingFee:
                    writer.WriteAttributeString(attrName, "MunicipalLienCertificateRecordingFee");
                    break;
                case E_RespaFeeType.NewLoanAdministrationFee:
                    writer.WriteAttributeString(attrName, "NewLoanAdministrationFee");
                    break;
                case E_RespaFeeType.NotaryFee:
                    writer.WriteAttributeString(attrName, "NotaryFee");
                    break;
                case E_RespaFeeType.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
                case E_RespaFeeType.PestInspectionFee:
                    writer.WriteAttributeString(attrName, "PestInspectionFee");
                    break;
                case E_RespaFeeType.ProcessingFee:
                    writer.WriteAttributeString(attrName, "ProcessingFee");
                    break;
                case E_RespaFeeType.RedrawFee:
                    writer.WriteAttributeString(attrName, "RedrawFee");
                    break;
                case E_RespaFeeType.RealEstateCommission:
                    writer.WriteAttributeString(attrName, "RealEstateCommission");
                    break;
                case E_RespaFeeType.ReinspectionFee:
                    writer.WriteAttributeString(attrName, "ReinspectionFee");
                    break;
                case E_RespaFeeType.ReleaseRecordingFee:
                    writer.WriteAttributeString(attrName, "ReleaseRecordingFee");
                    break;
                case E_RespaFeeType.RuralHousingFee:
                    writer.WriteAttributeString(attrName, "RuralHousingFee");
                    break;
                case E_RespaFeeType.SettlementOrClosingFee:
                    writer.WriteAttributeString(attrName, "SettlementOrClosingFee");
                    break;
                case E_RespaFeeType.StateDeedTaxStampFee:
                    writer.WriteAttributeString(attrName, "StateDeedTaxStampFee");
                    break;
                case E_RespaFeeType.StateMortgageTaxStampFee:
                    writer.WriteAttributeString(attrName, "StateMortgageTaxStampFee");
                    break;
                case E_RespaFeeType.SurveyFee:
                    writer.WriteAttributeString(attrName, "SurveyFee");
                    break;
                case E_RespaFeeType.TaxRelatedServiceFee:
                    writer.WriteAttributeString(attrName, "TaxRelatedServiceFee");
                    break;
                case E_RespaFeeType.TitleExaminationFee:
                    writer.WriteAttributeString(attrName, "TitleExaminationFee");
                    break;
                case E_RespaFeeType.TitleInsuranceBinderFee:
                    writer.WriteAttributeString(attrName, "TitleInsuranceBinderFee");
                    break;
                case E_RespaFeeType.TitleInsuranceFee:
                    writer.WriteAttributeString(attrName, "TitleInsuranceFee");
                    break;
                case E_RespaFeeType.UnderwritingFee:
                    writer.WriteAttributeString(attrName, "UnderwritingFee");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_RespaFeePaidToType value)
        {
            switch (value)
            {
                case E_RespaFeePaidToType.Broker:
                    writer.WriteAttributeString(attrName, "Broker");
                    break;
                case E_RespaFeePaidToType.Lender:
                    writer.WriteAttributeString(attrName, "Lender");
                    break;
                case E_RespaFeePaidToType.Investor:
                    writer.WriteAttributeString(attrName, "Investor");
                    break;
                case E_RespaFeePaidToType.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_PaymentCollectedByType value)
        {
            switch (value)
            {
                case E_PaymentCollectedByType.Broker:
                    writer.WriteAttributeString(attrName, "Broker");
                    break;
                case E_PaymentCollectedByType.Lender:
                    writer.WriteAttributeString(attrName, "Lender");
                    break;
                case E_PaymentCollectedByType.Investor:
                    writer.WriteAttributeString(attrName, "Investor");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_PaymentPaidByType value)
        {
            switch (value)
            {
                case E_PaymentPaidByType.Buyer:
                    writer.WriteAttributeString(attrName, "Buyer");
                    break;
                case E_PaymentPaidByType.Lender:
                    writer.WriteAttributeString(attrName, "Lender");
                    break;
                case E_PaymentPaidByType.Seller:
                    writer.WriteAttributeString(attrName, "Seller");
                    break;
                case E_PaymentPaidByType.ThirdParty:
                    writer.WriteAttributeString(attrName, "ThirdParty");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_PaymentProcessType value)
        {
            switch (value)
            {
                case E_PaymentProcessType.Closing:
                    writer.WriteAttributeString(attrName, "Closing");
                    break;
                case E_PaymentProcessType.Processing:
                    writer.WriteAttributeString(attrName, "Processing");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_TitleHolderLandTrustType value)
        {
            switch (value)
            {
                case E_TitleHolderLandTrustType.IllinoisLandTrust:
                    writer.WriteAttributeString(attrName, "IllinoisLandTrust");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_PurchaseCreditSourceType value)
        {
            switch (value)
            {
                case E_PurchaseCreditSourceType.BorrowerPaidOutsideClosing:
                    writer.WriteAttributeString(attrName, "BorrowerPaidOutsideClosing");
                    break;
                case E_PurchaseCreditSourceType.PropertySeller:
                    writer.WriteAttributeString(attrName, "PropertySeller");
                    break;
                case E_PurchaseCreditSourceType.Lender:
                    writer.WriteAttributeString(attrName, "Lender");
                    break;
                case E_PurchaseCreditSourceType.NonParentRelative:
                    writer.WriteAttributeString(attrName, "NonParentRelative");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_PurchaseCreditType value)
        {
            switch (value)
            {
                case E_PurchaseCreditType.EarnestMoney:
                    writer.WriteAttributeString(attrName, "EarnestMoney");
                    break;
                case E_PurchaseCreditType.RelocationFunds:
                    writer.WriteAttributeString(attrName, "RelocationFunds");
                    break;
                case E_PurchaseCreditType.EmployerAssistedHousing:
                    writer.WriteAttributeString(attrName, "EmployerAssistedHousing");
                    break;
                case E_PurchaseCreditType.LeasePurchaseFund:
                    writer.WriteAttributeString(attrName, "LeasePurchaseFund");
                    break;
                case E_PurchaseCreditType.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_BorrowerPrintPositionType value)
        {
            switch (value)
            {
                case E_BorrowerPrintPositionType.Borrower:
                    writer.WriteAttributeString(attrName, "Borrower");
                    break;
                case E_BorrowerPrintPositionType.CoBorrower:
                    writer.WriteAttributeString(attrName, "CoBorrower");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_BorrowerJointAssetLiabilityReportingType value)
        {
            switch (value)
            {
                case E_BorrowerJointAssetLiabilityReportingType.Jointly:
                    writer.WriteAttributeString(attrName, "Jointly");
                    break;
                case E_BorrowerJointAssetLiabilityReportingType.NotJointly:
                    writer.WriteAttributeString(attrName, "NotJointly");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_BorrowerMaritalStatusType value)
        {
            switch (value)
            {
                case E_BorrowerMaritalStatusType.Married:
                    writer.WriteAttributeString(attrName, "Married");
                    break;
                case E_BorrowerMaritalStatusType.NotProvided:
                    writer.WriteAttributeString(attrName, "NotProvided");
                    break;
                case E_BorrowerMaritalStatusType.Separated:
                    writer.WriteAttributeString(attrName, "Separated");
                    break;
                case E_BorrowerMaritalStatusType.Unknown:
                    writer.WriteAttributeString(attrName, "Unknown");
                    break;
                case E_BorrowerMaritalStatusType.Unmarried:
                    writer.WriteAttributeString(attrName, "Unmarried");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_BorrowerRelationshipTitleType value)
        {
            switch (value)
            {
                case E_BorrowerRelationshipTitleType.AMarriedMan:
                    writer.WriteAttributeString(attrName, "AMarriedMan");
                    break;
                case E_BorrowerRelationshipTitleType.AMarriedPerson:
                    writer.WriteAttributeString(attrName, "AMarriedPerson");
                    break;
                case E_BorrowerRelationshipTitleType.AMarriedWoman:
                    writer.WriteAttributeString(attrName, "AMarriedWoman");
                    break;
                case E_BorrowerRelationshipTitleType.AnUnmarriedMan:
                    writer.WriteAttributeString(attrName, "AnUnmarriedMan");
                    break;
                case E_BorrowerRelationshipTitleType.AnUnmarriedPerson:
                    writer.WriteAttributeString(attrName, "AnUnmarriedPerson");
                    break;
                case E_BorrowerRelationshipTitleType.AnUnmarriedWoman:
                    writer.WriteAttributeString(attrName, "AnUnmarriedWoman");
                    break;
                case E_BorrowerRelationshipTitleType.ASingleMan:
                    writer.WriteAttributeString(attrName, "ASingleMan");
                    break;
                case E_BorrowerRelationshipTitleType.ASinglePerson:
                    writer.WriteAttributeString(attrName, "ASinglePerson");
                    break;
                case E_BorrowerRelationshipTitleType.ASingleWoman:
                    writer.WriteAttributeString(attrName, "ASingleWoman");
                    break;
                case E_BorrowerRelationshipTitleType.AWidow:
                    writer.WriteAttributeString(attrName, "AWidow");
                    break;
                case E_BorrowerRelationshipTitleType.AWidower:
                    writer.WriteAttributeString(attrName, "AWidower");
                    break;
                case E_BorrowerRelationshipTitleType.HerHusband:
                    writer.WriteAttributeString(attrName, "HerHusband");
                    break;
                case E_BorrowerRelationshipTitleType.HisWife:
                    writer.WriteAttributeString(attrName, "HisWife");
                    break;
                case E_BorrowerRelationshipTitleType.HusbandAndWife:
                    writer.WriteAttributeString(attrName, "HusbandAndWife");
                    break;
                case E_BorrowerRelationshipTitleType.NotApplicable:
                    writer.WriteAttributeString(attrName, "NotApplicable");
                    break;
                case E_BorrowerRelationshipTitleType.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
                case E_BorrowerRelationshipTitleType.WifeAndHusband:
                    writer.WriteAttributeString(attrName, "WifeAndHusband");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_AliasType value)
        {
            switch (value)
            {
                case E_AliasType.FormerlyKnownAs:
                    writer.WriteAttributeString(attrName, "FormerlyKnownAs");
                    break;
                case E_AliasType.NowKnownAs:
                    writer.WriteAttributeString(attrName, "NowKnownAs");
                    break;
                case E_AliasType.AlsoKnownAs:
                    writer.WriteAttributeString(attrName, "AlsoKnownAs");
                    break;
                case E_AliasType.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_ResidenceBorrowerResidencyBasisType value)
        {
            switch (value)
            {
                case E_ResidenceBorrowerResidencyBasisType.LivingRentFree:
                    writer.WriteAttributeString(attrName, "LivingRentFree");
                    break;
                case E_ResidenceBorrowerResidencyBasisType.Own:
                    writer.WriteAttributeString(attrName, "Own");
                    break;
                case E_ResidenceBorrowerResidencyBasisType.Rent:
                    writer.WriteAttributeString(attrName, "Rent");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_ResidenceBorrowerResidencyType value)
        {
            switch (value)
            {
                case E_ResidenceBorrowerResidencyType.Current:
                    writer.WriteAttributeString(attrName, "Current");
                    break;
                case E_ResidenceBorrowerResidencyType.Prior:
                    writer.WriteAttributeString(attrName, "Prior");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CurrentIncomeIncomeType value)
        {
            switch (value)
            {
                case E_CurrentIncomeIncomeType.AlimonyChildSupport:
                    writer.WriteAttributeString(attrName, "AlimonyChildSupport");
                    break;
                case E_CurrentIncomeIncomeType.AutomobileExpenseAccount:
                    writer.WriteAttributeString(attrName, "AutomobileExpenseAccount");
                    break;
                case E_CurrentIncomeIncomeType.Base:
                    writer.WriteAttributeString(attrName, "Base");
                    break;
                case E_CurrentIncomeIncomeType.Bonus:
                    writer.WriteAttributeString(attrName, "Bonus");
                    break;
                case E_CurrentIncomeIncomeType.Commissions:
                    writer.WriteAttributeString(attrName, "Commissions");
                    break;
                case E_CurrentIncomeIncomeType.DividendsInterest:
                    writer.WriteAttributeString(attrName, "DividendsInterest");
                    break;
                case E_CurrentIncomeIncomeType.FosterCare:
                    writer.WriteAttributeString(attrName, "FosterCare");
                    break;
                case E_CurrentIncomeIncomeType.NetRentalIncome:
                    writer.WriteAttributeString(attrName, "NetRentalIncome");
                    break;
                case E_CurrentIncomeIncomeType.NotesReceivableInstallment:
                    writer.WriteAttributeString(attrName, "NotesReceivableInstallment");
                    break;
                case E_CurrentIncomeIncomeType.OtherTypesOfIncome:
                    writer.WriteAttributeString(attrName, "OtherTypesOfIncome");
                    break;
                case E_CurrentIncomeIncomeType.Overtime:
                    writer.WriteAttributeString(attrName, "Overtime");
                    break;
                case E_CurrentIncomeIncomeType.Pension:
                    writer.WriteAttributeString(attrName, "Pension");
                    break;
                case E_CurrentIncomeIncomeType.SocialSecurity:
                    writer.WriteAttributeString(attrName, "SocialSecurity");
                    break;
                case E_CurrentIncomeIncomeType.SubjectPropertyNetCashFlow:
                    writer.WriteAttributeString(attrName, "SubjectPropertyNetCashFlow");
                    break;
                case E_CurrentIncomeIncomeType.Trust:
                    writer.WriteAttributeString(attrName, "Trust");
                    break;
                case E_CurrentIncomeIncomeType.Unemployment:
                    writer.WriteAttributeString(attrName, "Unemployment");
                    break;
                case E_CurrentIncomeIncomeType.PublicAssistance:
                    writer.WriteAttributeString(attrName, "PublicAssistance");
                    break;
                case E_CurrentIncomeIncomeType.VABenefitsNonEducational:
                    writer.WriteAttributeString(attrName, "VABenefitsNonEducational");
                    break;
                case E_CurrentIncomeIncomeType.MortgageDifferential:
                    writer.WriteAttributeString(attrName, "MortgageDifferential");
                    break;
                case E_CurrentIncomeIncomeType.MilitaryBasePay:
                    writer.WriteAttributeString(attrName, "MilitaryBasePay");
                    break;
                case E_CurrentIncomeIncomeType.MilitaryRationsAllowance:
                    writer.WriteAttributeString(attrName, "MilitaryRationsAllowance");
                    break;
                case E_CurrentIncomeIncomeType.MilitaryFlightPay:
                    writer.WriteAttributeString(attrName, "MilitaryFlightPay");
                    break;
                case E_CurrentIncomeIncomeType.MilitaryHazardPay:
                    writer.WriteAttributeString(attrName, "MilitaryHazardPay");
                    break;
                case E_CurrentIncomeIncomeType.MilitaryClothesAllowance:
                    writer.WriteAttributeString(attrName, "MilitaryClothesAllowance");
                    break;
                case E_CurrentIncomeIncomeType.MilitaryQuartersAllowance:
                    writer.WriteAttributeString(attrName, "MilitaryQuartersAllowance");
                    break;
                case E_CurrentIncomeIncomeType.MilitaryPropPay:
                    writer.WriteAttributeString(attrName, "MilitaryPropPay");
                    break;
                case E_CurrentIncomeIncomeType.MilitaryOverseasPay:
                    writer.WriteAttributeString(attrName, "MilitaryOverseasPay");
                    break;
                case E_CurrentIncomeIncomeType.MilitaryCombatPay:
                    writer.WriteAttributeString(attrName, "MilitaryCombatPay");
                    break;
                case E_CurrentIncomeIncomeType.MilitaryVariableHousingAllowance:
                    writer.WriteAttributeString(attrName, "MilitaryVariableHousingAllowance");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_DeclarationCitizenshipResidencyType value)
        {
            switch (value)
            {
                case E_DeclarationCitizenshipResidencyType.USCitizen:
                    writer.WriteAttributeString(attrName, "USCitizen");
                    break;
                case E_DeclarationCitizenshipResidencyType.PermanentResidentAlien:
                    writer.WriteAttributeString(attrName, "PermanentResidentAlien");
                    break;
                case E_DeclarationCitizenshipResidencyType.NonPermanentResidentAlien:
                    writer.WriteAttributeString(attrName, "NonPermanentResidentAlien");
                    break;
                case E_DeclarationCitizenshipResidencyType.NonResidentAlien:
                    writer.WriteAttributeString(attrName, "NonResidentAlien");
                    break;
                case E_DeclarationCitizenshipResidencyType.Unknown:
                    writer.WriteAttributeString(attrName, "Unknown");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_DeclarationHomeownerPastThreeYearsType value)
        {
            switch (value)
            {
                case E_DeclarationHomeownerPastThreeYearsType.Yes:
                    writer.WriteAttributeString(attrName, "Yes");
                    break;
                case E_DeclarationHomeownerPastThreeYearsType.No:
                    writer.WriteAttributeString(attrName, "No");
                    break;
                case E_DeclarationHomeownerPastThreeYearsType.Unknown:
                    writer.WriteAttributeString(attrName, "Unknown");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_DeclarationIntentToOccupyType value)
        {
            switch (value)
            {
                case E_DeclarationIntentToOccupyType.Yes:
                    writer.WriteAttributeString(attrName, "Yes");
                    break;
                case E_DeclarationIntentToOccupyType.No:
                    writer.WriteAttributeString(attrName, "No");
                    break;
                case E_DeclarationIntentToOccupyType.Unknown:
                    writer.WriteAttributeString(attrName, "Unknown");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_DeclarationPriorPropertyTitleType value)
        {
            switch (value)
            {
                case E_DeclarationPriorPropertyTitleType.Sole:
                    writer.WriteAttributeString(attrName, "Sole");
                    break;
                case E_DeclarationPriorPropertyTitleType.JointWithSpouse:
                    writer.WriteAttributeString(attrName, "JointWithSpouse");
                    break;
                case E_DeclarationPriorPropertyTitleType.JointWithOtherThanSpouse:
                    writer.WriteAttributeString(attrName, "JointWithOtherThanSpouse");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_DeclarationPriorPropertyUsageType value)
        {
            switch (value)
            {
                case E_DeclarationPriorPropertyUsageType.Investment:
                    writer.WriteAttributeString(attrName, "Investment");
                    break;
                case E_DeclarationPriorPropertyUsageType.PrimaryResidence:
                    writer.WriteAttributeString(attrName, "PrimaryResidence");
                    break;
                case E_DeclarationPriorPropertyUsageType.SecondaryResidence:
                    writer.WriteAttributeString(attrName, "SecondaryResidence");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_ExplanationType value)
        {
            switch (value)
            {
                case E_ExplanationType.AlimonyChildSupport:
                    writer.WriteAttributeString(attrName, "AlimonyChildSupport");
                    break;
                case E_ExplanationType.BorrowedDownPayment:
                    writer.WriteAttributeString(attrName, "BorrowedDownPayment");
                    break;
                case E_ExplanationType.CoMakerEndorserOnNote:
                    writer.WriteAttributeString(attrName, "CoMakerEndorserOnNote");
                    break;
                case E_ExplanationType.DeclaredBankruptcyPastSevenYears:
                    writer.WriteAttributeString(attrName, "DeclaredBankruptcyPastSevenYears");
                    break;
                case E_ExplanationType.DelinquencyOrDefault:
                    writer.WriteAttributeString(attrName, "DelinquencyOrDefault");
                    break;
                case E_ExplanationType.DirectIndirectForeclosedPropertyPastSevenYears:
                    writer.WriteAttributeString(attrName, "DirectIndirectForeclosedPropertyPastSevenYears");
                    break;
                case E_ExplanationType.ObligatedOnLoanForeclosedOrDeedInLieuOfJudgement:
                    writer.WriteAttributeString(attrName, "ObligatedOnLoanForeclosedOrDeedInLieuOfJudgement");
                    break;
                case E_ExplanationType.OutstandingJudgments:
                    writer.WriteAttributeString(attrName, "OutstandingJudgments");
                    break;
                case E_ExplanationType.PartyToLawsuit:
                    writer.WriteAttributeString(attrName, "PartyToLawsuit");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_FhaVaBorrowerFnmCreditReportScoreType value)
        {
            switch (value)
            {
                case E_FhaVaBorrowerFnmCreditReportScoreType.CreditQuote:
                    writer.WriteAttributeString(attrName, "CreditQuote");
                    break;
                case E_FhaVaBorrowerFnmCreditReportScoreType.FICO:
                    writer.WriteAttributeString(attrName, "FICO");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_FhaVaBorrowerCertificationSalesPriceExceedsAppraisedValueType value)
        {
            switch (value)
            {
                case E_FhaVaBorrowerCertificationSalesPriceExceedsAppraisedValueType.A:
                    writer.WriteAttributeString(attrName, "A");
                    break;
                case E_FhaVaBorrowerCertificationSalesPriceExceedsAppraisedValueType.B:
                    writer.WriteAttributeString(attrName, "B");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_GovernmentMonitoringHmdaEthnicityType value)
        {
            switch (value)
            {
                case E_GovernmentMonitoringHmdaEthnicityType.HispanicOrLatino:
                    writer.WriteAttributeString(attrName, "HispanicOrLatino");
                    break;
                case E_GovernmentMonitoringHmdaEthnicityType.NotHispanicOrLatino:
                    writer.WriteAttributeString(attrName, "NotHispanicOrLatino");
                    break;
                case E_GovernmentMonitoringHmdaEthnicityType.InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication:
                    writer.WriteAttributeString(attrName, "InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication");
                    break;
                case E_GovernmentMonitoringHmdaEthnicityType.NotApplicable:
                    writer.WriteAttributeString(attrName, "NotApplicable");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_GovernmentMonitoringGenderType value)
        {
            switch (value)
            {
                case E_GovernmentMonitoringGenderType.Female:
                    writer.WriteAttributeString(attrName, "Female");
                    break;
                case E_GovernmentMonitoringGenderType.InformationNotProvidedUnknown:
                    writer.WriteAttributeString(attrName, "InformationNotProvidedUnknown");
                    break;
                case E_GovernmentMonitoringGenderType.Male:
                    writer.WriteAttributeString(attrName, "Male");
                    break;
                case E_GovernmentMonitoringGenderType.NotApplicable:
                    writer.WriteAttributeString(attrName, "NotApplicable");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_GovernmentMonitoringRaceNationalOriginType value)
        {
            switch (value)
            {
                case E_GovernmentMonitoringRaceNationalOriginType.AmericanIndianOrAlaskanNative:
                    writer.WriteAttributeString(attrName, "AmericanIndianOrAlaskanNative");
                    break;
                case E_GovernmentMonitoringRaceNationalOriginType.AsianOrPacificIslander:
                    writer.WriteAttributeString(attrName, "AsianOrPacificIslander");
                    break;
                case E_GovernmentMonitoringRaceNationalOriginType.BlackNotOfHispanicOrigin:
                    writer.WriteAttributeString(attrName, "BlackNotOfHispanicOrigin");
                    break;
                case E_GovernmentMonitoringRaceNationalOriginType.Hispanic:
                    writer.WriteAttributeString(attrName, "Hispanic");
                    break;
                case E_GovernmentMonitoringRaceNationalOriginType.InformationNotProvided:
                    writer.WriteAttributeString(attrName, "InformationNotProvided");
                    break;
                case E_GovernmentMonitoringRaceNationalOriginType.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
                case E_GovernmentMonitoringRaceNationalOriginType.WhiteNotOfHispanicOrigin:
                    writer.WriteAttributeString(attrName, "WhiteNotOfHispanicOrigin");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_HmdaRaceType value)
        {
            switch (value)
            {
                case E_HmdaRaceType.AmericanIndianOrAlaskaNative:
                    writer.WriteAttributeString(attrName, "AmericanIndianOrAlaskaNative");
                    break;
                case E_HmdaRaceType.Asian:
                    writer.WriteAttributeString(attrName, "Asian");
                    break;
                case E_HmdaRaceType.BlackOrAfricanAmerican:
                    writer.WriteAttributeString(attrName, "BlackOrAfricanAmerican");
                    break;
                case E_HmdaRaceType.NativeHawaiianOrOtherPacificIslander:
                    writer.WriteAttributeString(attrName, "NativeHawaiianOrOtherPacificIslander");
                    break;
                case E_HmdaRaceType.White:
                    writer.WriteAttributeString(attrName, "White");
                    break;
                case E_HmdaRaceType.InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication:
                    writer.WriteAttributeString(attrName, "InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication");
                    break;
                case E_HmdaRaceType.NotApplicable:
                    writer.WriteAttributeString(attrName, "NotApplicable");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_PresentHousingExpenseHousingExpenseType value)
        {
            switch (value)
            {
                case E_PresentHousingExpenseHousingExpenseType.FirstMortgagePrincipalAndInterest:
                    writer.WriteAttributeString(attrName, "FirstMortgagePrincipalAndInterest");
                    break;
                case E_PresentHousingExpenseHousingExpenseType.HazardInsurance:
                    writer.WriteAttributeString(attrName, "HazardInsurance");
                    break;
                case E_PresentHousingExpenseHousingExpenseType.HomeownersAssociationDuesAndCondominiumFees:
                    writer.WriteAttributeString(attrName, "HomeownersAssociationDuesAndCondominiumFees");
                    break;
                case E_PresentHousingExpenseHousingExpenseType.MI:
                    writer.WriteAttributeString(attrName, "MI");
                    break;
                case E_PresentHousingExpenseHousingExpenseType.OtherHousingExpense:
                    writer.WriteAttributeString(attrName, "OtherHousingExpense");
                    break;
                case E_PresentHousingExpenseHousingExpenseType.OtherMortgageLoanPrincipalAndInterest:
                    writer.WriteAttributeString(attrName, "OtherMortgageLoanPrincipalAndInterest");
                    break;
                case E_PresentHousingExpenseHousingExpenseType.RealEstateTax:
                    writer.WriteAttributeString(attrName, "RealEstateTax");
                    break;
                case E_PresentHousingExpenseHousingExpenseType.Rent:
                    writer.WriteAttributeString(attrName, "Rent");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_SummaryAmountType value)
        {
            switch (value)
            {
                case E_SummaryAmountType.TotalMonthlyIncomeNotIncludingNetRentalIncome:
                    writer.WriteAttributeString(attrName, "TotalMonthlyIncomeNotIncludingNetRentalIncome");
                    break;
                case E_SummaryAmountType.SubtotalLiabilitiesPaidByClosingNotIncludingSubjectPropertyLiensBalance:
                    writer.WriteAttributeString(attrName, "SubtotalLiabilitiesPaidByClosingNotIncludingSubjectPropertyLiensBalance");
                    break;
                case E_SummaryAmountType.TotalPresentHousingExpense:
                    writer.WriteAttributeString(attrName, "TotalPresentHousingExpense");
                    break;
                case E_SummaryAmountType.TotalLiabilitesBalance:
                    writer.WriteAttributeString(attrName, "TotalLiabilitesBalance");
                    break;
                case E_SummaryAmountType.SubtotalLiabilitesMonthlyPayment:
                    writer.WriteAttributeString(attrName, "SubtotalLiabilitesMonthlyPayment");
                    break;
                case E_SummaryAmountType.SubtotalOmittedLiabilitesBalance:
                    writer.WriteAttributeString(attrName, "SubtotalOmittedLiabilitesBalance");
                    break;
                case E_SummaryAmountType.SubtotalOmittedLiabilitiesMonthlyPayment:
                    writer.WriteAttributeString(attrName, "SubtotalOmittedLiabilitiesMonthlyPayment");
                    break;
                case E_SummaryAmountType.SubtotalLiabilitiesPaidByClosingNotIncludingSubjectPropertyLiensMonthlyPayment:
                    writer.WriteAttributeString(attrName, "SubtotalLiabilitiesPaidByClosingNotIncludingSubjectPropertyLiensMonthlyPayment");
                    break;
                case E_SummaryAmountType.SubtotalResubordinatedLiabilitesMonthlyPaymentForSubjectProperty:
                    writer.WriteAttributeString(attrName, "SubtotalResubordinatedLiabilitesMonthlyPaymentForSubjectProperty");
                    break;
                case E_SummaryAmountType.SubtotalSubjectPropertyLiensPaidByClosingBalance:
                    writer.WriteAttributeString(attrName, "SubtotalSubjectPropertyLiensPaidByClosingBalance");
                    break;
                case E_SummaryAmountType.SubtotalSubjectPropertyLiensPaidByClosingMonthlyPayment:
                    writer.WriteAttributeString(attrName, "SubtotalSubjectPropertyLiensPaidByClosingMonthlyPayment");
                    break;
                case E_SummaryAmountType.SubtotalLiabilitiesForRentalPropertyBalance:
                    writer.WriteAttributeString(attrName, "SubtotalLiabilitiesForRentalPropertyBalance");
                    break;
                case E_SummaryAmountType.SubtotalLiabilitiesForRentalPropertyMonthlyPayment:
                    writer.WriteAttributeString(attrName, "SubtotalLiabilitiesForRentalPropertyMonthlyPayment");
                    break;
                case E_SummaryAmountType.SubtotalResubordinatedLiabilitiesBalanceForSubjectProperty:
                    writer.WriteAttributeString(attrName, "SubtotalResubordinatedLiabilitiesBalanceForSubjectProperty");
                    break;
                case E_SummaryAmountType.SubtotalLiquidAssetsNotIncludingGift:
                    writer.WriteAttributeString(attrName, "SubtotalLiquidAssetsNotIncludingGift");
                    break;
                case E_SummaryAmountType.SubtotalNonLiquidAssets:
                    writer.WriteAttributeString(attrName, "SubtotalNonLiquidAssets");
                    break;
                case E_SummaryAmountType.TotalLiabilitiesBalance:
                    writer.WriteAttributeString(attrName, "TotalLiabilitiesBalance");
                    break;
                case E_SummaryAmountType.SubtotalLiabilitiesMonthlyPayment:
                    writer.WriteAttributeString(attrName, "SubtotalLiabilitiesMonthlyPayment");
                    break;
                case E_SummaryAmountType.SubtotalOmittedLiabilitiesBalance:
                    writer.WriteAttributeString(attrName, "SubtotalOmittedLiabilitiesBalance");
                    break;
                case E_SummaryAmountType.SubtotalResubordinatedLiabilitiesMonthlyPaymentForSubjectProperty:
                    writer.WriteAttributeString(attrName, "SubtotalResubordinatedLiabilitiesMonthlyPaymentForSubjectProperty");
                    break;
                case E_SummaryAmountType.UndrawnHELOC:
                    writer.WriteAttributeString(attrName, "UndrawnHELOC");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_VaBorrowerCertificationOccupancyType value)
        {
            switch (value)
            {
                case E_VaBorrowerCertificationOccupancyType.A:
                    writer.WriteAttributeString(attrName, "A");
                    break;
                case E_VaBorrowerCertificationOccupancyType.B:
                    writer.WriteAttributeString(attrName, "B");
                    break;
                case E_VaBorrowerCertificationOccupancyType.C:
                    writer.WriteAttributeString(attrName, "C");
                    break;
                case E_VaBorrowerCertificationOccupancyType.D:
                    writer.WriteAttributeString(attrName, "D");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_ClosingAgentType value)
        {
            switch (value)
            {
                case E_ClosingAgentType.Attorney:
                    writer.WriteAttributeString(attrName, "Attorney");
                    break;
                case E_ClosingAgentType.ClosingAgent:
                    writer.WriteAttributeString(attrName, "ClosingAgent");
                    break;
                case E_ClosingAgentType.EscrowCompany:
                    writer.WriteAttributeString(attrName, "EscrowCompany");
                    break;
                case E_ClosingAgentType.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
                case E_ClosingAgentType.SettlementAgent:
                    writer.WriteAttributeString(attrName, "SettlementAgent");
                    break;
                case E_ClosingAgentType.TitleCompany:
                    writer.WriteAttributeString(attrName, "TitleCompany");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CompensationPaidByType value)
        {
            switch (value)
            {
                case E_CompensationPaidByType.Lender:
                    writer.WriteAttributeString(attrName, "Lender");
                    break;
                case E_CompensationPaidByType.Investor:
                    writer.WriteAttributeString(attrName, "Investor");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CompensationPaidToType value)
        {
            switch (value)
            {
                case E_CompensationPaidToType.Broker:
                    writer.WriteAttributeString(attrName, "Broker");
                    break;
                case E_CompensationPaidToType.Lender:
                    writer.WriteAttributeString(attrName, "Lender");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CompensationType value)
        {
            switch (value)
            {
                case E_CompensationType.ServiceReleasePremium:
                    writer.WriteAttributeString(attrName, "ServiceReleasePremium");
                    break;
                case E_CompensationType.YieldSpreadDifferential:
                    writer.WriteAttributeString(attrName, "YieldSpreadDifferential");
                    break;
                case E_CompensationType.BrokerCompensation:
                    writer.WriteAttributeString(attrName, "BrokerCompensation");
                    break;
                case E_CompensationType.LenderCompensation:
                    writer.WriteAttributeString(attrName, "LenderCompensation");
                    break;
                case E_CompensationType.Rebate:
                    writer.WriteAttributeString(attrName, "Rebate");
                    break;
                case E_CompensationType.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_EscrowAccountActivityPaymentDescriptionType value)
        {
            switch (value)
            {
                case E_EscrowAccountActivityPaymentDescriptionType.Assessment:
                    writer.WriteAttributeString(attrName, "Assessment");
                    break;
                case E_EscrowAccountActivityPaymentDescriptionType.CityPropertyTax:
                    writer.WriteAttributeString(attrName, "CityPropertyTax");
                    break;
                case E_EscrowAccountActivityPaymentDescriptionType.CountyPropertyTax:
                    writer.WriteAttributeString(attrName, "CountyPropertyTax");
                    break;
                case E_EscrowAccountActivityPaymentDescriptionType.EarthquakeInsurance:
                    writer.WriteAttributeString(attrName, "EarthquakeInsurance");
                    break;
                case E_EscrowAccountActivityPaymentDescriptionType.FloodInsurance:
                    writer.WriteAttributeString(attrName, "FloodInsurance");
                    break;
                case E_EscrowAccountActivityPaymentDescriptionType.HazardInsurance:
                    writer.WriteAttributeString(attrName, "HazardInsurance");
                    break;
                case E_EscrowAccountActivityPaymentDescriptionType.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
                case E_EscrowAccountActivityPaymentDescriptionType.SchoolPropertyTax:
                    writer.WriteAttributeString(attrName, "SchoolPropertyTax");
                    break;
                case E_EscrowAccountActivityPaymentDescriptionType.TownPropertyTax:
                    writer.WriteAttributeString(attrName, "TownPropertyTax");
                    break;
                case E_EscrowAccountActivityPaymentDescriptionType.VillagePropertyTax:
                    writer.WriteAttributeString(attrName, "VillagePropertyTax");
                    break;
                case E_EscrowAccountActivityPaymentDescriptionType.Windstorm:
                    writer.WriteAttributeString(attrName, "Windstorm");
                    break;
                case E_EscrowAccountActivityPaymentDescriptionType.MortgageInsurance:
                    writer.WriteAttributeString(attrName, "MortgageInsurance");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_LoanDetailsDocumentOrderClassificationType value)
        {
            switch (value)
            {
                case E_LoanDetailsDocumentOrderClassificationType.Preliminary:
                    writer.WriteAttributeString(attrName, "Preliminary");
                    break;
                case E_LoanDetailsDocumentOrderClassificationType.Final:
                    writer.WriteAttributeString(attrName, "Final");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_InterimInterestPerDiemCalculationMethodType value)
        {
            switch (value)
            {
                case E_InterimInterestPerDiemCalculationMethodType._360:
                    writer.WriteAttributeString(attrName, "360");
                    break;
                case E_InterimInterestPerDiemCalculationMethodType._365:
                    writer.WriteAttributeString(attrName, "365");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_InterimInterestPerDiemPaymentOptionType value)
        {
            switch (value)
            {
                case E_InterimInterestPerDiemPaymentOptionType.Closing:
                    writer.WriteAttributeString(attrName, "Closing");
                    break;
                case E_InterimInterestPerDiemPaymentOptionType.FirstPayment:
                    writer.WriteAttributeString(attrName, "FirstPayment");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_LossPayeeType value)
        {
            switch (value)
            {
                case E_LossPayeeType.Investor:
                    writer.WriteAttributeString(attrName, "Investor");
                    break;
                case E_LossPayeeType.Lender:
                    writer.WriteAttributeString(attrName, "Lender");
                    break;
                case E_LossPayeeType.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_RecordableDocumentType value)
        {
            switch (value)
            {
                case E_RecordableDocumentType.All:
                    writer.WriteAttributeString(attrName, "All");
                    break;
                case E_RecordableDocumentType.AssignmentOfMortgage:
                    writer.WriteAttributeString(attrName, "AssignmentOfMortgage");
                    break;
                case E_RecordableDocumentType.DeedOfTrust:
                    writer.WriteAttributeString(attrName, "DeedOfTrust");
                    break;
                case E_RecordableDocumentType.Mortgage:
                    writer.WriteAttributeString(attrName, "Mortgage");
                    break;
                case E_RecordableDocumentType.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
                case E_RecordableDocumentType.QuitClaimDeed:
                    writer.WriteAttributeString(attrName, "QuitClaimDeed");
                    break;
                case E_RecordableDocumentType.ReleaseOfLien:
                    writer.WriteAttributeString(attrName, "ReleaseOfLien");
                    break;
                case E_RecordableDocumentType.SecurityInstrument:
                    writer.WriteAttributeString(attrName, "SecurityInstrument");
                    break;
                case E_RecordableDocumentType.SignatureAffidavit:
                    writer.WriteAttributeString(attrName, "SignatureAffidavit");
                    break;
                case E_RecordableDocumentType.WarrantyDeed:
                    writer.WriteAttributeString(attrName, "WarrantyDeed");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_SignerIdentificationType value)
        {
            switch (value)
            {
                case E_SignerIdentificationType.PersonallyKnown:
                    writer.WriteAttributeString(attrName, "PersonallyKnown");
                    break;
                case E_SignerIdentificationType.ProvidedIdentification:
                    writer.WriteAttributeString(attrName, "ProvidedIdentification");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_PreparedByElectronicRoutingMethodType value)
        {
            switch (value)
            {
                case E_PreparedByElectronicRoutingMethodType.EMAIL:
                    writer.WriteAttributeString(attrName, "EMAIL");
                    break;
                case E_PreparedByElectronicRoutingMethodType.FTP:
                    writer.WriteAttributeString(attrName, "FTP");
                    break;
                case E_PreparedByElectronicRoutingMethodType.HTTP:
                    writer.WriteAttributeString(attrName, "HTTP");
                    break;
                case E_PreparedByElectronicRoutingMethodType.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
                case E_PreparedByElectronicRoutingMethodType.URI:
                    writer.WriteAttributeString(attrName, "URI");
                    break;
                case E_PreparedByElectronicRoutingMethodType.URL:
                    writer.WriteAttributeString(attrName, "URL");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_AssociatedDocumentType value)
        {
            switch (value)
            {
                case E_AssociatedDocumentType.All:
                    writer.WriteAttributeString(attrName, "All");
                    break;
                case E_AssociatedDocumentType.AssignmentOfMortgage:
                    writer.WriteAttributeString(attrName, "AssignmentOfMortgage");
                    break;
                case E_AssociatedDocumentType.DeedOfTrust:
                    writer.WriteAttributeString(attrName, "DeedOfTrust");
                    break;
                case E_AssociatedDocumentType.Mortgage:
                    writer.WriteAttributeString(attrName, "Mortgage");
                    break;
                case E_AssociatedDocumentType.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
                case E_AssociatedDocumentType.QuitClaimDeed:
                    writer.WriteAttributeString(attrName, "QuitClaimDeed");
                    break;
                case E_AssociatedDocumentType.ReleaseOfLien:
                    writer.WriteAttributeString(attrName, "ReleaseOfLien");
                    break;
                case E_AssociatedDocumentType.SecurityInstrument:
                    writer.WriteAttributeString(attrName, "SecurityInstrument");
                    break;
                case E_AssociatedDocumentType.SignatureAffidavit:
                    writer.WriteAttributeString(attrName, "SignatureAffidavit");
                    break;
                case E_AssociatedDocumentType.WarrantyDeed:
                    writer.WriteAttributeString(attrName, "WarrantyDeed");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_AssociatedDocumentBookType value)
        {
            switch (value)
            {
                case E_AssociatedDocumentBookType.Deed:
                    writer.WriteAttributeString(attrName, "Deed");
                    break;
                case E_AssociatedDocumentBookType.Maps:
                    writer.WriteAttributeString(attrName, "Maps");
                    break;
                case E_AssociatedDocumentBookType.Mortgage:
                    writer.WriteAttributeString(attrName, "Mortgage");
                    break;
                case E_AssociatedDocumentBookType.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
                case E_AssociatedDocumentBookType.Plat:
                    writer.WriteAttributeString(attrName, "Plat");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_ReturnToElectronicRoutingMethodType value)
        {
            switch (value)
            {
                case E_ReturnToElectronicRoutingMethodType.EMAIL:
                    writer.WriteAttributeString(attrName, "EMAIL");
                    break;
                case E_ReturnToElectronicRoutingMethodType.FTP:
                    writer.WriteAttributeString(attrName, "FTP");
                    break;
                case E_ReturnToElectronicRoutingMethodType.HTTP:
                    writer.WriteAttributeString(attrName, "HTTP");
                    break;
                case E_ReturnToElectronicRoutingMethodType.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
                case E_ReturnToElectronicRoutingMethodType.URI:
                    writer.WriteAttributeString(attrName, "URI");
                    break;
                case E_ReturnToElectronicRoutingMethodType.URL:
                    writer.WriteAttributeString(attrName, "URL");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_TrusteeType value)
        {
            switch (value)
            {
                case E_TrusteeType.Primary:
                    writer.WriteAttributeString(attrName, "Primary");
                    break;
                case E_TrusteeType.Secondary:
                    writer.WriteAttributeString(attrName, "Secondary");
                    break;
                case E_TrusteeType.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_RespaHudDetailLineItemPaidByType value)
        {
            switch (value)
            {
                case E_RespaHudDetailLineItemPaidByType.Buyer:
                    writer.WriteAttributeString(attrName, "Buyer");
                    break;
                case E_RespaHudDetailLineItemPaidByType.LenderPremium:
                    writer.WriteAttributeString(attrName, "LenderPremium");
                    break;
                case E_RespaHudDetailLineItemPaidByType.Seller:
                    writer.WriteAttributeString(attrName, "Seller");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_TotalFeesPaidToType value)
        {
            switch (value)
            {
                case E_TotalFeesPaidToType.Broker:
                    writer.WriteAttributeString(attrName, "Broker");
                    break;
                case E_TotalFeesPaidToType.Lender:
                    writer.WriteAttributeString(attrName, "Lender");
                    break;
                case E_TotalFeesPaidToType.Investor:
                    writer.WriteAttributeString(attrName, "Investor");
                    break;
                case E_TotalFeesPaidToType.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_TotalFeesPaidByType value)
        {
            switch (value)
            {
                case E_TotalFeesPaidByType.Broker:
                    writer.WriteAttributeString(attrName, "Broker");
                    break;
                case E_TotalFeesPaidByType.Buyer:
                    writer.WriteAttributeString(attrName, "Buyer");
                    break;
                case E_TotalFeesPaidByType.Investor:
                    writer.WriteAttributeString(attrName, "Investor");
                    break;
                case E_TotalFeesPaidByType.Lender:
                    writer.WriteAttributeString(attrName, "Lender");
                    break;
                case E_TotalFeesPaidByType.Seller:
                    writer.WriteAttributeString(attrName, "Seller");
                    break;
                case E_TotalFeesPaidByType.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_ServicerType value)
        {
            switch (value)
            {
                case E_ServicerType.Lender:
                    writer.WriteAttributeString(attrName, "Lender");
                    break;
                case E_ServicerType.Investor:
                    writer.WriteAttributeString(attrName, "Investor");
                    break;
                case E_ServicerType.Present:
                    writer.WriteAttributeString(attrName, "Present");
                    break;
                case E_ServicerType.New:
                    writer.WriteAttributeString(attrName, "New");
                    break;
                case E_ServicerType.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
            }
        }

        #endregion

        #region Addition Read/Write Enum
        protected void ReadAttribute(XmlReader reader, string attrName, out E_DocumentsPackageType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Closing")
            {
                result = E_DocumentsPackageType.Closing;
            }
            else if (s == "Initial Disclosure")
            {
                result = E_DocumentsPackageType.InitialDisclosure;
            }
            else if (s == "Wire")
            {
                result = E_DocumentsPackageType.Wire;
            }
            else if (s == "Shipping")
            {
                result = E_DocumentsPackageType.Shipping;
            }
            else if (s == "Quality Assurance")
            {
                result = E_DocumentsPackageType.QualityAssurance;
            }
            else if (s == "Processing")
            {
                result = E_DocumentsPackageType.Processing;
            }
            else if (s == "Secondary")
            {
                result = E_DocumentsPackageType.Secondary;
            }
            else if (s == "Assignment")
            {
                result = E_DocumentsPackageType.Assignment;
            }
            else if (s == "Secure Delivery")
            {
                result = E_DocumentsPackageType.SecureDelivery;
            }
            else if (s == "Adverse Action")
            {
                result = E_DocumentsPackageType.AdverseAction;
            }
            else
            {
                result = E_DocumentsPackageType.Undefined;
            }
        }

        protected void WriteAttribute(XmlWriter writer, string attrName, E_DocumentsPackageType value)
        {
            switch (value)
            {
                case E_DocumentsPackageType.Closing:
                    writer.WriteAttributeString(attrName, "Closing");
                    break;
                case E_DocumentsPackageType.InitialDisclosure:
                    writer.WriteAttributeString(attrName, "Initial Disclosure");
                    break;
                case E_DocumentsPackageType.Wire:
                    writer.WriteAttributeString(attrName, "Wire");
                    break;
                case E_DocumentsPackageType.Shipping:
                    writer.WriteAttributeString(attrName, "Shipping");
                    break;
                case E_DocumentsPackageType.QualityAssurance:
                    writer.WriteAttributeString(attrName, "Quality Assurance");
                    break;
                case E_DocumentsPackageType.Processing:
                    writer.WriteAttributeString(attrName, "Processing");
                    break;
                case E_DocumentsPackageType.Secondary:
                    writer.WriteAttributeString(attrName, "Secondary");
                    break;
                case E_DocumentsPackageType.Assignment:
                    writer.WriteAttributeString(attrName, "Assignment");
                    break;
                case E_DocumentsPackageType.SecureDelivery:
                    writer.WriteAttributeString(attrName, "Secure Delivery");
                    break;
                case E_DocumentsPackageType.AdverseAction:
                    writer.WriteAttributeString(attrName, "Adverse Action");
                    break;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_EmbeddedFileType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "PDF")
            {
                result = E_EmbeddedFileType.Pdf;
            }
            else if (s == "XML")
            {
                result = E_EmbeddedFileType.Xml;
            }
            else if (s == "xHTML")
            {
                result = E_EmbeddedFileType.XHtml;
            }
            else
            {
                result = E_EmbeddedFileType.Undefined;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_EmbeddedFileType value)
        {
            switch (value)
            {
                case E_EmbeddedFileType.Pdf:
                    writer.WriteAttributeString(attrName, "PDF");
                    break;
                case E_EmbeddedFileType.Xml:
                    writer.WriteAttributeString(attrName, "XML");
                    break;
                case E_EmbeddedFileType.XHtml:
                    writer.WriteAttributeString(attrName, "xHTML");
                    break;
            }
        }

        protected void ReadAttribute(XmlReader reader, string attrName, out E_DeliveryFormatBundlingOptions result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "merged")
            {
                result = E_DeliveryFormatBundlingOptions.Merged;
            }
            else if (s == "individual")
            {
                result = E_DeliveryFormatBundlingOptions.Individual;
            }
            else
            {
                result = E_DeliveryFormatBundlingOptions.Undefined;
            }

        }

        protected void WriteAttribute(XmlWriter writer, string attrName, E_DeliveryFormatBundlingOptions value)
        {
            switch (value)
            {
                case E_DeliveryFormatBundlingOptions.Merged:
                    writer.WriteAttributeString(attrName, "merged");
                    break;
                case E_DeliveryFormatBundlingOptions.Individual:
                    writer.WriteAttributeString(attrName, "individual");
                    break;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_DeliveryFormatCompressionType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "zip")
            {
                result = E_DeliveryFormatCompressionType.Zip;
            }
            else
            {
                result = E_DeliveryFormatCompressionType.Undefined;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_DeliveryFormatCompressionType value)
        {
            switch (value)
            {
                case E_DeliveryFormatCompressionType.Zip:
                    writer.WriteAttributeString(attrName, "zip");
                    break;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_DocumentFormatType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "PDF")
            {
                result = E_DocumentFormatType.Pdf;
            }
            else if (s == "xHTML")
            {
                result = E_DocumentFormatType.xHtml;
            }
            else if (s == "SmartDoc")
            {
                result = E_DocumentFormatType.SmartDoc;
            }
            else
            {
                result = E_DocumentFormatType.Undefined;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_DocumentFormatType value)
        {
            switch (value)
            {
                case E_DocumentFormatType.Pdf:
                    writer.WriteAttributeString(attrName, "PDF");
                    break;
                case E_DocumentFormatType.xHtml:
                    writer.WriteAttributeString(attrName, "xHTML");
                    break;
                case E_DocumentFormatType.SmartDoc:
                    writer.WriteAttributeString(attrName, "SmartDoc");
                    break;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_EmbeddedFileEncodingType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Base64")
            {
                result = E_EmbeddedFileEncodingType.Base64;
            }
            else
            {
                result = E_EmbeddedFileEncodingType.Undefined;
            }
        }

        protected void ReadAttribute(XmlReader reader, string attrName, out E_EmbeddedFileMimeType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "image/jpeg")
            {
                result = E_EmbeddedFileMimeType.Image_Jpeg;
            }
            else if (s == "application/pdf")
            {
                result = E_EmbeddedFileMimeType.Application_Pdf;
            }
            else if (s == "text/plain")
            {
                result = E_EmbeddedFileMimeType.Text_Plain;
            }
            else if (s == "text/xml")
            {
                result = E_EmbeddedFileMimeType.Text_Xml;
            }
            else if (s == "application/zip")
            {
                result = E_EmbeddedFileMimeType.Application_Zip;
            }
            else if (s == "text/html")
            {
                result = E_EmbeddedFileMimeType.Text_Html;
            }
            else
            {
                result = E_EmbeddedFileMimeType.Undefined;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_EmbeddedFileEncodingType value)
        {
            switch (value)
            {
                case E_EmbeddedFileEncodingType.Base64:
                    writer.WriteAttributeString(attrName, "Base64");
                    break;
            }
        }

        protected void WriteAttribute(XmlWriter writer, string attrName, E_EmbeddedFileMimeType value)
        {
            switch (value)
            {
                case E_EmbeddedFileMimeType.Image_Jpeg:
                    writer.WriteAttributeString(attrName, "image/jpeg");
                    break;
                case E_EmbeddedFileMimeType.Application_Pdf:
                    writer.WriteAttributeString(attrName, "application/pdf");
                    break;
                case E_EmbeddedFileMimeType.Text_Plain:
                    writer.WriteAttributeString(attrName, "text/plain");
                    break;
                case E_EmbeddedFileMimeType.Text_Xml:
                    writer.WriteAttributeString(attrName, "text/xml");
                    break;
                case E_EmbeddedFileMimeType.Application_Zip:
                    writer.WriteAttributeString(attrName, "application/zip");
                    break;
                case E_EmbeddedFileMimeType.Text_Html:
                    writer.WriteAttributeString(attrName, "text/html");
                    break;
                default:
                    break;
            }
        }

        protected void ReadAttribute(XmlReader reader, string attrName, out E_TransmittalDataDtcAusType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Other")
            {
                result = E_TransmittalDataDtcAusType.Other;
            }
            else if (s == "DU")
            {
                result = E_TransmittalDataDtcAusType.DU;
            }
            else if (s == "LP")
            {
                result = E_TransmittalDataDtcAusType.LP;
            }
            else
            {
                result = E_TransmittalDataDtcAusType.Undefined;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_TransmittalDataDtcAusType value)
        {
            switch (value)
            {
                case E_TransmittalDataDtcAusType.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
                case E_TransmittalDataDtcAusType.DU:
                    writer.WriteAttributeString(attrName, "DU");
                    break;
                case E_TransmittalDataDtcAusType.LP:
                    writer.WriteAttributeString(attrName, "LP");
                    break;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_TransmittalDataDtcMortgageOriginatorType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Seller")
            {
                result = E_TransmittalDataDtcMortgageOriginatorType.Seller;
            }
            else if (s == "Broker")
            {
                result = E_TransmittalDataDtcMortgageOriginatorType.Broker;
            }
            else if (s == "Correspondent")
            {
                result = E_TransmittalDataDtcMortgageOriginatorType.Correspondent;
            }
            else
            {
                result = E_TransmittalDataDtcMortgageOriginatorType.Undefined;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_TransmittalDataDtcMortgageOriginatorType value)
        {
            switch (value)
            {
                case E_TransmittalDataDtcMortgageOriginatorType.Seller:
                    writer.WriteAttributeString(attrName, "Seller");
                    break;
                case E_TransmittalDataDtcMortgageOriginatorType.Broker:
                    writer.WriteAttributeString(attrName, "Broker");
                    break;
                case E_TransmittalDataDtcMortgageOriginatorType.Correspondent:
                    writer.WriteAttributeString(attrName, "Correspondent");
                    break;
            }
        }

        protected void ReadAttribute(XmlReader reader, string attrName, out E_TransmittalDataDtcRiskAssessment result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "ManualUnderwriting")
            {
                result = E_TransmittalDataDtcRiskAssessment.ManualUnderwriting;
            }
            else if (s == "AUS")
            {
                result = E_TransmittalDataDtcRiskAssessment.AUS;
            }
            else
            {
                result = E_TransmittalDataDtcRiskAssessment.Undefined;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_TransmittalDataDtcRiskAssessment value)
        {
            switch (value)
            {
                case E_TransmittalDataDtcRiskAssessment.ManualUnderwriting:
                    writer.WriteAttributeString(attrName, "ManualUnderwriting");
                    break;
                case E_TransmittalDataDtcRiskAssessment.AUS:
                    writer.WriteAttributeString(attrName, "AUS");
                    break;
            }
        }
        #endregion


        #region More Enum
        protected void ReadAttribute(XmlReader reader, string attrName, out E_GfeDetailGfeCreditOrChargeForChosenInterestRateType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "BorrowerCharge")
            {
                result = E_GfeDetailGfeCreditOrChargeForChosenInterestRateType.BorrowerCharge;
            }
            else if (s == "BorrowerCredit")
            {
                result = E_GfeDetailGfeCreditOrChargeForChosenInterestRateType.BorrowerCredit;
            }
            else if (s == "CreditOrChargeIncludedInOriginationCharge")
            {
                result = E_GfeDetailGfeCreditOrChargeForChosenInterestRateType.CreditOrChargeIncludedInOriginationCharge;
            }
            else if (s == "Other")
            {
                result = E_GfeDetailGfeCreditOrChargeForChosenInterestRateType.Other;
            }
            else
            {
                result = E_GfeDetailGfeCreditOrChargeForChosenInterestRateType.Undefined;
            }
        }

        protected void ReadAttribute(XmlReader reader, string attrName, out E_CreditBankruptcyInPastOrPresentIndicator result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Yes")
            {
                result = E_CreditBankruptcyInPastOrPresentIndicator.Yes;
            }
            else if (s == "No")
            {
                result = E_CreditBankruptcyInPastOrPresentIndicator.No;
            }
            else
            {
                result = E_CreditBankruptcyInPastOrPresentIndicator.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxEscrowItemItemType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "HazardInsurance")
            {
                result = E_CxEscrowItemItemType.HazardInsurance;
            }
            else if (s == "FloodInsurance")
            {
                result = E_CxEscrowItemItemType.FloodInsurance;
            }
            else if (s == "WindstormInsurance")
            {
                result = E_CxEscrowItemItemType.WindstormInsurance;
            }
            else if (s == "CountyPropertyTax")
            {
                result = E_CxEscrowItemItemType.CountyPropertyTax;
            }
            else if (s == "CityPropertyTax")
            {
                result = E_CxEscrowItemItemType.CityPropertyTax;
            }
            else if (s == "SchoolPropertyTax")
            {
                result = E_CxEscrowItemItemType.SchoolPropertyTax;
            }
            else if (s == "Other")
            {
                result = E_CxEscrowItemItemType.Other;
            }
            else if (s == "AggregateAdjustment")
            {
                result = E_CxEscrowItemItemType.AggregateAdjustment;
            }
            else
            {
                result = E_CxEscrowItemItemType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxEscrowItemPeriodicPaymentType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Annual")
            {
                result = E_CxEscrowItemPeriodicPaymentType.Annual;
            }
            else if (s == "SemiAnnual")
            {
                result = E_CxEscrowItemPeriodicPaymentType.SemiAnnual;
            }
            else if (s == "Quarterly")
            {
                result = E_CxEscrowItemPeriodicPaymentType.Quarterly;
            }
            else
            {
                result = E_CxEscrowItemPeriodicPaymentType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxEscrowItemNonBuyerExpensePaidBy result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Seller")
            {
                result = E_CxEscrowItemNonBuyerExpensePaidBy.Seller;
            }
            else if (s == "Lender")
            {
                result = E_CxEscrowItemNonBuyerExpensePaidBy.Lender;
            }
            else if (s == "Broker")
            {
                result = E_CxEscrowItemNonBuyerExpensePaidBy.Broker;
            }
            else
            {
                result = E_CxEscrowItemNonBuyerExpensePaidBy.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxVaContractPriceExceedsCrv result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "WasNotAware")
            {
                result = E_CxVaContractPriceExceedsCrv.WasNotAware;
            }
            else if (s == "WasAware")
            {
                result = E_CxVaContractPriceExceedsCrv.WasAware;
            }
            else
            {
                result = E_CxVaContractPriceExceedsCrv.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxVaInsuranceType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Guaranty")
            {
                result = E_CxVaInsuranceType.Guaranty;
            }
            else if (s == "Insurance")
            {
                result = E_CxVaInsuranceType.Insurance;
            }
            else
            {
                result = E_CxVaInsuranceType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxVaProcedureType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Automatic")
            {
                result = E_CxVaProcedureType.Automatic;
            }
            else if (s == "PriorApproval")
            {
                result = E_CxVaProcedureType.PriorApproval;
            }
            else
            {
                result = E_CxVaProcedureType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxVaVestingType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Other")
            {
                result = E_CxVaVestingType.Other;
            }
            else if (s == "VeteranAndSpouse")
            {
                result = E_CxVaVestingType.VeteranAndSpouse;
            }
            else if (s == "Veteran")
            {
                result = E_CxVaVestingType.Veteran;
            }
            else
            {
                result = E_CxVaVestingType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxVaLoanProceedsWithholdingDepositedInAccountType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "EarmarkedAccount")
            {
                result = E_CxVaLoanProceedsWithholdingDepositedInAccountType.EarmarkedAccount;
            }
            else if (s == "Escrow")
            {
                result = E_CxVaLoanProceedsWithholdingDepositedInAccountType.Escrow;
            }
            else
            {
                result = E_CxVaLoanProceedsWithholdingDepositedInAccountType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxVaLienType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Other")
            {
                result = E_CxVaLienType.Other;
            }
            else if (s == "FirstRealtyMortgage")
            {
                result = E_CxVaLienType.FirstRealtyMortgage;
            }
            else if (s == "SecondRealtyMortgage")
            {
                result = E_CxVaLienType.SecondRealtyMortgage;
            }
            else if (s == "FirstChatteMortgage")
            {
                result = E_CxVaLienType.FirstChatteMortgage;
            }
            else if (s == "Unsecured")
            {
                result = E_CxVaLienType.Unsecured;
            }
            else
            {
                result = E_CxVaLienType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxVaVetEthnicity result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "HispanicOrLatino")
            {
                result = E_CxVaVetEthnicity.HispanicOrLatino;
            }
            else if (s == "NotHispanicOrLatino")
            {
                result = E_CxVaVetEthnicity.NotHispanicOrLatino;
            }
            else if (s == "NotSpecified")
            {
                result = E_CxVaVetEthnicity.NotSpecified;
            }
            else
            {
                result = E_CxVaVetEthnicity.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxVaCoborrowerEthnicity result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "HispanicOrLatino")
            {
                result = E_CxVaCoborrowerEthnicity.HispanicOrLatino;
            }
            else if (s == "NotHispanicOrLatino")
            {
                result = E_CxVaCoborrowerEthnicity.NotHispanicOrLatino;
            }
            else if (s == "NotSpecified")
            {
                result = E_CxVaCoborrowerEthnicity.NotSpecified;
            }
            else
            {
                result = E_CxVaCoborrowerEthnicity.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_BorrowerCxCreditScoreCreditRepositorySourceType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Equifax")
            {
                result = E_BorrowerCxCreditScoreCreditRepositorySourceType.Equifax;
            }
            else if (s == "TransUnion")
            {
                result = E_BorrowerCxCreditScoreCreditRepositorySourceType.TransUnion;
            }
            else if (s == "Experian")
            {
                result = E_BorrowerCxCreditScoreCreditRepositorySourceType.Experian;
            }
            else if (s == "Other")
            {
                result = E_BorrowerCxCreditScoreCreditRepositorySourceType.Other;
            }
            else
            {
                result = E_BorrowerCxCreditScoreCreditRepositorySourceType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxLoanTimelyPaymentRewards result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "N")
            {
                result = E_CxLoanTimelyPaymentRewards.N;
            }
            else if (s == "Y")
            {
                result = E_CxLoanTimelyPaymentRewards.Y;
            }
            else if (s == "Y_Level1")
            {
                result = E_CxLoanTimelyPaymentRewards.Y_Level1;
            }
            else if (s == "Y_Level2")
            {
                result = E_CxLoanTimelyPaymentRewards.Y_Level2;
            }
            else if (s == "Y_Level3")
            {
                result = E_CxLoanTimelyPaymentRewards.Y_Level3;
            }
            else
            {
                result = E_CxLoanTimelyPaymentRewards.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxLoanAmountFinancedItemizationIndicator result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "N")
            {
                result = E_CxLoanAmountFinancedItemizationIndicator.N;
            }
            else if (s == "Y")
            {
                result = E_CxLoanAmountFinancedItemizationIndicator.Y;
            }
            else
            {
                result = E_CxLoanAmountFinancedItemizationIndicator.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxLoanDocumentsToUse result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Custom")
            {
                result = E_CxLoanDocumentsToUse.Custom;
            }
            else if (s == "FannieMae")
            {
                result = E_CxLoanDocumentsToUse.FannieMae;
            }
            else if (s == "FreddieMac")
            {
                result = E_CxLoanDocumentsToUse.FreddieMac;
            }
            else
            {
                result = E_CxLoanDocumentsToUse.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxLoanCemaIndicator result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "N")
            {
                result = E_CxLoanCemaIndicator.N;
            }
            else if (s == "Y")
            {
                result = E_CxLoanCemaIndicator.Y;
            }
            else
            {
                result = E_CxLoanCemaIndicator.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxLoanCalculate901UsingMarginPlusIndexIndicator result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "N")
            {
                result = E_CxLoanCalculate901UsingMarginPlusIndexIndicator.N;
            }
            else if (s == "Y")
            {
                result = E_CxLoanCalculate901UsingMarginPlusIndexIndicator.Y;
            }
            else
            {
                result = E_CxLoanCalculate901UsingMarginPlusIndexIndicator.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxLoanBiweeklyConversionIndictor result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "N")
            {
                result = E_CxLoanBiweeklyConversionIndictor.N;
            }
            else if (s == "Y")
            {
                result = E_CxLoanBiweeklyConversionIndictor.Y;
            }
            else
            {
                result = E_CxLoanBiweeklyConversionIndictor.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxLoanLotIndicator result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "N")
            {
                result = E_CxLoanLotIndicator.N;
            }
            else if (s == "Y")
            {
                result = E_CxLoanLotIndicator.Y;
            }
            else
            {
                result = E_CxLoanLotIndicator.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxLoanCreditLifeRequiredIndicator result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "N")
            {
                result = E_CxLoanCreditLifeRequiredIndicator.N;
            }
            else if (s == "Y")
            {
                result = E_CxLoanCreditLifeRequiredIndicator.Y;
            }
            else
            {
                result = E_CxLoanCreditLifeRequiredIndicator.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxLoanInvestorArmDisclosureAvailabilityIndicator result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "N")
            {
                result = E_CxLoanInvestorArmDisclosureAvailabilityIndicator.N;
            }
            else if (s == "Y")
            {
                result = E_CxLoanInvestorArmDisclosureAvailabilityIndicator.Y;
            }
            else
            {
                result = E_CxLoanInvestorArmDisclosureAvailabilityIndicator.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxLoanLastPaymentChangeIsFullyAmortizedIndicator result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "N")
            {
                result = E_CxLoanLastPaymentChangeIsFullyAmortizedIndicator.N;
            }
            else if (s == "Y")
            {
                result = E_CxLoanLastPaymentChangeIsFullyAmortizedIndicator.Y;
            }
            else
            {
                result = E_CxLoanLastPaymentChangeIsFullyAmortizedIndicator.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxLoanAdvantage90Indicator result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "N")
            {
                result = E_CxLoanAdvantage90Indicator.N;
            }
            else if (s == "Y")
            {
                result = E_CxLoanAdvantage90Indicator.Y;
            }
            else
            {
                result = E_CxLoanAdvantage90Indicator.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxLoanFactaIndicator result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "N")
            {
                result = E_CxLoanFactaIndicator.N;
            }
            else if (s == "Y")
            {
                result = E_CxLoanFactaIndicator.Y;
            }
            else
            {
                result = E_CxLoanFactaIndicator.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxLoanAdjustableRatePeriodicCap result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "No")
            {
                result = E_CxLoanAdjustableRatePeriodicCap.No;
            }
            else if (s == "Yes")
            {
                result = E_CxLoanAdjustableRatePeriodicCap.Yes;
            }
            else
            {
                result = E_CxLoanAdjustableRatePeriodicCap.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxLoanArmIndex result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "CODI")
            {
                result = E_CxLoanArmIndex.CODI;
            }
            else if (s == "DTC_FiveYearRateSwap")
            {
                result = E_CxLoanArmIndex.DTC_FiveYearRateSwap;
            }
            else if (s == "DTC_FiveYearTreasury")
            {
                result = E_CxLoanArmIndex.DTC_FiveYearTreasury;
            }
            else if (s == "DTC_OneMonthWSJLIBOR")
            {
                result = E_CxLoanArmIndex.DTC_OneMonthWSJLIBOR;
            }
            else if (s == "DTC_OneYearMTA")
            {
                result = E_CxLoanArmIndex.DTC_OneYearMTA;
            }
            else if (s == "DTC_OneYearWSJLIBOR")
            {
                result = E_CxLoanArmIndex.DTC_OneYearWSJLIBOR;
            }
            else if (s == "DTC_PrimePlus")
            {
                result = E_CxLoanArmIndex.DTC_PrimePlus;
            }
            else if (s == "DTC_SixMonthCD")
            {
                result = E_CxLoanArmIndex.DTC_SixMonthCD;
            }
            else if (s == "DTC_SixMonthWSJLIBOR")
            {
                result = E_CxLoanArmIndex.DTC_SixMonthWSJLIBOR;
            }
            else if (s == "DTC_SixMonthWeeklyAuctionTreasury")
            {
                result = E_CxLoanArmIndex.DTC_SixMonthWeeklyAuctionTreasury;
            }
            else if (s == "DTC_SixMonthsFNMALIBOR")
            {
                result = E_CxLoanArmIndex.DTC_SixMonthsFNMALIBOR;
            }
            else if (s == "DTC_TenYearTreasury")
            {
                result = E_CxLoanArmIndex.DTC_TenYearTreasury;
            }
            else if (s == "DTC_ThreeMonthPrimeRate")
            {
                result = E_CxLoanArmIndex.DTC_ThreeMonthPrimeRate;
            }
            else if (s == "DTC_ThreeMonthWSJLIBOR")
            {
                result = E_CxLoanArmIndex.DTC_ThreeMonthWSJLIBOR;
            }
            else if (s == "DTC_TwoYearTreasury")
            {
                result = E_CxLoanArmIndex.DTC_TwoYearTreasury;
            }
            else if (s == "EleventhDistrictCostOfFunds")
            {
                result = E_CxLoanArmIndex.EleventhDistrictCostOfFunds;
            }
            else if (s == "FNM60DayRequiredNetYield")
            {
                result = E_CxLoanArmIndex.FNM60DayRequiredNetYield;
            }
            else if (s == "NationalAverageContractRateFHLBB")
            {
                result = E_CxLoanArmIndex.NationalAverageContractRateFHLBB;
            }
            else if (s == "OneYearTreasury")
            {
                result = E_CxLoanArmIndex.OneYearTreasury;
            }
            else if (s == "Other")
            {
                result = E_CxLoanArmIndex.Other;
            }
            else if (s == "ThreeYearTreasury")
            {
                result = E_CxLoanArmIndex.ThreeYearTreasury;
            }
            else if (s == "WeeklyAveragePrimeRate")
            {
                result = E_CxLoanArmIndex.WeeklyAveragePrimeRate;
            }
            else
            {
                result = E_CxLoanArmIndex.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxLoanHelocProgram result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "AmericanSterlingHeloc")
            {
                result = E_CxLoanHelocProgram.AmericanSterlingHeloc;
            }
            else if (s == "BarringtonHeloc")
            {
                result = E_CxLoanHelocProgram.BarringtonHeloc;
            }
            else if (s == "Cal State 9 HELOC")
            {
                result = E_CxLoanHelocProgram._Cal_State_9_HELOC;
            }
            else if (s == "CalBayHeloc")
            {
                result = E_CxLoanHelocProgram.CalBayHeloc;
            }
            else if (s == "ChaseHeloc")
            {
                result = E_CxLoanHelocProgram.ChaseHeloc;
            }
            else if (s == "CountrywideFlexSaverHeloc")
            {
                result = E_CxLoanHelocProgram.CountrywideFlexSaverHeloc;
            }
            else if (s == "CountrywideHeloc")
            {
                result = E_CxLoanHelocProgram.CountrywideHeloc;
            }
            else if (s == "CountrywidePreferredHeloc")
            {
                result = E_CxLoanHelocProgram.CountrywidePreferredHeloc;
            }
            else if (s == "CountrywideSubprimeHeloc")
            {
                result = E_CxLoanHelocProgram.CountrywideSubprimeHeloc;
            }
            else if (s == "CreditSuisseHeloc")
            {
                result = E_CxLoanHelocProgram.CreditSuisseHeloc;
            }
            else if (s == "CunaMutualMortgageHeloc")
            {
                result = E_CxLoanHelocProgram.CunaMutualMortgageHeloc;
            }
            else if (s == "ETradeBankHeloc")
            {
                result = E_CxLoanHelocProgram.ETradeBankHeloc;
            }
            else if (s == "FirstAdvantageMortgageHeloc")
            {
                result = E_CxLoanHelocProgram.FirstAdvantageMortgageHeloc;
            }
            else if (s == "FirstMac C-X+ First Lien")
            {
                result = E_CxLoanHelocProgram._FirstMac_C_X__First_Lien;
            }
            else if (s == "FirstMac C-X+ Second Lien")
            {
                result = E_CxLoanHelocProgram._FirstMac_C_X__Second_Lien;
            }
            else if (s == "FirstMacXHeloc")
            {
                result = E_CxLoanHelocProgram.FirstMacXHeloc;
            }
            else if (s == "FirstMacXPlusHeloc")
            {
                result = E_CxLoanHelocProgram.FirstMacXPlusHeloc;
            }
            else if (s == "GMACHeloc")
            {
                result = E_CxLoanHelocProgram.GMACHeloc;
            }
            else if (s == "GreenPointHeloc")
            {
                result = E_CxLoanHelocProgram.GreenPointHeloc;
            }
            else if (s == "HorizonCreditUnionHeloc")
            {
                result = E_CxLoanHelocProgram.HorizonCreditUnionHeloc;
            }
            else if (s == "ImpacHeloc")
            {
                result = E_CxLoanHelocProgram.ImpacHeloc;
            }
            else if (s == "IndymacHeloc")
            {
                result = E_CxLoanHelocProgram.IndymacHeloc;
            }
            else if (s == "KaipermFederalCreditUnionHeloc")
            {
                result = E_CxLoanHelocProgram.KaipermFederalCreditUnionHeloc;
            }
            else if (s == "LimeHeloc")
            {
                result = E_CxLoanHelocProgram.LimeHeloc;
            }
            else if (s == "MITLending_AND_MortgageITHeloc")
            {
                result = E_CxLoanHelocProgram.MITLending_AND_MortgageITHeloc;
            }
            else if (s == "RFCHeloc")
            {
                result = E_CxLoanHelocProgram.RFCHeloc;
            }
            else if (s == "SovereignHeloc")
            {
                result = E_CxLoanHelocProgram.SovereignHeloc;
            }
            else if (s == "TranslandHeloc")
            {
                result = E_CxLoanHelocProgram.TranslandHeloc;
            }
            else if (s == "VenturaFederalCountyCreditUnionHeloc")
            {
                result = E_CxLoanHelocProgram.VenturaFederalCountyCreditUnionHeloc;
            }
            else if (s == "WellsFargoHeloc")
            {
                result = E_CxLoanHelocProgram.WellsFargoHeloc;
            }
            else if (s == "WinterGroupHeloc")
            {
                result = E_CxLoanHelocProgram.WinterGroupHeloc;
            }
            else
            {
                result = E_CxLoanHelocProgram.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxLoanLifeFloorSetting result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "InitialRate")
            {
                result = E_CxLoanLifeFloorSetting.InitialRate;
            }
            else if (s == "Margin")
            {
                result = E_CxLoanLifeFloorSetting.Margin;
            }
            else
            {
                result = E_CxLoanLifeFloorSetting.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxLoanRehabilitationOrRenovationIndicator result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "N")
            {
                result = E_CxLoanRehabilitationOrRenovationIndicator.N;
            }
            else if (s == "Y")
            {
                result = E_CxLoanRehabilitationOrRenovationIndicator.Y;
            }
            else
            {
                result = E_CxLoanRehabilitationOrRenovationIndicator.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxLoanLoanTableFundedInInvestorsNameIndicator result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "N")
            {
                result = E_CxLoanLoanTableFundedInInvestorsNameIndicator.N;
            }
            else if (s == "Y")
            {
                result = E_CxLoanLoanTableFundedInInvestorsNameIndicator.Y;
            }
            else
            {
                result = E_CxLoanLoanTableFundedInInvestorsNameIndicator.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxLoanIncludeOddDaysTimeIndicator result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "N")
            {
                result = E_CxLoanIncludeOddDaysTimeIndicator.N;
            }
            else if (s == "Y")
            {
                result = E_CxLoanIncludeOddDaysTimeIndicator.Y;
            }
            else
            {
                result = E_CxLoanIncludeOddDaysTimeIndicator.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxLoanPrepaymentMonthsOrPercentIndicator result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Months")
            {
                result = E_CxLoanPrepaymentMonthsOrPercentIndicator.Months;
            }
            else if (s == "Percent")
            {
                result = E_CxLoanPrepaymentMonthsOrPercentIndicator.Percent;
            }
            else
            {
                result = E_CxLoanPrepaymentMonthsOrPercentIndicator.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxBuydownBuydownOptionType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Yes")
            {
                result = E_CxBuydownBuydownOptionType.Yes;
            }
            else if (s == "No")
            {
                result = E_CxBuydownBuydownOptionType.No;
            }
            else
            {
                result = E_CxBuydownBuydownOptionType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_MortgageBrokerDtcType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Lender")
            {
                result = E_MortgageBrokerDtcType.Lender;
            }
            else if (s == "Broker")
            {
                result = E_MortgageBrokerDtcType.Broker;
            }
            else
            {
                result = E_MortgageBrokerDtcType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxArmDisclosureArmDisclosurePrintingPreferenceForClosingPackageType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "LenderAlways")
            {
                result = E_CxArmDisclosureArmDisclosurePrintingPreferenceForClosingPackageType.LenderAlways;
            }
            else if (s == "InvestorIfExists_ElseLender")
            {
                result = E_CxArmDisclosureArmDisclosurePrintingPreferenceForClosingPackageType.InvestorIfExists_ElseLender;
            }
            else if (s == "InvestorOrNone")
            {
                result = E_CxArmDisclosureArmDisclosurePrintingPreferenceForClosingPackageType.InvestorOrNone;
            }
            else
            {
                result = E_CxArmDisclosureArmDisclosurePrintingPreferenceForClosingPackageType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxArmDisclosureArmDisclosurePrintingPreferenceForInitialPackageType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "LenderAlways")
            {
                result = E_CxArmDisclosureArmDisclosurePrintingPreferenceForInitialPackageType.LenderAlways;
            }
            else if (s == "InvestorIfExists_ElseLender")
            {
                result = E_CxArmDisclosureArmDisclosurePrintingPreferenceForInitialPackageType.InvestorIfExists_ElseLender;
            }
            else if (s == "InvestorOrNone")
            {
                result = E_CxArmDisclosureArmDisclosurePrintingPreferenceForInitialPackageType.InvestorOrNone;
            }
            else
            {
                result = E_CxArmDisclosureArmDisclosurePrintingPreferenceForInitialPackageType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_InitialDisclosuresPreviousLoanPurposeType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "CashOutRefinance")
            {
                result = E_InitialDisclosuresPreviousLoanPurposeType.CashOutRefinance;
            }
            else if (s == "Construction")
            {
                result = E_InitialDisclosuresPreviousLoanPurposeType.Construction;
            }
            else if (s == "ConstructionToPerm")
            {
                result = E_InitialDisclosuresPreviousLoanPurposeType.ConstructionToPerm;
            }
            else if (s == "HELOC")
            {
                result = E_InitialDisclosuresPreviousLoanPurposeType.HELOC;
            }
            else if (s == "NoCashOutRefinance")
            {
                result = E_InitialDisclosuresPreviousLoanPurposeType.NoCashOutRefinance;
            }
            else if (s == "Purchase")
            {
                result = E_InitialDisclosuresPreviousLoanPurposeType.Purchase;
            }
            else
            {
                result = E_InitialDisclosuresPreviousLoanPurposeType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_InitialDisclosuresOpenOrClosedEndSelection result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Closed")
            {
                result = E_InitialDisclosuresOpenOrClosedEndSelection.Closed;
            }
            else if (s == "Open")
            {
                result = E_InitialDisclosuresOpenOrClosedEndSelection.Open;
            }
            else
            {
                result = E_InitialDisclosuresOpenOrClosedEndSelection.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_InitialDisclosuresPreviousLoanPrePayPenaltyIndicator result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "N")
            {
                result = E_InitialDisclosuresPreviousLoanPrePayPenaltyIndicator.N;
            }
            else if (s == "Y")
            {
                result = E_InitialDisclosuresPreviousLoanPrePayPenaltyIndicator.Y;
            }
            else
            {
                result = E_InitialDisclosuresPreviousLoanPrePayPenaltyIndicator.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_InitialDisclosuresIndexPublishPeriod result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Annually")
            {
                result = E_InitialDisclosuresIndexPublishPeriod.Annually;
            }
            else if (s == "Monthly")
            {
                result = E_InitialDisclosuresIndexPublishPeriod.Monthly;
            }
            else if (s == "Other")
            {
                result = E_InitialDisclosuresIndexPublishPeriod.Other;
            }
            else if (s == "Weekly")
            {
                result = E_InitialDisclosuresIndexPublishPeriod.Weekly;
            }
            else
            {
                result = E_InitialDisclosuresIndexPublishPeriod.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_InitialDisclosuresLockInFeeRefundableIndicator result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "N")
            {
                result = E_InitialDisclosuresLockInFeeRefundableIndicator.N;
            }
            else if (s == "Y")
            {
                result = E_InitialDisclosuresLockInFeeRefundableIndicator.Y;
            }
            else
            {
                result = E_InitialDisclosuresLockInFeeRefundableIndicator.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_InitialDisclosuresIndexEstablishedBy result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "FNMA")
            {
                result = E_InitialDisclosuresIndexEstablishedBy.FNMA;
            }
            else if (s == "FederalReserve")
            {
                result = E_InitialDisclosuresIndexEstablishedBy.FederalReserve;
            }
            else if (s == "Other")
            {
                result = E_InitialDisclosuresIndexEstablishedBy.Other;
            }
            else if (s == "WallStreetJournal")
            {
                result = E_InitialDisclosuresIndexEstablishedBy.WallStreetJournal;
            }
            else
            {
                result = E_InitialDisclosuresIndexEstablishedBy.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_InitialDisclosuresBalloonConditionalRighttoRefinanceIndicator result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "N")
            {
                result = E_InitialDisclosuresBalloonConditionalRighttoRefinanceIndicator.N;
            }
            else if (s == "Y")
            {
                result = E_InitialDisclosuresBalloonConditionalRighttoRefinanceIndicator.Y;
            }
            else
            {
                result = E_InitialDisclosuresBalloonConditionalRighttoRefinanceIndicator.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_InitialDisclosuresFeeCreditLifeDisabilityPaidIndicator result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "N")
            {
                result = E_InitialDisclosuresFeeCreditLifeDisabilityPaidIndicator.N;
            }
            else if (s == "Y")
            {
                result = E_InitialDisclosuresFeeCreditLifeDisabilityPaidIndicator.Y;
            }
            else
            {
                result = E_InitialDisclosuresFeeCreditLifeDisabilityPaidIndicator.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_InitialDisclosuresDisclosureStatementIndicator result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "N")
            {
                result = E_InitialDisclosuresDisclosureStatementIndicator.N;
            }
            else if (s == "Y")
            {
                result = E_InitialDisclosuresDisclosureStatementIndicator.Y;
            }
            else
            {
                result = E_InitialDisclosuresDisclosureStatementIndicator.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_InitialDisclosuresUffiIndicator result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "N")
            {
                result = E_InitialDisclosuresUffiIndicator.N;
            }
            else if (s == "Y")
            {
                result = E_InitialDisclosuresUffiIndicator.Y;
            }
            else
            {
                result = E_InitialDisclosuresUffiIndicator.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_InitialDisclosuresLenderWillSetRateAt result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Application")
            {
                result = E_InitialDisclosuresLenderWillSetRateAt.Application;
            }
            else if (s == "Closing")
            {
                result = E_InitialDisclosuresLenderWillSetRateAt.Closing;
            }
            else if (s == "Commitment")
            {
                result = E_InitialDisclosuresLenderWillSetRateAt.Commitment;
            }
            else if (s == "Other")
            {
                result = E_InitialDisclosuresLenderWillSetRateAt.Other;
            }
            else
            {
                result = E_InitialDisclosuresLenderWillSetRateAt.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_InitialDisclosuresLenderWillSetRateIndicator result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "N")
            {
                result = E_InitialDisclosuresLenderWillSetRateIndicator.N;
            }
            else if (s == "Y")
            {
                result = E_InitialDisclosuresLenderWillSetRateIndicator.Y;
            }
            else
            {
                result = E_InitialDisclosuresLenderWillSetRateIndicator.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_InitialDisclosuresRateCanChangeIndicator result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "N")
            {
                result = E_InitialDisclosuresRateCanChangeIndicator.N;
            }
            else if (s == "Y")
            {
                result = E_InitialDisclosuresRateCanChangeIndicator.Y;
            }
            else
            {
                result = E_InitialDisclosuresRateCanChangeIndicator.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxHelocDrawPeriodType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Months")
            {
                result = E_CxHelocDrawPeriodType.Months;
            }
            else if (s == "Years")
            {
                result = E_CxHelocDrawPeriodType.Years;
            }
            else
            {
                result = E_CxHelocDrawPeriodType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxHelocInitialPercentRateType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "DiscountedFixed")
            {
                result = E_CxHelocInitialPercentRateType.DiscountedFixed;
            }
            else if (s == "DiscountedVariable")
            {
                result = E_CxHelocInitialPercentRateType.DiscountedVariable;
            }
            else if (s == "NotDiscounted")
            {
                result = E_CxHelocInitialPercentRateType.NotDiscounted;
            }
            else
            {
                result = E_CxHelocInitialPercentRateType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxHelocInvestorDocuments result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Countrywide")
            {
                result = E_CxHelocInvestorDocuments.Countrywide;
            }
            else if (s == "RFC")
            {
                result = E_CxHelocInvestorDocuments.RFC;
            }
            else if (s == "GMAC")
            {
                result = E_CxHelocInvestorDocuments.GMAC;
            }
            else if (s == "GreenPoint")
            {
                result = E_CxHelocInvestorDocuments.GreenPoint;
            }
            else if (s == "Wells Fargo")
            {
                result = E_CxHelocInvestorDocuments._Wells_Fargo;
            }
            else if (s == "Macquarie")
            {
                result = E_CxHelocInvestorDocuments.Macquarie;
            }
            else if (s == "IndyMac")
            {
                result = E_CxHelocInvestorDocuments.IndyMac;
            }
            else if (s == "Sovereign")
            {
                result = E_CxHelocInvestorDocuments.Sovereign;
            }
            else if (s == "Impac")
            {
                result = E_CxHelocInvestorDocuments.Impac;
            }
            else if (s == "Credit Suisse")
            {
                result = E_CxHelocInvestorDocuments._Credit_Suisse;
            }
            else if (s == "MIT Lending & MortgageIT")
            {
                result = E_CxHelocInvestorDocuments._MIT_Lending___MortgageIT;
            }
            else if (s == "Ventura Federal County Credit Union")
            {
                result = E_CxHelocInvestorDocuments._Ventura_Federal_County_Credit_Union;
            }
            else if (s == "Cal Bay")
            {
                result = E_CxHelocInvestorDocuments._Cal_Bay;
            }
            else if (s == "FirstMac")
            {
                result = E_CxHelocInvestorDocuments.FirstMac;
            }
            else if (s == "1st Advantage Mortgage")
            {
                result = E_CxHelocInvestorDocuments._1st_Advantage_Mortgage;
            }
            else if (s == "The Winter Group")
            {
                result = E_CxHelocInvestorDocuments._The_Winter_Group;
            }
            else if (s == "Cuna Mutual Mortgage")
            {
                result = E_CxHelocInvestorDocuments._Cuna_Mutual_Mortgage;
            }
            else if (s == "Transland")
            {
                result = E_CxHelocInvestorDocuments.Transland;
            }
            else if (s == "Chase")
            {
                result = E_CxHelocInvestorDocuments.Chase;
            }
            else if (s == "American Sterling")
            {
                result = E_CxHelocInvestorDocuments._American_Sterling;
            }
            else if (s == "Horizon Credit Union")
            {
                result = E_CxHelocInvestorDocuments._Horizon_Credit_Union;
            }
            else if (s == "Barrington Bank & Trust")
            {
                result = E_CxHelocInvestorDocuments._Barrington_Bank___Trust;
            }
            else if (s == "Kaiperm Federal Credit Union")
            {
                result = E_CxHelocInvestorDocuments._Kaiperm_Federal_Credit_Union;
            }
            else if (s == "ETrade Bank")
            {
                result = E_CxHelocInvestorDocuments._ETrade_Bank;
            }
            else if (s == "Cal State 9-INV")
            {
                result = E_CxHelocInvestorDocuments._Cal_State_9_INV;
            }
            else
            {
                result = E_CxHelocInvestorDocuments.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxHelocPaymentAmountCalculationType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "GreaterOfAmountOrPercent")
            {
                result = E_CxHelocPaymentAmountCalculationType.GreaterOfAmountOrPercent;
            }
            else if (s == "FixedAmount")
            {
                result = E_CxHelocPaymentAmountCalculationType.FixedAmount;
            }
            else if (s == "FinanceChargePlusCreditInsurance")
            {
                result = E_CxHelocPaymentAmountCalculationType.FinanceChargePlusCreditInsurance;
            }
            else
            {
                result = E_CxHelocPaymentAmountCalculationType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxHelocPaymentMethodForLineOfCredit result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "BillMe")
            {
                result = E_CxHelocPaymentMethodForLineOfCredit.BillMe;
            }
            else if (s == "AutoChargeAccount")
            {
                result = E_CxHelocPaymentMethodForLineOfCredit.AutoChargeAccount;
            }
            else if (s == "AutoChargeAnotherAccount")
            {
                result = E_CxHelocPaymentMethodForLineOfCredit.AutoChargeAnotherAccount;
            }
            else
            {
                result = E_CxHelocPaymentMethodForLineOfCredit.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_RespaFeeGfeAggregationType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "ChosenInterestRateCreditOrCharge")
            {
                result = E_RespaFeeGfeAggregationType.ChosenInterestRateCreditOrCharge;
            }
            else if (s == "CombinedOurOriginationAndInterestRateCreditOrCharge")
            {
                result = E_RespaFeeGfeAggregationType.CombinedOurOriginationAndInterestRateCreditOrCharge;
            }
            else if (s == "GovernmentRecordingCharges")
            {
                result = E_RespaFeeGfeAggregationType.GovernmentRecordingCharges;
            }
            else if (s == "None")
            {
                result = E_RespaFeeGfeAggregationType.None;
            }
            else if (s == "OurOriginationCharge")
            {
                result = E_RespaFeeGfeAggregationType.OurOriginationCharge;
            }
            else if (s == "OwnersTitleInsurance")
            {
                result = E_RespaFeeGfeAggregationType.OwnersTitleInsurance;
            }
            else if (s == "RequiredServicesLenderSelected")
            {
                result = E_RespaFeeGfeAggregationType.RequiredServicesLenderSelected;
            }
            else if (s == "RequiredServicesYouCanShopFor")
            {
                result = E_RespaFeeGfeAggregationType.RequiredServicesYouCanShopFor;
            }
            else if (s == "TitleServices")
            {
                result = E_RespaFeeGfeAggregationType.TitleServices;
            }
            else if (s == "TransferTaxes")
            {
                result = E_RespaFeeGfeAggregationType.TransferTaxes;
            }
            else
            {
                result = E_RespaFeeGfeAggregationType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxHelocRepaymentPeriodType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Months")
            {
                result = E_CxHelocRepaymentPeriodType.Months;
            }
            else if (s == "Years")
            {
                result = E_CxHelocRepaymentPeriodType.Years;
            }
            else
            {
                result = E_CxHelocRepaymentPeriodType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxHelocFeeType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Finance Charge")
            {
                result = E_CxHelocFeeType._Finance_Charge;
            }
            else if (s == "Other")
            {
                result = E_CxHelocFeeType.Other;
            }
            else if (s == "Other Charge")
            {
                result = E_CxHelocFeeType._Other_Charge;
            }
            else
            {
                result = E_CxHelocFeeType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxImportantTermsName result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Administration")
            {
                result = E_CxImportantTermsName.Administration;
            }
            else if (s == "Annual Maintenance")
            {
                result = E_CxImportantTermsName._Annual_Maintenance;
            }
            else if (s == "Application")
            {
                result = E_CxImportantTermsName.Application;
            }
            else if (s == "Attorney Max")
            {
                result = E_CxImportantTermsName._Attorney_Max;
            }
            else if (s == "Attorney Min")
            {
                result = E_CxImportantTermsName._Attorney_Min;
            }
            else if (s == "Broker")
            {
                result = E_CxImportantTermsName.Broker;
            }
            else if (s == "Document Preparation")
            {
                result = E_CxImportantTermsName._Document_Preparation;
            }
            else if (s == "Expedited Delivery")
            {
                result = E_CxImportantTermsName._Expedited_Delivery;
            }
            else if (s == "Loan Tie In")
            {
                result = E_CxImportantTermsName._Loan_Tie_In;
            }
            else if (s == "Low Balance")
            {
                result = E_CxImportantTermsName._Low_Balance;
            }
            else if (s == "Maximum Interest")
            {
                result = E_CxImportantTermsName._Maximum_Interest;
            }
            else if (s == "Origination")
            {
                result = E_CxImportantTermsName.Origination;
            }
            else if (s == "Other 1")
            {
                result = E_CxImportantTermsName._Other_1;
            }
            else if (s == "Other 2")
            {
                result = E_CxImportantTermsName._Other_2;
            }
            else if (s == "Other Maximum")
            {
                result = E_CxImportantTermsName._Other_Maximum;
            }
            else if (s == "Points")
            {
                result = E_CxImportantTermsName.Points;
            }
            else if (s == "Processing")
            {
                result = E_CxImportantTermsName.Processing;
            }
            else if (s == "Termination")
            {
                result = E_CxImportantTermsName.Termination;
            }
            else
            {
                result = E_CxImportantTermsName.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxRecordingType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Mortgage")
            {
                result = E_CxRecordingType.Mortgage;
            }
            else if (s == "Assignment")
            {
                result = E_CxRecordingType.Assignment;
            }
            else
            {
                result = E_CxRecordingType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxRecordingRecordingJurisdictionType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "County")
            {
                result = E_CxRecordingRecordingJurisdictionType.County;
            }
            else if (s == "Parish")
            {
                result = E_CxRecordingRecordingJurisdictionType.Parish;
            }
            else if (s == "Borough")
            {
                result = E_CxRecordingRecordingJurisdictionType.Borough;
            }
            else if (s == "JudicialDistrict")
            {
                result = E_CxRecordingRecordingJurisdictionType.JudicialDistrict;
            }
            else
            {
                result = E_CxRecordingRecordingJurisdictionType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxAgentType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Closer")
            {
                result = E_CxAgentType.Closer;
            }
            else if (s == "Officer")
            {
                result = E_CxAgentType.Officer;
            }
            else if (s == "Processor")
            {
                result = E_CxAgentType.Processor;
            }
            else
            {
                result = E_CxAgentType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxCreditScoreCreditRepositorySourceType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Equifax")
            {
                result = E_CxCreditScoreCreditRepositorySourceType.Equifax;
            }
            else if (s == "Experian")
            {
                result = E_CxCreditScoreCreditRepositorySourceType.Experian;
            }
            else if (s == "Other")
            {
                result = E_CxCreditScoreCreditRepositorySourceType.Other;
            }
            else if (s == "TransUnion")
            {
                result = E_CxCreditScoreCreditRepositorySourceType.TransUnion;
            }
            else
            {
                result = E_CxCreditScoreCreditRepositorySourceType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxDocTrigger_4506 result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "None")
            {
                result = E_CxDocTrigger_4506.None;
            }
            else if (s == "4506")
            {
                result = E_CxDocTrigger_4506._4506;
            }
            else if (s == "4506-T")
            {
                result = E_CxDocTrigger_4506._4506_T;
            }
            else if (s == "Both")
            {
                result = E_CxDocTrigger_4506.Both;
            }
            else
            {
                result = E_CxDocTrigger_4506.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxStateSpecificInfoSubjectToKansasUniformCommercialCreditCode result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "N")
            {
                result = E_CxStateSpecificInfoSubjectToKansasUniformCommercialCreditCode.N;
            }
            else if (s == "Y")
            {
                result = E_CxStateSpecificInfoSubjectToKansasUniformCommercialCreditCode.Y;
            }
            else
            {
                result = E_CxStateSpecificInfoSubjectToKansasUniformCommercialCreditCode.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxStateSpecificInfoDerivationClauseDocumentType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "Other")
            {
                result = E_CxStateSpecificInfoDerivationClauseDocumentType.Other;
            }
            else if (s == "QuitClaimDeed")
            {
                result = E_CxStateSpecificInfoDerivationClauseDocumentType.QuitClaimDeed;
            }
            else if (s == "WarrantyDeed")
            {
                result = E_CxStateSpecificInfoDerivationClauseDocumentType.WarrantyDeed;
            }
            else
            {
                result = E_CxStateSpecificInfoDerivationClauseDocumentType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxStateSpecificInfoNonPara27PurchaseMoneyIndicator result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "N")
            {
                result = E_CxStateSpecificInfoNonPara27PurchaseMoneyIndicator.N;
            }
            else if (s == "Y")
            {
                result = E_CxStateSpecificInfoNonPara27PurchaseMoneyIndicator.Y;
            }
            else
            {
                result = E_CxStateSpecificInfoNonPara27PurchaseMoneyIndicator.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxStateSpecificInfoDerivationRecordingInfoType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "N/A")
            {
                result = E_CxStateSpecificInfoDerivationRecordingInfoType._N_A;
            }
            else if (s == "UseBookPage")
            {
                result = E_CxStateSpecificInfoDerivationRecordingInfoType.UseBookPage;
            }
            else if (s == "UseInstrument")
            {
                result = E_CxStateSpecificInfoDerivationRecordingInfoType.UseInstrument;
            }
            else
            {
                result = E_CxStateSpecificInfoDerivationRecordingInfoType.Undefined;
            }
        }
        protected void ReadAttribute(XmlReader reader, string attrName, out E_CxStateSpecificInfoTexasHomeEquitySection50LoanIndicator result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "N")
            {
                result = E_CxStateSpecificInfoTexasHomeEquitySection50LoanIndicator.N;
            }
            else if (s == "Y")
            {
                result = E_CxStateSpecificInfoTexasHomeEquitySection50LoanIndicator.Y;
            }
            else
            {
                result = E_CxStateSpecificInfoTexasHomeEquitySection50LoanIndicator.Undefined;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_RespaFeeGfeAggregationType value)
        {
            switch (value)
            {
                case E_RespaFeeGfeAggregationType.ChosenInterestRateCreditOrCharge:
                    writer.WriteAttributeString(attrName, "ChosenInterestRateCreditOrCharge");
                    break;
                case E_RespaFeeGfeAggregationType.CombinedOurOriginationAndInterestRateCreditOrCharge:
                    writer.WriteAttributeString(attrName, "CombinedOurOriginationAndInterestRateCreditOrCharge");
                    break;
                case E_RespaFeeGfeAggregationType.GovernmentRecordingCharges:
                    writer.WriteAttributeString(attrName, "GovernmentRecordingCharges");
                    break;
                case E_RespaFeeGfeAggregationType.None:
                    writer.WriteAttributeString(attrName, "None");
                    break;
                case E_RespaFeeGfeAggregationType.OurOriginationCharge:
                    writer.WriteAttributeString(attrName, "OurOriginationCharge");
                    break;
                case E_RespaFeeGfeAggregationType.OwnersTitleInsurance:
                    writer.WriteAttributeString(attrName, "OwnersTitleInsurance");
                    break;
                case E_RespaFeeGfeAggregationType.RequiredServicesLenderSelected:
                    writer.WriteAttributeString(attrName, "RequiredServicesLenderSelected");
                    break;
                case E_RespaFeeGfeAggregationType.RequiredServicesYouCanShopFor:
                    writer.WriteAttributeString(attrName, "RequiredServicesYouCanShopFor");
                    break;
                case E_RespaFeeGfeAggregationType.TitleServices:
                    writer.WriteAttributeString(attrName, "TitleServices");
                    break;
                case E_RespaFeeGfeAggregationType.TransferTaxes:
                    writer.WriteAttributeString(attrName, "TransferTaxes");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_GfeDetailGfeCreditOrChargeForChosenInterestRateType value)
        {
            switch (value)
            {
                case E_GfeDetailGfeCreditOrChargeForChosenInterestRateType.BorrowerCharge:
                    writer.WriteAttributeString(attrName, "BorrowerCharge");
                    break;
                case E_GfeDetailGfeCreditOrChargeForChosenInterestRateType.BorrowerCredit:
                    writer.WriteAttributeString(attrName, "BorrowerCredit");
                    break;
                case E_GfeDetailGfeCreditOrChargeForChosenInterestRateType.CreditOrChargeIncludedInOriginationCharge:
                    writer.WriteAttributeString(attrName, "CreditOrChargeIncludedInOriginationCharge");
                    break;
                case E_GfeDetailGfeCreditOrChargeForChosenInterestRateType.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CreditBankruptcyInPastOrPresentIndicator value)
        {
            switch (value)
            {
                case E_CreditBankruptcyInPastOrPresentIndicator.Yes:
                    writer.WriteAttributeString(attrName, "Yes");
                    break;
                case E_CreditBankruptcyInPastOrPresentIndicator.No:
                    writer.WriteAttributeString(attrName, "No");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxEscrowItemItemType value)
        {
            switch (value)
            {
                case E_CxEscrowItemItemType.HazardInsurance:
                    writer.WriteAttributeString(attrName, "HazardInsurance");
                    break;
                case E_CxEscrowItemItemType.FloodInsurance:
                    writer.WriteAttributeString(attrName, "FloodInsurance");
                    break;
                case E_CxEscrowItemItemType.WindstormInsurance:
                    writer.WriteAttributeString(attrName, "WindstormInsurance");
                    break;
                case E_CxEscrowItemItemType.CountyPropertyTax:
                    writer.WriteAttributeString(attrName, "CountyPropertyTax");
                    break;
                case E_CxEscrowItemItemType.CityPropertyTax:
                    writer.WriteAttributeString(attrName, "CityPropertyTax");
                    break;
                case E_CxEscrowItemItemType.SchoolPropertyTax:
                    writer.WriteAttributeString(attrName, "SchoolPropertyTax");
                    break;
                case E_CxEscrowItemItemType.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
                case E_CxEscrowItemItemType.AggregateAdjustment:
                    writer.WriteAttributeString(attrName, "AggregateAdjustment");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxEscrowItemPeriodicPaymentType value)
        {
            switch (value)
            {
                case E_CxEscrowItemPeriodicPaymentType.Annual:
                    writer.WriteAttributeString(attrName, "Annual");
                    break;
                case E_CxEscrowItemPeriodicPaymentType.SemiAnnual:
                    writer.WriteAttributeString(attrName, "SemiAnnual");
                    break;
                case E_CxEscrowItemPeriodicPaymentType.Quarterly:
                    writer.WriteAttributeString(attrName, "Quarterly");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxEscrowItemNonBuyerExpensePaidBy value)
        {
            switch (value)
            {
                case E_CxEscrowItemNonBuyerExpensePaidBy.Seller:
                    writer.WriteAttributeString(attrName, "Seller");
                    break;
                case E_CxEscrowItemNonBuyerExpensePaidBy.Lender:
                    writer.WriteAttributeString(attrName, "Lender");
                    break;
                case E_CxEscrowItemNonBuyerExpensePaidBy.Broker:
                    writer.WriteAttributeString(attrName, "Broker");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxVaContractPriceExceedsCrv value)
        {
            switch (value)
            {
                case E_CxVaContractPriceExceedsCrv.WasNotAware:
                    writer.WriteAttributeString(attrName, "WasNotAware");
                    break;
                case E_CxVaContractPriceExceedsCrv.WasAware:
                    writer.WriteAttributeString(attrName, "WasAware");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxVaInsuranceType value)
        {
            switch (value)
            {
                case E_CxVaInsuranceType.Guaranty:
                    writer.WriteAttributeString(attrName, "Guaranty");
                    break;
                case E_CxVaInsuranceType.Insurance:
                    writer.WriteAttributeString(attrName, "Insurance");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxVaProcedureType value)
        {
            switch (value)
            {
                case E_CxVaProcedureType.Automatic:
                    writer.WriteAttributeString(attrName, "Automatic");
                    break;
                case E_CxVaProcedureType.PriorApproval:
                    writer.WriteAttributeString(attrName, "PriorApproval");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxVaVestingType value)
        {
            switch (value)
            {
                case E_CxVaVestingType.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
                case E_CxVaVestingType.VeteranAndSpouse:
                    writer.WriteAttributeString(attrName, "VeteranAndSpouse");
                    break;
                case E_CxVaVestingType.Veteran:
                    writer.WriteAttributeString(attrName, "Veteran");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxVaLoanProceedsWithholdingDepositedInAccountType value)
        {
            switch (value)
            {
                case E_CxVaLoanProceedsWithholdingDepositedInAccountType.EarmarkedAccount:
                    writer.WriteAttributeString(attrName, "EarmarkedAccount");
                    break;
                case E_CxVaLoanProceedsWithholdingDepositedInAccountType.Escrow:
                    writer.WriteAttributeString(attrName, "Escrow");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxVaLienType value)
        {
            switch (value)
            {
                case E_CxVaLienType.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
                case E_CxVaLienType.FirstRealtyMortgage:
                    writer.WriteAttributeString(attrName, "FirstRealtyMortgage");
                    break;
                case E_CxVaLienType.SecondRealtyMortgage:
                    writer.WriteAttributeString(attrName, "SecondRealtyMortgage");
                    break;
                case E_CxVaLienType.FirstChatteMortgage:
                    writer.WriteAttributeString(attrName, "FirstChatteMortgage");
                    break;
                case E_CxVaLienType.Unsecured:
                    writer.WriteAttributeString(attrName, "Unsecured");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxVaVetEthnicity value)
        {
            switch (value)
            {
                case E_CxVaVetEthnicity.HispanicOrLatino:
                    writer.WriteAttributeString(attrName, "HispanicOrLatino");
                    break;
                case E_CxVaVetEthnicity.NotHispanicOrLatino:
                    writer.WriteAttributeString(attrName, "NotHispanicOrLatino");
                    break;
                case E_CxVaVetEthnicity.NotSpecified:
                    writer.WriteAttributeString(attrName, "NotSpecified");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxVaCoborrowerEthnicity value)
        {
            switch (value)
            {
                case E_CxVaCoborrowerEthnicity.HispanicOrLatino:
                    writer.WriteAttributeString(attrName, "HispanicOrLatino");
                    break;
                case E_CxVaCoborrowerEthnicity.NotHispanicOrLatino:
                    writer.WriteAttributeString(attrName, "NotHispanicOrLatino");
                    break;
                case E_CxVaCoborrowerEthnicity.NotSpecified:
                    writer.WriteAttributeString(attrName, "NotSpecified");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_BorrowerCxCreditScoreCreditRepositorySourceType value)
        {
            switch (value)
            {
                case E_BorrowerCxCreditScoreCreditRepositorySourceType.Equifax:
                    writer.WriteAttributeString(attrName, "Equifax");
                    break;
                case E_BorrowerCxCreditScoreCreditRepositorySourceType.TransUnion:
                    writer.WriteAttributeString(attrName, "TransUnion");
                    break;
                case E_BorrowerCxCreditScoreCreditRepositorySourceType.Experian:
                    writer.WriteAttributeString(attrName, "Experian");
                    break;
                case E_BorrowerCxCreditScoreCreditRepositorySourceType.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxLoanTimelyPaymentRewards value)
        {
            switch (value)
            {
                case E_CxLoanTimelyPaymentRewards.N:
                    writer.WriteAttributeString(attrName, "N");
                    break;
                case E_CxLoanTimelyPaymentRewards.Y:
                    writer.WriteAttributeString(attrName, "Y");
                    break;
                case E_CxLoanTimelyPaymentRewards.Y_Level1:
                    writer.WriteAttributeString(attrName, "Y_Level1");
                    break;
                case E_CxLoanTimelyPaymentRewards.Y_Level2:
                    writer.WriteAttributeString(attrName, "Y_Level2");
                    break;
                case E_CxLoanTimelyPaymentRewards.Y_Level3:
                    writer.WriteAttributeString(attrName, "Y_Level3");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxLoanAmountFinancedItemizationIndicator value)
        {
            switch (value)
            {
                case E_CxLoanAmountFinancedItemizationIndicator.N:
                    writer.WriteAttributeString(attrName, "N");
                    break;
                case E_CxLoanAmountFinancedItemizationIndicator.Y:
                    writer.WriteAttributeString(attrName, "Y");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxLoanDocumentsToUse value)
        {
            switch (value)
            {
                case E_CxLoanDocumentsToUse.Custom:
                    writer.WriteAttributeString(attrName, "Custom");
                    break;
                case E_CxLoanDocumentsToUse.FannieMae:
                    writer.WriteAttributeString(attrName, "FannieMae");
                    break;
                case E_CxLoanDocumentsToUse.FreddieMac:
                    writer.WriteAttributeString(attrName, "FreddieMac");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxLoanCemaIndicator value)
        {
            switch (value)
            {
                case E_CxLoanCemaIndicator.N:
                    writer.WriteAttributeString(attrName, "N");
                    break;
                case E_CxLoanCemaIndicator.Y:
                    writer.WriteAttributeString(attrName, "Y");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxLoanCalculate901UsingMarginPlusIndexIndicator value)
        {
            switch (value)
            {
                case E_CxLoanCalculate901UsingMarginPlusIndexIndicator.N:
                    writer.WriteAttributeString(attrName, "N");
                    break;
                case E_CxLoanCalculate901UsingMarginPlusIndexIndicator.Y:
                    writer.WriteAttributeString(attrName, "Y");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxLoanBiweeklyConversionIndictor value)
        {
            switch (value)
            {
                case E_CxLoanBiweeklyConversionIndictor.N:
                    writer.WriteAttributeString(attrName, "N");
                    break;
                case E_CxLoanBiweeklyConversionIndictor.Y:
                    writer.WriteAttributeString(attrName, "Y");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxLoanLotIndicator value)
        {
            switch (value)
            {
                case E_CxLoanLotIndicator.N:
                    writer.WriteAttributeString(attrName, "N");
                    break;
                case E_CxLoanLotIndicator.Y:
                    writer.WriteAttributeString(attrName, "Y");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxLoanCreditLifeRequiredIndicator value)
        {
            switch (value)
            {
                case E_CxLoanCreditLifeRequiredIndicator.N:
                    writer.WriteAttributeString(attrName, "N");
                    break;
                case E_CxLoanCreditLifeRequiredIndicator.Y:
                    writer.WriteAttributeString(attrName, "Y");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxLoanInvestorArmDisclosureAvailabilityIndicator value)
        {
            switch (value)
            {
                case E_CxLoanInvestorArmDisclosureAvailabilityIndicator.N:
                    writer.WriteAttributeString(attrName, "N");
                    break;
                case E_CxLoanInvestorArmDisclosureAvailabilityIndicator.Y:
                    writer.WriteAttributeString(attrName, "Y");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxLoanLastPaymentChangeIsFullyAmortizedIndicator value)
        {
            switch (value)
            {
                case E_CxLoanLastPaymentChangeIsFullyAmortizedIndicator.N:
                    writer.WriteAttributeString(attrName, "N");
                    break;
                case E_CxLoanLastPaymentChangeIsFullyAmortizedIndicator.Y:
                    writer.WriteAttributeString(attrName, "Y");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxLoanAdvantage90Indicator value)
        {
            switch (value)
            {
                case E_CxLoanAdvantage90Indicator.N:
                    writer.WriteAttributeString(attrName, "N");
                    break;
                case E_CxLoanAdvantage90Indicator.Y:
                    writer.WriteAttributeString(attrName, "Y");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxLoanFactaIndicator value)
        {
            switch (value)
            {
                case E_CxLoanFactaIndicator.N:
                    writer.WriteAttributeString(attrName, "N");
                    break;
                case E_CxLoanFactaIndicator.Y:
                    writer.WriteAttributeString(attrName, "Y");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxLoanAdjustableRatePeriodicCap value)
        {
            switch (value)
            {
                case E_CxLoanAdjustableRatePeriodicCap.No:
                    writer.WriteAttributeString(attrName, "No");
                    break;
                case E_CxLoanAdjustableRatePeriodicCap.Yes:
                    writer.WriteAttributeString(attrName, "Yes");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxLoanArmIndex value)
        {
            switch (value)
            {
                case E_CxLoanArmIndex.CODI:
                    writer.WriteAttributeString(attrName, "CODI");
                    break;
                case E_CxLoanArmIndex.DTC_FiveYearRateSwap:
                    writer.WriteAttributeString(attrName, "DTC_FiveYearRateSwap");
                    break;
                case E_CxLoanArmIndex.DTC_FiveYearTreasury:
                    writer.WriteAttributeString(attrName, "DTC_FiveYearTreasury");
                    break;
                case E_CxLoanArmIndex.DTC_OneMonthWSJLIBOR:
                    writer.WriteAttributeString(attrName, "DTC_OneMonthWSJLIBOR");
                    break;
                case E_CxLoanArmIndex.DTC_OneYearMTA:
                    writer.WriteAttributeString(attrName, "DTC_OneYearMTA");
                    break;
                case E_CxLoanArmIndex.DTC_OneYearWSJLIBOR:
                    writer.WriteAttributeString(attrName, "DTC_OneYearWSJLIBOR");
                    break;
                case E_CxLoanArmIndex.DTC_PrimePlus:
                    writer.WriteAttributeString(attrName, "DTC_PrimePlus");
                    break;
                case E_CxLoanArmIndex.DTC_SixMonthCD:
                    writer.WriteAttributeString(attrName, "DTC_SixMonthCD");
                    break;
                case E_CxLoanArmIndex.DTC_SixMonthWSJLIBOR:
                    writer.WriteAttributeString(attrName, "DTC_SixMonthWSJLIBOR");
                    break;
                case E_CxLoanArmIndex.DTC_SixMonthWeeklyAuctionTreasury:
                    writer.WriteAttributeString(attrName, "DTC_SixMonthWeeklyAuctionTreasury");
                    break;
                case E_CxLoanArmIndex.DTC_SixMonthsFNMALIBOR:
                    writer.WriteAttributeString(attrName, "DTC_SixMonthsFNMALIBOR");
                    break;
                case E_CxLoanArmIndex.DTC_TenYearTreasury:
                    writer.WriteAttributeString(attrName, "DTC_TenYearTreasury");
                    break;
                case E_CxLoanArmIndex.DTC_ThreeMonthPrimeRate:
                    writer.WriteAttributeString(attrName, "DTC_ThreeMonthPrimeRate");
                    break;
                case E_CxLoanArmIndex.DTC_ThreeMonthWSJLIBOR:
                    writer.WriteAttributeString(attrName, "DTC_ThreeMonthWSJLIBOR");
                    break;
                case E_CxLoanArmIndex.DTC_TwoYearTreasury:
                    writer.WriteAttributeString(attrName, "DTC_TwoYearTreasury");
                    break;
                case E_CxLoanArmIndex.EleventhDistrictCostOfFunds:
                    writer.WriteAttributeString(attrName, "EleventhDistrictCostOfFunds");
                    break;
                case E_CxLoanArmIndex.FNM60DayRequiredNetYield:
                    writer.WriteAttributeString(attrName, "FNM60DayRequiredNetYield");
                    break;
                case E_CxLoanArmIndex.NationalAverageContractRateFHLBB:
                    writer.WriteAttributeString(attrName, "NationalAverageContractRateFHLBB");
                    break;
                case E_CxLoanArmIndex.OneYearTreasury:
                    writer.WriteAttributeString(attrName, "OneYearTreasury");
                    break;
                case E_CxLoanArmIndex.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
                case E_CxLoanArmIndex.ThreeYearTreasury:
                    writer.WriteAttributeString(attrName, "ThreeYearTreasury");
                    break;
                case E_CxLoanArmIndex.WeeklyAveragePrimeRate:
                    writer.WriteAttributeString(attrName, "WeeklyAveragePrimeRate");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxLoanHelocProgram value)
        {
            switch (value)
            {
                case E_CxLoanHelocProgram.AmericanSterlingHeloc:
                    writer.WriteAttributeString(attrName, "AmericanSterlingHeloc");
                    break;
                case E_CxLoanHelocProgram.BarringtonHeloc:
                    writer.WriteAttributeString(attrName, "BarringtonHeloc");
                    break;
                case E_CxLoanHelocProgram._Cal_State_9_HELOC:
                    writer.WriteAttributeString(attrName, "Cal State 9 HELOC");
                    break;
                case E_CxLoanHelocProgram.CalBayHeloc:
                    writer.WriteAttributeString(attrName, "CalBayHeloc");
                    break;
                case E_CxLoanHelocProgram.ChaseHeloc:
                    writer.WriteAttributeString(attrName, "ChaseHeloc");
                    break;
                case E_CxLoanHelocProgram.CountrywideFlexSaverHeloc:
                    writer.WriteAttributeString(attrName, "CountrywideFlexSaverHeloc");
                    break;
                case E_CxLoanHelocProgram.CountrywideHeloc:
                    writer.WriteAttributeString(attrName, "CountrywideHeloc");
                    break;
                case E_CxLoanHelocProgram.CountrywidePreferredHeloc:
                    writer.WriteAttributeString(attrName, "CountrywidePreferredHeloc");
                    break;
                case E_CxLoanHelocProgram.CountrywideSubprimeHeloc:
                    writer.WriteAttributeString(attrName, "CountrywideSubprimeHeloc");
                    break;
                case E_CxLoanHelocProgram.CreditSuisseHeloc:
                    writer.WriteAttributeString(attrName, "CreditSuisseHeloc");
                    break;
                case E_CxLoanHelocProgram.CunaMutualMortgageHeloc:
                    writer.WriteAttributeString(attrName, "CunaMutualMortgageHeloc");
                    break;
                case E_CxLoanHelocProgram.ETradeBankHeloc:
                    writer.WriteAttributeString(attrName, "ETradeBankHeloc");
                    break;
                case E_CxLoanHelocProgram.FirstAdvantageMortgageHeloc:
                    writer.WriteAttributeString(attrName, "FirstAdvantageMortgageHeloc");
                    break;
                case E_CxLoanHelocProgram._FirstMac_C_X__First_Lien:
                    writer.WriteAttributeString(attrName, "FirstMac C-X+ First Lien");
                    break;
                case E_CxLoanHelocProgram._FirstMac_C_X__Second_Lien:
                    writer.WriteAttributeString(attrName, "FirstMac C-X+ Second Lien");
                    break;
                case E_CxLoanHelocProgram.FirstMacXHeloc:
                    writer.WriteAttributeString(attrName, "FirstMacXHeloc");
                    break;
                case E_CxLoanHelocProgram.FirstMacXPlusHeloc:
                    writer.WriteAttributeString(attrName, "FirstMacXPlusHeloc");
                    break;
                case E_CxLoanHelocProgram.GMACHeloc:
                    writer.WriteAttributeString(attrName, "GMACHeloc");
                    break;
                case E_CxLoanHelocProgram.GreenPointHeloc:
                    writer.WriteAttributeString(attrName, "GreenPointHeloc");
                    break;
                case E_CxLoanHelocProgram.HorizonCreditUnionHeloc:
                    writer.WriteAttributeString(attrName, "HorizonCreditUnionHeloc");
                    break;
                case E_CxLoanHelocProgram.ImpacHeloc:
                    writer.WriteAttributeString(attrName, "ImpacHeloc");
                    break;
                case E_CxLoanHelocProgram.IndymacHeloc:
                    writer.WriteAttributeString(attrName, "IndymacHeloc");
                    break;
                case E_CxLoanHelocProgram.KaipermFederalCreditUnionHeloc:
                    writer.WriteAttributeString(attrName, "KaipermFederalCreditUnionHeloc");
                    break;
                case E_CxLoanHelocProgram.LimeHeloc:
                    writer.WriteAttributeString(attrName, "LimeHeloc");
                    break;
                case E_CxLoanHelocProgram.MITLending_AND_MortgageITHeloc:
                    writer.WriteAttributeString(attrName, "MITLending_AND_MortgageITHeloc");
                    break;
                case E_CxLoanHelocProgram.RFCHeloc:
                    writer.WriteAttributeString(attrName, "RFCHeloc");
                    break;
                case E_CxLoanHelocProgram.SovereignHeloc:
                    writer.WriteAttributeString(attrName, "SovereignHeloc");
                    break;
                case E_CxLoanHelocProgram.TranslandHeloc:
                    writer.WriteAttributeString(attrName, "TranslandHeloc");
                    break;
                case E_CxLoanHelocProgram.VenturaFederalCountyCreditUnionHeloc:
                    writer.WriteAttributeString(attrName, "VenturaFederalCountyCreditUnionHeloc");
                    break;
                case E_CxLoanHelocProgram.WellsFargoHeloc:
                    writer.WriteAttributeString(attrName, "WellsFargoHeloc");
                    break;
                case E_CxLoanHelocProgram.WinterGroupHeloc:
                    writer.WriteAttributeString(attrName, "WinterGroupHeloc");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxLoanLifeFloorSetting value)
        {
            switch (value)
            {
                case E_CxLoanLifeFloorSetting.InitialRate:
                    writer.WriteAttributeString(attrName, "InitialRate");
                    break;
                case E_CxLoanLifeFloorSetting.Margin:
                    writer.WriteAttributeString(attrName, "Margin");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxLoanRehabilitationOrRenovationIndicator value)
        {
            switch (value)
            {
                case E_CxLoanRehabilitationOrRenovationIndicator.N:
                    writer.WriteAttributeString(attrName, "N");
                    break;
                case E_CxLoanRehabilitationOrRenovationIndicator.Y:
                    writer.WriteAttributeString(attrName, "Y");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxLoanLoanTableFundedInInvestorsNameIndicator value)
        {
            switch (value)
            {
                case E_CxLoanLoanTableFundedInInvestorsNameIndicator.N:
                    writer.WriteAttributeString(attrName, "N");
                    break;
                case E_CxLoanLoanTableFundedInInvestorsNameIndicator.Y:
                    writer.WriteAttributeString(attrName, "Y");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxLoanIncludeOddDaysTimeIndicator value)
        {
            switch (value)
            {
                case E_CxLoanIncludeOddDaysTimeIndicator.N:
                    writer.WriteAttributeString(attrName, "N");
                    break;
                case E_CxLoanIncludeOddDaysTimeIndicator.Y:
                    writer.WriteAttributeString(attrName, "Y");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxLoanPrepaymentMonthsOrPercentIndicator value)
        {
            switch (value)
            {
                case E_CxLoanPrepaymentMonthsOrPercentIndicator.Months:
                    writer.WriteAttributeString(attrName, "Months");
                    break;
                case E_CxLoanPrepaymentMonthsOrPercentIndicator.Percent:
                    writer.WriteAttributeString(attrName, "Percent");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxBuydownBuydownOptionType value)
        {
            switch (value)
            {
                case E_CxBuydownBuydownOptionType.Yes:
                    writer.WriteAttributeString(attrName, "Yes");
                    break;
                case E_CxBuydownBuydownOptionType.No:
                    writer.WriteAttributeString(attrName, "No");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_MortgageBrokerDtcType value)
        {
            switch (value)
            {
                case E_MortgageBrokerDtcType.Lender:
                    writer.WriteAttributeString(attrName, "Lender");
                    break;
                case E_MortgageBrokerDtcType.Broker:
                    writer.WriteAttributeString(attrName, "Broker");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxArmDisclosureArmDisclosurePrintingPreferenceForClosingPackageType value)
        {
            switch (value)
            {
                case E_CxArmDisclosureArmDisclosurePrintingPreferenceForClosingPackageType.LenderAlways:
                    writer.WriteAttributeString(attrName, "LenderAlways");
                    break;
                case E_CxArmDisclosureArmDisclosurePrintingPreferenceForClosingPackageType.InvestorIfExists_ElseLender:
                    writer.WriteAttributeString(attrName, "InvestorIfExists_ElseLender");
                    break;
                case E_CxArmDisclosureArmDisclosurePrintingPreferenceForClosingPackageType.InvestorOrNone:
                    writer.WriteAttributeString(attrName, "InvestorOrNone");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxArmDisclosureArmDisclosurePrintingPreferenceForInitialPackageType value)
        {
            switch (value)
            {
                case E_CxArmDisclosureArmDisclosurePrintingPreferenceForInitialPackageType.LenderAlways:
                    writer.WriteAttributeString(attrName, "LenderAlways");
                    break;
                case E_CxArmDisclosureArmDisclosurePrintingPreferenceForInitialPackageType.InvestorIfExists_ElseLender:
                    writer.WriteAttributeString(attrName, "InvestorIfExists_ElseLender");
                    break;
                case E_CxArmDisclosureArmDisclosurePrintingPreferenceForInitialPackageType.InvestorOrNone:
                    writer.WriteAttributeString(attrName, "InvestorOrNone");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_InitialDisclosuresPreviousLoanPurposeType value)
        {
            switch (value)
            {
                case E_InitialDisclosuresPreviousLoanPurposeType.CashOutRefinance:
                    writer.WriteAttributeString(attrName, "CashOutRefinance");
                    break;
                case E_InitialDisclosuresPreviousLoanPurposeType.Construction:
                    writer.WriteAttributeString(attrName, "Construction");
                    break;
                case E_InitialDisclosuresPreviousLoanPurposeType.ConstructionToPerm:
                    writer.WriteAttributeString(attrName, "ConstructionToPerm");
                    break;
                case E_InitialDisclosuresPreviousLoanPurposeType.HELOC:
                    writer.WriteAttributeString(attrName, "HELOC");
                    break;
                case E_InitialDisclosuresPreviousLoanPurposeType.NoCashOutRefinance:
                    writer.WriteAttributeString(attrName, "NoCashOutRefinance");
                    break;
                case E_InitialDisclosuresPreviousLoanPurposeType.Purchase:
                    writer.WriteAttributeString(attrName, "Purchase");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_InitialDisclosuresOpenOrClosedEndSelection value)
        {
            switch (value)
            {
                case E_InitialDisclosuresOpenOrClosedEndSelection.Closed:
                    writer.WriteAttributeString(attrName, "Closed");
                    break;
                case E_InitialDisclosuresOpenOrClosedEndSelection.Open:
                    writer.WriteAttributeString(attrName, "Open");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_InitialDisclosuresPreviousLoanPrePayPenaltyIndicator value)
        {
            switch (value)
            {
                case E_InitialDisclosuresPreviousLoanPrePayPenaltyIndicator.N:
                    writer.WriteAttributeString(attrName, "N");
                    break;
                case E_InitialDisclosuresPreviousLoanPrePayPenaltyIndicator.Y:
                    writer.WriteAttributeString(attrName, "Y");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_InitialDisclosuresIndexPublishPeriod value)
        {
            switch (value)
            {
                case E_InitialDisclosuresIndexPublishPeriod.Annually:
                    writer.WriteAttributeString(attrName, "Annually");
                    break;
                case E_InitialDisclosuresIndexPublishPeriod.Monthly:
                    writer.WriteAttributeString(attrName, "Monthly");
                    break;
                case E_InitialDisclosuresIndexPublishPeriod.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
                case E_InitialDisclosuresIndexPublishPeriod.Weekly:
                    writer.WriteAttributeString(attrName, "Weekly");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_InitialDisclosuresLockInFeeRefundableIndicator value)
        {
            switch (value)
            {
                case E_InitialDisclosuresLockInFeeRefundableIndicator.N:
                    writer.WriteAttributeString(attrName, "N");
                    break;
                case E_InitialDisclosuresLockInFeeRefundableIndicator.Y:
                    writer.WriteAttributeString(attrName, "Y");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_InitialDisclosuresIndexEstablishedBy value)
        {
            switch (value)
            {
                case E_InitialDisclosuresIndexEstablishedBy.FNMA:
                    writer.WriteAttributeString(attrName, "FNMA");
                    break;
                case E_InitialDisclosuresIndexEstablishedBy.FederalReserve:
                    writer.WriteAttributeString(attrName, "FederalReserve");
                    break;
                case E_InitialDisclosuresIndexEstablishedBy.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
                case E_InitialDisclosuresIndexEstablishedBy.WallStreetJournal:
                    writer.WriteAttributeString(attrName, "WallStreetJournal");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_InitialDisclosuresBalloonConditionalRighttoRefinanceIndicator value)
        {
            switch (value)
            {
                case E_InitialDisclosuresBalloonConditionalRighttoRefinanceIndicator.N:
                    writer.WriteAttributeString(attrName, "N");
                    break;
                case E_InitialDisclosuresBalloonConditionalRighttoRefinanceIndicator.Y:
                    writer.WriteAttributeString(attrName, "Y");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_InitialDisclosuresFeeCreditLifeDisabilityPaidIndicator value)
        {
            switch (value)
            {
                case E_InitialDisclosuresFeeCreditLifeDisabilityPaidIndicator.N:
                    writer.WriteAttributeString(attrName, "N");
                    break;
                case E_InitialDisclosuresFeeCreditLifeDisabilityPaidIndicator.Y:
                    writer.WriteAttributeString(attrName, "Y");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_InitialDisclosuresDisclosureStatementIndicator value)
        {
            switch (value)
            {
                case E_InitialDisclosuresDisclosureStatementIndicator.N:
                    writer.WriteAttributeString(attrName, "N");
                    break;
                case E_InitialDisclosuresDisclosureStatementIndicator.Y:
                    writer.WriteAttributeString(attrName, "Y");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_InitialDisclosuresUffiIndicator value)
        {
            switch (value)
            {
                case E_InitialDisclosuresUffiIndicator.N:
                    writer.WriteAttributeString(attrName, "N");
                    break;
                case E_InitialDisclosuresUffiIndicator.Y:
                    writer.WriteAttributeString(attrName, "Y");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_InitialDisclosuresLenderWillSetRateAt value)
        {
            switch (value)
            {
                case E_InitialDisclosuresLenderWillSetRateAt.Application:
                    writer.WriteAttributeString(attrName, "Application");
                    break;
                case E_InitialDisclosuresLenderWillSetRateAt.Closing:
                    writer.WriteAttributeString(attrName, "Closing");
                    break;
                case E_InitialDisclosuresLenderWillSetRateAt.Commitment:
                    writer.WriteAttributeString(attrName, "Commitment");
                    break;
                case E_InitialDisclosuresLenderWillSetRateAt.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_InitialDisclosuresLenderWillSetRateIndicator value)
        {
            switch (value)
            {
                case E_InitialDisclosuresLenderWillSetRateIndicator.N:
                    writer.WriteAttributeString(attrName, "N");
                    break;
                case E_InitialDisclosuresLenderWillSetRateIndicator.Y:
                    writer.WriteAttributeString(attrName, "Y");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_InitialDisclosuresRateCanChangeIndicator value)
        {
            switch (value)
            {
                case E_InitialDisclosuresRateCanChangeIndicator.N:
                    writer.WriteAttributeString(attrName, "N");
                    break;
                case E_InitialDisclosuresRateCanChangeIndicator.Y:
                    writer.WriteAttributeString(attrName, "Y");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxHelocDrawPeriodType value)
        {
            switch (value)
            {
                case E_CxHelocDrawPeriodType.Months:
                    writer.WriteAttributeString(attrName, "Months");
                    break;
                case E_CxHelocDrawPeriodType.Years:
                    writer.WriteAttributeString(attrName, "Years");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxHelocInitialPercentRateType value)
        {
            switch (value)
            {
                case E_CxHelocInitialPercentRateType.DiscountedFixed:
                    writer.WriteAttributeString(attrName, "DiscountedFixed");
                    break;
                case E_CxHelocInitialPercentRateType.DiscountedVariable:
                    writer.WriteAttributeString(attrName, "DiscountedVariable");
                    break;
                case E_CxHelocInitialPercentRateType.NotDiscounted:
                    writer.WriteAttributeString(attrName, "NotDiscounted");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxHelocInvestorDocuments value)
        {
            switch (value)
            {
                case E_CxHelocInvestorDocuments.Countrywide:
                    writer.WriteAttributeString(attrName, "Countrywide");
                    break;
                case E_CxHelocInvestorDocuments.RFC:
                    writer.WriteAttributeString(attrName, "RFC");
                    break;
                case E_CxHelocInvestorDocuments.GMAC:
                    writer.WriteAttributeString(attrName, "GMAC");
                    break;
                case E_CxHelocInvestorDocuments.GreenPoint:
                    writer.WriteAttributeString(attrName, "GreenPoint");
                    break;
                case E_CxHelocInvestorDocuments._Wells_Fargo:
                    writer.WriteAttributeString(attrName, "Wells Fargo");
                    break;
                case E_CxHelocInvestorDocuments.Macquarie:
                    writer.WriteAttributeString(attrName, "Macquarie");
                    break;
                case E_CxHelocInvestorDocuments.IndyMac:
                    writer.WriteAttributeString(attrName, "IndyMac");
                    break;
                case E_CxHelocInvestorDocuments.Sovereign:
                    writer.WriteAttributeString(attrName, "Sovereign");
                    break;
                case E_CxHelocInvestorDocuments.Impac:
                    writer.WriteAttributeString(attrName, "Impac");
                    break;
                case E_CxHelocInvestorDocuments._Credit_Suisse:
                    writer.WriteAttributeString(attrName, "Credit Suisse");
                    break;
                case E_CxHelocInvestorDocuments._MIT_Lending___MortgageIT:
                    writer.WriteAttributeString(attrName, "MIT Lending & MortgageIT");
                    break;
                case E_CxHelocInvestorDocuments._Ventura_Federal_County_Credit_Union:
                    writer.WriteAttributeString(attrName, "Ventura Federal County Credit Union");
                    break;
                case E_CxHelocInvestorDocuments._Cal_Bay:
                    writer.WriteAttributeString(attrName, "Cal Bay");
                    break;
                case E_CxHelocInvestorDocuments.FirstMac:
                    writer.WriteAttributeString(attrName, "FirstMac");
                    break;
                case E_CxHelocInvestorDocuments._1st_Advantage_Mortgage:
                    writer.WriteAttributeString(attrName, "1st Advantage Mortgage");
                    break;
                case E_CxHelocInvestorDocuments._The_Winter_Group:
                    writer.WriteAttributeString(attrName, "The Winter Group");
                    break;
                case E_CxHelocInvestorDocuments._Cuna_Mutual_Mortgage:
                    writer.WriteAttributeString(attrName, "Cuna Mutual Mortgage");
                    break;
                case E_CxHelocInvestorDocuments.Transland:
                    writer.WriteAttributeString(attrName, "Transland");
                    break;
                case E_CxHelocInvestorDocuments.Chase:
                    writer.WriteAttributeString(attrName, "Chase");
                    break;
                case E_CxHelocInvestorDocuments._American_Sterling:
                    writer.WriteAttributeString(attrName, "American Sterling");
                    break;
                case E_CxHelocInvestorDocuments._Horizon_Credit_Union:
                    writer.WriteAttributeString(attrName, "Horizon Credit Union");
                    break;
                case E_CxHelocInvestorDocuments._Barrington_Bank___Trust:
                    writer.WriteAttributeString(attrName, "Barrington Bank & Trust");
                    break;
                case E_CxHelocInvestorDocuments._Kaiperm_Federal_Credit_Union:
                    writer.WriteAttributeString(attrName, "Kaiperm Federal Credit Union");
                    break;
                case E_CxHelocInvestorDocuments._ETrade_Bank:
                    writer.WriteAttributeString(attrName, "ETrade Bank");
                    break;
                case E_CxHelocInvestorDocuments._Cal_State_9_INV:
                    writer.WriteAttributeString(attrName, "Cal State 9-INV");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxHelocPaymentAmountCalculationType value)
        {
            switch (value)
            {
                case E_CxHelocPaymentAmountCalculationType.GreaterOfAmountOrPercent:
                    writer.WriteAttributeString(attrName, "GreaterOfAmountOrPercent");
                    break;
                case E_CxHelocPaymentAmountCalculationType.FixedAmount:
                    writer.WriteAttributeString(attrName, "FixedAmount");
                    break;
                case E_CxHelocPaymentAmountCalculationType.FinanceChargePlusCreditInsurance:
                    writer.WriteAttributeString(attrName, "FinanceChargePlusCreditInsurance");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxHelocPaymentMethodForLineOfCredit value)
        {
            switch (value)
            {
                case E_CxHelocPaymentMethodForLineOfCredit.BillMe:
                    writer.WriteAttributeString(attrName, "BillMe");
                    break;
                case E_CxHelocPaymentMethodForLineOfCredit.AutoChargeAccount:
                    writer.WriteAttributeString(attrName, "AutoChargeAccount");
                    break;
                case E_CxHelocPaymentMethodForLineOfCredit.AutoChargeAnotherAccount:
                    writer.WriteAttributeString(attrName, "AutoChargeAnotherAccount");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxHelocRepaymentPeriodType value)
        {
            switch (value)
            {
                case E_CxHelocRepaymentPeriodType.Months:
                    writer.WriteAttributeString(attrName, "Months");
                    break;
                case E_CxHelocRepaymentPeriodType.Years:
                    writer.WriteAttributeString(attrName, "Years");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxHelocFeeType value)
        {
            switch (value)
            {
                case E_CxHelocFeeType._Finance_Charge:
                    writer.WriteAttributeString(attrName, "Finance Charge");
                    break;
                case E_CxHelocFeeType.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
                case E_CxHelocFeeType._Other_Charge:
                    writer.WriteAttributeString(attrName, "Other Charge");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxImportantTermsName value)
        {
            switch (value)
            {
                case E_CxImportantTermsName.Administration:
                    writer.WriteAttributeString(attrName, "Administration");
                    break;
                case E_CxImportantTermsName._Annual_Maintenance:
                    writer.WriteAttributeString(attrName, "Annual Maintenance");
                    break;
                case E_CxImportantTermsName.Application:
                    writer.WriteAttributeString(attrName, "Application");
                    break;
                case E_CxImportantTermsName._Attorney_Max:
                    writer.WriteAttributeString(attrName, "Attorney Max");
                    break;
                case E_CxImportantTermsName._Attorney_Min:
                    writer.WriteAttributeString(attrName, "Attorney Min");
                    break;
                case E_CxImportantTermsName.Broker:
                    writer.WriteAttributeString(attrName, "Broker");
                    break;
                case E_CxImportantTermsName._Document_Preparation:
                    writer.WriteAttributeString(attrName, "Document Preparation");
                    break;
                case E_CxImportantTermsName._Expedited_Delivery:
                    writer.WriteAttributeString(attrName, "Expedited Delivery");
                    break;
                case E_CxImportantTermsName._Loan_Tie_In:
                    writer.WriteAttributeString(attrName, "Loan Tie In");
                    break;
                case E_CxImportantTermsName._Low_Balance:
                    writer.WriteAttributeString(attrName, "Low Balance");
                    break;
                case E_CxImportantTermsName._Maximum_Interest:
                    writer.WriteAttributeString(attrName, "Maximum Interest");
                    break;
                case E_CxImportantTermsName.Origination:
                    writer.WriteAttributeString(attrName, "Origination");
                    break;
                case E_CxImportantTermsName._Other_1:
                    writer.WriteAttributeString(attrName, "Other 1");
                    break;
                case E_CxImportantTermsName._Other_2:
                    writer.WriteAttributeString(attrName, "Other 2");
                    break;
                case E_CxImportantTermsName._Other_Maximum:
                    writer.WriteAttributeString(attrName, "Other Maximum");
                    break;
                case E_CxImportantTermsName.Points:
                    writer.WriteAttributeString(attrName, "Points");
                    break;
                case E_CxImportantTermsName.Processing:
                    writer.WriteAttributeString(attrName, "Processing");
                    break;
                case E_CxImportantTermsName.Termination:
                    writer.WriteAttributeString(attrName, "Termination");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxRecordingType value)
        {
            switch (value)
            {
                case E_CxRecordingType.Mortgage:
                    writer.WriteAttributeString(attrName, "Mortgage");
                    break;
                case E_CxRecordingType.Assignment:
                    writer.WriteAttributeString(attrName, "Assignment");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxRecordingRecordingJurisdictionType value)
        {
            switch (value)
            {
                case E_CxRecordingRecordingJurisdictionType.County:
                    writer.WriteAttributeString(attrName, "County");
                    break;
                case E_CxRecordingRecordingJurisdictionType.Parish:
                    writer.WriteAttributeString(attrName, "Parish");
                    break;
                case E_CxRecordingRecordingJurisdictionType.Borough:
                    writer.WriteAttributeString(attrName, "Borough");
                    break;
                case E_CxRecordingRecordingJurisdictionType.JudicialDistrict:
                    writer.WriteAttributeString(attrName, "JudicialDistrict");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxAgentType value)
        {
            switch (value)
            {
                case E_CxAgentType.Closer:
                    writer.WriteAttributeString(attrName, "Closer");
                    break;
                case E_CxAgentType.Officer:
                    writer.WriteAttributeString(attrName, "Officer");
                    break;
                case E_CxAgentType.Processor:
                    writer.WriteAttributeString(attrName, "Processor");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxCreditScoreCreditRepositorySourceType value)
        {
            switch (value)
            {
                case E_CxCreditScoreCreditRepositorySourceType.Equifax:
                    writer.WriteAttributeString(attrName, "Equifax");
                    break;
                case E_CxCreditScoreCreditRepositorySourceType.Experian:
                    writer.WriteAttributeString(attrName, "Experian");
                    break;
                case E_CxCreditScoreCreditRepositorySourceType.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
                case E_CxCreditScoreCreditRepositorySourceType.TransUnion:
                    writer.WriteAttributeString(attrName, "TransUnion");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxDocTrigger_4506 value)
        {
            switch (value)
            {
                case E_CxDocTrigger_4506.None:
                    writer.WriteAttributeString(attrName, "None");
                    break;
                case E_CxDocTrigger_4506._4506:
                    writer.WriteAttributeString(attrName, "4506");
                    break;
                case E_CxDocTrigger_4506._4506_T:
                    writer.WriteAttributeString(attrName, "4506-T");
                    break;
                case E_CxDocTrigger_4506.Both:
                    writer.WriteAttributeString(attrName, "Both");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxStateSpecificInfoSubjectToKansasUniformCommercialCreditCode value)
        {
            switch (value)
            {
                case E_CxStateSpecificInfoSubjectToKansasUniformCommercialCreditCode.N:
                    writer.WriteAttributeString(attrName, "N");
                    break;
                case E_CxStateSpecificInfoSubjectToKansasUniformCommercialCreditCode.Y:
                    writer.WriteAttributeString(attrName, "Y");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxStateSpecificInfoDerivationClauseDocumentType value)
        {
            switch (value)
            {
                case E_CxStateSpecificInfoDerivationClauseDocumentType.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
                case E_CxStateSpecificInfoDerivationClauseDocumentType.QuitClaimDeed:
                    writer.WriteAttributeString(attrName, "QuitClaimDeed");
                    break;
                case E_CxStateSpecificInfoDerivationClauseDocumentType.WarrantyDeed:
                    writer.WriteAttributeString(attrName, "WarrantyDeed");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxStateSpecificInfoNonPara27PurchaseMoneyIndicator value)
        {
            switch (value)
            {
                case E_CxStateSpecificInfoNonPara27PurchaseMoneyIndicator.N:
                    writer.WriteAttributeString(attrName, "N");
                    break;
                case E_CxStateSpecificInfoNonPara27PurchaseMoneyIndicator.Y:
                    writer.WriteAttributeString(attrName, "Y");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxStateSpecificInfoDerivationRecordingInfoType value)
        {
            switch (value)
            {
                case E_CxStateSpecificInfoDerivationRecordingInfoType._N_A:
                    writer.WriteAttributeString(attrName, "N/A");
                    break;
                case E_CxStateSpecificInfoDerivationRecordingInfoType.UseBookPage:
                    writer.WriteAttributeString(attrName, "UseBookPage");
                    break;
                case E_CxStateSpecificInfoDerivationRecordingInfoType.UseInstrument:
                    writer.WriteAttributeString(attrName, "UseInstrument");
                    break;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_CxStateSpecificInfoTexasHomeEquitySection50LoanIndicator value)
        {
            switch (value)
            {
                case E_CxStateSpecificInfoTexasHomeEquitySection50LoanIndicator.N:
                    writer.WriteAttributeString(attrName, "N");
                    break;
                case E_CxStateSpecificInfoTexasHomeEquitySection50LoanIndicator.Y:
                    writer.WriteAttributeString(attrName, "Y");
                    break;
            }
        }

        protected void ReadAttribute(XmlReader reader, string attrName, out E_InterimInterestDisclosureType result)
        {
            string s = ReadAttribute(reader, attrName);
            if (s == "GFE")
            {
                result = E_InterimInterestDisclosureType.GFE;
            }
            else if (s == "HUD1")
            {
                result = E_InterimInterestDisclosureType.HUD1;
            }
            else if (s == "Other")
            {
                result = E_InterimInterestDisclosureType.Other;
            }
            else
            {
                result = E_InterimInterestDisclosureType.Undefined;
            }
        }
        protected void WriteAttribute(XmlWriter writer, string attrName, E_InterimInterestDisclosureType value)
        {
            switch (value)
            {
                case E_InterimInterestDisclosureType.GFE:
                    writer.WriteAttributeString(attrName, "GFE");
                    break;
                case E_InterimInterestDisclosureType.HUD1:
                    writer.WriteAttributeString(attrName, "HUD1");
                    break;
                case E_InterimInterestDisclosureType.Other:
                    writer.WriteAttributeString(attrName, "Other");
                    break;
            }
        }
        #endregion
        protected virtual void ReadAttributes(XmlReader reader)
        {
        }

        protected virtual void ReadElement(XmlReader reader)
        {
        }
        protected virtual void ReadTextContent(XmlReader reader)
        {
        }
        protected virtual void WriteXmlImpl(XmlWriter writer)
        {
        }
        #region IXmlSerializable Members

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.LocalName != XmlElementName)
            {
                reader.Read();
                if (reader.EOF)
                {
                    return;
                }
            }
            ReadAttributes(reader);

            if (reader.IsEmptyElement) return;

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Text)
                {
                    ReadTextContent(reader);
                    continue;
                }
                if (reader.NodeType == XmlNodeType.EndElement && reader.LocalName == XmlElementName)
                {
                    return;
                }
                if (reader.NodeType != XmlNodeType.Element)
                {
                    continue;
                }

                ReadElement(reader);
            }
        }


        public void WriteXml(System.Xml.XmlWriter writer)
        {
            //if (string.IsNullOrEmpty(Prefix) == false && string.IsNullOrEmpty(Namespace) == false)
            //{
            //    writer.WriteStartElement(Prefix, XmlElementName, Namespace);
            //}
            //else
            //{
                writer.WriteStartElement(XmlElementName);
            //}

            WriteXmlImpl(writer);
            writer.WriteEndElement();
        }


        #endregion
    }
}
