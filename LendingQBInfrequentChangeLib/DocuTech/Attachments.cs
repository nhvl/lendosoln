using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace DocuTech
{
    public sealed class Attachments : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "ATTACHMENTS"; }
        }

        private List<EMortgagePackage> m_emortgagePackageList;
        public List<EMortgagePackage> EMortgagePackageList
        {
            get
            {
                if (m_emortgagePackageList == null)
                {
                    m_emortgagePackageList = new List<EMortgagePackage>();
                }
                return m_emortgagePackageList;
            }
        }
        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "EMORTGAGE_PACKAGE": ReadElement(reader, EMortgagePackageList); break;
            }
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, m_emortgagePackageList);
        }
    }
}
