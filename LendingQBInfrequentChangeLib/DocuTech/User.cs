using System;
using System.Collections.Generic;
using System.Xml;

namespace DocuTech
{
    public sealed class User : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "USER"; }
        }

        public string UserName;
        public string Password;

        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "_Username", out UserName);
            ReadAttribute(reader, "_Password", out Password);
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "_Username", UserName);
            WriteAttribute(writer, "_Password", Password);
        }
    }
}
