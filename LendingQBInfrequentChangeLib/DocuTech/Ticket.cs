using System;
using System.Collections.Generic;
using System.Xml;

namespace DocuTech
{
    public sealed class Ticket : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "TICKET"; }
        }
        public string Id;
        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "_Id", out Id);
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "_Id", Id);
        }
    }
}
