using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace DocuTech
{
    public sealed class SmartDocumentData : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "DATA"; }
        }
        private SmartDocumentDataMain m_main;
        public SmartDocumentDataMain Main
        {
            get
            {
                if (m_main == null)
                {
                    m_main = new SmartDocumentDataMain();
                }
                return m_main;
            }
            set { m_main = value; } 
        }
        private Custom m_custom;
        public Custom Custom
        {
            get
            {
                if (m_custom == null)
                {
                    m_custom = new Custom();
                }
                return m_custom;
            }
            set { m_custom = value; } 
        }

        protected override void ReadElement(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "MAIN": ReadElement(reader, Main); break;
                case "CUSTOM": ReadElement(reader, Custom); break;
            }
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteElement(writer, m_main);
            WriteElement(writer, m_custom);
        }
    }
}
