using System;
using System.Collections.Generic;
using System.Xml;

namespace DocuTech
{
    public sealed class Email : AbstractXmlSerializable
    {
        public override string XmlElementName
        {
            get { return "EMAIL"; }
        }
        public string Address;

        protected override void ReadAttributes(XmlReader reader)
        {
            ReadAttribute(reader, "_Address", out Address);
        }
        protected override void WriteXmlImpl(XmlWriter writer)
        {
            WriteAttribute(writer, "_Address", Address);
        }
    }
}
