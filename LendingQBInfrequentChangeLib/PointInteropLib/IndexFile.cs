using System;
using System.IO;
using System.Xml;
using System.Text;

namespace PointInteropLib
{
	/// <summary>
	/// Summary description for IndexFile.
	/// </summary>
	public class IndexFile
	{
		public IndexFile()
		{
		}
		public IndexRecord[] ReadIndexFromStream(Stream reader)
		{
			CheckStream(reader) ;
			int cRecords = GetRecordCount(reader) ;
			IndexRecord[] rgRecords = new IndexRecord[cRecords] ;
			string NullString = GetNullString() ;

			byte[] buf = new byte[RECORD_SIZE] ;
			for (int i = 0 ; i < cRecords ; i++)
			{
				if (RECORD_SIZE != (reader.Read(buf, 0, RECORD_SIZE)))
					throw new Exception("An error occurred while trying to read in records") ;

				rgRecords[i].LastName = GetIndexFileString(buf, 0, 20) ;
				rgRecords[i].FirstName = GetIndexFileString(buf, 20, 20) ;
				rgRecords[i].FileName = GetIndexFileString(buf, 40, 12) ;
				rgRecords[i].RepName = GetIndexFileString(buf, 53, 25) ;
				rgRecords[i].ContactDate = GetIndexFileString(buf, 78, 8) ;
			}

			return rgRecords ;
		}
		/// <summary>
		/// Parse a record of the index file for a particular string. Strings inside the
		/// index file are stored as NULL terminated string. Since Point does not do a
		/// memset to clear out stale data, we will read in extra/invalid information if
		/// we do not STOP reading at a null termination.
		/// </summary>
		string GetIndexFileString(byte[] buf, int nStart, int nCount)
		{
			int nNullIndex = Array.IndexOf(buf, (byte)0, nStart, nCount) ;
			if (-1 == nNullIndex)
				return Encoding.ASCII.GetString(buf, nStart, nCount).Trim() ;
			else
				return Encoding.ASCII.GetString(buf, nStart, nNullIndex-nStart).Trim() ;
		}
		public void Convert2XML(Stream reader, Stream writer)
		{
			IndexRecord[] rgRecords = ReadIndexFromStream(reader) ;
			XmlTextWriter xmlWriter = new XmlTextWriter(writer, Encoding.ASCII) ;

			xmlWriter.WriteStartDocument() ;
			xmlWriter.WriteStartElement("POINT_INDEX") ;
			foreach (IndexRecord record in rgRecords)
			{
				xmlWriter.WriteStartElement("RECORD") ;
				xmlWriter.WriteElementString("FirstName", record.FirstName) ;
				xmlWriter.WriteElementString("LastName", record.LastName) ;
				xmlWriter.WriteElementString("FileName", record.FileName) ;
				xmlWriter.WriteElementString("RepName", record.RepName) ;
				xmlWriter.WriteElementString("ContactDate", record.ContactDate) ;
				xmlWriter.WriteEndElement() ;
			}
			xmlWriter.WriteEndElement() ;
			xmlWriter.WriteEndDocument() ;
			xmlWriter.Flush() ;
		}
		public void Convert2XML(string sIndexFile, string sXMLFile)
		{
			using (FileStream reader = new FileStream(sIndexFile, FileMode.Open, FileAccess.Read))
			{
				using (FileStream writer = new FileStream(sIndexFile, FileMode.Create, FileAccess.Write))
				{
					Convert2XML(reader, writer) ;
				}
			}
		}
		void CheckStream(Stream reader)
		{
			if (reader.Length % RECORD_SIZE != 0) 
				throw new Exception("Incorrect filesize") ;
		}
		int GetRecordCount(Stream reader)
		{
			return Convert.ToInt32(reader.Length / RECORD_SIZE) ;
		}
		string GetNullString()
		{
			char[] buf = new char[1] ;
			buf[0] = Convert.ToChar(0) ;
			return new string(buf) ;
		}

		const int RECORD_SIZE = 87 ;
	}
	public struct IndexRecord
	{
		public string FirstName ;
		public string LastName ;
		public string FileName ;
		public string RepName ;
		public string ContactDate ;
	}
}
