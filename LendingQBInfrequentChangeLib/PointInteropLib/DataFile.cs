using System;
using System.Collections;
using System.Collections.Specialized;
using System.IO;
using System.Text;

namespace PointInteropLib
{
	/// <summary>
	/// Summary description for DataFile.
	/// </summary>
	public class DataFile
	{
		public DataFile()
		{
		}
		public NameValueCollection ReadFile(string sFileName)
		{
			using (FileStream fs = File.OpenRead(sFileName))
			{
				return ReadStream(fs) ;
			}
		}
		public NameValueCollection ReadStream(Stream stream)
		{
			NameValueCollection rgRet = new NameValueCollection() ;
			byte[] rgHeader = new byte[3] ;
			byte[] rgData = new byte[1024] ;
			byte[] rgNewSize = new byte[2] ;
			while (3 == stream.Read(rgHeader, 0, 3))
			{
				string sID = BitConverter.ToInt16(rgHeader, 0).ToString() ;
				int nSize ;
				if (0xFF == rgHeader[2])
				{
					// if the size field is FF, then the actual length of the new
					// field is stored in the next 2 bytes
					if (2 != stream.Read(rgNewSize, 0, 2))
						break ;
					nSize = BitConverter.ToInt16(rgNewSize, 0) ;
				}
				else
					nSize = rgHeader[2] ;

				// re-allocate memory for the data buffer if necessary
				if (rgData.Length < nSize)
					rgData = new byte[nSize] ;

				int nBytesRead = stream.Read(rgData, 0, nSize) ;
				if (nBytesRead > 0)
					rgRet.Add(sID, Encoding.ASCII.GetString(rgData, 0, nBytesRead).ToUpper()) ;
			}

			return rgRet ;
		}
		/// <summary>
		/// Do not use me!  Use sorted stream instead :D  -av opm 23780
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="rgData"></param>
		public void WriteStream(Stream stream, NameValueCollection rgData)
		{
			int nCount = rgData.Count ;
			for (int i = 0 ; i < nCount ; i++)
			{
				short nKey = Convert.ToInt16(rgData.GetKey(i)) ;
				string sValue = rgData[i] ;
				if (nKey > 0 && null != sValue && sValue.Length > 0)
				{
					// write the header & length
					stream.Write(BitConverter.GetBytes(nKey), 0, 2) ;
					byte[] rgValue = Encoding.ASCII.GetBytes(sValue) ;
					byte[] rgLen = BitConverter.GetBytes(rgValue.Length) ;

					// if the size field is FF, then the actual length of the new
					// field is stored in the next 2 bytes
                    if (rgValue.Length >= 0xFF) 
                    {
                        stream.Write(new byte[] { 0xFF }, 0, 1); // 9/22/2005 dd - Write FF to indicate read size at the next 2 bytes
                        stream.Write(rgLen, 0, 2) ;
                    }
                    else
                        stream.Write(rgLen, 0, 1) ;
					stream.Write(rgValue, 0, rgValue.Length) ;
				}
			}
			// terminate the stream with 2 bytes of 0xFF
			byte[] rgTerminator = {0xFF, 0xFF} ;
			stream.Write(rgTerminator, 0, 2) ;
		}

		/// <summary>
		/// DONT USE.  AV 07 15 08  22255 This is Temp to see if it fixes DataTrec issues with a certain vendor. 
		/// UPDATE 08 29 08 this is now the way to export point files please disregard previous comment. opm 23870  
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="rgData"></param>
		public void WriteSortedStream( Stream stream, NameValueCollection rgData )
		{
			SortedList fields = new SortedList(); 
			for( int i = 0;  i < rgData.Count; i++ ) 
			{
				fields.Add(Convert.ToInt16(rgData.GetKey(i)), rgData[i] );
			}

			foreach ( short key in fields.Keys ) 
			{
				if ( key > 0 && fields[key] != null && fields[key].ToString().Length  > 0 ) 
				{
					stream.Write(BitConverter.GetBytes(key), 0, 2 ) ; 
					byte[] rgValue = Encoding.ASCII.GetBytes(fields[key].ToString()) ;
					byte[] rgLen = BitConverter.GetBytes(rgValue.Length) ;

					// if the size field is FF, then the actual length of the new
					// field is stored in the next 2 bytes
					if (rgValue.Length >= 0xFF) 
					{
						stream.Write(new byte[] { 0xFF }, 0, 1); // 9/22/2005 dd - Write FF to indicate read size at the next 2 bytes
						stream.Write(rgLen, 0, 2) ;
					}
					else
						stream.Write(rgLen, 0, 1) ;
					stream.Write(rgValue, 0, rgValue.Length) ;
				}
			}

			// terminate the stream with 2 bytes of 0xFF
			byte[] rgTerminator = {0xFF, 0xFF} ;
			stream.Write(rgTerminator, 0, 2) ;
		}
	}
}
