﻿using System.Reflection.Emit;

namespace DavidDao.Reflection
{
    public class ILInstruction
    {
        private OpCode m_opCode;
        private int m_metadataToken;
        private byte[] m_operandRaw;

        internal ILInstruction(OpCode opcode, byte[] operandRaw)
        {
            m_opCode = opcode;
            m_operandRaw = operandRaw;

            if (null != operandRaw && operandRaw.Length == 4)
            {
                m_metadataToken = operandRaw[0] | (operandRaw[1] << 8) | (operandRaw[2] << 16) | (operandRaw[3] << 24);
            }
        }
        public OpCode OpCode
        {
            get { return m_opCode; }
        }
        public int MetadataToken
        {
            get { return m_metadataToken; }
        }
        public byte[] OperandRaw
        {
            get { return m_operandRaw; }
        }

    }
}
