﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;

namespace DavidDao.Reflection

{
    public static class MSILExtensions {
        private static void GetDependsOnListImpl(MethodBase method, List<MethodBase> results)
        {
            MethodBody methodBody = method.GetMethodBody();
            if (null == methodBody)
            {
                return;
            }
            var instructionList = MSILParser.Parse(methodBody.GetILAsByteArray());
            Module module = method.DeclaringType.Module;

            foreach (var instruction in instructionList)
            {
                try
                {
                    if (instruction.OpCode == OpCodes.Call || instruction.OpCode == OpCodes.Callvirt || instruction.OpCode == OpCodes.Ldtoken || instruction.OpCode == OpCodes.Newobj)
                    {
                        MethodBase m = module.ResolveMethod(instruction.MetadataToken);

                        if (!results.Contains(m))
                        {
                            results.Add(m);
                        }
                    }
                    else if (instruction.OpCode == OpCodes.Ldftn || instruction.OpCode == OpCodes.Ldvirtftn)
                    {
                        // Lambda expressions, such as Select(loan => loan.sLT), will not be picked
                        // up by the dependency generation logic when checking only for calls or token 
                        // loads due to the way the C# compiler translates lambdas into intermediate code.
                        // Instead, we'll use the Ldftn and Ldvirtftn opcodes, which push the pointer
                        // to the lambda's method onto the stack, as the filter. If we find one, 
                        // we'll grab the dependency list for the lambda and add it to the results.
                        var lambdaMethod = module.ResolveMethod(instruction.MetadataToken);

                        GetDependsOnListImpl(lambdaMethod, results);
                    }
                }
                catch
                {
                    // Exception could be throw if method is generic.
                }
            }

        }
        public static List<MethodBase> GetDependsOnList(this MethodBase method)
        {
            List<MethodBase> results = new List<MethodBase>();

            GetDependsOnListImpl(method, results);
            return results;
        }
        public static List<MethodBase> GetDependsOnList(this Type type)
        {
            List<MethodBase> results = new List<MethodBase>();

            if (null == type)
                return results;

            MethodInfo[] methodList = type.GetMethods(BindingFlags.Static | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.DeclaredOnly);

            foreach (MethodInfo method in methodList)
            {
                GetDependsOnListImpl(method, results);
            }

            ConstructorInfo[] constructorList = type.GetConstructors();
            foreach (ConstructorInfo constructor in constructorList)
            {
                GetDependsOnListImpl(constructor, results);
            }
            return results;
        }
    }
}
