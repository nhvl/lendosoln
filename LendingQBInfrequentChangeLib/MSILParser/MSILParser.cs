﻿using System;
using System.Collections.Generic;
using System.Reflection.Emit;

namespace DavidDao.Reflection
{
    public static class MSILParser
    {
        #region MSIL Opcode for .NET 3.5
        private static readonly Dictionary<short, OpCode> OpCodeHash = new Dictionary<short, OpCode>
        {
{OpCodes.Nop.Value, OpCodes.Nop}
, {OpCodes.Break.Value, OpCodes.Break}
, {OpCodes.Ldarg_0.Value, OpCodes.Ldarg_0}
, {OpCodes.Ldarg_1.Value, OpCodes.Ldarg_1}
, {OpCodes.Ldarg_2.Value, OpCodes.Ldarg_2}
, {OpCodes.Ldarg_3.Value, OpCodes.Ldarg_3}
, {OpCodes.Ldloc_0.Value, OpCodes.Ldloc_0}
, {OpCodes.Ldloc_1.Value, OpCodes.Ldloc_1}
, {OpCodes.Ldloc_2.Value, OpCodes.Ldloc_2}
, {OpCodes.Ldloc_3.Value, OpCodes.Ldloc_3}
, {OpCodes.Stloc_0.Value, OpCodes.Stloc_0}
, {OpCodes.Stloc_1.Value, OpCodes.Stloc_1}
, {OpCodes.Stloc_2.Value, OpCodes.Stloc_2}
, {OpCodes.Stloc_3.Value, OpCodes.Stloc_3}
, {OpCodes.Ldarg_S.Value, OpCodes.Ldarg_S}
, {OpCodes.Ldarga_S.Value, OpCodes.Ldarga_S}
, {OpCodes.Starg_S.Value, OpCodes.Starg_S}
, {OpCodes.Ldloc_S.Value, OpCodes.Ldloc_S}
, {OpCodes.Ldloca_S.Value, OpCodes.Ldloca_S}
, {OpCodes.Stloc_S.Value, OpCodes.Stloc_S}
, {OpCodes.Ldnull.Value, OpCodes.Ldnull}
, {OpCodes.Ldc_I4_M1.Value, OpCodes.Ldc_I4_M1}
, {OpCodes.Ldc_I4_0.Value, OpCodes.Ldc_I4_0}
, {OpCodes.Ldc_I4_1.Value, OpCodes.Ldc_I4_1}
, {OpCodes.Ldc_I4_2.Value, OpCodes.Ldc_I4_2}
, {OpCodes.Ldc_I4_3.Value, OpCodes.Ldc_I4_3}
, {OpCodes.Ldc_I4_4.Value, OpCodes.Ldc_I4_4}
, {OpCodes.Ldc_I4_5.Value, OpCodes.Ldc_I4_5}
, {OpCodes.Ldc_I4_6.Value, OpCodes.Ldc_I4_6}
, {OpCodes.Ldc_I4_7.Value, OpCodes.Ldc_I4_7}
, {OpCodes.Ldc_I4_8.Value, OpCodes.Ldc_I4_8}
, {OpCodes.Ldc_I4_S.Value, OpCodes.Ldc_I4_S}
, {OpCodes.Ldc_I4.Value, OpCodes.Ldc_I4}
, {OpCodes.Ldc_I8.Value, OpCodes.Ldc_I8}
, {OpCodes.Ldc_R4.Value, OpCodes.Ldc_R4}
, {OpCodes.Ldc_R8.Value, OpCodes.Ldc_R8}
, {OpCodes.Dup.Value, OpCodes.Dup}
, {OpCodes.Pop.Value, OpCodes.Pop}
, {OpCodes.Jmp.Value, OpCodes.Jmp}
, {OpCodes.Call.Value, OpCodes.Call}
, {OpCodes.Calli.Value, OpCodes.Calli}
, {OpCodes.Ret.Value, OpCodes.Ret}
, {OpCodes.Br_S.Value, OpCodes.Br_S}
, {OpCodes.Brfalse_S.Value, OpCodes.Brfalse_S}
, {OpCodes.Brtrue_S.Value, OpCodes.Brtrue_S}
, {OpCodes.Beq_S.Value, OpCodes.Beq_S}
, {OpCodes.Bge_S.Value, OpCodes.Bge_S}
, {OpCodes.Bgt_S.Value, OpCodes.Bgt_S}
, {OpCodes.Ble_S.Value, OpCodes.Ble_S}
, {OpCodes.Blt_S.Value, OpCodes.Blt_S}
, {OpCodes.Bne_Un_S.Value, OpCodes.Bne_Un_S}
, {OpCodes.Bge_Un_S.Value, OpCodes.Bge_Un_S}
, {OpCodes.Bgt_Un_S.Value, OpCodes.Bgt_Un_S}
, {OpCodes.Ble_Un_S.Value, OpCodes.Ble_Un_S}
, {OpCodes.Blt_Un_S.Value, OpCodes.Blt_Un_S}
, {OpCodes.Br.Value, OpCodes.Br}
, {OpCodes.Brfalse.Value, OpCodes.Brfalse}
, {OpCodes.Brtrue.Value, OpCodes.Brtrue}
, {OpCodes.Beq.Value, OpCodes.Beq}
, {OpCodes.Bge.Value, OpCodes.Bge}
, {OpCodes.Bgt.Value, OpCodes.Bgt}
, {OpCodes.Ble.Value, OpCodes.Ble}
, {OpCodes.Blt.Value, OpCodes.Blt}
, {OpCodes.Bne_Un.Value, OpCodes.Bne_Un}
, {OpCodes.Bge_Un.Value, OpCodes.Bge_Un}
, {OpCodes.Bgt_Un.Value, OpCodes.Bgt_Un}
, {OpCodes.Ble_Un.Value, OpCodes.Ble_Un}
, {OpCodes.Blt_Un.Value, OpCodes.Blt_Un}
, {OpCodes.Switch.Value, OpCodes.Switch}
, {OpCodes.Ldind_I1.Value, OpCodes.Ldind_I1}
, {OpCodes.Ldind_U1.Value, OpCodes.Ldind_U1}
, {OpCodes.Ldind_I2.Value, OpCodes.Ldind_I2}
, {OpCodes.Ldind_U2.Value, OpCodes.Ldind_U2}
, {OpCodes.Ldind_I4.Value, OpCodes.Ldind_I4}
, {OpCodes.Ldind_U4.Value, OpCodes.Ldind_U4}
, {OpCodes.Ldind_I8.Value, OpCodes.Ldind_I8}
, {OpCodes.Ldind_I.Value, OpCodes.Ldind_I}
, {OpCodes.Ldind_R4.Value, OpCodes.Ldind_R4}
, {OpCodes.Ldind_R8.Value, OpCodes.Ldind_R8}
, {OpCodes.Ldind_Ref.Value, OpCodes.Ldind_Ref}
, {OpCodes.Stind_Ref.Value, OpCodes.Stind_Ref}
, {OpCodes.Stind_I1.Value, OpCodes.Stind_I1}
, {OpCodes.Stind_I2.Value, OpCodes.Stind_I2}
, {OpCodes.Stind_I4.Value, OpCodes.Stind_I4}
, {OpCodes.Stind_I8.Value, OpCodes.Stind_I8}
, {OpCodes.Stind_R4.Value, OpCodes.Stind_R4}
, {OpCodes.Stind_R8.Value, OpCodes.Stind_R8}
, {OpCodes.Add.Value, OpCodes.Add}
, {OpCodes.Sub.Value, OpCodes.Sub}
, {OpCodes.Mul.Value, OpCodes.Mul}
, {OpCodes.Div.Value, OpCodes.Div}
, {OpCodes.Div_Un.Value, OpCodes.Div_Un}
, {OpCodes.Rem.Value, OpCodes.Rem}
, {OpCodes.Rem_Un.Value, OpCodes.Rem_Un}
, {OpCodes.And.Value, OpCodes.And}
, {OpCodes.Or.Value, OpCodes.Or}
, {OpCodes.Xor.Value, OpCodes.Xor}
, {OpCodes.Shl.Value, OpCodes.Shl}
, {OpCodes.Shr.Value, OpCodes.Shr}
, {OpCodes.Shr_Un.Value, OpCodes.Shr_Un}
, {OpCodes.Neg.Value, OpCodes.Neg}
, {OpCodes.Not.Value, OpCodes.Not}
, {OpCodes.Conv_I1.Value, OpCodes.Conv_I1}
, {OpCodes.Conv_I2.Value, OpCodes.Conv_I2}
, {OpCodes.Conv_I4.Value, OpCodes.Conv_I4}
, {OpCodes.Conv_I8.Value, OpCodes.Conv_I8}
, {OpCodes.Conv_R4.Value, OpCodes.Conv_R4}
, {OpCodes.Conv_R8.Value, OpCodes.Conv_R8}
, {OpCodes.Conv_U4.Value, OpCodes.Conv_U4}
, {OpCodes.Conv_U8.Value, OpCodes.Conv_U8}
, {OpCodes.Callvirt.Value, OpCodes.Callvirt}
, {OpCodes.Cpobj.Value, OpCodes.Cpobj}
, {OpCodes.Ldobj.Value, OpCodes.Ldobj}
, {OpCodes.Ldstr.Value, OpCodes.Ldstr}
, {OpCodes.Newobj.Value, OpCodes.Newobj}
, {OpCodes.Castclass.Value, OpCodes.Castclass}
, {OpCodes.Isinst.Value, OpCodes.Isinst}
, {OpCodes.Conv_R_Un.Value, OpCodes.Conv_R_Un}
, {OpCodes.Unbox.Value, OpCodes.Unbox}
, {OpCodes.Throw.Value, OpCodes.Throw}
, {OpCodes.Ldfld.Value, OpCodes.Ldfld}
, {OpCodes.Ldflda.Value, OpCodes.Ldflda}
, {OpCodes.Stfld.Value, OpCodes.Stfld}
, {OpCodes.Ldsfld.Value, OpCodes.Ldsfld}
, {OpCodes.Ldsflda.Value, OpCodes.Ldsflda}
, {OpCodes.Stsfld.Value, OpCodes.Stsfld}
, {OpCodes.Stobj.Value, OpCodes.Stobj}
, {OpCodes.Conv_Ovf_I1_Un.Value, OpCodes.Conv_Ovf_I1_Un}
, {OpCodes.Conv_Ovf_I2_Un.Value, OpCodes.Conv_Ovf_I2_Un}
, {OpCodes.Conv_Ovf_I4_Un.Value, OpCodes.Conv_Ovf_I4_Un}
, {OpCodes.Conv_Ovf_I8_Un.Value, OpCodes.Conv_Ovf_I8_Un}
, {OpCodes.Conv_Ovf_U1_Un.Value, OpCodes.Conv_Ovf_U1_Un}
, {OpCodes.Conv_Ovf_U2_Un.Value, OpCodes.Conv_Ovf_U2_Un}
, {OpCodes.Conv_Ovf_U4_Un.Value, OpCodes.Conv_Ovf_U4_Un}
, {OpCodes.Conv_Ovf_U8_Un.Value, OpCodes.Conv_Ovf_U8_Un}
, {OpCodes.Conv_Ovf_I_Un.Value, OpCodes.Conv_Ovf_I_Un}
, {OpCodes.Conv_Ovf_U_Un.Value, OpCodes.Conv_Ovf_U_Un}
, {OpCodes.Box.Value, OpCodes.Box}
, {OpCodes.Newarr.Value, OpCodes.Newarr}
, {OpCodes.Ldlen.Value, OpCodes.Ldlen}
, {OpCodes.Ldelema.Value, OpCodes.Ldelema}
, {OpCodes.Ldelem_I1.Value, OpCodes.Ldelem_I1}
, {OpCodes.Ldelem_U1.Value, OpCodes.Ldelem_U1}
, {OpCodes.Ldelem_I2.Value, OpCodes.Ldelem_I2}
, {OpCodes.Ldelem_U2.Value, OpCodes.Ldelem_U2}
, {OpCodes.Ldelem_I4.Value, OpCodes.Ldelem_I4}
, {OpCodes.Ldelem_U4.Value, OpCodes.Ldelem_U4}
, {OpCodes.Ldelem_I8.Value, OpCodes.Ldelem_I8}
, {OpCodes.Ldelem_I.Value, OpCodes.Ldelem_I}
, {OpCodes.Ldelem_R4.Value, OpCodes.Ldelem_R4}
, {OpCodes.Ldelem_R8.Value, OpCodes.Ldelem_R8}
, {OpCodes.Ldelem_Ref.Value, OpCodes.Ldelem_Ref}
, {OpCodes.Stelem_I.Value, OpCodes.Stelem_I}
, {OpCodes.Stelem_I1.Value, OpCodes.Stelem_I1}
, {OpCodes.Stelem_I2.Value, OpCodes.Stelem_I2}
, {OpCodes.Stelem_I4.Value, OpCodes.Stelem_I4}
, {OpCodes.Stelem_I8.Value, OpCodes.Stelem_I8}
, {OpCodes.Stelem_R4.Value, OpCodes.Stelem_R4}
, {OpCodes.Stelem_R8.Value, OpCodes.Stelem_R8}
, {OpCodes.Stelem_Ref.Value, OpCodes.Stelem_Ref}
, {OpCodes.Ldelem.Value, OpCodes.Ldelem}
, {OpCodes.Stelem.Value, OpCodes.Stelem}
, {OpCodes.Unbox_Any.Value, OpCodes.Unbox_Any}
, {OpCodes.Conv_Ovf_I1.Value, OpCodes.Conv_Ovf_I1}
, {OpCodes.Conv_Ovf_U1.Value, OpCodes.Conv_Ovf_U1}
, {OpCodes.Conv_Ovf_I2.Value, OpCodes.Conv_Ovf_I2}
, {OpCodes.Conv_Ovf_U2.Value, OpCodes.Conv_Ovf_U2}
, {OpCodes.Conv_Ovf_I4.Value, OpCodes.Conv_Ovf_I4}
, {OpCodes.Conv_Ovf_U4.Value, OpCodes.Conv_Ovf_U4}
, {OpCodes.Conv_Ovf_I8.Value, OpCodes.Conv_Ovf_I8}
, {OpCodes.Conv_Ovf_U8.Value, OpCodes.Conv_Ovf_U8}
, {OpCodes.Refanyval.Value, OpCodes.Refanyval}
, {OpCodes.Ckfinite.Value, OpCodes.Ckfinite}
, {OpCodes.Mkrefany.Value, OpCodes.Mkrefany}
, {OpCodes.Ldtoken.Value, OpCodes.Ldtoken}
, {OpCodes.Conv_U2.Value, OpCodes.Conv_U2}
, {OpCodes.Conv_U1.Value, OpCodes.Conv_U1}
, {OpCodes.Conv_I.Value, OpCodes.Conv_I}
, {OpCodes.Conv_Ovf_I.Value, OpCodes.Conv_Ovf_I}
, {OpCodes.Conv_Ovf_U.Value, OpCodes.Conv_Ovf_U}
, {OpCodes.Add_Ovf.Value, OpCodes.Add_Ovf}
, {OpCodes.Add_Ovf_Un.Value, OpCodes.Add_Ovf_Un}
, {OpCodes.Mul_Ovf.Value, OpCodes.Mul_Ovf}
, {OpCodes.Mul_Ovf_Un.Value, OpCodes.Mul_Ovf_Un}
, {OpCodes.Sub_Ovf.Value, OpCodes.Sub_Ovf}
, {OpCodes.Sub_Ovf_Un.Value, OpCodes.Sub_Ovf_Un}
, {OpCodes.Endfinally.Value, OpCodes.Endfinally}
, {OpCodes.Leave.Value, OpCodes.Leave}
, {OpCodes.Leave_S.Value, OpCodes.Leave_S}
, {OpCodes.Stind_I.Value, OpCodes.Stind_I}
, {OpCodes.Conv_U.Value, OpCodes.Conv_U}
, {OpCodes.Prefix7.Value, OpCodes.Prefix7}
, {OpCodes.Prefix6.Value, OpCodes.Prefix6}
, {OpCodes.Prefix5.Value, OpCodes.Prefix5}
, {OpCodes.Prefix4.Value, OpCodes.Prefix4}
, {OpCodes.Prefix3.Value, OpCodes.Prefix3}
, {OpCodes.Prefix2.Value, OpCodes.Prefix2}
, {OpCodes.Prefix1.Value, OpCodes.Prefix1}
, {OpCodes.Prefixref.Value, OpCodes.Prefixref}
, {OpCodes.Arglist.Value, OpCodes.Arglist}
, {OpCodes.Ceq.Value, OpCodes.Ceq}
, {OpCodes.Cgt.Value, OpCodes.Cgt}
, {OpCodes.Cgt_Un.Value, OpCodes.Cgt_Un}
, {OpCodes.Clt.Value, OpCodes.Clt}
, {OpCodes.Clt_Un.Value, OpCodes.Clt_Un}
, {OpCodes.Ldftn.Value, OpCodes.Ldftn}
, {OpCodes.Ldvirtftn.Value, OpCodes.Ldvirtftn}
, {OpCodes.Ldarg.Value, OpCodes.Ldarg}
, {OpCodes.Ldarga.Value, OpCodes.Ldarga}
, {OpCodes.Starg.Value, OpCodes.Starg}
, {OpCodes.Ldloc.Value, OpCodes.Ldloc}
, {OpCodes.Ldloca.Value, OpCodes.Ldloca}
, {OpCodes.Stloc.Value, OpCodes.Stloc}
, {OpCodes.Localloc.Value, OpCodes.Localloc}
, {OpCodes.Endfilter.Value, OpCodes.Endfilter}
, {OpCodes.Unaligned.Value, OpCodes.Unaligned}
, {OpCodes.Volatile.Value, OpCodes.Volatile}
, {OpCodes.Tailcall.Value, OpCodes.Tailcall}
, {OpCodes.Initobj.Value, OpCodes.Initobj}
, {OpCodes.Constrained.Value, OpCodes.Constrained}
, {OpCodes.Cpblk.Value, OpCodes.Cpblk}
, {OpCodes.Initblk.Value, OpCodes.Initblk}
, {OpCodes.Rethrow.Value, OpCodes.Rethrow}
, {OpCodes.Sizeof.Value, OpCodes.Sizeof}
, {OpCodes.Refanytype.Value, OpCodes.Refanytype}
, {OpCodes.Readonly.Value, OpCodes.Readonly}

        };
        #endregion

        private static short GetOperandSize(OperandType type)
        {
            switch (type)
            {
                case OperandType.InlineBrTarget:
                case OperandType.InlineField:
                case OperandType.InlineI:
                case OperandType.InlineMethod:
                case OperandType.InlineSig:
                case OperandType.InlineString:
                case OperandType.InlineSwitch:
                case OperandType.InlineType:
                case OperandType.ShortInlineR:
                    return 4; // 32 bit
                case OperandType.InlineI8:
                    return 8;
                case OperandType.InlineR:
                    return 8;
                case OperandType.InlineVar:
                    return 2;
                case OperandType.ShortInlineBrTarget:
                case OperandType.ShortInlineI:
                case OperandType.ShortInlineVar:
                    return 1;
                case OperandType.InlineNone:
                    return 0;
                case OperandType.InlineTok:
                    return 4;
                default:
                    throw new NotImplementedException();
            }
        }

        public static List<ILInstruction> Parse(byte[] bytes)
        {
            List<ILInstruction> list = new List<ILInstruction>();

            if (null != bytes)
            {
                for (int i = 0; i < bytes.Length; i++)
                {
                    byte b = bytes[i];
                    short opCode = (short)b;
                    if (opCode == 0xFE)
                    {
                        opCode = (short)(bytes[++i] | 0xFE00);
                    }
                    OpCode op = OpCodeHash[opCode];
                    short operandSize = GetOperandSize(op.OperandType);
                    byte[] operandBytes = null;
                    if (operandSize > 0)
                    {
                        operandBytes = new byte[operandSize];
                        Array.Copy(bytes, i + 1, operandBytes, 0, operandSize);
                        i += operandSize;
                    }
                    ILInstruction il = new ILInstruction(op, operandBytes);

                    list.Add(il);
                    if (il.OpCode == OpCodes.Switch)
                    {
                        // This is a switch opcode. Need to skip N. Labels.
                        uint count = (uint) il.MetadataToken;
                        i += (int)(count * 4);
                    }

                }
            }

            return list;
        }
    }
}
