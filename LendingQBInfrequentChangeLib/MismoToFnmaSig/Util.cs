﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MismoToFnmaSig
{
	public abstract class Util
	{
		public static string SafeString(object obj)
		{
			return SafeString(obj, true);
		}
		public static string SafeString(object obj, bool trim)
		{
			try
			{
				if (Convert.IsDBNull(obj) || null == obj) return string.Empty;
				return (trim) ? Convert.ToString(obj).Trim() : Convert.ToString(obj);
			}
			catch
			{
				return string.Empty;
			}
		}
		public static int SafeInt(string input)
		{
			try
			{
				if (string.IsNullOrEmpty(input)) return 0;
				return Convert.ToInt32(input);
			}
			catch
			{
				return 0;
			}
		}
		public static int SafeInt(object obj)
		{
			try
			{
				if (Convert.IsDBNull(obj) || null == obj) return 0;
				return Convert.ToInt32(obj);
			}
			catch
			{
				return 0;
			}
		}		
		public static string Left(string input, int len)
		{
			if (string.IsNullOrEmpty(input)) return string.Empty;

			if (input.Length <= len)
				return input;
			else
				return input.Substring(0, len);
		}
		public static string Right(string input, int len)
		{
			if (string.IsNullOrEmpty(input)) return string.Empty;

			if (input.Length <= len)
				return input;
			else
				return input.Substring(input.Length - len, len);
		}

		public static string Mid(string input, int start)
		{
			if (string.IsNullOrEmpty(input) || start >= input.Length) 
				return string.Empty;
			return input.Substring(start);
		}

		public static string Mid(string input, int start, int len)
		{
			if (string.IsNullOrEmpty(input) || start >= input.Length)
				return string.Empty;

			if (len > (input.Length - start))
				return input.Substring(start);
			else
				return input.Substring(start, len);
		}		
	}

}
