﻿using System;
using System.IO;
using System.Xml;
using System.Text;

namespace MismoToFnmaSig
{

	/// <summary>
	/// Utility class to convert data from intermediate format to Fannie Mae's SIG format
	/// </summary>
	internal class CreditDataToSig
	{
		private XmlDocument m_xmlParsing;

		public CreditDataToSig(XmlDocument xmlParsingInstructions)
		{
			this.m_xmlParsing = xmlParsingInstructions;
		}

		public string Execute(string input)
		{
			XmlDocument xmlData = new XmlDocument();
			try
			{
				xmlData.LoadXml(input);
			}
			catch (Exception ex)
			{
				throw new ApplicationException("Failed to load data.", ex);
			}

			try
			{
				StringBuilder sb = new StringBuilder(1000);
				using (TextWriter writer = new StringWriter(sb))
				{
					this.ProcessSegments(xmlData.DocumentElement, m_xmlParsing.DocumentElement, writer);
				}
				return sb.ToString();
			}
			catch (Exception ex)
			{
				throw new ApplicationException("Failed to convert data.", ex);
			}
		}

		/// <summary>
		/// Process segments in the document.
		/// </summary>
		/// <param name="xmlData">Root element of the data document</param>
		/// <param name="xmlParsing">Root element of the parsing document</param>
		/// <param name="writer">TextWriter.</param>
		private void ProcessSegments(XmlElement xmlData, XmlElement xmlParsing, TextWriter writer)
		{
			foreach (XmlElement xmlParsingSeg in xmlParsing.SelectNodes("SEGMENT"))
			{
				string sSegName = Util.SafeString(xmlParsingSeg.GetAttribute("name"));
				if (sSegName.Length == 0) sSegName = "SEGMENT";
				foreach (XmlElement xmlSegData in xmlData.SelectNodes(sSegName))
				{
					string sID = xmlParsingSeg.GetAttribute("id");
					if (!string.IsNullOrEmpty(sID)) writer.Write(sID);

					this.ProcessFields(xmlSegData, xmlParsingSeg, writer);

					// Recursive call
					this.ProcessSegments(xmlSegData, xmlParsingSeg, writer);
				}
			}
		}

		/// <summary>
		/// Processes each data field in a segment.
		/// </summary>
		/// <param name="xmlData">Segment Data</param>
		/// <param name="xmlParsingSeg">Corresponding parsing element</param>
		/// <param name="writer">TextWriter.</param>
		private void ProcessFields(XmlElement xmlData, XmlElement xmlParsingSeg, TextWriter writer)
		{
			foreach (XmlElement xmlField in xmlParsingSeg.SelectNodes("FIELD"))
			{
				int nSize = Util.SafeInt(xmlField.GetAttribute("size"));
				string sFieldName = Util.SafeString(xmlField.GetAttribute("name"));
				string sFieldType = Util.SafeString(xmlField.GetAttribute("field_type"));
				if (sFieldType.Length == 0) sFieldType = "normal";
				switch (sFieldType)
				{
					case "normal":
						this.WriteField(xmlData.GetAttribute(sFieldName), nSize, writer);
						break;
					case "yn":
					case "default":
						this.WriteField(xmlField.GetAttribute("default_value"), nSize, writer);
						break;
				}
			}
			writer.WriteLine();
		}

		/// <summary>
		/// Writes a formatted segment.
		/// </summary>
		/// <param name="value">Field value to write.</param>
		/// <param name="len">Length of the field value.</param>
		/// <param name="writer">TextWriter.</param>
		private void WriteField(string value, int len, TextWriter writer)
		{
			writer.Write((Util.Left(Util.SafeString(value), len)).PadRight(len));
		}		
	}
}
