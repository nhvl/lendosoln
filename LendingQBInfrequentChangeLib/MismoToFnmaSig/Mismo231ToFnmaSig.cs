﻿using System;
using System.Text;
using System.Xml;
using System.Xml.Xsl;

namespace MismoToFnmaSig
{
	/// <summary>
	/// A utility class to convert reports from Mismo 2.3.1 to Fannie Mae's SIG format
	/// </summary>
	public class Mismo231ToFnmaSig : MismoToFnmaSig
	{
		private const string VALID_MISMO_VER_ID = "2.1;2.3;2.3.1";

		protected override string MismoVersionID { get { return VALID_MISMO_VER_ID; } }
		
		public Mismo231ToFnmaSig(string xsltStylesheetFile, string xmlParsingFile): base (xsltStylesheetFile, xmlParsingFile){}
	}
}
