﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;

namespace AuthTicket
{
    public abstract class AbstractXmlSerializable : IXmlSerializable
    {
        protected string ReadAttribute(XmlReader reader, string attrName)
        {
            string value = reader.GetAttribute(attrName);

            if (string.IsNullOrEmpty(value))
                return string.Empty;

            return value;
        }

        protected void WriteAttribute(XmlWriter writer, string attrName, string value)
        {
            if (string.IsNullOrEmpty(value))
                return;

            writer.WriteAttributeString(attrName, value);
        }

        #region IXmlSerializable Members

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public abstract void ReadXml(System.Xml.XmlReader reader);

        public abstract void WriteXml(System.Xml.XmlWriter writer);

        #endregion
    }
}