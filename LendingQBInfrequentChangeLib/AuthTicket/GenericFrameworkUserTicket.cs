﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;

namespace AuthTicket
{
    public class GenericFrameworkUserTicket : AbstractXmlSerializable
    {
        #region Private Member Variables
        private string m_encryptedTicket = string.Empty;
        private string m_siteCode = string.Empty;
        #endregion

        #region Public Properties
        public string EncryptedTicket
        {
            get { return m_encryptedTicket; }
            set { m_encryptedTicket = value; }
        }
        #endregion

        public GenericFrameworkUserTicket()
        {
            m_encryptedTicket = String.Empty;
            m_siteCode = string.Empty;
        }

        public GenericFrameworkUserTicket(string sTicket, string siteCode)
        {
            m_encryptedTicket = sTicket;
            m_siteCode = siteCode;
        }

        public override void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != "GENERIC_FRAMEWORK_USER_TICKET")
            {
                reader.Read();
                if (reader.EOF) return;
            }

            #region Read Attributes
            m_encryptedTicket = ReadAttribute(reader, "EncryptedTicket");
            m_siteCode = ReadAttribute(reader, "Site");
            #endregion

            if (reader.IsEmptyElement) return;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "GENERIC_FRAMEWORK_USER_TICKET") return;
            }
        }

        public override void WriteXml(XmlWriter writer)
        {
            // Authentication ticket is sometimes optional. If m_authTicket does not
            // contain a ticket, do not add it to the XML.
            if (m_encryptedTicket != String.Empty)
            {
                writer.WriteStartElement("GENERIC_FRAMEWORK_USER_TICKET");
                WriteAttribute(writer, "EncryptedTicket", m_encryptedTicket);
                WriteAttribute(writer, "Site", m_siteCode);
                writer.WriteEndElement();
            }
        }
    }
}