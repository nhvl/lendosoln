using System;
using System.Web;
using System.Xml;

namespace CommonLib
{
	public interface ISession
	{
		string this[int nIndex] { get; set; }
		string this[string sKey] { get; set; }
		int Count { get; }
		string SessionID { get; }
	}

	/// <summary>
	/// Summary description for FSession.
	/// </summary>
	public class FSession : IDisposable, ISession
	{
		XmlDocument m_xdData = new XmlDocument() ;
		string m_sRealID ;
		string m_sEncodedID ;
		static string m_sSessionPath ;
		const string DEFAULT_SESSION_PATH = @"c:\fsession\" ;
		bool m_bAbandoned = false ;
		bool m_bModified = false ;
		bool m_bPersistSession = true ;
		char [] m_HexTable = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' } ;

		public FSession(string sid)
		{
			throw new ApplicationException("This class is obsolete.  Please use MclObjProxy.FSession instead.");

//			m_sEncodedID = sid ;
//			m_sRealID = Decode(m_sEncodedID) ;
//			if (!LoadSession())
//				throw new FSessionException(FSessionException.ERROR_TYPE.SessionExpired) ;
		}
		public FSession(string sAppID, string sid, HttpResponse response)
		{
			throw new ApplicationException("This class is obsolete.  Please use MclObjProxy.FSession instead.");

//			sAppID = SafeString.Left(sAppID, 50) ;
//
//			m_sEncodedID = sid ;
//			m_sRealID = Decode(m_sEncodedID) ;
//			if (!LoadSession())
//				throw new FSessionException(FSessionException.ERROR_TYPE.SessionExpired) ;
//
//			response.Cookies[sAppID].Value = m_sEncodedID ;
		}
		public FSession(string sAppID, HttpRequest request, HttpResponse response)
		{
			throw new ApplicationException("This class is obsolete.  Please use MclObjProxy.FSession instead.");

//			sAppID = SafeString.Left(sAppID, 50) ;
//
//			if (null == request.Cookies[sAppID])
//				m_sEncodedID = "" ;
//			else
//				m_sEncodedID = request.Cookies[sAppID].Value ;
//
//			if (0 == m_sEncodedID.Length)
//			{
//				CreateSession() ;
//				response.Cookies[sAppID].Value = m_sEncodedID ;
//			}
//			else
//			{
//				m_sRealID = Decode(m_sEncodedID) ;
//				if (!LoadSession())
//				{
//					CreateSession() ;
//					response.Cookies[sAppID].Value = m_sEncodedID ;
//				}
//			}
//			LoadSession() ;
		}
		public string this[int nIndex]
		{
			get
			{
				if (nIndex < 1 || nIndex > this.Count)
					throw new ArgumentOutOfRangeException() ;
				return m_xdData.DocumentElement.ChildNodes[nIndex].InnerText ;
			}
			set
			{
				if (nIndex < 1 || nIndex > this.Count)
					throw new ArgumentOutOfRangeException() ;
				m_xdData.DocumentElement.ChildNodes[nIndex].InnerText = value ;

				m_bModified = true ;
			}
		}
		public string this[string sInKey]
		{
			// AD -- 2/25/03 -- fixed bug that didn't use ToUpper in set but did in get;
			//					and use local variable so calling string isn't changed
			get
			{
				System.Diagnostics.Debug.Assert(sInKey.Length > 0) ;
				string sKey = sInKey.ToUpper() ;

				XmlElement xe = (XmlElement)m_xdData.DocumentElement.SelectSingleNode(sKey) ;
				if (null == xe)
					return "" ;
				else
					return xe.InnerText ;
			}
			set
			{
				System.Diagnostics.Debug.Assert(sInKey.Length > 0) ;
				string sKey = sInKey.ToUpper();

				XmlElement xe = (XmlElement)m_xdData.DocumentElement.SelectSingleNode(sKey) ;
				if (null == xe)
				{
					xe = m_xdData.CreateElement(sKey) ;
					m_xdData.DocumentElement.AppendChild(xe) ;
				}
				xe.InnerText = value ;

				m_bModified = true ;
			}
		}
		public int Count
		{
			get { return m_xdData.DocumentElement.ChildNodes.Count ; }
		}
		public string SessionID
		{
			get { return m_sEncodedID ; }
		}
		public bool PersistSession
		{
			get { return m_bPersistSession ; }
			set { m_bPersistSession = value ; }
		}
		public void Dispose()
		{
			if (m_bModified && m_bPersistSession)
				m_xdData.Save(GetFileName()) ;
		}
		string Decode(string s)
		{
			System.Diagnostics.Debug.Assert(null != s, "String cannot be null.") ;
			System.Diagnostics.Debug.Assert(s.Length > 0, "String must be non zero.") ;
			System.Diagnostics.Debug.Assert(s.Length % 2 == 0, "String must be even length.") ;

			s = s.ToUpper() ;

			// verifies that the SID is valid
			int nHalfLen = s.Length / 4 ;
			string sRet = s.Substring(0, nHalfLen) + s.Substring(nHalfLen * 3) ;
			if (Encode(sRet) == s)
				return sRet ;
			else
			{
				CommonLib.Tracer.Trace("WAR", "s=" + s) ;
				CommonLib.Tracer.Trace("WAR", "Encode=" + Encode(sRet)) ;
				throw new FSessionException(FSessionException.ERROR_TYPE.InvalidSessionID) ;
			}
		}
		string Encode(string s)
		{
			System.Diagnostics.Debug.Assert(s.Length > 0, "String must be non zero.") ;
			System.Diagnostics.Debug.Assert(s.Length % 2 == 0, "String must be even length.") ;
			int nHalfLen = s.Length / 2 ;
			string sLeft = s.Substring(0, nHalfLen) ;
			string sRight= s.Substring(nHalfLen) ;
			char [] sKey = new char[s.Length] ;
			for (int i = 0 ; i < nHalfLen ; i++)
			{
				int n = sLeft[i] + sRight[i] - 48 ;
				sKey[i*2] = m_HexTable[(int)Math.Floor((double) (n / 16))] ;
				sKey[i*2+1] = m_HexTable[n % 16] ;

				// todo: this if block is here for backward compatibility with a buggy
				// VB version of FSESSION. Once we eliminate the buggy version, this
				// if block is no longer necessary
				if ('A' == sKey[i*2+1])
				{
					sKey[i*2] = '0' ;
					sKey[i*2+1] = '0' ;
				}
			}

			return sLeft + new String(sKey) + sRight ;
		}
		void CreateSession()
		{
			m_sRealID = System.Guid.NewGuid().ToString().Replace("-", "").ToUpper() ;
			m_sEncodedID = Encode(m_sRealID) ;
			m_xdData.LoadXml("<DATA/>") ;
		}
		bool LoadSession()
		{
			string sFileName = GetFileName() ;
			if (!System.IO.File.Exists(sFileName))
				return false ;

			try
			{
				m_xdData.Load(sFileName) ;
			}
			catch
			{
				return false ;
			}

			m_bAbandoned = this["__ABANDON"] == "T" ;
			return false == m_bAbandoned ;
		}
		string GetFileName()
		{
			if (null == m_sSessionPath || "" == m_sSessionPath)
			{
				Microsoft.Win32.RegistryKey key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"Software\MeridianLink\FSESSION") ;
				m_sSessionPath = (string)key.GetValue("PATH") ;
				if (null == m_sSessionPath || "" == m_sSessionPath)
					m_sSessionPath = DEFAULT_SESSION_PATH ;

				if (m_sSessionPath[m_sSessionPath.Length - 1] != '\\')
					m_sSessionPath += '\\' ;
			}
			return m_sSessionPath + m_sRealID + ".xml" ;
		}
	}
	public class FSessionException : Exception
	{
		public enum ERROR_TYPE { SessionExpired, InvalidSessionID } ;
	
		ERROR_TYPE m_eErrorType ;
		public FSessionException(ERROR_TYPE eErrorType)
		{
			m_eErrorType = eErrorType ;
		}
		public ERROR_TYPE ErrorType
		{
			get { return m_eErrorType ; }
		}
		public override string Message
		{
			get
			{
				switch(m_eErrorType)
				{
					case ERROR_TYPE.SessionExpired:
						return "Session expired" ;
					case ERROR_TYPE.InvalidSessionID:
						return "The session id is tainted" ;
					default:
						return base.Message ;
				}
			}
		}
	}
}
