using System;
using System.IO;
using System.Text;
using System.Xml;

namespace CommonLib
{
	/// <summary>
	/// Utility for hex and base64 encoding
	/// </summary>
	public class BinEncode
	{
		static public void ToHex(string toEncode, XmlElement elem)
		{
			elem.Value = ToHex(toEncode);
		}

		static public void ToHex(byte[] toEncode, XmlElement elem)
		{
			elem.Value = ToHex(toEncode);
		}

		static public string ToHex(string toEncode)
		{
			UnicodeEncoding uenc = new UnicodeEncoding();
			byte[] bytes = uenc.GetBytes(toEncode);

			return ToHex(bytes);
		}

		static public string ToHex(byte[] toEncode)
		{
			MemoryStream stream;
			XmlTextWriter writer;
			BeginTo(toEncode.Length, out stream, out writer);
			writer.WriteBinHex(toEncode, 0, toEncode.Length);
			return EndTo(stream, writer);
		}

		static public byte[] FromHex(XmlElement elem)
		{
			string name = elem.LocalName;
			return FromHexImpl(elem.OuterXml, name);
		}

		static public byte[] FromHex(string encoded)
		{
			string xml = "<doc>" + encoded + "</doc>";
			return FromHexImpl(xml, "doc");
		}

		static public void ToBase64(string toEncode, XmlElement elem)
		{
			elem.InnerText = ToBase64(toEncode);
		}

		static public void ToBase64Ascii(string toEncode, XmlElement elem)
		{
			elem.InnerText = ToBase64Ascii(toEncode);
		}

		static public void ToBase64(byte[] toEncode, XmlElement elem)
		{
			elem.InnerText = ToBase64(toEncode);
		}

		static public string ToBase64(string toEncode)
		{
			UnicodeEncoding uenc = new UnicodeEncoding();
			byte[] bytes = uenc.GetBytes(toEncode);

			return ToBase64(bytes);
		}

		static public string ToBase64Ascii(string toEncode)
		{
			ASCIIEncoding aenc = new ASCIIEncoding();
			byte[] bytes = aenc.GetBytes(toEncode);

			return ToBase64(bytes);
		}
/*
		static public string ToBase64(byte[] toEncode)
		{
			MemoryStream stream;
			XmlTextWriter writer;
			BeginTo(toEncode.Length, out stream, out writer);
			writer.WriteBase64(toEncode, 0, toEncode.Length);
			return EndTo(stream, writer);
		}
*/
		static public string ToBase64(byte[] toEncode)
		{
			return Convert.ToBase64String(toEncode);
		}

		static public byte[] FromBase64(XmlElement elem)
		{
			return FromBase64(elem.InnerText);
			//string name = elem.LocalName;
			//return FromBase64Impl(elem.OuterXml, name);
		}
/*
		static public byte[] FromBase64(string encoded)
		{
			string xml = "<doc>" + encoded + "</doc>";
			return FromBase64Impl(xml, "doc");
		}
*/
		static public byte[] FromBase64(string encoded)
		{
			return Convert.FromBase64String(encoded);
		}

		static public string StringFromBytes(byte[] bytes)
		{
			UnicodeEncoding uenc = new UnicodeEncoding();
			return uenc.GetString(bytes);
		}

		static public string StringFromAsciiBytes(byte[] bytes)
		{
			ASCIIEncoding asciiEnc = new ASCIIEncoding();
			return asciiEnc.GetString(bytes);
		}

		static private void BeginTo(int size, out MemoryStream stream, out XmlTextWriter writer)
		{
			stream = new MemoryStream(size);
			writer = new XmlTextWriter(stream, new System.Text.UTF8Encoding());
			writer.WriteStartDocument(true);
			writer.WriteStartElement("doc");
		}

		static private string EndTo(MemoryStream stream, XmlTextWriter writer)
		{
			writer.WriteEndElement();
			writer.WriteEndDocument();
			writer.Close();

			stream = new MemoryStream(stream.GetBuffer());
			XmlTextReader reader = new XmlTextReader(stream);

			while (reader.Read())
			{
				if ("doc" == reader.Name) break;
			}
			return reader.ReadString();
		}

		static private byte[] FromHexImpl(string xml, string elemName)
		{
			byte[] output;
			XmlTextReader reader;
			GetPreparedReaderAndBuffer(xml, elemName, out reader, out output);
			int read = reader.ReadBinHex(output, 0, output.Length);

			byte[] ret = new byte[read];
			Array.Copy(output, 0, ret, 0, read);
			return ret;
		}
/*
		static private byte[] FromBase64Impl(string xml, string elemName)
		{
			byte[] output;
			XmlTextReader reader;
			GetPreparedReaderAndBuffer(xml, elemName, out reader, out output);
			int read = reader.ReadBase64(output, 0, output.Length);

			byte[] ret = new byte[read];
			Array.Copy(output, 0, ret, 0, read);
			return ret;
		}

		static private byte[] FromBase64ImplAscii(string xml, string elemName)
		{
			byte[] output;
			XmlTextReader reader;
			GetPreparedReaderAndBufferAscii(xml, elemName, out reader, out output);
			int read = reader.ReadBase64(output, 0, output.Length);

			byte[] ret = new byte[read];
			Array.Copy(output, 0, ret, 0, read);
			return ret;
		}
*/
		private static void GetPreparedReaderAndBuffer(string xml, string elemName,
			out XmlTextReader reader, out byte[] output)
		{
			UTF8Encoding uenc = new UTF8Encoding();
			byte[] bytes = uenc.GetBytes(xml);

			int size = bytes.Length; // should be more than enough
			MemoryStream stream = new MemoryStream(bytes);
			stream.Seek(0, SeekOrigin.Begin);
			output = new byte[size];
			reader = new XmlTextReader(stream);

			while (reader.Read())
			{
				if (elemName == reader.Name) break;
			}
		}
/*
		private static void GetPreparedReaderAndBufferAscii(string xml, string elemName,
			out XmlTextReader reader, out byte[] output)
		{
			ASCIIEncoding aenc = new ASCIIEncoding();
			byte[] bytes = aenc.GetBytes(xml);

			int size = bytes.Length; // should be more than enough
			MemoryStream stream = new MemoryStream(bytes);
			stream.Seek(0, SeekOrigin.Begin);
			output = new byte[size];
			reader = new XmlTextReader(stream);

			while (reader.Read())
			{
				if (elemName == reader.Name) break;
			}
		}
*/
	}
}
