using System;
using System.Security.Cryptography;
using System.IO;
using System.Text;

namespace CommonLib
{
	/// <summary>
	/// Summary description for CryptoUtil.
	/// </summary>
	public class CryptoUtil : IDisposable
	{
		private SymmetricAlgorithm m_CryptoService ;
		private byte[] m_arrKey ;
		private string m_sKey = "eo+HtBPE5tnsxBD9" ;
		private bool m_bDisposed;
		
		public string Key
		{
			get
			{
				if (this.m_bDisposed)
					throw new ObjectDisposedException(GetType().ToString());

				return (m_sKey);
			}
			set 
			{
				if (this.m_bDisposed)
					throw new ObjectDisposedException(GetType().ToString());

				if (value.Length > 16) 
				{
					m_sKey = value.Substring(0, 16) ;
				}	
				else if (value.Length > 0) 
				{
					m_sKey = value ;
				}	
				m_arrKey = GetLegalKey(m_sKey) ;
			} 
		}
		public CryptoUtil()
		{
			m_CryptoService = new RijndaelManaged() ;
			m_arrKey = GetLegalKey(m_sKey) ;
		}
		public CryptoUtil(string sKey)
		{
			m_CryptoService = new RijndaelManaged() ;
			this.Key = sKey ;
		}
		~CryptoUtil()
		{
			Dispose(false);
		}
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
		private void Dispose(bool disposing)
		{
			if (!this.m_bDisposed)
			{
				if (disposing)
				{
					// Apparently the implementation is explicitely via IDisposable:Dispose() !
					IDisposable idisp = this.m_CryptoService as IDisposable;
					if (idisp != null) idisp.Dispose();
				}
			}
			this.m_bDisposed = true;
		}
		public string Encrypt(string sSrc)
		{
			if (this.m_bDisposed)
				throw new ObjectDisposedException(GetType().ToString());

			try
			{
				byte[] arrIn = System.Text.ASCIIEncoding.ASCII.GetBytes(sSrc) ;

				m_CryptoService.Key = m_arrKey ;
				m_CryptoService.IV = m_arrKey ;

				System.IO.MemoryStream oMemStream = new System.IO.MemoryStream() ;
				
				ICryptoTransform oEncryptor = m_CryptoService.CreateEncryptor() ;

				CryptoStream oCryptoStream = new CryptoStream(oMemStream, oEncryptor, CryptoStreamMode.Write) ;
				oCryptoStream.Write(arrIn, 0, arrIn.Length) ;
				oCryptoStream.FlushFinalBlock() ;
				oCryptoStream.Flush() ;
				
				byte[] arrOut = oMemStream.ToArray() ;
				
				oCryptoStream.Close() ;
				oMemStream.Close() ;
				
				return System.Convert.ToBase64String(arrOut, 0, arrOut.Length) ;
			}
			catch
			{
				return (sSrc) ;
			}
		}

		public string Decrypt(string sEncrypted)
		{
			if (this.m_bDisposed)
				throw new ObjectDisposedException(GetType().ToString());

			if (sEncrypted.Trim().Length == 0) return (sEncrypted) ;
			try
			{
				byte[] arrIn = System.Convert.FromBase64String(sEncrypted) ;

				m_CryptoService.Key = m_arrKey ;
				m_CryptoService.IV = m_arrKey ;
				
				System.IO.MemoryStream oMemStream = new System.IO.MemoryStream(arrIn, 0, arrIn.Length) ;

				ICryptoTransform oDecryptor = m_CryptoService.CreateDecryptor() ;

				CryptoStream oCryptoStream = new CryptoStream(oMemStream, oDecryptor, CryptoStreamMode.Read) ;
				System.IO.StreamReader oStreamReader = new System.IO.StreamReader(oCryptoStream) ;
				
				string ret = oStreamReader.ReadToEnd() ;
				
				oMemStream.Close() ;
				oStreamReader.Close() ;
				
				return (ret) ;
			}
			catch
			{
				return (sEncrypted) ;
			}	
		}
		private byte[] GetLegalKey(string sKey)
		{
			if (this.m_bDisposed)
				throw new ObjectDisposedException(GetType().ToString());

			string sTemp ;
			if (m_CryptoService.LegalKeySizes.Length > 0)
			{
				int Size1 = 0 ;
				int Size2 = m_CryptoService.LegalKeySizes[0].MinSize ;

				while (sKey.Length * 8 > Size2)
				{
					Size1 = Size2 ;
					Size2 += m_CryptoService.LegalKeySizes[0].SkipSize ;
				}
				sTemp = Key.PadRight(Size2 / 8, ' ') ;
			}
			else
				sTemp = sKey ;

			return ASCIIEncoding.ASCII.GetBytes(sTemp) ;
		}

		static public String CreateKey(int numBytes)
		{
			RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
			byte[] buff = new byte[numBytes];

			rng.GetBytes(buff);
			return BytesToHexString(buff);
		}

		static public String BytesToHexString(byte[] bytes)
		{
			StringBuilder hexString = new StringBuilder(64);

			for (int counter = 0; counter < bytes.Length; counter++)
			{
				hexString.Append(String.Format("{0:X2}", bytes[counter]));
			}
			return hexString.ToString();
		}

	}
	/// <summary>
	/// Hack class to encrypt/decrypt the UserData stored in a temporary cookie
	/// </summary>
	public class UserDataCrypto
	{
		static public string Encode(string s)
		{
			/*
			return Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(GetParityNumber(s) + s)) ;
			*/
			using (CryptoUtil util = new CryptoUtil())
			{
				return util.Encrypt(s) ;
			}
		}
		static public string Decode(string s)
		{
			/*
			string s = System.Text.ASCIIEncoding.ASCII.GetString(Convert.FromBase64String(s)) ;
			int nPos = s.IndexOf("<", 0) ;
			int nParityNumber = SafeConvert.ToInt(s.Substring(0, nPos+1)) ;
			string s = s.Substring(nPos) ;
			if (nParityNumber == GetParityNumber(s))
				return s ;
			else
				throw new ApplicationException("Decoding failed.") ;
			*/
			using (CryptoUtil util = new CryptoUtil())
			{
				return util.Decrypt(s) ;
			}
		}
		/*
		static int GetParityNumber(string s)
		{
			int n = 0 ;
			foreach (char c in s)
				n += (int)c ;
			return n % 12345 ;
		}
			*/
	}
}
