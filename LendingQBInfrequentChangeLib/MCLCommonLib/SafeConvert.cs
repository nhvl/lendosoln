using System;

namespace CommonLib
{
	public class SafeConvert
	{
		public static int ToInt(string s)
		{
			try
			{
				if ((null == s) || ("" == s)) return 0;
				return Convert.ToInt32(s);
			}
			catch
			{
				return 0;
			}
		}
		public static int ToInt(object s)
		{
			try
			{
				if (Convert.IsDBNull(s) || null == s) return 0 ;
				return Convert.ToInt32(s) ;
			}
			catch
			{
				return 0 ;
			}
		}
		public static int ToLong(object s)
		{
			return ToInt(s);
		}
		public static string ToString(object s)
		{
			return SafeConvert.ToString(s, true);
		}
		public static string ToString(object s, bool bTrim)
		{
			try
			{
				if (Convert.IsDBNull(s) || null == s) return "" ;
				return (bTrim) ? Convert.ToString(s).Trim() : Convert.ToString(s);
			}
			catch
			{
				return "" ;
			}
		}
		public static string ToHTMLString(object s)
		{
			try
			{
				if (Convert.IsDBNull(s) || null == s) return "&nbsp;" ;
				string str = Convert.ToString(s).Trim() ;
				if ( "" == str )
					return "&nbsp;" ;
				else
					return str.Replace("<", "&lt;").Replace(">", "&gt;").Replace("&", "&amp;") ;
			}
			catch
			{
				return "" ;
			}
		}
		public static double ToDouble(object s)
		{
			try
			{
				if (Convert.IsDBNull(s) || null == s) return 0 ;
				return Convert.ToDouble(s) ;
			}
			catch
			{
				return 0 ;
			}
		}
		public static System.DateTime ToDateTime(object s)
		{
			try
			{
				if (Convert.IsDBNull(s) || null == s) return DateTime.MinValue ;
				return (Convert.ToDateTime(s));
			}
			catch(ArgumentOutOfRangeException)
			{
				return DateTime.MaxValue ;
			}
			catch
			{
				return DateTime.MinValue ;
			}
		}
		public static System.DateTime ToDateTime(object s, string format)
		{
			try
			{
				if (Convert.IsDBNull(s) || null == s) 
					return (new DateTime(0)) ;
				else
				{	
					string temp = Convert.ToString(s).Trim() ;
					
					if (temp.Length != format.Length)
						return (new DateTime(0)) ;
					
					switch (format)
					{
						case "yyyyMMdd":
							return (Convert.ToDateTime(SafeString.Mid(temp, 4, 2) + "/" + SafeString.Right(temp, 2) + "/" + SafeString.Left(temp, 4))) ;
						case "MMddyyyy":
							return (Convert.ToDateTime(SafeString.Left(temp, 2) + "/" + SafeString.Mid(temp, 2, 2) + "/" + SafeString.Right(temp, 4))) ;
						case "yyyyMM" :
							return (Convert.ToDateTime(SafeString.Right(temp,2) + "/01/" + SafeString.Left(temp, 4))) ;
							// return (Convert.ToDateTime("01/" + SafeString.Right(temp, 1) + "/" + SafeString.Left(temp, 4))) ;
						case "MM/yyyy":
						case "MMyyyy":	
							return (Convert.ToDateTime(SafeString.Left(temp, 2) + "/01/" + SafeString.Right(temp,4))) ;
						default:
							return (Convert.ToDateTime(s)) ;
					}
				}
			}
			catch
			{
				return (new DateTime(0)) ;
			}
		}
		public static double MoneyStringToDouble(string s)
		{
			if (Convert.IsDBNull(s) || null == s)
				return 0 ;

			s = s.Replace("$", "") ;
			if (0 == s.Trim().Length)
				return 0 ;
				
			if (s[0] == '(' && s[s.Length-1] == ')')
				return -Convert.ToDouble(s.Substring(1, s.Length-2)) ;  // convert (7.00) to -7.00
			else
				return Convert.ToDouble(s) ;
		}
		public static Guid ToGuid(object o)
		{
			if (Convert.IsDBNull(o) || o == null)
				return Guid.Empty ;
			else
				return new Guid(o.ToString()) ;
		}
		public static string DBUniqueIDToDataKey(string uniqueID)
		{
			if ((uniqueID == null) || (uniqueID.Length != 36)) return string.Empty;
			try
			{
				Guid guid = new Guid(uniqueID);
			}
			catch
			{
				return string.Empty;
			}
			return uniqueID.Replace("-", string.Empty);
		}
		public static string DataKeyToDBUniqueID(string dataKey)
		{
			if ((dataKey == null) || (dataKey.Length != 32)) return string.Empty;
			string sGuid = string.Format("{0}-{1}-{2}-{3}-{4}", dataKey.Substring(0, 8), dataKey.Substring(8, 4), dataKey.Substring(12, 4), dataKey.Substring(16, 4), dataKey.Substring(20, 12));
			string sDataKey = DBUniqueIDToDataKey(sGuid);
			return (sDataKey.Length == 0) ? string.Empty : sGuid;
		}
	}
}
