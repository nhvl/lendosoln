using System;

namespace CommonLib
{
	/// <summary>
	/// Summary description for RiskScoreModelUtil.
	/// </summary>
	public class RiskScoreModelUtil
	{
		public RiskScoreModelUtil()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		public static S_ScoreRange GetScoreRange(string sBureauName, string sScoreModelCode)
		{
			S_ScoreRange oRange = new S_ScoreRange();
			switch(sBureauName)
			{
				case "EF":
					#region EFSELECT
				switch(sScoreModelCode)
				{
					case "01839":
					case "02470":
					case "02458":
					case "01838":
					case "01883":
					case "02957":
					case "E03":
						oRange.LowerBound = 0;
						oRange.UpperBound = 999;
						break;
					case "02502":
					case "02503":
					case "02783":
					case "02784":
					case "02718":
					case "02479":
						oRange.LowerBound = 1;
						oRange.UpperBound = 300;
						break;
					case "02781":
					case "02872":
						oRange.LowerBound = 1;
						oRange.UpperBound = 600;
						break;
					case "02725":
					case "02682":
					case "02875":
					case "02481":
					case "02505":
					case "02476":
					case "02538":
					case "01203":
						oRange.LowerBound = 1;
						oRange.UpperBound = 999;
						break;
					case "02514":
						oRange.LowerBound = 100;
						oRange.UpperBound = 400;
						break;
					case "02778":
						oRange.LowerBound = 100;
						oRange.UpperBound = 999;
						break;
					case "02825":
					case "02906":
						oRange.LowerBound = 150;
						oRange.UpperBound = 950;
						break;
					case "K":
					case "306":
					case "L":
					case "M":
					case "308":
					case "N":
					case "307":
					case "A":
					case "F":
					case "I":
					case "B":
					case "1":
					case "301":
					case "2":
					case "302":
					case "5":
					case "303":
					case "7":
					case "U":
					case "R":
					case "P":
					case "02803":
					case "02964":
						oRange.LowerBound = 250;
						oRange.UpperBound = 900;
						break;
					case "02874":
					case "01204":
						oRange.LowerBound = 280;
						oRange.UpperBound = 850;
						break;
					case "J":
					case "E01":
					case "Z":
					case "E":
					case "E04":
					case "6":
					case "304":
					case "305":
					case "Y":
					case "*":
					case "#":
					case "02801":
					case "02958":
						oRange.LowerBound = 300;
						oRange.UpperBound = 850;
						break;
					case "02527":
					case "02558":
					case "02599":
					case "02557":
						oRange.LowerBound = 350;
						oRange.UpperBound = 950;
						break;
					case "02978" :
					case "02991" :
						oRange.LowerBound = 501 ;
						oRange.UpperBound = 990 ;
						break ;
					case "02485":
						oRange.LowerBound = 999;
						oRange.UpperBound = 1;
						break;
					case "DAS":
					case "DASE":
					case "DASF":
						oRange.LowerBound = 1000;
						oRange.UpperBound = 1;
						oRange.NeedPlus = true;
						break;
					case "02912":
						throw new RiskScoreModelException("SubPro is a strongly tiered model that does not use a score range.") ;
					case "FV" :
						oRange.LowerBound = 10 ;
						oRange.UpperBound = 50 ;
						break ;
					case "FV2" :
						oRange.LowerBound = 10 ;
						oRange.UpperBound = 90 ;
						break ;
					case "02635" :
					case "03015" :
						oRange.LowerBound = 0 ;
						oRange.UpperBound = 200 ;
						break ;
					default:
						Tracer.Trace("CommonLib", "RiskScoreModelUtil.cs", "EF", "ERR",  "Unknown EF Model Code:" + sScoreModelCode, 136);
						throw new RiskScoreModelException("Unknown EF Model:" + sScoreModelCode);
				}
					#endregion
					break;
				case "XP":
					#region XPSELECT
				switch(sScoreModelCode)
				{
					case "RB":
					case "O":
						oRange.LowerBound = 0;
						oRange.UpperBound = 9;
						break;
					case "P":
					// case "Q":
					case "+":
						oRange.LowerBound = 0;
						oRange.UpperBound = 999;
						break;
					case "D":
					case "U":
					case "W":
					case "X":
					case "L":
					case "E":
					case "5":
					case "2":
					case "1":
						oRange.LowerBound = 1;
						oRange.UpperBound = 999;
						break;
					case "RA":
					case "0":
					case "G":
						oRange.LowerBound = 100;
						oRange.UpperBound = 400;
						break;
					case "FM":
					case "&":
					case "FS":
					case "!":
					case "FP":
					case "$":
					case "FN":
					case "#":
					case "F3":
					case "\"":
					case "F4":
					case "%":
						oRange.LowerBound = 100;
						oRange.UpperBound = 999;
						break;
					case "M":
						oRange.LowerBound = 150;
						oRange.UpperBound = 900;
						break;
					case "M2":
					case "*":
						oRange.LowerBound = 150;
						oRange.UpperBound = 950;
						break;
					case "V":
					case "N":
					case "R":
					case "K":
					case "AB":
					case "AC":
					case "AD":
					case "AE" :
						oRange.LowerBound = 250;
						oRange.UpperBound = 900;
						break;
					case "FB" :
						oRange.LowerBound = 250;
						oRange.UpperBound = 950;
						break;
					case "F":
					case "T02":
					case "I":
					case "AA":
						oRange.LowerBound = 300;
						oRange.UpperBound = 850;
						break;
					case "SP":
					case "[":
					case "S1":
						oRange.LowerBound = 330;
						oRange.UpperBound = 830;
						break;
					case "4":
					case "T04":
						oRange.LowerBound = 360;
						oRange.UpperBound = 840;
						break;
					case "8":
						oRange.LowerBound = 370;
						oRange.UpperBound = 850;
						break;
					case "9":
						oRange.LowerBound = 370;
						oRange.UpperBound = 950;
						break;
					case "6":
						oRange.LowerBound = 390;
						oRange.UpperBound = 860;
						break;
					case "H":
					case "J":
						oRange.LowerBound = 400;
						oRange.UpperBound = 800;
						break;
					case "7":
						oRange.LowerBound = 400;
						oRange.UpperBound = 870;
						break;
					case "3":
					case "T03":
					case "A":
						oRange.LowerBound = 1000;
						oRange.UpperBound = 0;
						break;
					case "B":
					case "T09":
						oRange.LowerBound = 1200;
						oRange.UpperBound = 108;
						oRange.NeedPlus = true;
						break;
					case "BP" :
						oRange.LowerBound = 1400 ;
						oRange.UpperBound = 1 ;
						break ;
					case "Q" :	// 11/16/2006- VantageScore, OPM 8318
						oRange.LowerBound = 501 ;
						oRange.UpperBound = 990 ;
						break ;
					default:
						Tracer.Trace("CommonLib", "RiskScoreModelUtil.cs", "XP", "ERR",  "Unknown XP Model Code:" + sScoreModelCode, 261);
						throw new RiskScoreModelException("Unknown XP Model:" + sScoreModelCode);
				}
					#endregion
					break;
				case "TU":
					#region TUSELECT
				switch(sScoreModelCode)
				{					
					case "00337":
					case "00896":
						oRange.LowerBound = 0;
						oRange.UpperBound = 999;
						break;
					case "00R97":
						oRange.LowerBound = 12;
						oRange.UpperBound = 836;
						break;
					case "00315":
					case "207":
					case "00R13":
						oRange.LowerBound = 37;
						oRange.UpperBound = 823;
						break;
					case "00219":
					case "00227":
					case "00730":
					case "00R59":
					case "00R82":
						oRange.LowerBound = 150;
						oRange.UpperBound = 950;
						break;
					case "00004":
					case "00008":
					case "00016":
					case "00256":
					case "00990":
					case "00991":
					case "00992":
					case "00993":
					case "00P11":
					case "00P12":
					case "00P13":
					case "00P14":
					case "204":
					case "U04":
					case "U05":
					case "U06":
						oRange.LowerBound = 250;
						oRange.UpperBound = 900;
						break;
					case "00002":
					case "00950":
					case "00P02":
					case "213":
					case "U03":
					case "P02":
						oRange.LowerBound = 300;
						oRange.UpperBound = 850;
						break;
					case "00701":
						oRange.LowerBound = 300;
						oRange.UpperBound = 900;
						break;
					case "00601":
					case "00X46":
						oRange.LowerBound = 350;
						oRange.UpperBound = 900;
						break;
					case "00032":
					case "U02":
					case "00501":
						oRange.LowerBound = 1879;
						oRange.UpperBound = 0;
						break;
					case "00P16": 
					case "00P17": 
					case "00P18": 
					case "00P23":
						oRange.LowerBound = 350;
						oRange.UpperBound = 850;
						break;
					case "00P94":
						oRange.LowerBound = 501;
						oRange.UpperBound = 990;
						break;
					case "00662":
						oRange.LowerBound = 999 ;
						oRange.UpperBound = 0 ;
						break ;
					default:
						Tracer.Trace("CommonLib", "RiskScoreModelUtil.cs", "TU", "ERR",  "Unknown TU Model Code:" + sScoreModelCode, 344);
						throw new RiskScoreModelException("Unknown TU Model:" + sScoreModelCode);
				}
					#endregion
					break;
				case "PRBC" :
				{
					// 07/03/2006-Lawrence- Add PRBC BPS score model
					oRange.LowerBound = 300;
					oRange.UpperBound = 850;
					break ;
				}
			}
			//return aiScoreRange;
			return oRange;
		}
	}
	public struct S_ScoreRange
	{
		public int UpperBound;
		public int LowerBound;
		public bool NeedPlus;
	}
	public class RiskScoreModelException:System.Exception
	{
		public RiskScoreModelException(string sErrMsg):base(sErrMsg){}
		public RiskScoreModelException(string sErrMsg, System.Exception inner):base(sErrMsg, inner){}
	}
}
