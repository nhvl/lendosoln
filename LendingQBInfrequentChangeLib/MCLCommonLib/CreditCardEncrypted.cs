// Copyright 2000-2005 MeridianLink, Inc.  All Rights Reserved.
using System;
using System.Text;
using System.Runtime.Serialization;

namespace CommonLib
{
	/// <summary>
	/// Permit code to work with either encrypted or unencrypted credit card data.
	/// </summary>
	[Serializable]
	public class CreditCardHolder : ISerializable
	{
		public const double Version = 2.0;

		private CreditCard m_normalCard;
		private CreditCardEncrypted m_encryptedCard; // defined below

		protected CreditCardHolder(SerializationInfo info, StreamingContext context)
		{
			double dVersion = info.GetDouble("version");

			this.m_normalCard = (CreditCard)info.GetValue("normalCard", typeof(CreditCard));
			this.m_encryptedCard = (CreditCardEncrypted)info.GetValue("encryptedCard", typeof(CreditCardEncrypted));
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("version", CreditCardHolder.Version);

			info.AddValue("normalCard", this.m_normalCard);
			info.AddValue("encryptedCard", this.m_encryptedCard);
		}

		public CreditCardHolder(CreditCard card, CreditCardEncrypted enCard)
		{
			if ((card == null) && (enCard == null)) throw new ApplicationException("NULL Credit Card");

			this.m_normalCard = card;
			this.m_encryptedCard = enCard;
		}

		public CreditCard UnEncryptedCard
		{
			get {return this.m_normalCard;}
		}

		public CreditCardEncrypted EncryptedCard
		{
			get {return this.m_encryptedCard;}
		}

		public string FirstName
		{
			get
			{
				return (this.m_encryptedCard != null) ? this.m_encryptedCard.FirstName : this.m_normalCard.FirstName;
			}
		}

		public string LastName
		{
			get
			{
				return (this.m_encryptedCard != null) ? this.m_encryptedCard.LastName : this.m_normalCard.LastName;
			}
		}

		public string Name
		{
			get
			{
				return (this.m_encryptedCard != null) ? this.m_encryptedCard.Name : this.m_normalCard.Name;
			}
		}

		public string UnEncryptedNumber
		{
			get
			{
				if ((this.m_encryptedCard != null) && (SafeString.HasValue(this.m_encryptedCard.UnencryptedCardNumber)))
					return this.m_encryptedCard.UnencryptedCardNumber;
				else return (this.m_normalCard != null) ? this.m_normalCard.Number : null;
			}
		}

		public string EncryptedNumber
		{
			get
			{
				return (this.m_encryptedCard != null) ? this.m_encryptedCard.Number : null;
			}
		}

		public string CVV
		{
			get
			{
				return (this.m_encryptedCard != null) ? this.m_encryptedCard.CVV : this.m_normalCard.CVV;
			}
		}

		public int ExpMonth
		{
			get
			{
				return (this.m_encryptedCard != null) ? this.m_encryptedCard.ExpMonth : this.m_normalCard.ExpMonth;
			}
		}

		public int ExpYear
		{
			get
			{
				return (this.m_encryptedCard != null) ? this.m_encryptedCard.ExpYear : this.m_normalCard.ExpYear;
			}
		}

		public SimpleAddress Address
		{
			get
			{
				return (this.m_encryptedCard != null) ? this.m_encryptedCard.Address : this.m_normalCard.Address;
			}

			set
			{
				if (this.m_normalCard != null) this.m_normalCard.Address = value;
				if (this.m_encryptedCard != null) this.m_encryptedCard.Address = value;
			}
		}

		public bool IsExpired
		{
			get {return CreditCard.IsDateExpired(this.ExpMonth, this.ExpYear);}
		}

		public string LastFourDigits
		{
			get
			{
				return (this.m_encryptedCard != null) ? this.m_encryptedCard.LastFourDigits : this.UnEncryptedNumber.Substring(this.UnEncryptedNumber.Length - 4);
			}
		}

		public CreditCardType Type
		{
			get
			{
				return (this.m_encryptedCard != null) ? this.m_encryptedCard.Type : this.m_normalCard.Type;
			}
		}
	}

	/// <summary>
	/// Holds all data that an actual credit card carries, and nothing else.
	/// The credit card number will be held as encrypted.
	/// This class will throw an exception if the input data is invalid in ANY way.
	/// However, for testing purposes it will permit the use of the invalid
	/// credit card number 4111-1111-1111-1112.
	/// </summary>
	[Serializable]
	public class CreditCardEncrypted : ISerializable
	{
		private const string s_sPassThruNumber = "4111111111111112";

		public const double Version = 1.0;
		private const string s_sEncryptKey = "fajfkhtie3903(i33RU0KJD"; // WARNING: changing will make earlier versions unreadable

		private string m_sFirstName;
		private string m_sLastName;
		private string m_sNumber = ""; // will be cleaned up and encrypted
		private string m_sLastFour = ""; // last four digits of credit card number
		private string m_sVerificationValue = ""; // some cards have this, some do not
		private int m_nExpMonth;
		private int m_nExpYear;
		private CreditCardType m_type;

		private SimpleAddress m_address;

		// During migration from unencrypted to encrypted card numbers, there will be a stage where we
		// will be using encryped but need the ability to go back to unencrypted if there are problems.
		// Because if this, there will be a temporary storage of the unencrypted number for purposes of
		// keeping the DB consistent.
		private string m_sUnencryptedCardNumber;

		protected CreditCardEncrypted(SerializationInfo info, StreamingContext context)
		{
			double dVersion = info.GetDouble("version");

			this.m_sFirstName = info.GetString("firstName");
			this.m_sLastName = info.GetString("lastName");
			this.m_sNumber = info.GetString("number");
			this.m_sLastFour = info.GetString("lastFour");
			this.m_sVerificationValue = Decrypt(info.GetString("cvv"));
			this.m_nExpMonth = info.GetInt32("expMonth");
			this.m_nExpYear = info.GetInt32("expYear");
			this.m_address = (SimpleAddress)info.GetValue("address", typeof(SimpleAddress));
			this.m_type = (CreditCardType)Enum.Parse(typeof(CreditCardType), info.GetString("type"));
			this.m_sUnencryptedCardNumber = Decrypt(info.GetString("unencrypted"));
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("version", CreditCard.Version);

			info.AddValue("firstName",this.m_sFirstName);
			info.AddValue("lastName", this.m_sLastName);
			info.AddValue("number", this.m_sNumber);
			info.AddValue("lastFour", this.m_sLastFour);
			info.AddValue("cvv", Encrypt(this.m_sVerificationValue));
			info.AddValue("expMonth", this.m_nExpMonth);
			info.AddValue("expYear", this.m_nExpYear);
			info.AddValue("address", this.m_address);
			info.AddValue("type", this.m_type.ToString());
			info.AddValue("unencrypted", Encrypt(this.m_sUnencryptedCardNumber));
		}

		/// <summary>
		/// Validates all the data; throws exceptions for bad data.  This can ONLY be called when pulling data from the DB.
		/// </summary>
		/// <param name="firstName">Card holder's first name exactly as appears on the card</param>
		/// <param name="lastName">Card holder's last name exactly as appears on the card</param>
		/// <param name="number">Number will be stripped of non-digits, then validated</param>
		/// <param name="lastFourDigits">Last four digits or credit card number.</param>
		/// <param name="month">Must be between 1 and 12</param>
		/// <param name="year">Should be the four digit year (e.g., 2003)</param>
		/// <param name="verVal">null or a CVV</param>
		/// <param name="address">Card holder's address</param>
		public CreditCardEncrypted(string firstName, string lastName, string number, string lastFourDigits, CreditCardType type, int month, int year, string verVal, SimpleAddress address)
		{
			InitRequired(firstName, lastName, number, month, year, address);
			if (verVal != null) SetCVV(verVal, this.m_sNumber);
			this.m_sLastFour = lastFourDigits;
			this.m_type = type;
		}

		private void InitRequired(string firstName, string lastName, string number, int month, int year, SimpleAddress address)
		{
			if ((null == firstName) || ("" == firstName))
			{
				string sMsg = "Invalid name";
				Tracer.Trace("CommonLib", "CreditCard", "InitRequired", "war", sMsg);
				throw new ApplicationException(sMsg);
			}

			if (null == address)
			{
				string sMsg = "Must have a valid address";
				Tracer.Trace("CommonLib", "CreditCard", "InitRequired", "war", sMsg);
				throw new ApplicationException(sMsg);
			}

			this.m_sFirstName = firstName;
			this.m_sLastName = lastName;
			this.m_address = address;
			this.m_sNumber = number;
			SetExpDate(month, year);
		}

		public string FirstName
		{
			get{return this.m_sFirstName;}
		}

		public string LastName
		{
			get{return this.m_sLastName;}
		}

		public string Name
		{
			get{return this.m_sFirstName + " " + this.m_sLastName;}
		}

		public string Number
		{
			get{return this.m_sNumber;}
		}

		public string LastFourDigits
		{
			get{return this.m_sLastFour;}
		}

		public CreditCardType Type
		{
			get{return this.m_type;}
		}

		public string CVV
		{
			get{return this.m_sVerificationValue;}
		}

		public int ExpMonth
		{
			get{return this.m_nExpMonth;}
		}

		public int ExpYear
		{
			get{return this.m_nExpYear;}
		}

		public SimpleAddress Address
		{
			get{return this.m_address;}
			set{this.m_address = value;}
		}

		public bool IsExpired
		{
			get {return CreditCard.IsDateExpired(this.m_nExpMonth, this.m_nExpYear);}
		}

		public string UnencryptedCardNumber
		{
			get {return this.m_sUnencryptedCardNumber;}
			set {this.m_sUnencryptedCardNumber = value;}
		}

		private void SetExpDate(int month, int year)
		{
			if ((0 >= month) || (12 < month))
			{
				string sMsg = "Month must be in range [1, 12]";
				Tracer.Trace("CommonLib", "CreditCard", "SetExpDate", "war", sMsg);
				throw new ApplicationException(sMsg);
			}

			DateTime today = DateTime.Today;
			DateTime lastPossible = today.AddYears(CreditCard.MaxExpYears);
			// Actually expires at end of input month
			int nExpMonth = (12 == month) ? 1 : month + 1;
			int nExpYear  = (12 == month) ? year + 1 : year;
			DateTime expires = new DateTime(nExpYear, nExpMonth, 1);

			if (expires >= lastPossible)
			{
				string sMsg = "Expiration date beyond " + CreditCard.MaxExpYears.ToString() + " year limit";
				Tracer.Trace("CommonLib", "CreditCard", "SetExpDate", "war", sMsg);
				throw new ApplicationException(sMsg);
			}

			// NOTE: do not check whether it is expired here.  The property IsExpired can be used
			//		 for that.  The reason is that valid cards when entered into db will become
			//		 expired with time.  We don't want to throw exceptions in this case.  Better
			//		 that the client code call IsExpired explicitely and take whatever action is
			//		 appropriate in the given context.

			this.m_nExpMonth = month;
			this.m_nExpYear = year;
		}

		private void SetCVV(string cvv, string number)
		{
			if ((null == cvv) || ("" == cvv)) return;

			try
			{
				Convert.ToInt32(cvv); // will throw an exception if not numeric
			}
			catch
			{
				string sMsg = "A Card Verification Value must be numeric.";
				throw new ApplicationException(sMsg);
			}

			this.m_sVerificationValue = cvv;
		}

		private string Encrypt(string inString)
		{
			if ((inString == null) || (inString.Length == 0)) return inString;

			using (CryptoUtil crypto = new CryptoUtil(CreditCardEncrypted.s_sEncryptKey))
			{
				return crypto.Encrypt(inString);
			}
		}

		private string Decrypt(string inString)
		{
			if ((inString == null) || (inString.Length == 0)) return inString;

			using (CryptoUtil crypto = new CryptoUtil(CreditCardEncrypted.s_sEncryptKey))
			{
				return crypto.Decrypt(inString);
			}
		}
	}
}
