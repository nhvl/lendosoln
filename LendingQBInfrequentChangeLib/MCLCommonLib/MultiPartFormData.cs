// Copyright 2000-2005 MeridianLink, Inc.  All Rights Reserved.
using System;
using System.Collections;
using System.IO;
using System.Net;
using System.Text;

namespace CommonLib
{
	/// <summary>
	/// Utility class to assist in the generation of multipart/form-data http request/response streams.
	/// </summary>
	/// <remarks>
	/// <see cref="http://www.ietf.org/rfc/rfc1867.txt"/>
	/// </remarks>
	public class MultiPartFormData
	{
		private const int MAX_ALLOWED = 100;
		private static readonly byte DASH;
		private static readonly byte CR;
		private static readonly byte LF;
		private static readonly char cDASH;
		private static readonly char cCR;
		private static readonly char cLF;
		private const string BOUNDARY_DELIM = "--";
		private const string LINE_TERMINATOR = "\r\n";
		private const string DISP_TEMPLATE = "Content-Disposition: form-data; name=\"{0}\";";

		private const string CONTENT_TYPE = "multipart/form-data";
		private const string BOUNDARY_IND = "boundary";
		private const string NAME_IND = "name";

		private FormDataCollection m_items = new FormDataCollection();

		private string m_sBoundary;

		static MultiPartFormData()
		{
			Encoding enc = GetEncoding();
			DASH = enc.GetBytes("-")[0];
			CR = enc.GetBytes("\r")[0];
			LF = enc.GetBytes("\n")[0];

			cDASH = '-';
			cCR = '\r';
			cLF = '\n';
		}

		static Encoding GetEncoding()
		{
			return new ASCIIEncoding();
		}

		public MultiPartFormData()
		{
			this.m_sBoundary = Guid.NewGuid().ToString("N");
		}

		public MultiPartFormData(string boundary)
		{
			this.m_sBoundary = boundary;
		}

		public FormDataCollection Forms
		{
			get {return this.m_items;}
		}

		#region Alan's test code
#if DEBUG
		public string TestWrite()
		{
			byte[] body = GetPostBody();
			Encoding enc = GetEncoding();
			return enc.GetString(body);
		}

		public static MultiPartFormData TestRead(string boundary, string body)
		{
			Encoding enc = GetEncoding();
			byte[] bodyBytes = enc.GetBytes(body);
			
			MultiPartFormData item = new MultiPartFormData(boundary);
			item.Parse(bodyBytes);

			return item;
		}
#endif
		#endregion

		#region Methods for reading an HttpResponse
		public static MultiPartFormData Process(HttpWebResponse res)
		{
			string sContentType = res.ContentType;
			int nIndex = sContentType.IndexOf(CONTENT_TYPE);
			if (nIndex < 0) return null;

			nIndex = sContentType.IndexOf(BOUNDARY_IND, nIndex + CONTENT_TYPE.Length);
			if (nIndex < 0) return null;

			string sTail = sContentType.Substring(nIndex);
			string[] pieces = sTail.Split();
			string[] sides = pieces[0].Split('=');
			if (sides.Length != 2) return null;

			// Handle quoted boundaries.
			if (sides[1].StartsWith("\""))
			{
				nIndex = sides[1].IndexOf('"', 1) ;
				if ( nIndex > 0 )
					sides[1] = sides[1].Substring(1, nIndex-1) ;
			}
			
			MultiPartFormData data = new MultiPartFormData(sides[1]);
			data.Parse(res);

			return data;
		}

		private void Parse(HttpWebResponse res)
		{
			Stream body = res.GetResponseStream();
			try
			{
				StreamReader reader = new StreamReader(res.GetResponseStream());
				string sReturned = reader.ReadToEnd();
				Parse(sReturned);
			}
			finally
			{
				body.Close();
			}
		}

		private void Parse(string data)
		{
			string sBoundary = BOUNDARY_DELIM + this.m_sBoundary;
			
			int nFound = 0;
			int nPosBegin = 0;
			int nPosEnd = 0;
			do
			{
				nPosBegin = (nPosEnd == 0) ? LocateFormData(data, sBoundary, 0) : nPosEnd;
				nPosEnd = LocateFormData(data, sBoundary, nPosBegin + sBoundary.Length);
				ParseFormData(data, nPosBegin, nPosEnd);
				++nFound;
			}
			while ((nFound < MAX_ALLOWED) && !IsEnd(data, sBoundary, nPosEnd));
		}

		private void Parse(byte[] data)
		{
			string sBoundary = BOUNDARY_DELIM + this.m_sBoundary;
			Encoding enc = GetEncoding();
			byte[] arrBoundary = enc.GetBytes(sBoundary);

			int nFound = 0;
			int nPosBegin = 0;
			int nPosEnd = 0;
			do
			{
				nPosBegin = (nPosEnd == 0) ? LocateFormData(data, arrBoundary, 0) : nPosEnd;
				nPosEnd = LocateFormData(data, arrBoundary, nPosBegin + arrBoundary.Length);
				ParseFormData(data, nPosBegin, nPosEnd);
				++nFound;
			}
			while ((nFound < MAX_ALLOWED) && !IsEnd(data, arrBoundary, nPosEnd));
		}

		private void ParseFormData(string data, int begin, int end)
		{
			int nDataPos;
			string sHeaders;
			ParseHeaders(data, begin, end, out sHeaders, out nDataPos);

			StringReader reader = new StringReader(sHeaders);
			string sDisp = reader.ReadLine();
			string sType = reader.ReadLine();
			reader.Close();

			int nIndex = sDisp.IndexOf(NAME_IND);
			string sRest = sDisp.Substring(nIndex + NAME_IND.Length + 2);
			nIndex = sRest.IndexOf("\"");
			string sName = sRest.Substring(0, nIndex);

			if (sType.Length > 0)
			{
				nIndex = sType.IndexOf(":");
				sType = sType.Substring(nIndex + 1).Trim();
			}

			bool bIsText = TypeIsText(sType);
			if (bIsText)
			{
				string sData = data.Substring(nDataPos, end - nDataPos);
				FormData form = new FormData(sName, sData);
				this.Forms.Add(form);
			}
			else
			{
				// Cannot handle non-text at this time.
				return;
			}
		}

		private void ParseFormData(byte[] data, int begin, int end)
		{
			int nDataPos;
			string sHeaders;
			ParseHeaders(data, begin, end, out sHeaders, out nDataPos);

			StringReader reader = new StringReader(sHeaders);
			string sDisp = reader.ReadLine();
			string sType = reader.ReadLine();
			reader.Close();

			int nIndex = sDisp.IndexOf(NAME_IND);
			string sRest = sDisp.Substring(nIndex + NAME_IND.Length + 2);
			nIndex = sRest.IndexOf("\"");
			string sName = sRest.Substring(0, nIndex);

			if (sType.Length > 0)
			{
				nIndex = sType.IndexOf(":");
				sType = sType.Substring(nIndex + 1).Trim();
			}

			bool bIsText = TypeIsText(sType);
			if (bIsText)
			{
				Encoding enc = GetEncoding();
				string sData = enc.GetString(data, nDataPos, end - nDataPos);
				FormData form = new FormData(sName, sData);
				this.Forms.Add(form);
			}
			else
			{
				byte[] formData = new byte[end - nDataPos];
				Array.Copy(data, nDataPos, formData, 0, end - nDataPos);
				FormData form = new FormData(sName, formData);
				this.Forms.Add(form);
			}
		}

		private enum State{Boundary, Seek, First, Second, Third}
		private void ParseHeaders(string data, int begin, int end, out string headers, out int dataPos)
		{
			headers = null;
			dataPos = begin;

			State state = State.Boundary;
			int nPos = begin;
			int nHdrStart = 0;
			while (nPos < end)
			{
				switch (state)
				{
					case State.Boundary:
						if (data[nPos] == cLF)
						{
							nHdrStart = nPos + 1;
							state = State.Seek;
						}
						break;
					case State.Seek:
						if (data[nPos] == cCR) state = State.First;
						break;
					case State.First:
						if (data[nPos] == cLF) state = State.Second;
						else state = State.Seek;
						break;
					case State.Second:
						if (data[nPos] == cCR) state = State.Third;
						else state = State.Seek;
						break;
					case State.Third:
						if (data[nPos] == cLF)
						{
							dataPos = nPos + 1;
							headers = data.Substring(nHdrStart, dataPos - nHdrStart);
							return;
						}
						else state = State.Seek;
						break;
				}
				++nPos;
			}
		}

		private void ParseHeaders(byte[] data, int begin, int end, out string headers, out int dataPos)
		{
			headers = null;
			dataPos = begin;

			State state = State.Boundary;
			int nPos = begin;
			int nHdrStart = 0;
			while (nPos < end)
			{
				switch (state)
				{
					case State.Boundary:
						if (data[nPos] == LF)
						{
							nHdrStart = nPos + 1;
							state = State.Seek;
						}
						break;
					case State.Seek:
						if (data[nPos] == CR) state = State.First;
						break;
					case State.First:
						if (data[nPos] == LF) state = State.Second;
						else state = State.Seek;
						break;
					case State.Second:
						if (data[nPos] == CR) state = State.Third;
						else state = State.Seek;
						break;
					case State.Third:
						if (data[nPos] == LF)
						{
							dataPos = nPos + 1;
							Encoding enc = GetEncoding();
							headers = enc.GetString(data, nHdrStart, dataPos - nHdrStart);
							return;
						}
						else state = State.Seek;
						break;
				}
				++nPos;
			}
		}

		private bool TypeIsText(string type)
		{
			switch (type)
			{
				case "":
				case "text/plain":
					return true;

				default:
					return false;
			}
		}

		private int LocateFormData(string data, string boundary, int start)
		{
			bool bInBoundary = false;
			int nBIndex = 0;
			int nPos = (start == 0) ? start : start + boundary.Length;
			while (nPos < data.Length)
			{
				if (data[nPos] == boundary[nBIndex])
				{
					if (bInBoundary)
					{
						if (nBIndex == boundary.Length - 1)
						{
							return nPos - nBIndex;
						}
					}
					else
					{
						bInBoundary = true;
					}
					++nBIndex;
				}
				else
				{
					if (bInBoundary)
					{
						bInBoundary = false;
						nBIndex = 0;
					}
				}
				++nPos;
			}

			return -1;
		}

		private int LocateFormData(byte[] data, byte[] boundary, int start)
		{
			bool bInBoundary = false;
			int nBIndex = 0;
			int nPos = (start == 0) ? start : start + boundary.Length;
			while (nPos < data.Length)
			{
				if (data[nPos] == boundary[nBIndex])
				{
					if (bInBoundary)
					{
						if (nBIndex == boundary.Length - 1)
						{
							return nPos - nBIndex;
						}
					}
					else
					{
						bInBoundary = true;
					}
					++nBIndex;
				}
				else
				{
					if (bInBoundary)
					{
						bInBoundary = false;
						nBIndex = 0;
					}
				}
				++nPos;
			}

			return -1;
		}

		private bool IsEnd(string data, string boundary, int start)
		{
			int nPos = start + boundary.Length;
			return ((data[nPos] == cDASH) && (data[nPos+1] == cDASH));
		}

		private bool IsEnd(byte[] data, byte[] boundary, int start)
		{
			int nPos = start + boundary.Length;
			return ((data[nPos] == DASH) && (data[nPos+1] == DASH));
		}
		#endregion

		#region Methods for writing to an HttpRequest
		public FormData AddFormData(string name, string textData)
		{
			FormData data = new FormData(name, textData);
			this.m_items.Add(data);
			return data;
		}

		public FormData AddFormData(string name, byte[] binData)
		{
			FormData data = new FormData(name, binData);
			this.m_items.Add(data);
			return data;
		}

		public void Write(HttpWebRequest req)
		{
			req.Method = "POST";
			req.ContentType = string.Format("{0}; {1}={2}", CONTENT_TYPE, BOUNDARY_IND, this.m_sBoundary);

			byte[] data = GetPostBody();
			req.ContentLength = data.Length;

			Stream body = req.GetRequestStream();
			try
			{
				body.Write(data, 0, data.Length);
			}
			finally
			{
				body.Close();
			}
		}

		private byte[] GetPostBody()
		{
			byte[][] arrSections = new byte[this.m_items.Count + 1][];
			Encoding enc = GetEncoding();
			int index = 0;
			foreach (FormData data in this.Forms)
			{
				arrSections[index++] = data.IsText ? FormText(data, enc) : FormBinary(data, enc);
			}
			arrSections[index++] = TerminalLine(enc);

			int nLen = 0;
			for (int i=0; i<index; ++i)
			{
				nLen += arrSections[i].Length;
			}

			byte[] total = new byte[nLen];
			int nPos = 0;
			for (int i=0; i<index; ++i)
			{
				arrSections[i].CopyTo(total, nPos);
				nPos += arrSections[i].Length;
			}

			return total;
		}

		private byte[] TerminalLine(Encoding enc)
		{
			int nLen = 2 * BOUNDARY_DELIM.Length + LINE_TERMINATOR.Length + this.m_sBoundary.Length;
			StringBuilder sb = new StringBuilder(nLen);
			sb.Append(BOUNDARY_DELIM);
			sb.Append(this.m_sBoundary);
			sb.Append(BOUNDARY_DELIM);
			sb.Append(LINE_TERMINATOR);

			return enc.GetBytes(sb.ToString());
		}

		private byte[] FormText(FormData data, Encoding enc)
		{
			int nLen = BOUNDARY_DELIM.Length + this.m_sBoundary.Length + DISP_TEMPLATE.Length + data.Name.Length + data.TextData.Length + 6 * LINE_TERMINATOR.Length;
			StringBuilder sb = new StringBuilder(nLen);
			sb.Append(BOUNDARY_DELIM);
			sb.Append(this.m_sBoundary);
			sb.Append(LINE_TERMINATOR);
			sb.AppendFormat(DISP_TEMPLATE, data.Name);
			sb.Append(LINE_TERMINATOR);
			sb.Append(LINE_TERMINATOR);
			sb.Append(data.TextData);
			sb.Append(LINE_TERMINATOR);
			sb.Append(LINE_TERMINATOR);

			return enc.GetBytes(sb.ToString());
		}

		private byte[] FormBinary(FormData data, Encoding enc)
		{
			int nLen = BOUNDARY_DELIM.Length + this.m_sBoundary.Length + DISP_TEMPLATE.Length + data.Name.Length + data.BinData.Length + 6 * LINE_TERMINATOR.Length;
			byte[] lineTerminator = enc.GetBytes(LINE_TERMINATOR);
			MemoryStream stream = new MemoryStream(nLen);

			int nPos = 0;
			stream.Write(enc.GetBytes(BOUNDARY_DELIM), nPos, BOUNDARY_DELIM.Length);
			nPos += BOUNDARY_DELIM.Length;

			stream.Write(enc.GetBytes(this.m_sBoundary), nPos, this.m_sBoundary.Length);
			nPos += this.m_sBoundary.Length;

			stream.Write(lineTerminator, nPos, LINE_TERMINATOR.Length);
			nPos += LINE_TERMINATOR.Length;

			string sDisp = string.Format(DISP_TEMPLATE, data.Name);
			stream.Write(enc.GetBytes(sDisp), nPos, sDisp.Length);
			nPos += sDisp.Length;

			stream.Write(lineTerminator, nPos, LINE_TERMINATOR.Length);
			nPos += LINE_TERMINATOR.Length;

			stream.Write(lineTerminator, nPos, LINE_TERMINATOR.Length);
			nPos += LINE_TERMINATOR.Length;

			stream.Write(data.BinData, nPos, data.BinData.Length);
			nPos += data.BinData.Length;

			stream.Write(lineTerminator, nPos, LINE_TERMINATOR.Length);
			nPos += LINE_TERMINATOR.Length;

			stream.Write(lineTerminator, nPos, LINE_TERMINATOR.Length);
			nPos += LINE_TERMINATOR.Length;

			return stream.GetBuffer();
		}
		#endregion
	}

	/// <summary>
	/// Type-safe collection
	/// </summary>
	public class FormDataCollection : IEnumerable
	{
		private ArrayList m_forms = new ArrayList();

		public int Count
		{
			get {return this.m_forms.Count;}
		}

		public FormData this[int index]
		{
			get {return this.m_forms[index] as FormData;}
		}

		public FormData this[string name]
		{
			get
			{
				foreach (FormData data in this.m_forms)
				{
					if (data.Name == name) return data;
				}
				return null;
			}
		}

		public IEnumerator GetEnumerator()
		{
			return this.m_forms.GetEnumerator();
		}

		internal void Add(FormData item)
		{
			this.m_forms.Add(item);
		}
	}

	public class FormData
	{
		private string m_sName;
		private byte[] m_binData;
		private string m_sTextData;

		internal FormData(string name, string textData)
		{
			this.m_sName = name;
			this.m_sTextData = textData;
		}

		internal FormData(string name, byte[] binData)
		{
			this.m_sName = name;
			this.m_binData = binData;
		}

		public string Name
		{
			get {return this.m_sName;}
		}

		public byte[] BinData
		{
			get {return this.m_binData;}
		}

		public string TextData
		{
			get {return this.m_sTextData;}
		}

		public bool IsText
		{
			get {return (this.TextData != null);}
		}
	}
}
