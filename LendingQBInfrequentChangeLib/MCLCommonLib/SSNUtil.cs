using System;
using System.Text.RegularExpressions;

namespace CommonLib
{
	/// <summary>
	/// Summary description for SSNUtil.
	/// </summary>
	public class SSNUtil
	{
		// Replace the last four digits of any occurence of SSN data (format: "999999999" or "999-99-9999") with "****"
		public static string MaskSSN(string sText)
		{
			if (sText == null) return null;

			string sReturn = sText;

			Regex re = new Regex(@"\b(\d{9})|(\d{3}-\d{2}-\d{4})\b");
			Match m;

			for (m = re.Match(sText); m.Success; m = m.NextMatch())
				if (m.Value.Length == 9)
					sReturn = sReturn.Replace(m.Value, "*****" + m.Value.Remove(0, 5));
				else
					sReturn = sReturn.Replace(m.Value, "***-**-" + m.Value.Remove(0, 7));

			return sReturn;
		}
	}

	/// <summary>
	/// Represents a social security number.
	/// </summary>
	/// <remarks>
	/// This would be a struct instead of a class if it weren't for the .NET 1.x position that structs aren't nullable.
	/// </remarks>
	public class SSN : IFormattable
	{
		/// <summary>
		/// The stored social security number.
		/// This is a string containing nine digits, 0-9.
		/// </summary>
		private string m_sSSN ;

		/// <summary>
		/// Initializes a new SSN.
		/// </summary>
		/// <param name="ssn">String containing an SSN.</param>
		/// <exception cref="FormatException">The input string does not contain exactly nine digits.</exception>
		public SSN(string ssn)
		{
			BuildSSN(ssn, false) ;			
		}

		/// <summary>
		/// Initializes a new SSN.
		/// </summary>
		/// <param name="ssn">String containing an SSN.</param>
		/// <param name="pad">
		/// True to left-pad zeroes if necessary.
		/// This is provided for compatibility with certain applications that consider SSNs to be integers,
		/// and lop off "leading zeroes" to be friendly.
		/// </param>
		/// <exception cref="FormatException">
		/// The input string contains either no digits or more than nine digits, or less than nine digits without padding.
		/// </exception>
		public SSN(string ssn, bool pad)
		{
			BuildSSN(ssn, pad) ;
		}

		/// <summary>
		/// Constructs a new SSN.
		/// </summary>
		/// <param name="ssn">String containing an SSN.</param>
		/// <param name="pad">True to left-pad zeroes if necessary.</param>
		/// <exception cref="FormatException">
		/// The input string contains either no digits or more than nine digits, or less than nine digits without padding.
		/// </exception>
		private void BuildSSN(string ssn, bool pad)
		{
			System.Text.StringBuilder sbSSN = new System.Text.StringBuilder(9) ;
			int nDigits = 0 ;

			foreach ( char c in ssn ) 
			{
				if ( c >= '0' && c <= '9' )
				{
					if ( ++nDigits > 9 )
						throw new FormatException("Invalid SSN.  Too many digits.") ;
					sbSSN.Append(c) ;
				}
			}

			if ( nDigits <= 0 )
				throw new FormatException("Invalid SSN.  No digits specified.") ;

			if ( nDigits == 9 )
				m_sSSN = sbSSN.ToString() ;
			else if ( nDigits < 9 )
			{
				if (pad)
					m_sSSN = new String('0', 9 - sbSSN.Length) + sbSSN.ToString() ;
				else
					throw new FormatException("Invalid SSN.  Not enough digits.") ;				
			}
			else
			{
				// This should never be called; the condition should be handled above.
				throw new FormatException("Invalid SSN.  Too many digits.") ;
			}
		}

		/// <summary>
		/// Determines whether or not this SSN is formatted correctly.
		/// </summary>
		/// <returns></returns>
		/// <remarks>
		/// This is not currently an exhaustive search to ensure that the area and group numbers line up.
		/// The SSA provides details on this at http://www.socialsecurity.gov/employer/ssnvhighgroup.htm.
		/// </remarks>
		public bool IsValid()
		{
			return SSNValidator.Validate(m_sSSN) ;
		}

		/// <summary>
		/// Returns a string representation of the SSN.
		/// </summary>
		/// <returns>The SSN, without any delimiters.</returns>
		public override string ToString()
		{
			return m_sSSN ;
		}

		/// <summary>
		/// Returns a string representation of the SSN.
		/// </summary>
		/// <param name="format">A format string.</param>
		/// <returns>The SSN, formatted as specified.</returns>
		/// <exception cref="FormatException">The format string is not valid.</exception>
		/// <remarks>
		/// Format characters:
		/// g, G:	General format.  The SSN is returned without delimiters.	[e.g., "123456789"]
		/// f, F:	Full format.  The SSN is returned with hyphen delimiters.	[e.g., "123-45-6789"]
		/// s, S:	Spaced format.  The SSN is returned with space delimiters.	[e.g., "123 45 6789"]
		/// </remarks>
		public string ToString(string format)
		{
			if ( format == null || format.Length < 1 )
				return this.ToString() ;

			char cFormat = format[0] ;
			if ( cFormat == 'G' || cFormat == 'g' )
				return this.ToString() ;
			else if ( cFormat == 'F' || cFormat == 'f' )
				return String.Format("{0}-{1}-{2}", m_sSSN.Substring(0,3), m_sSSN.Substring(3,2), m_sSSN.Substring(5,4)) ;
			else if ( cFormat == 'S' || cFormat == 's' )
				return String.Format("{0} {1} {2}", m_sSSN.Substring(0,3), m_sSSN.Substring(3,2), m_sSSN.Substring(5,4)) ;
			else
				throw new FormatException("Invalid format qualifier.") ;
		}

		/// <summary>
		/// Returns a string representation of the SSN.
		/// </summary>
		/// <param name="format">A format string.</param>
		/// <param name="provider">A format provider.</param>
		/// <returns>The SSN, formatted as specified.</returns>
		/// <exception cref="FormatException">The format string is not valid.</exception>
		/// <remarks>
		/// This is used for IFormattable.
		/// The format provider is ignored; SSNs are by definition only meant for the US (en-US; MS LCID 1033, English_United_States).
		/// </remarks>
		public string ToString(string format, IFormatProvider provider)
		{
			return this.ToString(format) ;
		}
	}
}
