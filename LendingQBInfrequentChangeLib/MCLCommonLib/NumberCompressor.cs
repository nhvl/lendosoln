using System;
using System.Text;

namespace CommonLib
{
	/// <summary>
	/// Used to give a more compact string representation of a number.
	/// </summary>
	public abstract class NumberCompressor
	{
		private const int NORMAL_BASE = 93;
		private const int NORMAL_OFFSET = 34;
		private const int NORMAL_NEG_CHAR = NORMAL_OFFSET - 1;
		private const int NORMAL_PNT_CHAR = NORMAL_NEG_CHAR - 1;

		private const int SUPER_BASE = 253;
		private const int SUPER_OFFSET = 3;
		private const int SUPER_NEG_CHAR = SUPER_OFFSET - 1;
		private const int SUPER_PNT_CHAR = SUPER_NEG_CHAR - 1;

		/// <summary>
		/// Compress an integer into a short string composed of visible characters
		/// </summary>
		public static string Compress(int input)
		{
			return CompressImpl(input, NORMAL_BASE, NORMAL_OFFSET, NORMAL_NEG_CHAR);
		}

		/// <summary>
		/// Compress a double into a short string composed of visible characters
		/// </summary>
		public static string Compress(double input)
		{
			string sInput = input.ToString();
			string[] parts = sInput.Split('.');
			int nIntPart = int.Parse(parts[0]);
			string sCompressed = Compress(nIntPart);
			if (parts.Length == 2)
			{
				int nFracPart = int.Parse(parts[1]);
				string sFracPart = Compress(nFracPart);
				sCompressed = string.Format("{0}{1}{2}", sCompressed, ((char)NORMAL_PNT_CHAR).ToString(), sFracPart);
			}

			return sCompressed;
		}

		/// <summary>
		/// Compress an integer into a short string composed of not-necessarily visible characters
		/// </summary>
		public static string SuperCompress(int input)
		{
			return CompressImpl(input, SUPER_BASE, SUPER_OFFSET, SUPER_NEG_CHAR);
		}

		/// <summary>
		/// Compress a double into a short string composed of not-necessarily visible characters
		/// </summary>
		public static string SuperCompress(double input)
		{
			string sInput = input.ToString();
			string[] parts = sInput.Split('.');
			int nIntPart = int.Parse(parts[0]);
			string sCompressed = SuperCompress(nIntPart);
			if (parts.Length == 2)
			{
				int nFracPart = int.Parse(parts[1]);
				string sFracPart = SuperCompress(nFracPart);
				sCompressed = string.Format("{0}{1}{2}", sCompressed, ((char)SUPER_PNT_CHAR).ToString(), sFracPart);
			}

			return sCompressed;
		}

		/// <summary>
		/// Decompress a string into an int; used when the input is known to have come from an integer.
		/// </summary>
		/// <param name="input">Compressed number</param>
		/// <returns>The original number if it was an int, else int.MinValue</returns>
		public static int DeCompressInt(string input)
		{
			int nInt;
			double dDouble;
			DeCompress(input, out nInt, out dDouble);
			return nInt;
		}

		/// <summary>
		/// Decompress a string into a double.
		/// </summary>
		/// <param name="input">Compressed number</param>
		/// <returns>The original number cast into a double</returns>
		public static double DeCompressDouble(string input)
		{
			int nInt;
			double dDouble;
			DeCompress(input, out nInt, out dDouble);
			return dDouble;
		}

		/// <summary>
		/// Decompress a string into the original number
		/// </summary>
		/// <param name="input">Compressed number</param>
		/// <param name="outInt">The original number if it was an int, else int.MinValue</param>
		/// <param name="outDouble">The original number cast into a double</param>
		public static void DeCompress(string input, out int outInt, out double outDouble)
		{
			outInt = int.MinValue;
			outDouble = double.MinValue;

			string[] parts = input.Split((char)NORMAL_PNT_CHAR);
			int nIntPart = DeCompressImpl(parts[0], NORMAL_BASE, NORMAL_OFFSET, NORMAL_NEG_CHAR);
			if (parts.Length == 1)
			{
				outInt = nIntPart;
				outDouble = outInt;
				return;
			}

			int nFracPart = DeCompressImpl(parts[1], NORMAL_BASE, NORMAL_OFFSET, NORMAL_NEG_CHAR);
			string sTotal = string.Format("{0}.{1}", nIntPart.ToString(), nFracPart.ToString());
			outDouble = double.Parse(sTotal);
		}

		/// <summary>
		/// Decompress a string into an int; used when the input is known to have come from an integer.
		/// </summary>
		/// <param name="input">Compressed number</param>
		/// <returns>The original number if it was an int, else int.MinValue</returns>
		public static int SuperDeCompressInt(string input)
		{
			int nInt;
			double dDouble;
			SuperDeCompress(input, out nInt, out dDouble);
			return nInt;
		}

		/// <summary>
		/// Decompress a string into a double.
		/// </summary>
		/// <param name="input">Compressed number</param>
		/// <returns>The original number cast into a double</returns>
		public static double SuperDeCompressDouble(string input)
		{
			int nInt;
			double dDouble;
			SuperDeCompress(input, out nInt, out dDouble);
			return dDouble;
		}

		/// <summary>
		/// Decompress a string into the original number
		/// </summary>
		/// <param name="input">Compressed number</param>
		/// <param name="outInt">the original number if it was an int, else int.MinValue</param>
		/// <param name="outDouble">the original number cast into a double</param>
		public static void SuperDeCompress(string input, out int outInt, out double outDouble)
		{
			outInt = int.MinValue;
			outDouble = double.MinValue;

			string[] parts = input.Split((char)SUPER_PNT_CHAR);
			int nIntPart = DeCompressImpl(parts[0], SUPER_BASE, SUPER_OFFSET, SUPER_NEG_CHAR);
			if (parts.Length == 1)
			{
				outInt = nIntPart;
				outDouble = outInt;
				return;
			}

			int nFracPart = DeCompressImpl(parts[1], SUPER_BASE, SUPER_OFFSET, SUPER_NEG_CHAR);
			string sTotal = string.Format("{0}.{1}", nIntPart.ToString(), nFracPart.ToString());
			outDouble = double.Parse(sTotal);
		}

		private static string CompressImpl(int input, int baseLoc, int offset, int neg)
		{
			StringBuilder sb = new StringBuilder(20);
			int nWorking = input;
			if (nWorking < 0)
			{
				sb.Append((char)neg);
				nWorking = Math.Abs(nWorking);
			}
			do
			{
				int nMod = nWorking % baseLoc;
				char ch = (char)(nMod + offset);
				sb.Append(ch);
				nWorking -= nMod;
				nWorking /= baseLoc;
			}
			while (nWorking > 0);

			return sb.ToString();
		}

		private static int DeCompressImpl(string input, int baseLoc, int offset, int neg)
		{
			int nSign = 1;
			int nValue = 0;
			int nFactor = 1;
			foreach (char ch in input)
			{
				if (ch == (char)neg)
				{
					nSign = -1;
					continue;
				}

				int nVal = ((int)ch - offset) * nFactor;
				nValue += nVal;
				nFactor *= baseLoc;
			}

			return nSign * nValue;
		}
	}
}
