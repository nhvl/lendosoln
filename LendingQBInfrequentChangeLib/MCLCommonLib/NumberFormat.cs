using System;
using System.Text;
using System.Globalization;

namespace CommonLib
{
	/// <summary>
	/// Summary description for NumberFormat.
	/// </summary>
    public class NumberFormat
    {
        static private NumberFormatInfo s_IntegerCommaFormatInfo  = null;

        static NumberFormat()
        {
            try
            {
                s_IntegerCommaFormatInfo  = new CultureInfo( "en-US", false ).NumberFormat;
            }
            catch( Exception )
            {
                s_IntegerCommaFormatInfo = new NumberFormatInfo();
            }
            s_IntegerCommaFormatInfo.NumberDecimalDigits = 0;
            s_IntegerCommaFormatInfo.NumberGroupSizes    = new int[] {3};
            
        }

        public static NumberFormatInfo IntegerAndCommaFormatter
        {
            get 
            {            
                return s_IntegerCommaFormatInfo;
            }
        }
	}

    public class TimeTool
    {
        public static string GetDurationString( DateTime startTime )
        {
            return TimeSpan2String( DateTime.Now - startTime );
        }

        public static string TimeSpan2String( TimeSpan  duration )
        {
            duration = new TimeSpan( ((duration.Ticks + TimeSpan.TicksPerSecond/2 )/ TimeSpan.TicksPerSecond) * TimeSpan.TicksPerSecond);

            string durationStr = duration.TotalSeconds.ToString( "N0" );

            StringBuilder sb = new StringBuilder();
            if( duration.TotalSeconds < 60 )
                sb.AppendFormat("{0} seconds", durationStr);
            else
                sb.AppendFormat("{0} seconds = {1}", durationStr, duration.ToString());
            return sb.ToString();
        }

        public static string GetDurationStringInMs( DateTime startTime )
        {
            return TimeSpan2StringInMs( DateTime.Now - startTime );
        }

        public static string TimeSpan2StringInMs( TimeSpan  duration )
        {
            string durationStr = ((long)duration.TotalMilliseconds).ToString( "N0" );

            StringBuilder sb = new StringBuilder();
            if( duration.TotalSeconds < 60 )
                sb.AppendFormat("{0} ms", durationStr);
            else if( duration.TotalSeconds < 2*3600 )
                sb.AppendFormat("{0} ms = {1}", durationStr, duration.ToString());
            else
                return duration.ToString();
            return sb.ToString();
        }

        // display format : month/day h:m:s AM. For example 2/26 5:53:59 PM
        public static string DateTime2String( DateTime time )
        {
            return string.Format( "{0}/{1} {2}", time.Month, time.Day, time.ToLongTimeString());
            //return string.Format( "{0}/{1} {3}:{4}:{5}", time.Month, time.Day, time.Hour, time.Minute, time.Second);
        }

    }
}
