using System;
using System.Text;
using System.Text.RegularExpressions;

namespace CommonLib.AddressParser
{
	/// <summary>
	/// Parses a street address
	/// 
	/// OPM # 11995
	/// We don't want to muck up the complex parsing so we are adding a hack here.
	/// If the first element of address string contains a dash then we will store
	/// it and return it as the street number.
	/// </summary>
	public class StreetParser
	{
		private static Regex scrubchars;
		private static Regex space_comp = new Regex(@" +");

		static internal char[] Symbols = new char[] {'#', '-'};

		#region Member data
		private bool m_bTrace = false;
		private string m_sTrace = "";

		private string m_sAddress;
		private string m_sAddress2;
		private string m_sNumber;
		private string m_sPreDir;
        private string m_sPostDir;
		private string m_sName;
		private string m_sType;
		private string m_sApt;

		private string m_sWorking;
		private string m_sDashHack; // OPM # 11995
		#endregion

		static StreetParser()
		{
			scrubchars = new Regex(GetScrubCharRegEx()); //clean up the string
		}

		public StreetParser()
		{
			Reset();
		}

		#region Public methods/properties

		public void SetTrace(bool trace)
		{
			this.m_bTrace = trace;
		}

		public string Trace
		{
			get{return this.m_sTrace;}
		}

		public void Parse(string address)
		{
			Reset();
			this.m_sAddress = address;
			if ((null == this.m_sAddress) || ("" == this.m_sAddress)) return;

			DoParse();
		}

		public string Address2
		{
			get{return this.m_sAddress2;}
		}

		public string StreetNumber
		{
			get
			{
				// OPM # 11995
				if (SafeString.HasValue(this.m_sDashHack)) return this.m_sDashHack;
				return this.m_sNumber;
			}
		}

		public string StreetDirection
		{
			get{return this.m_sPreDir;}
		}

        public string StreetPostDirection
        {
            get { return this.m_sPostDir; }
        }

		public string StreetName
		{
			get{return this.m_sName;}
		}

		public string StreetType
		{
			get{return this.m_sType;}
		}

		public string Apartment
		{
			get{return this.m_sApt;}
		}

		public string InputAddress
		{
			get{return this.m_sAddress;}
		}

		#endregion

		#region Private methods

		private void Reset()
		{
			this.m_sTrace = "";
			this.m_sAddress = "";
			this.m_sAddress2 = "";
			this.m_sNumber = "";
			this.m_sPreDir = "";
            this.m_sPostDir = "";
			this.m_sName = "";
			this.m_sType = "";
			this.m_sApt = "";
		}

		private void DoParse()
		{
			SetWorkingAddress();
			if ((null == this.m_sWorking) || ("" == this.m_sWorking)) return;

			Atom[] atoms = null;
			Token[] tokens = null;
			Clause[] clauses = null;
			ParsedAddress[] addresses = null;
			ParsedAddress final = null;
			try
			{
				atoms = Atom.Generate(this.m_sWorking);
				tokens = Token.Generate(atoms);
				clauses = Clause.Generate(tokens);
				addresses = DivisionRule.Parse(clauses);
				final = DecisionRule.Decide(addresses);
				RejectInvalid(final);
			}
			catch (Exception ex)
			{
				if (this.m_bTrace) SetTrace(atoms, tokens, clauses, addresses, final, ex.Message);
				throw new ApplicationException(string.Format("Address parsing failed [{0}]", ex.Message));
			}

			this.m_sNumber = final.StreetNumber;
			this.m_sPreDir = final.StreetDirection;
            this.m_sPostDir = final.StreetPostDirection;
			this.m_sName = final.StreetName;
			this.m_sType = final.StreetType;
			this.m_sApt = final.Apartment;
			this.m_sAddress2 = final.Address2;

			if (this.m_bTrace) SetTrace(atoms, tokens, clauses, addresses, final, null);
		}

		private void RejectInvalid(ParsedAddress final)
		{
			// Some address types are not permitted by MCL
			if (AddressType.Highway == final.Type)
			{
				if ((null == final.StreetNumber) || ("" == final.StreetNumber))
				throw new ApplicationException("Highway addresses not permitted");
			}
			if (AddressType.Hotel == final.Type)
				throw new ApplicationException("Hotel addresses not permitted");
			if (AddressType.Parking == final.Type)
				throw new ApplicationException("Parking addresses not permitted");
		}

		private void SetWorkingAddress()
		{
			//Regex scrubchars = new Regex(GetScrubCharRegEx()); //clean up the string
			//Regex space_comp = new Regex(@" +");

			string working_string = this.m_sAddress;
			working_string = StripOutFraction(working_string);

			working_string = scrubchars.Replace(working_string, " ");
			working_string = space_comp.Replace(working_string, " ");
			working_string = working_string.ToUpper();
			this.m_sWorking = working_string.Trim();

			// OPM # 11995
			int nSpIdx = this.m_sWorking.IndexOf(' ');
			if (nSpIdx > 0)
			{
				string sFirst = this.m_sWorking.Substring(0, nSpIdx);
				int nDashIdx = sFirst.IndexOf('-');
				if (nDashIdx >= 0) this.m_sDashHack = sFirst;
			}
		}

		private string StripOutFraction(string inString)
		{
			// Remove fractional part of the streetnumber
			string sStripped = inString.Replace("- 1/2", "");
			sStripped = sStripped.Replace("-1/2", "");
			sStripped = sStripped.Replace("1/2", "");
			return sStripped;
		}

		private static string GetScrubCharRegEx()
		{
			StringBuilder sb = new StringBuilder(30);
			sb.Append(@"[^ ");
			foreach (char c in StreetParser.Symbols)
			{
				sb.Append(@"\");
				sb.Append(c);
			}
			sb.Append(@"A-Za-z0-9]");

			return sb.ToString();
		}

		private void SetTrace(Atom[] atoms, Token[] tokens, Clause[] clauses, ParsedAddress[] addresses, ParsedAddress chosen, string exceptionMessage)
		{
			System.Text.StringBuilder sb = new System.Text.StringBuilder();
			sb.Append("<StreetParserResults>\n");
			sb.Append("<InputAddress>" + this.m_sAddress + "</InputAddress>\n");
			sb.Append("<WorkingAddress>" + this.m_sWorking + "</WorkingAddress>\n");
			if (SafeString.HasValue(this.m_sDashHack))
			{
				sb.Append("<DashHack memo='OPM 11995'>" + this.m_sDashHack + "</DashHack>\n");
			}
			if (null != exceptionMessage)
				sb.Append("<Exception>" + exceptionMessage + "</Exception>\n");
			if (null != chosen)
			{
				sb.Append("<ChosenFormat>\n");
				sb.Append(chosen.ToString());
				sb.Append("</ChosenFormat>\n");
			}
			sb.Append("<Details>\n");
			if (null != atoms) sb.Append(Atom.Serialize(atoms) + "\n");
			if (null != tokens) sb.Append(Token.Serialize(tokens) + "\n");
			if (null != clauses) sb.Append(Clause.Serialize(clauses) + "\n");
			if (null != addresses) sb.Append(ParsedAddress.Serialize(addresses) + "\n");
			sb.Append("</Details>\n");
			sb.Append("</StreetParserResults>\n");

			this.m_sTrace = sb.ToString();
		}

		#endregion
	}
}
