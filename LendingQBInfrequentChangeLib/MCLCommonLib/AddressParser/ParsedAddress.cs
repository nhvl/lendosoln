using System;
using System.Collections;

namespace CommonLib.AddressParser
{
	// NOTE: These should be placed in order of preference
	internal enum AddressType {Normal, POB, Hotel, Rural, Highway, Military, Parking, Undefined}

	internal abstract class ParsedAddress
	{
		static protected int s_nID = 0;

		#region Member data
		protected int m_nID;
		protected AddressType m_type = AddressType.Undefined;
		protected string m_sAddress2 = "";
		protected string m_sNumber = "";
		protected string m_sPreDir = "";
        protected string m_sPostDir = "";
		protected string m_sName = "";
		protected string m_sType = "";
		protected string m_sApt = "";
		#endregion

		public ParsedAddress()
		{
			this.m_nID = s_nID++;
		}

		public AddressType Type
		{
			get{return this.m_type;}
		}

		public string Address2
		{
			get{return this.m_sAddress2;}
		}

		public string StreetNumber
		{
			get{return this.m_sNumber;}
		}

		public string StreetDirection
		{
			get{return this.m_sPreDir;}
		}

        public string StreetPostDirection
        {
            get { return this.m_sPostDir; }
        }

		public string StreetName
		{
			get{return this.m_sName;}
		}

		public string StreetType
		{
			get{return this.m_sType;}
		}

		public string Apartment
		{
			get{return this.m_sApt;}
		}

		public virtual bool Better(ParsedAddress test)
		{
			int nThisRank = (int)(this.Type);
			int nThatRank = (int)(test.Type);
			return (nThisRank > nThatRank);
		}

		public override string ToString()
		{
			System.Text.StringBuilder sb = new System.Text.StringBuilder();
			sb.Append("<ParsedAddress id='" + this.m_nID.ToString() + "'");
			sb.Append(" type='" + this.Type.ToString() + "'>\n");
			if ("" != this.StreetNumber)
				sb.Append("<StreetNumber>" + this.StreetNumber + "</StreetNumber>\n");
			if ("" != this.StreetDirection)
				sb.Append("<StreetDirection>" + this.StreetDirection + "</StreetDirection>\n");
			if ("" != this.StreetName)
				sb.Append("<StreetName>" + this.StreetName + "</StreetName>\n");
			if ("" != this.StreetType)
				sb.Append("<StreetType>" + this.StreetType + "</StreetType>\n");
            if ("" != this.StreetPostDirection)
                sb.Append("<StreetPostDirection>" + this.StreetPostDirection + "</StreetPostDirection>\n");
			if ("" != this.Apartment)
				sb.Append("<Apartment>" + this.Apartment + "</Apartment>\n");
			if ("" != this.Address2)
				sb.Append("<Address2>" + this.Address2 + "</Address2>\n");
			sb.Append("</ParsedAddress>\n");

			return sb.ToString();
		}

		static public string Serialize(ParsedAddress[] addresses)
		{
			System.Text.StringBuilder sb = new System.Text.StringBuilder();
			sb.Append("<ParsedAddressList>");
			foreach (ParsedAddress pa in addresses) sb.Append(pa.ToString()); 
			sb.Append("</ParsedAddressList>");

			return sb.ToString();
		}

		protected void CheckValid()
		{
			if (("" == this.m_sNumber) || ("" == this.m_sName))
				throw new ApplicationException("Address must have street number and name");
		}

		protected void SetStreetType(Token token)
		{
			string sType = token.Key;
			for (int i=0; i<Token.STREETTYPE_KEYS.Length/2; ++i)
			{
				if (sType == Token.STREETTYPE_KEYS[i,0])
				{
					this.m_sType = Token.STREETTYPE_KEYS[i,1];
					return;
				}
			}
		}

		protected void SetApartment(Token[] tokens)
		{
			if (1 == tokens.Length)
			{
				// apartment number may be part of the token,
				// but the token can be just a single identifier (e.g. PENTHOUSE)
				Token token = tokens[0];
				string sToken = token.ToString();
				if ("INFERRED" == token.Key)
					this.m_sApt = sToken;
				else
					this.m_sApt = (token.Key == sToken) ? sToken : sToken.Substring(token.Key.Length);
			}
			else if (2 == tokens.Length)
			{
				if (TokenType.Ordinal == tokens[0].Type)
				{
					int nApt = Token.NumericFromOrdinal(tokens[0]);
					this.m_sApt = nApt.ToString();
				}
				else
				{
					this.m_sApt = tokens[1].ToString();
				}
			}
			else if (3 == tokens.Length)
			{
				// must be of type 2-e
				this.m_sApt = tokens[0].ToString() + tokens[1].ToString() + tokens[2].ToString();
			}
			else if (4 == tokens.Length)
			{
				// must be of type apt 2-e
				this.m_sApt = tokens[1].ToString() + tokens[2].ToString() + tokens[3].ToString();
			}
		}

        protected static string InterpretDirection(string unparsed)
        {
            switch (unparsed)
            {
                case "N":
                case "NO":
                case "NORTH":
                    return "N";

                case "NE":
                case "NORTHEAST":
                case "NORTH EAST":
                    return "NE";

                case "E":
                case "EAST":
                    return "E";

                case "SE":
                case "SOUTHEAST":
                case "SOUTH EAST":
                    return "SE";

                case "S":
                case "SO":
                case "SOUTH":
                    return "S";

                case "SW":
                case "SOUTHWEST":
                case "SOUTH WEST":
                    return "SW";

                case "W":
                case "WEST":
                    return "W";

                case "NW":
                case "NORTHWEST":
                case "NORTH WEST":
                    return "NW";
            }

            return "";
        }

        protected void SetDirection(Token token)
        {
            this.m_sPreDir = InterpretDirection(token.Key);
        }

        protected void SetPostDirection(Token token)
        {
            this.m_sPostDir = InterpretDirection(token.Key);
        }
	}

	internal class NormalAddress : ParsedAddress
	{
		private int m_nQuality = 0;
		private bool m_bStreetTypeBeforeStreetName = false;

		public NormalAddress(ArrayList clauseList)
		{
			bool bFirst = true;
			bool bNumberDone = false;
			bool bDirDone = false;
            bool bPostDirDone = false;
			bool bNameStarted = false;
			bool bNameDone = false;
			bool bTypeDone = false;
			bool bApartmentDone = false;
			int cDirs = 0;
			string sLastNameItem = "";
			bool bAbbreviationLastInName = false;
			bool bSymbolLastInName = false;
			foreach (Clause clause in clauseList)
			{
				Token[] tokens = clause.Tokens;
				switch (clause.Type)
				{
					case ClauseType.Abbreviation:
						if (!bNumberDone || bNameDone)
							throw new ApplicationException("Abbreviation at wrong place");
						if (bNameStarted) base.m_sName += " ";
						base.m_sName += tokens[0].ToString();
						bNameStarted = true;
						this.m_nQuality += 1; //prefer this to a street type
						bAbbreviationLastInName = true;
						break;

					case ClauseType.AlphaNumeric:
					case ClauseType.Numeric:
					case ClauseType.Text:
						if (!bNumberDone)
						{
							if (ClauseType.Text == clause.Type)
								throw new ApplicationException("Street number cannot be pure text");
							base.m_sNumber = tokens[0].ToString();
							bNumberDone = true;
							this.m_nQuality += (ClauseType.Numeric == clause.Type) ? 2 : 1;
						}
						else if (!bNameDone)
						{
							if (bNameStarted) base.m_sName += " ";
							sLastNameItem = tokens[0].ToString();
							base.m_sName += sLastNameItem;
							bNameStarted = true;
							bAbbreviationLastInName = false;
							bSymbolLastInName = false;
						}
						else if (!bApartmentDone)
						{
							base.m_sApt = tokens[0].ToString();
							bApartmentDone = true;
                            //If the apartment looks like a direction only, we don't want to increase quality because of it;
                            //we might be looking at a post-directional token.
                            if(InterpretDirection(tokens[0].ToString()) == "") this.m_nQuality += 1;
						}
						else
							throw new ApplicationException("Too many clauses");
						break;

					case ClauseType.Symbol:
						if (!bNameStarted || bNameDone)
							throw new ApplicationException("Symbol can only appear in a street name");
						if ("-" != clause.ToString())
							throw new ApplicationException("Only hyphen symbol can appear in name");
						base.m_sName += clause.ToString();
						bSymbolLastInName = true;
						break;

					case ClauseType.Direction:
						if (2 <= cDirs) throw new ApplicationException("More than two directions");
                        //Are we a pre-direction?
                        if (!bDirDone && bNumberDone && !bNameStarted)
                        {
                            base.SetDirection(tokens[0]);
                            bDirDone = true;
                            this.m_nQuality += 2;
                        }

                        //No, then are we a post-direction?
                        if(!bPostDirDone && bNameDone)
                        {
                            base.SetPostDirection(tokens[0]);
                            this.m_nQuality += 1;
                            bPostDirDone = true;
                        }
						if (bNameStarted && !bNameDone)  this.m_nQuality += 2;
						if (bNameStarted && bAbbreviationLastInName)
							throw new ApplicationException("Abbreviation cannot be last in street name");
						if (bNameStarted && bSymbolLastInName)
							throw new ApplicationException("Symbol cannot be last in street name");
						if (bNameStarted) bNameDone = true;
						++cDirs;
						break;

					case ClauseType.StreetType:
						if (bFirst) throw new ApplicationException("Street type cannot be first clause");
						if (bTypeDone) throw new ApplicationException("More than one street type");
						SetStreetType(tokens[0]);
						bTypeDone = true;
						this.m_bStreetTypeBeforeStreetName = !bNameStarted;
						if (bNameStarted && !bNameDone)  this.m_nQuality += 2;
						if (bNameStarted && bAbbreviationLastInName)
							throw new ApplicationException("Abbreviation cannot be last in street name");
						if (bNameStarted && bSymbolLastInName)
							throw new ApplicationException("Symbol cannot be last in street name");
						if (bNameStarted) bNameDone = true;
						this.m_nQuality += 1;
						break;

					case ClauseType.Apartment:
						if (bFirst) throw new ApplicationException("Apartment cannot be first clause");
						if (bApartmentDone) throw new ApplicationException("More than one apartment");
						SetApartment(tokens);
						bApartmentDone = true;
						if (bNameStarted && !bNameDone)  this.m_nQuality += 2;
						if (1 < tokens.Length) this.m_nQuality += 1;
						if (bNameStarted && bAbbreviationLastInName)
							throw new ApplicationException("Abbreviation cannot be last in street name");
						if (bNameStarted && bSymbolLastInName)
							throw new ApplicationException("Symbol cannot be last in street name");
						if (bNameStarted) bNameDone = true;
                        if (InterpretDirection(tokens[0].ToString()) == "")
                        {
                            this.m_nQuality += 1;
                        }
						break;

					default:
						throw new ApplicationException("Invalid clause in normal address");
				}

				bFirst = false;
			}

			if (bNameStarted && !bNameDone)
			{
				if (Token.IsAbbreviation(sLastNameItem))
					throw new ApplicationException("Abbreviation cannot be last in street name");

				this.m_nQuality += 2;
			}
			if (bNameStarted && bAbbreviationLastInName)
				throw new ApplicationException("Abbreviation cannot be last in street name");
			if (bNameStarted && bSymbolLastInName)
				throw new ApplicationException("Symbol cannot be last in street name");
			if (bNameStarted) bNameDone = true;

			base.CheckValid();
			
			// Handle special cases:
			if (this.m_sName.StartsWith("PORT AU ")) this.m_nQuality += 1;

			base.m_type = AddressType.Normal;
		}

		public override bool Better(ParsedAddress test)
		{
			if (test is NormalAddress)
			{
				NormalAddress testAddress = (NormalAddress)test;
				int nThisQuality = this.Quality();
				int nTestQuality = testAddress.Quality();
				if ((this.StreetType.Length > 0) && (testAddress.StreetType.Length > 0)) // both have street types
				{
					// give a demerit to an address if its street type preceeds the street name
					// only when both have street types
					if (this.m_bStreetTypeBeforeStreetName) --nThisQuality;
					if (testAddress.m_bStreetTypeBeforeStreetName) --nTestQuality;
				}
				return (nThisQuality > nTestQuality);
			}
			else
			{
				return !test.Better(this);
			}
		}

		private int Quality()
		{
			int nQual = this.m_nQuality;
			bool bHasType = false;
			bool bHasApt = false;
			bHasType = ("" != base.m_sType);
			bHasApt = ("" != base.m_sApt);
				
			// give a demerit if both streettype and apartment are streettypes
			// because the streetype was probably part of the streetname
			if (bHasType && bHasApt)
			{
				bool bAptIsType = false;
				for (int i=0; i<Token.STREETTYPE_KEYS.Length/2; ++i)
				{
					if (Token.STREETTYPE_KEYS[i,0] == base.m_sApt)
					{
						bAptIsType = true;
						break;
					}
				}
				if (bAptIsType) nQual -= 2;
			}

			return nQual;
		}
	}

	internal class HighwayAddress : ParsedAddress
	{
		public const string Key = "HWY";
		private int m_nQuality = 0;

		public HighwayAddress(Clause hwy)
		{
			HandleHwyClause(hwy);
			base.CheckValid();
		}

		public HighwayAddress(Clause number, Clause hwy)
		{
			HandleNumAndHwyClauses(number, hwy);
			base.CheckValid();
		}

		public HighwayAddress(ArrayList clauses)
		{
			if (1 == clauses.Count)
			{
				HandleHwyClause((Clause)clauses[0]);
				base.CheckValid();
				return;
			}
			else if (2 == clauses.Count)
			{
				Clause Clause0 = (Clause)clauses[0];
				Clause Clause1 = (Clause)clauses[1];
				if (ClauseType.Highway == Clause1.Type)
					HandleNumAndHwyClauses(Clause0, Clause1);
				else if (ClauseType.Highway == Clause0.Type)
					HandleHwyAndAptClauses(Clause0, Clause1);
				base.CheckValid();
				return;
			}
			else if (3 == clauses.Count)
			{
				Clause Clause0 = (Clause)clauses[0];
				Clause Clause1 = (Clause)clauses[1];
				Clause Clause2 = (Clause)clauses[2];
				HandleNumAndHwyAndAptClauses(Clause0, Clause1, Clause2);
				base.CheckValid();
				return;
			}

			throw new ApplicationException("Invalid Hwy address");
		}

		public override bool Better(ParsedAddress test)
		{
			if (test is HighwayAddress)
			{
				HighwayAddress testAddress = (HighwayAddress)test;
				return (this.m_nQuality > testAddress.m_nQuality);
			}
			else
			{
				return base.Better(test);
			}
		}

		private void HandleHwyClause(Clause hwy)
		{
			if (ClauseType.Highway != hwy.Type)
				throw new ApplicationException("Highway clause expected");

			Token[] tokens = hwy.Tokens;
			if (1 == tokens.Length)
			{
				// highway number should be part of the token
				Token token = tokens[0];
				string sToken = token.ToString();
				if (token.Key == sToken)
					throw new ApplicationException("No hwy number");

				string sNumber = sToken.Substring(token.Key.Length);
				base.m_sNumber = sNumber;
				base.m_sName = HighwayAddress.Key + " " + sNumber;
				this.m_nQuality += 1;
			}
			else if (2 == tokens.Length)
			{
				Token numToken = tokens[1];
				string sNumber = numToken.ToString();
				base.m_sNumber = sNumber;
				base.m_sName = HighwayAddress.Key + " " + sNumber;
				this.m_nQuality += 1;
			}
			else if (3 == tokens.Length) 
			{
				if (TokenType.Symbol == tokens[1].Type)
				{
					Token numToken = tokens[2];
					string sNumber = numToken.ToString();
					base.m_sNumber = sNumber;
					base.m_sName = HighwayAddress.Key + " " + base.m_sNumber;
					this.m_nQuality += 1;
				}
				else
				{
					Token numToken = tokens[1];
					Token dirToken = tokens[2];
					string sNumber = numToken.ToString();
					base.m_sNumber = sNumber + Token.DirectionAbbreviation(dirToken.Key);
					base.m_sName = HighwayAddress.Key + " " + base.m_sNumber;
					this.m_nQuality += 2;
				}
			}
			else
			{
				throw new ApplicationException("Invalid highway address");
			}

			base.m_type = AddressType.Highway;
		}

		private void HandleNumAndHwyClauses(Clause number, Clause hwy)
		{
			HandleHwyClause(hwy);

			Token[] tokens = number.Tokens;
			if (1 != tokens.Length)
				throw new ApplicationException("Only one token permitted for hwy house number");

			base.m_sNumber = tokens[0].ToString();
		}

		private void HandleHwyAndAptClauses(Clause hwy, Clause apt)
		{
			if (ClauseType.Apartment != apt.Type)
				throw new ApplicationException("Expecting apartment clause");

			HandleHwyClause(hwy);
			base.SetApartment(apt.Tokens);
		}

		private void HandleNumAndHwyAndAptClauses(Clause number, Clause hwy, Clause apt)
		{
			if (ClauseType.Apartment != apt.Type)
				throw new ApplicationException("Expecting apartment clause");

			HandleNumAndHwyClauses(number, hwy);
			base.SetApartment(apt.Tokens);
		}
	}

	internal class HotelAddress : ParsedAddress
	{
		private int m_nQuality = 0;

		public HotelAddress(Clause hotel)
		{
			SetHotel(hotel);

			base.CheckValid();
			base.m_type = AddressType.Hotel;
		}

		public HotelAddress(ArrayList clauseList)
		{
			bool bFirst = true;
			bool bHotelDone = false;
			bool bNumberDone = false;
			bool bDirDone = false;
			bool bNameStarted = false;
			bool bNameDone = false;
			bool bTypeDone = false;
			bool bApartmentDone = false;
			int cDirs = 0;
			bool bSymbolLastInName = false;
			bool bAbbreviationLastInName = false;
			foreach (Clause clause in clauseList)
			{
				Token[] tokens = clause.Tokens;
				switch (clause.Type)
				{
					case ClauseType.Abbreviation:
						if (bNameDone)
							throw new ApplicationException("Abbreviation at wrong place");
						if (bNameStarted) base.m_sName += " ";
						base.m_sName += tokens[0].ToString();
						bNumberDone = true;
						bNameStarted = true;
						this.m_nQuality += 2;
						bAbbreviationLastInName = true;
						break;

					case ClauseType.AlphaNumeric:
					case ClauseType.Numeric:
					case ClauseType.Text:
						if (!bNumberDone)
						{
							if (ClauseType.Text == clause.Type)
								throw new ApplicationException("Street number cannot be pure text");
							base.m_sNumber = tokens[0].ToString();
							bNumberDone = true;
							this.m_nQuality += (ClauseType.Numeric == clause.Type) ? 2 : 1;
						}
						else if (!bNameDone)
						{
							if (!bNameStarted && bHotelDone)
							{
								base.m_sAddress2 = this.m_sName;
								base.m_sName = "";
							}
							if (bNameStarted) this.m_sName += " ";
							base.m_sName += tokens[0].ToString();
							bAbbreviationLastInName = false;
							bSymbolLastInName = false;
							bNameStarted = true;
						}
						else if (!bApartmentDone)
						{
							base.m_sApt = tokens[0].ToString();
							bApartmentDone = true;
							this.m_nQuality += 1;
						}
						else
							throw new ApplicationException("Too many clauses");
						break;

					case ClauseType.Symbol:
						if (!bNameStarted || bNameDone)
							throw new ApplicationException("Symbol can only appear in a street name");
						if ("-" != clause.ToString())
							throw new ApplicationException("Only hyphen symbol can appear in name");
						base.m_sName += clause.ToString();
						bSymbolLastInName = true;
						break;

					case ClauseType.Direction:
						if (2 <= cDirs) throw new ApplicationException("More than two directions");
						if (bNameStarted) throw new ApplicationException("Only pre-dir permitted");
						if (!bDirDone)
						{
							base.SetDirection(tokens[0]);
							bDirDone = true;
							this.m_nQuality += 2;
						}
						if (bNameStarted && !bNameDone)  this.m_nQuality += 2;
						if (bNameStarted && bAbbreviationLastInName)
							throw new ApplicationException("Abbreviation cannot be last in street name");
						if (bNameStarted && bSymbolLastInName)
							throw new ApplicationException("Symbol cannot be last in street name");
						if (bNameStarted) bNameDone = true;
						++cDirs;
						break;

					case ClauseType.StreetType:
						if (bFirst) throw new ApplicationException("Street type cannot be first clause");
						if (bTypeDone) throw new ApplicationException("More than one street type");
						if (!bNumberDone) throw new ApplicationException("Street type cannot preceed street number");
						SetStreetType(tokens[0]);
						bTypeDone = true;
						if (bNameStarted && !bNameDone)  this.m_nQuality += 2;
						if (bNameStarted && bAbbreviationLastInName)
							throw new ApplicationException("Abbreviation cannot be last in street name");
						if (bNameStarted && bSymbolLastInName)
							throw new ApplicationException("Symbol cannot be last in street name");
						if (bNameStarted) bNameDone = true;
						this.m_nQuality += 1;
						break;

					case ClauseType.Apartment:
						if (bApartmentDone) throw new ApplicationException("More than one apartment");
						SetApartment(tokens);
						bApartmentDone = true;
						if (bNameStarted && !bNameDone)  this.m_nQuality += 2;
						if (1 < tokens.Length) this.m_nQuality += 1;
						if (bNameStarted && bAbbreviationLastInName)
							throw new ApplicationException("Abbreviation cannot be last in street name");
						if (bNameStarted && bSymbolLastInName)
							throw new ApplicationException("Symbol cannot be last in street name");
						if (bNameStarted) bNameDone = true;
						this.m_nQuality += (3 > this.m_sApt.Length) ? 2 : 1;
						break;

					case ClauseType.Hotel:
						if (bHotelDone) throw new ApplicationException("Can only have one hotel clause");
						if (bNumberDone && !bNameStarted)
						{
							if (bApartmentDone) throw new ApplicationException("More than one apartment");
							base.m_sApt = base.m_sNumber;
							base.m_sNumber = "";
							bApartmentDone = true;
							bNumberDone = false;
						}
						bHotelDone = true;
						this.m_nQuality += 1;
						if (bNameStarted && !bNameDone)  this.m_nQuality += 2;
						if (bNameStarted && bAbbreviationLastInName)
							throw new ApplicationException("Abbreviation cannot be last in street name");
						if (bNameStarted && bSymbolLastInName)
							throw new ApplicationException("Symbol cannot be last in street name");
						if (bNameStarted)
						{
							bNameDone = true;
							base.m_sAddress2 = clause.ToString();
						}
						else 
						{
							base.m_sName = clause.ToString();
						}
						break;

					default:
						throw new ApplicationException("Invalid clause in normal address");
				}

				bFirst = false;
			}
			if (!bHotelDone) throw new ApplicationException("Missing hotel clause");

			if (bNameStarted && !bNameDone)  this.m_nQuality += 2;
			if (bNameStarted && bAbbreviationLastInName)
				throw new ApplicationException("Abbreviation cannot be last in street name");
			if (bNameStarted && bSymbolLastInName)
				throw new ApplicationException("Symbol cannot be last in street name");
			if (bNameStarted) bNameDone = true;

			base.CheckValid();
			base.m_type = AddressType.Hotel;
		}

		public override bool Better(ParsedAddress test)
		{
			if (test is HotelAddress)
			{
				HotelAddress testAddress = (HotelAddress)test;
				return (this.Quality() > testAddress.Quality());
			}
			else if (test is NormalAddress)
			{
				// if the normal address is full, then it is better
				string sNormStrType = test.StreetType;
				if ("" != test.StreetType) return false;
				
				// if this has full information then it is better
				return ("" != this.m_sAddress2);
			}
			else
			{
				return base.Better(test);
			}
		}

		private int Quality()
		{
			int nQual = this.m_nQuality;
			if ("" == this.m_sAddress2)
			{
				// hotel name is used as street address,
				// so give demerits for any non-name elements
				if ("" != base.m_sNumber) nQual -= 1;
				if ("" != base.m_sPreDir) nQual -= 1;
				if ("" != base.m_sType) nQual -= 1;
			}

			return nQual;
		}

		private void SetHotel(Clause hotel)
		{
			if (ClauseType.Hotel != hotel.Type)
				throw new ApplicationException("Hotel clause expected");

			base.m_sName = hotel.ToString();
		}

		private void SetRoomAsApartment(Clause room)
		{
			if (ClauseType.Apartment != room.Type)
				throw new ApplicationException("Apartment clause expected");

			Token[] tokens = room.Tokens;

			if (1 == tokens.Length)
			{
				// apartment number may be part of the token,
				// but the token can be just a single identifier (e.g. PENTHOUSE)
				Token token = tokens[0];
				string sToken = token.ToString();
				base.m_sApt = (token.Key == sToken) ? sToken : sToken.Substring(token.Key.Length);
			}
			else
			{
				// last token is the room number
				Token numToken = tokens[tokens.Length - 1];
				base.m_sApt = numToken.ToString();
			}
		}
	}

	internal class MilitaryAddress : ParsedAddress
	{
		public MilitaryAddress(Clause milAddress)
		{
			if (ClauseType.Military != milAddress.Type)
				throw new ApplicationException("Military clause expected");

			Token[] tokens = milAddress.Tokens;
			if (1 == tokens.Length)
			{
				// barrack number should be part of the token
				Token token = tokens[0];
				string sToken = token.ToString();
				if (token.Key == sToken)
					throw new ApplicationException("No barrack number");

				string sNumber = sToken.Substring(token.Key.Length);
				base.m_sNumber = sNumber;
				base.m_sName = token.Key + " " + sNumber;
			}
			else if (2 == tokens.Length)
			{
				if (TokenType.Military == tokens[0].Type)
					base.m_sNumber = tokens[1].ToString();
				else
					base.m_sNumber = tokens[0].ToString();
				base.m_sName = milAddress.ToString();
			}
			else
				throw new ApplicationException("Military address can only have two tokens");

			base.CheckValid();
			base.m_type = AddressType.Military;
		}
	}

	internal class ParkingAddress : ParsedAddress
	{
		public ParkingAddress(Clause lot)
		{
			if (ClauseType.Parking != lot.Type)
				throw new ApplicationException("Parking clause expected");

			string sKey = "LOT";
			string sNumber = "";
			Token[] tokens = lot.Tokens;
			if (1 == tokens.Length)
			{
				// lot number should be part of the token
				Token token = tokens[0];
				string sToken = token.ToString();
				if (token.Key == sToken)
					throw new ApplicationException("No lot number");

				sNumber = sToken.Substring(token.Key.Length);
				sKey = token.Key;
			}
			else
			{
				// rural number is last token
				sKey = tokens[0].Key;
				sNumber = tokens[tokens.Length - 1].ToString();
			}

			for (int i=0; i<Token.PARKING_KEYS.Length/2; ++i)
			{
				if (sKey == Token.PARKING_KEYS[i,0])
				{
					sKey = Token.PARKING_KEYS[i,1];
					break;
				}
			}

			base.m_sName = sKey;
			base.m_sApt = sNumber;
			base.m_type = AddressType.Parking;
		}
	}

	internal class POBAddress : ParsedAddress
	{
		public const string Key = "POB";

		public POBAddress(Clause pob)
		{
			HandlePOBClause(pob);
			base.CheckValid();
		}

		public POBAddress(Clause preNum, Clause pob)
		{
			if ((ClauseType.Numeric != preNum.Type) && (ClauseType.AlphaNumeric != preNum.Type))
				throw new ApplicationException("Invalid POB address");

			HandlePOBClause(pob);

			if (preNum.ToString() != base.m_sNumber)
				throw new ApplicationException("Invalid POB address");

			base.CheckValid();
		}

		private void HandlePOBClause(Clause pob)
		{
			if (ClauseType.POB != pob.Type)
				throw new ApplicationException("POB clause expected");

			Token[] tokens = pob.Tokens;
			if (1 == tokens.Length)
			{
				// pob number should be part of the token
				Token token = tokens[0];
				string sToken = token.ToString();
				if (token.Key == sToken)
					throw new ApplicationException("No pob number");

				base.m_sNumber = sToken.Substring(token.Key.Length);
				base.m_sName = POBAddress.Key + " " + this.m_sNumber;
			}
			else
			{
				// pob number is last token
				base.m_sNumber = tokens[tokens.Length - 1].ToString();
				base.m_sName = POBAddress.Key + " " + this.m_sNumber;
			}

			this.m_type = AddressType.POB;
		}
	}

	internal class RuralAddress : ParsedAddress
	{
		public const string Key = "RT";
		public const string Box = "BOX";

		public RuralAddress(Clause rural)
		{
			HandleRuralClause(rural);
			base.CheckValid();
		}

		public RuralAddress(Clause number, Clause rural)
		{
			HandleNumAndRuralClauses(number, rural);
			base.CheckValid();
		}

		public RuralAddress(ArrayList clauses)
		{
			if (1 == clauses.Count)
			{
				HandleRuralClause((Clause)clauses[0]);
			}
			else if (2 == clauses.Count)
			{
				if (ClauseType.POB == ((Clause)clauses[1]).Type)
					HandleRuralAndPOBClauses((Clause)clauses[0], (Clause)clauses[1]);
				else if (ClauseType.POB == ((Clause)clauses[0]).Type)
					HandleRuralAndPOBClauses((Clause)clauses[1], (Clause)clauses[0]);
				else
					HandleNumAndRuralClauses((Clause)clauses[0], (Clause)clauses[1]);
			}
			else if (3 == clauses.Count)
			{
				if (ClauseType.Rural == ((Clause)clauses[1]).Type)
				{
					HandleNumAndRuralClauses((Clause)clauses[0], (Clause)clauses[1]);
					AddPOBClause((Clause)clauses[2]);
				}
				else if (ClauseType.POB == ((Clause)clauses[1]).Type)
				{
					HandleRuralClause((Clause)clauses[2]);
					AddPOBClauses((Clause)clauses[0], (Clause)clauses[1]);
				}
			}
			else
			{
				throw new ApplicationException("Invalid Rural address");
			}

			base.CheckValid();
		}

		private void HandleRuralClause(Clause rural)
		{
			if (ClauseType.Rural != rural.Type)
				throw new ApplicationException("Rural clause expected");

			Token[] tokens = rural.Tokens;
			if (1 == tokens.Length)
			{
				// rural number should be part of the token
				Token token = tokens[0];
				string sToken = token.ToString();
				if (token.Key == sToken)
					throw new ApplicationException("No rural number");

				base.m_sNumber = sToken.Substring(token.Key.Length);
				base.m_sName = RuralAddress.Key + " " + this.m_sNumber;
			}
			else
			{
				// rural number is last token
				base.m_sNumber = tokens[tokens.Length - 1].ToString();
				base.m_sName = RuralAddress.Key + " " + this.m_sNumber;
			}

			base.m_type = AddressType.Rural;
		}

		private void HandleNumAndRuralClauses(Clause number, Clause rural)
		{
			HandleRuralClause(rural);

			Token[] tokens = number.Tokens;
			if (1 != tokens.Length)
				throw new ApplicationException("Only one token permitted for rural house number");

			base.m_sNumber = number.ToString();
		}

		private void HandleRuralAndPOBClauses(Clause rural, Clause pob)
		{
			HandleRuralClause(rural);
			AddPOBClause(pob);
		}

		private void AddPOBClause(Clause pob)
		{
			POBAddress pobAdd = new POBAddress(pob);
			base.m_sName += " " + pobAdd.StreetName;
			base.m_sName = base.m_sName.Replace(POBAddress.Key, RuralAddress.Box);
		}

		private void AddPOBClauses(Clause num, Clause pob)
		{
			POBAddress pobAdd = new POBAddress(num, pob);
			base.m_sName += " " + pobAdd.StreetName;
			base.m_sName = base.m_sName.Replace(POBAddress.Key, RuralAddress.Box);
		}
	}
}
