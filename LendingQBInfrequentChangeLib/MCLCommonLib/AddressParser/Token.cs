using System;
using System.Collections;

namespace CommonLib.AddressParser
{
	internal enum TokenType {Text, Numeric, AlphaNumeric, Abbreviation, Symbol, Ordinal, StreetType, POB, Rural, Highway, Military, Parking, Hotel, Apartment, Direction}

	internal class Token : IComparable
	{
		static public string[] ABBREVIATION_KEYS = new string[] {"ST", "LA"};
		static public string[] ORDINAL_KEYS  = new string[] {"FIRST", "ST", "SECOND", "ND", "THIRD", "RD", "FOURTH", "TH"};
		static public string[] RURAL_KEYS  = new string[] {"RR", "RT", "RTE", "ROUTE", "HC", "HCR", "PSC"};
		static public string[,] PARKING_KEYS  = new string[,] {{"LOT", "LOT"}, {"PIER", "PIER"}, {"SLIP", "SLIP"}, {"SPACE", "SPC"}, {"SPC", "SPC"}, {"STOP", "STOP"}, {"TRAILER", "TRLR"}, {"TRLR", "TRLR"}};
		static public string[] COLLEGE_KEYS  = new string[] {"COLLEGE", "UNIVERSITY"};
		static public string[] HIGHWAY_KEYS  = new string[] {"HWY", "HIGHWAY", "FM"};
		static public string[] HOTEL_KEYS  = new string[] {"HOTEL", "INN", "MOTEL", "LODGE"};
		static public string[] MILITARY_KEYS  = new string[] {"BARRACK", "UNIT", "CO", "COMPANY"};
		static public string[] PO_KEYS ={"PO BOX", "P O BOX", "POBOX", "POB", "PO", "P O", "BOX"};
		static public string[] DIR_KEYS ={"N", "NO", "NORTH", "NE", "NORTHEAST", "NORTH EAST", "E", "EAST", "SE", "SOUTHEAST", "SOUTH EAST", "S", "SO", "SOUTH", "SW", "SOUTHWEST", "SOUTH WEST", "W", "WEST", "NW", "NORTHWEST", "NORTH WEST"};
		static public string [,] APT_KEYS =
		{
			{"APARTMENT", "APT", "Y"}, {"APT", "APT", "Y"}, {"BASEMENT", "BSMT", "N"}, {"BSMT", "BSMT", "N"}, {"BUILDING", "BLDG", "Y"}, {"BLDG", "BLDG", "Y"}, {"DEPARTMENT", "DEPT", "Y"}, {"DEPT", "DEPT", "Y"}, {"FLOOR", "FL", "Y"}, {"FLR", "FL", "Y"}, {"FL", "FL", "Y"}, {"FRONT", "FRNT", "N"}, {"FRNT", "FRNT", "N"}, {"HANGAR", "HNGR", "Y"}, {"HNGR", "HNGR", "Y"}, {"LOBBY", "LBBY", "N"}, {"LBBY", "LBBY", "N"}, {"LOWER", "LOWR", "N"}, {"LOWR", "LOWR", "N"}, {"OFFICE", "OFC", "Y"}, {"OFC", "OFC", "Y"}, {"PENTHOUSE", "PH", "N"}, {"PH", "PH", "N"}, {"REAR", "REAR", "N"}, {"ROOM", "RM", "Y"}, {"RM", "RM", "Y"}, {"SIDE", "SIDE", "N"}, {"SUITE", "STE", "Y"}, {"STE", "STE", "Y"}, {"UNIT", "UNIT", "Y"}, {"UPPER", "UPPR", "N"}, {"UPPR", "UPPR", "N"}
		};

		static public string [,] STREETTYPE_KEYS =
		{	{"AVENUE","AV"}, {"AVEN","AV"}, {"AVENU","AV"}, {"AVE","AV"}, {"AVN","AV"}, {"AVNUE","AV"}, {"AV","AV"},
			{"ALLEY","AL"}, {"ALLEE","AL"}, {"ALLY","AL"}, {"ALY","AL"}, {"AL","AL"},
			{"BOULEVARD","BV"}, {"BLVD","BV"}, {"BOULV","BV"}, {"BOUL","BV"}, {"BL","BV"}, {"BV", "BV"},
			{"CIRCLE","CI"}, {"CIRC","CI"}, {"CIRCL","CI"}, {"CIR","CI"}, {"CRCL","CI"}, {"CRCLE","CI"}, {"CIRCLES","CI"}, {"CI","CI"},
			{"CREEK", "CK"}, {"CK", "CK"},{"CRK","CK"}, 
			{"CENTER","CN"}, {"CENT","CN"}, {"CEN","CN"}, {"CENTR","CN"}, {"CENTRE","CN"}, {"CNTER","CN"}, {"CNTR","CN"}, {"CTR","CN"}, {"CENTERS","CN"}, {"CN", "CN"},
			{"CORNER","CO"}, {"COR","CO"}, {"CORNERS","CO"}, {"CORS","CO"},{"CN","CO"},
			{"CRESCENT","CR"}, {"CRECENT","CR"}, {"CRES","CR"}, {"CR","CR"}, {"CRESENT","CR"}, {"CRSCNT","CR"}, {"CRSENT","CR"}, {"CRSNT","CR"}, {"CREST","CR"}, 
			{"COURT", "CT"}, {"CT", "CT"}, {"CRT", "CT"}, {"COURTE", "CT"}, {"CORT", "CT"}, {"CORTE", "CT"}, {"CORTES", "CT"},
			{"CROSSING","CX"}, {"CRSSING","CX"}, {"CRSSNG","CX"}, {"XING","CX"}, {"CROSSROAD","CX"},{"CX","CX"},
			{"DRIVE","DR"}, {"DRIV","DR"}, {"DR","DR"}, {"DRV","DR"}, {"DRIVES","DR"},
			{"DALE", "DA"}, {"DA","DA"}, {"DLE", "DA"},
			{"EXPRESSWAY","EX"}, {"EXPR","EX"}, {"EXPRESS","EX"}, {"EXP","EX"}, {"EXPW","EX"}, {"EXPY","EX"}, {"EX","EX"},
			{"EXTENSION","ET"}, {"EXT","ET"}, {"EXTN","ET"}, {"EXTNSN","ET"}, {"EXTENSIONS","ET"}, {"EXTS","ET"}, {"ET", "ET"},
			{"FREEWAY","FY"}, {"FREEWY","FY"}, {"FRWAY","FY"}, {"FRWY","FY"}, {"FWY","FY"},{"FY","FY"},
			{"GARDEN","GA"}, {"GARDN","GA"}, {"GDN","GA"}, {"GRDEN","GA"}, {"GRDN","GA"}, {"GARDENS","GA"}, {"GDNS","GA"}, {"GRDNS","GA"},{"GA","GA"},
			{"GROVE","GR"}, {"GROV","GR"}, {"GRV","GR"}, {"GROVES","GR"},{"GR","GR"},
			{"HILL","HL"}, {"HL","HL"}, {"HILLS","HL"}, {"HLS","HL"},
			{"HEIGHTS","HT"}, {"HEIGHT","HT"}, {"HGTS","HT"}, {"HT","HT"}, {"HTS","HT"},
			{"HIGHWAY","HY"}, {"HIGHWY","HY"}, {"HIWAY","HY"}, {"HIWY","HY"}, {"HWAY","HY"}, {"HWY","HY"},{"HY","HY"},
			{"KNOLL","KN"}, {"KNOL","KN"}, {"KNL","KN"}, {"KNLS","KN"}, {"KNOLLS","KN"},{"KN","KN"},
			{"LANE","LN"}, {"LA","LN"}, {"LANES","LN"}, {"LN","LN"},
			{"LOOP","LP"}, {"LOOPS","LP"},{"LP","LP"},
			{"MALL","MA"}, {"MA","MA"},
			{"MANOR","MN"}, {"MNR","MN"}, {"MN","MN"},
			{"PASS","PS"}, {"PASSAGE","PS"}, {"PS", "PS"},
			{"PATH","PA"}, {"PATHS","PA"},{"PA","PA"},
			{"PIKE", "PI"}, {"PI","PI"},
			{"PARK","PK"}, {"PK","PK"}, {"PRK","PK"}, {"PARKS","PK"},
			{"PARKWAY","PY"}, {"PARKWY","PY"}, {"PKWAY","PY"}, {"PKWY","PY"}, {"PKY","PY"}, {"PARKWAYS","PY"}, {"PKWYS","PY"},{"PY","PY"},
			{"PLACE","PL"}, {"PL","PL"}, {"PLC", "PL"},
			{"PLAIN","PN"}, {"PLN","PN"}, {"PLAINES","PN"}, {"PLAINS","PN"}, {"PLNS","PN"}, {"PN", "PN"},
			{"PLAZA","PZ"}, {"PLZ","PZ"}, {"PLZA","PZ"},{"PZ","PZ"},
			{"POINT","PT"}, {"PT","PT"}, {"POINTS","PT"}, {"PTS","PT"}, {"PORT","PT"}, {"PRT","PT"}, {"PORTS","PT"}, {"PRTS","PT"},{"PT","PT"},
			{"ROAD","RD"}, {"RD","RD"}, {"RDS","RD"}, {"ROADS","RD"},
			{"RUN", "RN"}, {"RN", "RN"},
			{"RANCH","RC"}, {"RANCHES","RC"}, {"RNCH","RC"}, {"RNCHS","RC"},{"RN","RC"},
			{"ROW","RO"}, {"RO","RO"},
			{"ROUTE","RT"}, {"RT","RT"},
			{"SQUARE","SQ"}, {"SQR","SQ"}, {"SQRE","SQ"}, {"SQU","SQ"}, {"SQ","SQ"}, {"SQRS","SQ"}, {"SQUARES","SQ"},
			{"STATION","SN"}, {"STA","SN"}, {"STATN","SN"}, {"STN","SN"}, {"SN", "SN"},
			{"STREET","ST"}, {"STRAV","ST"}, {"STRAVE","ST"}, {"STRAVEN","ST"}, {"STRAVENUE","ST"}, {"STRAVN","ST"}, {"STRVN","ST"}, {"STRVNUE","ST"}, {"ST","ST"}, {"STR","ST"}, {"STRA","ST"}, {"STRT","ST"}, {"STREETS","ST"},
			{"STREAM","SM"}, {"STREME","SM"}, {"STRM","SM"}, {"SM", "SM"}, 
			{"TERRACE","TE"}, {"TERR","TE"}, {"TER","TE"},{"TE","TE"},
			{"TURNPIKE","TP"}, {"TPKE","TP"}, {"TRNPK","TP"}, {"TRPK","TP"}, {"TPK","TP"}, {"TURNPK","TP"},{"TP","TP"},
			{"TRACE","TC"}, {"TRACES","TC"}, {"TRCE","TC"}, {"TC", "TC"},
			{"TRACK","TK"}, {"TRACKS","TK"}, {"TRAK","TK"}, {"TRK","TK"}, {"TRKS","TK"}, {"TK", "TK"}, 
			{"TRAFFICWAY","TY"}, {"TRFY","TY"}, {"TY", "TY"},
			{"TRAIL","TR"}, {"TR","TR"}, {"TRAILS","TR"}, {"TRL","TR"}, {"TRLS","TR"},
			{"VIADUCT","VI"}, {"VIA","VI"}, {"VIADCT","VI"}, {"VDCT","VI"},{"VI","VI"},
			{"VISTA","VS"}, {"VIST","VS"}, {"VIS","VS"}, {"VST","VS"}, {"VSTA","VS"},{"VS","VS"},
			{"WALK","WK"}, {"WALKS","WK"},{"WK","WK"},
			{"WAY","WY"}, {"WY","WY"}, {"WAYS","WY"}
		};

		TokenType m_type;
		private Atom m_beginAtom;
		private Atom m_endAtom;
		private string m_sKey;

		private Token()
		{
		}

		static public Token[] Generate(Atom[] atoms)
		{
			// Although inefficient, separating into different GetXXXTokens
			// method calls is easier to maintian.  Since the initial input
			// string isn't extected to be long, the impact shouldn't be too bad.
			ArrayList tokenList = new ArrayList(100);
			tokenList.AddRange(GetAbbreviationTokens(atoms));
			tokenList.AddRange(GetTextTokens(atoms));
			tokenList.AddRange(GetNumericTokens(atoms));
			tokenList.AddRange(GetAlphaNumericTokens(atoms));
			tokenList.AddRange(GetOrdinalTokens(atoms));
			tokenList.AddRange(GetStreetTypeTokens(atoms));
			tokenList.AddRange(GetSymbolTokens(atoms));
			tokenList.AddRange(GetPOBTokens(atoms));
			tokenList.AddRange(GetRuralTokens(atoms));
			tokenList.AddRange(GetHighwayTokens(atoms));
			tokenList.AddRange(GetMilitaryTokens(atoms));
			tokenList.AddRange(GetParkingTokens(atoms));
			tokenList.AddRange(GetHotelTokens(atoms));
			tokenList.AddRange(GetApartmentTokens(atoms));
			tokenList.AddRange(GetDirectionTokens(atoms));

			tokenList.Sort();
			Token[] allTokens = (Token[])tokenList.ToArray(typeof(Token));
			return allTokens;
		}

		static public bool Overlap(Token first, Token second)
		{
			bool bFirstEndEarlier = first.EndIndex < second.StartIndex;
			bool bSecondEndEarlier = second.EndIndex < first.StartIndex;
			return !(bFirstEndEarlier || bSecondEndEarlier);
		}

		static public bool IsAbbreviation(string test)
		{
			foreach (string sAb in Token.ABBREVIATION_KEYS)
			{
				if (sAb == test) return true;
			}

			return false;
		}

		static public bool IsNumeric(string atom)
		{
			if ((null == atom) || ("" == atom)) return false;

			foreach (char c in atom)
			{
				if (!char.IsDigit(c)) return false;
			}

			return true;
		}

		static public bool IsText(string atom)
		{
			if ((null == atom) || ("" == atom)) return false;

			foreach (char c in atom)
			{
				if (!char.IsLetter(c)) return false;
			}

			return true;
		}

		static public bool IsAlphaNumeric(string atom)
		{
			if ((null == atom) || ("" == atom)) return false;

			bool bHasDigit = false;
			bool bHasLetter = false;
			foreach (char c in atom)
			{
				if (char.IsDigit(c)) bHasDigit = true;
				if (char.IsLetter(c)) bHasLetter = true;
				if (bHasDigit && bHasLetter) break;
			}

			return (bHasDigit && bHasLetter);
		}

		static public string DirectionAbbreviation(string dir)
		{
			string sAbbrev = "";
			switch (dir)
			{
				case "N":
				case "NO":
				case "NORTH":
					sAbbrev = "N";
					break;

				case "NE":
				case "NORTHEAST":
				case "NORTH EAST":
					sAbbrev = "NE";
					break;

				case "E":
				case "EAST":
					sAbbrev = "E";
					break;

				case "SE":
				case "SOUTHEAST":
				case "SOUTH EAST":
					sAbbrev = "SE";
					break;

				case "S":
				case "SO":
				case "SOUTH":
					sAbbrev = "S";
					break;

				case "SW":
				case "SOUTHWEST":
				case "SOUTH WEST":
					sAbbrev = "SW";
					break;

				case "W":
				case "WEST":
					sAbbrev = "W";
					break;

				case "NW":
				case "NORTHWEST":
				case "NORTH WEST":
					sAbbrev = "NW";
					break;

			}

			return sAbbrev;
		}

		#region Public methods/properties

		public TokenType Type
		{
			get{return this.m_type;}
		}

		public int StartIndex
		{
			get{return this.m_beginAtom.StartIndex;}
		}

		public int EndIndex
		{
			get{return this.m_endAtom.EndIndex;}
		}

		public int Length
		{
			get{return this.EndIndex - this.StartIndex + 1;}
		}

		public string Parent
		{
			get{return this.m_beginAtom.Parent;}
		}

		public string Key
		{
			get{return this.m_sKey;}
		}

		public override string ToString()
		{
			return this.Parent.Substring(this.StartIndex, this.Length);
		}

		static public string Serialize(Token[] tokens)
		{
			System.Text.StringBuilder sb = new System.Text.StringBuilder();
			sb.Append("<TokenList>");
			foreach (Token token in tokens)
			{
				sb.Append("<Token type='" + token.Type.ToString() + "' key='" + token.Key + "' start_index='" + token.StartIndex + "' end_index='" + token.EndIndex + "'>");
				sb.Append(token.ToString());
				sb.Append("</Token>");
			}
			sb.Append("</TokenList>");

			return sb.ToString();
		}

		int IComparable.CompareTo(object toCompare)
		{
			if (!(toCompare is Token))
				throw new ArgumentException("Cannot compare Token to " + toCompare.GetType().Name);

			Token compToken = (Token)toCompare;
			return this.StartIndex.CompareTo(compToken.StartIndex);
		}

		static public int CountNextOverlaps(int i, Token[] allTokens)
		{
			// allTokens must be sorted based upon start index
			int nCount = 0;
			for (int j=i+1; j<allTokens.Length; ++j)
			{
				if (Token.Overlap(allTokens[i], allTokens[j])) ++nCount;
				else break;
			}

			return nCount;
		}

		static public ArrayList GetNextTokens(int i, Token[] allTokens)
		{
			// allTokens must be sorted based upon start index
			ArrayList nextTokens = new ArrayList(100);
			bool bFirst = true;
			int nLow = 0;
			for (int j=i+1; j<allTokens.Length; ++j)
			{
				if (Token.Overlap(allTokens[i], allTokens[j])) continue;
				if (bFirst)
				{
					bFirst = false;
					nLow = allTokens[j].StartIndex;
				}

				if (nLow == allTokens[j].StartIndex)
					nextTokens.Add(allTokens[j]);
				else break;
			}

			return nextTokens;
		}

		static public int NumericFromOrdinal(Token token)
		{
			if (token.ToString() == token.Key)
			{
				switch (token.Key)
				{
					case "FIRST":
					case "1ST":
						return 1;

					case "SECOND":
					case "2ND":
						return 2;

					case "THIRD":
					case "3RD":
						return 3;

					case "FOURTH":
					case "4TH":
						return 4;
				}
			}
			else
			{
				string sToken = token.ToString();
				int nFirstLetter = 0;
				foreach (char c in sToken)
				{
					if (!char.IsDigit(c)) break;
					++nFirstLetter;
				}

				string sNumber = sToken.Substring(0, nFirstLetter);
				return int.Parse(sNumber);
			}

			return 0;
		}

		#endregion

		#region Private methods

		static private ArrayList GetAbbreviationTokens(Atom[] atoms)
		{
			// A symbol token is a symbol from the accepted symbol list
			ArrayList tokenList = new ArrayList(100);
			foreach (Atom atom in atoms)
			{
				foreach (string abbrev in Token.ABBREVIATION_KEYS)
				{
					if (abbrev == atom.ToString())
					{
						Token token = new Token();
						token.m_type = TokenType.Abbreviation;
						token.m_beginAtom = atom;
						token.m_endAtom = atom;
						token.m_sKey = abbrev;
						tokenList.Add(token);
					}
				}
			}

			return tokenList;
		}

		static private ArrayList GetTextTokens(Atom[] atoms)
		{
			// A text token contains only letters
			ArrayList tokenList = new ArrayList(100);
			foreach (Atom atom in atoms)
			{
				bool bAllLetters = true;
				string sAtom = atom.ToString();
				foreach (char c in sAtom)
				{
					bAllLetters = char.IsLetter(c);
					if (!bAllLetters) break;
				}
				
				if (bAllLetters)
				{
					Token token = new Token();
					token.m_type = TokenType.Text;
					token.m_beginAtom = atom;
					token.m_endAtom = atom;
					token.m_sKey = sAtom;
					tokenList.Add(token);
				}
			}

			return tokenList;
		}

		static private ArrayList GetNumericTokens(Atom[] atoms)
		{
			// A numeric token contains only digits
			ArrayList tokenList = new ArrayList(100);
			for (int i=0; i<atoms.Length; ++i)
			{
				Atom atom = atoms[i];
				string sAtom = atom.ToString();
				if (IsNumeric(sAtom))
				{
					Token token = new Token();
					token.m_type = TokenType.Numeric;
					token.m_beginAtom = atom;
					token.m_endAtom = atom;
					token.m_sKey = sAtom;
					tokenList.Add(token);
				}
			}

			return tokenList;
		}

		static private ArrayList GetAlphaNumericTokens(Atom[] atoms)
		{
			// An alphanumeric token contains a mixture of both letters and digits
			ArrayList tokenList = new ArrayList(100);
			foreach (Atom atom in atoms)
			{
				string sAtom = atom.ToString();
				if (IsAlphaNumeric(sAtom))
				{
					Token token = new Token();
					token.m_type = TokenType.AlphaNumeric;
					token.m_beginAtom = atom;
					token.m_endAtom = atom;
					token.m_sKey = sAtom;
					tokenList.Add(token);
				}
			}

			return tokenList;
		}

		static private ArrayList GetSymbolTokens(Atom[] atoms)
		{
			// A symbol token is a symbol from the accepted symbol list
			ArrayList tokenList = new ArrayList(100);
			foreach (Atom atom in atoms)
			{
				if (1 != atom.Length) continue;
				foreach (char symbol in StreetParser.Symbols)
				{
					if (symbol == atom.ToString()[0])
					{
						Token token = new Token();
						token.m_type = TokenType.Symbol;
						token.m_beginAtom = atom;
						token.m_endAtom = atom;
						token.m_sKey = symbol.ToString();
						tokenList.Add(token);

						// A symbol can also be the sole indicator of an apartment
						token = new Token();
						token.m_type = TokenType.Apartment;
						token.m_beginAtom = atom;
						token.m_endAtom = atom;
						token.m_sKey = symbol.ToString();
						tokenList.Add(token);
						break;
					}
				}
			}

			return tokenList;
		}

		static private ArrayList GetOrdinalTokens(Atom[] atoms)
		{
			ArrayList tokenList = new ArrayList(100);
			foreach (string ordIndicator in Token.ORDINAL_KEYS)
			{
				for (int i=0; i<atoms.Length; ++i)
				{
					string sAtom1 = atoms[i].ToString();
					string sAtom2 = (atoms.Length > i+1) ? atoms[i+1].ToString() : "";

					if ((2 < ordIndicator.Length) && (sAtom1 == ordIndicator))
					{
						Token token = new Token();
						token.m_type = TokenType.Ordinal;
						token.m_beginAtom = atoms[i];
						token.m_endAtom = atoms[i];
						token.m_sKey = ordIndicator;
						tokenList.Add(token);
					}
					else if ((2 == ordIndicator.Length) && (sAtom1.EndsWith(ordIndicator)))
					{
						string sPrefix = sAtom1.Substring(0, sAtom1.Length - 2);
						if (IsNumeric(sPrefix))
						{
							Token token = new Token();
							token.m_type = TokenType.Ordinal;
							token.m_beginAtom = atoms[i];
							token.m_endAtom = atoms[i];
							token.m_sKey = ordIndicator;
							tokenList.Add(token);
						}
					}
					else if ((2 == ordIndicator.Length) && (sAtom2 == ordIndicator))
					{
						if (IsNumeric(sAtom1))
						{
							Token token = new Token();
							token.m_type = TokenType.Ordinal;
							token.m_beginAtom = atoms[i];
							token.m_endAtom = atoms[i+1];
							token.m_sKey = ordIndicator;
							tokenList.Add(token);
						}
					}
				}
			}

			return tokenList;
		}

		static private ArrayList GetStreetTypeTokens(Atom[] atoms)
		{
			ArrayList tokenList = new ArrayList(100);
			for (int i=0; i<Token.STREETTYPE_KEYS.Length/2; ++i)
			{
				foreach (Atom atom in atoms)
				{
					string sAtom = atom.ToString();
					if (sAtom == Token.STREETTYPE_KEYS[i,0])
					{
						Token token = new Token();
						token.m_type = TokenType.StreetType;
						token.m_beginAtom = atom;
						token.m_endAtom = atom;
						token.m_sKey = Token.STREETTYPE_KEYS[i,0];
						tokenList.Add(token);
					}
				}
			}

			return tokenList;
		}

		static private ArrayList GetPOBTokens(Atom[] atoms)
		{
			ArrayList tokenList = new ArrayList(100);
			foreach (string pobIndicator in Token.PO_KEYS)
			{
				for (int i=0; i<atoms.Length; ++i)
				{
					string sAtom1 = atoms[i].ToString();
					string sAtom2 = (atoms.Length > i+1) ? sAtom1 + " " + atoms[i+1].ToString() : "";
					string sAtom3 = (atoms.Length > i+2) ? sAtom2 + " " + atoms[i+2].ToString() : "";

					if (sAtom1 == pobIndicator)
					{
						Token token = new Token();
						token.m_type = TokenType.POB;
						token.m_beginAtom = atoms[i];
						token.m_endAtom = atoms[i];
						token.m_sKey = pobIndicator;
						tokenList.Add(token);
					}
					else if (sAtom2 == pobIndicator)
					{
						Token token = new Token();
						token.m_type = TokenType.POB;
						token.m_beginAtom = atoms[i];
						token.m_endAtom = atoms[i+1];
						token.m_sKey = pobIndicator;
						tokenList.Add(token);
					}
					else if (sAtom3 == pobIndicator)
					{
						Token token = new Token();
						token.m_type = TokenType.POB;
						token.m_beginAtom = atoms[i];
						token.m_endAtom = atoms[i+2];
						token.m_sKey = pobIndicator;
						tokenList.Add(token);
					}
				}
			}

			return tokenList;
		}

		static private ArrayList GetRuralTokens(Atom[] atoms)
		{
			ArrayList tokenList = new ArrayList(100);
			foreach (string rrIndicator in Token.RURAL_KEYS)
			{
				foreach (Atom atom in atoms)
				{
					string sAtom = atom.ToString();
					if (sAtom.StartsWith(rrIndicator))
					{
						Token token = new Token();
						token.m_type = TokenType.Rural;
						token.m_beginAtom = atom;
						token.m_endAtom = atom;
						token.m_sKey = rrIndicator;
						tokenList.Add(token);
					}
				}
			}

			return tokenList;
		}

		static private ArrayList GetHighwayTokens(Atom[] atoms)
		{
			ArrayList tokenList = new ArrayList(100);
			foreach (string hwyIndicator in Token.HIGHWAY_KEYS)
			{
				foreach (Atom atom in atoms)
				{
					string sAtom = atom.ToString();
					if (sAtom.StartsWith(hwyIndicator))
					{
						Token token = new Token();
						token.m_type = TokenType.Highway;
						token.m_beginAtom = atom;
						token.m_endAtom = atom;
						token.m_sKey = hwyIndicator;
						tokenList.Add(token);
					}
				}
			}

			return tokenList;
		}

		static private ArrayList GetMilitaryTokens(Atom[] atoms)
		{
			ArrayList tokenList = new ArrayList(100);
			foreach (string milIndicator in Token.MILITARY_KEYS)
			{
				foreach (Atom atom in atoms)
				{
					string sAtom = atom.ToString();
					if (sAtom.StartsWith(milIndicator))
					{
						Token token = new Token();
						token.m_type = TokenType.Military;
						token.m_beginAtom = atom;
						token.m_endAtom = atom;
						token.m_sKey = milIndicator;
						tokenList.Add(token);
					}
				}
			}

			return tokenList;
		}

		static private ArrayList GetParkingTokens(Atom[] atoms)
		{
			ArrayList tokenList = new ArrayList(100);
			for (int i=0; i<Token.PARKING_KEYS.Length/2; ++i)
			{
				string parkIndicator = Token.PARKING_KEYS[i,0];
				foreach (Atom atom in atoms)
				{
					string sAtom = atom.ToString();
					if (sAtom.StartsWith(parkIndicator))
					{
						Token token = new Token();
						token.m_type = TokenType.Parking;
						token.m_beginAtom = atom;
						token.m_endAtom = atom;
						token.m_sKey = parkIndicator;
						tokenList.Add(token);
					}
				}
			}

			return tokenList;
		}

		static private ArrayList GetHotelTokens(Atom[] atoms)
		{
			ArrayList tokenList = new ArrayList(100);
			foreach (string htlIndicator in Token.HOTEL_KEYS)
			{
				foreach (Atom atom in atoms)
				{
					string sAtom = atom.ToString();
					if (sAtom == htlIndicator)
					{
						Token token = new Token();
						token.m_type = TokenType.Hotel;
						token.m_beginAtom = atom;
						token.m_endAtom = atom;
						token.m_sKey = htlIndicator;
						tokenList.Add(token);
					}
				}
			}

			return tokenList;
		}

		static private ArrayList GetApartmentTokens(Atom[] atoms)
		{
			ArrayList tokenList = new ArrayList(100);
			for (int i=0; i<Token.APT_KEYS.Length/3; ++i)
			{
				for (int j=0; j<atoms.Length; ++j)
				{
					Atom atom = atoms[j];
					string sAtom = atom.ToString();
					string sAptKey = Token.APT_KEYS[i,0];
					if (sAtom.StartsWith(sAptKey))
					{
						Token token = new Token();
						token.m_type = TokenType.Apartment;
						token.m_beginAtom = atom;
						token.m_endAtom = atom;
						token.m_sKey = sAptKey;
						tokenList.Add(token);
					}
				}
			}

			// check last atom to see whether could be an apt
			Atom lastAtom = atoms[atoms.Length - 1];
			string sLastAtom = lastAtom.ToString();
			if (Token.IsNumeric(sLastAtom) || Token.IsAlphaNumeric(sLastAtom))
			{
				Token token = new Token();
				token.m_type = TokenType.Apartment;
				token.m_beginAtom = lastAtom;
				token.m_endAtom = lastAtom;
				token.m_sKey = "INFERRED";
				tokenList.Add(token);
			}
			else if (('A' == sLastAtom.ToString()[0]) && (4 > sLastAtom.ToString().Length) && Token.IsText(sLastAtom))
			{
				Token token = new Token();
				token.m_type = TokenType.Apartment;
				token.m_beginAtom = lastAtom;
				token.m_endAtom = lastAtom;
				token.m_sKey = "INFERRED";
				tokenList.Add(token);
			}
			else if (1 == sLastAtom.ToString().Length)
			{
				Token token = new Token();
				token.m_type = TokenType.Apartment;
				token.m_beginAtom = lastAtom;
				token.m_endAtom = lastAtom;
				token.m_sKey = "INFERRED";
				tokenList.Add(token);
			}

			return tokenList;
		}

		static private ArrayList GetDirectionTokens(Atom[] atoms)
		{
			ArrayList tokenList = new ArrayList(100);
			foreach (string dirIndicator in Token.DIR_KEYS)
			{
				for (int i=0; i<atoms.Length; ++i)
				{
					string sAtom1 = atoms[i].ToString();
					string sAtom2 = (atoms.Length > i+1) ? sAtom1 + " " + atoms[i+1].ToString() : "";

					if (sAtom1 == dirIndicator)
					{
						Token token = new Token();
						token.m_type = TokenType.Direction;
						token.m_beginAtom = atoms[i];
						token.m_endAtom = atoms[i];
						token.m_sKey = dirIndicator;
						tokenList.Add(token);
					}
					else if (sAtom2 == dirIndicator)
					{
						Token token = new Token();
						token.m_type = TokenType.Direction;
						token.m_beginAtom = atoms[i];
						token.m_endAtom = atoms[i+1];
						token.m_sKey = dirIndicator;
						tokenList.Add(token);
					}
				}
			}

			return tokenList;
		}

		#endregion
	}
}
