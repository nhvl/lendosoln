using System;

namespace CommonLib.AddressParser
{
	/// <summary>
	/// Summary description for DecisionRule.
	/// </summary>
	internal class DecisionRule
	{
		private DecisionRule()
		{
		}

		static public ParsedAddress Decide(ParsedAddress[] addresses)
		{
			if ((null == addresses) || (0 == addresses.Length))
				throw new ApplicationException("Invalid street address");

			ParsedAddress bestAddress = addresses[0];
			if (1 < addresses.Length)
			{
				for (int i=1; i<addresses.Length; ++i)
				{
					if (addresses[i].Better(bestAddress)) bestAddress = addresses[i];
				}
			}

			return bestAddress;
		}
	}
}
