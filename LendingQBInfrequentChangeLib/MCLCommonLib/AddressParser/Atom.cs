using System;
using System.Collections;

namespace CommonLib.AddressParser
{
	/// <summary>
	/// Summary description for Atom.
	/// </summary>
	internal class Atom
	{
		private int m_nStartIndex = 0;
		private int m_nEndIndex = -1;
		internal string Parent;

		private Atom()
		{
		}

		static public Atom[] Generate(string address)
		{
			ArrayList atomList = new ArrayList(100);
			string[] parts = address.Split(null);

			// strip off the end of hyphenation in first atom
			int nHyphen = parts[0].IndexOf('-');
			if (-1 != nHyphen) parts[0] = parts[0].Substring(0, nHyphen);

			foreach (string part in parts)
			{
				ArrayList subparts = DivideBySymbols(part);
				atomList.AddRange(subparts);
			}

			int nItem = 0;
			int nIndex = 0;
			Atom[] atoms = new Atom[atomList.Count];
			foreach (string part in atomList)
			{
				int nStart = address.IndexOf(part, nIndex);
				int nEnd = nStart + part.Length - 1;
				nIndex = nEnd + 1;

				Atom atom = new Atom();
				atom.m_nStartIndex = nStart;
				atom.m_nEndIndex = nEnd;
				atom.Parent = address;
				atoms[nItem++] = atom;
			}

			return atoms;
		}

		#region Public methods/properties

		public int StartIndex
		{
			get{return this.m_nStartIndex;}
		}

		public int EndIndex
		{
			get{return this.m_nEndIndex;}
		}

		public int Length
		{
			get{return this.m_nEndIndex - this.m_nStartIndex + 1;}
		}

		public override string ToString()
		{
			return this.Parent.Substring(this.StartIndex, this.Length);
		}

		static public string Serialize(Atom[] atoms)
		{
			System.Text.StringBuilder sb = new System.Text.StringBuilder();
			sb.Append("<AtomList>");
			foreach (Atom atom in atoms)
			{
				sb.Append("<Atom start_index='" + atom.StartIndex + "' end_index='" + atom.EndIndex + "'>");
				sb.Append(atom.ToString());
				sb.Append("</Atom>");
			}
			sb.Append("</AtomList>");

			return sb.ToString();
		}

		#endregion

		#region Private methods

		static private ArrayList DivideBySymbols(string word)
		{
			ArrayList pieces = new ArrayList(100);
			if ((null == word) || ("" == word)) return pieces;

			string temp = word;
			foreach (char symbol in StreetParser.Symbols)
			{
				temp = temp.Replace(symbol, ' ');
			}

			string sAccum = "";
			for (int i=0; i<temp.Length; ++i)
			{
				if (' ' == temp[i])
				{
					if ("" != sAccum)
					{
						pieces.Add(sAccum);
						sAccum = "";
					}
					pieces.Add(word[i].ToString());
				}
				else
				{
					sAccum += temp[i];
				}
			}

			// add last piece
			if ("" != sAccum) pieces.Add(sAccum);

			return pieces;
		}

		#endregion
	}
}
