using System;
using System.Collections;

namespace CommonLib.AddressParser
{
	/// <summary>
	/// Summary description for DivisionRule.
	/// </summary>
	internal class DivisionRule
	{
		private DivisionRule()
		{
		}

		#region Public methods

		static public ParsedAddress[] Parse(Clause[] clauses)
		{
			if ((null == clauses) || (0 == clauses.Length))
				throw new ApplicationException("No clauses to parse");

			if (1 == clauses.Length) return ParseSingleClause(clauses[0]);

			return DoParse(clauses);
		}

		#endregion

		#region Private methods

		static private int CountNextOverlaps(int i, Clause[] allClauses)
		{
			// allClauses is sorted based upon start index
			int nCount = 0;
			for (int j=i+1; j<allClauses.Length; ++j)
			{
				if (Clause.Overlap(allClauses[i], allClauses[j])) ++nCount;
				else break;
			}

			return nCount;
		}

		static private ParsedAddress[] ParseSingleClause(Clause clause)
		{
			ParsedAddress[] parsed = new ParsedAddress[1];
			switch (clause.Type)
			{
				case ClauseType.Highway:
					parsed[0] = new HighwayAddress(clause);
					break;

				case ClauseType.Hotel:
					parsed[0] = new HotelAddress(clause);
					break;

				case ClauseType.Military:
					parsed[0] = new MilitaryAddress(clause);
					break;

				case ClauseType.Parking:
					parsed[0] = new ParkingAddress(clause);
					break;

				case ClauseType.POB:
					parsed[0] = new POBAddress(clause);
					break;

				case ClauseType.Rural:
					parsed[0] = new RuralAddress(clause);
					break;

				default:
					throw new ApplicationException(clause.Type.ToString() + " clause cannot stand alone");
			}

			return parsed;
		}

		static private ParsedAddress[] DoParse(Clause[] clauses)
		{
			// Clauses are sorted
			ArrayList parsedAddressList = new ArrayList(100);
			string sFull = clauses[0].Parent;
			int nLastIndex = clauses[clauses.Length - 1].EndIndex;
			for (int i=0; i<clauses.Length; ++i)
			{
				if (0 != clauses[i].StartIndex) break;
				try
				{
					ArrayList addressList = new ArrayList(100);
					ArrayList clauseList = new ArrayList(100);
					clauseList.Add(clauses[i]);
					if (nLastIndex == clauses[i].EndIndex)
						addressList.Add(clauseList);
					else
						FinishClauseList(nLastIndex, i, clauses, ref clauseList, ref addressList);

					foreach (ArrayList clauseString in addressList)
					{
						if (1 == clauseString.Count)
						{
							ParsedAddress[] theAddress = ParseSingleClause((Clause)(clauseString[0]));
							parsedAddressList.Add(theAddress[0]);
						}
						else
						{
							try
							{
								NormalAddress norm = new NormalAddress(clauseString);
								parsedAddressList.Add(norm);
							}
							catch{}
							try
							{
								HotelAddress norm = new HotelAddress(clauseString);
								parsedAddressList.Add(norm);
							}
							catch{}
							try
							{
								RuralAddress rural = new RuralAddress(clauseString);
								parsedAddressList.Add(rural);
							}
							catch{}
							try
							{
								HighwayAddress hwy = new HighwayAddress(clauseString);
								parsedAddressList.Add(hwy);
							}
							catch{}

                            if (2 == clauseString.Count)
                            {
                                try
                                {
                                    Clause preCandidate = (Clause)(clauseString[0]);
                                    Clause pobCandidate = (Clause)(clauseString[1]);
                                    POBAddress pob = new POBAddress(preCandidate, pobCandidate);
                                    parsedAddressList.Add(pob);
                                }
                                catch { }
                            }
						}
					}
				}
				catch{}
			}

			return (ParsedAddress[])(parsedAddressList.ToArray(typeof(ParsedAddress)));
		}

		static private bool FinishClauseList(int lastIndex, int current, Clause[] clauses, ref ArrayList workingList, ref ArrayList parsedList)
		{
			if (lastIndex == clauses[current].EndIndex) return true;

			int nOverlap = CountNextOverlaps(current, clauses);
			ArrayList nextList = Clause.GetNextClauses(current, clauses);
			if (0 == nextList.Count) throw new ApplicationException("Unable to finish string");

			for (int j=0; j<nextList.Count; ++j)
			{
				int nPosition = current + nOverlap + j + 1;
				workingList.Add(clauses[nPosition]);
				bool bEnd = FinishClauseList(lastIndex, nPosition, clauses, ref workingList, ref parsedList);
				if (bEnd)
				{
					ArrayList address = new ArrayList(workingList.Count);
					foreach (Clause clause in workingList) {address.Add(clause);}
					parsedList.Add(address);
				}

				workingList.RemoveAt(workingList.Count - 1);
			}

			return false;
		}

		#endregion
	}
}
