using System;
using System.Collections;

namespace CommonLib.AddressParser
{
	internal enum ClauseType {Abbreviation, Text, Numeric, AlphaNumeric, Symbol, StreetType, POB, Rural, Highway, Military, Parking, Hotel, Apartment, Direction}

	internal class Clause : IComparable
	{
		private Token m_beginToken;
		private Token m_endToken;
		ArrayList m_TokenList;
		private ClauseType m_type;
		private string m_sKey;

		public Clause()
		{
			this.m_TokenList = new ArrayList(10);
		}

		static public Clause[] Generate(Token[] tokens)
		{
			ArrayList clauseList = new ArrayList(100);
			for (int i=0; i<tokens.Length; ++i)
			{
				ArrayList clauses = null;
				switch (tokens[i].Type)
				{
					case TokenType.Abbreviation:
						clauses = ProcessAbbreviationToken(i, tokens[i], tokens);
						break;

					case TokenType.AlphaNumeric:
						clauses = ProcessAlphaNumericToken(i, tokens[i], tokens);
						break;

					case TokenType.Apartment:
						clauses = ProcessApartmentToken(i, tokens[i], tokens);
						break;

					case TokenType.Direction:
						clauses = ProcessDirectionToken(i, tokens[i], tokens);
						break;

					case TokenType.Highway:
						clauses = ProcessHighwayToken(i, tokens[i], tokens);
						break;

					case TokenType.Hotel:
						clauses = ProcessHotelToken(i, tokens[i], tokens);
						break;

					case TokenType.Military:
						clauses = ProcessMilitaryToken(i, tokens[i], tokens);
						break;

					case TokenType.Numeric:
						clauses = ProcessNumericToken(i, tokens[i], tokens);
						break;

					case TokenType.Ordinal:
						clauses = ProcessOrdinalToken(i, tokens[i], tokens);
						break;

					case TokenType.Parking:
						clauses = ProcessParkingToken(i, tokens[i], tokens);
						break;

					case TokenType.POB:
						clauses = ProcessPOBToken(i, tokens[i], tokens);
						break;

					case TokenType.Rural:
						clauses = ProcessRuralToken(i, tokens[i], tokens);
						break;

					case TokenType.StreetType:
						clauses = ProcessStreetTypeToken(i, tokens[i], tokens);
						break;

					case TokenType.Symbol:
						clauses = ProcessSymbolToken(i, tokens[i], tokens);
						break;

					case TokenType.Text:
						clauses = ProcessTextToken(i, tokens[i], tokens);
						break;
				}

				clauseList.AddRange(clauses);
			}
			
			clauseList.Sort();
			return (Clause[])clauseList.ToArray(typeof(Clause));
		}

		static public bool Overlap(Clause first, Clause second)
		{
			bool bFirstEndEarlier = first.EndIndex < second.StartIndex;
			bool bSecondEndEarlier = second.EndIndex < first.StartIndex;
			return !(bFirstEndEarlier || bSecondEndEarlier);
		}

		#region Public methods/properties

		public Token[] Tokens
		{
			get
			{
				return (Token[])(this.m_TokenList.ToArray(typeof(Token)));
			}
		}

		public int StartIndex
		{
			get{return this.m_beginToken.StartIndex;}
		}

		public int EndIndex
		{
			get{return this.m_endToken.EndIndex;}
		}

		public ClauseType Type
		{
			get{return this.m_type;}
		}

		public string Parent
		{
			get{return this.m_beginToken.Parent;}
		}

		public string Key
		{
			get{return this.m_sKey;}
		}

		public int Length
		{
			get{return this.EndIndex - this.StartIndex + 1;}
		}

		public override string ToString()
		{
			return this.Parent.Substring(this.StartIndex, this.Length);
		}

		static public string Serialize(Clause[] clauses)
		{
			System.Text.StringBuilder sb = new System.Text.StringBuilder();
			sb.Append("<ClauseList>");
			foreach (Clause clause in clauses)
			{
				sb.Append("<Clause type='" + clause.Type.ToString() + "' key='" + clause.Key + "' start_index='" + clause.StartIndex + "' end_index='" + clause.EndIndex + "'>");
				sb.Append(clause.ToString());
				sb.Append("</Clause>");
			}
			sb.Append("</ClauseList>");

			return sb.ToString();
		}

		int IComparable.CompareTo(object toCompare)
		{
			if (!(toCompare is Clause))
				throw new ArgumentException("Cannot compare Clause to " + toCompare.GetType().Name);

			Clause compClause = (Clause)toCompare;
			return this.StartIndex.CompareTo(compClause.StartIndex);
		}

		static public int CountNextOverlaps(int i, Clause[] allClauses)
		{
			// allClauses must be sorted based upon start index
			int nCount = 0;
			for (int j=i+1; j<allClauses.Length; ++j)
			{
				if (Clause.Overlap(allClauses[i], allClauses[j])) ++nCount;
				else break;
			}

			return nCount;
		}

		static public ArrayList GetNextClauses(int i, Clause[] allClauses)
		{
            // allClauses must be sorted based upon start index
            ArrayList nextClauses = new ArrayList(100);
			bool bFirst = true;
			int nLow = 0;
			for (int j=i+1; j< allClauses.Length; ++j)
			{
				if (Clause.Overlap(allClauses[i], allClauses[j])) continue;
				if (bFirst)
				{
					bFirst = false;
					nLow = allClauses[j].StartIndex;
				}

				if (nLow == allClauses[j].StartIndex)
					nextClauses.Add(allClauses[j]);
				else break;
			}

			return nextClauses;
		}

		#endregion

		#region Private methods

		static private ArrayList ProcessSimpleToken(Token token, ClauseType type)
		{
			ArrayList clauses = new ArrayList(1);
			Clause clause = new Clause();
			clause.m_beginToken = token;
			clause.m_endToken = token;
			clause.m_type = type;
			clause.m_sKey = token.Key;
			clause.m_TokenList.Add(token);
			clauses.Add(clause);
			return clauses;
		}

		static private ArrayList ProcessAbbreviationToken(int i, Token token, Token[] tokens)
		{
			return ProcessSimpleToken(token, ClauseType.Abbreviation);
		}

		static private ArrayList ProcessAlphaNumericToken(int i, Token token, Token[] tokens)
		{
			ArrayList clauses = new ArrayList(2);
			clauses.AddRange(ProcessSimpleToken(token, ClauseType.AlphaNumeric));

			// could also be apt of type a5-3
			int nOverlaps = Token.CountNextOverlaps(i, tokens);
			ArrayList listNext = Token.GetNextTokens(i, tokens);
			int nNextIndex = nOverlaps;
			foreach (Token myToken in listNext)
			{
				++nNextIndex;
				if (TokenType.Symbol != myToken.Type) continue;

				ArrayList symNext = Token.GetNextTokens(i + nNextIndex, tokens);
				foreach (Token sndToken in symNext)
				{
					switch (sndToken.Type)
					{
						case TokenType.Text:
						case TokenType.Numeric:
						case TokenType.AlphaNumeric:
							Clause clause = new Clause();
							clause.m_beginToken = token;
							clause.m_endToken = sndToken;
							clause.m_type = ClauseType.Apartment;
							clause.m_sKey = myToken.Key;
							clause.m_TokenList.Add(token);
							clause.m_TokenList.Add(myToken);
							clause.m_TokenList.Add(sndToken);
							clauses.Add(clause);
							break;
					}
				}
			}

			return clauses;
		}

		static private ArrayList ProcessApartmentToken(int i, Token token, Token[] tokens)
		{
			ArrayList clauses = new ArrayList(1);
			string sToken = token.ToString();
			if (token.Key == "INFERRED")
			{
				Clause clause = new Clause();
				clause.m_beginToken = token;
				clause.m_endToken = token;
				clause.m_type = ClauseType.Apartment;
				clause.m_sKey = token.Key;
				clause.m_TokenList.Add(token);
				clauses.Add(clause);

				return clauses;
			}

			if (token.Key != sToken)
			{
				// token must have the actual apt number appended
				// BUT make sure it is numeric or alphanumeric
				string sNumber = sToken.Substring(token.Key.Length);
				if (Token.IsNumeric(sNumber) || Token.IsAlphaNumeric(sNumber))
				{
					Clause clause = new Clause();
					clause.m_beginToken = token;
					clause.m_endToken = token;
					clause.m_type = ClauseType.Apartment;
					clause.m_sKey = token.Key;
					clause.m_TokenList.Add(token);
					clauses.Add(clause);
				}

				return clauses;
			}

			// Check whether the apartment token requires an aparment number
			bool bNeedNumber = true;
			if (1 < token.Key.Length) // symbols require apartment number
			{
				for (int j=0; j<Token.APT_KEYS.Length/3; ++j)
				{
					if (token.Key == Token.APT_KEYS[j,0])
					{
						bNeedNumber = ("Y" == Token.APT_KEYS[j,2]);
						break;
					}
				}
			}

			if (!bNeedNumber)
			{
				Clause clause = new Clause();
				clause.m_beginToken = token;
				clause.m_endToken = token;
				clause.m_type = ClauseType.Apartment;
				clause.m_sKey = token.Key;
				clause.m_TokenList.Add(token);
				clauses.Add(clause);
				return clauses;
			}

			int nOverlaps = Token.CountNextOverlaps(i, tokens);
			ArrayList listNext = Token.GetNextTokens(i, tokens);
			int nNextIndex = nOverlaps;
			foreach (Token myToken in listNext)
			{
				++nNextIndex;
				switch (myToken.Type)
				{
					case TokenType.Text:
					case TokenType.Numeric:
					case TokenType.AlphaNumeric:
						Clause clause = new Clause();
						clause.m_beginToken = token;
						clause.m_endToken = myToken;
						clause.m_type = ClauseType.Apartment;
						clause.m_sKey = token.Key;
						clause.m_TokenList.Add(token);
						clause.m_TokenList.Add(myToken);
						clauses.Add(clause);

						// maybe of form 2-B also
						nOverlaps = Token.CountNextOverlaps(i + nNextIndex, tokens);
						ArrayList theNext = Token.GetNextTokens(i + nNextIndex, tokens);
						int nNextIndex2 = nNextIndex + nOverlaps;
						foreach (Token sndToken in theNext)
						{
							++nNextIndex2;
							if (TokenType.Symbol == sndToken.Type)
							{
								nOverlaps = Token.CountNextOverlaps(i + nNextIndex2, tokens);
								ArrayList againNext = Token.GetNextTokens(i + nNextIndex2, tokens);
								foreach (Token thrdToken in againNext)
								{
									switch (thrdToken.Type)
									{
										case TokenType.Text:
										case TokenType.Numeric:
										case TokenType.AlphaNumeric:
											clause = new Clause();
											clause.m_beginToken = token;
											clause.m_endToken = thrdToken;
											clause.m_type = ClauseType.Apartment;
											clause.m_sKey = token.Key;
											clause.m_TokenList.Add(token);
											clause.m_TokenList.Add(myToken);
											clause.m_TokenList.Add(sndToken);
											clause.m_TokenList.Add(thrdToken);
											clauses.Add(clause);
											break;
									}
								}
								break;
							}
						}
						return clauses;

					case TokenType.Symbol:
						ArrayList symNext = Token.GetNextTokens(i + nNextIndex, tokens);
						foreach (Token sndToken in symNext)
						{
							switch (sndToken.Type)
							{
								case TokenType.Text:
								case TokenType.Numeric:
								case TokenType.AlphaNumeric:
									clause = new Clause();
									clause.m_beginToken = token;
									clause.m_endToken = sndToken;
									clause.m_type = ClauseType.Apartment;
									clause.m_sKey = token.Key;
									clause.m_TokenList.Add(token);
									clause.m_TokenList.Add(myToken);
									clause.m_TokenList.Add(sndToken);
									clauses.Add(clause);
									return clauses;

								default:
									continue;
							}
						}
						break;

					default:
						continue;
				}
			}

			return clauses;
		}

		static private ArrayList ProcessDirectionToken(int i, Token token, Token[] tokens)
		{
			return ProcessSimpleToken(token, ClauseType.Direction);
		}

		static private ArrayList ProcessHighwayToken(int i, Token token, Token[] tokens)
		{
			ArrayList clauses = new ArrayList(2);
			if (token.Key != token.ToString())
			{
				// token must have the actual highway number appended
				Clause clause = new Clause();
				clause.m_beginToken = token;
				clause.m_endToken = token;
				clause.m_type = ClauseType.Highway;
				clause.m_sKey = token.Key;
				clause.m_TokenList.Add(token);
				clauses.Add(clause);
				return clauses;
			}

			int nOverlaps = Token.CountNextOverlaps(i, tokens);
			ArrayList listNext = Token.GetNextTokens(i, tokens);
			int nNextIndex = nOverlaps;
			foreach (Token myToken in listNext)
			{
				++nNextIndex;
				switch (myToken.Type)
				{
					case TokenType.Text:
					case TokenType.Numeric:
					case TokenType.AlphaNumeric:
						Clause clause = new Clause();
						clause.m_beginToken = token;
						clause.m_endToken = myToken;
						clause.m_type = ClauseType.Highway;
						clause.m_sKey = token.Key;
						clause.m_TokenList.Add(token);
						clause.m_TokenList.Add(myToken);
						clauses.Add(clause);

						// may also have a direction appended as part of hwy name
						ArrayList dirNext = Token.GetNextTokens(i + nNextIndex, tokens);
						foreach (Token dirToken in dirNext)
						{
							if (TokenType.Direction == dirToken.Type)
							{
								clause = new Clause();
								clause.m_beginToken = token;
								clause.m_endToken = dirToken;
								clause.m_type = ClauseType.Highway;
								clause.m_sKey = token.Key;
								clause.m_TokenList.Add(token);
								clause.m_TokenList.Add(myToken);
								clause.m_TokenList.Add(dirToken);
								clauses.Add(clause);
							}
						}
						return clauses;

					case TokenType.Symbol:
						ArrayList symNext = Token.GetNextTokens(i + nNextIndex, tokens);
						foreach (Token sndToken in symNext)
						{
							switch (sndToken.Type)
							{
								case TokenType.Text:
								case TokenType.Numeric:
								case TokenType.AlphaNumeric:
									clause = new Clause();
									clause.m_beginToken = token;
									clause.m_endToken = sndToken;
									clause.m_type = ClauseType.Highway;
									clause.m_sKey = token.Key;
									clause.m_TokenList.Add(token);
									clause.m_TokenList.Add(myToken);
									clause.m_TokenList.Add(sndToken);
									clauses.Add(clause);
									return clauses;

								default:
									continue;
							}
						}
						break;

					default:
						continue;
				}
			}

			return clauses;
		}

		static private ArrayList ProcessHotelToken(int i, Token token, Token[] tokens)
		{
			// Could be Name Hotel OR Hotel Name
			// Process Hotel Name here
			// Process Name Hotel in Text token method

			ArrayList clauses = new ArrayList(2);
			int nOverlaps = Token.CountNextOverlaps(i, tokens);
			ArrayList listNext = Token.GetNextTokens(i, tokens);
			int nNextIndex = nOverlaps;
			foreach (Token myToken in listNext)
			{
				++nNextIndex;
				switch (myToken.Type)
				{
					// Can have up to two words for the name
					case TokenType.Text:
					case TokenType.Numeric:
						// first word
						Clause clause = new Clause();
						clause.m_beginToken = token;
						clause.m_endToken = myToken;
						clause.m_type = ClauseType.Hotel;
						clause.m_sKey = token.Key;
						clause.m_TokenList.Add(token);
						clause.m_TokenList.Add(myToken);
						clauses.Add(clause);

						ArrayList symNext = Token.GetNextTokens(i + nNextIndex, tokens);
						foreach (Token sndToken in symNext)
						{
							switch (sndToken.Type)
							{
								case TokenType.Text:
								case TokenType.Numeric:
									// second word
									clause = new Clause();
									clause.m_beginToken = token;
									clause.m_endToken = sndToken;
									clause.m_type = ClauseType.Hotel;
									clause.m_sKey = token.Key;
									clause.m_TokenList.Add(token);
									clause.m_TokenList.Add(myToken);
									clause.m_TokenList.Add(sndToken);
									clauses.Add(clause);
									break;

								default:
									continue;
							}
						}
						break;
				}
			}

			return clauses;
		}

		static private ArrayList ProcessMilitaryToken(int i, Token token, Token[] tokens)
		{
			// Can be Key Name or Ordinal Key
			// Process Key Name here
			// Process Ordinal Key in Ordinal token method

			ArrayList clauses = new ArrayList(1);
			ArrayList listNext = Token.GetNextTokens(i, tokens);
			foreach (Token myToken in listNext)
			{
				switch (myToken.Type)
				{
					case TokenType.Text:
					case TokenType.Numeric:
						Clause clause = new Clause();
						clause.m_beginToken = token;
						clause.m_endToken = myToken;
						clause.m_type = ClauseType.Military;
						clause.m_sKey = token.Key;
						clauses.Add(clause);
						clause.m_TokenList.Add(token);
						clause.m_TokenList.Add(myToken);
						return clauses;
				}
			}

			return clauses;
		}

		static private ArrayList ProcessNumericToken(int i, Token token, Token[] tokens)
		{
			ArrayList clauses = new ArrayList(2);
			clauses.AddRange(ProcessSimpleToken(token, ClauseType.Numeric));

			// could also be apt of type 2-3
			int nOverlaps = Token.CountNextOverlaps(i, tokens);
			ArrayList listNext = Token.GetNextTokens(i, tokens);
			int nNextIndex = nOverlaps;
			foreach (Token myToken in listNext)
			{
				++nNextIndex;
				if (TokenType.Symbol != myToken.Type) continue;

				ArrayList symNext = Token.GetNextTokens(i + nNextIndex, tokens);
				foreach (Token sndToken in symNext)
				{
					switch (sndToken.Type)
					{
						case TokenType.Text:
						case TokenType.Numeric:
						case TokenType.AlphaNumeric:
							Clause clause = new Clause();
							clause.m_beginToken = token;
							clause.m_endToken = sndToken;
							clause.m_type = ClauseType.Apartment;
							clause.m_sKey = myToken.Key;
							clause.m_TokenList.Add(token);
							clause.m_TokenList.Add(myToken);
							clause.m_TokenList.Add(sndToken);
							clauses.Add(clause);
							break;
					}
				}
			}

			return clauses;
		}

		static private ArrayList ProcessOrdinalToken(int i, Token token, Token[] tokens)
		{
			// Can be Ordinal Military-Key, Ordinal Parking-Key OR Ordinal-Apartment

			ArrayList clauses = new ArrayList(1);
			ArrayList listNext = Token.GetNextTokens(i, tokens);
			foreach (Token myToken in listNext)
			{
				if (myToken.Key != myToken.ToString()) continue;

				switch (myToken.Type)
				{
					case TokenType.Military:
						Clause clause = new Clause();
						clause.m_beginToken = token;
						clause.m_endToken = myToken;
						clause.m_type = ClauseType.Military;
						clause.m_sKey = myToken.Key;
						clause.m_TokenList.Add(token);
						clause.m_TokenList.Add(myToken);
						clauses.Add(clause);
						break;

					case TokenType.Parking:
						clause = new Clause();
						clause.m_beginToken = token;
						clause.m_endToken = myToken;
						clause.m_type = ClauseType.Parking;
						clause.m_sKey = myToken.Key;
						clause.m_TokenList.Add(token);
						clause.m_TokenList.Add(myToken);
						clauses.Add(clause);
						break;

					case TokenType.Apartment:
						clause = new Clause();
						clause.m_beginToken = token;
						clause.m_endToken = myToken;
						clause.m_type = ClauseType.Apartment;
						clause.m_sKey = myToken.Key;
						clause.m_TokenList.Add(token);
						clause.m_TokenList.Add(myToken);
						clauses.Add(clause);
						break;
				}
			}

			return clauses;
		}

		static private ArrayList ProcessParkingToken(int i, Token token, Token[] tokens)
		{
			// Can be Key Name OR Key Symbol Name OR Ordinal Key
			// Process Key Name and Key Symbol Name here
			// Process Ordinal Key in Ordinal token method

			ArrayList clauses = new ArrayList(1);
			if (token.Key != token.ToString())
			{
				// token must have the actual parking number appended
				Clause clause = new Clause();
				clause.m_beginToken = token;
				clause.m_endToken = token;
				clause.m_type = ClauseType.Parking;
				clause.m_sKey = token.Key;
				clause.m_TokenList.Add(token);
				clauses.Add(clause);
				return clauses;
			}

			int nOverlaps = Token.CountNextOverlaps(i, tokens);
			ArrayList listNext = Token.GetNextTokens(i, tokens);
			int nNextIndex = nOverlaps;
			foreach (Token myToken in listNext)
			{
				++nNextIndex;
				switch (myToken.Type)
				{
					case TokenType.Text:
					case TokenType.Numeric:
					case TokenType.AlphaNumeric:
						Clause clause = new Clause();
						clause.m_beginToken = token;
						clause.m_endToken = myToken;
						clause.m_type = ClauseType.Parking;
						clause.m_sKey = token.Key;
						clauses.Add(clause);
						clause.m_TokenList.Add(token);
						clause.m_TokenList.Add(myToken);
						return clauses;

					case TokenType.Symbol:
						ArrayList symNext = Token.GetNextTokens(i + nNextIndex, tokens);
						foreach (Token sndToken in symNext)
						{
							switch (sndToken.Type)
							{
								case TokenType.Text:
								case TokenType.Numeric:
								case TokenType.AlphaNumeric:
									clause = new Clause();
									clause.m_beginToken = token;
									clause.m_endToken = sndToken;
									clause.m_type = ClauseType.Parking;
									clause.m_sKey = token.Key;
									clause.m_TokenList.Add(token);
									clause.m_TokenList.Add(myToken);
									clause.m_TokenList.Add(sndToken);
									clauses.Add(clause);
									return clauses;

								default:
									continue;
							}
						}
						break;

					default:
						continue;
				}
			}

			return clauses;
		}

		static private ArrayList ProcessPOBToken(int i, Token token, Token[] tokens)
		{
			ArrayList clauses = new ArrayList(1);
			int nOverlaps = Token.CountNextOverlaps(i, tokens);
			ArrayList listNext = Token.GetNextTokens(i, tokens);
			int nNextIndex = nOverlaps;
			foreach (Token myToken in listNext)
			{
				++nNextIndex;
				switch (myToken.Type)
				{
					case TokenType.Numeric:
					case TokenType.AlphaNumeric:
						Clause clause = new Clause();
						clause.m_beginToken = token;
						clause.m_endToken = myToken;
						clause.m_type = ClauseType.POB;
						clause.m_sKey = token.Key;
						clause.m_TokenList.Add(token);
						clause.m_TokenList.Add(myToken);
						clauses.Add(clause);
						return clauses;

					case TokenType.Text:
						if (myToken.Length != 1) continue;
						clause = new Clause();
						clause.m_beginToken = token;
						clause.m_endToken = myToken;
						clause.m_type = ClauseType.POB;
						clause.m_sKey = token.Key;
						clause.m_TokenList.Add(token);
						clause.m_TokenList.Add(myToken);
						clauses.Add(clause);
						return clauses;

					case TokenType.Symbol:
						ArrayList symNext = Token.GetNextTokens(i + nNextIndex, tokens);
						foreach (Token sndToken in symNext)
						{
							switch (sndToken.Type)
							{
								case TokenType.Numeric:
								case TokenType.AlphaNumeric:
									clause = new Clause();
									clause.m_beginToken = token;
									clause.m_endToken = sndToken;
									clause.m_type = ClauseType.POB;
									clause.m_sKey = token.Key;
									clause.m_TokenList.Add(token);
									clause.m_TokenList.Add(myToken);
									clause.m_TokenList.Add(sndToken);
									clauses.Add(clause);
									return clauses;

								default:
									continue;
							}
						}
						break;

					default:
						continue;
				}
			}

			return clauses;
		}

		static private ArrayList ProcessRuralToken(int i, Token token, Token[] tokens)
		{
			ArrayList clauses = new ArrayList(1);
			if (token.Key != token.ToString())
			{
				// token must have the actual highway number appended
				Clause clause = new Clause();
				clause.m_beginToken = token;
				clause.m_endToken = token;
				clause.m_type = ClauseType.Rural;
				clause.m_sKey = token.Key;
				clauses.Add(clause);
				clause.m_TokenList.Add(token);
				return clauses;
			}

			int nOverlaps = Token.CountNextOverlaps(i, tokens);
			ArrayList listNext = Token.GetNextTokens(i, tokens);
			int nNextIndex = nOverlaps;
			foreach (Token myToken in listNext)
			{
				++nNextIndex;
				switch (myToken.Type)
				{
					case TokenType.Text:
					case TokenType.Numeric:
					case TokenType.AlphaNumeric:
						Clause clause = new Clause();
						clause.m_beginToken = token;
						clause.m_endToken = myToken;
						clause.m_type = ClauseType.Rural;
						clause.m_sKey = token.Key;
						clause.m_TokenList.Add(token);
						clause.m_TokenList.Add(myToken);
						clauses.Add(clause);
						return clauses;

					case TokenType.Symbol:
						ArrayList symNext = Token.GetNextTokens(i + nNextIndex, tokens);
						foreach (Token sndToken in symNext)
						{
							switch (sndToken.Type)
							{
								case TokenType.Text:
								case TokenType.Numeric:
								case TokenType.AlphaNumeric:
									clause = new Clause();
									clause.m_beginToken = token;
									clause.m_endToken = sndToken;
									clause.m_type = ClauseType.Rural;
									clause.m_sKey = token.Key;
									clause.m_TokenList.Add(token);
									clause.m_TokenList.Add(myToken);
									clause.m_TokenList.Add(sndToken);
									clauses.Add(clause);
									return clauses;

								default:
									continue;
							}
						}
						break;

					default:
						continue;
				}
			}

			return clauses;
		}

		static private ArrayList ProcessStreetTypeToken(int i, Token token, Token[] tokens)
		{
			return ProcessSimpleToken(token, ClauseType.StreetType);
		}

		static private ArrayList ProcessSymbolToken(int i, Token token, Token[] tokens)
		{
			return ProcessSimpleToken(token, ClauseType.Symbol);
		}

		static private ArrayList ProcessTextToken(int i, Token token, Token[] tokens)
		{
			// Can be Name Hotel or just simple text.  Hotels can have two words in the name

			// Simple text clause
			ArrayList clauses = new ArrayList(2);
			Clause clause = new Clause();
			clause.m_beginToken = token;
			clause.m_endToken = token;
			clause.m_type = ClauseType.Text;
			clause.m_sKey = token.Key;
			clause.m_TokenList.Add(token);
			clauses.Add(clause);

			// Name Hotel clause
			int nOverlaps = Token.CountNextOverlaps(i, tokens);
			ArrayList listNext = Token.GetNextTokens(i, tokens);
			int nNextIndex = nOverlaps;
			foreach (Token myToken in listNext)
			{
				++nNextIndex;
				switch (myToken.Type)
				{
					case TokenType.Text:
						ArrayList sndNext = Token.GetNextTokens(i + nNextIndex, tokens);
						foreach (Token sndToken in sndNext)
						{
							switch (sndToken.Type)
							{
								case TokenType.Hotel:
									clause = new Clause();
									clause.m_beginToken = token;
									clause.m_endToken = sndToken;
									clause.m_type = ClauseType.Hotel;
									clause.m_sKey = sndToken.Key;
									clause.m_TokenList.Add(token);
									clause.m_TokenList.Add(myToken);
									clause.m_TokenList.Add(sndToken);
									clauses.Add(clause);
									return clauses;

								default:
									continue;
							}
						}
						break;

					case TokenType.Hotel:
						clause = new Clause();
						clause.m_beginToken = token;
						clause.m_endToken = myToken;
						clause.m_type = ClauseType.Hotel;
						clause.m_sKey = myToken.Key;
						clause.m_TokenList.Add(token);
						clause.m_TokenList.Add(myToken);
						clauses.Add(clause);
						return clauses;

					default:
						continue;
				}
			}

			return clauses;
		}

		#endregion
	}
}
