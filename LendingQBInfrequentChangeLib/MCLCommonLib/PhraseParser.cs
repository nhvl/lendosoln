using System;
using System.Collections;
using System.Text;

namespace CommonLib
{
	/// <summary>
	/// Summary description for PhraseParser.
	/// </summary>
	public class PhraseParser
	{
		private string m_sPhrase;

		static public string[] Phonemes = {"ch", "gh", "ph", "rh", "sc", "sh", "th", "tle", "wh", "zh", "mb", "ng", "qu", "kn", "nk"};
		//static public string[] Pairs = {"bl", "br", "lt", "nt", "rt", "st"};
		static public char[] Vowels = {'a', 'e', 'i', 'o', 'u'};
		static public char[] Consonants = {'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'z'};

		public PhraseParser(){}

		public PhraseParser(string phrase)
		{
			if (0 == phrase.Trim().Length)
				throw new ApplicationException("Empty phrase");

			this.m_sPhrase = phrase;
		}

		public string[] ToWords()
		{
			return ToWords(this.m_sPhrase);
		}

		public string[] ToWords(string phrase)
		{
			string trimmed = phrase.Trim();
			string working = (trimmed.EndsWith(".")) ? trimmed.Substring(0, trimmed.Length - 1) : trimmed;

			// We permit phrases with '/' separators --> split at '/' but keep the '/'
			if (-1 != phrase.IndexOf("/"))
			{
				ArrayList wordList = new ArrayList();
				char[] delim = new char[] {'/'};
				string[] slashSplit = working.Split(delim);
				for (int i=0; i<slashSplit.Length; ++i)
				{
					string[] slashWords = ToWords(slashSplit[i]);
					if (i != 0) slashWords[0] = "/" + slashWords[0];
					foreach (string str in slashWords) wordList.Add(str);
				}

				string[] allWords = new string[wordList.Count];
				for (int i=0; i<wordList.Count; ++i) allWords[i] = (string)wordList[i];
				return allWords;
			}

			char[] space = new char[] {' '};
			string[] split = working.Split(space);
			for (int i=0; i<split.Length; ++i) split[i] = (split[i]).Trim();

			return split;
		}

		public static string GeneratePhrase(string[] words)
		{
			StringBuilder sb = new StringBuilder(10 * words.Length);
			foreach (string word in words)
			{
				if (word.Length == 0)
				{
					sb.Append(" ");
				}
				else
				{
					string sToAppend = ('/' == word[0]) ? word : " " + word.Trim();
					sb.Append(sToAppend);
				}
			}

			return sb.ToString().TrimStart();
		}

		public string Truncate(int maxSize)
		{
			return Truncate(this.m_sPhrase, maxSize);
		}

		public string Truncate(string phrase, int maxSize)
		{
			CheckAbbreviationSize(maxSize);
			return phrase.Substring(0, maxSize);
		}

		public string Acronym(int maxSize)
		{
			return Acronym(this.m_sPhrase, maxSize);
		}

		public string Acronym(string phrase, int maxSize)
		{
			CheckAbbreviationSize(maxSize);

			// break into words
			string[] words = ToWords(phrase);
			return GenerateAcronym(words, maxSize);
		}

		public string Abbreviate(int maxSize)
		{
			return Abbreviate(this.m_sPhrase, maxSize);
		}

		public string Abbreviate(string phrase, int maxSize)
		{
			CheckAbbreviationSize(maxSize);
			if (maxSize >= phrase.Length) return phrase;

			// break into words
			string[] words = ToWords(phrase);

			int nFirstSylSize = 0;
			int nSecondSylSize = 0;
			string[][] sylWords = new string[words.Length][];
			for (int i=0; i<words.Length; ++i)
			{
				sylWords[i] = WordToSyllables(words[i]);
				if (0 == i)
				{
					nFirstSylSize = sylWords[0][0].Length;
					if (nFirstSylSize > maxSize)
					{
						// syllable-based abbreviation will fail; use acronym
						return GenerateAcronym(words, maxSize);
					}
				}
				else if (1 == i)
				{
					nSecondSylSize = sylWords[1][0].Length;
					if (nFirstSylSize + nSecondSylSize + 1 > maxSize)
					{
						// syllable-based abbreviation will fail; use acronym
						return GenerateAcronym(words, maxSize);
					}
				}
			}

			string phraseResult = PhraseParser.GeneratePhrase(words);
			while (phraseResult.Length > maxSize)
			{
				AbbreviateLongestWord(ref words, ref sylWords);
				phraseResult = PhraseParser.GeneratePhrase(words);
			}

			return phraseResult;
		}

		private enum TokenType {unparsed, phoneme, consonant, vowel, symbol}
		private struct Token
		{
			public string Value;
			public TokenType Type;
		}

		public string[] WordToSyllables(string word)
		{
			string working = word.Trim();
			if (-1 != working.IndexOf(' '))
				throw new ApplicationException("Not a word");

			// locate phonemes/non-phonemes
			Token[] tokens = new Token[1];
			tokens[0].Value = word;
			tokens[0].Type = TokenType.unparsed;
			foreach (string phon in PhraseParser.Phonemes)
			{
				SeparatePhoneme(phon, ref tokens);
			}

			// locate vowels/consonants
			SeparateVowelsAndConsonants(ref tokens);

			// locate 'y's and decide role
			FixYRoles(ref tokens);

			// combine into syllables
			string [] syllables = TokensToSyllables(tokens);
			return syllables;
		}

		private void CheckAbbreviationSize(int maxSize)
		{
			if (0 >= maxSize)
				throw new ApplicationException("Invalid abbreviation size");
		}

		private string GenerateAcronym(string[] words, int maxSize)
		{
			string acronym = "";
			foreach (string word in words) acronym += char.ToUpper(word[0]);

			return (acronym.Length > maxSize) ? acronym.Substring(0, maxSize) : acronym;
		}

		private void AbbreviateLongestWord(ref string[] words, ref string[][] sylWords)
		{
			int nMultiSyllable = 0;
			int nMaxLen = 0;
			int nMaxIndex = 0;
			for (int i=0; i<words.Length; ++i)
			{
				if (sylWords[i].Length > 1)
				{
					++nMultiSyllable;
					if (nMaxLen <= words[i].Length)
					{
						nMaxLen = words[i].Length;
						nMaxIndex = i;
					}
				}
			}

			if (0 < nMultiSyllable)
			{
				words[nMaxIndex] = sylWords[nMaxIndex][0];
				sylWords[nMaxIndex] = new string[1];
				sylWords[nMaxIndex][0] = words[nMaxIndex];
			}
			else
			{
				string[] redim = new string[words.Length - 1];
				for (int i=0; i<words.Length - 1; ++i)
					redim[i] = words[i];
				words = redim;
			}
		}

		private string[] TokensToSyllables(Token[] tokens)
		{
			string[] ret = null;
			if (3 > tokens.Length)
			{
				ret = new string[1];
				ret[0] = "";
				foreach (Token t in tokens) ret[0] += t.Value;
				return ret;
			}

			ArrayList list = new ArrayList();
			int index = 0;
			int lastIndex = 0;
			while (index < tokens.Length - 1)
			{
				if (TokenType.vowel == tokens[index].Type)
				{
					if (TokenType.vowel == tokens[index+1].Type)
					{
						++index;
						continue;
					}

					int cNum = 0; // number of adjacent non-vowels
					while (TokenType.vowel != tokens[index + 1 + cNum].Type)
					{
						++cNum;
						if (tokens.Length <= index + 1 + cNum) break;
					}

					if (tokens.Length <= index + 1 + cNum) // at end of word
					{
						string lastSyllable = "";
						for (int i=lastIndex; i<tokens.Length; ++i)
							lastSyllable += tokens[i].Value;

						list.Add(lastSyllable);
					}
					else
					{
						// assume cNum <= 4
						int nEndSyllable;
						if (1 == cNum) nEndSyllable = 1;
						else if (2 == cNum) nEndSyllable = 1;
						else if (3 == cNum) nEndSyllable = 1;
						else if (4 == cNum) nEndSyllable = 2;
						else nEndSyllable = 3;

						string thisSyllable = "";
						for (int i=lastIndex; i<=index + nEndSyllable; ++i)
							thisSyllable += tokens[i].Value;

						list.Add(thisSyllable);
						index += nEndSyllable;
						lastIndex = index + 1;
					}
				}

				++index;
			}

			if (0 == list.Count) // no vowels --> just take whole thing as a syllable
			{
				ret = new string[1];
				ret[0] = "";
				for (int i=0; i<tokens.Length; ++i) ret[0] += tokens[i].Value;
			}
			else
			{
				ret = new string[list.Count];
				for (int i=0; i<list.Count; ++i) ret[i] = (string)list[i];
			}

			return ret;
		}

		private void SeparateVowelsAndConsonants(ref Token[] working)
		{
			ArrayList list = new ArrayList();
			foreach (Token piece in working)
			{
				if (TokenType.unparsed != piece.Type)
				{
					list.Add(piece);
					continue;
				};

				foreach (char ch in piece.Value)
				{
					Token token;
					token.Value = ch.ToString();
					if (IsVowel(ch))
						token.Type = TokenType.vowel;
					else if (IsConsonant(ch))
						token.Type = TokenType.consonant;
					else
						token.Type = TokenType.unparsed;
					list.Add(token);
				}
			}

			working = new Token[list.Count];
			for (int i=0; i<list.Count; ++i)
				working[i] = (Token)list[i];
		}

		private bool IsVowel(char inChar)
		{
			foreach (char ch in PhraseParser.Vowels)
			{
				if (ch == char.ToLower(inChar)) return true;
			}

			return false;
		}

		private bool IsConsonant(char inChar)
		{
			foreach (char ch in PhraseParser.Consonants)
			{
				if (ch == char.ToLower(inChar)) return true;
			}

			return false;
		}

		private void FixYRoles(ref Token[] working)
		{
			for (int i=0; i<working.Length; ++i)
			{
				Token piece = working[i];
				if (TokenType.unparsed != piece.Type) continue;

				if ("y" != piece.Value.ToLower())
				{
					// must be a symbol
					piece.Type = TokenType.symbol;
					return;
				}

				if (working.Length == i + 1) // y is last letter --> vowel
				{
					piece.Type = TokenType.vowel;
				}
				else if (TokenType.vowel == working[i+1].Type)
				{
					piece.Type = TokenType.consonant;
				}
				else
				{
					piece.Type = TokenType.vowel;
				}
			}
		}

		private void SeparatePhoneme(string phoneme, ref Token[] working)
		{
			ArrayList list = new ArrayList();
			foreach (Token piece in working)
			{
				if (TokenType.unparsed != piece.Type)
				{
					list.Add(piece);
					continue;
				};

				int lastIndex = 0;
				int index = piece.Value.ToLower().IndexOf(phoneme);
				while (-1 != index)
				{
					if (0 != index)
					{
						Token token;
						token.Value = piece.Value.Substring(lastIndex, index - lastIndex);
						token.Type = TokenType.unparsed;
						list.Add(token);
					}
					Token token1;
					token1.Value = piece.Value.Substring(index, phoneme.Length);
					token1.Type = TokenType.phoneme;
					list.Add(token1);
					lastIndex = index + phoneme.Length;
					index = piece.Value.ToLower().IndexOf(phoneme, lastIndex);
				}
				string remainder = piece.Value.Substring(lastIndex);
				if (0 < remainder.Length)
				{
					Token token;
					token.Value = remainder;
					token.Type = TokenType.unparsed;
					list.Add(token);
				}
			}

			working = new Token[list.Count];
			for (int i=0; i<list.Count; ++i)
				working[i] = (Token)list[i];
		}
	}
}
