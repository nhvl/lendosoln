// Copyright 2000-2005 MeridianLink, Inc.  All Rights Reserved.
using System;
using System.Collections;
using System.Xml;

namespace CommonLib
{
	/// <summary>
	/// Holds Application Settings in normal .NET format that is reloaded automatically in thread-safe
	/// manner whenever the settings file changes.  Note: this can be the normal .config file or
	/// any other file as the path to the config file is an argument to the constructor.  Hence, dynamic
	/// settings can be spread out between multiple files.
	/// 
	/// If the input path does not have a value or the file doesn't exist, then all settings
	/// return as string.Empty
	/// 
	/// If the settings file is deleted or is unreadable then the previous settings are retained.
	/// 
	/// If the input key is not found as a setting key, then the settting value is returned as string.Empty.
	/// </summary>
	public class DynamicApplicationSettings : IDisposable
	{
		private ModifiedFileExpiringResource m_check;
		private string m_sPath;
		private Hashtable m_dict; // We use a hashtable because a StringDictionary is not case-sensitive for keys
		private bool m_bDisposed;

		public DynamicApplicationSettings(string path)
		{
			if (SafeString.HasValue(path) && System.IO.File.Exists(path))
			{
				this.m_sPath = path;

				Load();
				if (this.m_dict == null)
				{
					this.m_sPath = null;
				}
				else
				{
					this.m_check = new ModifiedFileExpiringResource(path, new ExpiringResource.HandleExpiredDelegate(Load));
				}
			}
		}

		~DynamicApplicationSettings()
		{
			if (!SafeString.HasValue(this.m_sPath)) return;
			Dispose(false);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		private void Dispose(bool disposing)
		{
			if (this.m_bDisposed) return;

			if (disposing)
			{
				this.m_check.Dispose();
				this.m_sPath = null;
				this.m_check = null;
				this.m_dict = null;
			}
			this.m_bDisposed = true;
		}

		public string this[string key]
		{
			get
			{
				if (!SafeString.HasValue(this.m_sPath)) return string.Empty;

				this.m_check.CheckExpired(this);

				Hashtable dict = this.m_dict;
				if (dict == null) return string.Empty;

				object obj = dict[key];
				return (obj == null) ? string.Empty : (string)obj;
			}
		}

		private void Load()
		{
			Hashtable dict = null;
			try // If the file gets deleted or cannot be read, just retain the last settings
			{
				XmlDocument doc = new XmlDocument();
				doc.Load(this.m_sPath);
				dict = Read(doc);
			}
			catch (Exception ex)
			{
				Tracer.Trace("CommonLib", "DynamicApplicationSettings", "Load", "err", this.m_sPath + " : " + ex.Message);
				return;
			}
			this.m_dict = dict;
		}

		private Hashtable Read(XmlDocument doc)
		{
			XmlElement root = (XmlElement)doc.SelectSingleNode("/configuration/appSettings");
			if (root == null) return null;

			XmlNodeList nodes = root.SelectNodes("add");
			if ((nodes == null) || (nodes.Count == 0)) return null;

			Hashtable dict = new Hashtable();
			foreach (XmlElement elem in nodes)
			{
				string sKey = elem.GetAttribute("key");
				if (!SafeString.HasValue(sKey)) continue;

				string sVal = elem.GetAttribute("value");
				dict[sKey] = sVal;
			}
			return dict;
		}
	}
}
