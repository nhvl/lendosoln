// Copyright 2000-2005 MeridianLink, Inc.  All Rights Reserved.
using System;
using System.Text;

namespace CommonLib
{
	/// <summary>
	/// Summary description for HtmlTableWriter.
	/// </summary>
	public class HtmlTableWriter : IDataWriter
	{
		private const int TABLE_LENGTH_OFFSET = 40;
		private const int ROW_LENGTH_OFFSET = 50;
		private const int EST_CELL_SIZE = 30;
		private const int EST_CELLS_PER_LINE = 50;
		private const int EST_NUM_LINES = 100;

		private const string DEFAULT_TABLE_CLASS = "TableStyle";
		private const string DEFAULT_TABLE_STYLE = "";
		private const string DEFAULT_TABLE_HEIGHT = "";
        private const string DEFAULT_TABLE_WIDTH = "";
		private const string DEFAULT_TABLE_BORDER = "0";
		private const string DEFAULT_TABLE_CELLSPACING = "";
		private const string DEFAULT_TABLE_CELLPADDING = "";

		private const string DEFAULT_FIRST_ROW_CLASS = "TableHeaderStyle";
		private const string DEFAULT_ODD_ROW_CLASS = "";
		private const string DEFAULT_EVEN_ROW_CLASS = "TableAlternatingItemStyle";
		private const string DEFAULT_ROW_STYLE = "text-align:left";

		private int m_nEstLineSize;
		private int m_nCellCount;
		private StringBuilder m_sb;
		private HtmlRowWriter m_current;
		private bool m_bRowOdd;

		private string m_sTableClass = DEFAULT_TABLE_CLASS;
		private string m_sTableStyle = DEFAULT_TABLE_STYLE;
		private string m_sTableBorder = DEFAULT_TABLE_BORDER;
        private string m_sTableHeight = DEFAULT_TABLE_HEIGHT;
        private string m_sTableWidth = DEFAULT_TABLE_WIDTH;
		private string m_sTableCellspacing = DEFAULT_TABLE_CELLSPACING;
		private string m_sTableCellpadding = DEFAULT_TABLE_CELLPADDING;
		private string m_sFirstRowClass = DEFAULT_FIRST_ROW_CLASS;
		private string m_sOddRowClass = DEFAULT_ODD_ROW_CLASS;
		private string m_sEvenRowClass = DEFAULT_EVEN_ROW_CLASS;
		private string m_sRowStyle = DEFAULT_ROW_STYLE;
		private string m_sOddRowStyle = DEFAULT_ROW_STYLE;
		private string m_sEvenRowStyle = DEFAULT_ROW_STYLE;
		private string m_sFirstRowStyle = DEFAULT_ROW_STYLE;

		public HtmlTableWriter()
		{
			this.m_nEstLineSize = ROW_LENGTH_OFFSET + EST_CELL_SIZE * EST_CELLS_PER_LINE;
			int nEstSize = TABLE_LENGTH_OFFSET + EST_NUM_LINES * this.m_nEstLineSize;
			this.m_sb = new StringBuilder(nEstSize);
		}

		public HtmlTableWriter(int estNumLines, int estNumEntriesPerLine)
		{
			this.m_nEstLineSize = ROW_LENGTH_OFFSET + EST_CELL_SIZE * estNumEntriesPerLine;
			int nEstSize = TABLE_LENGTH_OFFSET + estNumLines * this.m_nEstLineSize;
			this.m_sb = new StringBuilder(nEstSize);
		}

		public string TableClass
		{
			set
			{
				CheckInitPhase();
				this.m_sTableClass = (value == null) ? "" : value.Trim();
			}
		}

		public string TableStyle
		{
			set
			{
				CheckInitPhase();
				this.m_sTableStyle = (value == null) ? "" : value.Trim();
			}
		}

		public string TableHeight
		{
			set
			{
				CheckInitPhase();
				this.m_sTableHeight = (value == null) ? "" : value.Trim();
			}
		}

        public string TableWidth
        {
            set
            {
                CheckInitPhase();
                this.m_sTableWidth = (value == null) ? "" : value.Trim();
            }
        }

		public string TableBorder
		{
			set
			{
				CheckInitPhase();
				this.m_sTableBorder = (value == null) ? "" : value.Trim();
			}
		}

		public string TableCellspacing
		{
			set
			{
				CheckInitPhase();
				this.m_sTableCellspacing = (value == null) ? "" : value.Trim();
			}
		}

		public string TableCellpadding
		{
			set
			{
				CheckInitPhase();
				this.m_sTableCellpadding = (value == null) ? "" : value.Trim();
			}
		}

		public string FirstRowClass
		{
			set
			{
				CheckInitPhase();
				this.m_sFirstRowClass = (value == null) ? "" : value.Trim();
			}
		}

		public string OddRowClass
		{
			set
			{
				CheckInitPhase();
				this.m_sOddRowClass = (value == null) ? "" : value.Trim();
			}
		}

		public string EvenRowClass
		{
			set
			{
				CheckInitPhase();
				this.m_sEvenRowClass = (value == null) ? "" : value.Trim();
			}
		}

		public string RowStyle
		{
			get {return this.m_sRowStyle;}
			set
			{
				this.m_sRowStyle = (value == null) ? "" : value.Trim();
			}
		}

		public string FirstRowStyle
		{
			set
			{
				this.m_sFirstRowStyle = (value == null) ? "" : value.Trim();
			}
		}

		public string OddRowStyle
		{
			get {return this.m_sRowStyle;}
			set
			{
				this.m_sOddRowStyle = (value == null) ? "" : value.Trim();
			}
		}

		public string EvenRowStyle
		{
			get {return this.m_sRowStyle;}
			set
			{
				this.m_sEvenRowStyle = (value == null) ? "" : value.Trim();
			}
		}

		public void WriteData(string data)
		{
			if (this.m_current == null)
			{
				string sRowClass = null;
				string sRowStyle = this.m_sRowStyle;
				
				if (this.m_sb.Length == 0) 
				{
					sRowClass = this.m_sFirstRowClass;
					sRowStyle = this.m_sFirstRowStyle;
				}
				else if (this.m_bRowOdd) 
				{
					sRowClass = this.m_sOddRowClass;
					sRowStyle = this.m_sOddRowStyle;
				}
				else 
				{
					sRowClass = this.m_sEvenRowClass;
					sRowStyle = this.m_sEvenRowStyle;
				}

				this.m_current = new HtmlRowWriter(this.m_nEstLineSize, sRowClass, sRowStyle);
				this.m_bRowOdd = !this.m_bRowOdd;
			}
			if (this.m_sb.Length == 0) 
				this.m_current.isHead = true;
			this.m_current.WriteData(data);
		}

		public void EndLine()
		{
			if (this.m_current == null)
				throw new ApplicationException("Cannot end a line before writing data to it.");

			if (!this.m_current.HasData())
				throw new ApplicationException("Cannot end a line before writing data to it.");
			
			this.m_current.EndLine();
			string sLine = this.m_current.ToString();
	
			if (this.m_sb.Length == 0) // first line...use it as better estimate of line length
			{
				this.m_nCellCount = this.m_current.CellCount;
				this.m_nEstLineSize = sLine.Length;

				BeginTable();
			}
			
			else if (this.m_nCellCount != this.m_current.CellCount)
				throw new ApplicationException("Incorrect number of data items written to last line. " + this.m_nCellCount + ", " + this.m_current.CellCount);

			this.m_sb.Append(sLine);
			this.m_current = null;
		}

		public override string ToString()
		{
			if (this.m_current != null) EndLine();
			EndTable();
			string sTable = this.m_sb.ToString();
			this.m_sb = null;
			return sTable;
		}

		private void BeginTable()
		{
			this.m_sb.Append("<table");
			if (this.m_sTableClass.Length > 0) this.m_sb.Append(string.Format(" class='{0}'", this.m_sTableClass));
			if (this.m_sTableStyle.Length > 0) this.m_sb.Append(string.Format(" style='{0}'", this.m_sTableStyle));
            if (this.m_sTableHeight.Length > 0) this.m_sb.Append(string.Format(" height='{0}'", this.m_sTableHeight));
            if (this.m_sTableWidth.Length > 0) this.m_sb.Append(string.Format(" width='{0}'", this.m_sTableWidth));
			if (this.m_sTableBorder.Length > 0) this.m_sb.Append(string.Format(" border='{0}'", this.m_sTableBorder));
			if (this.m_sTableCellspacing.Length > 0) this.m_sb.Append(string.Format(" cellspacing='{0}'", this.m_sTableCellspacing));
			if (this.m_sTableCellpadding.Length > 0) this.m_sb.Append(string.Format(" cellpadding='{0}'", this.m_sTableCellpadding));
			this.m_sb.Append(">\n");
		}

		private void EndTable()
		{
			this.m_sb.Append("</tbody>\n");
			this.m_sb.Append("</table>\n");
		}

		private void CheckInitPhase()
		{
			if (this.m_sb.Length != 0)
				throw new ApplicationException("Properties can only be set before data is written.");
		}
	}

	internal class HtmlRowWriter
	{
		private StringBuilder m_sb;
		private int m_nCellCount;
		private string m_sRowClass;
		private string m_sRowStyle;
		public bool isHead = false;

		public HtmlRowWriter(int size, string rowClass, string rowStyle)
		{
			this.m_sb = new StringBuilder(size);
			this.m_sRowClass = rowClass;
			this.m_sRowStyle = rowStyle;
		}

		public bool HasData()
		{
			return (this.m_sb.Length > 0);
		}

		public int CellCount
		{
			get {return this.m_nCellCount;}
		}

		public void WriteData(string inString)
		{
			if (this.m_sb.Length == 0)
			{
				if(this.isHead)
					this.m_sb.Append("\t<thead>");
				this.m_sb.Append("\t<tr");
				if (this.m_sRowClass.Length > 0) this.m_sb.Append(string.Format(" class='{0}'", this.m_sRowClass));
				if (this.m_sRowStyle.Length > 0) this.m_sb.Append(string.Format(" style='{0}'", this.m_sRowStyle));
				this.m_sb.Append(">\n");
			}
			SafeWriteData(inString, this.isHead);
			++this.m_nCellCount;
		}

		public void EndLine()
		{
			if (this.m_sb.Length == 0)
				throw new ApplicationException("No data written");

			if(this.isHead)
			{
				this.m_sb.Append("\t</tr></thead><tbody>\n");
				this.isHead = false;
			}
			else
				this.m_sb.Append("\t</tr>\n");
		}

		public override string ToString()
		{
			return this.m_sb.ToString();
		}

		private void SafeWriteData(string inString, bool isHead)
		{
			string sData = (inString == null) ? "" : inString.Trim();
			sData = (sData.Length == 0) ? "&nbsp;" : sData;
            if (isHead)
                this.m_sb.Append(string.Format("\t\t<th>{0}</th>\n", sData));
            else
                this.m_sb.Append(string.Format("\t\t<td>{0}</td>\n", sData));
		}
	}
}
