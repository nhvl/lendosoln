using System;
using System.Collections;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;

namespace CommonLib
{
	/// <summary>
	/// Miscellaneous date utilities.
	/// </summary>
	public class DateUtil
	{
		private static Regex s_rgDatePart = new Regex(@"(CC)|(YY)|(MM)");
		private static Regex s_rgTimePart = new Regex(@"(hh)|(mm)");

		public static int DateDiff(string DateInterval, System.DateTime Day1, System.DateTime Day2)
		{
			try
			{
				if (Day1.Ticks == 0 || Day2.Ticks == 0) return (0) ;
				
				System.TimeSpan n = Day1.Subtract(Day2) ;
				switch (DateInterval.ToLower())
				{
					case "d" :
						return (n.Days) ;
					default :
						return (0) ;
				}
			}
			catch
			{
				return (0) ;
			}
		}
		public static bool IsDate(string date) 
		{
			try 
			{
				DateTime.Parse(date);
				return true ;
			}
			catch (FormatException) 
			{
				return false ;
			}
		}
		// CSD Uptime: 
		// Monday - Friday	6:00 am - 2:00 am	(EST)	3:00 am - 11:00 pm	(PST)
		// Saturday			7:00 am - 2:00 am	(EST)	4:00 am - 11:00 pm	(PST)
		// Sunday			8:00 am - 12:00 am	(EST)	5:00 am - 9:00 pm	(PST)
		// cn, 10/31/02
		//					5:00 am - 4:00 am	(EST)	2:00 am - 1:00 am (PST)
		
		public static bool CSDIsUp()
		{
			int hour = DateTime.Now.Hour ;
			/*
			DayOfWeek dayofweek = DateTime.Today.DayOfWeek ;
			
			if (dayofweek == DayOfWeek.Sunday)
			{
				return (hour >= 5 && hour < 21) ;
			}
			else if (dayofweek == DayOfWeek.Saturday)
			{
				return (hour >= 4 && hour < 23) ;
			}
			else
			{
				return (hour >= 3 && hour < 23) ;
			}
			*/
			return (hour >=2 || hour < 1) ;
		}

		// TODO: Roll all of these functions into the MCLDateTime class.		

		/// <summary>
		/// Converts user input into a CCYYMMDD format.
		/// </summary>
		/// <param name="sInput">User date input.</param>
		/// <returns>A string in CCYYMMDD format - or "" if the input can't be parsed.</returns>
		public static string ToCCYYMMDD(string sInput)
		{
            if ( sInput == null || sInput == "" )
				return "" ;

			string sOutput ;

			try
			{
				DateTime dt = DateTime.Parse(sInput) ;
				sOutput = dt.ToString("yyyyMMdd") ;
			}
			catch
			{
				try 
				{
					if ( sInput.Length == 8 )
					{
						DateTime dt = new DateTime( SafeConvert.ToInt(sInput.Substring(0,4)),
							SafeConvert.ToInt(sInput.Substring(4,2)),
							SafeConvert.ToInt(sInput.Substring(6,2)) ) ;
						if ( dt.Month < 1 || dt.Day < 1 )
							throw new FormatException() ;
						sOutput = dt.ToString("yyyyMMdd") ;
					}
					else
						throw new FormatException() ;
				}
				catch
				{
					sOutput = "" ;
				}
			}

			return sOutput ;
		}


		/// <summary>
		/// Converts user input into a MMDDCCYY format.
		/// </summary>
		/// <param name="sInput">User date input.</param>
		/// <returns>A string in MMDDCCYY format - or "" if the input can't be parsed.</returns>
		public static string ToMMDDCCYY(string sInput)
		{
			if ( sInput == null || sInput == "" )
				return "" ;

			string sOutput ;

			try
			{
				DateTime dt = DateTime.Parse(sInput) ;
				sOutput = dt.ToString("MMddyyyy") ;
			}
			catch
			{
				try 
				{
					if ( sInput.Length == 8 )
					{
						DateTime dt = new DateTime( SafeConvert.ToInt(sInput.Substring(0,4)),
							SafeConvert.ToInt(sInput.Substring(4,2)),
							SafeConvert.ToInt(sInput.Substring(6,2)) ) ;
						if ( dt.Month < 1 || dt.Day < 1 )
							throw new FormatException() ;
						sOutput = dt.ToString("MMddyyyy") ;
					}
					else
						throw new FormatException() ;
				}
				catch
				{
					sOutput = "" ;
				}
			}

			return sOutput ;
		}

		/// <summary>
		/// Converts user input into a CCYYMM format.
		/// </summary>
		/// <param name="sInput">User date input.</param>
		/// <returns>A string in CCYYMM format - or "" if the input can't be parsed.</returns>
		public static string ToCCYYMM(string sInput)
		{
			if ( sInput == null || sInput == "" )
				return "" ;

			string sOutput ;

			try
			{
				DateTime dt = DateTime.Parse(sInput) ;
				sOutput = dt.ToString("yyyyMM") ;
			}
			catch
			{
				try 
				{
					if ( sInput.Length == 6 || sInput.Length == 8 )
					{
						DateTime dt = new DateTime( SafeConvert.ToInt(sInput.Substring(0,4)),
							SafeConvert.ToInt(sInput.Substring(4,2)), 1 ) ;

						if ( dt.Month < 1 || dt.Day < 1 )
							throw new FormatException() ;
						sOutput = dt.ToString("yyyyMM") ;					
					}
					else
						throw new FormatException() ;
				}
				catch
				{
					sOutput = "" ;
				}
			}

			return sOutput ;
		}

		/// <summary>
		/// Converts user input into MMYY format.
		/// </summary>
		/// <param name="sInput">User date input.</param>
		/// <returns>A string in MMYY format - or "" if the input can't be parsed.</returns>
		public static string ToMMYY(string sInput)
		{
			if ( sInput == null || sInput == "" )
				return "" ;

			string sOutput ;

			try
			{
				DateTime dt = DateTime.Parse(sInput) ;
				sOutput = dt.ToString("MMyy") ;
			}
			catch
			{
				try 
				{
					if ( sInput.Length == 6 || sInput.Length == 8 )
					{
						DateTime dt = new DateTime( SafeConvert.ToInt(sInput.Substring(0,4)),
							SafeConvert.ToInt(sInput.Substring(4,2)), 1 ) ;

						if ( dt.Month < 1 || dt.Day < 1 )
							throw new FormatException() ;
						sOutput = dt.ToString("MMyy") ;					
					}
					else
						throw new FormatException() ;
				}
				catch
				{
					sOutput = "" ;
				}
			}

			return sOutput ;
		}


		/// <summary>
		/// Converts user input into a four-digit year format.
		/// </summary>
		/// <param name="sInput">User date input.</param>
		/// <returns>A string in CCYY format - or "" if the input can't be parsed.</returns>
		public static string ToCCYY(string sInput)
		{
			try
			{
				DateTime dt = DateTime.Parse(sInput) ;
				return dt.Year.ToString() ;
			}
			catch
			{
				if ( sInput.Length >= 4 )
				{
					int year = SafeConvert.ToInt( sInput.Substring(0,4) ) ;
					if ( year >= 1000 )
						return year.ToString() ;
				}
			}

			return "" ;
		}

		/// <summary>
		/// Similar to DateTime, but allows increased flexibility in parsing and
		/// missing values.
		/// </summary>
		public class MCLDateTime
		{
			// Store the date/time as an array of integers.
			// CC = Century.  0-99, int.MaxValue if unknown.
			// YY = Year.  0-99, int.MaxValue if unknown.
			// MM = Month.  1-12, int.MaxValue if unknown.
			// DD = Day.  1-31, int.MaxValue if unknown.
			// hh = Hour.  0-23, int.MaxValue if unknown.
			// mm = Minute.  0-59, int.MaxValue if unknown.
			// ss = Second.  0-59, int.MaxValue if unknown.

			// private static string[] SEGMENTS = {"CC", "YY", "MM", "DD", "hh", "mm", "ss"} ;

            private static string[,] ISO_DATE_SIGNATURES =
				{
					{@"^(\d{4})-?(\d{2})-?(\d{2})$", "$1$2$3", "CCYYMMDD"} ,
					{@"^(\d{4})-(\d{2})$", "$1$2", "CCYYMM"} ,
					{@"^(\d{4})$", "$1", "CCYY"} ,
					{@"^(\d{2})$", "00$1", "YYCC"} ,
					{@"^(\d{2})-?(\d{2})-?(\d{2})$", "CC$1$2$3", "CCYYMMDD"} ,
					{@"^-(\d{2})-?(\d{2})$", "CC$1$2", "CCYYMM"} ,
					{@"^-(\d{2})$", "CC$1", "CCYY"} ,
					{@"^--(\d{2})-?(\d{2})$", "CCYY$1$2", "CCYYMMDD"} ,
					{@"^--(\d{2})$", "CCYY$1", "CCYYMM"} ,
					{@"^---(\d{2})$", "CCYYMM$1", "CCYYMMDD"}
				} ;

			private static string[,] ISO_TIME_SIGNATURES =
				{
					{ @"^(\d{2}):?(\d{2}):?(\d{2})$", "$1$2$3", "hhmmss" } ,
					{ @"^(\d{2}):?(\d{2})$", "$1$2", "hhmm" } ,
					{ @"^(\d{2})$", "$1", "hh" } ,
					{ @"^-(\d{2}):?(\d{2})$", "hh$1$2", "hhmmss" } ,
					{ @"^-(\d{2})$", "hh$1", "hhmm" } ,
					{ @"^--(\d{2})$", "hhmm$1", "hhmmss" }
				} ;

			private static System.Collections.Hashtable s_dateRegexTable;
			private static System.Collections.Hashtable s_timeRegexTable;
					

			private int[] MLDate ;
			// private string m_sLastFormat ;

			static MCLDateTime()
			{
				int nLen = ISO_DATE_SIGNATURES.Length / 3;
				s_dateRegexTable = new System.Collections.Hashtable();
				for (int i=0; i<nLen; ++i)
				{
					string sReg = ISO_DATE_SIGNATURES[i, 0];
					s_dateRegexTable[sReg] = new Regex(sReg);
				}

				nLen = ISO_TIME_SIGNATURES.Length / 3;
				s_timeRegexTable = new System.Collections.Hashtable();
				for (int i=0; i<nLen; ++i)
				{
					string sReg = ISO_TIME_SIGNATURES[i, 0];
					s_timeRegexTable[sReg] = new Regex(sReg);
				}
			}

			// Initializes a blank MCLDateTime object.
			public MCLDateTime()
			{
				MLDate = new int[7] ;
				ClearValues() ;
			}

			/// <summary>
			/// For internal use only.  Used to create a copy of an instance.
			/// </summary>
			/// <param name="date"></param>
			private MCLDateTime(int[] date)
			{
				MLDate = date ;
			}

			/// <summary>
			/// Configures a date using an input string and a format string.
			/// </summary>
			/// <param name="date">Date to parse.</param>
			/// <param name="formats">One or more format strings.
			/// Use an X12 format qualifier or an arrangement of CC, YY, MM, DD, hh, mm, ss.</param>
			/// <returns>The format string accepted through parsing.</returns>
			public string ParseDate(string date, params string[] formats)
			{
				if ( formats.Length == 0 )
					throw new MissingFieldException("No format string provided.") ;

				MLDate = new int[7] ;
				ClearValues() ;

				foreach ( string format in formats )
				{
					if ( ParseDateTime(date, format) )
					{
						// m_sLastFormat = format ;
						return format ;
					}
				}

				return "" ;
			}

			/// <summary>
			/// Attempts to initialize this instance using the specified date and time.
			/// </summary>
			/// <param name="date">Date to parse</param>
			/// <param name="format">Format to use</param>
			/// <returns>True if parsed successfully, False otherwise.</returns>
			private bool ParseDateTime(string date, string format)
			{
                format = X12Qualifier_2_Format(format) ;

				// Now, attempt to parse the string using the correct format information.
				try
				{
					int formatLen = format.Length ;
					if ( formatLen % 2 != 0 )
						throw new FormatException("Invalid format string.") ;

					if ( date.Length != formatLen )
						throw new FormatException("Date and Format lengths do not match.") ;
			
					for ( int idx = 0 ; idx < formatLen ; idx = idx + 2 )
					{
						string segFormat = format.Substring(idx, 2) ;
						int segValue = IntValue( date.Substring(idx, 2) ) ;

						switch ( segFormat )
						{
							case "CC" :
								Century = Range(segValue, 0, 99) ;
								break ;
							case "YY" :
								Year = Range(segValue, 0, 99) ;
								break ;
							case "MM" :
								Month = Range(segValue, 1, 12) ;
								break ;
							case "DD" :
								Day = Range(segValue, 1, 31) ;
								break ;
							case "hh" :
								Hour = Range(segValue, 0, 23) ;
								break ;
							case "mm" :
								Minute = Range(segValue, 0, 59) ;
								break ;
							case "ss" :
								Second = Range(segValue, 0, 59) ;
								break ;
							default :
								throw new FormatException("Unknown segment: " + segFormat) ;
						}
					}
				}
				catch ( FormatException )
				{
					// Clear anything we may have changed.
					ClearValues() ;
					return false ;
				}
				catch ( ArgumentOutOfRangeException )
				{
					ClearValues() ;
					return false ;
				}

				return true ;
			}

			/// <summary>
			/// Parses a date passed in ISO-8861 format - for example, MISMO.
			/// </summary>
			/// <param name="date">ISO-8861:1988 format date or time</param>
			/// <remarks>No periods, weeks, or day counts in this version.</remarks>
			public void ParseISO8861Date(string date)
			{
				MLDate = new int[7] ;
				ClearValues() ;

				DateTime dt = DateTime.Now ;

				string sDate, sTime ;
				int nTimeIdx = date.IndexOf('T') ;
				if ( 0 <= nTimeIdx )
				{
					sDate = date.Substring(0, nTimeIdx) ;
					sTime = date.Substring(nTimeIdx+1) ;
				}
				else
				{
					if ( 0 <= date.IndexOf(':') )
					{
						sDate = "" ;
						sTime = date ;						
					}
					else
					{
						sDate = date ;
						sTime = "" ;
					}
				}

				Match match ;
				string sLastFormat = null ;

				if ( sDate != "" )
				{
					for ( int nRow = 0 ; nRow < ISO_DATE_SIGNATURES.Length / 3 ; nRow++ )
					{
						match = ((Regex)s_dateRegexTable[ISO_DATE_SIGNATURES[nRow, 0]]).Match(sDate);
						//match = Regex.Match(sDate, ISO_DATE_SIGNATURES[nRow,0]) ;
						if ( match.Success )
						{
							string sValue, sFormat ;
							sValue = match.Result(ISO_DATE_SIGNATURES[nRow,1]) ;
							sFormat = ISO_DATE_SIGNATURES[nRow,2] ;

							if (s_rgDatePart.IsMatch(sValue))
							//if ( Regex.IsMatch(sValue, "(CC)|(YY)|(MM)") )
							{
								sValue = sValue.Replace("CC", (dt.Year/100).ToString("D2")) ;
								sValue = sValue.Replace("YY", (dt.Year%100).ToString("D2")) ;
								sValue = sValue.Replace("MM", dt.Month.ToString("D2")) ;
							}

							ParseDateTime(sValue, sFormat) ;
							sLastFormat = sFormat ;
							break ;
						}
					}

					if ( null == sLastFormat )
						throw new FormatException(String.Format("Unrecognized ISO-8861 date: '{0}'", sDate)) ;
				}

				if ( sTime != "" )
				{
					for ( int nRow = 0 ; nRow < ISO_TIME_SIGNATURES.Length / 3 ; nRow++ )
					{
						match = ((Regex)s_timeRegexTable[ISO_TIME_SIGNATURES[nRow, 0]]).Match(sTime);
						//match = Regex.Match(sTime, ISO_TIME_SIGNATURES[nRow,0]) ;
						if ( match.Success )
						{
							string sValue, sFormat ;
							sValue = match.Result(ISO_TIME_SIGNATURES[nRow,1]) ;
							sFormat = ISO_TIME_SIGNATURES[nRow,2] ;

							if (s_rgTimePart.IsMatch(sValue))
							//if ( Regex.IsMatch(sValue, "(hh)|(mm)") )
							{
								sValue = sValue.Replace("hh", dt.Hour.ToString("D2")) ;
								sValue = sValue.Replace("mm", dt.Minute.ToString("D2")) ;
							}

							ParseDateTime(sValue, sFormat) ;
							sLastFormat = ( null == sLastFormat ) ? sFormat : sLastFormat + sFormat ;
							break ;
						}
					}

					if ( null == sLastFormat )
						throw new FormatException(String.Format("Unrecognized ISO-8861 time: '{0}'", sTime)) ;
				}
			}

			/// <summary>
			/// Outputs a date formatted with the specified string.
			/// </summary>
			/// <param name="formats">One or more format strings, containing CC, YY, MM, DD, hh, mm, ss.
			/// Other characters (such as / or :) will be echoed in the output.</param>
			public string ToString(params string[] formats)
			{
				if ( formats.Length == 0 )
				{
//					if ( null == m_sLastFormat )					
						throw new MissingFieldException("No format string found.") ;
//					else
//						return FormatDate(m_sLastFormat) ;
				}

				foreach ( string format in formats )
				{
                   	string retVal = FormatDate(X12Qualifier_2_Format(format)) ;
					if ( retVal != "" )
						return retVal ;
				}

				return "" ;
			}

			/// <summary>
			/// Converts this representation into a System.DateTime.
			/// </summary>
			public DateTime ToDateTime()
			{
				return ToDateTime(true) ;
			}

			/// <summary>
			/// Converts this representation into a System.DateTime.
			/// </summary>
			/// <param name="bThrowExceptions">True to throw an exception on missing year/month/day data,
			/// False to initialize that missing data with today's values.</param>
			public DateTime ToDateTime(bool bThrowExceptions)
			{
				int year, month, day, hour, minute, second ;
				bool bUseTime = true ;
				DateTime dtNow = DateTime.Now ;

				year = Year ;
				if ( year == int.MaxValue )
				{
					if ( bThrowExceptions )
						throw new MissingFieldException("Year not specified.") ;
					else
						year = dtNow.Year % 100 ;
				}

				if ( Century == int.MaxValue )
				{
					if ( bThrowExceptions )
						throw new MissingFieldException("Century not specified.") ;
					else
						year += ( (dtNow.Year / 100) * 100 ) ;
				}
				else
					year += Century * 100 ;

				month = Month ;
				if ( month == int.MaxValue )
				{
					if ( bThrowExceptions )
						throw new MissingFieldException("Month not specified.") ;
					else
						month = dtNow.Month ;
				}

				day = Day ;
				if ( day == int.MaxValue )
				{
					if ( bThrowExceptions )
						throw new MissingFieldException("Day not specified.") ;
					else
						// 07/29/03-Binh-Why are you using the current day/date for this?
						// Anyway, an exception is being raised when the input data is
						// 200302. Note that we do not have 02/29/2003.
						//day = dtNow.Day ;
						day = 1 ;
				}

				hour = Hour ;
				if ( hour == int.MaxValue )
				{
					bUseTime = false ;
					minute = second = int.MaxValue ;
				}
				else
				{
					minute = Minute ;
					if ( minute == int.MaxValue )
					{
						bUseTime = false ;
						second = int.MaxValue ;
					}
					else
					{
						second = Second ;
						if ( second == int.MaxValue )
							bUseTime = false ;
					}
				}

                if ( bUseTime )
					return new DateTime(year, month, day, hour, minute, second) ;
				else
					return new DateTime(year, month, day) ;
			}

			private string FormatDate(string format)
			{
				int idx = 0 ;
				int formatLen = format.Length ;

				System.Text.StringBuilder sb = new System.Text.StringBuilder() ;

				try
				{

					while ( idx < formatLen ) 
					{
						if ( idx == formatLen - 1 )
						{
							sb.Append( format.Substring(idx,1) ) ;
							break ;
						}

						string formatCode = format.Substring(idx,2) ;
						switch ( formatCode )
						{
							case "CC" :
								sb.Append( StringValue(Century) ) ;
								break ;
							case "YY" :
								sb.Append( StringValue(Year) ) ;
								break ;
							case "MM" :
								sb.Append( StringValue(Month) ) ;
								break ;
							case "DD" :
								sb.Append( StringValue(Day) ) ;
								break ;
							case "hh" :
								sb.Append( StringValue(Hour) ) ;
								break ;
							case "mm" :
								sb.Append( StringValue(Minute) ) ;
								break ;
							case "ss" :
								sb.Append( StringValue(Second) ) ;
								break ;
							default :
								sb.Append( formatCode.Substring(0,1) ) ;
								idx++ ;
								continue ;
						}

						idx = idx + 2 ;
					}
				}
				catch
				{
					return "" ;
				}

				return sb.ToString() ;
			}

			/// <summary>
			/// Compares two MCLDateTimes.
			/// </summary>
			/// <param name="otherDT">Other MCL datetime.</param>
			/// <returns>1 if this instance is greater than the parameter, -1 if it is less, or 0 if they are equal.</returns>
			public int CompareTo(MCLDateTime otherDT)
			{
				if ( this.Century != otherDT.Century )
				{
					if ( this.Century > otherDT.Century && this.Century != int.MaxValue )
						return 1 ;
					else
						return -1 ;
				}

				if ( this.Year != otherDT.Year )
				{
					if ( this.Year > otherDT.Year && this.Year != int.MaxValue )
						return 1 ;
					else
						return -1 ;
				}

				if ( this.Month != otherDT.Month )
				{
					if ( this.Month > otherDT.Month && this.Month != int.MaxValue )
						return 1 ;
					else
						return -1 ;
				}

				if ( this.Day != otherDT.Day )
				{
					if ( this.Day > otherDT.Day && this.Day != int.MaxValue )
						return 1 ;
					else
						return -1 ;
				}
				if ( this.Hour != otherDT.Hour )
				{
					if ( this.Hour > otherDT.Hour && this.Hour != int.MaxValue )
						return 1 ;
					else
						return -1 ;
				}

				if ( this.Minute != otherDT.Minute )
				{
					if ( this.Minute > otherDT.Minute && this.Minute != int.MaxValue )
						return 1 ;
					else
						return -1 ;
				}

				if ( this.Second != otherDT.Second )
				{
					if ( this.Second > otherDT.Second && this.Second != int.MaxValue )
						return 1 ;
					else
						return -1 ;
				}

				return 0 ;
			}

			/// <summary>
			/// Generates a copy of this instance.
			/// </summary>
			/// <returns>A copy of this MCLDateTime value.</returns>
			public MCLDateTime Copy()
			{
				return new MCLDateTime(MLDate) ;
			}


			/// <summary>
			/// Converts an integer to a two-character string.
			/// </summary>
			/// <param name="num">Int to convert.</param>
			/// <returns>A two-character string.</returns>
			/// <exception>Throws a ArgumentOutOfRangeException on Int32.MaxValue</exception>
			private string StringValue(int num)
			{
				if ( num == int.MaxValue )
					throw new ArgumentOutOfRangeException("Field does not exist.") ;

				return num.ToString("D2") ;
			}

			/// <summary>
			/// Converts an X12 Date Format Qualifier to an absolute format string.
			/// If the string isn't X12, it is passed through unchanged.
			/// </summary>
			/// <param name="format">X12 Date Format Qualifier, or pass-through</param>
			/// <returns>A format string, or pass-through.</returns>
			private string X12Qualifier_2_Format( string format )
			{
				// If the format is an X12 type, then change it to its component analogue.
				switch ( format )
				{
					case "CM" :
						return "CCYYMM" ;
					case "CY" :
						return "CCYY" ;
					case "D8" :
						return "CCYYMMDD" ;
					case "DT" :
						return "CCYYMMDDhhmm" ;
					case "RTS" :
						return "CCYYMMDDhhmmss" ;
					default :	// In this case: it's regular format, invalid X12, or unsupported X12.
						return format ;
				}
			}


			/// <summary>
			/// Gets or sets the two-digit century.
			/// </summary>
			public int Century
			{
				get { return MLDate[0] ; }
				set { MLDate[0] = value ; }
			}
			
			/// <summary>
			/// Gets or sets the two-digit year.
			/// </summary>
			public int Year
			{
				get { return MLDate[1] ; }
				set { MLDate[1] = value ; }
			}

			/// <summary>
			/// Gets or sets the month.
			/// </summary>
			public int Month
			{
				get { return MLDate[2] ; }
				set { MLDate[2] = value ; }
			}

			/// <summary>
			/// Gets or sets the day.
			/// </summary>
			public int Day
			{
				get { return MLDate[3] ; }
				set { MLDate[3] = value ; }
			}

			/// <summary>
			/// Gets or sets the hour.
			/// </summary>
			public int Hour
			{
				get { return MLDate[4] ; }
				set { MLDate[4] = value ; }
			}

			/// <summary>
			/// Gets or sets the minute.
			/// </summary>
			public int Minute
			{
				get { return MLDate[5] ; }
				set { MLDate[5] = value ; }
			}

			/// <summary>
			/// Gets or sets the second.
			/// </summary>
			public int Second
			{
				get { return MLDate[6] ; }
				set { MLDate[6] = value ; }
			}
			
			/// <summary>
			/// Assures that a number is between a maximum and minimum value.
			/// Returns Int32.MaxValue otherwise.
			/// </summary>
			/// <param name="num">Int32 to convert.</param>
			/// <param name="minValue">Minimum value.</param>
			/// <param name="maxValue">Maximum value.</param>
			/// <returns>Integer value.</returns>
			/// <exception cref="ArgumentOutOfRangeException" />
			private int Range(int num, int minValue, int maxValue)
			{
				if ( minValue <= num && maxValue >= num )
					return num ;
				else
					throw new ArgumentOutOfRangeException("num", num, "Number out of range.") ;
			}

			/// <summary>
			/// Converts an object [String, Int32, etc] into an Int32.  Returns Int32.MaxValue on error.
			/// </summary>
			/// <param name="obj">Object to convert.</param>
			/// <returns>Integer value.</returns>
			private int IntValue(Object obj)
			{
				try
				{
					return Convert.ToInt32(obj) ;
				}
				catch
				{
					return int.MaxValue ;
				}
			}

			private void ClearValues()
			{
				for ( int idx = 0 ; idx < MLDate.Length ; idx++ )
					MLDate[idx] = int.MaxValue ;

				// m_sLastFormat = null ;
			}
		}


	}

	/// <summary>
	/// Utility class that applies calculations to dates as viewed as contained in a single calendar year.
	/// </summary>
	public abstract class CalendarYear
	{
		/// <summary>
		/// Midnight of January 1st for the input year.
		/// </summary>
		public static DateTime JanuaryFirst(int year)
		{
			return new DateTime(year, 1, 1);
		}

		/// <summary>
		/// Midnight of December 31st for the input year.
		/// </summary>
		public static DateTime DecemberLast(int year)
		{
			return new DateTime(year, 12, 31);
		}

		/// <summary>
		/// The number of days that fall on the first week (Sun - Sat) of the calendar for the input year.
		/// </summary>
		public static int FirstWeekDays(int year)
		{
			DateTime dtJanFirst = JanuaryFirst(year);
			int nFirstWeekDays = 7 - (int)dtJanFirst.DayOfWeek; // number of days in the first week
			return nFirstWeekDays;
		}

		/// <summary>
		/// Returns true if the input date falls in the first week of the year.
		/// </summary>
		public static bool IsInFirstWeek(DateTime date)
		{
			int nFirstWeekDays = FirstWeekDays(date.Year);
			return (nFirstWeekDays >= date.DayOfYear);
		}

		/// <summary>
		/// The number of days that fall on the last week (Sun - Sat) of the calendar for the input year.
		/// </summary>
		public static int LastWeekDays(int year)
		{
			DateTime dtDecemberLast = DecemberLast(year);
			DateTime dtLastWeekBegin = WeekBegin(dtDecemberLast);
			TimeSpan span = dtDecemberLast - dtLastWeekBegin;
			return span.Days + 1;
		}

		/// <summary>
		/// Returns true if the input date falls in the last week of the year.
		/// </summary>
		public static bool IsInLastWeek(DateTime date)
		{
			int nLastWeekDays = LastWeekDays(date.Year);
			int nDaysInYear = DaysInYear(date.Year);
			return (date.DayOfYear > (nDaysInYear - nLastWeekDays));
		}

		/// <summary>
		/// Return true if the input year is a leap year.
		/// </summary>
		public static bool IsLeapYear(int year)
		{
			DateTime dtFebFirst = new DateTime(year, 2, 1);
			DateTime dt = dtFebFirst.AddDays(28);
			return (dt.Month == 2);
		}

		/// <summary>
		/// Return the correct number of days in the year, taking into account leap years.
		/// </summary>
		/// <param name="year"></param>
		/// <returns></returns>
		public static int DaysInYear(int year)
		{
			return (IsLeapYear(year)) ? 366 : 365;
		}

		public static int WeeksInYear(int year)
		{
			DateTime dtDecLast = DecemberLast(year);
			return Week(dtDecLast);
		}

		/// <summary>
		/// Returns the midnight which starts the input date.
		/// </summary>
		public static DateTime ChopTime(DateTime date)
		{
			return new DateTime(date.Year, date.Month, date.Day);
		}

		/// <summary>
		/// Gives the row number (1-based) of the calendar as written as a contiguous stretch of week rows
		/// for the input date.
		/// </summary>
		public static int Week(DateTime date)
		{
			int nFirstWeekDays = FirstWeekDays(date.Year);
			int nWeek = (date.DayOfYear <= nFirstWeekDays) ? 1 : (date.DayOfYear - 1 - nFirstWeekDays) / 7 + 2;
			return nWeek;
		}

		/// <summary>
		/// Gives the midnight that starts the first day of the week in which the input date belongs.
		/// For the first week this returns January 1st.
		/// </summary>
		public static DateTime WeekBegin(DateTime date)
		{
			if (IsInFirstWeek(date)) return JanuaryFirst(date.Year);

			DateTime day = ChopTime(date);
			int nDayOfWeek = (int)day.DayOfWeek;
			return day.AddDays( - nDayOfWeek );
		}

		/// <summary>
		/// Gives the midnight that starts the last day of the week in which the input date belongs.
		/// For the last week this returns December 31st.
		/// </summary>
		public static DateTime WeekEnd(DateTime date)
		{
			if (IsInLastWeek(date)) return DecemberLast(date.Year);

			DateTime day = ChopTime(date);
			int nDayOfWeek = (int)day.DayOfWeek;
			return day.AddDays(6 - nDayOfWeek);
		}

		/// <summary>
		/// Gives the midnight that starts the first day of the month in which the input date belongs.
		/// </summary>
		public static DateTime MonthBegin(DateTime date)
		{
			return new DateTime(date.Year, date.Month, 1);
		}

		/// <summary>
		/// Gives the midnight that starts the last day of the month in which the input date belongs.
		/// </summary>
		public static DateTime MonthEnd(DateTime date)
		{
			DateTime dtFirst = MonthBegin(date);
			DateTime dtNext = dtFirst.AddMonths(1);
			return dtNext.AddDays(-1);
		}
	}

	/// <summary>
	/// Iterates through whole calendar weeks (except end of year is split into two weeks if it falls in the middle
	/// of the week).  WARNING: assumes that the timespan doesn't overlap more than two calendar years.
	/// </summary>
	/// <remarks>
	/// Translated from MCLDEV/support/admin_reports/trend.asp
	/// </remarks>
	public class WeekIter
	{
		private int m_nStartYear;
		private int m_nStartWeek;
		private DateTime m_dtWeekStart;
		private int m_nEndYear;
		private int m_nEndWeek;
	
		private int m_nCurrentYear;
		private int m_nCurrentWeek;
		private DateTime m_dtCurrentWeekStart;
		private int m_nWeekCount;

		public WeekIter(string startDate, string endDate)
		{
			DateTime dtStart = DateTime.Parse(startDate);
			DateTime dtEnd = DateTime.Parse(endDate);

			this.m_nStartYear = dtStart.Year;
			this.m_nStartWeek = CalendarYear.Week(dtStart);
			this.m_dtWeekStart = CalendarYear.WeekBegin(dtStart);

			this.m_nEndYear = dtEnd.Year;
			this.m_nEndWeek = CalendarYear.Week(dtEnd);

			if (this.m_nStartYear == this.m_nEndYear)
			{
				this.m_nWeekCount = this.m_nEndWeek - this.m_nStartWeek + 1;
			}
			else
			{
				// ASSUME that at most two years are involved
				int nFirstYear = CalendarYear.WeeksInYear(this.m_nStartYear);
				this.m_nWeekCount = (nFirstYear - this.m_nStartWeek + 1) + this.m_nEndWeek;
			}

			Reset();
		}

		public int WeekCount
		{
			get {return this.m_nWeekCount;}
		}

		public int CurrentYear
		{
			get {return this.m_nCurrentYear;}
		}

		public int CurrentWeek
		{
			get {return this.m_nCurrentWeek;}
		}

		public string CurrentDateRange
		{
			get
			{
				DateTime dtWeekEnd = CalendarYear.WeekEnd(this.m_dtCurrentWeekStart);
				return string.Format("{0} - {1}", this.m_dtCurrentWeekStart.ToShortDateString(), dtWeekEnd.ToShortDateString());
			}
		}

		public void Reset()
		{
			this.m_nCurrentYear = this.m_nStartYear;
			this.m_nCurrentWeek = this.m_nStartWeek;
			this.m_dtCurrentWeekStart = this.m_dtWeekStart;
		}

		public bool MoveNext()
		{
			if ((this.m_nCurrentYear == this.m_nEndYear) && (this.m_nCurrentWeek == this.m_nEndWeek)) return false;

			if (CalendarYear.IsInLastWeek(this.m_dtCurrentWeekStart))
			{
				this.m_nCurrentWeek = 1;
				this.m_dtCurrentWeekStart = CalendarYear.JanuaryFirst(++this.m_nCurrentYear);
			}
			else if (CalendarYear.IsInFirstWeek(this.m_dtCurrentWeekStart))
			{
				++this.m_nCurrentWeek;
				this.m_dtCurrentWeekStart = CalendarYear.WeekBegin(this.m_dtCurrentWeekStart.AddDays(7));
			}
			else
			{
				bool bFirstWeek = CalendarYear.IsInFirstWeek(this.m_dtCurrentWeekStart);
				++this.m_nCurrentWeek;
				this.m_dtCurrentWeekStart = this.m_dtCurrentWeekStart.AddDays(7);
				if (bFirstWeek) this.m_dtCurrentWeekStart = CalendarYear.WeekBegin(this.m_dtCurrentWeekStart);
			}

			return true;
		}
	}

	/// <summary>
	/// Represents a range of dates.
	/// </summary>
	public interface IDateRange
	{
		/// <summary>
		/// Expresses this date range in SQL syntax.
		/// </summary>
		/// <returns>
		/// A new SQLWhereStringBuilder containing this date range as an AND clause.
		/// This object may be blank.
		/// </returns>
		SQLWhereStringBuilder GetSQLDateRange(string sFieldName) ;

		/// <summary>
		/// Expresses this date range in SQL syntax.
		/// </summary>
		/// <param name="sbWhere">An existing SQLWhereStringBuilder.  The date range will be added as an AND clause.</param>
		void AddSQLDateRange(string sFieldName, SQLWhereStringBuilder sbWhere) ;

		/// <summary>
		/// Determines if a specified date is within the date range.
		/// </summary>
		/// <param name="dtToCompare">Date to compare.</param>
		/// <returns>True if the date is in this date range, False otherwise.</returns>
		bool IsInRange(DateTime dtToCompare) ;
	}

	/// <summary>
	/// Represents a consecutive date range.
	/// </summary>
	[Serializable]
	public class ConsecutiveDateRange : IDateRange, ISerializable
	{
		public const int Version = 1 ;

		private DateTime m_dtStart, m_dtEnd ;
		public bool m_bInclusive ;

		/// <summary>
		/// Creates an uninitialized consecutive date range.
		/// </summary>
		public ConsecutiveDateRange() 
		{
			m_dtStart = DateTime.MinValue ;
			m_dtEnd = DateTime.MaxValue ;
			m_bInclusive = false ;
		}

		/// <summary>
		/// Determines if the timestamps are inclusive or exclusive.
		/// If true, both the date and time for the given DateTimes are used, and generated SQL statements include both values.
		/// If false, only the dates are used.  [This is the default.]
		/// </summary>
		public bool TimeInclusive
		{
			get { return m_bInclusive ; }
			set { m_bInclusive = value ; }
		}

		/// <summary>
		/// Gets or sets the start date.
		/// </summary>
		public DateTime StartDate
		{
			get { return m_dtStart ; }
			set 
			{
				if ( value > m_dtEnd )
					throw new ArgumentException("The start date must not be after the end date.") ;
				m_dtStart = value ;
			}
		}

		/// <summary>
		/// Gets or sets the end date.
		/// </summary>
		public DateTime EndDate
		{
			get { return m_dtEnd ; }
			set 
			{
				if ( value < m_dtStart )
					throw new ArgumentException("The end date must not be before the start date.") ;
				m_dtEnd = value ;
			}
		}

		public SQLWhereStringBuilder GetSQLDateRange(string sFieldName)
		{
			SQLWhereStringBuilder sbWhere = new SQLWhereStringBuilder() ;
			AddSQLDateRange(sFieldName, sbWhere) ;
			return sbWhere ;
		}

		public void AddSQLDateRange(string sFieldName, SQLWhereStringBuilder sbWhere)
		{
			if ( ! SafeString.HasValue(sFieldName) )
				throw new ArgumentException("The SQL field name must be provided.", "sFieldName") ;

			bool bHasStart, bHasEnd ;
			bHasStart = (m_dtStart != DateTime.MinValue) ;
			bHasEnd = (m_dtEnd != DateTime.MaxValue) ;

			if ( ! (bHasStart || bHasEnd) )
				return ;

			bool bUseParens = (bHasStart && bHasEnd) ;

			if ( bUseParens )
				sbWhere.OpenANDParen() ;

			if ( bHasStart )
			{
				if ( m_bInclusive )
					sbWhere.AppendANDCondition(String.Format("{0} >= {1}", sFieldName, SqlUtil.SQLString(m_dtStart.ToString("G")))) ;
				else
					sbWhere.AppendANDCondition(String.Format("{0} >= {1}", sFieldName, SqlUtil.SQLString(m_dtStart.ToString("d")))) ;
			}
			if ( bHasEnd )
			{
				if ( m_bInclusive )
					sbWhere.AppendANDCondition(String.Format("{0} <= {1}", sFieldName, SqlUtil.SQLString(m_dtEnd.ToString("G")))) ;
				else
					sbWhere.AppendANDCondition(String.Format("{0} < {1}", sFieldName, SqlUtil.SQLString(m_dtEnd.AddDays(1.0d).ToString("d")))) ;
			}

			if ( bUseParens )
				sbWhere.CloseParen() ;
		}

		public bool IsInRange(DateTime dtToCompare)
		{
			if ( m_bInclusive )
				return ( dtToCompare >= m_dtStart && dtToCompare <= m_dtEnd ) ;
			else
				return ( dtToCompare >= m_dtStart.Date && (m_dtEnd == DateTime.MaxValue || dtToCompare < (m_dtEnd.Date.AddDays(1.0d))) ) ;
				
		}

		public override string ToString()
		{
			bool bHasStart, bHasEnd ;
			bHasStart = (m_dtStart != DateTime.MinValue) ;
			bHasEnd = (m_dtEnd != DateTime.MaxValue) ;

			if ( ! (bHasStart || bHasEnd) )
				return "No range specified" ;
			else if ( !m_bInclusive && (m_dtStart.Date == m_dtEnd.Date) )
				return "On " + m_dtStart.ToString("d") ;
			else
			{
				System.Text.StringBuilder sb = new System.Text.StringBuilder() ;
				if ( bHasStart )
				{
					sb.Append("From ") ;
					if (m_bInclusive)
                        sb.Append(m_dtStart.ToString("G")) ;
					else
						sb.Append(m_dtStart.ToString("d")) ;

					if (bHasEnd)
						sb.Append(" to ") ;
				}
				else
					sb.Append("Before ") ;

				if (bHasEnd)
				{
					if (m_bInclusive)
						sb.Append(m_dtEnd.ToString("G")) ;
					else
						sb.Append(m_dtEnd.ToString("d")) ;	// Yes, this differs from the SQL statement.
				}

				return sb.ToString() ;
			}
		}

		protected ConsecutiveDateRange(SerializationInfo info, StreamingContext context)
		{
			m_dtStart = info.GetDateTime("start") ;
			m_dtEnd = info.GetDateTime("end") ;
			m_bInclusive = info.GetBoolean("useTime") ;
		}
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("version", ConsecutiveDateRange.Version) ;
			info.AddValue("start", m_dtStart) ;
			info.AddValue("end", m_dtEnd) ;
			info.AddValue("useTime", m_bInclusive) ;
		}
	}

	/// <summary>
	/// Represents a potentially nonconsecutive date range.
	/// </summary>
	[Serializable]
	public class NonConsecutiveDateRange : IDateRange, ISerializable
	{
		public const int Version = 1 ;

		private ArrayList m_aDates ;

		/// <summary>
		/// Creates an uninitialized nonconsecutive date range.
		/// </summary>
		public NonConsecutiveDateRange()
		{
			m_aDates = new ArrayList() ;
		}

		public void AddDate(DateTime dt)
		{
			DateTime date = dt.Date ;
			if ( ! m_aDates.Contains(date) )
				m_aDates.Add(date) ;
		}

		public SQLWhereStringBuilder GetSQLDateRange(string sFieldName)
		{
			SQLWhereStringBuilder sbWhere = new SQLWhereStringBuilder() ;
			AddSQLDateRange(sFieldName, sbWhere) ;
			return sbWhere ;
		}

		public void AddSQLDateRange(string sFieldName, SQLWhereStringBuilder sbWhere)
		{
			if ( ! SafeString.HasValue(sFieldName) )
				throw new ArgumentException("The SQL field name must be provided.", "sFieldName") ;

			if ( m_aDates.Count == 0 )
				return ;

			sbWhere.OpenANDParen() ;

			foreach ( DateTime dt in m_aDates )
			{
				sbWhere.OpenORParen() ;
				sbWhere.AppendANDCondition(String.Format("{0} >= {1}", sFieldName, SqlUtil.SQLString(dt.ToString("d")))) ;
				sbWhere.AppendANDCondition(String.Format("{0} < {1}", sFieldName, SqlUtil.SQLString(dt.AddDays(1.0d).ToString("d")))) ;
				sbWhere.CloseParen() ;
			}

			sbWhere.CloseParen() ;
		}

		public bool IsInRange(DateTime dtToCompare)
		{
			return ( m_aDates.Contains(dtToCompare.Date) ) ;
		}

		public override string ToString()
		{
			if ( m_aDates.Count == 0 )
				return "No range specified" ;
			else
			{
				SafeString.DelimitedStringBuilder sb = new SafeString.DelimitedStringBuilder(", ", "On ") ;
				foreach ( DateTime dt in m_aDates )
					sb.Append(dt.ToString("d")) ;

				return sb.ToString() ;
			}
		}

		protected NonConsecutiveDateRange(SerializationInfo info, StreamingContext context)
		{
			m_aDates = new ArrayList() ;
			int nDates = info.GetInt32("numdates") ;
			for ( int idx = 0 ; idx < nDates ; idx++ )
			{
				m_aDates.Add(info.GetDateTime("date_" + idx.ToString())) ;
			}
		}
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("version", NonConsecutiveDateRange.Version) ;
			info.AddValue("numdates", m_aDates.Count) ;
			for ( int idx = 0 ; idx < m_aDates.Count ; idx++ )
				info.AddValue("date_" + idx.ToString(), (DateTime)m_aDates[idx]) ;
		}
	}
}
