using System;
using System.Runtime.Serialization;
using System.Text;

namespace CommonLib
{
	/// <summary>
	/// Holds address data and does some simple validity checks.  The street address
	/// is not held as parsed data, but its format is normalized by parsing and recombining.
	/// Note: blank strings passed into constructors will be treated as nulls.
	/// </summary>
	[Serializable]
	public class SimpleAddress : ISerializable
	{
		public enum FORMAT {ML, USPS, SPELLED_OUT}

		public const double Version = 2.0;

		/// <summary>
		/// Holds address data in a verified form.  Only the street address and zip are required.
		/// </summary>
		private const string s_sInvAddMessage = "Invalid street address";
		private const string s_sInvZipMessage = "Invalid zipcode";
		private const string s_sInvStateMessage = "Invalid state code";

		static private string[] s_aStateCodes = new string[] {"AA", "AE", "AK", "AL", "AP",
																 "AR", "AS", "AZ", "CA", "CO",
																 "CT", "DC", "DE", "FL", "GA",
																 "GU", "HI", "IA", "ID", "IL", "IN",
																 "KS", "KY", "LA", "MA", "MD",
																 "ME", "MH", "MI", "MN", "MO",
																 "MP", "MS", "MT", "NC", "ND",
																 "NE", "NH", "NJ", "NM", "NV",
																 "NY", "OH", "OK", "OR", "PA",
																 "PR", "RI", "SC", "SD", "TN",
																 "TX", "UT", "VA", "VI", "VT",
																 "WA", "WI", "WV", "WY"};

		static private string[] s_aCanadianProvinces = new string[] {"AB", "BC", "MB", "NB", "NL", "NS", "NT", "NU", "ON", "PE", "QC", "SK", "YT"};
		
		private string m_sAddress = null;
		private string m_sCity = null;
		private string m_sState = null;
		private string m_sZip = null;

		// Added in v2.0 in order to permit different handling of street types (yuck)
		private string m_sStreetNum = null;
		private string m_sStreetName = null;
		private string m_sApartment = null;
		AddressDirection m_direction = null;
		StreetType m_streettype = null;

		protected SimpleAddress(SerializationInfo info, StreamingContext context)
		{
			double dVersion = info.GetDouble("version");

			this.m_sAddress = info.GetString("streetAddress");
			this.m_sCity = info.GetString("city");
			this.m_sState = info.GetString("state");
			this.m_sZip = info.GetString("zip");

			if (dVersion >= 2.0)
			{
				this.m_sStreetNum = info.GetString("streetnumber");
				this.m_sStreetName = info.GetString("streetname");
				this.m_sApartment = info.GetString("apartment");
				this.m_direction = new AddressDirection(info.GetString("streetdir"));
				this.m_streettype = new StreetType(info.GetString("streettype"));
			}
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("version", SimpleAddress.Version);

			info.AddValue("streetAddress",this.m_sAddress);
			info.AddValue("city", this.m_sCity);
			info.AddValue("state",this.m_sState);
			info.AddValue("zip", this.m_sZip);

			// added in v2.0
			info.AddValue("streetnumber", this.m_sStreetNum);
			info.AddValue("streetname", this.m_sStreetName);
			info.AddValue("apartment", this.m_sApartment);
			info.AddValue("streetdir", (this.m_direction == null ? "" : this.m_direction.MLAbbreviation));
			info.AddValue("streettype", (this.m_streettype == null ? "" : this.m_streettype.MLAbbreviation));
		}

		private SimpleAddress(){}

		/// <summary>
		/// Minimal constructor
		/// </summary>
		/// <param name="streetAddress">
		/// Free form street address, will be parsed into a normalized form
		/// </param>
		/// <param name="zip">
		/// Must be 5 digits
		/// </param>
		public SimpleAddress(string streetAddress, string zip) : this(streetAddress, zip, true)
		{
		}

		/// <summary>
		/// Minimal constructor
		/// </summary>
		/// <param name="streetAddress">
		/// Free form street address, will be parsed into a normalized form
		/// </param>
		/// <param name="zip">
		/// Must be 5 digits
		/// </param>
		/// <param name="checkForValid">
		/// Used to turn on/off checking for validity of input address
		/// </param>
		public SimpleAddress(string streetAddress, string zip, bool checkForValid)
		{
			this.m_sZip = NormalizeString(zip);
			ParseStreetAddress(streetAddress, checkForValid);

			if (checkForValid)
			{
				CheckAddress();
				CheckZip();
			}
		}

		/// <summary>
		/// Maximal constructor
		/// </summary>
		/// <param name="streetAddress">
		/// Free form street address, will be parsed into a normalized form
		/// </param>
		/// <param name="city">
		/// Name of city, will not be parsed
		/// </param>
		/// <param name="state">
		/// Must be the 2-letter state code
		/// </param>
		/// <param name="zip">
		/// Must be 5 digits
		/// </param>
		public SimpleAddress(string streetAddress, string city, string state, string zip) : this(streetAddress, city, state, zip, true)
		{
		}
		public SimpleAddress(string streetAddress, string city, string state, string zip, bool checkForValid) : this(streetAddress, city, state, zip, checkForValid, false)
		{
		}
		/// <summary>
		/// Maximal constructor
		/// </summary>
		/// <param name="streetAddress">
		/// Free form street address, will be parsed into a normalized form
		/// </param>
		/// <param name="city">
		/// Name of city, will not be parsed
		/// </param>
		/// <param name="state">
		/// Must be the 2-letter state code
		/// </param>
		/// <param name="zip">
		/// Must be 5 digits
		/// </param>
		/// <param name="checkForValid">
		/// Used to turn on/off checking for validity of input address
		/// </param>
		/// <param name="acceptCanadianProvinces">
		/// Accept Canadian Provinces, OPM 2247
		/// </param>		
		public SimpleAddress(string streetAddress, string city, string state, string zip, bool checkForValid, bool acceptCanadianProvinces)
		{
			this.m_sCity = NormalizeString(city);
			this.m_sState = NormalizeString(state);
			if (null != this.m_sState) this.m_sState = this.m_sState.ToUpper();
			this.m_sZip = NormalizeString(zip);
			ParseStreetAddress(streetAddress, checkForValid);

			if (checkForValid)
			{
				CheckAddress();
				CheckZip();
				CheckState(acceptCanadianProvinces);
			}
		}

		// Full street address WITH apartment
		public string StreetAddress
		{
			get
			{
				string sAddress = this.m_sAddress;
				if ((this.m_sApartment != null) && (this.m_sApartment.Length > 0))
					sAddress += " " + this.m_sApartment;
				return sAddress;
			}
		}

		// Full street address WITHOUT apartment
		public string StreetAddressWithoutApartment
		{
			get{return this.m_sAddress;}
		}

		public string GetStreetAddressWithoutApartment(FORMAT format)
		{
			string sAddress = null;
			switch (format)
			{
				case FORMAT.ML:
					sAddress = this.StreetAddressWithoutApartment;
					break;
				case FORMAT.USPS:
					sAddress = MakeAddress(this.m_sStreetNum, this.m_direction.USPSAbbreviation, this.m_sStreetName, this.m_streettype.USPSAbbreviation);
					break;
				case FORMAT.SPELLED_OUT:
					sAddress = MakeAddress(this.m_sStreetNum, this.m_direction.SpelledOut, this.m_sStreetName, this.m_streettype.SpelledOut);
					break;
			}

			return sAddress;
		}

		public string GetStreetAddressWithApartment(FORMAT format)
		{
			string sAddress = GetStreetAddressWithoutApartment(format);
			if (sAddress == null) return null;

			if ((this.m_sApartment != null) && (this.m_sApartment.Length > 0))
				sAddress += " " + this.m_sApartment;
			return sAddress;
		}

		public string StreetNumber
		{
			get {return this.m_sStreetNum;}
		}

		public AddressDirection StreetDirection
		{
			get {return this.m_direction;}
		}

		public string StreetName
		{
			get {return this.m_sStreetName;}
		}

		public StreetType StreetType
		{
			get {return this.m_streettype;}
		}

		public string Apartment
		{
			get {return this.m_sApartment;}
		}

		public string Zipcode
		{
			get{return this.m_sZip;}
		}

		public string City
		{
			get{return this.m_sCity;}
		}

		public string State
		{
			get{return this.m_sState;}
		}

		public string FullAddressOneLine
		{
			get
			{
				StringBuilder sb = new StringBuilder();
				sb.Append(this.StreetAddress);
				if ((null != this.m_sCity) && ("" != this.m_sCity))
				{
					sb.Append(", " + this.m_sCity);
					if ((null != this.m_sState) && ("" != this.m_sState))
						sb.Append(", " + this.m_sState);
					sb.Append(" " + this.m_sZip);
				}

				return sb.ToString();
			}
		}

		public string FullAddressLabelFormat
		{
			get
			{
				StringBuilder sb = new StringBuilder();
				sb.Append(this.StreetAddress);
				if ((null != this.m_sCity) && ("" != this.m_sCity))
				{
					sb.Append("\n" + this.m_sCity);
					if ((null != this.m_sState) && ("" != this.m_sState))
						sb.Append(", " + this.m_sState);
					sb.Append(" " + this.m_sZip);
				}

				return sb.ToString();
			}
		}

		private void NullifyData()
		{
			this.m_sAddress = null;
			this.m_sCity = null;
			this.m_sState = null;
			this.m_sZip = null;
			this.m_sApartment = null;
			this.m_sStreetNum = null;
			this.m_sStreetName = null;
			this.m_direction = null;
			this.m_streettype = null;
		}

		private void HandleException(string message)
		{
			NullifyData();
			throw new ApplicationException(message);
		}

		private void ParseStreetAddress(string address, bool checkForValid)
		{
			if ((address == null) || (address.Length == 0)) return;

			try
			{
				CommonLib.Address normalizer = new CommonLib.Address();
				normalizer.ParseStreetAddress(address);
				this.m_sAddress = MakeAddress(normalizer.StreetNumber, normalizer.sStreetDir, normalizer.StreetName, normalizer.StreetType);
				
				this.m_sStreetNum = normalizer.StreetNumber;
				this.m_sStreetName = normalizer.StreetName;
				this.m_sApartment = normalizer.AptNum;
				this.m_direction = new AddressDirection(normalizer.sStreetDir);
				this.m_streettype = new StreetType(normalizer.StreetType);
			}
			catch
			{
				if (checkForValid)
					HandleException(SimpleAddress.s_sInvAddMessage);
			}
		}

		private void CheckAddress()
		{
			if ((null == this.m_sStreetNum) || ("" == this.m_sStreetNum) || (null == this.m_sStreetName) || ("" == this.m_sStreetName))
				HandleException(SimpleAddress.s_sInvAddMessage);
		}

		private void CheckZip()
		{
			this.m_sZip = (null == this.m_sZip) ? "" : this.m_sZip.Trim();

			int len = this.m_sZip.Length;
			if (5 != len) HandleException(SimpleAddress.s_sInvZipMessage);

			for (int i=0; i<len; ++i)
			{
				if (!Char.IsDigit(this.m_sZip, i)) HandleException(SimpleAddress.s_sInvZipMessage);
			}
		}

		private void CheckState(bool acceptCanadianProvinces)
		{
			if (null == this.m_sState) return;

			bool bFound = false;
			for (int i=0; i<SimpleAddress.s_aStateCodes.Length; ++i)
			{
				if (SimpleAddress.s_aStateCodes[i] == this.m_sState)
				{
					bFound = true;
					break;
				}
			}

			if (!bFound && acceptCanadianProvinces) 
			{
				for (int i=0; i<SimpleAddress.s_aCanadianProvinces.Length; ++i)
				{
					if (SimpleAddress.s_aCanadianProvinces[i] == this.m_sState)
					{
						bFound = true;
						break;
					}
				}
			}
			
			if (!bFound)
				HandleException(SimpleAddress.s_sInvStateMessage);
		}

		private string MakeAddress(string num, string dir, string name, string type)
		{
			StringBuilder sb = new StringBuilder();
			sb.Append(num);
			if ((null != dir) && ("" != dir))
				sb.Append(" " + dir);
			sb.Append(" " + name);
			if ((null != type) && ("" != type))
				sb.Append(" " + type);
			
			string normAddress = sb.ToString();

			return ("" == normAddress)? null : normAddress;
		}

		private string NormalizeString(string inString)
		{
			string outString = (null == inString) ? "" : inString.Trim().ToUpper() ;
			if ("" == outString) return null;
			return outString;
		}
	}

	/// <summary>
	/// Helper class to permit translation of ML direction codes to other formats
	/// </summary>
	public class AddressDirection
	{
		private string m_sMLDir;
		private string m_sSpelledOut;

		internal AddressDirection(string mlAbrev)
		{
			this.m_sMLDir = null;
			this.m_sSpelledOut = null;
			switch (mlAbrev)
			{
				case "N":
					this.m_sMLDir = "N";
					this.m_sSpelledOut = "NORTH";
					break;
				case "S":
					this.m_sMLDir = "S";
					this.m_sSpelledOut = "SOUTH";
					break;
				case "E":
					this.m_sMLDir = "E";
					this.m_sSpelledOut = "EAST";
					break;
				case "W":
					this.m_sMLDir = "W";
					this.m_sSpelledOut = "WEST";
					break;
				case "NE":
					this.m_sMLDir = "NE";
					this.m_sSpelledOut = "NORTHEAST";
					break;
				case "NW":
					this.m_sMLDir = "NW";
					this.m_sSpelledOut = "NORTHWEST";
					break;
				case "SE":
					this.m_sMLDir = "SE";
					this.m_sSpelledOut = "SOUTHEAST";
					break;
				case "SW":
					this.m_sMLDir = "SW";
					this.m_sSpelledOut = "SOUTHWEST";
					break;
			}
		}

		/// <summary>
		/// Return the MeridianLink abbreviation for the direction, or null.
		/// </summary>
		public string MLAbbreviation
		{
			get {return this.m_sMLDir;}
		}

		/// <summary>
		/// Return the spelled out name of the direction, or null.
		/// </summary>
		public string SpelledOut
		{
			get {return this.m_sSpelledOut;}
		}

		/// <summary>
		/// Return the USPS abbreviation for the direction, or null.
		/// </summary>
		public string USPSAbbreviation
		{
			get {return this.MLAbbreviation;}
		}

		public override string ToString()
		{
			return this.MLAbbreviation;
		}
	}

	/// <summary>
	/// Helper class to permit translation of ML street-type codes to other formats
	/// </summary>
	public class StreetType
	{
		private string m_sMLType;
		private string m_sSpelledOut;
		private string m_sUSPS;

		internal StreetType(string mlAbrev)
		{
			this.m_sSpelledOut = Address.StreetType_2_Text(mlAbrev);
			if (this.m_sSpelledOut.Length == 0)
			{
				this.m_sMLType = null;
				this.m_sSpelledOut = null;
				this.m_sUSPS = null;
			}
			else
			{
				this.m_sUSPS = Address.USPSStreetTypeFromMLStreetType(mlAbrev);
				this.m_sMLType = mlAbrev;
			}
		}

		/// <summary>
		/// Return the MeridianLink abbreviation for the street type, or null.
		/// </summary>
		public string MLAbbreviation
		{
			get {return this.m_sMLType;}
		}

		/// <summary>
		/// Return the spelled out name of the street type, or null.
		/// </summary>
		public string SpelledOut
		{
			get {return this.m_sSpelledOut;}
		}

		/// <summary>
		/// Return the USPS abbreviation for the street type, or null.
		/// </summary>
		public string USPSAbbreviation
		{
			get {return this.m_sUSPS;}
		}

		/// <summary>
		/// Return the MeridianLink abbreviation for the street type, or null.
		/// </summary>
		public override string ToString()
		{
			return this.MLAbbreviation;
		}
	}
}
