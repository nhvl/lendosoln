// Copyright 2000-2005 MeridianLink, Inc.  All Rights Reserved.
using System;
using System.Text;

namespace CommonLib
{
	/// <summary>
	/// Manage a large string so that the Large Object Heap isn't poorly impacted.
	/// </summary>
	public struct BigString
	{
		const int MAX_LENGTH = 9000; // each char is 2 bytes...so max length = 18K, small enough to avoid Large Object Heap 

		private WeakReference m_wr;
		private string[] m_pieces;
		private int m_nLen;

		private BigString(string whole, string[] pieces, int len)
		{
			this.m_wr = new WeakReference(whole);
			this.m_pieces = pieces;
			this.m_nLen = len;
		}

		public static implicit operator BigString(string inString)
		{
			int nNumWholePieces = inString.Length / MAX_LENGTH;
			int nRemainder = inString.Length % MAX_LENGTH;

			int nLen = 0;
			int nNumPieces = (nRemainder == 0) ? nNumWholePieces : nNumWholePieces + 1;
			string[] arrPieces = new string[nNumPieces];
			for (int i=0; i<nNumPieces; ++i)
			{
				if (i == (nNumPieces - 1))
				{
					int nStartIndex = i * MAX_LENGTH;
					arrPieces[i] = inString.Substring(nStartIndex);
					nLen += inString.Length - nStartIndex;
				}
				else
				{
					arrPieces[i] = inString.Substring(i * MAX_LENGTH, MAX_LENGTH);
					nLen += MAX_LENGTH;
				}
			}

			return new BigString(inString, arrPieces, nLen);
		}

		public static implicit operator string(BigString inString)
		{
			return inString.ToString();
		}

		public override string ToString()
		{
			string sOut = (string)this.m_wr.Target;
			if (sOut == null)
			{
				StringBuilder sb = new StringBuilder(this.m_nLen);
				foreach (string s in this.m_pieces) sb.Append(s);
				sOut = sb.ToString();
				this.m_wr = new WeakReference(sOut);
			}
			return sOut;
		}

#if DEBUG
		const char DEFAULT_DUMMY_CHAR = 'A';
		const int  DEFAULT_DUMMY_LEN  = 21000;

		public static string DummyString()
		{
			return DummyString(DEFAULT_DUMMY_CHAR, DEFAULT_DUMMY_LEN);
		}

		public static string DummyString(char character)
		{
			return DummyString(character, DEFAULT_DUMMY_LEN);
		}

		public static string DummyString(char character, int len)
		{
			return new string(character, len);
		}
#endif
	}
}
