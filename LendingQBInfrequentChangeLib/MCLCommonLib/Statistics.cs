// Copyright 2000-2005 MeridianLink, Inc.  All Rights Reserved.
using System;

namespace CommonLib
{
	public struct Statistics
	{
		private double[] m_data;
		private double m_min;
		private double m_max;
		private double m_av;
		private double m_stdev;
		private double m_sterr;

		public void SetData(double[] data)
		{
			this.m_data = data;
			this.m_min = double.MaxValue;
			this.m_max = double.MinValue;
			this.m_av = double.MinValue;
			this.m_stdev = double.MinValue;
			this.m_sterr = double.MinValue;
		}

		public double Min
		{
			get
			{
				if (this.m_av == double.MinValue) DoFirstCalc();
				return this.m_min;
			}
		}

		public double Max
		{
			get
			{
				if (this.m_av == double.MinValue) DoFirstCalc();
				return this.m_max;
			}
		}

		public double Average
		{
			get
			{
				if (this.m_av == double.MinValue) DoFirstCalc();
				return this.m_av;
			}
		}

		public double StdDeviation
		{
			get
			{
				if (this.m_stdev == double.MinValue) DoSecondCalc();
				return this.m_stdev;
			}
		}

		public double StdError
		{
			get
			{
				if (this.m_stdev == double.MinValue) DoSecondCalc();
				return this.m_sterr;
			}
		}

		public PercentileMarks CalculatePercentiles()
		{
			return new PercentileMarks(this.m_data);
		}

		public int[] CalculateHistogram(double[] boundaries)
		{
			Histogram histo = new Histogram(boundaries);
			histo.PopulateData(this.m_data);
			return histo.Counts;
		}

		private void DoFirstCalc()
		{
			if (this.m_data == null) throw new InvalidOperationException("Must call SetData first");

			double dSum = 0.0;
			foreach (double d in this.m_data)
			{
				if (d < this.m_min) this.m_min = d;
				if (d > this.m_max) this.m_max = d;
				dSum += d;
			}
			this.m_av = dSum / this.m_data.Length;
		}

		private void DoSecondCalc()
		{
			if (this.m_av == double.MinValue) DoFirstCalc();
			if (this.m_data.Length < 2) throw new InvalidOperationException("Data must have at least 2 elements");

			double dSum = 0.0;
			foreach (double d in this.m_data)
			{
				dSum += Math.Pow(d - this.m_av, 2);
			}
			this.m_stdev = Math.Sqrt(dSum / (this.m_data.Length - 1));
			this.m_sterr = this.m_stdev / Math.Sqrt(this.m_data.Length);
		}

		public struct PercentileMarks
		{
			private double m_d50; // value for 50th percentile
			private double m_d75; // value for 75th percentile
			private double m_d90; // value for 90th percentile
			private double m_d95; // value for 95th percentile
			private double m_d99; // value for 99th percentile

			public PercentileMarks(double[] data)
			{
				this.m_d50 = double.MinValue;
				this.m_d75 = double.MinValue;
				this.m_d90 = double.MinValue;
				this.m_d95 = double.MinValue;
				this.m_d99 = double.MinValue;

				Array.Sort(data);
				this.m_d50 = MarkValue(data, 2);
				this.m_d75 = MarkValue(data, 4);
				this.m_d90 = MarkValue(data, 10);
				this.m_d95 = MarkValue(data, 20);
				this.m_d99 = MarkValue(data, 100);
			}

			public double Percentile_50 {get{return this.m_d50;}}
			public double Percentile_75 {get{return this.m_d75;}}
			public double Percentile_90 {get{return this.m_d90;}}
			public double Percentile_95 {get{return this.m_d95;}}
			public double Percentile_99 {get{return this.m_d99;}}

			private double MarkValue(double[] data, int frac)
			{
				double dLen = data.Length;
				double d = dLen / frac;
				int n = data.Length - (int)Math.Floor(d) - 1;
				return data[n];
			}
		}

		public struct HistogramBin
		{
			private double m_dLow;
			private double m_dHigh;
			private int m_nCount;

			public HistogramBin(double low, double high)
			{
				this.m_dLow = low;
				this.m_dHigh = high;
				this.m_nCount = 0;
			}

			public void TryAdd(double val)
			{
				if (this.m_dLow > val) return;
				if (this.m_dHigh <= val) return;
				++this.m_nCount;
			}

			public int Count {get{return this.m_nCount;}}
		}

		public struct Histogram
		{
			private HistogramBin[] m_arrBins;

			public Histogram(double[] boundaries)
			{
				this.m_arrBins = new HistogramBin[boundaries.Length + 1];
				double dLow = double.MinValue;
				for (int i=0; i<boundaries.Length; ++i)
				{
					this.m_arrBins[i] = new HistogramBin(dLow, boundaries[i]);
					dLow = boundaries[i];
				}
				this.m_arrBins[boundaries.Length] = new HistogramBin(dLow, double.MaxValue);
			}

			public void PopulateData(double[] vals)
			{
				foreach (double d in vals)
				{
					for (int i=0; i<this.m_arrBins.Length; ++i)
					{
						this.m_arrBins[i].TryAdd(d);
					}
				}
			}

			public HistogramBin[] Bins {get{return this.m_arrBins;}}
			public int[] Counts
			{
				get
				{
					int[] arrCounts = new int[this.m_arrBins.Length];
					for (int i=0; i<this.m_arrBins.Length; ++i)
					{
						arrCounts[i] = this.m_arrBins[i].Count;
					}
					return arrCounts;
				}
			}
		}
	}
}
