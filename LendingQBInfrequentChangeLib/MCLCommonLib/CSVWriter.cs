// Copyright 2000-2005 MeridianLink, Inc.  All Rights Reserved.
using System;
using System.Text;

namespace CommonLib
{
	/// <summary>
	/// Write data as a table
	/// </summary>
	public interface IDataWriter
	{
		void WriteData(string data);
		void EndLine();
	}

	/// <summary>
	/// Utility class that assists in the generation of a CSV file.
	/// </summary>
	public class CSVWriter : IDataWriter
	{
		private const int EST_CELL_SIZE = 20;
		private const int EST_CELLS_PER_LINE = 50;
		private const int EST_NUM_LINES = 100;

		private int m_nEstLineSize;
		private int m_nCellCount;
		private StringBuilder m_sb;
		private CSVLineWriter m_current;

		public CSVWriter()
		{
			this.m_nEstLineSize = EST_CELL_SIZE * EST_CELLS_PER_LINE;
			int nEstSize = EST_NUM_LINES * this.m_nEstLineSize;
			this.m_sb = new StringBuilder(nEstSize);
		}

		public CSVWriter(int estNumLines, int estNumEntriesPerLine)
		{
			this.m_nEstLineSize = EST_CELL_SIZE * estNumEntriesPerLine;
			int nEstSize = estNumLines * this.m_nEstLineSize;
			this.m_sb = new StringBuilder(nEstSize);
		}

		public void WriteData(string data)
		{
			if (this.m_current == null) this.m_current = new CSVLineWriter(this.m_nEstLineSize);
			this.m_current.WriteData(data);
		}

		public void EndLine()
		{
			if (this.m_current == null)
				throw new ApplicationException("Cannot end a line before writing data to it.");

			string sLine = this.m_current.ToString();
			if (sLine.Length == 0)
				throw new ApplicationException("Cannot end a line before writing data to it.");

			if (this.m_sb.Length == 0) // first line...use it as better estimate of line length
			{
				this.m_nCellCount = this.m_current.CellCount;
				this.m_nEstLineSize = sLine.Length;
			}
			else if (this.m_nCellCount != this.m_current.CellCount)
			{
				throw new ApplicationException("Incorrect number of data items written to last line.");
			}

			this.m_sb.Append(sLine);
			this.m_sb.Append("\r\n");

			this.m_current = null;
		}

		public override string ToString()
		{
			if (this.m_current != null) EndLine();
			this.m_current = null;
			return this.m_sb.ToString();
		}
	}

	internal class CSVLineWriter
	{
		private StringBuilder m_sb;
		private int m_nCellCount;

		public CSVLineWriter(int size)
		{
			this.m_sb = new StringBuilder(size);
		}

		public int CellCount
		{
			get {return this.m_nCellCount;}
		}

		public void WriteData(string inString)
		{
			bool bFirst = (this.m_sb.Length == 0);
			if (!bFirst) WriteSeparator();
			SafeWriteData(inString);
			++this.m_nCellCount;
		}

		public override string ToString()
		{
			return this.m_sb.ToString();
		}

		private void WriteSeparator()
		{
			this.m_sb.Append(",");
		}

		private void SafeWriteData(string data)
		{
			string sSafe = (data == null) ? "" : data.Trim();
			if (sSafe.Length > 0) sSafe = StripUnsafeChars(sSafe);

			this.m_sb.Append("\"" + sSafe + "\"");
		}

		private string StripUnsafeChars(string inString)
		{
			string sSafeVal = inString.Replace('\"', '\'');
			return sSafeVal;
		}
	}
}
