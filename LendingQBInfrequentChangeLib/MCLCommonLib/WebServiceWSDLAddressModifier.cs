// Copyright 2000-2005 MeridianLink, Inc.  All Rights Reserved.
using System;
using System.Web.Services.Description;
using System.Configuration;

namespace CommonLib
{
	/// <summary>
	/// Modify the protocol and/or port of the returned wsdl file
	/// </summary>
	/// <remarks>
	/// Usage:
	/// Add the following section into the application's config file:
	/// <configuration>
	///		<system.web>
	///			<webServices>
	///				<soapExtensionReflectorTypes>
	///					<add type="CommonLib.WebServiceWSDLAddressModifier, CommonLib"/>
	///				</soapExtensionReflectorTypes>
	///			</webServices>
	///		</system.web>
	/// </configuration>
	/// 
	/// and configure a webservice named MyWebService with 
	/// <configuration>
	/// </configuration>
	///		<appSettings>
	///			<add key="WSDL_MyWebService_HTTP_OVERRIDE" value="T"/>
	///			<add key="WSDL_MyWebService_HTTPS" value="T"/>
	///			<add key="WSDL_MyWebService_PORT" value="666"/>
	///		</appSettings>
	/// </remarks>
	public class WebServiceWSDLAddressModifier : SoapExtensionReflector
	{
		private bool m_bFirst = true;

		private bool m_bHttpOverride = false; // if true then the value of m_bHttps will be used, otherwise it will be ignored
		private bool m_bHttps = true; // the protocol should be https, otherwise http
		private int m_nPort = 80;

		public WebServiceWSDLAddressModifier()
		{
		}

		public override void ReflectMethod()
		{
			if (!this.m_bFirst) return;
			this.m_bFirst = true;

			ProtocolReflector protoRef = base.ReflectionContext;

			string sServiceName = protoRef.Service.Name;
			ReadConfiguration(sServiceName);

			SoapAddressBinding sabAddress = (SoapAddressBinding)protoRef.Port.Extensions.Find(typeof(SoapAddressBinding));
			UriBuilder uriLocation = new UriBuilder(sabAddress.Location);
			if (this.m_bHttpOverride)
			{
				uriLocation.Scheme = (this.m_bHttps)? "https" : "http";
			}
			uriLocation.Port = this.m_nPort;
			sabAddress.Location = uriLocation.Uri.AbsoluteUri;
		}

		private void ReadConfiguration(string serviceName)
		{
			string sKeyHttpOverride = string.Format("WSDL_{0}_HTTP_OVERRIDE", serviceName);
			string sHttpOverride = SafeConvert.ToString(ConfigurationManager.AppSettings[sKeyHttpOverride]);
			if (sHttpOverride.Length > 0)
				this.m_bHttpOverride = (sHttpOverride[0].ToString().ToUpper() == "T");

			if (this.m_bHttpOverride)
			{
				string sKeyHttps = string.Format("WSDL_{0}_HTTPS", serviceName);
                string sHttps = SafeConvert.ToString(ConfigurationManager.AppSettings[sKeyHttps]);
				if (sHttps.Length > 0)
					this.m_bHttps = (sHttps[0].ToString().ToUpper() == "T");
			}

			string sKeyPort  = string.Format("WSDL_{0}_PORT", serviceName);
            string sPort = SafeConvert.ToString(ConfigurationManager.AppSettings[sKeyPort]);
			if (sPort.Length > 0)
				this.m_nPort = int.Parse(sPort);

		}
	}
}
