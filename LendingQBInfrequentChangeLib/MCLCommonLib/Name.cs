using System;
using System.Text.RegularExpressions;

namespace CommonLib
{
	public class Name
	{
		private static Regex token = new Regex(@"\G *[A-Z]+");
		private static Regex token2 = new Regex(@"\G *[A-Z]+\-[A-Z]+");
		private static Regex comma = new Regex(@"\G *\,");
		private static Regex scrubchars = new Regex(@"[^ a-zA-Z0-9\,\-]");//clean up the string
		private static Regex space_comp = new Regex(@" +");
		private static Regex comma_matcher = new Regex(@"\,");

		public string[] SUFFIX= { "JUNIOR", "SENIOR", "JR", "SR", "III", "II", "I", "IV", "V", "1ST", "2ND", "3RD", "4TH", "5TH" } ;
		private string m_sFirstName ;
		private string m_sMiddleName ;
		private string m_sLastName ;
		private string m_sSuffix ;
		//private Regex token = new Regex(@"\G *[A-Z]+");
		//private Regex token2 = new Regex(@"\G *[A-Z]+\-[A-Z]+");
		//private Regex comma = new Regex(@"\G *\,");
		private int current_index;

		static public string SuffixCodeToString(int nCode)
		{
			switch(nCode)
			{
				case 1:
					return "SR";
				case 2:
					return "JR";
				case 3:
					return "III";
				case 4:
					return "IV";
				case 5:
					return "I";
				case 6:
					return "II";
				default:
					return "";
			}
		}

		static public string SuffixCodeToString(char code)
		{
			switch(code)
			{
				case '1':
					return "SR";
				case '2':
					return "JR";
				case '3':
					return "III";
				case '4':
					return "IV";
				case '5':
					return "I";
				case '6':
					return "II";
				default:
					return "";
			}
		}

		static public char SuffixStringToCode(string suffix)
		{
			switch (suffix.ToUpper())
			{
				case "1":
				case "SR":
				case "SR.":
				case "SENIOR":
					return '1';

				case "2":
				case "JR":
				case "JR.":
				case "JUNIOR":
					return '2';

				case "3":
				case "III":
				case "3RD":
				case "THIRD":
					return '3';

				case "4":
				case "IV":
				case "4TH":
				case "FOURTH":
					return '4';

				case "5":
				case "I":
				case "1ST":
				case "FIRST":
					return '5';

				case "6":
				case "II":
				case "2ND":
				case "SECOND":
					return '6';

				default:
					return ' ';
			}
		}

        public void ParseName(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return;
            }

            this.current_index = 0;
            //Regex scrubchars = new Regex(@"[^ a-zA-Z0-9\,\-]");//clean up the string
            //Regex space_comp = new Regex(@" +");
            s = scrubchars.Replace(s," ");
            s = space_comp.Replace(s," ");
            s = s.ToUpper();
            s = s.Trim();

            string suf = this.check_suffix(s);
            if(suf!=null) 
            {
                s = s.Substring(0, s.Length - suf.Length);
                s = s.TrimEnd(',', ' ');
            }

            if (comma_matcher.IsMatch(s))
            //if(Regex.Match(s, @"\,").Success)
            {
                this.lastfirst(s);
                return;
            }
            else
            {
                this.firstlast(s);
                return;
                //deal with firstname m i d d l e n a m e lastname suffix
            }
            //return;
        }

		public string FirstName
		{
			get
			{
				return null == m_sFirstName ? "" : m_sFirstName.Trim();
			}
		}
		public string MiddleName
		{
			get
			{
				return null == m_sMiddleName ? "" : m_sMiddleName.Trim();
			}
		}
		public string LastName
		{
			get
			{
				return null == m_sLastName ? "" : m_sLastName.Trim();
			}
		}
		public string Suffix
		{
			get
			{
				return null == m_sSuffix ? "" : m_sSuffix.Trim();
			}
		}
		//---------------------------------begin private methods-----------------------------------

		private string getnexttoken(string s)
		{
			
			if(s.Length < current_index+1) return null;
			while( s.Length < current_index+1 && s[current_index]==' ') current_index++;
			if(s.Length < current_index +1) return null;

            Match m1=token.Match(s, current_index);
			Match m2=comma.Match(s, current_index);
			Match m3=token2.Match(s, current_index);

			if(m3.Success)
			{
				current_index += m3.ToString().Length;
				return m3.ToString();

			}
			if(m1.Success)  
			{
				current_index += m1.ToString().Length;
				return m1.ToString();
			}
			if(m2.Success)
			{
				current_index += m2.ToString().Length;
				return m2.ToString();
			}
			return null;
			
		}

		private string check_suffix(string s)
		{
			if(s==null) return null;
			for(int i =0;i<SUFFIX.Length;++i)
			{
                string currentSuffix = SUFFIX[i];
                if (!s.EndsWith(currentSuffix))
                {
                    continue;
                }

                int indexBeforeSuffix = s.Length - currentSuffix.Length - 1;
                char charBeforeSuffix = indexBeforeSuffix >= 0 ? s[indexBeforeSuffix] : ' ';
                if (charBeforeSuffix == ' ' || charBeforeSuffix == ',')
                {
                    m_sSuffix = SUFFIX[i];
                    return SUFFIX[i];
                }
			}

			return null;
		}

		private void lastfirst(string s)
		{
			string temp;
			temp=getnexttoken(s);
			temp.Trim();
			m_sLastName = temp;
			temp=getnexttoken(s);
			if(temp==null) return;
			while(!temp.Equals(","))
			{
				m_sLastName += " " + temp;
				temp=getnexttoken(s);

			}
			temp=getnexttoken(s);
			if(temp==null) return;
			m_sFirstName = temp;

			m_sMiddleName = s.Substring(current_index);
			return;
			
			
		}

		private void firstlast(string s)
		{
			if(s==null || s.Equals ("")) return;
			string temp = getnexttoken(s);
			string temp1;
			if(temp== null)  return;
			m_sFirstName = temp;
			temp=getnexttoken(s);
			if(temp==null)return;
			temp1=getnexttoken(s);
			while(temp1!=null)
			{
				if(m_sMiddleName!=null) m_sMiddleName += " ";
				m_sMiddleName += temp;
				temp=temp1;
				temp1=getnexttoken(s);

			}

			m_sLastName = temp;
			return;
		}


		
	}
}
