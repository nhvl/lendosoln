using System;
using System.IO;

namespace CommonLib
{
	/// <summary>
	/// Summary description for FileUtil.
	/// </summary>
	public class FileUtil
	{
		private FileUtil(){}

		static public bool WriteString(string path, string data)
		{
			try
			{
				System.IO.StreamWriter writer = System.IO.File.CreateText(path);
				writer.Write(data);
				writer.Close();
				return true;
			}
			catch
			{
				//For TestOnly
				return false;
			}
		}

		static public bool ReadString(string path, out string data)
		{
			data = null;
			try
			{
				System.IO.StreamReader reader = System.IO.File.OpenText(path);
				data = reader.ReadToEnd();
				reader.Close();
				return true;
			}
			catch
			{
				return false;
			}
		}

		static public bool BinaryWrite(string path, byte[] data)
		{
			try
			{
				System.IO.FileStream stream = new System.IO.FileStream(path, System.IO.FileMode.Create);
				stream.Write(data, 0, data.Length);
				stream.Close();
				return true;
			}
			catch
			{
				return false;
			}
		}

		static public bool BinaryRead(string path, out byte[] data)
		{
			data = null;
			try
			{
				FileInfo fInfo = new FileInfo(path);
				data = new byte[fInfo.Length];
				//modified by S.F. to enforce readonly.
				BinaryReader br = new BinaryReader(new FileStream(path, FileMode.Open, System.IO.FileAccess.Read));
				for (long i=0; i<fInfo.Length; ++i)
					data[i] = br.ReadByte();
				br.Close();
				return true;
			}
			catch
			{
				return false;
			}
		}
	}
}
