using System;
using System.Runtime.Serialization;
using System.Text;

namespace CommonLib
{
	public enum CreditCardType {Visa, MasterCard, AmericanExpress, Discover, DinersClub, JCB}

	/// <summary>
	/// Holds all data that an actual credit card carries, and nothing else.
	/// This class will throw an exception if the input data is invalid in ANY way.
	/// However, for testing purposes it will permit the use of the invalid
	/// credit card number 4111-1111-1111-1112.
	/// </summary>
	[Serializable]
	public class CreditCard : ISerializable
	{
		private const string s_sPassThruNumber = "4111111111111112";

		public const double Version = 1.0;
		private const string s_sEncryptKey = "tunm%e2#H90*-14B"; // WARNING: changing will make earlier versions unreadable

		public const char MaskCharacter = '*';
		public const int MaxExpYears = 10;

		private string m_sFirstName;
		private string m_sLastName;
		private string m_sNumber = ""; // will be cleaned up
		private CreditCardType m_type;
		private string m_sQualifiedType = null;
		private string m_sVerificationValue = ""; // some cards have this, some do not
		private int m_nExpMonth;
		private int m_nExpYear;

		private SimpleAddress m_address;

		protected CreditCard(SerializationInfo info, StreamingContext context)
		{
			double dVersion = info.GetDouble("version");

			this.m_sFirstName = info.GetString("firstName");
			this.m_sLastName = info.GetString("lastName");
			this.m_sNumber = Decrypt(info.GetString("number"));
			this.m_type = (CreditCardType)info.GetValue("type", typeof(int));
			this.m_sVerificationValue = Decrypt(info.GetString("cvv"));
			this.m_nExpMonth = info.GetInt32("expMonth");
			this.m_nExpYear = info.GetInt32("expYear");
			this.m_address = (SimpleAddress)info.GetValue("address", typeof(SimpleAddress));
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("version", CreditCard.Version);

			info.AddValue("firstName",this.m_sFirstName);
			info.AddValue("lastName", this.m_sLastName);
			info.AddValue("number", Encrypt(m_sNumber));
			info.AddValue("type", (int)this.m_type);
			info.AddValue("cvv", Encrypt(this.m_sVerificationValue));
			info.AddValue("expMonth", this.m_nExpMonth);
			info.AddValue("expYear", this.m_nExpYear);
			info.AddValue("address", this.m_address);
		}

		/// <summary>
		/// Validates all the data; throws exceptions for bad data.
		/// </summary>
		/// <param name="a_sName">Card holders name exactly as appears on the card</param>
		/// <param name="number">Number will be stripped of non-digits, then validated</param>
		/// <param name="month">Must be between 1 and 12</param>
		/// <param name="year">Should be the four digit year (e.g., 2003)</param>
		public CreditCard(string firstName, string lastName, string number, int month, int year, SimpleAddress address)
		{
			InitRequired(firstName, lastName, number, month, year, address);
		}

		/// <summary>
		/// Validates all the data; throws exceptions for bad data.
		/// </summary>
		/// <param name="a_sName">Card holders name exactly as appears on the card</param>
		/// <param name="number">Number will be stripped of non-digits, then validated</param>
		/// <param name="month">Must be between 1 and 12</param>
		/// <param name="year">Should be the four digit year (e.g., 2003)</param>
		/// <param name="verVal">Card Verification Value</param>
		public CreditCard(string firstName, string lastName, string number, int month, int year, string verVal, SimpleAddress address)
		{
			InitRequired(firstName, lastName, number, month, year, address);
			SetCVV(verVal);
		}

		private void InitRequired(string firstName, string lastName, string number, int month, int year, SimpleAddress address)
		{
			if ((null == firstName) || ("" == firstName))
			{
				string sMsg = "Invalid name";
				Tracer.Trace("CommonLib", "CreditCard", "InitRequired", "war", sMsg);
				throw new ApplicationException(sMsg);
			}

			if (null == address)
			{
				string sMsg = "Must have a valid address";
				Tracer.Trace("CommonLib", "CreditCard", "InitRequired", "war", sMsg);
				throw new ApplicationException(sMsg);
			}

			this.m_sFirstName = firstName;
			this.m_sLastName = lastName;
			this.m_address = address;
			SetNumberAndType(number);
			SetExpDate(month, year);
		}

		public string FirstName
		{
			get{return this.m_sFirstName;}
		}

		public string LastName
		{
			get{return this.m_sLastName;}
		}

		public string Name
		{
			get{return this.m_sFirstName + " " + this.m_sLastName;}
		}

		public string Number
		{
			get{return this.m_sNumber;}
		}

		public string FormattedNumber
		{
			get{return FormatNumber(this.m_sNumber);}
		}

		public CreditCardType Type
		{
			get{return this.m_type;}
		}

		public string QualifiedType
		{
			get
			{
				if (this.m_sQualifiedType == null)
					this.m_sQualifiedType = CreditCard.QualifiedTypeFromNumber(this.m_sNumber, this.m_type);
				return this.m_sQualifiedType;
			}
		}

		public string CVV
		{
			get{return this.m_sVerificationValue;}
		}

		public int ExpMonth
		{
			get{return this.m_nExpMonth;}
		}

		public int ExpYear
		{
			get{return this.m_nExpYear;}
		}

		public SimpleAddress Address
		{
			get{return this.m_address;}
			set{this.m_address = value;}
		}

		static public bool IsDateExpired(int month, int year)
		{
			DateTime today = DateTime.Today;
			// Actually expires at end of expiration month
			int nExpMonth = (12 == month) ? 1 : month + 1;
			int nExpYear  = (12 == month) ? year + 1 : year;
			DateTime expires = new DateTime(nExpYear, nExpMonth, 1);
			return (expires <= today);
		}

		public bool IsExpired
		{
			get {return CreditCard.IsDateExpired(this.m_nExpMonth, this.m_nExpYear);}
		}

		public static char CreditCardTypeCode(CreditCardType type)
		{
			switch (type)
			{
				case CreditCardType.MasterCard:
					return 'M';
				case CreditCardType.Visa:
					return 'V';
				case CreditCardType.Discover:
					return 'D';
				case CreditCardType.AmericanExpress:
					return 'A';
				case CreditCardType.DinersClub:
					return 'C';
				case CreditCardType.JCB:
					return 'J';
				default:
					return ' ';
			}
		}

		public static CreditCardType CreditCardTypeFromCode(char val)
		{
			switch (val)
			{
				case 'M':
					return CreditCardType.MasterCard;
				case 'V':
					return CreditCardType.Visa;
				case 'D':
					return CreditCardType.Discover;
				case 'A':
					return CreditCardType.AmericanExpress;
				case 'C':
					return CreditCardType.DinersClub;
				case 'J':
					return CreditCardType.JCB;
				default:
					throw new ApplicationException("Invalid CreditCardType Code");
			}
		}

		public static string FormatNumber(string number)
		{
			if ((null == number) || ("" == number)) return number;

			string sClean = CleanNumber(number);
			CreditCardType type = TypeFromNumber(sClean);
			string formatted = null;
			switch (type)
			{
				case CreditCardType.Visa:
					formatted = (16 == sClean.Length) ? Format16(sClean) : Format13(sClean);
					break;

				case CreditCardType.MasterCard:
					formatted = Format16(sClean);
					break;

				case CreditCardType.AmericanExpress:
					formatted = Format15(sClean);
					break;

				case CreditCardType.Discover:
					formatted = Format16(sClean);
					break;

				case CreditCardType.DinersClub:
					formatted = (16 == sClean.Length) ? Format16(sClean) : Format14(sClean);
					break;

				case CreditCardType.JCB:
					formatted = Format16(sClean);
					break;

				default:
					formatted = sClean;
					break;
			}

			return formatted;
		}

		static private string Format13(string number)
		{
			return number.Substring(0, 4) 
				+ "-" + number.Substring(4, 3) 
				+ "-" + number.Substring(7, 3)
				+ "-" + number.Substring(10, 3);
		}

		static private string Format14(string number)
		{
			return number.Substring(0, 4) 
				+ "-" + number.Substring(4, 6) 
				+ "-" + number.Substring(10, 4);
		}

		static private string Format15(string number)
		{
			return number.Substring(0, 4) 
				+ "-" + number.Substring(4, 6) 
				+ "-" + number.Substring(10, 5);
		}

		static private string Format16(string number)
		{
			return number.Substring(0, 4) 
				+ "-" + number.Substring(4, 4) 
				+ "-" + number.Substring(8, 4) 
				+ "-" + number.Substring(12, 4);
		}

		public static string MaskedNumber(string number)
		{
			if ((null == number) || ("" == number)) return "";

			string sClean = CleanNumber(number);
			if (4 >= sClean.Length) return sClean;

			string pad = new string(CreditCard.MaskCharacter, sClean.Length - 4);
			return pad + sClean.Substring(sClean.Length - 4, 4);
		}

		public static bool IsMaskedNumber(string number)
		{
			if (4 >= number.Length)
			{
				for (int i=0; i<number.Length; ++i)
				{
					if (!char.IsDigit(number[i])) return false;
				}
				return true;
			}
			else
			{
				int nPadLen = number.Length - 4;
				string sPad = number.Substring(0, nPadLen);
				if (sPad != new string(CreditCard.MaskCharacter, nPadLen)) return false;

				for (int i=nPadLen; i<number.Length; ++i)
				{
					if (!char.IsDigit(number[i])) return false;
				}
				return true;
			}
		}

		public static string TypeCode(CreditCardType type)
		{
			string code = "";
			switch (type)
			{
				case CreditCardType.Visa:
					code = "VI";
					break;

				case CreditCardType.MasterCard:
					code = "MC";
					break;

				case CreditCardType.AmericanExpress:
					code = "AE";
					break;

				case CreditCardType.Discover:
					code = "DI";
					break;

				case CreditCardType.DinersClub:
					code = "DC";
					break;

				case CreditCardType.JCB:
					code = "JC";
					break;

				default: // cannot get here
					break;
			}

			return code;
		}

		public static CreditCardType TypeFromNumber(string number)
		{
			return TypeFromNumber(number, true);
		}

		public static CreditCardType TypeFromNumber(string number, bool doValidation)
		{
			if ((null == number) || ("" == number))
			{
				if (doValidation)
				{
					string sMsg = "Null or empty card number.";
					Tracer.Trace("CommonLib", "CreditCard", "TypeFromNumber", "war", sMsg);
					throw new ApplicationException(sMsg);
				}
				else return CreditCardType.Visa;
			}

			string sClean = CleanNumber(number);
			if (13 > sClean.Length)
			{
				if (doValidation)
				{
					string sMsg = "Card number too short.";
					Tracer.Trace("CommonLib", "CreditCard", "TypeFromNumber", "war", sMsg);
					throw new ApplicationException(sMsg);
				}
				else return CreditCardType.Visa;
			}

			try
			{
				CreditCardType type = CreditCardTypeFromNumber(sClean);
				if (doValidation) ValidateNumber(sClean, type);
				return type;
			}
			catch (Exception ex)
			{
				if (doValidation) throw ex;
				else return CreditCardType.Visa;
			}
		}

		static private CreditCardType CreditCardTypeFromNumber(string number)
		{
			// the following algorithm was taken from the EPX documentation
			// Code was copied from CCGateway

			int firstSix = SafeConvert.ToInt(number.Substring(0, 6));
			if ((firstSix >= 400000) && (firstSix < 500000))
				return CreditCardType.Visa;
			if ((firstSix >= 510000) && (firstSix < 560000))
				return CreditCardType.MasterCard;
			if (((firstSix >= 340000) && (firstSix < 350000))
				|| ((firstSix >= 370000) && (firstSix < 380000)))
				return CreditCardType.AmericanExpress;
			if ((firstSix >= 601100) && (firstSix < 601200))
				return CreditCardType.Discover;
			if (((firstSix >= 300000) && (firstSix < 306000))
				|| ((firstSix >= 309500) && (firstSix < 309600))
				|| ((firstSix >= 360000) && (firstSix < 370000))
				|| ((firstSix >= 380000) && (firstSix < 390000)))
				return CreditCardType.DinersClub;
			if ((firstSix >= 352800) && (firstSix < 359000))
				return CreditCardType.JCB;

			string sMsg = "Card number cannot be assigned to a credit card type.";
			Tracer.Trace("CommonLib", "CreditCard", "CreditCardTypeFromNumber", "war", sMsg);
			throw new ApplicationException(sMsg);
		}

		// Implementation adapted from http://www.blackmarket-press.net/info/plastic/CCPrefix1.htm
		static private string QualifiedTypeFromNumber(string number, CreditCardType type)
		{
			switch (type)
			{
				case CreditCardType.AmericanExpress:
					return QualifiedAmericanExpress(number);

				case CreditCardType.DinersClub:
					return QualifiedDinersClub(number);
					
				case CreditCardType.Discover:
					return QualifiedDiscover(number);
					
				case CreditCardType.JCB:
					return QualifiedJCB(number);
					
				case CreditCardType.MasterCard:
					return QualifiedMasterCard(number);
					
				case CreditCardType.Visa:
					return QualifiedVisa(number);
				default: // should never get here
					return type.ToString();
			}
		}

		static private string QualifiedAmericanExpress(string number)
		{
			if (number.Substring(0, 4) == "3782") return "AmEx - Small Corporate Card";
			if (number.Substring(0, 4) == "3787") return "AmEx - Small Corporate Card";
			if ((number.Substring(0, 2) == "37") && (number.Substring(3, 1) == "8")) return "AmEx Gold";
			if ((number.Substring(0, 2) == "37") && (number.Substring(3, 2) == "37")) return "AmEx Platinum";
			if ((number.Substring(0, 2) == "37") && (number.Substring(10, 2) == "11")) return "AmEx issued since 1995";
			if (number.Substring(0, 2) == "37") return "AmEx";
			if (number.Substring(0, 1) == "3") return "AmEx Gold";
			return "AmEx";
		}

		static private string QualifiedDinersClub(string number)
		{
			if (number.Substring(0, 2) == "30") return "Diners Club";
			if (number.Substring(0, 2) == "31") return "Diners Club";
			if (number.Substring(0, 2) == "35") return "Diners Club";
			if (number.Substring(0, 2) == "36") return "Diners Club";
			if (number.Substring(0, 2) == "38") return "Carte Blanche";
			return "Diners Club";
		}

		static private string QualifiedDiscover(string number)
		{
			if (number.Substring(0, 4) == "6013") return "Discover - MBNA Bank";
			if (number.Substring(0, 2) == "60") return "Discover";
			return "Discover";
		}

		static private string QualifiedJCB(string number)
		{
			return "JCB (Japanese Credit Bureau)";
		}

		static private string QualifiedMasterCard(string number)
		{
			if (number.Substring(0, 4) == "5031") return "MasterCard - Maryland of North America";
			if (number.Substring(0, 4) == "5100") return "MasterCard - Southwestern States Bankard Association";
			if (number.Substring(0, 4) == "5110") return "MasterCard - Universal Travel Voucher";
			if (number.Substring(0, 4) == "5120") return "MasterCard - Western States Bankard Association";
			if (number.Substring(0, 4) == "5130") return "MasterCard - Eurocard France";
			if (number.Substring(0, 4) == "5140") return "MasterCard - Mountain States Bankard Association";
			if (number.Substring(0, 4) == "5150") return "MasterCard - Credit Systems, Inc.";
			if (number.Substring(0, 4) == "5160") return "MasterCard - Westpac Banking Corporation";
			if (number.Substring(0, 4) == "5170") return "MasterCard - Midamerica Bankard Association";
			if (number.Substring(0, 4) == "5172") return "MasterCard - First Bank Card Center";
			if (number.Substring(0, 3) == "518")  return "MasterCard - Computer Communications of America";
			if (number.Substring(0, 3) == "519")  return "MasterCard - Bank of Montreal";
			if (number.Substring(0, 4) == "5201") return "MasterCard - Mellon Bank, N.A.";
			if (number.Substring(0, 4) == "5202") return "MasterCard - Central Trust Company, N.A.";
			if (number.Substring(0, 4) == "5204") return "MasterCard - Security Pacific National Bank";
			if (number.Substring(0, 4) == "5205") return "MasterCard - Promocion y Operacion, S.A.";
			if (number.Substring(0, 4) == "5206") return "MasterCard - Banco Nacional de Mexico";
			if (number.Substring(0, 4) == "5207") return "MasterCard - New England Bankard Association";
			if (number.Substring(0, 4) == "5208") return "MasterCard - Million Card Service Co., Ltd.";
			if (number.Substring(0, 4) == "5209") return "MasterCard - The Citizens & Southern National Bank";
			if (number.Substring(0, 4) == "5210") return "MasterCard - Kokunai Shinpan Company, Ltd.";
			if (number.Substring(0, 4) == "5211") return "MasterCard - Chemical Bank Delaware";
			if (number.Substring(0, 4) == "5212") return "MasterCard - F.C.C. National Bank";
			if (number.Substring(0, 4) == "5213") return "MasterCard - The Bankcard Association, Inc.";
			if (number.Substring(0, 4) == "5215") return "MasterCard - Marine Midland Bank, N.A.";
			if (number.Substring(0, 4) == "5216") return "MasterCard - Old Kent Bank & Trust Co.";
			if (number.Substring(0, 4) == "5217") return "MasterCard - Union Trust";
			if (number.Substring(0, 4) == "5218") return "MasterCard - Citibank/Citicorp";
			if (number.Substring(0, 4) == "5219") return "MasterCard - Central Finance Co., Ltd.";
			if (number.Substring(0, 4) == "5220") return "MasterCard - Sovran Bank/Central South";
			if (number.Substring(0, 4) == "5221") return "MasterCard - Standard Bank of South Africa, Ltd.";
			if (number.Substring(0, 4) == "5222") return "MasterCard - Security Bank & Trust Company";
			if (number.Substring(0, 4) == "5223") return "MasterCard - Trustmark National Bank";
			if (number.Substring(0, 4) == "5224") return "MasterCard - Midland Bank";
			if (number.Substring(0, 4) == "5225") return "MasterCard - First Pennsylvania Bank, N.A.";
			if (number.Substring(0, 4) == "5226") return "MasterCard - Eurocard Ab";
			if (number.Substring(0, 4) == "5227") return "MasterCard - Rocky Mountain Bankcard System, Inc.";
			if (number.Substring(0, 4) == "5228") return "MasterCard - First Union National Bank of North Carolina";
			if (number.Substring(0, 4) == "5229") return "MasterCard - Sunwest Bank of Albuquerque, N.A.";
			if (number.Substring(0, 4) == "5230") return "MasterCard - Harris Trust & Savings Bank";
			if (number.Substring(0, 4) == "5231") return "MasterCard - Badische Beamtenbank EG";
			if (number.Substring(0, 4) == "5232") return "MasterCard - Eurocard Deutschland";
			if (number.Substring(0, 4) == "5233") return "MasterCard - Computer Systems Association, Inc.";
			if (number.Substring(0, 4) == "5234") return "MasterCard - Citibank Arizona";
			if (number.Substring(0, 4) == "5235") return "MasterCard - Financial Transaction System, Inc.";
			if (number.Substring(0, 4) == "5236") return "MasterCard - First Tennessee Bank, N.A.";
			if (number.Substring(0, 4) == "5254") return "MasterCard - Bank of America";
			if (number.Substring(0, 4) == "5273") return "MasterCard (can be Gold) - Bank of America";
			if (number.Substring(0, 4) == "5286") return "MasterCard - Home Federal";
			if (number.Substring(0, 4) == "5291") return "MasterCard - Signet Bank";
			if (number.Substring(0, 4) == "5329") return "MasterCard - Maryland of North America";
			if (number.Substring(0, 4) == "5410") return "MasterCard - Wells Fargo";
			if (number.Substring(0, 4) == "5412") return "MasterCard - Wells Fargo";
			if (number.Substring(0, 4) == "5419") return "MasterCard - Bank of Hoven";
			if (number.Substring(0, 4) == "5424") return "MasterCard - Citibank/Citicorp";
			if (number.Substring(0, 4) == "5434") return "MasterCard - National Westminster Bank";
			if (number.Substring(0, 4) == "5465") return "MasterCard - Chase Manhattan";
			if (number.Substring(0, 6) == "530693") return "MasterCard - Bancolombia Cadenalco (Colombia)";
			if (number.Substring(0, 7) == "5406251") return "MasterCard - Banco de Occidente (Colombia)";
			if (number.Substring(0, 4) == "5426") return "MasterCard - Granahorrar (Colombia)";
			if (number.Substring(0, 4) == "5406") return "MasterCard - Granahorrar (Colombia)";
			if (number.Substring(0, 1) == "5") return "MasterCard/Access/Eurocard";
			return "MasterCard";
		}

		static private string QualifiedVisa(string number)
		{
			if (number.Length == 13)
			{
				if (number.Substring(0, 4) == "4128") return "Visa CV - Citibank";
				if (number.Substring(0, 7) == "4448020") return "Visa Premier card";
				if (number.Substring(0, 1) == "4") return "Visa";
				return "Visa";
			}
			// length is 16
			if (number.Substring(0, 8) == "40240238") return "Visa Gold - Bank of America";
			if (number.Substring(0, 4) == "4019") return "Visa CV/Gold - Bank of America";
			if (number.Substring(0, 4) == "4024") return "Visa PV - Bank of America";
			if (number.Substring(0, 4) == "4040") return "Visa CV - Wells Fargo";
			if (number.Substring(0, 4) == "4048") return "Visa CV";
			if (number.Substring(0, 8) == "40240071") return "Visa - Wells Fargo";
			if (number.Substring(0, 4) == "4013") return "Visa - Citibank";
			if (number.Substring(0, 4) == "4019") return "Visa - Bank of America";
			if (number.Substring(0, 4) == "4024") return "Visa - Bank of America";
			if (number.Substring(0, 4) == "4027") return "Visa - Rockwell Federal Credit Union";
			if (number.Substring(0, 4) == "4032") return "Visa - Household Bank";
			if (number.Substring(0, 4) == "4052") return "Visa - First Cincinnati";
			if (number.Substring(0, 4) == "4060") return "Visa - Associates National Bank";
			if (number.Substring(0, 4) == "4070") return "Visa - Security Pacific";
			if (number.Substring(0, 4) == "4071") return "Visa - Colonial National Bank";
			if (number.Substring(0, 4) == "4094") return "Visa - A.M.C. Federal Credit Union";
			if (number.Substring(0, 4) == "4113") return "Visa - Valley National Bank";
			if (number.Substring(0, 4) == "4114") return "Visa - Chemical Bank";
			if (number.Substring(0, 4) == "4121") return "Visa - Pennsylvania State Employees Credit Union";
			if (number.Substring(0, 4) == "4122") return "Visa - Union Trust";
			if (number.Substring(0, 4) == "4125") return "Visa - Marine Midland";
			if (number.Substring(0, 4) == "4128") return "Visa CV - Citibank";
			if (number.Substring(0, 4) == "4131") return "Visa - State Street Bank";
			if (number.Substring(0, 4) == "4225") return "Visa - Chase Manhattan Bank";
			if (number.Substring(0, 4) == "4226") return "Visa - Chase Manhattan Bank";
			if (number.Substring(0, 4) == "4231") return "Visa - Chase Lincoln First Classic";
			if (number.Substring(0, 4) == "4232") return "Visa - Chase Lincoln First Classic";
			if (number.Substring(0, 4) == "4239") return "Visa - Corestates";
			if (number.Substring(0, 4) == "4241") return "Visa - National Westminster Bank";
			if (number.Substring(0, 4) == "4250") return "Visa - First Chicago Bank";
			if (number.Substring(0, 4) == "4253") return "Visa - Consumers Edge";
			if (number.Substring(0, 12) == "425451236000") return "Visa Premier card - Security First";
			if (number.Substring(0, 12) == "425451238500") return "Visa Premier card - Security First";
			if (number.Substring(0, 4) == "4254") return "Visa - Security First";
			if (number.Substring(0, 7) == "4271382") return "Visa PV - Citibank";
			if (number.Substring(0, 4) == "4271") return "Visa - Citibank/Citicorp";
			if (number.Substring(0, 4) == "4301") return "Visa - Monogram Bank";
			if (number.Substring(0, 4) == "4302") return "Visa - H.H.B.C.";
			if (number.Substring(0, 4) == "4311") return "Visa - First National Bank of Louisville";
			if (number.Substring(0, 4) == "4317") return "Visa - Gold Dome";
			if (number.Substring(0, 4) == "4327") return "Visa - First Atlanta";
			if (number.Substring(0, 4) == "4332") return "Visa - First American Bank";
			if (number.Substring(0, 4) == "4339") return "Visa - Primerica Bank";
			if (number.Substring(0, 4) == "4342") return "Visa - N.C.M.B. / Nations Bank";
			if (number.Substring(0, 4) == "4356") return "Visa - National Bank of Delaware";
			if (number.Substring(0, 4) == "4368") return "Visa - National West";
			if (number.Substring(0, 4) == "4387") return "Visa - Bank One";
			if (number.Substring(0, 4) == "4388") return "Visa - First Signature Bank & Trust";
			if (number.Substring(0, 4) == "4401") return "Visa - Gary-Wheaton Bank";
			if (number.Substring(0, 4) == "4413") return "Visa - Firstier Bank Lincoln";
			if (number.Substring(0, 4) == "4418") return "Visa - Bank of Omaha";
			if (number.Substring(0, 4) == "4421") return "Visa - Indiana National Bank";
			if (number.Substring(0, 4) == "4424") return "Visa - Security Pacific National Bank";
			if (number.Substring(0, 4) == "4428") return "Visa - Bank of Hoven";
			if (number.Substring(0, 4) == "4436") return "Visa - Security Bank & Trust";
			if (number.Substring(0, 4) == "4443") return "Visa - Merril Lynch Bank & Trust";
			if (number.Substring(0, 4) == "4447") return "Visa - AmeriTrust";
			if (number.Substring(0, 4) == "4452") return "Visa - Empire Affiliates Federal Credit Union";
			if (number.Substring(0, 4) == "4498") return "Visa - Republic Savings";
			if (number.Substring(0, 4) == "4502") return "Visa - C.I.B.C.";
			if (number.Substring(0, 4) == "4503") return "Visa - Canadian Imperial Bank";
			if (number.Substring(0, 4) == "4506") return "Visa - Belgium A.S.L.K.";
			if (number.Substring(0, 4) == "4510") return "Visa - Royal Bank of Canada";
			if (number.Substring(0, 4) == "4520") return "Visa - Toronto Dominion of Canada";
			if (number.Substring(0, 4) == "4537") return "Visa - Bank of Nova Scotia";
			if (number.Substring(0, 4) == "4538") return "Visa - Bank of Nova Scotia";
			if (number.Substring(0, 4) == "4539") return "Visa - Barclays (UK)";
			if (number.Substring(0, 4) == "4543") return "Visa - First Direct";
			if (number.Substring(0, 4) == "4544") return "Visa - T.S.B. Bank";
			if (number.Substring(0, 4) == "4556") return "Visa - Citibank";
			if (number.Substring(0, 4) == "4564") return "Visa - Bank of Queensland";
			if (number.Substring(0, 4) == "4673") return "Visa - First Card";
			if (number.Substring(0, 4) == "4678") return "Visa - Home Federal";
			if (number.Substring(0, 4) == "4707") return "Visa - Tompkins County Trust";
			if (number.Substring(0, 8) == "47121250") return "Visa - IBM Credit Union";
			if (number.Substring(0, 4) == "4719") return "Visa - Rocky Mountain";
			if (number.Substring(0, 4) == "4721") return "Visa - First Security";
			if (number.Substring(0, 4) == "4722") return "Visa - West Bank";
			if (number.Substring(0, 4) == "4726") return "Visa CV - Wells Fargo";
			if (number.Substring(0, 4) == "4783") return "Visa - AT&T's Universal Card";
			if (number.Substring(0, 4) == "4784") return "Visa - AT&T's Universal Card";
			if (number.Substring(0, 4) == "4800") return "Visa - M.B.N.A. North America";
			if (number.Substring(0, 4) == "4811") return "Visa - Bank of Hawaii";
			if (number.Substring(0, 4) == "4819") return "Visa - Macom Federal Credit Union";
			if (number.Substring(0, 4) == "4820") return "Visa - IBM Mid America Federal Credit Union";
			if (number.Substring(0, 4) == "4833") return "Visa - U.S. Bank";
			if (number.Substring(0, 4) == "4842") return "Visa - Security Pacific Washington";
			if (number.Substring(0, 4) == "4897") return "Visa - Village Bank of Chicago";
			if (number.Substring(0, 4) == "4921") return "Visa - Hong Kong National Bank";
			if (number.Substring(0, 4) == "4929") return "Visa CV - Barclay Card (UK)";
			if (number.Substring(0, 8) == "45399710") return "Visa - Banco di Napoli (Italy)";
			if (number.Substring(0, 4) == "4557") return "Visa - BNL (Italy)";
			if (number.Substring(0, 4) == "4908") return "Visa - CARIPLO (Italy)";
			if (number.Substring(0, 4) == "4532") return "Visa - Credito Italiano (Italy)";
			if (number.Substring(0, 8) == "45475900") return "Visa Gold - bank ganadero BBV (Colombia)";
			if (number.Substring(0, 4) == "4916") return "Visa - MBNA Bank";
			if (number.Substring(0, 1) == "4") return "Visa";
			return "Visa";
		}

		static private void ValidateNumber(string number, CreditCardType type)
		{
			ValidateLength(number, type);
			ValidateValue(number);
		}

		static private void ValidateLength(string number, CreditCardType type)
		{
			switch (type)
			{
				case CreditCardType.Visa:
					if ((16 != number.Length) && (13 != number.Length))
					{
						string sMsg = "Visa card must have 13 or 16 digits.";
						Tracer.Trace("CommonLib", "CreditCard", "ValidateLength", "war", sMsg);
						throw new ApplicationException(sMsg);
					}
					break;

				case CreditCardType.AmericanExpress:
					if (15 != number.Length)
					{
						string sMsg = "AmericanExpress card must have 15 digits.";
						Tracer.Trace("CommonLib", "CreditCard", "ValidateLength", "war", sMsg);
						throw new ApplicationException(sMsg);
					}
					break;

				case CreditCardType.MasterCard:
					if (16 != number.Length)
					{
						string sMsg = "MasterCard card must have 16 digits.";
						Tracer.Trace("CommonLib", "CreditCard", "ValidateLength", "war", sMsg);
						throw new ApplicationException(sMsg);
					}
					break;

				case CreditCardType.Discover:
					if (16 != number.Length)
					{
						string sMsg = "Discover card must have 16 digits.";
						Tracer.Trace("CommonLib", "CreditCard", "ValidateLength", "war", sMsg);
						throw new ApplicationException(sMsg);
					}
					break;

				case CreditCardType.JCB:
					if (16 != number.Length)
					{
						string sMsg = "JCB card must have 16 digits.";
						Tracer.Trace("CommonLib", "CreditCard", "ValidateLength", "war", sMsg);
						throw new ApplicationException(sMsg);
					}
					break;

				case CreditCardType.DinersClub:
					if ((16 != number.Length) && (14 != number.Length))
					{
						string sMsg = "DinersClub card must have 14 or 16 digits.";
						Tracer.Trace("CommonLib", "CreditCard", "ValidateLength", "war", sMsg);
						throw new ApplicationException(sMsg);
					}
					break;

				default:
				{
					string sMsg = "DEVELOPER ERROR";
					Tracer.Trace("CommonLib", "CreditCard", "ValidateLength", "war", sMsg);
					throw new ApplicationException(sMsg);
				}
			}
		}

		static private void ValidateValue(string number)
		{
			if (CreditCard.s_sPassThruNumber == number) return;

			// The Luhn-10 algorithm was taken from EPX documentation
			// Code was copied from CCGateway

			int idx, sum ;
			string sWorking = number;
			int[] validator ;

			validator = new int[sWorking.Length-1] ;

			// The rather unwieldy statement SafeConvert.ToInt(sWorking[idx].ToString())
			// is used because Convert.ToInt32(char) returns the ASCII value rather than the digit.
			sum = 1 ;
			for ( idx = sWorking.Length-3 ; idx < sWorking.Length-1 ; idx++ )
			{
				validator[idx] = sum * SafeConvert.ToInt( sWorking[idx].ToString() ) ;
				sum = sum == 1 ? 2 : 1 ;
			}

			sum = 2 ;
			for ( idx = sWorking.Length-4; idx >= 0; idx-- )
			{
				validator[idx] = sum * SafeConvert.ToInt( sWorking[idx].ToString() ) ;
				sum = sum == 1 ? 2 : 1 ;
			}

			sum = 0;
			for ( idx = 0 ; idx < sWorking.Length-1; idx++ )
			{
				if ( validator[idx] >= 10 )
					sum += ( validator[idx] / 10 ) + ( validator[idx] % 10 ) ;
				else
					sum += validator[idx] ;
			}

			sum += SafeConvert.ToInt( sWorking[sWorking.Length-1].ToString() ) ;

			if ( sum % 10 != 0 )
			{
				string sMsg = "Credit card number is invalid.";
				Tracer.Trace("CommonLib", "CreditCard", "ValidateValue", "war", sMsg);
				throw new ApplicationException(sMsg);
			}
		}

		private void SetNumberAndType(string number)
		{
			this.m_type = TypeFromNumber(number);
			this.m_sNumber = CleanNumber(number);
		}

		private void SetExpDate(int month, int year)
		{
			if ((0 >= month) || (12 < month))
			{
				string sMsg = "Month must be in range [1, 12]";
				Tracer.Trace("CommonLib", "CreditCard", "SetExpDate", "war", sMsg);
				throw new ApplicationException(sMsg);
			}

			DateTime today = DateTime.Today;
			DateTime lastPossible = today.AddYears(CreditCard.MaxExpYears);
			// Actually expires at end of input month
			int nExpMonth = (12 == month) ? 1 : month + 1;
			int nExpYear  = (12 == month) ? year + 1 : year;
			DateTime expires = new DateTime(nExpYear, nExpMonth, 1);

			if (expires >= lastPossible)
			{
				string sMsg = "Expiration date beyond " + CreditCard.MaxExpYears.ToString() + " year limit";
				Tracer.Trace("CommonLib", "CreditCard", "SetExpDate", "war", sMsg);
				throw new ApplicationException(sMsg);
			}

			// NOTE: do not check whether it is expired here.  The property IsExpired can be used
			//		 for that.  The reason is that valid cards when entered into db will become
			//		 expired with time.  We don't want to throw exceptions in this case.  Better
			//		 that the client code call IsExpired explicitely and take whatever action is
			//		 appropriate in the given context.

			this.m_nExpMonth = month;
			this.m_nExpYear = year;
		}

		private void SetCVV(string cvv)
		{
			if ((null == cvv) || ("" == cvv)) return;

			// Assumes that this is called after card type is set.
			switch (this.m_type)
			{
				case CreditCardType.Visa:
					if (3 != cvv.Length) 
					{
						string sMsg = "CVV must have 3 digits for Visa.";
						Tracer.Trace("CommonLib", "CreditCard", "SetCVV", "war", sMsg);
						throw new ApplicationException(sMsg);
					}
					break;

				case CreditCardType.MasterCard:
					if (3 != cvv.Length)
					{
						string sMsg = "CVV must have 3 digits for MasterCard.";
						Tracer.Trace("CommonLib", "CreditCard", "SetCVV", "war", sMsg);
						throw new ApplicationException(sMsg);
					}
					break;

				case CreditCardType.AmericanExpress:
					if (4 != cvv.Length)
					{
						string sMsg = "CVV must have 4 digits for AmericanExpress.";
						Tracer.Trace("CommonLib", "CreditCard", "SetCVV", "war", sMsg);
						throw new ApplicationException(sMsg);
					}
					break;

				case CreditCardType.Discover:
					if (3 != cvv.Length)
					{
						string sMsg = "CVV must have 3 digits for Discover.";
						Tracer.Trace("CommonLib", "CreditCard", "SetCVV", "war", sMsg);
						throw new ApplicationException(sMsg);
					}
					break;

				default:
					return; // Don't do anything if the card type doesn't have a CVV
			}

			try
			{
				Convert.ToInt32(cvv); // will throw an exception if not numeric
			}
			catch
			{
				string sMsg = "A Card Verification Value must be numeric.";
				Tracer.Trace("CommonLib", "CreditCard", "SetCVV", "war", sMsg);
				throw new ApplicationException(sMsg);
			}

			this.m_sVerificationValue = cvv;
		}

		private string Encrypt(string inString)
		{
			using (CryptoUtil crypto = new CryptoUtil(CreditCard.s_sEncryptKey))
			{
				return crypto.Encrypt(inString);
			}
		}

		private string Decrypt(string inString)
		{
			using (CryptoUtil crypto = new CryptoUtil(CreditCard.s_sEncryptKey))
			{
				return crypto.Decrypt(inString);
			}
		}

		static private string CleanNumber(string toClean)
		{
			// strip out all but digits from string
			StringBuilder sb = new StringBuilder("");
			for (int i=0; i<toClean.Length; ++i)
			{
				if (char.IsDigit(toClean, i)) sb.Append(toClean[i]);
			}

			return sb.ToString();
		}
	}
}
