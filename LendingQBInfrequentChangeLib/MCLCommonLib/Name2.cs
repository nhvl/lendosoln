using System;
using System.Collections;
using System.Runtime.Serialization;
using System.Text;

namespace CommonLib
{
	/// <summary>
	/// Utility class for parsing a name into components title, first, middle, last, suffix
	/// Note: implemented as a struct so it can be allocated on the stack -- no GC burden
	///	Note: Religious names that omit the lastname (a common practice) will be misparsed as lastname is assumed
	///	
	///	Note: There is a difficulty of interpretation with compound names when the prefix can also be a firstname.
	///		  Some arbitrary choices need to be made.  The choice will depend upon the existence
	///		  and "degree of formality" of the title.  See the code below for details.
	///		  For example, the 'Van' in Van Morrison will be interpreted as a firstname in 'Van Morrison' and 
	///		  'Mr. Van Morrison', but as part of a compound lastname in 'Dr. Van Morrison'.
	/// </summary>
	[Serializable]
	public struct Name2 : ISerializable
	{
		const double Version = 1.0;

		private enum TitleType {None = 0, MR, MRS, MISS, MS, M_S, DON, DONA, MME, MMLLE, FATHER, BROTHER, SISTER, MOTHER, REV, DR, SIR, PROF, LORD, LADY, DAME, CAPT, MAJ, COL, GEN}
		private enum SuffixType {None = 0, SR = 1, JR = 2, III = 3, IV = 4, I = 5, II = 6} // values chosen to match MCL suffix codes (except None)

		static private string[] PREFIX_WORDS = new string[] {"DE", "DEL", "DELA", "DELLA" , "MC", "MAC", "O'", "VAN", "VON"};

		TitleType m_Title;
		private string m_sFirst;
		private string m_sLast;
		private string m_sMiddle;
		SuffixType m_Suffix;

		[NonSerialized]
		private ArrayList m_goodPieces;

		public Name2(SerializationInfo info, StreamingContext context)
		{
			double dVersion = info.GetDouble("version");

			this.m_Title = TitleStringToType(info.GetString("title"));
			this.m_sFirst = info.GetString("first");
			this.m_sMiddle = info.GetString("middle");
			this.m_sLast = info.GetString("last");

			// 06/15/04-Binh-Added as a work-around for a bug in the SoapSerializer with blank characters.
			char c = info.GetChar("suffix") ;
			if (c == '_') c = ' ' ;
			this.m_Suffix = SuffixCodeToType(c);

			this.m_goodPieces = null;
		}

		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("version", Name2.Version);
			
			info.AddValue("title", TitleTypeToString(this.m_Title));
			info.AddValue("first", this.FirstName);
			info.AddValue("middle", this.MiddleName);
			info.AddValue("last", this.LastName);

			// 06/15/04-Binh-Added as a work-around for a bug in the SoapSerializer with blank characters.
			char c = SuffixTypeToCode(this.m_Suffix) ;
			if (c == ' ') c = '_' ;
			info.AddValue("suffix", c);
		}

		// MCL uses a coding system for suffix
		static private SuffixType SuffixCodeToType(char code)
		{
			switch (code)
			{
				case ' ':
					return SuffixType.None;
				case '1':
					return SuffixType.SR;
				case '2':
					return SuffixType.JR;
				case '3':
					return SuffixType.III;
				case '4':
					return SuffixType.IV;
				case '5':
					return SuffixType.I;
				case '6':
					return SuffixType.II;
				default:
					return SuffixType.None;
			}
		}

		static private char SuffixTypeToCode(SuffixType type)
		{
			switch (type)
			{
				case SuffixType.None:
					return ' ';
				case SuffixType.SR:
					return '1';
				case SuffixType.JR:
					return '2';
				case SuffixType.III:
					return '3';
				case SuffixType.IV:
					return '4';
				case SuffixType.I:
					return '5';
				case SuffixType.II:
					return '6';
				default:
					return ' ';
			}
		}

		/// <summary>
		/// Convert MCL suffix code to standardized suffix string
		/// </summary>
		static public string SuffixCodeToString(char code)
		{
			SuffixType type = SuffixCodeToType(code);
			return (SuffixType.None == type) ? "" : type.ToString();
		}

		/// <summary>
		/// Sometimes a name may be misinterpreted as a suffix.  This method indicates that this may be the case.
		/// </summary>
		static public bool SuffixIsName(string suffix)
		{
			switch (suffix.ToUpper().Trim())
			{
				case "SENIOR":
				case "JUNIOR":
					return true;
				default:
					return false;
			}
		}

		/// <summary>
		/// Convert input string into an MCL suffix code, or ' ' if not a recognized suffix
		/// </summary>
		static public char SuffixStringToCode(string suffix)
		{
			switch (suffix.ToUpper().Trim())
			{
				case "": // placed here for efficiency
					return ' ';

				case "1":
				case "SR":
				case "SR.":
				case "SNR":
				case "SNR.":
				case "SENIOR":
					return '1';

				case "2":
				case "JR":
				case "JR.":
				case "JNR":
				case "JNR.":
				case "JUNIOR":
					return '2';

				case "3":
				case "III":
				case "3RD":
				case "THIRD":
					return '3';

				case "4":
				case "IV":
				case "4TH":
				case "FOURTH":
					return '4';

				case "5":
				case "I":
				case "1ST":
				case "FIRST":
					return '5';

				case "6":
				case "II":
				case "2ND":
				case "SECOND":
					return '6';

				default:
					return ' ';
			}
		}

		static private string TitleTypeToString(TitleType title)
		{
			if (TitleType.None == title) return "";
			else if (TitleType.M_S == title) return "M/S";
			else return title.ToString();
		}

		static private TitleType TitleStringToType(string title)
		{
			switch (title.Trim().ToUpper())
			{
				case "": // placed here for efficiency
					return TitleType.None;

				case "MR":
				case "MR.":
				case "MISTER":
					return TitleType.MR;

				case "MRS":
				case "MRS.":
				case "MISSES":
					return TitleType.MRS;

				case "MISS":
					return TitleType.MISS;

				case "MS":
				case "MS.":
					return TitleType.MS;

				case "M/S":
					return TitleType.M_S;

					/* -- There are people with first name of DON, DONA and DONNA
				case "DON":
					return TitleType.DON;

				case "DONA":
				case "DONNA":
					return TitleType.DONA;
					*/
				case "MME":
				case "MME.":
				case "MDM":
				case "MDM.":
				case "MDME":
				case "MDME.":
				case "MADAM":
				case "MADAME":
					return TitleType.MME;

				case "MMLLE":
				case "MMLLE.":
				case "MADEMOISELLE":
					return TitleType.MMLLE;

				case "FR":
				case "FR.":
				case "FATHER":
					return TitleType.FATHER;

				case "BROTHER":
					return TitleType.BROTHER;

				case "SISTER":
					return TitleType.SISTER;

				case "MOTHER":
					return TitleType.MOTHER;

				case "REV":
				case "REV.":
				case "REVEREND":
				case "REVERAND":
					return TitleType.REV;

				case "DR":
				case "DR.":
				case "DCTR":
				case "DOCTOR":
					return TitleType.DR;

				case "SIR":
					return TitleType.SIR;

				case "PROF":
				case "PROF.":
				case "PROFESSOR":
					return TitleType.PROF;

				case "LORD":
				case "LRD":
				case "LRD.":
					return TitleType.LORD;

				case "LADY":
					return TitleType.LADY;

				case "DAME":
				case "DME":
				case "DME.":
					return TitleType.DAME;

				case "CPT":
				case "CPT.":
				case "CAPT":
				case "CAPT.":
				case "CAPTAIN":
					return TitleType.CAPT;

				case "MJR":
				case "MJR.":
				case "MAJ":
				case "MAJ.":
				case "MAJOR":
					return TitleType.MAJ;

				case "COL":
				case "COL.":
				case "CNL":
				case "CNL.":
				case "CRNL":
				case "CRNL.":
				case "CLNL":
				case "CLNL.":
				case "COLONEL":
					return TitleType.COL;

				case "GEN":
				case "GEN.":
				case "GENL":
				case "GENL.":
				case "GNRL":
				case "GNRL.":
				case "GENERAL":
					return TitleType.GEN;

				default:
					return TitleType.None;
			}
		}

		/// <summary>
		/// get returns standardized title string; set parses input to a title;  "" used when no title.
		/// </summary>
		public string Title
		{
			get {return TitleTypeToString(this.m_Title);}
			set {this.m_Title = TitleStringToType(value);}
		}

		public string TitleSpelledOut
		{
			get
			{
				switch (this.m_Title)
				{
					case TitleType.MR:
						return "MISTER";
					case TitleType.MRS:
						return "MISSES";
					case TitleType.MISS:
						return "MISS";
					case TitleType.MS:
						return "MS";
					case TitleType.M_S:
						return "M/S";
					case TitleType.DON:
						return "DON";
					case TitleType.DONA:
						return "DONA";
					case TitleType.MME:
						return "MADAME";
					case TitleType.MMLLE:
						return "MADEMOISELLE";
					case TitleType.FATHER:
						return "FATHER";
					case TitleType.BROTHER:
						return "BROTHER";
					case TitleType.SISTER:
						return "SISTER";
					case TitleType.MOTHER:
						return "MOTHER";
					case TitleType.REV:
						return "REVERAND";
					case TitleType.DR:
						return "DOCTOR";
					case TitleType.SIR:
						return "SIR";
					case TitleType.PROF:
						return "PROFESSOR";
					case TitleType.LORD:
						return "LORD";
					case TitleType.LADY:
						return "LADY";
					case TitleType.DAME:
						return "DAME";
					case TitleType.CAPT:
						return "CAPTAIN";
					case TitleType.MAJ:
						return "MAJOR";
					case TitleType.COL:
						return "COLONEL";
					case TitleType.GEN:
						return "GENERAL";
					case TitleType.None:
					default:
						return string.Empty;
				}
			}
		}

		public string FirstName
		{
			get {return (null == this.m_sFirst) ? "" : this.m_sFirst;}
			set {SetStringValue(value, ref this.m_sFirst);}
		}

		public string MiddleName
		{
			get {return (null == this.m_sMiddle) ? "" : this.m_sMiddle;}
			set {SetStringValue(value, ref this.m_sMiddle);}
		}

		public string LastName
		{
			get {return (null == this.m_sLast) ? "" : this.m_sLast;}
			set {SetStringValue(value, ref this.m_sLast);}
		}

		/// <summary>
		/// get returns standardized suffix string; set parses input to a suffix;  "" used when no suffix.
		/// </summary>
		public string Suffix
		{
			get {return SuffixCodeToString(this.SuffixCode);}
			set {this.SuffixCode = SuffixStringToCode(value);}
		}

		/// <summary>
		/// MCL suffix code
		/// </summary>
		public char SuffixCode
		{
			get {return SuffixTypeToCode(this.m_Suffix);}
			set {this.m_Suffix = SuffixCodeToType(value);}
		}

		// Fullname in title first middle last suffix format
		public string FullName
		{
			get
			{
				return CombineName(this.Title, this.FirstName, this.MiddleName, this.LastName, this.SuffixCode);
			}
		}

		// Fullname in last, title first middle suffix format
		public string LastFirstName
		{
			get
			{
				StringBuilder sb = new StringBuilder(100);
				string sTemp = this.LastName;
				if (sTemp != "") sb.Append(sTemp + ", ");
				sTemp = this.Title;
				if (sTemp != "") sb.Append(sTemp + " ");
				sTemp = this.FirstName;
				if (sTemp != "") sb.Append(sTemp + " ");
				sTemp = this.MiddleName;
				if (sTemp != "") sb.Append(sTemp + " ");
				sTemp = this.Suffix;
				if (sTemp != "") sb.Append(sTemp + " ");

				return sb.ToString().Trim();
			}
		}

		public override string ToString()
		{
			return this.FullName;
		}

		public void Clear()
		{
			this.m_sFirst = "";
			this.m_sMiddle = "";
			this.m_sLast = "";
			this.m_Suffix = SuffixType.None;
			this.m_Title = TitleType.None;
		}

		public static string CombineName(string title, string first, string middle, string last, char suffixCode)
		{
			StringBuilder sb = new StringBuilder(100);
			if (SafeString.HasValue(title)) sb.Append(title + " ");
			if (SafeString.HasValue(first)) sb.Append(first + " ");
			if (SafeString.HasValue(middle)) sb.Append(middle + " ");
			if (SafeString.HasValue(last)) sb.Append(last + " ");
			string sSuffix = SuffixCodeToString(suffixCode);
			if (SafeString.HasValue(sSuffix)) sb.Append(sSuffix);

			return sb.ToString().Trim();
		}

		/// <summary>
		/// Parse input string into name components
		/// </summary>
		public void ParseName(string inString)
		{
			Clear();

			bool bException = false;
			try
			{
				// normalize into array of valid tokens
				string sName = inString.Replace(",", " , ").Trim().ToUpper();
				string[] pieces = sName.Split(null);
				ArrayList goodPieces = new ArrayList(pieces.Length);
				foreach (string piece in pieces)
				{
					if ("" != piece) goodPieces.Add(piece);
				}

				if (goodPieces.Count == 0)
					throw new ApplicationException("Invalid name");

				this.m_goodPieces = (ArrayList)goodPieces.Clone();

				if (goodPieces.Count == 1)
				{
					this.m_sLast = (string)goodPieces[0];
					return;
				}

				// peel off title
				this.Title = (string)goodPieces[0];
				if (TitleType.None != this.m_Title)
					goodPieces.RemoveAt(0);

				if (goodPieces.Count == 0)
					throw new ApplicationException("Invalid name");

				if (goodPieces.Count == 1)
				{
					this.m_sLast = (string)goodPieces[0];
					return;
				}

				// peel off suffix
				char cTest = SuffixStringToCode((string)goodPieces[goodPieces.Count - 1]);
				if (cTest != ' ')
				{
					this.SuffixCode = cTest;
					goodPieces.RemoveAt(goodPieces.Count - 1);
					string sLast = (string)goodPieces[goodPieces.Count - 1];
					if (sLast == ",") // sometimes a comma precedes the suffix
						goodPieces.RemoveAt(goodPieces.Count - 1);

					if (SuffixIsName((string)this.m_goodPieces[this.m_goodPieces.Count - 1]))
					{
						bool bIsLastName = (goodPieces.Count == 0);
						if (!bIsLastName) bIsLastName = ((goodPieces.Count == 1) && (this.m_Title == TitleType.None));
						if (bIsLastName)
						{
							// In this case the suffix is most likely a last name, we'll add it back and continue processing
							this.SuffixCode = ' ';
							goodPieces.Add(this.m_goodPieces[this.m_goodPieces.Count - 1]);
						}
					}
				}

				// peel off last name
				bool bCommaFound = false;
				int commaIndex = 0;
				foreach (string piece in goodPieces)
				{
					if (bCommaFound && (piece == ","))
						throw new ApplicationException("Invalid name");

					if (piece == ",") bCommaFound = true;
					else if (!bCommaFound) ++commaIndex;
				}
				if (bCommaFound) // in form last , first ...
				{
					if (commaIndex == 0)
						throw new ApplicationException("Invalid name");

					StringBuilder sb = new StringBuilder(100);
					sb.Append((string)goodPieces[0]);
					if (commaIndex > 1)
					{
						for (int i=1; i<commaIndex; ++i)
						{
							sb.Append(" " + (string)goodPieces[i]);
						}
					}
					this.m_sLast = sb.ToString();

					for (int i=commaIndex; i>=0; --i)
						goodPieces.RemoveAt(i);

					// title may be in this section
					if (TitleType.None == this.m_Title)
					{
						this.Title = (string)goodPieces[0];
						if (TitleType.None != this.m_Title)
							goodPieces.RemoveAt(0);
					}
				}
				else // in form first ... last
				{
					if (goodPieces.Count == 1)
					{
						// just last
						this.m_sLast = (string)goodPieces[0];
						return;
					}

					string sTest = (string)goodPieces[goodPieces.Count - 2];
					if (IsPrefix(sTest))
					{
						this.m_sLast = sTest + " " + (string)goodPieces[goodPieces.Count - 1];
						goodPieces.RemoveAt(goodPieces.Count - 1);
						goodPieces.RemoveAt(goodPieces.Count - 1);
					}
					else
					{
						this.m_sLast = (string)goodPieces[goodPieces.Count - 1];
						goodPieces.RemoveAt(goodPieces.Count - 1);
					}
				}

				if (goodPieces.Count > 0)
				{
					// peel off first name
					this.m_sFirst = (string)goodPieces[0];
					goodPieces.RemoveAt(0);

					// remainder is middle name
					if (goodPieces.Count > 0)
					{
						StringBuilder sb = new StringBuilder(100);
						sb.Append((string)goodPieces[0]);
						goodPieces.RemoveAt(0);
						foreach (string piece in goodPieces)
						{
							sb.Append(" " + piece);
						}
						this.m_sMiddle = sb.ToString();
						if ((goodPieces.Count == 0) && this.m_sMiddle.EndsWith("."))
							this.m_sMiddle = this.m_sMiddle.Substring(0, this.m_sMiddle.Length - 1);
					}
				}
			}
			catch
			{
				bException = true;
				throw;
			}
			finally
			{
				if (!bException) PostProcess();
			}
		}

		private void PostProcess()
		{
			RepairCompoundLastName();
			RepairMistakenSuffix();
		}

		// some prefix words can be firstnames...restore if no first name found
		private void RepairCompoundLastName()
		{
			if (this.FirstName != "") return;

			string[] pieces = this.LastName.Split(null);
			if (pieces.Length < 2) return;

			switch (pieces[0])
			{
				case "DEL":
				case "DELLA":
				case "MAC":
				case "VAN":
					// Defer...not all cases are repaired
					RepairBasedOnTitle(pieces);
					break;
			}
		}

		private void RepairMistakenSuffix()
		{
			if (this.m_Suffix == SuffixType.None) return;

			string sSuffix = (string)this.m_goodPieces[this.m_goodPieces.Count - 1];
			if (!SuffixIsName(sSuffix)) return;

			if ((this.m_sLast.Length == 1) && (!SafeString.HasValue(this.m_sMiddle)))
			{
				// Suffix most likely a name and the single letter a middle initial
				this.m_sMiddle = this.m_sLast;
				this.m_sLast = sSuffix;
				this.m_Suffix = SuffixType.None;
			}
		}

		// only make repairs for certain more "formal" titles
		private void RepairBasedOnTitle(string[] pieces)
		{
			// Leave prefix as part of lastname for formal titles
			switch (this.m_Title)
			{
				case TitleType.DR:
				case TitleType.PROF:
				case TitleType.LORD:
				case TitleType.LADY:
				case TitleType.SIR:
				case TitleType.DAME:
				case TitleType.DON:
				case TitleType.DONA:
					return;
			}

			// Otherwise interpret prefix as first name
			this.m_sFirst = pieces[0];
			StringBuilder sb = new StringBuilder(100);
			for (int i=1; i<pieces.Length; ++i)
				sb.Append(pieces[i] + " ");
			this.m_sLast = sb.ToString().Trim();
		}

		private void SetStringValue(string val, ref string member)
		{
			if ((null == val) || ("" == val)) return;
			member = val.Trim().ToUpper();
		}

		private bool IsPrefix(string inString)
		{
			foreach (string pref in PREFIX_WORDS)
			{
				if (pref == inString) return true;
			}

			return false;
		}
	}
}
