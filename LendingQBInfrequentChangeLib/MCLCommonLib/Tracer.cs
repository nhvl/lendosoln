using System;
using System.Collections;
using System.Threading;

namespace CommonLib
{
	/// <summary>
	/// The Tracer class below can get screwed up if you don't insure that every Enter is matched
	/// with a single call to Leave.  This utility class can be used to insure this, even in the
	/// face of exceptions, or careless coding of returns.
	/// 
	/// USAGE:
	/// public void SomeMethod()
	/// {
	///		using (SafeTracer tracer = new SafeTracer(this.m_myTracer, "SomeMethod"))
	///		{
	///			// code here
	///		}
	///	}
	/// </summary>
	public class SafeTracer : IDisposable
	{
		protected Tracer m_tracer = null;
		protected string m_sLastContext = "" ;

		public SafeTracer(Tracer tracer, string method)
		{
			this.m_tracer = tracer;
			tracer.Enter(method);
		}

		protected SafeTracer(){}

		virtual public void Dispose()
		{
			m_tracer.Leave();
			m_tracer = null;
		}

		/// <summary>
		/// Used to retrieve a thread-safe version of SafeTracer.  Threads will block during
		/// the entire lifetime (creation --> dispose) of the active SafeTracer.  This permits 
		/// protection of the callstack from corruption.
		/// </summary>
		/// <param name="tracer"></param>
		/// <param name="method"></param>
		/// <returns></returns>
		static public SafeTracer Synchronized(Tracer tracer, string method)
		{
			return new SafeTracerSync(tracer, method);
		}

		virtual public void Verbose(string message)
		{
			m_tracer.Verbose(message);
		}

		virtual public void Verbose(string message, int line)
		{
			m_tracer.Verbose(message, line);
		}

		virtual public void Inform(string message)
		{
			m_tracer.Inform(message);
		}

		virtual public void Inform(string message, int line)
		{
			m_tracer.Inform(message, line);
		}

		virtual public void TraceLog(string message)
		{
			m_tracer.TraceLog(message);
		}

		virtual public void TraceLog(string type, string message)
		{
			m_tracer.TraceLog(type, message);
		}

		virtual public void TraceLog(string type, string message, int line)
		{
			m_tracer.TraceLog(type, message, line);
		}

		virtual public void Warn(string message)
		{
			m_tracer.Warn(message);
		}

		virtual public void Warn(string message, int line)
		{
			m_tracer.Warn(message, line);
		}

		virtual public void Error(string message)
		{
			m_tracer.Error(message);
		}

		virtual public void Error(string message, int line)
		{
			m_tracer.Error(message, line);
		}

		virtual public void SetTraceID(string traceID)
		{

		}
	}

	internal class SafeTracerSync : SafeTracer
	{
		internal SafeTracerSync(Tracer tracer, string method)
		{
			try
			{
				this.m_tracer = tracer;
				Monitor.Enter(this.m_tracer); 
				this.m_tracer.Enter(method);
			}
			catch
			{
				Monitor.Exit(this.m_tracer);
				this.m_tracer = null;
				throw;
			}
		}

		override public void Dispose()
		{


			this.m_tracer.Leave();
			Monitor.Exit(this.m_tracer);
			m_tracer = null;
		}

		override public void Verbose(string message)
		{
			m_tracer.Verbose(message);
		}

		override public void Verbose(string message, int line)
		{
			m_tracer.Verbose(message, line);
		}

		override public void Inform(string message)
		{
			m_tracer.Inform(message);
		}

		override public void Inform(string message, int line)
		{
			m_tracer.Inform(message, line);
		}

		override public void TraceLog(string message)
		{
			m_tracer.TraceLog(message);
		}

		override public void TraceLog(string type, string message)
		{
			m_tracer.TraceLog(type, message);
		}

		override public void TraceLog(string type, string message, int line)
		{
			m_tracer.TraceLog(type, message, line);
		}

		override public void Warn(string message)
		{
			m_tracer.Warn(message);
		}

		override public void Warn(string message, int line)
		{
			m_tracer.Warn(message, line);
		}

		override public void Error(string message)
		{
			m_tracer.Error(message);
		}

		override public void Error(string message, int line)
		{
			m_tracer.Error(message, line);
		}

		override public void SetTraceID(string traceID)
		{
		}
	}

	/// <summary>
	/// Utility for sending trace output to Paul Bunyan.
	/// 
	/// Features:
	/// 1) Enter/Leave methods that maintain a call stack for proper context handling.
	/// 2) TraceID for identifying caller (e.g., broker's username) is also part of context.
	/// 3) Trace flag that turns off tracing for verbose/info messages.
	/// 4) More flexible interface than Paul Bunyan.
	/// 5) Thread-safe static trace methods
	/// 
	/// NOTE: The static methods shouldn't be used from within loops or performance is likely
	///		  to be adversely affected.
	/// </summary>
	public class Tracer
	{
		private bool m_bTrace = false;
		private string m_sTraceID = "";

		public Tracer()
		{
		}

		static public void Trace(string type, string message)
		{
			Tracer.Trace("", type, message);
		}

		static public void Trace(string context, string type, string message)
		{
			Tracer.Trace("", context, type, message);
		}

		static public void Trace(string component, string context, string type, string message)
		{
			Tracer.Trace("", component, context, type, message);
		}

		static public void Trace(string module, string component, string context, string type, string message)
		{
			Tracer.Trace(module, component, context, type, message, Tracer.GetCallerLineNumber(0));
		}

		static public void Trace(string module, string component, string context, string type, string message, int line)
		{

		}

		public void SetTraceID(string traceID)
		{
			this.m_sTraceID = (null == traceID) ? "" : traceID;
		}

		public void SetLogContext(string context)
		{

		}

		public void SetModule(string module)
		{
		}

		public void SetComponent(string component)
		{
		}

		public void Enter(string method)
		{
		}

		public void Leave()
		{
		}

		public void DoTrace(bool flag)
		{
			this.m_bTrace = flag;
		}

		public void Verbose(string message)
		{
			Verbose(message, Tracer.GetCallerLineNumber(0));
		}

		public void Verbose(string message, int line)
		{
			TraceLog("ver", message, line);
		}

		public void Inform(string message)
		{
			Inform(message, Tracer.GetCallerLineNumber(0));
		}

		public void Inform(string message, int line)
		{
			TraceLog("inf", message, line);
		}

		public void TraceLog(string message)
		{
			TraceLog("ver", message, GetCallerLineNumber(0));
		}

		public void TraceLog(string type, string message)
		{
			if ("err" == type.ToLower())
				Error(message);
			else if ("war" == type.ToLower())
				Warn(message);
			else if (this.m_bTrace)
				TraceLog(type, message, Tracer.GetCallerLineNumber(0));
		}

		public void TraceLog(string type, string message, int line)
		{
			if ("err" == type.ToLower())
				Error(message, line);
			else if ("war" == type.ToLower())
				Warn(message, line);
			else if (this.m_bTrace)
				SafeLog(type, message, line);
		}

		public void Warn(string message)
		{
			Warn(message, Tracer.GetCallerLineNumber(333));
		}

		public void Warn(string message, int line)
		{
			SafeLog("war", message, line);
		}

		public void Error(string message)
		{
			Error(message, Tracer.GetCallerLineNumber(666));
		}

		public void Error(string message, int line)
		{
			SafeLog("err", message, line);
		}

		private void SafeLog(string type, string message, int line)
		{
		}

        //internal string Context
        //{

        //}

		/// <summary>
		/// Gets the line number from the object ultimately calling the tracer.
		/// </summary>
		/// <param name="defaultLineNumber">The default line number will be returned if the line number cannot be found.</param>
		/// <returns>If available, the calling line number; otherwise, the specified default.</returns>
		private static int GetCallerLineNumber(int defaultLineNumber)
		{
#if DEBUG
			// Get the stack trace.
			// We can skip this method and its caller in this class.
            System.Diagnostics.StackTrace stackTrace = new System.Diagnostics.StackTrace(2, true) ;

            // Find the most recent frame that isn't a tracer.
			for ( int idx = 0 ; idx < stackTrace.FrameCount ;  idx++ )
			{
				System.Diagnostics.StackFrame stackFrame = stackTrace.GetFrame(idx) ;

				string sFileName = stackFrame.GetFileName() ;
				if ( null != sFileName && ! sFileName.ToLower().EndsWith("tracer.cs") )
				{
					int nLineNumber = stackFrame.GetFileLineNumber() ;
					if ( 0 != nLineNumber )
						return nLineNumber ;
				}
			}

			// If we can't find the line number, return the default.
			return defaultLineNumber ;
#else
			// Non-DEBUG builds typically don't have debugging information and line numbers.
			return defaultLineNumber ;
#endif
		}
	}
}
