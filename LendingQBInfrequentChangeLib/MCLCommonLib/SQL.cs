#define TIMER

using System;
using System.Data;
using System.Data.SqlClient;

namespace CommonLib
{
	/// <summary>
	/// Utilities for accessing SQL databases,
	/// </summary>
	public class SqlUtil
	{
		public static string SQLString(string s)
		{
			if (null == s)
				return "''" ;
			else
				return "'" + s.Trim().Replace("'", "''") + "'" ;
		}
		public static string SQLString(char c)
		{
			if ('\'' == c)
				return "''''" ;
			else
				return "'" + c + "'" ;
		}
	}

	public class SQLCommaStringBuilder
	{
		public string SQL
		{
			get { return m_sSQL ; }
		}
		public void Reset()
		{
			m_sSQL = "" ;
		}
		public void Add(string s)
		{
			if ("" == m_sSQL)
				m_sSQL = s ;
			else
				m_sSQL += ", " + s ;
		}
		private string m_sSQL = "" ;
	}

	public interface ISQLStringBuilder
	{
		void Reset() ;

		/// <summary>
		/// The traditional method of adding a string value to an SQL statement.
		/// </summary>
		/// <param name="sField">Field name to use</param>
		/// <param name="sValue">Value to add</param>
		/// <example>builder.AddString("Password", "mypass")</example>
		/// <exception cref="System.Data.SqlClient.SqlException">Thrown if the value is too long.</exception>
		void AddString(string sField, string sValue) ;

		/// <summary>
		/// Adds a string value to a statement, allowing a maximum length to be set.
		/// </summary>
		/// <param name="sField">Field name to use</param>
		/// <param name="sValue">Value to add, truncated to maxLength</param>
		/// <param name="maxLength">Maximum field length</param>
		/// <example>builder.AddString("Password", "mysuperlongpassword", 15) is handled as 'mysuperlongpass'.</example>
		void AddString(string sField, string sValue, int maxLength) ;

		/// <summary>
		/// Optionally adds a string value to a statement.
		/// </summary>
		/// <param name="sField">Field name to use.</param>
		/// <param name="sValue">Value to add, trimmed and truncated to maxLength.</param>
		/// <param name="maxLength">Maximum field length.</param>
		/// <param name="bAddBlank">True to add a blank string to the SQL statement, False to omit blanks.</param>
		/// <param name="bUpperCase">True to convert the value to upper case, False otherwise.</param>
		/// <example>Using builder.AddString("StreetType", "", 20, false), nothing will be added;
		/// builder.AddString("StreetType", null, 20, true) will add ''.</example>
		void AddString(string sField, string sValue, int maxLength, bool bAddBlank, bool bUpperCase) ;

		/// <summary>
		/// Adds a non-string value to a statement.
		/// </summary>
		/// <param name="sField">Field name to use.</param>
		/// <param name="sValue">Value to add.</param>
		/// <example>builder.AddNonString("OrderID", "123"); builder.AddNonString("LogDate", "GetDate()")</example>
		void AddNonString(string sField, string sValue) ;
		void AddNonString(string sField, int nValue) ;
		void AddNonString(string sField, double nValue) ;
	}

	public class SQLInsertStringBuilder : ISQLStringBuilder
	{
		public override string ToString()
		{
			if ("" == m_oFields.SQL)
				return "" ;
			else
				return "(" + m_oFields.SQL + ") VALUES (" + m_oValues.SQL + ")" ;
		}
		public void Reset()
		{
			m_oFields.Reset() ;
			m_oValues.Reset() ;
		}
		public void AddString(string sField, string sValue)
		{
			m_oFields.Add(sField) ;
			m_oValues.Add(SqlUtil.SQLString(sValue)) ;
		}
		public void AddString(string sField, char cValue)
		{
			m_oFields.Add(sField) ;
			m_oValues.Add(SqlUtil.SQLString(cValue)) ;
		}
		public void AddString(string sField, string sValue, int maxLength)
		{
			m_oFields.Add(sField) ;
			m_oValues.Add(SqlUtil.SQLString( SafeString.Left(sValue, maxLength) ) ) ;
		}
		public void AddString(string sField, string sValue, int maxLength, bool bAddBlank, bool bUpperCase)
		{
			if ( sValue == null )
				sValue = "" ;
			else
			{
				sValue = sValue.Trim() ;
				if ( bUpperCase )
					sValue = sValue.ToUpper() ;
			}

			if ( sValue != "" || bAddBlank )
				AddString(sField, sValue, maxLength) ;
		}
		public void AddNonString(string sField, string sValue)
		{
			m_oFields.Add(sField) ;
			m_oValues.Add(sValue) ;
		}
		public void AddNonString(string sField, int nValue)
		{
			m_oFields.Add(sField) ;
			m_oValues.Add(nValue.ToString()) ;
		}
		public void AddNonString(string sField, double dblValue)
		{
			m_oFields.Add(sField) ;
			m_oValues.Add(dblValue.ToString()) ;
		}

		private SQLCommaStringBuilder m_oFields = new SQLCommaStringBuilder() ;
		private SQLCommaStringBuilder m_oValues = new SQLCommaStringBuilder() ;
	}

	public class SQLUpdateStringBuilder : ISQLStringBuilder
	{
		public override string ToString()
		{
			if ("" == m_oFields.SQL)
				return "" ;
			else
				return "SET " + m_oFields.SQL ;
		}
		public void Reset()
		{
			m_oFields.Reset() ;
		}
		public void AddString(string sField, string sValue)
		{
			m_oFields.Add(sField + "=" + SqlUtil.SQLString(sValue)) ;
		}
		public void AddString(string sField, string sValue, int maxLength)
		{
			m_oFields.Add(sField + "=" + SqlUtil.SQLString( SafeString.Left(sValue, maxLength) ) ) ;
		}
		public void AddString(string sField, string sValue, int maxLength, bool bAddBlank, bool bUpperCase)
		{
			if ( sValue == null )
				sValue = "" ;
			else
			{
				sValue = sValue.Trim() ;
				if ( bUpperCase )
					sValue = sValue.ToUpper() ;
			}

			if ( sValue != "" || bAddBlank )
				AddString(sField, sValue, maxLength) ;
		}
		public void AddNonString(string sField, string sValue)
		{
			m_oFields.Add(sField + "=" + sValue) ;
		}
		public void AddNonString(string sField, int nValue)
		{
			m_oFields.Add(sField + "=" + nValue.ToString()) ;
		}
		public void AddNonString(string sField, double dblValue)
		{
			m_oFields.Add(sField + "=" + dblValue.ToString()) ;
		}

		private SQLCommaStringBuilder m_oFields = new SQLCommaStringBuilder() ;
	}
	public class SQLWhereStringBuilder
	{
		System.Collections.Stack m_parenStack = new System.Collections.Stack() ;

		struct StackEntry
		{
			public string Sql ;
			public string Operator ;
			public StackEntry(string sSql, string sOperator)
			{
				Sql = sSql ;
				Operator = sOperator ;
			}
		}

		string m_sSQL = null ;

		public override string ToString()
		{
			// unwind the paren stack
			while (m_parenStack.Count > 0)
				CloseParen() ;

			if (null != m_sSQL)
				return " WHERE " + m_sSQL ;
			else
				return "" ;
		}
		public void Reset()
		{
			m_sSQL = null ;
		}
		public bool IsEmpty()
		{
			return null == m_sSQL ;
		}
		public void OpenANDParen()
		{
			m_parenStack.Push(new StackEntry(m_sSQL, "AND")) ;
			m_sSQL = null ;
		}
		public void OpenORParen()
		{
			m_parenStack.Push(new StackEntry(m_sSQL, "OR")) ;
			m_sSQL = null ;
		}
		public void CloseParen()
		{
			StackEntry entry = (StackEntry)m_parenStack.Pop() ;

			if (null == m_sSQL)
				m_sSQL = entry.Sql ;
			else if (null == entry.Sql)
				m_sSQL = "(" + m_sSQL + ")" ;
			else
				m_sSQL = entry.Sql + " " + entry.Operator + " (" + m_sSQL + ")" ;
		}
		public void AppendOptionalANDCondition(string sField, string sOptionalValue, string sOperator)
		{
			if (sOptionalValue != "" && sOptionalValue != "''" && sOptionalValue != "'%'")
				AppendANDCondition(sField + " " + sOperator + " " + sOptionalValue) ;
		}
		public void AppendANDCondition(string sCondition)
		{
			if (null == m_sSQL)
				m_sSQL = sCondition ;
			else
				m_sSQL += " AND " + sCondition ;
		}
		public void AppendORCondition(string sCondition)
		{
			if (null == m_sSQL)
				m_sSQL = sCondition ;
			else
				m_sSQL += " OR " + sCondition ;
		}
	}
}
