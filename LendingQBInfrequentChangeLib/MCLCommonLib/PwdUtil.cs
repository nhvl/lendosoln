using System;

namespace CommonLib
{
	public enum PWD_CHECK { OK = 0, TooShort = 1, MissingDigit = 2, MissingChar = 3 }

	/// <summary>
	/// Summary description for PwdUtil.
	/// </summary>
	public class PwdUtil
	{
		private const int MIN_LEN = 8;

		static public PWD_CHECK CheckPassword(string pwd)
		{
			if (MIN_LEN > pwd.Length) return PWD_CHECK.TooShort;
			if (!ContainsDigit(pwd)) return PWD_CHECK.MissingDigit;
			if (!ContainsChar(pwd)) return PWD_CHECK.MissingChar;
			return PWD_CHECK.OK;
		}

		static public string GetMessage(PWD_CHECK chkResult)
		{
			string message = null;
			switch (chkResult)
			{
				case PWD_CHECK.OK:
					message = "";
					break;

				case PWD_CHECK.TooShort:
					message = "Your password must be at least 8 characters long and contain at least 1 digit.";
					break;

				case PWD_CHECK.MissingDigit:
					message = "Your password must be at least 8 characters long and contain at least 1 digit.";
					break;

				case PWD_CHECK.MissingChar:
					message = "Your password must be at least 8 characters long and contain at least 1 digit.";
					break;
			}

			return message;
		}

		static private bool ContainsDigit(string pwd)
		{
			bool bHasDigit = false;
			for (int i=0; i<pwd.Length; ++i)
			{
				if (char.IsDigit(pwd, i))
				{
					bHasDigit = true;
					break;
				}
			}

			return bHasDigit;
		}

		static private bool ContainsChar(string pwd)
		{
			bool bHasChar = false;
			for (int i=0; i<pwd.Length; ++i)
			{
				if (!char.IsDigit(pwd, i))
				{
					bHasChar = true;
					break;
				}
			}

			return bHasChar;
		}



		/// <summary>
		/// Generates a pseudo-random password.
		/// </summary>
		/// <param name="length">Password length.  Must be greater than 0.</param>
		/// <param name="caseInsensitive">True if all letters must be upper-case, False to allow mixed case.</param>
		/// <param name="alphaNumericOnly">True to limit characters to numbers and letters, False to allow other punctuation.</param>
		/// <returns>A password.</returns>
		public static string GeneratePassword(int length, bool caseInsensitive, bool alphaNumericOnly)
		{
			if ( 0 >= length )
				throw new ArgumentException("The generated password must be longer than 0 characters.", "a_nLength") ;

			Random rand = new Random() ;
			System.Text.StringBuilder sbPassword = new System.Text.StringBuilder(length) ;

			for ( int idx = 0 ; idx < length ; idx++ )
			{
				int nCode = 0 ;

				if ( alphaNumericOnly )
				{
					if ( caseInsensitive )
					{
						nCode = rand.Next(0, 36) ;
						if ( nCode < 10 )
							nCode += 48 ;
						else
							nCode += 55 ;
					}
					else
					{
						nCode = rand.Next(0, 62) ;
						if ( nCode < 10 )
							nCode += 48 ;
						else if ( nCode < 36 )
							nCode += 55 ;
						else
							nCode += 61 ;
					}
				}
				else
				{
					if ( caseInsensitive )
					{
						nCode = rand.Next(33, 101) ;
						if ( 97 <= nCode )
							nCode += 26 ;
					}
					else
					{
						nCode = rand.Next(33, 127) ;
					}
				}

				sbPassword.Append( (char)nCode ) ;
			}


			return sbPassword.ToString() ;
		}

		
	}
}
