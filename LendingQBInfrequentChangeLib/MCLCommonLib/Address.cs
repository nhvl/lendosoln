using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace CommonLib
{
    [Serializable]
    public class Address
    {
        // 09/05/02-Binh-TODO: this street address is added for compatibility with LendersOffice. I probably
        // will want to tie it into the parsing module..(maybe)
        string m_sStreetAddress;
        string m_sCityStateZipcode;
        public string StreetAddress
        {
            get
            {
                if (m_sStreetAddress == null)
                    ComposeStreetAddressFromParts();
                return m_sStreetAddress;
            }
            set
            {
                m_bDirty = true;
                m_sStreetAddress = value;
            }
        }
        // 09/05/02-Binh-TODO: this street address is added for compatibility with LendersOffice. I probably
        // will want to tie it into the parsing module..(maybe)
        public string CityStateZipcode
        {
            get
            {
                if (m_sCityStateZipcode == null)
                    ComposeCityStateZipFromParts();
                return m_sCityStateZipcode;
            }
            set
            {
                m_bDirty = true;
                m_sCityStateZipcode = value;
            }
        }

        public enum STREET_DIR { blank, N, S, E, W, NW, SW, NE, SE } ;
        static public string[,] s_Street_Type =
		{ // Nathan - 10/13/2005 - OPM 3106, rearranged to most common/frequent order according to the first letter.
			// cn, 12/09/04 - rearranged in alphabetical order sets of entries (base on the first entry of each set)
			// AD -- 4/28/03; rearragned to place full spelling as first in each set of entries
			//				there is code that relies on this placement, please keep it intact.
			//				Made it static, as it should be (others should be too, but that's for another day...)
			{"AVENUE","AV"}, {"AVEN","AV"}, {"AVENU","AV"}, {"AVE","AV"}, {"AVN","AV"}, {"AVNUE","AV"}, {"AV","AV"},
			{"ALLEY","AL"}, {"ALLEE","AL"}, {"ALLY","AL"}, {"ALY","AL"}, {"AL","AL"},
			{"BOULEVARD","BV"}, {"BLVD","BV"}, {"BOULV","BV"}, {"BOUL","BV"}, {"BL","BV"}, {"BV", "BV"},
			{"COURT", "CT"}, {"CT", "CT"}, {"CRT", "CT"}, {"COURTE", "CT"}, {"CORT", "CT"}, {"CORTE", "CT"}, {"CORTES", "CT"},
			{"CIRCLE","CI"}, {"CIRC","CI"}, {"CIRCL","CI"}, {"CIR","CI"}, {"CRCL","CI"}, {"CRCLE","CI"}, {"CIRCLES","CI"}, {"CI","CI"},
			{"CREEK", "CK"}, {"CK", "CK"},{"CRK","CK"}, 
			{"CRESCENT","CR"}, {"CRECENT","CR"}, {"CRES","CR"}, {"CR","CR"}, {"CRESENT","CR"}, {"CRSCNT","CR"}, {"CRSENT","CR"}, {"CRSNT","CR"}, {"CREST","CR"}, 
			{"CORNER","CO"}, {"COR","CO"}, {"CORNERS","CO"}, {"CORS","CO"},{"CN","CO"},
			{"CENTER","CN"}, {"CENT","CN"}, {"CEN","CN"}, {"CENTR","CN"}, {"CENTRE","CN"}, {"CNTER","CN"}, {"CNTR","CN"}, {"CTR","CN"}, {"CENTERS","CN"}, {"CN", "CN"},
			{"CROSSING","CX"}, {"CRSSING","CX"}, {"CRSSNG","CX"}, {"XING","CX"}, {"CROSSROAD","CX"},{"CX","CX"},
			{"DRIVE","DR"}, {"DRIV","DR"}, {"DR","DR"}, {"DRV","DR"}, {"DRIVES","DR"},
			{"DALE", "DA"}, {"DA","DA"}, {"DLE", "DA"},
			{"EXPRESSWAY","EX"}, {"EXPR","EX"}, {"EXPRESS","EX"}, {"EXP","EX"}, {"EXPW","EX"}, {"EXPY","EX"}, {"EX","EX"},
			{"EXTENSION","ET"}, {"EXT","ET"}, {"EXTN","ET"}, {"EXTNSN","ET"}, {"EXTENSIONS","ET"}, {"EXTS","ET"}, {"ET", "ET"},
			{"FREEWAY","FY"}, {"FREEWY","FY"}, {"FRWAY","FY"}, {"FRWY","FY"}, {"FWY","FY"},{"FY","FY"},
			{"GARDEN","GA"}, {"GARDN","GA"}, {"GDN","GA"}, {"GRDEN","GA"}, {"GRDN","GA"}, {"GARDENS","GA"}, {"GDNS","GA"}, {"GRDNS","GA"},{"GA","GA"},
			{"GROVE","GR"}, {"GROV","GR"}, {"GRV","GR"}, {"GROVES","GR"},{"GR","GR"},
			{"HILL","HL"}, {"HL","HL"}, {"HILLS","HL"}, {"HLS","HL"},
			{"HEIGHTS","HT"}, {"HEIGHT","HT"}, {"HGTS","HT"}, {"HT","HT"}, {"HTS","HT"},
			{"HIGHWAY","HY"}, {"HIGHWY","HY"}, {"HIWAY","HY"}, {"HIWY","HY"}, {"HWAY","HY"}, {"HWY","HY"},{"HY","HY"},
			{"KNOLL","KN"}, {"KNOL","KN"}, {"KNL","KN"}, {"KNLS","KN"}, {"KNOLLS","KN"},{"KN","KN"},
			{"LANE","LN"}, {"LA","LN"}, {"LANES","LN"}, {"LN","LN"},
			{"LOOP","LP"}, {"LOOPS","LP"},{"LP","LP"},
			{"MALL","MA"}, {"MA","MA"},
			{"MANOR","MN"}, {"MNR","MN"}, {"MN","MN"},
			{"PARK","PK"}, {"PK","PK"}, {"PRK","PK"}, {"PARKS","PK"},
			{"PLACE","PL"}, {"PL","PL"}, {"PLC", "PL"},
			{"PARKWAY","PY"}, {"PARKWY","PY"}, {"PKWAY","PY"}, {"PKWY","PY"}, {"PKY","PY"}, {"PARKWAYS","PY"}, {"PKWYS","PY"},{"PY","PY"},
			{"PLAZA","PZ"}, {"PLZ","PZ"}, {"PLZA","PZ"},{"PZ","PZ"},
			{"PLAIN","PN"}, {"PLN","PN"}, {"PLAINES","PN"}, {"PLAINS","PN"}, {"PLNS","PN"}, {"PN", "PN"},
			{"PASS","PS"}, {"PASSAGE","PS"}, {"PS", "PS"},
			{"PATH","PA"}, {"PATHS","PA"},{"PA","PA"},
			{"PIKE", "PI"}, {"PI","PI"},
			{"POINT","PT"}, {"PT","PT"}, {"POINTS","PT"}, {"PTS","PT"}, {"PORT","PT"}, {"PRT","PT"}, {"PORTS","PT"}, {"PRTS","PT"},{"PT","PT"},
			{"ROAD","RD"}, {"RD","RD"}, {"RDS","RD"}, {"ROADS","RD"},
			{"ROUTE","RT"}, {"RT","RT"},
			{"RANCH","RC"}, {"RANCHES","RC"}, {"RNCH","RC"}, {"RNCHS","RC"},{"RN","RC"},
			{"ROW","RO"}, {"RO","RO"},
			{"RUN", "RN"}, {"RN", "RN"},
			{"STREET","ST"}, {"STRAV","ST"}, {"STRAVE","ST"}, {"STRAVEN","ST"}, {"STRAVENUE","ST"}, {"STRAVN","ST"}, {"STRVN","ST"}, {"STRVNUE","ST"}, {"ST","ST"}, {"STR","ST"}, {"STRA","ST"}, {"STRT","ST"}, {"STREETS","ST"},
			{"SQUARE","SQ"}, {"SQR","SQ"}, {"SQRE","SQ"}, {"SQU","SQ"}, {"SQ","SQ"}, {"SQRS","SQ"}, {"SQUARES","SQ"},
			{"STATION","SN"}, {"STA","SN"}, {"STATN","SN"}, {"STN","SN"}, {"SN", "SN"},
			{"STREAM","SM"}, {"STREME","SM"}, {"STRM","SM"}, {"SM", "SM"}, 
			{"TERRACE","TE"}, {"TERR","TE"}, {"TER","TE"},{"TE","TE"},
			{"TRAIL","TR"}, {"TR","TR"}, {"TRAILS","TR"}, {"TRL","TR"}, {"TRLS","TR"},
			{"TURNPIKE","TP"}, {"TPKE","TP"}, {"TRNPK","TP"}, {"TRPK","TP"}, {"TPK","TP"}, {"TURNPK","TP"},{"TP","TP"},
			{"TRACE","TC"}, {"TRACES","TC"}, {"TRCE","TC"}, {"TC", "TC"},
			{"TRACK","TK"}, {"TRACKS","TK"}, {"TRAK","TK"}, {"TRK","TK"}, {"TRKS","TK"}, {"TK", "TK"}, 
			{"TRAFFICWAY","TY"}, {"TRFY","TY"}, {"TY", "TY"},
			{"VISTA","VS"}, {"VIST","VS"}, {"VIS","VS"}, {"VST","VS"}, {"VSTA","VS"},{"VS","VS"},
			{"VIADUCT","VI"}, {"VIA","VI"}, {"VIADCT","VI"}, {"VDCT","VI"},{"VI","VI"},
			{"WAY","WY"}, {"WY","WY"}, {"WAYS","WY"},
			{"WALK","WK"}, {"WALKS","WK"},{"WK","WK"}
		};
        public string[,] s_Apt_Type =
		{
			{"APARTMENT", "APT"}, {"BASEMENT", "BSMT"}, {"BUILDING", "BLDG"}, {"DEPARTMENT", "DEPT"}, {"FLOOR", "FL"}, {"FRONT", "FRNT"}, {"HANGAR", "HNGR"}, {"LOBBY", "LBBY"}, {"LOT", "LOT"}, {"LOWER", "LOWR"}, {"OFFICE", "OFC"}, {"PENTHOUSE", "PH"}, {"PIER", "PIER"}, {"REAR", "REAR"}, {"ROOM", "RM"}, {"SIDE", "SIDE"}, {"SLIP", "SLIP"}, {"SPACE", "SPC"}, {"STOP", "STOP"}, {"SUITE", "STE"}, {"TRAILER", "TRLR"}, {"UNIT", "UNIT"}, {"UPPER", "UPPR"}
		};
        public static string[,] s_State_Type = 
		{
			{"ALABAMA","AL"},{"ALASKA","AK"},{"AMERICAN SAMOA","AS"},{"ARIZONA","AZ"},{"ARKANSAS","AR"},
			{"CALIFORNIA","CA"},{"COLORADO","CO"},{"CONNECTICUT","CT"},{"DELAWARE","DE"},
			{"DISTRICT OF COLUMBIA","DC"},{"FEDERATED STATES OF MICRONESIA","FM"},{"FLORIDA","FL"},
			{"GEORGIA","GA"},{"GUAM","GU"},{"HAWAII","HI"},{"IDAHO","ID"},{"ILLINOIS","IL"},
			{"INDIANA","IN"},{"IOWA","IA"},{"KANSAS","KS"},{"KENTUCKY","KY"},{"LOUISIANA","LA"},
			{"MAINE","ME"},{"MARSHALL ISLANDS","MH"},{"MARYLAND","MD"},{"MASSACHUSETTS","MA"},
			{"MICHIGAN","MI"},{"MINNESOTA","MN"},{"MISSISSIPPI","MS"},{"MISSOURI","MO"},
			{"MONTANA","MT"},{"NEBRASKA","NE"},{"NEVADA","NV"},{"NEW HAMPSHIRE","NH"},{"NEW JERSEY","NJ"},
			{"NEW MEXICO","NM"},{"NEW YORK","NY"},{"NORTH CAROLINA","NC"},{"NORTH DAKOTA","ND"},
			{"NORTHERN MARIANA ISLANDS","MP"},{"OHIO","OH"},{"OKLAHOMA","OK"},{"OREGON","OR"},
			{"PALAU","PW"},{"PENNSYLVANIA","PA"},{"PUERTO RICO","PR"},{"RHODE ISLAND","RI"},
			{"SOUTH CAROLINA","SC"},{"SOUTH DAKOTA","SD"},{"TENNESSEE","TN"},{"TEXAS","TX"},{"UTAH","UT"},
			{"VERMONT","VT"},{"VIRGIN ISLANDS","VI"},{"VIRGINIA","VA"},{"WASHINGTON","WA"},
			{"WEST VIRGINIA","WV"},{"WISCONSIN","WI"},{"WYOMING","WY"},{"ARMED FORCES AFRICA","AE"},
			{"ARMED FORCES AMERICAS","AA"},{"ARMED FORCES CANADA","AE"},{"ARMED FORCES EUROPE","AE"},
			{"ARMED FORCES MIDDLE EAST","AE"},{"ARMED FORCES PACIFIC","AP"}

		};
        public enum RURAL_KEYS
        {
            RT, ROUTE

        };
        public enum COLLEGE_KEYS { COLLEGE, UNIVERSITY };
        public enum HIGHWAY_KEYS { HWY, HIGHWAY };
        public enum HOTEL_KEYS { HOTEL, INN };
        public enum MILITARY_KEYS { BARRACK, UNIT, CO, COMPANY };
        public enum APT_TYPE { blank, APT, SUITE };
        public string[] PO_KEYS = { "PO BOX", "P O BOX", "POBOX", "POB", "PO", "P O", "BOX" };

        public static string LongDirection(string abbreviation)
        {
            switch (abbreviation.ToUpper())
            {
                case "N":
                    return "NORTH";
                case "S":
                    return "SOUTH";
                case "E":
                    return "EAST";
                case "W":
                    return "WEST";
                case "NE":
                    return "NORTHEAST";
                case "NW":
                    return "NORTHWEST";
                case "SE":
                    return "SOUTHEAST";
                case "SW":
                    return "SOUTHWEST";
                default:
                    return "";
            }
        }



        private enum return_type { LET, NUM, DEM, END };
        private return_type type;
        private int current_index;

        private string working_string;//either a street address or state, zip and city
        private static string LETTER_ONLY;
        private static string NUMBER_ONLY;

        private static Regex r_letter_only;
        private static Regex r_number_only;
        private static Regex scrubchars = new Regex(@"[^ \#\-A-Za-z0-9]");//clean up the string
        private static Regex space_comp = new Regex(@" +");
        private static Regex rx1 = new Regex(@"\GCOMPANY");
        private static Regex rx2 = new Regex(@"\GCO");
        private static Regex rx3 = new Regex(@"\GUNIT");
        private static Regex rx4 = new Regex(@"\G[0-9]+BARRACK");
        private static Regex rx5 = new Regex(@"\G[0-9]+ BARRACK");
        private static Regex pc = new Regex(@"\G *LOT");
        private static Regex[] boxformats = new Regex[] { new Regex(@"B*[0-9]+\-[0-9]+"), new Regex(@"B*[0-9]+"), new Regex(@"B [0-9]+"), new Regex(@"B [0-9]+\-[0-9]+") };
        private static Regex lotnumber2 = new Regex(@"\G *LOT *[0-9A-Z]*[0-9]");
        private static Regex lotnumber1 = new Regex(@"\G *LOT *[0-9]*[A-Z] ");
        private static Regex[] numformats = new Regex[] { new Regex(@"\G[A-Z]?[0-9]+[A-Z]?[ ]?[A-Z]?[0-9]+[A-Z]? "), new Regex(@"\G[0-9]+[A-Z] "), new Regex(@"\G[A-Z][0-9]+ "), new Regex(@"\G[0-9]+[\-\#][A-Z0-9]+"), new Regex(@"\G[0-9]+") };
        private static Regex zipcode0 = new Regex(@"\G *[0-9]+ *\- *[0-9]+");
        private static Regex zipcode1 = new Regex(@"\G *[0-9]+");

        private string m_stnum;
        private STREET_DIR m_stdir;
        private string m_stname;
        private string m_sttype;
        private STREET_DIR m_stPostDir;
        private string m_apttype;
        private string m_aptnum;
        private string m_city;
        private string m_state;
        private string m_zipcode;
        private string m_county;

        private bool m_bDirty;

        static Address()
        {
            LETTER_ONLY = @"\G[A-Z]+";
            NUMBER_ONLY = @"\G[0-9]+";
            r_letter_only = new Regex(LETTER_ONLY);
            r_number_only = new Regex(NUMBER_ONLY);
        }

        public Address()
        {
            current_index = 0;
            Init();
            m_bDirty = false;
        }

        private void Init()
        {
            InitStreetAddress();
            InitCityStateZip();
            InitCounty();
        }
        private void InitCityStateZip()
        {
            m_city = "";
            m_state = "";
            m_zipcode = "";
            m_bDirty = true;
        }
        private void InitStreetAddress()
        {
            m_stnum = "";
            m_stdir = STREET_DIR.blank;
            m_stPostDir = STREET_DIR.blank;
            m_stname = "";
            m_sttype = "";
            m_apttype = "";
            m_aptnum = "";
            m_bDirty = true;
        }

        private void InitCounty()
        {
            m_county = string.Empty;
            m_bDirty = true;
        }

        public bool Dirty
        {
            get { return m_bDirty; }
            set { m_bDirty = value; }
        }

        public void ParseCityStateZip(string sCityStateZip)
        {
            InitCityStateZip(); //changed by S.F. 03/31/2004

            if (sCityStateZip.Equals("") || sCityStateZip == null) return;
            current_index = 0;
            working_string = sCityStateZip;
            working_string = scrubchars.Replace(working_string, " ");
            working_string = space_comp.Replace(working_string, " ");
            working_string = working_string.ToUpper();
            working_string.Trim();
            string s;
            string A = Check_State();
            string B = Check_Zip();
            type = return_type.LET;
            while (A == null && B == null && type != return_type.END)
            {
                s = getnexttoken();
                if (m_city != null) m_city += " ";
                m_city += s;
                A = Check_State();
                B = Check_Zip();

            }

            if (type == return_type.END) return;
            if (A != null)
            {
                m_state = A;
                B = Check_Zip();

            }
            if (type == return_type.END) return;
            if (B != null)
            {
                m_zipcode = B;
            }

            m_sCityStateZipcode = sCityStateZip;
        }

        public void ParseStreetAddress(string sStreetAddress)
        {
            ParseStreetAddress(sStreetAddress, false);
        }

        public void ParseStreetAddress(string sStreetAddress, bool bDoTrace)
        {
            //* UPDATE IsEquivalentTo as needed when you update this.
            // 11/23/03; AD -- Replaced with more complicated (but better?) implementation
            AddressParser.StreetParser parser = new AddressParser.StreetParser();
            parser.SetTrace(bDoTrace);
            try
            {
                parser.Parse(sStreetAddress);
                if (bDoTrace) Tracer.Trace("CommonLib", "Address", "ParseStreetAddress", "ver", parser.Trace);
            }
            catch
            {
                if (bDoTrace) Tracer.Trace("CommonLib", "Address", "ParseStreetAddress", "ver", parser.Trace);
                throw;
            }

            InitStreetAddress();
            this.m_sStreetAddress = sStreetAddress;
            this.m_aptnum = parser.Apartment;
            this.m_stname = parser.StreetName;
            this.m_stnum = parser.StreetNumber;
            this.m_sttype = parser.StreetType;
            this.sStreetDir = parser.StreetDirection;
            this.sStPostDir = parser.StreetPostDirection;
        }

        public string StreetNumber
        {
            get
            {
                return m_stnum.Trim();
            }
            set
            {
                m_bDirty = true;
                m_stnum = value;
            }
        }

        public string sStPostDir
        {
            get
            {
                if (m_stPostDir != STREET_DIR.blank)
                {
                    return m_stPostDir.ToString();
                }
                return "";
            }
            set
            {
                m_bDirty = true;
                m_stPostDir = parseStreetDir(value);
            }
        }

        public string sStreetDir
        {
            get
            {
                if (STREET_DIR.blank != m_stdir)
                    return m_stdir.ToString();
                else
                    return "";
            }
            set
            {
                m_bDirty = true;
                m_stdir = parseStreetDir(value);
            }
        }

        private STREET_DIR parseStreetDir(string str)
        {
            switch (str.ToUpper())
            {
                case "N":
                    return STREET_DIR.N;
                case "S":
                    return STREET_DIR.S;
                case "E":
                    return STREET_DIR.E;
                case "W":
                    return STREET_DIR.W;
                case "NW":
                    return STREET_DIR.NW;
                case "SW":
                    return STREET_DIR.SW;
                case "NE":
                    return STREET_DIR.NE;
                case "SE":
                    return STREET_DIR.SE;
                default:
                    return STREET_DIR.blank;
            }
        }

        public STREET_DIR StreetPostDir
        {
            get
            {
                return m_stPostDir;
            }
            set
            {
                m_bDirty = true;
                m_stPostDir = value;
            }
        }

        public STREET_DIR StreetDir
        {
            get
            {
                return m_stdir;
            }
            set
            {
                m_bDirty = true;
                m_stdir = value;
            }
        }
        public string StreetName
        {
            get
            {
                return m_stname.Trim();
            }
            set
            {
                m_bDirty = true;
                m_stname = value;
            }
        }
        public string StreetType
        {
            get
            {
                return m_sttype.Trim();
            }
            set
            {
                m_bDirty = true;
                m_sttype = value;
            }
        }
        public string AptNum
        {
            get
            {
                return m_aptnum.Trim();
            }
            set
            {
                m_bDirty = true;
                m_aptnum = value;
            }
        }

        public string City
        {
            get
            {
                return m_city.Trim();
            }
            set
            {
                m_bDirty = true;
                m_city = value;
            }
        }
        public string State
        {
            get
            {
                return m_state.Trim();
            }
            set
            {
                m_bDirty = true;
                m_state = value;
            }
        }
        public string Zipcode
        {
            get
            {
                return m_zipcode.Trim();
            }
            set
            {
                m_bDirty = true;
                if (value != null)
                {
                    m_zipcode = value.Trim();
                }
                else
                {
                    m_zipcode = "";
                }
            }
        }

        public string County
        {
            get
            {
                return this.m_county.Trim();
            }

            set
            {
                this.m_bDirty = true;
                this.m_county = value == null ? string.Empty : value.Trim();
            }
        }

        /// <summary>
        /// A dictionary mapping ML street types to USPS street types.
        /// </summary>
        private static Dictionary<string, string> uspsStreetTypesByMLStreetTypes = new Dictionary<string, string>()
        {
            ["AV"] = "AVE", // AVENUE
            ["AL"] = "ALY", // ALLEY
            ["BV"] = "BLVD", // BOULEVARD
            ["CI"] = "CIR", // CIRCLE
            ["CK"] = "CRK", // CREEK
            ["CN"] = "CTR", // CENTER
            ["CO"] = "COR", // CORNER
            ["CR"] = "CRES", // CRESCENT
            ["CT"] = "CT", // COURT
            ["CX"] = "XING", // CROSSING
            ["DR"] = "DR", // DRIVE
            ["DA"] = "DL", // DALE
            ["EX"] = "EXPY", // EXPRESSWAY
            ["ET"] = "EXT", // EXTENSION
            ["FY"] = "FWY", // FREEWAY
            ["GA"] = "GDN", // GARDEN
            ["GR"] = "GRV", // GROVE
            ["HL"] = "HL", // HILL
            ["HT"] = "HTS", // HEIGHTS
            ["HY"] = "HWY", // HIGHWAY
            ["KN"] = "KNL", // KNOLL
            ["LN"] = "LN", // LANE
            ["LP"] = "LOOP", // LOOP
            ["MA"] = "MALL", // MALL
            ["MN"] = "MNR", // MANOR
            ["PS"] = "PASS", // PASS
            ["PA"] = "PATH", // PATH
            ["PI"] = "PIKE", // PIKE
            ["PK"] = "PARK", // PARK
            ["PY"] = "PKWY", // PARKWAY
            ["PL"] = "PL", // PLACE
            ["PN"] = "PLN", // PLAIN
            ["PZ"] = "PLZ", // PLAZA
            ["PT"] = "PT", // POINT
            ["RD"] = "RD", // ROAD
            ["RN"] = "RUN", // RUN
            ["RC"] = "RNCH", // RANCH
            ["RO"] = "ROW", // ROW
            ["RT"] = "RTE", // ROUTE
            ["SQ"] = "SQ", // SQUARE
            ["SN"] = "STA", // STATION
            ["ST"] = "ST", // STREET
            ["SM"] = "STRM", // STREAM
            ["TE"] = "TER", // TERRACE
            ["TP"] = "TPKE", // TURNPIKE
            ["TC"] = "TRCE", // TRACE
            ["TK"] = "TRAK", // TRACK
            ["TY"] = "TRFY", // TRAFFICWAY
            ["TR"] = "TRL", // TRAIL
            ["VI"] = "VIA", // VIADUCT
            ["VS"] = "VIS", // VISTA
            ["WK"] = "WALK", // WALK
            ["WY"] = "WAY", // WAY
        };

        /// <summary>
        /// Gets a dictionary mapping ML street types to USPS street types.
        /// </summary>
        public static Dictionary<string, string> UspsStreetTypesByMLStreetTypes
        {
            get
            {
                return uspsStreetTypesByMLStreetTypes;
            }
        }

        /// <summary>
        /// Convert MeridianLink street type to USPS standard street type
        /// </summary>
        public static string USPSStreetTypeFromMLStreetType(string mlStreetType)
        {
            if (mlStreetType == null || mlStreetType.Length == 0)
            {
                return null;
            }

            if (uspsStreetTypesByMLStreetTypes.ContainsKey(mlStreetType))
            {
                return uspsStreetTypesByMLStreetTypes[mlStreetType];
            }
            
            // should never get here
            return mlStreetType;
        }

        //---------------------------------begin private function------------------------------------
        #region Hateful things
        private string nexttoken()
        {
            if (working_string.Length < current_index + 1)
            {
                type = return_type.END;
                return "";
            }

            Match thismatch = r_letter_only.Match(working_string, current_index);


            if (thismatch.Success)
            {
                type = return_type.LET;
                current_index += thismatch.ToString().Length;
                return thismatch.ToString();

            }
            thismatch = r_number_only.Match(working_string, current_index);
            if (thismatch.Success)
            {
                type = return_type.NUM;
                current_index += thismatch.ToString().Length;
                return thismatch.ToString();
            }
            type = return_type.DEM;
            current_index++;
            return "";
        }

        private string getnexttoken()
        {
            string current_token;
            current_token = nexttoken();
            while (type == return_type.DEM)
            {

                current_token = nexttoken();
            }
            return current_token;

        }


        private bool Check_Rural(string Address)
        {
            for (RURAL_KEYS rk = RURAL_KEYS.RT; rk <= RURAL_KEYS.ROUTE; rk++)
            {
                Regex r = new Regex(@"\G" + rk.ToString());
                if (r.Match(Address).Success) return true;
            }
            return false;
        }

        private bool Check_College(string Address)
        {
            for (COLLEGE_KEYS ck = COLLEGE_KEYS.COLLEGE; ck <= COLLEGE_KEYS.UNIVERSITY; ck++)
            {
                Regex r = new Regex(ck.ToString());
                if (r.Match(Address).Success)
                {
                    return true;
                }


            }
            return false;
        }


        private bool Check_Highway(string Address)
        {
            for (HIGHWAY_KEYS ck = HIGHWAY_KEYS.HWY; ck <= HIGHWAY_KEYS.HIGHWAY; ck++)
            {
                Regex r = new Regex(ck.ToString());
                if (r.Match(Address).Success)
                {
                    return true;
                }


            }
            return false;
        }

        private bool Check_Hotel(string Address)
        {
            for (HOTEL_KEYS ck = HOTEL_KEYS.HOTEL; ck < HOTEL_KEYS.INN; ck++)
            {
                Regex r = new Regex(ck.ToString());
                if (r.Match(Address).Success)
                {
                    return true;
                }


            }
            return false;
        }

        private bool Check_Military(string Address)
        {
            //Regex rx1 = new Regex (@"\GCOMPANY");
            //Regex rx2 = new Regex (@"\GCO");
            //Regex rx3 = new Regex (@"\GUNIT");
            //Regex rx4 = new Regex (@"\G[0-9]+BARRACK");
            //Regex rx5 = new Regex (@"\G[0-9]+ BARRACK");

            if (rx1.Match(Address).Success == true || rx2.Match(Address).Success == true || rx3.Match(Address).Success == true || rx4.Match(Address).Success == true || rx5.Match(Address).Success == true)
            {
                return true;

            }
            return false;

        }

        private bool Check_PO(string address)
        {

            for (int i = 0; i < PO_KEYS.Length; ++i)
            {
                if (address.StartsWith(PO_KEYS[i]))
                {
                    return true;
                }
            }
            return false;


        }

        private bool Check_Parking(string address)
        {
            //Regex pc= new Regex(@"\G *LOT");
            if (pc.Match(address).Success) return true;
            return false;
        }

        private void Deal_Rural()
        {
            Console.WriteLine("This is a Rural Address!!");
            string s;
            m_stname = getnexttoken();
            if (type == return_type.END)
            {
                return;
            }
            m_stname += " " + getnexttoken();
            s = getnexttoken();
            if (type == return_type.END)
            {
                m_stnum = m_stname;
                m_stname = "";
                return;
            }
            if (type == return_type.LET)
            {

                m_stnum = s;
                while (type != return_type.END)
                {
                    m_stnum += " " + getnexttoken();
                }

            }

            return;




        }


        private void Deal_Highway()
        {
            // 09/24/2003-Check to see if the input doesn't have a street number.
            // The "HWY 215" from EFX on MERCHANT #113931 caused an infinite loop here.
            string s;
            s = getnexttoken();

            if (!Check_Highway(s))
            {
                m_stnum = s;
                s = getnexttoken();
                while (!Check_Highway(s))
                {
                    m_stnum += " " + s;
                    s = getnexttoken();
                }
            }

            m_stname = s;
            s = getnexttoken();
            while (type != return_type.END)
            {
                m_stname += " " + s;
                s = getnexttoken();
            }

            return;

        }

        private void Deal_Hotel()
        {

        }

        private void Deal_Military()
        {
            m_stname = working_string;
            return;

        }

        private void Deal_PO()
        {
            m_stname = "PO";
            //Regex [] boxformats = new Regex[4];
            //boxformats[0] = new Regex( @"B*[0-9]+\-[0-9]+");
            //boxformats[1] = new Regex(@"B*[0-9]+");
            //boxformats[2] = new Regex(@"B [0-9]+");
            //boxformats[3] = new Regex(@"B [0-9]+\-[0-9]+");

            for (int i = 0; i < 4; ++i)
            {
                Match m = boxformats[i].Match(working_string);
                if (m.Success)
                {
                    if (!m.ToString().StartsWith("B")) m_stnum = "BOX";
                    m_stnum += m.ToString();
                    return;

                }

            }

            Match number = r_number_only.Match(working_string);

            if (number.Success)
            {
                m_stnum = "BOX" + number.ToString();

            }
            return;


        }

        private void Deal_parking()
        {
            string s;

            //Regex lotnumber2 = new Regex(@"\G *LOT *[0-9A-Z]*[0-9]");
            //Regex lotnumber1 = new Regex(@"\G *LOT *[0-9]*[A-Z] ");
            Match mat1 = lotnumber1.Match(working_string);
            Match mat2 = lotnumber2.Match(working_string);
            if (mat1.Success)
            {
                m_stnum = mat1.ToString();
            }
            else
            {
                if (mat2.Success) m_stnum = mat2.ToString();
            }
            current_index += m_stnum.Length;

            s = getnexttoken();
            m_stname = s;
            while (type != return_type.END)
            {
                s = getnexttoken();
                m_stname += " " + s;

            }
            return;


        }

        private void Deal_Street()
        {
            //Regex [] numformats = new Regex [5];
            //numformats[0]= new Regex(@"\G[A-Z]?[0-9]+[A-Z]?[ ]?[A-Z]?[0-9]+[A-Z]? ");
            //numformats[1]= new Regex(@"\G[0-9]+[A-Z] ");
            //numformats[2]= new Regex(@"\G[A-Z][0-9]+ ");
            //numformats[3]= new Regex(@"\G[0-9]+[\-\#][A-Z0-9]+");
            //numformats[4]= new Regex(@"\G[0-9]+");

            string s, s2; //, s3 ;
            bool flag = false;
            int n, n2;

            for (int i = 0; i < 5; ++i)
            {
                Match m = numformats[i].Match(working_string);
                if (m.Success)
                {
                    if (i < 2)
                    {
                        s = working_string;
                        n = current_index;

                        current_index += m.ToString().Length;
                        s2 = getnexttoken();
                        n2 = SafeConvert.ToInt(SafeString.Right(m.ToString().Trim(), 1));

                        if (!(n2 == 1 && s2 == "ST") &&
                            !(n2 == 2 && s2 == "ND") &&
                            !(n2 == 3 && s2 == "RD") &&
                            !((n2 == 0 || n2 >= 4) && s2 == "TH"))
                        {
                            m_stnum = m.ToString();
                            current_index = n + m.ToString().Length;
                            break;
                        }
                        else
                        {
                            working_string = s;
                            current_index = n;
                        }
                    }
                    else
                    {
                        m_stnum = m.ToString();
                        current_index += m.ToString().Length;
                        break;
                    }
                }
            }

            s = getnexttoken();

            if (check_stdir(s) != STREET_DIR.blank)
            {
                m_stdir = check_stdir(s);
                s = getnexttoken();
                if (check_sttype(s) != null)
                {
                    m_stname = m_stdir.ToString();
                    m_stdir = STREET_DIR.blank;
                    flag = true;
                }
            }
            if (flag == false) m_stname = s;
            if (type == return_type.NUM && (s.EndsWith("3") || s.EndsWith("1")))
            {
                string s1 = getnexttoken();
                if ((s1.Equals("RD") && s.EndsWith("3")) || (s1.Equals("ST") && s.EndsWith("1")))
                {
                    m_stname += s1;
                    s = getnexttoken();
                }
            }
            else
            {
                if (s.Equals("ROUTE") || s.Equals("HWY"))
                {
                    s = getnexttoken();
                    m_stname += " " + s;
                }
                if (flag == false) s = getnexttoken();
            }

            //s=getnexttoken();

            while (type != return_type.END && type != return_type.NUM && check_apt(s) == null && check_sttype(s) == null)
            {
                m_stname += " " + s;
                s = getnexttoken();

            }
            if (type == return_type.END) return;

            if (type == return_type.NUM)
            {
                m_aptnum = s;
                s = getnexttoken();
                while (type != return_type.END)
                {
                    if (check_sttype(s) != null)
                    {
                        m_stname += " " + m_aptnum;
                        // 04/10/03-Lawrence-Don't do this!  Use an empty string instead, or it blows up when we trim it!
                        // m_aptnum=null;
                        m_aptnum = "";
                        break;
                    }
                    else
                    {
                        m_aptnum += " " + s;
                        s = getnexttoken();
                    }
                }
            }

            if (check_apt(s) != null)
            {
                m_apttype = check_apt(s);
                s = getnexttoken();
                while (type != return_type.END)
                {
                    m_aptnum += s;
                    s = getnexttoken();
                }
            }

            if (check_sttype(s) != null)
            {
                string s1 = s; //added by cn, 03/25/03

                m_sttype = check_sttype(s);
                s = getnexttoken();

                //----------------------------
                //added by cn, 03/25/03
                //----------------------------
                while (check_sttype(s) != null)
                {
                    m_stname += " " + s1;
                    s1 = s;
                    m_sttype = check_sttype(s);
                    s = getnexttoken();
                }
                //----------------------------

                if (check_apt(s) != null)
                {
                    m_apttype = check_apt(s);
                    s = getnexttoken();
                }

                while (type != return_type.END)
                {
                    m_aptnum += s;
                    s = getnexttoken();
                }
            }

            return;
        }

        private string check_apt(string s)
        {
            for (int i = 0; i < s_Apt_Type.Length / 2; ++i)
            {
                Regex rg1 = new Regex(@"\G *" + s_Apt_Type[i, 0] + "([^A-Z]|$)");
                Regex rg2 = new Regex(@"\G *" + s_Apt_Type[i, 1] + "([^A-Z]|$)");

                Match mg1 = rg1.Match(s);
                Match mg2 = rg2.Match(s);

                if (mg1.Success || mg2.Success)
                {
                    //current_index += mg1.ToString().Length-1;
                    return s_Apt_Type[i, 1];
                }

            }
            return null;
        }




        private string check_sttype(string s)
        {
            for (int i = 0; i < s_Street_Type.Length / 2; ++i)
            {
                Regex rg1 = new Regex(@"\G *" + s_Street_Type[i, 0] + "([^A-Z]|$)");
                Match mg1 = rg1.Match(s);


                if (mg1.Success)
                {

                    return s_Street_Type[i, 1];
                }

            }
            return null;



        }

        private STREET_DIR check_stdir(string s)
        {
            for (STREET_DIR sd = STREET_DIR.N; sd <= STREET_DIR.SE; sd++)
            {
                Regex sttypes = new Regex(@"\G" + sd.ToString() + "$");
                if (sttypes.Match(s).Success) return sd;
            }

            return STREET_DIR.blank;
        }


        private string Check_State()
        {
            //string rr=s_State_Type[0,0];
            //string vr=s_State_Type[0,1];
            for (int i = 0; i < s_State_Type.Length / 2; ++i)
            {
                Regex rg1 = new Regex(@"\G *" + s_State_Type[i, 0] + " *([0-9]|$)");
                Regex rg2 = new Regex(@"\G *" + s_State_Type[i, 1] + " *([0-9]|$)");
                //Regex rg3 = new Regex (@"\G *" + s_State_Type[i,1] + "([^A-Z]|$)");

                Match mg1 = rg1.Match(working_string, current_index);
                Match mg2 = rg2.Match(working_string, current_index);

                if (mg1.Success)
                {
                    current_index += mg1.ToString().Length - 1;
                    return s_State_Type[i, 1];
                }

                if (mg2.Success)
                {
                    current_index += mg2.ToString().Length - 1;
                    return s_State_Type[i, 1];
                }
            }
            return null;

        }



        private string Check_Zip()
        {
            //address.Trim();
            //Regex zipcode0 = new Regex (@"\G *[0-9]+ *\- *[0-9]+");
            //Regex zipcode1 = new Regex(@"\G *[0-9]+");
            Match zip0 = zipcode0.Match(working_string, current_index);
            Match zip1 = zipcode1.Match(working_string, current_index);
            if (zip0.Success)
            {
                return zip0.ToString();

            }
            if (zip1.Success)
            {
                return zip1.ToString();
            }

            return null;

        }
        private void ClearStreetAddress()
        {
            m_sStreetAddress = null;
        }

        private void ClearCityStateZip()
        {
            m_sCityStateZipcode = null;
        }

        private void ComposeStreetAddressFromParts()
        {
            m_sStreetAddress = "";
            if (m_stnum.Length > 0)
                m_sStreetAddress = m_stnum;
            if (STREET_DIR.blank != m_stdir)
                m_sStreetAddress = AddPart(m_sStreetAddress, m_stdir.ToString());
            m_sStreetAddress = AddPart(m_sStreetAddress, m_stname);
            m_sStreetAddress = AddPart(m_sStreetAddress, m_sttype);
            if (m_stPostDir != STREET_DIR.blank) m_sStreetAddress = AddPart(m_sStreetAddress, m_stPostDir.ToString());
            m_sStreetAddress = AddPart(m_sStreetAddress, m_apttype);
            m_sStreetAddress = AddPart(m_sStreetAddress, m_aptnum);
        }
        private void ComposeCityStateZipFromParts()
        {
            m_sCityStateZipcode = "";
            m_sCityStateZipcode = AddPart(m_sCityStateZipcode, m_city);
            m_sCityStateZipcode = AddPart(m_sCityStateZipcode, m_state);
            m_sCityStateZipcode = AddPart(m_sCityStateZipcode, m_zipcode);
        }
        private string AddPart(string s, string sPart)
        {
            if (null == sPart || sPart.Length == 0)
                return s;
            else if (s.Length == 0)
                return sPart;
            else
                return s + ' ' + sPart;
        }

        /// <summary>
        /// Converts text street type to MCL street type abbreviation
        /// </summary>
        /// <param name="text">Any text description of street type</param>
        /// <returns>MCL abbreviation</returns>
        /// <example>Text_2_StreetType("Walk") = "WK"</example>
        public static string Text_2_StreetType(string text)
        {
            if ((null == text) && (2 > text.Length))
                return "";

            string sType = text.Trim().ToUpper();
            for (int i = 0; i < s_Street_Type.GetLength(0); ++i)
            {
                if (s_Street_Type[i, 0] == sType)
                    return s_Street_Type[i, 1];
            }

            return "";
        }
        #endregion

        public static bool CheckState(string state)
        {
            for (int i = 0; i < s_State_Type.Length / 2; ++i)
            {
                if (state == s_State_Type[i, 1]) return true;
            }
            return false;
        }
        /// <summary>
        /// Converts a street type abbreviation into a text description
        /// </summary>
        /// <param name="code">MCL street type abbreviation</param>
        /// <returns>A street type, as text</returns>
        /// <example>StreetType_2_Text("WK") = "WALK"</example>
        public static string StreetType_2_Text(string code)
        {
            if (code == null || code.Length != 2)
                return "";

            for (int idx = 0; idx < s_Street_Type.GetLength(0); idx++)
            {
                if (s_Street_Type[idx, 1] == code)
                    return s_Street_Type[idx, 0];
            }

            return "";
        }

        public static string CombinedAddress(string sStreet, string sCity, string sState, string sZip)
        {
            sStreet = SafeConvert.ToString(sStreet);
            sCity = SafeConvert.ToString(sCity);
            sState = SafeConvert.ToString(sState);
            sZip = SafeConvert.ToString(sZip);

            if (sState != "" && sZip != "")
                sState = sState + " " + sZip;
            else
                sState = sState + sZip;

            string ret = sStreet;
            if (sCity != "") ret += (ret == "" ? "" : ", ") + sCity;
            if (sState != "") ret += (ret == "" ? "" : ", ") + sState;
            return (ret);
        }
        public static string CombinedAddress(string sCity, string sState, string sZip)
        {
            sCity = SafeConvert.ToString(sCity);
            sState = SafeConvert.ToString(sState);
            sZip = SafeConvert.ToString(sZip);

            if (sState != "" && sZip != "")
                sState = sState + " " + sZip;
            else
                sState = sState + sZip;

            if (sCity != "" && sState != "")
                return (sCity + ", " + sState);
            else
                return (sCity + sState);
        }

        /// <summary>
        /// Forms a new string composed of the first five digits in the input string.
        /// Based on the credit card AVS check.
        /// </summary>
        /// <param name="sInput">A street address</param>
        /// <returns>The digits in that address - or "" on null input.</returns>
        public static string AVSString(string sInput)
        {
            return AVSString(sInput, 5);
        }

        /// <summary>
        /// Forms a new string composed of the first few digits in the input string.
        /// Based on the credit card AVS check.
        /// </summary>
        /// <param name="sInput">A street address</param>
        /// <param name="nCodeLength">Maximum length of the AVS code.  Traditional credit card applications use 5 characters.</param>
        /// <returns>The digits in that address - or "" on null input.</returns>
        public static string AVSString(string sInput, int nCodeLength)
        {
            if (sInput == null)
                return "";

            if (nCodeLength < 1)
                throw new ArgumentOutOfRangeException("nCodeLength", nCodeLength, "Code length may not be less than 1.");

            System.Text.StringBuilder builder = new System.Text.StringBuilder(nCodeLength);

            for (int idx = 0; idx < sInput.Length; idx++)
            {
                if (sInput[idx] >= '0' && sInput[idx] <= '9')
                    builder.Append(sInput[idx]);
                if (builder.Length == nCodeLength)
                    return builder.ToString();
            }

            return builder.ToString();
        }

        /// <summary>
        /// Returns if two addresses are equivalent.
        /// Both addresses should have been obtained from ParseAddress(string)
        /// </summary>
        private bool IsEquivalentTo(Address parsedAddress2)
        {
            return IsEquivalentTo(this, parsedAddress2);
        }

        private static bool IsEquivalentTo(Address parsedAddress1, Address parsedAddress2)
        {
            //* UPDATE from ParseStreetAddress if that changes.
            if (null == parsedAddress1)
            {
                if (null == parsedAddress2)
                {
                    return true;
                }
                return false;
            }
            if (null == parsedAddress2)
            {
                return false;
            }
            
            return (
                        string.Compare(parsedAddress1.AptNum, parsedAddress2.AptNum, true) == 0
                    && string.Compare(parsedAddress1.StreetName, parsedAddress2.StreetName, true) == 0
                    && string.Compare(parsedAddress1.StreetNumber, parsedAddress2.StreetNumber, true) == 0
                    && string.Compare(parsedAddress1.StreetType, parsedAddress2.StreetType, true) == 0
                    && string.Compare(parsedAddress1.sStreetDir, parsedAddress2.sStreetDir, true) == 0
                    && string.Compare(parsedAddress1.sStPostDir, parsedAddress2.sStPostDir, true) == 0
                    );
        }

        /// <summary>
        /// Returns if two addresses strings are equivalent.  
        /// If it can't parse them both, it returns false, unless they are the same string (ignoring case)
        /// </summary>
        public static bool AreEquivalent(string addr1String, string addr2String)
        {
            return AreEquivalentByComparator(addr1String, addr2String, IsEquivalentTo);
        }

        /// <summary>
        /// Returns if two addresses strings are equivalent by the address comparator provided.
        /// If it can't parse them both, it returns false, unless they are the same string (ignoring case)
        /// </summary>
        public static bool AreEquivalentByComparator(string addr1String, string addr2String, Func<Address, Address, bool> areEquivalent)
        {
            addr1String = addr1String.Trim().ToLowerInvariant();
            addr2String = addr2String.Trim().ToLowerInvariant();

            if (addr1String == addr2String)
            {
                return true;
            }

            if (string.IsNullOrEmpty(addr1String))
            {
                if (string.IsNullOrEmpty(addr2String))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                if (string.IsNullOrEmpty(addr2String))
                {
                    return false;
                }
            }

            // at this point, neither address is null or empty.

            bool parsedAddr1 = false;
            bool parsedAddr2 = false;
            var addr1 = new CommonLib.Address();
            try
            {
                addr1.ParseStreetAddress(addr1String);
                parsedAddr1 = true;
            }
            catch (Exception)
            {
                // didn't parse it, parsedAddr1 is false.
            }


            var addr2 = new CommonLib.Address();
            try
            {
                addr2.ParseStreetAddress(addr2String);
                parsedAddr2 = true;
            }
            catch (Exception)
            {
                // didn't parse it, parsedAddr1 is false.
            }

            if (parsedAddr1 ^ parsedAddr2) // xor
            {
                return false;
            }
            else if (parsedAddr1 && parsedAddr2)
            {
                //return addr1.IsEquivalentTo(addr2);
                return areEquivalent(addr1, addr2);
            }
            else // if (!parsedAddr1 && !parsedAddr2)
            {
                // Per DW: if we can't parse either, return false.
                return false;
            }
        }

        
    }
}
