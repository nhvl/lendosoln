using System;

namespace CommonLib
{
	public class Timer
	{
		private System.DateTime m_start;
		private System.DateTime m_end;
		private int m_nMinDuration ;

		public Timer()
		{
			m_nMinDuration = 0 ;
		}
		public Timer(int nMinDuration)
		{
			m_nMinDuration = nMinDuration ;
		}
		private void reset()
		{
			m_start = System.DateTime.Now ;
			m_end = System.DateTime.Now ;
		}
		public void start() 
		{
			m_start = System.DateTime.Now ;
		}
		public void stop() 
		{
			m_end = System.DateTime.Now ;
		}
		public void display(string method) 
		{
			long duration = ( long )( ( TimeSpan )( m_end - m_start ) ).TotalMilliseconds ;
			if (duration >= m_nMinDuration)
				Tracer.Trace(string.Format(@"Timing: {0} ms.", duration), "VER", method);
		}
		public override string ToString()
		{
			long duration = ( long )( ( TimeSpan )( m_end - m_start ) ).TotalMilliseconds ;
			return String.Format("{0}\t{1}", m_start.ToString("yyyy-MM-ddThh:mm:ss.fff"), duration) ;
		}
	}
}
