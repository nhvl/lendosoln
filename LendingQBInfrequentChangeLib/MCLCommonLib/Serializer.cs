using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Formatters.Soap;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace CommonLib
{
	/// <summary>
	/// Utility for serialization.
	/// </summary>
	public class Serializer
	{
		/// <summary>
		/// Stream an object into SOAP format.
		/// </summary>
		/// <param name="obj">Object to stream.</param>
		/// <returns>A SOAP XML string encoding object's data.</returns>
		static public string ToSoap(object obj)
		{
			if (null == obj) return null;

			MemoryStream stream = new MemoryStream();
			SoapFormatter ser = new SoapFormatter();
			ser.Serialize(stream, obj);
			UTF8Encoding enc = new UTF8Encoding();
			//ASCIIEncoding enc = new ASCIIEncoding();
			string encoded = enc.GetString(stream.GetBuffer());
			int nIndex = encoded.LastIndexOf('>');
			encoded = encoded.Substring(0, nIndex+1);
			//return SafeString.NormalizeUnicodeString(encoded);
			return encoded;
		}

		/// <summary>
		/// Take a SOAP XML string and restore an object from it.  Can handle
		/// strings of format <![CDATA[ soap xml here ]]>.
		/// </summary>
		/// <param name="state">The soap xml string.</param>
		/// <returns>The instantiated object.</returns>
		static public object FromSoap(string state)
		{
			if ((null == state) || ("" == state)) return null;

			string sUnformat = UnFormat(state);
			if ("" == sUnformat) return null;

			//ASCIIEncoding enc = new ASCIIEncoding();
			UTF8Encoding enc = new UTF8Encoding();
			MemoryStream stream = new MemoryStream(enc.GetBytes(sUnformat));
			SoapFormatter ser = new SoapFormatter();
			return ser.Deserialize(stream);
		}

		/// <summary>
		/// Stream an object into XML format.
		/// NOTE : NOT TESTED YET
		/// </summary>
		/// <param name="obj">Object to stream.</param>
		/// <returns>An XML string encoding object's data.</returns>
		static public string ToXml(object obj)
		{
			if (null == obj) return null;

			MemoryStream stream = new MemoryStream();
			XmlSerializer ser = new XmlSerializer(obj.GetType());
			ser.Serialize(stream, obj);
			ASCIIEncoding enc = new ASCIIEncoding();
			string encoded = enc.GetString(stream.GetBuffer());
			int nIndex = encoded.LastIndexOf('>');
			encoded = encoded.Substring(0, nIndex+1);
			return encoded;
		}

		/// <summary>
		/// Take an XML string and restore an object from it.  Can handle
		/// strings of format <![CDATA[ xml here ]]>.
		/// NOTE : NOT TESTED YET
		/// </summary>
		/// <param name="state">The xml string.</param>
		/// <returns>The instantiated object.</returns>
		static public object FromXml(string state, Type type)
		{
			if ((null == state) || ("" == state)) return null;

			string sUnformat = UnFormat(state);
			if ("" == sUnformat) return null;

			ASCIIEncoding enc = new ASCIIEncoding();
			MemoryStream stream = new MemoryStream(enc.GetBytes(sUnformat));
			XmlSerializer ser = new XmlSerializer(type);
			return ser.Deserialize(stream);
		}

		static private string UnFormat(string formatted)
		{
			if ((null == formatted) || ("" == formatted)) return "";

			if (formatted.Length > 9 && formatted.Substring(0, 9) != "<![CDATA[") return formatted;

			XmlDocument doc = new XmlDocument();
			doc.LoadXml("<PRODUCT/>");
			doc.DocumentElement.InnerXml = formatted;

			XmlCDataSection cdata = null;
			try
			{
				cdata = (XmlCDataSection)doc.DocumentElement.FirstChild;
			}
			catch{}

			string data = (null == cdata) ? formatted : cdata.Data;
			return data;
		}
	}
}
