using System;

namespace CommonLib
{
	// todo: this needs to be modified to match the MCLDEV project
	public enum CREDITDATA_LOCK_TYPE { NoLock = 1, SystemLock = 2, EditLock = 3, Hidden = 4 } ;

	public enum FANNIE_STATUS
	{
		None = 0 ,
		Submitted = 1,
		NonFannie = 2,
		Unsolicited = 3,
		Reissue = 4,
		Freddie = 5,
		Reprint = 6,
		Upgrade = 7,
		SubmittedLocalData = 8
	} ;

	// 04/19/03-Binh-obsolete
	//public enum IDENTITY_SEARCH_STATUS { New = 0, Completed = 1, Error = 2 } ;
}
