using System;

namespace CommonLib
{
	/// <summary>
	/// Created by cn, 01/04/2007
	/// To validate Social Security Numbers using the rules from SSA website (http://www.ssa.gov)
	/// Initial implementation: mark the following SSNs as invalid:
	///		000 xx xxxx
	///		666 xx xxxx
	///		8xx xx xxxx
	///		9xx xx xxxx
	///		xxx 00 xxxx
	///		xxx xx 0000
	///		111 11 1111
	///		333 33 3333
	///		123 45 6789	
	/// </summary>
	public class SSNValidator
	{
		// An invalid SSN is one which has not yet been assigned
		public static bool Validate(string sSSN)
		{
			if (sSSN == "111111111" || sSSN == "333333333" || sSSN == "123456789" ||
				sSSN.StartsWith("000") || sSSN.StartsWith("666") || sSSN.StartsWith("8") || sSSN.StartsWith("9") ||
				sSSN.EndsWith("0000") || sSSN.Substring(3, 2) == "00")
				return false;
			else
				return true;	
		}
	}
}
