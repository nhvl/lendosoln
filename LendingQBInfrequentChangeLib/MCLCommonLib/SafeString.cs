using System;
using System.Text.RegularExpressions;

namespace CommonLib
{
	/// <summary>
	/// String Utilities class.
	/// </summary>
	public class SafeString
	{
        public const string EMAIL_REGEX_PATTERN = @"^[A-Za-z0-9&._%+-]+@[A-Za-z0-9&.-]+\.[A-Za-z]{2,}$"; // No Access to ConstApp from here. So just copy/paste actual string.
		private static Regex s_rgEmail = new Regex(EMAIL_REGEX_PATTERN);
		private static Regex s_rgUncamel1 = new Regex(@"([A-Z]+)([A-Z])(?=[a-z])");
		private static Regex s_rgUncamel2 = new Regex(@"([a-z])([A-Z])");
		private static Regex s_rgUncamelTRANS = new Regex(@"(TRANS)\s(\S)");
		private static Regex s_rgPhone = new Regex(@"^((?<CountryCode>1)?(?<AreaCode>[2-9]\d{2}))?(?<Exchange>[2-9]\d{2})(?<Local>\d{4})$");

		public SafeString()
		{
		}

		public static bool IsValidEmail(string inString)
		{
			if (!SafeString.HasValue(inString)) return false;
			return s_rgEmail.IsMatch(inString);
		}

		public static string Left(string s, int nLen)
		{
			if ( s == null || nLen <= 0 )
				return String.Empty ;

			if (s.Length <= nLen)
				return s ;
			else
				return s.Substring(0, nLen) ;
		}
		public static string Right(string s, int nLen)
		{
			if ( s == null || nLen <= 0 )
				return String.Empty ;

			if (s.Length <= nLen)
				return s ;
			else
				return s.Substring(s.Length - nLen, nLen) ;
		}
		public static char Index(string s, int nPos)
		{
			if (s == null || nPos < 0 || nPos >= s.Length)
				return ' ' ;
			else
				return s[nPos] ;
		}
		/// <REVISION date="10/23/02" author="Lawrence">
		/// Revised to accomodate the fact that C# uses zero-based arrays.
		/// </REVISION>
		public static string Mid(string s, int nStart)
		{
			if (s == null || nStart >= s.Length)
				return "" ;
			else
				return s.Substring(nStart) ;
		}
		/// <REVISION date="10/23/02" author="Lawrence">
		/// Revised to accomodate (a) zero-based arrays and (b) calls with too great of a length.
		/// </REVISION>
		public static string Mid(string s, int nStart, int nLen)
		{
			if (s == null || nStart >= s.Length)
				return "" ;
			else
				if ( nLen > (s.Length - nStart) )
					return s.Substring(nStart) ;
				else
					return s.Substring(nStart, nLen) ;
		}

		/// <summary>
		/// Removes all whitespace from a string.
		/// </summary>
		/// <param name="s">Source string.</param>
		/// <returns>The input string, with all whitespace removed.  Returns "" on null input.</returns>
		public static string StripWhitespace(string s)
		{
			if ( s == null )
				return "" ;

			int idx = 0 ;
			while ( idx < s.Length )
			{
				if ( Char.IsWhiteSpace(s, idx) )
					s = s.Replace( s.Substring(idx,1), "" ) ;
				else
					idx++ ;
			}

			return s ;

		}

		/// <summary>
		/// Removes all control characters from a string; only printable chars remain.
		/// </summary>
		/// <param name="s">Source string.</param>
		/// <returns>The input string, with all control chars removed.  Returns "" on null input.</returns>
		public static string StripControlChars(string s)
		{
			if ( s == null )
				return "" ;

			int idx = 0 ;
			while ( idx < s.Length )
			{
				if ( Char.IsControl(s, idx) )
					s = s.Replace( s.Substring(idx,1), "" ) ;
				else
					idx++ ;
			}

			return s ;
		}

		/// <summary>
		/// Strips any non-alphanumeric formatting from a string.
		/// </summary>
		/// <param name="s">Source string.</param>
		/// <returns>The input string, consisting of only 7-bit ASCII letters and digits.  Returns "" on null input.</returns>
		public static string StripFormatting(string s)
		{
			if ( s == null )
				return "" ;

			int idx = 0 ;
			while ( idx < s.Length )
			{
				if ( ! Char.IsLetterOrDigit(s, idx) )
					s = s.Replace( s.Substring(idx,1), "" ) ;
				else
					idx++ ;
			}

			//return ProcessHighOrderAsciiChars(s) ;
			return NormalizeUnicodeString(s) ;
		}

		/// <summary>
		/// Process all ascii chars of value >= 0x80.
		/// Accented chars get demoted to unaccented equivalents.
		/// Other symbols get stripped.
		/// </summary>
		/// <param name="s"></param>
		/// <returns></returns>
		public static string ProcessHighOrderAsciiChars(string s)
		{
			if (s == null) return "";

			int idx = 0;
			while (idx < s.Length)
			{
				int charVal = (int)s[idx];
				if (charVal < 0x80)
				{
					++idx;
					continue;
				}

				switch (charVal)
				{
					case 0x80:
						s = s.Replace(s.Substring(idx,1), "C");
						++idx;
						break;
					case 0x81:
					case 0x96:
					case 0x97:
					case 0xa3:
						s = s.Replace(s.Substring(idx,1), "u");
						++idx;
						break;
					case 0x82:
					case 0x88:
					case 0x89:
					case 0x8a:
						s = s.Replace(s.Substring(idx,1), "e");
						++idx;
						break;
					case 0x83:
					case 0x84:
					case 0x85:
					case 0x86:
					case 0xa0:
					case 0xc6:
						s = s.Replace(s.Substring(idx,1), "a");
						++idx;
						break;
					case 0x87:
						s = s.Replace(s.Substring(idx,1), "c");
						++idx;
						break;
					case 0x8b:
					case 0x8c:
					case 0x8d:
					case 0xa1:
						s = s.Replace(s.Substring(idx,1), "i");
						++idx;
						break;
					case 0x8e:
					case 0x8f:
					case 0xb5:
					case 0xb6:
					case 0xb7:
					case 0xc7:
						s = s.Replace(s.Substring(idx,1), "A");
						++idx;
						break;
					case 0x90:
					case 0xd2:
					case 0xd3:
					case 0xd4:
						s = s.Replace(s.Substring(idx,1), "E");
						++idx;
						break;
					case 0x93:
					case 0x94:
					case 0x95:
					case 0x99:
					case 0x9b:
					case 0xa2:
					case 0xe4:
					case 0xe5:
						s = s.Replace(s.Substring(idx,1), "o");
						++idx;
						break;
					case 0x98:
					case 0xec:
						s = s.Replace(s.Substring(idx,1), "y");
						++idx;
						break;
					case 0x9a:
					case 0xe9:
					case 0xea:
					case 0xeb:
						s = s.Replace(s.Substring(idx,1), "U");
						++idx;
						break;
					case 0x9d:
					case 0xe0:
					case 0xe2:
					case 0xe3:
						s = s.Replace(s.Substring(idx,1), "O");
						++idx;
						break;
					case 0x9e:
						s = s.Replace(s.Substring(idx,1), "x");
						++idx;
						break;
					case 0x9f:
						s = s.Replace(s.Substring(idx,1), "f");
						++idx;
						break;
					case 0xa4:
						s = s.Replace(s.Substring(idx,1), "n");
						++idx;
						break;
					case 0xa5:
						s = s.Replace(s.Substring(idx,1), "N");
						++idx;
						break;
					case 0xd1:
						s = s.Replace(s.Substring(idx,1), "D");
						++idx;
						break;
					case 0xd6:
					case 0xd7:
					case 0xd8:
					case 0xde:
						s = s.Replace(s.Substring(idx,1), "I");
						++idx;
						break;
					case 0xed:
						s = s.Replace(s.Substring(idx,1), "Y");
						++idx;
						break;
					default:
						s = s.Replace(s.Substring(idx,1), "");
						break;
				}
			}

			return s;
		}

		/// <summary>
		/// Normalizes a Unicode string by converting it to simple characters.
		/// Similar in concept to the above ProcessHighOrderAsciiChars.
		/// </summary>
		/// <param name="s">A string to normalize.</param>
		/// <returns>The normalized simple string, interchangeable with ASCII.</returns>
		/// <remarks>
		///	See http://www.unicode.org/charts/ for Basic Latin, Latin-1 Supplement, and Latin Extended-A.
		/// </remarks>
		public static string NormalizeUnicodeString(string s)
		{
			if ( s == null )
				return "" ;

			bool bMustNormalize = false ;
			foreach ( char c in s )
			{
				if ( c < ' ' || c > '~' )
				{
					bMustNormalize = true ;
					break ;
				}
			}

			if ( ! bMustNormalize )
				return s ;


			System.Text.StringBuilder sb = new System.Text.StringBuilder(s) ;

			// Although a StringBuilder may be better suited for heavy-duty string manipulation,
			// the vast majority of inputs won't need any transformation.
			for ( int idx = 0 ; idx < sb.Length ; idx++ )
			{
				char c = sb[idx] ;

				// Allow regular lower-ASCII characters.
				// This will not strip lower-ASCII punctuation.
				if ( c >= ' ' && c <= '~' )
					continue ;

				int nValue = (int)c ;

				switch ( nValue )
				{
					case 0x00A0 :	// ' ' - NBSP
					case 0x00A8 :	// '¨'
					case 0x00AF :	// '¯'
					case 0x00B4 :	// '´'
					case 0x00B8 :	// '¸'
						sb = sb.Remove(idx, 1).Insert(idx, ' ') ;
						break ;
					case 0x00A1 :	// '¡'
						sb = sb.Remove(idx, 1).Insert(idx, '!') ;
						break ;
					case 0x00A4 :	// '¤'
						sb = sb.Remove(idx, 1).Insert(idx, '$') ;
						break ;
					case 0x00AC :	// '¬'
					case 0x00AD :	// '­'
						sb = sb.Remove(idx, 1).Insert(idx, '-') ;
						break ;
					case 0x00B7 :	// '·'
						sb = sb.Remove(idx, 1).Insert(idx, '.') ;
						break ;
					case 0x00BF :	// '¿'
						sb = sb.Remove(idx, 1).Insert(idx, '?') ;
						break ;
					case 0x00A6 :	// '¦'
						sb = sb.Remove(idx, 1).Insert(idx, '|') ;
						break ;									
					case 0x00B9 :	// '¹'
						sb = sb.Remove(idx, 1).Insert(idx, '1') ;
						break ;
					case 0x00BC :	// '¼'
						sb = sb.Remove(idx, 1).Insert(idx, "1/4") ;
						idx += 2 ;
						break ;
					case 0x00BD :	// '½'
						sb = sb.Remove(idx, 1).Insert(idx, "1/2") ;
						idx += 2 ;
						break ;
					case 0x00BE :	// '¾'
						sb = sb.Remove(idx, 1).Insert(idx, "3/4") ;
						idx += 2 ;
						break ;
					case 0x00B2 :	// '²'
						sb = sb.Remove(idx, 1).Insert(idx, '2') ;
						break ;
					case 0x00B3 :	// '³'
						sb = sb.Remove(idx, 1).Insert(idx, '3') ;
						break ;
					case 0x00C0 :	// 'À'
					case 0x00C1 :	// 'Á'
					case 0x00C2 :	// 'Â'
					case 0x00C3 :	// 'Ã'
					case 0x00C4 :	// 'Ä'
					case 0x00C5 :	// 'Å'
					case 0x0100 :
					case 0x0102 :
					case 0x0104 :
						sb = sb.Remove(idx, 1).Insert(idx, 'A') ;
						break ;
					case 0x00C6 :	// 'Æ'
						sb = sb.Remove(idx, 1).Insert(idx, "AE") ;
						idx++ ;
						break ;
					case 0x00C7 :	// 'Ç'
					case 0x0106 :
					case 0x0108 :
					case 0x010A :
					case 0x010C :
						sb = sb.Remove(idx, 1).Insert(idx, 'C') ;
						break ;
					case 0x00D0 :	// 'Ð'
					case 0x010E :
					case 0x0110 :
						sb = sb.Remove(idx, 1).Insert(idx, 'D') ;
						break ;
					case 0x00C8 :	// 'È'
					case 0x00C9 :	// 'É'
					case 0x00CA :	// 'Ê'
					case 0x00CB :	// 'Ë'
					case 0x0112 :
					case 0x0114 :
					case 0x0116 :
					case 0x0118 :
					case 0x011A :
						sb = sb.Remove(idx, 1).Insert(idx, 'E') ;
						break ;
					case 0x011C :
					case 0x011E :
					case 0x0120 :
					case 0x0122 :
						sb = sb.Remove(idx, 1).Insert(idx, 'G') ;
						break ;
					case 0x0124 :
					case 0x0126 :
						sb = sb.Remove(idx, 1).Insert(idx, 'H') ;
						break ;
					case 0x00CC :	// 'Í'
					case 0x00CD :	// 'Ì'
					case 0x00CE :	// 'Î'
					case 0x00CF :	// 'Ï'
					case 0x0128 :
					case 0x012A :
					case 0x012C :
					case 0x012E :
					case 0x0130 :
						sb = sb.Remove(idx, 1).Insert(idx, 'I') ;
						break ;
					case 0x0132 :
						sb = sb.Remove(idx, 1).Insert(idx, "IJ") ;
						idx++ ;
						break ;
					case 0x0134 :
						sb = sb.Remove(idx, 1).Insert(idx, 'J') ;
						break ;
					case 0x0136 :
						sb = sb.Remove(idx, 1).Insert(idx, 'K') ;
						break ;
					case 0x0139 :
					case 0x013B :
					case 0x013D :
					case 0x013F :
					case 0x0141 :
						sb = sb.Remove(idx, 1).Insert(idx, 'L') ;
						break ;
					case 0x00D1 :	// 'Ñ'
					case 0x0143 :
					case 0x0145 :
					case 0x0147 :
					case 0x014A :
						sb = sb.Remove(idx, 1).Insert(idx, 'N') ;
						break ;
					case 0x00D2 :	// 'Ò'
					case 0x00D3 :	// 'Ó'
					case 0x00D4 :	// 'Ô'
					case 0x00D5 :	// 'Õ'
					case 0x00D6 :	// 'Ø'
					case 0x014C :
					case 0x014E :
					case 0x0150 :
						sb = sb.Remove(idx, 1).Insert(idx, 'O') ;
						break ;
					case 0x0152 :
						sb = sb.Remove(idx, 1).Insert(idx, "OE") ;
						idx++ ;
						break ;
					case 0x0154 :
					case 0x0156 :
					case 0x0158 :
						sb = sb.Remove(idx, 1).Insert(idx, 'R') ;
						break ;
					case 0x015A :
					case 0x015C :
					case 0x015E :
					case 0x0160 :
						sb = sb.Remove(idx, 1).Insert(idx, 'S') ;
						break ;
					case 0x0162 :
					case 0x0164 :
					case 0x0166 :
						sb = sb.Remove(idx, 1).Insert(idx, 'T') ;
						break ;
					case 0x00DE :	// 'Þ'
						sb = sb.Remove(idx, 1).Insert(idx, "TH") ;
						idx++ ;
						break ;
					case 0x00D9 :	// 'Ù'
					case 0x00DA :	// 'Ú'
					case 0x00DB :	// 'Û'
					case 0x00DC :	// 'Ü'
					case 0x0168 :
					case 0x016A :
					case 0x016C :
					case 0x016E :
					case 0x0170 :
					case 0x0172 :
						sb = sb.Remove(idx, 1).Insert(idx, 'U') ;
						break ;
					case 0x0174 :
						sb = sb.Remove(idx, 1).Insert(idx, 'W') ;
						break ;
					case 0x00DD :	// 'Ý'
					case 0x0176 :
					case 0x0178 :
						sb = sb.Remove(idx, 1).Insert(idx, 'Y') ;
						break ;
					case 0x0179 :
					case 0x017B :
					case 0x017D :
						sb = sb.Remove(idx, 1).Insert(idx, 'Z') ;
						break ;
					case 0x00F7 :	// '÷'
						sb = sb.Remove(idx, 1).Insert(idx, '/') ;
						break ;
					case 0x00AA :	// 'ª'
					case 0x00E0 :	// 'à'
					case 0x00E1 :	// 'á'
					case 0x00E2 :	// 'â'
					case 0x00E3 :	// 'ã'
					case 0x00E4 :	// 'ä'
					case 0x00E5 :	// 'å'
					case 0x0101 :
					case 0x0103 :
					case 0x0105 :
						sb = sb.Remove(idx, 1).Insert(idx, 'a') ;
						break ;	
					case 0x00E6 :	// 'æ'
						sb = sb.Remove(idx, 1).Insert(idx, "ae") ;
						idx++ ;
						break ;
					case 0x00E7 :	// 'ç'
					case 0x0107 :
					case 0x0109 :
					case 0x010B :
					case 0x010D :
						sb = sb.Remove(idx, 1).Insert(idx, 'c') ;
						break ;
					case 0x00F0 :	// 'ð'
					case 0x010F :
					case 0x0111 :
						sb = sb.Remove(idx, 1).Insert(idx, 'd') ;
						break ;
					case 0x00E8 :	// 'è'
					case 0x00E9 :	// 'é'
					case 0x00EA :	// 'ê'
					case 0x00EB :	// 'ë'
					case 0x0113 :
					case 0x0115 :
					case 0x0117 :
					case 0x0119 :
					case 0x011B :
						sb = sb.Remove(idx, 1).Insert(idx, 'e') ;
						break ;
					case 0x011D :
					case 0x011F :
					case 0x0121 :
					case 0x0123 :
						sb = sb.Remove(idx, 1).Insert(idx, 'g') ;
						break ;
					case 0x0125 :
					case 0x0127 :
						sb = sb.Remove(idx, 1).Insert(idx, 'h') ;
						break ;
					case 0x00EC :	// 'ì'
					case 0x00ED :	// 'í'
					case 0x00EE :	// 'î'
					case 0x00EF :	// 'ï'
					case 0x0129 :
					case 0x012B :
					case 0x012D :
					case 0x012F :
					case 0x0131 :
						sb = sb.Remove(idx, 1).Insert(idx, 'i') ;
						break ;
					case 0x0133 :
						sb = sb.Remove(idx, 1).Insert(idx, "ij") ;
						idx++ ;
						break ;
					case 0x0135 :
						sb = sb.Remove(idx, 1).Insert(idx, 'j') ;
						break ;
					case 0x0137 :
					case 0x0138 :
						sb = sb.Remove(idx, 1).Insert(idx, 'k') ;
						break ;
					case 0x013A :
					case 0x013C :
					case 0x013E :
					case 0x0140 :
					case 0x0142 :
						sb = sb.Remove(idx, 1).Insert(idx, 'l') ;
						break ;
					case 0x00F1 :	// 'ñ'
					case 0x0144 :
					case 0x0146 :
					case 0x0148 :
					case 0x014B :
						sb = sb.Remove(idx, 1).Insert(idx, 'n') ;
						break ;
					case 0x0149 :
						sb = sb.Remove(idx, 1).Insert(idx, "'n") ;
						idx++ ;
						break ;
                    case 0x00BA :	// 'º'
					case 0x00F2 :	// 'ó'
					case 0x00F3 :	// 'ô'
					case 0x00F4 :	// 'õ'
					case 0x00F5 :	// 'ö'
					case 0x00F6 :	// 'ø'
					case 0x014D :
					case 0x014F :
					case 0x0151 :
						sb = sb.Remove(idx, 1).Insert(idx, 'o') ;
						break ;
					case 0x0153 :
						sb = sb.Remove(idx, 1).Insert(idx, "oe") ;
						idx++ ;
						break ;
					case 0x0155 :
					case 0x0157 :
					case 0x0159 :
						sb = sb.Remove(idx, 1).Insert(idx, 'r') ;
						break ;
					case 0x015B :
					case 0x015D :
					case 0x015F :
					case 0x0161 :
					case 0x017F :
						sb = sb.Remove(idx, 1).Insert(idx, 's') ;
						break ;
					case 0x00DF :	// 'ß'
						sb = sb.Remove(idx, 1).Insert(idx, "SS") ;
						idx++ ;
						break ;
					case 0x0163 :
					case 0x0165 :
					case 0x0167 :
						sb = sb.Remove(idx, 1).Insert(idx, 't') ;
						break ;
					case 0x00FE :	// 'þ'
						sb = sb.Remove(idx, 1).Insert(idx, "th") ;
						idx++ ;
						break ;
					case 0x00F9 :	// 'ù'
					case 0x00FA :	// 'ú'
					case 0x00FB :	// 'û'
					case 0x00FC :	// 'ü'
					case 0x0169 :
					case 0x016B :
					case 0x016D :
					case 0x016F :
					case 0x0171 :
					case 0x0173 :
						sb = sb.Remove(idx, 1).Insert(idx, 'u') ;
						break ;
					case 0x0175 :
						sb = sb.Remove(idx, 1).Insert(idx, 'w') ;
						break ;
					case 0x00D7 :	// '×'
						sb = sb.Remove(idx, 1).Insert(idx, 'x') ;
						break ;
					case 0x00FD :	// 'ý'
					case 0x00FF :	// 'ÿ'
					case 0x0177 :
						sb = sb.Remove(idx, 1).Insert(idx, 'y') ;
						break ;
					case 0x017A :
					case 0x017C :
					case 0x017E :
						sb = sb.Remove(idx, 1).Insert(idx, 'z') ;
						break ;
					default :
						sb = sb.Remove(idx, 1) ;
						idx-- ;
						break ;
				}
			}

			return sb.ToString() ;
		}

		/// <summary>
		/// Reverses a string.
		/// </summary>
		/// <param name="s">Source string to reverse.</param>
		/// <returns>The input string, in reverse order.  Returns "" on null input.</returns>
		public static string Reverse(string s)
		{
			if ( s == null )
				return "" ;

			char[] aChars = s.ToCharArray() ;
			Array.Reverse(aChars) ;
			return new string(aChars) ;
		}

		/// <summary>
		/// Converts a string from "camel-case" formatting to upper case with spaces.
		/// </summary>
		/// <param name="s">String to convert.</param>
		/// <returns>The processed string, or the empty string on missing input.</returns>
		public static string UnCamel(string s)
		{
			if ( null == s || String.Empty == s )
				return String.Empty ;

			string s2 ;			

			// Split off the last letter of an uppercase string,
			// unless we're at the end.
			// Example 1: "EquifaxSSNVerified" > "EquifaxSSN Verified"
			// Example 2: "EquifaxSAFESCAN" > "EquifaxSAFESCAN"
			s2 = s_rgUncamel1.Replace(s, "$1 $2");
			//s2 = System.Text.RegularExpressions.Regex.Replace(s, "([A-Z]+)([A-Z])(?=[a-z])", "$1 $2") ;

			// Split sequences with a lower-case letter immediately followed
			// by an upper-case letter, then capitalize everything.
			// Example 1: "EquifaxSSN Verified" > "EQUIFAX SSN VERIFIED"
			// Example 2: "EquifaxSAFESCAN" > "EQUIFAX SAFESCAN"
			// Example 3: "ExperianFairIsaac" > "EXPERIAN FAIR ISAAC"
			s2 = s_rgUncamel2.Replace(s2, "$1 $2").ToUpper();
			//s2 = System.Text.RegularExpressions.Regex.Replace(s2, "([a-z])([A-Z])", "$1 $2").ToUpper() ;

			// If we have the phrase 'TRANS' followed by spacing and another phrase,
			// merge them together.
			// Example 1: "TRANS UNION HAWK ALERT" > "TRANSUNION HAWK ALERT"
			// Example 2: "TRANS UNION TRANS ALERT" > "TRANSUNION TRANSALERT"
			s2 = s_rgUncamelTRANS.Replace(s2, "$1$2");
			//s2 = System.Text.RegularExpressions.Regex.Replace(s2, @"(TRANS)\s(\S)", "$1$2") ;

			return s2 ;
		}

		/// <summary>
		/// Determines whether or not a string has a value.
		/// </summary>
		/// <param name="s">String to gauge</param>
		/// <returns>False if the input is null or trims to String.Empty, True otherwise.</returns>
		public static bool HasValue(string s)
		{
			if ( s == null || s.Trim().Length == 0 )
				return false ;
			else
				return true ;
		}

		/// <summary>
		/// Determines whether or not a string object has a value.
		/// </summary>
		/// <param name="obj">String to gauge, as an Object</param>
		/// <returns>False if SafeConvert.ToString would return String.Empty, True otherwise.</returns>
		public static bool HasValue(Object obj)
		{
			string s = SafeConvert.ToString(obj) ;
			return HasValue(s) ;
		}

		/// <summary>
		/// Converts a standard NANPA phone number into human-readable format.
		/// </summary>
		/// <param name="inString">Phone number to parse.</param>
		/// <returns>
		/// If <paramref="a_sInput"/> is a string of digits resembling a phone number, the segments are separated using hyphens.
		/// If <paramref="a_sInput"/> is <see langword="null"/>, <see cref="String.Empty"/>
		/// Otherwise, the input string is trimmed.
		/// </returns>
		public static string ParsePhoneNumber(string inString)
		{
			if ( null == inString )
				return String.Empty ;

			inString = inString.Trim() ;

			//System.Text.RegularExpressions.Regex expr = new System.Text.RegularExpressions.Regex(@"^((?<CountryCode>1)?(?<AreaCode>[2-9]\d{2}))?(?<Exchange>[2-9]\d{2})(?<Local>\d{4})$") ;
			Match match = s_rgPhone.Match(inString) ;

			if ( System.Text.RegularExpressions.Match.Empty == match )
				return inString ;

			System.Text.StringBuilder sbOutput = new System.Text.StringBuilder() ;
			if ( String.Empty != match.Groups["CountryCode"].Value )
				sbOutput.Append(match.Groups["CountryCode"].Value + "-") ;
			if ( String.Empty != match.Groups["AreaCode"].Value )
				sbOutput.Append(match.Groups["AreaCode"].Value + "-") ;
			sbOutput.Append(match.Groups["Exchange"].Value + "-") ;
			sbOutput.Append(match.Groups["Local"].Value) ;

			return sbOutput.ToString() ;
		}

		/// <summary>
		/// Encases a string for use with client-side script.
		/// </summary>
		/// <param name="str">String to encase.</param>
		/// <param name="bSingleQuotes">True to encase the string in single quotes ['], False to use double quotes ["].</param>
		/// <returns>
		/// The string encased in quotes.
		/// If the input string is null, a string containing the word null will be returned.
		/// </returns>
		public static string ScriptString(string str, bool bSingleQuotes)
		{
			if ( str == null )			
				return "null" ;

			string sEncoded = str.Replace("'", "\\'").Replace("\"", "\\\"")  ;
			if ( bSingleQuotes )
				return "'" + sEncoded + "'" ;
			else
				return "\"" + sEncoded + "\"" ;
		}

		/// <summary>
		/// Returns a left-justified string, right-padded with blanks.
		/// </summary>
		/// <param name="sValue">String to process.</param>
		/// <param name="nLength">Total length of the string and the padding.</param>
		/// <returns>A right-padded string.  If the actual length is greater than the desired length, the returned string will be truncated.</returns>
		public static string PadRight(string sValue, int nLength)
		{
			return PadRight(sValue, nLength, ' ') ;
		}

		/// <summary>
		/// Returns a right-padded, left-justified string.
		/// </summary>
		/// <param name="sValue">String to process.</param>
		/// <param name="nLength">Total length of the string and the padding.</param>
		/// <param name="cPad">Padding character.</param>
		/// <returns>A right-padded string.  If the actual length is greater than the desired length, the returned string will be truncated.</returns>
		/// <example>PadRight("ABC", 5, ' ') = "ABC  "</example>
		public static string PadRight(string sValue, int nLength, char cPad)
		{
			if ( nLength < 0 )
				throw new ArgumentOutOfRangeException("nLength", nLength, "String length cannot be negative.") ;

			if ( sValue == null )
				return new string(cPad, nLength) ;

			int nActualLen = sValue.Length ;
			if ( nActualLen < nLength )
				return String.Concat(sValue, new String(cPad, nLength-nActualLen)) ;
			else if ( nActualLen > nLength )
				return sValue.Substring(0, nLength) ;
			else
				return sValue ;
		}


		/// <summary>
		/// Returns a right-justified string, left-padded with blanks.
		/// </summary>
		/// <param name="sValue">String to process.</param>
		/// <param name="nLength">Total length of the string and the padding.</param>
		/// <returns>A left-padded string.  If the actual length is greater than the desired length, the returned string will be truncated.</returns>
        public static string PadLeft(string sValue, int nLength)
		{
			return PadLeft(sValue, nLength, ' ') ;
		}

		/// <summary>
		/// Returns a left-padded right-justified string.
		/// </summary>
		/// <param name="sValue">String to process.</param>
		/// <param name="nLength">Total length of the string and the padding.</param>
		/// <param name="cPad">Padding character.</param>
		/// <returns>A left-padded string.  If the actual length is greater than the desired length, the returned string will be truncated.</returns>
		/// <example>PadLeft("ABC", 5, ' ') = "  ABC"</example>
		public static string PadLeft(string sValue, int nLength, char cPad)
		{
			if ( nLength < 0 )
				throw new ArgumentOutOfRangeException("nLength", nLength, "String length cannot be negative.") ;

			if ( sValue == null )
				return new string(cPad, nLength) ;

			int nActualLen = sValue.Length ;
			if ( nActualLen < nLength )
				return String.Concat(new String(cPad, nLength-nActualLen), sValue) ;
			else if ( nActualLen > nLength )
				return sValue.Substring(0, nLength) ;
			else
				return sValue ;
		}

		/// <summary>
		/// A variant of StringBuilder that inserts delimiters between each entry.
		/// </summary>
		public class DelimitedStringBuilder
		{
			private string delim ;
			private bool isEmpty ;
			private bool bAddEmpty ;
			private System.Text.StringBuilder builder ;

			/// <summary>
			/// Creates an empty DelimitedStringBuilder, with the default delimiter "; ".
			/// </summary>
			public DelimitedStringBuilder()
			{
				delim = "; " ;
				isEmpty = true ;
				bAddEmpty = false ;
				builder = new System.Text.StringBuilder() ;
			}

			/// <summary>
			/// Creates an empty DelimitedStringBuilder, with the default delimiter "; ".
			/// </summary>
			/// <param name="nInitialSize">Initial size for the string builder.</param>
			public DelimitedStringBuilder(int nInitialSize)
			{
				delim = "; " ;
				isEmpty = true ;
				bAddEmpty = false ;
				builder = new System.Text.StringBuilder(nInitialSize) ;
			}

			/// <summary>
			/// Creates an empty DelimitedStringBuilder.
			/// </summary>
			/// <param name="sDelim">The delimiter to use.</param>
			public DelimitedStringBuilder(string sDelim)
			{
				delim = sDelim ;
				isEmpty = true ;
				bAddEmpty = false ;
				builder = new System.Text.StringBuilder() ;
			}

			/// <summary>
			/// Creates an empty DelimitedStringBuilder.
			/// </summary>
			/// <param name="sDelim">The delimiter to use.</param>
			/// <param name="nInitialSize">Initial size for the string builder.</param>
			public DelimitedStringBuilder(string sDelim, int nInitialSize)
			{
				delim = sDelim ;
				isEmpty = true ;
				bAddEmpty = false ;
				builder = new System.Text.StringBuilder(nInitialSize) ;
			}

			/// <summary>
			/// Creates a new DelimitedStringBuilder.
			/// No delimiter will be appended after the initial value. 
			/// </summary>
			/// <param name="sDelim">The delimiter to use.</param>
			/// <param name="sInitialValue">Initial value to add.</param>
			public DelimitedStringBuilder(string sDelim, string sInitialValue)
			{
				// This method previously changed an empty delimiter into the default delimiter.
				// That behavior was removed to parallel the other constructors.

				delim = sDelim ;

				if ( sInitialValue == null || sInitialValue.Length == 0 )
				{
					isEmpty = true ;
					bAddEmpty = false ;
					builder = new System.Text.StringBuilder() ;
				}
				else
				{
					isEmpty = true ; // not false, because we don't want to add a delimiter...
					bAddEmpty = false ;
					builder = new System.Text.StringBuilder(sInitialValue) ;
				}
			}

			/// <summary>
			/// Creates a new DelimitedStringBuilder.
			/// No delimiter will be appended after the initial value. 
			/// </summary>
			/// <param name="sDelim">The delimiter to use.</param>
			/// <param name="sInitialValue">Initial value to add.</param>
			/// <param name="nInitialSize">Initial size for the string builder.</param>
			public DelimitedStringBuilder(string sDelim, string sInitialValue, int nInitialSize)
			{
				delim = sDelim ;

				if ( sInitialValue == null || sInitialValue.Length == 0 )
				{
					isEmpty = true ;
					bAddEmpty = false ;
					builder = new System.Text.StringBuilder(nInitialSize) ;
				}
				else
				{
					isEmpty = true ; // not false, because we don't want to add a delimiter...
					bAddEmpty = false ;
					builder = new System.Text.StringBuilder(sInitialValue, nInitialSize) ;
				}
			}

			/// <summary>
			/// Appends a string to the DelimitedStringBuilder.
			/// </summary>
			/// <param name="sValue">String to append.</param>
			/// <returns>Changed by Shenfeng.  It should be consistent with the append method in its base class.  It should return itself so that the append could be chained.</returns>
			/// <remarks>Base class?  Sounds nice, but System.Text.StringBuilder is sealed.</remarks>
			public DelimitedStringBuilder Append(string sValue)
			{
				bool bHasInput = ( null != sValue && "" != sValue ) ;

				if ( bHasInput || bAddEmpty )
				{
					if ( isEmpty )
						isEmpty = false ;
					else
						builder.Append(delim) ;

                    if ( bHasInput )
						builder.Append(sValue) ;
				}
				return this;
			}

			/// <summary>
			/// Appends a string with formatting arguments to the DelimitedStringBuilder.
			/// </summary>
			/// <param name="sFormat">Format string</param>
			/// <param name="args">Arguments</param>
			/// <remarks>
			/// The format handling is identical to that of the regular System.Text.StringBuilder --
			/// and, for that matter, System.Console.WriteLine.
			/// </remarks>
			/// <example>
			/// AppendFormat("LAST LATE DATE: {0}", sDate) where sDate = "12/02" will append
			/// "LAST LATE DATE: 12/02"
			/// </example>
			public void AppendFormat(string sFormat, params object[] args)
			{
				if ( sFormat != null && sFormat != "" )
				{
					if ( isEmpty )
						isEmpty = false ;
					else
						builder.Append(delim) ;

					builder.AppendFormat(sFormat, args) ;
				}
			}

			/// <summary>
			/// Appends a string with formatting arguments to the DelimitedStringBuilder,
			/// with conditional omission for blank strings.
			/// </summary>
			/// <param name="bRequireArgs">If true, then all arguments must be non-null and non-empty strings.</param>
			/// <param name="sFormat">Format string</param>
			/// <param name="args">Arguments</param>
			public void AppendFormat(bool bRequireArgs, string sFormat, params object[] args)
			{
				if ( bRequireArgs )
				{
					if ( 0 == args.Length )
						return ;

					foreach ( object arg in args )
					{
						if ( null == arg )
							return ;
						if ( arg is string && String.Empty == (string)arg )
							return ;
					}
				}

				AppendFormat(sFormat, args) ;
			}

			/// <summary>
			/// Trims a string, and appends it to the DelimitedStringBuilder.
			/// </summary>
			/// <param name="oValue">String to append, represented as an Object</param>
			public void SafeAppend(object oValue)
			{
				Append( SafeConvert.ToString(oValue) ) ;
			}

			/// <summary>
			/// Inserts a string at the beginning of the DelimitedStringBuilder.
			/// </summary>
			/// <param name="sValue">String to prepend.</param>
			public void Prepend(string sValue)
			{
				bool bHasInput = ( null != sValue && "" != sValue ) ;

				if ( bHasInput || bAddEmpty )
				{
					if ( isEmpty )
					{
						isEmpty = false ;
						if ( bHasInput )
							builder.Append(sValue) ;
					}
					else
					{
						if ( bHasInput )
							builder.Insert(0, sValue + delim) ;
						else
							builder.Insert(0, delim) ;
					}
				}
			}

			/// <summary>
			/// Trims a string, and prepends it to the DelimitedStringBuilder.
			/// </summary>
			/// <param name="sValue">String to trim and prepend, represented as an Object.</param>
			public void SafePrepend(object oValue)
			{
				Prepend( SafeConvert.ToString(oValue) ) ;
			}

			/// <summary>
			/// Resets this DelimitedStringBuilder.
			/// </summary>
			public void Reset()
			{
				builder.Remove(0, builder.Length) ;
				isEmpty = true ;
			}

			/// <summary>
			/// Returns the string contained in this DelimitedStringBuilder.
			/// </summary>
			public override string ToString()
			{
				return builder.ToString() ;
			}

			/// <summary>
			/// Gets or sets the length of this DelimitedStringBuilder.
			/// </summary>
			public int Length
			{
				get { return builder.Length ; }
				set { builder.Length = value ; }
			}

			/// <summary>
			/// Gets or sets whether or not to add empty strings to the builder.
			/// Does not apply to AppendFormat.
			/// Defaults to false.
			/// </summary>
			public bool AddEmpty
			{
				get { return bAddEmpty ; }
				set { bAddEmpty = value ; }
			}
		}
	}
}
