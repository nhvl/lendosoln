// Copyright 2000-2005 MeridianLink, Inc.  All Rights Reserved.
using System;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.IO;
using System.Net;

namespace CommonLib
{
	/// <summary>
	/// Summary description for WebServiceSoapCapture.
	/// </summary>
	/// <remarks>
	/// Code adapted from:
	/// http://msdn.microsoft.com/library/default.asp?url=/library/en-us/cpguide/html/cpconalteringsoapmessageusingsoapextensions.asp
	/// 
	/// Usage:
	/// Add the following section into the application's config file:
	/// <configuration>
	///		<system.web>
	///			<webServices>
	///				<soapExtensionTypes>
	///					<add type="CommonLib.WebServiceSoapCapture, CommonLib" priority="1" group="0"/>
	///				</soapExtensionTypes>
	///			</webServices>
	///		</system.web>
	/// </configuration>
	/// </remarks>
	public class WebServiceSoapCapture : SoapExtension
	{
		Stream oldStream;
		Stream newStream;

		private string m_sBeforeSerialize = "";
		private string m_sAfterSerialize = "";
		private string m_sBeforeDeSerialize = "";
		private string m_sAfterDeSerialize = "";

		public string BeforeSerialize
		{
			get {return this.m_sBeforeSerialize;}
		}

		public string AfterSerialize
		{
			get {return this.m_sAfterSerialize;}
		}

		public string BeforeDeSerialize
		{
			get {return this.m_sBeforeDeSerialize;}
		}

		public string AfterDeSerialize
		{
			get {return this.m_sAfterDeSerialize;}
		}

		public override Stream ChainStream( Stream stream )
		{
			oldStream = stream;
			newStream = new MemoryStream();
			return newStream;
		}

		public override object GetInitializer(LogicalMethodInfo methodInfo, SoapExtensionAttribute attribute) 
		{
			return null;
		}

		public override object GetInitializer(Type WebServiceType) 
		{
			return null;    
		}

		public override void Initialize(object initializer) 
		{
		}

		public override void ProcessMessage(SoapMessage message) 
		{
			switch (message.Stage) 
			{
				case SoapMessageStage.BeforeSerialize:
					//this.m_sBeforeSerialize = CaptureOutput(message);
					//Trace("BEFORE SERIALIZE", this.m_sBeforeSerialize);
					break;
				case SoapMessageStage.AfterSerialize:
					this.m_sAfterSerialize = CaptureOutput(message);
					Trace("AFTER SERIALIZE", this.m_sAfterSerialize);
					break;
				case SoapMessageStage.BeforeDeserialize:
					this.m_sBeforeDeSerialize = CaptureInput(message);
					Trace("BEFORE DE-SERIALIZE", this.m_sBeforeDeSerialize);
					break;
				case SoapMessageStage.AfterDeserialize:
					//this.m_sAfterDeSerialize = CaptureInput(message);
					//Trace("AFTER DE-SERIALIZE", this.m_sAfterDeSerialize);
					break;
				default:
					throw new ApplicationException("invalid stage " + message.Stage);
			}
		}

		public string CaptureOutput(SoapMessage message)
		{
			newStream.Position = 0;
			StreamReader reader = new StreamReader(newStream);
			string sOut = reader.ReadToEnd();
			newStream.Position = 0;
			Copy(newStream, oldStream);

			return sOut;
		}

		public string CaptureInput(SoapMessage message)
		{
			Copy(oldStream, newStream);
			newStream.Position = 0;
			StreamReader reader = new StreamReader(newStream);
			string sOut = reader.ReadToEnd();
			newStream.Position = 0;

			return sOut;
		}

		void Copy(Stream from, Stream to) 
		{
			TextReader reader = new StreamReader(from);
			TextWriter writer = new StreamWriter(to);
			writer.WriteLine(reader.ReadToEnd());
			writer.Flush();
		}

		private void Trace(string header, string message)
		{
#if DEBUG
			Tracer.Trace("CommonLib", "WebServiceSoapCapture", "Trace", "inf", header + " : " + message);
#endif
		}
	}
}
