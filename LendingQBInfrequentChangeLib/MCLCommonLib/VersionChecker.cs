// Copyright 2000-2005 MeridianLink, Inc.  All Rights Reserved.
using System;

namespace CommonLib
{
	/// <summary>
	/// When objects are serialized, they need to store a version field.
	/// Later when objects are restored, the code may be an earlier or later version.
	/// This class encapsulates some logic for handling discrepencies
	/// </summary>
	public abstract class VersionChecker
	{
		public enum StoredToCodeRelation {StoredIsEarlier, Same, StoredIsLater}

		public static StoredToCodeRelation Relation(double storedVersion, double codeVersion)
		{
			if (storedVersion < codeVersion) return StoredToCodeRelation.StoredIsEarlier;
			else if (storedVersion > codeVersion) return StoredToCodeRelation.StoredIsLater;
			return StoredToCodeRelation.Same;
		}

		public static bool AreSame(double storedVersion, double codeVersion)
		{
			return (storedVersion == codeVersion);
		}

		/// <summary>
		/// IF the stored version is newer than the code's version, will throw an exception.
		/// </summary>
		/// <param name="storedVersion">Version for the serialized object</param>
		/// <param name="codeVersion">Version for the importing code</param>
		/// <param name="marker">String to distinguish where the error occurred, usually a class' name.</param>
		public static void EnforceNoForwardCompatible(double storedVersion, double codeVersion, string marker)
		{
			StoredToCodeRelation rel = Relation(storedVersion, codeVersion);
			if (rel == StoredToCodeRelation.StoredIsLater)
			{
				string sMsg = "Stored version is LATER than code version for " + marker;
				throw new ApplicationException(sMsg);
			}
		}
	}
}
