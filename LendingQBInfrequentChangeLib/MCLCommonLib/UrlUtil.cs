using System;

namespace CommonLib
{
	/// <summary>
	/// Summary description for UrlUtil.
	/// </summary>
	public class UrlUtil
	{
		static public bool IPMatch(string sIPMask, string sUserIP)
		{
			try
			{
				string[] sArrMask = sIPMask.Split('.');
				string[] sArrUserIP = sUserIP.Split('.');
				if (sArrMask.Length != sArrUserIP.Length) return false;

				for (int i=0; i<sArrMask.Length; ++i)
				{
					string sMask = ((string)sArrMask[i]).Trim();
					string sUser = ((string)sArrUserIP[i]).Trim();

					if (sMask == "*") continue;

					int nIndex = sMask.IndexOf('-');
					if (nIndex < 0)
					{
						int nMask = int.Parse(sMask);
						int nUser = int.Parse(sUser);
						if (nMask != nUser) return false;
					}
					else
					{
						int nLow = int.Parse(sMask.Substring(0, nIndex));
						int nHigh = int.Parse(sMask.Substring(nIndex + 1));
						int nUser = int.Parse(sUser);
						if (nUser < nLow) return false;
						if (nUser > nHigh) return false;
					}
				}
				return true;
			}
			catch
			{
				return false;
			}
		}
		public static bool HasParam(string sURL, string sParam)
		{
			if (sURL == "" || sParam == "") return false ;
			sParam += "=" ;
			
			return ((sURL.IndexOf("?" + sParam) > -1) || (sURL.IndexOf("&" + sParam) > -1)) ;
		}
		public static string GetParameterValue(string sURL, string sParam)
		{
			if (sURL == "" || sParam == "") return ("") ;
			
			sParam += "=" ;
			int n = sURL.IndexOf("?" + sParam) ;
			if (n == -1)
			{	
				n = sURL.IndexOf("&" + sParam) ;
				if (n == -1) return ("") ;
				sParam = "&" + sParam ;
			}	
			else
				sParam = "?" + sParam ;
			
			string temp = SafeString.Mid(sURL, n + sParam.Length) ;
			n = temp.IndexOf("&") ;
			
			return ((n == -1) ? temp : SafeString.Left(temp, n)) ;
		}
		public static string RemoveParameter(string sURL, string sParam)
		{
			string sSearch = sParam + "=";
			int nStart = sURL.IndexOf(sSearch);
			if (nStart < 0) return sURL;

			int nEnd = sURL.IndexOf("&", nStart);
			if (nEnd < 0) // param is at end of url
			{
				return sURL.Substring(0, nStart - 1); // -1 also removes the '?' or '&' preceding param
			}
			else // param is in middle
			{
				return sURL.Remove(nStart, nEnd - nStart + 1); // +1 also removes the following '&'
			}
		}
		public static string AppendParameter(string sURL, string sParam, string sValue)
		{
			if (sParam == "" || sURL.IndexOf("?" + sParam + "=") > 0 || sURL.IndexOf("&" + sParam + "=") > 0) return (sURL) ;
			
			if (sURL.IndexOf("?") == -1)
				return (sURL + "?" + sParam + "=" + sValue) ;
			else
				return (sURL + "&" + sParam + "=" + sValue) ;	
		}
		public static string AppendParameter(string sURL, string sParam, int nValue)
		{
			return AppendParameter(sURL, sParam, nValue.ToString()) ;
		}
		public static void SplitUrl(string url, out string path, out string args)
		{
			path = args = null;
			if ((url == null) || (url.Length == 0)) return;

			int nSplitIndex = url.IndexOf('?');
			if ((nSplitIndex < 0) || (nSplitIndex == url.Length - 1))
			{
				path = url;
			}
			else if (nSplitIndex == 0)
			{
				args = url;
			}
			else
			{
				path = url.Substring(0, nSplitIndex);
				args = url.Substring(nSplitIndex + 1);
			}
		}
		public static System.Collections.Specialized.NameValueCollection ParseQueryArgs(string queryArgs)
		{
			if ((queryArgs == null) || (queryArgs.Length == 0)) return null;

			System.Collections.Specialized.NameValueCollection dict = null;
			string[] pieces = queryArgs.Split('&');
			foreach (string sPiece in pieces)
			{
				string[] sides = sPiece.Split('=');
				if (sides.Length != 2) return null; // invalid query string
				if ((sides[0] == null) || (sides[0].Trim().Length == 0)) return null; // invalid query param name

				if (dict == null) dict = new System.Collections.Specialized.NameValueCollection(pieces.Length);
				dict[sides[0].Trim()] = sides[1].Trim();
			}

			return dict;
		}
	}

	/// <summary>
	/// Builds a URL string by concatenating query arguments.
	/// </summary>
	public class UrlStringBuilder
	{
		private string m_sBaseUrl ;
		private System.Collections.Specialized.NameValueCollection m_aQueryArgs ;

		private const string m_sQuerySeparator = "?" ;
		private const string m_sQueryAssignment = "=" ;
		private string m_sArgSeparator = "&" ;

		/// <summary>
		/// Specifies the base URL for a query string.
		/// </summary>
		/// <param name="baseUrl">Base URL.  The URL can be relative or blank, but not null.</param>
		public UrlStringBuilder(string baseUrl)
		{
			if ( baseUrl == null )
				throw new ArgumentNullException("a_sBaseUrl") ;

			m_sBaseUrl = baseUrl ;
			m_aQueryArgs = new System.Collections.Specialized.NameValueCollection() ;
		}

		/// <summary>
		/// Adds a query argument to the URL String Builder.
		/// </summary>
		/// <param name="name">Query argument name.  The name must not be null or blank.</param>
		/// <param name="value">Query argument value</param>
		public void Add(string name, string value)
		{
			if ( !SafeString.HasValue(name) )
				throw new ArgumentNullException("a_sName") ;

			m_aQueryArgs.Add(name, value) ;
		}

		/// <summary>
		/// Gets or sets the argument separator.  The separator may not be set to null or blank.  The default separator is "&".
		/// </summary>
		/// <remarks>
		/// IIS prefers "&"; some Apache variants use ";".
		/// </remarks>
		public string ArgumentSeparator
		{
			get { return m_sArgSeparator ; }
			set
			{
				if ( !SafeString.HasValue(value) )
					throw new ArgumentNullException() ;
				else
					m_sArgSeparator = value ;
			}
		}

		/// <summary>
		/// Gets the full URL with all query arguments.
		/// </summary>
		public string FullUrl
		{
			get 
			{
				int nItems = m_aQueryArgs.Count ;

				if ( nItems == 0 )
					return m_sBaseUrl ;

				System.Text.StringBuilder sb = new System.Text.StringBuilder(m_sBaseUrl) ;
				sb.Append(m_sQuerySeparator) ;
				sb.Append(NameValueString(m_aQueryArgs.Keys[0], m_aQueryArgs[0])) ;

				for ( int nItem = 1 ; nItem < nItems ; nItem++ )
				{
					sb.Append(m_sArgSeparator) ;
					sb.Append(NameValueString(m_aQueryArgs.Keys[nItem], m_aQueryArgs[nItem])) ;
				}

				return sb.ToString() ;
			}
		}

		/// <summary>
		/// Overridden.  Gets the full URL with all query arguments.
		/// </summary>
		/// <returns>Same as Get FullUrl.</returns>
		public override string ToString()
		{
			return this.FullUrl ;
		}

		private string NameValueString(string name, string value)
		{
			return String.Concat(System.Web.HttpUtility.UrlEncode(name), m_sQueryAssignment, System.Web.HttpUtility.UrlEncode(value)) ;
		}
	}
}
