// Copyright 2000-2005 MeridianLink, Inc.  All Rights Reserved.
using System;

namespace CommonLib
{
	/// <summary>
	/// Encapsulates logic for dealing with checking and updating a resource that expires
	/// in a thread safe way.
	/// </summary>
	public class ExpiringResource : IDisposable
	{
		/// <summary>
		/// Return true if the expiration condition is reached.
		/// </summary>
		public delegate bool IsExpiredDelegate();

		/// <summary>
		/// Do whatever is expected to happen upon expiration, AND reset switches used to detect
		/// expiration condition.
		/// </summary>
		public delegate void HandleExpiredDelegate();

		private bool m_bUpdateInProgress;
		private string m_sSlotName;
		private LocalDataStoreSlot m_slot;
		private IsExpiredDelegate m_fnIsExpired;
		private HandleExpiredDelegate m_fnHandleExpired;
		private bool m_bDisposed;

		public ExpiringResource()
		{
			this.m_bUpdateInProgress = false;
			this.m_sSlotName = Guid.NewGuid().ToString();
			this.m_slot = System.Threading.Thread.AllocateNamedDataSlot(this.m_sSlotName);
		}

		public IsExpiredDelegate IsExpired {set{this.m_fnIsExpired = value;}}
		public HandleExpiredDelegate HandleExpired {set{this.m_fnHandleExpired = value;}}

		~ExpiringResource()
		{
			Dispose(false);
		}

		public virtual void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		private void Dispose(bool disposing)
		{
			if (!this.m_bDisposed)
			{
				this.m_bDisposed = true;
				System.Threading.Thread.FreeNamedDataSlot(this.m_sSlotName);
				this.m_slot = null;
				this.m_sSlotName = null;
			}
		}

		/// <summary>
		/// Call this method before using a resource.  This will call the IsExpired
		/// delegate, and if it returns true it will call the HandleExpired delegate
		/// in a thread safe manner.
		/// </summary>
		/// <param name="toLock">Usually the this pointer of the calling object</param>
		/// <remarks>
		/// IMPORTANT: WHEN USING THE RESOURCE, YOU SHOULD ALWAYS REFERENCE IT FROM A
		/// LOCAL VARIABLE, NOT THE MEMBER DATA VARIABLE.  ALSO, WHEN UPDATING, YOU SHOULD CREATE
		/// A NEW OBJECT AND FULLY INITIALIZE AND ONLY THEN ASSIGN IT TO THE MEMBER VARIABLE.  THIS
		/// IS IMPORTANT TO PRESERVE THREAD SAFETY
		/// </remarks>
		public void CheckExpired(object toLock)
		{
			if (this.m_bDisposed)
				throw new ObjectDisposedException(GetType().ToString());

			// Thread-safe checking function, releases lock so the potentially
			// costly resource update is done when unlocked and only by one thread.
			if (this.m_fnIsExpired())
			{
				lock (toLock)
				{
					if (!this.m_bUpdateInProgress && this.m_fnIsExpired())
					{
						this.m_bUpdateInProgress = true;
						System.Threading.Thread.SetData(this.m_slot, "T");
					}
				}
			}

			if ((string)System.Threading.Thread.GetData(this.m_slot) == "T")
			{
				try
				{
					this.m_fnHandleExpired();
				}
				finally
				{
					this.m_bUpdateInProgress = false;
					System.Threading.Thread.SetData(this.m_slot, "F");
				}
			}
		}
	}

	/// <summary>
	/// Encapsulates the logic of a resource which expires periodically.  For example, a database query which
	/// must performed on a schedule.
	/// </summary>
	public class PeriodicExpiringResource : ExpiringResource
	{
		private string m_sExpireTime; // I use a string instead of DateTime for reasons of thread safety
		private int m_nPeriodInMilliseconds;
		private HandleExpiredDelegate m_fnHandleExpired2;

		public PeriodicExpiringResource(int periodInMilliseconds, HandleExpiredDelegate fnHandleExpired)
		{
			this.m_nPeriodInMilliseconds = periodInMilliseconds;
			this.m_sExpireTime = DateTime.Now.AddMilliseconds(periodInMilliseconds).ToString();
			this.m_fnHandleExpired2 = fnHandleExpired;
			base.IsExpired = new IsExpiredDelegate(this.PeriodIsOver);
			base.HandleExpired = new HandleExpiredDelegate(this.HandleExpired2);
		}

		private bool PeriodIsOver()
		{
			DateTime dtNow = DateTime.Now;
			DateTime dtExpires = DateTime.Parse(this.m_sExpireTime);
			return (dtNow > dtExpires);
		}

		private void HandleExpired2()
		{
			this.m_fnHandleExpired2();

			DateTime dtNextExpires = DateTime.Now.AddMilliseconds(this.m_nPeriodInMilliseconds);
			string sExpire = dtNextExpires.ToString();
			this.m_sExpireTime = sExpire;
		}
	}

	/// <summary>
	/// Encapsulates the logic of a file which must be re-read when it has been modified.  For example, a 
	/// configuration file or a cached xslt file.
	/// </summary>
	public class ModifiedFileExpiringResource : ExpiringResource
	{
		private string m_sLastTime; // I use a string instead of DateTime for reasons of thread safety
		private string m_sPath;
		private HandleExpiredDelegate m_fnHandleExpired2;

		public ModifiedFileExpiringResource(string path, HandleExpiredDelegate fnHandleExpired)
		{
			this.m_sPath = path;
			this.m_sLastTime = DateTime.Now.ToString();
			this.m_fnHandleExpired2 = fnHandleExpired;
			base.IsExpired = new IsExpiredDelegate(this.FileIsModified);
			base.HandleExpired = new HandleExpiredDelegate(this.HandleExpired2);
		}

		private bool FileIsModified()
		{
			DateTime dtModified = System.IO.File.GetLastWriteTime(this.m_sPath);
			DateTime dtLastTime = DateTime.Parse(this.m_sLastTime);
			return (dtLastTime < dtModified);
		}

		private void HandleExpired2()
		{
			this.m_fnHandleExpired2();

			string sLast = DateTime.Now.ToString();
			this.m_sLastTime = sLast;
		}
	}
}
