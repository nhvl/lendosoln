﻿using System.Text.RegularExpressions;

namespace Mismo3Specification
{
    /// <summary>
    /// Constants used by the MISMO 3 data classes.
    /// </summary>
    public static class Mismo3Constants
    {
        /// <summary>
        /// The standard namespace used by MISMO.
        /// </summary>
        public const string MismoNamespace = "http://www.mismo.org/residential/2009/schemas";

        /// <summary>
        /// The namespace for custom LendingQB MISMO extensions.
        /// </summary>
        public const string LqbExtensionNamespace = "http://www.lendingqb.com";

        /// <summary>
        /// The namespace to serialize MCL's proprietary extensions.
        /// </summary>
        public const string MclExtensionNamespace = "inetapi/MISMO3_4_MCL_Extension.xsd";

        /// <summary>
        /// The namespace to serialize industry extensions that support ULAD.
        /// </summary>
        public const string UladExtensionNamespace = "http://www.datamodelextension.org/Schema/ULAD";

        /// <summary>
        /// The uniform resource name used as a prefix in RELATIONSHIP arcrole attributes.
        /// </summary>
        public const string XlinkArcroleURN = "urn:fdc:mismo.org:2009:residential/";

        /// <summary>
        /// The uniform resource name used as a prefix in RELATIONSHIP arcrole attributes for MCL Smart API.
        /// </summary>
        public const string MclXlinkArcroleUrn = "urn:fdc:Meridianlink.com:2017:mortgage/";

        /// <summary>
        /// The namespace used when serializing $$xlink$$ attributes.
        /// </summary>
        public const string XlinkNamespace = "http://www.w3.org/1999/xlink";

        /// <summary>
        /// A collection of MISMO type-validation regexes.
        /// </summary>
        public static class TypeRegexes
        {
            /// <summary>
            /// Matches a valid MISMOAmount value.
            /// </summary>
            public static Regex MismoAmountRegex = new Regex(@"^[\-]?[\d]*(\.[\d]{2,})?$");

            /// <summary>
            /// Matches money amounts with fractional cents.
            /// </summary>
            public static Regex FractionalCentsRegex = new Regex(@"\.[\d]{3,}");

            /// <summary>
            /// Matches a valid MISMODate value.
            /// </summary>
            public static Regex MismoDateRegex = new Regex(@"^[0-9]{4}([\-][0-9]{2}){0,2}([Z]|(([\+]|[\-])[0-9]{2}[:][0-9]{2}))?$");

            /// <summary>
            /// Matches a valid MISMODateTime value.
            /// </summary>
            public static Regex MismoDateTimeRegex = new Regex(@"^[0-9]{4}\-[0-9]{2}\-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}(\.[0-9]{1,9})?(Z|([\+|\-][0-9]{2}:[0-9]{2}))$");

            /// <summary>
            /// Matches a valid MISMODay value.
            /// </summary>
            public static Regex MismoDayRegex = new Regex(@"^---(0[1-9]|[12][0-9]|3[01])(Z|(\+|\-)((0[0-9]|1[0-3]):[0-5][0-9]|14:00))?$");

            /// <summary>
            /// Matches a valid MISMOMonth value.
            /// </summary>
            public static Regex MismoMonthRegex = new Regex(@"^--(0[1-9]|1[0-2])(Z|(\+|\-)((0[0-9]|1[0-3]):[0-5][0-9]|14:00))?$");

            /// <summary>
            /// Matches a valid MISMONumeric value.
            /// </summary>
            public static Regex MismoNumericRegex = new Regex(@"^(\+|\-)?([0-9]+(\.[0-9]*)?|\.[0-9]+)$");

            /// <summary>
            /// Matches non-numeric characters in a string.
            /// </summary>
            public static Regex NonNumericCharacterRegex = new Regex(@"[\D]");

            /// <summary>
            /// Matches a valid MISMOPercent value.
            /// </summary>
            public static Regex MismoPercentRegex = new Regex(@"^(\+|\-)?([0-9]+(\.[0-9]*)?|\.[0-9]+)$");

            /// <summary>
            /// Matches a valid MISMOTime value.
            /// </summary>
            public static Regex MismoTimeRegex = new Regex(@"^(([01][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9](\.[0-9]+)?|(24:00:00(\.0+)?))(Z|(\+|\-)((0[0-9]|1[0-3]):[0-5][0-9]|14:00))?$");

            /// <summary>
            /// Matches a valid MISMOYear value.
            /// </summary>
            public static Regex MismoYearRegex = new Regex(@"^-?([1-9][0-9]{3,}|0[0-9]{3})(Z|(\+|-)((0[0-9]|1[0-3]):[0-5][0-9]|14:00))?$");
        }
    }
}
