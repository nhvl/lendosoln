﻿namespace Mismo3Specification
{
    public static class Mismo3Errors
    {
        private const string ContactLQB = "Please contact your LendingQB system administrator to troubleshoot.";

        public static string InvalidFieldFormat
        {
            get { return $"Failed to convert field to MISMO 3 format. {ContactLQB}"; }
        }

        public static string InvalidYearFormat(string fieldName, string invalidValue)
        {
            return $"Invalid year value '{invalidValue}' for field {fieldName}. This field must contain a four-digit number.";
        }
    }
}
