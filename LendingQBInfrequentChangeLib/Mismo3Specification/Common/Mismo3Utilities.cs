﻿namespace Mismo3Specification
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml.Serialization;

    /// <summary>
    /// Helper methods for serializing the MISMO 3.3 data classes.
    /// </summary>
    public static class Mismo3Utilities
    {
        /// <summary>
        /// Allows declaration of enumerated types that specify both an in-code name and an XML string name, for use with XML serialization.
        /// Example:
        /// [XmlEnum("")]
        /// None,
        /// [XmlEnum("Y")]
        /// Yes,
        /// [XmlEnum("N")]
        /// No,
        /// <para>Copied from Utilities.cs.</para>
        /// </summary>
        /// <param name="e">The enum value to be converted to a string.</param>
        /// <returns>A string representing the enum value.</returns>
        public static string GetXmlEnumName(Enum e)
        {
            Type t = e.GetType();

            FieldInfo info = t.GetField(e.ToString("G"));

            if (!info.IsDefined(typeof(XmlEnumAttribute), false))
            {
                return e.ToString("G");
            }

            object[] o = info.GetCustomAttributes(typeof(XmlEnumAttribute), false);
            var att = (XmlEnumAttribute)o[0];
            return att.Name;
        }

        /// <summary>
        /// Gets the next available (1-based) sequence number for the given collection.
        /// </summary>
        /// <typeparam name="T">The type of item contained by the collection.</typeparam>
        /// <param name="collection">A collection for which the next sequence number is needed.</param>
        /// <returns>The next available sequence number for the collection.</returns>
        public static int GetNextSequenceNumber<T>(IEnumerable<T> collection)
        {
            return collection.Count(item => item != null) + 1;
        }

        /// <summary>
        /// Converts a boolean value to a lowercase string as expected by the MISMO 3.3 serialization.
        /// </summary>
        /// <param name="value">The value to be converted to a string.</param>
        /// <returns>A lowercase string representation of the boolean value.</returns>
        public static string ConvertBoolToString(bool value)
        {
            return value.ToString().ToLower();
        }

        /// <summary>
        /// Generates an xsd:choice violation error message based on the given parent element and list of sub-element choices.
        /// </summary>
        /// <param name="parent">The parent XML element for which the xsd:choice violation occurred.</param>
        /// <param name="choices">The list of sub-elements within the xsd:choice declaration.</param>
        /// <returns>An xsd:choice violation error message based on the given parent element and list of sub-element choices.</returns>
        public static string GetXSDChoiceExceptionMessage(string parent, List<string> choices)
        {
            string error = string.Format("MISMO 3 schema validation error in element {0}. This element has more than one child element among the list of choices, which is an XML schema violation.", parent);

            if (choices.Count > 0)
            {
                string firstChoice = choices[0];
                error += " Choices are as follows: " + firstChoice;

                choices.Remove(firstChoice);
            }

            foreach (string choice in choices)
            {
                error += ", " + choice;
            }

            return error;
        }

        /// <summary>
        /// Determines whether the given list of boolean values contains more true values than the given threshold.
        /// </summary>
        /// <param name="threshold">The permitted number of true values.</param>
        /// <param name="bools">The list of boolean values.</param>
        /// <returns>True if the given list of boolean values contains more true values than the given threshold. Otherwise false.</returns>
        public static bool ExceedsThreshold(int threshold, IEnumerable<bool> bools)
        { 
            return bools.Count(b => b) > threshold; 
        }

        /// <summary>
        /// Creates boolean indicating whether a given string representation is null, empty, or $0.
        /// </summary>
        /// <param name="amount">String value representing monetary value.</param>
        /// <returns>Returns a boolean indicating whether a given string representation is null, empty, or $0.</returns>
        public static bool IsAmountStringBlankOrZero(string amount)
        {
            return string.IsNullOrEmpty(amount) || amount == "0.00" || amount == "$0.00";
        }

        /// <summary>
        /// Truncates any characters in the given string value beyond the max length of 16383. Returns the result.
        /// </summary>
        /// <param name="value">The string to truncate.</param>
        /// <returns>The string value as-is, if the length is within the max (16383). Otherwise the value truncated to max length.</returns>
        public static string EnforceMISMOStringMaxLength(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return value;
            }

            return value.Length > 16383 ? value.Substring(0, 16383) : value;
        }

        /// <summary>
        /// Converts the given string representation of a decimal count to an integer. Fractional digits are truncated.
        /// </summary>
        /// <param name="countValue">The decimal value to convert.</param>
        /// <param name="count">The given count converted to an integer. Fractional digits are truncated.</param>
        /// <returns>True if the given decimal count is converted to an integer. False if the value cannot be converted to an integer or is not a decimal.</returns>
        public static bool ConvertDecimalCountToInt(string countValue, out int count)
        {
            count = -1;
            decimal decimalCount;

            if (decimal.TryParse(countValue, out decimalCount))
            {
                try
                {
                    count = decimal.ToInt32(decimalCount);
                    return true;
                }
                catch (OverflowException)
                {
                    return false;
                }
            }

            return false;
        }
    }
}