namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class INTERIOR
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AtticSpecified
                    || this.BasementSpecified
                    || this.InteriorRoomSummariesSpecified
                    || this.RoomsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ATTIC", Order = 0)]
        public ATTIC Attic { get; set; }
    
        [XmlIgnore]
        public bool AtticSpecified
        {
            get { return this.Attic != null && this.Attic.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("BASEMENT", Order = 1)]
        public BASEMENT Basement { get; set; }
    
        [XmlIgnore]
        public bool BasementSpecified
        {
            get { return this.Basement != null && this.Basement.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("INTERIOR_ROOM_SUMMARIES", Order = 2)]
        public INTERIOR_ROOM_SUMMARIES InteriorRoomSummaries { get; set; }
    
        [XmlIgnore]
        public bool InteriorRoomSummariesSpecified
        {
            get { return this.InteriorRoomSummaries != null && this.InteriorRoomSummaries.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("ROOMS", Order = 3)]
        public ROOMS Rooms { get; set; }
    
        [XmlIgnore]
        public bool RoomsSpecified
        {
            get { return this.Rooms != null && this.Rooms.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public INTERIOR_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
