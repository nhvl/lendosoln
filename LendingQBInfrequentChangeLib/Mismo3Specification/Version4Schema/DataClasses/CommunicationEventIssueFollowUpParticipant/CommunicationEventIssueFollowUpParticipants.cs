namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class COMMUNICATION_EVENT_ISSUE_FOLLOW_UP_PARTICIPANTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CommunicationEventIssueFollowUpParticipantListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("COMMUNICATION_EVENT_ISSUE_FOLLOW_UP_PARTICIPANT", Order = 0)]
        public List<COMMUNICATION_EVENT_ISSUE_FOLLOW_UP_PARTICIPANT> CommunicationEventIssueFollowUpParticipantList { get; set; } = new List<COMMUNICATION_EVENT_ISSUE_FOLLOW_UP_PARTICIPANT>();
    
        [XmlIgnore]
        public bool CommunicationEventIssueFollowUpParticipantListSpecified
        {
            get { return this.CommunicationEventIssueFollowUpParticipantList != null && this.CommunicationEventIssueFollowUpParticipantList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public COMMUNICATION_EVENT_ISSUE_FOLLOW_UP_PARTICIPANTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
