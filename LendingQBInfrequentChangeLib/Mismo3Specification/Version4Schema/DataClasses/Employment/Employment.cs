namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class EMPLOYMENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EmployedAbroadIndicatorSpecified
                    || this.EmploymentBorrowerHomeOfficeIndicatorSpecified
                    || this.EmploymentBorrowerSelfEmployedIndicatorSpecified
                    || this.EmploymentClassificationTypeSpecified
                    || this.EmploymentEndDateSpecified
                    || this.EmploymentMonthlyIncomeAmountSpecified
                    || this.EmploymentMonthsOnJobCountSpecified
                    || this.EmploymentPositionDescriptionSpecified
                    || this.EmploymentReportedDateSpecified
                    || this.EmploymentSelfOwnedBusinessOperationsEndDateSpecified
                    || this.EmploymentSelfOwnedBusinessOperationsStartDateSpecified
                    || this.EmploymentStartDateSpecified
                    || this.EmploymentStatusTypeSpecified
                    || this.EmploymentTimeInLineOfWorkMonthsCountSpecified
                    || this.EmploymentTimeInLineOfWorkYearsCountSpecified
                    || this.EmploymentTypeOfBusinessDescriptionSpecified
                    || this.EmploymentYearsOnJobCountSpecified
                    || this.OwnershipInterestTypeSpecified
                    || this.OwnershipInterestTypeOtherDescriptionSpecified
                    || this.SpecialBorrowerEmployerRelationshipIndicatorSpecified
                    || this.SpecialBorrowerEmployerRelationshipTypeSpecified
                    || this.SpecialBorrowerEmployerRelationshipTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("EmployedAbroadIndicator", Order = 0)]
        public MISMOIndicator EmployedAbroadIndicator { get; set; }
    
        [XmlIgnore]
        public bool EmployedAbroadIndicatorSpecified
        {
            get { return this.EmployedAbroadIndicator != null; }
            set { }
        }
    
        [XmlElement("EmploymentBorrowerHomeOfficeIndicator", Order = 1)]
        public MISMOIndicator EmploymentBorrowerHomeOfficeIndicator { get; set; }
    
        [XmlIgnore]
        public bool EmploymentBorrowerHomeOfficeIndicatorSpecified
        {
            get { return this.EmploymentBorrowerHomeOfficeIndicator != null; }
            set { }
        }
    
        [XmlElement("EmploymentBorrowerSelfEmployedIndicator", Order = 2)]
        public MISMOIndicator EmploymentBorrowerSelfEmployedIndicator { get; set; }
    
        [XmlIgnore]
        public bool EmploymentBorrowerSelfEmployedIndicatorSpecified
        {
            get { return this.EmploymentBorrowerSelfEmployedIndicator != null; }
            set { }
        }
    
        [XmlElement("EmploymentClassificationType", Order = 3)]
        public MISMOEnum<EmploymentClassificationBase> EmploymentClassificationType { get; set; }
    
        [XmlIgnore]
        public bool EmploymentClassificationTypeSpecified
        {
            get { return this.EmploymentClassificationType != null; }
            set { }
        }
    
        [XmlElement("EmploymentEndDate", Order = 4)]
        public MISMODate EmploymentEndDate { get; set; }
    
        [XmlIgnore]
        public bool EmploymentEndDateSpecified
        {
            get { return this.EmploymentEndDate != null; }
            set { }
        }
    
        [XmlElement("EmploymentMonthlyIncomeAmount", Order = 5)]
        public MISMOAmount EmploymentMonthlyIncomeAmount { get; set; }
    
        [XmlIgnore]
        public bool EmploymentMonthlyIncomeAmountSpecified
        {
            get { return this.EmploymentMonthlyIncomeAmount != null; }
            set { }
        }
    
        [XmlElement("EmploymentMonthsOnJobCount", Order = 6)]
        public MISMOCount EmploymentMonthsOnJobCount { get; set; }
    
        [XmlIgnore]
        public bool EmploymentMonthsOnJobCountSpecified
        {
            get { return this.EmploymentMonthsOnJobCount != null; }
            set { }
        }
    
        [XmlElement("EmploymentPositionDescription", Order = 7)]
        public MISMOString EmploymentPositionDescription { get; set; }
    
        [XmlIgnore]
        public bool EmploymentPositionDescriptionSpecified
        {
            get { return this.EmploymentPositionDescription != null; }
            set { }
        }
    
        [XmlElement("EmploymentReportedDate", Order = 8)]
        public MISMODate EmploymentReportedDate { get; set; }
    
        [XmlIgnore]
        public bool EmploymentReportedDateSpecified
        {
            get { return this.EmploymentReportedDate != null; }
            set { }
        }
    
        [XmlElement("EmploymentSelfOwnedBusinessOperationsEndDate", Order = 9)]
        public MISMODate EmploymentSelfOwnedBusinessOperationsEndDate { get; set; }
    
        [XmlIgnore]
        public bool EmploymentSelfOwnedBusinessOperationsEndDateSpecified
        {
            get { return this.EmploymentSelfOwnedBusinessOperationsEndDate != null; }
            set { }
        }
    
        [XmlElement("EmploymentSelfOwnedBusinessOperationsStartDate", Order = 10)]
        public MISMODate EmploymentSelfOwnedBusinessOperationsStartDate { get; set; }
    
        [XmlIgnore]
        public bool EmploymentSelfOwnedBusinessOperationsStartDateSpecified
        {
            get { return this.EmploymentSelfOwnedBusinessOperationsStartDate != null; }
            set { }
        }
    
        [XmlElement("EmploymentStartDate", Order = 11)]
        public MISMODate EmploymentStartDate { get; set; }
    
        [XmlIgnore]
        public bool EmploymentStartDateSpecified
        {
            get { return this.EmploymentStartDate != null; }
            set { }
        }
    
        [XmlElement("EmploymentStatusType", Order = 12)]
        public MISMOEnum<EmploymentStatusBase> EmploymentStatusType { get; set; }
    
        [XmlIgnore]
        public bool EmploymentStatusTypeSpecified
        {
            get { return this.EmploymentStatusType != null; }
            set { }
        }
    
        [XmlElement("EmploymentTimeInLineOfWorkMonthsCount", Order = 13)]
        public MISMOCount EmploymentTimeInLineOfWorkMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool EmploymentTimeInLineOfWorkMonthsCountSpecified
        {
            get { return this.EmploymentTimeInLineOfWorkMonthsCount != null; }
            set { }
        }
    
        [XmlElement("EmploymentTimeInLineOfWorkYearsCount", Order = 14)]
        public MISMOCount EmploymentTimeInLineOfWorkYearsCount { get; set; }
    
        [XmlIgnore]
        public bool EmploymentTimeInLineOfWorkYearsCountSpecified
        {
            get { return this.EmploymentTimeInLineOfWorkYearsCount != null; }
            set { }
        }
    
        [XmlElement("EmploymentTypeOfBusinessDescription", Order = 15)]
        public MISMOString EmploymentTypeOfBusinessDescription { get; set; }
    
        [XmlIgnore]
        public bool EmploymentTypeOfBusinessDescriptionSpecified
        {
            get { return this.EmploymentTypeOfBusinessDescription != null; }
            set { }
        }
    
        [XmlElement("EmploymentYearsOnJobCount", Order = 16)]
        public MISMOCount EmploymentYearsOnJobCount { get; set; }
    
        [XmlIgnore]
        public bool EmploymentYearsOnJobCountSpecified
        {
            get { return this.EmploymentYearsOnJobCount != null; }
            set { }
        }
    
        [XmlElement("OwnershipInterestType", Order = 17)]
        public MISMOEnum<OwnershipInterestBase> OwnershipInterestType { get; set; }
    
        [XmlIgnore]
        public bool OwnershipInterestTypeSpecified
        {
            get { return this.OwnershipInterestType != null; }
            set { }
        }
    
        [XmlElement("OwnershipInterestTypeOtherDescription", Order = 18)]
        public MISMOString OwnershipInterestTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool OwnershipInterestTypeOtherDescriptionSpecified
        {
            get { return this.OwnershipInterestTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("SpecialBorrowerEmployerRelationshipIndicator", Order = 19)]
        public MISMOIndicator SpecialBorrowerEmployerRelationshipIndicator { get; set; }
    
        [XmlIgnore]
        public bool SpecialBorrowerEmployerRelationshipIndicatorSpecified
        {
            get { return this.SpecialBorrowerEmployerRelationshipIndicator != null; }
            set { }
        }
    
        [XmlElement("SpecialBorrowerEmployerRelationshipType", Order = 20)]
        public MISMOEnum<SpecialBorrowerEmployerRelationshipBase> SpecialBorrowerEmployerRelationshipType { get; set; }
    
        [XmlIgnore]
        public bool SpecialBorrowerEmployerRelationshipTypeSpecified
        {
            get { return this.SpecialBorrowerEmployerRelationshipType != null; }
            set { }
        }
    
        [XmlElement("SpecialBorrowerEmployerRelationshipTypeOtherDescription", Order = 21)]
        public MISMOString SpecialBorrowerEmployerRelationshipTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool SpecialBorrowerEmployerRelationshipTypeOtherDescriptionSpecified
        {
            get { return this.SpecialBorrowerEmployerRelationshipTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 22)]
        public EMPLOYMENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
