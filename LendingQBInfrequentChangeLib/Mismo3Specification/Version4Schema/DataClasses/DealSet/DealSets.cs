namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class DEAL_SETS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DealSetListSpecified
                    || this.DealSetServicesSpecified
                    || this.PartiesSpecified
                    || this.RelationshipsSpecified
                    || this.VerificationDataSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DEAL_SET", Order = 0)]
        public List<DEAL_SET> DealSetList { get; set; } = new List<DEAL_SET>();
    
        [XmlIgnore]
        public bool DealSetListSpecified
        {
            get { return this.DealSetList != null && this.DealSetList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("DEAL_SET_SERVICES", Order = 1)]
        public DEAL_SET_SERVICES DealSetServices { get; set; }
    
        [XmlIgnore]
        public bool DealSetServicesSpecified
        {
            get { return this.DealSetServices != null && this.DealSetServices.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PARTIES", Order = 2)]
        public PARTIES Parties { get; set; }
    
        [XmlIgnore]
        public bool PartiesSpecified
        {
            get { return this.Parties != null && this.Parties.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("RELATIONSHIPS", Order = 3)]
        public RELATIONSHIPS Relationships { get; set; }
    
        [XmlIgnore]
        public bool RelationshipsSpecified
        {
            get { return this.Relationships != null && this.Relationships.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("VERIFICATION_DATA", Order = 4)]
        public VERIFICATION_DATA VerificationData { get; set; }
    
        [XmlIgnore]
        public bool VerificationDataSpecified
        {
            get { return this.VerificationData != null && this.VerificationData.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public DEAL_SETS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
