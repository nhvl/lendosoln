namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class DEAL_SET
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AchSpecified
                    || this.CashRemittanceSummaryNotificationSpecified
                    || this.DealSetExpensesSpecified
                    || this.DealSetIncomesSpecified
                    || this.DealsSpecified
                    || this.InvestorFeaturesSpecified
                    || this.PartiesSpecified
                    || this.PoolSpecified
                    || this.RelationshipsSpecified
                    || this.ServicerReportingSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ACH", Order = 0)]
        public ACH Ach { get; set; }
    
        [XmlIgnore]
        public bool AchSpecified
        {
            get { return this.Ach != null && this.Ach.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CASH_REMITTANCE_SUMMARY_NOTIFICATION", Order = 1)]
        public CASH_REMITTANCE_SUMMARY_NOTIFICATION CashRemittanceSummaryNotification { get; set; }
    
        [XmlIgnore]
        public bool CashRemittanceSummaryNotificationSpecified
        {
            get { return this.CashRemittanceSummaryNotification != null && this.CashRemittanceSummaryNotification.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("DEAL_SET_EXPENSES", Order = 2)]
        public DEAL_SET_EXPENSES DealSetExpenses { get; set; }
    
        [XmlIgnore]
        public bool DealSetExpensesSpecified
        {
            get { return this.DealSetExpenses != null && this.DealSetExpenses.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("DEAL_SET_INCOMES", Order = 3)]
        public DEAL_SET_INCOMES DealSetIncomes { get; set; }
    
        [XmlIgnore]
        public bool DealSetIncomesSpecified
        {
            get { return this.DealSetIncomes != null && this.DealSetIncomes.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("DEALS", Order = 4)]
        public DEALS Deals { get; set; }
    
        [XmlIgnore]
        public bool DealsSpecified
        {
            get { return this.Deals != null && this.Deals.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("INVESTOR_FEATURES", Order = 5)]
        public INVESTOR_FEATURES InvestorFeatures { get; set; }
    
        [XmlIgnore]
        public bool InvestorFeaturesSpecified
        {
            get { return this.InvestorFeatures != null && this.InvestorFeatures.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PARTIES", Order = 6)]
        public PARTIES Parties { get; set; }
    
        [XmlIgnore]
        public bool PartiesSpecified
        {
            get { return this.Parties != null && this.Parties.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("POOL", Order = 7)]
        public POOL Pool { get; set; }
    
        [XmlIgnore]
        public bool PoolSpecified
        {
            get { return this.Pool != null && this.Pool.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("RELATIONSHIPS", Order = 8)]
        public RELATIONSHIPS Relationships { get; set; }
    
        [XmlIgnore]
        public bool RelationshipsSpecified
        {
            get { return this.Relationships != null && this.Relationships.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SERVICER_REPORTING", Order = 9)]
        public SERVICER_REPORTING ServicerReporting { get; set; }
    
        [XmlIgnore]
        public bool ServicerReportingSpecified
        {
            get { return this.ServicerReporting != null && this.ServicerReporting.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 10)]
        public DEAL_SET_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
