namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    public class LICENSE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                if (Mismo3Utilities.ExceedsThreshold(1,
                    new List<bool> { this.AppraiserLicenseSpecified, this.PropertyLicenseSpecified }))
                {
                    throw new System.Xml.Schema.XmlSchemaValidationException(
                        Mismo3Utilities.GetXSDChoiceExceptionMessage(
                        "LICENSE",
                        new List<string> { "APPRAISER_LICENSE", "PROPERTY_LICENSE" }));
                }

                return this.AppraiserLicenseSpecified
                    || this.PropertyLicenseSpecified
                    || this.LicenseDetailSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("APPRAISER_LICENSE", Order = 0)]
        public APPRAISER_LICENSE AppraiserLicense { get; set; }
    
        [XmlIgnore]
        public bool AppraiserLicenseSpecified
        {
            get { return this.AppraiserLicense != null && this.AppraiserLicense.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PROPERTY_LICENSE", Order = 1)]
        public PROPERTY_LICENSE PropertyLicense { get; set; }
    
        [XmlIgnore]
        public bool PropertyLicenseSpecified
        {
            get { return this.PropertyLicense != null && this.PropertyLicense.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("LICENSE_DETAIL", Order = 2)]
        public LICENSE_DETAIL LicenseDetail { get; set; }
    
        [XmlIgnore]
        public bool LicenseDetailSpecified
        {
            get { return this.LicenseDetail != null && this.LicenseDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public LICENSE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
