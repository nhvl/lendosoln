namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class LICENSES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LicenseListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LICENSE", Order = 0)]
        public List<LICENSE> LicenseList { get; set; } = new List<LICENSE>();
    
        [XmlIgnore]
        public bool LicenseListSpecified
        {
            get { return this.LicenseList != null && this.LicenseList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public LICENSES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
