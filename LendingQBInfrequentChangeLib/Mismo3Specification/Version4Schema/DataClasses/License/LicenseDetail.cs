namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class LICENSE_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LicenseAuthorityLevelTypeSpecified
                    || this.LicenseAuthorityLevelTypeOtherDescriptionSpecified
                    || this.LicenseExemptIndicatorSpecified
                    || this.LicenseExpirationDateSpecified
                    || this.LicenseIdentifierSpecified
                    || this.LicenseIssueDateSpecified
                    || this.LicenseIssuingAuthorityNameSpecified
                    || this.LicenseIssuingAuthorityStateCodeSpecified
                    || this.LicenseIssuingAuthorityStateNameSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LicenseAuthorityLevelType", Order = 0)]
        public MISMOEnum<LicenseAuthorityLevelBase> LicenseAuthorityLevelType { get; set; }
    
        [XmlIgnore]
        public bool LicenseAuthorityLevelTypeSpecified
        {
            get { return this.LicenseAuthorityLevelType != null; }
            set { }
        }
    
        [XmlElement("LicenseAuthorityLevelTypeOtherDescription", Order = 1)]
        public MISMOString LicenseAuthorityLevelTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool LicenseAuthorityLevelTypeOtherDescriptionSpecified
        {
            get { return this.LicenseAuthorityLevelTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("LicenseExemptIndicator", Order = 2)]
        public MISMOIndicator LicenseExemptIndicator { get; set; }
    
        [XmlIgnore]
        public bool LicenseExemptIndicatorSpecified
        {
            get { return this.LicenseExemptIndicator != null; }
            set { }
        }
    
        [XmlElement("LicenseExpirationDate", Order = 3)]
        public MISMODate LicenseExpirationDate { get; set; }
    
        [XmlIgnore]
        public bool LicenseExpirationDateSpecified
        {
            get { return this.LicenseExpirationDate != null; }
            set { }
        }
    
        [XmlElement("LicenseIdentifier", Order = 4)]
        public MISMOIdentifier LicenseIdentifier { get; set; }
    
        [XmlIgnore]
        public bool LicenseIdentifierSpecified
        {
            get { return this.LicenseIdentifier != null; }
            set { }
        }
    
        [XmlElement("LicenseIssueDate", Order = 5)]
        public MISMODate LicenseIssueDate { get; set; }
    
        [XmlIgnore]
        public bool LicenseIssueDateSpecified
        {
            get { return this.LicenseIssueDate != null; }
            set { }
        }
    
        [XmlElement("LicenseIssuingAuthorityName", Order = 6)]
        public MISMOString LicenseIssuingAuthorityName { get; set; }
    
        [XmlIgnore]
        public bool LicenseIssuingAuthorityNameSpecified
        {
            get { return this.LicenseIssuingAuthorityName != null; }
            set { }
        }
    
        [XmlElement("LicenseIssuingAuthorityStateCode", Order = 7)]
        public MISMOCode LicenseIssuingAuthorityStateCode { get; set; }
    
        [XmlIgnore]
        public bool LicenseIssuingAuthorityStateCodeSpecified
        {
            get { return this.LicenseIssuingAuthorityStateCode != null; }
            set { }
        }
    
        [XmlElement("LicenseIssuingAuthorityStateName", Order = 8)]
        public MISMOString LicenseIssuingAuthorityStateName { get; set; }
    
        [XmlIgnore]
        public bool LicenseIssuingAuthorityStateNameSpecified
        {
            get { return this.LicenseIssuingAuthorityStateName != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 9)]
        public LICENSE_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
