namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class SITE_VIEWS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SiteViewListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SITE_VIEW", Order = 0)]
        public List<SITE_VIEW> SiteViewList { get; set; } = new List<SITE_VIEW>();
    
        [XmlIgnore]
        public bool SiteViewListSpecified
        {
            get { return this.SiteViewList != null && this.SiteViewList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public SITE_VIEWS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
