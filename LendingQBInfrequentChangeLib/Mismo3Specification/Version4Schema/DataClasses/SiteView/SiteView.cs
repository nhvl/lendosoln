namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SITE_VIEW
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.InfluenceImpactTypeSpecified
                    || this.SiteViewDescriptionSpecified
                    || this.SiteViewTypeSpecified
                    || this.SiteViewTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("InfluenceImpactType", Order = 0)]
        public MISMOEnum<InfluenceImpactBase> InfluenceImpactType { get; set; }
    
        [XmlIgnore]
        public bool InfluenceImpactTypeSpecified
        {
            get { return this.InfluenceImpactType != null; }
            set { }
        }
    
        [XmlElement("SiteViewDescription", Order = 1)]
        public MISMOString SiteViewDescription { get; set; }
    
        [XmlIgnore]
        public bool SiteViewDescriptionSpecified
        {
            get { return this.SiteViewDescription != null; }
            set { }
        }
    
        [XmlElement("SiteViewType", Order = 2)]
        public MISMOEnum<SiteViewBase> SiteViewType { get; set; }
    
        [XmlIgnore]
        public bool SiteViewTypeSpecified
        {
            get { return this.SiteViewType != null; }
            set { }
        }
    
        [XmlElement("SiteViewTypeOtherDescription", Order = 3)]
        public MISMOString SiteViewTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool SiteViewTypeOtherDescriptionSpecified
        {
            get { return this.SiteViewTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public SITE_VIEW_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
