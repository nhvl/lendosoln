namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class REAL_ESTATE_AGENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.RealEstateAgentTypeSpecified
                    || this.RealEstateAgentTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("RealEstateAgentType", Order = 0)]
        public MISMOEnum<RealEstateAgentBase> RealEstateAgentType { get; set; }
    
        [XmlIgnore]
        public bool RealEstateAgentTypeSpecified
        {
            get { return this.RealEstateAgentType != null; }
            set { }
        }
    
        [XmlElement("RealEstateAgentTypeOtherDescription", Order = 1)]
        public MISMOString RealEstateAgentTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool RealEstateAgentTypeOtherDescriptionSpecified
        {
            get { return this.RealEstateAgentTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public REAL_ESTATE_AGENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
