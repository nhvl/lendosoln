namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class LOAN_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AcceleratedPaymentProgramIndicatorSpecified
                    || this.ACHIndicatorSpecified
                    || this.ActiveLitigationIndicatorSpecified
                    || this.ApplicationReceivedDateSpecified
                    || this.ArmsLengthIndicatorSpecified
                    || this.AssumabilityIndicatorSpecified
                    || this.AssumedIndicatorSpecified
                    || this.BalloonGuaranteedRefinancingIndicatorSpecified
                    || this.BalloonIndicatorSpecified
                    || this.BalloonPaymentAmountSpecified
                    || this.BalloonPaymentMaximumAmountSpecified
                    || this.BalloonPaymentMinimumAmountSpecified
                    || this.BalloonResetIndicatorSpecified
                    || this.BalloonResetOptionIndicatorSpecified
                    || this.BelowMarketSubordinateFinancingIndicatorSpecified
                    || this.BiweeklyToMonthlyOnDefaultIndicatorSpecified
                    || this.BorrowerCountSpecified
                    || this.BuydownTemporarySubsidyFundingIndicatorSpecified
                    || this.CapitalizedFeesIndicatorSpecified
                    || this.CapitalizedLoanIndicatorSpecified
                    || this.ClosingCostFinancedIndicatorSpecified
                    || this.ClosingDisclosureWaitingPeriodWaivedIndicatorSpecified
                    || this.CollateralPledgedToNameSpecified
                    || this.ConcurrentOriginationIndicatorSpecified
                    || this.ConcurrentOriginationLenderIndicatorSpecified
                    || this.ConformingIndicatorSpecified
                    || this.ConstructionLoanIndicatorSpecified
                    || this.ConversionOfContractForDeedIndicatorSpecified
                    || this.ConvertibleIndicatorSpecified
                    || this.CreditEnhancementIndicatorSpecified
                    || this.CreditorServicingOfLoanStatementTypeSpecified
                    || this.CreditorServicingOfLoanStatementTypeOtherDescriptionSpecified
                    || this.CurrentInterestRatePercentSpecified
                    || this.DeferredInterestBalanceAmountSpecified
                    || this.DemandFeatureIndicatorSpecified
                    || this.EligibleForLenderPaidMIIndicatorSpecified
                    || this.EmployeeLoanProgramIndicatorSpecified
                    || this.EnergyRelatedImprovementsIndicatorSpecified
                    || this.ENoteCertifiedIndicatorSpecified
                    || this.ENoteIndicatorSpecified
                    || this.EscrowAbsenceReasonTypeSpecified
                    || this.EscrowAbsenceReasonTypeOtherDescriptionSpecified
                    || this.EscrowAccountLenderRequirementTypeSpecified
                    || this.EscrowAccountRequestedIndicatorSpecified
                    || this.EscrowIndicatorSpecified
                    || this.EscrowNotAllowedIndicatorSpecified
                    || this.EscrowsElectedByBorrowerIndicatorSpecified
                    || this.HELOCIndicatorSpecified
                    || this.HigherPricedMortgageLoanIndicatorSpecified
                    || this.HomeBuyersHomeownershipEducationCertificateIndicatorSpecified
                    || this.InitialFixedPeriodEffectiveMonthsCountSpecified
                    || this.InterestCreditedToBorrowerIndicatorSpecified
                    || this.InterestOnlyIndicatorSpecified
                    || this.InterestRateIncreaseIndicatorSpecified
                    || this.JurisdictionHighCostLoanIndicatorSpecified
                    || this.LenderPlacedHazardInsuranceIndicatorSpecified
                    || this.LenderSelfInsuredIndicatorSpecified
                    || this.LendingLimitAmountSpecified
                    || this.LendingLimitTypeSpecified
                    || this.LendingLimitTypeOtherDescriptionSpecified
                    || this.LienHolderSameAsSubjectLoanIndicatorSpecified
                    || this.LoanAffordableIndicatorSpecified
                    || this.LoanAgeMonthsCountSpecified
                    || this.LoanAmountIncreaseIndicatorSpecified
                    || this.LoanApprovalDateSpecified
                    || this.LoanApprovalExpirationDateSpecified
                    || this.LoanApprovalPeriodDaysCountSpecified
                    || this.LoanClosingStatusTypeSpecified
                    || this.LoanCommitmentDateSpecified
                    || this.LoanCommitmentExpirationDateSpecified
                    || this.LoanCommitmentPeriodDaysCountSpecified
                    || this.LoanEstimateWaitingPeriodWaivedIndicatorSpecified
                    || this.LoanFundingDateSpecified
                    || this.LoanLevelCreditScoreIndicatorSpecified
                    || this.LoanRepaymentTypeSpecified
                    || this.LoanRepaymentTypeOtherDescriptionSpecified
                    || this.LoanSaleFundingDateSpecified
                    || this.LoanSecuredByTypeSpecified
                    || this.LoanSecuredByTypeOtherDescriptionSpecified
                    || this.LoanSecuredIndicatorSpecified
                    || this.LoanSellerProvidedInvestmentRatingDescriptionSpecified
                    || this.LostNoteAffidavitIndicatorSpecified
                    || this.MICoverageExistsIndicatorSpecified
                    || this.MIRequiredIndicatorSpecified
                    || this.MortgageModificationIndicatorSpecified
                    || this.NegativeAmortizationIndicatorSpecified
                    || this.OpenEndCreditIndicatorSpecified
                    || this.OriginalLoanStandardEscrowProvisionsIndicatorSpecified
                    || this.OverdueInterestAmountSpecified
                    || this.PaymentIncreaseIndicatorSpecified
                    || this.PiggybackLoanIndicatorSpecified
                    || this.PoolIdentifierSpecified
                    || this.PoolIssueDateSpecified
                    || this.PoolPrefixIdentifierSpecified
                    || this.PrepaymentPenaltyIndicatorSpecified
                    || this.PrepaymentPenaltyWaivedIndicatorSpecified
                    || this.PrepaymentRestrictionIndicatorSpecified
                    || this.PropertiesFinancedByLenderCountSpecified
                    || this.PropertyInspectionWaiverIndicatorSpecified
                    || this.QualifiedMortgageIndicatorSpecified
                    || this.RecoverableCorporateAdvanceBalanceFromBorrowerAmountSpecified
                    || this.RecoverableCorporateAdvanceBalanceFromThirdPartyAmountSpecified
                    || this.RefundOfOverpaidInterestCalendarYearAmountSpecified
                    || this.RegulationZHighCostLoanIndicatorSpecified
                    || this.RelocationLoanIndicatorSpecified
                    || this.RemainingUnearnedInterestAmountSpecified
                    || this.RenovationLoanIndicatorSpecified
                    || this.RESPAConformingYearTypeSpecified
                    || this.RESPAConformingYearTypeOtherDescriptionSpecified
                    || this.ReverseMortgageIndicatorSpecified
                    || this.RightOfRescissionWaitingPeriodWaivedIndicatorSpecified
                    || this.SCRAIndicatorSpecified
                    || this.SCRAReliefForbearanceRequestedIndicatorSpecified
                    || this.SCRAReliefStatusTypeSpecified
                    || this.SCRAReliefStatusTypeOtherDescriptionSpecified
                    || this.SeasonalPaymentFeatureDescriptionSpecified
                    || this.SeasonalPaymentFeatureIndicatorSpecified
                    || this.ServicingTransferProhibitedIndicatorSpecified
                    || this.ServicingTransferStatusTypeSpecified
                    || this.SharedAppreciationIndicatorSpecified
                    || this.SharedEquityIndicatorSpecified
                    || this.SignedAuthorizationToRequestTaxRecordsIndicatorSpecified
                    || this.StepPaymentsFeatureDescriptionSpecified
                    || this.TaxRecordsObtainedIndicatorSpecified
                    || this.TimelyPaymentRateReductionIndicatorSpecified
                    || this.TotalMortgagedPropertiesCountSpecified
                    || this.TotalSalesConcessionAmountSpecified
                    || this.TotalSubordinateFinancingAmountSpecified
                    || this.WarehouseLenderIndicatorSpecified
                    || this.WorkoutActiveIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AcceleratedPaymentProgramIndicator", Order = 0)]
        public MISMOIndicator AcceleratedPaymentProgramIndicator { get; set; }
    
        [XmlIgnore]
        public bool AcceleratedPaymentProgramIndicatorSpecified
        {
            get { return this.AcceleratedPaymentProgramIndicator != null; }
            set { }
        }
    
        [XmlElement("ACHIndicator", Order = 1)]
        public MISMOIndicator ACHIndicator { get; set; }
    
        [XmlIgnore]
        public bool ACHIndicatorSpecified
        {
            get { return this.ACHIndicator != null; }
            set { }
        }
    
        [XmlElement("ActiveLitigationIndicator", Order = 2)]
        public MISMOIndicator ActiveLitigationIndicator { get; set; }
    
        [XmlIgnore]
        public bool ActiveLitigationIndicatorSpecified
        {
            get { return this.ActiveLitigationIndicator != null; }
            set { }
        }
    
        [XmlElement("ApplicationReceivedDate", Order = 3)]
        public MISMODate ApplicationReceivedDate { get; set; }
    
        [XmlIgnore]
        public bool ApplicationReceivedDateSpecified
        {
            get { return this.ApplicationReceivedDate != null; }
            set { }
        }
    
        [XmlElement("ArmsLengthIndicator", Order = 4)]
        public MISMOIndicator ArmsLengthIndicator { get; set; }
    
        [XmlIgnore]
        public bool ArmsLengthIndicatorSpecified
        {
            get { return this.ArmsLengthIndicator != null; }
            set { }
        }
    
        [XmlElement("AssumabilityIndicator", Order = 5)]
        public MISMOIndicator AssumabilityIndicator { get; set; }
    
        [XmlIgnore]
        public bool AssumabilityIndicatorSpecified
        {
            get { return this.AssumabilityIndicator != null; }
            set { }
        }
    
        [XmlElement("AssumedIndicator", Order = 6)]
        public MISMOIndicator AssumedIndicator { get; set; }
    
        [XmlIgnore]
        public bool AssumedIndicatorSpecified
        {
            get { return this.AssumedIndicator != null; }
            set { }
        }
    
        [XmlElement("BalloonGuaranteedRefinancingIndicator", Order = 7)]
        public MISMOIndicator BalloonGuaranteedRefinancingIndicator { get; set; }
    
        [XmlIgnore]
        public bool BalloonGuaranteedRefinancingIndicatorSpecified
        {
            get { return this.BalloonGuaranteedRefinancingIndicator != null; }
            set { }
        }
    
        [XmlElement("BalloonIndicator", Order = 8)]
        public MISMOIndicator BalloonIndicator { get; set; }
    
        [XmlIgnore]
        public bool BalloonIndicatorSpecified
        {
            get { return this.BalloonIndicator != null; }
            set { }
        }
    
        [XmlElement("BalloonPaymentAmount", Order = 9)]
        public MISMOAmount BalloonPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool BalloonPaymentAmountSpecified
        {
            get { return this.BalloonPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("BalloonPaymentMaximumAmount", Order = 10)]
        public MISMOAmount BalloonPaymentMaximumAmount { get; set; }
    
        [XmlIgnore]
        public bool BalloonPaymentMaximumAmountSpecified
        {
            get { return this.BalloonPaymentMaximumAmount != null; }
            set { }
        }
    
        [XmlElement("BalloonPaymentMinimumAmount", Order = 11)]
        public MISMOAmount BalloonPaymentMinimumAmount { get; set; }
    
        [XmlIgnore]
        public bool BalloonPaymentMinimumAmountSpecified
        {
            get { return this.BalloonPaymentMinimumAmount != null; }
            set { }
        }
    
        [XmlElement("BalloonResetIndicator", Order = 12)]
        public MISMOIndicator BalloonResetIndicator { get; set; }
    
        [XmlIgnore]
        public bool BalloonResetIndicatorSpecified
        {
            get { return this.BalloonResetIndicator != null; }
            set { }
        }
    
        [XmlElement("BalloonResetOptionIndicator", Order = 13)]
        public MISMOIndicator BalloonResetOptionIndicator { get; set; }
    
        [XmlIgnore]
        public bool BalloonResetOptionIndicatorSpecified
        {
            get { return this.BalloonResetOptionIndicator != null; }
            set { }
        }
    
        [XmlElement("BelowMarketSubordinateFinancingIndicator", Order = 14)]
        public MISMOIndicator BelowMarketSubordinateFinancingIndicator { get; set; }
    
        [XmlIgnore]
        public bool BelowMarketSubordinateFinancingIndicatorSpecified
        {
            get { return this.BelowMarketSubordinateFinancingIndicator != null; }
            set { }
        }
    
        [XmlElement("BiweeklyToMonthlyOnDefaultIndicator", Order = 15)]
        public MISMOIndicator BiweeklyToMonthlyOnDefaultIndicator { get; set; }
    
        [XmlIgnore]
        public bool BiweeklyToMonthlyOnDefaultIndicatorSpecified
        {
            get { return this.BiweeklyToMonthlyOnDefaultIndicator != null; }
            set { }
        }
    
        [XmlElement("BorrowerCount", Order = 16)]
        public MISMOCount BorrowerCount { get; set; }
    
        [XmlIgnore]
        public bool BorrowerCountSpecified
        {
            get { return this.BorrowerCount != null; }
            set { }
        }
    
        [XmlElement("BuydownTemporarySubsidyFundingIndicator", Order = 17)]
        public MISMOIndicator BuydownTemporarySubsidyFundingIndicator { get; set; }
    
        [XmlIgnore]
        public bool BuydownTemporarySubsidyFundingIndicatorSpecified
        {
            get { return this.BuydownTemporarySubsidyFundingIndicator != null; }
            set { }
        }
    
        [XmlElement("CapitalizedFeesIndicator", Order = 18)]
        public MISMOIndicator CapitalizedFeesIndicator { get; set; }
    
        [XmlIgnore]
        public bool CapitalizedFeesIndicatorSpecified
        {
            get { return this.CapitalizedFeesIndicator != null; }
            set { }
        }
    
        [XmlElement("CapitalizedLoanIndicator", Order = 19)]
        public MISMOIndicator CapitalizedLoanIndicator { get; set; }
    
        [XmlIgnore]
        public bool CapitalizedLoanIndicatorSpecified
        {
            get { return this.CapitalizedLoanIndicator != null; }
            set { }
        }
    
        [XmlElement("ClosingCostFinancedIndicator", Order = 20)]
        public MISMOIndicator ClosingCostFinancedIndicator { get; set; }
    
        [XmlIgnore]
        public bool ClosingCostFinancedIndicatorSpecified
        {
            get { return this.ClosingCostFinancedIndicator != null; }
            set { }
        }
    
        [XmlElement("ClosingDisclosureWaitingPeriodWaivedIndicator", Order = 21)]
        public MISMOIndicator ClosingDisclosureWaitingPeriodWaivedIndicator { get; set; }
    
        [XmlIgnore]
        public bool ClosingDisclosureWaitingPeriodWaivedIndicatorSpecified
        {
            get { return this.ClosingDisclosureWaitingPeriodWaivedIndicator != null; }
            set { }
        }
    
        [XmlElement("CollateralPledgedToName", Order = 22)]
        public MISMOString CollateralPledgedToName { get; set; }
    
        [XmlIgnore]
        public bool CollateralPledgedToNameSpecified
        {
            get { return this.CollateralPledgedToName != null; }
            set { }
        }
    
        [XmlElement("ConcurrentOriginationIndicator", Order = 23)]
        public MISMOIndicator ConcurrentOriginationIndicator { get; set; }
    
        [XmlIgnore]
        public bool ConcurrentOriginationIndicatorSpecified
        {
            get { return this.ConcurrentOriginationIndicator != null; }
            set { }
        }
    
        [XmlElement("ConcurrentOriginationLenderIndicator", Order = 24)]
        public MISMOIndicator ConcurrentOriginationLenderIndicator { get; set; }
    
        [XmlIgnore]
        public bool ConcurrentOriginationLenderIndicatorSpecified
        {
            get { return this.ConcurrentOriginationLenderIndicator != null; }
            set { }
        }
    
        [XmlElement("ConformingIndicator", Order = 25)]
        public MISMOIndicator ConformingIndicator { get; set; }
    
        [XmlIgnore]
        public bool ConformingIndicatorSpecified
        {
            get { return this.ConformingIndicator != null; }
            set { }
        }
    
        [XmlElement("ConstructionLoanIndicator", Order = 26)]
        public MISMOIndicator ConstructionLoanIndicator { get; set; }
    
        [XmlIgnore]
        public bool ConstructionLoanIndicatorSpecified
        {
            get { return this.ConstructionLoanIndicator != null; }
            set { }
        }
    
        [XmlElement("ConversionOfContractForDeedIndicator", Order = 27)]
        public MISMOIndicator ConversionOfContractForDeedIndicator { get; set; }
    
        [XmlIgnore]
        public bool ConversionOfContractForDeedIndicatorSpecified
        {
            get { return this.ConversionOfContractForDeedIndicator != null; }
            set { }
        }
    
        [XmlElement("ConvertibleIndicator", Order = 28)]
        public MISMOIndicator ConvertibleIndicator { get; set; }
    
        [XmlIgnore]
        public bool ConvertibleIndicatorSpecified
        {
            get { return this.ConvertibleIndicator != null; }
            set { }
        }
    
        [XmlElement("CreditEnhancementIndicator", Order = 29)]
        public MISMOIndicator CreditEnhancementIndicator { get; set; }
    
        [XmlIgnore]
        public bool CreditEnhancementIndicatorSpecified
        {
            get { return this.CreditEnhancementIndicator != null; }
            set { }
        }
    
        [XmlElement("CreditorServicingOfLoanStatementType", Order = 30)]
        public MISMOEnum<CreditorServicingOfLoanStatementBase> CreditorServicingOfLoanStatementType { get; set; }
    
        [XmlIgnore]
        public bool CreditorServicingOfLoanStatementTypeSpecified
        {
            get { return this.CreditorServicingOfLoanStatementType != null; }
            set { }
        }
    
        [XmlElement("CreditorServicingOfLoanStatementTypeOtherDescription", Order = 31)]
        public MISMOString CreditorServicingOfLoanStatementTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CreditorServicingOfLoanStatementTypeOtherDescriptionSpecified
        {
            get { return this.CreditorServicingOfLoanStatementTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("CurrentInterestRatePercent", Order = 32)]
        public MISMOPercent CurrentInterestRatePercent { get; set; }
    
        [XmlIgnore]
        public bool CurrentInterestRatePercentSpecified
        {
            get { return this.CurrentInterestRatePercent != null; }
            set { }
        }
    
        [XmlElement("DeferredInterestBalanceAmount", Order = 33)]
        public MISMOAmount DeferredInterestBalanceAmount { get; set; }
    
        [XmlIgnore]
        public bool DeferredInterestBalanceAmountSpecified
        {
            get { return this.DeferredInterestBalanceAmount != null; }
            set { }
        }
    
        [XmlElement("DemandFeatureIndicator", Order = 34)]
        public MISMOIndicator DemandFeatureIndicator { get; set; }
    
        [XmlIgnore]
        public bool DemandFeatureIndicatorSpecified
        {
            get { return this.DemandFeatureIndicator != null; }
            set { }
        }
    
        [XmlElement("EligibleForLenderPaidMIIndicator", Order = 35)]
        public MISMOIndicator EligibleForLenderPaidMIIndicator { get; set; }
    
        [XmlIgnore]
        public bool EligibleForLenderPaidMIIndicatorSpecified
        {
            get { return this.EligibleForLenderPaidMIIndicator != null; }
            set { }
        }
    
        [XmlElement("EmployeeLoanProgramIndicator", Order = 36)]
        public MISMOIndicator EmployeeLoanProgramIndicator { get; set; }
    
        [XmlIgnore]
        public bool EmployeeLoanProgramIndicatorSpecified
        {
            get { return this.EmployeeLoanProgramIndicator != null; }
            set { }
        }
    
        [XmlElement("EnergyRelatedImprovementsIndicator", Order = 37)]
        public MISMOIndicator EnergyRelatedImprovementsIndicator { get; set; }
    
        [XmlIgnore]
        public bool EnergyRelatedImprovementsIndicatorSpecified
        {
            get { return this.EnergyRelatedImprovementsIndicator != null; }
            set { }
        }
    
        [XmlElement("ENoteCertifiedIndicator", Order = 38)]
        public MISMOIndicator ENoteCertifiedIndicator { get; set; }
    
        [XmlIgnore]
        public bool ENoteCertifiedIndicatorSpecified
        {
            get { return this.ENoteCertifiedIndicator != null; }
            set { }
        }
    
        [XmlElement("ENoteIndicator", Order = 39)]
        public MISMOIndicator ENoteIndicator { get; set; }
    
        [XmlIgnore]
        public bool ENoteIndicatorSpecified
        {
            get { return this.ENoteIndicator != null; }
            set { }
        }
    
        [XmlElement("EscrowAbsenceReasonType", Order = 40)]
        public MISMOEnum<EscrowAbsenceReasonBase> EscrowAbsenceReasonType { get; set; }
    
        [XmlIgnore]
        public bool EscrowAbsenceReasonTypeSpecified
        {
            get { return this.EscrowAbsenceReasonType != null; }
            set { }
        }
    
        [XmlElement("EscrowAbsenceReasonTypeOtherDescription", Order = 41)]
        public MISMOString EscrowAbsenceReasonTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool EscrowAbsenceReasonTypeOtherDescriptionSpecified
        {
            get { return this.EscrowAbsenceReasonTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EscrowAccountLenderRequirementType", Order = 42)]
        public MISMOEnum<EscrowAccountLenderRequirementBase> EscrowAccountLenderRequirementType { get; set; }
    
        [XmlIgnore]
        public bool EscrowAccountLenderRequirementTypeSpecified
        {
            get { return this.EscrowAccountLenderRequirementType != null; }
            set { }
        }
    
        [XmlElement("EscrowAccountRequestedIndicator", Order = 43)]
        public MISMOIndicator EscrowAccountRequestedIndicator { get; set; }
    
        [XmlIgnore]
        public bool EscrowAccountRequestedIndicatorSpecified
        {
            get { return this.EscrowAccountRequestedIndicator != null; }
            set { }
        }
    
        [XmlElement("EscrowIndicator", Order = 44)]
        public MISMOIndicator EscrowIndicator { get; set; }
    
        [XmlIgnore]
        public bool EscrowIndicatorSpecified
        {
            get { return this.EscrowIndicator != null; }
            set { }
        }
    
        [XmlElement("EscrowNotAllowedIndicator", Order = 45)]
        public MISMOIndicator EscrowNotAllowedIndicator { get; set; }
    
        [XmlIgnore]
        public bool EscrowNotAllowedIndicatorSpecified
        {
            get { return this.EscrowNotAllowedIndicator != null; }
            set { }
        }
    
        [XmlElement("EscrowsElectedByBorrowerIndicator", Order = 46)]
        public MISMOIndicator EscrowsElectedByBorrowerIndicator { get; set; }
    
        [XmlIgnore]
        public bool EscrowsElectedByBorrowerIndicatorSpecified
        {
            get { return this.EscrowsElectedByBorrowerIndicator != null; }
            set { }
        }
    
        [XmlElement("HELOCIndicator", Order = 47)]
        public MISMOIndicator HELOCIndicator { get; set; }
    
        [XmlIgnore]
        public bool HELOCIndicatorSpecified
        {
            get { return this.HELOCIndicator != null; }
            set { }
        }
    
        [XmlElement("HigherPricedMortgageLoanIndicator", Order = 48)]
        public MISMOIndicator HigherPricedMortgageLoanIndicator { get; set; }
    
        [XmlIgnore]
        public bool HigherPricedMortgageLoanIndicatorSpecified
        {
            get { return this.HigherPricedMortgageLoanIndicator != null; }
            set { }
        }
    
        [XmlElement("HomeBuyersHomeownershipEducationCertificateIndicator", Order = 49)]
        public MISMOIndicator HomeBuyersHomeownershipEducationCertificateIndicator { get; set; }
    
        [XmlIgnore]
        public bool HomeBuyersHomeownershipEducationCertificateIndicatorSpecified
        {
            get { return this.HomeBuyersHomeownershipEducationCertificateIndicator != null; }
            set { }
        }
    
        [XmlElement("InitialFixedPeriodEffectiveMonthsCount", Order = 50)]
        public MISMOCount InitialFixedPeriodEffectiveMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool InitialFixedPeriodEffectiveMonthsCountSpecified
        {
            get { return this.InitialFixedPeriodEffectiveMonthsCount != null; }
            set { }
        }
    
        [XmlElement("InterestCreditedToBorrowerIndicator", Order = 51)]
        public MISMOIndicator InterestCreditedToBorrowerIndicator { get; set; }
    
        [XmlIgnore]
        public bool InterestCreditedToBorrowerIndicatorSpecified
        {
            get { return this.InterestCreditedToBorrowerIndicator != null; }
            set { }
        }
    
        [XmlElement("InterestOnlyIndicator", Order = 52)]
        public MISMOIndicator InterestOnlyIndicator { get; set; }
    
        [XmlIgnore]
        public bool InterestOnlyIndicatorSpecified
        {
            get { return this.InterestOnlyIndicator != null; }
            set { }
        }
    
        [XmlElement("InterestRateIncreaseIndicator", Order = 53)]
        public MISMOIndicator InterestRateIncreaseIndicator { get; set; }
    
        [XmlIgnore]
        public bool InterestRateIncreaseIndicatorSpecified
        {
            get { return this.InterestRateIncreaseIndicator != null; }
            set { }
        }
    
        [XmlElement("JurisdictionHighCostLoanIndicator", Order = 54)]
        public MISMOIndicator JurisdictionHighCostLoanIndicator { get; set; }
    
        [XmlIgnore]
        public bool JurisdictionHighCostLoanIndicatorSpecified
        {
            get { return this.JurisdictionHighCostLoanIndicator != null; }
            set { }
        }
    
        [XmlElement("LenderPlacedHazardInsuranceIndicator", Order = 55)]
        public MISMOIndicator LenderPlacedHazardInsuranceIndicator { get; set; }
    
        [XmlIgnore]
        public bool LenderPlacedHazardInsuranceIndicatorSpecified
        {
            get { return this.LenderPlacedHazardInsuranceIndicator != null; }
            set { }
        }
    
        [XmlElement("LenderSelfInsuredIndicator", Order = 56)]
        public MISMOIndicator LenderSelfInsuredIndicator { get; set; }
    
        [XmlIgnore]
        public bool LenderSelfInsuredIndicatorSpecified
        {
            get { return this.LenderSelfInsuredIndicator != null; }
            set { }
        }
    
        [XmlElement("LendingLimitAmount", Order = 57)]
        public MISMOAmount LendingLimitAmount { get; set; }
    
        [XmlIgnore]
        public bool LendingLimitAmountSpecified
        {
            get { return this.LendingLimitAmount != null; }
            set { }
        }
    
        [XmlElement("LendingLimitType", Order = 58)]
        public MISMOEnum<LendingLimitBase> LendingLimitType { get; set; }
    
        [XmlIgnore]
        public bool LendingLimitTypeSpecified
        {
            get { return this.LendingLimitType != null; }
            set { }
        }
    
        [XmlElement("LendingLimitTypeOtherDescription", Order = 59)]
        public MISMOString LendingLimitTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool LendingLimitTypeOtherDescriptionSpecified
        {
            get { return this.LendingLimitTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("LienHolderSameAsSubjectLoanIndicator", Order = 60)]
        public MISMOIndicator LienHolderSameAsSubjectLoanIndicator { get; set; }
    
        [XmlIgnore]
        public bool LienHolderSameAsSubjectLoanIndicatorSpecified
        {
            get { return this.LienHolderSameAsSubjectLoanIndicator != null; }
            set { }
        }
    
        [XmlElement("LoanAffordableIndicator", Order = 61)]
        public MISMOIndicator LoanAffordableIndicator { get; set; }
    
        [XmlIgnore]
        public bool LoanAffordableIndicatorSpecified
        {
            get { return this.LoanAffordableIndicator != null; }
            set { }
        }
    
        [XmlElement("LoanAgeMonthsCount", Order = 62)]
        public MISMOCount LoanAgeMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool LoanAgeMonthsCountSpecified
        {
            get { return this.LoanAgeMonthsCount != null; }
            set { }
        }
    
        [XmlElement("LoanAmountIncreaseIndicator", Order = 63)]
        public MISMOIndicator LoanAmountIncreaseIndicator { get; set; }
    
        [XmlIgnore]
        public bool LoanAmountIncreaseIndicatorSpecified
        {
            get { return this.LoanAmountIncreaseIndicator != null; }
            set { }
        }
    
        [XmlElement("LoanApprovalDate", Order = 64)]
        public MISMODate LoanApprovalDate { get; set; }
    
        [XmlIgnore]
        public bool LoanApprovalDateSpecified
        {
            get { return this.LoanApprovalDate != null; }
            set { }
        }
    
        [XmlElement("LoanApprovalExpirationDate", Order = 65)]
        public MISMODate LoanApprovalExpirationDate { get; set; }
    
        [XmlIgnore]
        public bool LoanApprovalExpirationDateSpecified
        {
            get { return this.LoanApprovalExpirationDate != null; }
            set { }
        }
    
        [XmlElement("LoanApprovalPeriodDaysCount", Order = 66)]
        public MISMOCount LoanApprovalPeriodDaysCount { get; set; }
    
        [XmlIgnore]
        public bool LoanApprovalPeriodDaysCountSpecified
        {
            get { return this.LoanApprovalPeriodDaysCount != null; }
            set { }
        }
    
        [XmlElement("LoanClosingStatusType", Order = 67)]
        public MISMOEnum<LoanClosingStatusBase> LoanClosingStatusType { get; set; }
    
        [XmlIgnore]
        public bool LoanClosingStatusTypeSpecified
        {
            get { return this.LoanClosingStatusType != null; }
            set { }
        }
    
        [XmlElement("LoanCommitmentDate", Order = 68)]
        public MISMODate LoanCommitmentDate { get; set; }
    
        [XmlIgnore]
        public bool LoanCommitmentDateSpecified
        {
            get { return this.LoanCommitmentDate != null; }
            set { }
        }
    
        [XmlElement("LoanCommitmentExpirationDate", Order = 69)]
        public MISMODate LoanCommitmentExpirationDate { get; set; }
    
        [XmlIgnore]
        public bool LoanCommitmentExpirationDateSpecified
        {
            get { return this.LoanCommitmentExpirationDate != null; }
            set { }
        }
    
        [XmlElement("LoanCommitmentPeriodDaysCount", Order = 70)]
        public MISMOCount LoanCommitmentPeriodDaysCount { get; set; }
    
        [XmlIgnore]
        public bool LoanCommitmentPeriodDaysCountSpecified
        {
            get { return this.LoanCommitmentPeriodDaysCount != null; }
            set { }
        }
    
        [XmlElement("LoanEstimateWaitingPeriodWaivedIndicator", Order = 71)]
        public MISMOIndicator LoanEstimateWaitingPeriodWaivedIndicator { get; set; }
    
        [XmlIgnore]
        public bool LoanEstimateWaitingPeriodWaivedIndicatorSpecified
        {
            get { return this.LoanEstimateWaitingPeriodWaivedIndicator != null; }
            set { }
        }
    
        [XmlElement("LoanFundingDate", Order = 72)]
        public MISMODate LoanFundingDate { get; set; }
    
        [XmlIgnore]
        public bool LoanFundingDateSpecified
        {
            get { return this.LoanFundingDate != null; }
            set { }
        }
    
        [XmlElement("LoanLevelCreditScoreIndicator", Order = 73)]
        public MISMOIndicator LoanLevelCreditScoreIndicator { get; set; }
    
        [XmlIgnore]
        public bool LoanLevelCreditScoreIndicatorSpecified
        {
            get { return this.LoanLevelCreditScoreIndicator != null; }
            set { }
        }
    
        [XmlElement("LoanRepaymentType", Order = 74)]
        public MISMOEnum<LoanRepaymentBase> LoanRepaymentType { get; set; }
    
        [XmlIgnore]
        public bool LoanRepaymentTypeSpecified
        {
            get { return this.LoanRepaymentType != null; }
            set { }
        }
    
        [XmlElement("LoanRepaymentTypeOtherDescription", Order = 75)]
        public MISMOString LoanRepaymentTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool LoanRepaymentTypeOtherDescriptionSpecified
        {
            get { return this.LoanRepaymentTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("LoanSaleFundingDate", Order = 76)]
        public MISMODate LoanSaleFundingDate { get; set; }
    
        [XmlIgnore]
        public bool LoanSaleFundingDateSpecified
        {
            get { return this.LoanSaleFundingDate != null; }
            set { }
        }
    
        [XmlElement("LoanSecuredByType", Order = 77)]
        public MISMOEnum<LoanSecuredByBase> LoanSecuredByType { get; set; }
    
        [XmlIgnore]
        public bool LoanSecuredByTypeSpecified
        {
            get { return this.LoanSecuredByType != null; }
            set { }
        }
    
        [XmlElement("LoanSecuredByTypeOtherDescription", Order = 78)]
        public MISMOString LoanSecuredByTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool LoanSecuredByTypeOtherDescriptionSpecified
        {
            get { return this.LoanSecuredByTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("LoanSecuredIndicator", Order = 79)]
        public MISMOIndicator LoanSecuredIndicator { get; set; }
    
        [XmlIgnore]
        public bool LoanSecuredIndicatorSpecified
        {
            get { return this.LoanSecuredIndicator != null; }
            set { }
        }
    
        [XmlElement("LoanSellerProvidedInvestmentRatingDescription", Order = 80)]
        public MISMOString LoanSellerProvidedInvestmentRatingDescription { get; set; }
    
        [XmlIgnore]
        public bool LoanSellerProvidedInvestmentRatingDescriptionSpecified
        {
            get { return this.LoanSellerProvidedInvestmentRatingDescription != null; }
            set { }
        }
    
        [XmlElement("LostNoteAffidavitIndicator", Order = 81)]
        public MISMOIndicator LostNoteAffidavitIndicator { get; set; }
    
        [XmlIgnore]
        public bool LostNoteAffidavitIndicatorSpecified
        {
            get { return this.LostNoteAffidavitIndicator != null; }
            set { }
        }
    
        [XmlElement("MICoverageExistsIndicator", Order = 82)]
        public MISMOIndicator MICoverageExistsIndicator { get; set; }
    
        [XmlIgnore]
        public bool MICoverageExistsIndicatorSpecified
        {
            get { return this.MICoverageExistsIndicator != null; }
            set { }
        }
    
        [XmlElement("MIRequiredIndicator", Order = 83)]
        public MISMOIndicator MIRequiredIndicator { get; set; }
    
        [XmlIgnore]
        public bool MIRequiredIndicatorSpecified
        {
            get { return this.MIRequiredIndicator != null; }
            set { }
        }
    
        [XmlElement("MortgageModificationIndicator", Order = 84)]
        public MISMOIndicator MortgageModificationIndicator { get; set; }
    
        [XmlIgnore]
        public bool MortgageModificationIndicatorSpecified
        {
            get { return this.MortgageModificationIndicator != null; }
            set { }
        }
    
        [XmlElement("NegativeAmortizationIndicator", Order = 85)]
        public MISMOIndicator NegativeAmortizationIndicator { get; set; }
    
        [XmlIgnore]
        public bool NegativeAmortizationIndicatorSpecified
        {
            get { return this.NegativeAmortizationIndicator != null; }
            set { }
        }
    
        [XmlElement("OpenEndCreditIndicator", Order = 86)]
        public MISMOIndicator OpenEndCreditIndicator { get; set; }
    
        [XmlIgnore]
        public bool OpenEndCreditIndicatorSpecified
        {
            get { return this.OpenEndCreditIndicator != null; }
            set { }
        }
    
        [XmlElement("OriginalLoanStandardEscrowProvisionsIndicator", Order = 87)]
        public MISMOIndicator OriginalLoanStandardEscrowProvisionsIndicator { get; set; }
    
        [XmlIgnore]
        public bool OriginalLoanStandardEscrowProvisionsIndicatorSpecified
        {
            get { return this.OriginalLoanStandardEscrowProvisionsIndicator != null; }
            set { }
        }
    
        [XmlElement("OverdueInterestAmount", Order = 88)]
        public MISMOAmount OverdueInterestAmount { get; set; }
    
        [XmlIgnore]
        public bool OverdueInterestAmountSpecified
        {
            get { return this.OverdueInterestAmount != null; }
            set { }
        }
    
        [XmlElement("PaymentIncreaseIndicator", Order = 89)]
        public MISMOIndicator PaymentIncreaseIndicator { get; set; }
    
        [XmlIgnore]
        public bool PaymentIncreaseIndicatorSpecified
        {
            get { return this.PaymentIncreaseIndicator != null; }
            set { }
        }
    
        [XmlElement("PiggybackLoanIndicator", Order = 90)]
        public MISMOIndicator PiggybackLoanIndicator { get; set; }
    
        [XmlIgnore]
        public bool PiggybackLoanIndicatorSpecified
        {
            get { return this.PiggybackLoanIndicator != null; }
            set { }
        }
    
        [XmlElement("PoolIdentifier", Order = 91)]
        public MISMOIdentifier PoolIdentifier { get; set; }
    
        [XmlIgnore]
        public bool PoolIdentifierSpecified
        {
            get { return this.PoolIdentifier != null; }
            set { }
        }
    
        [XmlElement("PoolIssueDate", Order = 92)]
        public MISMODate PoolIssueDate { get; set; }
    
        [XmlIgnore]
        public bool PoolIssueDateSpecified
        {
            get { return this.PoolIssueDate != null; }
            set { }
        }
    
        [XmlElement("PoolPrefixIdentifier", Order = 93)]
        public MISMOIdentifier PoolPrefixIdentifier { get; set; }
    
        [XmlIgnore]
        public bool PoolPrefixIdentifierSpecified
        {
            get { return this.PoolPrefixIdentifier != null; }
            set { }
        }
    
        [XmlElement("PrepaymentPenaltyIndicator", Order = 94)]
        public MISMOIndicator PrepaymentPenaltyIndicator { get; set; }
    
        [XmlIgnore]
        public bool PrepaymentPenaltyIndicatorSpecified
        {
            get { return this.PrepaymentPenaltyIndicator != null; }
            set { }
        }
    
        [XmlElement("PrepaymentPenaltyWaivedIndicator", Order = 95)]
        public MISMOIndicator PrepaymentPenaltyWaivedIndicator { get; set; }
    
        [XmlIgnore]
        public bool PrepaymentPenaltyWaivedIndicatorSpecified
        {
            get { return this.PrepaymentPenaltyWaivedIndicator != null; }
            set { }
        }
    
        [XmlElement("PrepaymentRestrictionIndicator", Order = 96)]
        public MISMOIndicator PrepaymentRestrictionIndicator { get; set; }
    
        [XmlIgnore]
        public bool PrepaymentRestrictionIndicatorSpecified
        {
            get { return this.PrepaymentRestrictionIndicator != null; }
            set { }
        }
    
        [XmlElement("PropertiesFinancedByLenderCount", Order = 97)]
        public MISMOCount PropertiesFinancedByLenderCount { get; set; }
    
        [XmlIgnore]
        public bool PropertiesFinancedByLenderCountSpecified
        {
            get { return this.PropertiesFinancedByLenderCount != null; }
            set { }
        }
    
        [XmlElement("PropertyInspectionWaiverIndicator", Order = 98)]
        public MISMOIndicator PropertyInspectionWaiverIndicator { get; set; }
    
        [XmlIgnore]
        public bool PropertyInspectionWaiverIndicatorSpecified
        {
            get { return this.PropertyInspectionWaiverIndicator != null; }
            set { }
        }
    
        [XmlElement("QualifiedMortgageIndicator", Order = 99)]
        public MISMOIndicator QualifiedMortgageIndicator { get; set; }
    
        [XmlIgnore]
        public bool QualifiedMortgageIndicatorSpecified
        {
            get { return this.QualifiedMortgageIndicator != null; }
            set { }
        }
    
        [XmlElement("RecoverableCorporateAdvanceBalanceFromBorrowerAmount", Order = 100)]
        public MISMOAmount RecoverableCorporateAdvanceBalanceFromBorrowerAmount { get; set; }
    
        [XmlIgnore]
        public bool RecoverableCorporateAdvanceBalanceFromBorrowerAmountSpecified
        {
            get { return this.RecoverableCorporateAdvanceBalanceFromBorrowerAmount != null; }
            set { }
        }
    
        [XmlElement("RecoverableCorporateAdvanceBalanceFromThirdPartyAmount", Order = 101)]
        public MISMOAmount RecoverableCorporateAdvanceBalanceFromThirdPartyAmount { get; set; }
    
        [XmlIgnore]
        public bool RecoverableCorporateAdvanceBalanceFromThirdPartyAmountSpecified
        {
            get { return this.RecoverableCorporateAdvanceBalanceFromThirdPartyAmount != null; }
            set { }
        }
    
        [XmlElement("RefundOfOverpaidInterestCalendarYearAmount", Order = 102)]
        public MISMOAmount RefundOfOverpaidInterestCalendarYearAmount { get; set; }
    
        [XmlIgnore]
        public bool RefundOfOverpaidInterestCalendarYearAmountSpecified
        {
            get { return this.RefundOfOverpaidInterestCalendarYearAmount != null; }
            set { }
        }
    
        [XmlElement("RegulationZHighCostLoanIndicator", Order = 103)]
        public MISMOIndicator RegulationZHighCostLoanIndicator { get; set; }
    
        [XmlIgnore]
        public bool RegulationZHighCostLoanIndicatorSpecified
        {
            get { return this.RegulationZHighCostLoanIndicator != null; }
            set { }
        }
    
        [XmlElement("RelocationLoanIndicator", Order = 104)]
        public MISMOIndicator RelocationLoanIndicator { get; set; }
    
        [XmlIgnore]
        public bool RelocationLoanIndicatorSpecified
        {
            get { return this.RelocationLoanIndicator != null; }
            set { }
        }
    
        [XmlElement("RemainingUnearnedInterestAmount", Order = 105)]
        public MISMOAmount RemainingUnearnedInterestAmount { get; set; }
    
        [XmlIgnore]
        public bool RemainingUnearnedInterestAmountSpecified
        {
            get { return this.RemainingUnearnedInterestAmount != null; }
            set { }
        }
    
        [XmlElement("RenovationLoanIndicator", Order = 106)]
        public MISMOIndicator RenovationLoanIndicator { get; set; }
    
        [XmlIgnore]
        public bool RenovationLoanIndicatorSpecified
        {
            get { return this.RenovationLoanIndicator != null; }
            set { }
        }
    
        [XmlElement("RESPAConformingYearType", Order = 107)]
        public MISMOEnum<RESPAConformingYearBase> RESPAConformingYearType { get; set; }
    
        [XmlIgnore]
        public bool RESPAConformingYearTypeSpecified
        {
            get { return this.RESPAConformingYearType != null; }
            set { }
        }
    
        [XmlElement("RESPAConformingYearTypeOtherDescription", Order = 108)]
        public MISMOString RESPAConformingYearTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool RESPAConformingYearTypeOtherDescriptionSpecified
        {
            get { return this.RESPAConformingYearTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ReverseMortgageIndicator", Order = 109)]
        public MISMOIndicator ReverseMortgageIndicator { get; set; }
    
        [XmlIgnore]
        public bool ReverseMortgageIndicatorSpecified
        {
            get { return this.ReverseMortgageIndicator != null; }
            set { }
        }
    
        [XmlElement("RightOfRescissionWaitingPeriodWaivedIndicator", Order = 110)]
        public MISMOIndicator RightOfRescissionWaitingPeriodWaivedIndicator { get; set; }
    
        [XmlIgnore]
        public bool RightOfRescissionWaitingPeriodWaivedIndicatorSpecified
        {
            get { return this.RightOfRescissionWaitingPeriodWaivedIndicator != null; }
            set { }
        }
    
        [XmlElement("SCRAIndicator", Order = 111)]
        public MISMOIndicator SCRAIndicator { get; set; }
    
        [XmlIgnore]
        public bool SCRAIndicatorSpecified
        {
            get { return this.SCRAIndicator != null; }
            set { }
        }
    
        [XmlElement("SCRAReliefForbearanceRequestedIndicator", Order = 112)]
        public MISMOIndicator SCRAReliefForbearanceRequestedIndicator { get; set; }
    
        [XmlIgnore]
        public bool SCRAReliefForbearanceRequestedIndicatorSpecified
        {
            get { return this.SCRAReliefForbearanceRequestedIndicator != null; }
            set { }
        }
    
        [XmlElement("SCRAReliefStatusType", Order = 113)]
        public MISMOEnum<SCRAReliefStatusBase> SCRAReliefStatusType { get; set; }
    
        [XmlIgnore]
        public bool SCRAReliefStatusTypeSpecified
        {
            get { return this.SCRAReliefStatusType != null; }
            set { }
        }
    
        [XmlElement("SCRAReliefStatusTypeOtherDescription", Order = 114)]
        public MISMOString SCRAReliefStatusTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool SCRAReliefStatusTypeOtherDescriptionSpecified
        {
            get { return this.SCRAReliefStatusTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("SeasonalPaymentFeatureDescription", Order = 115)]
        public MISMOString SeasonalPaymentFeatureDescription { get; set; }
    
        [XmlIgnore]
        public bool SeasonalPaymentFeatureDescriptionSpecified
        {
            get { return this.SeasonalPaymentFeatureDescription != null; }
            set { }
        }
    
        [XmlElement("SeasonalPaymentFeatureIndicator", Order = 116)]
        public MISMOIndicator SeasonalPaymentFeatureIndicator { get; set; }
    
        [XmlIgnore]
        public bool SeasonalPaymentFeatureIndicatorSpecified
        {
            get { return this.SeasonalPaymentFeatureIndicator != null; }
            set { }
        }
    
        [XmlElement("ServicingTransferProhibitedIndicator", Order = 117)]
        public MISMOIndicator ServicingTransferProhibitedIndicator { get; set; }
    
        [XmlIgnore]
        public bool ServicingTransferProhibitedIndicatorSpecified
        {
            get { return this.ServicingTransferProhibitedIndicator != null; }
            set { }
        }
    
        [XmlElement("ServicingTransferStatusType", Order = 118)]
        public MISMOEnum<ServicingTransferStatusBase> ServicingTransferStatusType { get; set; }
    
        [XmlIgnore]
        public bool ServicingTransferStatusTypeSpecified
        {
            get { return this.ServicingTransferStatusType != null; }
            set { }
        }
    
        [XmlElement("SharedAppreciationIndicator", Order = 119)]
        public MISMOIndicator SharedAppreciationIndicator { get; set; }
    
        [XmlIgnore]
        public bool SharedAppreciationIndicatorSpecified
        {
            get { return this.SharedAppreciationIndicator != null; }
            set { }
        }
    
        [XmlElement("SharedEquityIndicator", Order = 120)]
        public MISMOIndicator SharedEquityIndicator { get; set; }
    
        [XmlIgnore]
        public bool SharedEquityIndicatorSpecified
        {
            get { return this.SharedEquityIndicator != null; }
            set { }
        }
    
        [XmlElement("SignedAuthorizationToRequestTaxRecordsIndicator", Order = 121)]
        public MISMOIndicator SignedAuthorizationToRequestTaxRecordsIndicator { get; set; }
    
        [XmlIgnore]
        public bool SignedAuthorizationToRequestTaxRecordsIndicatorSpecified
        {
            get { return this.SignedAuthorizationToRequestTaxRecordsIndicator != null; }
            set { }
        }
    
        [XmlElement("StepPaymentsFeatureDescription", Order = 122)]
        public MISMOString StepPaymentsFeatureDescription { get; set; }
    
        [XmlIgnore]
        public bool StepPaymentsFeatureDescriptionSpecified
        {
            get { return this.StepPaymentsFeatureDescription != null; }
            set { }
        }
    
        [XmlElement("TaxRecordsObtainedIndicator", Order = 123)]
        public MISMOIndicator TaxRecordsObtainedIndicator { get; set; }
    
        [XmlIgnore]
        public bool TaxRecordsObtainedIndicatorSpecified
        {
            get { return this.TaxRecordsObtainedIndicator != null; }
            set { }
        }
    
        [XmlElement("TimelyPaymentRateReductionIndicator", Order = 124)]
        public MISMOIndicator TimelyPaymentRateReductionIndicator { get; set; }
    
        [XmlIgnore]
        public bool TimelyPaymentRateReductionIndicatorSpecified
        {
            get { return this.TimelyPaymentRateReductionIndicator != null; }
            set { }
        }
    
        [XmlElement("TotalMortgagedPropertiesCount", Order = 125)]
        public MISMOCount TotalMortgagedPropertiesCount { get; set; }
    
        [XmlIgnore]
        public bool TotalMortgagedPropertiesCountSpecified
        {
            get { return this.TotalMortgagedPropertiesCount != null; }
            set { }
        }
    
        [XmlElement("TotalSalesConcessionAmount", Order = 126)]
        public MISMOAmount TotalSalesConcessionAmount { get; set; }
    
        [XmlIgnore]
        public bool TotalSalesConcessionAmountSpecified
        {
            get { return this.TotalSalesConcessionAmount != null; }
            set { }
        }
    
        [XmlElement("TotalSubordinateFinancingAmount", Order = 127)]
        public MISMOAmount TotalSubordinateFinancingAmount { get; set; }
    
        [XmlIgnore]
        public bool TotalSubordinateFinancingAmountSpecified
        {
            get { return this.TotalSubordinateFinancingAmount != null; }
            set { }
        }
    
        [XmlElement("WarehouseLenderIndicator", Order = 128)]
        public MISMOIndicator WarehouseLenderIndicator { get; set; }
    
        [XmlIgnore]
        public bool WarehouseLenderIndicatorSpecified
        {
            get { return this.WarehouseLenderIndicator != null; }
            set { }
        }
    
        [XmlElement("WorkoutActiveIndicator", Order = 129)]
        public MISMOIndicator WorkoutActiveIndicator { get; set; }
    
        [XmlIgnore]
        public bool WorkoutActiveIndicatorSpecified
        {
            get { return this.WorkoutActiveIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 130)]
        public LOAN_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
