namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class LOANS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CombinedLtvsSpecified
                    || this.LoanListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("COMBINED_LTVS", Order = 0)]
        public COMBINED_LTVS CombinedLtvs { get; set; }
    
        [XmlIgnore]
        public bool CombinedLtvsSpecified
        {
            get { return this.CombinedLtvs != null && this.CombinedLtvs.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("LOAN", Order = 1)]
        public List<LOAN> LoanList { get; set; } = new List<LOAN>();
    
        [XmlIgnore]
        public bool LoanListSpecified
        {
            get { return this.LoanList != null && this.LoanList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public LOANS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
