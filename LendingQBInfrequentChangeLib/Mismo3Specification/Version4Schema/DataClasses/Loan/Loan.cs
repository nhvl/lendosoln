namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Schema;
    using System.Xml.Serialization;

    public class LOAN
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AchSpecified
                    || this.AdjustmentSpecified
                    || this.AffordableLendingSpecified
                    || this.AmortizationSpecified
                    || this.AssumabilitySpecified
                    || this.BillingAddressSpecified
                    || this.BuydownSpecified
                    || this.CapitalizationsSpecified
                    || this.ChargeOffSpecified
                    || this.ClosingInformationSpecified
                    || this.CollectionsSpecified
                    || this.ConstructionSpecified
                    || this.ContractVariancesSpecified
                    || this.CreditEnhancementsSpecified
                    || this.DisclosuresSpecified
                    || this.DocumentSpecificDataSetsSpecified
                    || this.DocumentationsSpecified
                    || this.DownPaymentsSpecified
                    || this.DrawSpecified
                    || this.EscrowSpecified
                    || this.FeeInformationSpecified
                    || this.ForeclosuresSpecified
                    || this.GovernmentLoanSpecified
                    || this.HelocSpecified
                    || this.HighCostMortgagesSpecified
                    || this.HmdaLoanSpecified
                    || this.HousingExpensesSpecified
                    || this.InsuranceClaimsSpecified
                    || this.InterestCalculationSpecified
                    || this.InterestOnlySpecified
                    || this.InvestorFeaturesSpecified
                    || this.InvestorLoanInformationSpecified
                    || this.LateChargeSpecified
                    || this.LoanCommentsSpecified
                    || this.LoanDetailSpecified
                    || this.LoanIdentifiersSpecified
                    || this.LoanLevelCreditSpecified
                    || this.LoanProductSpecified
                    || this.LoanProgramsSpecified
                    || this.LoanStateSpecified
                    || this.LoanStatusesSpecified
                    || this.LtvSpecified
                    || this.MaturitySpecified
                    || this.MersRegistrationsSpecified
                    || this.MiDataSpecified
                    || this.ModificationsSpecified
                    || this.MortgageScoresSpecified
                    || this.NegativeAmortizationSpecified
                    || this.OptionalProductsSpecified
                    || this.OriginationFundsSpecified
                    || this.OriginationSystemsSpecified
                    || this.PaymentSpecified
                    || this.PrepaymentPenaltySpecified
                    || this.PurchaseCreditsSpecified
                    || this.QualificationSpecified
                    || this.QualifiedMortgageSpecified
                    || this.RefinanceSpecified
                    || this.RehabilitationSpecified
                    || this.ReverseMortgageSpecified
                    || this.ServicingSpecified
                    || this.TermsOfLoanSpecified
                    || this.TreasuryNpvSpecified
                    || this.UnderwritingSpecified
                    || this.WorkoutsSpecified
                    || this.ExtensionSpecified
                    || this.IneligibleLoanProductIndicatorStringifiedSpecified;
            }
        }
    
        [XmlElement("ACH", Order = 0)]
        public ACH Ach { get; set; }
    
        [XmlIgnore]
        public bool AchSpecified
        {
            get { return this.Ach != null && this.Ach.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("ADJUSTMENT", Order = 1)]
        public ADJUSTMENT Adjustment { get; set; }
    
        [XmlIgnore]
        public bool AdjustmentSpecified
        {
            get { return this.Adjustment != null && this.Adjustment.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("AFFORDABLE_LENDING", Order = 2)]
        public AFFORDABLE_LENDING AffordableLending { get; set; }
    
        [XmlIgnore]
        public bool AffordableLendingSpecified
        {
            get { return this.AffordableLending != null && this.AffordableLending.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("AMORTIZATION", Order = 3)]
        public AMORTIZATION Amortization { get; set; }
    
        [XmlIgnore]
        public bool AmortizationSpecified
        {
            get { return this.Amortization != null && this.Amortization.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("ASSUMABILITY", Order = 4)]
        public ASSUMABILITY Assumability { get; set; }
    
        [XmlIgnore]
        public bool AssumabilitySpecified
        {
            get { return this.Assumability != null && this.Assumability.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("BILLING_ADDRESS", Order = 5)]
        public BILLING_ADDRESS BillingAddress { get; set; }
    
        [XmlIgnore]
        public bool BillingAddressSpecified
        {
            get { return this.BillingAddress != null && this.BillingAddress.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("BUYDOWN", Order = 6)]
        public BUYDOWN Buydown { get; set; }
    
        [XmlIgnore]
        public bool BuydownSpecified
        {
            get { return this.Buydown != null && this.Buydown.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CAPITALIZATIONS", Order = 7)]
        public CAPITALIZATIONS Capitalizations { get; set; }
    
        [XmlIgnore]
        public bool CapitalizationsSpecified
        {
            get { return this.Capitalizations != null && this.Capitalizations.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CHARGE_OFF", Order = 8)]
        public CHARGE_OFF ChargeOff { get; set; }
    
        [XmlIgnore]
        public bool ChargeOffSpecified
        {
            get { return this.ChargeOff != null && this.ChargeOff.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CLOSING_INFORMATION", Order = 9)]
        public CLOSING_INFORMATION ClosingInformation { get; set; }
    
        [XmlIgnore]
        public bool ClosingInformationSpecified
        {
            get { return this.ClosingInformation != null && this.ClosingInformation.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("COLLECTIONS", Order = 10)]
        public COLLECTIONS Collections { get; set; }
    
        [XmlIgnore]
        public bool CollectionsSpecified
        {
            get { return this.Collections != null && this.Collections.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CONSTRUCTION", Order = 11)]
        public CONSTRUCTION Construction { get; set; }
    
        [XmlIgnore]
        public bool ConstructionSpecified
        {
            get { return this.Construction != null && this.Construction.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CONTRACT_VARIANCES", Order = 12)]
        public CONTRACT_VARIANCES ContractVariances { get; set; }
    
        [XmlIgnore]
        public bool ContractVariancesSpecified
        {
            get { return this.ContractVariances != null && this.ContractVariances.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_ENHANCEMENTS", Order = 13)]
        public CREDIT_ENHANCEMENTS CreditEnhancements { get; set; }
    
        [XmlIgnore]
        public bool CreditEnhancementsSpecified
        {
            get { return this.CreditEnhancements != null && this.CreditEnhancements.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("DISCLOSURES", Order = 14)]
        public DISCLOSURES Disclosures { get; set; }
    
        [XmlIgnore]
        public bool DisclosuresSpecified
        {
            get { return this.Disclosures != null && this.Disclosures.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("DOCUMENT_SPECIFIC_DATA_SETS", Order = 15)]
        public DOCUMENT_SPECIFIC_DATA_SETS DocumentSpecificDataSets { get; set; }
    
        [XmlIgnore]
        public bool DocumentSpecificDataSetsSpecified
        {
            get { return this.DocumentSpecificDataSets != null && this.DocumentSpecificDataSets.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("DOCUMENTATIONS", Order = 16)]
        public DOCUMENTATIONS Documentations { get; set; }
    
        [XmlIgnore]
        public bool DocumentationsSpecified
        {
            get { return this.Documentations != null && this.Documentations.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("DOWN_PAYMENTS", Order = 17)]
        public DOWN_PAYMENTS DownPayments { get; set; }
    
        [XmlIgnore]
        public bool DownPaymentsSpecified
        {
            get { return this.DownPayments != null && this.DownPayments.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("DRAW", Order = 18)]
        public DRAW Draw { get; set; }
    
        [XmlIgnore]
        public bool DrawSpecified
        {
            get { return this.Draw != null && this.Draw.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("ESCROW", Order = 19)]
        public ESCROW Escrow { get; set; }
    
        [XmlIgnore]
        public bool EscrowSpecified
        {
            get { return this.Escrow != null && this.Escrow.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("FEE_INFORMATION", Order = 20)]
        public FEE_INFORMATION FeeInformation { get; set; }
    
        [XmlIgnore]
        public bool FeeInformationSpecified
        {
            get { return this.FeeInformation != null && this.FeeInformation.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("FORECLOSURES", Order = 21)]
        public FORECLOSURES Foreclosures { get; set; }
    
        [XmlIgnore]
        public bool ForeclosuresSpecified
        {
            get { return this.Foreclosures != null && this.Foreclosures.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("GOVERNMENT_LOAN", Order = 22)]
        public GOVERNMENT_LOAN GovernmentLoan { get; set; }
    
        [XmlIgnore]
        public bool GovernmentLoanSpecified
        {
            get { return this.GovernmentLoan != null && this.GovernmentLoan.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("HELOC", Order = 23)]
        public HELOC Heloc { get; set; }
    
        [XmlIgnore]
        public bool HelocSpecified
        {
            get { return this.Heloc != null && this.Heloc.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("HIGH_COST_MORTGAGES", Order = 24)]
        public HIGH_COST_MORTGAGES HighCostMortgages { get; set; }
    
        [XmlIgnore]
        public bool HighCostMortgagesSpecified
        {
            get { return this.HighCostMortgages != null && this.HighCostMortgages.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("HMDA_LOAN", Order = 25)]
        public HMDA_LOAN HmdaLoan { get; set; }
    
        [XmlIgnore]
        public bool HmdaLoanSpecified
        {
            get { return this.HmdaLoan != null && this.HmdaLoan.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("HOUSING_EXPENSES", Order = 26)]
        public HOUSING_EXPENSES HousingExpenses { get; set; }
    
        [XmlIgnore]
        public bool HousingExpensesSpecified
        {
            get { return this.HousingExpenses != null && this.HousingExpenses.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("INSURANCE_CLAIMS", Order = 27)]
        public INSURANCE_CLAIMS InsuranceClaims { get; set; }
    
        [XmlIgnore]
        public bool InsuranceClaimsSpecified
        {
            get { return this.InsuranceClaims != null && this.InsuranceClaims.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("INTEREST_CALCULATION", Order = 28)]
        public INTEREST_CALCULATION InterestCalculation { get; set; }
    
        [XmlIgnore]
        public bool InterestCalculationSpecified
        {
            get { return this.InterestCalculation != null && this.InterestCalculation.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("INTEREST_ONLY", Order = 29)]
        public INTEREST_ONLY InterestOnly { get; set; }
    
        [XmlIgnore]
        public bool InterestOnlySpecified
        {
            get { return this.InterestOnly != null && this.InterestOnly.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("INVESTOR_FEATURES", Order = 30)]
        public INVESTOR_FEATURES InvestorFeatures { get; set; }
    
        [XmlIgnore]
        public bool InvestorFeaturesSpecified
        {
            get { return this.InvestorFeatures != null && this.InvestorFeatures.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("INVESTOR_LOAN_INFORMATION", Order = 31)]
        public INVESTOR_LOAN_INFORMATION InvestorLoanInformation { get; set; }
    
        [XmlIgnore]
        public bool InvestorLoanInformationSpecified
        {
            get { return this.InvestorLoanInformation != null && this.InvestorLoanInformation.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("LATE_CHARGE", Order = 32)]
        public LATE_CHARGE LateCharge { get; set; }
    
        [XmlIgnore]
        public bool LateChargeSpecified
        {
            get { return this.LateCharge != null && this.LateCharge.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("LOAN_COMMENTS", Order = 33)]
        public LOAN_COMMENTS LoanComments { get; set; }
    
        [XmlIgnore]
        public bool LoanCommentsSpecified
        {
            get { return this.LoanComments != null && this.LoanComments.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("LOAN_DETAIL", Order = 34)]
        public LOAN_DETAIL LoanDetail { get; set; }
    
        [XmlIgnore]
        public bool LoanDetailSpecified
        {
            get { return this.LoanDetail != null && this.LoanDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("LOAN_IDENTIFIERS", Order = 35)]
        public LOAN_IDENTIFIERS LoanIdentifiers { get; set; }
    
        [XmlIgnore]
        public bool LoanIdentifiersSpecified
        {
            get { return this.LoanIdentifiers != null && this.LoanIdentifiers.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("LOAN_LEVEL_CREDIT", Order = 36)]
        public LOAN_LEVEL_CREDIT LoanLevelCredit { get; set; }
    
        [XmlIgnore]
        public bool LoanLevelCreditSpecified
        {
            get { return this.LoanLevelCredit != null && this.LoanLevelCredit.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("LOAN_PRODUCT", Order = 37)]
        public LOAN_PRODUCT LoanProduct { get; set; }
    
        [XmlIgnore]
        public bool LoanProductSpecified
        {
            get { return this.LoanProduct != null && this.LoanProduct.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("LOAN_PROGRAMS", Order = 38)]
        public LOAN_PROGRAMS LoanPrograms { get; set; }
    
        [XmlIgnore]
        public bool LoanProgramsSpecified
        {
            get { return this.LoanPrograms != null && this.LoanPrograms.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("LOAN_STATE", Order = 39)]
        public LOAN_STATE LoanState { get; set; }
    
        [XmlIgnore]
        public bool LoanStateSpecified
        {
            get { return this.LoanState != null && this.LoanState.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("LOAN_STATUSES", Order = 40)]
        public LOAN_STATUSES LoanStatuses { get; set; }
    
        [XmlIgnore]
        public bool LoanStatusesSpecified
        {
            get { return this.LoanStatuses != null && this.LoanStatuses.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("LTV", Order = 41)]
        public LTV Ltv { get; set; }
    
        [XmlIgnore]
        public bool LtvSpecified
        {
            get { return this.Ltv != null && this.Ltv.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("MATURITY", Order = 42)]
        public MATURITY Maturity { get; set; }
    
        [XmlIgnore]
        public bool MaturitySpecified
        {
            get { return this.Maturity != null && this.Maturity.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("MERS_REGISTRATIONS", Order = 43)]
        public MERS_REGISTRATIONS MersRegistrations { get; set; }
    
        [XmlIgnore]
        public bool MersRegistrationsSpecified
        {
            get { return this.MersRegistrations != null && this.MersRegistrations.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("MI_DATA", Order = 44)]
        public MI_DATA MiData { get; set; }
    
        [XmlIgnore]
        public bool MiDataSpecified
        {
            get { return this.MiData != null && this.MiData.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("MODIFICATIONS", Order = 45)]
        public MODIFICATIONS Modifications { get; set; }
    
        [XmlIgnore]
        public bool ModificationsSpecified
        {
            get { return this.Modifications != null && this.Modifications.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("MORTGAGE_SCORES", Order = 46)]
        public MORTGAGE_SCORES MortgageScores { get; set; }
    
        [XmlIgnore]
        public bool MortgageScoresSpecified
        {
            get { return this.MortgageScores != null && this.MortgageScores.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("NEGATIVE_AMORTIZATION", Order = 47)]
        public NEGATIVE_AMORTIZATION NegativeAmortization { get; set; }
    
        [XmlIgnore]
        public bool NegativeAmortizationSpecified
        {
            get { return this.NegativeAmortization != null && this.NegativeAmortization.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("OPTIONAL_PRODUCTS", Order = 48)]
        public OPTIONAL_PRODUCTS OptionalProducts { get; set; }
    
        [XmlIgnore]
        public bool OptionalProductsSpecified
        {
            get { return this.OptionalProducts != null && this.OptionalProducts.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("ORIGINATION_FUNDS", Order = 49)]
        public ORIGINATION_FUNDS OriginationFunds { get; set; }
    
        [XmlIgnore]
        public bool OriginationFundsSpecified
        {
            get { return this.OriginationFunds != null && this.OriginationFunds.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("ORIGINATION_SYSTEMS", Order = 50)]
        public ORIGINATION_SYSTEMS OriginationSystems { get; set; }
    
        [XmlIgnore]
        public bool OriginationSystemsSpecified
        {
            get { return this.OriginationSystems != null && this.OriginationSystems.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PAYMENT", Order = 51)]
        public PAYMENT Payment { get; set; }
    
        [XmlIgnore]
        public bool PaymentSpecified
        {
            get { return this.Payment != null && this.Payment.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PREPAYMENT_PENALTY", Order = 52)]
        public PREPAYMENT_PENALTY PrepaymentPenalty { get; set; }
    
        [XmlIgnore]
        public bool PrepaymentPenaltySpecified
        {
            get { return this.PrepaymentPenalty != null && this.PrepaymentPenalty.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PURCHASE_CREDITS", Order = 53)]
        public PURCHASE_CREDITS PurchaseCredits { get; set; }
    
        [XmlIgnore]
        public bool PurchaseCreditsSpecified
        {
            get { return this.PurchaseCredits != null && this.PurchaseCredits.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("QUALIFICATION", Order = 54)]
        public QUALIFICATION Qualification { get; set; }
    
        [XmlIgnore]
        public bool QualificationSpecified
        {
            get { return this.Qualification != null && this.Qualification.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("QUALIFIED_MORTGAGE", Order = 55)]
        public QUALIFIED_MORTGAGE QualifiedMortgage { get; set; }
    
        [XmlIgnore]
        public bool QualifiedMortgageSpecified
        {
            get { return this.QualifiedMortgage != null && this.QualifiedMortgage.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("REFINANCE", Order = 56)]
        public REFINANCE Refinance { get; set; }
    
        [XmlIgnore]
        public bool RefinanceSpecified
        {
            get { return this.Refinance != null && this.Refinance.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("REHABILITATION", Order = 57)]
        public REHABILITATION Rehabilitation { get; set; }
    
        [XmlIgnore]
        public bool RehabilitationSpecified
        {
            get { return this.Rehabilitation != null && this.Rehabilitation.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("REVERSE_MORTGAGE", Order = 58)]
        public REVERSE_MORTGAGE ReverseMortgage { get; set; }
    
        [XmlIgnore]
        public bool ReverseMortgageSpecified
        {
            get { return this.ReverseMortgage != null && this.ReverseMortgage.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SERVICING", Order = 59)]
        public SERVICING Servicing { get; set; }
    
        [XmlIgnore]
        public bool ServicingSpecified
        {
            get { return this.Servicing != null && this.Servicing.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("TERMS_OF_LOAN", Order = 60)]
        public TERMS_OF_LOAN TermsOfLoan { get; set; }
    
        [XmlIgnore]
        public bool TermsOfLoanSpecified
        {
            get { return this.TermsOfLoan != null && this.TermsOfLoan.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("TREASURY_NPV", Order = 61)]
        public TREASURY_NPV TreasuryNpv { get; set; }
    
        [XmlIgnore]
        public bool TreasuryNpvSpecified
        {
            get { return this.TreasuryNpv != null && this.TreasuryNpv.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("UNDERWRITING", Order = 62)]
        public UNDERWRITING Underwriting { get; set; }
    
        [XmlIgnore]
        public bool UnderwritingSpecified
        {
            get { return this.Underwriting != null && this.Underwriting.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("WORKOUTS", Order = 63)]
        public WORKOUTS Workouts { get; set; }
    
        [XmlIgnore]
        public bool WorkoutsSpecified
        {
            get { return this.Workouts != null && this.Workouts.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 64)]
        public LOAN_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    
        [XmlIgnore]
        public bool? IneligibleLoanProductIndicator { get; set; } = null;
    
        [XmlAttribute(AttributeName = "IneligibleLoanProductIndicator")]
        public string IneligibleLoanProductIndicatorStringified
        {
            get { return this.IneligibleLoanProductIndicator?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool IneligibleLoanProductIndicatorStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.IneligibleLoanProductIndicatorStringified); }
            set { }
        }
    
        [XmlIgnore]
        public LoanRoleBase? LoanRoleType { get; set; }

        [XmlAttribute(AttributeName = "LoanRoleType")]
        public string LoanRoleTypeStringified
        {
            get { return this.LoanRoleType?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool LoanRoleTypeStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.LoanRoleTypeStringified); }
            set { }
        }
    
        [XmlIgnore]
        public SelectionBase? SelectionType { get; set; }

        [XmlAttribute(AttributeName = "SelectionType")]
        public string SelectionTypeStringified
        {
            get { return this.SelectionType?.ToString() ?? string.Empty; }
            set { }
        }

        [XmlIgnore]
        public bool SelectionTypeStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SelectionTypeStringified); }
            set { }
        }

        [XmlAttribute("label", Form = XmlSchemaForm.Qualified, Namespace = Mismo3Constants.XlinkNamespace)]
        public string XlinkLabel { get; set; }

        [XmlIgnore]
        public bool XlinkLabelSpecified
        {
            get { return !string.IsNullOrEmpty(this.XlinkLabel); }
            set { }
        }
    }
}
