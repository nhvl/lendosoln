namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class BASEMENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BasementDetailSpecified
                    || this.BasementFeaturesSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BASEMENT_DETAIL", Order = 0)]
        public BASEMENT_DETAIL BasementDetail { get; set; }
    
        [XmlIgnore]
        public bool BasementDetailSpecified
        {
            get { return this.BasementDetail != null && this.BasementDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("BASEMENT_FEATURES", Order = 1)]
        public BASEMENT_FEATURES BasementFeatures { get; set; }
    
        [XmlIgnore]
        public bool BasementFeaturesSpecified
        {
            get { return this.BasementFeatures != null && this.BasementFeatures.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public BASEMENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
