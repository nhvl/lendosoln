namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class BASEMENT_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BasementCeilingMaterialDescriptionSpecified
                    || this.BasementExitTypeSpecified
                    || this.BasementFinishDescriptionSpecified
                    || this.BasementFinishedAreaSquareFeetNumberSpecified
                    || this.BasementFinishedIndicatorSpecified
                    || this.BasementFinishedPercentSpecified
                    || this.BasementFirstFloorAreaPercentSpecified
                    || this.BasementFloorMaterialDescriptionSpecified
                    || this.BasementWallMaterialDescriptionSpecified
                    || this.ComponentAdjustmentAmountSpecified
                    || this.ConditionRatingDescriptionSpecified
                    || this.ConditionRatingTypeSpecified
                    || this.QualityRatingDescriptionSpecified
                    || this.QualityRatingTypeSpecified
                    || this.SquareFeetNumberSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BasementCeilingMaterialDescription", Order = 0)]
        public MISMOString BasementCeilingMaterialDescription { get; set; }
    
        [XmlIgnore]
        public bool BasementCeilingMaterialDescriptionSpecified
        {
            get { return this.BasementCeilingMaterialDescription != null; }
            set { }
        }
    
        [XmlElement("BasementExitType", Order = 1)]
        public MISMOEnum<BasementExitBase> BasementExitType { get; set; }
    
        [XmlIgnore]
        public bool BasementExitTypeSpecified
        {
            get { return this.BasementExitType != null; }
            set { }
        }
    
        [XmlElement("BasementFinishDescription", Order = 2)]
        public MISMOString BasementFinishDescription { get; set; }
    
        [XmlIgnore]
        public bool BasementFinishDescriptionSpecified
        {
            get { return this.BasementFinishDescription != null; }
            set { }
        }
    
        [XmlElement("BasementFinishedAreaSquareFeetNumber", Order = 3)]
        public MISMONumeric BasementFinishedAreaSquareFeetNumber { get; set; }
    
        [XmlIgnore]
        public bool BasementFinishedAreaSquareFeetNumberSpecified
        {
            get { return this.BasementFinishedAreaSquareFeetNumber != null; }
            set { }
        }
    
        [XmlElement("BasementFinishedIndicator", Order = 4)]
        public MISMOIndicator BasementFinishedIndicator { get; set; }
    
        [XmlIgnore]
        public bool BasementFinishedIndicatorSpecified
        {
            get { return this.BasementFinishedIndicator != null; }
            set { }
        }
    
        [XmlElement("BasementFinishedPercent", Order = 5)]
        public MISMOPercent BasementFinishedPercent { get; set; }
    
        [XmlIgnore]
        public bool BasementFinishedPercentSpecified
        {
            get { return this.BasementFinishedPercent != null; }
            set { }
        }
    
        [XmlElement("BasementFirstFloorAreaPercent", Order = 6)]
        public MISMOPercent BasementFirstFloorAreaPercent { get; set; }
    
        [XmlIgnore]
        public bool BasementFirstFloorAreaPercentSpecified
        {
            get { return this.BasementFirstFloorAreaPercent != null; }
            set { }
        }
    
        [XmlElement("BasementFloorMaterialDescription", Order = 7)]
        public MISMOString BasementFloorMaterialDescription { get; set; }
    
        [XmlIgnore]
        public bool BasementFloorMaterialDescriptionSpecified
        {
            get { return this.BasementFloorMaterialDescription != null; }
            set { }
        }
    
        [XmlElement("BasementWallMaterialDescription", Order = 8)]
        public MISMOString BasementWallMaterialDescription { get; set; }
    
        [XmlIgnore]
        public bool BasementWallMaterialDescriptionSpecified
        {
            get { return this.BasementWallMaterialDescription != null; }
            set { }
        }
    
        [XmlElement("ComponentAdjustmentAmount", Order = 9)]
        public MISMOAmount ComponentAdjustmentAmount { get; set; }
    
        [XmlIgnore]
        public bool ComponentAdjustmentAmountSpecified
        {
            get { return this.ComponentAdjustmentAmount != null; }
            set { }
        }
    
        [XmlElement("ConditionRatingDescription", Order = 10)]
        public MISMOString ConditionRatingDescription { get; set; }
    
        [XmlIgnore]
        public bool ConditionRatingDescriptionSpecified
        {
            get { return this.ConditionRatingDescription != null; }
            set { }
        }
    
        [XmlElement("ConditionRatingType", Order = 11)]
        public MISMOEnum<ConditionRatingBase> ConditionRatingType { get; set; }
    
        [XmlIgnore]
        public bool ConditionRatingTypeSpecified
        {
            get { return this.ConditionRatingType != null; }
            set { }
        }
    
        [XmlElement("QualityRatingDescription", Order = 12)]
        public MISMOString QualityRatingDescription { get; set; }
    
        [XmlIgnore]
        public bool QualityRatingDescriptionSpecified
        {
            get { return this.QualityRatingDescription != null; }
            set { }
        }
    
        [XmlElement("QualityRatingType", Order = 13)]
        public MISMOEnum<QualityRatingBase> QualityRatingType { get; set; }
    
        [XmlIgnore]
        public bool QualityRatingTypeSpecified
        {
            get { return this.QualityRatingType != null; }
            set { }
        }
    
        [XmlElement("SquareFeetNumber", Order = 14)]
        public MISMONumeric SquareFeetNumber { get; set; }
    
        [XmlIgnore]
        public bool SquareFeetNumberSpecified
        {
            get { return this.SquareFeetNumber != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 15)]
        public BASEMENT_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
