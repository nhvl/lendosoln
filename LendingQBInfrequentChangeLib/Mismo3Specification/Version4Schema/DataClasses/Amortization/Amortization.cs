namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class AMORTIZATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AmortizationRuleSpecified
                    || this.AmortizationScheduleItemsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AMORTIZATION_RULE", Order = 0)]
        public AMORTIZATION_RULE AmortizationRule { get; set; }
    
        [XmlIgnore]
        public bool AmortizationRuleSpecified
        {
            get { return this.AmortizationRule != null && this.AmortizationRule.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("AMORTIZATION_SCHEDULE_ITEMS", Order = 1)]
        public AMORTIZATION_SCHEDULE_ITEMS AmortizationScheduleItems { get; set; }
    
        [XmlIgnore]
        public bool AmortizationScheduleItemsSpecified
        {
            get { return this.AmortizationScheduleItems != null && this.AmortizationScheduleItems.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public AMORTIZATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
