namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class SUPPORTING_RECORDS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SupportingRecordListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SUPPORTING_RECORD", Order = 0)]
        public List<SUPPORTING_RECORD> SupportingRecordList { get; set; } = new List<SUPPORTING_RECORD>();
    
        [XmlIgnore]
        public bool SupportingRecordListSpecified
        {
            get { return this.SupportingRecordList != null && this.SupportingRecordList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public SUPPORTING_RECORDS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
