namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class FLOOD_DETERMINATION_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.FloodCertificationIdentifierSpecified
                    || this.FloodDeterminationLifeOfLoanIndicatorSpecified
                    || this.FloodDiscrepancyGrandfatheredIndicatorSpecified
                    || this.FloodDiscrepancyIndicatorSpecified
                    || this.FloodDiscrepancyResolvedIndicatorSpecified
                    || this.FloodPartialIndicatorSpecified
                    || this.FloodProductCertifyDateSpecified
                    || this.NFIPCommunityFIRMDateSpecified
                    || this.NFIPCommunityIdentifierSpecified
                    || this.NFIPCommunityNameSpecified
                    || this.NFIPCommunityParticipationStartDateSpecified
                    || this.NFIPCommunityParticipationStatusTypeSpecified
                    || this.NFIPCommunityParticipationStatusTypeOtherDescriptionSpecified
                    || this.NFIPCountyNameSpecified
                    || this.NFIPFloodDataRevisionTypeSpecified
                    || this.NFIPFloodZoneIdentifierSpecified
                    || this.NFIPLetterOfMapDateSpecified
                    || this.NFIPMapIdentifierSpecified
                    || this.NFIPMapIndicatorSpecified
                    || this.NFIPMapPanelDateSpecified
                    || this.NFIPMapPanelIdentifierSpecified
                    || this.NFIPMapPanelSuffixIdentifierSpecified
                    || this.NFIPStateCodeSpecified
                    || this.ProtectedAreaDesignationDateSpecified
                    || this.ProtectedAreaIndicatorSpecified
                    || this.SpecialFloodHazardAreaIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("FloodCertificationIdentifier", Order = 0)]
        public MISMOIdentifier FloodCertificationIdentifier { get; set; }
    
        [XmlIgnore]
        public bool FloodCertificationIdentifierSpecified
        {
            get { return this.FloodCertificationIdentifier != null; }
            set { }
        }
    
        [XmlElement("FloodDeterminationLifeOfLoanIndicator", Order = 1)]
        public MISMOIndicator FloodDeterminationLifeOfLoanIndicator { get; set; }
    
        [XmlIgnore]
        public bool FloodDeterminationLifeOfLoanIndicatorSpecified
        {
            get { return this.FloodDeterminationLifeOfLoanIndicator != null; }
            set { }
        }
    
        [XmlElement("FloodDiscrepancyGrandfatheredIndicator", Order = 2)]
        public MISMOIndicator FloodDiscrepancyGrandfatheredIndicator { get; set; }
    
        [XmlIgnore]
        public bool FloodDiscrepancyGrandfatheredIndicatorSpecified
        {
            get { return this.FloodDiscrepancyGrandfatheredIndicator != null; }
            set { }
        }
    
        [XmlElement("FloodDiscrepancyIndicator", Order = 3)]
        public MISMOIndicator FloodDiscrepancyIndicator { get; set; }
    
        [XmlIgnore]
        public bool FloodDiscrepancyIndicatorSpecified
        {
            get { return this.FloodDiscrepancyIndicator != null; }
            set { }
        }
    
        [XmlElement("FloodDiscrepancyResolvedIndicator", Order = 4)]
        public MISMOIndicator FloodDiscrepancyResolvedIndicator { get; set; }
    
        [XmlIgnore]
        public bool FloodDiscrepancyResolvedIndicatorSpecified
        {
            get { return this.FloodDiscrepancyResolvedIndicator != null; }
            set { }
        }
    
        [XmlElement("FloodPartialIndicator", Order = 5)]
        public MISMOIndicator FloodPartialIndicator { get; set; }
    
        [XmlIgnore]
        public bool FloodPartialIndicatorSpecified
        {
            get { return this.FloodPartialIndicator != null; }
            set { }
        }
    
        [XmlElement("FloodProductCertifyDate", Order = 6)]
        public MISMODate FloodProductCertifyDate { get; set; }
    
        [XmlIgnore]
        public bool FloodProductCertifyDateSpecified
        {
            get { return this.FloodProductCertifyDate != null; }
            set { }
        }
    
        [XmlElement("NFIPCommunityFIRMDate", Order = 7)]
        public MISMODate NFIPCommunityFIRMDate { get; set; }
    
        [XmlIgnore]
        public bool NFIPCommunityFIRMDateSpecified
        {
            get { return this.NFIPCommunityFIRMDate != null; }
            set { }
        }
    
        [XmlElement("NFIPCommunityIdentifier", Order = 8)]
        public MISMOIdentifier NFIPCommunityIdentifier { get; set; }
    
        [XmlIgnore]
        public bool NFIPCommunityIdentifierSpecified
        {
            get { return this.NFIPCommunityIdentifier != null; }
            set { }
        }
    
        [XmlElement("NFIPCommunityName", Order = 9)]
        public MISMOString NFIPCommunityName { get; set; }
    
        [XmlIgnore]
        public bool NFIPCommunityNameSpecified
        {
            get { return this.NFIPCommunityName != null; }
            set { }
        }
    
        [XmlElement("NFIPCommunityParticipationStartDate", Order = 10)]
        public MISMODate NFIPCommunityParticipationStartDate { get; set; }
    
        [XmlIgnore]
        public bool NFIPCommunityParticipationStartDateSpecified
        {
            get { return this.NFIPCommunityParticipationStartDate != null; }
            set { }
        }
    
        [XmlElement("NFIPCommunityParticipationStatusType", Order = 11)]
        public MISMOEnum<NFIPCommunityParticipationStatusBase> NFIPCommunityParticipationStatusType { get; set; }
    
        [XmlIgnore]
        public bool NFIPCommunityParticipationStatusTypeSpecified
        {
            get { return this.NFIPCommunityParticipationStatusType != null; }
            set { }
        }
    
        [XmlElement("NFIPCommunityParticipationStatusTypeOtherDescription", Order = 12)]
        public MISMOString NFIPCommunityParticipationStatusTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool NFIPCommunityParticipationStatusTypeOtherDescriptionSpecified
        {
            get { return this.NFIPCommunityParticipationStatusTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("NFIPCountyName", Order = 13)]
        public MISMOString NFIPCountyName { get; set; }
    
        [XmlIgnore]
        public bool NFIPCountyNameSpecified
        {
            get { return this.NFIPCountyName != null; }
            set { }
        }
    
        [XmlElement("NFIPFloodDataRevisionType", Order = 14)]
        public MISMOEnum<NFIPFloodDataRevisionBase> NFIPFloodDataRevisionType { get; set; }
    
        [XmlIgnore]
        public bool NFIPFloodDataRevisionTypeSpecified
        {
            get { return this.NFIPFloodDataRevisionType != null; }
            set { }
        }
    
        [XmlElement("NFIPFloodZoneIdentifier", Order = 15)]
        public MISMOIdentifier NFIPFloodZoneIdentifier { get; set; }
    
        [XmlIgnore]
        public bool NFIPFloodZoneIdentifierSpecified
        {
            get { return this.NFIPFloodZoneIdentifier != null; }
            set { }
        }
    
        [XmlElement("NFIPLetterOfMapDate", Order = 16)]
        public MISMODate NFIPLetterOfMapDate { get; set; }
    
        [XmlIgnore]
        public bool NFIPLetterOfMapDateSpecified
        {
            get { return this.NFIPLetterOfMapDate != null; }
            set { }
        }
    
        [XmlElement("NFIPMapIdentifier", Order = 17)]
        public MISMOIdentifier NFIPMapIdentifier { get; set; }
    
        [XmlIgnore]
        public bool NFIPMapIdentifierSpecified
        {
            get { return this.NFIPMapIdentifier != null; }
            set { }
        }
    
        [XmlElement("NFIPMapIndicator", Order = 18)]
        public MISMOIndicator NFIPMapIndicator { get; set; }
    
        [XmlIgnore]
        public bool NFIPMapIndicatorSpecified
        {
            get { return this.NFIPMapIndicator != null; }
            set { }
        }
    
        [XmlElement("NFIPMapPanelDate", Order = 19)]
        public MISMODate NFIPMapPanelDate { get; set; }
    
        [XmlIgnore]
        public bool NFIPMapPanelDateSpecified
        {
            get { return this.NFIPMapPanelDate != null; }
            set { }
        }
    
        [XmlElement("NFIPMapPanelIdentifier", Order = 20)]
        public MISMOIdentifier NFIPMapPanelIdentifier { get; set; }
    
        [XmlIgnore]
        public bool NFIPMapPanelIdentifierSpecified
        {
            get { return this.NFIPMapPanelIdentifier != null; }
            set { }
        }
    
        [XmlElement("NFIPMapPanelSuffixIdentifier", Order = 21)]
        public MISMOIdentifier NFIPMapPanelSuffixIdentifier { get; set; }
    
        [XmlIgnore]
        public bool NFIPMapPanelSuffixIdentifierSpecified
        {
            get { return this.NFIPMapPanelSuffixIdentifier != null; }
            set { }
        }
    
        [XmlElement("NFIPStateCode", Order = 22)]
        public MISMOCode NFIPStateCode { get; set; }
    
        [XmlIgnore]
        public bool NFIPStateCodeSpecified
        {
            get { return this.NFIPStateCode != null; }
            set { }
        }
    
        [XmlElement("ProtectedAreaDesignationDate", Order = 23)]
        public MISMODate ProtectedAreaDesignationDate { get; set; }
    
        [XmlIgnore]
        public bool ProtectedAreaDesignationDateSpecified
        {
            get { return this.ProtectedAreaDesignationDate != null; }
            set { }
        }
    
        [XmlElement("ProtectedAreaIndicator", Order = 24)]
        public MISMOIndicator ProtectedAreaIndicator { get; set; }
    
        [XmlIgnore]
        public bool ProtectedAreaIndicatorSpecified
        {
            get { return this.ProtectedAreaIndicator != null; }
            set { }
        }
    
        [XmlElement("SpecialFloodHazardAreaIndicator", Order = 25)]
        public MISMOIndicator SpecialFloodHazardAreaIndicator { get; set; }
    
        [XmlIgnore]
        public bool SpecialFloodHazardAreaIndicatorSpecified
        {
            get { return this.SpecialFloodHazardAreaIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 26)]
        public FLOOD_DETERMINATION_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
