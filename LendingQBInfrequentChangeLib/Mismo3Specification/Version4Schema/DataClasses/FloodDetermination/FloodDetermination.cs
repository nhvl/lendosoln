namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class FLOOD_DETERMINATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.FloodDeterminationDetailSpecified
                    || this.FloodRiskFactorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("FLOOD_DETERMINATION_DETAIL", Order = 0)]
        public FLOOD_DETERMINATION_DETAIL FloodDeterminationDetail { get; set; }
    
        [XmlIgnore]
        public bool FloodDeterminationDetailSpecified
        {
            get { return this.FloodDeterminationDetail != null && this.FloodDeterminationDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("FLOOD_RISK_FACTOR", Order = 1)]
        public FLOOD_RISK_FACTOR FloodRiskFactor { get; set; }
    
        [XmlIgnore]
        public bool FloodRiskFactorSpecified
        {
            get { return this.FloodRiskFactor != null && this.FloodRiskFactor.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public FLOOD_DETERMINATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
