namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Schema;
    using System.Xml.Serialization;

    public class EXPENSE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AlimonyOwedToNameSpecified
                    || this.ExpenseDescriptionSpecified
                    || this.ExpenseMonthlyPaymentAmountSpecified
                    || this.ExpenseRemainingTermMonthsCountSpecified
                    || this.ExpenseTypeSpecified
                    || this.ExpenseTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AlimonyOwedToName", Order = 0)]
        public MISMOString AlimonyOwedToName { get; set; }
    
        [XmlIgnore]
        public bool AlimonyOwedToNameSpecified
        {
            get { return this.AlimonyOwedToName != null; }
            set { }
        }
    
        [XmlElement("ExpenseDescription", Order = 1)]
        public MISMOString ExpenseDescription { get; set; }
    
        [XmlIgnore]
        public bool ExpenseDescriptionSpecified
        {
            get { return this.ExpenseDescription != null; }
            set { }
        }
    
        [XmlElement("ExpenseMonthlyPaymentAmount", Order = 2)]
        public MISMOAmount ExpenseMonthlyPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool ExpenseMonthlyPaymentAmountSpecified
        {
            get { return this.ExpenseMonthlyPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("ExpenseRemainingTermMonthsCount", Order = 3)]
        public MISMOCount ExpenseRemainingTermMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool ExpenseRemainingTermMonthsCountSpecified
        {
            get { return this.ExpenseRemainingTermMonthsCount != null; }
            set { }
        }
    
        [XmlElement("ExpenseType", Order = 4)]
        public MISMOEnum<ExpenseBase> ExpenseType { get; set; }
    
        [XmlIgnore]
        public bool ExpenseTypeSpecified
        {
            get { return this.ExpenseType != null; }
            set { }
        }
    
        [XmlElement("ExpenseTypeOtherDescription", Order = 5)]
        public MISMOString ExpenseTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ExpenseTypeOtherDescriptionSpecified
        {
            get { return this.ExpenseTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 6)]
        public EXPENSE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }

        [XmlAttribute("label", Form = XmlSchemaForm.Qualified, Namespace = Mismo3Constants.XlinkNamespace)]
        public string XlinkLabel { get; set; }

        [XmlIgnore]
        public bool XlinkLabelSpecified
        {
            get { return !string.IsNullOrEmpty(this.XlinkLabel); }
            set { }
        }
    }
}
