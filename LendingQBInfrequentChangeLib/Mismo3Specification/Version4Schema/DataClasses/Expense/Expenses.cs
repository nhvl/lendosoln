namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class EXPENSES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExpenseListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("EXPENSE", Order = 0)]
        public List<EXPENSE> ExpenseList { get; set; } = new List<EXPENSE>();
    
        [XmlIgnore]
        public bool ExpenseListSpecified
        {
            get { return this.ExpenseList != null && this.ExpenseList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public EXPENSES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
