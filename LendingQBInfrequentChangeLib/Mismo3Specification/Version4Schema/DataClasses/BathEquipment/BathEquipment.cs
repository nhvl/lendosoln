namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class BATH_EQUIPMENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BathEquipmentCountSpecified
                    || this.BathEquipmentDescriptionSpecified
                    || this.BathEquipmentTypeSpecified
                    || this.BathEquipmentTypeOtherDescriptionSpecified
                    || this.ComponentAdjustmentAmountSpecified
                    || this.ComponentClassificationTypeSpecified
                    || this.ConditionRatingDescriptionSpecified
                    || this.ConditionRatingTypeSpecified
                    || this.QualityRatingDescriptionSpecified
                    || this.QualityRatingTypeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BathEquipmentCount", Order = 0)]
        public MISMOCount BathEquipmentCount { get; set; }
    
        [XmlIgnore]
        public bool BathEquipmentCountSpecified
        {
            get { return this.BathEquipmentCount != null; }
            set { }
        }
    
        [XmlElement("BathEquipmentDescription", Order = 1)]
        public MISMOString BathEquipmentDescription { get; set; }
    
        [XmlIgnore]
        public bool BathEquipmentDescriptionSpecified
        {
            get { return this.BathEquipmentDescription != null; }
            set { }
        }
    
        [XmlElement("BathEquipmentType", Order = 2)]
        public MISMOEnum<BathEquipmentBase> BathEquipmentType { get; set; }
    
        [XmlIgnore]
        public bool BathEquipmentTypeSpecified
        {
            get { return this.BathEquipmentType != null; }
            set { }
        }
    
        [XmlElement("BathEquipmentTypeOtherDescription", Order = 3)]
        public MISMOString BathEquipmentTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool BathEquipmentTypeOtherDescriptionSpecified
        {
            get { return this.BathEquipmentTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ComponentAdjustmentAmount", Order = 4)]
        public MISMOAmount ComponentAdjustmentAmount { get; set; }
    
        [XmlIgnore]
        public bool ComponentAdjustmentAmountSpecified
        {
            get { return this.ComponentAdjustmentAmount != null; }
            set { }
        }
    
        [XmlElement("ComponentClassificationType", Order = 5)]
        public MISMOEnum<ComponentClassificationBase> ComponentClassificationType { get; set; }
    
        [XmlIgnore]
        public bool ComponentClassificationTypeSpecified
        {
            get { return this.ComponentClassificationType != null; }
            set { }
        }
    
        [XmlElement("ConditionRatingDescription", Order = 6)]
        public MISMOString ConditionRatingDescription { get; set; }
    
        [XmlIgnore]
        public bool ConditionRatingDescriptionSpecified
        {
            get { return this.ConditionRatingDescription != null; }
            set { }
        }
    
        [XmlElement("ConditionRatingType", Order = 7)]
        public MISMOEnum<ConditionRatingBase> ConditionRatingType { get; set; }
    
        [XmlIgnore]
        public bool ConditionRatingTypeSpecified
        {
            get { return this.ConditionRatingType != null; }
            set { }
        }
    
        [XmlElement("QualityRatingDescription", Order = 8)]
        public MISMOString QualityRatingDescription { get; set; }
    
        [XmlIgnore]
        public bool QualityRatingDescriptionSpecified
        {
            get { return this.QualityRatingDescription != null; }
            set { }
        }
    
        [XmlElement("QualityRatingType", Order = 9)]
        public MISMOEnum<QualityRatingBase> QualityRatingType { get; set; }
    
        [XmlIgnore]
        public bool QualityRatingTypeSpecified
        {
            get { return this.QualityRatingType != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 10)]
        public BATH_EQUIPMENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
