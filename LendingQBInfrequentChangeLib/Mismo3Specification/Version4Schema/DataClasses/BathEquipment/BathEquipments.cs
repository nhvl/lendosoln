namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class BATH_EQUIPMENTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BathEquipmentListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BATH_EQUIPMENT", Order = 0)]
        public List<BATH_EQUIPMENT> BathEquipmentList { get; set; } = new List<BATH_EQUIPMENT>();
    
        [XmlIgnore]
        public bool BathEquipmentListSpecified
        {
            get { return this.BathEquipmentList != null && this.BathEquipmentList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public BATH_EQUIPMENTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
