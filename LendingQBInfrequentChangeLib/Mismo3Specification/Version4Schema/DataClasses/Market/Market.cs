namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MARKET
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MarketInventoriesSpecified
                    || this.MarketTrendSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MARKET_INVENTORIES", Order = 0)]
        public MARKET_INVENTORIES MarketInventories { get; set; }
    
        [XmlIgnore]
        public bool MarketInventoriesSpecified
        {
            get { return this.MarketInventories != null && this.MarketInventories.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("MARKET_TREND", Order = 1)]
        public MARKET_TREND MarketTrend { get; set; }
    
        [XmlIgnore]
        public bool MarketTrendSpecified
        {
            get { return this.MarketTrend != null && this.MarketTrend.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public MARKET_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
