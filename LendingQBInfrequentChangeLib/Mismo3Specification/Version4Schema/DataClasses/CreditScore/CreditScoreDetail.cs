namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CREDIT_SCORE_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditReportIdentifierSpecified
                    || this.CreditReportTypeSpecified
                    || this.CreditReportTypeOtherDescriptionSpecified
                    || this.CreditRepositorySingleSourceIndicatorSpecified
                    || this.CreditRepositorySourceIndicatorSpecified
                    || this.CreditRepositorySourceTypeSpecified
                    || this.CreditRepositorySourceTypeOtherDescriptionSpecified
                    || this.CreditScoreCategoryTypeSpecified
                    || this.CreditScoreCategoryTypeOtherDescriptionSpecified
                    || this.CreditScoreCategoryVersionTypeSpecified
                    || this.CreditScoreCategoryVersionTypeOtherDescriptionSpecified
                    || this.CreditScoreDateSpecified
                    || this.CreditScoreExclusionReasonTypeSpecified
                    || this.CreditScoreFACTAInquiriesIndicatorSpecified
                    || this.CreditScoreImpairmentTypeSpecified
                    || this.CreditScoreImpairmentTypeOtherDescriptionSpecified
                    || this.CreditScoreModelNameTypeSpecified
                    || this.CreditScoreModelNameTypeOtherDescriptionSpecified
                    || this.CreditScoreRankPercentileDescriptionSpecified
                    || this.CreditScoreRankPercentileValueSpecified
                    || this.CreditScoreValueSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CreditReportIdentifier", Order = 0)]
        public MISMOIdentifier CreditReportIdentifier { get; set; }
    
        [XmlIgnore]
        public bool CreditReportIdentifierSpecified
        {
            get { return this.CreditReportIdentifier != null; }
            set { }
        }
    
        [XmlElement("CreditReportType", Order = 1)]
        public MISMOEnum<CreditReportBase> CreditReportType { get; set; }
    
        [XmlIgnore]
        public bool CreditReportTypeSpecified
        {
            get { return this.CreditReportType != null; }
            set { }
        }
    
        [XmlElement("CreditReportTypeOtherDescription", Order = 2)]
        public MISMOString CreditReportTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CreditReportTypeOtherDescriptionSpecified
        {
            get { return this.CreditReportTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("CreditRepositorySingleSourceIndicator", Order = 3)]
        public MISMOIndicator CreditRepositorySingleSourceIndicator { get; set; }
    
        [XmlIgnore]
        public bool CreditRepositorySingleSourceIndicatorSpecified
        {
            get { return this.CreditRepositorySingleSourceIndicator != null; }
            set { }
        }
    
        [XmlElement("CreditRepositorySourceIndicator", Order = 4)]
        public MISMOIndicator CreditRepositorySourceIndicator { get; set; }
    
        [XmlIgnore]
        public bool CreditRepositorySourceIndicatorSpecified
        {
            get { return this.CreditRepositorySourceIndicator != null; }
            set { }
        }
    
        [XmlElement("CreditRepositorySourceType", Order = 5)]
        public MISMOEnum<CreditRepositorySourceBase> CreditRepositorySourceType { get; set; }
    
        [XmlIgnore]
        public bool CreditRepositorySourceTypeSpecified
        {
            get { return this.CreditRepositorySourceType != null; }
            set { }
        }
    
        [XmlElement("CreditRepositorySourceTypeOtherDescription", Order = 6)]
        public MISMOString CreditRepositorySourceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CreditRepositorySourceTypeOtherDescriptionSpecified
        {
            get { return this.CreditRepositorySourceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("CreditScoreCategoryType", Order = 7)]
        public MISMOEnum<CreditScoreCategoryBase> CreditScoreCategoryType { get; set; }
    
        [XmlIgnore]
        public bool CreditScoreCategoryTypeSpecified
        {
            get { return this.CreditScoreCategoryType != null; }
            set { }
        }
    
        [XmlElement("CreditScoreCategoryTypeOtherDescription", Order = 8)]
        public MISMOString CreditScoreCategoryTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CreditScoreCategoryTypeOtherDescriptionSpecified
        {
            get { return this.CreditScoreCategoryTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("CreditScoreCategoryVersionType", Order = 9)]
        public MISMOEnum<CreditScoreCategoryVersionBase> CreditScoreCategoryVersionType { get; set; }
    
        [XmlIgnore]
        public bool CreditScoreCategoryVersionTypeSpecified
        {
            get { return this.CreditScoreCategoryVersionType != null; }
            set { }
        }
    
        [XmlElement("CreditScoreCategoryVersionTypeOtherDescription", Order = 10)]
        public MISMOString CreditScoreCategoryVersionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CreditScoreCategoryVersionTypeOtherDescriptionSpecified
        {
            get { return this.CreditScoreCategoryVersionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("CreditScoreDate", Order = 11)]
        public MISMODate CreditScoreDate { get; set; }
    
        [XmlIgnore]
        public bool CreditScoreDateSpecified
        {
            get { return this.CreditScoreDate != null; }
            set { }
        }
    
        [XmlElement("CreditScoreExclusionReasonType", Order = 12)]
        public MISMOEnum<CreditScoreExclusionReasonBase> CreditScoreExclusionReasonType { get; set; }
    
        [XmlIgnore]
        public bool CreditScoreExclusionReasonTypeSpecified
        {
            get { return this.CreditScoreExclusionReasonType != null; }
            set { }
        }
    
        [XmlElement("CreditScoreFACTAInquiriesIndicator", Order = 13)]
        public MISMOIndicator CreditScoreFACTAInquiriesIndicator { get; set; }
    
        [XmlIgnore]
        public bool CreditScoreFACTAInquiriesIndicatorSpecified
        {
            get { return this.CreditScoreFACTAInquiriesIndicator != null; }
            set { }
        }
    
        [XmlElement("CreditScoreImpairmentType", Order = 14)]
        public MISMOEnum<CreditScoreImpairmentBase> CreditScoreImpairmentType { get; set; }
    
        [XmlIgnore]
        public bool CreditScoreImpairmentTypeSpecified
        {
            get { return this.CreditScoreImpairmentType != null; }
            set { }
        }
    
        [XmlElement("CreditScoreImpairmentTypeOtherDescription", Order = 15)]
        public MISMOString CreditScoreImpairmentTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CreditScoreImpairmentTypeOtherDescriptionSpecified
        {
            get { return this.CreditScoreImpairmentTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("CreditScoreModelNameType", Order = 16)]
        public MISMOEnum<CreditScoreModelNameBase> CreditScoreModelNameType { get; set; }
    
        [XmlIgnore]
        public bool CreditScoreModelNameTypeSpecified
        {
            get { return this.CreditScoreModelNameType != null; }
            set { }
        }
    
        [XmlElement("CreditScoreModelNameTypeOtherDescription", Order = 17)]
        public MISMOString CreditScoreModelNameTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CreditScoreModelNameTypeOtherDescriptionSpecified
        {
            get { return this.CreditScoreModelNameTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("CreditScoreRankPercentileDescription", Order = 18)]
        public MISMOString CreditScoreRankPercentileDescription { get; set; }
    
        [XmlIgnore]
        public bool CreditScoreRankPercentileDescriptionSpecified
        {
            get { return this.CreditScoreRankPercentileDescription != null; }
            set { }
        }
    
        [XmlElement("CreditScoreRankPercentileValue", Order = 19)]
        public MISMOValue CreditScoreRankPercentileValue { get; set; }
    
        [XmlIgnore]
        public bool CreditScoreRankPercentileValueSpecified
        {
            get { return this.CreditScoreRankPercentileValue != null; }
            set { }
        }
    
        [XmlElement("CreditScoreValue", Order = 20)]
        public MISMOValue CreditScoreValue { get; set; }
    
        [XmlIgnore]
        public bool CreditScoreValueSpecified
        {
            get { return this.CreditScoreValue != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 21)]
        public CREDIT_SCORE_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
