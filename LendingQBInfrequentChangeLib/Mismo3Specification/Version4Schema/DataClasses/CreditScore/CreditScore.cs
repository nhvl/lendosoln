namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CREDIT_SCORE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditScoreDetailSpecified
                    || this.CreditScoreFactorsSpecified
                    || this.CreditScoreProviderSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CREDIT_SCORE_DETAIL", Order = 0)]
        public CREDIT_SCORE_DETAIL CreditScoreDetail { get; set; }
    
        [XmlIgnore]
        public bool CreditScoreDetailSpecified
        {
            get { return this.CreditScoreDetail != null && this.CreditScoreDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_SCORE_FACTORS", Order = 1)]
        public CREDIT_SCORE_FACTORS CreditScoreFactors { get; set; }
    
        [XmlIgnore]
        public bool CreditScoreFactorsSpecified
        {
            get { return this.CreditScoreFactors != null && this.CreditScoreFactors.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_SCORE_PROVIDER", Order = 2)]
        public CREDIT_SCORE_PROVIDER CreditScoreProvider { get; set; }
    
        [XmlIgnore]
        public bool CreditScoreProviderSpecified
        {
            get { return this.CreditScoreProvider != null && this.CreditScoreProvider.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public CREDIT_SCORE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
