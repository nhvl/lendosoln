namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class CREDIT_SCORES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditScoreListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CREDIT_SCORE", Order = 0)]
        public List<CREDIT_SCORE> CreditScoreList { get; set; } = new List<CREDIT_SCORE>();
    
        [XmlIgnore]
        public bool CreditScoreListSpecified
        {
            get { return this.CreditScoreList != null && this.CreditScoreList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public CREDIT_SCORES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
