namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class VERIFICATION_DATA
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.VerificationDataInvestorSpecified
                    || this.VerificationDataLoanSpecified
                    || this.VerificationDataPayeeSpecified
                    || this.VerificationDataPoolSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("VERIFICATION_DATA_INVESTOR", Order = 0)]
        public VERIFICATION_DATA_INVESTOR VerificationDataInvestor { get; set; }
    
        [XmlIgnore]
        public bool VerificationDataInvestorSpecified
        {
            get { return this.VerificationDataInvestor != null && this.VerificationDataInvestor.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("VERIFICATION_DATA_LOAN", Order = 1)]
        public VERIFICATION_DATA_LOAN VerificationDataLoan { get; set; }
    
        [XmlIgnore]
        public bool VerificationDataLoanSpecified
        {
            get { return this.VerificationDataLoan != null && this.VerificationDataLoan.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("VERIFICATION_DATA_PAYEE", Order = 2)]
        public VERIFICATION_DATA_PAYEE VerificationDataPayee { get; set; }
    
        [XmlIgnore]
        public bool VerificationDataPayeeSpecified
        {
            get { return this.VerificationDataPayee != null && this.VerificationDataPayee.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("VERIFICATION_DATA_POOL", Order = 3)]
        public VERIFICATION_DATA_POOL VerificationDataPool { get; set; }
    
        [XmlIgnore]
        public bool VerificationDataPoolSpecified
        {
            get { return this.VerificationDataPool != null && this.VerificationDataPool.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public VERIFICATION_DATA_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
