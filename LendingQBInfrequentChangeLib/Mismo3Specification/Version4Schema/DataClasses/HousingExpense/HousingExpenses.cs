namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class HOUSING_EXPENSES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.HousingExpenseListSpecified
                    || this.HousingExpenseSummarySpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("HOUSING_EXPENSE", Order = 0)]
        public List<HOUSING_EXPENSE> HousingExpenseList { get; set; } = new List<HOUSING_EXPENSE>();
    
        [XmlIgnore]
        public bool HousingExpenseListSpecified
        {
            get { return this.HousingExpenseList != null && this.HousingExpenseList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("HOUSING_EXPENSE_SUMMARY", Order = 1)]
        public HOUSING_EXPENSE_SUMMARY HousingExpenseSummary { get; set; }
    
        [XmlIgnore]
        public bool HousingExpenseSummarySpecified
        {
            get { return this.HousingExpenseSummary != null && this.HousingExpenseSummary.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public HOUSING_EXPENSES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
