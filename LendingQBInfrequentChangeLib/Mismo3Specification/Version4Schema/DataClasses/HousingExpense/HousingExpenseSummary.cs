namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class HOUSING_EXPENSE_SUMMARY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.HousingExpensePresentTotalMonthlyPaymentAmountSpecified
                    || this.HousingExpenseProposedTotalMonthlyPaymentAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("HousingExpensePresentTotalMonthlyPaymentAmount", Order = 0)]
        public MISMOAmount HousingExpensePresentTotalMonthlyPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool HousingExpensePresentTotalMonthlyPaymentAmountSpecified
        {
            get { return this.HousingExpensePresentTotalMonthlyPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("HousingExpenseProposedTotalMonthlyPaymentAmount", Order = 1)]
        public MISMOAmount HousingExpenseProposedTotalMonthlyPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool HousingExpenseProposedTotalMonthlyPaymentAmountSpecified
        {
            get { return this.HousingExpenseProposedTotalMonthlyPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public HOUSING_EXPENSE_SUMMARY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
