namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class HOUSING_EXPENSE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.HousingExpensePaymentAmountSpecified
                    || this.HousingExpenseTimingTypeSpecified
                    || this.HousingExpenseTypeSpecified
                    || this.HousingExpenseTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("HousingExpensePaymentAmount", Order = 0)]
        public MISMOAmount HousingExpensePaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool HousingExpensePaymentAmountSpecified
        {
            get { return this.HousingExpensePaymentAmount != null; }
            set { }
        }
    
        [XmlElement("HousingExpenseTimingType", Order = 1)]
        public MISMOEnum<HousingExpenseTimingBase> HousingExpenseTimingType { get; set; }
    
        [XmlIgnore]
        public bool HousingExpenseTimingTypeSpecified
        {
            get { return this.HousingExpenseTimingType != null; }
            set { }
        }
    
        [XmlElement("HousingExpenseType", Order = 2)]
        public MISMOEnum<HousingExpenseBase> HousingExpenseType { get; set; }
    
        [XmlIgnore]
        public bool HousingExpenseTypeSpecified
        {
            get { return this.HousingExpenseType != null; }
            set { }
        }
    
        [XmlElement("HousingExpenseTypeOtherDescription", Order = 3)]
        public MISMOString HousingExpenseTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool HousingExpenseTypeOtherDescriptionSpecified
        {
            get { return this.HousingExpenseTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public HOUSING_EXPENSE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
