namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class REQUESTING_PARTY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.InternalAccountIdentifierSpecified
                    || this.RequestedByNameSpecified
                    || this.RequestingPartyBranchIdentifierSpecified
                    || this.RequestingPartySequenceNumberSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("InternalAccountIdentifier", Order = 0)]
        public MISMOIdentifier InternalAccountIdentifier { get; set; }
    
        [XmlIgnore]
        public bool InternalAccountIdentifierSpecified
        {
            get { return this.InternalAccountIdentifier != null; }
            set { }
        }
    
        [XmlElement("RequestedByName", Order = 1)]
        public MISMOString RequestedByName { get; set; }
    
        [XmlIgnore]
        public bool RequestedByNameSpecified
        {
            get { return this.RequestedByName != null; }
            set { }
        }
    
        [XmlElement("RequestingPartyBranchIdentifier", Order = 2)]
        public MISMOIdentifier RequestingPartyBranchIdentifier { get; set; }
    
        [XmlIgnore]
        public bool RequestingPartyBranchIdentifierSpecified
        {
            get { return this.RequestingPartyBranchIdentifier != null; }
            set { }
        }
    
        [XmlElement("RequestingPartySequenceNumber", Order = 3)]
        public MISMOSequenceNumber RequestingPartySequenceNumber { get; set; }
    
        [XmlIgnore]
        public bool RequestingPartySequenceNumberSpecified
        {
            get { return this.RequestingPartySequenceNumber != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public REQUESTING_PARTY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
