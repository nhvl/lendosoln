namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class VERIFICATION_DATA_POOL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PoolTotalCountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PoolTotalCount", Order = 0)]
        public MISMOCount PoolTotalCount { get; set; }
    
        [XmlIgnore]
        public bool PoolTotalCountSpecified
        {
            get { return this.PoolTotalCount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public VERIFICATION_DATA_POOL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
