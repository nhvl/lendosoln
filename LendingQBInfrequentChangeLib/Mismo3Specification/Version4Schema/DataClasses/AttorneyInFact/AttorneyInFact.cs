namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ATTORNEY_IN_FACT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AttorneyInFactSigningCapacityTextSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AttorneyInFactSigningCapacityText", Order = 0)]
        public MISMOString AttorneyInFactSigningCapacityText { get; set; }
    
        [XmlIgnore]
        public bool AttorneyInFactSigningCapacityTextSpecified
        {
            get { return this.AttorneyInFactSigningCapacityText != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public ATTORNEY_IN_FACT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
