namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class VALUATION_REVIEW_CATEGORY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AppraisalReviewCategoryAccuracyTypeSpecified
                    || this.AppraisalReviewCategoryCommentDescriptionSpecified
                    || this.AppraisalReviewCategoryTypeSpecified
                    || this.AppraisalReviewCategoryTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AppraisalReviewCategoryAccuracyType", Order = 0)]
        public MISMOEnum<AppraisalReviewCategoryAccuracyBase> AppraisalReviewCategoryAccuracyType { get; set; }
    
        [XmlIgnore]
        public bool AppraisalReviewCategoryAccuracyTypeSpecified
        {
            get { return this.AppraisalReviewCategoryAccuracyType != null; }
            set { }
        }
    
        [XmlElement("AppraisalReviewCategoryCommentDescription", Order = 1)]
        public MISMOString AppraisalReviewCategoryCommentDescription { get; set; }
    
        [XmlIgnore]
        public bool AppraisalReviewCategoryCommentDescriptionSpecified
        {
            get { return this.AppraisalReviewCategoryCommentDescription != null; }
            set { }
        }
    
        [XmlElement("AppraisalReviewCategoryType", Order = 2)]
        public MISMOEnum<AppraisalReviewCategoryBase> AppraisalReviewCategoryType { get; set; }
    
        [XmlIgnore]
        public bool AppraisalReviewCategoryTypeSpecified
        {
            get { return this.AppraisalReviewCategoryType != null; }
            set { }
        }
    
        [XmlElement("AppraisalReviewCategoryTypeOtherDescription", Order = 3)]
        public MISMOString AppraisalReviewCategoryTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool AppraisalReviewCategoryTypeOtherDescriptionSpecified
        {
            get { return this.AppraisalReviewCategoryTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public VALUATION_REVIEW_CATEGORY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
