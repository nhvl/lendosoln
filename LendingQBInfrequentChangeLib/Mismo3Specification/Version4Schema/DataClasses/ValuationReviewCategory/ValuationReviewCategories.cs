namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class VALUATION_REVIEW_CATEGORIES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ValuationReviewCategoryListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("VALUATION_REVIEW_CATEGORY", Order = 0)]
        public List<VALUATION_REVIEW_CATEGORY> ValuationReviewCategoryList { get; set; } = new List<VALUATION_REVIEW_CATEGORY>();
    
        [XmlIgnore]
        public bool ValuationReviewCategoryListSpecified
        {
            get { return this.ValuationReviewCategoryList != null && this.ValuationReviewCategoryList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public VALUATION_REVIEW_CATEGORIES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
