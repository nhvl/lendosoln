namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MERS_REGISTRATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MERS_IRegistrationIndicatorSpecified
                    || this.MERSOriginalMortgageeOfRecordIndicatorSpecified
                    || this.MERSRegistrationDateSpecified
                    || this.MERSRegistrationStatusTypeSpecified
                    || this.MERSRegistrationStatusTypeOtherDescriptionSpecified
                    || this.MERSRegistrationTypeSpecified
                    || this.MERSRegistrationTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MERS_IRegistrationIndicator", Order = 0)]
        public MISMOIndicator MERS_IRegistrationIndicator { get; set; }
    
        [XmlIgnore]
        public bool MERS_IRegistrationIndicatorSpecified
        {
            get { return this.MERS_IRegistrationIndicator != null; }
            set { }
        }
    
        [XmlElement("MERSOriginalMortgageeOfRecordIndicator", Order = 1)]
        public MISMOIndicator MERSOriginalMortgageeOfRecordIndicator { get; set; }
    
        [XmlIgnore]
        public bool MERSOriginalMortgageeOfRecordIndicatorSpecified
        {
            get { return this.MERSOriginalMortgageeOfRecordIndicator != null; }
            set { }
        }
    
        [XmlElement("MERSRegistrationDate", Order = 2)]
        public MISMODate MERSRegistrationDate { get; set; }
    
        [XmlIgnore]
        public bool MERSRegistrationDateSpecified
        {
            get { return this.MERSRegistrationDate != null; }
            set { }
        }
    
        [XmlElement("MERSRegistrationStatusType", Order = 3)]
        public MISMOEnum<MERSRegistrationStatusBase> MERSRegistrationStatusType { get; set; }
    
        [XmlIgnore]
        public bool MERSRegistrationStatusTypeSpecified
        {
            get { return this.MERSRegistrationStatusType != null; }
            set { }
        }
    
        [XmlElement("MERSRegistrationStatusTypeOtherDescription", Order = 4)]
        public MISMOString MERSRegistrationStatusTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MERSRegistrationStatusTypeOtherDescriptionSpecified
        {
            get { return this.MERSRegistrationStatusTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MERSRegistrationType", Order = 5)]
        public MISMOEnum<MERSRegistrationBase> MERSRegistrationType { get; set; }
    
        [XmlIgnore]
        public bool MERSRegistrationTypeSpecified
        {
            get { return this.MERSRegistrationType != null; }
            set { }
        }
    
        [XmlElement("MERSRegistrationTypeOtherDescription", Order = 6)]
        public MISMOString MERSRegistrationTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MERSRegistrationTypeOtherDescriptionSpecified
        {
            get { return this.MERSRegistrationTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 7)]
        public MERS_REGISTRATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
