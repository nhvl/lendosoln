namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class MERS_REGISTRATIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MersRegistrationListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MERS_REGISTRATION", Order = 0)]
        public List<MERS_REGISTRATION> MersRegistrationList { get; set; } = new List<MERS_REGISTRATION>();
    
        [XmlIgnore]
        public bool MersRegistrationListSpecified
        {
            get { return this.MersRegistrationList != null && this.MersRegistrationList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public MERS_REGISTRATIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
