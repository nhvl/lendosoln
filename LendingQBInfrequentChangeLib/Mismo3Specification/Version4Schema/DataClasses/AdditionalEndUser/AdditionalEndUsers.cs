namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class ADDITIONAL_END_USERS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AdditionalEndUserListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ADDITIONAL_END_USER", Order = 0)]
        public List<ADDITIONAL_END_USER> AdditionalEndUserList { get; set; } = new List<ADDITIONAL_END_USER>();
    
        [XmlIgnore]
        public bool AdditionalEndUserListSpecified
        {
            get { return this.AdditionalEndUserList != null && this.AdditionalEndUserList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public ADDITIONAL_END_USERS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
