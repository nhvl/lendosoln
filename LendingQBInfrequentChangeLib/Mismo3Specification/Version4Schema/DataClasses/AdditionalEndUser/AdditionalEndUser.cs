namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ADDITIONAL_END_USER
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EndUserBillingInstructionTypeSpecified
                    || this.EndUserBillingInstructionTypeOtherDescriptionSpecified
                    || this.EndUserNameSpecified
                    || this.InternalAccountIdentifierSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("EndUserBillingInstructionType", Order = 0)]
        public MISMOEnum<EndUserBillingInstructionBase> EndUserBillingInstructionType { get; set; }
    
        [XmlIgnore]
        public bool EndUserBillingInstructionTypeSpecified
        {
            get { return this.EndUserBillingInstructionType != null; }
            set { }
        }
    
        [XmlElement("EndUserBillingInstructionTypeOtherDescription", Order = 1)]
        public MISMOString EndUserBillingInstructionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool EndUserBillingInstructionTypeOtherDescriptionSpecified
        {
            get { return this.EndUserBillingInstructionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EndUserName", Order = 2)]
        public MISMOString EndUserName { get; set; }
    
        [XmlIgnore]
        public bool EndUserNameSpecified
        {
            get { return this.EndUserName != null; }
            set { }
        }
    
        [XmlElement("InternalAccountIdentifier", Order = 3)]
        public MISMOIdentifier InternalAccountIdentifier { get; set; }
    
        [XmlIgnore]
        public bool InternalAccountIdentifierSpecified
        {
            get { return this.InternalAccountIdentifier != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public ADDITIONAL_END_USER_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
