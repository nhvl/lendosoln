namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class DISQUALIFICATION_REASON
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DisqualificationReasonTextSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DisqualificationReasonText", Order = 0)]
        public MISMOString DisqualificationReasonText { get; set; }
    
        [XmlIgnore]
        public bool DisqualificationReasonTextSpecified
        {
            get { return this.DisqualificationReasonText != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public DISQUALIFICATION_REASON_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
