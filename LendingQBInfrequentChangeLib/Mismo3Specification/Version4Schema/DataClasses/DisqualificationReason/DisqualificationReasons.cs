namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class DISQUALIFICATION_REASONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DisqualificationReasonListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DISQUALIFICATION_REASON", Order = 0)]
        public List<DISQUALIFICATION_REASON> DisqualificationReasonList { get; set; } = new List<DISQUALIFICATION_REASON>();
    
        [XmlIgnore]
        public bool DisqualificationReasonListSpecified
        {
            get { return this.DisqualificationReasonList != null && this.DisqualificationReasonList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public DISQUALIFICATION_REASONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
