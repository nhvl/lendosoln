namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class AUDIT_TRAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AuditTrailEntriesSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AUDIT_TRAIL_ENTRIES", Order = 0)]
        public AUDIT_TRAIL_ENTRIES AuditTrailEntries { get; set; }
    
        [XmlIgnore]
        public bool AuditTrailEntriesSpecified
        {
            get { return this.AuditTrailEntries != null && this.AuditTrailEntries.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public AUDIT_TRAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
