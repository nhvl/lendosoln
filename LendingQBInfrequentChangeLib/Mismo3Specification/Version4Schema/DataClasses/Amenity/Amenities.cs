namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class AMENITIES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AmenityListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AMENITY", Order = 0)]
        public List<AMENITY> AmenityList { get; set; } = new List<AMENITY>();
    
        [XmlIgnore]
        public bool AmenityListSpecified
        {
            get { return this.AmenityList != null && this.AmenityList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public AMENITIES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
