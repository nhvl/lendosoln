namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class AMENITY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AmenityCountSpecified
                    || this.AmenityDescriptionSpecified
                    || this.AmenityTypeSpecified
                    || this.AmenityTypeOtherDescriptionSpecified
                    || this.ComponentAdjustmentAmountSpecified
                    || this.ComponentClassificationTypeSpecified
                    || this.ConditionRatingDescriptionSpecified
                    || this.ConditionRatingTypeSpecified
                    || this.QualityRatingDescriptionSpecified
                    || this.QualityRatingTypeSpecified
                    || this.SquareFeetNumberSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AmenityCount", Order = 0)]
        public MISMOCount AmenityCount { get; set; }
    
        [XmlIgnore]
        public bool AmenityCountSpecified
        {
            get { return this.AmenityCount != null; }
            set { }
        }
    
        [XmlElement("AmenityDescription", Order = 1)]
        public MISMOString AmenityDescription { get; set; }
    
        [XmlIgnore]
        public bool AmenityDescriptionSpecified
        {
            get { return this.AmenityDescription != null; }
            set { }
        }
    
        [XmlElement("AmenityType", Order = 2)]
        public MISMOEnum<AmenityBase> AmenityType { get; set; }
    
        [XmlIgnore]
        public bool AmenityTypeSpecified
        {
            get { return this.AmenityType != null; }
            set { }
        }
    
        [XmlElement("AmenityTypeOtherDescription", Order = 3)]
        public MISMOString AmenityTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool AmenityTypeOtherDescriptionSpecified
        {
            get { return this.AmenityTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ComponentAdjustmentAmount", Order = 4)]
        public MISMOAmount ComponentAdjustmentAmount { get; set; }
    
        [XmlIgnore]
        public bool ComponentAdjustmentAmountSpecified
        {
            get { return this.ComponentAdjustmentAmount != null; }
            set { }
        }
    
        [XmlElement("ComponentClassificationType", Order = 5)]
        public MISMOEnum<ComponentClassificationBase> ComponentClassificationType { get; set; }
    
        [XmlIgnore]
        public bool ComponentClassificationTypeSpecified
        {
            get { return this.ComponentClassificationType != null; }
            set { }
        }
    
        [XmlElement("ConditionRatingDescription", Order = 6)]
        public MISMOString ConditionRatingDescription { get; set; }
    
        [XmlIgnore]
        public bool ConditionRatingDescriptionSpecified
        {
            get { return this.ConditionRatingDescription != null; }
            set { }
        }
    
        [XmlElement("ConditionRatingType", Order = 7)]
        public MISMOEnum<ConditionRatingBase> ConditionRatingType { get; set; }
    
        [XmlIgnore]
        public bool ConditionRatingTypeSpecified
        {
            get { return this.ConditionRatingType != null; }
            set { }
        }
    
        [XmlElement("QualityRatingDescription", Order = 8)]
        public MISMOString QualityRatingDescription { get; set; }
    
        [XmlIgnore]
        public bool QualityRatingDescriptionSpecified
        {
            get { return this.QualityRatingDescription != null; }
            set { }
        }
    
        [XmlElement("QualityRatingType", Order = 9)]
        public MISMOEnum<QualityRatingBase> QualityRatingType { get; set; }
    
        [XmlIgnore]
        public bool QualityRatingTypeSpecified
        {
            get { return this.QualityRatingType != null; }
            set { }
        }
    
        [XmlElement("SquareFeetNumber", Order = 10)]
        public MISMONumeric SquareFeetNumber { get; set; }
    
        [XmlIgnore]
        public bool SquareFeetNumberSpecified
        {
            get { return this.SquareFeetNumber != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 11)]
        public AMENITY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
