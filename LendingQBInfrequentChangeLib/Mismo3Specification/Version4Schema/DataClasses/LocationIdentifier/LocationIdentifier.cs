namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class LOCATION_IDENTIFIER
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CensusInformationSpecified
                    || this.FipsInformationSpecified
                    || this.GeneralIdentifierSpecified
                    || this.GeospatialInformationSpecified
                    || this.UniquePropertyIdentificationsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CENSUS_INFORMATION", Order = 0)]
        public CENSUS_INFORMATION CensusInformation { get; set; }
    
        [XmlIgnore]
        public bool CensusInformationSpecified
        {
            get { return this.CensusInformation != null && this.CensusInformation.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("FIPS_INFORMATION", Order = 1)]
        public FIPS_INFORMATION FipsInformation { get; set; }
    
        [XmlIgnore]
        public bool FipsInformationSpecified
        {
            get { return this.FipsInformation != null && this.FipsInformation.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("GENERAL_IDENTIFIER", Order = 2)]
        public GENERAL_IDENTIFIER GeneralIdentifier { get; set; }
    
        [XmlIgnore]
        public bool GeneralIdentifierSpecified
        {
            get { return this.GeneralIdentifier != null && this.GeneralIdentifier.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("GEOSPATIAL_INFORMATION", Order = 3)]
        public GEOSPATIAL_INFORMATION GeospatialInformation { get; set; }
    
        [XmlIgnore]
        public bool GeospatialInformationSpecified
        {
            get { return this.GeospatialInformation != null && this.GeospatialInformation.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("UNIQUE_PROPERTY_IDENTIFICATIONS", Order = 4)]
        public UNIQUE_PROPERTY_IDENTIFICATIONS UniquePropertyIdentifications { get; set; }
    
        [XmlIgnore]
        public bool UniquePropertyIdentificationsSpecified
        {
            get { return this.UniquePropertyIdentifications != null && this.UniquePropertyIdentifications.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public LOCATION_IDENTIFIER_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
