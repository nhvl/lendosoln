namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class TIL_PAYMENT_SUMMARY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.TilPaymentSummaryDetailSpecified
                    || this.TilPaymentSummaryItemsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("TIL_PAYMENT_SUMMARY_DETAIL", Order = 0)]
        public TIL_PAYMENT_SUMMARY_DETAIL TilPaymentSummaryDetail { get; set; }
    
        [XmlIgnore]
        public bool TilPaymentSummaryDetailSpecified
        {
            get { return this.TilPaymentSummaryDetail != null && this.TilPaymentSummaryDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("TIL_PAYMENT_SUMMARY_ITEMS", Order = 1)]
        public TIL_PAYMENT_SUMMARY_ITEMS TilPaymentSummaryItems { get; set; }
    
        [XmlIgnore]
        public bool TilPaymentSummaryItemsSpecified
        {
            get { return this.TilPaymentSummaryItems != null && this.TilPaymentSummaryItems.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public TIL_PAYMENT_SUMMARY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
