namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class TIL_PAYMENT_SUMMARY_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MinimumPaymentOnlyAdditionalBorrowedAmountSpecified
                    || this.MinimumPaymentOnlyAdditionalBorrowedDateSpecified
                    || this.NonPrincipalAndInterestPaymentIncludesMortgageInsuranceIndicatorSpecified
                    || this.NonPrincipalAndInterestPaymentIncludesPrivateMortgageInsuranceIndicatorSpecified
                    || this.NonPrincipalAndInterestPaymentIncludesTaxesAndInsuranceEscrowsIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MinimumPaymentOnlyAdditionalBorrowedAmount", Order = 0)]
        public MISMOAmount MinimumPaymentOnlyAdditionalBorrowedAmount { get; set; }
    
        [XmlIgnore]
        public bool MinimumPaymentOnlyAdditionalBorrowedAmountSpecified
        {
            get { return this.MinimumPaymentOnlyAdditionalBorrowedAmount != null; }
            set { }
        }
    
        [XmlElement("MinimumPaymentOnlyAdditionalBorrowedDate", Order = 1)]
        public MISMODate MinimumPaymentOnlyAdditionalBorrowedDate { get; set; }
    
        [XmlIgnore]
        public bool MinimumPaymentOnlyAdditionalBorrowedDateSpecified
        {
            get { return this.MinimumPaymentOnlyAdditionalBorrowedDate != null; }
            set { }
        }
    
        [XmlElement("NonPrincipalAndInterestPaymentIncludesMortgageInsuranceIndicator", Order = 2)]
        public MISMOIndicator NonPrincipalAndInterestPaymentIncludesMortgageInsuranceIndicator { get; set; }
    
        [XmlIgnore]
        public bool NonPrincipalAndInterestPaymentIncludesMortgageInsuranceIndicatorSpecified
        {
            get { return this.NonPrincipalAndInterestPaymentIncludesMortgageInsuranceIndicator != null; }
            set { }
        }
    
        [XmlElement("NonPrincipalAndInterestPaymentIncludesPrivateMortgageInsuranceIndicator", Order = 3)]
        public MISMOIndicator NonPrincipalAndInterestPaymentIncludesPrivateMortgageInsuranceIndicator { get; set; }
    
        [XmlIgnore]
        public bool NonPrincipalAndInterestPaymentIncludesPrivateMortgageInsuranceIndicatorSpecified
        {
            get { return this.NonPrincipalAndInterestPaymentIncludesPrivateMortgageInsuranceIndicator != null; }
            set { }
        }
    
        [XmlElement("NonPrincipalAndInterestPaymentIncludesTaxesAndInsuranceEscrowsIndicator", Order = 4)]
        public MISMOIndicator NonPrincipalAndInterestPaymentIncludesTaxesAndInsuranceEscrowsIndicator { get; set; }
    
        [XmlIgnore]
        public bool NonPrincipalAndInterestPaymentIncludesTaxesAndInsuranceEscrowsIndicatorSpecified
        {
            get { return this.NonPrincipalAndInterestPaymentIncludesTaxesAndInsuranceEscrowsIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public TIL_PAYMENT_SUMMARY_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
