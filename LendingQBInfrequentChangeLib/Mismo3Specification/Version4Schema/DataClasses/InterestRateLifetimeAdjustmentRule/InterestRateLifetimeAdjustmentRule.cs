namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class INTEREST_RATE_LIFETIME_ADJUSTMENT_RULE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CarryoverRateIndicatorSpecified
                    || this.CeilingRatePercentSpecified
                    || this.CeilingRatePercentEarliestEffectiveMonthsCountSpecified
                    || this.DeferredInterestExclusionIndicatorSpecified
                    || this.FirstRateChangeMonthsCountSpecified
                    || this.FirstRateChangePaymentEffectiveDateSpecified
                    || this.FloorRatePercentSpecified
                    || this.InterestRateAdjustmentCalculationMethodTypeSpecified
                    || this.InterestRateAdjustmentCalculationMethodTypeOtherDescriptionSpecified
                    || this.InterestRateAverageValueCountSpecified
                    || this.InterestRateLifetimeAdjustmentCeilingTypeSpecified
                    || this.InterestRateLifetimeAdjustmentCeilingTypeOtherDescriptionSpecified
                    || this.InterestRateLifetimeAdjustmentFloorTypeSpecified
                    || this.InterestRateLifetimeAdjustmentFloorTypeOtherDescriptionSpecified
                    || this.InterestRateRoundingPercentSpecified
                    || this.InterestRateRoundingTypeSpecified
                    || this.InterestRateTruncatedDigitsCountSpecified
                    || this.LoanMarginCalculationMethodTypeSpecified
                    || this.MarginRatePercentSpecified
                    || this.MaximumDecreaseRatePercentSpecified
                    || this.MaximumIncreaseRatePercentSpecified
                    || this.PaymentsBetweenInterestRateValuesCountSpecified
                    || this.PaymentsBetweenRateChangesCountSpecified
                    || this.TimelyPaymentRateReductionPercentSpecified
                    || this.TotalStepCountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CarryoverRateIndicator", Order = 0)]
        public MISMOIndicator CarryoverRateIndicator { get; set; }
    
        [XmlIgnore]
        public bool CarryoverRateIndicatorSpecified
        {
            get { return this.CarryoverRateIndicator != null; }
            set { }
        }
    
        [XmlElement("CeilingRatePercent", Order = 1)]
        public MISMOPercent CeilingRatePercent { get; set; }
    
        [XmlIgnore]
        public bool CeilingRatePercentSpecified
        {
            get { return this.CeilingRatePercent != null; }
            set { }
        }
    
        [XmlElement("CeilingRatePercentEarliestEffectiveMonthsCount", Order = 2)]
        public MISMOCount CeilingRatePercentEarliestEffectiveMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool CeilingRatePercentEarliestEffectiveMonthsCountSpecified
        {
            get { return this.CeilingRatePercentEarliestEffectiveMonthsCount != null; }
            set { }
        }
    
        [XmlElement("DeferredInterestExclusionIndicator", Order = 3)]
        public MISMOIndicator DeferredInterestExclusionIndicator { get; set; }
    
        [XmlIgnore]
        public bool DeferredInterestExclusionIndicatorSpecified
        {
            get { return this.DeferredInterestExclusionIndicator != null; }
            set { }
        }
    
        [XmlElement("FirstRateChangeMonthsCount", Order = 4)]
        public MISMOCount FirstRateChangeMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool FirstRateChangeMonthsCountSpecified
        {
            get { return this.FirstRateChangeMonthsCount != null; }
            set { }
        }
    
        [XmlElement("FirstRateChangePaymentEffectiveDate", Order = 5)]
        public MISMODate FirstRateChangePaymentEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool FirstRateChangePaymentEffectiveDateSpecified
        {
            get { return this.FirstRateChangePaymentEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("FloorRatePercent", Order = 6)]
        public MISMOPercent FloorRatePercent { get; set; }
    
        [XmlIgnore]
        public bool FloorRatePercentSpecified
        {
            get { return this.FloorRatePercent != null; }
            set { }
        }
    
        [XmlElement("InterestRateAdjustmentCalculationMethodType", Order = 7)]
        public MISMOEnum<InterestRateAdjustmentCalculationMethodBase> InterestRateAdjustmentCalculationMethodType { get; set; }
    
        [XmlIgnore]
        public bool InterestRateAdjustmentCalculationMethodTypeSpecified
        {
            get { return this.InterestRateAdjustmentCalculationMethodType != null; }
            set { }
        }
    
        [XmlElement("InterestRateAdjustmentCalculationMethodTypeOtherDescription", Order = 8)]
        public MISMOString InterestRateAdjustmentCalculationMethodTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool InterestRateAdjustmentCalculationMethodTypeOtherDescriptionSpecified
        {
            get { return this.InterestRateAdjustmentCalculationMethodTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("InterestRateAverageValueCount", Order = 9)]
        public MISMOCount InterestRateAverageValueCount { get; set; }
    
        [XmlIgnore]
        public bool InterestRateAverageValueCountSpecified
        {
            get { return this.InterestRateAverageValueCount != null; }
            set { }
        }
    
        [XmlElement("InterestRateLifetimeAdjustmentCeilingType", Order = 10)]
        public MISMOEnum<InterestRateLifetimeAdjustmentCeilingBase> InterestRateLifetimeAdjustmentCeilingType { get; set; }
    
        [XmlIgnore]
        public bool InterestRateLifetimeAdjustmentCeilingTypeSpecified
        {
            get { return this.InterestRateLifetimeAdjustmentCeilingType != null; }
            set { }
        }
    
        [XmlElement("InterestRateLifetimeAdjustmentCeilingTypeOtherDescription", Order = 11)]
        public MISMOString InterestRateLifetimeAdjustmentCeilingTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool InterestRateLifetimeAdjustmentCeilingTypeOtherDescriptionSpecified
        {
            get { return this.InterestRateLifetimeAdjustmentCeilingTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("InterestRateLifetimeAdjustmentFloorType", Order = 12)]
        public MISMOEnum<InterestRateLifetimeAdjustmentFloorBase> InterestRateLifetimeAdjustmentFloorType { get; set; }
    
        [XmlIgnore]
        public bool InterestRateLifetimeAdjustmentFloorTypeSpecified
        {
            get { return this.InterestRateLifetimeAdjustmentFloorType != null; }
            set { }
        }
    
        [XmlElement("InterestRateLifetimeAdjustmentFloorTypeOtherDescription", Order = 13)]
        public MISMOString InterestRateLifetimeAdjustmentFloorTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool InterestRateLifetimeAdjustmentFloorTypeOtherDescriptionSpecified
        {
            get { return this.InterestRateLifetimeAdjustmentFloorTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("InterestRateRoundingPercent", Order = 14)]
        public MISMOPercent InterestRateRoundingPercent { get; set; }
    
        [XmlIgnore]
        public bool InterestRateRoundingPercentSpecified
        {
            get { return this.InterestRateRoundingPercent != null; }
            set { }
        }
    
        [XmlElement("InterestRateRoundingType", Order = 15)]
        public MISMOEnum<InterestRateRoundingBase> InterestRateRoundingType { get; set; }
    
        [XmlIgnore]
        public bool InterestRateRoundingTypeSpecified
        {
            get { return this.InterestRateRoundingType != null; }
            set { }
        }
    
        [XmlElement("InterestRateTruncatedDigitsCount", Order = 16)]
        public MISMOCount InterestRateTruncatedDigitsCount { get; set; }
    
        [XmlIgnore]
        public bool InterestRateTruncatedDigitsCountSpecified
        {
            get { return this.InterestRateTruncatedDigitsCount != null; }
            set { }
        }
    
        [XmlElement("LoanMarginCalculationMethodType", Order = 17)]
        public MISMOEnum<LoanMarginCalculationMethodBase> LoanMarginCalculationMethodType { get; set; }
    
        [XmlIgnore]
        public bool LoanMarginCalculationMethodTypeSpecified
        {
            get { return this.LoanMarginCalculationMethodType != null; }
            set { }
        }
    
        [XmlElement("MarginRatePercent", Order = 18)]
        public MISMOPercent MarginRatePercent { get; set; }
    
        [XmlIgnore]
        public bool MarginRatePercentSpecified
        {
            get { return this.MarginRatePercent != null; }
            set { }
        }
    
        [XmlElement("MaximumDecreaseRatePercent", Order = 19)]
        public MISMOPercent MaximumDecreaseRatePercent { get; set; }
    
        [XmlIgnore]
        public bool MaximumDecreaseRatePercentSpecified
        {
            get { return this.MaximumDecreaseRatePercent != null; }
            set { }
        }
    
        [XmlElement("MaximumIncreaseRatePercent", Order = 20)]
        public MISMOPercent MaximumIncreaseRatePercent { get; set; }
    
        [XmlIgnore]
        public bool MaximumIncreaseRatePercentSpecified
        {
            get { return this.MaximumIncreaseRatePercent != null; }
            set { }
        }
    
        [XmlElement("PaymentsBetweenInterestRateValuesCount", Order = 21)]
        public MISMOCount PaymentsBetweenInterestRateValuesCount { get; set; }
    
        [XmlIgnore]
        public bool PaymentsBetweenInterestRateValuesCountSpecified
        {
            get { return this.PaymentsBetweenInterestRateValuesCount != null; }
            set { }
        }
    
        [XmlElement("PaymentsBetweenRateChangesCount", Order = 22)]
        public MISMOCount PaymentsBetweenRateChangesCount { get; set; }
    
        [XmlIgnore]
        public bool PaymentsBetweenRateChangesCountSpecified
        {
            get { return this.PaymentsBetweenRateChangesCount != null; }
            set { }
        }
    
        [XmlElement("TimelyPaymentRateReductionPercent", Order = 23)]
        public MISMOPercent TimelyPaymentRateReductionPercent { get; set; }
    
        [XmlIgnore]
        public bool TimelyPaymentRateReductionPercentSpecified
        {
            get { return this.TimelyPaymentRateReductionPercent != null; }
            set { }
        }
    
        [XmlElement("TotalStepCount", Order = 24)]
        public MISMOCount TotalStepCount { get; set; }
    
        [XmlIgnore]
        public bool TotalStepCountSpecified
        {
            get { return this.TotalStepCount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 25)]
        public INTEREST_RATE_LIFETIME_ADJUSTMENT_RULE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
