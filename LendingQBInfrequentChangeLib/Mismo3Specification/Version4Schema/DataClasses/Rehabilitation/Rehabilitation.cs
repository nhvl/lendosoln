namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class REHABILITATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.RehabilitationCompletionDateSpecified
                    || this.RehabilitationContingencyPercentSpecified
                    || this.RehabilitationEstimatedPropertyValueAmountSpecified
                    || this.RehabilitationLoanFundsAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("RehabilitationCompletionDate", Order = 0)]
        public MISMODate RehabilitationCompletionDate { get; set; }
    
        [XmlIgnore]
        public bool RehabilitationCompletionDateSpecified
        {
            get { return this.RehabilitationCompletionDate != null; }
            set { }
        }
    
        [XmlElement("RehabilitationContingencyPercent", Order = 1)]
        public MISMOPercent RehabilitationContingencyPercent { get; set; }
    
        [XmlIgnore]
        public bool RehabilitationContingencyPercentSpecified
        {
            get { return this.RehabilitationContingencyPercent != null; }
            set { }
        }
    
        [XmlElement("RehabilitationEstimatedPropertyValueAmount", Order = 2)]
        public MISMOAmount RehabilitationEstimatedPropertyValueAmount { get; set; }
    
        [XmlIgnore]
        public bool RehabilitationEstimatedPropertyValueAmountSpecified
        {
            get { return this.RehabilitationEstimatedPropertyValueAmount != null; }
            set { }
        }
    
        [XmlElement("RehabilitationLoanFundsAmount", Order = 3)]
        public MISMOAmount RehabilitationLoanFundsAmount { get; set; }
    
        [XmlIgnore]
        public bool RehabilitationLoanFundsAmountSpecified
        {
            get { return this.RehabilitationLoanFundsAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public REHABILITATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
