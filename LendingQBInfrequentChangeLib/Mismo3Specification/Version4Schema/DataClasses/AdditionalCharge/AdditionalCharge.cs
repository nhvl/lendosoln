namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ADDITIONAL_CHARGE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AdditionalChargeAmountSpecified
                    || this.AdditionalChargeDateSpecified
                    || this.AdditionalChargeTypeSpecified
                    || this.AdditionalChargeTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AdditionalChargeAmount", Order = 0)]
        public MISMOAmount AdditionalChargeAmount { get; set; }
    
        [XmlIgnore]
        public bool AdditionalChargeAmountSpecified
        {
            get { return this.AdditionalChargeAmount != null; }
            set { }
        }
    
        [XmlElement("AdditionalChargeDate", Order = 1)]
        public MISMODate AdditionalChargeDate { get; set; }
    
        [XmlIgnore]
        public bool AdditionalChargeDateSpecified
        {
            get { return this.AdditionalChargeDate != null; }
            set { }
        }
    
        [XmlElement("AdditionalChargeType", Order = 2)]
        public MISMOEnum<AdditionalChargeBase> AdditionalChargeType { get; set; }
    
        [XmlIgnore]
        public bool AdditionalChargeTypeSpecified
        {
            get { return this.AdditionalChargeType != null; }
            set { }
        }
    
        [XmlElement("AdditionalChargeTypeOtherDescription", Order = 3)]
        public MISMOString AdditionalChargeTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool AdditionalChargeTypeOtherDescriptionSpecified
        {
            get { return this.AdditionalChargeTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public ADDITIONAL_CHARGE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
