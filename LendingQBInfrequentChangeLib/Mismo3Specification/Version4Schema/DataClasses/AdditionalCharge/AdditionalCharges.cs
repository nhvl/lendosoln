namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class ADDITIONAL_CHARGES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AdditionalChargeListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ADDITIONAL_CHARGE", Order = 0)]
        public List<ADDITIONAL_CHARGE> AdditionalChargeList { get; set; } = new List<ADDITIONAL_CHARGE>();
    
        [XmlIgnore]
        public bool AdditionalChargeListSpecified
        {
            get { return this.AdditionalChargeList != null && this.AdditionalChargeList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public ADDITIONAL_CHARGES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
