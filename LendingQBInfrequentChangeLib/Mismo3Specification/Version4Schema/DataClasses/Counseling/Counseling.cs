namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class COUNSELING
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CounselingDetailSpecified
                    || this.CounselingEventsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("COUNSELING_DETAIL", Order = 0)]
        public COUNSELING_DETAIL CounselingDetail { get; set; }
    
        [XmlIgnore]
        public bool CounselingDetailSpecified
        {
            get { return this.CounselingDetail != null && this.CounselingDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("COUNSELING_EVENTS", Order = 1)]
        public COUNSELING_EVENTS CounselingEvents { get; set; }
    
        [XmlIgnore]
        public bool CounselingEventsSpecified
        {
            get { return this.CounselingEvents != null && this.CounselingEvents.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public COUNSELING_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
