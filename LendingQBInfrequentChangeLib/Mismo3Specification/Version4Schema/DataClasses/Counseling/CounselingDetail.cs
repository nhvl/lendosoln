namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class COUNSELING_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.HousingCounselingAgenciesListProvidedDateSpecified
                    || this.HousingCounselingAgencyListSourceTypeSpecified
                    || this.HousingCounselingAgencyListSourceTypeOtherDescriptionSpecified
                    || this.HUDCounselingInitiatedIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("HousingCounselingAgenciesListProvidedDate", Order = 0)]
        public MISMODate HousingCounselingAgenciesListProvidedDate { get; set; }
    
        [XmlIgnore]
        public bool HousingCounselingAgenciesListProvidedDateSpecified
        {
            get { return this.HousingCounselingAgenciesListProvidedDate != null; }
            set { }
        }
    
        [XmlElement("HousingCounselingAgencyListSourceType", Order = 1)]
        public MISMOEnum<HousingCounselingAgencyListSourceBase> HousingCounselingAgencyListSourceType { get; set; }
    
        [XmlIgnore]
        public bool HousingCounselingAgencyListSourceTypeSpecified
        {
            get { return this.HousingCounselingAgencyListSourceType != null; }
            set { }
        }
    
        [XmlElement("HousingCounselingAgencyListSourceTypeOtherDescription", Order = 2)]
        public MISMOString HousingCounselingAgencyListSourceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool HousingCounselingAgencyListSourceTypeOtherDescriptionSpecified
        {
            get { return this.HousingCounselingAgencyListSourceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("HUDCounselingInitiatedIndicator", Order = 3)]
        public MISMOIndicator HUDCounselingInitiatedIndicator { get; set; }
    
        [XmlIgnore]
        public bool HUDCounselingInitiatedIndicatorSpecified
        {
            get { return this.HUDCounselingInitiatedIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public COUNSELING_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
