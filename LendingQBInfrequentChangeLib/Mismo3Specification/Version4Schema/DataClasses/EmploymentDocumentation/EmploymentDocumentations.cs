namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class EMPLOYMENT_DOCUMENTATIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EmploymentDocumentationListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("EMPLOYMENT_DOCUMENTATION", Order = 0)]
        public List<EMPLOYMENT_DOCUMENTATION> EmploymentDocumentationList { get; set; } = new List<EMPLOYMENT_DOCUMENTATION>();
    
        [XmlIgnore]
        public bool EmploymentDocumentationListSpecified
        {
            get { return this.EmploymentDocumentationList != null && this.EmploymentDocumentationList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public EMPLOYMENT_DOCUMENTATIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
