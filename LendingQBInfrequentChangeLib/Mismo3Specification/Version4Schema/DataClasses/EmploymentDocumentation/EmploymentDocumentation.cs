namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class EMPLOYMENT_DOCUMENTATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DocumentationStateTypeSpecified
                    || this.DocumentationStateTypeOtherDescriptionSpecified
                    || this.EmploymentDocumentTypeSpecified
                    || this.EmploymentDocumentTypeOtherDescriptionSpecified
                    || this.EmploymentVerificationRangeCountSpecified
                    || this.EmploymentVerificationRangeTypeSpecified
                    || this.EmploymentVerificationRangeTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DocumentationStateType", Order = 0)]
        public MISMOEnum<DocumentationStateBase> DocumentationStateType { get; set; }
    
        [XmlIgnore]
        public bool DocumentationStateTypeSpecified
        {
            get { return this.DocumentationStateType != null; }
            set { }
        }
    
        [XmlElement("DocumentationStateTypeOtherDescription", Order = 1)]
        public MISMOString DocumentationStateTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool DocumentationStateTypeOtherDescriptionSpecified
        {
            get { return this.DocumentationStateTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EmploymentDocumentType", Order = 2)]
        public MISMOEnum<EmploymentDocumentBase> EmploymentDocumentType { get; set; }
    
        [XmlIgnore]
        public bool EmploymentDocumentTypeSpecified
        {
            get { return this.EmploymentDocumentType != null; }
            set { }
        }
    
        [XmlElement("EmploymentDocumentTypeOtherDescription", Order = 3)]
        public MISMOString EmploymentDocumentTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool EmploymentDocumentTypeOtherDescriptionSpecified
        {
            get { return this.EmploymentDocumentTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EmploymentVerificationRangeCount", Order = 4)]
        public MISMOCount EmploymentVerificationRangeCount { get; set; }
    
        [XmlIgnore]
        public bool EmploymentVerificationRangeCountSpecified
        {
            get { return this.EmploymentVerificationRangeCount != null; }
            set { }
        }
    
        [XmlElement("EmploymentVerificationRangeType", Order = 5)]
        public MISMOEnum<VerificationRangeBase> EmploymentVerificationRangeType { get; set; }
    
        [XmlIgnore]
        public bool EmploymentVerificationRangeTypeSpecified
        {
            get { return this.EmploymentVerificationRangeType != null; }
            set { }
        }
    
        [XmlElement("EmploymentVerificationRangeTypeOtherDescription", Order = 6)]
        public MISMOString EmploymentVerificationRangeTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool EmploymentVerificationRangeTypeOtherDescriptionSpecified
        {
            get { return this.EmploymentVerificationRangeTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 7)]
        public EMPLOYMENT_DOCUMENTATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
