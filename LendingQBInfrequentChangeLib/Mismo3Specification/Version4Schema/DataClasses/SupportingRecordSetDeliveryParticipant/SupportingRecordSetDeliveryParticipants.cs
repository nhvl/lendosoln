namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class SUPPORTING_RECORD_SET_DELIVERY_PARTICIPANTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SupportingRecordSetDeliveryParticipantListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SUPPORTING_RECORD_SET_DELIVERY_PARTICIPANT", Order = 0)]
        public List<SUPPORTING_RECORD_SET_DELIVERY_PARTICIPANT> SupportingRecordSetDeliveryParticipantList { get; set; } = new List<SUPPORTING_RECORD_SET_DELIVERY_PARTICIPANT>();
    
        [XmlIgnore]
        public bool SupportingRecordSetDeliveryParticipantListSpecified
        {
            get { return this.SupportingRecordSetDeliveryParticipantList != null && this.SupportingRecordSetDeliveryParticipantList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public SUPPORTING_RECORD_SET_DELIVERY_PARTICIPANTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
