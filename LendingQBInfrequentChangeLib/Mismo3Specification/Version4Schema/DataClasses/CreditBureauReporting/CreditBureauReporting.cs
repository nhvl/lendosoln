namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CREDIT_BUREAU_REPORTING
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditCommentCodeSpecified
                    || this.CreditCommentExpirationDateSpecified
                    || this.CreditCommentSourceTypeSpecified
                    || this.CreditLiabilityAccountStatusTypeSpecified
                    || this.CreditLiabilityCurrentRatingCodeSpecified
                    || this.CreditLiabilityCurrentRatingTypeSpecified
                    || this.CreditLiabilityFirstReportedDefaultDateSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CreditCommentCode", Order = 0)]
        public MISMOCode CreditCommentCode { get; set; }
    
        [XmlIgnore]
        public bool CreditCommentCodeSpecified
        {
            get { return this.CreditCommentCode != null; }
            set { }
        }
    
        [XmlElement("CreditCommentExpirationDate", Order = 1)]
        public MISMODate CreditCommentExpirationDate { get; set; }
    
        [XmlIgnore]
        public bool CreditCommentExpirationDateSpecified
        {
            get { return this.CreditCommentExpirationDate != null; }
            set { }
        }
    
        [XmlElement("CreditCommentSourceType", Order = 2)]
        public MISMOEnum<CreditCommentSourceBase> CreditCommentSourceType { get; set; }
    
        [XmlIgnore]
        public bool CreditCommentSourceTypeSpecified
        {
            get { return this.CreditCommentSourceType != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityAccountStatusType", Order = 3)]
        public MISMOEnum<CreditLiabilityAccountStatusBase> CreditLiabilityAccountStatusType { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityAccountStatusTypeSpecified
        {
            get { return this.CreditLiabilityAccountStatusType != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityCurrentRatingCode", Order = 4)]
        public MISMOCode CreditLiabilityCurrentRatingCode { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityCurrentRatingCodeSpecified
        {
            get { return this.CreditLiabilityCurrentRatingCode != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityCurrentRatingType", Order = 5)]
        public MISMOEnum<CreditLiabilityCurrentRatingBase> CreditLiabilityCurrentRatingType { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityCurrentRatingTypeSpecified
        {
            get { return this.CreditLiabilityCurrentRatingType != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityFirstReportedDefaultDate", Order = 6)]
        public MISMODate CreditLiabilityFirstReportedDefaultDate { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityFirstReportedDefaultDateSpecified
        {
            get { return this.CreditLiabilityFirstReportedDefaultDate != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 7)]
        public CREDIT_BUREAU_REPORTING_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
