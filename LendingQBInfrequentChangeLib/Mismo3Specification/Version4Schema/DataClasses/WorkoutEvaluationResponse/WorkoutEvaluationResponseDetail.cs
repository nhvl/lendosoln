namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class WORKOUT_EVALUATION_RESPONSE_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ProcessingSuccessIndicatorSpecified
                    || this.ResponseBatchIdentifierSpecified
                    || this.ServiceRequestReceiptDatetimeSpecified
                    || this.ServiceResponseDatetimeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ProcessingSuccessIndicator", Order = 0)]
        public MISMOIndicator ProcessingSuccessIndicator { get; set; }
    
        [XmlIgnore]
        public bool ProcessingSuccessIndicatorSpecified
        {
            get { return this.ProcessingSuccessIndicator != null; }
            set { }
        }
    
        [XmlElement("ResponseBatchIdentifier", Order = 1)]
        public MISMOIdentifier ResponseBatchIdentifier { get; set; }
    
        [XmlIgnore]
        public bool ResponseBatchIdentifierSpecified
        {
            get { return this.ResponseBatchIdentifier != null; }
            set { }
        }
    
        [XmlElement("ServiceRequestReceiptDatetime", Order = 2)]
        public MISMODatetime ServiceRequestReceiptDatetime { get; set; }
    
        [XmlIgnore]
        public bool ServiceRequestReceiptDatetimeSpecified
        {
            get { return this.ServiceRequestReceiptDatetime != null; }
            set { }
        }
    
        [XmlElement("ServiceResponseDatetime", Order = 3)]
        public MISMODatetime ServiceResponseDatetime { get; set; }
    
        [XmlIgnore]
        public bool ServiceResponseDatetimeSpecified
        {
            get { return this.ServiceResponseDatetime != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public WORKOUT_EVALUATION_RESPONSE_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
