namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class WORKOUT_EVALUATION_RESPONSE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DealSetSpecified
                    || this.WorkoutEvaluationResponseDetailSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DEAL_SET", Order = 0)]
        public DEAL_SET DealSet { get; set; }
    
        [XmlIgnore]
        public bool DealSetSpecified
        {
            get { return this.DealSet != null && this.DealSet.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("WORKOUT_EVALUATION_RESPONSE_DETAIL", Order = 1)]
        public WORKOUT_EVALUATION_RESPONSE_DETAIL WorkoutEvaluationResponseDetail { get; set; }
    
        [XmlIgnore]
        public bool WorkoutEvaluationResponseDetailSpecified
        {
            get { return this.WorkoutEvaluationResponseDetail != null && this.WorkoutEvaluationResponseDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public WORKOUT_EVALUATION_RESPONSE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
