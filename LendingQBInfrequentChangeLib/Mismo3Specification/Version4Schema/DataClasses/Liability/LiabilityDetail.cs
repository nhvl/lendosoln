namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class LIABILITY_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.HELOCMaximumBalanceAmountSpecified
                    || this.IntegratedDisclosureSectionTypeSpecified
                    || this.IntegratedDisclosureSectionTypeOtherDescriptionSpecified
                    || this.LiabilityAccountIdentifierSpecified
                    || this.LiabilityDescriptionSpecified
                    || this.LiabilityExclusionIndicatorSpecified
                    || this.LiabilityMonthlyPaymentAmountSpecified
                    || this.LiabilityPaymentIncludesTaxesInsuranceIndicatorSpecified
                    || this.LiabilityPayoffStatusIndicatorSpecified
                    || this.LiabilityPayoffWithCurrentAssetsIndicatorSpecified
                    || this.LiabilityRemainingTermMonthsCountSpecified
                    || this.LiabilitySecuredBySubjectPropertyIndicatorSpecified
                    || this.LiabilityTypeSpecified
                    || this.LiabilityTypeOtherDescriptionSpecified
                    || this.LiabilityUnpaidBalanceAmountSpecified
                    || this.MortgageTypeSpecified
                    || this.MortgageTypeOtherDescriptionSpecified
                    || this.SubjectLoanResubordinationIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("HELOCMaximumBalanceAmount", Order = 0)]
        public MISMOAmount HELOCMaximumBalanceAmount { get; set; }
    
        [XmlIgnore]
        public bool HELOCMaximumBalanceAmountSpecified
        {
            get { return this.HELOCMaximumBalanceAmount != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureSectionType", Order = 1)]
        public MISMOEnum<IntegratedDisclosureSectionBase> IntegratedDisclosureSectionType { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureSectionTypeSpecified
        {
            get { return this.IntegratedDisclosureSectionType != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureSectionTypeOtherDescription", Order = 2)]
        public MISMOString IntegratedDisclosureSectionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureSectionTypeOtherDescriptionSpecified
        {
            get { return this.IntegratedDisclosureSectionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("LiabilityAccountIdentifier", Order = 3)]
        public MISMOIdentifier LiabilityAccountIdentifier { get; set; }
    
        [XmlIgnore]
        public bool LiabilityAccountIdentifierSpecified
        {
            get { return this.LiabilityAccountIdentifier != null; }
            set { }
        }
    
        [XmlElement("LiabilityDescription", Order = 4)]
        public MISMOString LiabilityDescription { get; set; }
    
        [XmlIgnore]
        public bool LiabilityDescriptionSpecified
        {
            get { return this.LiabilityDescription != null; }
            set { }
        }
    
        [XmlElement("LiabilityExclusionIndicator", Order = 5)]
        public MISMOIndicator LiabilityExclusionIndicator { get; set; }
    
        [XmlIgnore]
        public bool LiabilityExclusionIndicatorSpecified
        {
            get { return this.LiabilityExclusionIndicator != null; }
            set { }
        }
    
        [XmlElement("LiabilityMonthlyPaymentAmount", Order = 6)]
        public MISMOAmount LiabilityMonthlyPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool LiabilityMonthlyPaymentAmountSpecified
        {
            get { return this.LiabilityMonthlyPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("LiabilityPaymentIncludesTaxesInsuranceIndicator", Order = 7)]
        public MISMOIndicator LiabilityPaymentIncludesTaxesInsuranceIndicator { get; set; }
    
        [XmlIgnore]
        public bool LiabilityPaymentIncludesTaxesInsuranceIndicatorSpecified
        {
            get { return this.LiabilityPaymentIncludesTaxesInsuranceIndicator != null; }
            set { }
        }
    
        [XmlElement("LiabilityPayoffStatusIndicator", Order = 8)]
        public MISMOIndicator LiabilityPayoffStatusIndicator { get; set; }
    
        [XmlIgnore]
        public bool LiabilityPayoffStatusIndicatorSpecified
        {
            get { return this.LiabilityPayoffStatusIndicator != null; }
            set { }
        }
    
        [XmlElement("LiabilityPayoffWithCurrentAssetsIndicator", Order = 9)]
        public MISMOIndicator LiabilityPayoffWithCurrentAssetsIndicator { get; set; }
    
        [XmlIgnore]
        public bool LiabilityPayoffWithCurrentAssetsIndicatorSpecified
        {
            get { return this.LiabilityPayoffWithCurrentAssetsIndicator != null; }
            set { }
        }
    
        [XmlElement("LiabilityRemainingTermMonthsCount", Order = 10)]
        public MISMOCount LiabilityRemainingTermMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool LiabilityRemainingTermMonthsCountSpecified
        {
            get { return this.LiabilityRemainingTermMonthsCount != null; }
            set { }
        }
    
        [XmlElement("LiabilitySecuredBySubjectPropertyIndicator", Order = 11)]
        public MISMOIndicator LiabilitySecuredBySubjectPropertyIndicator { get; set; }
    
        [XmlIgnore]
        public bool LiabilitySecuredBySubjectPropertyIndicatorSpecified
        {
            get { return this.LiabilitySecuredBySubjectPropertyIndicator != null; }
            set { }
        }
    
        [XmlElement("LiabilityType", Order = 12)]
        public MISMOEnum<LiabilityBase> LiabilityType { get; set; }
    
        [XmlIgnore]
        public bool LiabilityTypeSpecified
        {
            get { return this.LiabilityType != null; }
            set { }
        }
    
        [XmlElement("LiabilityTypeOtherDescription", Order = 13)]
        public MISMOString LiabilityTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool LiabilityTypeOtherDescriptionSpecified
        {
            get { return this.LiabilityTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("LiabilityUnpaidBalanceAmount", Order = 14)]
        public MISMOAmount LiabilityUnpaidBalanceAmount { get; set; }
    
        [XmlIgnore]
        public bool LiabilityUnpaidBalanceAmountSpecified
        {
            get { return this.LiabilityUnpaidBalanceAmount != null; }
            set { }
        }
    
        [XmlElement("MortgageType", Order = 15)]
        public MISMOEnum<MortgageBase> MortgageType { get; set; }
    
        [XmlIgnore]
        public bool MortgageTypeSpecified
        {
            get { return this.MortgageType != null; }
            set { }
        }
    
        [XmlElement("MortgageTypeOtherDescription", Order = 16)]
        public MISMOString MortgageTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MortgageTypeOtherDescriptionSpecified
        {
            get { return this.MortgageTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("SubjectLoanResubordinationIndicator", Order = 17)]
        public MISMOIndicator SubjectLoanResubordinationIndicator { get; set; }
    
        [XmlIgnore]
        public bool SubjectLoanResubordinationIndicatorSpecified
        {
            get { return this.SubjectLoanResubordinationIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 18)]
        public LIABILITY_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
