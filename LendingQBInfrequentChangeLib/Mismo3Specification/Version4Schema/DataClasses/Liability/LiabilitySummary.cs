namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class LIABILITY_SUMMARY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.TotalNonSubjectPropertyDebtsToBePaidOffAmountSpecified
                    || this.TotalSubjectPropertyPayoffsAndPaymentsAmountSpecified
                    || this.TotalSubordinateLiensPayOffAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("TotalNonSubjectPropertyDebtsToBePaidOffAmount", Order = 0)]
        public MISMOAmount TotalNonSubjectPropertyDebtsToBePaidOffAmount { get; set; }
    
        [XmlIgnore]
        public bool TotalNonSubjectPropertyDebtsToBePaidOffAmountSpecified
        {
            get { return this.TotalNonSubjectPropertyDebtsToBePaidOffAmount != null; }
            set { }
        }
    
        [XmlElement("TotalSubjectPropertyPayoffsAndPaymentsAmount", Order = 1)]
        public MISMOAmount TotalSubjectPropertyPayoffsAndPaymentsAmount { get; set; }
    
        [XmlIgnore]
        public bool TotalSubjectPropertyPayoffsAndPaymentsAmountSpecified
        {
            get { return this.TotalSubjectPropertyPayoffsAndPaymentsAmount != null; }
            set { }
        }
    
        [XmlElement("TotalSubordinateLiensPayOffAmount", Order = 2)]
        public MISMOAmount TotalSubordinateLiensPayOffAmount { get; set; }
    
        [XmlIgnore]
        public bool TotalSubordinateLiensPayOffAmountSpecified
        {
            get { return this.TotalSubordinateLiensPayOffAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public LIABILITY_SUMMARY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
