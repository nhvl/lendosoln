namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class LIABILITIES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LiabilityListSpecified
                    || this.LiabilitySummarySpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LIABILITY", Order = 0)]
        public List<LIABILITY> LiabilityList { get; set; } = new List<LIABILITY>();
    
        [XmlIgnore]
        public bool LiabilityListSpecified
        {
            get { return this.LiabilityList != null && this.LiabilityList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("LIABILITY_SUMMARY", Order = 1)]
        public LIABILITY_SUMMARY LiabilitySummary { get; set; }
    
        [XmlIgnore]
        public bool LiabilitySummarySpecified
        {
            get { return this.LiabilitySummary != null && this.LiabilitySummary.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public LIABILITIES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
