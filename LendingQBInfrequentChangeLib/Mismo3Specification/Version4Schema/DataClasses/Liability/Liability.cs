namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Schema;
    using System.Xml.Serialization;

    public class LIABILITY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LiabilityDetailSpecified
                    || this.LiabilityHolderSpecified
                    || this.PayoffSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LIABILITY_DETAIL", Order = 0)]
        public LIABILITY_DETAIL LiabilityDetail { get; set; }
    
        [XmlIgnore]
        public bool LiabilityDetailSpecified
        {
            get { return this.LiabilityDetail != null && this.LiabilityDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("LIABILITY_HOLDER", Order = 1)]
        public LIABILITY_HOLDER LiabilityHolder { get; set; }
    
        [XmlIgnore]
        public bool LiabilityHolderSpecified
        {
            get { return this.LiabilityHolder != null && this.LiabilityHolder.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PAYOFF", Order = 2)]
        public PAYOFF Payoff { get; set; }
    
        [XmlIgnore]
        public bool PayoffSpecified
        {
            get { return this.Payoff != null && this.Payoff.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public LIABILITY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }

        [XmlAttribute("label", Form = XmlSchemaForm.Qualified, Namespace = Mismo3Constants.XlinkNamespace)]
        public string XlinkLabel { get; set; }

        [XmlIgnore]
        public bool XlinkLabelSpecified
        {
            get { return !string.IsNullOrEmpty(this.XlinkLabel); }
            set { }
        }
    }
}
