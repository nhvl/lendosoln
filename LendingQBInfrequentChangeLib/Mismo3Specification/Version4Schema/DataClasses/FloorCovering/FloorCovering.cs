namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class FLOOR_COVERING
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ComponentAdjustmentAmountSpecified
                    || this.ComponentClassificationTypeSpecified
                    || this.ConditionRatingDescriptionSpecified
                    || this.ConditionRatingTypeSpecified
                    || this.FloorCoveringDescriptionSpecified
                    || this.FloorCoveringTypeSpecified
                    || this.FloorCoveringTypeOtherDescriptionSpecified
                    || this.QualityRatingDescriptionSpecified
                    || this.QualityRatingTypeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ComponentAdjustmentAmount", Order = 0)]
        public MISMOAmount ComponentAdjustmentAmount { get; set; }
    
        [XmlIgnore]
        public bool ComponentAdjustmentAmountSpecified
        {
            get { return this.ComponentAdjustmentAmount != null; }
            set { }
        }
    
        [XmlElement("ComponentClassificationType", Order = 1)]
        public MISMOEnum<ComponentClassificationBase> ComponentClassificationType { get; set; }
    
        [XmlIgnore]
        public bool ComponentClassificationTypeSpecified
        {
            get { return this.ComponentClassificationType != null; }
            set { }
        }
    
        [XmlElement("ConditionRatingDescription", Order = 2)]
        public MISMOString ConditionRatingDescription { get; set; }
    
        [XmlIgnore]
        public bool ConditionRatingDescriptionSpecified
        {
            get { return this.ConditionRatingDescription != null; }
            set { }
        }
    
        [XmlElement("ConditionRatingType", Order = 3)]
        public MISMOEnum<ConditionRatingBase> ConditionRatingType { get; set; }
    
        [XmlIgnore]
        public bool ConditionRatingTypeSpecified
        {
            get { return this.ConditionRatingType != null; }
            set { }
        }
    
        [XmlElement("FloorCoveringDescription", Order = 4)]
        public MISMOString FloorCoveringDescription { get; set; }
    
        [XmlIgnore]
        public bool FloorCoveringDescriptionSpecified
        {
            get { return this.FloorCoveringDescription != null; }
            set { }
        }
    
        [XmlElement("FloorCoveringType", Order = 5)]
        public MISMOEnum<FloorCoveringBase> FloorCoveringType { get; set; }
    
        [XmlIgnore]
        public bool FloorCoveringTypeSpecified
        {
            get { return this.FloorCoveringType != null; }
            set { }
        }
    
        [XmlElement("FloorCoveringTypeOtherDescription", Order = 6)]
        public MISMOString FloorCoveringTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool FloorCoveringTypeOtherDescriptionSpecified
        {
            get { return this.FloorCoveringTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("QualityRatingDescription", Order = 7)]
        public MISMOString QualityRatingDescription { get; set; }
    
        [XmlIgnore]
        public bool QualityRatingDescriptionSpecified
        {
            get { return this.QualityRatingDescription != null; }
            set { }
        }
    
        [XmlElement("QualityRatingType", Order = 8)]
        public MISMOEnum<QualityRatingBase> QualityRatingType { get; set; }
    
        [XmlIgnore]
        public bool QualityRatingTypeSpecified
        {
            get { return this.QualityRatingType != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 9)]
        public FLOOR_COVERING_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
