namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PRIA_REQUEST
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.OriginatingRecordingRequestSpecified
                    || this.PriaConsiderationsSpecified
                    || this.PriaRequestDetailSpecified
                    || this.RecordingTransactionIdentifierSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ORIGINATING_RECORDING_REQUEST", Order = 0)]
        public ORIGINATING_RECORDING_REQUEST OriginatingRecordingRequest { get; set; }
    
        [XmlIgnore]
        public bool OriginatingRecordingRequestSpecified
        {
            get { return this.OriginatingRecordingRequest != null && this.OriginatingRecordingRequest.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PRIA_CONSIDERATIONS", Order = 1)]
        public PRIA_CONSIDERATIONS PriaConsiderations { get; set; }
    
        [XmlIgnore]
        public bool PriaConsiderationsSpecified
        {
            get { return this.PriaConsiderations != null && this.PriaConsiderations.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PRIA_REQUEST_DETAIL", Order = 2)]
        public PRIA_REQUEST_DETAIL PriaRequestDetail { get; set; }
    
        [XmlIgnore]
        public bool PriaRequestDetailSpecified
        {
            get { return this.PriaRequestDetail != null && this.PriaRequestDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("RECORDING_TRANSACTION_IDENTIFIER", Order = 3)]
        public RECORDING_TRANSACTION_IDENTIFIER RecordingTransactionIdentifier { get; set; }
    
        [XmlIgnore]
        public bool RecordingTransactionIdentifierSpecified
        {
            get { return this.RecordingTransactionIdentifier != null && this.RecordingTransactionIdentifier.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public PRIA_REQUEST_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
