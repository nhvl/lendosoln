namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PRIA_REQUEST_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PRIARecordingJurisdictionIdentifierSpecified
                    || this.PRIARequestNoteTextSpecified
                    || this.PRIARequestRelatedDocumentsIndicatorSpecified
                    || this.PRIARequestTypeSpecified
                    || this.PRIARequestTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PRIARecordingJurisdictionIdentifier", Order = 0)]
        public MISMOIdentifier PRIARecordingJurisdictionIdentifier { get; set; }
    
        [XmlIgnore]
        public bool PRIARecordingJurisdictionIdentifierSpecified
        {
            get { return this.PRIARecordingJurisdictionIdentifier != null; }
            set { }
        }
    
        [XmlElement("PRIARequestNoteText", Order = 1)]
        public MISMOString PRIARequestNoteText { get; set; }
    
        [XmlIgnore]
        public bool PRIARequestNoteTextSpecified
        {
            get { return this.PRIARequestNoteText != null; }
            set { }
        }
    
        [XmlElement("PRIARequestRelatedDocumentsIndicator", Order = 2)]
        public MISMOIndicator PRIARequestRelatedDocumentsIndicator { get; set; }
    
        [XmlIgnore]
        public bool PRIARequestRelatedDocumentsIndicatorSpecified
        {
            get { return this.PRIARequestRelatedDocumentsIndicator != null; }
            set { }
        }
    
        [XmlElement("PRIARequestType", Order = 3)]
        public MISMOEnum<PRIARequestBase> PRIARequestType { get; set; }
    
        [XmlIgnore]
        public bool PRIARequestTypeSpecified
        {
            get { return this.PRIARequestType != null; }
            set { }
        }
    
        [XmlElement("PRIARequestTypeOtherDescription", Order = 4)]
        public MISMOString PRIARequestTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PRIARequestTypeOtherDescriptionSpecified
        {
            get { return this.PRIARequestTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public PRIA_REQUEST_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
