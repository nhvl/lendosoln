namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class BANKRUPTCY_STATUS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BankruptcyStatusDateSpecified
                    || this.BankruptcyStatusTypeSpecified
                    || this.BankruptcyStatusTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BankruptcyStatusDate", Order = 0)]
        public MISMODate BankruptcyStatusDate { get; set; }
    
        [XmlIgnore]
        public bool BankruptcyStatusDateSpecified
        {
            get { return this.BankruptcyStatusDate != null; }
            set { }
        }
    
        [XmlElement("BankruptcyStatusType", Order = 1)]
        public MISMOEnum<BankruptcyStatusBase> BankruptcyStatusType { get; set; }
    
        [XmlIgnore]
        public bool BankruptcyStatusTypeSpecified
        {
            get { return this.BankruptcyStatusType != null; }
            set { }
        }
    
        [XmlElement("BankruptcyStatusTypeOtherDescription", Order = 2)]
        public MISMOString BankruptcyStatusTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool BankruptcyStatusTypeOtherDescriptionSpecified
        {
            get { return this.BankruptcyStatusTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public BANKRUPTCY_STATUS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
