namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class BANKRUPTCY_STATUSES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BankruptcyStatusListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BANKRUPTCY_STATUS", Order = 0)]
        public List<BANKRUPTCY_STATUS> BankruptcyStatusList { get; set; } = new List<BANKRUPTCY_STATUS>();
    
        [XmlIgnore]
        public bool BankruptcyStatusListSpecified
        {
            get { return this.BankruptcyStatusList != null && this.BankruptcyStatusList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public BANKRUPTCY_STATUSES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
