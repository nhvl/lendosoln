namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class DATA_ITEM_CHANGE_CONTEXT_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DataItemChangeContextDescriptionSpecified
                    || this.DataItemChangeContextXPathSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DataItemChangeContextDescription", Order = 0)]
        public MISMOString DataItemChangeContextDescription { get; set; }
    
        [XmlIgnore]
        public bool DataItemChangeContextDescriptionSpecified
        {
            get { return this.DataItemChangeContextDescription != null; }
            set { }
        }
    
        [XmlElement("DataItemChangeContextXPath", Order = 1)]
        public MISMOXPath DataItemChangeContextXPath { get; set; }
    
        [XmlIgnore]
        public bool DataItemChangeContextXPathSpecified
        {
            get { return this.DataItemChangeContextXPath != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public DATA_ITEM_CHANGE_CONTEXT_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
