namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class DATA_ITEM_CHANGE_CONTEXT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DataItemChangeContextDetailSpecified
                    || this.DataItemChangesSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DATA_ITEM_CHANGE_CONTEXT_DETAIL", Order = 0)]
        public DATA_ITEM_CHANGE_CONTEXT_DETAIL DataItemChangeContextDetail { get; set; }
    
        [XmlIgnore]
        public bool DataItemChangeContextDetailSpecified
        {
            get { return this.DataItemChangeContextDetail != null && this.DataItemChangeContextDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("DATA_ITEM_CHANGES", Order = 1)]
        public DATA_ITEM_CHANGES DataItemChanges { get; set; }
    
        [XmlIgnore]
        public bool DataItemChangesSpecified
        {
            get { return this.DataItemChanges != null && this.DataItemChanges.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public DATA_ITEM_CHANGE_CONTEXT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
