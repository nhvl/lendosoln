namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class DATA_ITEM_CHANGE_CONTEXTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DataItemChangeContextListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DATA_ITEM_CHANGE_CONTEXT", Order = 0)]
        public List<DATA_ITEM_CHANGE_CONTEXT> DataItemChangeContextList { get; set; } = new List<DATA_ITEM_CHANGE_CONTEXT>();
    
        [XmlIgnore]
        public bool DataItemChangeContextListSpecified
        {
            get { return this.DataItemChangeContextList != null && this.DataItemChangeContextList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public DATA_ITEM_CHANGE_CONTEXTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
