namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CONVERSION_ADJUSTMENT_LIFETIME_ADJUSTMENT_RULE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ConversionDateMaximumExtensionMonthsCountSpecified
                    || this.ConversionMaximumAllowedCountSpecified
                    || this.ConversionOptionDurationMonthsCountSpecified
                    || this.ConversionOptionMarginRatePercentSpecified
                    || this.ConversionOptionMaximumRatePercentSpecified
                    || this.ConversionOptionMinimumRatePercentSpecified
                    || this.ConversionOptionNoteTermGreaterThanFifteenYearsAdditionalPercentSpecified
                    || this.ConversionOptionNoteTermLessThanFifteenYearsAdditionalPercentSpecified
                    || this.ConversionScheduleTypeSpecified
                    || this.ConversionScheduleTypeOtherDescriptionSpecified
                    || this.ConversionTypeSpecified
                    || this.ConversionTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ConversionDateMaximumExtensionMonthsCount", Order = 0)]
        public MISMOCount ConversionDateMaximumExtensionMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool ConversionDateMaximumExtensionMonthsCountSpecified
        {
            get { return this.ConversionDateMaximumExtensionMonthsCount != null; }
            set { }
        }
    
        [XmlElement("ConversionMaximumAllowedCount", Order = 1)]
        public MISMOCount ConversionMaximumAllowedCount { get; set; }
    
        [XmlIgnore]
        public bool ConversionMaximumAllowedCountSpecified
        {
            get { return this.ConversionMaximumAllowedCount != null; }
            set { }
        }
    
        [XmlElement("ConversionOptionDurationMonthsCount", Order = 2)]
        public MISMOCount ConversionOptionDurationMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool ConversionOptionDurationMonthsCountSpecified
        {
            get { return this.ConversionOptionDurationMonthsCount != null; }
            set { }
        }
    
        [XmlElement("ConversionOptionMarginRatePercent", Order = 3)]
        public MISMOPercent ConversionOptionMarginRatePercent { get; set; }
    
        [XmlIgnore]
        public bool ConversionOptionMarginRatePercentSpecified
        {
            get { return this.ConversionOptionMarginRatePercent != null; }
            set { }
        }
    
        [XmlElement("ConversionOptionMaximumRatePercent", Order = 4)]
        public MISMOPercent ConversionOptionMaximumRatePercent { get; set; }
    
        [XmlIgnore]
        public bool ConversionOptionMaximumRatePercentSpecified
        {
            get { return this.ConversionOptionMaximumRatePercent != null; }
            set { }
        }
    
        [XmlElement("ConversionOptionMinimumRatePercent", Order = 5)]
        public MISMOPercent ConversionOptionMinimumRatePercent { get; set; }
    
        [XmlIgnore]
        public bool ConversionOptionMinimumRatePercentSpecified
        {
            get { return this.ConversionOptionMinimumRatePercent != null; }
            set { }
        }
    
        [XmlElement("ConversionOptionNoteTermGreaterThanFifteenYearsAdditionalPercent", Order = 6)]
        public MISMOPercent ConversionOptionNoteTermGreaterThanFifteenYearsAdditionalPercent { get; set; }
    
        [XmlIgnore]
        public bool ConversionOptionNoteTermGreaterThanFifteenYearsAdditionalPercentSpecified
        {
            get { return this.ConversionOptionNoteTermGreaterThanFifteenYearsAdditionalPercent != null; }
            set { }
        }
    
        [XmlElement("ConversionOptionNoteTermLessThanFifteenYearsAdditionalPercent", Order = 7)]
        public MISMOPercent ConversionOptionNoteTermLessThanFifteenYearsAdditionalPercent { get; set; }
    
        [XmlIgnore]
        public bool ConversionOptionNoteTermLessThanFifteenYearsAdditionalPercentSpecified
        {
            get { return this.ConversionOptionNoteTermLessThanFifteenYearsAdditionalPercent != null; }
            set { }
        }
    
        [XmlElement("ConversionScheduleType", Order = 8)]
        public MISMOEnum<ConversionScheduleBase> ConversionScheduleType { get; set; }
    
        [XmlIgnore]
        public bool ConversionScheduleTypeSpecified
        {
            get { return this.ConversionScheduleType != null; }
            set { }
        }
    
        [XmlElement("ConversionScheduleTypeOtherDescription", Order = 9)]
        public MISMOString ConversionScheduleTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ConversionScheduleTypeOtherDescriptionSpecified
        {
            get { return this.ConversionScheduleTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ConversionType", Order = 10)]
        public MISMOEnum<ConversionBase> ConversionType { get; set; }
    
        [XmlIgnore]
        public bool ConversionTypeSpecified
        {
            get { return this.ConversionType != null; }
            set { }
        }
    
        [XmlElement("ConversionTypeOtherDescription", Order = 11)]
        public MISMOString ConversionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ConversionTypeOtherDescriptionSpecified
        {
            get { return this.ConversionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 12)]
        public CONVERSION_ADJUSTMENT_LIFETIME_ADJUSTMENT_RULE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
