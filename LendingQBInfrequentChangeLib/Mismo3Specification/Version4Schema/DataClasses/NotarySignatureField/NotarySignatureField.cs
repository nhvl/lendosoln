namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class NOTARY_SIGNATURE_FIELD
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.NOTARIAL_CERTIFICATE_LANGUAGE_FIELD_REFERENCESpecified
                    || this.NOTARIAL_SEAL_FIELD_REFERENCESpecified
                    || this.NotarySignatureFieldDetailSpecified
                    || this.SIGNATURE_FIELD_REFERENCESpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("NOTARIAL_CERTIFICATE_LANGUAGE_FIELD_REFERENCE", Order = 0)]
        public FIELD_REFERENCE NOTARIAL_CERTIFICATE_LANGUAGE_FIELD_REFERENCE { get; set; }
    
        [XmlIgnore]
        public bool NOTARIAL_CERTIFICATE_LANGUAGE_FIELD_REFERENCESpecified
        {
            get { return this.NOTARIAL_CERTIFICATE_LANGUAGE_FIELD_REFERENCE != null; }
            set { }
        }
    
        [XmlElement("NOTARIAL_SEAL_FIELD_REFERENCE", Order = 1)]
        public FIELD_REFERENCE NOTARIAL_SEAL_FIELD_REFERENCE { get; set; }
    
        [XmlIgnore]
        public bool NOTARIAL_SEAL_FIELD_REFERENCESpecified
        {
            get { return this.NOTARIAL_SEAL_FIELD_REFERENCE != null; }
            set { }
        }
    
        [XmlElement("NOTARY_SIGNATURE_FIELD_DETAIL", Order = 2)]
        public NOTARY_SIGNATURE_FIELD_DETAIL NotarySignatureFieldDetail { get; set; }
    
        [XmlIgnore]
        public bool NotarySignatureFieldDetailSpecified
        {
            get { return this.NotarySignatureFieldDetail != null && this.NotarySignatureFieldDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SIGNATURE_FIELD_REFERENCE", Order = 3)]
        public FIELD_REFERENCE SIGNATURE_FIELD_REFERENCE { get; set; }
    
        [XmlIgnore]
        public bool SIGNATURE_FIELD_REFERENCESpecified
        {
            get { return this.SIGNATURE_FIELD_REFERENCE != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public NOTARY_SIGNATURE_FIELD_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
