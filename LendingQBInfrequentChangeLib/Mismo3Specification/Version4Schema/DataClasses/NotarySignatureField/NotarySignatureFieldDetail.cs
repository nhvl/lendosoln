namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class NOTARY_SIGNATURE_FIELD_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SignatureTypeSpecified
                    || this.SignatureTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SignatureType", Order = 0)]
        public MISMOEnum<SignatureBase> SignatureType { get; set; }
    
        [XmlIgnore]
        public bool SignatureTypeSpecified
        {
            get { return this.SignatureType != null; }
            set { }
        }
    
        [XmlElement("SignatureTypeOtherDescription", Order = 1)]
        public MISMOString SignatureTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool SignatureTypeOtherDescriptionSpecified
        {
            get { return this.SignatureTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public NOTARY_SIGNATURE_FIELD_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
