namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CLAIM_RESPONSE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified;
            }
        }
    
        [XmlElement("EXTENSION", Order = 0)]
        public CLAIM_RESPONSE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
