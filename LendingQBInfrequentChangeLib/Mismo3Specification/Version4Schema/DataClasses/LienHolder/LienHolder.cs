namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class LIEN_HOLDER
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LienHolderTypeSpecified
                    || this.LienHolderTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LienHolderType", Order = 0)]
        public MISMOEnum<LienHolderBase> LienHolderType { get; set; }
    
        [XmlIgnore]
        public bool LienHolderTypeSpecified
        {
            get { return this.LienHolderType != null; }
            set { }
        }
    
        [XmlElement("LienHolderTypeOtherDescription", Order = 1)]
        public MISMOString LienHolderTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool LienHolderTypeOtherDescriptionSpecified
        {
            get { return this.LienHolderTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public LIEN_HOLDER_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
