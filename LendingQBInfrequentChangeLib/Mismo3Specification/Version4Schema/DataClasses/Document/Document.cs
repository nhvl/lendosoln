namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Xml.Schema;
    using System.Xml.Serialization;

    public class DOCUMENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                if (Mismo3Utilities.ExceedsThreshold(1,
                    new List<bool> {
                        this.ReferenceSpecified,
                        this.AboutVersionsSpecified
                        || this.AuditTrailSpecified
                        || this.DealSetsSpecified
                        || this.DocumentClassificationSpecified
                        || this.ExtensionSpecified
                        || this.MapSpecified
                        || this.RelationshipsSpecified
                        || this.SignatoriesSpecified
                        || this.SystemSignaturesSpecified
                        || this.UnknownVersion3DocumentSpecified
                        || this.ViewsSpecified }))
                {
                    throw new System.Xml.Schema.XmlSchemaValidationException(
                        Mismo3Utilities.GetXSDChoiceExceptionMessage(
                        "DOCUMENT",
                        new List<string> { "REFERENCE", "Any other child element of the DOCUMENT" }));
                }

                if (Mismo3Utilities.ExceedsThreshold(1,
                    new List<bool> {
                        this.UnknownVersion3DocumentSpecified,
                        this.AuditTrailSpecified
                        || this.DealSetsSpecified
                        || this.MapSpecified
                        || this.RelationshipsSpecified
                        || this.SignatoriesSpecified
                        || this.SystemSignaturesSpecified
                        || this.ViewsSpecified }))
                {
                    throw new System.Xml.Schema.XmlSchemaValidationException(
                        Mismo3Utilities.GetXSDChoiceExceptionMessage(
                        "DOCUMENT",
                        new List<string> { "UNKNOWN_VERSION3_DOCUMENT", "AUDIT_TRAIL, DEAL_SETS, MAP, RELATIONSHIPS, SIGNATORIES, SYSTEM_SIGNATURES, and/or VIEWS" }));
                }

                return this.AboutVersionsSpecified
                    || this.AuditTrailSpecified
                    || this.DealSetsSpecified
                    || this.DocumentClassificationSpecified
                    || this.ExtensionSpecified
                    || this.MapSpecified
                    || this.ReferenceSpecified
                    || this.RelationshipsSpecified
                    || this.SignatoriesSpecified
                    || this.SystemSignaturesSpecified
                    || this.UnknownVersion3DocumentSpecified
                    || this.ViewsSpecified;
            }
        }

        [XmlElement("REFERENCE", Order = 0)]
        public REFERENCE Reference { get; set; }

        [XmlIgnore]
        public bool ReferenceSpecified
        {
            get { return this.Reference != null && this.Reference.ShouldSerialize; }
            set { }
        }

        [XmlElement("UNKNOWN_VERSION3_DOCUMENT", Order = 1)]
        public UNKNOWN_VERSION3_DOCUMENT UnknownVersion3Document { get; set; }

        [XmlIgnore]
        public bool UnknownVersion3DocumentSpecified
        {
            get { return this.UnknownVersion3Document != null && this.UnknownVersion3Document.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("AUDIT_TRAIL", Order = 2)]
        public AUDIT_TRAIL AuditTrail { get; set; }
    
        [XmlIgnore]
        public bool AuditTrailSpecified
        {
            get { return this.AuditTrail != null && this.AuditTrail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("DEAL_SETS", Order = 3)]
        public DEAL_SETS DealSets { get; set; }
    
        [XmlIgnore]
        public bool DealSetsSpecified
        {
            get { return this.DealSets != null && this.DealSets.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("MAP", Order = 4)]
        public MAP Map { get; set; }
    
        [XmlIgnore]
        public bool MapSpecified
        {
            get { return this.Map != null && this.Map.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("RELATIONSHIPS", Order = 5)]
        public RELATIONSHIPS Relationships { get; set; }
    
        [XmlIgnore]
        public bool RelationshipsSpecified
        {
            get { return this.Relationships != null && this.Relationships.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SIGNATORIES", Order = 6)]
        public SIGNATORIES Signatories { get; set; }
    
        [XmlIgnore]
        public bool SignatoriesSpecified
        {
            get { return this.Signatories != null && this.Signatories.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SYSTEM_SIGNATURES", Order = 7)]
        public SYSTEM_SIGNATURES SystemSignatures { get; set; }
    
        [XmlIgnore]
        public bool SystemSignaturesSpecified
        {
            get { return this.SystemSignatures != null && this.SystemSignatures.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("VIEWS", Order = 8)]
        public VIEWS Views { get; set; }
    
        [XmlIgnore]
        public bool ViewsSpecified
        {
            get { return this.Views != null && this.Views.ShouldSerialize; }
            set { }
        }

        [XmlElement("ABOUT_VERSIONS", Order = 9)]
        public ABOUT_VERSIONS AboutVersions { get; set; }

        [XmlIgnore]
        public bool AboutVersionsSpecified
        {
            get { return this.AboutVersions != null && this.AboutVersions.ShouldSerialize; }
            set { }
        }

        [XmlElement("DOCUMENT_CLASSIFICATION", Order = 10)]
        public DOCUMENT_CLASSIFICATION DocumentClassification { get; set; }

        [XmlIgnore]
        public bool DocumentClassificationSpecified
        {
            get { return this.DocumentClassification != null && this.DocumentClassification.ShouldSerialize; }
            set { }
        }

        [XmlElement("EXTENSION", Order = 11)]
        public DOCUMENT_EXTENSION Extension { get; set; }

        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }

        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    
        [XmlAttribute(AttributeName = "MISMOLogicalDataDictionaryIdentifier")]
        public string MISMOLogicalDataDictionaryIdentifier { get; set; }
    
        [XmlIgnore]
        public bool MISMOLogicalDataDictionaryIdentifierSpecified
        {
            get { return !string.IsNullOrEmpty(this.MISMOLogicalDataDictionaryIdentifier); }
            set { }
        }
    
        [XmlAttribute(AttributeName = "MISMOReferenceModelIdentifier")]
        public string MISMOReferenceModelIdentifier { get; set; }
    
        [XmlIgnore]
        public bool MISMOReferenceModelIdentifierSpecified
        {
            get { return !string.IsNullOrEmpty(this.MISMOReferenceModelIdentifier); }
            set { }
        }

        [XmlAttribute("label", Form = XmlSchemaForm.Qualified, Namespace = Mismo3Constants.XlinkNamespace)]
        public string XlinkLabel { get; set; }

        [XmlIgnore]
        public bool XlinkLabelSpecified
        {
            get { return !string.IsNullOrEmpty(this.XlinkLabel); }
            set { }
        }
    }
}
