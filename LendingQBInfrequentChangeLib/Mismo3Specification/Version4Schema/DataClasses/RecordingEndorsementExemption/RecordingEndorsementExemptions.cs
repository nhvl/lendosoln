namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class RECORDING_ENDORSEMENT_EXEMPTIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.RecordingEndorsementExemptionListSpecified
                    || this.RecordingEndorsementExemptionsSummarySpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("RECORDING_ENDORSEMENT_EXEMPTION", Order = 0)]
        public List<RECORDING_ENDORSEMENT_EXEMPTION> RecordingEndorsementExemptionList { get; set; } = new List<RECORDING_ENDORSEMENT_EXEMPTION>();
    
        [XmlIgnore]
        public bool RecordingEndorsementExemptionListSpecified
        {
            get { return this.RecordingEndorsementExemptionList != null && this.RecordingEndorsementExemptionList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("RECORDING_ENDORSEMENT_EXEMPTIONS_SUMMARY", Order = 1)]
        public RECORDING_ENDORSEMENT_EXEMPTIONS_SUMMARY RecordingEndorsementExemptionsSummary { get; set; }
    
        [XmlIgnore]
        public bool RecordingEndorsementExemptionsSummarySpecified
        {
            get { return this.RecordingEndorsementExemptionsSummary != null && this.RecordingEndorsementExemptionsSummary.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public RECORDING_ENDORSEMENT_EXEMPTIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
