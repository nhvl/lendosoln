namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class RECORDING_ENDORSEMENT_EXEMPTION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.RecordingEndorsementExemptionAmountSpecified
                    || this.RecordingEndorsementExemptionCodeSpecified
                    || this.RecordingEndorsementExemptionDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("RecordingEndorsementExemptionAmount", Order = 0)]
        public MISMOAmount RecordingEndorsementExemptionAmount { get; set; }
    
        [XmlIgnore]
        public bool RecordingEndorsementExemptionAmountSpecified
        {
            get { return this.RecordingEndorsementExemptionAmount != null; }
            set { }
        }
    
        [XmlElement("RecordingEndorsementExemptionCode", Order = 1)]
        public MISMOCode RecordingEndorsementExemptionCode { get; set; }
    
        [XmlIgnore]
        public bool RecordingEndorsementExemptionCodeSpecified
        {
            get { return this.RecordingEndorsementExemptionCode != null; }
            set { }
        }
    
        [XmlElement("RecordingEndorsementExemptionDescription", Order = 2)]
        public MISMOString RecordingEndorsementExemptionDescription { get; set; }
    
        [XmlIgnore]
        public bool RecordingEndorsementExemptionDescriptionSpecified
        {
            get { return this.RecordingEndorsementExemptionDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public RECORDING_ENDORSEMENT_EXEMPTION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
