namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class RECORDING_ENDORSEMENT_EXEMPTIONS_SUMMARY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.RecordingEndorsementExemptionsTotalAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("RecordingEndorsementExemptionsTotalAmount", Order = 0)]
        public MISMOAmount RecordingEndorsementExemptionsTotalAmount { get; set; }
    
        [XmlIgnore]
        public bool RecordingEndorsementExemptionsTotalAmountSpecified
        {
            get { return this.RecordingEndorsementExemptionsTotalAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public RECORDING_ENDORSEMENT_EXEMPTIONS_SUMMARY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
