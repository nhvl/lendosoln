namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CREDIT_RESPONSE_DATA_INFORMATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DataVersionsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DATA_VERSIONS", Order = 0)]
        public DATA_VERSIONS DataVersions { get; set; }
    
        [XmlIgnore]
        public bool DataVersionsSpecified
        {
            get { return this.DataVersions != null && this.DataVersions.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public CREDIT_RESPONSE_DATA_INFORMATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
