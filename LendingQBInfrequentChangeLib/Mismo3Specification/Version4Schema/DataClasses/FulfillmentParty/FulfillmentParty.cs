namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class FULFILLMENT_PARTY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.FulfillmentPartyRoleTypeSpecified
                    || this.FulfillmentPartyRoleTypeOtherDescriptionSpecified
                    || this.FulfillmentPartyServiceTypeSpecified
                    || this.FulfillmentPartyServiceTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("FulfillmentPartyRoleType", Order = 0)]
        public MISMOEnum<FulfillmentPartyRoleBase> FulfillmentPartyRoleType { get; set; }
    
        [XmlIgnore]
        public bool FulfillmentPartyRoleTypeSpecified
        {
            get { return this.FulfillmentPartyRoleType != null; }
            set { }
        }
    
        [XmlElement("FulfillmentPartyRoleTypeOtherDescription", Order = 1)]
        public MISMOString FulfillmentPartyRoleTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool FulfillmentPartyRoleTypeOtherDescriptionSpecified
        {
            get { return this.FulfillmentPartyRoleTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("FulfillmentPartyServiceType", Order = 2)]
        public MISMOEnum<FulfillmentPartyServiceBase> FulfillmentPartyServiceType { get; set; }
    
        [XmlIgnore]
        public bool FulfillmentPartyServiceTypeSpecified
        {
            get { return this.FulfillmentPartyServiceType != null; }
            set { }
        }
    
        [XmlElement("FulfillmentPartyServiceTypeOtherDescription", Order = 3)]
        public MISMOString FulfillmentPartyServiceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool FulfillmentPartyServiceTypeOtherDescriptionSpecified
        {
            get { return this.FulfillmentPartyServiceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public FULFILLMENT_PARTY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
