namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class FORECLOSURE_ACTIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ForeclosureActionListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("FORECLOSURE_ACTION", Order = 0)]
        public List<FORECLOSURE_ACTION> ForeclosureActionList { get; set; } = new List<FORECLOSURE_ACTION>();
    
        [XmlIgnore]
        public bool ForeclosureActionListSpecified
        {
            get { return this.ForeclosureActionList != null && this.ForeclosureActionList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public FORECLOSURE_ACTIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
