namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class FORECLOSURE_ACTION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ForeclosureActionDateSpecified
                    || this.ForeclosureActionTypeSpecified
                    || this.ForeclosureActionTypeOtherDescriptionSpecified
                    || this.ForeclosureProcedureTerminatedReasonTypeSpecified
                    || this.ForeclosureProcedureTerminatedReasonTypeAdditionalDescriptionSpecified
                    || this.ForeclosureProcedureTerminatedReasonTypeOtherDescriptionSpecified
                    || this.ForeclosureSaleCancellationReasonTypeSpecified
                    || this.ForeclosureSaleCancellationReasonTypeAdditionalDescriptionSpecified
                    || this.ForeclosureSaleCancellationReasonTypeOtherDescriptionSpecified
                    || this.ForeclosureSalePostponedToDateSpecified
                    || this.ForeclosureSalePostponementReasonDateSpecified
                    || this.ForeclosureSalePostponementReasonTypeSpecified
                    || this.ForeclosureSalePostponementReasonTypeAdditionalDescriptionSpecified
                    || this.ForeclosureSalePostponementReasonTypeOtherDescriptionSpecified
                    || this.ForeclosureSaleRescindedReasonTypeSpecified
                    || this.ForeclosureSaleRescindedReasonTypeAdditionalDescriptionSpecified
                    || this.ForeclosureSaleRescindedReasonTypeOtherDescriptionSpecified
                    || this.ForeclosureSaleScheduledDateSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ForeclosureActionDate", Order = 0)]
        public MISMODate ForeclosureActionDate { get; set; }
    
        [XmlIgnore]
        public bool ForeclosureActionDateSpecified
        {
            get { return this.ForeclosureActionDate != null; }
            set { }
        }
    
        [XmlElement("ForeclosureActionType", Order = 1)]
        public MISMOEnum<ForeclosureActionBase> ForeclosureActionType { get; set; }
    
        [XmlIgnore]
        public bool ForeclosureActionTypeSpecified
        {
            get { return this.ForeclosureActionType != null; }
            set { }
        }
    
        [XmlElement("ForeclosureActionTypeOtherDescription", Order = 2)]
        public MISMOString ForeclosureActionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ForeclosureActionTypeOtherDescriptionSpecified
        {
            get { return this.ForeclosureActionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ForeclosureProcedureTerminatedReasonType", Order = 3)]
        public MISMOEnum<ForeclosureProcedureTerminatedReasonBase> ForeclosureProcedureTerminatedReasonType { get; set; }
    
        [XmlIgnore]
        public bool ForeclosureProcedureTerminatedReasonTypeSpecified
        {
            get { return this.ForeclosureProcedureTerminatedReasonType != null; }
            set { }
        }
    
        [XmlElement("ForeclosureProcedureTerminatedReasonTypeAdditionalDescription", Order = 4)]
        public MISMOString ForeclosureProcedureTerminatedReasonTypeAdditionalDescription { get; set; }
    
        [XmlIgnore]
        public bool ForeclosureProcedureTerminatedReasonTypeAdditionalDescriptionSpecified
        {
            get { return this.ForeclosureProcedureTerminatedReasonTypeAdditionalDescription != null; }
            set { }
        }
    
        [XmlElement("ForeclosureProcedureTerminatedReasonTypeOtherDescription", Order = 5)]
        public MISMOString ForeclosureProcedureTerminatedReasonTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ForeclosureProcedureTerminatedReasonTypeOtherDescriptionSpecified
        {
            get { return this.ForeclosureProcedureTerminatedReasonTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ForeclosureSaleCancellationReasonType", Order = 6)]
        public MISMOEnum<ForeclosureSaleCancellationReasonBase> ForeclosureSaleCancellationReasonType { get; set; }
    
        [XmlIgnore]
        public bool ForeclosureSaleCancellationReasonTypeSpecified
        {
            get { return this.ForeclosureSaleCancellationReasonType != null; }
            set { }
        }
    
        [XmlElement("ForeclosureSaleCancellationReasonTypeAdditionalDescription", Order = 7)]
        public MISMOString ForeclosureSaleCancellationReasonTypeAdditionalDescription { get; set; }
    
        [XmlIgnore]
        public bool ForeclosureSaleCancellationReasonTypeAdditionalDescriptionSpecified
        {
            get { return this.ForeclosureSaleCancellationReasonTypeAdditionalDescription != null; }
            set { }
        }
    
        [XmlElement("ForeclosureSaleCancellationReasonTypeOtherDescription", Order = 8)]
        public MISMOString ForeclosureSaleCancellationReasonTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ForeclosureSaleCancellationReasonTypeOtherDescriptionSpecified
        {
            get { return this.ForeclosureSaleCancellationReasonTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ForeclosureSalePostponedToDate", Order = 9)]
        public MISMODate ForeclosureSalePostponedToDate { get; set; }
    
        [XmlIgnore]
        public bool ForeclosureSalePostponedToDateSpecified
        {
            get { return this.ForeclosureSalePostponedToDate != null; }
            set { }
        }
    
        [XmlElement("ForeclosureSalePostponementReasonDate", Order = 10)]
        public MISMODate ForeclosureSalePostponementReasonDate { get; set; }
    
        [XmlIgnore]
        public bool ForeclosureSalePostponementReasonDateSpecified
        {
            get { return this.ForeclosureSalePostponementReasonDate != null; }
            set { }
        }
    
        [XmlElement("ForeclosureSalePostponementReasonType", Order = 11)]
        public MISMOEnum<ForeclosureSalePostponementReasonBase> ForeclosureSalePostponementReasonType { get; set; }
    
        [XmlIgnore]
        public bool ForeclosureSalePostponementReasonTypeSpecified
        {
            get { return this.ForeclosureSalePostponementReasonType != null; }
            set { }
        }
    
        [XmlElement("ForeclosureSalePostponementReasonTypeAdditionalDescription", Order = 12)]
        public MISMOString ForeclosureSalePostponementReasonTypeAdditionalDescription { get; set; }
    
        [XmlIgnore]
        public bool ForeclosureSalePostponementReasonTypeAdditionalDescriptionSpecified
        {
            get { return this.ForeclosureSalePostponementReasonTypeAdditionalDescription != null; }
            set { }
        }
    
        [XmlElement("ForeclosureSalePostponementReasonTypeOtherDescription", Order = 13)]
        public MISMOString ForeclosureSalePostponementReasonTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ForeclosureSalePostponementReasonTypeOtherDescriptionSpecified
        {
            get { return this.ForeclosureSalePostponementReasonTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ForeclosureSaleRescindedReasonType", Order = 14)]
        public MISMOEnum<ForeclosureSaleRescindedReasonBase> ForeclosureSaleRescindedReasonType { get; set; }
    
        [XmlIgnore]
        public bool ForeclosureSaleRescindedReasonTypeSpecified
        {
            get { return this.ForeclosureSaleRescindedReasonType != null; }
            set { }
        }
    
        [XmlElement("ForeclosureSaleRescindedReasonTypeAdditionalDescription", Order = 15)]
        public MISMOString ForeclosureSaleRescindedReasonTypeAdditionalDescription { get; set; }
    
        [XmlIgnore]
        public bool ForeclosureSaleRescindedReasonTypeAdditionalDescriptionSpecified
        {
            get { return this.ForeclosureSaleRescindedReasonTypeAdditionalDescription != null; }
            set { }
        }
    
        [XmlElement("ForeclosureSaleRescindedReasonTypeOtherDescription", Order = 16)]
        public MISMOString ForeclosureSaleRescindedReasonTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ForeclosureSaleRescindedReasonTypeOtherDescriptionSpecified
        {
            get { return this.ForeclosureSaleRescindedReasonTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ForeclosureSaleScheduledDate", Order = 17)]
        public MISMODate ForeclosureSaleScheduledDate { get; set; }
    
        [XmlIgnore]
        public bool ForeclosureSaleScheduledDateSpecified
        {
            get { return this.ForeclosureSaleScheduledDate != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 18)]
        public FORECLOSURE_ACTION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
