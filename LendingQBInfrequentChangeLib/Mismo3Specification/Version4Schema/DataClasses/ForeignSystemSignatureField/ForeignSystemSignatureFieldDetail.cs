namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class FOREIGN_SYSTEM_SIGNATURE_FIELD_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EventTypeSpecified
                    || this.EventTypeOtherDescriptionSpecified
                    || this.SignatureRequiredIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("EventType", Order = 0)]
        public MISMOEnum<EventBase> EventType { get; set; }
    
        [XmlIgnore]
        public bool EventTypeSpecified
        {
            get { return this.EventType != null; }
            set { }
        }
    
        [XmlElement("EventTypeOtherDescription", Order = 1)]
        public MISMOString EventTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool EventTypeOtherDescriptionSpecified
        {
            get { return this.EventTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("SignatureRequiredIndicator", Order = 2)]
        public MISMOIndicator SignatureRequiredIndicator { get; set; }
    
        [XmlIgnore]
        public bool SignatureRequiredIndicatorSpecified
        {
            get { return this.SignatureRequiredIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public FOREIGN_SYSTEM_SIGNATURE_FIELD_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
