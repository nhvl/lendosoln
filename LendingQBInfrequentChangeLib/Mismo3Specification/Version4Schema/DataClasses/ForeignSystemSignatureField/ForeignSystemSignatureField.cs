namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class FOREIGN_SYSTEM_SIGNATURE_FIELD
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ForeignSystemSignatureFieldDetailSpecified
                    || this.SIGNATURE_FIELD_REFERENCESpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("FOREIGN_SYSTEM_SIGNATURE_FIELD_DETAIL", Order = 0)]
        public FOREIGN_SYSTEM_SIGNATURE_FIELD_DETAIL ForeignSystemSignatureFieldDetail { get; set; }
    
        [XmlIgnore]
        public bool ForeignSystemSignatureFieldDetailSpecified
        {
            get { return this.ForeignSystemSignatureFieldDetail != null && this.ForeignSystemSignatureFieldDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SIGNATURE_FIELD_REFERENCE", Order = 1)]
        public FIELD_REFERENCE SIGNATURE_FIELD_REFERENCE { get; set; }
    
        [XmlIgnore]
        public bool SIGNATURE_FIELD_REFERENCESpecified
        {
            get { return this.SIGNATURE_FIELD_REFERENCE != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public FOREIGN_SYSTEM_SIGNATURE_FIELD_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
