namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class INSULATION_AREAS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.InsulationAreaListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("INSULATION_AREA", Order = 0)]
        public List<INSULATION_AREA> InsulationAreaList { get; set; } = new List<INSULATION_AREA>();
    
        [XmlIgnore]
        public bool InsulationAreaListSpecified
        {
            get { return this.InsulationAreaList != null && this.InsulationAreaList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public INSULATION_AREAS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
