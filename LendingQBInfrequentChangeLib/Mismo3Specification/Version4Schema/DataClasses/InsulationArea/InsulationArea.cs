namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class INSULATION_AREA
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.InsulationAreaTypeSpecified
                    || this.InsulationAreaTypeOtherDescriptionSpecified
                    || this.InsulationCommentDescriptionSpecified
                    || this.InsulationMaterialTypeSpecified
                    || this.InsulationMaterialTypeOtherDescriptionSpecified
                    || this.InsulationPresenceTypeSpecified
                    || this.InsulationPresenceTypeOtherDescriptionSpecified
                    || this.InsulationRatingDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("InsulationAreaType", Order = 0)]
        public MISMOEnum<InsulationAreaBase> InsulationAreaType { get; set; }
    
        [XmlIgnore]
        public bool InsulationAreaTypeSpecified
        {
            get { return this.InsulationAreaType != null; }
            set { }
        }
    
        [XmlElement("InsulationAreaTypeOtherDescription", Order = 1)]
        public MISMOString InsulationAreaTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool InsulationAreaTypeOtherDescriptionSpecified
        {
            get { return this.InsulationAreaTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("InsulationCommentDescription", Order = 2)]
        public MISMOString InsulationCommentDescription { get; set; }
    
        [XmlIgnore]
        public bool InsulationCommentDescriptionSpecified
        {
            get { return this.InsulationCommentDescription != null; }
            set { }
        }
    
        [XmlElement("InsulationMaterialType", Order = 3)]
        public MISMOEnum<InsulationMaterialBase> InsulationMaterialType { get; set; }
    
        [XmlIgnore]
        public bool InsulationMaterialTypeSpecified
        {
            get { return this.InsulationMaterialType != null; }
            set { }
        }
    
        [XmlElement("InsulationMaterialTypeOtherDescription", Order = 4)]
        public MISMOString InsulationMaterialTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool InsulationMaterialTypeOtherDescriptionSpecified
        {
            get { return this.InsulationMaterialTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("InsulationPresenceType", Order = 5)]
        public MISMOEnum<InsulationPresenceBase> InsulationPresenceType { get; set; }
    
        [XmlIgnore]
        public bool InsulationPresenceTypeSpecified
        {
            get { return this.InsulationPresenceType != null; }
            set { }
        }
    
        [XmlElement("InsulationPresenceTypeOtherDescription", Order = 6)]
        public MISMOString InsulationPresenceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool InsulationPresenceTypeOtherDescriptionSpecified
        {
            get { return this.InsulationPresenceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("InsulationRatingDescription", Order = 7)]
        public MISMOString InsulationRatingDescription { get; set; }
    
        [XmlIgnore]
        public bool InsulationRatingDescriptionSpecified
        {
            get { return this.InsulationRatingDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 8)]
        public INSULATION_AREA_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
