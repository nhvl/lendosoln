namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CREDIT_LIABILITY_PRIOR_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditLiabilityAccountBalanceDateSpecified
                    || this.CreditLiabilityAccountReportedDateSpecified
                    || this.CreditLiabilityAccountStatusDateSpecified
                    || this.CreditLiabilityAccountStatusTypeSpecified
                    || this.CreditLiabilityActualPaymentAmountSpecified
                    || this.CreditLiabilityCreditLimitAmountSpecified
                    || this.CreditLiabilityCurrentRatingCodeSpecified
                    || this.CreditLiabilityCurrentRatingTypeSpecified
                    || this.CreditLiabilityHighBalanceAmountSpecified
                    || this.CreditLiabilityLastActivityDateSpecified
                    || this.CreditLiabilityLastPaymentDateSpecified
                    || this.CreditLiabilityMonthlyPaymentAmountSpecified
                    || this.CreditLiabilityMonthsRemainingCountSpecified
                    || this.CreditLiabilityPastDueAmountSpecified
                    || this.CreditLiabilityUnpaidBalanceAmountSpecified
                    || this.DeferredPaymentAmountSpecified
                    || this.DeferredPaymentDateSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CreditLiabilityAccountBalanceDate", Order = 0)]
        public MISMODate CreditLiabilityAccountBalanceDate { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityAccountBalanceDateSpecified
        {
            get { return this.CreditLiabilityAccountBalanceDate != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityAccountReportedDate", Order = 1)]
        public MISMODate CreditLiabilityAccountReportedDate { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityAccountReportedDateSpecified
        {
            get { return this.CreditLiabilityAccountReportedDate != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityAccountStatusDate", Order = 2)]
        public MISMODate CreditLiabilityAccountStatusDate { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityAccountStatusDateSpecified
        {
            get { return this.CreditLiabilityAccountStatusDate != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityAccountStatusType", Order = 3)]
        public MISMOEnum<CreditLiabilityAccountStatusBase> CreditLiabilityAccountStatusType { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityAccountStatusTypeSpecified
        {
            get { return this.CreditLiabilityAccountStatusType != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityActualPaymentAmount", Order = 4)]
        public MISMOAmount CreditLiabilityActualPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityActualPaymentAmountSpecified
        {
            get { return this.CreditLiabilityActualPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityCreditLimitAmount", Order = 5)]
        public MISMOAmount CreditLiabilityCreditLimitAmount { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityCreditLimitAmountSpecified
        {
            get { return this.CreditLiabilityCreditLimitAmount != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityCurrentRatingCode", Order = 6)]
        public MISMOCode CreditLiabilityCurrentRatingCode { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityCurrentRatingCodeSpecified
        {
            get { return this.CreditLiabilityCurrentRatingCode != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityCurrentRatingType", Order = 7)]
        public MISMOEnum<CreditLiabilityCurrentRatingBase> CreditLiabilityCurrentRatingType { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityCurrentRatingTypeSpecified
        {
            get { return this.CreditLiabilityCurrentRatingType != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityHighBalanceAmount", Order = 8)]
        public MISMOAmount CreditLiabilityHighBalanceAmount { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityHighBalanceAmountSpecified
        {
            get { return this.CreditLiabilityHighBalanceAmount != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityLastActivityDate", Order = 9)]
        public MISMODate CreditLiabilityLastActivityDate { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityLastActivityDateSpecified
        {
            get { return this.CreditLiabilityLastActivityDate != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityLastPaymentDate", Order = 10)]
        public MISMODate CreditLiabilityLastPaymentDate { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityLastPaymentDateSpecified
        {
            get { return this.CreditLiabilityLastPaymentDate != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityMonthlyPaymentAmount", Order = 11)]
        public MISMOAmount CreditLiabilityMonthlyPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityMonthlyPaymentAmountSpecified
        {
            get { return this.CreditLiabilityMonthlyPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityMonthsRemainingCount", Order = 12)]
        public MISMOCount CreditLiabilityMonthsRemainingCount { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityMonthsRemainingCountSpecified
        {
            get { return this.CreditLiabilityMonthsRemainingCount != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityPastDueAmount", Order = 13)]
        public MISMOAmount CreditLiabilityPastDueAmount { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityPastDueAmountSpecified
        {
            get { return this.CreditLiabilityPastDueAmount != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityUnpaidBalanceAmount", Order = 14)]
        public MISMOAmount CreditLiabilityUnpaidBalanceAmount { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityUnpaidBalanceAmountSpecified
        {
            get { return this.CreditLiabilityUnpaidBalanceAmount != null; }
            set { }
        }
    
        [XmlElement("DeferredPaymentAmount", Order = 15)]
        public MISMOAmount DeferredPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool DeferredPaymentAmountSpecified
        {
            get { return this.DeferredPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("DeferredPaymentDate", Order = 16)]
        public MISMODate DeferredPaymentDate { get; set; }
    
        [XmlIgnore]
        public bool DeferredPaymentDateSpecified
        {
            get { return this.DeferredPaymentDate != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 17)]
        public CREDIT_LIABILITY_PRIOR_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
