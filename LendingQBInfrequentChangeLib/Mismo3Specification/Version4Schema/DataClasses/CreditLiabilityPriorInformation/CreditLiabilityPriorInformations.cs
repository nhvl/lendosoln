namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class CREDIT_LIABILITY_PRIOR_INFORMATIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditLiabilityPriorInformationListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CREDIT_LIABILITY_PRIOR_INFORMATION", Order = 0)]
        public List<CREDIT_LIABILITY_PRIOR_INFORMATION> CreditLiabilityPriorInformationList { get; set; } = new List<CREDIT_LIABILITY_PRIOR_INFORMATION>();
    
        [XmlIgnore]
        public bool CreditLiabilityPriorInformationListSpecified
        {
            get { return this.CreditLiabilityPriorInformationList != null && this.CreditLiabilityPriorInformationList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public CREDIT_LIABILITY_PRIOR_INFORMATIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
