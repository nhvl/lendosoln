namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CREDIT_LIABILITY_PRIOR_INFORMATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditCommentsSpecified
                    || this.CreditLiabilityPriorDetailSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CREDIT_COMMENTS", Order = 0)]
        public CREDIT_COMMENTS CreditComments { get; set; }
    
        [XmlIgnore]
        public bool CreditCommentsSpecified
        {
            get { return this.CreditComments != null && this.CreditComments.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_LIABILITY_PRIOR_DETAIL", Order = 1)]
        public CREDIT_LIABILITY_PRIOR_DETAIL CreditLiabilityPriorDetail { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityPriorDetailSpecified
        {
            get { return this.CreditLiabilityPriorDetail != null && this.CreditLiabilityPriorDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public CREDIT_LIABILITY_PRIOR_INFORMATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
