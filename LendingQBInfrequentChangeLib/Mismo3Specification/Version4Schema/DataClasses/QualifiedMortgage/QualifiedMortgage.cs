namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class QUALIFIED_MORTGAGE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExemptionsSpecified
                    || this.QualifiedMortgageDetailSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("EXEMPTIONS", Order = 0)]
        public EXEMPTIONS Exemptions { get; set; }
    
        [XmlIgnore]
        public bool ExemptionsSpecified
        {
            get { return this.Exemptions != null && this.Exemptions.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("QUALIFIED_MORTGAGE_DETAIL", Order = 1)]
        public QUALIFIED_MORTGAGE_DETAIL QualifiedMortgageDetail { get; set; }
    
        [XmlIgnore]
        public bool QualifiedMortgageDetailSpecified
        {
            get { return this.QualifiedMortgageDetail != null && this.QualifiedMortgageDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public QUALIFIED_MORTGAGE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
