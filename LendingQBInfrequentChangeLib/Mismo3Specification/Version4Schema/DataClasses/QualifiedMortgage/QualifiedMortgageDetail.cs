namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class QUALIFIED_MORTGAGE_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AbilityToRepayInterestRateDebtExpenseRatioPercentSpecified
                    || this.AbilityToRepayMethodTypeSpecified
                    || this.PortfolioPeriodEffectiveMonthsCountSpecified
                    || this.PresumptionOfComplianceTypeSpecified
                    || this.QualifiedMortgageHighestRateDebtExpenseRatioPercentSpecified
                    || this.QualifiedMortgageMinimumRequiredFICOScoreValueSpecified
                    || this.QualifiedMortgageTemporaryGSETypeSpecified
                    || this.QualifiedMortgageTotalPointsAndFeesThresholdAmountSpecified
                    || this.QualifiedMortgageTotalPointsAndFeesThresholdPercentSpecified
                    || this.QualifiedMortgageTypeSpecified
                    || this.QualifiedMortgageTypeOtherDescriptionSpecified
                    || this.SafeHarborThresholdAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AbilityToRepayInterestRateDebtExpenseRatioPercent", Order = 0)]
        public MISMOPercent AbilityToRepayInterestRateDebtExpenseRatioPercent { get; set; }
    
        [XmlIgnore]
        public bool AbilityToRepayInterestRateDebtExpenseRatioPercentSpecified
        {
            get { return this.AbilityToRepayInterestRateDebtExpenseRatioPercent != null; }
            set { }
        }
    
        [XmlElement("AbilityToRepayMethodType", Order = 1)]
        public MISMOEnum<AbilityToRepayMethodBase> AbilityToRepayMethodType { get; set; }
    
        [XmlIgnore]
        public bool AbilityToRepayMethodTypeSpecified
        {
            get { return this.AbilityToRepayMethodType != null; }
            set { }
        }
    
        [XmlElement("PortfolioPeriodEffectiveMonthsCount", Order = 2)]
        public MISMOCount PortfolioPeriodEffectiveMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool PortfolioPeriodEffectiveMonthsCountSpecified
        {
            get { return this.PortfolioPeriodEffectiveMonthsCount != null; }
            set { }
        }
    
        [XmlElement("PresumptionOfComplianceType", Order = 3)]
        public MISMOEnum<PresumptionOfComplianceBase> PresumptionOfComplianceType { get; set; }
    
        [XmlIgnore]
        public bool PresumptionOfComplianceTypeSpecified
        {
            get { return this.PresumptionOfComplianceType != null; }
            set { }
        }
    
        [XmlElement("QualifiedMortgageHighestRateDebtExpenseRatioPercent", Order = 4)]
        public MISMOPercent QualifiedMortgageHighestRateDebtExpenseRatioPercent { get; set; }
    
        [XmlIgnore]
        public bool QualifiedMortgageHighestRateDebtExpenseRatioPercentSpecified
        {
            get { return this.QualifiedMortgageHighestRateDebtExpenseRatioPercent != null; }
            set { }
        }
    
        [XmlElement("QualifiedMortgageMinimumRequiredFICOScoreValue", Order = 5)]
        public MISMOValue QualifiedMortgageMinimumRequiredFICOScoreValue { get; set; }
    
        [XmlIgnore]
        public bool QualifiedMortgageMinimumRequiredFICOScoreValueSpecified
        {
            get { return this.QualifiedMortgageMinimumRequiredFICOScoreValue != null; }
            set { }
        }
    
        [XmlElement("QualifiedMortgageTemporaryGSEType", Order = 6)]
        public MISMOEnum<QualifiedMortgageTemporaryGSEBase> QualifiedMortgageTemporaryGSEType { get; set; }
    
        [XmlIgnore]
        public bool QualifiedMortgageTemporaryGSETypeSpecified
        {
            get { return this.QualifiedMortgageTemporaryGSEType != null; }
            set { }
        }
    
        [XmlElement("QualifiedMortgageTotalPointsAndFeesThresholdAmount", Order = 7)]
        public MISMOAmount QualifiedMortgageTotalPointsAndFeesThresholdAmount { get; set; }
    
        [XmlIgnore]
        public bool QualifiedMortgageTotalPointsAndFeesThresholdAmountSpecified
        {
            get { return this.QualifiedMortgageTotalPointsAndFeesThresholdAmount != null; }
            set { }
        }
    
        [XmlElement("QualifiedMortgageTotalPointsAndFeesThresholdPercent", Order = 8)]
        public MISMOPercent QualifiedMortgageTotalPointsAndFeesThresholdPercent { get; set; }
    
        [XmlIgnore]
        public bool QualifiedMortgageTotalPointsAndFeesThresholdPercentSpecified
        {
            get { return this.QualifiedMortgageTotalPointsAndFeesThresholdPercent != null; }
            set { }
        }
    
        [XmlElement("QualifiedMortgageType", Order = 9)]
        public MISMOEnum<QualifiedMortgageBase> QualifiedMortgageType { get; set; }
    
        [XmlIgnore]
        public bool QualifiedMortgageTypeSpecified
        {
            get { return this.QualifiedMortgageType != null; }
            set { }
        }
    
        [XmlElement("QualifiedMortgageTypeOtherDescription", Order = 10)]
        public MISMOString QualifiedMortgageTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool QualifiedMortgageTypeOtherDescriptionSpecified
        {
            get { return this.QualifiedMortgageTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("SafeHarborThresholdAmount", Order = 11)]
        public MISMOAmount SafeHarborThresholdAmount { get; set; }
    
        [XmlIgnore]
        public bool SafeHarborThresholdAmountSpecified
        {
            get { return this.SafeHarborThresholdAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 12)]
        public QUALIFIED_MORTGAGE_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
