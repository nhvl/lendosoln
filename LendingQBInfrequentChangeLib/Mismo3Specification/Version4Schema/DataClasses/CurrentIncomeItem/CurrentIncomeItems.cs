namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class CURRENT_INCOME_ITEMS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CurrentIncomeItemListSpecified
                    || this.CurrentIncomeItemSummarySpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CURRENT_INCOME_ITEM", Order = 0)]
        public List<CURRENT_INCOME_ITEM> CurrentIncomeItemList { get; set; } = new List<CURRENT_INCOME_ITEM>();
    
        [XmlIgnore]
        public bool CurrentIncomeItemListSpecified
        {
            get { return this.CurrentIncomeItemList != null && this.CurrentIncomeItemList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("CURRENT_INCOME_ITEM_SUMMARY", Order = 1)]
        public CURRENT_INCOME_ITEM_SUMMARY CurrentIncomeItemSummary { get; set; }
    
        [XmlIgnore]
        public bool CurrentIncomeItemSummarySpecified
        {
            get { return this.CurrentIncomeItemSummary != null && this.CurrentIncomeItemSummary.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public CURRENT_INCOME_ITEMS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
