namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CURRENT_INCOME_ITEM
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CurrentIncomeItemDetailSpecified
                    || this.IncomeDocumentationsSpecified
                    || this.VerificationSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CURRENT_INCOME_ITEM_DETAIL", Order = 0)]
        public CURRENT_INCOME_ITEM_DETAIL CurrentIncomeItemDetail { get; set; }
    
        [XmlIgnore]
        public bool CurrentIncomeItemDetailSpecified
        {
            get { return this.CurrentIncomeItemDetail != null && this.CurrentIncomeItemDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("INCOME_DOCUMENTATIONS", Order = 1)]
        public INCOME_DOCUMENTATIONS IncomeDocumentations { get; set; }
    
        [XmlIgnore]
        public bool IncomeDocumentationsSpecified
        {
            get { return this.IncomeDocumentations != null && this.IncomeDocumentations.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("VERIFICATION", Order = 2)]
        public VERIFICATION Verification { get; set; }
    
        [XmlIgnore]
        public bool VerificationSpecified
        {
            get { return this.Verification != null && this.Verification.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public CURRENT_INCOME_ITEM_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
