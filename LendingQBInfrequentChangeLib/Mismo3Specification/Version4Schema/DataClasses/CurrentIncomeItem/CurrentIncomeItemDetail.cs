namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CURRENT_INCOME_ITEM_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CurrentIncomeMonthlyTotalAmountSpecified
                    || this.EmploymentIncomeIndicatorSpecified
                    || this.ForeignIncomeIndicatorSpecified
                    || this.IncomeFederalTaxExemptIndicatorSpecified
                    || this.IncomeTypeSpecified
                    || this.IncomeTypeOtherDescriptionSpecified
                    || this.SeasonalIncomeIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CurrentIncomeMonthlyTotalAmount", Order = 0)]
        public MISMOAmount CurrentIncomeMonthlyTotalAmount { get; set; }
    
        [XmlIgnore]
        public bool CurrentIncomeMonthlyTotalAmountSpecified
        {
            get { return this.CurrentIncomeMonthlyTotalAmount != null; }
            set { }
        }
    
        [XmlElement("EmploymentIncomeIndicator", Order = 1)]
        public MISMOIndicator EmploymentIncomeIndicator { get; set; }
    
        [XmlIgnore]
        public bool EmploymentIncomeIndicatorSpecified
        {
            get { return this.EmploymentIncomeIndicator != null; }
            set { }
        }
    
        [XmlElement("ForeignIncomeIndicator", Order = 2)]
        public MISMOIndicator ForeignIncomeIndicator { get; set; }
    
        [XmlIgnore]
        public bool ForeignIncomeIndicatorSpecified
        {
            get { return this.ForeignIncomeIndicator != null; }
            set { }
        }
    
        [XmlElement("IncomeFederalTaxExemptIndicator", Order = 3)]
        public MISMOIndicator IncomeFederalTaxExemptIndicator { get; set; }
    
        [XmlIgnore]
        public bool IncomeFederalTaxExemptIndicatorSpecified
        {
            get { return this.IncomeFederalTaxExemptIndicator != null; }
            set { }
        }
    
        [XmlElement("IncomeType", Order = 4)]
        public MISMOEnum<IncomeBase> IncomeType { get; set; }
    
        [XmlIgnore]
        public bool IncomeTypeSpecified
        {
            get { return this.IncomeType != null; }
            set { }
        }
    
        [XmlElement("IncomeTypeOtherDescription", Order = 5)]
        public MISMOString IncomeTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool IncomeTypeOtherDescriptionSpecified
        {
            get { return this.IncomeTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("SeasonalIncomeIndicator", Order = 6)]
        public MISMOIndicator SeasonalIncomeIndicator { get; set; }
    
        [XmlIgnore]
        public bool SeasonalIncomeIndicatorSpecified
        {
            get { return this.SeasonalIncomeIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 7)]
        public CURRENT_INCOME_ITEM_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
