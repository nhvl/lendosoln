namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CURRENT_INCOME_ITEM_SUMMARY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MonthlyMilitaryEntitlementsAmountSpecified
                    || this.MonthlyRetirementAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MonthlyMilitaryEntitlementsAmount", Order = 0)]
        public MISMOAmount MonthlyMilitaryEntitlementsAmount { get; set; }
    
        [XmlIgnore]
        public bool MonthlyMilitaryEntitlementsAmountSpecified
        {
            get { return this.MonthlyMilitaryEntitlementsAmount != null; }
            set { }
        }
    
        [XmlElement("MonthlyRetirementAmount", Order = 1)]
        public MISMOAmount MonthlyRetirementAmount { get; set; }
    
        [XmlIgnore]
        public bool MonthlyRetirementAmountSpecified
        {
            get { return this.MonthlyRetirementAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public CURRENT_INCOME_ITEM_SUMMARY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
