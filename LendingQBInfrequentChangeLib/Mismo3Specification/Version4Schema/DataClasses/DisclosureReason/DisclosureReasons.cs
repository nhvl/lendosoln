namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class DISCLOSURE_REASONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DisclosureReasonListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DISCLOSURE_REASON", Order = 0)]
        public List<DISCLOSURE_REASON> DisclosureReasonList { get; set; } = new List<DISCLOSURE_REASON>();
    
        [XmlIgnore]
        public bool DisclosureReasonListSpecified
        {
            get { return this.DisclosureReasonList != null && this.DisclosureReasonList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public DISCLOSURE_REASONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
