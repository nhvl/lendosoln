namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class DISCLOSURE_REASON
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ChangedCircumstanceIndicatorSpecified
                    || this.DisclosureCommentTextSpecified
                    || this.DisclosureReasonAdditionalDescriptionSpecified
                    || this.DisclosureReasonOccurrenceDateSpecified
                    || this.DisclosureReasonTypeSpecified
                    || this.DisclosureReasonTypeOtherDescriptionSpecified
                    || this.FeePreviouslyDisclosedAmountSpecified
                    || this.FeeTypeSpecified
                    || this.FeeTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ChangedCircumstanceIndicator", Order = 0)]
        public MISMOIndicator ChangedCircumstanceIndicator { get; set; }
    
        [XmlIgnore]
        public bool ChangedCircumstanceIndicatorSpecified
        {
            get { return this.ChangedCircumstanceIndicator != null; }
            set { }
        }
    
        [XmlElement("DisclosureCommentText", Order = 1)]
        public MISMOString DisclosureCommentText { get; set; }
    
        [XmlIgnore]
        public bool DisclosureCommentTextSpecified
        {
            get { return this.DisclosureCommentText != null; }
            set { }
        }
    
        [XmlElement("DisclosureReasonAdditionalDescription", Order = 2)]
        public MISMOString DisclosureReasonAdditionalDescription { get; set; }
    
        [XmlIgnore]
        public bool DisclosureReasonAdditionalDescriptionSpecified
        {
            get { return this.DisclosureReasonAdditionalDescription != null; }
            set { }
        }
    
        [XmlElement("DisclosureReasonOccurrenceDate", Order = 3)]
        public MISMODate DisclosureReasonOccurrenceDate { get; set; }
    
        [XmlIgnore]
        public bool DisclosureReasonOccurrenceDateSpecified
        {
            get { return this.DisclosureReasonOccurrenceDate != null; }
            set { }
        }
    
        [XmlElement("DisclosureReasonType", Order = 4)]
        public MISMOEnum<DisclosureReasonBase> DisclosureReasonType { get; set; }
    
        [XmlIgnore]
        public bool DisclosureReasonTypeSpecified
        {
            get { return this.DisclosureReasonType != null; }
            set { }
        }
    
        [XmlElement("DisclosureReasonTypeOtherDescription", Order = 5)]
        public MISMOString DisclosureReasonTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool DisclosureReasonTypeOtherDescriptionSpecified
        {
            get { return this.DisclosureReasonTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("FeePreviouslyDisclosedAmount", Order = 6)]
        public MISMOAmount FeePreviouslyDisclosedAmount { get; set; }
    
        [XmlIgnore]
        public bool FeePreviouslyDisclosedAmountSpecified
        {
            get { return this.FeePreviouslyDisclosedAmount != null; }
            set { }
        }
    
        [XmlElement("FeeType", Order = 7)]
        public MISMOEnum<FeeBase> FeeType { get; set; }
    
        [XmlIgnore]
        public bool FeeTypeSpecified
        {
            get { return this.FeeType != null; }
            set { }
        }
    
        [XmlElement("FeeTypeOtherDescription", Order = 8)]
        public MISMOString FeeTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool FeeTypeOtherDescriptionSpecified
        {
            get { return this.FeeTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 9)]
        public DISCLOSURE_REASON_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
