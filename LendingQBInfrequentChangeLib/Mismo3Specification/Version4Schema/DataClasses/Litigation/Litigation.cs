namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class LITIGATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.JudgmentsSpecified
                    || this.JurisdictionSpecified
                    || this.LitigationDetailSpecified
                    || this.LitigationStatusesSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("JUDGMENTS", Order = 0)]
        public JUDGMENTS Judgments { get; set; }
    
        [XmlIgnore]
        public bool JudgmentsSpecified
        {
            get { return this.Judgments != null && this.Judgments.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("JURISDICTION", Order = 1)]
        public JURISDICTION Jurisdiction { get; set; }
    
        [XmlIgnore]
        public bool JurisdictionSpecified
        {
            get { return this.Jurisdiction != null && this.Jurisdiction.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("LITIGATION_DETAIL", Order = 2)]
        public LITIGATION_DETAIL LitigationDetail { get; set; }
    
        [XmlIgnore]
        public bool LitigationDetailSpecified
        {
            get { return this.LitigationDetail != null && this.LitigationDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("LITIGATION_STATUSES", Order = 3)]
        public LITIGATION_STATUSES LitigationStatuses { get; set; }
    
        [XmlIgnore]
        public bool LitigationStatusesSpecified
        {
            get { return this.LitigationStatuses != null && this.LitigationStatuses.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public LITIGATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
