namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class LITIGATION_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DismissalTypeSpecified
                    || this.LitigationAffectsInvestorIndicatorSpecified
                    || this.LitigationAffectsTitleIndicatorSpecified
                    || this.LitigationClassActionIndicatorSpecified
                    || this.LitigationEndReasonTypeSpecified
                    || this.LitigationEndReasonTypeAdditionalDescriptionSpecified
                    || this.LitigationEndReasonTypeOtherDescriptionSpecified
                    || this.LitigationFiledDateSpecified
                    || this.LitigationProceedingsEndDateSpecified
                    || this.LitigationThreatenedDateSpecified
                    || this.LitigationTypeSpecified
                    || this.LitigationTypeAdditionalDescriptionSpecified
                    || this.LitigationTypeOtherDescriptionSpecified
                    || this.NonTitleLitigationAffectsPropertyIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DismissalType", Order = 0)]
        public MISMOEnum<DismissalBase> DismissalType { get; set; }
    
        [XmlIgnore]
        public bool DismissalTypeSpecified
        {
            get { return this.DismissalType != null; }
            set { }
        }
    
        [XmlElement("LitigationAffectsInvestorIndicator", Order = 1)]
        public MISMOIndicator LitigationAffectsInvestorIndicator { get; set; }
    
        [XmlIgnore]
        public bool LitigationAffectsInvestorIndicatorSpecified
        {
            get { return this.LitigationAffectsInvestorIndicator != null; }
            set { }
        }
    
        [XmlElement("LitigationAffectsTitleIndicator", Order = 2)]
        public MISMOIndicator LitigationAffectsTitleIndicator { get; set; }
    
        [XmlIgnore]
        public bool LitigationAffectsTitleIndicatorSpecified
        {
            get { return this.LitigationAffectsTitleIndicator != null; }
            set { }
        }
    
        [XmlElement("LitigationClassActionIndicator", Order = 3)]
        public MISMOIndicator LitigationClassActionIndicator { get; set; }
    
        [XmlIgnore]
        public bool LitigationClassActionIndicatorSpecified
        {
            get { return this.LitigationClassActionIndicator != null; }
            set { }
        }
    
        [XmlElement("LitigationEndReasonType", Order = 4)]
        public MISMOEnum<LitigationEndReasonBase> LitigationEndReasonType { get; set; }
    
        [XmlIgnore]
        public bool LitigationEndReasonTypeSpecified
        {
            get { return this.LitigationEndReasonType != null; }
            set { }
        }
    
        [XmlElement("LitigationEndReasonTypeAdditionalDescription", Order = 5)]
        public MISMOString LitigationEndReasonTypeAdditionalDescription { get; set; }
    
        [XmlIgnore]
        public bool LitigationEndReasonTypeAdditionalDescriptionSpecified
        {
            get { return this.LitigationEndReasonTypeAdditionalDescription != null; }
            set { }
        }
    
        [XmlElement("LitigationEndReasonTypeOtherDescription", Order = 6)]
        public MISMOString LitigationEndReasonTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool LitigationEndReasonTypeOtherDescriptionSpecified
        {
            get { return this.LitigationEndReasonTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("LitigationFiledDate", Order = 7)]
        public MISMODate LitigationFiledDate { get; set; }
    
        [XmlIgnore]
        public bool LitigationFiledDateSpecified
        {
            get { return this.LitigationFiledDate != null; }
            set { }
        }
    
        [XmlElement("LitigationProceedingsEndDate", Order = 8)]
        public MISMODate LitigationProceedingsEndDate { get; set; }
    
        [XmlIgnore]
        public bool LitigationProceedingsEndDateSpecified
        {
            get { return this.LitigationProceedingsEndDate != null; }
            set { }
        }
    
        [XmlElement("LitigationThreatenedDate", Order = 9)]
        public MISMODate LitigationThreatenedDate { get; set; }
    
        [XmlIgnore]
        public bool LitigationThreatenedDateSpecified
        {
            get { return this.LitigationThreatenedDate != null; }
            set { }
        }
    
        [XmlElement("LitigationType", Order = 10)]
        public MISMOEnum<LitigationBase> LitigationType { get; set; }
    
        [XmlIgnore]
        public bool LitigationTypeSpecified
        {
            get { return this.LitigationType != null; }
            set { }
        }
    
        [XmlElement("LitigationTypeAdditionalDescription", Order = 11)]
        public MISMOString LitigationTypeAdditionalDescription { get; set; }
    
        [XmlIgnore]
        public bool LitigationTypeAdditionalDescriptionSpecified
        {
            get { return this.LitigationTypeAdditionalDescription != null; }
            set { }
        }
    
        [XmlElement("LitigationTypeOtherDescription", Order = 12)]
        public MISMOString LitigationTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool LitigationTypeOtherDescriptionSpecified
        {
            get { return this.LitigationTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("NonTitleLitigationAffectsPropertyIndicator", Order = 13)]
        public MISMOIndicator NonTitleLitigationAffectsPropertyIndicator { get; set; }
    
        [XmlIgnore]
        public bool NonTitleLitigationAffectsPropertyIndicatorSpecified
        {
            get { return this.NonTitleLitigationAffectsPropertyIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 14)]
        public LITIGATION_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
