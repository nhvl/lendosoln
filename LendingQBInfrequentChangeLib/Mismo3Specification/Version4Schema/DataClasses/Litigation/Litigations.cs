namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class LITIGATIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LitigationListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LITIGATION", Order = 0)]
        public List<LITIGATION> LitigationList { get; set; } = new List<LITIGATION>();
    
        [XmlIgnore]
        public bool LitigationListSpecified
        {
            get { return this.LitigationList != null && this.LitigationList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public LITIGATIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
