namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CREDIT_REPOSITORY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditRepositorySourceTypeSpecified
                    || this.CreditRepositorySourceTypeOtherDescriptionSpecified
                    || this.CreditRepositorySubscriberCodeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CreditRepositorySourceType", Order = 0)]
        public MISMOEnum<CreditRepositorySourceBase> CreditRepositorySourceType { get; set; }
    
        [XmlIgnore]
        public bool CreditRepositorySourceTypeSpecified
        {
            get { return this.CreditRepositorySourceType != null; }
            set { }
        }
    
        [XmlElement("CreditRepositorySourceTypeOtherDescription", Order = 1)]
        public MISMOString CreditRepositorySourceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CreditRepositorySourceTypeOtherDescriptionSpecified
        {
            get { return this.CreditRepositorySourceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("CreditRepositorySubscriberCode", Order = 2)]
        public MISMOCode CreditRepositorySubscriberCode { get; set; }
    
        [XmlIgnore]
        public bool CreditRepositorySubscriberCodeSpecified
        {
            get { return this.CreditRepositorySubscriberCode != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public CREDIT_REPOSITORY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
