namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class CREDIT_REPOSITORIES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditRepositoryListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CREDIT_REPOSITORY", Order = 0)]
        public List<CREDIT_REPOSITORY> CreditRepositoryList { get; set; } = new List<CREDIT_REPOSITORY>();
    
        [XmlIgnore]
        public bool CreditRepositoryListSpecified
        {
            get { return this.CreditRepositoryList != null && this.CreditRepositoryList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public CREDIT_REPOSITORIES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
