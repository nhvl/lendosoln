namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CLOSING_COST_FUND
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ClosingCostFundAmountSpecified
                    || this.FundsSourceTypeSpecified
                    || this.FundsSourceTypeOtherDescriptionSpecified
                    || this.FundsTypeSpecified
                    || this.FundsTypeOtherDescriptionSpecified
                    || this.IntegratedDisclosureLineNumberValueSpecified
                    || this.IntegratedDisclosureSectionTypeSpecified
                    || this.IntegratedDisclosureSectionTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ClosingCostFundAmount", Order = 0)]
        public MISMOAmount ClosingCostFundAmount { get; set; }
    
        [XmlIgnore]
        public bool ClosingCostFundAmountSpecified
        {
            get { return this.ClosingCostFundAmount != null; }
            set { }
        }
    
        [XmlElement("FundsSourceType", Order = 1)]
        public MISMOEnum<FundsSourceBase> FundsSourceType { get; set; }
    
        [XmlIgnore]
        public bool FundsSourceTypeSpecified
        {
            get { return this.FundsSourceType != null; }
            set { }
        }
    
        [XmlElement("FundsSourceTypeOtherDescription", Order = 2)]
        public MISMOString FundsSourceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool FundsSourceTypeOtherDescriptionSpecified
        {
            get { return this.FundsSourceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("FundsType", Order = 3)]
        public MISMOEnum<FundsBase> FundsType { get; set; }
    
        [XmlIgnore]
        public bool FundsTypeSpecified
        {
            get { return this.FundsType != null; }
            set { }
        }
    
        [XmlElement("FundsTypeOtherDescription", Order = 4)]
        public MISMOString FundsTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool FundsTypeOtherDescriptionSpecified
        {
            get { return this.FundsTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureLineNumberValue", Order = 5)]
        public MISMOValue IntegratedDisclosureLineNumberValue { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureLineNumberValueSpecified
        {
            get { return this.IntegratedDisclosureLineNumberValue != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureSectionType", Order = 6)]
        public MISMOEnum<IntegratedDisclosureSectionBase> IntegratedDisclosureSectionType { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureSectionTypeSpecified
        {
            get { return this.IntegratedDisclosureSectionType != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureSectionTypeOtherDescription", Order = 7)]
        public MISMOString IntegratedDisclosureSectionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureSectionTypeOtherDescriptionSpecified
        {
            get { return this.IntegratedDisclosureSectionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 8)]
        public CLOSING_COST_FUND_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
