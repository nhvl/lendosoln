namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CLOSING_COST_FUND_SUMMARY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.TotalClosingCostsToBePaidBySellerAndOthersAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("TotalClosingCostsToBePaidBySellerAndOthersAmount", Order = 0)]
        public MISMOAmount TotalClosingCostsToBePaidBySellerAndOthersAmount { get; set; }
    
        [XmlIgnore]
        public bool TotalClosingCostsToBePaidBySellerAndOthersAmountSpecified
        {
            get { return this.TotalClosingCostsToBePaidBySellerAndOthersAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public CLOSING_COST_FUND_SUMMARY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
