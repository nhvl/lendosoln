namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class CLOSING_COST_FUNDS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ClosingCostFundListSpecified
                    || this.ClosingCostFundSummarySpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CLOSING_COST_FUND", Order = 0)]
        public List<CLOSING_COST_FUND> ClosingCostFundList { get; set; } = new List<CLOSING_COST_FUND>();
    
        [XmlIgnore]
        public bool ClosingCostFundListSpecified
        {
            get { return this.ClosingCostFundList != null && this.ClosingCostFundList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("CLOSING_COST_FUND_SUMMARY", Order = 1)]
        public CLOSING_COST_FUND_SUMMARY ClosingCostFundSummary { get; set; }
    
        [XmlIgnore]
        public bool ClosingCostFundSummarySpecified
        {
            get { return this.ClosingCostFundSummary != null && this.ClosingCostFundSummary.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public CLOSING_COST_FUNDS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
