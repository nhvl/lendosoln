namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class STRUCTURE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ArchitecturalDesignSpecified
                    || this.EnergyEfficiencyConsiderationsSpecified
                    || this.ExteriorFeaturesSpecified
                    || this.ExteriorWallsSpecified
                    || this.FoundationsSpecified
                    || this.InsulationSpecified
                    || this.RoofSpecified
                    || this.StructureAnalysesSpecified
                    || this.StructureDetailSpecified
                    || this.WindowsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ARCHITECTURAL_DESIGN", Order = 0)]
        public ARCHITECTURAL_DESIGN ArchitecturalDesign { get; set; }
    
        [XmlIgnore]
        public bool ArchitecturalDesignSpecified
        {
            get { return this.ArchitecturalDesign != null && this.ArchitecturalDesign.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("ENERGY_EFFICIENCY_CONSIDERATIONS", Order = 1)]
        public ENERGY_EFFICIENCY_CONSIDERATIONS EnergyEfficiencyConsiderations { get; set; }
    
        [XmlIgnore]
        public bool EnergyEfficiencyConsiderationsSpecified
        {
            get { return this.EnergyEfficiencyConsiderations != null && this.EnergyEfficiencyConsiderations.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTERIOR_FEATURES", Order = 2)]
        public EXTERIOR_FEATURES ExteriorFeatures { get; set; }
    
        [XmlIgnore]
        public bool ExteriorFeaturesSpecified
        {
            get { return this.ExteriorFeatures != null && this.ExteriorFeatures.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTERIOR_WALLS", Order = 3)]
        public EXTERIOR_WALLS ExteriorWalls { get; set; }
    
        [XmlIgnore]
        public bool ExteriorWallsSpecified
        {
            get { return this.ExteriorWalls != null && this.ExteriorWalls.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("FOUNDATIONS", Order = 4)]
        public FOUNDATIONS Foundations { get; set; }
    
        [XmlIgnore]
        public bool FoundationsSpecified
        {
            get { return this.Foundations != null && this.Foundations.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("INSULATION", Order = 5)]
        public INSULATION Insulation { get; set; }
    
        [XmlIgnore]
        public bool InsulationSpecified
        {
            get { return this.Insulation != null && this.Insulation.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("ROOF", Order = 6)]
        public ROOF Roof { get; set; }
    
        [XmlIgnore]
        public bool RoofSpecified
        {
            get { return this.Roof != null && this.Roof.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("STRUCTURE_ANALYSES", Order = 7)]
        public STRUCTURE_ANALYSES StructureAnalyses { get; set; }
    
        [XmlIgnore]
        public bool StructureAnalysesSpecified
        {
            get { return this.StructureAnalyses != null && this.StructureAnalyses.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("STRUCTURE_DETAIL", Order = 8)]
        public STRUCTURE_DETAIL StructureDetail { get; set; }
    
        [XmlIgnore]
        public bool StructureDetailSpecified
        {
            get { return this.StructureDetail != null && this.StructureDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("WINDOWS", Order = 9)]
        public WINDOWS Windows { get; set; }
    
        [XmlIgnore]
        public bool WindowsSpecified
        {
            get { return this.Windows != null && this.Windows.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 10)]
        public STRUCTURE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
