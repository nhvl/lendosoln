namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class STRUCTURE_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AccessoryUnitCountSpecified
                    || this.AdditionsDescriptionSpecified
                    || this.AgeYearsCountSpecified
                    || this.AtticExistsIndicatorSpecified
                    || this.BuildingCountSpecified
                    || this.CharacteristicsAffectMarketabilityDescriptionSpecified
                    || this.ElevatorCountSpecified
                    || this.GrossBuildingAreaSquareFeetNumberSpecified
                    || this.GrossLivingAreaSquareFeetDataSourceDescriptionSpecified
                    || this.LivingUnitCountSpecified
                    || this.RentControlDescriptionSpecified
                    || this.RentControlStatusTypeSpecified
                    || this.StoriesCountSpecified
                    || this.StructureConditionDescriptionSpecified
                    || this.StructureConstructionStatusDescriptionSpecified
                    || this.StructureNeverOccupiedIndicatorSpecified
                    || this.StructureSingleFamilyResidenceIndicatorSpecified
                    || this.StructureStateDescriptionSpecified
                    || this.StructureStateTypeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AccessoryUnitCount", Order = 0)]
        public MISMOCount AccessoryUnitCount { get; set; }
    
        [XmlIgnore]
        public bool AccessoryUnitCountSpecified
        {
            get { return this.AccessoryUnitCount != null; }
            set { }
        }
    
        [XmlElement("AdditionsDescription", Order = 1)]
        public MISMOString AdditionsDescription { get; set; }
    
        [XmlIgnore]
        public bool AdditionsDescriptionSpecified
        {
            get { return this.AdditionsDescription != null; }
            set { }
        }
    
        [XmlElement("AgeYearsCount", Order = 2)]
        public MISMOCount AgeYearsCount { get; set; }
    
        [XmlIgnore]
        public bool AgeYearsCountSpecified
        {
            get { return this.AgeYearsCount != null; }
            set { }
        }
    
        [XmlElement("AtticExistsIndicator", Order = 3)]
        public MISMOIndicator AtticExistsIndicator { get; set; }
    
        [XmlIgnore]
        public bool AtticExistsIndicatorSpecified
        {
            get { return this.AtticExistsIndicator != null; }
            set { }
        }
    
        [XmlElement("BuildingCount", Order = 4)]
        public MISMOCount BuildingCount { get; set; }
    
        [XmlIgnore]
        public bool BuildingCountSpecified
        {
            get { return this.BuildingCount != null; }
            set { }
        }
    
        [XmlElement("CharacteristicsAffectMarketabilityDescription", Order = 5)]
        public MISMOString CharacteristicsAffectMarketabilityDescription { get; set; }
    
        [XmlIgnore]
        public bool CharacteristicsAffectMarketabilityDescriptionSpecified
        {
            get { return this.CharacteristicsAffectMarketabilityDescription != null; }
            set { }
        }
    
        [XmlElement("ElevatorCount", Order = 6)]
        public MISMOCount ElevatorCount { get; set; }
    
        [XmlIgnore]
        public bool ElevatorCountSpecified
        {
            get { return this.ElevatorCount != null; }
            set { }
        }
    
        [XmlElement("GrossBuildingAreaSquareFeetNumber", Order = 7)]
        public MISMONumeric GrossBuildingAreaSquareFeetNumber { get; set; }
    
        [XmlIgnore]
        public bool GrossBuildingAreaSquareFeetNumberSpecified
        {
            get { return this.GrossBuildingAreaSquareFeetNumber != null; }
            set { }
        }
    
        [XmlElement("GrossLivingAreaSquareFeetDataSourceDescription", Order = 8)]
        public MISMOString GrossLivingAreaSquareFeetDataSourceDescription { get; set; }
    
        [XmlIgnore]
        public bool GrossLivingAreaSquareFeetDataSourceDescriptionSpecified
        {
            get { return this.GrossLivingAreaSquareFeetDataSourceDescription != null; }
            set { }
        }
    
        [XmlElement("LivingUnitCount", Order = 9)]
        public MISMOCount LivingUnitCount { get; set; }
    
        [XmlIgnore]
        public bool LivingUnitCountSpecified
        {
            get { return this.LivingUnitCount != null; }
            set { }
        }
    
        [XmlElement("RentControlDescription", Order = 10)]
        public MISMOString RentControlDescription { get; set; }
    
        [XmlIgnore]
        public bool RentControlDescriptionSpecified
        {
            get { return this.RentControlDescription != null; }
            set { }
        }
    
        [XmlElement("RentControlStatusType", Order = 11)]
        public MISMOEnum<RentControlStatusBase> RentControlStatusType { get; set; }
    
        [XmlIgnore]
        public bool RentControlStatusTypeSpecified
        {
            get { return this.RentControlStatusType != null; }
            set { }
        }
    
        [XmlElement("StoriesCount", Order = 12)]
        public MISMOCount StoriesCount { get; set; }
    
        [XmlIgnore]
        public bool StoriesCountSpecified
        {
            get { return this.StoriesCount != null; }
            set { }
        }
    
        [XmlElement("StructureConditionDescription", Order = 13)]
        public MISMOString StructureConditionDescription { get; set; }
    
        [XmlIgnore]
        public bool StructureConditionDescriptionSpecified
        {
            get { return this.StructureConditionDescription != null; }
            set { }
        }
    
        [XmlElement("StructureConstructionStatusDescription", Order = 14)]
        public MISMOString StructureConstructionStatusDescription { get; set; }
    
        [XmlIgnore]
        public bool StructureConstructionStatusDescriptionSpecified
        {
            get { return this.StructureConstructionStatusDescription != null; }
            set { }
        }
    
        [XmlElement("StructureNeverOccupiedIndicator", Order = 15)]
        public MISMOIndicator StructureNeverOccupiedIndicator { get; set; }
    
        [XmlIgnore]
        public bool StructureNeverOccupiedIndicatorSpecified
        {
            get { return this.StructureNeverOccupiedIndicator != null; }
            set { }
        }
    
        [XmlElement("StructureSingleFamilyResidenceIndicator", Order = 16)]
        public MISMOIndicator StructureSingleFamilyResidenceIndicator { get; set; }
    
        [XmlIgnore]
        public bool StructureSingleFamilyResidenceIndicatorSpecified
        {
            get { return this.StructureSingleFamilyResidenceIndicator != null; }
            set { }
        }
    
        [XmlElement("StructureStateDescription", Order = 17)]
        public MISMOString StructureStateDescription { get; set; }
    
        [XmlIgnore]
        public bool StructureStateDescriptionSpecified
        {
            get { return this.StructureStateDescription != null; }
            set { }
        }
    
        [XmlElement("StructureStateType", Order = 18)]
        public MISMOEnum<StructureStateBase> StructureStateType { get; set; }
    
        [XmlIgnore]
        public bool StructureStateTypeSpecified
        {
            get { return this.StructureStateType != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 19)]
        public STRUCTURE_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
