namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class REFINANCE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CurrentFirstMortgageHolderTypeSpecified
                    || this.CurrentFirstMortgageHolderTypeOtherDescriptionSpecified
                    || this.EstimatedCashOutAmountSpecified
                    || this.NonStructuralAlterationsConventionalAmountSpecified
                    || this.RefinanceAssetsAcquiredAmountSpecified
                    || this.RefinanceCashOutAmountSpecified
                    || this.RefinanceCashOutDeterminationTypeSpecified
                    || this.RefinanceCashOutPercentSpecified
                    || this.RefinanceDebtReductionAmountSpecified
                    || this.RefinanceHomeImprovementAmountSpecified
                    || this.RefinancePrimaryPurposeTypeSpecified
                    || this.RefinancePrimaryPurposeTypeOtherDescriptionSpecified
                    || this.RefinanceSameLenderIndicatorSpecified
                    || this.SecondaryFinancingRefinanceIndicatorSpecified
                    || this.StateRefinanceProgramTypeSpecified
                    || this.StateRefinanceProgramTypeOtherDescriptionSpecified
                    || this.StructuralAlterationsConventionalAmountSpecified
                    || this.TotalPriorLienPayoffAmountSpecified
                    || this.TotalPriorLienPrepaymentPenaltyAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CurrentFirstMortgageHolderType", Order = 0)]
        public MISMOEnum<CurrentFirstMortgageHolderBase> CurrentFirstMortgageHolderType { get; set; }
    
        [XmlIgnore]
        public bool CurrentFirstMortgageHolderTypeSpecified
        {
            get { return this.CurrentFirstMortgageHolderType != null; }
            set { }
        }
    
        [XmlElement("CurrentFirstMortgageHolderTypeOtherDescription", Order = 1)]
        public MISMOString CurrentFirstMortgageHolderTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CurrentFirstMortgageHolderTypeOtherDescriptionSpecified
        {
            get { return this.CurrentFirstMortgageHolderTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EstimatedCashOutAmount", Order = 2)]
        public MISMOAmount EstimatedCashOutAmount { get; set; }
    
        [XmlIgnore]
        public bool EstimatedCashOutAmountSpecified
        {
            get { return this.EstimatedCashOutAmount != null; }
            set { }
        }
    
        [XmlElement("NonStructuralAlterationsConventionalAmount", Order = 3)]
        public MISMOAmount NonStructuralAlterationsConventionalAmount { get; set; }
    
        [XmlIgnore]
        public bool NonStructuralAlterationsConventionalAmountSpecified
        {
            get { return this.NonStructuralAlterationsConventionalAmount != null; }
            set { }
        }
    
        [XmlElement("RefinanceAssetsAcquiredAmount", Order = 4)]
        public MISMOAmount RefinanceAssetsAcquiredAmount { get; set; }
    
        [XmlIgnore]
        public bool RefinanceAssetsAcquiredAmountSpecified
        {
            get { return this.RefinanceAssetsAcquiredAmount != null; }
            set { }
        }
    
        [XmlElement("RefinanceCashOutAmount", Order = 5)]
        public MISMOAmount RefinanceCashOutAmount { get; set; }
    
        [XmlIgnore]
        public bool RefinanceCashOutAmountSpecified
        {
            get { return this.RefinanceCashOutAmount != null; }
            set { }
        }
    
        [XmlElement("RefinanceCashOutDeterminationType", Order = 6)]
        public MISMOEnum<RefinanceCashOutDeterminationBase> RefinanceCashOutDeterminationType { get; set; }
    
        [XmlIgnore]
        public bool RefinanceCashOutDeterminationTypeSpecified
        {
            get { return this.RefinanceCashOutDeterminationType != null; }
            set { }
        }
    
        [XmlElement("RefinanceCashOutPercent", Order = 7)]
        public MISMOPercent RefinanceCashOutPercent { get; set; }
    
        [XmlIgnore]
        public bool RefinanceCashOutPercentSpecified
        {
            get { return this.RefinanceCashOutPercent != null; }
            set { }
        }
    
        [XmlElement("RefinanceDebtReductionAmount", Order = 8)]
        public MISMOAmount RefinanceDebtReductionAmount { get; set; }
    
        [XmlIgnore]
        public bool RefinanceDebtReductionAmountSpecified
        {
            get { return this.RefinanceDebtReductionAmount != null; }
            set { }
        }
    
        [XmlElement("RefinanceHomeImprovementAmount", Order = 9)]
        public MISMOAmount RefinanceHomeImprovementAmount { get; set; }
    
        [XmlIgnore]
        public bool RefinanceHomeImprovementAmountSpecified
        {
            get { return this.RefinanceHomeImprovementAmount != null; }
            set { }
        }
    
        [XmlElement("RefinancePrimaryPurposeType", Order = 10)]
        public MISMOEnum<RefinancePrimaryPurposeBase> RefinancePrimaryPurposeType { get; set; }
    
        [XmlIgnore]
        public bool RefinancePrimaryPurposeTypeSpecified
        {
            get { return this.RefinancePrimaryPurposeType != null; }
            set { }
        }
    
        [XmlElement("RefinancePrimaryPurposeTypeOtherDescription", Order = 11)]
        public MISMOString RefinancePrimaryPurposeTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool RefinancePrimaryPurposeTypeOtherDescriptionSpecified
        {
            get { return this.RefinancePrimaryPurposeTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("RefinanceSameLenderIndicator", Order = 12)]
        public MISMOIndicator RefinanceSameLenderIndicator { get; set; }
    
        [XmlIgnore]
        public bool RefinanceSameLenderIndicatorSpecified
        {
            get { return this.RefinanceSameLenderIndicator != null; }
            set { }
        }
    
        [XmlElement("SecondaryFinancingRefinanceIndicator", Order = 13)]
        public MISMOIndicator SecondaryFinancingRefinanceIndicator { get; set; }
    
        [XmlIgnore]
        public bool SecondaryFinancingRefinanceIndicatorSpecified
        {
            get { return this.SecondaryFinancingRefinanceIndicator != null; }
            set { }
        }
    
        [XmlElement("StateRefinanceProgramType", Order = 14)]
        public MISMOEnum<StateRefinanceProgramBase> StateRefinanceProgramType { get; set; }
    
        [XmlIgnore]
        public bool StateRefinanceProgramTypeSpecified
        {
            get { return this.StateRefinanceProgramType != null; }
            set { }
        }
    
        [XmlElement("StateRefinanceProgramTypeOtherDescription", Order = 15)]
        public MISMOString StateRefinanceProgramTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool StateRefinanceProgramTypeOtherDescriptionSpecified
        {
            get { return this.StateRefinanceProgramTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("StructuralAlterationsConventionalAmount", Order = 16)]
        public MISMOAmount StructuralAlterationsConventionalAmount { get; set; }
    
        [XmlIgnore]
        public bool StructuralAlterationsConventionalAmountSpecified
        {
            get { return this.StructuralAlterationsConventionalAmount != null; }
            set { }
        }
    
        [XmlElement("TotalPriorLienPayoffAmount", Order = 17)]
        public MISMOAmount TotalPriorLienPayoffAmount { get; set; }
    
        [XmlIgnore]
        public bool TotalPriorLienPayoffAmountSpecified
        {
            get { return this.TotalPriorLienPayoffAmount != null; }
            set { }
        }
    
        [XmlElement("TotalPriorLienPrepaymentPenaltyAmount", Order = 18)]
        public MISMOAmount TotalPriorLienPrepaymentPenaltyAmount { get; set; }
    
        [XmlIgnore]
        public bool TotalPriorLienPrepaymentPenaltyAmountSpecified
        {
            get { return this.TotalPriorLienPrepaymentPenaltyAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 19)]
        public REFINANCE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
