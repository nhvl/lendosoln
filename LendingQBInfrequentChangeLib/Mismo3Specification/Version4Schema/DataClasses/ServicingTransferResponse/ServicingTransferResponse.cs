namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SERVICING_TRANSFER_RESPONSE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified;
            }
        }
    
        [XmlElement("EXTENSION", Order = 0)]
        public SERVICING_TRANSFER_RESPONSE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
