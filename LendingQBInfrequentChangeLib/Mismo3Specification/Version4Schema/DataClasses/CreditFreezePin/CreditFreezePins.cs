namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class CREDIT_FREEZE_PINS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditFreezePinListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CREDIT_FREEZE_PIN", Order = 0)]
        public List<CREDIT_FREEZE_PIN> CreditFreezePinList { get; set; } = new List<CREDIT_FREEZE_PIN>();
    
        [XmlIgnore]
        public bool CreditFreezePinListSpecified
        {
            get { return this.CreditFreezePinList != null && this.CreditFreezePinList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public CREDIT_FREEZE_PINS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
