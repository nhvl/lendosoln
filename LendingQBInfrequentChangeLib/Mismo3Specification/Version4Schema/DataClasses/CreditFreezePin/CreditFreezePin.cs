namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CREDIT_FREEZE_PIN
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditFreezePINValueSpecified
                    || this.CreditRepositorySourceTypeSpecified
                    || this.CreditRepositorySourceTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CreditFreezePINValue", Order = 0)]
        public MISMOValue CreditFreezePINValue { get; set; }
    
        [XmlIgnore]
        public bool CreditFreezePINValueSpecified
        {
            get { return this.CreditFreezePINValue != null; }
            set { }
        }
    
        [XmlElement("CreditRepositorySourceType", Order = 1)]
        public MISMOEnum<CreditRepositorySourceBase> CreditRepositorySourceType { get; set; }
    
        [XmlIgnore]
        public bool CreditRepositorySourceTypeSpecified
        {
            get { return this.CreditRepositorySourceType != null; }
            set { }
        }
    
        [XmlElement("CreditRepositorySourceTypeOtherDescription", Order = 2)]
        public MISMOString CreditRepositorySourceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CreditRepositorySourceTypeOtherDescriptionSpecified
        {
            get { return this.CreditRepositorySourceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public CREDIT_FREEZE_PIN_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
