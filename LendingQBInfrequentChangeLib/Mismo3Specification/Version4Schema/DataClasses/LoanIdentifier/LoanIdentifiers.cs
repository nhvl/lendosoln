namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class LOAN_IDENTIFIERS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LoanIdentifierListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LOAN_IDENTIFIER", Order = 0)]
        public List<LOAN_IDENTIFIER> LoanIdentifierList { get; set; } = new List<LOAN_IDENTIFIER>();
    
        [XmlIgnore]
        public bool LoanIdentifierListSpecified
        {
            get { return this.LoanIdentifierList != null && this.LoanIdentifierList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public LOAN_IDENTIFIERS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
