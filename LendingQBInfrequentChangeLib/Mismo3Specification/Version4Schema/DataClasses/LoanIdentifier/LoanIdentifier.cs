namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class LOAN_IDENTIFIER
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LoanIdentifierSpecified
                    || this.LoanIdentifierTypeSpecified
                    || this.LoanIdentifierTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LoanIdentifier", Order = 0)]
        public MISMOIdentifier LoanIdentifier { get; set; }
    
        [XmlIgnore]
        public bool LoanIdentifierSpecified
        {
            get { return this.LoanIdentifier != null; }
            set { }
        }
    
        [XmlElement("LoanIdentifierType", Order = 1)]
        public MISMOEnum<LoanIdentifierBase> LoanIdentifierType { get; set; }
    
        [XmlIgnore]
        public bool LoanIdentifierTypeSpecified
        {
            get { return this.LoanIdentifierType != null; }
            set { }
        }
    
        [XmlElement("LoanIdentifierTypeOtherDescription", Order = 2)]
        public MISMOString LoanIdentifierTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool LoanIdentifierTypeOtherDescriptionSpecified
        {
            get { return this.LoanIdentifierTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public LOAN_IDENTIFIER_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
