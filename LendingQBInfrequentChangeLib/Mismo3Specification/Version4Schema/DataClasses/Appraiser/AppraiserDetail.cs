namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class APPRAISER_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AppraisalSubCommitteeAppraiserIdentifierSpecified
                    || this.AppraiserCompanyNameSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AppraisalSubCommitteeAppraiserIdentifier", Order = 0)]
        public MISMOIdentifier AppraisalSubCommitteeAppraiserIdentifier { get; set; }
    
        [XmlIgnore]
        public bool AppraisalSubCommitteeAppraiserIdentifierSpecified
        {
            get { return this.AppraisalSubCommitteeAppraiserIdentifier != null; }
            set { }
        }
    
        [XmlElement("AppraiserCompanyName", Order = 1)]
        public MISMOString AppraiserCompanyName { get; set; }
    
        [XmlIgnore]
        public bool AppraiserCompanyNameSpecified
        {
            get { return this.AppraiserCompanyName != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public APPRAISER_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
