namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class APPRAISER
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AppraiserDetailSpecified
                    || this.DesignationsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("APPRAISER_DETAIL", Order = 0)]
        public APPRAISER_DETAIL AppraiserDetail { get; set; }
    
        [XmlIgnore]
        public bool AppraiserDetailSpecified
        {
            get { return this.AppraiserDetail != null && this.AppraiserDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("DESIGNATIONS", Order = 1)]
        public DESIGNATIONS Designations { get; set; }
    
        [XmlIgnore]
        public bool DesignationsSpecified
        {
            get { return this.Designations != null && this.Designations.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public APPRAISER_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
