namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SERVICE_PRODUCT_RESPONSE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ServiceProductDetailSpecified
                    || this.ServiceProductNamesSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SERVICE_PRODUCT_DETAIL", Order = 0)]
        public SERVICE_PRODUCT_DETAIL ServiceProductDetail { get; set; }
    
        [XmlIgnore]
        public bool ServiceProductDetailSpecified
        {
            get { return this.ServiceProductDetail != null && this.ServiceProductDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SERVICE_PRODUCT_NAMES", Order = 1)]
        public SERVICE_PRODUCT_NAMES ServiceProductNames { get; set; }
    
        [XmlIgnore]
        public bool ServiceProductNamesSpecified
        {
            get { return this.ServiceProductNames != null && this.ServiceProductNames.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public SERVICE_PRODUCT_RESPONSE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
