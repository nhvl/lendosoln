namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CREDIT_SCORE_FACTOR
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditScoreFactorCodeSpecified
                    || this.CreditScoreFactorTextSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CreditScoreFactorCode", Order = 0)]
        public MISMOCode CreditScoreFactorCode { get; set; }
    
        [XmlIgnore]
        public bool CreditScoreFactorCodeSpecified
        {
            get { return this.CreditScoreFactorCode != null; }
            set { }
        }
    
        [XmlElement("CreditScoreFactorText", Order = 1)]
        public MISMOString CreditScoreFactorText { get; set; }
    
        [XmlIgnore]
        public bool CreditScoreFactorTextSpecified
        {
            get { return this.CreditScoreFactorText != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public CREDIT_SCORE_FACTOR_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
