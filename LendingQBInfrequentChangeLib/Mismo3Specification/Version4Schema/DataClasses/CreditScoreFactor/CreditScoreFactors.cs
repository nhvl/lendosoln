namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class CREDIT_SCORE_FACTORS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditScoreFactorListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CREDIT_SCORE_FACTOR", Order = 0)]
        public List<CREDIT_SCORE_FACTOR> CreditScoreFactorList { get; set; } = new List<CREDIT_SCORE_FACTOR>();
    
        [XmlIgnore]
        public bool CreditScoreFactorListSpecified
        {
            get { return this.CreditScoreFactorList != null && this.CreditScoreFactorList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public CREDIT_SCORE_FACTORS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
