namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class HMDA_LOAN
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.HmdaLoanDenialsSpecified
                    || this.HmdaLoanDetailSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("HMDA_LOAN_DENIALS", Order = 0)]
        public HMDA_LOAN_DENIALS HmdaLoanDenials { get; set; }
    
        [XmlIgnore]
        public bool HmdaLoanDenialsSpecified
        {
            get { return this.HmdaLoanDenials != null && this.HmdaLoanDenials.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("HMDA_LOAN_DETAIL", Order = 1)]
        public HMDA_LOAN_DETAIL HmdaLoanDetail { get; set; }
    
        [XmlIgnore]
        public bool HmdaLoanDetailSpecified
        {
            get { return this.HmdaLoanDetail != null && this.HmdaLoanDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public HMDA_LOAN_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
