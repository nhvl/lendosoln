namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class HMDA_LOAN_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.HMDA_HOEPALoanStatusIndicatorSpecified
                    || this.HMDA_HOEPALoanStatusReasonTypeSpecified
                    || this.HMDA_HOEPALoanStatusReasonTypeOtherDescriptionSpecified
                    || this.HMDAApplicationSubmissionTypeSpecified
                    || this.HMDABusinessPurposeIndicatorSpecified
                    || this.HMDACoveredLoanInitiallyPayableToReportingInstitutionStatusTypeSpecified
                    || this.HMDADispositionDateSpecified
                    || this.HMDADispositionTypeSpecified
                    || this.HMDAManufacturedHomeLegalClassificationTypeSpecified
                    || this.HMDAMultipleCreditScoresUsedIndicatorSpecified
                    || this.HMDAOtherNonAmortizingFeaturesIndicatorSpecified
                    || this.HMDAPostalAddressUnknownTypeSpecified
                    || this.HMDAPreapprovalTypeSpecified
                    || this.HMDAPurchaserTypeSpecified
                    || this.HMDAPurchaserTypeOtherDescriptionSpecified
                    || this.HMDAPurposeOfLoanTypeSpecified
                    || this.HMDAPurposeOfLoanTypeOtherDescriptionSpecified
                    || this.HMDARateSpreadPercentSpecified
                    || this.HMDAReportingCRAExemptionIndicatorSpecified
                    || this.HMDAReportingSmallPopulationIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("HMDA_HOEPALoanStatusIndicator", Order = 0)]
        public MISMOIndicator HMDA_HOEPALoanStatusIndicator { get; set; }
    
        [XmlIgnore]
        public bool HMDA_HOEPALoanStatusIndicatorSpecified
        {
            get { return this.HMDA_HOEPALoanStatusIndicator != null; }
            set { }
        }
    
        [XmlElement("HMDA_HOEPALoanStatusReasonType", Order = 1)]
        public MISMOEnum<HMDA_HOEPALoanStatusReasonBase> HMDA_HOEPALoanStatusReasonType { get; set; }
    
        [XmlIgnore]
        public bool HMDA_HOEPALoanStatusReasonTypeSpecified
        {
            get { return this.HMDA_HOEPALoanStatusReasonType != null; }
            set { }
        }
    
        [XmlElement("HMDA_HOEPALoanStatusReasonTypeOtherDescription", Order = 2)]
        public MISMOString HMDA_HOEPALoanStatusReasonTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool HMDA_HOEPALoanStatusReasonTypeOtherDescriptionSpecified
        {
            get { return this.HMDA_HOEPALoanStatusReasonTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("HMDAApplicationSubmissionType", Order = 3)]
        public MISMOEnum<HMDAApplicationSubmissionBase> HMDAApplicationSubmissionType { get; set; }
    
        [XmlIgnore]
        public bool HMDAApplicationSubmissionTypeSpecified
        {
            get { return this.HMDAApplicationSubmissionType != null; }
            set { }
        }
    
        [XmlElement("HMDABusinessPurposeIndicator", Order = 4)]
        public MISMOIndicator HMDABusinessPurposeIndicator { get; set; }
    
        [XmlIgnore]
        public bool HMDABusinessPurposeIndicatorSpecified
        {
            get { return this.HMDABusinessPurposeIndicator != null; }
            set { }
        }
    
        [XmlElement("HMDACoveredLoanInitiallyPayableToReportingInstitutionStatusType", Order = 5)]
        public MISMOEnum<HMDACoveredLoanInitiallyPayableToReportingInstitutionStatusBase> HMDACoveredLoanInitiallyPayableToReportingInstitutionStatusType { get; set; }
    
        [XmlIgnore]
        public bool HMDACoveredLoanInitiallyPayableToReportingInstitutionStatusTypeSpecified
        {
            get { return this.HMDACoveredLoanInitiallyPayableToReportingInstitutionStatusType != null; }
            set { }
        }
    
        [XmlElement("HMDADispositionDate", Order = 6)]
        public MISMODate HMDADispositionDate { get; set; }
    
        [XmlIgnore]
        public bool HMDADispositionDateSpecified
        {
            get { return this.HMDADispositionDate != null; }
            set { }
        }
    
        [XmlElement("HMDADispositionType", Order = 7)]
        public MISMOEnum<HMDADispositionBase> HMDADispositionType { get; set; }
    
        [XmlIgnore]
        public bool HMDADispositionTypeSpecified
        {
            get { return this.HMDADispositionType != null; }
            set { }
        }
    
        [XmlElement("HMDAManufacturedHomeLegalClassificationType", Order = 8)]
        public MISMOEnum<HMDAManufacturedHomeLegalClassificationBase> HMDAManufacturedHomeLegalClassificationType { get; set; }
    
        [XmlIgnore]
        public bool HMDAManufacturedHomeLegalClassificationTypeSpecified
        {
            get { return this.HMDAManufacturedHomeLegalClassificationType != null; }
            set { }
        }
    
        [XmlElement("HMDAMultipleCreditScoresUsedIndicator", Order = 9)]
        public MISMOIndicator HMDAMultipleCreditScoresUsedIndicator { get; set; }
    
        [XmlIgnore]
        public bool HMDAMultipleCreditScoresUsedIndicatorSpecified
        {
            get { return this.HMDAMultipleCreditScoresUsedIndicator != null; }
            set { }
        }
    
        [XmlElement("HMDAOtherNonAmortizingFeaturesIndicator", Order = 10)]
        public MISMOIndicator HMDAOtherNonAmortizingFeaturesIndicator { get; set; }
    
        [XmlIgnore]
        public bool HMDAOtherNonAmortizingFeaturesIndicatorSpecified
        {
            get { return this.HMDAOtherNonAmortizingFeaturesIndicator != null; }
            set { }
        }
    
        [XmlElement("HMDAPostalAddressUnknownType", Order = 11)]
        public MISMOEnum<HMDAPostalAddressUnknownBase> HMDAPostalAddressUnknownType { get; set; }
    
        [XmlIgnore]
        public bool HMDAPostalAddressUnknownTypeSpecified
        {
            get { return this.HMDAPostalAddressUnknownType != null; }
            set { }
        }
    
        [XmlElement("HMDAPreapprovalType", Order = 12)]
        public MISMOEnum<HMDAPreapprovalBase> HMDAPreapprovalType { get; set; }
    
        [XmlIgnore]
        public bool HMDAPreapprovalTypeSpecified
        {
            get { return this.HMDAPreapprovalType != null; }
            set { }
        }
    
        [XmlElement("HMDAPurchaserType", Order = 13)]
        public MISMOEnum<HMDAPurchaserBase> HMDAPurchaserType { get; set; }
    
        [XmlIgnore]
        public bool HMDAPurchaserTypeSpecified
        {
            get { return this.HMDAPurchaserType != null; }
            set { }
        }
    
        [XmlElement("HMDAPurchaserTypeOtherDescription", Order = 14)]
        public MISMOString HMDAPurchaserTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool HMDAPurchaserTypeOtherDescriptionSpecified
        {
            get { return this.HMDAPurchaserTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("HMDAPurposeOfLoanType", Order = 15)]
        public MISMOEnum<HMDAPurposeOfLoanBase> HMDAPurposeOfLoanType { get; set; }
    
        [XmlIgnore]
        public bool HMDAPurposeOfLoanTypeSpecified
        {
            get { return this.HMDAPurposeOfLoanType != null; }
            set { }
        }
    
        [XmlElement("HMDAPurposeOfLoanTypeOtherDescription", Order = 16)]
        public MISMOString HMDAPurposeOfLoanTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool HMDAPurposeOfLoanTypeOtherDescriptionSpecified
        {
            get { return this.HMDAPurposeOfLoanTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("HMDARateSpreadPercent", Order = 17)]
        public MISMOPercent HMDARateSpreadPercent { get; set; }
    
        [XmlIgnore]
        public bool HMDARateSpreadPercentSpecified
        {
            get { return this.HMDARateSpreadPercent != null; }
            set { }
        }
    
        [XmlElement("HMDAReportingCRAExemptionIndicator", Order = 18)]
        public MISMOIndicator HMDAReportingCRAExemptionIndicator { get; set; }
    
        [XmlIgnore]
        public bool HMDAReportingCRAExemptionIndicatorSpecified
        {
            get { return this.HMDAReportingCRAExemptionIndicator != null; }
            set { }
        }
    
        [XmlElement("HMDAReportingSmallPopulationIndicator", Order = 19)]
        public MISMOIndicator HMDAReportingSmallPopulationIndicator { get; set; }
    
        [XmlIgnore]
        public bool HMDAReportingSmallPopulationIndicatorSpecified
        {
            get { return this.HMDAReportingSmallPopulationIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 20)]
        public HMDA_LOAN_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
