namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class JUDGMENTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.JudgmentListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("JUDGMENT", Order = 0)]
        public List<JUDGMENT> JudgmentList { get; set; } = new List<JUDGMENT>();
    
        [XmlIgnore]
        public bool JudgmentListSpecified
        {
            get { return this.JudgmentList != null && this.JudgmentList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public JUDGMENTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
