namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class JUDGMENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.JudgmentAppealedIndicatorSpecified
                    || this.JudgmentDateSpecified
                    || this.JudgmentEnforcementNumberOfDaysCountSpecified
                    || this.JudgmentInFavorOfPlaintiffIndicatorSpecified
                    || this.JudgmentInWholeIndicatorSpecified
                    || this.JudgmentResultTypeSpecified
                    || this.JudgmentTypeSpecified
                    || this.JudgmentTypeAdditionalDescriptionSpecified
                    || this.JudgmentTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("JudgmentAppealedIndicator", Order = 0)]
        public MISMOIndicator JudgmentAppealedIndicator { get; set; }
    
        [XmlIgnore]
        public bool JudgmentAppealedIndicatorSpecified
        {
            get { return this.JudgmentAppealedIndicator != null; }
            set { }
        }
    
        [XmlElement("JudgmentDate", Order = 1)]
        public MISMODate JudgmentDate { get; set; }
    
        [XmlIgnore]
        public bool JudgmentDateSpecified
        {
            get { return this.JudgmentDate != null; }
            set { }
        }
    
        [XmlElement("JudgmentEnforcementNumberOfDaysCount", Order = 2)]
        public MISMOCount JudgmentEnforcementNumberOfDaysCount { get; set; }
    
        [XmlIgnore]
        public bool JudgmentEnforcementNumberOfDaysCountSpecified
        {
            get { return this.JudgmentEnforcementNumberOfDaysCount != null; }
            set { }
        }
    
        [XmlElement("JudgmentInFavorOfPlaintiffIndicator", Order = 3)]
        public MISMOIndicator JudgmentInFavorOfPlaintiffIndicator { get; set; }
    
        [XmlIgnore]
        public bool JudgmentInFavorOfPlaintiffIndicatorSpecified
        {
            get { return this.JudgmentInFavorOfPlaintiffIndicator != null; }
            set { }
        }
    
        [XmlElement("JudgmentInWholeIndicator", Order = 4)]
        public MISMOIndicator JudgmentInWholeIndicator { get; set; }
    
        [XmlIgnore]
        public bool JudgmentInWholeIndicatorSpecified
        {
            get { return this.JudgmentInWholeIndicator != null; }
            set { }
        }
    
        [XmlElement("JudgmentResultType", Order = 5)]
        public MISMOEnum<JudgmentResultBase> JudgmentResultType { get; set; }
    
        [XmlIgnore]
        public bool JudgmentResultTypeSpecified
        {
            get { return this.JudgmentResultType != null; }
            set { }
        }
    
        [XmlElement("JudgmentType", Order = 6)]
        public MISMOEnum<JudgmentBase> JudgmentType { get; set; }
    
        [XmlIgnore]
        public bool JudgmentTypeSpecified
        {
            get { return this.JudgmentType != null; }
            set { }
        }
    
        [XmlElement("JudgmentTypeAdditionalDescription", Order = 7)]
        public MISMOString JudgmentTypeAdditionalDescription { get; set; }
    
        [XmlIgnore]
        public bool JudgmentTypeAdditionalDescriptionSpecified
        {
            get { return this.JudgmentTypeAdditionalDescription != null; }
            set { }
        }
    
        [XmlElement("JudgmentTypeOtherDescription", Order = 8)]
        public MISMOString JudgmentTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool JudgmentTypeOtherDescriptionSpecified
        {
            get { return this.JudgmentTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 9)]
        public JUDGMENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
