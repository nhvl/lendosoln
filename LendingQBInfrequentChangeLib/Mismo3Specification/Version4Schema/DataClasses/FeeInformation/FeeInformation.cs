namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class FEE_INFORMATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.FeesSpecified
                    || this.FeesSummarySpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("FEES", Order = 0)]
        public FEES Fees { get; set; }
    
        [XmlIgnore]
        public bool FeesSpecified
        {
            get { return this.Fees != null && this.Fees.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("FEES_SUMMARY", Order = 1)]
        public FEES_SUMMARY FeesSummary { get; set; }
    
        [XmlIgnore]
        public bool FeesSummarySpecified
        {
            get { return this.FeesSummary != null && this.FeesSummary.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public FEE_INFORMATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
