namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class FORECLOSURE_EXPENSE_SUMMARY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.TotalForeclosureExpenseAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("TotalForeclosureExpenseAmount", Order = 0)]
        public MISMOAmount TotalForeclosureExpenseAmount { get; set; }
    
        [XmlIgnore]
        public bool TotalForeclosureExpenseAmountSpecified
        {
            get { return this.TotalForeclosureExpenseAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public FORECLOSURE_EXPENSE_SUMMARY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
