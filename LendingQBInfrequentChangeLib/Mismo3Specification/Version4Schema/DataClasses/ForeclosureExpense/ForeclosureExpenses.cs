namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class FORECLOSURE_EXPENSES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ForeclosureExpenseListSpecified
                    || this.ForeclosureExpenseSummarySpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("FORECLOSURE_EXPENSE", Order = 0)]
        public List<FORECLOSURE_EXPENSE> ForeclosureExpenseList { get; set; } = new List<FORECLOSURE_EXPENSE>();
    
        [XmlIgnore]
        public bool ForeclosureExpenseListSpecified
        {
            get { return this.ForeclosureExpenseList != null && this.ForeclosureExpenseList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("FORECLOSURE_EXPENSE_SUMMARY", Order = 1)]
        public FORECLOSURE_EXPENSE_SUMMARY ForeclosureExpenseSummary { get; set; }
    
        [XmlIgnore]
        public bool ForeclosureExpenseSummarySpecified
        {
            get { return this.ForeclosureExpenseSummary != null && this.ForeclosureExpenseSummary.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public FORECLOSURE_EXPENSES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
