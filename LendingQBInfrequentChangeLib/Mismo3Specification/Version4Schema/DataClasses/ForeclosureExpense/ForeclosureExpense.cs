namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class FORECLOSURE_EXPENSE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ForeclosureExpenseAmountSpecified
                    || this.ForeclosureExpenseTypeSpecified
                    || this.ForeclosureExpenseTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ForeclosureExpenseAmount", Order = 0)]
        public MISMOAmount ForeclosureExpenseAmount { get; set; }
    
        [XmlIgnore]
        public bool ForeclosureExpenseAmountSpecified
        {
            get { return this.ForeclosureExpenseAmount != null; }
            set { }
        }
    
        [XmlElement("ForeclosureExpenseType", Order = 1)]
        public MISMOEnum<ForeclosureExpenseBase> ForeclosureExpenseType { get; set; }
    
        [XmlIgnore]
        public bool ForeclosureExpenseTypeSpecified
        {
            get { return this.ForeclosureExpenseType != null; }
            set { }
        }
    
        [XmlElement("ForeclosureExpenseTypeOtherDescription", Order = 2)]
        public MISMOString ForeclosureExpenseTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ForeclosureExpenseTypeOtherDescriptionSpecified
        {
            get { return this.ForeclosureExpenseTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public FORECLOSURE_EXPENSE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
