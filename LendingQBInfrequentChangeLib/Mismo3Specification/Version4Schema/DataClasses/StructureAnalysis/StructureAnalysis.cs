namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class STRUCTURE_ANALYSIS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.StructureAnalysisDetailSpecified
                    || this.StructureAnalysisRatingsSpecified
                    || this.StructureAnalysisSummarySpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("STRUCTURE_ANALYSIS_DETAIL", Order = 0)]
        public STRUCTURE_ANALYSIS_DETAIL StructureAnalysisDetail { get; set; }
    
        [XmlIgnore]
        public bool StructureAnalysisDetailSpecified
        {
            get { return this.StructureAnalysisDetail != null && this.StructureAnalysisDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("STRUCTURE_ANALYSIS_RATINGS", Order = 1)]
        public STRUCTURE_ANALYSIS_RATINGS StructureAnalysisRatings { get; set; }
    
        [XmlIgnore]
        public bool StructureAnalysisRatingsSpecified
        {
            get { return this.StructureAnalysisRatings != null && this.StructureAnalysisRatings.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("STRUCTURE_ANALYSIS_SUMMARY", Order = 2)]
        public STRUCTURE_ANALYSIS_SUMMARY StructureAnalysisSummary { get; set; }
    
        [XmlIgnore]
        public bool StructureAnalysisSummarySpecified
        {
            get { return this.StructureAnalysisSummary != null && this.StructureAnalysisSummary.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public STRUCTURE_ANALYSIS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
