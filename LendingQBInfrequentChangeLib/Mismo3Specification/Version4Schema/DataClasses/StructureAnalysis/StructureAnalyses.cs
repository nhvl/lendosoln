namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class STRUCTURE_ANALYSES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.StructureAnalysisListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("STRUCTURE_ANALYSIS", Order = 0)]
        public List<STRUCTURE_ANALYSIS> StructureAnalysisList { get; set; } = new List<STRUCTURE_ANALYSIS>();
    
        [XmlIgnore]
        public bool StructureAnalysisListSpecified
        {
            get { return this.StructureAnalysisList != null && this.StructureAnalysisList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public STRUCTURE_ANALYSES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
