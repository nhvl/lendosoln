namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class STRUCTURE_ANALYSIS_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AnalysisComponentActualCostAmountSpecified
                    || this.AnalysisComponentEstimatedCostAmountSpecified
                    || this.AnalysisComponentTypeSpecified
                    || this.AnalysisComponentTypeOtherDescriptionSpecified
                    || this.ComponentStatusTimeframeTypeSpecified
                    || this.ComponentStatusTimeframeTypeOtherDescriptionSpecified
                    || this.ComponentStatusTypeSpecified
                    || this.ComponentStatusTypeOtherDescriptionSpecified
                    || this.CostOfAnalysisComponentStatusTypeSpecified
                    || this.EffectiveAgeRangeHighYearsCountSpecified
                    || this.EffectiveAgeRangeLowYearsCountSpecified
                    || this.EffectiveAgeYearsCountSpecified
                    || this.MaterialUpdateAfterOriginalConstructionIndicatorSpecified
                    || this.MaterialUpdateTimeFrameRangeHighYearsCountSpecified
                    || this.MaterialUpdateTimeFrameRangeLowYearsCountSpecified
                    || this.OverallConditionRatingIdentifierSpecified
                    || this.OverallQualityRatingIdentifierSpecified
                    || this.PhysicalDeficiencyIndicatorSpecified
                    || this.PropertyCondemnedDateSpecified
                    || this.PropertyDamageNotificationDateSpecified
                    || this.RemainingEconomicLifeRangeHighYearsCountSpecified
                    || this.RemainingEconomicLifeRangeLowYearsCountSpecified
                    || this.StructureAppealDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AnalysisComponentActualCostAmount", Order = 0)]
        public MISMOAmount AnalysisComponentActualCostAmount { get; set; }
    
        [XmlIgnore]
        public bool AnalysisComponentActualCostAmountSpecified
        {
            get { return this.AnalysisComponentActualCostAmount != null; }
            set { }
        }
    
        [XmlElement("AnalysisComponentEstimatedCostAmount", Order = 1)]
        public MISMOAmount AnalysisComponentEstimatedCostAmount { get; set; }
    
        [XmlIgnore]
        public bool AnalysisComponentEstimatedCostAmountSpecified
        {
            get { return this.AnalysisComponentEstimatedCostAmount != null; }
            set { }
        }
    
        [XmlElement("AnalysisComponentType", Order = 2)]
        public MISMOEnum<AnalysisComponentBase> AnalysisComponentType { get; set; }
    
        [XmlIgnore]
        public bool AnalysisComponentTypeSpecified
        {
            get { return this.AnalysisComponentType != null; }
            set { }
        }
    
        [XmlElement("AnalysisComponentTypeOtherDescription", Order = 3)]
        public MISMOString AnalysisComponentTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool AnalysisComponentTypeOtherDescriptionSpecified
        {
            get { return this.AnalysisComponentTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ComponentStatusTimeframeType", Order = 4)]
        public MISMOEnum<ComponentStatusTimeframeBase> ComponentStatusTimeframeType { get; set; }
    
        [XmlIgnore]
        public bool ComponentStatusTimeframeTypeSpecified
        {
            get { return this.ComponentStatusTimeframeType != null; }
            set { }
        }
    
        [XmlElement("ComponentStatusTimeframeTypeOtherDescription", Order = 5)]
        public MISMOString ComponentStatusTimeframeTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ComponentStatusTimeframeTypeOtherDescriptionSpecified
        {
            get { return this.ComponentStatusTimeframeTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ComponentStatusType", Order = 6)]
        public MISMOEnum<ComponentStatusBase> ComponentStatusType { get; set; }
    
        [XmlIgnore]
        public bool ComponentStatusTypeSpecified
        {
            get { return this.ComponentStatusType != null; }
            set { }
        }
    
        [XmlElement("ComponentStatusTypeOtherDescription", Order = 7)]
        public MISMOString ComponentStatusTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ComponentStatusTypeOtherDescriptionSpecified
        {
            get { return this.ComponentStatusTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("CostOfAnalysisComponentStatusType", Order = 8)]
        public MISMOEnum<CostOfAnalysisComponentStatusBase> CostOfAnalysisComponentStatusType { get; set; }
    
        [XmlIgnore]
        public bool CostOfAnalysisComponentStatusTypeSpecified
        {
            get { return this.CostOfAnalysisComponentStatusType != null; }
            set { }
        }
    
        [XmlElement("EffectiveAgeRangeHighYearsCount", Order = 9)]
        public MISMOCount EffectiveAgeRangeHighYearsCount { get; set; }
    
        [XmlIgnore]
        public bool EffectiveAgeRangeHighYearsCountSpecified
        {
            get { return this.EffectiveAgeRangeHighYearsCount != null; }
            set { }
        }
    
        [XmlElement("EffectiveAgeRangeLowYearsCount", Order = 10)]
        public MISMOCount EffectiveAgeRangeLowYearsCount { get; set; }
    
        [XmlIgnore]
        public bool EffectiveAgeRangeLowYearsCountSpecified
        {
            get { return this.EffectiveAgeRangeLowYearsCount != null; }
            set { }
        }
    
        [XmlElement("EffectiveAgeYearsCount", Order = 11)]
        public MISMOCount EffectiveAgeYearsCount { get; set; }
    
        [XmlIgnore]
        public bool EffectiveAgeYearsCountSpecified
        {
            get { return this.EffectiveAgeYearsCount != null; }
            set { }
        }
    
        [XmlElement("MaterialUpdateAfterOriginalConstructionIndicator", Order = 12)]
        public MISMOIndicator MaterialUpdateAfterOriginalConstructionIndicator { get; set; }
    
        [XmlIgnore]
        public bool MaterialUpdateAfterOriginalConstructionIndicatorSpecified
        {
            get { return this.MaterialUpdateAfterOriginalConstructionIndicator != null; }
            set { }
        }
    
        [XmlElement("MaterialUpdateTimeFrameRangeHighYearsCount", Order = 13)]
        public MISMOCount MaterialUpdateTimeFrameRangeHighYearsCount { get; set; }
    
        [XmlIgnore]
        public bool MaterialUpdateTimeFrameRangeHighYearsCountSpecified
        {
            get { return this.MaterialUpdateTimeFrameRangeHighYearsCount != null; }
            set { }
        }
    
        [XmlElement("MaterialUpdateTimeFrameRangeLowYearsCount", Order = 14)]
        public MISMOCount MaterialUpdateTimeFrameRangeLowYearsCount { get; set; }
    
        [XmlIgnore]
        public bool MaterialUpdateTimeFrameRangeLowYearsCountSpecified
        {
            get { return this.MaterialUpdateTimeFrameRangeLowYearsCount != null; }
            set { }
        }
    
        [XmlElement("OverallConditionRatingIdentifier", Order = 15)]
        public MISMOIdentifier OverallConditionRatingIdentifier { get; set; }
    
        [XmlIgnore]
        public bool OverallConditionRatingIdentifierSpecified
        {
            get { return this.OverallConditionRatingIdentifier != null; }
            set { }
        }
    
        [XmlElement("OverallQualityRatingIdentifier", Order = 16)]
        public MISMOIdentifier OverallQualityRatingIdentifier { get; set; }
    
        [XmlIgnore]
        public bool OverallQualityRatingIdentifierSpecified
        {
            get { return this.OverallQualityRatingIdentifier != null; }
            set { }
        }
    
        [XmlElement("PhysicalDeficiencyIndicator", Order = 17)]
        public MISMOIndicator PhysicalDeficiencyIndicator { get; set; }
    
        [XmlIgnore]
        public bool PhysicalDeficiencyIndicatorSpecified
        {
            get { return this.PhysicalDeficiencyIndicator != null; }
            set { }
        }
    
        [XmlElement("PropertyCondemnedDate", Order = 18)]
        public MISMODate PropertyCondemnedDate { get; set; }
    
        [XmlIgnore]
        public bool PropertyCondemnedDateSpecified
        {
            get { return this.PropertyCondemnedDate != null; }
            set { }
        }
    
        [XmlElement("PropertyDamageNotificationDate", Order = 19)]
        public MISMODate PropertyDamageNotificationDate { get; set; }
    
        [XmlIgnore]
        public bool PropertyDamageNotificationDateSpecified
        {
            get { return this.PropertyDamageNotificationDate != null; }
            set { }
        }
    
        [XmlElement("RemainingEconomicLifeRangeHighYearsCount", Order = 20)]
        public MISMOCount RemainingEconomicLifeRangeHighYearsCount { get; set; }
    
        [XmlIgnore]
        public bool RemainingEconomicLifeRangeHighYearsCountSpecified
        {
            get { return this.RemainingEconomicLifeRangeHighYearsCount != null; }
            set { }
        }
    
        [XmlElement("RemainingEconomicLifeRangeLowYearsCount", Order = 21)]
        public MISMOCount RemainingEconomicLifeRangeLowYearsCount { get; set; }
    
        [XmlIgnore]
        public bool RemainingEconomicLifeRangeLowYearsCountSpecified
        {
            get { return this.RemainingEconomicLifeRangeLowYearsCount != null; }
            set { }
        }
    
        [XmlElement("StructureAppealDescription", Order = 22)]
        public MISMOString StructureAppealDescription { get; set; }
    
        [XmlIgnore]
        public bool StructureAppealDescriptionSpecified
        {
            get { return this.StructureAppealDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 23)]
        public STRUCTURE_ANALYSIS_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
