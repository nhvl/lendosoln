namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class STRUCTURE_ANALYSIS_SUMMARY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AnalysisComponentActualTotalCostAmountSpecified
                    || this.AnalysisComponentEstimatedTotalCostAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AnalysisComponentActualTotalCostAmount", Order = 0)]
        public MISMOAmount AnalysisComponentActualTotalCostAmount { get; set; }
    
        [XmlIgnore]
        public bool AnalysisComponentActualTotalCostAmountSpecified
        {
            get { return this.AnalysisComponentActualTotalCostAmount != null; }
            set { }
        }
    
        [XmlElement("AnalysisComponentEstimatedTotalCostAmount", Order = 1)]
        public MISMOAmount AnalysisComponentEstimatedTotalCostAmount { get; set; }
    
        [XmlIgnore]
        public bool AnalysisComponentEstimatedTotalCostAmountSpecified
        {
            get { return this.AnalysisComponentEstimatedTotalCostAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public STRUCTURE_ANALYSIS_SUMMARY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
