namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class SALES_CONTRACTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SalesContractListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SALES_CONTRACT", Order = 0)]
        public List<SALES_CONTRACT> SalesContractList { get; set; } = new List<SALES_CONTRACT>();
    
        [XmlIgnore]
        public bool SalesContractListSpecified
        {
            get { return this.SalesContractList != null && this.SalesContractList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public SALES_CONTRACTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
