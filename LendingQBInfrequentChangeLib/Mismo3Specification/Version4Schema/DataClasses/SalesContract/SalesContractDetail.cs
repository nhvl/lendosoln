namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SALES_CONTRACT_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ArmsLengthIndicatorSpecified
                    || this.BuyerSellerSameAgentIndicatorSpecified
                    || this.ExcessiveFinancingConcesssionsAmountSpecified
                    || this.NetSaleProceedsAmountSpecified
                    || this.PersonalPropertyAmountSpecified
                    || this.PersonalPropertyIncludedIndicatorSpecified
                    || this.PreForeclosureSaleClosingDateSpecified
                    || this.PreForeclosureSaleIndicatorSpecified
                    || this.ProjectedClosingDateSpecified
                    || this.ProposedFinancingDescriptionSpecified
                    || this.RealEstateAgentCountSpecified
                    || this.RealPropertyAmountSpecified
                    || this.SalesConcessionDescriptionSpecified
                    || this.SalesConcessionIndicatorSpecified
                    || this.SalesConditionsOfSaleDescriptionSpecified
                    || this.SalesContractAcceptedIndicatorSpecified
                    || this.SalesContractAmountSpecified
                    || this.SalesContractAnalysisDescriptionSpecified
                    || this.SalesContractDateSpecified
                    || this.SalesContractInvoiceRevewCommentDescriptionSpecified
                    || this.SalesContractInvoiceReviewedIndicatorSpecified
                    || this.SalesContractRetailerNameSpecified
                    || this.SalesContractReviewCommentDescriptionSpecified
                    || this.SalesContractReviewDescriptionSpecified
                    || this.SalesContractReviewedIndicatorSpecified
                    || this.SalesContractTermsConsistentWithMarketIndicatorSpecified
                    || this.SaleTypeSpecified
                    || this.SaleTypeOtherDescriptionSpecified
                    || this.SellerIsOwnerIndicatorSpecified
                    || this.TotalSalesConcessionAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ArmsLengthIndicator", Order = 0)]
        public MISMOIndicator ArmsLengthIndicator { get; set; }
    
        [XmlIgnore]
        public bool ArmsLengthIndicatorSpecified
        {
            get { return this.ArmsLengthIndicator != null; }
            set { }
        }
    
        [XmlElement("BuyerSellerSameAgentIndicator", Order = 1)]
        public MISMOIndicator BuyerSellerSameAgentIndicator { get; set; }
    
        [XmlIgnore]
        public bool BuyerSellerSameAgentIndicatorSpecified
        {
            get { return this.BuyerSellerSameAgentIndicator != null; }
            set { }
        }
    
        [XmlElement("ExcessiveFinancingConcesssionsAmount", Order = 2)]
        public MISMOAmount ExcessiveFinancingConcesssionsAmount { get; set; }
    
        [XmlIgnore]
        public bool ExcessiveFinancingConcesssionsAmountSpecified
        {
            get { return this.ExcessiveFinancingConcesssionsAmount != null; }
            set { }
        }
    
        [XmlElement("NetSaleProceedsAmount", Order = 3)]
        public MISMOAmount NetSaleProceedsAmount { get; set; }
    
        [XmlIgnore]
        public bool NetSaleProceedsAmountSpecified
        {
            get { return this.NetSaleProceedsAmount != null; }
            set { }
        }
    
        [XmlElement("PersonalPropertyAmount", Order = 4)]
        public MISMOAmount PersonalPropertyAmount { get; set; }
    
        [XmlIgnore]
        public bool PersonalPropertyAmountSpecified
        {
            get { return this.PersonalPropertyAmount != null; }
            set { }
        }
    
        [XmlElement("PersonalPropertyIncludedIndicator", Order = 5)]
        public MISMOIndicator PersonalPropertyIncludedIndicator { get; set; }
    
        [XmlIgnore]
        public bool PersonalPropertyIncludedIndicatorSpecified
        {
            get { return this.PersonalPropertyIncludedIndicator != null; }
            set { }
        }
    
        [XmlElement("PreForeclosureSaleClosingDate", Order = 6)]
        public MISMODate PreForeclosureSaleClosingDate { get; set; }
    
        [XmlIgnore]
        public bool PreForeclosureSaleClosingDateSpecified
        {
            get { return this.PreForeclosureSaleClosingDate != null; }
            set { }
        }
    
        [XmlElement("PreForeclosureSaleIndicator", Order = 7)]
        public MISMOIndicator PreForeclosureSaleIndicator { get; set; }
    
        [XmlIgnore]
        public bool PreForeclosureSaleIndicatorSpecified
        {
            get { return this.PreForeclosureSaleIndicator != null; }
            set { }
        }
    
        [XmlElement("ProjectedClosingDate", Order = 8)]
        public MISMODate ProjectedClosingDate { get; set; }
    
        [XmlIgnore]
        public bool ProjectedClosingDateSpecified
        {
            get { return this.ProjectedClosingDate != null; }
            set { }
        }
    
        [XmlElement("ProposedFinancingDescription", Order = 9)]
        public MISMOString ProposedFinancingDescription { get; set; }
    
        [XmlIgnore]
        public bool ProposedFinancingDescriptionSpecified
        {
            get { return this.ProposedFinancingDescription != null; }
            set { }
        }
    
        [XmlElement("RealEstateAgentCount", Order = 10)]
        public MISMOCount RealEstateAgentCount { get; set; }
    
        [XmlIgnore]
        public bool RealEstateAgentCountSpecified
        {
            get { return this.RealEstateAgentCount != null; }
            set { }
        }
    
        [XmlElement("RealPropertyAmount", Order = 11)]
        public MISMOAmount RealPropertyAmount { get; set; }
    
        [XmlIgnore]
        public bool RealPropertyAmountSpecified
        {
            get { return this.RealPropertyAmount != null; }
            set { }
        }
    
        [XmlElement("SalesConcessionDescription", Order = 12)]
        public MISMOString SalesConcessionDescription { get; set; }
    
        [XmlIgnore]
        public bool SalesConcessionDescriptionSpecified
        {
            get { return this.SalesConcessionDescription != null; }
            set { }
        }
    
        [XmlElement("SalesConcessionIndicator", Order = 13)]
        public MISMOIndicator SalesConcessionIndicator { get; set; }
    
        [XmlIgnore]
        public bool SalesConcessionIndicatorSpecified
        {
            get { return this.SalesConcessionIndicator != null; }
            set { }
        }
    
        [XmlElement("SalesConditionsOfSaleDescription", Order = 14)]
        public MISMOString SalesConditionsOfSaleDescription { get; set; }
    
        [XmlIgnore]
        public bool SalesConditionsOfSaleDescriptionSpecified
        {
            get { return this.SalesConditionsOfSaleDescription != null; }
            set { }
        }
    
        [XmlElement("SalesContractAcceptedIndicator", Order = 15)]
        public MISMOIndicator SalesContractAcceptedIndicator { get; set; }
    
        [XmlIgnore]
        public bool SalesContractAcceptedIndicatorSpecified
        {
            get { return this.SalesContractAcceptedIndicator != null; }
            set { }
        }
    
        [XmlElement("SalesContractAmount", Order = 16)]
        public MISMOAmount SalesContractAmount { get; set; }
    
        [XmlIgnore]
        public bool SalesContractAmountSpecified
        {
            get { return this.SalesContractAmount != null; }
            set { }
        }
    
        [XmlElement("SalesContractAnalysisDescription", Order = 17)]
        public MISMOString SalesContractAnalysisDescription { get; set; }
    
        [XmlIgnore]
        public bool SalesContractAnalysisDescriptionSpecified
        {
            get { return this.SalesContractAnalysisDescription != null; }
            set { }
        }
    
        [XmlElement("SalesContractDate", Order = 18)]
        public MISMODate SalesContractDate { get; set; }
    
        [XmlIgnore]
        public bool SalesContractDateSpecified
        {
            get { return this.SalesContractDate != null; }
            set { }
        }
    
        [XmlElement("SalesContractInvoiceRevewCommentDescription", Order = 19)]
        public MISMOString SalesContractInvoiceRevewCommentDescription { get; set; }
    
        [XmlIgnore]
        public bool SalesContractInvoiceRevewCommentDescriptionSpecified
        {
            get { return this.SalesContractInvoiceRevewCommentDescription != null; }
            set { }
        }
    
        [XmlElement("SalesContractInvoiceReviewedIndicator", Order = 20)]
        public MISMOIndicator SalesContractInvoiceReviewedIndicator { get; set; }
    
        [XmlIgnore]
        public bool SalesContractInvoiceReviewedIndicatorSpecified
        {
            get { return this.SalesContractInvoiceReviewedIndicator != null; }
            set { }
        }
    
        [XmlElement("SalesContractRetailerName", Order = 21)]
        public MISMOString SalesContractRetailerName { get; set; }
    
        [XmlIgnore]
        public bool SalesContractRetailerNameSpecified
        {
            get { return this.SalesContractRetailerName != null; }
            set { }
        }
    
        [XmlElement("SalesContractReviewCommentDescription", Order = 22)]
        public MISMOString SalesContractReviewCommentDescription { get; set; }
    
        [XmlIgnore]
        public bool SalesContractReviewCommentDescriptionSpecified
        {
            get { return this.SalesContractReviewCommentDescription != null; }
            set { }
        }
    
        [XmlElement("SalesContractReviewDescription", Order = 23)]
        public MISMOString SalesContractReviewDescription { get; set; }
    
        [XmlIgnore]
        public bool SalesContractReviewDescriptionSpecified
        {
            get { return this.SalesContractReviewDescription != null; }
            set { }
        }
    
        [XmlElement("SalesContractReviewedIndicator", Order = 24)]
        public MISMOIndicator SalesContractReviewedIndicator { get; set; }
    
        [XmlIgnore]
        public bool SalesContractReviewedIndicatorSpecified
        {
            get { return this.SalesContractReviewedIndicator != null; }
            set { }
        }
    
        [XmlElement("SalesContractTermsConsistentWithMarketIndicator", Order = 25)]
        public MISMOIndicator SalesContractTermsConsistentWithMarketIndicator { get; set; }
    
        [XmlIgnore]
        public bool SalesContractTermsConsistentWithMarketIndicatorSpecified
        {
            get { return this.SalesContractTermsConsistentWithMarketIndicator != null; }
            set { }
        }
    
        [XmlElement("SaleType", Order = 26)]
        public MISMOEnum<SaleBase> SaleType { get; set; }
    
        [XmlIgnore]
        public bool SaleTypeSpecified
        {
            get { return this.SaleType != null; }
            set { }
        }
    
        [XmlElement("SaleTypeOtherDescription", Order = 27)]
        public MISMOString SaleTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool SaleTypeOtherDescriptionSpecified
        {
            get { return this.SaleTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("SellerIsOwnerIndicator", Order = 28)]
        public MISMOIndicator SellerIsOwnerIndicator { get; set; }
    
        [XmlIgnore]
        public bool SellerIsOwnerIndicatorSpecified
        {
            get { return this.SellerIsOwnerIndicator != null; }
            set { }
        }
    
        [XmlElement("TotalSalesConcessionAmount", Order = 29)]
        public MISMOAmount TotalSalesConcessionAmount { get; set; }
    
        [XmlIgnore]
        public bool TotalSalesConcessionAmountSpecified
        {
            get { return this.TotalSalesConcessionAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 30)]
        public SALES_CONTRACT_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
