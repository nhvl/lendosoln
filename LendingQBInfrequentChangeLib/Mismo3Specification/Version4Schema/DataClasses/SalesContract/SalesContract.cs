namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SALES_CONTRACT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SalesConcessionsSpecified
                    || this.SalesContingenciesSpecified
                    || this.SalesContractDetailSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SALES_CONCESSIONS", Order = 0)]
        public SALES_CONCESSIONS SalesConcessions { get; set; }
    
        [XmlIgnore]
        public bool SalesConcessionsSpecified
        {
            get { return this.SalesConcessions != null && this.SalesConcessions.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SALES_CONTINGENCIES", Order = 1)]
        public SALES_CONTINGENCIES SalesContingencies { get; set; }
    
        [XmlIgnore]
        public bool SalesContingenciesSpecified
        {
            get { return this.SalesContingencies != null && this.SalesContingencies.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SALES_CONTRACT_DETAIL", Order = 2)]
        public SALES_CONTRACT_DETAIL SalesContractDetail { get; set; }
    
        [XmlIgnore]
        public bool SalesContractDetailSpecified
        {
            get { return this.SalesContractDetail != null && this.SalesContractDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public SALES_CONTRACT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
