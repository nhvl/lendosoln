namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CONSTRUCTION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ConstructionImprovementCostsAmountSpecified
                    || this.ConstructionLoanEstimatedInterestCalculationMethodTypeSpecified
                    || this.ConstructionLoanInterestReserveAmountSpecified
                    || this.ConstructionLoanTotalTermMonthsCountSpecified
                    || this.ConstructionLoanTypeSpecified
                    || this.ConstructionPeriodInterestRatePercentSpecified
                    || this.ConstructionPeriodNumberOfMonthsCountSpecified
                    || this.ConstructionPhaseInterestPaymentFrequencyTypeSpecified
                    || this.ConstructionPhaseInterestPaymentFrequencyTypeOtherDescriptionSpecified
                    || this.ConstructionPhaseInterestPaymentMethodTypeSpecified
                    || this.ConstructionPhaseInterestPaymentMethodTypeOtherDescriptionSpecified
                    || this.ConstructionPhaseInterestPaymentTypeSpecified
                    || this.ConstructionPhaseInterestPaymentTypeOtherDescriptionSpecified
                    || this.ConstructionToPermanentClosingFeatureTypeSpecified
                    || this.ConstructionToPermanentClosingFeatureTypeOtherDescriptionSpecified
                    || this.ConstructionToPermanentClosingTypeSpecified
                    || this.ConstructionToPermanentClosingTypeOtherDescriptionSpecified
                    || this.ConstructionToPermanentFirstPaymentDueDateSpecified
                    || this.ConstructionToPermanentRecertificationDateSpecified
                    || this.ConstructionToPermanentRecertificationValueAmountSpecified
                    || this.LandAppraisedValueAmountSpecified
                    || this.LandEstimatedValueAmountSpecified
                    || this.LandOriginalCostAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ConstructionImprovementCostsAmount", Order = 0)]
        public MISMOAmount ConstructionImprovementCostsAmount { get; set; }
    
        [XmlIgnore]
        public bool ConstructionImprovementCostsAmountSpecified
        {
            get { return this.ConstructionImprovementCostsAmount != null; }
            set { }
        }
    
        [XmlElement("ConstructionLoanEstimatedInterestCalculationMethodType", Order = 1)]
        public MISMOEnum<ConstructionLoanEstimatedInterestCalculationMethodBase> ConstructionLoanEstimatedInterestCalculationMethodType { get; set; }
    
        [XmlIgnore]
        public bool ConstructionLoanEstimatedInterestCalculationMethodTypeSpecified
        {
            get { return this.ConstructionLoanEstimatedInterestCalculationMethodType != null; }
            set { }
        }
    
        [XmlElement("ConstructionLoanInterestReserveAmount", Order = 2)]
        public MISMOAmount ConstructionLoanInterestReserveAmount { get; set; }
    
        [XmlIgnore]
        public bool ConstructionLoanInterestReserveAmountSpecified
        {
            get { return this.ConstructionLoanInterestReserveAmount != null; }
            set { }
        }
    
        [XmlElement("ConstructionLoanTotalTermMonthsCount", Order = 3)]
        public MISMOCount ConstructionLoanTotalTermMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool ConstructionLoanTotalTermMonthsCountSpecified
        {
            get { return this.ConstructionLoanTotalTermMonthsCount != null; }
            set { }
        }
    
        [XmlElement("ConstructionLoanType", Order = 4)]
        public MISMOEnum<ConstructionLoanBase> ConstructionLoanType { get; set; }
    
        [XmlIgnore]
        public bool ConstructionLoanTypeSpecified
        {
            get { return this.ConstructionLoanType != null; }
            set { }
        }
    
        [XmlElement("ConstructionPeriodInterestRatePercent", Order = 5)]
        public MISMOPercent ConstructionPeriodInterestRatePercent { get; set; }
    
        [XmlIgnore]
        public bool ConstructionPeriodInterestRatePercentSpecified
        {
            get { return this.ConstructionPeriodInterestRatePercent != null; }
            set { }
        }
    
        [XmlElement("ConstructionPeriodNumberOfMonthsCount", Order = 6)]
        public MISMOCount ConstructionPeriodNumberOfMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool ConstructionPeriodNumberOfMonthsCountSpecified
        {
            get { return this.ConstructionPeriodNumberOfMonthsCount != null; }
            set { }
        }
    
        [XmlElement("ConstructionPhaseInterestPaymentFrequencyType", Order = 7)]
        public MISMOEnum<ConstructionPhaseInterestPaymentFrequencyBase> ConstructionPhaseInterestPaymentFrequencyType { get; set; }
    
        [XmlIgnore]
        public bool ConstructionPhaseInterestPaymentFrequencyTypeSpecified
        {
            get { return this.ConstructionPhaseInterestPaymentFrequencyType != null; }
            set { }
        }
    
        [XmlElement("ConstructionPhaseInterestPaymentFrequencyTypeOtherDescription", Order = 8)]
        public MISMOString ConstructionPhaseInterestPaymentFrequencyTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ConstructionPhaseInterestPaymentFrequencyTypeOtherDescriptionSpecified
        {
            get { return this.ConstructionPhaseInterestPaymentFrequencyTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ConstructionPhaseInterestPaymentMethodType", Order = 9)]
        public MISMOEnum<ConstructionPhaseInterestPaymentMethodBase> ConstructionPhaseInterestPaymentMethodType { get; set; }
    
        [XmlIgnore]
        public bool ConstructionPhaseInterestPaymentMethodTypeSpecified
        {
            get { return this.ConstructionPhaseInterestPaymentMethodType != null; }
            set { }
        }
    
        [XmlElement("ConstructionPhaseInterestPaymentMethodTypeOtherDescription", Order = 10)]
        public MISMOString ConstructionPhaseInterestPaymentMethodTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ConstructionPhaseInterestPaymentMethodTypeOtherDescriptionSpecified
        {
            get { return this.ConstructionPhaseInterestPaymentMethodTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ConstructionPhaseInterestPaymentType", Order = 11)]
        public MISMOEnum<ConstructionPhaseInterestPaymentBase> ConstructionPhaseInterestPaymentType { get; set; }
    
        [XmlIgnore]
        public bool ConstructionPhaseInterestPaymentTypeSpecified
        {
            get { return this.ConstructionPhaseInterestPaymentType != null; }
            set { }
        }
    
        [XmlElement("ConstructionPhaseInterestPaymentTypeOtherDescription", Order = 12)]
        public MISMOString ConstructionPhaseInterestPaymentTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ConstructionPhaseInterestPaymentTypeOtherDescriptionSpecified
        {
            get { return this.ConstructionPhaseInterestPaymentTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ConstructionToPermanentClosingFeatureType", Order = 13)]
        public MISMOEnum<ConstructionToPermanentClosingFeatureBase> ConstructionToPermanentClosingFeatureType { get; set; }
    
        [XmlIgnore]
        public bool ConstructionToPermanentClosingFeatureTypeSpecified
        {
            get { return this.ConstructionToPermanentClosingFeatureType != null; }
            set { }
        }
    
        [XmlElement("ConstructionToPermanentClosingFeatureTypeOtherDescription", Order = 14)]
        public MISMOString ConstructionToPermanentClosingFeatureTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ConstructionToPermanentClosingFeatureTypeOtherDescriptionSpecified
        {
            get { return this.ConstructionToPermanentClosingFeatureTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ConstructionToPermanentClosingType", Order = 15)]
        public MISMOEnum<ConstructionToPermanentClosingBase> ConstructionToPermanentClosingType { get; set; }
    
        [XmlIgnore]
        public bool ConstructionToPermanentClosingTypeSpecified
        {
            get { return this.ConstructionToPermanentClosingType != null; }
            set { }
        }
    
        [XmlElement("ConstructionToPermanentClosingTypeOtherDescription", Order = 16)]
        public MISMOString ConstructionToPermanentClosingTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ConstructionToPermanentClosingTypeOtherDescriptionSpecified
        {
            get { return this.ConstructionToPermanentClosingTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ConstructionToPermanentFirstPaymentDueDate", Order = 17)]
        public MISMODate ConstructionToPermanentFirstPaymentDueDate { get; set; }
    
        [XmlIgnore]
        public bool ConstructionToPermanentFirstPaymentDueDateSpecified
        {
            get { return this.ConstructionToPermanentFirstPaymentDueDate != null; }
            set { }
        }
    
        [XmlElement("ConstructionToPermanentRecertificationDate", Order = 18)]
        public MISMODate ConstructionToPermanentRecertificationDate { get; set; }
    
        [XmlIgnore]
        public bool ConstructionToPermanentRecertificationDateSpecified
        {
            get { return this.ConstructionToPermanentRecertificationDate != null; }
            set { }
        }
    
        [XmlElement("ConstructionToPermanentRecertificationValueAmount", Order = 19)]
        public MISMOAmount ConstructionToPermanentRecertificationValueAmount { get; set; }
    
        [XmlIgnore]
        public bool ConstructionToPermanentRecertificationValueAmountSpecified
        {
            get { return this.ConstructionToPermanentRecertificationValueAmount != null; }
            set { }
        }
    
        [XmlElement("LandAppraisedValueAmount", Order = 20)]
        public MISMOAmount LandAppraisedValueAmount { get; set; }
    
        [XmlIgnore]
        public bool LandAppraisedValueAmountSpecified
        {
            get { return this.LandAppraisedValueAmount != null; }
            set { }
        }
    
        [XmlElement("LandEstimatedValueAmount", Order = 21)]
        public MISMOAmount LandEstimatedValueAmount { get; set; }
    
        [XmlIgnore]
        public bool LandEstimatedValueAmountSpecified
        {
            get { return this.LandEstimatedValueAmount != null; }
            set { }
        }
    
        [XmlElement("LandOriginalCostAmount", Order = 22)]
        public MISMOAmount LandOriginalCostAmount { get; set; }
    
        [XmlIgnore]
        public bool LandOriginalCostAmountSpecified
        {
            get { return this.LandOriginalCostAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 23)]
        public CONSTRUCTION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
