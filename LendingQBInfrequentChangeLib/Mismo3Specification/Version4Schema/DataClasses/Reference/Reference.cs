namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class REFERENCE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LocationURLSpecified
                    || this.LocationLabelValueSpecified
                    || this.OriginalCreatorDigestValueSpecified
                    || this.ReferenceSigningTypeSpecified;
            }
        }
    
        [XmlElement("LocationURL", Order = 0)]
        public MISMOURL LocationURL { get; set; }
    
        [XmlIgnore]
        public bool LocationURLSpecified
        {
            get { return this.LocationURL != null; }
            set { }
        }
    
        [XmlElement("LocationLabelValue", Order = 1)]
        public MISMOValue LocationLabelValue { get; set; }
    
        [XmlIgnore]
        public bool LocationLabelValueSpecified
        {
            get { return this.LocationLabelValue != null; }
            set { }
        }
    
        [XmlElement("OriginalCreatorDigestValue", Order = 2)]
        public MISMOValue OriginalCreatorDigestValue { get; set; }
    
        [XmlIgnore]
        public bool OriginalCreatorDigestValueSpecified
        {
            get { return this.OriginalCreatorDigestValue != null; }
            set { }
        }
    
        [XmlElement("ReferenceSigningType", Order = 3)]
        public MISMOEnum<ReferenceSigningBase> ReferenceSigningType { get; set; }
    
        [XmlIgnore]
        public bool ReferenceSigningTypeSpecified
        {
            get { return this.ReferenceSigningType != null; }
            set { }
        }
    }
}
