namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class RENTAL_FEATURES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.RentalFeatureListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("RENTAL_FEATURE", Order = 0)]
        public List<RENTAL_FEATURE> RentalFeatureList { get; set; } = new List<RENTAL_FEATURE>();
    
        [XmlIgnore]
        public bool RentalFeatureListSpecified
        {
            get { return this.RentalFeatureList != null && this.RentalFeatureList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public RENTAL_FEATURES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
