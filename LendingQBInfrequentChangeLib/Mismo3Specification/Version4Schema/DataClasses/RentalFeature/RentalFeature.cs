namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class RENTAL_FEATURE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.RentalFeatureDescriptionSpecified
                    || this.RentalFeatureTypeSpecified
                    || this.RentalFeatureTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("RentalFeatureDescription", Order = 0)]
        public MISMOString RentalFeatureDescription { get; set; }
    
        [XmlIgnore]
        public bool RentalFeatureDescriptionSpecified
        {
            get { return this.RentalFeatureDescription != null; }
            set { }
        }
    
        [XmlElement("RentalFeatureType", Order = 1)]
        public MISMOEnum<RentalFeatureBase> RentalFeatureType { get; set; }
    
        [XmlIgnore]
        public bool RentalFeatureTypeSpecified
        {
            get { return this.RentalFeatureType != null; }
            set { }
        }
    
        [XmlElement("RentalFeatureTypeOtherDescription", Order = 2)]
        public MISMOString RentalFeatureTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool RentalFeatureTypeOtherDescriptionSpecified
        {
            get { return this.RentalFeatureTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public RENTAL_FEATURE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
