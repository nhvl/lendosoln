namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class MODIFICATION_ASPECTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ModificationAspectListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MODIFICATION_ASPECT", Order = 0)]
        public List<MODIFICATION_ASPECT> ModificationAspectList { get; set; } = new List<MODIFICATION_ASPECT>();
    
        [XmlIgnore]
        public bool ModificationAspectListSpecified
        {
            get { return this.ModificationAspectList != null && this.ModificationAspectList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public MODIFICATION_ASPECTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
