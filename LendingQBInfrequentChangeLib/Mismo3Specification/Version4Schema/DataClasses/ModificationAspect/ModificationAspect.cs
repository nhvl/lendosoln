namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MODIFICATION_ASPECT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LoanModificationTypeSpecified
                    || this.LoanModificationTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LoanModificationType", Order = 0)]
        public MISMOEnum<LoanModificationBase> LoanModificationType { get; set; }
    
        [XmlIgnore]
        public bool LoanModificationTypeSpecified
        {
            get { return this.LoanModificationType != null; }
            set { }
        }
    
        [XmlElement("LoanModificationTypeOtherDescription", Order = 1)]
        public MISMOString LoanModificationTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool LoanModificationTypeOtherDescriptionSpecified
        {
            get { return this.LoanModificationTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public MODIFICATION_ASPECT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
