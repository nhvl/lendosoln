namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class INSULATION_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.InsulationCommentDescriptionSpecified
                    || this.InsulationHERSTypeSpecified
                    || this.ThermalRatedItemsTypeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("InsulationCommentDescription", Order = 0)]
        public MISMOString InsulationCommentDescription { get; set; }
    
        [XmlIgnore]
        public bool InsulationCommentDescriptionSpecified
        {
            get { return this.InsulationCommentDescription != null; }
            set { }
        }
    
        [XmlElement("InsulationHERSType", Order = 1)]
        public MISMOEnum<InsulationHERSBase> InsulationHERSType { get; set; }
    
        [XmlIgnore]
        public bool InsulationHERSTypeSpecified
        {
            get { return this.InsulationHERSType != null; }
            set { }
        }
    
        [XmlElement("ThermalRatedItemsType", Order = 2)]
        public MISMOEnum<ThermalRatedItemsBase> ThermalRatedItemsType { get; set; }
    
        [XmlIgnore]
        public bool ThermalRatedItemsTypeSpecified
        {
            get { return this.ThermalRatedItemsType != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public INSULATION_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
