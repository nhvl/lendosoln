namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class INSULATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.InsulationAreasSpecified
                    || this.InsulationDetailSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("INSULATION_AREAS", Order = 0)]
        public INSULATION_AREAS InsulationAreas { get; set; }
    
        [XmlIgnore]
        public bool InsulationAreasSpecified
        {
            get { return this.InsulationAreas != null && this.InsulationAreas.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("INSULATION_DETAIL", Order = 1)]
        public INSULATION_DETAIL InsulationDetail { get; set; }
    
        [XmlIgnore]
        public bool InsulationDetailSpecified
        {
            get { return this.InsulationDetail != null && this.InsulationDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public INSULATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
