namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class CERTIFICATES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CertificateListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CERTIFICATE", Order = 0)]
        public List<CERTIFICATE> CertificateList { get; set; } = new List<CERTIFICATE>();
    
        [XmlIgnore]
        public bool CertificateListSpecified
        {
            get { return this.CertificateList != null && this.CertificateList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public CERTIFICATES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
