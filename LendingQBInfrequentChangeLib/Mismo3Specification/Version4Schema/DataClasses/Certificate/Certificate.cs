namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CERTIFICATE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CertificateApplicationDateSpecified
                    || this.CertificateAuthorityTypeSpecified
                    || this.CertificateAuthorityTypeOtherDescriptionSpecified
                    || this.CertificateExpirationDateSpecified
                    || this.CertificateIssuedDateSpecified
                    || this.CertificateStatusDateSpecified
                    || this.CertificateStatusTypeSpecified
                    || this.CertificateStatusTypeOtherDescriptionSpecified
                    || this.CertificateTypeSpecified
                    || this.CertificateTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CertificateApplicationDate", Order = 0)]
        public MISMODate CertificateApplicationDate { get; set; }
    
        [XmlIgnore]
        public bool CertificateApplicationDateSpecified
        {
            get { return this.CertificateApplicationDate != null; }
            set { }
        }
    
        [XmlElement("CertificateAuthorityType", Order = 1)]
        public MISMOEnum<CertificateAuthorityBase> CertificateAuthorityType { get; set; }
    
        [XmlIgnore]
        public bool CertificateAuthorityTypeSpecified
        {
            get { return this.CertificateAuthorityType != null; }
            set { }
        }
    
        [XmlElement("CertificateAuthorityTypeOtherDescription", Order = 2)]
        public MISMOString CertificateAuthorityTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CertificateAuthorityTypeOtherDescriptionSpecified
        {
            get { return this.CertificateAuthorityTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("CertificateExpirationDate", Order = 3)]
        public MISMODate CertificateExpirationDate { get; set; }
    
        [XmlIgnore]
        public bool CertificateExpirationDateSpecified
        {
            get { return this.CertificateExpirationDate != null; }
            set { }
        }
    
        [XmlElement("CertificateIssuedDate", Order = 4)]
        public MISMODate CertificateIssuedDate { get; set; }
    
        [XmlIgnore]
        public bool CertificateIssuedDateSpecified
        {
            get { return this.CertificateIssuedDate != null; }
            set { }
        }
    
        [XmlElement("CertificateStatusDate", Order = 5)]
        public MISMODate CertificateStatusDate { get; set; }
    
        [XmlIgnore]
        public bool CertificateStatusDateSpecified
        {
            get { return this.CertificateStatusDate != null; }
            set { }
        }
    
        [XmlElement("CertificateStatusType", Order = 6)]
        public MISMOEnum<CertificateStatusBase> CertificateStatusType { get; set; }
    
        [XmlIgnore]
        public bool CertificateStatusTypeSpecified
        {
            get { return this.CertificateStatusType != null; }
            set { }
        }
    
        [XmlElement("CertificateStatusTypeOtherDescription", Order = 7)]
        public MISMOString CertificateStatusTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CertificateStatusTypeOtherDescriptionSpecified
        {
            get { return this.CertificateStatusTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("CertificateType", Order = 8)]
        public MISMOEnum<CertificateBase> CertificateType { get; set; }
    
        [XmlIgnore]
        public bool CertificateTypeSpecified
        {
            get { return this.CertificateType != null; }
            set { }
        }
    
        [XmlElement("CertificateTypeOtherDescription", Order = 9)]
        public MISMOString CertificateTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CertificateTypeOtherDescriptionSpecified
        {
            get { return this.CertificateTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 10)]
        public CERTIFICATE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
