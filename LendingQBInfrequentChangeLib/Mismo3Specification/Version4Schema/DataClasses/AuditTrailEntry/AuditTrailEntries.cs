namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class AUDIT_TRAIL_ENTRIES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AuditTrailEntryListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AUDIT_TRAIL_ENTRY", Order = 0)]
        public List<AUDIT_TRAIL_ENTRY> AuditTrailEntryList { get; set; } = new List<AUDIT_TRAIL_ENTRY>();
    
        [XmlIgnore]
        public bool AuditTrailEntryListSpecified
        {
            get { return this.AuditTrailEntryList != null && this.AuditTrailEntryList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public AUDIT_TRAIL_ENTRIES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
