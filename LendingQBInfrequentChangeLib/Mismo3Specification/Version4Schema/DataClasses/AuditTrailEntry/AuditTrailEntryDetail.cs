namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class AUDIT_TRAIL_ENTRY_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EntryDatetimeSpecified
                    || this.EntryDescriptionSpecified
                    || this.EventTypeSpecified
                    || this.EventTypeOtherDescriptionSpecified
                    || this.PerformedByOrganizationNameSpecified
                    || this.PerformedBySystemEntryIdentifierSpecified
                    || this.PerformedBySystemNameSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("EntryDatetime", Order = 0)]
        public MISMODatetime EntryDatetime { get; set; }
    
        [XmlIgnore]
        public bool EntryDatetimeSpecified
        {
            get { return this.EntryDatetime != null; }
            set { }
        }
    
        [XmlElement("EntryDescription", Order = 1)]
        public MISMOString EntryDescription { get; set; }
    
        [XmlIgnore]
        public bool EntryDescriptionSpecified
        {
            get { return this.EntryDescription != null; }
            set { }
        }
    
        [XmlElement("EventType", Order = 2)]
        public MISMOEnum<EventBase> EventType { get; set; }
    
        [XmlIgnore]
        public bool EventTypeSpecified
        {
            get { return this.EventType != null; }
            set { }
        }
    
        [XmlElement("EventTypeOtherDescription", Order = 3)]
        public MISMOString EventTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool EventTypeOtherDescriptionSpecified
        {
            get { return this.EventTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PerformedByOrganizationName", Order = 4)]
        public MISMOString PerformedByOrganizationName { get; set; }
    
        [XmlIgnore]
        public bool PerformedByOrganizationNameSpecified
        {
            get { return this.PerformedByOrganizationName != null; }
            set { }
        }
    
        [XmlElement("PerformedBySystemEntryIdentifier", Order = 5)]
        public MISMOIdentifier PerformedBySystemEntryIdentifier { get; set; }
    
        [XmlIgnore]
        public bool PerformedBySystemEntryIdentifierSpecified
        {
            get { return this.PerformedBySystemEntryIdentifier != null; }
            set { }
        }
    
        [XmlElement("PerformedBySystemName", Order = 6)]
        public MISMOString PerformedBySystemName { get; set; }
    
        [XmlIgnore]
        public bool PerformedBySystemNameSpecified
        {
            get { return this.PerformedBySystemName != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 7)]
        public AUDIT_TRAIL_ENTRY_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
