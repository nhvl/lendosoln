namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class AUDIT_TRAIL_ENTRY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AuditTrailEntryDetailSpecified
                    || this.AuditTrailEntryEvidenceSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AUDIT_TRAIL_ENTRY_DETAIL", Order = 0)]
        public AUDIT_TRAIL_ENTRY_DETAIL AuditTrailEntryDetail { get; set; }
    
        [XmlIgnore]
        public bool AuditTrailEntryDetailSpecified
        {
            get { return this.AuditTrailEntryDetail != null && this.AuditTrailEntryDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("AUDIT_TRAIL_ENTRY_EVIDENCE", Order = 1)]
        public AUDIT_TRAIL_ENTRY_EVIDENCE AuditTrailEntryEvidence { get; set; }
    
        [XmlIgnore]
        public bool AuditTrailEntryEvidenceSpecified
        {
            get { return this.AuditTrailEntryEvidence != null && this.AuditTrailEntryEvidence.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public AUDIT_TRAIL_ENTRY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
