namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class MILITARY_SERVICES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MilitaryServiceListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MILITARY_SERVICE", Order = 0)]
        public List<MILITARY_SERVICE> MilitaryServiceList { get; set; } = new List<MILITARY_SERVICE>();
    
        [XmlIgnore]
        public bool MilitaryServiceListSpecified
        {
            get { return this.MilitaryServiceList != null && this.MilitaryServiceList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public MILITARY_SERVICES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
