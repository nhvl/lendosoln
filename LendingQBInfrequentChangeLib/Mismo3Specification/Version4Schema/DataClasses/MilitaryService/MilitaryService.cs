namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MILITARY_SERVICE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MilitaryBranchTypeSpecified
                    || this.MilitaryBranchTypeOtherDescriptionSpecified
                    || this.MilitaryServiceExpectedCompletionDateSpecified
                    || this.MilitaryServiceFromDateSpecified
                    || this.MilitaryServiceNumberIdentifierSpecified
                    || this.MilitaryServiceServedAsNameSpecified
                    || this.MilitaryServiceToDateSpecified
                    || this.MilitaryStatusTypeSpecified
                    || this.MilitaryStatusTypeOtherDescriptionSpecified
                    || this.VADisabilityBenefitClaimIndicatorSpecified
                    || this.VAEligibilityIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MilitaryBranchType", Order = 0)]
        public MISMOEnum<MilitaryBranchBase> MilitaryBranchType { get; set; }
    
        [XmlIgnore]
        public bool MilitaryBranchTypeSpecified
        {
            get { return this.MilitaryBranchType != null; }
            set { }
        }
    
        [XmlElement("MilitaryBranchTypeOtherDescription", Order = 1)]
        public MISMOString MilitaryBranchTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MilitaryBranchTypeOtherDescriptionSpecified
        {
            get { return this.MilitaryBranchTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MilitaryServiceExpectedCompletionDate", Order = 2)]
        public MISMODate MilitaryServiceExpectedCompletionDate { get; set; }
    
        [XmlIgnore]
        public bool MilitaryServiceExpectedCompletionDateSpecified
        {
            get { return this.MilitaryServiceExpectedCompletionDate != null; }
            set { }
        }
    
        [XmlElement("MilitaryServiceFromDate", Order = 3)]
        public MISMODate MilitaryServiceFromDate { get; set; }
    
        [XmlIgnore]
        public bool MilitaryServiceFromDateSpecified
        {
            get { return this.MilitaryServiceFromDate != null; }
            set { }
        }
    
        [XmlElement("MilitaryServiceNumberIdentifier", Order = 4)]
        public MISMOIdentifier MilitaryServiceNumberIdentifier { get; set; }
    
        [XmlIgnore]
        public bool MilitaryServiceNumberIdentifierSpecified
        {
            get { return this.MilitaryServiceNumberIdentifier != null; }
            set { }
        }
    
        [XmlElement("MilitaryServiceServedAsName", Order = 5)]
        public MISMOString MilitaryServiceServedAsName { get; set; }
    
        [XmlIgnore]
        public bool MilitaryServiceServedAsNameSpecified
        {
            get { return this.MilitaryServiceServedAsName != null; }
            set { }
        }
    
        [XmlElement("MilitaryServiceToDate", Order = 6)]
        public MISMODate MilitaryServiceToDate { get; set; }
    
        [XmlIgnore]
        public bool MilitaryServiceToDateSpecified
        {
            get { return this.MilitaryServiceToDate != null; }
            set { }
        }
    
        [XmlElement("MilitaryStatusType", Order = 7)]
        public MISMOEnum<MilitaryStatusBase> MilitaryStatusType { get; set; }
    
        [XmlIgnore]
        public bool MilitaryStatusTypeSpecified
        {
            get { return this.MilitaryStatusType != null; }
            set { }
        }
    
        [XmlElement("MilitaryStatusTypeOtherDescription", Order = 8)]
        public MISMOString MilitaryStatusTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MilitaryStatusTypeOtherDescriptionSpecified
        {
            get { return this.MilitaryStatusTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("VADisabilityBenefitClaimIndicator", Order = 9)]
        public MISMOIndicator VADisabilityBenefitClaimIndicator { get; set; }
    
        [XmlIgnore]
        public bool VADisabilityBenefitClaimIndicatorSpecified
        {
            get { return this.VADisabilityBenefitClaimIndicator != null; }
            set { }
        }
    
        [XmlElement("VAEligibilityIndicator", Order = 10)]
        public MISMOIndicator VAEligibilityIndicator { get; set; }
    
        [XmlIgnore]
        public bool VAEligibilityIndicatorSpecified
        {
            get { return this.VAEligibilityIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 11)]
        public MILITARY_SERVICE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
