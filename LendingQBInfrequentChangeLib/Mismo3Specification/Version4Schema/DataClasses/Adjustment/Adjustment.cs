namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ADJUSTMENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ConversionAdjustmentSpecified
                    || this.InterestRateAdjustmentSpecified
                    || this.PrincipalAndInterestPaymentAdjustmentSpecified
                    || this.RateOrPaymentChangeOccurrencesSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CONVERSION_ADJUSTMENT", Order = 0)]
        public CONVERSION_ADJUSTMENT ConversionAdjustment { get; set; }
    
        [XmlIgnore]
        public bool ConversionAdjustmentSpecified
        {
            get { return this.ConversionAdjustment != null && this.ConversionAdjustment.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("INTEREST_RATE_ADJUSTMENT", Order = 1)]
        public INTEREST_RATE_ADJUSTMENT InterestRateAdjustment { get; set; }
    
        [XmlIgnore]
        public bool InterestRateAdjustmentSpecified
        {
            get { return this.InterestRateAdjustment != null && this.InterestRateAdjustment.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PRINCIPAL_AND_INTEREST_PAYMENT_ADJUSTMENT", Order = 2)]
        public PRINCIPAL_AND_INTEREST_PAYMENT_ADJUSTMENT PrincipalAndInterestPaymentAdjustment { get; set; }
    
        [XmlIgnore]
        public bool PrincipalAndInterestPaymentAdjustmentSpecified
        {
            get { return this.PrincipalAndInterestPaymentAdjustment != null && this.PrincipalAndInterestPaymentAdjustment.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("RATE_OR_PAYMENT_CHANGE_OCCURRENCES", Order = 3)]
        public RATE_OR_PAYMENT_CHANGE_OCCURRENCES RateOrPaymentChangeOccurrences { get; set; }
    
        [XmlIgnore]
        public bool RateOrPaymentChangeOccurrencesSpecified
        {
            get { return this.RateOrPaymentChangeOccurrences != null && this.RateOrPaymentChangeOccurrences.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public ADJUSTMENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
