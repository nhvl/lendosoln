namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class CREDIT_FILE_VARIATIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditFileVariationListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CREDIT_FILE_VARIATION", Order = 0)]
        public List<CREDIT_FILE_VARIATION> CreditFileVariationList { get; set; } = new List<CREDIT_FILE_VARIATION>();
    
        [XmlIgnore]
        public bool CreditFileVariationListSpecified
        {
            get { return this.CreditFileVariationList != null && this.CreditFileVariationList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public CREDIT_FILE_VARIATIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
