namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CREDIT_FILE_VARIATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditFileVariationTypeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CreditFileVariationType", Order = 0)]
        public MISMOEnum<CreditFileVariationBase> CreditFileVariationType { get; set; }
    
        [XmlIgnore]
        public bool CreditFileVariationTypeSpecified
        {
            get { return this.CreditFileVariationType != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public CREDIT_FILE_VARIATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
