namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CREDIT_INQUIRY_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditBusinessTypeSpecified
                    || this.CreditInquiryDateSpecified
                    || this.CreditInquiryResultTypeSpecified
                    || this.CreditLoanTypeSpecified
                    || this.CreditLoanTypeOtherDescriptionSpecified
                    || this.DetailCreditBusinessTypeSpecified
                    || this.DuplicateGroupIdentifierSpecified
                    || this.PrimaryRecordIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CreditBusinessType", Order = 0)]
        public MISMOEnum<CreditBusinessBase> CreditBusinessType { get; set; }
    
        [XmlIgnore]
        public bool CreditBusinessTypeSpecified
        {
            get { return this.CreditBusinessType != null; }
            set { }
        }
    
        [XmlElement("CreditInquiryDate", Order = 1)]
        public MISMODate CreditInquiryDate { get; set; }
    
        [XmlIgnore]
        public bool CreditInquiryDateSpecified
        {
            get { return this.CreditInquiryDate != null; }
            set { }
        }
    
        [XmlElement("CreditInquiryResultType", Order = 2)]
        public MISMOEnum<CreditInquiryResultBase> CreditInquiryResultType { get; set; }
    
        [XmlIgnore]
        public bool CreditInquiryResultTypeSpecified
        {
            get { return this.CreditInquiryResultType != null; }
            set { }
        }
    
        [XmlElement("CreditLoanType", Order = 3)]
        public MISMOEnum<CreditLoanBase> CreditLoanType { get; set; }
    
        [XmlIgnore]
        public bool CreditLoanTypeSpecified
        {
            get { return this.CreditLoanType != null; }
            set { }
        }
    
        [XmlElement("CreditLoanTypeOtherDescription", Order = 4)]
        public MISMOString CreditLoanTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CreditLoanTypeOtherDescriptionSpecified
        {
            get { return this.CreditLoanTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("DetailCreditBusinessType", Order = 5)]
        public MISMOEnum<DetailCreditBusinessBase> DetailCreditBusinessType { get; set; }
    
        [XmlIgnore]
        public bool DetailCreditBusinessTypeSpecified
        {
            get { return this.DetailCreditBusinessType != null; }
            set { }
        }
    
        [XmlElement("DuplicateGroupIdentifier", Order = 6)]
        public MISMOIdentifier DuplicateGroupIdentifier { get; set; }
    
        [XmlIgnore]
        public bool DuplicateGroupIdentifierSpecified
        {
            get { return this.DuplicateGroupIdentifier != null; }
            set { }
        }
    
        [XmlElement("PrimaryRecordIndicator", Order = 7)]
        public MISMOIndicator PrimaryRecordIndicator { get; set; }
    
        [XmlIgnore]
        public bool PrimaryRecordIndicatorSpecified
        {
            get { return this.PrimaryRecordIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 8)]
        public CREDIT_INQUIRY_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
