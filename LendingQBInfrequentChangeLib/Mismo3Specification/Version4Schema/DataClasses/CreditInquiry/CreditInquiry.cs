namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CREDIT_INQUIRY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AddressSpecified
                    || this.CreditCommentsSpecified
                    || this.CreditInquiryDetailSpecified
                    || this.CreditRepositoriesSpecified
                    || this.NameSpecified
                    || this.VerificationSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ADDRESS", Order = 0)]
        public ADDRESS Address { get; set; }
    
        [XmlIgnore]
        public bool AddressSpecified
        {
            get { return this.Address != null && this.Address.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_COMMENTS", Order = 1)]
        public CREDIT_COMMENTS CreditComments { get; set; }
    
        [XmlIgnore]
        public bool CreditCommentsSpecified
        {
            get { return this.CreditComments != null && this.CreditComments.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_INQUIRY_DETAIL", Order = 2)]
        public CREDIT_INQUIRY_DETAIL CreditInquiryDetail { get; set; }
    
        [XmlIgnore]
        public bool CreditInquiryDetailSpecified
        {
            get { return this.CreditInquiryDetail != null && this.CreditInquiryDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_REPOSITORIES", Order = 3)]
        public CREDIT_REPOSITORIES CreditRepositories { get; set; }
    
        [XmlIgnore]
        public bool CreditRepositoriesSpecified
        {
            get { return this.CreditRepositories != null && this.CreditRepositories.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("NAME", Order = 4)]
        public NAME Name { get; set; }
    
        [XmlIgnore]
        public bool NameSpecified
        {
            get { return this.Name != null && this.Name.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("VERIFICATION", Order = 5)]
        public VERIFICATION Verification { get; set; }
    
        [XmlIgnore]
        public bool VerificationSpecified
        {
            get { return this.Verification != null && this.Verification.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 6)]
        public CREDIT_INQUIRY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
