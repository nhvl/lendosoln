namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class CREDIT_INQUIRIES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditInquiryListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CREDIT_INQUIRY", Order = 0)]
        public List<CREDIT_INQUIRY> CreditInquiryList { get; set; } = new List<CREDIT_INQUIRY>();
    
        [XmlIgnore]
        public bool CreditInquiryListSpecified
        {
            get { return this.CreditInquiryList != null && this.CreditInquiryList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public CREDIT_INQUIRIES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
