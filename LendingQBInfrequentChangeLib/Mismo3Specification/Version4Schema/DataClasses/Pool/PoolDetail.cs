namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class POOL_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AmortizationTypeSpecified
                    || this.AmortizationTypeOtherDescriptionSpecified
                    || this.CUSIPIdentifierSpecified
                    || this.GovernmentBondFinanceIndicatorSpecified
                    || this.GovernmentBondFinancingProgramTypeSpecified
                    || this.GovernmentBondFinancingProgramTypeOtherDescriptionSpecified
                    || this.PoolAccrualRateStructureTypeSpecified
                    || this.PoolAssumabilityIndicatorSpecified
                    || this.PoolBalloonIndicatorSpecified
                    || this.PoolClassTypeSpecified
                    || this.PoolClassTypeOtherDescriptionSpecified
                    || this.PoolConcurrentTransferIndicatorSpecified
                    || this.PoolCurrentLoanCountSpecified
                    || this.PoolCurrentPrincipalBalanceAmountSpecified
                    || this.PoolFixedServicingFeePercentSpecified
                    || this.PoolGuarantyFeeRatePercentSpecified
                    || this.PoolIdentifierSpecified
                    || this.PoolingMethodTypeSpecified
                    || this.PoolingMethodTypeOtherDescriptionSpecified
                    || this.PoolInterestAdjustmentEffectiveDateSpecified
                    || this.PoolInterestAdjustmentIndexLeadDaysCountSpecified
                    || this.PoolInterestAndPaymentAdjustmentIndexLeadDaysCountSpecified
                    || this.PoolInterestOnlyIndicatorSpecified
                    || this.PoolInterestRateRoundingPercentSpecified
                    || this.PoolInterestRateRoundingTypeSpecified
                    || this.PoolInterestRateTruncatedDigitsCountSpecified
                    || this.PoolInvestorProductPlanIdentifierSpecified
                    || this.PoolIssueDateSpecified
                    || this.PoolIssuerIdentifierSpecified
                    || this.PoolMarginRatePercentSpecified
                    || this.PoolMaturityDateSpecified
                    || this.PoolMaximumAccrualRatePercentSpecified
                    || this.PoolMinimumAccrualRatePercentSpecified
                    || this.PoolMortgageRatePercentSpecified
                    || this.PoolMortgageTypeSpecified
                    || this.PoolMortgageTypeOtherDescriptionSpecified
                    || this.PoolOriginalLoanCountSpecified
                    || this.PoolOriginalPrincipalBalanceAmountSpecified
                    || this.PoolOwnershipPercentSpecified
                    || this.PoolPrefixIdentifierSpecified
                    || this.PoolPriorPeriodPrincipalBalanceAmountSpecified
                    || this.PoolScheduledPrincipalAndInterestPaymentAmountSpecified
                    || this.PoolScheduledPrincipalBalanceAmountSpecified
                    || this.PoolScheduledRemittancePaymentDaySpecified
                    || this.PoolSecurityInterestRatePercentSpecified
                    || this.PoolSecurityIssueDateInterestRatePercentSpecified
                    || this.PoolServiceFeeRatePercentSpecified
                    || this.PoolStructureTypeSpecified
                    || this.PoolStructureTypeOtherDescriptionSpecified
                    || this.PoolSuffixIdentifierSpecified
                    || this.PoolUnscheduledPrincipalAmountSpecified
                    || this.PoolUnscheduledPrincipalPaymentDaySpecified
                    || this.SecurityTradeBookEntryDateSpecified
                    || this.SecurityTradeCustomerAccountIdentifierSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AmortizationType", Order = 0)]
        public MISMOEnum<AmortizationBase> AmortizationType { get; set; }
    
        [XmlIgnore]
        public bool AmortizationTypeSpecified
        {
            get { return this.AmortizationType != null; }
            set { }
        }
    
        [XmlElement("AmortizationTypeOtherDescription", Order = 1)]
        public MISMOString AmortizationTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool AmortizationTypeOtherDescriptionSpecified
        {
            get { return this.AmortizationTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("CUSIPIdentifier", Order = 2)]
        public MISMOIdentifier CUSIPIdentifier { get; set; }
    
        [XmlIgnore]
        public bool CUSIPIdentifierSpecified
        {
            get { return this.CUSIPIdentifier != null; }
            set { }
        }
    
        [XmlElement("GovernmentBondFinanceIndicator", Order = 3)]
        public MISMOIndicator GovernmentBondFinanceIndicator { get; set; }
    
        [XmlIgnore]
        public bool GovernmentBondFinanceIndicatorSpecified
        {
            get { return this.GovernmentBondFinanceIndicator != null; }
            set { }
        }
    
        [XmlElement("GovernmentBondFinancingProgramType", Order = 4)]
        public MISMOEnum<GovernmentBondFinancingProgramBase> GovernmentBondFinancingProgramType { get; set; }
    
        [XmlIgnore]
        public bool GovernmentBondFinancingProgramTypeSpecified
        {
            get { return this.GovernmentBondFinancingProgramType != null; }
            set { }
        }
    
        [XmlElement("GovernmentBondFinancingProgramTypeOtherDescription", Order = 5)]
        public MISMOString GovernmentBondFinancingProgramTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool GovernmentBondFinancingProgramTypeOtherDescriptionSpecified
        {
            get { return this.GovernmentBondFinancingProgramTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PoolAccrualRateStructureType", Order = 6)]
        public MISMOEnum<PoolAccrualRateStructureBase> PoolAccrualRateStructureType { get; set; }
    
        [XmlIgnore]
        public bool PoolAccrualRateStructureTypeSpecified
        {
            get { return this.PoolAccrualRateStructureType != null; }
            set { }
        }
    
        [XmlElement("PoolAssumabilityIndicator", Order = 7)]
        public MISMOIndicator PoolAssumabilityIndicator { get; set; }
    
        [XmlIgnore]
        public bool PoolAssumabilityIndicatorSpecified
        {
            get { return this.PoolAssumabilityIndicator != null; }
            set { }
        }
    
        [XmlElement("PoolBalloonIndicator", Order = 8)]
        public MISMOIndicator PoolBalloonIndicator { get; set; }
    
        [XmlIgnore]
        public bool PoolBalloonIndicatorSpecified
        {
            get { return this.PoolBalloonIndicator != null; }
            set { }
        }
    
        [XmlElement("PoolClassType", Order = 9)]
        public MISMOEnum<PoolClassBase> PoolClassType { get; set; }
    
        [XmlIgnore]
        public bool PoolClassTypeSpecified
        {
            get { return this.PoolClassType != null; }
            set { }
        }
    
        [XmlElement("PoolClassTypeOtherDescription", Order = 10)]
        public MISMOString PoolClassTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PoolClassTypeOtherDescriptionSpecified
        {
            get { return this.PoolClassTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PoolConcurrentTransferIndicator", Order = 11)]
        public MISMOIndicator PoolConcurrentTransferIndicator { get; set; }
    
        [XmlIgnore]
        public bool PoolConcurrentTransferIndicatorSpecified
        {
            get { return this.PoolConcurrentTransferIndicator != null; }
            set { }
        }
    
        [XmlElement("PoolCurrentLoanCount", Order = 12)]
        public MISMOCount PoolCurrentLoanCount { get; set; }
    
        [XmlIgnore]
        public bool PoolCurrentLoanCountSpecified
        {
            get { return this.PoolCurrentLoanCount != null; }
            set { }
        }
    
        [XmlElement("PoolCurrentPrincipalBalanceAmount", Order = 13)]
        public MISMOAmount PoolCurrentPrincipalBalanceAmount { get; set; }
    
        [XmlIgnore]
        public bool PoolCurrentPrincipalBalanceAmountSpecified
        {
            get { return this.PoolCurrentPrincipalBalanceAmount != null; }
            set { }
        }
    
        [XmlElement("PoolFixedServicingFeePercent", Order = 14)]
        public MISMOPercent PoolFixedServicingFeePercent { get; set; }
    
        [XmlIgnore]
        public bool PoolFixedServicingFeePercentSpecified
        {
            get { return this.PoolFixedServicingFeePercent != null; }
            set { }
        }
    
        [XmlElement("PoolGuarantyFeeRatePercent", Order = 15)]
        public MISMOPercent PoolGuarantyFeeRatePercent { get; set; }
    
        [XmlIgnore]
        public bool PoolGuarantyFeeRatePercentSpecified
        {
            get { return this.PoolGuarantyFeeRatePercent != null; }
            set { }
        }
    
        [XmlElement("PoolIdentifier", Order = 16)]
        public MISMOIdentifier PoolIdentifier { get; set; }
    
        [XmlIgnore]
        public bool PoolIdentifierSpecified
        {
            get { return this.PoolIdentifier != null; }
            set { }
        }
    
        [XmlElement("PoolingMethodType", Order = 17)]
        public MISMOEnum<PoolingMethodBase> PoolingMethodType { get; set; }
    
        [XmlIgnore]
        public bool PoolingMethodTypeSpecified
        {
            get { return this.PoolingMethodType != null; }
            set { }
        }
    
        [XmlElement("PoolingMethodTypeOtherDescription", Order = 18)]
        public MISMOString PoolingMethodTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PoolingMethodTypeOtherDescriptionSpecified
        {
            get { return this.PoolingMethodTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PoolInterestAdjustmentEffectiveDate", Order = 19)]
        public MISMODate PoolInterestAdjustmentEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool PoolInterestAdjustmentEffectiveDateSpecified
        {
            get { return this.PoolInterestAdjustmentEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("PoolInterestAdjustmentIndexLeadDaysCount", Order = 20)]
        public MISMOCount PoolInterestAdjustmentIndexLeadDaysCount { get; set; }
    
        [XmlIgnore]
        public bool PoolInterestAdjustmentIndexLeadDaysCountSpecified
        {
            get { return this.PoolInterestAdjustmentIndexLeadDaysCount != null; }
            set { }
        }
    
        [XmlElement("PoolInterestAndPaymentAdjustmentIndexLeadDaysCount", Order = 21)]
        public MISMOCount PoolInterestAndPaymentAdjustmentIndexLeadDaysCount { get; set; }
    
        [XmlIgnore]
        public bool PoolInterestAndPaymentAdjustmentIndexLeadDaysCountSpecified
        {
            get { return this.PoolInterestAndPaymentAdjustmentIndexLeadDaysCount != null; }
            set { }
        }
    
        [XmlElement("PoolInterestOnlyIndicator", Order = 22)]
        public MISMOIndicator PoolInterestOnlyIndicator { get; set; }
    
        [XmlIgnore]
        public bool PoolInterestOnlyIndicatorSpecified
        {
            get { return this.PoolInterestOnlyIndicator != null; }
            set { }
        }
    
        [XmlElement("PoolInterestRateRoundingPercent", Order = 23)]
        public MISMOPercent PoolInterestRateRoundingPercent { get; set; }
    
        [XmlIgnore]
        public bool PoolInterestRateRoundingPercentSpecified
        {
            get { return this.PoolInterestRateRoundingPercent != null; }
            set { }
        }
    
        [XmlElement("PoolInterestRateRoundingType", Order = 24)]
        public MISMOEnum<InterestRateRoundingBase> PoolInterestRateRoundingType { get; set; }
    
        [XmlIgnore]
        public bool PoolInterestRateRoundingTypeSpecified
        {
            get { return this.PoolInterestRateRoundingType != null; }
            set { }
        }
    
        [XmlElement("PoolInterestRateTruncatedDigitsCount", Order = 25)]
        public MISMOCount PoolInterestRateTruncatedDigitsCount { get; set; }
    
        [XmlIgnore]
        public bool PoolInterestRateTruncatedDigitsCountSpecified
        {
            get { return this.PoolInterestRateTruncatedDigitsCount != null; }
            set { }
        }
    
        [XmlElement("PoolInvestorProductPlanIdentifier", Order = 26)]
        public MISMOIdentifier PoolInvestorProductPlanIdentifier { get; set; }
    
        [XmlIgnore]
        public bool PoolInvestorProductPlanIdentifierSpecified
        {
            get { return this.PoolInvestorProductPlanIdentifier != null; }
            set { }
        }
    
        [XmlElement("PoolIssueDate", Order = 27)]
        public MISMODate PoolIssueDate { get; set; }
    
        [XmlIgnore]
        public bool PoolIssueDateSpecified
        {
            get { return this.PoolIssueDate != null; }
            set { }
        }
    
        [XmlElement("PoolIssuerIdentifier", Order = 28)]
        public MISMOIdentifier PoolIssuerIdentifier { get; set; }
    
        [XmlIgnore]
        public bool PoolIssuerIdentifierSpecified
        {
            get { return this.PoolIssuerIdentifier != null; }
            set { }
        }
    
        [XmlElement("PoolMarginRatePercent", Order = 29)]
        public MISMOPercent PoolMarginRatePercent { get; set; }
    
        [XmlIgnore]
        public bool PoolMarginRatePercentSpecified
        {
            get { return this.PoolMarginRatePercent != null; }
            set { }
        }
    
        [XmlElement("PoolMaturityDate", Order = 30)]
        public MISMODate PoolMaturityDate { get; set; }
    
        [XmlIgnore]
        public bool PoolMaturityDateSpecified
        {
            get { return this.PoolMaturityDate != null; }
            set { }
        }
    
        [XmlElement("PoolMaximumAccrualRatePercent", Order = 31)]
        public MISMOPercent PoolMaximumAccrualRatePercent { get; set; }
    
        [XmlIgnore]
        public bool PoolMaximumAccrualRatePercentSpecified
        {
            get { return this.PoolMaximumAccrualRatePercent != null; }
            set { }
        }
    
        [XmlElement("PoolMinimumAccrualRatePercent", Order = 32)]
        public MISMOPercent PoolMinimumAccrualRatePercent { get; set; }
    
        [XmlIgnore]
        public bool PoolMinimumAccrualRatePercentSpecified
        {
            get { return this.PoolMinimumAccrualRatePercent != null; }
            set { }
        }
    
        [XmlElement("PoolMortgageRatePercent", Order = 33)]
        public MISMOPercent PoolMortgageRatePercent { get; set; }
    
        [XmlIgnore]
        public bool PoolMortgageRatePercentSpecified
        {
            get { return this.PoolMortgageRatePercent != null; }
            set { }
        }
    
        [XmlElement("PoolMortgageType", Order = 34)]
        public MISMOEnum<MortgageBase> PoolMortgageType { get; set; }
    
        [XmlIgnore]
        public bool PoolMortgageTypeSpecified
        {
            get { return this.PoolMortgageType != null; }
            set { }
        }
    
        [XmlElement("PoolMortgageTypeOtherDescription", Order = 35)]
        public MISMOString PoolMortgageTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PoolMortgageTypeOtherDescriptionSpecified
        {
            get { return this.PoolMortgageTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PoolOriginalLoanCount", Order = 36)]
        public MISMOCount PoolOriginalLoanCount { get; set; }
    
        [XmlIgnore]
        public bool PoolOriginalLoanCountSpecified
        {
            get { return this.PoolOriginalLoanCount != null; }
            set { }
        }
    
        [XmlElement("PoolOriginalPrincipalBalanceAmount", Order = 37)]
        public MISMOAmount PoolOriginalPrincipalBalanceAmount { get; set; }
    
        [XmlIgnore]
        public bool PoolOriginalPrincipalBalanceAmountSpecified
        {
            get { return this.PoolOriginalPrincipalBalanceAmount != null; }
            set { }
        }
    
        [XmlElement("PoolOwnershipPercent", Order = 38)]
        public MISMOPercent PoolOwnershipPercent { get; set; }
    
        [XmlIgnore]
        public bool PoolOwnershipPercentSpecified
        {
            get { return this.PoolOwnershipPercent != null; }
            set { }
        }
    
        [XmlElement("PoolPrefixIdentifier", Order = 39)]
        public MISMOIdentifier PoolPrefixIdentifier { get; set; }
    
        [XmlIgnore]
        public bool PoolPrefixIdentifierSpecified
        {
            get { return this.PoolPrefixIdentifier != null; }
            set { }
        }
    
        [XmlElement("PoolPriorPeriodPrincipalBalanceAmount", Order = 40)]
        public MISMOAmount PoolPriorPeriodPrincipalBalanceAmount { get; set; }
    
        [XmlIgnore]
        public bool PoolPriorPeriodPrincipalBalanceAmountSpecified
        {
            get { return this.PoolPriorPeriodPrincipalBalanceAmount != null; }
            set { }
        }
    
        [XmlElement("PoolScheduledPrincipalAndInterestPaymentAmount", Order = 41)]
        public MISMOAmount PoolScheduledPrincipalAndInterestPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool PoolScheduledPrincipalAndInterestPaymentAmountSpecified
        {
            get { return this.PoolScheduledPrincipalAndInterestPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("PoolScheduledPrincipalBalanceAmount", Order = 42)]
        public MISMOAmount PoolScheduledPrincipalBalanceAmount { get; set; }
    
        [XmlIgnore]
        public bool PoolScheduledPrincipalBalanceAmountSpecified
        {
            get { return this.PoolScheduledPrincipalBalanceAmount != null; }
            set { }
        }
    
        [XmlElement("PoolScheduledRemittancePaymentDay", Order = 43)]
        public MISMODay PoolScheduledRemittancePaymentDay { get; set; }
    
        [XmlIgnore]
        public bool PoolScheduledRemittancePaymentDaySpecified
        {
            get { return this.PoolScheduledRemittancePaymentDay != null; }
            set { }
        }
    
        [XmlElement("PoolSecurityInterestRatePercent", Order = 44)]
        public MISMOPercent PoolSecurityInterestRatePercent { get; set; }
    
        [XmlIgnore]
        public bool PoolSecurityInterestRatePercentSpecified
        {
            get { return this.PoolSecurityInterestRatePercent != null; }
            set { }
        }
    
        [XmlElement("PoolSecurityIssueDateInterestRatePercent", Order = 45)]
        public MISMOPercent PoolSecurityIssueDateInterestRatePercent { get; set; }
    
        [XmlIgnore]
        public bool PoolSecurityIssueDateInterestRatePercentSpecified
        {
            get { return this.PoolSecurityIssueDateInterestRatePercent != null; }
            set { }
        }
    
        [XmlElement("PoolServiceFeeRatePercent", Order = 46)]
        public MISMOPercent PoolServiceFeeRatePercent { get; set; }
    
        [XmlIgnore]
        public bool PoolServiceFeeRatePercentSpecified
        {
            get { return this.PoolServiceFeeRatePercent != null; }
            set { }
        }
    
        [XmlElement("PoolStructureType", Order = 47)]
        public MISMOEnum<PoolStructureBase> PoolStructureType { get; set; }
    
        [XmlIgnore]
        public bool PoolStructureTypeSpecified
        {
            get { return this.PoolStructureType != null; }
            set { }
        }
    
        [XmlElement("PoolStructureTypeOtherDescription", Order = 48)]
        public MISMOString PoolStructureTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PoolStructureTypeOtherDescriptionSpecified
        {
            get { return this.PoolStructureTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PoolSuffixIdentifier", Order = 49)]
        public MISMOIdentifier PoolSuffixIdentifier { get; set; }
    
        [XmlIgnore]
        public bool PoolSuffixIdentifierSpecified
        {
            get { return this.PoolSuffixIdentifier != null; }
            set { }
        }
    
        [XmlElement("PoolUnscheduledPrincipalAmount", Order = 50)]
        public MISMOAmount PoolUnscheduledPrincipalAmount { get; set; }
    
        [XmlIgnore]
        public bool PoolUnscheduledPrincipalAmountSpecified
        {
            get { return this.PoolUnscheduledPrincipalAmount != null; }
            set { }
        }
    
        [XmlElement("PoolUnscheduledPrincipalPaymentDay", Order = 51)]
        public MISMODay PoolUnscheduledPrincipalPaymentDay { get; set; }
    
        [XmlIgnore]
        public bool PoolUnscheduledPrincipalPaymentDaySpecified
        {
            get { return this.PoolUnscheduledPrincipalPaymentDay != null; }
            set { }
        }
    
        [XmlElement("SecurityTradeBookEntryDate", Order = 52)]
        public MISMODate SecurityTradeBookEntryDate { get; set; }
    
        [XmlIgnore]
        public bool SecurityTradeBookEntryDateSpecified
        {
            get { return this.SecurityTradeBookEntryDate != null; }
            set { }
        }
    
        [XmlElement("SecurityTradeCustomerAccountIdentifier", Order = 53)]
        public MISMOIdentifier SecurityTradeCustomerAccountIdentifier { get; set; }
    
        [XmlIgnore]
        public bool SecurityTradeCustomerAccountIdentifierSpecified
        {
            get { return this.SecurityTradeCustomerAccountIdentifier != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 54)]
        public POOL_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
