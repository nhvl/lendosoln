namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class POOL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PoolCertificateSpecified
                    || this.PoolDetailSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("POOL_CERTIFICATE", Order = 0)]
        public POOL_CERTIFICATE PoolCertificate { get; set; }
    
        [XmlIgnore]
        public bool PoolCertificateSpecified
        {
            get { return this.PoolCertificate != null && this.PoolCertificate.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("POOL_DETAIL", Order = 1)]
        public POOL_DETAIL PoolDetail { get; set; }
    
        [XmlIgnore]
        public bool PoolDetailSpecified
        {
            get { return this.PoolDetail != null && this.PoolDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public POOL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
