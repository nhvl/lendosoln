namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class GFE_SECTION_SUMMARIES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.GfeSectionSummaryListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("GFE_SECTION_SUMMARY", Order = 0)]
        public List<GFE_SECTION_SUMMARY> GfeSectionSummaryList { get; set; } = new List<GFE_SECTION_SUMMARY>();
    
        [XmlIgnore]
        public bool GfeSectionSummaryListSpecified
        {
            get { return this.GfeSectionSummaryList != null && this.GfeSectionSummaryList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public GFE_SECTION_SUMMARIES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
