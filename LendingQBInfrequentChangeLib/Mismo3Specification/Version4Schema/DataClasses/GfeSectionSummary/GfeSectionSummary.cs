namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class GFE_SECTION_SUMMARY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.GFESectionDisclosedTotalAmountSpecified
                    || this.GFESectionIdentifierTypeSpecified
                    || this.GFESectionIdentifierTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("GFESectionDisclosedTotalAmount", Order = 0)]
        public MISMOAmount GFESectionDisclosedTotalAmount { get; set; }
    
        [XmlIgnore]
        public bool GFESectionDisclosedTotalAmountSpecified
        {
            get { return this.GFESectionDisclosedTotalAmount != null; }
            set { }
        }
    
        [XmlElement("GFESectionIdentifierType", Order = 1)]
        public MISMOEnum<GFESectionIdentifierBase> GFESectionIdentifierType { get; set; }
    
        [XmlIgnore]
        public bool GFESectionIdentifierTypeSpecified
        {
            get { return this.GFESectionIdentifierType != null; }
            set { }
        }
    
        [XmlElement("GFESectionIdentifierTypeOtherDescription", Order = 2)]
        public MISMOString GFESectionIdentifierTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool GFESectionIdentifierTypeOtherDescriptionSpecified
        {
            get { return this.GFESectionIdentifierTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public GFE_SECTION_SUMMARY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
