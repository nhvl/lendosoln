namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class COMPARISON_TO_NEIGHBORHOOD
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LivingUnitCountSpecified
                    || this.RentControlDescriptionSpecified
                    || this.RentControlStatusTypeSpecified
                    || this.StoriesCountSpecified
                    || this.StructureFeatureTypeSpecified
                    || this.StructureFeatureTypeOtherDescriptionSpecified
                    || this.StructureTypicalToNeighborhoodAtypicalDescriptionSpecified
                    || this.StructureTypicalToNeighborhoodExistsIndicatorSpecified
                    || this.TypicalApartmentDescriptionSpecified
                    || this.TypicalApartmentEstimatedVacancyPercentSpecified
                    || this.TypicalApartmentRentHighPriceAmountSpecified
                    || this.TypicalApartmentRentLowPriceAmountSpecified
                    || this.TypicalApartmentRentTrendTypeSpecified
                    || this.TypicalApartmentVacancyTrendTypeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LivingUnitCount", Order = 0)]
        public MISMOCount LivingUnitCount { get; set; }
    
        [XmlIgnore]
        public bool LivingUnitCountSpecified
        {
            get { return this.LivingUnitCount != null; }
            set { }
        }
    
        [XmlElement("RentControlDescription", Order = 1)]
        public MISMOString RentControlDescription { get; set; }
    
        [XmlIgnore]
        public bool RentControlDescriptionSpecified
        {
            get { return this.RentControlDescription != null; }
            set { }
        }
    
        [XmlElement("RentControlStatusType", Order = 2)]
        public MISMOEnum<RentControlStatusBase> RentControlStatusType { get; set; }
    
        [XmlIgnore]
        public bool RentControlStatusTypeSpecified
        {
            get { return this.RentControlStatusType != null; }
            set { }
        }
    
        [XmlElement("StoriesCount", Order = 3)]
        public MISMOCount StoriesCount { get; set; }
    
        [XmlIgnore]
        public bool StoriesCountSpecified
        {
            get { return this.StoriesCount != null; }
            set { }
        }
    
        [XmlElement("StructureFeatureType", Order = 4)]
        public MISMOEnum<StructureFeatureBase> StructureFeatureType { get; set; }
    
        [XmlIgnore]
        public bool StructureFeatureTypeSpecified
        {
            get { return this.StructureFeatureType != null; }
            set { }
        }
    
        [XmlElement("StructureFeatureTypeOtherDescription", Order = 5)]
        public MISMOString StructureFeatureTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool StructureFeatureTypeOtherDescriptionSpecified
        {
            get { return this.StructureFeatureTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("StructureTypicalToNeighborhoodAtypicalDescription", Order = 6)]
        public MISMOString StructureTypicalToNeighborhoodAtypicalDescription { get; set; }
    
        [XmlIgnore]
        public bool StructureTypicalToNeighborhoodAtypicalDescriptionSpecified
        {
            get { return this.StructureTypicalToNeighborhoodAtypicalDescription != null; }
            set { }
        }
    
        [XmlElement("StructureTypicalToNeighborhoodExistsIndicator", Order = 7)]
        public MISMOIndicator StructureTypicalToNeighborhoodExistsIndicator { get; set; }
    
        [XmlIgnore]
        public bool StructureTypicalToNeighborhoodExistsIndicatorSpecified
        {
            get { return this.StructureTypicalToNeighborhoodExistsIndicator != null; }
            set { }
        }
    
        [XmlElement("TypicalApartmentDescription", Order = 8)]
        public MISMOString TypicalApartmentDescription { get; set; }
    
        [XmlIgnore]
        public bool TypicalApartmentDescriptionSpecified
        {
            get { return this.TypicalApartmentDescription != null; }
            set { }
        }
    
        [XmlElement("TypicalApartmentEstimatedVacancyPercent", Order = 9)]
        public MISMOPercent TypicalApartmentEstimatedVacancyPercent { get; set; }
    
        [XmlIgnore]
        public bool TypicalApartmentEstimatedVacancyPercentSpecified
        {
            get { return this.TypicalApartmentEstimatedVacancyPercent != null; }
            set { }
        }
    
        [XmlElement("TypicalApartmentRentHighPriceAmount", Order = 10)]
        public MISMOAmount TypicalApartmentRentHighPriceAmount { get; set; }
    
        [XmlIgnore]
        public bool TypicalApartmentRentHighPriceAmountSpecified
        {
            get { return this.TypicalApartmentRentHighPriceAmount != null; }
            set { }
        }
    
        [XmlElement("TypicalApartmentRentLowPriceAmount", Order = 11)]
        public MISMOAmount TypicalApartmentRentLowPriceAmount { get; set; }
    
        [XmlIgnore]
        public bool TypicalApartmentRentLowPriceAmountSpecified
        {
            get { return this.TypicalApartmentRentLowPriceAmount != null; }
            set { }
        }
    
        [XmlElement("TypicalApartmentRentTrendType", Order = 12)]
        public MISMOEnum<TypicalApartmentRentTrendBase> TypicalApartmentRentTrendType { get; set; }
    
        [XmlIgnore]
        public bool TypicalApartmentRentTrendTypeSpecified
        {
            get { return this.TypicalApartmentRentTrendType != null; }
            set { }
        }
    
        [XmlElement("TypicalApartmentVacancyTrendType", Order = 13)]
        public MISMOEnum<TypicalApartmentVacancyTrendBase> TypicalApartmentVacancyTrendType { get; set; }
    
        [XmlIgnore]
        public bool TypicalApartmentVacancyTrendTypeSpecified
        {
            get { return this.TypicalApartmentVacancyTrendType != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 14)]
        public COMPARISON_TO_NEIGHBORHOOD_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
