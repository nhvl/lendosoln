namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class COMPARISON_TO_NEIGHBORHOODS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ComparisonToNeighborhoodListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("COMPARISON_TO_NEIGHBORHOOD", Order = 0)]
        public List<COMPARISON_TO_NEIGHBORHOOD> ComparisonToNeighborhoodList { get; set; } = new List<COMPARISON_TO_NEIGHBORHOOD>();
    
        [XmlIgnore]
        public bool ComparisonToNeighborhoodListSpecified
        {
            get { return this.ComparisonToNeighborhoodList != null && this.ComparisonToNeighborhoodList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public COMPARISON_TO_NEIGHBORHOODS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
