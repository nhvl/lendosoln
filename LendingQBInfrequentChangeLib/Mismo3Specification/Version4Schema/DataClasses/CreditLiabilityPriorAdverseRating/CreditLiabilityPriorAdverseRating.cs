namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CREDIT_LIABILITY_PRIOR_ADVERSE_RATING
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditLiabilityPriorAdverseRatingCodeSpecified
                    || this.CreditLiabilityPriorAdverseRatingDateSpecified
                    || this.CreditLiabilityPriorAdverseRatingTypeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CreditLiabilityPriorAdverseRatingCode", Order = 0)]
        public MISMOCode CreditLiabilityPriorAdverseRatingCode { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityPriorAdverseRatingCodeSpecified
        {
            get { return this.CreditLiabilityPriorAdverseRatingCode != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityPriorAdverseRatingDate", Order = 1)]
        public MISMODate CreditLiabilityPriorAdverseRatingDate { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityPriorAdverseRatingDateSpecified
        {
            get { return this.CreditLiabilityPriorAdverseRatingDate != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityPriorAdverseRatingType", Order = 2)]
        public MISMOEnum<CreditLiabilityPriorAdverseRatingBase> CreditLiabilityPriorAdverseRatingType { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityPriorAdverseRatingTypeSpecified
        {
            get { return this.CreditLiabilityPriorAdverseRatingType != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public CREDIT_LIABILITY_PRIOR_ADVERSE_RATING_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
