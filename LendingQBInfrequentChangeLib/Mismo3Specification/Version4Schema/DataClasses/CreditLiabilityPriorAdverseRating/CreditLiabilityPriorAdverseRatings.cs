namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class CREDIT_LIABILITY_PRIOR_ADVERSE_RATINGS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditLiabilityPriorAdverseRatingListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CREDIT_LIABILITY_PRIOR_ADVERSE_RATING", Order = 0)]
        public List<CREDIT_LIABILITY_PRIOR_ADVERSE_RATING> CreditLiabilityPriorAdverseRatingList { get; set; } = new List<CREDIT_LIABILITY_PRIOR_ADVERSE_RATING>();
    
        [XmlIgnore]
        public bool CreditLiabilityPriorAdverseRatingListSpecified
        {
            get { return this.CreditLiabilityPriorAdverseRatingList != null && this.CreditLiabilityPriorAdverseRatingList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public CREDIT_LIABILITY_PRIOR_ADVERSE_RATINGS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
