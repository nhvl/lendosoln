namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class APPRAISER_LICENSE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AppraiserLicenseTypeSpecified
                    || this.AppraiserLicenseTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AppraiserLicenseType", Order = 0)]
        public MISMOEnum<AppraiserLicenseBase> AppraiserLicenseType { get; set; }
    
        [XmlIgnore]
        public bool AppraiserLicenseTypeSpecified
        {
            get { return this.AppraiserLicenseType != null; }
            set { }
        }
    
        [XmlElement("AppraiserLicenseTypeOtherDescription", Order = 1)]
        public MISMOString AppraiserLicenseTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool AppraiserLicenseTypeOtherDescriptionSpecified
        {
            get { return this.AppraiserLicenseTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public APPRAISER_LICENSE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
