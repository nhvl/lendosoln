namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PROPERTY_TAX_SPECIAL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PropertyTaxSpecialAmountSpecified
                    || this.PropertyTaxSpecialDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PropertyTaxSpecialAmount", Order = 0)]
        public MISMOAmount PropertyTaxSpecialAmount { get; set; }
    
        [XmlIgnore]
        public bool PropertyTaxSpecialAmountSpecified
        {
            get { return this.PropertyTaxSpecialAmount != null; }
            set { }
        }
    
        [XmlElement("PropertyTaxSpecialDescription", Order = 1)]
        public MISMOString PropertyTaxSpecialDescription { get; set; }
    
        [XmlIgnore]
        public bool PropertyTaxSpecialDescriptionSpecified
        {
            get { return this.PropertyTaxSpecialDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public PROPERTY_TAX_SPECIAL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
