namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class PROPERTY_TAX_SPECIALS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PropertyTaxSpecialListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PROPERTY_TAX_SPECIAL", Order = 0)]
        public List<PROPERTY_TAX_SPECIAL> PropertyTaxSpecialList { get; set; } = new List<PROPERTY_TAX_SPECIAL>();
    
        [XmlIgnore]
        public bool PropertyTaxSpecialListSpecified
        {
            get { return this.PropertyTaxSpecialList != null && this.PropertyTaxSpecialList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public PROPERTY_TAX_SPECIALS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
