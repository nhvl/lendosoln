namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class VALUATION_REQUEST
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AssignmentParametersSpecified
                    || this.ScopeOfWorkSpecified
                    || this.ValuationRequestDetailSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ASSIGNMENT_PARAMETERS", Order = 0)]
        public ASSIGNMENT_PARAMETERS AssignmentParameters { get; set; }
    
        [XmlIgnore]
        public bool AssignmentParametersSpecified
        {
            get { return this.AssignmentParameters != null && this.AssignmentParameters.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SCOPE_OF_WORK", Order = 1)]
        public SCOPE_OF_WORK ScopeOfWork { get; set; }
    
        [XmlIgnore]
        public bool ScopeOfWorkSpecified
        {
            get { return this.ScopeOfWork != null && this.ScopeOfWork.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("VALUATION_REQUEST_DETAIL", Order = 2)]
        public VALUATION_REQUEST_DETAIL ValuationRequestDetail { get; set; }
    
        [XmlIgnore]
        public bool ValuationRequestDetailSpecified
        {
            get { return this.ValuationRequestDetail != null && this.ValuationRequestDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public VALUATION_REQUEST_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
