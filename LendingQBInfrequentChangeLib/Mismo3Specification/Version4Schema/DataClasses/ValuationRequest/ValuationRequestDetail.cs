namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class VALUATION_REQUEST_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AppraiserFileIdentifierSpecified
                    || this.OrderingSystemNameSpecified
                    || this.PriorInterestIndicatorSpecified
                    || this.PropertyValuationFormIdentifierSpecified
                    || this.PropertyValuationFormTypeSpecified
                    || this.PropertyValuationFormTypeOtherDescriptionSpecified
                    || this.ValuationRequestActionTypeSpecified
                    || this.ValuationRequestActionTypeOtherDescriptionSpecified
                    || this.ValuationRequestCommentTextSpecified
                    || this.VendorOrderIdentifierSpecified
                    || this.VendorRelationshipTypeSpecified
                    || this.VendorRelationshipTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AppraiserFileIdentifier", Order = 0)]
        public MISMOIdentifier AppraiserFileIdentifier { get; set; }
    
        [XmlIgnore]
        public bool AppraiserFileIdentifierSpecified
        {
            get { return this.AppraiserFileIdentifier != null; }
            set { }
        }
    
        [XmlElement("OrderingSystemName", Order = 1)]
        public MISMOString OrderingSystemName { get; set; }
    
        [XmlIgnore]
        public bool OrderingSystemNameSpecified
        {
            get { return this.OrderingSystemName != null; }
            set { }
        }
    
        [XmlElement("PriorInterestIndicator", Order = 2)]
        public MISMOIndicator PriorInterestIndicator { get; set; }
    
        [XmlIgnore]
        public bool PriorInterestIndicatorSpecified
        {
            get { return this.PriorInterestIndicator != null; }
            set { }
        }
    
        [XmlElement("PropertyValuationFormIdentifier", Order = 3)]
        public MISMOIdentifier PropertyValuationFormIdentifier { get; set; }
    
        [XmlIgnore]
        public bool PropertyValuationFormIdentifierSpecified
        {
            get { return this.PropertyValuationFormIdentifier != null; }
            set { }
        }
    
        [XmlElement("PropertyValuationFormType", Order = 4)]
        public MISMOEnum<PropertyValuationFormBase> PropertyValuationFormType { get; set; }
    
        [XmlIgnore]
        public bool PropertyValuationFormTypeSpecified
        {
            get { return this.PropertyValuationFormType != null; }
            set { }
        }
    
        [XmlElement("PropertyValuationFormTypeOtherDescription", Order = 5)]
        public MISMOString PropertyValuationFormTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PropertyValuationFormTypeOtherDescriptionSpecified
        {
            get { return this.PropertyValuationFormTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ValuationRequestActionType", Order = 6)]
        public MISMOEnum<ValuationRequestActionBase> ValuationRequestActionType { get; set; }
    
        [XmlIgnore]
        public bool ValuationRequestActionTypeSpecified
        {
            get { return this.ValuationRequestActionType != null; }
            set { }
        }
    
        [XmlElement("ValuationRequestActionTypeOtherDescription", Order = 7)]
        public MISMOString ValuationRequestActionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ValuationRequestActionTypeOtherDescriptionSpecified
        {
            get { return this.ValuationRequestActionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ValuationRequestCommentText", Order = 8)]
        public MISMOString ValuationRequestCommentText { get; set; }
    
        [XmlIgnore]
        public bool ValuationRequestCommentTextSpecified
        {
            get { return this.ValuationRequestCommentText != null; }
            set { }
        }
    
        [XmlElement("VendorOrderIdentifier", Order = 9)]
        public MISMOIdentifier VendorOrderIdentifier { get; set; }
    
        [XmlIgnore]
        public bool VendorOrderIdentifierSpecified
        {
            get { return this.VendorOrderIdentifier != null; }
            set { }
        }
    
        [XmlElement("VendorRelationshipType", Order = 10)]
        public MISMOEnum<VendorRelationshipBase> VendorRelationshipType { get; set; }
    
        [XmlIgnore]
        public bool VendorRelationshipTypeSpecified
        {
            get { return this.VendorRelationshipType != null; }
            set { }
        }
    
        [XmlElement("VendorRelationshipTypeOtherDescription", Order = 11)]
        public MISMOString VendorRelationshipTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool VendorRelationshipTypeOtherDescriptionSpecified
        {
            get { return this.VendorRelationshipTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 12)]
        public VALUATION_REQUEST_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
