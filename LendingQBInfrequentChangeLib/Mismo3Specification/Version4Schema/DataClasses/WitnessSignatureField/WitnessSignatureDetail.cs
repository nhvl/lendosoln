namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class WITNESS_SIGNATURE_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SignatureFieldRequiredIndicatorSpecified
                    || this.SignatureTypeSpecified
                    || this.SignatureTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SignatureFieldRequiredIndicator", Order = 0)]
        public MISMOIndicator SignatureFieldRequiredIndicator { get; set; }
    
        [XmlIgnore]
        public bool SignatureFieldRequiredIndicatorSpecified
        {
            get { return this.SignatureFieldRequiredIndicator != null; }
            set { }
        }
    
        [XmlElement("SignatureType", Order = 1)]
        public MISMOEnum<SignatureBase> SignatureType { get; set; }
    
        [XmlIgnore]
        public bool SignatureTypeSpecified
        {
            get { return this.SignatureType != null; }
            set { }
        }
    
        [XmlElement("SignatureTypeOtherDescription", Order = 2)]
        public MISMOString SignatureTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool SignatureTypeOtherDescriptionSpecified
        {
            get { return this.SignatureTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public WITNESS_SIGNATURE_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
