namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class WITNESS_SIGNATURE_FIELD
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SignatureFieldReferenceSpecified
                    || this.WitnessSignatureDetailSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SIGNATURE_FIELD_REFERENCE", Order = 0)]
        public FIELD_REFERENCE SignatureFieldReference { get; set; }
    
        [XmlIgnore]
        public bool SignatureFieldReferenceSpecified
        {
            get { return this.SignatureFieldReference != null; }
            set { }
        }
    
        [XmlElement("WITNESS_SIGNATURE_DETAIL", Order = 1)]
        public WITNESS_SIGNATURE_DETAIL WitnessSignatureDetail { get; set; }
    
        [XmlIgnore]
        public bool WitnessSignatureDetailSpecified
        {
            get { return this.WitnessSignatureDetail != null && this.WitnessSignatureDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public WITNESS_SIGNATURE_FIELD_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
