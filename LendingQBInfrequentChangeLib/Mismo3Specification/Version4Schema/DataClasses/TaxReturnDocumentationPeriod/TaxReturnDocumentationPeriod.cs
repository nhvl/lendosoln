namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class TAX_RETURN_DOCUMENTATION_PERIOD
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.TaxReturnPeriodEndDateSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("TaxReturnPeriodEndDate", Order = 0)]
        public MISMODate TaxReturnPeriodEndDate { get; set; }
    
        [XmlIgnore]
        public bool TaxReturnPeriodEndDateSpecified
        {
            get { return this.TaxReturnPeriodEndDate != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public TAX_RETURN_DOCUMENTATION_PERIOD_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
