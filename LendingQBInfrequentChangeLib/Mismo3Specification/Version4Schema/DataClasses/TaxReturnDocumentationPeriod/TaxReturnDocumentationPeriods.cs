namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class TAX_RETURN_DOCUMENTATION_PERIODS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.TaxReturnDocumentationPeriodListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("TAX_RETURN_DOCUMENTATION_PERIOD", Order = 0)]
        public List<TAX_RETURN_DOCUMENTATION_PERIOD> TaxReturnDocumentationPeriodList { get; set; } = new List<TAX_RETURN_DOCUMENTATION_PERIOD>();
    
        [XmlIgnore]
        public bool TaxReturnDocumentationPeriodListSpecified
        {
            get { return this.TaxReturnDocumentationPeriodList != null && this.TaxReturnDocumentationPeriodList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public TAX_RETURN_DOCUMENTATION_PERIODS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
