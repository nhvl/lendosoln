namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class UNIT_CHARGE_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.UnitChargeAmountSpecified
                    || this.UnitChargePeriodTypeSpecified
                    || this.UnitChargePerSquareFootAmountSpecified
                    || this.UnitChargeRatingTypeSpecified
                    || this.UnitChargeRatingTypeOtherDescriptionSpecified
                    || this.UnitChargesIncludeUtilitiesIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("UnitChargeAmount", Order = 0)]
        public MISMOAmount UnitChargeAmount { get; set; }
    
        [XmlIgnore]
        public bool UnitChargeAmountSpecified
        {
            get { return this.UnitChargeAmount != null; }
            set { }
        }
    
        [XmlElement("UnitChargePeriodType", Order = 1)]
        public MISMOEnum<UnitChargePeriodBase> UnitChargePeriodType { get; set; }
    
        [XmlIgnore]
        public bool UnitChargePeriodTypeSpecified
        {
            get { return this.UnitChargePeriodType != null; }
            set { }
        }
    
        [XmlElement("UnitChargePerSquareFootAmount", Order = 2)]
        public MISMOAmount UnitChargePerSquareFootAmount { get; set; }
    
        [XmlIgnore]
        public bool UnitChargePerSquareFootAmountSpecified
        {
            get { return this.UnitChargePerSquareFootAmount != null; }
            set { }
        }
    
        [XmlElement("UnitChargeRatingType", Order = 3)]
        public MISMOEnum<UnitChargeRatingBase> UnitChargeRatingType { get; set; }
    
        [XmlIgnore]
        public bool UnitChargeRatingTypeSpecified
        {
            get { return this.UnitChargeRatingType != null; }
            set { }
        }
    
        [XmlElement("UnitChargeRatingTypeOtherDescription", Order = 4)]
        public MISMOString UnitChargeRatingTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool UnitChargeRatingTypeOtherDescriptionSpecified
        {
            get { return this.UnitChargeRatingTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("UnitChargesIncludeUtilitiesIndicator", Order = 5)]
        public MISMOIndicator UnitChargesIncludeUtilitiesIndicator { get; set; }
    
        [XmlIgnore]
        public bool UnitChargesIncludeUtilitiesIndicatorSpecified
        {
            get { return this.UnitChargesIncludeUtilitiesIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 6)]
        public UNIT_CHARGE_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
