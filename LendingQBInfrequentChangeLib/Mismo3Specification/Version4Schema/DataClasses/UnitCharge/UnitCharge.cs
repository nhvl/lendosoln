namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class UNIT_CHARGE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.UnitChargeDetailSpecified
                    || this.UnitChargeUtilitiesSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("UNIT_CHARGE_DETAIL", Order = 0)]
        public UNIT_CHARGE_DETAIL UnitChargeDetail { get; set; }
    
        [XmlIgnore]
        public bool UnitChargeDetailSpecified
        {
            get { return this.UnitChargeDetail != null && this.UnitChargeDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("UNIT_CHARGE_UTILITIES", Order = 1)]
        public UNIT_CHARGE_UTILITIES UnitChargeUtilities { get; set; }
    
        [XmlIgnore]
        public bool UnitChargeUtilitiesSpecified
        {
            get { return this.UnitChargeUtilities != null && this.UnitChargeUtilities.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public UNIT_CHARGE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
