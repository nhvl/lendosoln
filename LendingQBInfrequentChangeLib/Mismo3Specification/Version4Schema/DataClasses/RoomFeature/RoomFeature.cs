namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ROOM_FEATURE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ComponentAdjustmentAmountSpecified
                    || this.ComponentClassificationTypeSpecified
                    || this.ConditionRatingDescriptionSpecified
                    || this.ConditionRatingTypeSpecified
                    || this.MaterialDescriptionSpecified
                    || this.QualityRatingDescriptionSpecified
                    || this.QualityRatingTypeSpecified
                    || this.RoomFeatureDescriptionSpecified
                    || this.RoomFeatureTypeSpecified
                    || this.RoomFeatureTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ComponentAdjustmentAmount", Order = 0)]
        public MISMOAmount ComponentAdjustmentAmount { get; set; }
    
        [XmlIgnore]
        public bool ComponentAdjustmentAmountSpecified
        {
            get { return this.ComponentAdjustmentAmount != null; }
            set { }
        }
    
        [XmlElement("ComponentClassificationType", Order = 1)]
        public MISMOEnum<ComponentClassificationBase> ComponentClassificationType { get; set; }
    
        [XmlIgnore]
        public bool ComponentClassificationTypeSpecified
        {
            get { return this.ComponentClassificationType != null; }
            set { }
        }
    
        [XmlElement("ConditionRatingDescription", Order = 2)]
        public MISMOString ConditionRatingDescription { get; set; }
    
        [XmlIgnore]
        public bool ConditionRatingDescriptionSpecified
        {
            get { return this.ConditionRatingDescription != null; }
            set { }
        }
    
        [XmlElement("ConditionRatingType", Order = 3)]
        public MISMOEnum<ConditionRatingBase> ConditionRatingType { get; set; }
    
        [XmlIgnore]
        public bool ConditionRatingTypeSpecified
        {
            get { return this.ConditionRatingType != null; }
            set { }
        }
    
        [XmlElement("MaterialDescription", Order = 4)]
        public MISMOString MaterialDescription { get; set; }
    
        [XmlIgnore]
        public bool MaterialDescriptionSpecified
        {
            get { return this.MaterialDescription != null; }
            set { }
        }
    
        [XmlElement("QualityRatingDescription", Order = 5)]
        public MISMOString QualityRatingDescription { get; set; }
    
        [XmlIgnore]
        public bool QualityRatingDescriptionSpecified
        {
            get { return this.QualityRatingDescription != null; }
            set { }
        }
    
        [XmlElement("QualityRatingType", Order = 6)]
        public MISMOEnum<QualityRatingBase> QualityRatingType { get; set; }
    
        [XmlIgnore]
        public bool QualityRatingTypeSpecified
        {
            get { return this.QualityRatingType != null; }
            set { }
        }
    
        [XmlElement("RoomFeatureDescription", Order = 7)]
        public MISMOString RoomFeatureDescription { get; set; }
    
        [XmlIgnore]
        public bool RoomFeatureDescriptionSpecified
        {
            get { return this.RoomFeatureDescription != null; }
            set { }
        }
    
        [XmlElement("RoomFeatureType", Order = 8)]
        public MISMOEnum<RoomFeatureBase> RoomFeatureType { get; set; }
    
        [XmlIgnore]
        public bool RoomFeatureTypeSpecified
        {
            get { return this.RoomFeatureType != null; }
            set { }
        }
    
        [XmlElement("RoomFeatureTypeOtherDescription", Order = 9)]
        public MISMOString RoomFeatureTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool RoomFeatureTypeOtherDescriptionSpecified
        {
            get { return this.RoomFeatureTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 10)]
        public ROOM_FEATURE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
