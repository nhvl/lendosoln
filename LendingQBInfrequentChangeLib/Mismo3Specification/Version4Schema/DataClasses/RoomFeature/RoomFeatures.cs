namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class ROOM_FEATURES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.RoomFeatureListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ROOM_FEATURE", Order = 0)]
        public List<ROOM_FEATURE> RoomFeatureList { get; set; } = new List<ROOM_FEATURE>();
    
        [XmlIgnore]
        public bool RoomFeatureListSpecified
        {
            get { return this.RoomFeatureList != null && this.RoomFeatureList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public ROOM_FEATURES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
