namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class STAKEHOLDER_SIGNATURE_FIELD_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MutuallyExclusiveSignatureGroupNameSpecified
                    || this.SignatureFieldMarkTypeSpecified
                    || this.SignatureFieldRequiredIndicatorSpecified
                    || this.SignatureTypeSpecified
                    || this.SignatureTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MutuallyExclusiveSignatureGroupName", Order = 0)]
        public MISMOString MutuallyExclusiveSignatureGroupName { get; set; }
    
        [XmlIgnore]
        public bool MutuallyExclusiveSignatureGroupNameSpecified
        {
            get { return this.MutuallyExclusiveSignatureGroupName != null; }
            set { }
        }
    
        [XmlElement("SignatureFieldMarkType", Order = 1)]
        public MISMOEnum<SignatureFieldMarkBase> SignatureFieldMarkType { get; set; }
    
        [XmlIgnore]
        public bool SignatureFieldMarkTypeSpecified
        {
            get { return this.SignatureFieldMarkType != null; }
            set { }
        }
    
        [XmlElement("SignatureFieldRequiredIndicator", Order = 2)]
        public MISMOIndicator SignatureFieldRequiredIndicator { get; set; }
    
        [XmlIgnore]
        public bool SignatureFieldRequiredIndicatorSpecified
        {
            get { return this.SignatureFieldRequiredIndicator != null; }
            set { }
        }
    
        [XmlElement("SignatureType", Order = 3)]
        public MISMOEnum<SignatureBase> SignatureType { get; set; }
    
        [XmlIgnore]
        public bool SignatureTypeSpecified
        {
            get { return this.SignatureType != null; }
            set { }
        }
    
        [XmlElement("SignatureTypeOtherDescription", Order = 4)]
        public MISMOString SignatureTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool SignatureTypeOtherDescriptionSpecified
        {
            get { return this.SignatureTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public STAKEHOLDER_SIGNATURE_FIELD_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
