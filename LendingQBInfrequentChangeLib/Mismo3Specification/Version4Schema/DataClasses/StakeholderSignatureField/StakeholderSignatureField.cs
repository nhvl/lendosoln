namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class STAKEHOLDER_SIGNATURE_FIELD
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SIGNATURE_ABOVE_LINE_FIELD_REFERENCESpecified
                    || this.SIGNATURE_AREA_FIELD_REFERENCESpecified
                    || this.SIGNATURE_BELOW_LINE_FIELD_REFERENCESpecified
                    || this.SIGNATURE_PRESENTATION_FIELD_REFERENCESpecified
                    || this.StakeholderSignatureFieldDetailSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SIGNATURE_ABOVE_LINE_FIELD_REFERENCE", Order = 0)]
        public FIELD_REFERENCE SIGNATURE_ABOVE_LINE_FIELD_REFERENCE { get; set; }
    
        [XmlIgnore]
        public bool SIGNATURE_ABOVE_LINE_FIELD_REFERENCESpecified
        {
            get { return this.SIGNATURE_ABOVE_LINE_FIELD_REFERENCE != null; }
            set { }
        }
    
        [XmlElement("SIGNATURE_AREA_FIELD_REFERENCE", Order = 1)]
        public FIELD_REFERENCE SIGNATURE_AREA_FIELD_REFERENCE { get; set; }
    
        [XmlIgnore]
        public bool SIGNATURE_AREA_FIELD_REFERENCESpecified
        {
            get { return this.SIGNATURE_AREA_FIELD_REFERENCE != null; }
            set { }
        }
    
        [XmlElement("SIGNATURE_BELOW_LINE_FIELD_REFERENCE", Order = 2)]
        public FIELD_REFERENCE SIGNATURE_BELOW_LINE_FIELD_REFERENCE { get; set; }
    
        [XmlIgnore]
        public bool SIGNATURE_BELOW_LINE_FIELD_REFERENCESpecified
        {
            get { return this.SIGNATURE_BELOW_LINE_FIELD_REFERENCE != null; }
            set { }
        }
    
        [XmlElement("SIGNATURE_PRESENTATION_FIELD_REFERENCE", Order = 3)]
        public FIELD_REFERENCE SIGNATURE_PRESENTATION_FIELD_REFERENCE { get; set; }
    
        [XmlIgnore]
        public bool SIGNATURE_PRESENTATION_FIELD_REFERENCESpecified
        {
            get { return this.SIGNATURE_PRESENTATION_FIELD_REFERENCE != null; }
            set { }
        }
    
        [XmlElement("STAKEHOLDER_SIGNATURE_FIELD_DETAIL", Order = 4)]
        public STAKEHOLDER_SIGNATURE_FIELD_DETAIL StakeholderSignatureFieldDetail { get; set; }
    
        [XmlIgnore]
        public bool StakeholderSignatureFieldDetailSpecified
        {
            get { return this.StakeholderSignatureFieldDetail != null && this.StakeholderSignatureFieldDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public STAKEHOLDER_SIGNATURE_FIELD_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
