namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class TITLE_EXISTING_LOANS_AND_LIABILITIES_INFORMATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LiabilitiesSpecified
                    || this.LoansSpecified
                    || this.RelationshipsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LIABILITIES", Order = 0)]
        public LIABILITIES Liabilities { get; set; }
    
        [XmlIgnore]
        public bool LiabilitiesSpecified
        {
            get { return this.Liabilities != null && this.Liabilities.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("LOANS", Order = 1)]
        public LOANS Loans { get; set; }
    
        [XmlIgnore]
        public bool LoansSpecified
        {
            get { return this.Loans != null && this.Loans.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("RELATIONSHIPS", Order = 2)]
        public RELATIONSHIPS Relationships { get; set; }
    
        [XmlIgnore]
        public bool RelationshipsSpecified
        {
            get { return this.Relationships != null && this.Relationships.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public TITLE_EXISTING_LOANS_AND_LIABILITIES_INFORMATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
