namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class DEAL_SET_SERVICES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DealSetServiceListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DEAL_SET_SERVICE", Order = 0)]
        public List<DEAL_SET_SERVICE> DealSetServiceList { get; set; } = new List<DEAL_SET_SERVICE>();
    
        [XmlIgnore]
        public bool DealSetServiceListSpecified
        {
            get { return this.DealSetServiceList != null && this.DealSetServiceList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public DEAL_SET_SERVICES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
