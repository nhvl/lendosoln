namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    public class DEAL_SET_SERVICE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                if (Mismo3Utilities.ExceedsThreshold(1,
                    new List<bool> { this.DataModificationSpecified, this.ServicingTransferSpecified, this.WorkoutEvaluationSpecified }))
                {
                    throw new System.Xml.Schema.XmlSchemaValidationException(
                        Mismo3Utilities.GetXSDChoiceExceptionMessage(
                        "DEAL_SET_SERVICE",
                        new List<string> { "DATA_MODIFICATION", "SERVICING_TRANSFER", "WORKOUT_EVALUATION" }));
                }

                return this.DataModificationSpecified
                    || this.ReportingInformationSpecified
                    || this.ServicingTransferSpecified
                    || this.WorkoutEvaluationSpecified
                    || this.ErrorsSpecified
                    || this.StatusesSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DATA_MODIFICATION", Order = 0)]
        public DATA_MODIFICATION DataModification { get; set; }
    
        [XmlIgnore]
        public bool DataModificationSpecified
        {
            get { return this.DataModification != null && this.DataModification.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("REPORTING_INFORMATION", Order = 1)]
        public REPORTING_INFORMATION ReportingInformation { get; set; }
    
        [XmlIgnore]
        public bool ReportingInformationSpecified
        {
            get { return this.ReportingInformation != null && this.ReportingInformation.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SERVICING_TRANSFER", Order = 2)]
        public SERVICING_TRANSFER ServicingTransfer { get; set; }
    
        [XmlIgnore]
        public bool ServicingTransferSpecified
        {
            get { return this.ServicingTransfer != null && this.ServicingTransfer.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("WORKOUT_EVALUATION", Order = 3)]
        public WORKOUT_EVALUATION WorkoutEvaluation { get; set; }
    
        [XmlIgnore]
        public bool WorkoutEvaluationSpecified
        {
            get { return this.WorkoutEvaluation != null && this.WorkoutEvaluation.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("ERRORS", Order = 4)]
        public ERRORS Errors { get; set; }
    
        [XmlIgnore]
        public bool ErrorsSpecified
        {
            get { return this.Errors != null && this.Errors.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("STATUSES", Order = 5)]
        public STATUSES Statuses { get; set; }
    
        [XmlIgnore]
        public bool StatusesSpecified
        {
            get { return this.Statuses != null && this.Statuses.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 6)]
        public DEAL_SET_SERVICE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
