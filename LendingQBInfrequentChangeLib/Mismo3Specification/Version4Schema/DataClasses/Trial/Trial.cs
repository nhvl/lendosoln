namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class TRIAL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.HardshipAffidavitWaivedIndicatorSpecified
                    || this.PriorToTrialPlanExpectedPaymentAmountSpecified
                    || this.ServicerAttestedLastTrialPaymentIndicatorSpecified
                    || this.TrialPaymentCurrentIndicatorSpecified
                    || this.TrialPeriodTotalPrincipalAppliedAmountSpecified
                    || this.TrialPlanAgreementSignedDateSpecified
                    || this.TrialPlanCommitmentDateSpecified
                    || this.TrialPlanEstimatedPaymentAmountSpecified
                    || this.TrialPlanExtendableIndicatorSpecified
                    || this.TrialPlanExtendedIndicatorSpecified
                    || this.TrialPlanExtendedMonthsCountSpecified
                    || this.TrialPlanExtensionAdditionalPaymentRequiredIndicatorSpecified
                    || this.TrialPlanFirstPaymentReceivedDateSpecified
                    || this.TrialPlanIncomeVerificationWaivedIndicatorSpecified
                    || this.TrialPlanInterestAppliedAmountSpecified
                    || this.TrialPlanPaymentRemainingAmountSpecified
                    || this.TrialPlanPaymentsReceivedCountSpecified
                    || this.TrialPlanScheduledPaymentAmountSpecified
                    || this.TrialPlanUnsuccessfulReasonTypeSpecified
                    || this.TrialPlanUnsuccessfulReasonTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("HardshipAffidavitWaivedIndicator", Order = 0)]
        public MISMOIndicator HardshipAffidavitWaivedIndicator { get; set; }
    
        [XmlIgnore]
        public bool HardshipAffidavitWaivedIndicatorSpecified
        {
            get { return this.HardshipAffidavitWaivedIndicator != null; }
            set { }
        }
    
        [XmlElement("PriorToTrialPlanExpectedPaymentAmount", Order = 1)]
        public MISMOAmount PriorToTrialPlanExpectedPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool PriorToTrialPlanExpectedPaymentAmountSpecified
        {
            get { return this.PriorToTrialPlanExpectedPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("ServicerAttestedLastTrialPaymentIndicator", Order = 2)]
        public MISMOIndicator ServicerAttestedLastTrialPaymentIndicator { get; set; }
    
        [XmlIgnore]
        public bool ServicerAttestedLastTrialPaymentIndicatorSpecified
        {
            get { return this.ServicerAttestedLastTrialPaymentIndicator != null; }
            set { }
        }
    
        [XmlElement("TrialPaymentCurrentIndicator", Order = 3)]
        public MISMOIndicator TrialPaymentCurrentIndicator { get; set; }
    
        [XmlIgnore]
        public bool TrialPaymentCurrentIndicatorSpecified
        {
            get { return this.TrialPaymentCurrentIndicator != null; }
            set { }
        }
    
        [XmlElement("TrialPeriodTotalPrincipalAppliedAmount", Order = 4)]
        public MISMOAmount TrialPeriodTotalPrincipalAppliedAmount { get; set; }
    
        [XmlIgnore]
        public bool TrialPeriodTotalPrincipalAppliedAmountSpecified
        {
            get { return this.TrialPeriodTotalPrincipalAppliedAmount != null; }
            set { }
        }
    
        [XmlElement("TrialPlanAgreementSignedDate", Order = 5)]
        public MISMODate TrialPlanAgreementSignedDate { get; set; }
    
        [XmlIgnore]
        public bool TrialPlanAgreementSignedDateSpecified
        {
            get { return this.TrialPlanAgreementSignedDate != null; }
            set { }
        }
    
        [XmlElement("TrialPlanCommitmentDate", Order = 6)]
        public MISMODate TrialPlanCommitmentDate { get; set; }
    
        [XmlIgnore]
        public bool TrialPlanCommitmentDateSpecified
        {
            get { return this.TrialPlanCommitmentDate != null; }
            set { }
        }
    
        [XmlElement("TrialPlanEstimatedPaymentAmount", Order = 7)]
        public MISMOAmount TrialPlanEstimatedPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool TrialPlanEstimatedPaymentAmountSpecified
        {
            get { return this.TrialPlanEstimatedPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("TrialPlanExtendableIndicator", Order = 8)]
        public MISMOIndicator TrialPlanExtendableIndicator { get; set; }
    
        [XmlIgnore]
        public bool TrialPlanExtendableIndicatorSpecified
        {
            get { return this.TrialPlanExtendableIndicator != null; }
            set { }
        }
    
        [XmlElement("TrialPlanExtendedIndicator", Order = 9)]
        public MISMOIndicator TrialPlanExtendedIndicator { get; set; }
    
        [XmlIgnore]
        public bool TrialPlanExtendedIndicatorSpecified
        {
            get { return this.TrialPlanExtendedIndicator != null; }
            set { }
        }
    
        [XmlElement("TrialPlanExtendedMonthsCount", Order = 10)]
        public MISMOCount TrialPlanExtendedMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool TrialPlanExtendedMonthsCountSpecified
        {
            get { return this.TrialPlanExtendedMonthsCount != null; }
            set { }
        }
    
        [XmlElement("TrialPlanExtensionAdditionalPaymentRequiredIndicator", Order = 11)]
        public MISMOIndicator TrialPlanExtensionAdditionalPaymentRequiredIndicator { get; set; }
    
        [XmlIgnore]
        public bool TrialPlanExtensionAdditionalPaymentRequiredIndicatorSpecified
        {
            get { return this.TrialPlanExtensionAdditionalPaymentRequiredIndicator != null; }
            set { }
        }
    
        [XmlElement("TrialPlanFirstPaymentReceivedDate", Order = 12)]
        public MISMODate TrialPlanFirstPaymentReceivedDate { get; set; }
    
        [XmlIgnore]
        public bool TrialPlanFirstPaymentReceivedDateSpecified
        {
            get { return this.TrialPlanFirstPaymentReceivedDate != null; }
            set { }
        }
    
        [XmlElement("TrialPlanIncomeVerificationWaivedIndicator", Order = 13)]
        public MISMOIndicator TrialPlanIncomeVerificationWaivedIndicator { get; set; }
    
        [XmlIgnore]
        public bool TrialPlanIncomeVerificationWaivedIndicatorSpecified
        {
            get { return this.TrialPlanIncomeVerificationWaivedIndicator != null; }
            set { }
        }
    
        [XmlElement("TrialPlanInterestAppliedAmount", Order = 14)]
        public MISMOAmount TrialPlanInterestAppliedAmount { get; set; }
    
        [XmlIgnore]
        public bool TrialPlanInterestAppliedAmountSpecified
        {
            get { return this.TrialPlanInterestAppliedAmount != null; }
            set { }
        }
    
        [XmlElement("TrialPlanPaymentRemainingAmount", Order = 15)]
        public MISMOAmount TrialPlanPaymentRemainingAmount { get; set; }
    
        [XmlIgnore]
        public bool TrialPlanPaymentRemainingAmountSpecified
        {
            get { return this.TrialPlanPaymentRemainingAmount != null; }
            set { }
        }
    
        [XmlElement("TrialPlanPaymentsReceivedCount", Order = 16)]
        public MISMOCount TrialPlanPaymentsReceivedCount { get; set; }
    
        [XmlIgnore]
        public bool TrialPlanPaymentsReceivedCountSpecified
        {
            get { return this.TrialPlanPaymentsReceivedCount != null; }
            set { }
        }
    
        [XmlElement("TrialPlanScheduledPaymentAmount", Order = 17)]
        public MISMOAmount TrialPlanScheduledPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool TrialPlanScheduledPaymentAmountSpecified
        {
            get { return this.TrialPlanScheduledPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("TrialPlanUnsuccessfulReasonType", Order = 18)]
        public MISMOEnum<TrialPlanUnsuccessfulReasonBase> TrialPlanUnsuccessfulReasonType { get; set; }
    
        [XmlIgnore]
        public bool TrialPlanUnsuccessfulReasonTypeSpecified
        {
            get { return this.TrialPlanUnsuccessfulReasonType != null; }
            set { }
        }
    
        [XmlElement("TrialPlanUnsuccessfulReasonTypeOtherDescription", Order = 19)]
        public MISMOString TrialPlanUnsuccessfulReasonTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool TrialPlanUnsuccessfulReasonTypeOtherDescriptionSpecified
        {
            get { return this.TrialPlanUnsuccessfulReasonTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 20)]
        public TRIAL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
