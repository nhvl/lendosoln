namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class OFFERING_HISTORY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.OfferingHistoryAmountSpecified
                    || this.OfferingHistoryDateSpecified
                    || this.OfferingHistoryTypeSpecified
                    || this.OfferingHistoryTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("OfferingHistoryAmount", Order = 0)]
        public MISMOAmount OfferingHistoryAmount { get; set; }
    
        [XmlIgnore]
        public bool OfferingHistoryAmountSpecified
        {
            get { return this.OfferingHistoryAmount != null; }
            set { }
        }
    
        [XmlElement("OfferingHistoryDate", Order = 1)]
        public MISMODate OfferingHistoryDate { get; set; }
    
        [XmlIgnore]
        public bool OfferingHistoryDateSpecified
        {
            get { return this.OfferingHistoryDate != null; }
            set { }
        }
    
        [XmlElement("OfferingHistoryType", Order = 2)]
        public MISMOEnum<OfferingHistoryBase> OfferingHistoryType { get; set; }
    
        [XmlIgnore]
        public bool OfferingHistoryTypeSpecified
        {
            get { return this.OfferingHistoryType != null; }
            set { }
        }
    
        [XmlElement("OfferingHistoryTypeOtherDescription", Order = 3)]
        public MISMOString OfferingHistoryTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool OfferingHistoryTypeOtherDescriptionSpecified
        {
            get { return this.OfferingHistoryTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public OFFERING_HISTORY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
