namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class OFFERING_HISTORIES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.OfferingHistoryListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("OFFERING_HISTORY", Order = 0)]
        public List<OFFERING_HISTORY> OfferingHistoryList { get; set; } = new List<OFFERING_HISTORY>();
    
        [XmlIgnore]
        public bool OfferingHistoryListSpecified
        {
            get { return this.OfferingHistoryList != null && this.OfferingHistoryList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public OFFERING_HISTORIES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
