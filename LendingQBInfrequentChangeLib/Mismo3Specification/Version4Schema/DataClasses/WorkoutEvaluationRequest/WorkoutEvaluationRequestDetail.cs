namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class WORKOUT_EVALUATION_REQUEST_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LoansSubmittedCountSpecified
                    || this.RequestBatchIdentifierSpecified
                    || this.ServiceRequestDatetimeSpecified
                    || this.ServiceRequestReasonIdentifierSpecified
                    || this.ServicerLossMitigationSoftwarePlatformNameSpecified
                    || this.ServicerLossMitigationSoftwarePlatformVersionIdentifierSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LoansSubmittedCount", Order = 0)]
        public MISMOCount LoansSubmittedCount { get; set; }
    
        [XmlIgnore]
        public bool LoansSubmittedCountSpecified
        {
            get { return this.LoansSubmittedCount != null; }
            set { }
        }
    
        [XmlElement("RequestBatchIdentifier", Order = 1)]
        public MISMOIdentifier RequestBatchIdentifier { get; set; }
    
        [XmlIgnore]
        public bool RequestBatchIdentifierSpecified
        {
            get { return this.RequestBatchIdentifier != null; }
            set { }
        }
    
        [XmlElement("ServiceRequestDatetime", Order = 2)]
        public MISMODatetime ServiceRequestDatetime { get; set; }
    
        [XmlIgnore]
        public bool ServiceRequestDatetimeSpecified
        {
            get { return this.ServiceRequestDatetime != null; }
            set { }
        }
    
        [XmlElement("ServiceRequestReasonIdentifier", Order = 3)]
        public MISMOIdentifier ServiceRequestReasonIdentifier { get; set; }
    
        [XmlIgnore]
        public bool ServiceRequestReasonIdentifierSpecified
        {
            get { return this.ServiceRequestReasonIdentifier != null; }
            set { }
        }
    
        [XmlElement("ServicerLossMitigationSoftwarePlatformName", Order = 4)]
        public MISMOString ServicerLossMitigationSoftwarePlatformName { get; set; }
    
        [XmlIgnore]
        public bool ServicerLossMitigationSoftwarePlatformNameSpecified
        {
            get { return this.ServicerLossMitigationSoftwarePlatformName != null; }
            set { }
        }
    
        [XmlElement("ServicerLossMitigationSoftwarePlatformVersionIdentifier", Order = 5)]
        public MISMOIdentifier ServicerLossMitigationSoftwarePlatformVersionIdentifier { get; set; }
    
        [XmlIgnore]
        public bool ServicerLossMitigationSoftwarePlatformVersionIdentifierSpecified
        {
            get { return this.ServicerLossMitigationSoftwarePlatformVersionIdentifier != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 6)]
        public WORKOUT_EVALUATION_REQUEST_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
