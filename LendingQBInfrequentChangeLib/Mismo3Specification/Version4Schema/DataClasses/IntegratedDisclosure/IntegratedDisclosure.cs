namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class INTEGRATED_DISCLOSURE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CashToCloseItemsSpecified
                    || this.EstimatedPropertyCostSpecified
                    || this.IntegratedDisclosureDetailSpecified
                    || this.IntegratedDisclosureSectionSummariesSpecified
                    || this.OtherLoanConsiderationsAndDisclosuresItemsSpecified
                    || this.ProjectedPaymentsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CASH_TO_CLOSE_ITEMS", Order = 0)]
        public CASH_TO_CLOSE_ITEMS CashToCloseItems { get; set; }
    
        [XmlIgnore]
        public bool CashToCloseItemsSpecified
        {
            get { return this.CashToCloseItems != null && this.CashToCloseItems.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("ESTIMATED_PROPERTY_COST", Order = 1)]
        public ESTIMATED_PROPERTY_COST EstimatedPropertyCost { get; set; }
    
        [XmlIgnore]
        public bool EstimatedPropertyCostSpecified
        {
            get { return this.EstimatedPropertyCost != null && this.EstimatedPropertyCost.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("INTEGRATED_DISCLOSURE_DETAIL", Order = 2)]
        public INTEGRATED_DISCLOSURE_DETAIL IntegratedDisclosureDetail { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureDetailSpecified
        {
            get { return this.IntegratedDisclosureDetail != null && this.IntegratedDisclosureDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("INTEGRATED_DISCLOSURE_SECTION_SUMMARIES", Order = 3)]
        public INTEGRATED_DISCLOSURE_SECTION_SUMMARIES IntegratedDisclosureSectionSummaries { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureSectionSummariesSpecified
        {
            get { return this.IntegratedDisclosureSectionSummaries != null && this.IntegratedDisclosureSectionSummaries.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("OTHER_LOAN_CONSIDERATIONS_AND_DISCLOSURES_ITEMS", Order = 4)]
        public OTHER_LOAN_CONSIDERATIONS_AND_DISCLOSURES_ITEMS OtherLoanConsiderationsAndDisclosuresItems { get; set; }
    
        [XmlIgnore]
        public bool OtherLoanConsiderationsAndDisclosuresItemsSpecified
        {
            get { return this.OtherLoanConsiderationsAndDisclosuresItems != null && this.OtherLoanConsiderationsAndDisclosuresItems.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PROJECTED_PAYMENTS", Order = 5)]
        public PROJECTED_PAYMENTS ProjectedPayments { get; set; }
    
        [XmlIgnore]
        public bool ProjectedPaymentsSpecified
        {
            get { return this.ProjectedPayments != null && this.ProjectedPayments.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 6)]
        public INTEGRATED_DISCLOSURE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
