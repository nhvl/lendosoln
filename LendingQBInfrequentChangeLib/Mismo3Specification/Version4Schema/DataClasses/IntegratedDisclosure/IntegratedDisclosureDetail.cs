namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class INTEGRATED_DISCLOSURE_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.FirstYearTotalEscrowPaymentAmountSpecified
                    || this.FirstYearTotalEscrowPaymentDescriptionSpecified
                    || this.FirstYearTotalNonEscrowPaymentAmountSpecified
                    || this.FirstYearTotalNonEscrowPaymentDescriptionSpecified
                    || this.FiveYearPrincipalReductionComparisonAmountSpecified
                    || this.FiveYearTotalOfPaymentsComparisonAmountSpecified
                    || this.IncludeSignatureLinesIndicatorSpecified
                    || this.IntegratedDisclosureDocumentTypeSpecified
                    || this.IntegratedDisclosureDocumentTypeOtherDescriptionSpecified
                    || this.IntegratedDisclosureEstimatedClosingCostsExpirationDatetimeSpecified
                    || this.IntegratedDisclosureHomeEquityLoanIndicatorSpecified
                    || this.IntegratedDisclosureInitialPrincipalAndInterestPaymentAmountSpecified
                    || this.IntegratedDisclosureIssuedDateSpecified
                    || this.IntegratedDisclosureLoanProductDescriptionSpecified
                    || this.IntegratedDisclosureReceivedDateSpecified
                    || this.TotalOfPaymentsAndLoanCostsAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("FirstYearTotalEscrowPaymentAmount", Order = 0)]
        public MISMOAmount FirstYearTotalEscrowPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool FirstYearTotalEscrowPaymentAmountSpecified
        {
            get { return this.FirstYearTotalEscrowPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("FirstYearTotalEscrowPaymentDescription", Order = 1)]
        public MISMOString FirstYearTotalEscrowPaymentDescription { get; set; }
    
        [XmlIgnore]
        public bool FirstYearTotalEscrowPaymentDescriptionSpecified
        {
            get { return this.FirstYearTotalEscrowPaymentDescription != null; }
            set { }
        }
    
        [XmlElement("FirstYearTotalNonEscrowPaymentAmount", Order = 2)]
        public MISMOAmount FirstYearTotalNonEscrowPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool FirstYearTotalNonEscrowPaymentAmountSpecified
        {
            get { return this.FirstYearTotalNonEscrowPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("FirstYearTotalNonEscrowPaymentDescription", Order = 3)]
        public MISMOString FirstYearTotalNonEscrowPaymentDescription { get; set; }
    
        [XmlIgnore]
        public bool FirstYearTotalNonEscrowPaymentDescriptionSpecified
        {
            get { return this.FirstYearTotalNonEscrowPaymentDescription != null; }
            set { }
        }
    
        [XmlElement("FiveYearPrincipalReductionComparisonAmount", Order = 4)]
        public MISMOAmount FiveYearPrincipalReductionComparisonAmount { get; set; }
    
        [XmlIgnore]
        public bool FiveYearPrincipalReductionComparisonAmountSpecified
        {
            get { return this.FiveYearPrincipalReductionComparisonAmount != null; }
            set { }
        }
    
        [XmlElement("FiveYearTotalOfPaymentsComparisonAmount", Order = 5)]
        public MISMOAmount FiveYearTotalOfPaymentsComparisonAmount { get; set; }
    
        [XmlIgnore]
        public bool FiveYearTotalOfPaymentsComparisonAmountSpecified
        {
            get { return this.FiveYearTotalOfPaymentsComparisonAmount != null; }
            set { }
        }
    
        [XmlElement("IncludeSignatureLinesIndicator", Order = 6)]
        public MISMOIndicator IncludeSignatureLinesIndicator { get; set; }
    
        [XmlIgnore]
        public bool IncludeSignatureLinesIndicatorSpecified
        {
            get { return this.IncludeSignatureLinesIndicator != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureDocumentType", Order = 7)]
        public MISMOEnum<IntegratedDisclosureDocumentBase> IntegratedDisclosureDocumentType { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureDocumentTypeSpecified
        {
            get { return this.IntegratedDisclosureDocumentType != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureDocumentTypeOtherDescription", Order = 8)]
        public MISMOString IntegratedDisclosureDocumentTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureDocumentTypeOtherDescriptionSpecified
        {
            get { return this.IntegratedDisclosureDocumentTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureEstimatedClosingCostsExpirationDatetime", Order = 9)]
        public MISMODatetime IntegratedDisclosureEstimatedClosingCostsExpirationDatetime { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureEstimatedClosingCostsExpirationDatetimeSpecified
        {
            get { return this.IntegratedDisclosureEstimatedClosingCostsExpirationDatetime != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureHomeEquityLoanIndicator", Order = 10)]
        public MISMOIndicator IntegratedDisclosureHomeEquityLoanIndicator { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureHomeEquityLoanIndicatorSpecified
        {
            get { return this.IntegratedDisclosureHomeEquityLoanIndicator != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureInitialPrincipalAndInterestPaymentAmount", Order = 11)]
        public MISMOAmount IntegratedDisclosureInitialPrincipalAndInterestPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureInitialPrincipalAndInterestPaymentAmountSpecified
        {
            get { return this.IntegratedDisclosureInitialPrincipalAndInterestPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureIssuedDate", Order = 12)]
        public MISMODate IntegratedDisclosureIssuedDate { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureIssuedDateSpecified
        {
            get { return this.IntegratedDisclosureIssuedDate != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureLoanProductDescription", Order = 13)]
        public MISMOString IntegratedDisclosureLoanProductDescription { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureLoanProductDescriptionSpecified
        {
            get { return this.IntegratedDisclosureLoanProductDescription != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureReceivedDate", Order = 14)]
        public MISMODate IntegratedDisclosureReceivedDate { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureReceivedDateSpecified
        {
            get { return this.IntegratedDisclosureReceivedDate != null; }
            set { }
        }
    
        [XmlElement("TotalOfPaymentsAndLoanCostsAmount", Order = 15)]
        public MISMOAmount TotalOfPaymentsAndLoanCostsAmount { get; set; }
    
        [XmlIgnore]
        public bool TotalOfPaymentsAndLoanCostsAmountSpecified
        {
            get { return this.TotalOfPaymentsAndLoanCostsAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 16)]
        public INTEGRATED_DISCLOSURE_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
