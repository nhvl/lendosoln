namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class DOCUMENT_SET
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DocumentSetIdentifiersSpecified
                    || this.DocumentSetUsagesSpecified
                    || this.DocumentsSpecified
                    || this.ForeignObjectsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DOCUMENT_SET_IDENTIFIERS", Order = 0)]
        public DOCUMENT_SET_IDENTIFIERS DocumentSetIdentifiers { get; set; }
    
        [XmlIgnore]
        public bool DocumentSetIdentifiersSpecified
        {
            get { return this.DocumentSetIdentifiers != null && this.DocumentSetIdentifiers.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("DOCUMENT_SET_USAGES", Order = 1)]
        public DOCUMENT_SET_USAGES DocumentSetUsages { get; set; }
    
        [XmlIgnore]
        public bool DocumentSetUsagesSpecified
        {
            get { return this.DocumentSetUsages != null && this.DocumentSetUsages.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("DOCUMENTS", Order = 2)]
        public DOCUMENTS Documents { get; set; }
    
        [XmlIgnore]
        public bool DocumentsSpecified
        {
            get { return this.Documents != null && this.Documents.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("FOREIGN_OBJECTS", Order = 3)]
        public FOREIGN_OBJECTS ForeignObjects { get; set; }
    
        [XmlIgnore]
        public bool ForeignObjectsSpecified
        {
            get { return this.ForeignObjects != null && this.ForeignObjects.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public DOCUMENT_SET_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
