namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class DOCUMENT_SETS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DocumentSetListSpecified
                    || this.ForeignObjectsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DOCUMENT_SET", Order = 0)]
        public List<DOCUMENT_SET> DocumentSetList { get; set; } = new List<DOCUMENT_SET>();
    
        [XmlIgnore]
        public bool DocumentSetListSpecified
        {
            get { return this.DocumentSetList != null && this.DocumentSetList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("FOREIGN_OBJECTS", Order = 1)]
        public FOREIGN_OBJECTS ForeignObjects { get; set; }
    
        [XmlIgnore]
        public bool ForeignObjectsSpecified
        {
            get { return this.ForeignObjects != null && this.ForeignObjects.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public DOCUMENT_SETS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
