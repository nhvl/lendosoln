namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SALES_CONCESSION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SalesConcessionAmountSpecified
                    || this.SalesConcessionProvidedByTypeSpecified
                    || this.SalesConcessionProvidedByTypeOtherDescriptionSpecified
                    || this.SalesConcessionTypeSpecified
                    || this.SalesConcessionTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SalesConcessionAmount", Order = 0)]
        public MISMOAmount SalesConcessionAmount { get; set; }
    
        [XmlIgnore]
        public bool SalesConcessionAmountSpecified
        {
            get { return this.SalesConcessionAmount != null; }
            set { }
        }
    
        [XmlElement("SalesConcessionProvidedByType", Order = 1)]
        public MISMOEnum<SalesConcessionProvidedByBase> SalesConcessionProvidedByType { get; set; }
    
        [XmlIgnore]
        public bool SalesConcessionProvidedByTypeSpecified
        {
            get { return this.SalesConcessionProvidedByType != null; }
            set { }
        }
    
        [XmlElement("SalesConcessionProvidedByTypeOtherDescription", Order = 2)]
        public MISMOString SalesConcessionProvidedByTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool SalesConcessionProvidedByTypeOtherDescriptionSpecified
        {
            get { return this.SalesConcessionProvidedByTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("SalesConcessionType", Order = 3)]
        public MISMOEnum<SalesConcessionBase> SalesConcessionType { get; set; }
    
        [XmlIgnore]
        public bool SalesConcessionTypeSpecified
        {
            get { return this.SalesConcessionType != null; }
            set { }
        }
    
        [XmlElement("SalesConcessionTypeOtherDescription", Order = 4)]
        public MISMOString SalesConcessionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool SalesConcessionTypeOtherDescriptionSpecified
        {
            get { return this.SalesConcessionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public SALES_CONCESSION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
