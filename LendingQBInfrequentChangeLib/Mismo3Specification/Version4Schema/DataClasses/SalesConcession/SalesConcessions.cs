namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class SALES_CONCESSIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SalesConcessionListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SALES_CONCESSION", Order = 0)]
        public List<SALES_CONCESSION> SalesConcessionList { get; set; } = new List<SALES_CONCESSION>();
    
        [XmlIgnore]
        public bool SalesConcessionListSpecified
        {
            get { return this.SalesConcessionList != null && this.SalesConcessionList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public SALES_CONCESSIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
