namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class PRINCIPAL_AND_INTEREST_ADJUSTMENT_LIMITED_PAYMENT_OPTIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PrincipalAndInterestAdjustmentLimitedPaymentOptionListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PRINCIPAL_AND_INTEREST_ADJUSTMENT_LIMITED_PAYMENT_OPTION", Order = 0)]
        public List<PRINCIPAL_AND_INTEREST_ADJUSTMENT_LIMITED_PAYMENT_OPTION> PrincipalAndInterestAdjustmentLimitedPaymentOptionList { get; set; } = new List<PRINCIPAL_AND_INTEREST_ADJUSTMENT_LIMITED_PAYMENT_OPTION>();
    
        [XmlIgnore]
        public bool PrincipalAndInterestAdjustmentLimitedPaymentOptionListSpecified
        {
            get { return this.PrincipalAndInterestAdjustmentLimitedPaymentOptionList != null && this.PrincipalAndInterestAdjustmentLimitedPaymentOptionList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public PRINCIPAL_AND_INTEREST_ADJUSTMENT_LIMITED_PAYMENT_OPTIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
