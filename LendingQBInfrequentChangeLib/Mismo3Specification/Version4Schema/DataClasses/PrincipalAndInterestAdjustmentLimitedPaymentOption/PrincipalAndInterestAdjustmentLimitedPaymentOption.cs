namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PRINCIPAL_AND_INTEREST_ADJUSTMENT_LIMITED_PAYMENT_OPTION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LimitedPrincipalAndInterestPaymentActivationAmountSpecified
                    || this.LimitedPrincipalAndInterestPaymentActivationIndicatorSpecified
                    || this.LimitedPrincipalAndInterestPaymentActivationPercentSpecified
                    || this.LimitedPrincipalAndInterestPaymentEffectiveDateSpecified
                    || this.LimitedPrincipalAndInterestPaymentOptionDescriptionSpecified
                    || this.LimitedPrincipalAndInterestPaymentPeriodEndDateSpecified
                    || this.LimitedPrincipalAndInterestPaymentPullTheNoteIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LimitedPrincipalAndInterestPaymentActivationAmount", Order = 0)]
        public MISMOAmount LimitedPrincipalAndInterestPaymentActivationAmount { get; set; }
    
        [XmlIgnore]
        public bool LimitedPrincipalAndInterestPaymentActivationAmountSpecified
        {
            get { return this.LimitedPrincipalAndInterestPaymentActivationAmount != null; }
            set { }
        }
    
        [XmlElement("LimitedPrincipalAndInterestPaymentActivationIndicator", Order = 1)]
        public MISMOIndicator LimitedPrincipalAndInterestPaymentActivationIndicator { get; set; }
    
        [XmlIgnore]
        public bool LimitedPrincipalAndInterestPaymentActivationIndicatorSpecified
        {
            get { return this.LimitedPrincipalAndInterestPaymentActivationIndicator != null; }
            set { }
        }
    
        [XmlElement("LimitedPrincipalAndInterestPaymentActivationPercent", Order = 2)]
        public MISMOPercent LimitedPrincipalAndInterestPaymentActivationPercent { get; set; }
    
        [XmlIgnore]
        public bool LimitedPrincipalAndInterestPaymentActivationPercentSpecified
        {
            get { return this.LimitedPrincipalAndInterestPaymentActivationPercent != null; }
            set { }
        }
    
        [XmlElement("LimitedPrincipalAndInterestPaymentEffectiveDate", Order = 3)]
        public MISMODate LimitedPrincipalAndInterestPaymentEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool LimitedPrincipalAndInterestPaymentEffectiveDateSpecified
        {
            get { return this.LimitedPrincipalAndInterestPaymentEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("LimitedPrincipalAndInterestPaymentOptionDescription", Order = 4)]
        public MISMOString LimitedPrincipalAndInterestPaymentOptionDescription { get; set; }
    
        [XmlIgnore]
        public bool LimitedPrincipalAndInterestPaymentOptionDescriptionSpecified
        {
            get { return this.LimitedPrincipalAndInterestPaymentOptionDescription != null; }
            set { }
        }
    
        [XmlElement("LimitedPrincipalAndInterestPaymentPeriodEndDate", Order = 5)]
        public MISMODate LimitedPrincipalAndInterestPaymentPeriodEndDate { get; set; }
    
        [XmlIgnore]
        public bool LimitedPrincipalAndInterestPaymentPeriodEndDateSpecified
        {
            get { return this.LimitedPrincipalAndInterestPaymentPeriodEndDate != null; }
            set { }
        }
    
        [XmlElement("LimitedPrincipalAndInterestPaymentPullTheNoteIndicator", Order = 6)]
        public MISMOIndicator LimitedPrincipalAndInterestPaymentPullTheNoteIndicator { get; set; }
    
        [XmlIgnore]
        public bool LimitedPrincipalAndInterestPaymentPullTheNoteIndicatorSpecified
        {
            get { return this.LimitedPrincipalAndInterestPaymentPullTheNoteIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 7)]
        public PRINCIPAL_AND_INTEREST_ADJUSTMENT_LIMITED_PAYMENT_OPTION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
