namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PAYMENT_SUMMARY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AggregateLoanCurtailmentAmountSpecified
                    || this.DefaultedFirstPaymentIndicatorSpecified
                    || this.InterestBearingUPBAmountSpecified
                    || this.InterestPaidThroughDateSpecified
                    || this.LastPaidInstallmentDueDateSpecified
                    || this.LastPaymentReceivedDateSpecified
                    || this.NextPaymentDueDateSpecified
                    || this.NonInterestBearingUPBAmountSpecified
                    || this.PaymentHistoryPatternTextSpecified
                    || this.PostPetitionNextPaymentDueDateSpecified
                    || this.ScheduledUPBAmountSpecified
                    || this.ServicingTerminationUPBAmountSpecified
                    || this.TotalPITIMonthsCountSpecified
                    || this.TotalPITIReceivedAmountSpecified
                    || this.UPBAmountSpecified
                    || this.YearToDatePrincipalPaidAmountSpecified
                    || this.YearToDateTaxesPaidAmountSpecified
                    || this.YearToDateTotalInterestPaidAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AggregateLoanCurtailmentAmount", Order = 0)]
        public MISMOAmount AggregateLoanCurtailmentAmount { get; set; }
    
        [XmlIgnore]
        public bool AggregateLoanCurtailmentAmountSpecified
        {
            get { return this.AggregateLoanCurtailmentAmount != null; }
            set { }
        }
    
        [XmlElement("DefaultedFirstPaymentIndicator", Order = 1)]
        public MISMOIndicator DefaultedFirstPaymentIndicator { get; set; }
    
        [XmlIgnore]
        public bool DefaultedFirstPaymentIndicatorSpecified
        {
            get { return this.DefaultedFirstPaymentIndicator != null; }
            set { }
        }
    
        [XmlElement("InterestBearingUPBAmount", Order = 2)]
        public MISMOAmount InterestBearingUPBAmount { get; set; }
    
        [XmlIgnore]
        public bool InterestBearingUPBAmountSpecified
        {
            get { return this.InterestBearingUPBAmount != null; }
            set { }
        }
    
        [XmlElement("InterestPaidThroughDate", Order = 3)]
        public MISMODate InterestPaidThroughDate { get; set; }
    
        [XmlIgnore]
        public bool InterestPaidThroughDateSpecified
        {
            get { return this.InterestPaidThroughDate != null; }
            set { }
        }
    
        [XmlElement("LastPaidInstallmentDueDate", Order = 4)]
        public MISMODate LastPaidInstallmentDueDate { get; set; }
    
        [XmlIgnore]
        public bool LastPaidInstallmentDueDateSpecified
        {
            get { return this.LastPaidInstallmentDueDate != null; }
            set { }
        }
    
        [XmlElement("LastPaymentReceivedDate", Order = 5)]
        public MISMODate LastPaymentReceivedDate { get; set; }
    
        [XmlIgnore]
        public bool LastPaymentReceivedDateSpecified
        {
            get { return this.LastPaymentReceivedDate != null; }
            set { }
        }
    
        [XmlElement("NextPaymentDueDate", Order = 6)]
        public MISMODate NextPaymentDueDate { get; set; }
    
        [XmlIgnore]
        public bool NextPaymentDueDateSpecified
        {
            get { return this.NextPaymentDueDate != null; }
            set { }
        }
    
        [XmlElement("NonInterestBearingUPBAmount", Order = 7)]
        public MISMOAmount NonInterestBearingUPBAmount { get; set; }
    
        [XmlIgnore]
        public bool NonInterestBearingUPBAmountSpecified
        {
            get { return this.NonInterestBearingUPBAmount != null; }
            set { }
        }
    
        [XmlElement("PaymentHistoryPatternText", Order = 8)]
        public MISMOString PaymentHistoryPatternText { get; set; }
    
        [XmlIgnore]
        public bool PaymentHistoryPatternTextSpecified
        {
            get { return this.PaymentHistoryPatternText != null; }
            set { }
        }
    
        [XmlElement("PostPetitionNextPaymentDueDate", Order = 9)]
        public MISMODate PostPetitionNextPaymentDueDate { get; set; }
    
        [XmlIgnore]
        public bool PostPetitionNextPaymentDueDateSpecified
        {
            get { return this.PostPetitionNextPaymentDueDate != null; }
            set { }
        }
    
        [XmlElement("ScheduledUPBAmount", Order = 10)]
        public MISMOAmount ScheduledUPBAmount { get; set; }
    
        [XmlIgnore]
        public bool ScheduledUPBAmountSpecified
        {
            get { return this.ScheduledUPBAmount != null; }
            set { }
        }
    
        [XmlElement("ServicingTerminationUPBAmount", Order = 11)]
        public MISMOAmount ServicingTerminationUPBAmount { get; set; }
    
        [XmlIgnore]
        public bool ServicingTerminationUPBAmountSpecified
        {
            get { return this.ServicingTerminationUPBAmount != null; }
            set { }
        }
    
        [XmlElement("TotalPITIMonthsCount", Order = 12)]
        public MISMOCount TotalPITIMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool TotalPITIMonthsCountSpecified
        {
            get { return this.TotalPITIMonthsCount != null; }
            set { }
        }
    
        [XmlElement("TotalPITIReceivedAmount", Order = 13)]
        public MISMOAmount TotalPITIReceivedAmount { get; set; }
    
        [XmlIgnore]
        public bool TotalPITIReceivedAmountSpecified
        {
            get { return this.TotalPITIReceivedAmount != null; }
            set { }
        }
    
        [XmlElement("UPBAmount", Order = 14)]
        public MISMOAmount UPBAmount { get; set; }
    
        [XmlIgnore]
        public bool UPBAmountSpecified
        {
            get { return this.UPBAmount != null; }
            set { }
        }
    
        [XmlElement("YearToDatePrincipalPaidAmount", Order = 15)]
        public MISMOAmount YearToDatePrincipalPaidAmount { get; set; }
    
        [XmlIgnore]
        public bool YearToDatePrincipalPaidAmountSpecified
        {
            get { return this.YearToDatePrincipalPaidAmount != null; }
            set { }
        }
    
        [XmlElement("YearToDateTaxesPaidAmount", Order = 16)]
        public MISMOAmount YearToDateTaxesPaidAmount { get; set; }
    
        [XmlIgnore]
        public bool YearToDateTaxesPaidAmountSpecified
        {
            get { return this.YearToDateTaxesPaidAmount != null; }
            set { }
        }
    
        [XmlElement("YearToDateTotalInterestPaidAmount", Order = 17)]
        public MISMOAmount YearToDateTotalInterestPaidAmount { get; set; }
    
        [XmlIgnore]
        public bool YearToDateTotalInterestPaidAmountSpecified
        {
            get { return this.YearToDateTotalInterestPaidAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 18)]
        public PAYMENT_SUMMARY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
