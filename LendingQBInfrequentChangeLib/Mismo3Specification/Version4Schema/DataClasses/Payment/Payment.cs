namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PAYMENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ArrearageSpecified
                    || this.PartialPaymentsSpecified
                    || this.PaymentComponentBreakoutsSpecified
                    || this.PaymentRuleSpecified
                    || this.PaymentScheduleItemsSpecified
                    || this.PaymentSummarySpecified
                    || this.PeriodicLateCountsSpecified
                    || this.SkipPaymentSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ARREARAGE", Order = 0)]
        public ARREARAGE Arrearage { get; set; }
    
        [XmlIgnore]
        public bool ArrearageSpecified
        {
            get { return this.Arrearage != null && this.Arrearage.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PARTIAL_PAYMENTS", Order = 1)]
        public PARTIAL_PAYMENTS PartialPayments { get; set; }
    
        [XmlIgnore]
        public bool PartialPaymentsSpecified
        {
            get { return this.PartialPayments != null && this.PartialPayments.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PAYMENT_COMPONENT_BREAKOUTS", Order = 2)]
        public PAYMENT_COMPONENT_BREAKOUTS PaymentComponentBreakouts { get; set; }
    
        [XmlIgnore]
        public bool PaymentComponentBreakoutsSpecified
        {
            get { return this.PaymentComponentBreakouts != null && this.PaymentComponentBreakouts.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PAYMENT_RULE", Order = 3)]
        public PAYMENT_RULE PaymentRule { get; set; }
    
        [XmlIgnore]
        public bool PaymentRuleSpecified
        {
            get { return this.PaymentRule != null && this.PaymentRule.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PAYMENT_SCHEDULE_ITEMS", Order = 4)]
        public PAYMENT_SCHEDULE_ITEMS PaymentScheduleItems { get; set; }
    
        [XmlIgnore]
        public bool PaymentScheduleItemsSpecified
        {
            get { return this.PaymentScheduleItems != null && this.PaymentScheduleItems.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PAYMENT_SUMMARY", Order = 5)]
        public PAYMENT_SUMMARY PaymentSummary { get; set; }
    
        [XmlIgnore]
        public bool PaymentSummarySpecified
        {
            get { return this.PaymentSummary != null && this.PaymentSummary.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PERIODIC_LATE_COUNTS", Order = 6)]
        public PERIODIC_LATE_COUNTS PeriodicLateCounts { get; set; }
    
        [XmlIgnore]
        public bool PeriodicLateCountsSpecified
        {
            get { return this.PeriodicLateCounts != null && this.PeriodicLateCounts.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SKIP_PAYMENT", Order = 7)]
        public SKIP_PAYMENT SkipPayment { get; set; }
    
        [XmlIgnore]
        public bool SkipPaymentSpecified
        {
            get { return this.SkipPayment != null && this.SkipPayment.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 8)]
        public PAYMENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
