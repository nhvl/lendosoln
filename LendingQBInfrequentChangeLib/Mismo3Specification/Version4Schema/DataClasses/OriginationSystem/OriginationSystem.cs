namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ORIGINATION_SYSTEM
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LoanOriginationSystemLoanIdentifierSpecified
                    || this.LoanOriginationSystemNameSpecified
                    || this.LoanOriginationSystemVendorIdentifierSpecified
                    || this.LoanOriginationSystemVersionIdentifierSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LoanOriginationSystemLoanIdentifier", Order = 0)]
        public MISMOIdentifier LoanOriginationSystemLoanIdentifier { get; set; }
    
        [XmlIgnore]
        public bool LoanOriginationSystemLoanIdentifierSpecified
        {
            get { return this.LoanOriginationSystemLoanIdentifier != null; }
            set { }
        }
    
        [XmlElement("LoanOriginationSystemName", Order = 1)]
        public MISMOString LoanOriginationSystemName { get; set; }
    
        [XmlIgnore]
        public bool LoanOriginationSystemNameSpecified
        {
            get { return this.LoanOriginationSystemName != null; }
            set { }
        }
    
        [XmlElement("LoanOriginationSystemVendorIdentifier", Order = 2)]
        public MISMOIdentifier LoanOriginationSystemVendorIdentifier { get; set; }
    
        [XmlIgnore]
        public bool LoanOriginationSystemVendorIdentifierSpecified
        {
            get { return this.LoanOriginationSystemVendorIdentifier != null; }
            set { }
        }
    
        [XmlElement("LoanOriginationSystemVersionIdentifier", Order = 3)]
        public MISMOIdentifier LoanOriginationSystemVersionIdentifier { get; set; }
    
        [XmlIgnore]
        public bool LoanOriginationSystemVersionIdentifierSpecified
        {
            get { return this.LoanOriginationSystemVersionIdentifier != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public ORIGINATION_SYSTEM_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
