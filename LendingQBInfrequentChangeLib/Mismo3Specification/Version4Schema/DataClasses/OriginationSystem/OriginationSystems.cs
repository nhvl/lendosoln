namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class ORIGINATION_SYSTEMS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.OriginationSystemListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ORIGINATION_SYSTEM", Order = 0)]
        public List<ORIGINATION_SYSTEM> OriginationSystemList { get; set; } = new List<ORIGINATION_SYSTEM>();
    
        [XmlIgnore]
        public bool OriginationSystemListSpecified
        {
            get { return this.OriginationSystemList != null && this.OriginationSystemList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public ORIGINATION_SYSTEMS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
