namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ROLE_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ConsentToReceiveDocumentsElectronicallyIndicatorSpecified
                    || this.ConsentToSignUsingElectronicSignatureIndicatorSpecified
                    || this.PartyRoleTypeSpecified
                    || this.PartyRoleTypeAdditionalDescriptionSpecified
                    || this.PartyRoleTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ConsentToReceiveDocumentsElectronicallyIndicator", Order = 0)]
        public MISMOIndicator ConsentToReceiveDocumentsElectronicallyIndicator { get; set; }
    
        [XmlIgnore]
        public bool ConsentToReceiveDocumentsElectronicallyIndicatorSpecified
        {
            get { return this.ConsentToReceiveDocumentsElectronicallyIndicator != null; }
            set { }
        }
    
        [XmlElement("ConsentToSignUsingElectronicSignatureIndicator", Order = 1)]
        public MISMOIndicator ConsentToSignUsingElectronicSignatureIndicator { get; set; }
    
        [XmlIgnore]
        public bool ConsentToSignUsingElectronicSignatureIndicatorSpecified
        {
            get { return this.ConsentToSignUsingElectronicSignatureIndicator != null; }
            set { }
        }
    
        [XmlElement("PartyRoleType", Order = 2)]
        public MISMOEnum<PartyRoleBase> PartyRoleType { get; set; }
    
        [XmlIgnore]
        public bool PartyRoleTypeSpecified
        {
            get { return this.PartyRoleType != null; }
            set { }
        }
    
        [XmlElement("PartyRoleTypeAdditionalDescription", Order = 3)]
        public MISMOString PartyRoleTypeAdditionalDescription { get; set; }
    
        [XmlIgnore]
        public bool PartyRoleTypeAdditionalDescriptionSpecified
        {
            get { return this.PartyRoleTypeAdditionalDescription != null; }
            set { }
        }
    
        [XmlElement("PartyRoleTypeOtherDescription", Order = 4)]
        public MISMOString PartyRoleTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PartyRoleTypeOtherDescriptionSpecified
        {
            get { return this.PartyRoleTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public ROLE_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
