namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class ROLES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.RoleListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ROLE", Order = 0)]
        public List<ROLE> RoleList { get; set; } = new List<ROLE>();
    
        [XmlIgnore]
        public bool RoleListSpecified
        {
            get { return this.RoleList != null && this.RoleList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public ROLES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
