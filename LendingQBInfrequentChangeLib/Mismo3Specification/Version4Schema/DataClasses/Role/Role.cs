namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Xml.Schema;
    using System.Xml.Serialization;

    public class ROLE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                if (Mismo3Utilities.ExceedsThreshold(1,
                    new List<bool> {
                        this.AppraiserSpecified,
                        this.AppraiserSupervisorSpecified,
                        this.AttorneyInFactSpecified,
                        this.AttorneySpecified,
                        this.BorrowerSpecified,
                        this.ClosingAgentSpecified,
                        this.DefendantSpecified,
                        this.FulfillmentPartySpecified,
                        this.HousingCounselingAgencySpecified,
                        this.LenderSpecified,
                        this.LienHolderSpecified,
                        this.LoanOriginatorSpecified,
                        this.LossPayeeSpecified,
                        this.NotarySpecified,
                        this.PayeeSpecified,
                        this.PlaintiffSpecified,
                        this.PropertyOwnerSpecified,
                        this.PropertySellerSpecified,
                        this.RealEstateAgentSpecified,
                        this.RegulatoryAgencySpecified,
                        this.RequestingPartySpecified,
                        this.RespondingPartySpecified,
                        this.ReturnToSpecified,
                        this.ReviewAppraiserSpecified,
                        this.ServicerSpecified,
                        this.ServiceProviderSpecified,
                        this.ServicingTransferorSpecified,
                        this.SubmittingPartySpecified,
                        this.TrusteeSpecified,
                        this.TrustSpecified }))
                {
                    throw new System.Xml.Schema.XmlSchemaValidationException(
                        Mismo3Utilities.GetXSDChoiceExceptionMessage(
                        "ROLE",
                        new List<string> {
                            "APPRAISER",
                            "APPRAISER_SUPERVISOR",
                            "ATTORNEY",
                            "ATTORNEY_IN_FACT",
                            "BORROWER",
                            "CLOSING_AGENT",
                            "DEFENDANT",
                            "FULFILLMENT_PARTY",
                            "HOUSING_COUNSELING_AGENCY",
                            "LENDER",
                            "LIEN_HOLDER",
                            "LOAN_ORIGINATOR",
                            "LOSS_PAYEE",
                            "NOTARY",
                            "PAYEE",
                            "PLAINTIFF",
                            "PROPERTY_OWNER",
                            "PROPERTY_SELLER",
                            "REAL_ESTATE_AGENT",
                            "REGULATORY_AGENCY",
                            "REQUESTING_PARTY",
                            "RESPONDING_PARTY",
                            "RETURN_TO",
                            "REVIEW_APPRAISER",
                            "SERVICER",
                            "SERVICE_PROVIDER",
                            "SERVICING_TRANSFEROR",
                            "SUBMITTING_PARTY",
                            "TRUST",
                            "TRUSTEE"}));
                }

                return this.AppraiserSpecified
                    || this.AppraiserSupervisorSpecified
                    || this.AttorneySpecified
                    || this.AttorneyInFactSpecified
                    || this.BorrowerSpecified
                    || this.ClosingAgentSpecified
                    || this.DefendantSpecified
                    || this.FulfillmentPartySpecified
                    || this.HousingCounselingAgencySpecified
                    || this.LenderSpecified
                    || this.LienHolderSpecified
                    || this.LoanOriginatorSpecified
                    || this.LossPayeeSpecified
                    || this.NotarySpecified
                    || this.PayeeSpecified
                    || this.PlaintiffSpecified
                    || this.PropertyOwnerSpecified
                    || this.PropertySellerSpecified
                    || this.RealEstateAgentSpecified
                    || this.RegulatoryAgencySpecified
                    || this.RequestingPartySpecified
                    || this.RespondingPartySpecified
                    || this.ReturnToSpecified
                    || this.ReviewAppraiserSpecified
                    || this.ServicerSpecified
                    || this.ServiceProviderSpecified
                    || this.ServicingTransferorSpecified
                    || this.SubmittingPartySpecified
                    || this.TrustSpecified
                    || this.TrusteeSpecified
                    || this.LicensesSpecified
                    || this.PartyRoleIdentifiersSpecified
                    || this.RoleDetailSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("APPRAISER", Order = 0)]
        public APPRAISER Appraiser { get; set; }
    
        [XmlIgnore]
        public bool AppraiserSpecified
        {
            get { return this.Appraiser != null && this.Appraiser.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("APPRAISER_SUPERVISOR", Order = 1)]
        public APPRAISER_SUPERVISOR AppraiserSupervisor { get; set; }
    
        [XmlIgnore]
        public bool AppraiserSupervisorSpecified
        {
            get { return this.AppraiserSupervisor != null && this.AppraiserSupervisor.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("ATTORNEY", Order = 2)]
        public ATTORNEY Attorney { get; set; }
    
        [XmlIgnore]
        public bool AttorneySpecified
        {
            get { return this.Attorney != null && this.Attorney.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("ATTORNEY_IN_FACT", Order = 3)]
        public ATTORNEY_IN_FACT AttorneyInFact { get; set; }
    
        [XmlIgnore]
        public bool AttorneyInFactSpecified
        {
            get { return this.AttorneyInFact != null && this.AttorneyInFact.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("BORROWER", Order = 4)]
        public BORROWER Borrower { get; set; }
    
        [XmlIgnore]
        public bool BorrowerSpecified
        {
            get { return this.Borrower != null && this.Borrower.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CLOSING_AGENT", Order = 5)]
        public CLOSING_AGENT ClosingAgent { get; set; }
    
        [XmlIgnore]
        public bool ClosingAgentSpecified
        {
            get { return this.ClosingAgent != null && this.ClosingAgent.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("DEFENDANT", Order = 6)]
        public DEFENDANT Defendant { get; set; }
    
        [XmlIgnore]
        public bool DefendantSpecified
        {
            get { return this.Defendant != null && this.Defendant.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("FULFILLMENT_PARTY", Order = 7)]
        public FULFILLMENT_PARTY FulfillmentParty { get; set; }
    
        [XmlIgnore]
        public bool FulfillmentPartySpecified
        {
            get { return this.FulfillmentParty != null && this.FulfillmentParty.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("HOUSING_COUNSELING_AGENCY", Order = 8)]
        public HOUSING_COUNSELING_AGENCY HousingCounselingAgency { get; set; }
    
        [XmlIgnore]
        public bool HousingCounselingAgencySpecified
        {
            get { return this.HousingCounselingAgency != null && this.HousingCounselingAgency.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("LENDER", Order = 9)]
        public LENDER Lender { get; set; }
    
        [XmlIgnore]
        public bool LenderSpecified
        {
            get { return this.Lender != null && this.Lender.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("LIEN_HOLDER", Order = 10)]
        public LIEN_HOLDER LienHolder { get; set; }
    
        [XmlIgnore]
        public bool LienHolderSpecified
        {
            get { return this.LienHolder != null && this.LienHolder.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("LOAN_ORIGINATOR", Order = 11)]
        public LOAN_ORIGINATOR LoanOriginator { get; set; }
    
        [XmlIgnore]
        public bool LoanOriginatorSpecified
        {
            get { return this.LoanOriginator != null && this.LoanOriginator.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("LOSS_PAYEE", Order = 12)]
        public LOSS_PAYEE LossPayee { get; set; }
    
        [XmlIgnore]
        public bool LossPayeeSpecified
        {
            get { return this.LossPayee != null && this.LossPayee.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("NOTARY", Order = 13)]
        public NOTARY Notary { get; set; }
    
        [XmlIgnore]
        public bool NotarySpecified
        {
            get { return this.Notary != null && this.Notary.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PAYEE", Order = 14)]
        public PAYEE Payee { get; set; }
    
        [XmlIgnore]
        public bool PayeeSpecified
        {
            get { return this.Payee != null && this.Payee.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PLAINTIFF", Order = 15)]
        public PLAINTIFF Plaintiff { get; set; }
    
        [XmlIgnore]
        public bool PlaintiffSpecified
        {
            get { return this.Plaintiff != null && this.Plaintiff.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PROPERTY_OWNER", Order = 16)]
        public PROPERTY_OWNER PropertyOwner { get; set; }
    
        [XmlIgnore]
        public bool PropertyOwnerSpecified
        {
            get { return this.PropertyOwner != null && this.PropertyOwner.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PROPERTY_SELLER", Order = 17)]
        public PROPERTY_SELLER PropertySeller { get; set; }
    
        [XmlIgnore]
        public bool PropertySellerSpecified
        {
            get { return this.PropertySeller != null && this.PropertySeller.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("REAL_ESTATE_AGENT", Order = 18)]
        public REAL_ESTATE_AGENT RealEstateAgent { get; set; }
    
        [XmlIgnore]
        public bool RealEstateAgentSpecified
        {
            get { return this.RealEstateAgent != null && this.RealEstateAgent.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("REGULATORY_AGENCY", Order = 19)]
        public REGULATORY_AGENCY RegulatoryAgency { get; set; }
    
        [XmlIgnore]
        public bool RegulatoryAgencySpecified
        {
            get { return this.RegulatoryAgency != null && this.RegulatoryAgency.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("REQUESTING_PARTY", Order = 20)]
        public REQUESTING_PARTY RequestingParty { get; set; }
    
        [XmlIgnore]
        public bool RequestingPartySpecified
        {
            get { return this.RequestingParty != null && this.RequestingParty.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("RESPONDING_PARTY", Order = 21)]
        public RESPONDING_PARTY RespondingParty { get; set; }
    
        [XmlIgnore]
        public bool RespondingPartySpecified
        {
            get { return this.RespondingParty != null && this.RespondingParty.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("RETURN_TO", Order = 22)]
        public RETURN_TO ReturnTo { get; set; }
    
        [XmlIgnore]
        public bool ReturnToSpecified
        {
            get { return this.ReturnTo != null && this.ReturnTo.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("REVIEW_APPRAISER", Order = 23)]
        public REVIEW_APPRAISER ReviewAppraiser { get; set; }
    
        [XmlIgnore]
        public bool ReviewAppraiserSpecified
        {
            get { return this.ReviewAppraiser != null && this.ReviewAppraiser.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SERVICER", Order = 24)]
        public SERVICER Servicer { get; set; }
    
        [XmlIgnore]
        public bool ServicerSpecified
        {
            get { return this.Servicer != null && this.Servicer.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SERVICE_PROVIDER", Order = 25)]
        public SERVICE_PROVIDER ServiceProvider { get; set; }
    
        [XmlIgnore]
        public bool ServiceProviderSpecified
        {
            get { return this.ServiceProvider != null && this.ServiceProvider.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SERVICING_TRANSFEROR", Order = 26)]
        public SERVICING_TRANSFEROR ServicingTransferor { get; set; }
    
        [XmlIgnore]
        public bool ServicingTransferorSpecified
        {
            get { return this.ServicingTransferor != null && this.ServicingTransferor.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SUBMITTING_PARTY", Order = 27)]
        public SUBMITTING_PARTY SubmittingParty { get; set; }
    
        [XmlIgnore]
        public bool SubmittingPartySpecified
        {
            get { return this.SubmittingParty != null && this.SubmittingParty.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("TRUST", Order = 28)]
        public TRUST Trust { get; set; }
    
        [XmlIgnore]
        public bool TrustSpecified
        {
            get { return this.Trust != null && this.Trust.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("TRUSTEE", Order = 29)]
        public TRUSTEE Trustee { get; set; }
    
        [XmlIgnore]
        public bool TrusteeSpecified
        {
            get { return this.Trustee != null && this.Trustee.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("LICENSES", Order = 30)]
        public LICENSES Licenses { get; set; }
    
        [XmlIgnore]
        public bool LicensesSpecified
        {
            get { return this.Licenses != null && this.Licenses.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PARTY_ROLE_IDENTIFIERS", Order = 31)]
        public PARTY_ROLE_IDENTIFIERS PartyRoleIdentifiers { get; set; }
    
        [XmlIgnore]
        public bool PartyRoleIdentifiersSpecified
        {
            get { return this.PartyRoleIdentifiers != null && this.PartyRoleIdentifiers.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("ROLE_DETAIL", Order = 32)]
        public ROLE_DETAIL RoleDetail { get; set; }
    
        [XmlIgnore]
        public bool RoleDetailSpecified
        {
            get { return this.RoleDetail != null && this.RoleDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 33)]
        public ROLE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }

        [XmlAttribute("label", Form = XmlSchemaForm.Qualified, Namespace = Mismo3Constants.XlinkNamespace)]
        public string XlinkLabel
        {
            get; set;
        }

        [XmlIgnore]
        public bool XlinkLabelSpecified
        {
            get
            {
                return !string.IsNullOrEmpty(this.XlinkLabel);
            }
            set
            {
            }
        }
    }
}
