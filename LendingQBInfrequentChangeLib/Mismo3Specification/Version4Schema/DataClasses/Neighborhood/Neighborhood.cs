namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class NEIGHBORHOOD
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BoundariesSpecified
                    || this.HousingsSpecified
                    || this.NeighborhoodDetailSpecified
                    || this.NeighborhoodInfluencesSpecified
                    || this.PresentLandUsesSpecified
                    || this.SchoolsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BOUNDARIES", Order = 0)]
        public BOUNDARIES Boundaries { get; set; }
    
        [XmlIgnore]
        public bool BoundariesSpecified
        {
            get { return this.Boundaries != null && this.Boundaries.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("HOUSINGS", Order = 1)]
        public HOUSINGS Housings { get; set; }
    
        [XmlIgnore]
        public bool HousingsSpecified
        {
            get { return this.Housings != null && this.Housings.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("NEIGHBORHOOD_DETAIL", Order = 2)]
        public NEIGHBORHOOD_DETAIL NeighborhoodDetail { get; set; }
    
        [XmlIgnore]
        public bool NeighborhoodDetailSpecified
        {
            get { return this.NeighborhoodDetail != null && this.NeighborhoodDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("NEIGHBORHOOD_INFLUENCES", Order = 3)]
        public NEIGHBORHOOD_INFLUENCES NeighborhoodInfluences { get; set; }
    
        [XmlIgnore]
        public bool NeighborhoodInfluencesSpecified
        {
            get { return this.NeighborhoodInfluences != null && this.NeighborhoodInfluences.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PRESENT_LAND_USES", Order = 4)]
        public PRESENT_LAND_USES PresentLandUses { get; set; }
    
        [XmlIgnore]
        public bool PresentLandUsesSpecified
        {
            get { return this.PresentLandUses != null && this.PresentLandUses.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SCHOOLS", Order = 5)]
        public SCHOOLS Schools { get; set; }
    
        [XmlIgnore]
        public bool SchoolsSpecified
        {
            get { return this.Schools != null && this.Schools.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 6)]
        public NEIGHBORHOOD_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
