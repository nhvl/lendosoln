namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class NEIGHBORHOOD_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AdverseNeighborhoodInfluenceIndicatorSpecified
                    || this.NeighborhoodAcceptabilityOfCooperativeOwnershipDescriptionSpecified
                    || this.NeighborhoodAcceptanceOfCooperativeOwnershipIndicatorSpecified
                    || this.NeighborhoodBoundariesDescriptionSpecified
                    || this.NeighborhoodBuiltUpPercentSpecified
                    || this.NeighborhoodDescriptionSpecified
                    || this.NeighborhoodGrowthPaceTypeSpecified
                    || this.NeighborhoodMarketabilityFactorsDescriptionSpecified
                    || this.NeighborhoodMarketConditionsDescriptionSpecified
                    || this.NeighborhoodNameSpecified
                    || this.NeighborhoodPopulationDensityDescriptionSpecified
                    || this.NeighborhoodPotentialManufacturedHomeParkDescriptionSpecified
                    || this.NeighborhoodPropertyLocationTypeSpecified
                    || this.NeighborhoodPropertyLocationTypeOtherDescriptionSpecified
                    || this.NeighborhoodStreetMeetsStandardsIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AdverseNeighborhoodInfluenceIndicator", Order = 0)]
        public MISMOIndicator AdverseNeighborhoodInfluenceIndicator { get; set; }
    
        [XmlIgnore]
        public bool AdverseNeighborhoodInfluenceIndicatorSpecified
        {
            get { return this.AdverseNeighborhoodInfluenceIndicator != null; }
            set { }
        }
    
        [XmlElement("NeighborhoodAcceptabilityOfCooperativeOwnershipDescription", Order = 1)]
        public MISMOString NeighborhoodAcceptabilityOfCooperativeOwnershipDescription { get; set; }
    
        [XmlIgnore]
        public bool NeighborhoodAcceptabilityOfCooperativeOwnershipDescriptionSpecified
        {
            get { return this.NeighborhoodAcceptabilityOfCooperativeOwnershipDescription != null; }
            set { }
        }
    
        [XmlElement("NeighborhoodAcceptanceOfCooperativeOwnershipIndicator", Order = 2)]
        public MISMOIndicator NeighborhoodAcceptanceOfCooperativeOwnershipIndicator { get; set; }
    
        [XmlIgnore]
        public bool NeighborhoodAcceptanceOfCooperativeOwnershipIndicatorSpecified
        {
            get { return this.NeighborhoodAcceptanceOfCooperativeOwnershipIndicator != null; }
            set { }
        }
    
        [XmlElement("NeighborhoodBoundariesDescription", Order = 3)]
        public MISMOString NeighborhoodBoundariesDescription { get; set; }
    
        [XmlIgnore]
        public bool NeighborhoodBoundariesDescriptionSpecified
        {
            get { return this.NeighborhoodBoundariesDescription != null; }
            set { }
        }
    
        [XmlElement("NeighborhoodBuiltUpPercent", Order = 4)]
        public MISMOPercent NeighborhoodBuiltUpPercent { get; set; }
    
        [XmlIgnore]
        public bool NeighborhoodBuiltUpPercentSpecified
        {
            get { return this.NeighborhoodBuiltUpPercent != null; }
            set { }
        }
    
        [XmlElement("NeighborhoodDescription", Order = 5)]
        public MISMOString NeighborhoodDescription { get; set; }
    
        [XmlIgnore]
        public bool NeighborhoodDescriptionSpecified
        {
            get { return this.NeighborhoodDescription != null; }
            set { }
        }
    
        [XmlElement("NeighborhoodGrowthPaceType", Order = 6)]
        public MISMOEnum<NeighborhoodGrowthPaceBase> NeighborhoodGrowthPaceType { get; set; }
    
        [XmlIgnore]
        public bool NeighborhoodGrowthPaceTypeSpecified
        {
            get { return this.NeighborhoodGrowthPaceType != null; }
            set { }
        }
    
        [XmlElement("NeighborhoodMarketabilityFactorsDescription", Order = 7)]
        public MISMOString NeighborhoodMarketabilityFactorsDescription { get; set; }
    
        [XmlIgnore]
        public bool NeighborhoodMarketabilityFactorsDescriptionSpecified
        {
            get { return this.NeighborhoodMarketabilityFactorsDescription != null; }
            set { }
        }
    
        [XmlElement("NeighborhoodMarketConditionsDescription", Order = 8)]
        public MISMOString NeighborhoodMarketConditionsDescription { get; set; }
    
        [XmlIgnore]
        public bool NeighborhoodMarketConditionsDescriptionSpecified
        {
            get { return this.NeighborhoodMarketConditionsDescription != null; }
            set { }
        }
    
        [XmlElement("NeighborhoodName", Order = 9)]
        public MISMOString NeighborhoodName { get; set; }
    
        [XmlIgnore]
        public bool NeighborhoodNameSpecified
        {
            get { return this.NeighborhoodName != null; }
            set { }
        }
    
        [XmlElement("NeighborhoodPopulationDensityDescription", Order = 10)]
        public MISMOString NeighborhoodPopulationDensityDescription { get; set; }
    
        [XmlIgnore]
        public bool NeighborhoodPopulationDensityDescriptionSpecified
        {
            get { return this.NeighborhoodPopulationDensityDescription != null; }
            set { }
        }
    
        [XmlElement("NeighborhoodPotentialManufacturedHomeParkDescription", Order = 11)]
        public MISMOString NeighborhoodPotentialManufacturedHomeParkDescription { get; set; }
    
        [XmlIgnore]
        public bool NeighborhoodPotentialManufacturedHomeParkDescriptionSpecified
        {
            get { return this.NeighborhoodPotentialManufacturedHomeParkDescription != null; }
            set { }
        }
    
        [XmlElement("NeighborhoodPropertyLocationType", Order = 12)]
        public MISMOEnum<NeighborhoodPropertyLocationBase> NeighborhoodPropertyLocationType { get; set; }
    
        [XmlIgnore]
        public bool NeighborhoodPropertyLocationTypeSpecified
        {
            get { return this.NeighborhoodPropertyLocationType != null; }
            set { }
        }
    
        [XmlElement("NeighborhoodPropertyLocationTypeOtherDescription", Order = 13)]
        public MISMOString NeighborhoodPropertyLocationTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool NeighborhoodPropertyLocationTypeOtherDescriptionSpecified
        {
            get { return this.NeighborhoodPropertyLocationTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("NeighborhoodStreetMeetsStandardsIndicator", Order = 14)]
        public MISMOIndicator NeighborhoodStreetMeetsStandardsIndicator { get; set; }
    
        [XmlIgnore]
        public bool NeighborhoodStreetMeetsStandardsIndicatorSpecified
        {
            get { return this.NeighborhoodStreetMeetsStandardsIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 15)]
        public NEIGHBORHOOD_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
