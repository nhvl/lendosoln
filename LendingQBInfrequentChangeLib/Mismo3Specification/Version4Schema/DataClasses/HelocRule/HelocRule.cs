namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class HELOC_RULE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.HELOCAnnualFeeAmountSpecified
                    || this.HELOCCreditCardAccountIdentifierSpecified
                    || this.HELOCCreditCardIndicatorSpecified
                    || this.HELOCCreditLineDrawAccessFeeAmountSpecified
                    || this.HELOCDailyPeriodicInterestRateCalculationTypeSpecified
                    || this.HELOCInitialAdvanceAmountSpecified
                    || this.HELOCMaximumAPRPercentSpecified
                    || this.HELOCMaximumBalanceAmountSpecified
                    || this.HELOCMinimumAdvanceAmountSpecified
                    || this.HELOCMinimumPaymentAmountSpecified
                    || this.HELOCMinimumPaymentPercentSpecified
                    || this.HELOCRepayPeriodMonthsCountSpecified
                    || this.HELOCReturnedCheckChargeAmountSpecified
                    || this.HELOCStopPaymentChargeAmountSpecified
                    || this.HELOCTeaserMarginRatePercentSpecified
                    || this.HELOCTeaserRateTypeSpecified
                    || this.HELOCTeaserTermMonthsCountSpecified
                    || this.HELOCTerminationFeeAmountSpecified
                    || this.HELOCTerminationPeriodMonthsCountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("HELOCAnnualFeeAmount", Order = 0)]
        public MISMOAmount HELOCAnnualFeeAmount { get; set; }
    
        [XmlIgnore]
        public bool HELOCAnnualFeeAmountSpecified
        {
            get { return this.HELOCAnnualFeeAmount != null; }
            set { }
        }
    
        [XmlElement("HELOCCreditCardAccountIdentifier", Order = 1)]
        public MISMOIdentifier HELOCCreditCardAccountIdentifier { get; set; }
    
        [XmlIgnore]
        public bool HELOCCreditCardAccountIdentifierSpecified
        {
            get { return this.HELOCCreditCardAccountIdentifier != null; }
            set { }
        }
    
        [XmlElement("HELOCCreditCardIndicator", Order = 2)]
        public MISMOIndicator HELOCCreditCardIndicator { get; set; }
    
        [XmlIgnore]
        public bool HELOCCreditCardIndicatorSpecified
        {
            get { return this.HELOCCreditCardIndicator != null; }
            set { }
        }
    
        [XmlElement("HELOCCreditLineDrawAccessFeeAmount", Order = 3)]
        public MISMOAmount HELOCCreditLineDrawAccessFeeAmount { get; set; }
    
        [XmlIgnore]
        public bool HELOCCreditLineDrawAccessFeeAmountSpecified
        {
            get { return this.HELOCCreditLineDrawAccessFeeAmount != null; }
            set { }
        }
    
        [XmlElement("HELOCDailyPeriodicInterestRateCalculationType", Order = 4)]
        public MISMOEnum<HELOCDailyPeriodicInterestRateCalculationBase> HELOCDailyPeriodicInterestRateCalculationType { get; set; }
    
        [XmlIgnore]
        public bool HELOCDailyPeriodicInterestRateCalculationTypeSpecified
        {
            get { return this.HELOCDailyPeriodicInterestRateCalculationType != null; }
            set { }
        }
    
        [XmlElement("HELOCInitialAdvanceAmount", Order = 5)]
        public MISMOAmount HELOCInitialAdvanceAmount { get; set; }
    
        [XmlIgnore]
        public bool HELOCInitialAdvanceAmountSpecified
        {
            get { return this.HELOCInitialAdvanceAmount != null; }
            set { }
        }
    
        [XmlElement("HELOCMaximumAPRPercent", Order = 6)]
        public MISMOPercent HELOCMaximumAPRPercent { get; set; }
    
        [XmlIgnore]
        public bool HELOCMaximumAPRPercentSpecified
        {
            get { return this.HELOCMaximumAPRPercent != null; }
            set { }
        }
    
        [XmlElement("HELOCMaximumBalanceAmount", Order = 7)]
        public MISMOAmount HELOCMaximumBalanceAmount { get; set; }
    
        [XmlIgnore]
        public bool HELOCMaximumBalanceAmountSpecified
        {
            get { return this.HELOCMaximumBalanceAmount != null; }
            set { }
        }
    
        [XmlElement("HELOCMinimumAdvanceAmount", Order = 8)]
        public MISMOAmount HELOCMinimumAdvanceAmount { get; set; }
    
        [XmlIgnore]
        public bool HELOCMinimumAdvanceAmountSpecified
        {
            get { return this.HELOCMinimumAdvanceAmount != null; }
            set { }
        }
    
        [XmlElement("HELOCMinimumPaymentAmount", Order = 9)]
        public MISMOAmount HELOCMinimumPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool HELOCMinimumPaymentAmountSpecified
        {
            get { return this.HELOCMinimumPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("HELOCMinimumPaymentPercent", Order = 10)]
        public MISMOPercent HELOCMinimumPaymentPercent { get; set; }
    
        [XmlIgnore]
        public bool HELOCMinimumPaymentPercentSpecified
        {
            get { return this.HELOCMinimumPaymentPercent != null; }
            set { }
        }
    
        [XmlElement("HELOCRepayPeriodMonthsCount", Order = 11)]
        public MISMOCount HELOCRepayPeriodMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool HELOCRepayPeriodMonthsCountSpecified
        {
            get { return this.HELOCRepayPeriodMonthsCount != null; }
            set { }
        }
    
        [XmlElement("HELOCReturnedCheckChargeAmount", Order = 12)]
        public MISMOAmount HELOCReturnedCheckChargeAmount { get; set; }
    
        [XmlIgnore]
        public bool HELOCReturnedCheckChargeAmountSpecified
        {
            get { return this.HELOCReturnedCheckChargeAmount != null; }
            set { }
        }
    
        [XmlElement("HELOCStopPaymentChargeAmount", Order = 13)]
        public MISMOAmount HELOCStopPaymentChargeAmount { get; set; }
    
        [XmlIgnore]
        public bool HELOCStopPaymentChargeAmountSpecified
        {
            get { return this.HELOCStopPaymentChargeAmount != null; }
            set { }
        }
    
        [XmlElement("HELOCTeaserMarginRatePercent", Order = 14)]
        public MISMOPercent HELOCTeaserMarginRatePercent { get; set; }
    
        [XmlIgnore]
        public bool HELOCTeaserMarginRatePercentSpecified
        {
            get { return this.HELOCTeaserMarginRatePercent != null; }
            set { }
        }
    
        [XmlElement("HELOCTeaserRateType", Order = 15)]
        public MISMOEnum<HELOCTeaserRateBase> HELOCTeaserRateType { get; set; }
    
        [XmlIgnore]
        public bool HELOCTeaserRateTypeSpecified
        {
            get { return this.HELOCTeaserRateType != null; }
            set { }
        }
    
        [XmlElement("HELOCTeaserTermMonthsCount", Order = 16)]
        public MISMOCount HELOCTeaserTermMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool HELOCTeaserTermMonthsCountSpecified
        {
            get { return this.HELOCTeaserTermMonthsCount != null; }
            set { }
        }
    
        [XmlElement("HELOCTerminationFeeAmount", Order = 17)]
        public MISMOAmount HELOCTerminationFeeAmount { get; set; }
    
        [XmlIgnore]
        public bool HELOCTerminationFeeAmountSpecified
        {
            get { return this.HELOCTerminationFeeAmount != null; }
            set { }
        }
    
        [XmlElement("HELOCTerminationPeriodMonthsCount", Order = 18)]
        public MISMOCount HELOCTerminationPeriodMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool HELOCTerminationPeriodMonthsCountSpecified
        {
            get { return this.HELOCTerminationPeriodMonthsCount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 19)]
        public HELOC_RULE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
