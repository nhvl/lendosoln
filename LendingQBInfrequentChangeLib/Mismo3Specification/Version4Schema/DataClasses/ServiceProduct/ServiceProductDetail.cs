namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SERVICE_PRODUCT_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ServiceExpediteIndicatorSpecified
                    || this.ServiceNeedByDateSpecified
                    || this.ServiceProductDescriptionSpecified
                    || this.ServiceProductIdentifierSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ServiceExpediteIndicator", Order = 0)]
        public MISMOIndicator ServiceExpediteIndicator { get; set; }
    
        [XmlIgnore]
        public bool ServiceExpediteIndicatorSpecified
        {
            get { return this.ServiceExpediteIndicator != null; }
            set { }
        }
    
        [XmlElement("ServiceNeedByDate", Order = 1)]
        public MISMODate ServiceNeedByDate { get; set; }
    
        [XmlIgnore]
        public bool ServiceNeedByDateSpecified
        {
            get { return this.ServiceNeedByDate != null; }
            set { }
        }
    
        [XmlElement("ServiceProductDescription", Order = 2)]
        public MISMOString ServiceProductDescription { get; set; }
    
        [XmlIgnore]
        public bool ServiceProductDescriptionSpecified
        {
            get { return this.ServiceProductDescription != null; }
            set { }
        }
    
        [XmlElement("ServiceProductIdentifier", Order = 3)]
        public MISMOIdentifier ServiceProductIdentifier { get; set; }
    
        [XmlIgnore]
        public bool ServiceProductIdentifierSpecified
        {
            get { return this.ServiceProductIdentifier != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public SERVICE_PRODUCT_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
