namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SERVICE_PRODUCT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ServiceProductRequestSpecified
                    || this.ServiceProductResponseSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SERVICE_PRODUCT_REQUEST", Order = 0)]
        public SERVICE_PRODUCT_REQUEST ServiceProductRequest { get; set; }
    
        [XmlIgnore]
        public bool ServiceProductRequestSpecified
        {
            get { return this.ServiceProductRequest != null && this.ServiceProductRequest.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SERVICE_PRODUCT_RESPONSE", Order = 1)]
        public SERVICE_PRODUCT_RESPONSE ServiceProductResponse { get; set; }
    
        [XmlIgnore]
        public bool ServiceProductResponseSpecified
        {
            get { return this.ServiceProductResponse != null && this.ServiceProductResponse.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public SERVICE_PRODUCT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
