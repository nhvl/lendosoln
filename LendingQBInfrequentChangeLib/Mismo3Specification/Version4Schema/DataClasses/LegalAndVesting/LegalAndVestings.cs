namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class LEGAL_AND_VESTINGS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LegalAndVestingListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LEGAL_AND_VESTING", Order = 0)]
        public List<LEGAL_AND_VESTING> LegalAndVestingList { get; set; } = new List<LEGAL_AND_VESTING>();
    
        [XmlIgnore]
        public bool LegalAndVestingListSpecified
        {
            get { return this.LegalAndVestingList != null && this.LegalAndVestingList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public LEGAL_AND_VESTINGS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
