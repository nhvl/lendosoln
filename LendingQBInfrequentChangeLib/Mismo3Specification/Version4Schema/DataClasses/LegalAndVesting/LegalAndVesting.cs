namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class LEGAL_AND_VESTING
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LegalAndVestingCommentDescriptionSpecified
                    || this.LegalAndVestingPlantDateSpecified
                    || this.LegalValidationIndicatorSpecified
                    || this.LienDescriptionSpecified
                    || this.TitleHeldByNameSpecified
                    || this.VestingValidationIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LegalAndVestingCommentDescription", Order = 0)]
        public MISMOString LegalAndVestingCommentDescription { get; set; }
    
        [XmlIgnore]
        public bool LegalAndVestingCommentDescriptionSpecified
        {
            get { return this.LegalAndVestingCommentDescription != null; }
            set { }
        }
    
        [XmlElement("LegalAndVestingPlantDate", Order = 1)]
        public MISMODate LegalAndVestingPlantDate { get; set; }
    
        [XmlIgnore]
        public bool LegalAndVestingPlantDateSpecified
        {
            get { return this.LegalAndVestingPlantDate != null; }
            set { }
        }
    
        [XmlElement("LegalValidationIndicator", Order = 2)]
        public MISMOIndicator LegalValidationIndicator { get; set; }
    
        [XmlIgnore]
        public bool LegalValidationIndicatorSpecified
        {
            get { return this.LegalValidationIndicator != null; }
            set { }
        }
    
        [XmlElement("LienDescription", Order = 3)]
        public MISMOString LienDescription { get; set; }
    
        [XmlIgnore]
        public bool LienDescriptionSpecified
        {
            get { return this.LienDescription != null; }
            set { }
        }
    
        [XmlElement("TitleHeldByName", Order = 4)]
        public MISMOString TitleHeldByName { get; set; }
    
        [XmlIgnore]
        public bool TitleHeldByNameSpecified
        {
            get { return this.TitleHeldByName != null; }
            set { }
        }
    
        [XmlElement("VestingValidationIndicator", Order = 5)]
        public MISMOIndicator VestingValidationIndicator { get; set; }
    
        [XmlIgnore]
        public bool VestingValidationIndicatorSpecified
        {
            get { return this.VestingValidationIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 6)]
        public LEGAL_AND_VESTING_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
