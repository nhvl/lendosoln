namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SKIP_PAYMENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SkipPaymentActionTypeSpecified
                    || this.SkipPaymentActionTypeOtherDescriptionSpecified
                    || this.SkipPaymentInitialRestrictionTermMonthsCountSpecified
                    || this.SkippedPaymentCountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SkipPaymentActionType", Order = 0)]
        public MISMOEnum<SkipPaymentActionBase> SkipPaymentActionType { get; set; }
    
        [XmlIgnore]
        public bool SkipPaymentActionTypeSpecified
        {
            get { return this.SkipPaymentActionType != null; }
            set { }
        }
    
        [XmlElement("SkipPaymentActionTypeOtherDescription", Order = 1)]
        public MISMOString SkipPaymentActionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool SkipPaymentActionTypeOtherDescriptionSpecified
        {
            get { return this.SkipPaymentActionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("SkipPaymentInitialRestrictionTermMonthsCount", Order = 2)]
        public MISMOCount SkipPaymentInitialRestrictionTermMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool SkipPaymentInitialRestrictionTermMonthsCountSpecified
        {
            get { return this.SkipPaymentInitialRestrictionTermMonthsCount != null; }
            set { }
        }
    
        [XmlElement("SkippedPaymentCount", Order = 3)]
        public MISMOCount SkippedPaymentCount { get; set; }
    
        [XmlIgnore]
        public bool SkippedPaymentCountSpecified
        {
            get { return this.SkippedPaymentCount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public SKIP_PAYMENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
