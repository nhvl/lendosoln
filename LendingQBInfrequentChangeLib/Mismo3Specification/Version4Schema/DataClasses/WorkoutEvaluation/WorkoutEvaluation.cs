namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class WORKOUT_EVALUATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.WorkoutEvaluationRequestSpecified
                    || this.WorkoutEvaluationResponseSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("WORKOUT_EVALUATION_REQUEST", Order = 0)]
        public WORKOUT_EVALUATION_REQUEST WorkoutEvaluationRequest { get; set; }
    
        [XmlIgnore]
        public bool WorkoutEvaluationRequestSpecified
        {
            get { return this.WorkoutEvaluationRequest != null && this.WorkoutEvaluationRequest.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("WORKOUT_EVALUATION_RESPONSE", Order = 1)]
        public WORKOUT_EVALUATION_RESPONSE WorkoutEvaluationResponse { get; set; }
    
        [XmlIgnore]
        public bool WorkoutEvaluationResponseSpecified
        {
            get { return this.WorkoutEvaluationResponse != null && this.WorkoutEvaluationResponse.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public WORKOUT_EVALUATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
