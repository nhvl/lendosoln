namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SCOPE_OF_WORK
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EncumbrancesSpecified
                    || this.RealPropertyInterestsSpecified
                    || this.ScopeOfWorkDetailSpecified
                    || this.ValuationIntendedUsersSpecified
                    || this.ValuationIntendedUsesSpecified
                    || this.ValueDefinitionsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ENCUMBRANCES", Order = 0)]
        public ENCUMBRANCES Encumbrances { get; set; }
    
        [XmlIgnore]
        public bool EncumbrancesSpecified
        {
            get { return this.Encumbrances != null && this.Encumbrances.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("REAL_PROPERTY_INTERESTS", Order = 1)]
        public REAL_PROPERTY_INTERESTS RealPropertyInterests { get; set; }
    
        [XmlIgnore]
        public bool RealPropertyInterestsSpecified
        {
            get { return this.RealPropertyInterests != null && this.RealPropertyInterests.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SCOPE_OF_WORK_DETAIL", Order = 2)]
        public SCOPE_OF_WORK_DETAIL ScopeOfWorkDetail { get; set; }
    
        [XmlIgnore]
        public bool ScopeOfWorkDetailSpecified
        {
            get { return this.ScopeOfWorkDetail != null && this.ScopeOfWorkDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("VALUATION_INTENDED_USERS", Order = 3)]
        public VALUATION_INTENDED_USERS ValuationIntendedUsers { get; set; }
    
        [XmlIgnore]
        public bool ValuationIntendedUsersSpecified
        {
            get { return this.ValuationIntendedUsers != null && this.ValuationIntendedUsers.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("VALUATION_INTENDED_USES", Order = 4)]
        public VALUATION_INTENDED_USES ValuationIntendedUses { get; set; }
    
        [XmlIgnore]
        public bool ValuationIntendedUsesSpecified
        {
            get { return this.ValuationIntendedUses != null && this.ValuationIntendedUses.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("VALUE_DEFINITIONS", Order = 5)]
        public VALUE_DEFINITIONS ValueDefinitions { get; set; }
    
        [XmlIgnore]
        public bool ValueDefinitionsSpecified
        {
            get { return this.ValueDefinitions != null && this.ValueDefinitions.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 6)]
        public SCOPE_OF_WORK_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
