namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SCOPE_OF_WORK_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PropertyInspectionRequestCommentDescriptionSpecified
                    || this.PropertyInspectionResultCommentDescriptionSpecified
                    || this.PropertyInspectionTypeSpecified
                    || this.PropertyInspectionTypeOtherDescriptionSpecified
                    || this.PropertyValuationServiceTypeSpecified
                    || this.PropertyValuationServiceTypeOtherDescriptionSpecified
                    || this.USPAPReportDescriptionSpecified
                    || this.ValuationAssignmentTypeSpecified
                    || this.ValuationAssignmentTypeOtherDescriptionSpecified
                    || this.ValuationScopeOfWorkDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PropertyInspectionRequestCommentDescription", Order = 0)]
        public MISMOString PropertyInspectionRequestCommentDescription { get; set; }
    
        [XmlIgnore]
        public bool PropertyInspectionRequestCommentDescriptionSpecified
        {
            get { return this.PropertyInspectionRequestCommentDescription != null; }
            set { }
        }
    
        [XmlElement("PropertyInspectionResultCommentDescription", Order = 1)]
        public MISMOString PropertyInspectionResultCommentDescription { get; set; }
    
        [XmlIgnore]
        public bool PropertyInspectionResultCommentDescriptionSpecified
        {
            get { return this.PropertyInspectionResultCommentDescription != null; }
            set { }
        }
    
        [XmlElement("PropertyInspectionType", Order = 2)]
        public MISMOEnum<PropertyInspectionBase> PropertyInspectionType { get; set; }
    
        [XmlIgnore]
        public bool PropertyInspectionTypeSpecified
        {
            get { return this.PropertyInspectionType != null; }
            set { }
        }
    
        [XmlElement("PropertyInspectionTypeOtherDescription", Order = 3)]
        public MISMOString PropertyInspectionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PropertyInspectionTypeOtherDescriptionSpecified
        {
            get { return this.PropertyInspectionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PropertyValuationServiceType", Order = 4)]
        public MISMOEnum<PropertyValuationServiceBase> PropertyValuationServiceType { get; set; }
    
        [XmlIgnore]
        public bool PropertyValuationServiceTypeSpecified
        {
            get { return this.PropertyValuationServiceType != null; }
            set { }
        }
    
        [XmlElement("PropertyValuationServiceTypeOtherDescription", Order = 5)]
        public MISMOString PropertyValuationServiceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PropertyValuationServiceTypeOtherDescriptionSpecified
        {
            get { return this.PropertyValuationServiceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("USPAPReportDescription", Order = 6)]
        public MISMOString USPAPReportDescription { get; set; }
    
        [XmlIgnore]
        public bool USPAPReportDescriptionSpecified
        {
            get { return this.USPAPReportDescription != null; }
            set { }
        }
    
        [XmlElement("ValuationAssignmentType", Order = 7)]
        public MISMOEnum<ValuationAssignmentBase> ValuationAssignmentType { get; set; }
    
        [XmlIgnore]
        public bool ValuationAssignmentTypeSpecified
        {
            get { return this.ValuationAssignmentType != null; }
            set { }
        }
    
        [XmlElement("ValuationAssignmentTypeOtherDescription", Order = 8)]
        public MISMOString ValuationAssignmentTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ValuationAssignmentTypeOtherDescriptionSpecified
        {
            get { return this.ValuationAssignmentTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ValuationScopeOfWorkDescription", Order = 9)]
        public MISMOString ValuationScopeOfWorkDescription { get; set; }
    
        [XmlIgnore]
        public bool ValuationScopeOfWorkDescriptionSpecified
        {
            get { return this.ValuationScopeOfWorkDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 10)]
        public SCOPE_OF_WORK_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
