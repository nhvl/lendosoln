namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class CREDIT_ERROR_MESSAGES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditErrorMessageListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CREDIT_ERROR_MESSAGE", Order = 0)]
        public List<CREDIT_ERROR_MESSAGE> CreditErrorMessageList { get; set; } = new List<CREDIT_ERROR_MESSAGE>();
    
        [XmlIgnore]
        public bool CreditErrorMessageListSpecified
        {
            get { return this.CreditErrorMessageList != null && this.CreditErrorMessageList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public CREDIT_ERROR_MESSAGES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
