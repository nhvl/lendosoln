namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CREDIT_ERROR_MESSAGE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditErrorMessageCodeSpecified
                    || this.CreditErrorMessageSourceTypeSpecified
                    || this.CreditErrorMessageTextSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CreditErrorMessageCode", Order = 0)]
        public MISMOCode CreditErrorMessageCode { get; set; }
    
        [XmlIgnore]
        public bool CreditErrorMessageCodeSpecified
        {
            get { return this.CreditErrorMessageCode != null; }
            set { }
        }
    
        [XmlElement("CreditErrorMessageSourceType", Order = 1)]
        public MISMOEnum<CreditErrorMessageSourceBase> CreditErrorMessageSourceType { get; set; }
    
        [XmlIgnore]
        public bool CreditErrorMessageSourceTypeSpecified
        {
            get { return this.CreditErrorMessageSourceType != null; }
            set { }
        }
    
        [XmlElement("CreditErrorMessageText", Order = 2)]
        public MISMOString CreditErrorMessageText { get; set; }
    
        [XmlIgnore]
        public bool CreditErrorMessageTextSpecified
        {
            get { return this.CreditErrorMessageText != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public CREDIT_ERROR_MESSAGE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
