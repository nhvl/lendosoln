namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class VALUATION_ANALYSES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ValuationAnalysisListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("VALUATION_ANALYSIS", Order = 0)]
        public List<VALUATION_ANALYSIS> ValuationAnalysisList { get; set; } = new List<VALUATION_ANALYSIS>();
    
        [XmlIgnore]
        public bool ValuationAnalysisListSpecified
        {
            get { return this.ValuationAnalysisList != null && this.ValuationAnalysisList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public VALUATION_ANALYSES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
