namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class VALUATION_ANALYSIS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PriorSaleCommentDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PriorSaleCommentDescription", Order = 0)]
        public MISMOString PriorSaleCommentDescription { get; set; }
    
        [XmlIgnore]
        public bool PriorSaleCommentDescriptionSpecified
        {
            get { return this.PriorSaleCommentDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public VALUATION_ANALYSIS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
