namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MI
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MiApplicationRequestSpecified
                    || this.MiApplicationResponseSpecified
                    || this.MiRateQuoteResponseSpecified
                    || this.MiValidationRequestSpecified
                    || this.MiValidationResponseSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MI_APPLICATION_REQUEST", Order = 0)]
        public MI_APPLICATION_REQUEST MiApplicationRequest { get; set; }
    
        [XmlIgnore]
        public bool MiApplicationRequestSpecified
        {
            get { return this.MiApplicationRequest != null && this.MiApplicationRequest.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("MI_APPLICATION_RESPONSE", Order = 1)]
        public MI_APPLICATION_RESPONSE MiApplicationResponse { get; set; }
    
        [XmlIgnore]
        public bool MiApplicationResponseSpecified
        {
            get { return this.MiApplicationResponse != null && this.MiApplicationResponse.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("MI_RATE_QUOTE_RESPONSE", Order = 2)]
        public MI_RATE_QUOTE_RESPONSE MiRateQuoteResponse { get; set; }
    
        [XmlIgnore]
        public bool MiRateQuoteResponseSpecified
        {
            get { return this.MiRateQuoteResponse != null && this.MiRateQuoteResponse.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("MI_VALIDATION_REQUEST", Order = 3)]
        public MI_VALIDATION_REQUEST MiValidationRequest { get; set; }
    
        [XmlIgnore]
        public bool MiValidationRequestSpecified
        {
            get { return this.MiValidationRequest != null && this.MiValidationRequest.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("MI_VALIDATION_RESPONSE", Order = 4)]
        public MI_VALIDATION_RESPONSE MiValidationResponse { get; set; }
    
        [XmlIgnore]
        public bool MiValidationResponseSpecified
        {
            get { return this.MiValidationResponse != null && this.MiValidationResponse.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public MI_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
