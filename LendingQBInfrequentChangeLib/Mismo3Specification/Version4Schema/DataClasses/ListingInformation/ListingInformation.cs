namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class LISTING_INFORMATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ListingInformationDetailSpecified
                    || this.OfferingHistoriesSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LISTING_INFORMATION_DETAIL", Order = 0)]
        public LISTING_INFORMATION_DETAIL ListingInformationDetail { get; set; }
    
        [XmlIgnore]
        public bool ListingInformationDetailSpecified
        {
            get { return this.ListingInformationDetail != null && this.ListingInformationDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("OFFERING_HISTORIES", Order = 1)]
        public OFFERING_HISTORIES OfferingHistories { get; set; }
    
        [XmlIgnore]
        public bool OfferingHistoriesSpecified
        {
            get { return this.OfferingHistories != null && this.OfferingHistories.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public LISTING_INFORMATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
