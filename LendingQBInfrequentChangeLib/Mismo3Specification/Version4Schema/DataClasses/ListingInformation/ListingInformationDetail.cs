namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class LISTING_INFORMATION_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CumulativeDaysOnMarketCountSpecified
                    || this.CurrentListPriceAmountSpecified
                    || this.CurrentlyListedIndicatorSpecified
                    || this.DaysOnMarketCountSpecified
                    || this.FinalListPriceAmountSpecified
                    || this.ForSaleByOwnerIndicatorSpecified
                    || this.LastListPriceRevisionDateSpecified
                    || this.ListedWithinPreviousYearDescriptionSpecified
                    || this.ListedWithinPreviousYearIndicatorSpecified
                    || this.ListingStatusDateSpecified
                    || this.ListingStatusTypeSpecified
                    || this.ListingStatusTypeOtherDescriptionSpecified
                    || this.MLSNumberIdentifierSpecified
                    || this.PropertyForecastSalePriceAmountSpecified
                    || this.PropertyTypicalMarketingDaysDurationTypeSpecified
                    || this.ShortSaleOfferingTypeSpecified
                    || this.ShortSaleOfferingTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CumulativeDaysOnMarketCount", Order = 0)]
        public MISMOCount CumulativeDaysOnMarketCount { get; set; }
    
        [XmlIgnore]
        public bool CumulativeDaysOnMarketCountSpecified
        {
            get { return this.CumulativeDaysOnMarketCount != null; }
            set { }
        }
    
        [XmlElement("CurrentListPriceAmount", Order = 1)]
        public MISMOAmount CurrentListPriceAmount { get; set; }
    
        [XmlIgnore]
        public bool CurrentListPriceAmountSpecified
        {
            get { return this.CurrentListPriceAmount != null; }
            set { }
        }
    
        [XmlElement("CurrentlyListedIndicator", Order = 2)]
        public MISMOIndicator CurrentlyListedIndicator { get; set; }
    
        [XmlIgnore]
        public bool CurrentlyListedIndicatorSpecified
        {
            get { return this.CurrentlyListedIndicator != null; }
            set { }
        }
    
        [XmlElement("DaysOnMarketCount", Order = 3)]
        public MISMOCount DaysOnMarketCount { get; set; }
    
        [XmlIgnore]
        public bool DaysOnMarketCountSpecified
        {
            get { return this.DaysOnMarketCount != null; }
            set { }
        }
    
        [XmlElement("FinalListPriceAmount", Order = 4)]
        public MISMOAmount FinalListPriceAmount { get; set; }
    
        [XmlIgnore]
        public bool FinalListPriceAmountSpecified
        {
            get { return this.FinalListPriceAmount != null; }
            set { }
        }
    
        [XmlElement("ForSaleByOwnerIndicator", Order = 5)]
        public MISMOIndicator ForSaleByOwnerIndicator { get; set; }
    
        [XmlIgnore]
        public bool ForSaleByOwnerIndicatorSpecified
        {
            get { return this.ForSaleByOwnerIndicator != null; }
            set { }
        }
    
        [XmlElement("LastListPriceRevisionDate", Order = 6)]
        public MISMODate LastListPriceRevisionDate { get; set; }
    
        [XmlIgnore]
        public bool LastListPriceRevisionDateSpecified
        {
            get { return this.LastListPriceRevisionDate != null; }
            set { }
        }
    
        [XmlElement("ListedWithinPreviousYearDescription", Order = 7)]
        public MISMOString ListedWithinPreviousYearDescription { get; set; }
    
        [XmlIgnore]
        public bool ListedWithinPreviousYearDescriptionSpecified
        {
            get { return this.ListedWithinPreviousYearDescription != null; }
            set { }
        }
    
        [XmlElement("ListedWithinPreviousYearIndicator", Order = 8)]
        public MISMOIndicator ListedWithinPreviousYearIndicator { get; set; }
    
        [XmlIgnore]
        public bool ListedWithinPreviousYearIndicatorSpecified
        {
            get { return this.ListedWithinPreviousYearIndicator != null; }
            set { }
        }
    
        [XmlElement("ListingStatusDate", Order = 9)]
        public MISMODate ListingStatusDate { get; set; }
    
        [XmlIgnore]
        public bool ListingStatusDateSpecified
        {
            get { return this.ListingStatusDate != null; }
            set { }
        }
    
        [XmlElement("ListingStatusType", Order = 10)]
        public MISMOEnum<ListingStatusBase> ListingStatusType { get; set; }
    
        [XmlIgnore]
        public bool ListingStatusTypeSpecified
        {
            get { return this.ListingStatusType != null; }
            set { }
        }
    
        [XmlElement("ListingStatusTypeOtherDescription", Order = 11)]
        public MISMOString ListingStatusTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ListingStatusTypeOtherDescriptionSpecified
        {
            get { return this.ListingStatusTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MLSNumberIdentifier", Order = 12)]
        public MISMOIdentifier MLSNumberIdentifier { get; set; }
    
        [XmlIgnore]
        public bool MLSNumberIdentifierSpecified
        {
            get { return this.MLSNumberIdentifier != null; }
            set { }
        }
    
        [XmlElement("PropertyForecastSalePriceAmount", Order = 13)]
        public MISMOAmount PropertyForecastSalePriceAmount { get; set; }
    
        [XmlIgnore]
        public bool PropertyForecastSalePriceAmountSpecified
        {
            get { return this.PropertyForecastSalePriceAmount != null; }
            set { }
        }
    
        [XmlElement("PropertyTypicalMarketingDaysDurationType", Order = 14)]
        public MISMOEnum<PropertyTypicalMarketingDaysDurationBase> PropertyTypicalMarketingDaysDurationType { get; set; }
    
        [XmlIgnore]
        public bool PropertyTypicalMarketingDaysDurationTypeSpecified
        {
            get { return this.PropertyTypicalMarketingDaysDurationType != null; }
            set { }
        }
    
        [XmlElement("ShortSaleOfferingType", Order = 15)]
        public MISMOEnum<ShortSaleOfferingBase> ShortSaleOfferingType { get; set; }
    
        [XmlIgnore]
        public bool ShortSaleOfferingTypeSpecified
        {
            get { return this.ShortSaleOfferingType != null; }
            set { }
        }
    
        [XmlElement("ShortSaleOfferingTypeOtherDescription", Order = 16)]
        public MISMOString ShortSaleOfferingTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ShortSaleOfferingTypeOtherDescriptionSpecified
        {
            get { return this.ShortSaleOfferingTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 17)]
        public LISTING_INFORMATION_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
