namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class LISTING_INFORMATIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ListingInformationListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LISTING_INFORMATION", Order = 0)]
        public List<LISTING_INFORMATION> ListingInformationList { get; set; } = new List<LISTING_INFORMATION>();
    
        [XmlIgnore]
        public bool ListingInformationListSpecified
        {
            get { return this.ListingInformationList != null && this.ListingInformationList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public LISTING_INFORMATIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
