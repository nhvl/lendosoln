namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PROPERTY_UNIT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AddressSpecified
                    || this.PropertyUnitDetailSpecified
                    || this.UnitChargeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ADDRESS", Order = 0)]
        public ADDRESS Address { get; set; }
    
        [XmlIgnore]
        public bool AddressSpecified
        {
            get { return this.Address != null && this.Address.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PROPERTY_UNIT_DETAIL", Order = 1)]
        public PROPERTY_UNIT_DETAIL PropertyUnitDetail { get; set; }
    
        [XmlIgnore]
        public bool PropertyUnitDetailSpecified
        {
            get { return this.PropertyUnitDetail != null && this.PropertyUnitDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("UNIT_CHARGE", Order = 2)]
        public UNIT_CHARGE UnitCharge { get; set; }
    
        [XmlIgnore]
        public bool UnitChargeSpecified
        {
            get { return this.UnitCharge != null && this.UnitCharge.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public PROPERTY_UNIT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
