namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PROPERTY_UNIT_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BathroomNumberSpecified
                    || this.BedroomCountSpecified
                    || this.CooperativeAssignmentOfLeaseDateSpecified
                    || this.CooperativeProprietaryLeaseDateSpecified
                    || this.CooperativeStockCertificateNumberIdentifierSpecified
                    || this.CooperativeUnitSharesCountSpecified
                    || this.FloorIdentifierSpecified
                    || this.LeaseExpirationDateSpecified
                    || this.LevelCountSpecified
                    || this.PropertyDwellingUnitEligibleRentAmountSpecified
                    || this.PropertyDwellingUnitLeaseProvidedIndicatorSpecified
                    || this.PropertyDwellingUnitPastDueRentAmountSpecified
                    || this.SquareFeetPerUnitNumberSpecified
                    || this.TotalRoomCountSpecified
                    || this.UnitIdentifierSpecified
                    || this.UnitOccupancyTypeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BathroomNumber", Order = 0)]
        public MISMONumeric BathroomNumber { get; set; }
    
        [XmlIgnore]
        public bool BathroomNumberSpecified
        {
            get { return this.BathroomNumber != null; }
            set { }
        }
    
        [XmlElement("BedroomCount", Order = 1)]
        public MISMOCount BedroomCount { get; set; }
    
        [XmlIgnore]
        public bool BedroomCountSpecified
        {
            get { return this.BedroomCount != null; }
            set { }
        }
    
        [XmlElement("CooperativeAssignmentOfLeaseDate", Order = 2)]
        public MISMODate CooperativeAssignmentOfLeaseDate { get; set; }
    
        [XmlIgnore]
        public bool CooperativeAssignmentOfLeaseDateSpecified
        {
            get { return this.CooperativeAssignmentOfLeaseDate != null; }
            set { }
        }
    
        [XmlElement("CooperativeProprietaryLeaseDate", Order = 3)]
        public MISMODate CooperativeProprietaryLeaseDate { get; set; }
    
        [XmlIgnore]
        public bool CooperativeProprietaryLeaseDateSpecified
        {
            get { return this.CooperativeProprietaryLeaseDate != null; }
            set { }
        }
    
        [XmlElement("CooperativeStockCertificateNumberIdentifier", Order = 4)]
        public MISMOIdentifier CooperativeStockCertificateNumberIdentifier { get; set; }
    
        [XmlIgnore]
        public bool CooperativeStockCertificateNumberIdentifierSpecified
        {
            get { return this.CooperativeStockCertificateNumberIdentifier != null; }
            set { }
        }
    
        [XmlElement("CooperativeUnitSharesCount", Order = 5)]
        public MISMOCount CooperativeUnitSharesCount { get; set; }
    
        [XmlIgnore]
        public bool CooperativeUnitSharesCountSpecified
        {
            get { return this.CooperativeUnitSharesCount != null; }
            set { }
        }
    
        [XmlElement("FloorIdentifier", Order = 6)]
        public MISMOIdentifier FloorIdentifier { get; set; }
    
        [XmlIgnore]
        public bool FloorIdentifierSpecified
        {
            get { return this.FloorIdentifier != null; }
            set { }
        }
    
        [XmlElement("LeaseExpirationDate", Order = 7)]
        public MISMODate LeaseExpirationDate { get; set; }
    
        [XmlIgnore]
        public bool LeaseExpirationDateSpecified
        {
            get { return this.LeaseExpirationDate != null; }
            set { }
        }
    
        [XmlElement("LevelCount", Order = 8)]
        public MISMOCount LevelCount { get; set; }
    
        [XmlIgnore]
        public bool LevelCountSpecified
        {
            get { return this.LevelCount != null; }
            set { }
        }
    
        [XmlElement("PropertyDwellingUnitEligibleRentAmount", Order = 9)]
        public MISMOAmount PropertyDwellingUnitEligibleRentAmount { get; set; }
    
        [XmlIgnore]
        public bool PropertyDwellingUnitEligibleRentAmountSpecified
        {
            get { return this.PropertyDwellingUnitEligibleRentAmount != null; }
            set { }
        }
    
        [XmlElement("PropertyDwellingUnitLeaseProvidedIndicator", Order = 10)]
        public MISMOIndicator PropertyDwellingUnitLeaseProvidedIndicator { get; set; }
    
        [XmlIgnore]
        public bool PropertyDwellingUnitLeaseProvidedIndicatorSpecified
        {
            get { return this.PropertyDwellingUnitLeaseProvidedIndicator != null; }
            set { }
        }
    
        [XmlElement("PropertyDwellingUnitPastDueRentAmount", Order = 11)]
        public MISMOAmount PropertyDwellingUnitPastDueRentAmount { get; set; }
    
        [XmlIgnore]
        public bool PropertyDwellingUnitPastDueRentAmountSpecified
        {
            get { return this.PropertyDwellingUnitPastDueRentAmount != null; }
            set { }
        }
    
        [XmlElement("SquareFeetPerUnitNumber", Order = 12)]
        public MISMONumeric SquareFeetPerUnitNumber { get; set; }
    
        [XmlIgnore]
        public bool SquareFeetPerUnitNumberSpecified
        {
            get { return this.SquareFeetPerUnitNumber != null; }
            set { }
        }
    
        [XmlElement("TotalRoomCount", Order = 13)]
        public MISMOCount TotalRoomCount { get; set; }
    
        [XmlIgnore]
        public bool TotalRoomCountSpecified
        {
            get { return this.TotalRoomCount != null; }
            set { }
        }
    
        [XmlElement("UnitIdentifier", Order = 14)]
        public MISMOIdentifier UnitIdentifier { get; set; }
    
        [XmlIgnore]
        public bool UnitIdentifierSpecified
        {
            get { return this.UnitIdentifier != null; }
            set { }
        }
    
        [XmlElement("UnitOccupancyType", Order = 15)]
        public MISMOEnum<UnitOccupancyBase> UnitOccupancyType { get; set; }
    
        [XmlIgnore]
        public bool UnitOccupancyTypeSpecified
        {
            get { return this.UnitOccupancyType != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 16)]
        public PROPERTY_UNIT_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
