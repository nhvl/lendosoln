namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class PROPERTY_UNITS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PropertyUnitListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PROPERTY_UNIT", Order = 0)]
        public List<PROPERTY_UNIT> PropertyUnitList { get; set; } = new List<PROPERTY_UNIT>();
    
        [XmlIgnore]
        public bool PropertyUnitListSpecified
        {
            get { return this.PropertyUnitList != null && this.PropertyUnitList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public PROPERTY_UNITS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
