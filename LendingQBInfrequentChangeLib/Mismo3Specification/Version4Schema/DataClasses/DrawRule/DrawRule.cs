namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class DRAW_RULE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LoanDrawExpirationDateSpecified
                    || this.LoanDrawExtensionTermMonthsCountSpecified
                    || this.LoanDrawMaximumAmountSpecified
                    || this.LoanDrawMaximumTermMonthsCountSpecified
                    || this.LoanDrawMinimumAmountSpecified
                    || this.LoanDrawMinimumInitialDrawAmountSpecified
                    || this.LoanDrawPeriodMaximumDrawCountSpecified
                    || this.LoanDrawStartPeriodMonthsCountSpecified
                    || this.LoanDrawTermMonthsCountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LoanDrawExpirationDate", Order = 0)]
        public MISMODate LoanDrawExpirationDate { get; set; }
    
        [XmlIgnore]
        public bool LoanDrawExpirationDateSpecified
        {
            get { return this.LoanDrawExpirationDate != null; }
            set { }
        }
    
        [XmlElement("LoanDrawExtensionTermMonthsCount", Order = 1)]
        public MISMOCount LoanDrawExtensionTermMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool LoanDrawExtensionTermMonthsCountSpecified
        {
            get { return this.LoanDrawExtensionTermMonthsCount != null; }
            set { }
        }
    
        [XmlElement("LoanDrawMaximumAmount", Order = 2)]
        public MISMOAmount LoanDrawMaximumAmount { get; set; }
    
        [XmlIgnore]
        public bool LoanDrawMaximumAmountSpecified
        {
            get { return this.LoanDrawMaximumAmount != null; }
            set { }
        }
    
        [XmlElement("LoanDrawMaximumTermMonthsCount", Order = 3)]
        public MISMOCount LoanDrawMaximumTermMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool LoanDrawMaximumTermMonthsCountSpecified
        {
            get { return this.LoanDrawMaximumTermMonthsCount != null; }
            set { }
        }
    
        [XmlElement("LoanDrawMinimumAmount", Order = 4)]
        public MISMOAmount LoanDrawMinimumAmount { get; set; }
    
        [XmlIgnore]
        public bool LoanDrawMinimumAmountSpecified
        {
            get { return this.LoanDrawMinimumAmount != null; }
            set { }
        }
    
        [XmlElement("LoanDrawMinimumInitialDrawAmount", Order = 5)]
        public MISMOAmount LoanDrawMinimumInitialDrawAmount { get; set; }
    
        [XmlIgnore]
        public bool LoanDrawMinimumInitialDrawAmountSpecified
        {
            get { return this.LoanDrawMinimumInitialDrawAmount != null; }
            set { }
        }
    
        [XmlElement("LoanDrawPeriodMaximumDrawCount", Order = 6)]
        public MISMOCount LoanDrawPeriodMaximumDrawCount { get; set; }
    
        [XmlIgnore]
        public bool LoanDrawPeriodMaximumDrawCountSpecified
        {
            get { return this.LoanDrawPeriodMaximumDrawCount != null; }
            set { }
        }
    
        [XmlElement("LoanDrawStartPeriodMonthsCount", Order = 7)]
        public MISMOCount LoanDrawStartPeriodMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool LoanDrawStartPeriodMonthsCountSpecified
        {
            get { return this.LoanDrawStartPeriodMonthsCount != null; }
            set { }
        }
    
        [XmlElement("LoanDrawTermMonthsCount", Order = 8)]
        public MISMOCount LoanDrawTermMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool LoanDrawTermMonthsCountSpecified
        {
            get { return this.LoanDrawTermMonthsCount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 9)]
        public DRAW_RULE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
