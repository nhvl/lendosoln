namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class FLOOD_REQUEST_DISPUTE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.FloodRequestDisputeDescriptionSpecified
                    || this.FloodRequestDisputeItemTypeSpecified
                    || this.FloodRequestDisputeItemTypeOtherDescriptionSpecified
                    || this.FloodRequestDisputeSupportingDocumentsDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("FloodRequestDisputeDescription", Order = 0)]
        public MISMOString FloodRequestDisputeDescription { get; set; }
    
        [XmlIgnore]
        public bool FloodRequestDisputeDescriptionSpecified
        {
            get { return this.FloodRequestDisputeDescription != null; }
            set { }
        }
    
        [XmlElement("FloodRequestDisputeItemType", Order = 1)]
        public MISMOEnum<FloodRequestDisputeItemBase> FloodRequestDisputeItemType { get; set; }
    
        [XmlIgnore]
        public bool FloodRequestDisputeItemTypeSpecified
        {
            get { return this.FloodRequestDisputeItemType != null; }
            set { }
        }
    
        [XmlElement("FloodRequestDisputeItemTypeOtherDescription", Order = 2)]
        public MISMOString FloodRequestDisputeItemTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool FloodRequestDisputeItemTypeOtherDescriptionSpecified
        {
            get { return this.FloodRequestDisputeItemTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("FloodRequestDisputeSupportingDocumentsDescription", Order = 3)]
        public MISMOString FloodRequestDisputeSupportingDocumentsDescription { get; set; }
    
        [XmlIgnore]
        public bool FloodRequestDisputeSupportingDocumentsDescriptionSpecified
        {
            get { return this.FloodRequestDisputeSupportingDocumentsDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public FLOOD_REQUEST_DISPUTE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
