namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class AUTOMATED_UNDERWRITING_SYSTEM_RESPONSE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AutomatedUnderwritingSystemResponseMessagesSpecified
                    || this.LoansSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AUTOMATED_UNDERWRITING_SYSTEM_RESPONSE_MESSAGES", Order = 0)]
        public AUTOMATED_UNDERWRITING_SYSTEM_RESPONSE_MESSAGES AutomatedUnderwritingSystemResponseMessages { get; set; }
    
        [XmlIgnore]
        public bool AutomatedUnderwritingSystemResponseMessagesSpecified
        {
            get { return this.AutomatedUnderwritingSystemResponseMessages != null && this.AutomatedUnderwritingSystemResponseMessages.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("LOANS", Order = 1)]
        public LOANS Loans { get; set; }
    
        [XmlIgnore]
        public bool LoansSpecified
        {
            get { return this.Loans != null && this.Loans.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public AUTOMATED_UNDERWRITING_SYSTEM_RESPONSE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
