namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class FEE_SUMMARY_TOTAL_FEES_PAID_TOS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.FeeSummaryTotalFeesPaidToListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("FEE_SUMMARY_TOTAL_FEES_PAID_TO", Order = 0)]
        public List<FEE_SUMMARY_TOTAL_FEES_PAID_TO> FeeSummaryTotalFeesPaidToList { get; set; } = new List<FEE_SUMMARY_TOTAL_FEES_PAID_TO>();
    
        [XmlIgnore]
        public bool FeeSummaryTotalFeesPaidToListSpecified
        {
            get { return this.FeeSummaryTotalFeesPaidToList != null && this.FeeSummaryTotalFeesPaidToList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public FEE_SUMMARY_TOTAL_FEES_PAID_TOS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
