namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class FEE_SUMMARY_TOTAL_FEES_PAID_TO
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.FeeSummaryTotalFeesPaidToAmountSpecified
                    || this.FeeSummaryTotalFeesPaidToTypeSpecified
                    || this.FeeSummaryTotalFeesPaidToTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("FeeSummaryTotalFeesPaidToAmount", Order = 0)]
        public MISMOAmount FeeSummaryTotalFeesPaidToAmount { get; set; }
    
        [XmlIgnore]
        public bool FeeSummaryTotalFeesPaidToAmountSpecified
        {
            get { return this.FeeSummaryTotalFeesPaidToAmount != null; }
            set { }
        }
    
        [XmlElement("FeeSummaryTotalFeesPaidToType", Order = 1)]
        public MISMOEnum<FeeSummaryTotalFeesPaidToBase> FeeSummaryTotalFeesPaidToType { get; set; }
    
        [XmlIgnore]
        public bool FeeSummaryTotalFeesPaidToTypeSpecified
        {
            get { return this.FeeSummaryTotalFeesPaidToType != null; }
            set { }
        }
    
        [XmlElement("FeeSummaryTotalFeesPaidToTypeOtherDescription", Order = 2)]
        public MISMOString FeeSummaryTotalFeesPaidToTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool FeeSummaryTotalFeesPaidToTypeOtherDescriptionSpecified
        {
            get { return this.FeeSummaryTotalFeesPaidToTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public FEE_SUMMARY_TOTAL_FEES_PAID_TO_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
