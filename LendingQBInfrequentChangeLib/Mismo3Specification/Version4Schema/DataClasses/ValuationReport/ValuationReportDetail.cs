namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class VALUATION_REPORT_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AppraiserAdditionalFileIdentifierSpecified
                    || this.AppraiserFileAdditionalIdentifierDescriptionSpecified
                    || this.AppraiserFileIdentifierSpecified
                    || this.AppraiserFileIdentifierDescriptionSpecified
                    || this.AppraiserReportSignedDateSpecified
                    || this.LoanOriginationSystemLoanIdentifierSpecified
                    || this.SupervisorReportSignedDateSpecified
                    || this.ValuationAddendumTextSpecified
                    || this.ValuationReportContentDescriptionSpecified
                    || this.ValuationReportContentIdentifierSpecified
                    || this.ValuationReportContentNameSpecified
                    || this.ValuationReportContentTypeSpecified
                    || this.ValuationReportContentTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AppraiserAdditionalFileIdentifier", Order = 0)]
        public MISMOIdentifier AppraiserAdditionalFileIdentifier { get; set; }
    
        [XmlIgnore]
        public bool AppraiserAdditionalFileIdentifierSpecified
        {
            get { return this.AppraiserAdditionalFileIdentifier != null; }
            set { }
        }
    
        [XmlElement("AppraiserFileAdditionalIdentifierDescription", Order = 1)]
        public MISMOString AppraiserFileAdditionalIdentifierDescription { get; set; }
    
        [XmlIgnore]
        public bool AppraiserFileAdditionalIdentifierDescriptionSpecified
        {
            get { return this.AppraiserFileAdditionalIdentifierDescription != null; }
            set { }
        }
    
        [XmlElement("AppraiserFileIdentifier", Order = 2)]
        public MISMOIdentifier AppraiserFileIdentifier { get; set; }
    
        [XmlIgnore]
        public bool AppraiserFileIdentifierSpecified
        {
            get { return this.AppraiserFileIdentifier != null; }
            set { }
        }
    
        [XmlElement("AppraiserFileIdentifierDescription", Order = 3)]
        public MISMOString AppraiserFileIdentifierDescription { get; set; }
    
        [XmlIgnore]
        public bool AppraiserFileIdentifierDescriptionSpecified
        {
            get { return this.AppraiserFileIdentifierDescription != null; }
            set { }
        }
    
        [XmlElement("AppraiserReportSignedDate", Order = 4)]
        public MISMODate AppraiserReportSignedDate { get; set; }
    
        [XmlIgnore]
        public bool AppraiserReportSignedDateSpecified
        {
            get { return this.AppraiserReportSignedDate != null; }
            set { }
        }
    
        [XmlElement("LoanOriginationSystemLoanIdentifier", Order = 5)]
        public MISMOIdentifier LoanOriginationSystemLoanIdentifier { get; set; }
    
        [XmlIgnore]
        public bool LoanOriginationSystemLoanIdentifierSpecified
        {
            get { return this.LoanOriginationSystemLoanIdentifier != null; }
            set { }
        }
    
        [XmlElement("SupervisorReportSignedDate", Order = 6)]
        public MISMODate SupervisorReportSignedDate { get; set; }
    
        [XmlIgnore]
        public bool SupervisorReportSignedDateSpecified
        {
            get { return this.SupervisorReportSignedDate != null; }
            set { }
        }
    
        [XmlElement("ValuationAddendumText", Order = 7)]
        public MISMOString ValuationAddendumText { get; set; }
    
        [XmlIgnore]
        public bool ValuationAddendumTextSpecified
        {
            get { return this.ValuationAddendumText != null; }
            set { }
        }
    
        [XmlElement("ValuationReportContentDescription", Order = 8)]
        public MISMOString ValuationReportContentDescription { get; set; }
    
        [XmlIgnore]
        public bool ValuationReportContentDescriptionSpecified
        {
            get { return this.ValuationReportContentDescription != null; }
            set { }
        }
    
        [XmlElement("ValuationReportContentIdentifier", Order = 9)]
        public MISMOIdentifier ValuationReportContentIdentifier { get; set; }
    
        [XmlIgnore]
        public bool ValuationReportContentIdentifierSpecified
        {
            get { return this.ValuationReportContentIdentifier != null; }
            set { }
        }
    
        [XmlElement("ValuationReportContentName", Order = 10)]
        public MISMOString ValuationReportContentName { get; set; }
    
        [XmlIgnore]
        public bool ValuationReportContentNameSpecified
        {
            get { return this.ValuationReportContentName != null; }
            set { }
        }
    
        [XmlElement("ValuationReportContentType", Order = 11)]
        public MISMOEnum<ValuationReportContentBase> ValuationReportContentType { get; set; }
    
        [XmlIgnore]
        public bool ValuationReportContentTypeSpecified
        {
            get { return this.ValuationReportContentType != null; }
            set { }
        }
    
        [XmlElement("ValuationReportContentTypeOtherDescription", Order = 12)]
        public MISMOString ValuationReportContentTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ValuationReportContentTypeOtherDescriptionSpecified
        {
            get { return this.ValuationReportContentTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 13)]
        public VALUATION_REPORT_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
