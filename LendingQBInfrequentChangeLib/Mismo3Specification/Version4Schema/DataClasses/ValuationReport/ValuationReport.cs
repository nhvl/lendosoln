namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class VALUATION_REPORT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ApproachToValueSpecified
                    || this.ScopeOfWorkSpecified
                    || this.ValuationAnalysesSpecified
                    || this.ValuationFormsSpecified
                    || this.ValuationReconciliationSpecified
                    || this.ValuationReportDetailSpecified
                    || this.ValuationReviewSpecified
                    || this.ValuationUpdateSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("APPROACH_TO_VALUE", Order = 0)]
        public APPROACH_TO_VALUE ApproachToValue { get; set; }
    
        [XmlIgnore]
        public bool ApproachToValueSpecified
        {
            get { return this.ApproachToValue != null && this.ApproachToValue.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SCOPE_OF_WORK", Order = 1)]
        public SCOPE_OF_WORK ScopeOfWork { get; set; }
    
        [XmlIgnore]
        public bool ScopeOfWorkSpecified
        {
            get { return this.ScopeOfWork != null && this.ScopeOfWork.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("VALUATION_ANALYSES", Order = 2)]
        public VALUATION_ANALYSES ValuationAnalyses { get; set; }
    
        [XmlIgnore]
        public bool ValuationAnalysesSpecified
        {
            get { return this.ValuationAnalyses != null && this.ValuationAnalyses.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("VALUATION_FORMS", Order = 3)]
        public VALUATION_FORMS ValuationForms { get; set; }
    
        [XmlIgnore]
        public bool ValuationFormsSpecified
        {
            get { return this.ValuationForms != null && this.ValuationForms.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("VALUATION_RECONCILIATION", Order = 4)]
        public VALUATION_RECONCILIATION ValuationReconciliation { get; set; }
    
        [XmlIgnore]
        public bool ValuationReconciliationSpecified
        {
            get { return this.ValuationReconciliation != null && this.ValuationReconciliation.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("VALUATION_REPORT_DETAIL", Order = 5)]
        public VALUATION_REPORT_DETAIL ValuationReportDetail { get; set; }
    
        [XmlIgnore]
        public bool ValuationReportDetailSpecified
        {
            get { return this.ValuationReportDetail != null && this.ValuationReportDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("VALUATION_REVIEW", Order = 6)]
        public VALUATION_REVIEW ValuationReview { get; set; }
    
        [XmlIgnore]
        public bool ValuationReviewSpecified
        {
            get { return this.ValuationReview != null && this.ValuationReview.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("VALUATION_UPDATE", Order = 7)]
        public VALUATION_UPDATE ValuationUpdate { get; set; }
    
        [XmlIgnore]
        public bool ValuationUpdateSpecified
        {
            get { return this.ValuationUpdate != null && this.ValuationUpdate.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 8)]
        public VALUATION_REPORT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
