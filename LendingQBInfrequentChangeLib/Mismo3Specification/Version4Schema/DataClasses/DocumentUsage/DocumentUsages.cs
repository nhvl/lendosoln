namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class DOCUMENT_USAGES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DocumentUsageListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DOCUMENT_USAGE", Order = 0)]
        public List<DOCUMENT_USAGE> DocumentUsageList { get; set; } = new List<DOCUMENT_USAGE>();
    
        [XmlIgnore]
        public bool DocumentUsageListSpecified
        {
            get { return this.DocumentUsageList != null && this.DocumentUsageList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public DOCUMENT_USAGES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
