namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class DOCUMENT_USAGE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DocumentUsageCodeSpecified
                    || this.DocumentUsageDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DocumentUsageCode", Order = 0)]
        public MISMOCode DocumentUsageCode { get; set; }
    
        [XmlIgnore]
        public bool DocumentUsageCodeSpecified
        {
            get { return this.DocumentUsageCode != null; }
            set { }
        }
    
        [XmlElement("DocumentUsageDescription", Order = 1)]
        public MISMOString DocumentUsageDescription { get; set; }
    
        [XmlIgnore]
        public bool DocumentUsageDescriptionSpecified
        {
            get { return this.DocumentUsageDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public DOCUMENT_USAGE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
