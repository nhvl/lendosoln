namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PHASE_STRUCTURE_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ElevatorCountSpecified
                    || this.PropertyStructureBuiltYearSpecified
                    || this.StoriesCountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ElevatorCount", Order = 0)]
        public MISMOCount ElevatorCount { get; set; }
    
        [XmlIgnore]
        public bool ElevatorCountSpecified
        {
            get { return this.ElevatorCount != null; }
            set { }
        }
    
        [XmlElement("PropertyStructureBuiltYear", Order = 1)]
        public MISMOYear PropertyStructureBuiltYear { get; set; }
    
        [XmlIgnore]
        public bool PropertyStructureBuiltYearSpecified
        {
            get { return this.PropertyStructureBuiltYear != null; }
            set { }
        }
    
        [XmlElement("StoriesCount", Order = 2)]
        public MISMOCount StoriesCount { get; set; }
    
        [XmlIgnore]
        public bool StoriesCountSpecified
        {
            get { return this.StoriesCount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public PHASE_STRUCTURE_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
