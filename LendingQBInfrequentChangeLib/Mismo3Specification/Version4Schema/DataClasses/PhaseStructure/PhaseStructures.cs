namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class PHASE_STRUCTURES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PhaseStructureListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PHASE_STRUCTURE", Order = 0)]
        public List<PHASE_STRUCTURE> PhaseStructureList { get; set; } = new List<PHASE_STRUCTURE>();
    
        [XmlIgnore]
        public bool PhaseStructureListSpecified
        {
            get { return this.PhaseStructureList != null && this.PhaseStructureList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public PHASE_STRUCTURES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
