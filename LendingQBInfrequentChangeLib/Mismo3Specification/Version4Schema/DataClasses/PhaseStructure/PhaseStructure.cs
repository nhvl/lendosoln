namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PHASE_STRUCTURE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PhaseStructureCarStoragesSpecified
                    || this.PhaseStructureCommonElementsSpecified
                    || this.PhaseStructureConversionSpecified
                    || this.PhaseStructureDetailSpecified
                    || this.PhaseStructureHousingUnitInventoriesSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PHASE_STRUCTURE_CAR_STORAGES", Order = 0)]
        public CAR_STORAGES PhaseStructureCarStorages { get; set; }
    
        [XmlIgnore]
        public bool PhaseStructureCarStoragesSpecified
        {
            get { return this.PhaseStructureCarStorages != null; }
            set { }
        }
    
        [XmlElement("PHASE_STRUCTURE_COMMON_ELEMENTS", Order = 1)]
        public COMMON_ELEMENTS PhaseStructureCommonElements { get; set; }
    
        [XmlIgnore]
        public bool PhaseStructureCommonElementsSpecified
        {
            get { return this.PhaseStructureCommonElements != null; }
            set { }
        }
    
        [XmlElement("PHASE_STRUCTURE_CONVERSION", Order = 2)]
        public CONVERSION PhaseStructureConversion { get; set; }
    
        [XmlIgnore]
        public bool PhaseStructureConversionSpecified
        {
            get { return this.PhaseStructureConversion != null; }
            set { }
        }
    
        [XmlElement("PHASE_STRUCTURE_DETAIL", Order = 3)]
        public PHASE_STRUCTURE_DETAIL PhaseStructureDetail { get; set; }
    
        [XmlIgnore]
        public bool PhaseStructureDetailSpecified
        {
            get { return this.PhaseStructureDetail != null && this.PhaseStructureDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PHASE_STRUCTURE_HOUSING_UNIT_INVENTORIES", Order = 4)]
        public HOUSING_UNIT_INVENTORIES PhaseStructureHousingUnitInventories { get; set; }
    
        [XmlIgnore]
        public bool PhaseStructureHousingUnitInventoriesSpecified
        {
            get { return this.PhaseStructureHousingUnitInventories != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public PHASE_STRUCTURE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
