namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class APPRAISER_SUPERVISOR
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AppraiserSupervisorDetailSpecified
                    || this.DesignationsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("APPRAISER_SUPERVISOR_DETAIL", Order = 0)]
        public APPRAISER_SUPERVISOR_DETAIL AppraiserSupervisorDetail { get; set; }
    
        [XmlIgnore]
        public bool AppraiserSupervisorDetailSpecified
        {
            get { return this.AppraiserSupervisorDetail != null && this.AppraiserSupervisorDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("DESIGNATIONS", Order = 1)]
        public DESIGNATIONS Designations { get; set; }
    
        [XmlIgnore]
        public bool DesignationsSpecified
        {
            get { return this.Designations != null && this.Designations.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public APPRAISER_SUPERVISOR_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
