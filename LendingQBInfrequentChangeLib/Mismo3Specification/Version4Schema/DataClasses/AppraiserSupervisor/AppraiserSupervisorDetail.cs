namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class APPRAISER_SUPERVISOR_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AppraiserSupervisorCompanyNameSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AppraiserSupervisorCompanyName", Order = 0)]
        public MISMOString AppraiserSupervisorCompanyName { get; set; }
    
        [XmlIgnore]
        public bool AppraiserSupervisorCompanyNameSpecified
        {
            get { return this.AppraiserSupervisorCompanyName != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public APPRAISER_SUPERVISOR_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
