namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class STRUCTURE_ANALYSIS_RATING
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ConditionRatingTypeSpecified
                    || this.StructureAnalysisRatingCommentDescriptionSpecified
                    || this.StructureAnalysisRatingItemTypeSpecified
                    || this.StructureAnalysisRatingItemTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ConditionRatingType", Order = 0)]
        public MISMOEnum<ConditionRatingBase> ConditionRatingType { get; set; }
    
        [XmlIgnore]
        public bool ConditionRatingTypeSpecified
        {
            get { return this.ConditionRatingType != null; }
            set { }
        }
    
        [XmlElement("StructureAnalysisRatingCommentDescription", Order = 1)]
        public MISMOString StructureAnalysisRatingCommentDescription { get; set; }
    
        [XmlIgnore]
        public bool StructureAnalysisRatingCommentDescriptionSpecified
        {
            get { return this.StructureAnalysisRatingCommentDescription != null; }
            set { }
        }
    
        [XmlElement("StructureAnalysisRatingItemType", Order = 2)]
        public MISMOEnum<StructureAnalysisRatingItemBase> StructureAnalysisRatingItemType { get; set; }
    
        [XmlIgnore]
        public bool StructureAnalysisRatingItemTypeSpecified
        {
            get { return this.StructureAnalysisRatingItemType != null; }
            set { }
        }
    
        [XmlElement("StructureAnalysisRatingItemTypeOtherDescription", Order = 3)]
        public MISMOString StructureAnalysisRatingItemTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool StructureAnalysisRatingItemTypeOtherDescriptionSpecified
        {
            get { return this.StructureAnalysisRatingItemTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public STRUCTURE_ANALYSIS_RATING_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
