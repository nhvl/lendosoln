namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class STRUCTURE_ANALYSIS_RATINGS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.StructureAnalysisRatingListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("STRUCTURE_ANALYSIS_RATING", Order = 0)]
        public List<STRUCTURE_ANALYSIS_RATING> StructureAnalysisRatingList { get; set; } = new List<STRUCTURE_ANALYSIS_RATING>();
    
        [XmlIgnore]
        public bool StructureAnalysisRatingListSpecified
        {
            get { return this.StructureAnalysisRatingList != null && this.StructureAnalysisRatingList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public STRUCTURE_ANALYSIS_RATINGS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
