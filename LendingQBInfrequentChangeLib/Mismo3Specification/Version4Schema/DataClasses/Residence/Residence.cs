namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class RESIDENCE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AddressSpecified
                    || this.LandlordSpecified
                    || this.ResidenceDetailSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ADDRESS", Order = 0)]
        public ADDRESS Address { get; set; }
    
        [XmlIgnore]
        public bool AddressSpecified
        {
            get { return this.Address != null && this.Address.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("LANDLORD", Order = 1)]
        public LANDLORD Landlord { get; set; }
    
        [XmlIgnore]
        public bool LandlordSpecified
        {
            get { return this.Landlord != null && this.Landlord.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("RESIDENCE_DETAIL", Order = 2)]
        public RESIDENCE_DETAIL ResidenceDetail { get; set; }
    
        [XmlIgnore]
        public bool ResidenceDetailSpecified
        {
            get { return this.ResidenceDetail != null && this.ResidenceDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public RESIDENCE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
