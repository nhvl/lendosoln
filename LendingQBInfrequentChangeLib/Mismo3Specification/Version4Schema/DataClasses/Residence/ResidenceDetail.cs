namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class RESIDENCE_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BorrowerResidencyBasisTypeSpecified
                    || this.BorrowerResidencyDurationMonthsCountSpecified
                    || this.BorrowerResidencyDurationYearsCountSpecified
                    || this.BorrowerResidencyEndDateSpecified
                    || this.BorrowerResidencyReportedDateSpecified
                    || this.BorrowerResidencyStartDateSpecified
                    || this.BorrowerResidencyTypeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BorrowerResidencyBasisType", Order = 0)]
        public MISMOEnum<BorrowerResidencyBasisBase> BorrowerResidencyBasisType { get; set; }
    
        [XmlIgnore]
        public bool BorrowerResidencyBasisTypeSpecified
        {
            get { return this.BorrowerResidencyBasisType != null; }
            set { }
        }
    
        [XmlElement("BorrowerResidencyDurationMonthsCount", Order = 1)]
        public MISMOCount BorrowerResidencyDurationMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool BorrowerResidencyDurationMonthsCountSpecified
        {
            get { return this.BorrowerResidencyDurationMonthsCount != null; }
            set { }
        }
    
        [XmlElement("BorrowerResidencyDurationYearsCount", Order = 2)]
        public MISMOCount BorrowerResidencyDurationYearsCount { get; set; }
    
        [XmlIgnore]
        public bool BorrowerResidencyDurationYearsCountSpecified
        {
            get { return this.BorrowerResidencyDurationYearsCount != null; }
            set { }
        }
    
        [XmlElement("BorrowerResidencyEndDate", Order = 3)]
        public MISMODate BorrowerResidencyEndDate { get; set; }
    
        [XmlIgnore]
        public bool BorrowerResidencyEndDateSpecified
        {
            get { return this.BorrowerResidencyEndDate != null; }
            set { }
        }
    
        [XmlElement("BorrowerResidencyReportedDate", Order = 4)]
        public MISMODate BorrowerResidencyReportedDate { get; set; }
    
        [XmlIgnore]
        public bool BorrowerResidencyReportedDateSpecified
        {
            get { return this.BorrowerResidencyReportedDate != null; }
            set { }
        }
    
        [XmlElement("BorrowerResidencyStartDate", Order = 5)]
        public MISMODate BorrowerResidencyStartDate { get; set; }
    
        [XmlIgnore]
        public bool BorrowerResidencyStartDateSpecified
        {
            get { return this.BorrowerResidencyStartDate != null; }
            set { }
        }
    
        [XmlElement("BorrowerResidencyType", Order = 6)]
        public MISMOEnum<BorrowerResidencyBase> BorrowerResidencyType { get; set; }
    
        [XmlIgnore]
        public bool BorrowerResidencyTypeSpecified
        {
            get { return this.BorrowerResidencyType != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 7)]
        public RESIDENCE_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
