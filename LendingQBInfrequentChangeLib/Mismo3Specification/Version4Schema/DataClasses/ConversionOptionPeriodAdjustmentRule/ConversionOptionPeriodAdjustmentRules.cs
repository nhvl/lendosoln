namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class CONVERSION_OPTION_PERIOD_ADJUSTMENT_RULES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ConversionOptionPeriodAdjustmentRuleListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CONVERSION_OPTION_PERIOD_ADJUSTMENT_RULE", Order = 0)]
        public List<CONVERSION_OPTION_PERIOD_ADJUSTMENT_RULE> ConversionOptionPeriodAdjustmentRuleList { get; set; } = new List<CONVERSION_OPTION_PERIOD_ADJUSTMENT_RULE>();
    
        [XmlIgnore]
        public bool ConversionOptionPeriodAdjustmentRuleListSpecified
        {
            get { return this.ConversionOptionPeriodAdjustmentRuleList != null && this.ConversionOptionPeriodAdjustmentRuleList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public CONVERSION_OPTION_PERIOD_ADJUSTMENT_RULES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
