namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CONVERSION_OPTION_PERIOD_ADJUSTMENT_RULE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ConversionOptionPeriodAdjustmentEffectiveDateSpecified
                    || this.ConversionOptionPeriodDemandNotificationFrequencyCountSpecified
                    || this.ConversionOptionPeriodExpirationDateSpecified
                    || this.ConversionOptionPeriodFeeAmountSpecified
                    || this.ConversionOptionPeriodFeeBalanceCalculationTypeSpecified
                    || this.ConversionOptionPeriodFeePercentSpecified
                    || this.ConversionOptionPeriodReplyDaysCountSpecified
                    || this.ConversionOptionPeriodRoundingPercentSpecified
                    || this.ConversionOptionPeriodRoundingTypeSpecified
                    || this.ConversionOptionPeriodTypeSpecified
                    || this.ConversionRateCalculationMethodDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ConversionOptionPeriodAdjustmentEffectiveDate", Order = 0)]
        public MISMODate ConversionOptionPeriodAdjustmentEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool ConversionOptionPeriodAdjustmentEffectiveDateSpecified
        {
            get { return this.ConversionOptionPeriodAdjustmentEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("ConversionOptionPeriodDemandNotificationFrequencyCount", Order = 1)]
        public MISMOCount ConversionOptionPeriodDemandNotificationFrequencyCount { get; set; }
    
        [XmlIgnore]
        public bool ConversionOptionPeriodDemandNotificationFrequencyCountSpecified
        {
            get { return this.ConversionOptionPeriodDemandNotificationFrequencyCount != null; }
            set { }
        }
    
        [XmlElement("ConversionOptionPeriodExpirationDate", Order = 2)]
        public MISMODate ConversionOptionPeriodExpirationDate { get; set; }
    
        [XmlIgnore]
        public bool ConversionOptionPeriodExpirationDateSpecified
        {
            get { return this.ConversionOptionPeriodExpirationDate != null; }
            set { }
        }
    
        [XmlElement("ConversionOptionPeriodFeeAmount", Order = 3)]
        public MISMOAmount ConversionOptionPeriodFeeAmount { get; set; }
    
        [XmlIgnore]
        public bool ConversionOptionPeriodFeeAmountSpecified
        {
            get { return this.ConversionOptionPeriodFeeAmount != null; }
            set { }
        }
    
        [XmlElement("ConversionOptionPeriodFeeBalanceCalculationType", Order = 4)]
        public MISMOEnum<ConversionOptionPeriodFeeBalanceCalculationBase> ConversionOptionPeriodFeeBalanceCalculationType { get; set; }
    
        [XmlIgnore]
        public bool ConversionOptionPeriodFeeBalanceCalculationTypeSpecified
        {
            get { return this.ConversionOptionPeriodFeeBalanceCalculationType != null; }
            set { }
        }
    
        [XmlElement("ConversionOptionPeriodFeePercent", Order = 5)]
        public MISMOPercent ConversionOptionPeriodFeePercent { get; set; }
    
        [XmlIgnore]
        public bool ConversionOptionPeriodFeePercentSpecified
        {
            get { return this.ConversionOptionPeriodFeePercent != null; }
            set { }
        }
    
        [XmlElement("ConversionOptionPeriodReplyDaysCount", Order = 6)]
        public MISMOCount ConversionOptionPeriodReplyDaysCount { get; set; }
    
        [XmlIgnore]
        public bool ConversionOptionPeriodReplyDaysCountSpecified
        {
            get { return this.ConversionOptionPeriodReplyDaysCount != null; }
            set { }
        }
    
        [XmlElement("ConversionOptionPeriodRoundingPercent", Order = 7)]
        public MISMOPercent ConversionOptionPeriodRoundingPercent { get; set; }
    
        [XmlIgnore]
        public bool ConversionOptionPeriodRoundingPercentSpecified
        {
            get { return this.ConversionOptionPeriodRoundingPercent != null; }
            set { }
        }
    
        [XmlElement("ConversionOptionPeriodRoundingType", Order = 8)]
        public MISMOEnum<ConversionOptionPeriodRoundingBase> ConversionOptionPeriodRoundingType { get; set; }
    
        [XmlIgnore]
        public bool ConversionOptionPeriodRoundingTypeSpecified
        {
            get { return this.ConversionOptionPeriodRoundingType != null; }
            set { }
        }
    
        [XmlElement("ConversionOptionPeriodType", Order = 9)]
        public MISMOEnum<ConversionOptionPeriodBase> ConversionOptionPeriodType { get; set; }
    
        [XmlIgnore]
        public bool ConversionOptionPeriodTypeSpecified
        {
            get { return this.ConversionOptionPeriodType != null; }
            set { }
        }
    
        [XmlElement("ConversionRateCalculationMethodDescription", Order = 10)]
        public MISMOString ConversionRateCalculationMethodDescription { get; set; }
    
        [XmlIgnore]
        public bool ConversionRateCalculationMethodDescriptionSpecified
        {
            get { return this.ConversionRateCalculationMethodDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 11)]
        public CONVERSION_OPTION_PERIOD_ADJUSTMENT_RULE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
