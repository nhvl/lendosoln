namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class DISASTERS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DisasterListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DISASTER", Order = 0)]
        public List<DISASTER> DisasterList { get; set; } = new List<DISASTER>();
    
        [XmlIgnore]
        public bool DisasterListSpecified
        {
            get { return this.DisasterList != null && this.DisasterList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public DISASTERS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
