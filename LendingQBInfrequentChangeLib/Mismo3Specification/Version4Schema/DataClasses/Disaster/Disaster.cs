namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class DISASTER
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DisasterAreaDescriptionSpecified
                    || this.DisasterDeclarationDateSpecified
                    || this.DisasterTypeSpecified
                    || this.DisasterTypeOtherDescriptionSpecified
                    || this.FEMADisasterNameSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DisasterAreaDescription", Order = 0)]
        public MISMOString DisasterAreaDescription { get; set; }
    
        [XmlIgnore]
        public bool DisasterAreaDescriptionSpecified
        {
            get { return this.DisasterAreaDescription != null; }
            set { }
        }
    
        [XmlElement("DisasterDeclarationDate", Order = 1)]
        public MISMODate DisasterDeclarationDate { get; set; }
    
        [XmlIgnore]
        public bool DisasterDeclarationDateSpecified
        {
            get { return this.DisasterDeclarationDate != null; }
            set { }
        }
    
        [XmlElement("DisasterType", Order = 2)]
        public MISMOEnum<DisasterBase> DisasterType { get; set; }
    
        [XmlIgnore]
        public bool DisasterTypeSpecified
        {
            get { return this.DisasterType != null; }
            set { }
        }
    
        [XmlElement("DisasterTypeOtherDescription", Order = 3)]
        public MISMOString DisasterTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool DisasterTypeOtherDescriptionSpecified
        {
            get { return this.DisasterTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("FEMADisasterName", Order = 4)]
        public MISMOString FEMADisasterName { get; set; }
    
        [XmlIgnore]
        public bool FEMADisasterNameSpecified
        {
            get { return this.FEMADisasterName != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public DISASTER_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
