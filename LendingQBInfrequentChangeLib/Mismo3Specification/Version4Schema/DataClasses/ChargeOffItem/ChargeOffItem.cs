namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CHARGE_OFF_ITEM
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ChargeOffItemAmountSpecified
                    || this.ChargeOffItemTypeSpecified
                    || this.ChargeOffItemTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ChargeOffItemAmount", Order = 0)]
        public MISMOAmount ChargeOffItemAmount { get; set; }
    
        [XmlIgnore]
        public bool ChargeOffItemAmountSpecified
        {
            get { return this.ChargeOffItemAmount != null; }
            set { }
        }
    
        [XmlElement("ChargeOffItemType", Order = 1)]
        public MISMOEnum<ChargeOffItemBase> ChargeOffItemType { get; set; }
    
        [XmlIgnore]
        public bool ChargeOffItemTypeSpecified
        {
            get { return this.ChargeOffItemType != null; }
            set { }
        }
    
        [XmlElement("ChargeOffItemTypeOtherDescription", Order = 2)]
        public MISMOString ChargeOffItemTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ChargeOffItemTypeOtherDescriptionSpecified
        {
            get { return this.ChargeOffItemTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public CHARGE_OFF_ITEM_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
