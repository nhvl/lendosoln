namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class CHARGE_OFF_ITEMS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ChargeOffItemListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CHARGE_OFF_ITEM", Order = 0)]
        public List<CHARGE_OFF_ITEM> ChargeOffItemList { get; set; } = new List<CHARGE_OFF_ITEM>();
    
        [XmlIgnore]
        public bool ChargeOffItemListSpecified
        {
            get { return this.ChargeOffItemList != null && this.ChargeOffItemList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public CHARGE_OFF_ITEMS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
