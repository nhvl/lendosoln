namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class RECORDING_TRANSACTION_IDENTIFIER
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.RecordingTransactionIdentifierSpecified
                    || this.RecordingTransactionIdentifierTypeSpecified
                    || this.RecordingTransactionIdentifierTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("RecordingTransactionIdentifier", Order = 0)]
        public MISMOIdentifier RecordingTransactionIdentifier { get; set; }
    
        [XmlIgnore]
        public bool RecordingTransactionIdentifierSpecified
        {
            get { return this.RecordingTransactionIdentifier != null; }
            set { }
        }
    
        [XmlElement("RecordingTransactionIdentifierType", Order = 1)]
        public MISMOEnum<RecordingTransactionIdentifierBase> RecordingTransactionIdentifierType { get; set; }
    
        [XmlIgnore]
        public bool RecordingTransactionIdentifierTypeSpecified
        {
            get { return this.RecordingTransactionIdentifierType != null; }
            set { }
        }
    
        [XmlElement("RecordingTransactionIdentifierTypeOtherDescription", Order = 2)]
        public MISMOString RecordingTransactionIdentifierTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool RecordingTransactionIdentifierTypeOtherDescriptionSpecified
        {
            get { return this.RecordingTransactionIdentifierTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public RECORDING_TRANSACTION_IDENTIFIER_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
