namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class IDENTITY_DOCUMENTATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.IdentityDocumentExpirationDateSpecified
                    || this.IdentityDocumentIdentifierSpecified
                    || this.IdentityDocumentIssuedByCountryNameSpecified
                    || this.IdentityDocumentIssuedByNameSpecified
                    || this.IdentityDocumentIssuedByStateCodeSpecified
                    || this.IdentityDocumentIssuedByStateProvinceNameSpecified
                    || this.IdentityDocumentIssuedByTypeSpecified
                    || this.IdentityDocumentIssuedByTypeOtherDescriptionSpecified
                    || this.IdentityDocumentIssuedDateSpecified
                    || this.IdentityDocumentTypeSpecified
                    || this.IdentityDocumentTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("IdentityDocumentExpirationDate", Order = 0)]
        public MISMODate IdentityDocumentExpirationDate { get; set; }
    
        [XmlIgnore]
        public bool IdentityDocumentExpirationDateSpecified
        {
            get { return this.IdentityDocumentExpirationDate != null; }
            set { }
        }
    
        [XmlElement("IdentityDocumentIdentifier", Order = 1)]
        public MISMOIdentifier IdentityDocumentIdentifier { get; set; }
    
        [XmlIgnore]
        public bool IdentityDocumentIdentifierSpecified
        {
            get { return this.IdentityDocumentIdentifier != null; }
            set { }
        }
    
        [XmlElement("IdentityDocumentIssuedByCountryName", Order = 2)]
        public MISMOString IdentityDocumentIssuedByCountryName { get; set; }
    
        [XmlIgnore]
        public bool IdentityDocumentIssuedByCountryNameSpecified
        {
            get { return this.IdentityDocumentIssuedByCountryName != null; }
            set { }
        }
    
        [XmlElement("IdentityDocumentIssuedByName", Order = 3)]
        public MISMOString IdentityDocumentIssuedByName { get; set; }
    
        [XmlIgnore]
        public bool IdentityDocumentIssuedByNameSpecified
        {
            get { return this.IdentityDocumentIssuedByName != null; }
            set { }
        }
    
        [XmlElement("IdentityDocumentIssuedByStateCode", Order = 4)]
        public MISMOCode IdentityDocumentIssuedByStateCode { get; set; }
    
        [XmlIgnore]
        public bool IdentityDocumentIssuedByStateCodeSpecified
        {
            get { return this.IdentityDocumentIssuedByStateCode != null; }
            set { }
        }
    
        [XmlElement("IdentityDocumentIssuedByStateProvinceName", Order = 5)]
        public MISMOString IdentityDocumentIssuedByStateProvinceName { get; set; }
    
        [XmlIgnore]
        public bool IdentityDocumentIssuedByStateProvinceNameSpecified
        {
            get { return this.IdentityDocumentIssuedByStateProvinceName != null; }
            set { }
        }
    
        [XmlElement("IdentityDocumentIssuedByType", Order = 6)]
        public MISMOEnum<IdentityDocumentIssuedByBase> IdentityDocumentIssuedByType { get; set; }
    
        [XmlIgnore]
        public bool IdentityDocumentIssuedByTypeSpecified
        {
            get { return this.IdentityDocumentIssuedByType != null; }
            set { }
        }
    
        [XmlElement("IdentityDocumentIssuedByTypeOtherDescription", Order = 7)]
        public MISMOString IdentityDocumentIssuedByTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool IdentityDocumentIssuedByTypeOtherDescriptionSpecified
        {
            get { return this.IdentityDocumentIssuedByTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("IdentityDocumentIssuedDate", Order = 8)]
        public MISMODate IdentityDocumentIssuedDate { get; set; }
    
        [XmlIgnore]
        public bool IdentityDocumentIssuedDateSpecified
        {
            get { return this.IdentityDocumentIssuedDate != null; }
            set { }
        }
    
        [XmlElement("IdentityDocumentType", Order = 9)]
        public MISMOEnum<IdentityDocumentBase> IdentityDocumentType { get; set; }
    
        [XmlIgnore]
        public bool IdentityDocumentTypeSpecified
        {
            get { return this.IdentityDocumentType != null; }
            set { }
        }
    
        [XmlElement("IdentityDocumentTypeOtherDescription", Order = 10)]
        public MISMOString IdentityDocumentTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool IdentityDocumentTypeOtherDescriptionSpecified
        {
            get { return this.IdentityDocumentTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 11)]
        public IDENTITY_DOCUMENTATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
