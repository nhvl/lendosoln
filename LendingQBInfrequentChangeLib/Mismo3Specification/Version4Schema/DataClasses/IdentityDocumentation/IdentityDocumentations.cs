namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class IDENTITY_DOCUMENTATIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.IdentityDocumentationListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("IDENTITY_DOCUMENTATION", Order = 0)]
        public List<IDENTITY_DOCUMENTATION> IdentityDocumentationList { get; set; } = new List<IDENTITY_DOCUMENTATION>();
    
        [XmlIgnore]
        public bool IdentityDocumentationListSpecified
        {
            get { return this.IdentityDocumentationList != null && this.IdentityDocumentationList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public IDENTITY_DOCUMENTATIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
