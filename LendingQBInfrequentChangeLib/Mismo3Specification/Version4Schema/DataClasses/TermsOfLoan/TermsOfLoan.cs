namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class TERMS_OF_LOAN
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AssumedLoanAmountSpecified
                    || this.BaseLoanAmountSpecified
                    || this.DisclosedFullyIndexedRatePercentSpecified
                    || this.DisclosedIndexRatePercentSpecified
                    || this.DisclosedMarginRatePercentSpecified
                    || this.LienPriorityTypeSpecified
                    || this.LienPriorityTypeOtherDescriptionSpecified
                    || this.LoanPurposeTypeSpecified
                    || this.LoanPurposeTypeOtherDescriptionSpecified
                    || this.MortgageTypeSpecified
                    || this.MortgageTypeOtherDescriptionSpecified
                    || this.NoteAmountSpecified
                    || this.NoteCityNameSpecified
                    || this.NoteDateSpecified
                    || this.NoteRatePercentSpecified
                    || this.NoteStateNameSpecified
                    || this.OriginalInterestRateDiscountPercentSpecified
                    || this.SharedAppreciationCapAmountSpecified
                    || this.SharedAppreciationRatePercentSpecified
                    || this.SupplementalMortgageTypeSpecified
                    || this.SupplementalMortgageTypeOtherDescriptionSpecified
                    || this.UPBChangeFrequencyTypeSpecified
                    || this.WeightedAverageInterestRatePercentSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AssumedLoanAmount", Order = 0)]
        public MISMOAmount AssumedLoanAmount { get; set; }
    
        [XmlIgnore]
        public bool AssumedLoanAmountSpecified
        {
            get { return this.AssumedLoanAmount != null; }
            set { }
        }
    
        [XmlElement("BaseLoanAmount", Order = 1)]
        public MISMOAmount BaseLoanAmount { get; set; }
    
        [XmlIgnore]
        public bool BaseLoanAmountSpecified
        {
            get { return this.BaseLoanAmount != null; }
            set { }
        }
    
        [XmlElement("DisclosedFullyIndexedRatePercent", Order = 2)]
        public MISMOPercent DisclosedFullyIndexedRatePercent { get; set; }
    
        [XmlIgnore]
        public bool DisclosedFullyIndexedRatePercentSpecified
        {
            get { return this.DisclosedFullyIndexedRatePercent != null; }
            set { }
        }
    
        [XmlElement("DisclosedIndexRatePercent", Order = 3)]
        public MISMOPercent DisclosedIndexRatePercent { get; set; }
    
        [XmlIgnore]
        public bool DisclosedIndexRatePercentSpecified
        {
            get { return this.DisclosedIndexRatePercent != null; }
            set { }
        }
    
        [XmlElement("DisclosedMarginRatePercent", Order = 4)]
        public MISMOPercent DisclosedMarginRatePercent { get; set; }
    
        [XmlIgnore]
        public bool DisclosedMarginRatePercentSpecified
        {
            get { return this.DisclosedMarginRatePercent != null; }
            set { }
        }
    
        [XmlElement("LienPriorityType", Order = 5)]
        public MISMOEnum<LienPriorityBase> LienPriorityType { get; set; }
    
        [XmlIgnore]
        public bool LienPriorityTypeSpecified
        {
            get { return this.LienPriorityType != null; }
            set { }
        }
    
        [XmlElement("LienPriorityTypeOtherDescription", Order = 6)]
        public MISMOString LienPriorityTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool LienPriorityTypeOtherDescriptionSpecified
        {
            get { return this.LienPriorityTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("LoanPurposeType", Order = 7)]
        public MISMOEnum<LoanPurposeBase> LoanPurposeType { get; set; }
    
        [XmlIgnore]
        public bool LoanPurposeTypeSpecified
        {
            get { return this.LoanPurposeType != null; }
            set { }
        }
    
        [XmlElement("LoanPurposeTypeOtherDescription", Order = 8)]
        public MISMOString LoanPurposeTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool LoanPurposeTypeOtherDescriptionSpecified
        {
            get { return this.LoanPurposeTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MortgageType", Order = 9)]
        public MISMOEnum<MortgageBase> MortgageType { get; set; }
    
        [XmlIgnore]
        public bool MortgageTypeSpecified
        {
            get { return this.MortgageType != null; }
            set { }
        }
    
        [XmlElement("MortgageTypeOtherDescription", Order = 10)]
        public MISMOString MortgageTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MortgageTypeOtherDescriptionSpecified
        {
            get { return this.MortgageTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("NoteAmount", Order = 11)]
        public MISMOAmount NoteAmount { get; set; }
    
        [XmlIgnore]
        public bool NoteAmountSpecified
        {
            get { return this.NoteAmount != null; }
            set { }
        }
    
        [XmlElement("NoteCityName", Order = 12)]
        public MISMOString NoteCityName { get; set; }
    
        [XmlIgnore]
        public bool NoteCityNameSpecified
        {
            get { return this.NoteCityName != null; }
            set { }
        }
    
        [XmlElement("NoteDate", Order = 13)]
        public MISMODate NoteDate { get; set; }
    
        [XmlIgnore]
        public bool NoteDateSpecified
        {
            get { return this.NoteDate != null; }
            set { }
        }
    
        [XmlElement("NoteRatePercent", Order = 14)]
        public MISMOPercent NoteRatePercent { get; set; }
    
        [XmlIgnore]
        public bool NoteRatePercentSpecified
        {
            get { return this.NoteRatePercent != null; }
            set { }
        }
    
        [XmlElement("NoteStateName", Order = 15)]
        public MISMOString NoteStateName { get; set; }
    
        [XmlIgnore]
        public bool NoteStateNameSpecified
        {
            get { return this.NoteStateName != null; }
            set { }
        }
    
        [XmlElement("OriginalInterestRateDiscountPercent", Order = 16)]
        public MISMOPercent OriginalInterestRateDiscountPercent { get; set; }
    
        [XmlIgnore]
        public bool OriginalInterestRateDiscountPercentSpecified
        {
            get { return this.OriginalInterestRateDiscountPercent != null; }
            set { }
        }
    
        [XmlElement("SharedAppreciationCapAmount", Order = 17)]
        public MISMOAmount SharedAppreciationCapAmount { get; set; }
    
        [XmlIgnore]
        public bool SharedAppreciationCapAmountSpecified
        {
            get { return this.SharedAppreciationCapAmount != null; }
            set { }
        }
    
        [XmlElement("SharedAppreciationRatePercent", Order = 18)]
        public MISMOPercent SharedAppreciationRatePercent { get; set; }
    
        [XmlIgnore]
        public bool SharedAppreciationRatePercentSpecified
        {
            get { return this.SharedAppreciationRatePercent != null; }
            set { }
        }
    
        [XmlElement("SupplementalMortgageType", Order = 19)]
        public MISMOEnum<MortgageBase> SupplementalMortgageType { get; set; }
    
        [XmlIgnore]
        public bool SupplementalMortgageTypeSpecified
        {
            get { return this.SupplementalMortgageType != null; }
            set { }
        }
    
        [XmlElement("SupplementalMortgageTypeOtherDescription", Order = 20)]
        public MISMOString SupplementalMortgageTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool SupplementalMortgageTypeOtherDescriptionSpecified
        {
            get { return this.SupplementalMortgageTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("UPBChangeFrequencyType", Order = 21)]
        public MISMOEnum<UPBChangeFrequencyBase> UPBChangeFrequencyType { get; set; }
    
        [XmlIgnore]
        public bool UPBChangeFrequencyTypeSpecified
        {
            get { return this.UPBChangeFrequencyType != null; }
            set { }
        }
    
        [XmlElement("WeightedAverageInterestRatePercent", Order = 22)]
        public MISMOPercent WeightedAverageInterestRatePercent { get; set; }
    
        [XmlIgnore]
        public bool WeightedAverageInterestRatePercentSpecified
        {
            get { return this.WeightedAverageInterestRatePercent != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 23)]
        public TERMS_OF_LOAN_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
