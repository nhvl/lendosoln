namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class CLOSING_REVISIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ClosingRevisionListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CLOSING_REVISION", Order = 0)]
        public List<CLOSING_REVISION> ClosingRevisionList { get; set; } = new List<CLOSING_REVISION>();
    
        [XmlIgnore]
        public bool ClosingRevisionListSpecified
        {
            get { return this.ClosingRevisionList != null && this.ClosingRevisionList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public CLOSING_REVISIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
