namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CLOSING_REVISION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ClosingInformationRevisionsSpecified
                    || this.ClosingLocationsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CLOSING_INFORMATION_REVISIONS", Order = 0)]
        public CLOSING_INFORMATION_REVISIONS ClosingInformationRevisions { get; set; }
    
        [XmlIgnore]
        public bool ClosingInformationRevisionsSpecified
        {
            get { return this.ClosingInformationRevisions != null && this.ClosingInformationRevisions.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CLOSING_LOCATIONS", Order = 1)]
        public CLOSING_LOCATIONS ClosingLocations { get; set; }
    
        [XmlIgnore]
        public bool ClosingLocationsSpecified
        {
            get { return this.ClosingLocations != null && this.ClosingLocations.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public CLOSING_REVISION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
