namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class IMAGES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ImageListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("IMAGE", Order = 0)]
        public List<IMAGE> ImageList { get; set; } = new List<IMAGE>();
    
        [XmlIgnore]
        public bool ImageListSpecified
        {
            get { return this.ImageList != null && this.ImageList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public IMAGES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
