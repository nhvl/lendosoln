namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class IMAGE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ImageCaptionCommentDescriptionSpecified
                    || this.ImageDateSpecified
                    || this.ImageGroupNameSpecified
                    || this.ImageGroupSequenceNumberSpecified
                    || this.ImageIdentifierSpecified
                    || this.ImageNameSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ImageCaptionCommentDescription", Order = 0)]
        public MISMOString ImageCaptionCommentDescription { get; set; }
    
        [XmlIgnore]
        public bool ImageCaptionCommentDescriptionSpecified
        {
            get { return this.ImageCaptionCommentDescription != null; }
            set { }
        }
    
        [XmlElement("ImageDate", Order = 1)]
        public MISMODate ImageDate { get; set; }
    
        [XmlIgnore]
        public bool ImageDateSpecified
        {
            get { return this.ImageDate != null; }
            set { }
        }
    
        [XmlElement("ImageGroupName", Order = 2)]
        public MISMOString ImageGroupName { get; set; }
    
        [XmlIgnore]
        public bool ImageGroupNameSpecified
        {
            get { return this.ImageGroupName != null; }
            set { }
        }
    
        [XmlElement("ImageGroupSequenceNumber", Order = 3)]
        public MISMOSequenceNumber ImageGroupSequenceNumber { get; set; }
    
        [XmlIgnore]
        public bool ImageGroupSequenceNumberSpecified
        {
            get { return this.ImageGroupSequenceNumber != null; }
            set { }
        }
    
        [XmlElement("ImageIdentifier", Order = 4)]
        public MISMOIdentifier ImageIdentifier { get; set; }
    
        [XmlIgnore]
        public bool ImageIdentifierSpecified
        {
            get { return this.ImageIdentifier != null; }
            set { }
        }
    
        [XmlElement("ImageName", Order = 5)]
        public MISMOString ImageName { get; set; }
    
        [XmlIgnore]
        public bool ImageNameSpecified
        {
            get { return this.ImageName != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 6)]
        public IMAGE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
