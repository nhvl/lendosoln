namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class DEED_RESTRICTIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DeedRestrictionListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DEED_RESTRICTION", Order = 0)]
        public List<DEED_RESTRICTION> DeedRestrictionList { get; set; } = new List<DEED_RESTRICTION>();
    
        [XmlIgnore]
        public bool DeedRestrictionListSpecified
        {
            get { return this.DeedRestrictionList != null && this.DeedRestrictionList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public DEED_RESTRICTIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
