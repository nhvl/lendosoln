namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class DEED_RESTRICTION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DeedRestrictionTypeSpecified
                    || this.DeedRestrictionTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DeedRestrictionType", Order = 0)]
        public MISMOEnum<DeedRestrictionBase> DeedRestrictionType { get; set; }
    
        [XmlIgnore]
        public bool DeedRestrictionTypeSpecified
        {
            get { return this.DeedRestrictionType != null; }
            set { }
        }
    
        [XmlElement("DeedRestrictionTypeOtherDescription", Order = 1)]
        public MISMOString DeedRestrictionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool DeedRestrictionTypeOtherDescriptionSpecified
        {
            get { return this.DeedRestrictionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public DEED_RESTRICTION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
