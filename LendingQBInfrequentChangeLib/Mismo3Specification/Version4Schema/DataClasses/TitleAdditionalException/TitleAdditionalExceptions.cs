namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class TITLE_ADDITIONAL_EXCEPTIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.TitleAdditionalExceptionListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("TITLE_ADDITIONAL_EXCEPTION", Order = 0)]
        public List<TITLE_ADDITIONAL_EXCEPTION> TitleAdditionalExceptionList { get; set; } = new List<TITLE_ADDITIONAL_EXCEPTION>();
    
        [XmlIgnore]
        public bool TitleAdditionalExceptionListSpecified
        {
            get { return this.TitleAdditionalExceptionList != null && this.TitleAdditionalExceptionList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public TITLE_ADDITIONAL_EXCEPTIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
