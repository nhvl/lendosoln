namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class TITLE_ADDITIONAL_EXCEPTION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.TitleAdditionalExceptionDescriptionSpecified
                    || this.TitleAdditionalExceptionIncludedIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("TitleAdditionalExceptionDescription", Order = 0)]
        public MISMOString TitleAdditionalExceptionDescription { get; set; }
    
        [XmlIgnore]
        public bool TitleAdditionalExceptionDescriptionSpecified
        {
            get { return this.TitleAdditionalExceptionDescription != null; }
            set { }
        }
    
        [XmlElement("TitleAdditionalExceptionIncludedIndicator", Order = 1)]
        public MISMOIndicator TitleAdditionalExceptionIncludedIndicator { get; set; }
    
        [XmlIgnore]
        public bool TitleAdditionalExceptionIncludedIndicatorSpecified
        {
            get { return this.TitleAdditionalExceptionIncludedIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public TITLE_ADDITIONAL_EXCEPTION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
