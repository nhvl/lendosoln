namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CREDIT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditRequestSpecified
                    || this.CreditResponseSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CREDIT_REQUEST", Order = 0)]
        public CREDIT_REQUEST CreditRequest { get; set; }
    
        [XmlIgnore]
        public bool CreditRequestSpecified
        {
            get { return this.CreditRequest != null && this.CreditRequest.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_RESPONSE", Order = 1)]
        public CREDIT_RESPONSE CreditResponse { get; set; }
    
        [XmlIgnore]
        public bool CreditResponseSpecified
        {
            get { return this.CreditResponse != null && this.CreditResponse.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public CREDIT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
