namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ATTIC_FEATURE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AtticFeatureDescriptionSpecified
                    || this.AtticFeatureTypeSpecified
                    || this.AtticFeatureTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AtticFeatureDescription", Order = 0)]
        public MISMOString AtticFeatureDescription { get; set; }
    
        [XmlIgnore]
        public bool AtticFeatureDescriptionSpecified
        {
            get { return this.AtticFeatureDescription != null; }
            set { }
        }
    
        [XmlElement("AtticFeatureType", Order = 1)]
        public MISMOEnum<AtticFeatureBase> AtticFeatureType { get; set; }
    
        [XmlIgnore]
        public bool AtticFeatureTypeSpecified
        {
            get { return this.AtticFeatureType != null; }
            set { }
        }
    
        [XmlElement("AtticFeatureTypeOtherDescription", Order = 2)]
        public MISMOString AtticFeatureTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool AtticFeatureTypeOtherDescriptionSpecified
        {
            get { return this.AtticFeatureTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public ATTIC_FEATURE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
