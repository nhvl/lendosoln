namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class ATTIC_FEATURES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AtticFeatureListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ATTIC_FEATURE", Order = 0)]
        public List<ATTIC_FEATURE> AtticFeatureList { get; set; } = new List<ATTIC_FEATURE>();
    
        [XmlIgnore]
        public bool AtticFeatureListSpecified
        {
            get { return this.AtticFeatureList != null && this.AtticFeatureList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public ATTIC_FEATURES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
