namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class COMMUNICATION_EVENT_ISSUE_FOLLOW_UPS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CommunicationEventIssueFollowUpListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("COMMUNICATION_EVENT_ISSUE_FOLLOW_UP", Order = 0)]
        public List<COMMUNICATION_EVENT_ISSUE_FOLLOW_UP> CommunicationEventIssueFollowUpList { get; set; } = new List<COMMUNICATION_EVENT_ISSUE_FOLLOW_UP>();
    
        [XmlIgnore]
        public bool CommunicationEventIssueFollowUpListSpecified
        {
            get { return this.CommunicationEventIssueFollowUpList != null && this.CommunicationEventIssueFollowUpList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public COMMUNICATION_EVENT_ISSUE_FOLLOW_UPS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
