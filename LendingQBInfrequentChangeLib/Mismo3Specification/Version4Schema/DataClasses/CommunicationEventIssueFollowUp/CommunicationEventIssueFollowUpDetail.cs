namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class COMMUNICATION_EVENT_ISSUE_FOLLOW_UP_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.IssueFollowUpExtendedResolutionDueDateSpecified
                    || this.IssueFollowUpMaterialDescriptionSpecified
                    || this.IssueFollowUpRequestedDateSpecified
                    || this.IssueFollowUpRespondByDateSpecified
                    || this.IssueFollowUpTypeSpecified
                    || this.IssueFollowUpTypeAdditionalDescriptionSpecified
                    || this.IssueFollowUpTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("IssueFollowUpExtendedResolutionDueDate", Order = 0)]
        public MISMODate IssueFollowUpExtendedResolutionDueDate { get; set; }
    
        [XmlIgnore]
        public bool IssueFollowUpExtendedResolutionDueDateSpecified
        {
            get { return this.IssueFollowUpExtendedResolutionDueDate != null; }
            set { }
        }
    
        [XmlElement("IssueFollowUpMaterialDescription", Order = 1)]
        public MISMOString IssueFollowUpMaterialDescription { get; set; }
    
        [XmlIgnore]
        public bool IssueFollowUpMaterialDescriptionSpecified
        {
            get { return this.IssueFollowUpMaterialDescription != null; }
            set { }
        }
    
        [XmlElement("IssueFollowUpRequestedDate", Order = 2)]
        public MISMODate IssueFollowUpRequestedDate { get; set; }
    
        [XmlIgnore]
        public bool IssueFollowUpRequestedDateSpecified
        {
            get { return this.IssueFollowUpRequestedDate != null; }
            set { }
        }
    
        [XmlElement("IssueFollowUpRespondByDate", Order = 3)]
        public MISMODate IssueFollowUpRespondByDate { get; set; }
    
        [XmlIgnore]
        public bool IssueFollowUpRespondByDateSpecified
        {
            get { return this.IssueFollowUpRespondByDate != null; }
            set { }
        }
    
        [XmlElement("IssueFollowUpType", Order = 4)]
        public MISMOEnum<IssueFollowUpBase> IssueFollowUpType { get; set; }
    
        [XmlIgnore]
        public bool IssueFollowUpTypeSpecified
        {
            get { return this.IssueFollowUpType != null; }
            set { }
        }
    
        [XmlElement("IssueFollowUpTypeAdditionalDescription", Order = 5)]
        public MISMOString IssueFollowUpTypeAdditionalDescription { get; set; }
    
        [XmlIgnore]
        public bool IssueFollowUpTypeAdditionalDescriptionSpecified
        {
            get { return this.IssueFollowUpTypeAdditionalDescription != null; }
            set { }
        }
    
        [XmlElement("IssueFollowUpTypeOtherDescription", Order = 6)]
        public MISMOString IssueFollowUpTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool IssueFollowUpTypeOtherDescriptionSpecified
        {
            get { return this.IssueFollowUpTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 7)]
        public COMMUNICATION_EVENT_ISSUE_FOLLOW_UP_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
