namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class COMMUNICATION_EVENT_ISSUE_FOLLOW_UP
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CommunicationEventIssueFollowUpDetailSpecified
                    || this.CommunicationEventIssueFollowUpParticipantsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("COMMUNICATION_EVENT_ISSUE_FOLLOW_UP_DETAIL", Order = 0)]
        public COMMUNICATION_EVENT_ISSUE_FOLLOW_UP_DETAIL CommunicationEventIssueFollowUpDetail { get; set; }
    
        [XmlIgnore]
        public bool CommunicationEventIssueFollowUpDetailSpecified
        {
            get { return this.CommunicationEventIssueFollowUpDetail != null && this.CommunicationEventIssueFollowUpDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("COMMUNICATION_EVENT_ISSUE_FOLLOW_UP_PARTICIPANTS", Order = 1)]
        public COMMUNICATION_EVENT_ISSUE_FOLLOW_UP_PARTICIPANTS CommunicationEventIssueFollowUpParticipants { get; set; }
    
        [XmlIgnore]
        public bool CommunicationEventIssueFollowUpParticipantsSpecified
        {
            get { return this.CommunicationEventIssueFollowUpParticipants != null && this.CommunicationEventIssueFollowUpParticipants.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public COMMUNICATION_EVENT_ISSUE_FOLLOW_UP_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
