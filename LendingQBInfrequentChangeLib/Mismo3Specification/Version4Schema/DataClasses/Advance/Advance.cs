namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ADVANCE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AdvanceRecoveryWaivedAmountSpecified
                    || this.AdvanceTypeSpecified
                    || this.AdvanceTypeOtherDescriptionSpecified
                    || this.CumulativeAdvanceAmountSpecified
                    || this.CurrentAdvanceAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AdvanceRecoveryWaivedAmount", Order = 0)]
        public MISMOAmount AdvanceRecoveryWaivedAmount { get; set; }
    
        [XmlIgnore]
        public bool AdvanceRecoveryWaivedAmountSpecified
        {
            get { return this.AdvanceRecoveryWaivedAmount != null; }
            set { }
        }
    
        [XmlElement("AdvanceType", Order = 1)]
        public MISMOEnum<AdvanceBase> AdvanceType { get; set; }
    
        [XmlIgnore]
        public bool AdvanceTypeSpecified
        {
            get { return this.AdvanceType != null; }
            set { }
        }
    
        [XmlElement("AdvanceTypeOtherDescription", Order = 2)]
        public MISMOString AdvanceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool AdvanceTypeOtherDescriptionSpecified
        {
            get { return this.AdvanceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("CumulativeAdvanceAmount", Order = 3)]
        public MISMOAmount CumulativeAdvanceAmount { get; set; }
    
        [XmlIgnore]
        public bool CumulativeAdvanceAmountSpecified
        {
            get { return this.CumulativeAdvanceAmount != null; }
            set { }
        }
    
        [XmlElement("CurrentAdvanceAmount", Order = 4)]
        public MISMOAmount CurrentAdvanceAmount { get; set; }
    
        [XmlIgnore]
        public bool CurrentAdvanceAmountSpecified
        {
            get { return this.CurrentAdvanceAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public ADVANCE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
