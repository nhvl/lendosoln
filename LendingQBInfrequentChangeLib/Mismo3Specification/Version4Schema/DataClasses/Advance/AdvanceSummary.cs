namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ADVANCE_SUMMARY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PrincipalOrInterestAdvanceEndDateSpecified
                    || this.TotalAdvancesAmountSpecified
                    || this.TotalAdvancesExcludingPrincipalAndInterestAmountSpecified
                    || this.TotalRecoverableAdvancesAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PrincipalOrInterestAdvanceEndDate", Order = 0)]
        public MISMODate PrincipalOrInterestAdvanceEndDate { get; set; }
    
        [XmlIgnore]
        public bool PrincipalOrInterestAdvanceEndDateSpecified
        {
            get { return this.PrincipalOrInterestAdvanceEndDate != null; }
            set { }
        }
    
        [XmlElement("TotalAdvancesAmount", Order = 1)]
        public MISMOAmount TotalAdvancesAmount { get; set; }
    
        [XmlIgnore]
        public bool TotalAdvancesAmountSpecified
        {
            get { return this.TotalAdvancesAmount != null; }
            set { }
        }
    
        [XmlElement("TotalAdvancesExcludingPrincipalAndInterestAmount", Order = 2)]
        public MISMOAmount TotalAdvancesExcludingPrincipalAndInterestAmount { get; set; }
    
        [XmlIgnore]
        public bool TotalAdvancesExcludingPrincipalAndInterestAmountSpecified
        {
            get { return this.TotalAdvancesExcludingPrincipalAndInterestAmount != null; }
            set { }
        }
    
        [XmlElement("TotalRecoverableAdvancesAmount", Order = 3)]
        public MISMOAmount TotalRecoverableAdvancesAmount { get; set; }
    
        [XmlIgnore]
        public bool TotalRecoverableAdvancesAmountSpecified
        {
            get { return this.TotalRecoverableAdvancesAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public ADVANCE_SUMMARY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
