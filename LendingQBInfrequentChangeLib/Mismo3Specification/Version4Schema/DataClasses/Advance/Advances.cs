namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class ADVANCES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AdvanceListSpecified
                    || this.AdvanceSummarySpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ADVANCE", Order = 0)]
        public List<ADVANCE> AdvanceList { get; set; } = new List<ADVANCE>();
    
        [XmlIgnore]
        public bool AdvanceListSpecified
        {
            get { return this.AdvanceList != null && this.AdvanceList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("ADVANCE_SUMMARY", Order = 1)]
        public ADVANCE_SUMMARY AdvanceSummary { get; set; }
    
        [XmlIgnore]
        public bool AdvanceSummarySpecified
        {
            get { return this.AdvanceSummary != null && this.AdvanceSummary.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public ADVANCES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
