namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ENCUMBRANCE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DeedRestrictionsSpecified
                    || this.EncumbranceDetailSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DEED_RESTRICTIONS", Order = 0)]
        public DEED_RESTRICTIONS DeedRestrictions { get; set; }
    
        [XmlIgnore]
        public bool DeedRestrictionsSpecified
        {
            get { return this.DeedRestrictions != null && this.DeedRestrictions.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("ENCUMBRANCE_DETAIL", Order = 1)]
        public ENCUMBRANCE_DETAIL EncumbranceDetail { get; set; }
    
        [XmlIgnore]
        public bool EncumbranceDetailSpecified
        {
            get { return this.EncumbranceDetail != null && this.EncumbranceDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public ENCUMBRANCE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
