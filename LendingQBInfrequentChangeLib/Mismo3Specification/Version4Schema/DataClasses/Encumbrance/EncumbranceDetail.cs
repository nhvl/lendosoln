namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ENCUMBRANCE_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EncumbranceTypeSpecified
                    || this.EncumbranceTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("EncumbranceType", Order = 0)]
        public MISMOEnum<EncumbranceBase> EncumbranceType { get; set; }
    
        [XmlIgnore]
        public bool EncumbranceTypeSpecified
        {
            get { return this.EncumbranceType != null; }
            set { }
        }
    
        [XmlElement("EncumbranceTypeOtherDescription", Order = 1)]
        public MISMOString EncumbranceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool EncumbranceTypeOtherDescriptionSpecified
        {
            get { return this.EncumbranceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public ENCUMBRANCE_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
