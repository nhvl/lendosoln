namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class MANUFACTURED_HOME_SECTIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ManufacturedHomeSectionListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MANUFACTURED_HOME_SECTION", Order = 0)]
        public List<MANUFACTURED_HOME_SECTION> ManufacturedHomeSectionList { get; set; } = new List<MANUFACTURED_HOME_SECTION>();
    
        [XmlIgnore]
        public bool ManufacturedHomeSectionListSpecified
        {
            get { return this.ManufacturedHomeSectionList != null && this.ManufacturedHomeSectionList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public MANUFACTURED_HOME_SECTIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
