namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MANUFACTURED_HOME_SECTION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LengthFeetNumberSpecified
                    || this.ManufacturedHomeHUDCertificationLabelIdentifierSpecified
                    || this.ManufacturedHomeSectionTypeSpecified
                    || this.ManufacturedHomeSectionTypeOtherDescriptionSpecified
                    || this.SquareFeetNumberSpecified
                    || this.WidthFeetNumberSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LengthFeetNumber", Order = 0)]
        public MISMONumeric LengthFeetNumber { get; set; }
    
        [XmlIgnore]
        public bool LengthFeetNumberSpecified
        {
            get { return this.LengthFeetNumber != null; }
            set { }
        }
    
        [XmlElement("ManufacturedHomeHUDCertificationLabelIdentifier", Order = 1)]
        public MISMOIdentifier ManufacturedHomeHUDCertificationLabelIdentifier { get; set; }
    
        [XmlIgnore]
        public bool ManufacturedHomeHUDCertificationLabelIdentifierSpecified
        {
            get { return this.ManufacturedHomeHUDCertificationLabelIdentifier != null; }
            set { }
        }
    
        [XmlElement("ManufacturedHomeSectionType", Order = 2)]
        public MISMOEnum<ManufacturedHomeSectionBase> ManufacturedHomeSectionType { get; set; }
    
        [XmlIgnore]
        public bool ManufacturedHomeSectionTypeSpecified
        {
            get { return this.ManufacturedHomeSectionType != null; }
            set { }
        }
    
        [XmlElement("ManufacturedHomeSectionTypeOtherDescription", Order = 3)]
        public MISMOString ManufacturedHomeSectionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ManufacturedHomeSectionTypeOtherDescriptionSpecified
        {
            get { return this.ManufacturedHomeSectionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("SquareFeetNumber", Order = 4)]
        public MISMONumeric SquareFeetNumber { get; set; }
    
        [XmlIgnore]
        public bool SquareFeetNumberSpecified
        {
            get { return this.SquareFeetNumber != null; }
            set { }
        }
    
        [XmlElement("WidthFeetNumber", Order = 5)]
        public MISMONumeric WidthFeetNumber { get; set; }
    
        [XmlIgnore]
        public bool WidthFeetNumberSpecified
        {
            get { return this.WidthFeetNumber != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 6)]
        public MANUFACTURED_HOME_SECTION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
