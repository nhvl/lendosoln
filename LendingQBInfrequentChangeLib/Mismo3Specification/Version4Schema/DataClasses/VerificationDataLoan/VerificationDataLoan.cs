namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class VERIFICATION_DATA_LOAN
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BankruptcySuspenseBalanceAmountTotalAmountSpecified
                    || this.CurrentBuydownSubsidyAmountTotalAmountSpecified
                    || this.CurrentPrincipalAndInterestPaymentAmountTotalAmountSpecified
                    || this.CurrentTaxAndInsurancePaymentAmountTotalAmountSpecified
                    || this.CurrentTotalOptionalProductsPaymentAmountTotalAmountSpecified
                    || this.CurrentTotalPaymentAmountTotalAmountSpecified
                    || this.DeferredInterestBalanceAmountTotalAmountSpecified
                    || this.EscrowBalanceTotalAmountSpecified
                    || this.HUD235SubsidyPaymentAmountSpecified
                    || this.LoanTotalCountSpecified
                    || this.RecoverableCorporateAdvanceFromBorrowerAmountTotalAmountSpecified
                    || this.RecoverableCorporateAdvanceFromThirdPartyAmountTotalAmountSpecified
                    || this.RemainingBuydownBalanceAmountTotalAmountSpecified
                    || this.RestrictedEscrowBalanceAmountTotalAmountSpecified
                    || this.SuspenseBalanceAmountTotalAmountSpecified
                    || this.UncollectedLateChargeBalanceAmountTotalAmountSpecified
                    || this.UPBTotalAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BankruptcySuspenseBalanceAmountTotalAmount", Order = 0)]
        public MISMOAmount BankruptcySuspenseBalanceAmountTotalAmount { get; set; }
    
        [XmlIgnore]
        public bool BankruptcySuspenseBalanceAmountTotalAmountSpecified
        {
            get { return this.BankruptcySuspenseBalanceAmountTotalAmount != null; }
            set { }
        }
    
        [XmlElement("CurrentBuydownSubsidyAmountTotalAmount", Order = 1)]
        public MISMOAmount CurrentBuydownSubsidyAmountTotalAmount { get; set; }
    
        [XmlIgnore]
        public bool CurrentBuydownSubsidyAmountTotalAmountSpecified
        {
            get { return this.CurrentBuydownSubsidyAmountTotalAmount != null; }
            set { }
        }
    
        [XmlElement("CurrentPrincipalAndInterestPaymentAmountTotalAmount", Order = 2)]
        public MISMOAmount CurrentPrincipalAndInterestPaymentAmountTotalAmount { get; set; }
    
        [XmlIgnore]
        public bool CurrentPrincipalAndInterestPaymentAmountTotalAmountSpecified
        {
            get { return this.CurrentPrincipalAndInterestPaymentAmountTotalAmount != null; }
            set { }
        }
    
        [XmlElement("CurrentTaxAndInsurancePaymentAmountTotalAmount", Order = 3)]
        public MISMOAmount CurrentTaxAndInsurancePaymentAmountTotalAmount { get; set; }
    
        [XmlIgnore]
        public bool CurrentTaxAndInsurancePaymentAmountTotalAmountSpecified
        {
            get { return this.CurrentTaxAndInsurancePaymentAmountTotalAmount != null; }
            set { }
        }
    
        [XmlElement("CurrentTotalOptionalProductsPaymentAmountTotalAmount", Order = 4)]
        public MISMOAmount CurrentTotalOptionalProductsPaymentAmountTotalAmount { get; set; }
    
        [XmlIgnore]
        public bool CurrentTotalOptionalProductsPaymentAmountTotalAmountSpecified
        {
            get { return this.CurrentTotalOptionalProductsPaymentAmountTotalAmount != null; }
            set { }
        }
    
        [XmlElement("CurrentTotalPaymentAmountTotalAmount", Order = 5)]
        public MISMOAmount CurrentTotalPaymentAmountTotalAmount { get; set; }
    
        [XmlIgnore]
        public bool CurrentTotalPaymentAmountTotalAmountSpecified
        {
            get { return this.CurrentTotalPaymentAmountTotalAmount != null; }
            set { }
        }
    
        [XmlElement("DeferredInterestBalanceAmountTotalAmount", Order = 6)]
        public MISMOAmount DeferredInterestBalanceAmountTotalAmount { get; set; }
    
        [XmlIgnore]
        public bool DeferredInterestBalanceAmountTotalAmountSpecified
        {
            get { return this.DeferredInterestBalanceAmountTotalAmount != null; }
            set { }
        }
    
        [XmlElement("EscrowBalanceTotalAmount", Order = 7)]
        public MISMOAmount EscrowBalanceTotalAmount { get; set; }
    
        [XmlIgnore]
        public bool EscrowBalanceTotalAmountSpecified
        {
            get { return this.EscrowBalanceTotalAmount != null; }
            set { }
        }
    
        [XmlElement("HUD235SubsidyPaymentAmount", Order = 8)]
        public MISMOAmount HUD235SubsidyPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool HUD235SubsidyPaymentAmountSpecified
        {
            get { return this.HUD235SubsidyPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("LoanTotalCount", Order = 9)]
        public MISMOCount LoanTotalCount { get; set; }
    
        [XmlIgnore]
        public bool LoanTotalCountSpecified
        {
            get { return this.LoanTotalCount != null; }
            set { }
        }
    
        [XmlElement("RecoverableCorporateAdvanceFromBorrowerAmountTotalAmount", Order = 10)]
        public MISMOAmount RecoverableCorporateAdvanceFromBorrowerAmountTotalAmount { get; set; }
    
        [XmlIgnore]
        public bool RecoverableCorporateAdvanceFromBorrowerAmountTotalAmountSpecified
        {
            get { return this.RecoverableCorporateAdvanceFromBorrowerAmountTotalAmount != null; }
            set { }
        }
    
        [XmlElement("RecoverableCorporateAdvanceFromThirdPartyAmountTotalAmount", Order = 11)]
        public MISMOAmount RecoverableCorporateAdvanceFromThirdPartyAmountTotalAmount { get; set; }
    
        [XmlIgnore]
        public bool RecoverableCorporateAdvanceFromThirdPartyAmountTotalAmountSpecified
        {
            get { return this.RecoverableCorporateAdvanceFromThirdPartyAmountTotalAmount != null; }
            set { }
        }
    
        [XmlElement("RemainingBuydownBalanceAmountTotalAmount", Order = 12)]
        public MISMOAmount RemainingBuydownBalanceAmountTotalAmount { get; set; }
    
        [XmlIgnore]
        public bool RemainingBuydownBalanceAmountTotalAmountSpecified
        {
            get { return this.RemainingBuydownBalanceAmountTotalAmount != null; }
            set { }
        }
    
        [XmlElement("RestrictedEscrowBalanceAmountTotalAmount", Order = 13)]
        public MISMOAmount RestrictedEscrowBalanceAmountTotalAmount { get; set; }
    
        [XmlIgnore]
        public bool RestrictedEscrowBalanceAmountTotalAmountSpecified
        {
            get { return this.RestrictedEscrowBalanceAmountTotalAmount != null; }
            set { }
        }
    
        [XmlElement("SuspenseBalanceAmountTotalAmount", Order = 14)]
        public MISMOAmount SuspenseBalanceAmountTotalAmount { get; set; }
    
        [XmlIgnore]
        public bool SuspenseBalanceAmountTotalAmountSpecified
        {
            get { return this.SuspenseBalanceAmountTotalAmount != null; }
            set { }
        }
    
        [XmlElement("UncollectedLateChargeBalanceAmountTotalAmount", Order = 15)]
        public MISMOAmount UncollectedLateChargeBalanceAmountTotalAmount { get; set; }
    
        [XmlIgnore]
        public bool UncollectedLateChargeBalanceAmountTotalAmountSpecified
        {
            get { return this.UncollectedLateChargeBalanceAmountTotalAmount != null; }
            set { }
        }
    
        [XmlElement("UPBTotalAmount", Order = 16)]
        public MISMOAmount UPBTotalAmount { get; set; }
    
        [XmlIgnore]
        public bool UPBTotalAmountSpecified
        {
            get { return this.UPBTotalAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 17)]
        public VERIFICATION_DATA_LOAN_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
