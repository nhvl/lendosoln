namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class TITLE_COMMITMENT_PROPOSED_POLICIES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.TitleCommitmentProposedPolicyListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("TITLE_COMMITMENT_PROPOSED_POLICY", Order = 0)]
        public List<TITLE_COMMITMENT_PROPOSED_POLICY> TitleCommitmentProposedPolicyList { get; set; } = new List<TITLE_COMMITMENT_PROPOSED_POLICY>();
    
        [XmlIgnore]
        public bool TitleCommitmentProposedPolicyListSpecified
        {
            get { return this.TitleCommitmentProposedPolicyList != null && this.TitleCommitmentProposedPolicyList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public TITLE_COMMITMENT_PROPOSED_POLICIES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
