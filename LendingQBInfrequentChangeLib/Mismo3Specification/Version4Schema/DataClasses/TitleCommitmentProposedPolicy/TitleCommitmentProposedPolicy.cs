namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class TITLE_COMMITMENT_PROPOSED_POLICY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.NamedInsuredDescriptionSpecified
                    || this.NamedInsuredTypeSpecified
                    || this.TitleInsuranceAmountSpecified
                    || this.TitlePolicyFormDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("NamedInsuredDescription", Order = 0)]
        public MISMOString NamedInsuredDescription { get; set; }
    
        [XmlIgnore]
        public bool NamedInsuredDescriptionSpecified
        {
            get { return this.NamedInsuredDescription != null; }
            set { }
        }
    
        [XmlElement("NamedInsuredType", Order = 1)]
        public MISMOEnum<NamedInsuredBase> NamedInsuredType { get; set; }
    
        [XmlIgnore]
        public bool NamedInsuredTypeSpecified
        {
            get { return this.NamedInsuredType != null; }
            set { }
        }
    
        [XmlElement("TitleInsuranceAmount", Order = 2)]
        public MISMOAmount TitleInsuranceAmount { get; set; }
    
        [XmlIgnore]
        public bool TitleInsuranceAmountSpecified
        {
            get { return this.TitleInsuranceAmount != null; }
            set { }
        }
    
        [XmlElement("TitlePolicyFormDescription", Order = 3)]
        public MISMOString TitlePolicyFormDescription { get; set; }
    
        [XmlIgnore]
        public bool TitlePolicyFormDescriptionSpecified
        {
            get { return this.TitlePolicyFormDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public TITLE_COMMITMENT_PROPOSED_POLICY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
