namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class DOCUMENT_CLASSIFICATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DocumentClassesSpecified
                    || this.DocumentClassificationDetailSpecified
                    || this.DocumentFormContributorsSpecified
                    || this.DocumentUsagesSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DOCUMENT_CLASSES", Order = 0)]
        public DOCUMENT_CLASSES DocumentClasses { get; set; }
    
        [XmlIgnore]
        public bool DocumentClassesSpecified
        {
            get { return this.DocumentClasses != null && this.DocumentClasses.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("DOCUMENT_CLASSIFICATION_DETAIL", Order = 1)]
        public DOCUMENT_CLASSIFICATION_DETAIL DocumentClassificationDetail { get; set; }
    
        [XmlIgnore]
        public bool DocumentClassificationDetailSpecified
        {
            get { return this.DocumentClassificationDetail != null && this.DocumentClassificationDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("DOCUMENT_FORM_CONTRIBUTORS", Order = 2)]
        public DOCUMENT_FORM_CONTRIBUTORS DocumentFormContributors { get; set; }
    
        [XmlIgnore]
        public bool DocumentFormContributorsSpecified
        {
            get { return this.DocumentFormContributors != null && this.DocumentFormContributors.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("DOCUMENT_USAGES", Order = 3)]
        public DOCUMENT_USAGES DocumentUsages { get; set; }
    
        [XmlIgnore]
        public bool DocumentUsagesSpecified
        {
            get { return this.DocumentUsages != null && this.DocumentUsages.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public DOCUMENT_CLASSIFICATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
