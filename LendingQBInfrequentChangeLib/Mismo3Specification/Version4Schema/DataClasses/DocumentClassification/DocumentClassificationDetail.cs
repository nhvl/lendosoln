namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class DOCUMENT_CLASSIFICATION_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AcceptableSigningMethodTypeSpecified
                    || this.DocumentFormIssuingEntityNameTypeSpecified
                    || this.DocumentFormIssuingEntityNameTypeOtherDescriptionSpecified
                    || this.DocumentFormIssuingEntityNumberIdentifierSpecified
                    || this.DocumentFormIssuingEntityVersionIdentifierSpecified
                    || this.DocumentFormPublisherEntityNameSpecified
                    || this.DocumentFormPublisherNumberIdentifierSpecified
                    || this.DocumentFormPublisherVersionIdentifierSpecified
                    || this.DocumentNameSpecified
                    || this.DocumentNegotiableInstrumentIndicatorSpecified
                    || this.DocumentPeriodEndDateSpecified
                    || this.DocumentPeriodStartDateSpecified
                    || this.DocumentRecordationProcessingTypeSpecified
                    || this.DocumentSignatureRequiredIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AcceptableSigningMethodType", Order = 0)]
        public MISMOEnum<AcceptableSigningMethodBase> AcceptableSigningMethodType { get; set; }
    
        [XmlIgnore]
        public bool AcceptableSigningMethodTypeSpecified
        {
            get { return this.AcceptableSigningMethodType != null; }
            set { }
        }
    
        [XmlElement("DocumentFormIssuingEntityNameType", Order = 1)]
        public MISMOEnum<DocumentFormIssuingEntityNameBase> DocumentFormIssuingEntityNameType { get; set; }
    
        [XmlIgnore]
        public bool DocumentFormIssuingEntityNameTypeSpecified
        {
            get { return this.DocumentFormIssuingEntityNameType != null; }
            set { }
        }
    
        [XmlElement("DocumentFormIssuingEntityNameTypeOtherDescription", Order = 2)]
        public MISMOString DocumentFormIssuingEntityNameTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool DocumentFormIssuingEntityNameTypeOtherDescriptionSpecified
        {
            get { return this.DocumentFormIssuingEntityNameTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("DocumentFormIssuingEntityNumberIdentifier", Order = 3)]
        public MISMOIdentifier DocumentFormIssuingEntityNumberIdentifier { get; set; }
    
        [XmlIgnore]
        public bool DocumentFormIssuingEntityNumberIdentifierSpecified
        {
            get { return this.DocumentFormIssuingEntityNumberIdentifier != null; }
            set { }
        }
    
        [XmlElement("DocumentFormIssuingEntityVersionIdentifier", Order = 4)]
        public MISMOIdentifier DocumentFormIssuingEntityVersionIdentifier { get; set; }
    
        [XmlIgnore]
        public bool DocumentFormIssuingEntityVersionIdentifierSpecified
        {
            get { return this.DocumentFormIssuingEntityVersionIdentifier != null; }
            set { }
        }
    
        [XmlElement("DocumentFormPublisherEntityName", Order = 5)]
        public MISMOString DocumentFormPublisherEntityName { get; set; }
    
        [XmlIgnore]
        public bool DocumentFormPublisherEntityNameSpecified
        {
            get { return this.DocumentFormPublisherEntityName != null; }
            set { }
        }
    
        [XmlElement("DocumentFormPublisherNumberIdentifier", Order = 6)]
        public MISMOIdentifier DocumentFormPublisherNumberIdentifier { get; set; }
    
        [XmlIgnore]
        public bool DocumentFormPublisherNumberIdentifierSpecified
        {
            get { return this.DocumentFormPublisherNumberIdentifier != null; }
            set { }
        }
    
        [XmlElement("DocumentFormPublisherVersionIdentifier", Order = 7)]
        public MISMOIdentifier DocumentFormPublisherVersionIdentifier { get; set; }
    
        [XmlIgnore]
        public bool DocumentFormPublisherVersionIdentifierSpecified
        {
            get { return this.DocumentFormPublisherVersionIdentifier != null; }
            set { }
        }
    
        [XmlElement("DocumentName", Order = 8)]
        public MISMOString DocumentName { get; set; }
    
        [XmlIgnore]
        public bool DocumentNameSpecified
        {
            get { return this.DocumentName != null; }
            set { }
        }
    
        [XmlElement("DocumentNegotiableInstrumentIndicator", Order = 9)]
        public MISMOIndicator DocumentNegotiableInstrumentIndicator { get; set; }
    
        [XmlIgnore]
        public bool DocumentNegotiableInstrumentIndicatorSpecified
        {
            get { return this.DocumentNegotiableInstrumentIndicator != null; }
            set { }
        }
    
        [XmlElement("DocumentPeriodEndDate", Order = 10)]
        public MISMODate DocumentPeriodEndDate { get; set; }
    
        [XmlIgnore]
        public bool DocumentPeriodEndDateSpecified
        {
            get { return this.DocumentPeriodEndDate != null; }
            set { }
        }
    
        [XmlElement("DocumentPeriodStartDate", Order = 11)]
        public MISMODate DocumentPeriodStartDate { get; set; }
    
        [XmlIgnore]
        public bool DocumentPeriodStartDateSpecified
        {
            get { return this.DocumentPeriodStartDate != null; }
            set { }
        }
    
        [XmlElement("DocumentRecordationProcessingType", Order = 12)]
        public MISMOEnum<DocumentRecordationProcessingBase> DocumentRecordationProcessingType { get; set; }
    
        [XmlIgnore]
        public bool DocumentRecordationProcessingTypeSpecified
        {
            get { return this.DocumentRecordationProcessingType != null; }
            set { }
        }
    
        [XmlElement("DocumentSignatureRequiredIndicator", Order = 13)]
        public MISMOIndicator DocumentSignatureRequiredIndicator { get; set; }
    
        [XmlIgnore]
        public bool DocumentSignatureRequiredIndicatorSpecified
        {
            get { return this.DocumentSignatureRequiredIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 14)]
        public DOCUMENT_CLASSIFICATION_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
