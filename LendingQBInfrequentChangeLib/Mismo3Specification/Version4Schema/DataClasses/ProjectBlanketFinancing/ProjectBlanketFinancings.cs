namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class PROJECT_BLANKET_FINANCINGS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ProjectBlanketFinancingListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PROJECT_BLANKET_FINANCING", Order = 0)]
        public List<PROJECT_BLANKET_FINANCING> ProjectBlanketFinancingList { get; set; } = new List<PROJECT_BLANKET_FINANCING>();
    
        [XmlIgnore]
        public bool ProjectBlanketFinancingListSpecified
        {
            get { return this.ProjectBlanketFinancingList != null && this.ProjectBlanketFinancingList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public PROJECT_BLANKET_FINANCINGS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
