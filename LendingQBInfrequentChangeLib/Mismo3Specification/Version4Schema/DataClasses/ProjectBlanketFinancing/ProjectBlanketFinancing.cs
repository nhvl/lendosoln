namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PROJECT_BLANKET_FINANCING
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AmortizationTypeSpecified
                    || this.AmortizationTypeOtherDescriptionSpecified
                    || this.BalloonIndicatorSpecified
                    || this.CurrentInterestRatePercentSpecified
                    || this.LienDescriptionSpecified
                    || this.LienPriorityTypeSpecified
                    || this.LienPriorityTypeOtherDescriptionSpecified
                    || this.LoanRemainingMaturityTermMonthsCountSpecified
                    || this.ProjectLienHolderNameSpecified
                    || this.TotalPaymentAmountSpecified
                    || this.UPBAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AmortizationType", Order = 0)]
        public MISMOEnum<AmortizationBase> AmortizationType { get; set; }
    
        [XmlIgnore]
        public bool AmortizationTypeSpecified
        {
            get { return this.AmortizationType != null; }
            set { }
        }
    
        [XmlElement("AmortizationTypeOtherDescription", Order = 1)]
        public MISMOString AmortizationTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool AmortizationTypeOtherDescriptionSpecified
        {
            get { return this.AmortizationTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("BalloonIndicator", Order = 2)]
        public MISMOIndicator BalloonIndicator { get; set; }
    
        [XmlIgnore]
        public bool BalloonIndicatorSpecified
        {
            get { return this.BalloonIndicator != null; }
            set { }
        }
    
        [XmlElement("CurrentInterestRatePercent", Order = 3)]
        public MISMOPercent CurrentInterestRatePercent { get; set; }
    
        [XmlIgnore]
        public bool CurrentInterestRatePercentSpecified
        {
            get { return this.CurrentInterestRatePercent != null; }
            set { }
        }
    
        [XmlElement("LienDescription", Order = 4)]
        public MISMOString LienDescription { get; set; }
    
        [XmlIgnore]
        public bool LienDescriptionSpecified
        {
            get { return this.LienDescription != null; }
            set { }
        }
    
        [XmlElement("LienPriorityType", Order = 5)]
        public MISMOEnum<LienPriorityBase> LienPriorityType { get; set; }
    
        [XmlIgnore]
        public bool LienPriorityTypeSpecified
        {
            get { return this.LienPriorityType != null; }
            set { }
        }
    
        [XmlElement("LienPriorityTypeOtherDescription", Order = 6)]
        public MISMOString LienPriorityTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool LienPriorityTypeOtherDescriptionSpecified
        {
            get { return this.LienPriorityTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("LoanRemainingMaturityTermMonthsCount", Order = 7)]
        public MISMOCount LoanRemainingMaturityTermMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool LoanRemainingMaturityTermMonthsCountSpecified
        {
            get { return this.LoanRemainingMaturityTermMonthsCount != null; }
            set { }
        }
    
        [XmlElement("ProjectLienHolderName", Order = 8)]
        public MISMOString ProjectLienHolderName { get; set; }
    
        [XmlIgnore]
        public bool ProjectLienHolderNameSpecified
        {
            get { return this.ProjectLienHolderName != null; }
            set { }
        }
    
        [XmlElement("TotalPaymentAmount", Order = 9)]
        public MISMOAmount TotalPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool TotalPaymentAmountSpecified
        {
            get { return this.TotalPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("UPBAmount", Order = 10)]
        public MISMOAmount UPBAmount { get; set; }
    
        [XmlIgnore]
        public bool UPBAmountSpecified
        {
            get { return this.UPBAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 11)]
        public PROJECT_BLANKET_FINANCING_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
