namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PLAINTIFF
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PrimaryPlaintiffIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PrimaryPlaintiffIndicator", Order = 0)]
        public MISMOIndicator PrimaryPlaintiffIndicator { get; set; }
    
        [XmlIgnore]
        public bool PrimaryPlaintiffIndicatorSpecified
        {
            get { return this.PrimaryPlaintiffIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public PLAINTIFF_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
