namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class TIL_DISCLOSURE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.TilDisclosureDetailSpecified
                    || this.TilPaymentSummarySpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("TIL_DISCLOSURE_DETAIL", Order = 0)]
        public TIL_DISCLOSURE_DETAIL TilDisclosureDetail { get; set; }
    
        [XmlIgnore]
        public bool TilDisclosureDetailSpecified
        {
            get { return this.TilDisclosureDetail != null && this.TilDisclosureDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("TIL_PAYMENT_SUMMARY", Order = 1)]
        public TIL_PAYMENT_SUMMARY TilPaymentSummary { get; set; }
    
        [XmlIgnore]
        public bool TilPaymentSummarySpecified
        {
            get { return this.TilPaymentSummary != null && this.TilPaymentSummary.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public TIL_DISCLOSURE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
