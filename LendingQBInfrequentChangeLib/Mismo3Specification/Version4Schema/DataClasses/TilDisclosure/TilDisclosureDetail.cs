namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class TIL_DISCLOSURE_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DatesAndNumericalDisclosuresAreEstimatesIndicatorSpecified
                    || this.TILDisclosureDateSpecified
                    || this.TILPrimaryBoxesLoanCalculationsAreEstimatesIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DatesAndNumericalDisclosuresAreEstimatesIndicator", Order = 0)]
        public MISMOIndicator DatesAndNumericalDisclosuresAreEstimatesIndicator { get; set; }
    
        [XmlIgnore]
        public bool DatesAndNumericalDisclosuresAreEstimatesIndicatorSpecified
        {
            get { return this.DatesAndNumericalDisclosuresAreEstimatesIndicator != null; }
            set { }
        }
    
        [XmlElement("TILDisclosureDate", Order = 1)]
        public MISMODate TILDisclosureDate { get; set; }
    
        [XmlIgnore]
        public bool TILDisclosureDateSpecified
        {
            get { return this.TILDisclosureDate != null; }
            set { }
        }
    
        [XmlElement("TILPrimaryBoxesLoanCalculationsAreEstimatesIndicator", Order = 2)]
        public MISMOIndicator TILPrimaryBoxesLoanCalculationsAreEstimatesIndicator { get; set; }
    
        [XmlIgnore]
        public bool TILPrimaryBoxesLoanCalculationsAreEstimatesIndicatorSpecified
        {
            get { return this.TILPrimaryBoxesLoanCalculationsAreEstimatesIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public TIL_DISCLOSURE_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
