namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class REMITTANCE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AgentPortionAmountSpecified
                    || this.BasisOfInsuranceDiscountAmountSpecified
                    || this.GrossPremiumAmountSpecified
                    || this.InsuredNameSpecified
                    || this.NAICTitlePolicyClassificationTypeSpecified
                    || this.NamedInsuredTypeSpecified
                    || this.OtherBasisOfInsuranceDiscountAmountSpecified
                    || this.PolicyFormDescriptionSpecified
                    || this.PriorPolicyEffectiveDateSpecified
                    || this.PriorPolicyIdentifierSpecified
                    || this.RemittancePolicyDescriptionSpecified
                    || this.RemittancePolicyEffectiveDateSpecified
                    || this.RemittancePolicyPrefixCodeSpecified
                    || this.RemittancePolicySuffixIdentifierSpecified
                    || this.RemittanceRecordTransactionCodeSpecified
                    || this.RemittanceRecordTransactionCodeDescriptionSpecified
                    || this.RemittanceRecordTypeSpecified
                    || this.RemittanceRecordTypeOtherDescriptionSpecified
                    || this.RemittanceTitleInsuranceAmountSpecified
                    || this.UnderwriterPortionAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AgentPortionAmount", Order = 0)]
        public MISMOAmount AgentPortionAmount { get; set; }
    
        [XmlIgnore]
        public bool AgentPortionAmountSpecified
        {
            get { return this.AgentPortionAmount != null; }
            set { }
        }
    
        [XmlElement("BasisOfInsuranceDiscountAmount", Order = 1)]
        public MISMOAmount BasisOfInsuranceDiscountAmount { get; set; }
    
        [XmlIgnore]
        public bool BasisOfInsuranceDiscountAmountSpecified
        {
            get { return this.BasisOfInsuranceDiscountAmount != null; }
            set { }
        }
    
        [XmlElement("GrossPremiumAmount", Order = 2)]
        public MISMOAmount GrossPremiumAmount { get; set; }
    
        [XmlIgnore]
        public bool GrossPremiumAmountSpecified
        {
            get { return this.GrossPremiumAmount != null; }
            set { }
        }
    
        [XmlElement("InsuredName", Order = 3)]
        public MISMOString InsuredName { get; set; }
    
        [XmlIgnore]
        public bool InsuredNameSpecified
        {
            get { return this.InsuredName != null; }
            set { }
        }
    
        [XmlElement("NAICTitlePolicyClassificationType", Order = 4)]
        public MISMOEnum<NAICTitlePolicyClassificationBase> NAICTitlePolicyClassificationType { get; set; }
    
        [XmlIgnore]
        public bool NAICTitlePolicyClassificationTypeSpecified
        {
            get { return this.NAICTitlePolicyClassificationType != null; }
            set { }
        }
    
        [XmlElement("NamedInsuredType", Order = 5)]
        public MISMOEnum<NamedInsuredBase> NamedInsuredType { get; set; }
    
        [XmlIgnore]
        public bool NamedInsuredTypeSpecified
        {
            get { return this.NamedInsuredType != null; }
            set { }
        }
    
        [XmlElement("OtherBasisOfInsuranceDiscountAmount", Order = 6)]
        public MISMOAmount OtherBasisOfInsuranceDiscountAmount { get; set; }
    
        [XmlIgnore]
        public bool OtherBasisOfInsuranceDiscountAmountSpecified
        {
            get { return this.OtherBasisOfInsuranceDiscountAmount != null; }
            set { }
        }
    
        [XmlElement("PolicyFormDescription", Order = 7)]
        public MISMOString PolicyFormDescription { get; set; }
    
        [XmlIgnore]
        public bool PolicyFormDescriptionSpecified
        {
            get { return this.PolicyFormDescription != null; }
            set { }
        }
    
        [XmlElement("PriorPolicyEffectiveDate", Order = 8)]
        public MISMODate PriorPolicyEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool PriorPolicyEffectiveDateSpecified
        {
            get { return this.PriorPolicyEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("PriorPolicyIdentifier", Order = 9)]
        public MISMOIdentifier PriorPolicyIdentifier { get; set; }
    
        [XmlIgnore]
        public bool PriorPolicyIdentifierSpecified
        {
            get { return this.PriorPolicyIdentifier != null; }
            set { }
        }
    
        [XmlElement("RemittancePolicyDescription", Order = 10)]
        public MISMOString RemittancePolicyDescription { get; set; }
    
        [XmlIgnore]
        public bool RemittancePolicyDescriptionSpecified
        {
            get { return this.RemittancePolicyDescription != null; }
            set { }
        }
    
        [XmlElement("RemittancePolicyEffectiveDate", Order = 11)]
        public MISMODate RemittancePolicyEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool RemittancePolicyEffectiveDateSpecified
        {
            get { return this.RemittancePolicyEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("RemittancePolicyPrefixCode", Order = 12)]
        public MISMOCode RemittancePolicyPrefixCode { get; set; }
    
        [XmlIgnore]
        public bool RemittancePolicyPrefixCodeSpecified
        {
            get { return this.RemittancePolicyPrefixCode != null; }
            set { }
        }
    
        [XmlElement("RemittancePolicySuffixIdentifier", Order = 13)]
        public MISMOIdentifier RemittancePolicySuffixIdentifier { get; set; }
    
        [XmlIgnore]
        public bool RemittancePolicySuffixIdentifierSpecified
        {
            get { return this.RemittancePolicySuffixIdentifier != null; }
            set { }
        }
    
        [XmlElement("RemittanceRecordTransactionCode", Order = 14)]
        public MISMOCode RemittanceRecordTransactionCode { get; set; }
    
        [XmlIgnore]
        public bool RemittanceRecordTransactionCodeSpecified
        {
            get { return this.RemittanceRecordTransactionCode != null; }
            set { }
        }
    
        [XmlElement("RemittanceRecordTransactionCodeDescription", Order = 15)]
        public MISMOString RemittanceRecordTransactionCodeDescription { get; set; }
    
        [XmlIgnore]
        public bool RemittanceRecordTransactionCodeDescriptionSpecified
        {
            get { return this.RemittanceRecordTransactionCodeDescription != null; }
            set { }
        }
    
        [XmlElement("RemittanceRecordType", Order = 16)]
        public MISMOEnum<RemittanceRecordBase> RemittanceRecordType { get; set; }
    
        [XmlIgnore]
        public bool RemittanceRecordTypeSpecified
        {
            get { return this.RemittanceRecordType != null; }
            set { }
        }
    
        [XmlElement("RemittanceRecordTypeOtherDescription", Order = 17)]
        public MISMOString RemittanceRecordTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool RemittanceRecordTypeOtherDescriptionSpecified
        {
            get { return this.RemittanceRecordTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("RemittanceTitleInsuranceAmount", Order = 18)]
        public MISMOAmount RemittanceTitleInsuranceAmount { get; set; }
    
        [XmlIgnore]
        public bool RemittanceTitleInsuranceAmountSpecified
        {
            get { return this.RemittanceTitleInsuranceAmount != null; }
            set { }
        }
    
        [XmlElement("UnderwriterPortionAmount", Order = 19)]
        public MISMOAmount UnderwriterPortionAmount { get; set; }
    
        [XmlIgnore]
        public bool UnderwriterPortionAmountSpecified
        {
            get { return this.UnderwriterPortionAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 20)]
        public REMITTANCE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
