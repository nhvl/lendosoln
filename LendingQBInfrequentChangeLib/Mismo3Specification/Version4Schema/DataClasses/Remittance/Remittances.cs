namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class REMITTANCES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.RemittanceListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("REMITTANCE", Order = 0)]
        public List<REMITTANCE> RemittanceList { get; set; } = new List<REMITTANCE>();
    
        [XmlIgnore]
        public bool RemittanceListSpecified
        {
            get { return this.RemittanceList != null && this.RemittanceList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public REMITTANCES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
