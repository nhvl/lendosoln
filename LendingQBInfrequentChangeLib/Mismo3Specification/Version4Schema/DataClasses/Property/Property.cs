namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PROPERTY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AddressSpecified
                    || this.BuildingPermitsSpecified
                    || this.CertificatesSpecified
                    || this.ComparableSpecified
                    || this.DataSourcesSpecified
                    || this.DisastersSpecified
                    || this.EncumbrancesSpecified
                    || this.EnvironmentalConditionsSpecified
                    || this.FloodDeterminationSpecified
                    || this.HazardInsurancesSpecified
                    || this.HomeownersAssociationsSpecified
                    || this.ImprovementSpecified
                    || this.InspectionsSpecified
                    || this.LegalDescriptionsSpecified
                    || this.LicensesSpecified
                    || this.ListingInformationsSpecified
                    || this.LocationIdentifierSpecified
                    || this.ManufacturedHomeSpecified
                    || this.MarketSpecified
                    || this.NeighborhoodSpecified
                    || this.ProjectSpecified
                    || this.PropertyDetailSpecified
                    || this.PropertyTaxesSpecified
                    || this.PropertyTitleSpecified
                    || this.PropertyUnitsSpecified
                    || this.PropertyValuationsSpecified
                    || this.RepairSpecified
                    || this.SalesContractsSpecified
                    || this.SalesHistoriesSpecified
                    || this.ServitudesSpecified
                    || this.SiteSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ADDRESS", Order = 0)]
        public ADDRESS Address { get; set; }
    
        [XmlIgnore]
        public bool AddressSpecified
        {
            get { return this.Address != null && this.Address.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("BUILDING_PERMITS", Order = 1)]
        public BUILDING_PERMITS BuildingPermits { get; set; }
    
        [XmlIgnore]
        public bool BuildingPermitsSpecified
        {
            get { return this.BuildingPermits != null && this.BuildingPermits.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CERTIFICATES", Order = 2)]
        public CERTIFICATES Certificates { get; set; }
    
        [XmlIgnore]
        public bool CertificatesSpecified
        {
            get { return this.Certificates != null && this.Certificates.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("COMPARABLE", Order = 3)]
        public COMPARABLE Comparable { get; set; }
    
        [XmlIgnore]
        public bool ComparableSpecified
        {
            get { return this.Comparable != null && this.Comparable.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("DATA_SOURCES", Order = 4)]
        public DATA_SOURCES DataSources { get; set; }
    
        [XmlIgnore]
        public bool DataSourcesSpecified
        {
            get { return this.DataSources != null && this.DataSources.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("DISASTERS", Order = 5)]
        public DISASTERS Disasters { get; set; }
    
        [XmlIgnore]
        public bool DisastersSpecified
        {
            get { return this.Disasters != null && this.Disasters.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("ENCUMBRANCES", Order = 6)]
        public ENCUMBRANCES Encumbrances { get; set; }
    
        [XmlIgnore]
        public bool EncumbrancesSpecified
        {
            get { return this.Encumbrances != null && this.Encumbrances.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("ENVIRONMENTAL_CONDITIONS", Order = 7)]
        public ENVIRONMENTAL_CONDITIONS EnvironmentalConditions { get; set; }
    
        [XmlIgnore]
        public bool EnvironmentalConditionsSpecified
        {
            get { return this.EnvironmentalConditions != null && this.EnvironmentalConditions.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("FLOOD_DETERMINATION", Order = 8)]
        public FLOOD_DETERMINATION FloodDetermination { get; set; }
    
        [XmlIgnore]
        public bool FloodDeterminationSpecified
        {
            get { return this.FloodDetermination != null && this.FloodDetermination.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("HAZARD_INSURANCES", Order = 9)]
        public HAZARD_INSURANCES HazardInsurances { get; set; }
    
        [XmlIgnore]
        public bool HazardInsurancesSpecified
        {
            get { return this.HazardInsurances != null && this.HazardInsurances.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("HOMEOWNERS_ASSOCIATIONS", Order = 10)]
        public HOMEOWNERS_ASSOCIATIONS HomeownersAssociations { get; set; }
    
        [XmlIgnore]
        public bool HomeownersAssociationsSpecified
        {
            get { return this.HomeownersAssociations != null && this.HomeownersAssociations.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("IMPROVEMENT", Order = 11)]
        public IMPROVEMENT Improvement { get; set; }
    
        [XmlIgnore]
        public bool ImprovementSpecified
        {
            get { return this.Improvement != null && this.Improvement.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("INSPECTIONS", Order = 12)]
        public INSPECTIONS Inspections { get; set; }
    
        [XmlIgnore]
        public bool InspectionsSpecified
        {
            get { return this.Inspections != null && this.Inspections.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("LEGAL_DESCRIPTIONS", Order = 13)]
        public LEGAL_DESCRIPTIONS LegalDescriptions { get; set; }
    
        [XmlIgnore]
        public bool LegalDescriptionsSpecified
        {
            get { return this.LegalDescriptions != null && this.LegalDescriptions.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("LICENSES", Order = 14)]
        public LICENSES Licenses { get; set; }
    
        [XmlIgnore]
        public bool LicensesSpecified
        {
            get { return this.Licenses != null && this.Licenses.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("LISTING_INFORMATIONS", Order = 15)]
        public LISTING_INFORMATIONS ListingInformations { get; set; }
    
        [XmlIgnore]
        public bool ListingInformationsSpecified
        {
            get { return this.ListingInformations != null && this.ListingInformations.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("LOCATION_IDENTIFIER", Order = 16)]
        public LOCATION_IDENTIFIER LocationIdentifier { get; set; }
    
        [XmlIgnore]
        public bool LocationIdentifierSpecified
        {
            get { return this.LocationIdentifier != null && this.LocationIdentifier.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("MANUFACTURED_HOME", Order = 17)]
        public MANUFACTURED_HOME ManufacturedHome { get; set; }
    
        [XmlIgnore]
        public bool ManufacturedHomeSpecified
        {
            get { return this.ManufacturedHome != null && this.ManufacturedHome.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("MARKET", Order = 18)]
        public MARKET Market { get; set; }
    
        [XmlIgnore]
        public bool MarketSpecified
        {
            get { return this.Market != null && this.Market.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("NEIGHBORHOOD", Order = 19)]
        public NEIGHBORHOOD Neighborhood { get; set; }
    
        [XmlIgnore]
        public bool NeighborhoodSpecified
        {
            get { return this.Neighborhood != null && this.Neighborhood.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PROJECT", Order = 20)]
        public PROJECT Project { get; set; }
    
        [XmlIgnore]
        public bool ProjectSpecified
        {
            get { return this.Project != null && this.Project.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PROPERTY_DETAIL", Order = 21)]
        public PROPERTY_DETAIL PropertyDetail { get; set; }
    
        [XmlIgnore]
        public bool PropertyDetailSpecified
        {
            get { return this.PropertyDetail != null && this.PropertyDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PROPERTY_TAXES", Order = 22)]
        public PROPERTY_TAXES PropertyTaxes { get; set; }
    
        [XmlIgnore]
        public bool PropertyTaxesSpecified
        {
            get { return this.PropertyTaxes != null && this.PropertyTaxes.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PROPERTY_TITLE", Order = 23)]
        public PROPERTY_TITLE PropertyTitle { get; set; }
    
        [XmlIgnore]
        public bool PropertyTitleSpecified
        {
            get { return this.PropertyTitle != null && this.PropertyTitle.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PROPERTY_UNITS", Order = 24)]
        public PROPERTY_UNITS PropertyUnits { get; set; }
    
        [XmlIgnore]
        public bool PropertyUnitsSpecified
        {
            get { return this.PropertyUnits != null && this.PropertyUnits.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PROPERTY_VALUATIONS", Order = 25)]
        public PROPERTY_VALUATIONS PropertyValuations { get; set; }
    
        [XmlIgnore]
        public bool PropertyValuationsSpecified
        {
            get { return this.PropertyValuations != null && this.PropertyValuations.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("REPAIR", Order = 26)]
        public REPAIR Repair { get; set; }
    
        [XmlIgnore]
        public bool RepairSpecified
        {
            get { return this.Repair != null && this.Repair.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SALES_CONTRACTS", Order = 27)]
        public SALES_CONTRACTS SalesContracts { get; set; }
    
        [XmlIgnore]
        public bool SalesContractsSpecified
        {
            get { return this.SalesContracts != null && this.SalesContracts.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SALES_HISTORIES", Order = 28)]
        public SALES_HISTORIES SalesHistories { get; set; }
    
        [XmlIgnore]
        public bool SalesHistoriesSpecified
        {
            get { return this.SalesHistories != null && this.SalesHistories.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SERVITUDES", Order = 29)]
        public SERVITUDES Servitudes { get; set; }
    
        [XmlIgnore]
        public bool ServitudesSpecified
        {
            get { return this.Servitudes != null && this.Servitudes.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SITE", Order = 30)]
        public SITE Site { get; set; }
    
        [XmlIgnore]
        public bool SiteSpecified
        {
            get { return this.Site != null && this.Site.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 31)]
        public PROPERTY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }

        /// <summary>
        /// Used as an attribute on PROPERTY to specify whether the referenced property is the Subject property or a property valuation comparable.
        /// </summary>
        [XmlIgnore]
        private ValuationUseBase? valuationUseType;

        /// <summary>
        /// Gets or sets the Valuation Use Type.
        /// </summary>
        /// <value>A boolean indicating whether the ExpenseType element has been assigned a value.</value>
        [XmlIgnore]
        public ValuationUseBase? ValuationUseType
        {
            get
            {
                return this.valuationUseType;
            }
            set
            {
                this.valuationUseType = value;
                this.IsValuationUseTypeSpecified = this.valuationUseType != ValuationUseBase.Blank;
            }
        }

        /// <summary>
        /// Indicates whether the Valuation Use Type has been assigned a value.
        /// </summary>
        [XmlIgnore]
        protected bool IsValuationUseTypeSpecified = false;

        /// <summary>
        /// Gets or sets the Valuation Use Type member as a string for serialization.
        /// </summary>
        /// <value>A boolean indicating whether the ExpenseType element has been assigned a value.</value>
        [XmlAttribute(AttributeName = "ValuationUseType")]
        public string ValuationUseTypeSerialized
        {
            get
            {
                return this.ValuationUseType.HasValue ? Mismo3Utilities.GetXmlEnumName(this.ValuationUseType) : string.Empty;
            }
            set
            {
            }
        }

        /// <summary>
        /// Indicates whether the Valuation Use Type can be serialized, which depends on whether it has been assigned a value.
        /// </summary>
        /// <returns>A boolean indicating whether the Valuation Use Type can be serialized.</returns>
        public bool ShouldSerializeValuationUseTypeSerialized()
        {
            return this.IsValuationUseTypeSpecified;
        }
    }
}
