namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PROPERTY_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AffordableUnitsCountSpecified
                    || this.AssignmentOfRentsIndicatorSpecified
                    || this.AttachmentTypeSpecified
                    || this.BorrowerContinuesToOwnPropertyIndicatorSpecified
                    || this.CommunityLandTrustIndicatorSpecified
                    || this.CommunityPropertyStateIndicatorSpecified
                    || this.CommunityReinvestmentActDelineatedCommunityIndicatorSpecified
                    || this.ConstructionMethodTypeSpecified
                    || this.ConstructionMethodTypeOtherDescriptionSpecified
                    || this.ConstructionStatusTypeSpecified
                    || this.ConstructionStatusTypeOtherDescriptionSpecified
                    || this.DeedRestrictionIndicatorSpecified
                    || this.DisasterIndicatorSpecified
                    || this.FHASecondaryResidenceIndicatorSpecified
                    || this.FinancedUnitCountSpecified
                    || this.GrossLivingAreaSquareFeetNumberSpecified
                    || this.GroupHomeIndicatorSpecified
                    || this.InvestmentRentalIncomePresentIndicatorSpecified
                    || this.InvestorREOPropertyIdentifierSpecified
                    || this.LandTrustTypeSpecified
                    || this.LandTrustTypeOtherDescriptionSpecified
                    || this.LandUseDescriptionSpecified
                    || this.LandUseTypeSpecified
                    || this.LandUseTypeOtherDescriptionSpecified
                    || this.LenderDesignatedDecliningMarketIdentifierSpecified
                    || this.MetropolitanDivisionIndicatorSpecified
                    || this.MSAIndicatorSpecified
                    || this.NativeAmericanLandsTypeSpecified
                    || this.NativeAmericanLandsTypeOtherDescriptionSpecified
                    || this.PriorSaleInLastTwelveMonthsIndicatorSpecified
                    || this.PropertyAcquiredDateSpecified
                    || this.PropertyAcquiredYearSpecified
                    || this.PropertyAcreageNumberSpecified
                    || this.PropertyConditionDescriptionSpecified
                    || this.PropertyCurrentOccupancyTypeSpecified
                    || this.PropertyCurrentOccupantNameSpecified
                    || this.PropertyCurrentUsageTypeSpecified
                    || this.PropertyCurrentUsageTypeOtherDescriptionSpecified
                    || this.PropertyDispositionStatusTypeSpecified
                    || this.PropertyDispositionStatusTypeOtherDescriptionSpecified
                    || this.PropertyEarthquakeInsuranceIndicatorSpecified
                    || this.PropertyEstateTypeSpecified
                    || this.PropertyEstateTypeOtherDescriptionSpecified
                    || this.PropertyEstimatedValueAmountSpecified
                    || this.PropertyExistingCleanEnergyLienIndicatorSpecified
                    || this.PropertyExistingLienAmountSpecified
                    || this.PropertyFloodInsuranceIndicatorSpecified
                    || this.PropertyGroundLeaseExpirationDateSpecified
                    || this.PropertyGroundLeasePerpetualIndicatorSpecified
                    || this.PropertyInclusionaryZoningIndicatorSpecified
                    || this.PropertyInProjectIndicatorSpecified
                    || this.PropertyMixedUsageIndicatorSpecified
                    || this.PropertyOriginalCostAmountSpecified
                    || this.PropertyPreviouslyOccupiedIndicatorSpecified
                    || this.PropertyStructureBuiltYearSpecified
                    || this.PropertyStructureBuiltYearEstimatedIndicatorSpecified
                    || this.PropertyStructureHabitableYearRoundIndicatorSpecified
                    || this.PropertyUsageTypeSpecified
                    || this.PropertyUsageTypeOtherDescriptionSpecified
                    || this.PropertyVacancyDateSpecified
                    || this.PUDIndicatorSpecified
                    || this.RentalEstimatedGrossMonthlyRentAmountSpecified
                    || this.RentalEstimatedNetMonthlyRentAmountSpecified
                    || this.RuralUnderservedCountyIndicatorSpecified
                    || this.UndefinedPropertyTypeSpecified
                    || this.UniqueDwellingTypeSpecified
                    || this.UniqueDwellingTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AffordableUnitsCount", Order = 0)]
        public MISMOCount AffordableUnitsCount { get; set; }
    
        [XmlIgnore]
        public bool AffordableUnitsCountSpecified
        {
            get { return this.AffordableUnitsCount != null; }
            set { }
        }
    
        [XmlElement("AssignmentOfRentsIndicator", Order = 1)]
        public MISMOIndicator AssignmentOfRentsIndicator { get; set; }
    
        [XmlIgnore]
        public bool AssignmentOfRentsIndicatorSpecified
        {
            get { return this.AssignmentOfRentsIndicator != null; }
            set { }
        }
    
        [XmlElement("AttachmentType", Order = 2)]
        public MISMOEnum<AttachmentBase> AttachmentType { get; set; }
    
        [XmlIgnore]
        public bool AttachmentTypeSpecified
        {
            get { return this.AttachmentType != null; }
            set { }
        }
    
        [XmlElement("BorrowerContinuesToOwnPropertyIndicator", Order = 3)]
        public MISMOIndicator BorrowerContinuesToOwnPropertyIndicator { get; set; }
    
        [XmlIgnore]
        public bool BorrowerContinuesToOwnPropertyIndicatorSpecified
        {
            get { return this.BorrowerContinuesToOwnPropertyIndicator != null; }
            set { }
        }
    
        [XmlElement("CommunityLandTrustIndicator", Order = 4)]
        public MISMOIndicator CommunityLandTrustIndicator { get; set; }
    
        [XmlIgnore]
        public bool CommunityLandTrustIndicatorSpecified
        {
            get { return this.CommunityLandTrustIndicator != null; }
            set { }
        }
    
        [XmlElement("CommunityPropertyStateIndicator", Order = 5)]
        public MISMOIndicator CommunityPropertyStateIndicator { get; set; }
    
        [XmlIgnore]
        public bool CommunityPropertyStateIndicatorSpecified
        {
            get { return this.CommunityPropertyStateIndicator != null; }
            set { }
        }
    
        [XmlElement("CommunityReinvestmentActDelineatedCommunityIndicator", Order = 6)]
        public MISMOIndicator CommunityReinvestmentActDelineatedCommunityIndicator { get; set; }
    
        [XmlIgnore]
        public bool CommunityReinvestmentActDelineatedCommunityIndicatorSpecified
        {
            get { return this.CommunityReinvestmentActDelineatedCommunityIndicator != null; }
            set { }
        }
    
        [XmlElement("ConstructionMethodType", Order = 7)]
        public MISMOEnum<ConstructionMethodBase> ConstructionMethodType { get; set; }
    
        [XmlIgnore]
        public bool ConstructionMethodTypeSpecified
        {
            get { return this.ConstructionMethodType != null; }
            set { }
        }
    
        [XmlElement("ConstructionMethodTypeOtherDescription", Order = 8)]
        public MISMOString ConstructionMethodTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ConstructionMethodTypeOtherDescriptionSpecified
        {
            get { return this.ConstructionMethodTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ConstructionStatusType", Order = 9)]
        public MISMOEnum<ConstructionStatusBase> ConstructionStatusType { get; set; }
    
        [XmlIgnore]
        public bool ConstructionStatusTypeSpecified
        {
            get { return this.ConstructionStatusType != null; }
            set { }
        }
    
        [XmlElement("ConstructionStatusTypeOtherDescription", Order = 10)]
        public MISMOString ConstructionStatusTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ConstructionStatusTypeOtherDescriptionSpecified
        {
            get { return this.ConstructionStatusTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("DeedRestrictionIndicator", Order = 11)]
        public MISMOIndicator DeedRestrictionIndicator { get; set; }
    
        [XmlIgnore]
        public bool DeedRestrictionIndicatorSpecified
        {
            get { return this.DeedRestrictionIndicator != null; }
            set { }
        }
    
        [XmlElement("DisasterIndicator", Order = 12)]
        public MISMOIndicator DisasterIndicator { get; set; }
    
        [XmlIgnore]
        public bool DisasterIndicatorSpecified
        {
            get { return this.DisasterIndicator != null; }
            set { }
        }
    
        [XmlElement("FHASecondaryResidenceIndicator", Order = 13)]
        public MISMOIndicator FHASecondaryResidenceIndicator { get; set; }
    
        [XmlIgnore]
        public bool FHASecondaryResidenceIndicatorSpecified
        {
            get { return this.FHASecondaryResidenceIndicator != null; }
            set { }
        }
    
        [XmlElement("FinancedUnitCount", Order = 14)]
        public MISMOCount FinancedUnitCount { get; set; }
    
        [XmlIgnore]
        public bool FinancedUnitCountSpecified
        {
            get { return this.FinancedUnitCount != null; }
            set { }
        }
    
        [XmlElement("GrossLivingAreaSquareFeetNumber", Order = 15)]
        public MISMONumeric GrossLivingAreaSquareFeetNumber { get; set; }
    
        [XmlIgnore]
        public bool GrossLivingAreaSquareFeetNumberSpecified
        {
            get { return this.GrossLivingAreaSquareFeetNumber != null; }
            set { }
        }
    
        [XmlElement("GroupHomeIndicator", Order = 16)]
        public MISMOIndicator GroupHomeIndicator { get; set; }
    
        [XmlIgnore]
        public bool GroupHomeIndicatorSpecified
        {
            get { return this.GroupHomeIndicator != null; }
            set { }
        }
    
        [XmlElement("InvestmentRentalIncomePresentIndicator", Order = 17)]
        public MISMOIndicator InvestmentRentalIncomePresentIndicator { get; set; }
    
        [XmlIgnore]
        public bool InvestmentRentalIncomePresentIndicatorSpecified
        {
            get { return this.InvestmentRentalIncomePresentIndicator != null; }
            set { }
        }
    
        [XmlElement("InvestorREOPropertyIdentifier", Order = 18)]
        public MISMOIdentifier InvestorREOPropertyIdentifier { get; set; }
    
        [XmlIgnore]
        public bool InvestorREOPropertyIdentifierSpecified
        {
            get { return this.InvestorREOPropertyIdentifier != null; }
            set { }
        }
    
        [XmlElement("LandTrustType", Order = 19)]
        public MISMOEnum<LandTrustBase> LandTrustType { get; set; }
    
        [XmlIgnore]
        public bool LandTrustTypeSpecified
        {
            get { return this.LandTrustType != null; }
            set { }
        }
    
        [XmlElement("LandTrustTypeOtherDescription", Order = 20)]
        public MISMOString LandTrustTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool LandTrustTypeOtherDescriptionSpecified
        {
            get { return this.LandTrustTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("LandUseDescription", Order = 21)]
        public MISMOString LandUseDescription { get; set; }
    
        [XmlIgnore]
        public bool LandUseDescriptionSpecified
        {
            get { return this.LandUseDescription != null; }
            set { }
        }
    
        [XmlElement("LandUseType", Order = 22)]
        public MISMOEnum<LandUseBase> LandUseType { get; set; }
    
        [XmlIgnore]
        public bool LandUseTypeSpecified
        {
            get { return this.LandUseType != null; }
            set { }
        }
    
        [XmlElement("LandUseTypeOtherDescription", Order = 23)]
        public MISMOString LandUseTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool LandUseTypeOtherDescriptionSpecified
        {
            get { return this.LandUseTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("LenderDesignatedDecliningMarketIdentifier", Order = 24)]
        public MISMOIdentifier LenderDesignatedDecliningMarketIdentifier { get; set; }
    
        [XmlIgnore]
        public bool LenderDesignatedDecliningMarketIdentifierSpecified
        {
            get { return this.LenderDesignatedDecliningMarketIdentifier != null; }
            set { }
        }
    
        [XmlElement("MetropolitanDivisionIndicator", Order = 25)]
        public MISMOIndicator MetropolitanDivisionIndicator { get; set; }
    
        [XmlIgnore]
        public bool MetropolitanDivisionIndicatorSpecified
        {
            get { return this.MetropolitanDivisionIndicator != null; }
            set { }
        }
    
        [XmlElement("MSAIndicator", Order = 26)]
        public MISMOIndicator MSAIndicator { get; set; }
    
        [XmlIgnore]
        public bool MSAIndicatorSpecified
        {
            get { return this.MSAIndicator != null; }
            set { }
        }
    
        [XmlElement("NativeAmericanLandsType", Order = 27)]
        public MISMOEnum<NativeAmericanLandsBase> NativeAmericanLandsType { get; set; }
    
        [XmlIgnore]
        public bool NativeAmericanLandsTypeSpecified
        {
            get { return this.NativeAmericanLandsType != null; }
            set { }
        }
    
        [XmlElement("NativeAmericanLandsTypeOtherDescription", Order = 28)]
        public MISMOString NativeAmericanLandsTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool NativeAmericanLandsTypeOtherDescriptionSpecified
        {
            get { return this.NativeAmericanLandsTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PriorSaleInLastTwelveMonthsIndicator", Order = 29)]
        public MISMOIndicator PriorSaleInLastTwelveMonthsIndicator { get; set; }
    
        [XmlIgnore]
        public bool PriorSaleInLastTwelveMonthsIndicatorSpecified
        {
            get { return this.PriorSaleInLastTwelveMonthsIndicator != null; }
            set { }
        }
    
        [XmlElement("PropertyAcquiredDate", Order = 30)]
        public MISMODate PropertyAcquiredDate { get; set; }
    
        [XmlIgnore]
        public bool PropertyAcquiredDateSpecified
        {
            get { return this.PropertyAcquiredDate != null; }
            set { }
        }
    
        [XmlElement("PropertyAcquiredYear", Order = 31)]
        public MISMOYear PropertyAcquiredYear { get; set; }
    
        [XmlIgnore]
        public bool PropertyAcquiredYearSpecified
        {
            get { return this.PropertyAcquiredYear != null; }
            set { }
        }
    
        [XmlElement("PropertyAcreageNumber", Order = 32)]
        public MISMONumeric PropertyAcreageNumber { get; set; }
    
        [XmlIgnore]
        public bool PropertyAcreageNumberSpecified
        {
            get { return this.PropertyAcreageNumber != null; }
            set { }
        }
    
        [XmlElement("PropertyConditionDescription", Order = 33)]
        public MISMOString PropertyConditionDescription { get; set; }
    
        [XmlIgnore]
        public bool PropertyConditionDescriptionSpecified
        {
            get { return this.PropertyConditionDescription != null; }
            set { }
        }
    
        [XmlElement("PropertyCurrentOccupancyType", Order = 34)]
        public MISMOEnum<PropertyCurrentOccupancyBase> PropertyCurrentOccupancyType { get; set; }
    
        [XmlIgnore]
        public bool PropertyCurrentOccupancyTypeSpecified
        {
            get { return this.PropertyCurrentOccupancyType != null; }
            set { }
        }
    
        [XmlElement("PropertyCurrentOccupantName", Order = 35)]
        public MISMOString PropertyCurrentOccupantName { get; set; }
    
        [XmlIgnore]
        public bool PropertyCurrentOccupantNameSpecified
        {
            get { return this.PropertyCurrentOccupantName != null; }
            set { }
        }
    
        [XmlElement("PropertyCurrentUsageType", Order = 36)]
        public MISMOEnum<PropertyCurrentUsageBase> PropertyCurrentUsageType { get; set; }
    
        [XmlIgnore]
        public bool PropertyCurrentUsageTypeSpecified
        {
            get { return this.PropertyCurrentUsageType != null; }
            set { }
        }
    
        [XmlElement("PropertyCurrentUsageTypeOtherDescription", Order = 37)]
        public MISMOString PropertyCurrentUsageTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PropertyCurrentUsageTypeOtherDescriptionSpecified
        {
            get { return this.PropertyCurrentUsageTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PropertyDispositionStatusType", Order = 38)]
        public MISMOEnum<PropertyDispositionStatusBase> PropertyDispositionStatusType { get; set; }
    
        [XmlIgnore]
        public bool PropertyDispositionStatusTypeSpecified
        {
            get { return this.PropertyDispositionStatusType != null; }
            set { }
        }
    
        [XmlElement("PropertyDispositionStatusTypeOtherDescription", Order = 39)]
        public MISMOString PropertyDispositionStatusTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PropertyDispositionStatusTypeOtherDescriptionSpecified
        {
            get { return this.PropertyDispositionStatusTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PropertyEarthquakeInsuranceIndicator", Order = 40)]
        public MISMOIndicator PropertyEarthquakeInsuranceIndicator { get; set; }
    
        [XmlIgnore]
        public bool PropertyEarthquakeInsuranceIndicatorSpecified
        {
            get { return this.PropertyEarthquakeInsuranceIndicator != null; }
            set { }
        }
    
        [XmlElement("PropertyEstateType", Order = 41)]
        public MISMOEnum<PropertyEstateBase> PropertyEstateType { get; set; }
    
        [XmlIgnore]
        public bool PropertyEstateTypeSpecified
        {
            get { return this.PropertyEstateType != null; }
            set { }
        }
    
        [XmlElement("PropertyEstateTypeOtherDescription", Order = 42)]
        public MISMOString PropertyEstateTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PropertyEstateTypeOtherDescriptionSpecified
        {
            get { return this.PropertyEstateTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PropertyEstimatedValueAmount", Order = 43)]
        public MISMOAmount PropertyEstimatedValueAmount { get; set; }
    
        [XmlIgnore]
        public bool PropertyEstimatedValueAmountSpecified
        {
            get { return this.PropertyEstimatedValueAmount != null; }
            set { }
        }
    
        [XmlElement("PropertyExistingCleanEnergyLienIndicator", Order = 44)]
        public MISMOIndicator PropertyExistingCleanEnergyLienIndicator { get; set; }
    
        [XmlIgnore]
        public bool PropertyExistingCleanEnergyLienIndicatorSpecified
        {
            get { return this.PropertyExistingCleanEnergyLienIndicator != null; }
            set { }
        }
    
        [XmlElement("PropertyExistingLienAmount", Order = 45)]
        public MISMOAmount PropertyExistingLienAmount { get; set; }
    
        [XmlIgnore]
        public bool PropertyExistingLienAmountSpecified
        {
            get { return this.PropertyExistingLienAmount != null; }
            set { }
        }
    
        [XmlElement("PropertyFloodInsuranceIndicator", Order = 46)]
        public MISMOIndicator PropertyFloodInsuranceIndicator { get; set; }
    
        [XmlIgnore]
        public bool PropertyFloodInsuranceIndicatorSpecified
        {
            get { return this.PropertyFloodInsuranceIndicator != null; }
            set { }
        }
    
        [XmlElement("PropertyGroundLeaseExpirationDate", Order = 47)]
        public MISMODate PropertyGroundLeaseExpirationDate { get; set; }
    
        [XmlIgnore]
        public bool PropertyGroundLeaseExpirationDateSpecified
        {
            get { return this.PropertyGroundLeaseExpirationDate != null; }
            set { }
        }
    
        [XmlElement("PropertyGroundLeasePerpetualIndicator", Order = 48)]
        public MISMOIndicator PropertyGroundLeasePerpetualIndicator { get; set; }
    
        [XmlIgnore]
        public bool PropertyGroundLeasePerpetualIndicatorSpecified
        {
            get { return this.PropertyGroundLeasePerpetualIndicator != null; }
            set { }
        }
    
        [XmlElement("PropertyInclusionaryZoningIndicator", Order = 49)]
        public MISMOIndicator PropertyInclusionaryZoningIndicator { get; set; }
    
        [XmlIgnore]
        public bool PropertyInclusionaryZoningIndicatorSpecified
        {
            get { return this.PropertyInclusionaryZoningIndicator != null; }
            set { }
        }
    
        [XmlElement("PropertyInProjectIndicator", Order = 50)]
        public MISMOIndicator PropertyInProjectIndicator { get; set; }
    
        [XmlIgnore]
        public bool PropertyInProjectIndicatorSpecified
        {
            get { return this.PropertyInProjectIndicator != null; }
            set { }
        }
    
        [XmlElement("PropertyMixedUsageIndicator", Order = 51)]
        public MISMOIndicator PropertyMixedUsageIndicator { get; set; }
    
        [XmlIgnore]
        public bool PropertyMixedUsageIndicatorSpecified
        {
            get { return this.PropertyMixedUsageIndicator != null; }
            set { }
        }
    
        [XmlElement("PropertyOriginalCostAmount", Order = 52)]
        public MISMOAmount PropertyOriginalCostAmount { get; set; }
    
        [XmlIgnore]
        public bool PropertyOriginalCostAmountSpecified
        {
            get { return this.PropertyOriginalCostAmount != null; }
            set { }
        }
    
        [XmlElement("PropertyPreviouslyOccupiedIndicator", Order = 53)]
        public MISMOIndicator PropertyPreviouslyOccupiedIndicator { get; set; }
    
        [XmlIgnore]
        public bool PropertyPreviouslyOccupiedIndicatorSpecified
        {
            get { return this.PropertyPreviouslyOccupiedIndicator != null; }
            set { }
        }
    
        [XmlElement("PropertyStructureBuiltYear", Order = 54)]
        public MISMOYear PropertyStructureBuiltYear { get; set; }
    
        [XmlIgnore]
        public bool PropertyStructureBuiltYearSpecified
        {
            get { return this.PropertyStructureBuiltYear != null; }
            set { }
        }
    
        [XmlElement("PropertyStructureBuiltYearEstimatedIndicator", Order = 55)]
        public MISMOIndicator PropertyStructureBuiltYearEstimatedIndicator { get; set; }
    
        [XmlIgnore]
        public bool PropertyStructureBuiltYearEstimatedIndicatorSpecified
        {
            get { return this.PropertyStructureBuiltYearEstimatedIndicator != null; }
            set { }
        }
    
        [XmlElement("PropertyStructureHabitableYearRoundIndicator", Order = 56)]
        public MISMOIndicator PropertyStructureHabitableYearRoundIndicator { get; set; }
    
        [XmlIgnore]
        public bool PropertyStructureHabitableYearRoundIndicatorSpecified
        {
            get { return this.PropertyStructureHabitableYearRoundIndicator != null; }
            set { }
        }
    
        [XmlElement("PropertyUsageType", Order = 57)]
        public MISMOEnum<PropertyUsageBase> PropertyUsageType { get; set; }
    
        [XmlIgnore]
        public bool PropertyUsageTypeSpecified
        {
            get { return this.PropertyUsageType != null; }
            set { }
        }
    
        [XmlElement("PropertyUsageTypeOtherDescription", Order = 58)]
        public MISMOString PropertyUsageTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PropertyUsageTypeOtherDescriptionSpecified
        {
            get { return this.PropertyUsageTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PropertyVacancyDate", Order = 59)]
        public MISMODate PropertyVacancyDate { get; set; }
    
        [XmlIgnore]
        public bool PropertyVacancyDateSpecified
        {
            get { return this.PropertyVacancyDate != null; }
            set { }
        }
    
        [XmlElement("PUDIndicator", Order = 60)]
        public MISMOIndicator PUDIndicator { get; set; }
    
        [XmlIgnore]
        public bool PUDIndicatorSpecified
        {
            get { return this.PUDIndicator != null; }
            set { }
        }
    
        [XmlElement("RentalEstimatedGrossMonthlyRentAmount", Order = 61)]
        public MISMOAmount RentalEstimatedGrossMonthlyRentAmount { get; set; }
    
        [XmlIgnore]
        public bool RentalEstimatedGrossMonthlyRentAmountSpecified
        {
            get { return this.RentalEstimatedGrossMonthlyRentAmount != null; }
            set { }
        }
    
        [XmlElement("RentalEstimatedNetMonthlyRentAmount", Order = 62)]
        public MISMOAmount RentalEstimatedNetMonthlyRentAmount { get; set; }
    
        [XmlIgnore]
        public bool RentalEstimatedNetMonthlyRentAmountSpecified
        {
            get { return this.RentalEstimatedNetMonthlyRentAmount != null; }
            set { }
        }
    
        [XmlElement("RuralUnderservedCountyIndicator", Order = 63)]
        public MISMOIndicator RuralUnderservedCountyIndicator { get; set; }
    
        [XmlIgnore]
        public bool RuralUnderservedCountyIndicatorSpecified
        {
            get { return this.RuralUnderservedCountyIndicator != null; }
            set { }
        }
    
        [XmlElement("UndefinedPropertyType", Order = 64)]
        public MISMOEnum<UndefinedPropertyBase> UndefinedPropertyType { get; set; }
    
        [XmlIgnore]
        public bool UndefinedPropertyTypeSpecified
        {
            get { return this.UndefinedPropertyType != null; }
            set { }
        }
    
        [XmlElement("UniqueDwellingType", Order = 65)]
        public MISMOEnum<UniqueDwellingBase> UniqueDwellingType { get; set; }
    
        [XmlIgnore]
        public bool UniqueDwellingTypeSpecified
        {
            get { return this.UniqueDwellingType != null; }
            set { }
        }
    
        [XmlElement("UniqueDwellingTypeOtherDescription", Order = 66)]
        public MISMOString UniqueDwellingTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool UniqueDwellingTypeOtherDescriptionSpecified
        {
            get { return this.UniqueDwellingTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 67)]
        public PROPERTY_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
