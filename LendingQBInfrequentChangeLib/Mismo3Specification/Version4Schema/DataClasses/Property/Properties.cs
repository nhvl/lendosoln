namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class PROPERTIES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PropertyListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PROPERTY", Order = 0)]
        public List<PROPERTY> PropertyList { get; set; } = new List<PROPERTY>();
    
        [XmlIgnore]
        public bool PropertyListSpecified
        {
            get { return this.PropertyList != null && this.PropertyList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public PROPERTIES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
