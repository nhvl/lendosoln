namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PRINCIPAL_AND_INTEREST_PAYMENT_LIFETIME_ADJUSTMENT_RULE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.FinalPrincipalAndInterestPaymentChangeDateSpecified
                    || this.FirstPrincipalAndInterestPaymentChangeDateSpecified
                    || this.FirstPrincipalAndInterestPaymentChangeMonthsCountSpecified
                    || this.GEMPayoffYearsCountSpecified
                    || this.GPMAndGEMPrincipalAndInterestPaymentChangesCountSpecified
                    || this.GPMMultiplierRatePercentSpecified
                    || this.InitialPaymentDiscountPercentSpecified
                    || this.PaymentAdjustmentLifetimeCapAmountSpecified
                    || this.PaymentAdjustmentLifetimeCapPercentSpecified
                    || this.PaymentsBetweenPrincipalAndInterestPaymentChangesCountSpecified
                    || this.PrincipalAndInterestCalculationPaymentPeriodTypeSpecified
                    || this.PrincipalAndInterestCalculationPaymentPeriodTypeOtherDescriptionSpecified
                    || this.PrincipalAndInterestPaymentDecreaseCapTypeSpecified
                    || this.PrincipalAndInterestPaymentFinalRecastTypeSpecified
                    || this.PrincipalAndInterestPaymentFinalRecastTypeOtherDescriptionSpecified
                    || this.PrincipalAndInterestPaymentIncreaseCapTypeSpecified
                    || this.PrincipalAndInterestPaymentMaximumAmountSpecified
                    || this.PrincipalAndInterestPaymentMaximumAmountEarliestEffectiveMonthsCountSpecified
                    || this.PrincipalAndInterestPaymentMaximumDecreaseRatePercentSpecified
                    || this.PrincipalAndInterestPaymentMaximumExtensionCountSpecified
                    || this.PrincipalAndInterestPaymentMinimumAmountSpecified
                    || this.PrincipalBalanceCalculationMethodTypeSpecified
                    || this.TotalStepCountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("FinalPrincipalAndInterestPaymentChangeDate", Order = 0)]
        public MISMODate FinalPrincipalAndInterestPaymentChangeDate { get; set; }
    
        [XmlIgnore]
        public bool FinalPrincipalAndInterestPaymentChangeDateSpecified
        {
            get { return this.FinalPrincipalAndInterestPaymentChangeDate != null; }
            set { }
        }
    
        [XmlElement("FirstPrincipalAndInterestPaymentChangeDate", Order = 1)]
        public MISMODate FirstPrincipalAndInterestPaymentChangeDate { get; set; }
    
        [XmlIgnore]
        public bool FirstPrincipalAndInterestPaymentChangeDateSpecified
        {
            get { return this.FirstPrincipalAndInterestPaymentChangeDate != null; }
            set { }
        }
    
        [XmlElement("FirstPrincipalAndInterestPaymentChangeMonthsCount", Order = 2)]
        public MISMOCount FirstPrincipalAndInterestPaymentChangeMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool FirstPrincipalAndInterestPaymentChangeMonthsCountSpecified
        {
            get { return this.FirstPrincipalAndInterestPaymentChangeMonthsCount != null; }
            set { }
        }
    
        [XmlElement("GEMPayoffYearsCount", Order = 3)]
        public MISMOCount GEMPayoffYearsCount { get; set; }
    
        [XmlIgnore]
        public bool GEMPayoffYearsCountSpecified
        {
            get { return this.GEMPayoffYearsCount != null; }
            set { }
        }
    
        [XmlElement("GPMAndGEMPrincipalAndInterestPaymentChangesCount", Order = 4)]
        public MISMOCount GPMAndGEMPrincipalAndInterestPaymentChangesCount { get; set; }
    
        [XmlIgnore]
        public bool GPMAndGEMPrincipalAndInterestPaymentChangesCountSpecified
        {
            get { return this.GPMAndGEMPrincipalAndInterestPaymentChangesCount != null; }
            set { }
        }
    
        [XmlElement("GPMMultiplierRatePercent", Order = 5)]
        public MISMOPercent GPMMultiplierRatePercent { get; set; }
    
        [XmlIgnore]
        public bool GPMMultiplierRatePercentSpecified
        {
            get { return this.GPMMultiplierRatePercent != null; }
            set { }
        }
    
        [XmlElement("InitialPaymentDiscountPercent", Order = 6)]
        public MISMOPercent InitialPaymentDiscountPercent { get; set; }
    
        [XmlIgnore]
        public bool InitialPaymentDiscountPercentSpecified
        {
            get { return this.InitialPaymentDiscountPercent != null; }
            set { }
        }
    
        [XmlElement("PaymentAdjustmentLifetimeCapAmount", Order = 7)]
        public MISMOAmount PaymentAdjustmentLifetimeCapAmount { get; set; }
    
        [XmlIgnore]
        public bool PaymentAdjustmentLifetimeCapAmountSpecified
        {
            get { return this.PaymentAdjustmentLifetimeCapAmount != null; }
            set { }
        }
    
        [XmlElement("PaymentAdjustmentLifetimeCapPercent", Order = 8)]
        public MISMOPercent PaymentAdjustmentLifetimeCapPercent { get; set; }
    
        [XmlIgnore]
        public bool PaymentAdjustmentLifetimeCapPercentSpecified
        {
            get { return this.PaymentAdjustmentLifetimeCapPercent != null; }
            set { }
        }
    
        [XmlElement("PaymentsBetweenPrincipalAndInterestPaymentChangesCount", Order = 9)]
        public MISMOCount PaymentsBetweenPrincipalAndInterestPaymentChangesCount { get; set; }
    
        [XmlIgnore]
        public bool PaymentsBetweenPrincipalAndInterestPaymentChangesCountSpecified
        {
            get { return this.PaymentsBetweenPrincipalAndInterestPaymentChangesCount != null; }
            set { }
        }
    
        [XmlElement("PrincipalAndInterestCalculationPaymentPeriodType", Order = 10)]
        public MISMOEnum<PrincipalAndInterestCalculationPaymentPeriodBase> PrincipalAndInterestCalculationPaymentPeriodType { get; set; }
    
        [XmlIgnore]
        public bool PrincipalAndInterestCalculationPaymentPeriodTypeSpecified
        {
            get { return this.PrincipalAndInterestCalculationPaymentPeriodType != null; }
            set { }
        }
    
        [XmlElement("PrincipalAndInterestCalculationPaymentPeriodTypeOtherDescription", Order = 11)]
        public MISMOString PrincipalAndInterestCalculationPaymentPeriodTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PrincipalAndInterestCalculationPaymentPeriodTypeOtherDescriptionSpecified
        {
            get { return this.PrincipalAndInterestCalculationPaymentPeriodTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PrincipalAndInterestPaymentDecreaseCapType", Order = 12)]
        public MISMOEnum<PrincipalAndInterestPaymentDecreaseCapBase> PrincipalAndInterestPaymentDecreaseCapType { get; set; }
    
        [XmlIgnore]
        public bool PrincipalAndInterestPaymentDecreaseCapTypeSpecified
        {
            get { return this.PrincipalAndInterestPaymentDecreaseCapType != null; }
            set { }
        }
    
        [XmlElement("PrincipalAndInterestPaymentFinalRecastType", Order = 13)]
        public MISMOEnum<PrincipalAndInterestPaymentFinalRecastBase> PrincipalAndInterestPaymentFinalRecastType { get; set; }
    
        [XmlIgnore]
        public bool PrincipalAndInterestPaymentFinalRecastTypeSpecified
        {
            get { return this.PrincipalAndInterestPaymentFinalRecastType != null; }
            set { }
        }
    
        [XmlElement("PrincipalAndInterestPaymentFinalRecastTypeOtherDescription", Order = 14)]
        public MISMOString PrincipalAndInterestPaymentFinalRecastTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PrincipalAndInterestPaymentFinalRecastTypeOtherDescriptionSpecified
        {
            get { return this.PrincipalAndInterestPaymentFinalRecastTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PrincipalAndInterestPaymentIncreaseCapType", Order = 15)]
        public MISMOEnum<PrincipalAndInterestPaymentIncreaseCapBase> PrincipalAndInterestPaymentIncreaseCapType { get; set; }
    
        [XmlIgnore]
        public bool PrincipalAndInterestPaymentIncreaseCapTypeSpecified
        {
            get { return this.PrincipalAndInterestPaymentIncreaseCapType != null; }
            set { }
        }
    
        [XmlElement("PrincipalAndInterestPaymentMaximumAmount", Order = 16)]
        public MISMOAmount PrincipalAndInterestPaymentMaximumAmount { get; set; }
    
        [XmlIgnore]
        public bool PrincipalAndInterestPaymentMaximumAmountSpecified
        {
            get { return this.PrincipalAndInterestPaymentMaximumAmount != null; }
            set { }
        }
    
        [XmlElement("PrincipalAndInterestPaymentMaximumAmountEarliestEffectiveMonthsCount", Order = 17)]
        public MISMOCount PrincipalAndInterestPaymentMaximumAmountEarliestEffectiveMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool PrincipalAndInterestPaymentMaximumAmountEarliestEffectiveMonthsCountSpecified
        {
            get { return this.PrincipalAndInterestPaymentMaximumAmountEarliestEffectiveMonthsCount != null; }
            set { }
        }
    
        [XmlElement("PrincipalAndInterestPaymentMaximumDecreaseRatePercent", Order = 18)]
        public MISMOPercent PrincipalAndInterestPaymentMaximumDecreaseRatePercent { get; set; }
    
        [XmlIgnore]
        public bool PrincipalAndInterestPaymentMaximumDecreaseRatePercentSpecified
        {
            get { return this.PrincipalAndInterestPaymentMaximumDecreaseRatePercent != null; }
            set { }
        }
    
        [XmlElement("PrincipalAndInterestPaymentMaximumExtensionCount", Order = 19)]
        public MISMOCount PrincipalAndInterestPaymentMaximumExtensionCount { get; set; }
    
        [XmlIgnore]
        public bool PrincipalAndInterestPaymentMaximumExtensionCountSpecified
        {
            get { return this.PrincipalAndInterestPaymentMaximumExtensionCount != null; }
            set { }
        }
    
        [XmlElement("PrincipalAndInterestPaymentMinimumAmount", Order = 20)]
        public MISMOAmount PrincipalAndInterestPaymentMinimumAmount { get; set; }
    
        [XmlIgnore]
        public bool PrincipalAndInterestPaymentMinimumAmountSpecified
        {
            get { return this.PrincipalAndInterestPaymentMinimumAmount != null; }
            set { }
        }
    
        [XmlElement("PrincipalBalanceCalculationMethodType", Order = 21)]
        public MISMOEnum<PrincipalBalanceCalculationMethodBase> PrincipalBalanceCalculationMethodType { get; set; }
    
        [XmlIgnore]
        public bool PrincipalBalanceCalculationMethodTypeSpecified
        {
            get { return this.PrincipalBalanceCalculationMethodType != null; }
            set { }
        }
    
        [XmlElement("TotalStepCount", Order = 22)]
        public MISMOCount TotalStepCount { get; set; }
    
        [XmlIgnore]
        public bool TotalStepCountSpecified
        {
            get { return this.TotalStepCount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 23)]
        public PRINCIPAL_AND_INTEREST_PAYMENT_LIFETIME_ADJUSTMENT_RULE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
