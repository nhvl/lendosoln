namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PROJECT_ANALYSIS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CharacteristicsAffectMarketabilityDescriptionSpecified
                    || this.OwnersWithinProjectMoreThanTwoMonthsDelinquentCountSpecified
                    || this.ProjectAnalysisAdditionalFacilitiesFeeAmountSpecified
                    || this.ProjectAnalysisAdditionalFacilitiesFeeDescriptionSpecified
                    || this.ProjectAnalysisAdditionalFacilitiesFeeIndicatorSpecified
                    || this.ProjectAnalysisAdequacyOfBudgetTypeSpecified
                    || this.ProjectAnalysisAdequacyOfManagementTypeSpecified
                    || this.ProjectAnalysisBudgetAnalysisCommentDescriptionSpecified
                    || this.ProjectAnalysisBudgetAnalyzedIndicatorSpecified
                    || this.ProjectAnalysisCharacteristicsAffectMarketabilityIndicatorSpecified
                    || this.ProjectAnalysisCompetitiveProjectComparisonDescriptionSpecified
                    || this.ProjectAnalysisCompetitiveProjectComparisonTypeSpecified
                    || this.ProjectAnalysisGroundRentAmountSpecified
                    || this.ProjectAnalysisGroundRentDescriptionSpecified
                    || this.ProjectAnalysisGroundRentIndicatorSpecified
                    || this.ProjectAnalysisSpecialCharacteristicsDescriptionSpecified
                    || this.ProjectAnalysisSpecialCharacteristicsIndicatorSpecified
                    || this.ProjectConditionAndQualityDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CharacteristicsAffectMarketabilityDescription", Order = 0)]
        public MISMOString CharacteristicsAffectMarketabilityDescription { get; set; }
    
        [XmlIgnore]
        public bool CharacteristicsAffectMarketabilityDescriptionSpecified
        {
            get { return this.CharacteristicsAffectMarketabilityDescription != null; }
            set { }
        }
    
        [XmlElement("OwnersWithinProjectMoreThanTwoMonthsDelinquentCount", Order = 1)]
        public MISMOCount OwnersWithinProjectMoreThanTwoMonthsDelinquentCount { get; set; }
    
        [XmlIgnore]
        public bool OwnersWithinProjectMoreThanTwoMonthsDelinquentCountSpecified
        {
            get { return this.OwnersWithinProjectMoreThanTwoMonthsDelinquentCount != null; }
            set { }
        }
    
        [XmlElement("ProjectAnalysisAdditionalFacilitiesFeeAmount", Order = 2)]
        public MISMOAmount ProjectAnalysisAdditionalFacilitiesFeeAmount { get; set; }
    
        [XmlIgnore]
        public bool ProjectAnalysisAdditionalFacilitiesFeeAmountSpecified
        {
            get { return this.ProjectAnalysisAdditionalFacilitiesFeeAmount != null; }
            set { }
        }
    
        [XmlElement("ProjectAnalysisAdditionalFacilitiesFeeDescription", Order = 3)]
        public MISMOString ProjectAnalysisAdditionalFacilitiesFeeDescription { get; set; }
    
        [XmlIgnore]
        public bool ProjectAnalysisAdditionalFacilitiesFeeDescriptionSpecified
        {
            get { return this.ProjectAnalysisAdditionalFacilitiesFeeDescription != null; }
            set { }
        }
    
        [XmlElement("ProjectAnalysisAdditionalFacilitiesFeeIndicator", Order = 4)]
        public MISMOIndicator ProjectAnalysisAdditionalFacilitiesFeeIndicator { get; set; }
    
        [XmlIgnore]
        public bool ProjectAnalysisAdditionalFacilitiesFeeIndicatorSpecified
        {
            get { return this.ProjectAnalysisAdditionalFacilitiesFeeIndicator != null; }
            set { }
        }
    
        [XmlElement("ProjectAnalysisAdequacyOfBudgetType", Order = 5)]
        public MISMOEnum<ProjectAnalysisAdequacyOfBudgetBase> ProjectAnalysisAdequacyOfBudgetType { get; set; }
    
        [XmlIgnore]
        public bool ProjectAnalysisAdequacyOfBudgetTypeSpecified
        {
            get { return this.ProjectAnalysisAdequacyOfBudgetType != null; }
            set { }
        }
    
        [XmlElement("ProjectAnalysisAdequacyOfManagementType", Order = 6)]
        public MISMOEnum<ProjectAnalysisAdequacyOfManagementBase> ProjectAnalysisAdequacyOfManagementType { get; set; }
    
        [XmlIgnore]
        public bool ProjectAnalysisAdequacyOfManagementTypeSpecified
        {
            get { return this.ProjectAnalysisAdequacyOfManagementType != null; }
            set { }
        }
    
        [XmlElement("ProjectAnalysisBudgetAnalysisCommentDescription", Order = 7)]
        public MISMOString ProjectAnalysisBudgetAnalysisCommentDescription { get; set; }
    
        [XmlIgnore]
        public bool ProjectAnalysisBudgetAnalysisCommentDescriptionSpecified
        {
            get { return this.ProjectAnalysisBudgetAnalysisCommentDescription != null; }
            set { }
        }
    
        [XmlElement("ProjectAnalysisBudgetAnalyzedIndicator", Order = 8)]
        public MISMOIndicator ProjectAnalysisBudgetAnalyzedIndicator { get; set; }
    
        [XmlIgnore]
        public bool ProjectAnalysisBudgetAnalyzedIndicatorSpecified
        {
            get { return this.ProjectAnalysisBudgetAnalyzedIndicator != null; }
            set { }
        }
    
        [XmlElement("ProjectAnalysisCharacteristicsAffectMarketabilityIndicator", Order = 9)]
        public MISMOIndicator ProjectAnalysisCharacteristicsAffectMarketabilityIndicator { get; set; }
    
        [XmlIgnore]
        public bool ProjectAnalysisCharacteristicsAffectMarketabilityIndicatorSpecified
        {
            get { return this.ProjectAnalysisCharacteristicsAffectMarketabilityIndicator != null; }
            set { }
        }
    
        [XmlElement("ProjectAnalysisCompetitiveProjectComparisonDescription", Order = 10)]
        public MISMOString ProjectAnalysisCompetitiveProjectComparisonDescription { get; set; }
    
        [XmlIgnore]
        public bool ProjectAnalysisCompetitiveProjectComparisonDescriptionSpecified
        {
            get { return this.ProjectAnalysisCompetitiveProjectComparisonDescription != null; }
            set { }
        }
    
        [XmlElement("ProjectAnalysisCompetitiveProjectComparisonType", Order = 11)]
        public MISMOEnum<ProjectAnalysisCompetitiveProjectComparisonBase> ProjectAnalysisCompetitiveProjectComparisonType { get; set; }
    
        [XmlIgnore]
        public bool ProjectAnalysisCompetitiveProjectComparisonTypeSpecified
        {
            get { return this.ProjectAnalysisCompetitiveProjectComparisonType != null; }
            set { }
        }
    
        [XmlElement("ProjectAnalysisGroundRentAmount", Order = 12)]
        public MISMOAmount ProjectAnalysisGroundRentAmount { get; set; }
    
        [XmlIgnore]
        public bool ProjectAnalysisGroundRentAmountSpecified
        {
            get { return this.ProjectAnalysisGroundRentAmount != null; }
            set { }
        }
    
        [XmlElement("ProjectAnalysisGroundRentDescription", Order = 13)]
        public MISMOString ProjectAnalysisGroundRentDescription { get; set; }
    
        [XmlIgnore]
        public bool ProjectAnalysisGroundRentDescriptionSpecified
        {
            get { return this.ProjectAnalysisGroundRentDescription != null; }
            set { }
        }
    
        [XmlElement("ProjectAnalysisGroundRentIndicator", Order = 14)]
        public MISMOIndicator ProjectAnalysisGroundRentIndicator { get; set; }
    
        [XmlIgnore]
        public bool ProjectAnalysisGroundRentIndicatorSpecified
        {
            get { return this.ProjectAnalysisGroundRentIndicator != null; }
            set { }
        }
    
        [XmlElement("ProjectAnalysisSpecialCharacteristicsDescription", Order = 15)]
        public MISMOString ProjectAnalysisSpecialCharacteristicsDescription { get; set; }
    
        [XmlIgnore]
        public bool ProjectAnalysisSpecialCharacteristicsDescriptionSpecified
        {
            get { return this.ProjectAnalysisSpecialCharacteristicsDescription != null; }
            set { }
        }
    
        [XmlElement("ProjectAnalysisSpecialCharacteristicsIndicator", Order = 16)]
        public MISMOIndicator ProjectAnalysisSpecialCharacteristicsIndicator { get; set; }
    
        [XmlIgnore]
        public bool ProjectAnalysisSpecialCharacteristicsIndicatorSpecified
        {
            get { return this.ProjectAnalysisSpecialCharacteristicsIndicator != null; }
            set { }
        }
    
        [XmlElement("ProjectConditionAndQualityDescription", Order = 17)]
        public MISMOString ProjectConditionAndQualityDescription { get; set; }
    
        [XmlIgnore]
        public bool ProjectConditionAndQualityDescriptionSpecified
        {
            get { return this.ProjectConditionAndQualityDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 18)]
        public PROJECT_ANALYSIS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
