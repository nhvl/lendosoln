namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class CONDITIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ConditionListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CONDITION", Order = 0)]
        public List<CONDITION> ConditionList { get; set; } = new List<CONDITION>();
    
        [XmlIgnore]
        public bool ConditionListSpecified
        {
            get { return this.ConditionList != null && this.ConditionList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public CONDITIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
