namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CONDITION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ConditionDescriptionSpecified
                    || this.ConditionMetIndicatorSpecified
                    || this.ConditionSatisfactionApprovedByNameSpecified
                    || this.ConditionSatisfactionDateSpecified
                    || this.ConditionSatisfactionDueDateSpecified
                    || this.ConditionSatisfactionResponsiblePartyTypeSpecified
                    || this.ConditionSatisfactionResponsiblePartyTypeOtherDescriptionSpecified
                    || this.ConditionSatisfactionTimeframeTypeSpecified
                    || this.ConditionSatisfactionTimeframeTypeOtherDescriptionSpecified
                    || this.ConditionWaivedIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ConditionDescription", Order = 0)]
        public MISMOString ConditionDescription { get; set; }
    
        [XmlIgnore]
        public bool ConditionDescriptionSpecified
        {
            get { return this.ConditionDescription != null; }
            set { }
        }
    
        [XmlElement("ConditionMetIndicator", Order = 1)]
        public MISMOIndicator ConditionMetIndicator { get; set; }
    
        [XmlIgnore]
        public bool ConditionMetIndicatorSpecified
        {
            get { return this.ConditionMetIndicator != null; }
            set { }
        }
    
        [XmlElement("ConditionSatisfactionApprovedByName", Order = 2)]
        public MISMOString ConditionSatisfactionApprovedByName { get; set; }
    
        [XmlIgnore]
        public bool ConditionSatisfactionApprovedByNameSpecified
        {
            get { return this.ConditionSatisfactionApprovedByName != null; }
            set { }
        }
    
        [XmlElement("ConditionSatisfactionDate", Order = 3)]
        public MISMODate ConditionSatisfactionDate { get; set; }
    
        [XmlIgnore]
        public bool ConditionSatisfactionDateSpecified
        {
            get { return this.ConditionSatisfactionDate != null; }
            set { }
        }
    
        [XmlElement("ConditionSatisfactionDueDate", Order = 4)]
        public MISMODate ConditionSatisfactionDueDate { get; set; }
    
        [XmlIgnore]
        public bool ConditionSatisfactionDueDateSpecified
        {
            get { return this.ConditionSatisfactionDueDate != null; }
            set { }
        }
    
        [XmlElement("ConditionSatisfactionResponsiblePartyType", Order = 5)]
        public MISMOEnum<ConditionSatisfactionResponsiblePartyBase> ConditionSatisfactionResponsiblePartyType { get; set; }
    
        [XmlIgnore]
        public bool ConditionSatisfactionResponsiblePartyTypeSpecified
        {
            get { return this.ConditionSatisfactionResponsiblePartyType != null; }
            set { }
        }
    
        [XmlElement("ConditionSatisfactionResponsiblePartyTypeOtherDescription", Order = 6)]
        public MISMOString ConditionSatisfactionResponsiblePartyTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ConditionSatisfactionResponsiblePartyTypeOtherDescriptionSpecified
        {
            get { return this.ConditionSatisfactionResponsiblePartyTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ConditionSatisfactionTimeframeType", Order = 7)]
        public MISMOEnum<ConditionSatisfactionTimeframeBase> ConditionSatisfactionTimeframeType { get; set; }
    
        [XmlIgnore]
        public bool ConditionSatisfactionTimeframeTypeSpecified
        {
            get { return this.ConditionSatisfactionTimeframeType != null; }
            set { }
        }
    
        [XmlElement("ConditionSatisfactionTimeframeTypeOtherDescription", Order = 8)]
        public MISMOString ConditionSatisfactionTimeframeTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ConditionSatisfactionTimeframeTypeOtherDescriptionSpecified
        {
            get { return this.ConditionSatisfactionTimeframeTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ConditionWaivedIndicator", Order = 9)]
        public MISMOIndicator ConditionWaivedIndicator { get; set; }
    
        [XmlIgnore]
        public bool ConditionWaivedIndicatorSpecified
        {
            get { return this.ConditionWaivedIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 10)]
        public CONDITION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
