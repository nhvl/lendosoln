namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class SUPPORTING_RECORD_SETS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SupportingRecordSetListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SUPPORTING_RECORD_SET", Order = 0)]
        public List<SUPPORTING_RECORD_SET> SupportingRecordSetList { get; set; } = new List<SUPPORTING_RECORD_SET>();
    
        [XmlIgnore]
        public bool SupportingRecordSetListSpecified
        {
            get { return this.SupportingRecordSetList != null && this.SupportingRecordSetList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public SUPPORTING_RECORD_SETS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
