namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SUPPORTING_RECORD_SET_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SupportingRecordSetPurposeTypeSpecified
                    || this.SupportingRecordSetPurposeTypeAdditionalDescriptionSpecified
                    || this.SupportingRecordSetPurposeTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SupportingRecordSetPurposeType", Order = 0)]
        public MISMOEnum<SupportingRecordSetPurposeBase> SupportingRecordSetPurposeType { get; set; }
    
        [XmlIgnore]
        public bool SupportingRecordSetPurposeTypeSpecified
        {
            get { return this.SupportingRecordSetPurposeType != null; }
            set { }
        }
    
        [XmlElement("SupportingRecordSetPurposeTypeAdditionalDescription", Order = 1)]
        public MISMOString SupportingRecordSetPurposeTypeAdditionalDescription { get; set; }
    
        [XmlIgnore]
        public bool SupportingRecordSetPurposeTypeAdditionalDescriptionSpecified
        {
            get { return this.SupportingRecordSetPurposeTypeAdditionalDescription != null; }
            set { }
        }
    
        [XmlElement("SupportingRecordSetPurposeTypeOtherDescription", Order = 2)]
        public MISMOString SupportingRecordSetPurposeTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool SupportingRecordSetPurposeTypeOtherDescriptionSpecified
        {
            get { return this.SupportingRecordSetPurposeTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public SUPPORTING_RECORD_SET_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
