namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SUPPORTING_RECORD_SET
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SupportingRecordSetDeliveriesSpecified
                    || this.SupportingRecordSetDetailSpecified
                    || this.SupportingRecordsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SUPPORTING_RECORD_SET_DELIVERIES", Order = 0)]
        public SUPPORTING_RECORD_SET_DELIVERIES SupportingRecordSetDeliveries { get; set; }
    
        [XmlIgnore]
        public bool SupportingRecordSetDeliveriesSpecified
        {
            get { return this.SupportingRecordSetDeliveries != null && this.SupportingRecordSetDeliveries.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SUPPORTING_RECORD_SET_DETAIL", Order = 1)]
        public SUPPORTING_RECORD_SET_DETAIL SupportingRecordSetDetail { get; set; }
    
        [XmlIgnore]
        public bool SupportingRecordSetDetailSpecified
        {
            get { return this.SupportingRecordSetDetail != null && this.SupportingRecordSetDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SUPPORTING_RECORDS", Order = 2)]
        public SUPPORTING_RECORDS SupportingRecords { get; set; }
    
        [XmlIgnore]
        public bool SupportingRecordsSpecified
        {
            get { return this.SupportingRecords != null && this.SupportingRecords.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public SUPPORTING_RECORD_SET_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
