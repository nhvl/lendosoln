namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ORIGINATING_RECORDING_REQUEST
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DigitalSignatureDigestValueSpecified
                    || this.OriginatingRecordingRequestIdentifierSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DigitalSignatureDigestValue", Order = 0)]
        public MISMOValue DigitalSignatureDigestValue { get; set; }
    
        [XmlIgnore]
        public bool DigitalSignatureDigestValueSpecified
        {
            get { return this.DigitalSignatureDigestValue != null; }
            set { }
        }
    
        [XmlElement("OriginatingRecordingRequestIdentifier", Order = 1)]
        public MISMOIdentifier OriginatingRecordingRequestIdentifier { get; set; }
    
        [XmlIgnore]
        public bool OriginatingRecordingRequestIdentifierSpecified
        {
            get { return this.OriginatingRecordingRequestIdentifier != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public ORIGINATING_RECORDING_REQUEST_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
