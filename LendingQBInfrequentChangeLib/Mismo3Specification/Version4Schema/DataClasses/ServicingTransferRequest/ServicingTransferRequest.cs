namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SERVICING_TRANSFER_REQUEST
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DealSetSpecified
                    || this.ServicingTransferRequestDetailSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DEAL_SET", Order = 0)]
        public DEAL_SET DealSet { get; set; }
    
        [XmlIgnore]
        public bool DealSetSpecified
        {
            get { return this.DealSet != null && this.DealSet.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SERVICING_TRANSFER_REQUEST_DETAIL", Order = 1)]
        public SERVICING_TRANSFER_REQUEST_DETAIL ServicingTransferRequestDetail { get; set; }
    
        [XmlIgnore]
        public bool ServicingTransferRequestDetailSpecified
        {
            get { return this.ServicingTransferRequestDetail != null && this.ServicingTransferRequestDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public SERVICING_TRANSFER_REQUEST_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
