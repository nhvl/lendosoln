namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SERVICING_TRANSFER_REQUEST_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ServicingTransferEffectiveDateSpecified
                    || this.TransferRequestProductIdentifierSpecified
                    || this.TransferRequestTypeSpecified
                    || this.TransferRequestTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ServicingTransferEffectiveDate", Order = 0)]
        public MISMODate ServicingTransferEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool ServicingTransferEffectiveDateSpecified
        {
            get { return this.ServicingTransferEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("TransferRequestProductIdentifier", Order = 1)]
        public MISMOIdentifier TransferRequestProductIdentifier { get; set; }
    
        [XmlIgnore]
        public bool TransferRequestProductIdentifierSpecified
        {
            get { return this.TransferRequestProductIdentifier != null; }
            set { }
        }
    
        [XmlElement("TransferRequestType", Order = 2)]
        public MISMOEnum<TransferRequestBase> TransferRequestType { get; set; }
    
        [XmlIgnore]
        public bool TransferRequestTypeSpecified
        {
            get { return this.TransferRequestType != null; }
            set { }
        }
    
        [XmlElement("TransferRequestTypeOtherDescription", Order = 3)]
        public MISMOString TransferRequestTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool TransferRequestTypeOtherDescriptionSpecified
        {
            get { return this.TransferRequestTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public SERVICING_TRANSFER_REQUEST_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
