namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CREDIT_RESPONSE_ALERT_MESSAGE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditRepositorySourceTypeSpecified
                    || this.CreditRepositorySourceTypeOtherDescriptionSpecified
                    || this.CreditResponseAlertMessageAdverseIndicatorSpecified
                    || this.CreditResponseAlertMessageCategoryTypeSpecified
                    || this.CreditResponseAlertMessageCategoryTypeOtherDescriptionSpecified
                    || this.CreditResponseAlertMessageCodeSpecified
                    || this.CreditResponseAlertMessageCodeSourceTypeSpecified
                    || this.CreditResponseAlertMessageCodeSourceTypeOtherDescriptionSpecified
                    || this.CreditResponseAlertMessageTextSpecified
                    || this.CreditResponseAlertMessageTypeSpecified
                    || this.CreditResponseAlertMessageTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CreditRepositorySourceType", Order = 0)]
        public MISMOEnum<CreditRepositorySourceBase> CreditRepositorySourceType { get; set; }
    
        [XmlIgnore]
        public bool CreditRepositorySourceTypeSpecified
        {
            get { return this.CreditRepositorySourceType != null; }
            set { }
        }
    
        [XmlElement("CreditRepositorySourceTypeOtherDescription", Order = 1)]
        public MISMOString CreditRepositorySourceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CreditRepositorySourceTypeOtherDescriptionSpecified
        {
            get { return this.CreditRepositorySourceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("CreditResponseAlertMessageAdverseIndicator", Order = 2)]
        public MISMOIndicator CreditResponseAlertMessageAdverseIndicator { get; set; }
    
        [XmlIgnore]
        public bool CreditResponseAlertMessageAdverseIndicatorSpecified
        {
            get { return this.CreditResponseAlertMessageAdverseIndicator != null; }
            set { }
        }
    
        [XmlElement("CreditResponseAlertMessageCategoryType", Order = 3)]
        public MISMOEnum<CreditResponseAlertMessageCategoryBase> CreditResponseAlertMessageCategoryType { get; set; }
    
        [XmlIgnore]
        public bool CreditResponseAlertMessageCategoryTypeSpecified
        {
            get { return this.CreditResponseAlertMessageCategoryType != null; }
            set { }
        }
    
        [XmlElement("CreditResponseAlertMessageCategoryTypeOtherDescription", Order = 4)]
        public MISMOString CreditResponseAlertMessageCategoryTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CreditResponseAlertMessageCategoryTypeOtherDescriptionSpecified
        {
            get { return this.CreditResponseAlertMessageCategoryTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("CreditResponseAlertMessageCode", Order = 5)]
        public MISMOCode CreditResponseAlertMessageCode { get; set; }
    
        [XmlIgnore]
        public bool CreditResponseAlertMessageCodeSpecified
        {
            get { return this.CreditResponseAlertMessageCode != null; }
            set { }
        }
    
        [XmlElement("CreditResponseAlertMessageCodeSourceType", Order = 6)]
        public MISMOEnum<CreditResponseAlertMessageCodeSourceBase> CreditResponseAlertMessageCodeSourceType { get; set; }
    
        [XmlIgnore]
        public bool CreditResponseAlertMessageCodeSourceTypeSpecified
        {
            get { return this.CreditResponseAlertMessageCodeSourceType != null; }
            set { }
        }
    
        [XmlElement("CreditResponseAlertMessageCodeSourceTypeOtherDescription", Order = 7)]
        public MISMOString CreditResponseAlertMessageCodeSourceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CreditResponseAlertMessageCodeSourceTypeOtherDescriptionSpecified
        {
            get { return this.CreditResponseAlertMessageCodeSourceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("CreditResponseAlertMessageText", Order = 8)]
        public MISMOString CreditResponseAlertMessageText { get; set; }
    
        [XmlIgnore]
        public bool CreditResponseAlertMessageTextSpecified
        {
            get { return this.CreditResponseAlertMessageText != null; }
            set { }
        }
    
        [XmlElement("CreditResponseAlertMessageType", Order = 9)]
        public MISMOEnum<CreditResponseAlertMessageBase> CreditResponseAlertMessageType { get; set; }
    
        [XmlIgnore]
        public bool CreditResponseAlertMessageTypeSpecified
        {
            get { return this.CreditResponseAlertMessageType != null; }
            set { }
        }
    
        [XmlElement("CreditResponseAlertMessageTypeOtherDescription", Order = 10)]
        public MISMOString CreditResponseAlertMessageTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CreditResponseAlertMessageTypeOtherDescriptionSpecified
        {
            get { return this.CreditResponseAlertMessageTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 11)]
        public CREDIT_RESPONSE_ALERT_MESSAGE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
