namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class FLOOD_REQUEST_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BorrowerFloodAcknowledgementLetterIndicatorSpecified
                    || this.FEMAAdditionalLenderDescriptionSpecified
                    || this.FloodCertificationIdentifierSpecified
                    || this.FloodInsuranceAmountSpecified
                    || this.FloodRequestActionTypeSpecified
                    || this.FloodRequestCommentTextSpecified
                    || this.FloodRequestRushIndicatorSpecified
                    || this.FloodTransactionIdentifierSpecified
                    || this.NewServicerAccountIdentifierSpecified
                    || this.OrderingSystemNameSpecified
                    || this.OrderingSystemVersionIdentifierSpecified
                    || this.OriginalFloodDeterminationLoanIdentifierSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BorrowerFloodAcknowledgementLetterIndicator", Order = 0)]
        public MISMOIndicator BorrowerFloodAcknowledgementLetterIndicator { get; set; }
    
        [XmlIgnore]
        public bool BorrowerFloodAcknowledgementLetterIndicatorSpecified
        {
            get { return this.BorrowerFloodAcknowledgementLetterIndicator != null; }
            set { }
        }
    
        [XmlElement("FEMAAdditionalLenderDescription", Order = 1)]
        public MISMOString FEMAAdditionalLenderDescription { get; set; }
    
        [XmlIgnore]
        public bool FEMAAdditionalLenderDescriptionSpecified
        {
            get { return this.FEMAAdditionalLenderDescription != null; }
            set { }
        }
    
        [XmlElement("FloodCertificationIdentifier", Order = 2)]
        public MISMOIdentifier FloodCertificationIdentifier { get; set; }
    
        [XmlIgnore]
        public bool FloodCertificationIdentifierSpecified
        {
            get { return this.FloodCertificationIdentifier != null; }
            set { }
        }
    
        [XmlElement("FloodInsuranceAmount", Order = 3)]
        public MISMOAmount FloodInsuranceAmount { get; set; }
    
        [XmlIgnore]
        public bool FloodInsuranceAmountSpecified
        {
            get { return this.FloodInsuranceAmount != null; }
            set { }
        }
    
        [XmlElement("FloodRequestActionType", Order = 4)]
        public MISMOEnum<FloodRequestActionBase> FloodRequestActionType { get; set; }
    
        [XmlIgnore]
        public bool FloodRequestActionTypeSpecified
        {
            get { return this.FloodRequestActionType != null; }
            set { }
        }
    
        [XmlElement("FloodRequestCommentText", Order = 5)]
        public MISMOString FloodRequestCommentText { get; set; }
    
        [XmlIgnore]
        public bool FloodRequestCommentTextSpecified
        {
            get { return this.FloodRequestCommentText != null; }
            set { }
        }
    
        [XmlElement("FloodRequestRushIndicator", Order = 6)]
        public MISMOIndicator FloodRequestRushIndicator { get; set; }
    
        [XmlIgnore]
        public bool FloodRequestRushIndicatorSpecified
        {
            get { return this.FloodRequestRushIndicator != null; }
            set { }
        }
    
        [XmlElement("FloodTransactionIdentifier", Order = 7)]
        public MISMOIdentifier FloodTransactionIdentifier { get; set; }
    
        [XmlIgnore]
        public bool FloodTransactionIdentifierSpecified
        {
            get { return this.FloodTransactionIdentifier != null; }
            set { }
        }
    
        [XmlElement("NewServicerAccountIdentifier", Order = 8)]
        public MISMOIdentifier NewServicerAccountIdentifier { get; set; }
    
        [XmlIgnore]
        public bool NewServicerAccountIdentifierSpecified
        {
            get { return this.NewServicerAccountIdentifier != null; }
            set { }
        }
    
        [XmlElement("OrderingSystemName", Order = 9)]
        public MISMOString OrderingSystemName { get; set; }
    
        [XmlIgnore]
        public bool OrderingSystemNameSpecified
        {
            get { return this.OrderingSystemName != null; }
            set { }
        }
    
        [XmlElement("OrderingSystemVersionIdentifier", Order = 10)]
        public MISMOIdentifier OrderingSystemVersionIdentifier { get; set; }
    
        [XmlIgnore]
        public bool OrderingSystemVersionIdentifierSpecified
        {
            get { return this.OrderingSystemVersionIdentifier != null; }
            set { }
        }
    
        [XmlElement("OriginalFloodDeterminationLoanIdentifier", Order = 11)]
        public MISMOIdentifier OriginalFloodDeterminationLoanIdentifier { get; set; }
    
        [XmlIgnore]
        public bool OriginalFloodDeterminationLoanIdentifierSpecified
        {
            get { return this.OriginalFloodDeterminationLoanIdentifier != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 12)]
        public FLOOD_REQUEST_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
