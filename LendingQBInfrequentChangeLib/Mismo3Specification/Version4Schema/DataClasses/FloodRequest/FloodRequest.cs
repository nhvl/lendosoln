namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class FLOOD_REQUEST
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.FloodRequestDetailSpecified
                    || this.FloodRequestDisputeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("FLOOD_REQUEST_DETAIL", Order = 0)]
        public FLOOD_REQUEST_DETAIL FloodRequestDetail { get; set; }
    
        [XmlIgnore]
        public bool FloodRequestDetailSpecified
        {
            get { return this.FloodRequestDetail != null && this.FloodRequestDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("FLOOD_REQUEST_DISPUTE", Order = 1)]
        public FLOOD_REQUEST_DISPUTE FloodRequestDispute { get; set; }
    
        [XmlIgnore]
        public bool FloodRequestDisputeSpecified
        {
            get { return this.FloodRequestDispute != null && this.FloodRequestDispute.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public FLOOD_REQUEST_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
