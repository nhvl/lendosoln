namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class TITLE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.TitleRequestSpecified
                    || this.TitleResponseSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("TITLE_REQUEST", Order = 0)]
        public TITLE_REQUEST TitleRequest { get; set; }
    
        [XmlIgnore]
        public bool TitleRequestSpecified
        {
            get { return this.TitleRequest != null && this.TitleRequest.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("TITLE_RESPONSE", Order = 1)]
        public TITLE_RESPONSE TitleResponse { get; set; }
    
        [XmlIgnore]
        public bool TitleResponseSpecified
        {
            get { return this.TitleResponse != null && this.TitleResponse.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public TITLE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
