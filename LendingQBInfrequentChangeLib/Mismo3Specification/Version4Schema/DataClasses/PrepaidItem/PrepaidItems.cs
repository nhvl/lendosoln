namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class PREPAID_ITEMS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PrepaidItemListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PREPAID_ITEM", Order = 0)]
        public List<PREPAID_ITEM> PrepaidItemList { get; set; } = new List<PREPAID_ITEM>();
    
        [XmlIgnore]
        public bool PrepaidItemListSpecified
        {
            get { return this.PrepaidItemList != null && this.PrepaidItemList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public PREPAID_ITEMS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
