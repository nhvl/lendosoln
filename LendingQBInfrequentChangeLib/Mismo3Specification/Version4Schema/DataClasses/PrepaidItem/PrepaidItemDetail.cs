namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PREPAID_ITEM_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BorrowerChosenProviderIndicatorSpecified
                    || this.FeePaidToTypeSpecified
                    || this.FeePaidToTypeOtherDescriptionSpecified
                    || this.IntegratedDisclosureLineNumberValueSpecified
                    || this.IntegratedDisclosureSectionTypeSpecified
                    || this.IntegratedDisclosureSectionTypeOtherDescriptionSpecified
                    || this.PrepaidItemActualTotalAmountSpecified
                    || this.PrepaidItemEstimatedTotalAmountSpecified
                    || this.PrepaidItemMonthlyAmountSpecified
                    || this.PrepaidItemMonthsPaidCountSpecified
                    || this.PrepaidItemNumberOfDaysCountSpecified
                    || this.PrepaidItemPaidFromDateSpecified
                    || this.PrepaidItemPaidThroughDateSpecified
                    || this.PrepaidItemPerDiemAmountSpecified
                    || this.PrepaidItemPerDiemCalculationMethodTypeSpecified
                    || this.PrepaidItemPerDiemCalculationMethodTypeOtherDescriptionSpecified
                    || this.PrepaidItemPerDiemPrintDecimalCountSpecified
                    || this.PrepaidItemPerDiemRoundingDecimalCountSpecified
                    || this.PrepaidItemPerDiemRoundingTypeSpecified
                    || this.PrepaidItemPerDiemRoundingTypeOtherDescriptionSpecified
                    || this.PrepaidItemRefundableAmountSpecified
                    || this.PrepaidItemRefundableConditionsDescriptionSpecified
                    || this.PrepaidItemRefundableIndicatorSpecified
                    || this.PrepaidItemTypeSpecified
                    || this.PrepaidItemTypeOtherDescriptionSpecified
                    || this.RegulationZPointsAndFeesIndicatorSpecified
                    || this.RequiredProviderOfServiceIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BorrowerChosenProviderIndicator", Order = 0)]
        public MISMOIndicator BorrowerChosenProviderIndicator { get; set; }
    
        [XmlIgnore]
        public bool BorrowerChosenProviderIndicatorSpecified
        {
            get { return this.BorrowerChosenProviderIndicator != null; }
            set { }
        }
    
        [XmlElement("FeePaidToType", Order = 1)]
        public MISMOEnum<FeePaidToBase> FeePaidToType { get; set; }
    
        [XmlIgnore]
        public bool FeePaidToTypeSpecified
        {
            get { return this.FeePaidToType != null; }
            set { }
        }
    
        [XmlElement("FeePaidToTypeOtherDescription", Order = 2)]
        public MISMOString FeePaidToTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool FeePaidToTypeOtherDescriptionSpecified
        {
            get { return this.FeePaidToTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureLineNumberValue", Order = 3)]
        public MISMOValue IntegratedDisclosureLineNumberValue { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureLineNumberValueSpecified
        {
            get { return this.IntegratedDisclosureLineNumberValue != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureSectionType", Order = 4)]
        public MISMOEnum<IntegratedDisclosureSectionBase> IntegratedDisclosureSectionType { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureSectionTypeSpecified
        {
            get { return this.IntegratedDisclosureSectionType != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureSectionTypeOtherDescription", Order = 5)]
        public MISMOString IntegratedDisclosureSectionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureSectionTypeOtherDescriptionSpecified
        {
            get { return this.IntegratedDisclosureSectionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PrepaidItemActualTotalAmount", Order = 6)]
        public MISMOAmount PrepaidItemActualTotalAmount { get; set; }
    
        [XmlIgnore]
        public bool PrepaidItemActualTotalAmountSpecified
        {
            get { return this.PrepaidItemActualTotalAmount != null; }
            set { }
        }
    
        [XmlElement("PrepaidItemEstimatedTotalAmount", Order = 7)]
        public MISMOAmount PrepaidItemEstimatedTotalAmount { get; set; }
    
        [XmlIgnore]
        public bool PrepaidItemEstimatedTotalAmountSpecified
        {
            get { return this.PrepaidItemEstimatedTotalAmount != null; }
            set { }
        }
    
        [XmlElement("PrepaidItemMonthlyAmount", Order = 8)]
        public MISMOAmount PrepaidItemMonthlyAmount { get; set; }
    
        [XmlIgnore]
        public bool PrepaidItemMonthlyAmountSpecified
        {
            get { return this.PrepaidItemMonthlyAmount != null; }
            set { }
        }
    
        [XmlElement("PrepaidItemMonthsPaidCount", Order = 9)]
        public MISMOCount PrepaidItemMonthsPaidCount { get; set; }
    
        [XmlIgnore]
        public bool PrepaidItemMonthsPaidCountSpecified
        {
            get { return this.PrepaidItemMonthsPaidCount != null; }
            set { }
        }
    
        [XmlElement("PrepaidItemNumberOfDaysCount", Order = 10)]
        public MISMOCount PrepaidItemNumberOfDaysCount { get; set; }
    
        [XmlIgnore]
        public bool PrepaidItemNumberOfDaysCountSpecified
        {
            get { return this.PrepaidItemNumberOfDaysCount != null; }
            set { }
        }
    
        [XmlElement("PrepaidItemPaidFromDate", Order = 11)]
        public MISMODate PrepaidItemPaidFromDate { get; set; }
    
        [XmlIgnore]
        public bool PrepaidItemPaidFromDateSpecified
        {
            get { return this.PrepaidItemPaidFromDate != null; }
            set { }
        }
    
        [XmlElement("PrepaidItemPaidThroughDate", Order = 12)]
        public MISMODate PrepaidItemPaidThroughDate { get; set; }
    
        [XmlIgnore]
        public bool PrepaidItemPaidThroughDateSpecified
        {
            get { return this.PrepaidItemPaidThroughDate != null; }
            set { }
        }
    
        [XmlElement("PrepaidItemPerDiemAmount", Order = 13)]
        public MISMOAmount PrepaidItemPerDiemAmount { get; set; }
    
        [XmlIgnore]
        public bool PrepaidItemPerDiemAmountSpecified
        {
            get { return this.PrepaidItemPerDiemAmount != null; }
            set { }
        }
    
        [XmlElement("PrepaidItemPerDiemCalculationMethodType", Order = 14)]
        public MISMOEnum<PrepaidItemPerDiemCalculationMethodBase> PrepaidItemPerDiemCalculationMethodType { get; set; }
    
        [XmlIgnore]
        public bool PrepaidItemPerDiemCalculationMethodTypeSpecified
        {
            get { return this.PrepaidItemPerDiemCalculationMethodType != null; }
            set { }
        }
    
        [XmlElement("PrepaidItemPerDiemCalculationMethodTypeOtherDescription", Order = 15)]
        public MISMOString PrepaidItemPerDiemCalculationMethodTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PrepaidItemPerDiemCalculationMethodTypeOtherDescriptionSpecified
        {
            get { return this.PrepaidItemPerDiemCalculationMethodTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PrepaidItemPerDiemPrintDecimalCount", Order = 16)]
        public MISMOCount PrepaidItemPerDiemPrintDecimalCount { get; set; }
    
        [XmlIgnore]
        public bool PrepaidItemPerDiemPrintDecimalCountSpecified
        {
            get { return this.PrepaidItemPerDiemPrintDecimalCount != null; }
            set { }
        }
    
        [XmlElement("PrepaidItemPerDiemRoundingDecimalCount", Order = 17)]
        public MISMOCount PrepaidItemPerDiemRoundingDecimalCount { get; set; }
    
        [XmlIgnore]
        public bool PrepaidItemPerDiemRoundingDecimalCountSpecified
        {
            get { return this.PrepaidItemPerDiemRoundingDecimalCount != null; }
            set { }
        }
    
        [XmlElement("PrepaidItemPerDiemRoundingType", Order = 18)]
        public MISMOEnum<PrepaidItemPerDiemRoundingBase> PrepaidItemPerDiemRoundingType { get; set; }
    
        [XmlIgnore]
        public bool PrepaidItemPerDiemRoundingTypeSpecified
        {
            get { return this.PrepaidItemPerDiemRoundingType != null; }
            set { }
        }
    
        [XmlElement("PrepaidItemPerDiemRoundingTypeOtherDescription", Order = 19)]
        public MISMOString PrepaidItemPerDiemRoundingTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PrepaidItemPerDiemRoundingTypeOtherDescriptionSpecified
        {
            get { return this.PrepaidItemPerDiemRoundingTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PrepaidItemRefundableAmount", Order = 20)]
        public MISMOAmount PrepaidItemRefundableAmount { get; set; }
    
        [XmlIgnore]
        public bool PrepaidItemRefundableAmountSpecified
        {
            get { return this.PrepaidItemRefundableAmount != null; }
            set { }
        }
    
        [XmlElement("PrepaidItemRefundableConditionsDescription", Order = 21)]
        public MISMOString PrepaidItemRefundableConditionsDescription { get; set; }
    
        [XmlIgnore]
        public bool PrepaidItemRefundableConditionsDescriptionSpecified
        {
            get { return this.PrepaidItemRefundableConditionsDescription != null; }
            set { }
        }
    
        [XmlElement("PrepaidItemRefundableIndicator", Order = 22)]
        public MISMOIndicator PrepaidItemRefundableIndicator { get; set; }
    
        [XmlIgnore]
        public bool PrepaidItemRefundableIndicatorSpecified
        {
            get { return this.PrepaidItemRefundableIndicator != null; }
            set { }
        }
    
        [XmlElement("PrepaidItemType", Order = 23)]
        public MISMOEnum<PrepaidItemBase> PrepaidItemType { get; set; }
    
        [XmlIgnore]
        public bool PrepaidItemTypeSpecified
        {
            get { return this.PrepaidItemType != null; }
            set { }
        }
    
        [XmlElement("PrepaidItemTypeOtherDescription", Order = 24)]
        public MISMOString PrepaidItemTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PrepaidItemTypeOtherDescriptionSpecified
        {
            get { return this.PrepaidItemTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("RegulationZPointsAndFeesIndicator", Order = 25)]
        public MISMOIndicator RegulationZPointsAndFeesIndicator { get; set; }
    
        [XmlIgnore]
        public bool RegulationZPointsAndFeesIndicatorSpecified
        {
            get { return this.RegulationZPointsAndFeesIndicator != null; }
            set { }
        }
    
        [XmlElement("RequiredProviderOfServiceIndicator", Order = 26)]
        public MISMOIndicator RequiredProviderOfServiceIndicator { get; set; }
    
        [XmlIgnore]
        public bool RequiredProviderOfServiceIndicatorSpecified
        {
            get { return this.RequiredProviderOfServiceIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 27)]
        public PREPAID_ITEM_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
