namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PREPAID_ITEM
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PrepaidItemDetailSpecified
                    || this.PrepaidItemPaidToSpecified
                    || this.PrepaidItemPaymentsSpecified
                    || this.SelectedServiceProviderSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PREPAID_ITEM_DETAIL", Order = 0)]
        public PREPAID_ITEM_DETAIL PrepaidItemDetail { get; set; }
    
        [XmlIgnore]
        public bool PrepaidItemDetailSpecified
        {
            get { return this.PrepaidItemDetail != null && this.PrepaidItemDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PREPAID_ITEM_PAID_TO", Order = 1)]
        public PAID_TO PrepaidItemPaidTo { get; set; }
    
        [XmlIgnore]
        public bool PrepaidItemPaidToSpecified
        {
            get { return this.PrepaidItemPaidTo != null; }
            set { }
        }
    
        [XmlElement("PREPAID_ITEM_PAYMENTS", Order = 2)]
        public PREPAID_ITEM_PAYMENTS PrepaidItemPayments { get; set; }
    
        [XmlIgnore]
        public bool PrepaidItemPaymentsSpecified
        {
            get { return this.PrepaidItemPayments != null && this.PrepaidItemPayments.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SELECTED_SERVICE_PROVIDER", Order = 3)]
        public SELECTED_SERVICE_PROVIDER SelectedServiceProvider { get; set; }
    
        [XmlIgnore]
        public bool SelectedServiceProviderSpecified
        {
            get { return this.SelectedServiceProvider != null && this.SelectedServiceProvider.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public PREPAID_ITEM_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
