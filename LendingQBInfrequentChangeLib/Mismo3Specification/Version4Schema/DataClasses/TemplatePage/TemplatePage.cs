namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class TEMPLATE_PAGE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.TemplatePageFilesSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("TEMPLATE_PAGE_FILES", Order = 0)]
        public TEMPLATE_PAGE_FILES TemplatePageFiles { get; set; }
    
        [XmlIgnore]
        public bool TemplatePageFilesSpecified
        {
            get { return this.TemplatePageFiles != null && this.TemplatePageFiles.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public TEMPLATE_PAGE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
