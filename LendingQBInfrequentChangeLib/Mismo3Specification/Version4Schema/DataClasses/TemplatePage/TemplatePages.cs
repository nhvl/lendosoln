namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class TEMPLATE_PAGES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.TemplatePageListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("TEMPLATE_PAGE", Order = 0)]
        public List<TEMPLATE_PAGE> TemplatePageList { get; set; } = new List<TEMPLATE_PAGE>();
    
        [XmlIgnore]
        public bool TemplatePageListSpecified
        {
            get { return this.TemplatePageList != null && this.TemplatePageList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public TEMPLATE_PAGES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
