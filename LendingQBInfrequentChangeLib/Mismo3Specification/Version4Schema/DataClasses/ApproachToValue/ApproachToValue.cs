namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class APPROACH_TO_VALUE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CostApproachSpecified
                    || this.IncomeApproachSpecified
                    || this.SalesComparisonApproachSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("COST_APPROACH", Order = 0)]
        public COST_APPROACH CostApproach { get; set; }
    
        [XmlIgnore]
        public bool CostApproachSpecified
        {
            get { return this.CostApproach != null && this.CostApproach.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("INCOME_APPROACH", Order = 1)]
        public INCOME_APPROACH IncomeApproach { get; set; }
    
        [XmlIgnore]
        public bool IncomeApproachSpecified
        {
            get { return this.IncomeApproach != null && this.IncomeApproach.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SALES_COMPARISON_APPROACH", Order = 2)]
        public SALES_COMPARISON_APPROACH SalesComparisonApproach { get; set; }
    
        [XmlIgnore]
        public bool SalesComparisonApproachSpecified
        {
            get { return this.SalesComparisonApproach != null && this.SalesComparisonApproach.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public APPROACH_TO_VALUE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
