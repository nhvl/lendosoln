namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class REPORTING_INFORMATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ReportingInformationIdentifierSpecified
                    || this.ReportingInformationNameSpecified
                    || this.ReportingPeriodEndDateSpecified
                    || this.ReportingPeriodStartDateSpecified
                    || this.TotalReportingEntriesCountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ReportingInformationIdentifier", Order = 0)]
        public MISMOIdentifier ReportingInformationIdentifier { get; set; }
    
        [XmlIgnore]
        public bool ReportingInformationIdentifierSpecified
        {
            get { return this.ReportingInformationIdentifier != null; }
            set { }
        }
    
        [XmlElement("ReportingInformationName", Order = 1)]
        public MISMOString ReportingInformationName { get; set; }
    
        [XmlIgnore]
        public bool ReportingInformationNameSpecified
        {
            get { return this.ReportingInformationName != null; }
            set { }
        }
    
        [XmlElement("ReportingPeriodEndDate", Order = 2)]
        public MISMODate ReportingPeriodEndDate { get; set; }
    
        [XmlIgnore]
        public bool ReportingPeriodEndDateSpecified
        {
            get { return this.ReportingPeriodEndDate != null; }
            set { }
        }
    
        [XmlElement("ReportingPeriodStartDate", Order = 3)]
        public MISMODate ReportingPeriodStartDate { get; set; }
    
        [XmlIgnore]
        public bool ReportingPeriodStartDateSpecified
        {
            get { return this.ReportingPeriodStartDate != null; }
            set { }
        }
    
        [XmlElement("TotalReportingEntriesCount", Order = 4)]
        public MISMOCount TotalReportingEntriesCount { get; set; }
    
        [XmlIgnore]
        public bool TotalReportingEntriesCountSpecified
        {
            get { return this.TotalReportingEntriesCount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public REPORTING_INFORMATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
