namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class OTHER_CONTACT_POINT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ContactPointOtherValueSpecified
                    || this.ContactPointOtherValueDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ContactPointOtherValue", Order = 0)]
        public MISMOValue ContactPointOtherValue { get; set; }
    
        [XmlIgnore]
        public bool ContactPointOtherValueSpecified
        {
            get { return this.ContactPointOtherValue != null; }
            set { }
        }
    
        [XmlElement("ContactPointOtherValueDescription", Order = 1)]
        public MISMOString ContactPointOtherValueDescription { get; set; }
    
        [XmlIgnore]
        public bool ContactPointOtherValueDescriptionSpecified
        {
            get { return this.ContactPointOtherValueDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public OTHER_CONTACT_POINT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
