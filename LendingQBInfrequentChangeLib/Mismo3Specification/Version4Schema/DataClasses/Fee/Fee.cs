namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class FEE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.FeeDetailSpecified
                    || this.FeePaidToSpecified
                    || this.FeePaymentsSpecified
                    || this.SelectedServiceProviderSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("FEE_DETAIL", Order = 0)]
        public FEE_DETAIL FeeDetail { get; set; }
    
        [XmlIgnore]
        public bool FeeDetailSpecified
        {
            get { return this.FeeDetail != null && this.FeeDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("FEE_PAID_TO", Order = 1)]
        public PAID_TO FeePaidTo { get; set; }
    
        [XmlIgnore]
        public bool FeePaidToSpecified
        {
            get { return this.FeePaidTo != null; }
            set { }
        }
    
        [XmlElement("FEE_PAYMENTS", Order = 2)]
        public FEE_PAYMENTS FeePayments { get; set; }
    
        [XmlIgnore]
        public bool FeePaymentsSpecified
        {
            get { return this.FeePayments != null && this.FeePayments.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SELECTED_SERVICE_PROVIDER", Order = 3)]
        public SELECTED_SERVICE_PROVIDER SelectedServiceProvider { get; set; }
    
        [XmlIgnore]
        public bool SelectedServiceProviderSpecified
        {
            get { return this.SelectedServiceProvider != null && this.SelectedServiceProvider.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public FEE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
