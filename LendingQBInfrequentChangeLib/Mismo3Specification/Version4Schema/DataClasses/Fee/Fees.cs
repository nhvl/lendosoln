namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class FEES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.FeeListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("FEE", Order = 0)]
        public List<FEE> FeeList { get; set; } = new List<FEE>();
    
        [XmlIgnore]
        public bool FeeListSpecified
        {
            get { return this.FeeList != null && this.FeeList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public FEES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
