namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class FEE_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BorrowerChosenProviderIndicatorSpecified
                    || this.FeeActualTotalAmountSpecified
                    || this.FeeDescriptionSpecified
                    || this.FeeEstimatedTotalAmountSpecified
                    || this.FeePaidToTypeSpecified
                    || this.FeePaidToTypeOtherDescriptionSpecified
                    || this.FeePercentBasisTypeSpecified
                    || this.FeePercentBasisTypeOtherDescriptionSpecified
                    || this.FeeSpecifiedFixedAmountSpecified
                    || this.FeeSpecifiedHUD1LineNumberValueSpecified
                    || this.FeeSpecifiedLineNumberValueSpecified
                    || this.FeeTotalPercentSpecified
                    || this.FeeTypeSpecified
                    || this.FeeTypeOtherDescriptionSpecified
                    || this.GFEAggregationTypeSpecified
                    || this.GFEAggregationTypeOtherDescriptionSpecified
                    || this.GFEDisclosedFeeAmountSpecified
                    || this.IntegratedDisclosureLineNumberValueSpecified
                    || this.IntegratedDisclosureSectionTypeSpecified
                    || this.IntegratedDisclosureSectionTypeOtherDescriptionSpecified
                    || this.IntegratedDisclosureSubsectionTypeSpecified
                    || this.IntegratedDisclosureSubsectionTypeOtherDescriptionSpecified
                    || this.OptionalCostIndicatorSpecified
                    || this.RegulationZPointsAndFeesIndicatorSpecified
                    || this.RequiredProviderOfServiceIndicatorSpecified
                    || this.SectionClassificationTypeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BorrowerChosenProviderIndicator", Order = 0)]
        public MISMOIndicator BorrowerChosenProviderIndicator { get; set; }
    
        [XmlIgnore]
        public bool BorrowerChosenProviderIndicatorSpecified
        {
            get { return this.BorrowerChosenProviderIndicator != null; }
            set { }
        }
    
        [XmlElement("FeeActualTotalAmount", Order = 1)]
        public MISMOAmount FeeActualTotalAmount { get; set; }
    
        [XmlIgnore]
        public bool FeeActualTotalAmountSpecified
        {
            get { return this.FeeActualTotalAmount != null; }
            set { }
        }
    
        [XmlElement("FeeDescription", Order = 2)]
        public MISMOString FeeDescription { get; set; }
    
        [XmlIgnore]
        public bool FeeDescriptionSpecified
        {
            get { return this.FeeDescription != null; }
            set { }
        }
    
        [XmlElement("FeeEstimatedTotalAmount", Order = 3)]
        public MISMOAmount FeeEstimatedTotalAmount { get; set; }
    
        [XmlIgnore]
        public bool FeeEstimatedTotalAmountSpecified
        {
            get { return this.FeeEstimatedTotalAmount != null; }
            set { }
        }
    
        [XmlElement("FeePaidToType", Order = 4)]
        public MISMOEnum<FeePaidToBase> FeePaidToType { get; set; }
    
        [XmlIgnore]
        public bool FeePaidToTypeSpecified
        {
            get { return this.FeePaidToType != null; }
            set { }
        }
    
        [XmlElement("FeePaidToTypeOtherDescription", Order = 5)]
        public MISMOString FeePaidToTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool FeePaidToTypeOtherDescriptionSpecified
        {
            get { return this.FeePaidToTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("FeePercentBasisType", Order = 6)]
        public MISMOEnum<FeePercentBasisBase> FeePercentBasisType { get; set; }
    
        [XmlIgnore]
        public bool FeePercentBasisTypeSpecified
        {
            get { return this.FeePercentBasisType != null; }
            set { }
        }
    
        [XmlElement("FeePercentBasisTypeOtherDescription", Order = 7)]
        public MISMOString FeePercentBasisTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool FeePercentBasisTypeOtherDescriptionSpecified
        {
            get { return this.FeePercentBasisTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("FeeSpecifiedFixedAmount", Order = 8)]
        public MISMOAmount FeeSpecifiedFixedAmount { get; set; }
    
        [XmlIgnore]
        public bool FeeSpecifiedFixedAmountSpecified
        {
            get { return this.FeeSpecifiedFixedAmount != null; }
            set { }
        }
    
        [XmlElement("FeeSpecifiedHUD1LineNumberValue", Order = 9)]
        public MISMOValue FeeSpecifiedHUD1LineNumberValue { get; set; }
    
        [XmlIgnore]
        public bool FeeSpecifiedHUD1LineNumberValueSpecified
        {
            get { return this.FeeSpecifiedHUD1LineNumberValue != null; }
            set { }
        }
    
        [XmlElement("FeeSpecifiedLineNumberValue", Order = 10)]
        public MISMOString FeeSpecifiedLineNumberValue { get; set; }
    
        [XmlIgnore]
        public bool FeeSpecifiedLineNumberValueSpecified
        {
            get { return this.FeeSpecifiedLineNumberValue != null; }
            set { }
        }
    
        [XmlElement("FeeTotalPercent", Order = 11)]
        public MISMOPercent FeeTotalPercent { get; set; }
    
        [XmlIgnore]
        public bool FeeTotalPercentSpecified
        {
            get { return this.FeeTotalPercent != null; }
            set { }
        }
    
        [XmlElement("FeeType", Order = 12)]
        public MISMOEnum<FeeBase> FeeType { get; set; }
    
        [XmlIgnore]
        public bool FeeTypeSpecified
        {
            get { return this.FeeType != null; }
            set { }
        }
    
        [XmlElement("FeeTypeOtherDescription", Order = 13)]
        public MISMOString FeeTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool FeeTypeOtherDescriptionSpecified
        {
            get { return this.FeeTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("GFEAggregationType", Order = 14)]
        public MISMOEnum<GFEAggregationBase> GFEAggregationType { get; set; }
    
        [XmlIgnore]
        public bool GFEAggregationTypeSpecified
        {
            get { return this.GFEAggregationType != null; }
            set { }
        }
    
        [XmlElement("GFEAggregationTypeOtherDescription", Order = 15)]
        public MISMOString GFEAggregationTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool GFEAggregationTypeOtherDescriptionSpecified
        {
            get { return this.GFEAggregationTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("GFEDisclosedFeeAmount", Order = 16)]
        public MISMOAmount GFEDisclosedFeeAmount { get; set; }
    
        [XmlIgnore]
        public bool GFEDisclosedFeeAmountSpecified
        {
            get { return this.GFEDisclosedFeeAmount != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureLineNumberValue", Order = 17)]
        public MISMOValue IntegratedDisclosureLineNumberValue { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureLineNumberValueSpecified
        {
            get { return this.IntegratedDisclosureLineNumberValue != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureSectionType", Order = 18)]
        public MISMOEnum<IntegratedDisclosureSectionBase> IntegratedDisclosureSectionType { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureSectionTypeSpecified
        {
            get { return this.IntegratedDisclosureSectionType != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureSectionTypeOtherDescription", Order = 19)]
        public MISMOString IntegratedDisclosureSectionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureSectionTypeOtherDescriptionSpecified
        {
            get { return this.IntegratedDisclosureSectionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureSubsectionType", Order = 20)]
        public MISMOEnum<IntegratedDisclosureSubsectionBase> IntegratedDisclosureSubsectionType { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureSubsectionTypeSpecified
        {
            get { return this.IntegratedDisclosureSubsectionType != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureSubsectionTypeOtherDescription", Order = 21)]
        public MISMOString IntegratedDisclosureSubsectionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureSubsectionTypeOtherDescriptionSpecified
        {
            get { return this.IntegratedDisclosureSubsectionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("OptionalCostIndicator", Order = 22)]
        public MISMOIndicator OptionalCostIndicator { get; set; }
    
        [XmlIgnore]
        public bool OptionalCostIndicatorSpecified
        {
            get { return this.OptionalCostIndicator != null; }
            set { }
        }
    
        [XmlElement("RegulationZPointsAndFeesIndicator", Order = 23)]
        public MISMOIndicator RegulationZPointsAndFeesIndicator { get; set; }
    
        [XmlIgnore]
        public bool RegulationZPointsAndFeesIndicatorSpecified
        {
            get { return this.RegulationZPointsAndFeesIndicator != null; }
            set { }
        }
    
        [XmlElement("RequiredProviderOfServiceIndicator", Order = 24)]
        public MISMOIndicator RequiredProviderOfServiceIndicator { get; set; }
    
        [XmlIgnore]
        public bool RequiredProviderOfServiceIndicatorSpecified
        {
            get { return this.RequiredProviderOfServiceIndicator != null; }
            set { }
        }
    
        [XmlElement("SectionClassificationType", Order = 25)]
        public MISMOEnum<SectionClassificationBase> SectionClassificationType { get; set; }
    
        [XmlIgnore]
        public bool SectionClassificationTypeSpecified
        {
            get { return this.SectionClassificationType != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 26)]
        public FEE_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
