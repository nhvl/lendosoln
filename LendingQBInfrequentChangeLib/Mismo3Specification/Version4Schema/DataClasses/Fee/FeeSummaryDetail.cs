namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class FEE_SUMMARY_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.APRPercentSpecified
                    || this.FeeSummaryApproximateCostOfFundsPercentSpecified
                    || this.FeeSummaryDisclosedTotalSalesPriceAmountSpecified
                    || this.FeeSummaryTotalAmountFinancedAmountSpecified
                    || this.FeeSummaryTotalAPRFeesAmountSpecified
                    || this.FeeSummaryTotalDepositedReservesAmountSpecified
                    || this.FeeSummaryTotalFeesAmountSpecified
                    || this.FeeSummaryTotalFilingRecordingFeeAmountSpecified
                    || this.FeeSummaryTotalFinanceChargeAmountSpecified
                    || this.FeeSummaryTotalInterestPercentSpecified
                    || this.FeeSummaryTotalLoanOriginationFeesAmountSpecified
                    || this.FeeSummaryTotalNetBorrowerFeesAmountSpecified
                    || this.FeeSummaryTotalNetProceedsForFundingAmountSpecified
                    || this.FeeSummaryTotalNetSellerFeesAmountSpecified
                    || this.FeeSummaryTotalNonAPRFeesAmountSpecified
                    || this.FeeSummaryTotalOfAllPaymentsAmountSpecified
                    || this.FeeSummaryTotalPaidToOthersAmountSpecified
                    || this.FeeSummaryTotalPrepaidFinanceChargeAmountSpecified
                    || this.HOEPA_APRPercentSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("APRPercent", Order = 0)]
        public MISMOPercent APRPercent { get; set; }
    
        [XmlIgnore]
        public bool APRPercentSpecified
        {
            get { return this.APRPercent != null; }
            set { }
        }
    
        [XmlElement("FeeSummaryApproximateCostOfFundsPercent", Order = 1)]
        public MISMOPercent FeeSummaryApproximateCostOfFundsPercent { get; set; }
    
        [XmlIgnore]
        public bool FeeSummaryApproximateCostOfFundsPercentSpecified
        {
            get { return this.FeeSummaryApproximateCostOfFundsPercent != null; }
            set { }
        }
    
        [XmlElement("FeeSummaryDisclosedTotalSalesPriceAmount", Order = 2)]
        public MISMOAmount FeeSummaryDisclosedTotalSalesPriceAmount { get; set; }
    
        [XmlIgnore]
        public bool FeeSummaryDisclosedTotalSalesPriceAmountSpecified
        {
            get { return this.FeeSummaryDisclosedTotalSalesPriceAmount != null; }
            set { }
        }
    
        [XmlElement("FeeSummaryTotalAmountFinancedAmount", Order = 3)]
        public MISMOAmount FeeSummaryTotalAmountFinancedAmount { get; set; }
    
        [XmlIgnore]
        public bool FeeSummaryTotalAmountFinancedAmountSpecified
        {
            get { return this.FeeSummaryTotalAmountFinancedAmount != null; }
            set { }
        }
    
        [XmlElement("FeeSummaryTotalAPRFeesAmount", Order = 4)]
        public MISMOAmount FeeSummaryTotalAPRFeesAmount { get; set; }
    
        [XmlIgnore]
        public bool FeeSummaryTotalAPRFeesAmountSpecified
        {
            get { return this.FeeSummaryTotalAPRFeesAmount != null; }
            set { }
        }
    
        [XmlElement("FeeSummaryTotalDepositedReservesAmount", Order = 5)]
        public MISMOAmount FeeSummaryTotalDepositedReservesAmount { get; set; }
    
        [XmlIgnore]
        public bool FeeSummaryTotalDepositedReservesAmountSpecified
        {
            get { return this.FeeSummaryTotalDepositedReservesAmount != null; }
            set { }
        }
    
        [XmlElement("FeeSummaryTotalFeesAmount", Order = 6)]
        public MISMOAmount FeeSummaryTotalFeesAmount { get; set; }
    
        [XmlIgnore]
        public bool FeeSummaryTotalFeesAmountSpecified
        {
            get { return this.FeeSummaryTotalFeesAmount != null; }
            set { }
        }
    
        [XmlElement("FeeSummaryTotalFilingRecordingFeeAmount", Order = 7)]
        public MISMOAmount FeeSummaryTotalFilingRecordingFeeAmount { get; set; }
    
        [XmlIgnore]
        public bool FeeSummaryTotalFilingRecordingFeeAmountSpecified
        {
            get { return this.FeeSummaryTotalFilingRecordingFeeAmount != null; }
            set { }
        }
    
        [XmlElement("FeeSummaryTotalFinanceChargeAmount", Order = 8)]
        public MISMOAmount FeeSummaryTotalFinanceChargeAmount { get; set; }
    
        [XmlIgnore]
        public bool FeeSummaryTotalFinanceChargeAmountSpecified
        {
            get { return this.FeeSummaryTotalFinanceChargeAmount != null; }
            set { }
        }
    
        [XmlElement("FeeSummaryTotalInterestPercent", Order = 9)]
        public MISMOPercent FeeSummaryTotalInterestPercent { get; set; }
    
        [XmlIgnore]
        public bool FeeSummaryTotalInterestPercentSpecified
        {
            get { return this.FeeSummaryTotalInterestPercent != null; }
            set { }
        }
    
        [XmlElement("FeeSummaryTotalLoanOriginationFeesAmount", Order = 10)]
        public MISMOAmount FeeSummaryTotalLoanOriginationFeesAmount { get; set; }
    
        [XmlIgnore]
        public bool FeeSummaryTotalLoanOriginationFeesAmountSpecified
        {
            get { return this.FeeSummaryTotalLoanOriginationFeesAmount != null; }
            set { }
        }
    
        [XmlElement("FeeSummaryTotalNetBorrowerFeesAmount", Order = 11)]
        public MISMOAmount FeeSummaryTotalNetBorrowerFeesAmount { get; set; }
    
        [XmlIgnore]
        public bool FeeSummaryTotalNetBorrowerFeesAmountSpecified
        {
            get { return this.FeeSummaryTotalNetBorrowerFeesAmount != null; }
            set { }
        }
    
        [XmlElement("FeeSummaryTotalNetProceedsForFundingAmount", Order = 12)]
        public MISMOAmount FeeSummaryTotalNetProceedsForFundingAmount { get; set; }
    
        [XmlIgnore]
        public bool FeeSummaryTotalNetProceedsForFundingAmountSpecified
        {
            get { return this.FeeSummaryTotalNetProceedsForFundingAmount != null; }
            set { }
        }
    
        [XmlElement("FeeSummaryTotalNetSellerFeesAmount", Order = 13)]
        public MISMOAmount FeeSummaryTotalNetSellerFeesAmount { get; set; }
    
        [XmlIgnore]
        public bool FeeSummaryTotalNetSellerFeesAmountSpecified
        {
            get { return this.FeeSummaryTotalNetSellerFeesAmount != null; }
            set { }
        }
    
        [XmlElement("FeeSummaryTotalNonAPRFeesAmount", Order = 14)]
        public MISMOAmount FeeSummaryTotalNonAPRFeesAmount { get; set; }
    
        [XmlIgnore]
        public bool FeeSummaryTotalNonAPRFeesAmountSpecified
        {
            get { return this.FeeSummaryTotalNonAPRFeesAmount != null; }
            set { }
        }
    
        [XmlElement("FeeSummaryTotalOfAllPaymentsAmount", Order = 15)]
        public MISMOAmount FeeSummaryTotalOfAllPaymentsAmount { get; set; }
    
        [XmlIgnore]
        public bool FeeSummaryTotalOfAllPaymentsAmountSpecified
        {
            get { return this.FeeSummaryTotalOfAllPaymentsAmount != null; }
            set { }
        }
    
        [XmlElement("FeeSummaryTotalPaidToOthersAmount", Order = 16)]
        public MISMOAmount FeeSummaryTotalPaidToOthersAmount { get; set; }
    
        [XmlIgnore]
        public bool FeeSummaryTotalPaidToOthersAmountSpecified
        {
            get { return this.FeeSummaryTotalPaidToOthersAmount != null; }
            set { }
        }
    
        [XmlElement("FeeSummaryTotalPrepaidFinanceChargeAmount", Order = 17)]
        public MISMOAmount FeeSummaryTotalPrepaidFinanceChargeAmount { get; set; }
    
        [XmlIgnore]
        public bool FeeSummaryTotalPrepaidFinanceChargeAmountSpecified
        {
            get { return this.FeeSummaryTotalPrepaidFinanceChargeAmount != null; }
            set { }
        }
    
        [XmlElement("HOEPA_APRPercent", Order = 18)]
        public MISMOPercent HOEPA_APRPercent { get; set; }
    
        [XmlIgnore]
        public bool HOEPA_APRPercentSpecified
        {
            get { return this.HOEPA_APRPercent != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 19)]
        public FEE_SUMMARY_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
