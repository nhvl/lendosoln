namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class FEES_SUMMARY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.FeeSummaryDetailSpecified
                    || this.FeeSummaryTotalFeesPaidBysSpecified
                    || this.FeeSummaryTotalFeesPaidTosSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("FEE_SUMMARY_DETAIL", Order = 0)]
        public FEE_SUMMARY_DETAIL FeeSummaryDetail { get; set; }
    
        [XmlIgnore]
        public bool FeeSummaryDetailSpecified
        {
            get { return this.FeeSummaryDetail != null && this.FeeSummaryDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("FEE_SUMMARY_TOTAL_FEES_PAID_BYS", Order = 1)]
        public FEE_SUMMARY_TOTAL_FEES_PAID_BYS FeeSummaryTotalFeesPaidBys { get; set; }
    
        [XmlIgnore]
        public bool FeeSummaryTotalFeesPaidBysSpecified
        {
            get { return this.FeeSummaryTotalFeesPaidBys != null && this.FeeSummaryTotalFeesPaidBys.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("FEE_SUMMARY_TOTAL_FEES_PAID_TOS", Order = 2)]
        public FEE_SUMMARY_TOTAL_FEES_PAID_TOS FeeSummaryTotalFeesPaidTos { get; set; }
    
        [XmlIgnore]
        public bool FeeSummaryTotalFeesPaidTosSpecified
        {
            get { return this.FeeSummaryTotalFeesPaidTos != null && this.FeeSummaryTotalFeesPaidTos.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public FEES_SUMMARY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
