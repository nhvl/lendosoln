namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MULTIFAMILY_RENTAL_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DataSourceDescriptionSpecified
                    || this.GrossBuildingAreaSquareFeetNumberSpecified
                    || this.MonthlyRentAmountSpecified
                    || this.RentalDataAnalysisCommentDescriptionSpecified
                    || this.RentControlStatusTypeSpecified
                    || this.RentPerGrossBuildingAreaAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DataSourceDescription", Order = 0)]
        public MISMOString DataSourceDescription { get; set; }
    
        [XmlIgnore]
        public bool DataSourceDescriptionSpecified
        {
            get { return this.DataSourceDescription != null; }
            set { }
        }
    
        [XmlElement("GrossBuildingAreaSquareFeetNumber", Order = 1)]
        public MISMONumeric GrossBuildingAreaSquareFeetNumber { get; set; }
    
        [XmlIgnore]
        public bool GrossBuildingAreaSquareFeetNumberSpecified
        {
            get { return this.GrossBuildingAreaSquareFeetNumber != null; }
            set { }
        }
    
        [XmlElement("MonthlyRentAmount", Order = 2)]
        public MISMOAmount MonthlyRentAmount { get; set; }
    
        [XmlIgnore]
        public bool MonthlyRentAmountSpecified
        {
            get { return this.MonthlyRentAmount != null; }
            set { }
        }
    
        [XmlElement("RentalDataAnalysisCommentDescription", Order = 3)]
        public MISMOString RentalDataAnalysisCommentDescription { get; set; }
    
        [XmlIgnore]
        public bool RentalDataAnalysisCommentDescriptionSpecified
        {
            get { return this.RentalDataAnalysisCommentDescription != null; }
            set { }
        }
    
        [XmlElement("RentControlStatusType", Order = 4)]
        public MISMOEnum<RentControlStatusBase> RentControlStatusType { get; set; }
    
        [XmlIgnore]
        public bool RentControlStatusTypeSpecified
        {
            get { return this.RentControlStatusType != null; }
            set { }
        }
    
        [XmlElement("RentPerGrossBuildingAreaAmount", Order = 5)]
        public MISMOAmount RentPerGrossBuildingAreaAmount { get; set; }
    
        [XmlIgnore]
        public bool RentPerGrossBuildingAreaAmountSpecified
        {
            get { return this.RentPerGrossBuildingAreaAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 6)]
        public MULTIFAMILY_RENTAL_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
