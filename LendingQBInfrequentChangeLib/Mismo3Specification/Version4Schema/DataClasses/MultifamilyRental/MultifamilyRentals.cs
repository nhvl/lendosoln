namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class MULTIFAMILY_RENTALS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MultifamilyRentalListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MULTIFAMILY_RENTAL", Order = 0)]
        public List<MULTIFAMILY_RENTAL> MultifamilyRentalList { get; set; } = new List<MULTIFAMILY_RENTAL>();
    
        [XmlIgnore]
        public bool MultifamilyRentalListSpecified
        {
            get { return this.MultifamilyRentalList != null && this.MultifamilyRentalList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public MULTIFAMILY_RENTALS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
