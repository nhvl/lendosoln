namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MULTIFAMILY_RENTAL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MultifamilyRentalDetailSpecified
                    || this.RentIncludesUtilitiesSpecified
                    || this.RentalFeaturesSpecified
                    || this.RentalUnitsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MULTIFAMILY_RENTAL_DETAIL", Order = 0)]
        public MULTIFAMILY_RENTAL_DETAIL MultifamilyRentalDetail { get; set; }
    
        [XmlIgnore]
        public bool MultifamilyRentalDetailSpecified
        {
            get { return this.MultifamilyRentalDetail != null && this.MultifamilyRentalDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("RENT_INCLUDES_UTILITIES", Order = 1)]
        public RENT_INCLUDES_UTILITIES RentIncludesUtilities { get; set; }
    
        [XmlIgnore]
        public bool RentIncludesUtilitiesSpecified
        {
            get { return this.RentIncludesUtilities != null && this.RentIncludesUtilities.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("RENTAL_FEATURES", Order = 2)]
        public RENTAL_FEATURES RentalFeatures { get; set; }
    
        [XmlIgnore]
        public bool RentalFeaturesSpecified
        {
            get { return this.RentalFeatures != null && this.RentalFeatures.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("RENTAL_UNITS", Order = 3)]
        public RENTAL_UNITS RentalUnits { get; set; }
    
        [XmlIgnore]
        public bool RentalUnitsSpecified
        {
            get { return this.RentalUnits != null && this.RentalUnits.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public MULTIFAMILY_RENTAL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
