namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class INTEGRATED_DISCLOSURE_SECTION_SUMMARY_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.IntegratedDisclosureSectionTotalAmountSpecified
                    || this.IntegratedDisclosureSectionTypeSpecified
                    || this.IntegratedDisclosureSectionTypeOtherDescriptionSpecified
                    || this.IntegratedDisclosureSubsectionTotalAmountSpecified
                    || this.IntegratedDisclosureSubsectionTypeSpecified
                    || this.IntegratedDisclosureSubsectionTypeOtherDescriptionSpecified
                    || this.LenderCreditToleranceCureAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("IntegratedDisclosureSectionTotalAmount", Order = 0)]
        public MISMOAmount IntegratedDisclosureSectionTotalAmount { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureSectionTotalAmountSpecified
        {
            get { return this.IntegratedDisclosureSectionTotalAmount != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureSectionType", Order = 1)]
        public MISMOEnum<IntegratedDisclosureSectionBase> IntegratedDisclosureSectionType { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureSectionTypeSpecified
        {
            get { return this.IntegratedDisclosureSectionType != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureSectionTypeOtherDescription", Order = 2)]
        public MISMOString IntegratedDisclosureSectionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureSectionTypeOtherDescriptionSpecified
        {
            get { return this.IntegratedDisclosureSectionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureSubsectionTotalAmount", Order = 3)]
        public MISMOAmount IntegratedDisclosureSubsectionTotalAmount { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureSubsectionTotalAmountSpecified
        {
            get { return this.IntegratedDisclosureSubsectionTotalAmount != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureSubsectionType", Order = 4)]
        public MISMOEnum<IntegratedDisclosureSubsectionBase> IntegratedDisclosureSubsectionType { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureSubsectionTypeSpecified
        {
            get { return this.IntegratedDisclosureSubsectionType != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureSubsectionTypeOtherDescription", Order = 5)]
        public MISMOString IntegratedDisclosureSubsectionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureSubsectionTypeOtherDescriptionSpecified
        {
            get { return this.IntegratedDisclosureSubsectionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("LenderCreditToleranceCureAmount", Order = 6)]
        public MISMOAmount LenderCreditToleranceCureAmount { get; set; }
    
        [XmlIgnore]
        public bool LenderCreditToleranceCureAmountSpecified
        {
            get { return this.LenderCreditToleranceCureAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 7)]
        public INTEGRATED_DISCLOSURE_SECTION_SUMMARY_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
