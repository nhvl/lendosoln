namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class INTEGRATED_DISCLOSURE_SECTION_SUMMARY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.IntegratedDisclosureSectionSummaryDetailSpecified
                    || this.IntegratedDisclosureSubsectionPaymentsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("INTEGRATED_DISCLOSURE_SECTION_SUMMARY_DETAIL", Order = 0)]
        public INTEGRATED_DISCLOSURE_SECTION_SUMMARY_DETAIL IntegratedDisclosureSectionSummaryDetail { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureSectionSummaryDetailSpecified
        {
            get { return this.IntegratedDisclosureSectionSummaryDetail != null && this.IntegratedDisclosureSectionSummaryDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("INTEGRATED_DISCLOSURE_SUBSECTION_PAYMENTS", Order = 1)]
        public INTEGRATED_DISCLOSURE_SUBSECTION_PAYMENTS IntegratedDisclosureSubsectionPayments { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureSubsectionPaymentsSpecified
        {
            get { return this.IntegratedDisclosureSubsectionPayments != null && this.IntegratedDisclosureSubsectionPayments.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public INTEGRATED_DISCLOSURE_SECTION_SUMMARY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
