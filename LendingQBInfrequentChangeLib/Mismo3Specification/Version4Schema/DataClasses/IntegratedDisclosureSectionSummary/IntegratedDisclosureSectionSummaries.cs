namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class INTEGRATED_DISCLOSURE_SECTION_SUMMARIES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.IntegratedDisclosureSectionSummaryListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("INTEGRATED_DISCLOSURE_SECTION_SUMMARY", Order = 0)]
        public List<INTEGRATED_DISCLOSURE_SECTION_SUMMARY> IntegratedDisclosureSectionSummaryList { get; set; } = new List<INTEGRATED_DISCLOSURE_SECTION_SUMMARY>();
    
        [XmlIgnore]
        public bool IntegratedDisclosureSectionSummaryListSpecified
        {
            get { return this.IntegratedDisclosureSectionSummaryList != null && this.IntegratedDisclosureSectionSummaryList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public INTEGRATED_DISCLOSURE_SECTION_SUMMARIES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
