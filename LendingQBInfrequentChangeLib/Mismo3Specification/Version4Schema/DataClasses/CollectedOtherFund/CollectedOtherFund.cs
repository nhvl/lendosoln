namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class COLLECTED_OTHER_FUND
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CollectedOtherFundAmountSpecified
                    || this.CollectedOtherFundPercentSpecified
                    || this.CollectedOtherFundPurposeTypeSpecified
                    || this.CollectedOtherFundPurposeTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CollectedOtherFundAmount", Order = 0)]
        public MISMOAmount CollectedOtherFundAmount { get; set; }
    
        [XmlIgnore]
        public bool CollectedOtherFundAmountSpecified
        {
            get { return this.CollectedOtherFundAmount != null; }
            set { }
        }
    
        [XmlElement("CollectedOtherFundPercent", Order = 1)]
        public MISMOPercent CollectedOtherFundPercent { get; set; }
    
        [XmlIgnore]
        public bool CollectedOtherFundPercentSpecified
        {
            get { return this.CollectedOtherFundPercent != null; }
            set { }
        }
    
        [XmlElement("CollectedOtherFundPurposeType", Order = 2)]
        public MISMOEnum<CollectedOtherFundPurposeBase> CollectedOtherFundPurposeType { get; set; }
    
        [XmlIgnore]
        public bool CollectedOtherFundPurposeTypeSpecified
        {
            get { return this.CollectedOtherFundPurposeType != null; }
            set { }
        }
    
        [XmlElement("CollectedOtherFundPurposeTypeOtherDescription", Order = 3)]
        public MISMOString CollectedOtherFundPurposeTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CollectedOtherFundPurposeTypeOtherDescriptionSpecified
        {
            get { return this.CollectedOtherFundPurposeTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public COLLECTED_OTHER_FUND_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
