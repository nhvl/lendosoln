namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class COLLECTED_OTHER_FUNDS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CollectedOtherFundListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("COLLECTED_OTHER_FUND", Order = 0)]
        public List<COLLECTED_OTHER_FUND> CollectedOtherFundList { get; set; } = new List<COLLECTED_OTHER_FUND>();
    
        [XmlIgnore]
        public bool CollectedOtherFundListSpecified
        {
            get { return this.CollectedOtherFundList != null && this.CollectedOtherFundList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public COLLECTED_OTHER_FUNDS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
