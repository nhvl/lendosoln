namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class WORKOUT_STATUSES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.WorkoutStatusListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("WORKOUT_STATUS", Order = 0)]
        public List<WORKOUT_STATUS> WorkoutStatusList { get; set; } = new List<WORKOUT_STATUS>();
    
        [XmlIgnore]
        public bool WorkoutStatusListSpecified
        {
            get { return this.WorkoutStatusList != null && this.WorkoutStatusList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public WORKOUT_STATUSES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
