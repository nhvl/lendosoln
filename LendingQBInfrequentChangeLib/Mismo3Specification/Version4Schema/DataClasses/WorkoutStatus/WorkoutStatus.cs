namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class WORKOUT_STATUS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.WorkoutProcessStatusTypeSpecified
                    || this.WorkoutStatusEndDateSpecified
                    || this.WorkoutStatusOccurredDateSpecified
                    || this.WorkoutStatusTypeSpecified
                    || this.WorkoutStatusTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("WorkoutProcessStatusType", Order = 0)]
        public MISMOEnum<WorkoutProcessStatusBase> WorkoutProcessStatusType { get; set; }
    
        [XmlIgnore]
        public bool WorkoutProcessStatusTypeSpecified
        {
            get { return this.WorkoutProcessStatusType != null; }
            set { }
        }
    
        [XmlElement("WorkoutStatusEndDate", Order = 1)]
        public MISMODate WorkoutStatusEndDate { get; set; }
    
        [XmlIgnore]
        public bool WorkoutStatusEndDateSpecified
        {
            get { return this.WorkoutStatusEndDate != null; }
            set { }
        }
    
        [XmlElement("WorkoutStatusOccurredDate", Order = 2)]
        public MISMODate WorkoutStatusOccurredDate { get; set; }
    
        [XmlIgnore]
        public bool WorkoutStatusOccurredDateSpecified
        {
            get { return this.WorkoutStatusOccurredDate != null; }
            set { }
        }
    
        [XmlElement("WorkoutStatusType", Order = 3)]
        public MISMOEnum<WorkoutStatusBase> WorkoutStatusType { get; set; }
    
        [XmlIgnore]
        public bool WorkoutStatusTypeSpecified
        {
            get { return this.WorkoutStatusType != null; }
            set { }
        }
    
        [XmlElement("WorkoutStatusTypeOtherDescription", Order = 4)]
        public MISMOString WorkoutStatusTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool WorkoutStatusTypeOtherDescriptionSpecified
        {
            get { return this.WorkoutStatusTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public WORKOUT_STATUS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
