namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CREDIT_FILE_OWNING_BUREAU_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditFileOwningBureauIdentifierSpecified
                    || this.CreditFileOwningBureauNameSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CreditFileOwningBureauIdentifier", Order = 0)]
        public MISMOIdentifier CreditFileOwningBureauIdentifier { get; set; }
    
        [XmlIgnore]
        public bool CreditFileOwningBureauIdentifierSpecified
        {
            get { return this.CreditFileOwningBureauIdentifier != null; }
            set { }
        }
    
        [XmlElement("CreditFileOwningBureauName", Order = 1)]
        public MISMOString CreditFileOwningBureauName { get; set; }
    
        [XmlIgnore]
        public bool CreditFileOwningBureauNameSpecified
        {
            get { return this.CreditFileOwningBureauName != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public CREDIT_FILE_OWNING_BUREAU_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
