namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class DOWN_PAYMENTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DownPaymentListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DOWN_PAYMENT", Order = 0)]
        public List<DOWN_PAYMENT> DownPaymentList { get; set; } = new List<DOWN_PAYMENT>();
    
        [XmlIgnore]
        public bool DownPaymentListSpecified
        {
            get { return this.DownPaymentList != null && this.DownPaymentList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public DOWN_PAYMENTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
