namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class DOWN_PAYMENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DownPaymentAmountSpecified
                    || this.DownPaymentOptionTypeSpecified
                    || this.DownPaymentOptionTypeOtherDescriptionSpecified
                    || this.FundsSourceTypeSpecified
                    || this.FundsSourceTypeOtherDescriptionSpecified
                    || this.FundsTypeSpecified
                    || this.FundsTypeOtherDescriptionSpecified
                    || this.PropertySellerFundingIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DownPaymentAmount", Order = 0)]
        public MISMOAmount DownPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool DownPaymentAmountSpecified
        {
            get { return this.DownPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("DownPaymentOptionType", Order = 1)]
        public MISMOEnum<DownPaymentOptionBase> DownPaymentOptionType { get; set; }
    
        [XmlIgnore]
        public bool DownPaymentOptionTypeSpecified
        {
            get { return this.DownPaymentOptionType != null; }
            set { }
        }
    
        [XmlElement("DownPaymentOptionTypeOtherDescription", Order = 2)]
        public MISMOString DownPaymentOptionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool DownPaymentOptionTypeOtherDescriptionSpecified
        {
            get { return this.DownPaymentOptionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("FundsSourceType", Order = 3)]
        public MISMOEnum<FundsSourceBase> FundsSourceType { get; set; }
    
        [XmlIgnore]
        public bool FundsSourceTypeSpecified
        {
            get { return this.FundsSourceType != null; }
            set { }
        }
    
        [XmlElement("FundsSourceTypeOtherDescription", Order = 4)]
        public MISMOString FundsSourceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool FundsSourceTypeOtherDescriptionSpecified
        {
            get { return this.FundsSourceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("FundsType", Order = 5)]
        public MISMOEnum<FundsBase> FundsType { get; set; }
    
        [XmlIgnore]
        public bool FundsTypeSpecified
        {
            get { return this.FundsType != null; }
            set { }
        }
    
        [XmlElement("FundsTypeOtherDescription", Order = 6)]
        public MISMOString FundsTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool FundsTypeOtherDescriptionSpecified
        {
            get { return this.FundsTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PropertySellerFundingIndicator", Order = 7)]
        public MISMOIndicator PropertySellerFundingIndicator { get; set; }
    
        [XmlIgnore]
        public bool PropertySellerFundingIndicatorSpecified
        {
            get { return this.PropertySellerFundingIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 8)]
        public DOWN_PAYMENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
