namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class INSURANCE_CLAIM
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ActualInsuranceClaimAmountSpecified
                    || this.ActualInsuranceProceedsAmountSpecified
                    || this.EstimatedInsuranceClaimAmountSpecified
                    || this.EstimatedInsuranceProceedsAmountSpecified
                    || this.InsuranceClaimEligibilityDateSpecified
                    || this.InsuranceClaimFiledDateSpecified
                    || this.InsuranceClaimPaidDateSpecified
                    || this.InsuranceClaimPendingIndicatorSpecified
                    || this.InsuranceClaimTypeSpecified
                    || this.InsuranceClaimTypeOtherDescriptionSpecified
                    || this.InsurancePartialClaimIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ActualInsuranceClaimAmount", Order = 0)]
        public MISMOAmount ActualInsuranceClaimAmount { get; set; }
    
        [XmlIgnore]
        public bool ActualInsuranceClaimAmountSpecified
        {
            get { return this.ActualInsuranceClaimAmount != null; }
            set { }
        }
    
        [XmlElement("ActualInsuranceProceedsAmount", Order = 1)]
        public MISMOAmount ActualInsuranceProceedsAmount { get; set; }
    
        [XmlIgnore]
        public bool ActualInsuranceProceedsAmountSpecified
        {
            get { return this.ActualInsuranceProceedsAmount != null; }
            set { }
        }
    
        [XmlElement("EstimatedInsuranceClaimAmount", Order = 2)]
        public MISMOAmount EstimatedInsuranceClaimAmount { get; set; }
    
        [XmlIgnore]
        public bool EstimatedInsuranceClaimAmountSpecified
        {
            get { return this.EstimatedInsuranceClaimAmount != null; }
            set { }
        }
    
        [XmlElement("EstimatedInsuranceProceedsAmount", Order = 3)]
        public MISMOAmount EstimatedInsuranceProceedsAmount { get; set; }
    
        [XmlIgnore]
        public bool EstimatedInsuranceProceedsAmountSpecified
        {
            get { return this.EstimatedInsuranceProceedsAmount != null; }
            set { }
        }
    
        [XmlElement("InsuranceClaimEligibilityDate", Order = 4)]
        public MISMODate InsuranceClaimEligibilityDate { get; set; }
    
        [XmlIgnore]
        public bool InsuranceClaimEligibilityDateSpecified
        {
            get { return this.InsuranceClaimEligibilityDate != null; }
            set { }
        }
    
        [XmlElement("InsuranceClaimFiledDate", Order = 5)]
        public MISMODate InsuranceClaimFiledDate { get; set; }
    
        [XmlIgnore]
        public bool InsuranceClaimFiledDateSpecified
        {
            get { return this.InsuranceClaimFiledDate != null; }
            set { }
        }
    
        [XmlElement("InsuranceClaimPaidDate", Order = 6)]
        public MISMODate InsuranceClaimPaidDate { get; set; }
    
        [XmlIgnore]
        public bool InsuranceClaimPaidDateSpecified
        {
            get { return this.InsuranceClaimPaidDate != null; }
            set { }
        }
    
        [XmlElement("InsuranceClaimPendingIndicator", Order = 7)]
        public MISMOIndicator InsuranceClaimPendingIndicator { get; set; }
    
        [XmlIgnore]
        public bool InsuranceClaimPendingIndicatorSpecified
        {
            get { return this.InsuranceClaimPendingIndicator != null; }
            set { }
        }
    
        [XmlElement("InsuranceClaimType", Order = 8)]
        public MISMOEnum<InsuranceClaimBase> InsuranceClaimType { get; set; }
    
        [XmlIgnore]
        public bool InsuranceClaimTypeSpecified
        {
            get { return this.InsuranceClaimType != null; }
            set { }
        }
    
        [XmlElement("InsuranceClaimTypeOtherDescription", Order = 9)]
        public MISMOString InsuranceClaimTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool InsuranceClaimTypeOtherDescriptionSpecified
        {
            get { return this.InsuranceClaimTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("InsurancePartialClaimIndicator", Order = 10)]
        public MISMOIndicator InsurancePartialClaimIndicator { get; set; }
    
        [XmlIgnore]
        public bool InsurancePartialClaimIndicatorSpecified
        {
            get { return this.InsurancePartialClaimIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 11)]
        public INSURANCE_CLAIM_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
