namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class INSURANCE_CLAIMS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.InsuranceClaimListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("INSURANCE_CLAIM", Order = 0)]
        public List<INSURANCE_CLAIM> InsuranceClaimList { get; set; } = new List<INSURANCE_CLAIM>();
    
        [XmlIgnore]
        public bool InsuranceClaimListSpecified
        {
            get { return this.InsuranceClaimList != null && this.InsuranceClaimList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public INSURANCE_CLAIMS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
