namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class FOREIGN_OBJECTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ForeignObjectListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("FOREIGN_OBJECT", Order = 0)]
        public List<FOREIGN_OBJECT> ForeignObjectList { get; set; } = new List<FOREIGN_OBJECT>();
    
        [XmlIgnore]
        public bool ForeignObjectListSpecified
        {
            get { return this.ForeignObjectList != null && this.ForeignObjectList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public FOREIGN_OBJECTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
