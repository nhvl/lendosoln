namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    public class FOREIGN_OBJECT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                if (Mismo3Utilities.ExceedsThreshold(1,
                    new List<bool> {
                        this.ReferenceSpecified,
                        this.CharacterEncodingSetTypeSpecified
                        || this.CharacterEncodingSetTypeOtherDescriptionSpecified
                        || this.ExtensionSpecified
                        || this.EmbeddedContentXMLSpecified
                        || this.MIMETypeIdentifierSpecified
                        || this.MIMETypeVersionIdentifierSpecified
                        || this.ObjectCreatedDatetimeSpecified
                        || this.ObjectDescriptionSpecified
                        || this.ObjectEncodingTypeSpecified
                        || this.ObjectEncodingTypeOtherDescriptionSpecified
                        || this.ObjectNameSpecified
                        || this.ObjectURLSpecified
                        || this.OriginalCreatorDigestValueSpecified
                        || this.ReferenceSpecified
                        || this.UnencodedObjectByteCountSpecified }))
                {
                    throw new System.Xml.Schema.XmlSchemaValidationException(
                        Mismo3Utilities.GetXSDChoiceExceptionMessage(
                        "FOREIGN_OBJECT",
                        new List<string> { "REFERENCE", "Any other child element of the FOREIGN_OBJECT" }));
                }

                if (Mismo3Utilities.ExceedsThreshold(1,
                    new List<bool> { this.EmbeddedContentXMLSpecified, this.ObjectURLSpecified }))
                {
                    throw new System.Xml.Schema.XmlSchemaValidationException(
                        Mismo3Utilities.GetXSDChoiceExceptionMessage(
                        "FOREIGN_OBJECT",
                        new List<string> { "EmbeddedContentXML", "ObjectURL" }));
                }

                return this.CharacterEncodingSetTypeSpecified
                    || this.CharacterEncodingSetTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified
                    || this.EmbeddedContentXMLSpecified
                    || this.MIMETypeIdentifierSpecified
                    || this.MIMETypeVersionIdentifierSpecified
                    || this.ObjectCreatedDatetimeSpecified
                    || this.ObjectDescriptionSpecified
                    || this.ObjectEncodingTypeSpecified
                    || this.ObjectEncodingTypeOtherDescriptionSpecified
                    || this.ObjectNameSpecified
                    || this.ObjectURLSpecified
                    || this.OriginalCreatorDigestValueSpecified
                    || this.ReferenceSpecified
                    || this.UnencodedObjectByteCountSpecified;
            }
        }

        [XmlElement("REFERENCE", Order = 0)]
        public REFERENCE Reference { get; set; }

        [XmlIgnore]
        public bool ReferenceSpecified
        {
            get { return this.Reference != null && this.Reference.ShouldSerialize; }
            set { }
        }

        [XmlElement("EmbeddedContentXML", Order = 1)]
        public MISMOXML EmbeddedContentXML { get; set; }

        [XmlIgnore]
        public bool EmbeddedContentXMLSpecified
        {
            get { return this.EmbeddedContentXML != null; }
            set { }
        }

        [XmlElement("ObjectURL", Order = 2)]
        public MISMOObjectURL ObjectURL { get; set; }

        [XmlIgnore]
        public bool ObjectURLSpecified
        {
            get { return this.ObjectURL != null; }
            set { }
        }

        [XmlElement("CharacterEncodingSetType", Order = 3)]
        public MISMOEnum<CharacterEncodingSetBase> CharacterEncodingSetType { get; set; }
    
        [XmlIgnore]
        public bool CharacterEncodingSetTypeSpecified
        {
            get { return this.CharacterEncodingSetType != null; }
            set { }
        }
    
        [XmlElement("CharacterEncodingSetTypeOtherDescription", Order = 4)]
        public MISMOString CharacterEncodingSetTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CharacterEncodingSetTypeOtherDescriptionSpecified
        {
            get { return this.CharacterEncodingSetTypeOtherDescription != null; }
            set { }
        }

        [XmlElement("MIMETypeIdentifier", Order = 5)]
        public MISMOIdentifier MIMETypeIdentifier { get; set; }

        [XmlIgnore]
        public bool MIMETypeIdentifierSpecified
        {
            get { return this.MIMETypeIdentifier != null; }
            set { }
        }

        [XmlElement("MIMETypeVersionIdentifier", Order = 6)]
        public MISMOIdentifier MIMETypeVersionIdentifier { get; set; }

        [XmlIgnore]
        public bool MIMETypeVersionIdentifierSpecified
        {
            get { return this.MIMETypeVersionIdentifier != null; }
            set { }
        }

        [XmlElement("ObjectCreatedDatetime", Order = 7)]
        public MISMODatetime ObjectCreatedDatetime { get; set; }

        [XmlIgnore]
        public bool ObjectCreatedDatetimeSpecified
        {
            get { return this.ObjectCreatedDatetime != null; }
            set { }
        }

        [XmlElement("ObjectDescription", Order = 8)]
        public MISMOString ObjectDescription { get; set; }

        [XmlIgnore]
        public bool ObjectDescriptionSpecified
        {
            get { return this.ObjectDescription != null; }
            set { }
        }

        [XmlElement("ObjectEncodingType", Order = 9)]
        public MISMOEnum<ObjectEncodingBase> ObjectEncodingType { get; set; }

        [XmlIgnore]
        public bool ObjectEncodingTypeSpecified
        {
            get { return this.ObjectEncodingType != null; }
            set { }
        }

        [XmlElement("ObjectEncodingTypeOtherDescription", Order = 10)]
        public MISMOString ObjectEncodingTypeOtherDescription { get; set; }

        [XmlIgnore]
        public bool ObjectEncodingTypeOtherDescriptionSpecified
        {
            get { return this.ObjectEncodingTypeOtherDescription != null; }
            set { }
        }

        [XmlElement("OriginalCreatorDigestValue", Order = 11)]
        public MISMOValue OriginalCreatorDigestValue { get; set; }

        [XmlIgnore]
        public bool OriginalCreatorDigestValueSpecified
        {
            get { return this.OriginalCreatorDigestValue != null; }
            set { }
        }

        [XmlElement("ObjectName", Order = 12)]
        public MISMOString ObjectName { get; set; }

        [XmlIgnore]
        public bool ObjectNameSpecified
        {
            get { return this.ObjectName != null; }
            set { }
        }

        [XmlElement("UnencodedObjectByteCount", Order = 13)]
        public MISMOCount UnencodedObjectByteCount { get; set; }

        [XmlIgnore]
        public bool UnencodedObjectByteCountSpecified
        {
            get { return this.UnencodedObjectByteCount != null; }
            set { }
        }

        [XmlElement("EXTENSION", Order = 14)]
        public FOREIGN_OBJECT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
