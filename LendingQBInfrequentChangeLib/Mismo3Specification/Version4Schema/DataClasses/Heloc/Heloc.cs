namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class HELOC
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.HelocOccurrencesSpecified
                    || this.HelocRuleSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("HELOC_OCCURRENCES", Order = 0)]
        public HELOC_OCCURRENCES HelocOccurrences { get; set; }
    
        [XmlIgnore]
        public bool HelocOccurrencesSpecified
        {
            get { return this.HelocOccurrences != null && this.HelocOccurrences.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("HELOC_RULE", Order = 1)]
        public HELOC_RULE HelocRule { get; set; }
    
        [XmlIgnore]
        public bool HelocRuleSpecified
        {
            get { return this.HelocRule != null && this.HelocRule.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public HELOC_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
