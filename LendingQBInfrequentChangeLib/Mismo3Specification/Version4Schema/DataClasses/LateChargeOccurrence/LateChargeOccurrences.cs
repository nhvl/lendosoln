namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class LATE_CHARGE_OCCURRENCES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LateChargeOccurrenceListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LATE_CHARGE_OCCURRENCE", Order = 0)]
        public List<LATE_CHARGE_OCCURRENCE> LateChargeOccurrenceList { get; set; } = new List<LATE_CHARGE_OCCURRENCE>();
    
        [XmlIgnore]
        public bool LateChargeOccurrenceListSpecified
        {
            get { return this.LateChargeOccurrenceList != null && this.LateChargeOccurrenceList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public LATE_CHARGE_OCCURRENCES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
