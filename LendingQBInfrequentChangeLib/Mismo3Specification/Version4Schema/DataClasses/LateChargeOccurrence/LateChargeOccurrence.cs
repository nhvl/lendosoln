namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class LATE_CHARGE_OCCURRENCE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LateChargeLoanPaymentAmountSpecified
                    || this.UncollectedLateChargeBalanceAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LateChargeLoanPaymentAmount", Order = 0)]
        public MISMOAmount LateChargeLoanPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool LateChargeLoanPaymentAmountSpecified
        {
            get { return this.LateChargeLoanPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("UncollectedLateChargeBalanceAmount", Order = 1)]
        public MISMOAmount UncollectedLateChargeBalanceAmount { get; set; }
    
        [XmlIgnore]
        public bool UncollectedLateChargeBalanceAmountSpecified
        {
            get { return this.UncollectedLateChargeBalanceAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public LATE_CHARGE_OCCURRENCE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
