namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class VALUATION_UPDATE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AppraisalCompletionCertificateCommentDescriptionSpecified
                    || this.AppraisalCompletionCertificateIndicatorSpecified
                    || this.AppraisalReviewOriginalAppraisalEffectiveDateSpecified
                    || this.AppraisalReviewOriginalAppraisedValueAmountSpecified
                    || this.AppraisalUpdateCommentDescriptionSpecified
                    || this.AppraisalUpdateIndicatorSpecified
                    || this.AppraisalUpdateOriginalAppraiserCompanyNameSpecified
                    || this.AppraisalUpdateOriginalAppraiserUnparsedNameSpecified
                    || this.AppraisalUpdateOriginalLenderAddressLineTextSpecified
                    || this.AppraisalUpdateOriginalLenderUnparsedNameSpecified
                    || this.PropertyImprovementsCompletedIndicatorSpecified
                    || this.PropertyMarketValueDecreasedIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AppraisalCompletionCertificateCommentDescription", Order = 0)]
        public MISMOString AppraisalCompletionCertificateCommentDescription { get; set; }
    
        [XmlIgnore]
        public bool AppraisalCompletionCertificateCommentDescriptionSpecified
        {
            get { return this.AppraisalCompletionCertificateCommentDescription != null; }
            set { }
        }
    
        [XmlElement("AppraisalCompletionCertificateIndicator", Order = 1)]
        public MISMOIndicator AppraisalCompletionCertificateIndicator { get; set; }
    
        [XmlIgnore]
        public bool AppraisalCompletionCertificateIndicatorSpecified
        {
            get { return this.AppraisalCompletionCertificateIndicator != null; }
            set { }
        }
    
        [XmlElement("AppraisalReviewOriginalAppraisalEffectiveDate", Order = 2)]
        public MISMODate AppraisalReviewOriginalAppraisalEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool AppraisalReviewOriginalAppraisalEffectiveDateSpecified
        {
            get { return this.AppraisalReviewOriginalAppraisalEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("AppraisalReviewOriginalAppraisedValueAmount", Order = 3)]
        public MISMOAmount AppraisalReviewOriginalAppraisedValueAmount { get; set; }
    
        [XmlIgnore]
        public bool AppraisalReviewOriginalAppraisedValueAmountSpecified
        {
            get { return this.AppraisalReviewOriginalAppraisedValueAmount != null; }
            set { }
        }
    
        [XmlElement("AppraisalUpdateCommentDescription", Order = 4)]
        public MISMOString AppraisalUpdateCommentDescription { get; set; }
    
        [XmlIgnore]
        public bool AppraisalUpdateCommentDescriptionSpecified
        {
            get { return this.AppraisalUpdateCommentDescription != null; }
            set { }
        }
    
        [XmlElement("AppraisalUpdateIndicator", Order = 5)]
        public MISMOIndicator AppraisalUpdateIndicator { get; set; }
    
        [XmlIgnore]
        public bool AppraisalUpdateIndicatorSpecified
        {
            get { return this.AppraisalUpdateIndicator != null; }
            set { }
        }
    
        [XmlElement("AppraisalUpdateOriginalAppraiserCompanyName", Order = 6)]
        public MISMOString AppraisalUpdateOriginalAppraiserCompanyName { get; set; }
    
        [XmlIgnore]
        public bool AppraisalUpdateOriginalAppraiserCompanyNameSpecified
        {
            get { return this.AppraisalUpdateOriginalAppraiserCompanyName != null; }
            set { }
        }
    
        [XmlElement("AppraisalUpdateOriginalAppraiserUnparsedName", Order = 7)]
        public MISMOString AppraisalUpdateOriginalAppraiserUnparsedName { get; set; }
    
        [XmlIgnore]
        public bool AppraisalUpdateOriginalAppraiserUnparsedNameSpecified
        {
            get { return this.AppraisalUpdateOriginalAppraiserUnparsedName != null; }
            set { }
        }
    
        [XmlElement("AppraisalUpdateOriginalLenderAddressLineText", Order = 8)]
        public MISMOString AppraisalUpdateOriginalLenderAddressLineText { get; set; }
    
        [XmlIgnore]
        public bool AppraisalUpdateOriginalLenderAddressLineTextSpecified
        {
            get { return this.AppraisalUpdateOriginalLenderAddressLineText != null; }
            set { }
        }
    
        [XmlElement("AppraisalUpdateOriginalLenderUnparsedName", Order = 9)]
        public MISMOString AppraisalUpdateOriginalLenderUnparsedName { get; set; }
    
        [XmlIgnore]
        public bool AppraisalUpdateOriginalLenderUnparsedNameSpecified
        {
            get { return this.AppraisalUpdateOriginalLenderUnparsedName != null; }
            set { }
        }
    
        [XmlElement("PropertyImprovementsCompletedIndicator", Order = 10)]
        public MISMOIndicator PropertyImprovementsCompletedIndicator { get; set; }
    
        [XmlIgnore]
        public bool PropertyImprovementsCompletedIndicatorSpecified
        {
            get { return this.PropertyImprovementsCompletedIndicator != null; }
            set { }
        }
    
        [XmlElement("PropertyMarketValueDecreasedIndicator", Order = 11)]
        public MISMOIndicator PropertyMarketValueDecreasedIndicator { get; set; }
    
        [XmlIgnore]
        public bool PropertyMarketValueDecreasedIndicatorSpecified
        {
            get { return this.PropertyMarketValueDecreasedIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 12)]
        public VALUATION_UPDATE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
