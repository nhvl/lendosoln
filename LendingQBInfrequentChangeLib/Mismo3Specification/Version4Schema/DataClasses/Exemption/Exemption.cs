namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class EXEMPTION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AbilityToRepayExemptionCreditExtendedPurposeIndicatorSpecified
                    || this.AbilityToRepayExemptionCreditorOrganizationTypeSpecified
                    || this.AbilityToRepayExemptionCreditorOrganizationTypeOtherDescriptionSpecified
                    || this.AbilityToRepayExemptionLoanProgramTypeSpecified
                    || this.AbilityToRepayExemptionLoanProgramTypeOtherDescriptionSpecified
                    || this.AbilityToRepayExemptionReasonTypeSpecified
                    || this.AbilityToRepayExemptionReasonTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AbilityToRepayExemptionCreditExtendedPurposeIndicator", Order = 0)]
        public MISMOIndicator AbilityToRepayExemptionCreditExtendedPurposeIndicator { get; set; }
    
        [XmlIgnore]
        public bool AbilityToRepayExemptionCreditExtendedPurposeIndicatorSpecified
        {
            get { return this.AbilityToRepayExemptionCreditExtendedPurposeIndicator != null; }
            set { }
        }
    
        [XmlElement("AbilityToRepayExemptionCreditorOrganizationType", Order = 1)]
        public MISMOEnum<AbilityToRepayExemptionCreditorOrganizationBase> AbilityToRepayExemptionCreditorOrganizationType { get; set; }
    
        [XmlIgnore]
        public bool AbilityToRepayExemptionCreditorOrganizationTypeSpecified
        {
            get { return this.AbilityToRepayExemptionCreditorOrganizationType != null; }
            set { }
        }
    
        [XmlElement("AbilityToRepayExemptionCreditorOrganizationTypeOtherDescription", Order = 2)]
        public MISMOString AbilityToRepayExemptionCreditorOrganizationTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool AbilityToRepayExemptionCreditorOrganizationTypeOtherDescriptionSpecified
        {
            get { return this.AbilityToRepayExemptionCreditorOrganizationTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("AbilityToRepayExemptionLoanProgramType", Order = 3)]
        public MISMOEnum<AbilityToRepayExemptionLoanProgramBase> AbilityToRepayExemptionLoanProgramType { get; set; }
    
        [XmlIgnore]
        public bool AbilityToRepayExemptionLoanProgramTypeSpecified
        {
            get { return this.AbilityToRepayExemptionLoanProgramType != null; }
            set { }
        }
    
        [XmlElement("AbilityToRepayExemptionLoanProgramTypeOtherDescription", Order = 4)]
        public MISMOString AbilityToRepayExemptionLoanProgramTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool AbilityToRepayExemptionLoanProgramTypeOtherDescriptionSpecified
        {
            get { return this.AbilityToRepayExemptionLoanProgramTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("AbilityToRepayExemptionReasonType", Order = 5)]
        public MISMOEnum<AbilityToRepayExemptionReasonBase> AbilityToRepayExemptionReasonType { get; set; }
    
        [XmlIgnore]
        public bool AbilityToRepayExemptionReasonTypeSpecified
        {
            get { return this.AbilityToRepayExemptionReasonType != null; }
            set { }
        }
    
        [XmlElement("AbilityToRepayExemptionReasonTypeOtherDescription", Order = 6)]
        public MISMOString AbilityToRepayExemptionReasonTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool AbilityToRepayExemptionReasonTypeOtherDescriptionSpecified
        {
            get { return this.AbilityToRepayExemptionReasonTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 7)]
        public EXEMPTION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
