namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class EXEMPTIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExemptionListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("EXEMPTION", Order = 0)]
        public List<EXEMPTION> ExemptionList { get; set; } = new List<EXEMPTION>();
    
        [XmlIgnore]
        public bool ExemptionListSpecified
        {
            get { return this.ExemptionList != null && this.ExemptionList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public EXEMPTIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
