namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class LEGAL_DESCRIPTIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LegalDescriptionListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LEGAL_DESCRIPTION", Order = 0)]
        public List<LEGAL_DESCRIPTION> LegalDescriptionList { get; set; } = new List<LEGAL_DESCRIPTION>();
    
        [XmlIgnore]
        public bool LegalDescriptionListSpecified
        {
            get { return this.LegalDescriptionList != null && this.LegalDescriptionList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public LEGAL_DESCRIPTIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
