namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class LEGAL_DESCRIPTION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ParcelIdentificationsSpecified
                    || this.ParsedLegalDescriptionSpecified
                    || this.UnparsedLegalDescriptionsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PARCEL_IDENTIFICATIONS", Order = 0)]
        public PARCEL_IDENTIFICATIONS ParcelIdentifications { get; set; }
    
        [XmlIgnore]
        public bool ParcelIdentificationsSpecified
        {
            get { return this.ParcelIdentifications != null && this.ParcelIdentifications.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PARSED_LEGAL_DESCRIPTION", Order = 1)]
        public PARSED_LEGAL_DESCRIPTION ParsedLegalDescription { get; set; }
    
        [XmlIgnore]
        public bool ParsedLegalDescriptionSpecified
        {
            get { return this.ParsedLegalDescription != null && this.ParsedLegalDescription.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("UNPARSED_LEGAL_DESCRIPTIONS", Order = 2)]
        public UNPARSED_LEGAL_DESCRIPTIONS UnparsedLegalDescriptions { get; set; }
    
        [XmlIgnore]
        public bool UnparsedLegalDescriptionsSpecified
        {
            get { return this.UnparsedLegalDescriptions != null && this.UnparsedLegalDescriptions.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public LEGAL_DESCRIPTION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
