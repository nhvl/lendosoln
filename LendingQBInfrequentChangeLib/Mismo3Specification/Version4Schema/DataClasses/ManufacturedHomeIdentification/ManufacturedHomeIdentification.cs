namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MANUFACTURED_HOME_IDENTIFICATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ManufacturedHomeBuiltStateNameSpecified
                    || this.ManufacturedHomeCostDataCommentDescriptionSpecified
                    || this.ManufacturedHomeRegionIdentifierSpecified
                    || this.NADAManufacturedHousingGuideBlackPageIdentifierSpecified
                    || this.NADAManufacturedHousingGuideConversionChartPageIdentifierSpecified
                    || this.NADAManufacturedHousingGuideEditionMonthSpecified
                    || this.NADAManufacturedHousingGuideEditionYearSpecified
                    || this.NADAManufacturedHousingGuideGreyPageIdentifierSpecified
                    || this.NADAManufacturedHousingGuideWhitePageIdentifierSpecified
                    || this.NADAManufacturedHousingGuideYellowPageIdentifierSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ManufacturedHomeBuiltStateName", Order = 0)]
        public MISMOString ManufacturedHomeBuiltStateName { get; set; }
    
        [XmlIgnore]
        public bool ManufacturedHomeBuiltStateNameSpecified
        {
            get { return this.ManufacturedHomeBuiltStateName != null; }
            set { }
        }
    
        [XmlElement("ManufacturedHomeCostDataCommentDescription", Order = 1)]
        public MISMOString ManufacturedHomeCostDataCommentDescription { get; set; }
    
        [XmlIgnore]
        public bool ManufacturedHomeCostDataCommentDescriptionSpecified
        {
            get { return this.ManufacturedHomeCostDataCommentDescription != null; }
            set { }
        }
    
        [XmlElement("ManufacturedHomeRegionIdentifier", Order = 2)]
        public MISMOIdentifier ManufacturedHomeRegionIdentifier { get; set; }
    
        [XmlIgnore]
        public bool ManufacturedHomeRegionIdentifierSpecified
        {
            get { return this.ManufacturedHomeRegionIdentifier != null; }
            set { }
        }
    
        [XmlElement("NADAManufacturedHousingGuideBlackPageIdentifier", Order = 3)]
        public MISMOIdentifier NADAManufacturedHousingGuideBlackPageIdentifier { get; set; }
    
        [XmlIgnore]
        public bool NADAManufacturedHousingGuideBlackPageIdentifierSpecified
        {
            get { return this.NADAManufacturedHousingGuideBlackPageIdentifier != null; }
            set { }
        }
    
        [XmlElement("NADAManufacturedHousingGuideConversionChartPageIdentifier", Order = 4)]
        public MISMOIdentifier NADAManufacturedHousingGuideConversionChartPageIdentifier { get; set; }
    
        [XmlIgnore]
        public bool NADAManufacturedHousingGuideConversionChartPageIdentifierSpecified
        {
            get { return this.NADAManufacturedHousingGuideConversionChartPageIdentifier != null; }
            set { }
        }
    
        [XmlElement("NADAManufacturedHousingGuideEditionMonth", Order = 5)]
        public MISMOMonth NADAManufacturedHousingGuideEditionMonth { get; set; }
    
        [XmlIgnore]
        public bool NADAManufacturedHousingGuideEditionMonthSpecified
        {
            get { return this.NADAManufacturedHousingGuideEditionMonth != null; }
            set { }
        }
    
        [XmlElement("NADAManufacturedHousingGuideEditionYear", Order = 6)]
        public MISMOYear NADAManufacturedHousingGuideEditionYear { get; set; }
    
        [XmlIgnore]
        public bool NADAManufacturedHousingGuideEditionYearSpecified
        {
            get { return this.NADAManufacturedHousingGuideEditionYear != null; }
            set { }
        }
    
        [XmlElement("NADAManufacturedHousingGuideGreyPageIdentifier", Order = 7)]
        public MISMOIdentifier NADAManufacturedHousingGuideGreyPageIdentifier { get; set; }
    
        [XmlIgnore]
        public bool NADAManufacturedHousingGuideGreyPageIdentifierSpecified
        {
            get { return this.NADAManufacturedHousingGuideGreyPageIdentifier != null; }
            set { }
        }
    
        [XmlElement("NADAManufacturedHousingGuideWhitePageIdentifier", Order = 8)]
        public MISMOIdentifier NADAManufacturedHousingGuideWhitePageIdentifier { get; set; }
    
        [XmlIgnore]
        public bool NADAManufacturedHousingGuideWhitePageIdentifierSpecified
        {
            get { return this.NADAManufacturedHousingGuideWhitePageIdentifier != null; }
            set { }
        }
    
        [XmlElement("NADAManufacturedHousingGuideYellowPageIdentifier", Order = 9)]
        public MISMOIdentifier NADAManufacturedHousingGuideYellowPageIdentifier { get; set; }
    
        [XmlIgnore]
        public bool NADAManufacturedHousingGuideYellowPageIdentifierSpecified
        {
            get { return this.NADAManufacturedHousingGuideYellowPageIdentifier != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 10)]
        public MANUFACTURED_HOME_IDENTIFICATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
