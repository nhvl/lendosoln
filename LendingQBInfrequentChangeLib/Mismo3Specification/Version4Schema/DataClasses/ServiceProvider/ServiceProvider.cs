namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SERVICE_PROVIDER
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ProvidedServicesSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PROVIDED_SERVICES", Order = 0)]
        public PROVIDED_SERVICES ProvidedServices { get; set; }
    
        [XmlIgnore]
        public bool ProvidedServicesSpecified
        {
            get { return this.ProvidedServices != null && this.ProvidedServices.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public SERVICE_PROVIDER_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
