namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class CREDIT_REQUEST_DATAS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditRequestDataListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CREDIT_REQUEST_DATA", Order = 0)]
        public List<CREDIT_REQUEST_DATA> CreditRequestDataList { get; set; } = new List<CREDIT_REQUEST_DATA>();
    
        [XmlIgnore]
        public bool CreditRequestDataListSpecified
        {
            get { return this.CreditRequestDataList != null && this.CreditRequestDataList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public CREDIT_REQUEST_DATAS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
