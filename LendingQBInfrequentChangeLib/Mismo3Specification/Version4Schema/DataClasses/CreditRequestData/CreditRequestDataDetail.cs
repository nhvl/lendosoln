namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CREDIT_REQUEST_DATA_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditRatingCodeTypeSpecified
                    || this.CreditRatingCodeTypeOtherDescriptionSpecified
                    || this.CreditReportIdentifierSpecified
                    || this.CreditReportProductDescriptionSpecified
                    || this.CreditReportRequestActionTypeSpecified
                    || this.CreditReportRequestActionTypeOtherDescriptionSpecified
                    || this.CreditReportTransactionIdentifierSpecified
                    || this.CreditReportTypeSpecified
                    || this.CreditReportTypeOtherDescriptionSpecified
                    || this.CreditRepositoriesSelectedCountSpecified
                    || this.CreditRequestDatetimeSpecified
                    || this.CreditRequestTypeSpecified
                    || this.InitialCreditReportFirstIssuedDateSpecified
                    || this.PaymentPatternRatingCodeTypeSpecified
                    || this.PaymentPatternRatingCodeTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CreditRatingCodeType", Order = 0)]
        public MISMOEnum<CreditRatingCodeBase> CreditRatingCodeType { get; set; }
    
        [XmlIgnore]
        public bool CreditRatingCodeTypeSpecified
        {
            get { return this.CreditRatingCodeType != null; }
            set { }
        }
    
        [XmlElement("CreditRatingCodeTypeOtherDescription", Order = 1)]
        public MISMOString CreditRatingCodeTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CreditRatingCodeTypeOtherDescriptionSpecified
        {
            get { return this.CreditRatingCodeTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("CreditReportIdentifier", Order = 2)]
        public MISMOIdentifier CreditReportIdentifier { get; set; }
    
        [XmlIgnore]
        public bool CreditReportIdentifierSpecified
        {
            get { return this.CreditReportIdentifier != null; }
            set { }
        }
    
        [XmlElement("CreditReportProductDescription", Order = 3)]
        public MISMOString CreditReportProductDescription { get; set; }
    
        [XmlIgnore]
        public bool CreditReportProductDescriptionSpecified
        {
            get { return this.CreditReportProductDescription != null; }
            set { }
        }
    
        [XmlElement("CreditReportRequestActionType", Order = 4)]
        public MISMOEnum<CreditReportRequestActionBase> CreditReportRequestActionType { get; set; }
    
        [XmlIgnore]
        public bool CreditReportRequestActionTypeSpecified
        {
            get { return this.CreditReportRequestActionType != null; }
            set { }
        }
    
        [XmlElement("CreditReportRequestActionTypeOtherDescription", Order = 5)]
        public MISMOString CreditReportRequestActionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CreditReportRequestActionTypeOtherDescriptionSpecified
        {
            get { return this.CreditReportRequestActionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("CreditReportTransactionIdentifier", Order = 6)]
        public MISMOIdentifier CreditReportTransactionIdentifier { get; set; }
    
        [XmlIgnore]
        public bool CreditReportTransactionIdentifierSpecified
        {
            get { return this.CreditReportTransactionIdentifier != null; }
            set { }
        }
    
        [XmlElement("CreditReportType", Order = 7)]
        public MISMOEnum<CreditReportBase> CreditReportType { get; set; }
    
        [XmlIgnore]
        public bool CreditReportTypeSpecified
        {
            get { return this.CreditReportType != null; }
            set { }
        }
    
        [XmlElement("CreditReportTypeOtherDescription", Order = 8)]
        public MISMOString CreditReportTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CreditReportTypeOtherDescriptionSpecified
        {
            get { return this.CreditReportTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("CreditRepositoriesSelectedCount", Order = 9)]
        public MISMOCount CreditRepositoriesSelectedCount { get; set; }
    
        [XmlIgnore]
        public bool CreditRepositoriesSelectedCountSpecified
        {
            get { return this.CreditRepositoriesSelectedCount != null; }
            set { }
        }
    
        [XmlElement("CreditRequestDatetime", Order = 10)]
        public MISMODatetime CreditRequestDatetime { get; set; }
    
        [XmlIgnore]
        public bool CreditRequestDatetimeSpecified
        {
            get { return this.CreditRequestDatetime != null; }
            set { }
        }
    
        [XmlElement("CreditRequestType", Order = 11)]
        public MISMOEnum<CreditRequestBase> CreditRequestType { get; set; }
    
        [XmlIgnore]
        public bool CreditRequestTypeSpecified
        {
            get { return this.CreditRequestType != null; }
            set { }
        }
    
        [XmlElement("InitialCreditReportFirstIssuedDate", Order = 12)]
        public MISMODate InitialCreditReportFirstIssuedDate { get; set; }
    
        [XmlIgnore]
        public bool InitialCreditReportFirstIssuedDateSpecified
        {
            get { return this.InitialCreditReportFirstIssuedDate != null; }
            set { }
        }
    
        [XmlElement("PaymentPatternRatingCodeType", Order = 13)]
        public MISMOEnum<PaymentPatternRatingCodeBase> PaymentPatternRatingCodeType { get; set; }
    
        [XmlIgnore]
        public bool PaymentPatternRatingCodeTypeSpecified
        {
            get { return this.PaymentPatternRatingCodeType != null; }
            set { }
        }
    
        [XmlElement("PaymentPatternRatingCodeTypeOtherDescription", Order = 14)]
        public MISMOString PaymentPatternRatingCodeTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PaymentPatternRatingCodeTypeOtherDescriptionSpecified
        {
            get { return this.PaymentPatternRatingCodeTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 15)]
        public CREDIT_REQUEST_DATA_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
