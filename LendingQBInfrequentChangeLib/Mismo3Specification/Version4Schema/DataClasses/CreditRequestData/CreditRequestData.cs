namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CREDIT_REQUEST_DATA
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AdditionalEndUsersSpecified
                    || this.CreditFreezePinsSpecified
                    || this.CreditRepositoryIncludedSpecified
                    || this.CreditRequestDataDetailSpecified
                    || this.CreditScoreModelsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ADDITIONAL_END_USERS", Order = 0)]
        public ADDITIONAL_END_USERS AdditionalEndUsers { get; set; }
    
        [XmlIgnore]
        public bool AdditionalEndUsersSpecified
        {
            get { return this.AdditionalEndUsers != null && this.AdditionalEndUsers.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_FREEZE_PINS", Order = 1)]
        public CREDIT_FREEZE_PINS CreditFreezePins { get; set; }
    
        [XmlIgnore]
        public bool CreditFreezePinsSpecified
        {
            get { return this.CreditFreezePins != null && this.CreditFreezePins.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_REPOSITORY_INCLUDED", Order = 2)]
        public CREDIT_REPOSITORY_INCLUDED CreditRepositoryIncluded { get; set; }
    
        [XmlIgnore]
        public bool CreditRepositoryIncludedSpecified
        {
            get { return this.CreditRepositoryIncluded != null && this.CreditRepositoryIncluded.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_REQUEST_DATA_DETAIL", Order = 3)]
        public CREDIT_REQUEST_DATA_DETAIL CreditRequestDataDetail { get; set; }
    
        [XmlIgnore]
        public bool CreditRequestDataDetailSpecified
        {
            get { return this.CreditRequestDataDetail != null && this.CreditRequestDataDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_SCORE_MODELS", Order = 4)]
        public CREDIT_SCORE_MODELS CreditScoreModels { get; set; }
    
        [XmlIgnore]
        public bool CreditScoreModelsSpecified
        {
            get { return this.CreditScoreModels != null && this.CreditScoreModels.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public CREDIT_REQUEST_DATA_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
