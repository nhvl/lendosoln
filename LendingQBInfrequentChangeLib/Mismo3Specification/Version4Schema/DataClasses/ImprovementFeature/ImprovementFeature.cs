namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class IMPROVEMENT_FEATURE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AmenitiesSpecified
                    || this.BathEquipmentsSpecified
                    || this.KitchenEquipmentsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AMENITIES", Order = 0)]
        public AMENITIES Amenities { get; set; }
    
        [XmlIgnore]
        public bool AmenitiesSpecified
        {
            get { return this.Amenities != null && this.Amenities.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("BATH_EQUIPMENTS", Order = 1)]
        public BATH_EQUIPMENTS BathEquipments { get; set; }
    
        [XmlIgnore]
        public bool BathEquipmentsSpecified
        {
            get { return this.BathEquipments != null && this.BathEquipments.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("KITCHEN_EQUIPMENTS", Order = 2)]
        public KITCHEN_EQUIPMENTS KitchenEquipments { get; set; }
    
        [XmlIgnore]
        public bool KitchenEquipmentsSpecified
        {
            get { return this.KitchenEquipments != null && this.KitchenEquipments.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public IMPROVEMENT_FEATURE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
