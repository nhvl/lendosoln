namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class CREDIT_CONSUMER_REFERRALS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditConsumerReferralListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CREDIT_CONSUMER_REFERRAL", Order = 0)]
        public List<CREDIT_CONSUMER_REFERRAL> CreditConsumerReferralList { get; set; } = new List<CREDIT_CONSUMER_REFERRAL>();
    
        [XmlIgnore]
        public bool CreditConsumerReferralListSpecified
        {
            get { return this.CreditConsumerReferralList != null && this.CreditConsumerReferralList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public CREDIT_CONSUMER_REFERRALS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
