namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CREDIT_CONSUMER_REFERRAL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AddressSpecified
                    || this.ContactPointsSpecified
                    || this.NameSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ADDRESS", Order = 0)]
        public ADDRESS Address { get; set; }
    
        [XmlIgnore]
        public bool AddressSpecified
        {
            get { return this.Address != null && this.Address.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CONTACT_POINTS", Order = 1)]
        public CONTACT_POINTS ContactPoints { get; set; }
    
        [XmlIgnore]
        public bool ContactPointsSpecified
        {
            get { return this.ContactPoints != null && this.ContactPoints.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("NAME", Order = 2)]
        public NAME Name { get; set; }
    
        [XmlIgnore]
        public bool NameSpecified
        {
            get { return this.Name != null && this.Name.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public CREDIT_CONSUMER_REFERRAL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
