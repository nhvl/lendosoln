namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CASH_TO_CLOSE_ITEM
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.IntegratedDisclosureCashToCloseItemAmountChangedIndicatorSpecified
                    || this.IntegratedDisclosureCashToCloseItemChangeDescriptionSpecified
                    || this.IntegratedDisclosureCashToCloseItemEstimatedAmountSpecified
                    || this.IntegratedDisclosureCashToCloseItemFinalAmountSpecified
                    || this.IntegratedDisclosureCashToCloseItemPaymentTypeSpecified
                    || this.IntegratedDisclosureCashToCloseItemTypeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("IntegratedDisclosureCashToCloseItemAmountChangedIndicator", Order = 0)]
        public MISMOIndicator IntegratedDisclosureCashToCloseItemAmountChangedIndicator { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureCashToCloseItemAmountChangedIndicatorSpecified
        {
            get { return this.IntegratedDisclosureCashToCloseItemAmountChangedIndicator != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureCashToCloseItemChangeDescription", Order = 1)]
        public MISMOString IntegratedDisclosureCashToCloseItemChangeDescription { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureCashToCloseItemChangeDescriptionSpecified
        {
            get { return this.IntegratedDisclosureCashToCloseItemChangeDescription != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureCashToCloseItemEstimatedAmount", Order = 2)]
        public MISMOAmount IntegratedDisclosureCashToCloseItemEstimatedAmount { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureCashToCloseItemEstimatedAmountSpecified
        {
            get { return this.IntegratedDisclosureCashToCloseItemEstimatedAmount != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureCashToCloseItemFinalAmount", Order = 3)]
        public MISMOAmount IntegratedDisclosureCashToCloseItemFinalAmount { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureCashToCloseItemFinalAmountSpecified
        {
            get { return this.IntegratedDisclosureCashToCloseItemFinalAmount != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureCashToCloseItemPaymentType", Order = 4)]
        public MISMOEnum<IntegratedDisclosureCashToCloseItemPaymentBase> IntegratedDisclosureCashToCloseItemPaymentType { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureCashToCloseItemPaymentTypeSpecified
        {
            get { return this.IntegratedDisclosureCashToCloseItemPaymentType != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureCashToCloseItemType", Order = 5)]
        public MISMOEnum<IntegratedDisclosureCashToCloseItemBase> IntegratedDisclosureCashToCloseItemType { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureCashToCloseItemTypeSpecified
        {
            get { return this.IntegratedDisclosureCashToCloseItemType != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 6)]
        public CASH_TO_CLOSE_ITEM_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
