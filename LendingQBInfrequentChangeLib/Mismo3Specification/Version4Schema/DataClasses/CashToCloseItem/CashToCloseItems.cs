namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class CASH_TO_CLOSE_ITEMS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CashToCloseItemListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CASH_TO_CLOSE_ITEM", Order = 0)]
        public List<CASH_TO_CLOSE_ITEM> CashToCloseItemList { get; set; } = new List<CASH_TO_CLOSE_ITEM>();
    
        [XmlIgnore]
        public bool CashToCloseItemListSpecified
        {
            get { return this.CashToCloseItemList != null && this.CashToCloseItemList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public CASH_TO_CLOSE_ITEMS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
