namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class COMMUNICATION_EVENT_ISSUES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CommunicationEventIssueListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("COMMUNICATION_EVENT_ISSUE", Order = 0)]
        public List<COMMUNICATION_EVENT_ISSUE> CommunicationEventIssueList { get; set; } = new List<COMMUNICATION_EVENT_ISSUE>();
    
        [XmlIgnore]
        public bool CommunicationEventIssueListSpecified
        {
            get { return this.CommunicationEventIssueList != null && this.CommunicationEventIssueList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public COMMUNICATION_EVENT_ISSUES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
