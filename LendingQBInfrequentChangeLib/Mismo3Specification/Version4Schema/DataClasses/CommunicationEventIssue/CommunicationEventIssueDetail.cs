namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class COMMUNICATION_EVENT_ISSUE_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AssertionOfErrorTypeSpecified
                    || this.AssertionOfErrorTypeAdditionalDescriptionSpecified
                    || this.AssertionOfErrorTypeOtherDescriptionSpecified
                    || this.IssueAcknowledgmentAndResolutionIndicatorSpecified
                    || this.IssueActionableIndicatorSpecified
                    || this.IssueActualInitialResponseDateSpecified
                    || this.IssueDesiredResolutionTextSpecified
                    || this.IssueIdentifierSpecified
                    || this.IssueRegulationRelatedIndicatorSpecified
                    || this.IssueRequiredInitialResponseDateSpecified
                    || this.IssueResolutionCommunicationDateSpecified
                    || this.IssueResolutionCorrectiveActionEffectiveDateSpecified
                    || this.IssueResolutionDueDateSpecified
                    || this.IssueResolutionMonetaryReliefAmountSpecified
                    || this.IssueResolutionMonetaryReliefCalculationDescriptionSpecified
                    || this.IssueResolutionMonetaryReliefGrantedDateSpecified
                    || this.IssueResolutionMonetaryReliefGrantedIndicatorSpecified
                    || this.IssueResolutionMonetaryReliefShouldBeGrantedIndicatorSpecified
                    || this.IssueResolutionResponderPolicyOrProcedureTextSpecified
                    || this.IssueResolutionSourceOfResponderPolicyOrProcedureTextSpecified
                    || this.IssueResolutionSummaryTextSpecified
                    || this.IssueResolutionSupportingRecordsSentToInitiatorIndicatorSpecified
                    || this.IssueResolutionTypeSpecified
                    || this.IssueResolutionTypeAdditionalDescriptionSpecified
                    || this.IssueResolutionTypeDeterminationDateSpecified
                    || this.IssueResolutionTypeOtherDescriptionSpecified
                    || this.IssueSelfIdentifiedIndicatorSpecified
                    || this.IssueTypeSpecified
                    || this.IssueTypeAdditionalDescriptionSpecified
                    || this.IssueTypeOtherDescriptionSpecified
                    || this.QualifiedWrittenRequestIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AssertionOfErrorType", Order = 0)]
        public MISMOEnum<AssertionOfErrorBase> AssertionOfErrorType { get; set; }
    
        [XmlIgnore]
        public bool AssertionOfErrorTypeSpecified
        {
            get { return this.AssertionOfErrorType != null; }
            set { }
        }
    
        [XmlElement("AssertionOfErrorTypeAdditionalDescription", Order = 1)]
        public MISMOString AssertionOfErrorTypeAdditionalDescription { get; set; }
    
        [XmlIgnore]
        public bool AssertionOfErrorTypeAdditionalDescriptionSpecified
        {
            get { return this.AssertionOfErrorTypeAdditionalDescription != null; }
            set { }
        }
    
        [XmlElement("AssertionOfErrorTypeOtherDescription", Order = 2)]
        public MISMOString AssertionOfErrorTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool AssertionOfErrorTypeOtherDescriptionSpecified
        {
            get { return this.AssertionOfErrorTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("IssueAcknowledgmentAndResolutionIndicator", Order = 3)]
        public MISMOIndicator IssueAcknowledgmentAndResolutionIndicator { get; set; }
    
        [XmlIgnore]
        public bool IssueAcknowledgmentAndResolutionIndicatorSpecified
        {
            get { return this.IssueAcknowledgmentAndResolutionIndicator != null; }
            set { }
        }
    
        [XmlElement("IssueActionableIndicator", Order = 4)]
        public MISMOIndicator IssueActionableIndicator { get; set; }
    
        [XmlIgnore]
        public bool IssueActionableIndicatorSpecified
        {
            get { return this.IssueActionableIndicator != null; }
            set { }
        }
    
        [XmlElement("IssueActualInitialResponseDate", Order = 5)]
        public MISMODate IssueActualInitialResponseDate { get; set; }
    
        [XmlIgnore]
        public bool IssueActualInitialResponseDateSpecified
        {
            get { return this.IssueActualInitialResponseDate != null; }
            set { }
        }
    
        [XmlElement("IssueDesiredResolutionText", Order = 6)]
        public MISMOString IssueDesiredResolutionText { get; set; }
    
        [XmlIgnore]
        public bool IssueDesiredResolutionTextSpecified
        {
            get { return this.IssueDesiredResolutionText != null; }
            set { }
        }
    
        [XmlElement("IssueIdentifier", Order = 7)]
        public MISMOIdentifier IssueIdentifier { get; set; }
    
        [XmlIgnore]
        public bool IssueIdentifierSpecified
        {
            get { return this.IssueIdentifier != null; }
            set { }
        }
    
        [XmlElement("IssueRegulationRelatedIndicator", Order = 8)]
        public MISMOIndicator IssueRegulationRelatedIndicator { get; set; }
    
        [XmlIgnore]
        public bool IssueRegulationRelatedIndicatorSpecified
        {
            get { return this.IssueRegulationRelatedIndicator != null; }
            set { }
        }
    
        [XmlElement("IssueRequiredInitialResponseDate", Order = 9)]
        public MISMODate IssueRequiredInitialResponseDate { get; set; }
    
        [XmlIgnore]
        public bool IssueRequiredInitialResponseDateSpecified
        {
            get { return this.IssueRequiredInitialResponseDate != null; }
            set { }
        }
    
        [XmlElement("IssueResolutionCommunicationDate", Order = 10)]
        public MISMODate IssueResolutionCommunicationDate { get; set; }
    
        [XmlIgnore]
        public bool IssueResolutionCommunicationDateSpecified
        {
            get { return this.IssueResolutionCommunicationDate != null; }
            set { }
        }
    
        [XmlElement("IssueResolutionCorrectiveActionEffectiveDate", Order = 11)]
        public MISMODate IssueResolutionCorrectiveActionEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool IssueResolutionCorrectiveActionEffectiveDateSpecified
        {
            get { return this.IssueResolutionCorrectiveActionEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("IssueResolutionDueDate", Order = 12)]
        public MISMODate IssueResolutionDueDate { get; set; }
    
        [XmlIgnore]
        public bool IssueResolutionDueDateSpecified
        {
            get { return this.IssueResolutionDueDate != null; }
            set { }
        }
    
        [XmlElement("IssueResolutionMonetaryReliefAmount", Order = 13)]
        public MISMOAmount IssueResolutionMonetaryReliefAmount { get; set; }
    
        [XmlIgnore]
        public bool IssueResolutionMonetaryReliefAmountSpecified
        {
            get { return this.IssueResolutionMonetaryReliefAmount != null; }
            set { }
        }
    
        [XmlElement("IssueResolutionMonetaryReliefCalculationDescription", Order = 14)]
        public MISMOString IssueResolutionMonetaryReliefCalculationDescription { get; set; }
    
        [XmlIgnore]
        public bool IssueResolutionMonetaryReliefCalculationDescriptionSpecified
        {
            get { return this.IssueResolutionMonetaryReliefCalculationDescription != null; }
            set { }
        }
    
        [XmlElement("IssueResolutionMonetaryReliefGrantedDate", Order = 15)]
        public MISMODate IssueResolutionMonetaryReliefGrantedDate { get; set; }
    
        [XmlIgnore]
        public bool IssueResolutionMonetaryReliefGrantedDateSpecified
        {
            get { return this.IssueResolutionMonetaryReliefGrantedDate != null; }
            set { }
        }
    
        [XmlElement("IssueResolutionMonetaryReliefGrantedIndicator", Order = 16)]
        public MISMOIndicator IssueResolutionMonetaryReliefGrantedIndicator { get; set; }
    
        [XmlIgnore]
        public bool IssueResolutionMonetaryReliefGrantedIndicatorSpecified
        {
            get { return this.IssueResolutionMonetaryReliefGrantedIndicator != null; }
            set { }
        }
    
        [XmlElement("IssueResolutionMonetaryReliefShouldBeGrantedIndicator", Order = 17)]
        public MISMOIndicator IssueResolutionMonetaryReliefShouldBeGrantedIndicator { get; set; }
    
        [XmlIgnore]
        public bool IssueResolutionMonetaryReliefShouldBeGrantedIndicatorSpecified
        {
            get { return this.IssueResolutionMonetaryReliefShouldBeGrantedIndicator != null; }
            set { }
        }
    
        [XmlElement("IssueResolutionResponderPolicyOrProcedureText", Order = 18)]
        public MISMOString IssueResolutionResponderPolicyOrProcedureText { get; set; }
    
        [XmlIgnore]
        public bool IssueResolutionResponderPolicyOrProcedureTextSpecified
        {
            get { return this.IssueResolutionResponderPolicyOrProcedureText != null; }
            set { }
        }
    
        [XmlElement("IssueResolutionSourceOfResponderPolicyOrProcedureText", Order = 19)]
        public MISMOString IssueResolutionSourceOfResponderPolicyOrProcedureText { get; set; }
    
        [XmlIgnore]
        public bool IssueResolutionSourceOfResponderPolicyOrProcedureTextSpecified
        {
            get { return this.IssueResolutionSourceOfResponderPolicyOrProcedureText != null; }
            set { }
        }
    
        [XmlElement("IssueResolutionSummaryText", Order = 20)]
        public MISMOString IssueResolutionSummaryText { get; set; }
    
        [XmlIgnore]
        public bool IssueResolutionSummaryTextSpecified
        {
            get { return this.IssueResolutionSummaryText != null; }
            set { }
        }
    
        [XmlElement("IssueResolutionSupportingRecordsSentToInitiatorIndicator", Order = 21)]
        public MISMOIndicator IssueResolutionSupportingRecordsSentToInitiatorIndicator { get; set; }
    
        [XmlIgnore]
        public bool IssueResolutionSupportingRecordsSentToInitiatorIndicatorSpecified
        {
            get { return this.IssueResolutionSupportingRecordsSentToInitiatorIndicator != null; }
            set { }
        }
    
        [XmlElement("IssueResolutionType", Order = 22)]
        public MISMOEnum<IssueResolutionBase> IssueResolutionType { get; set; }
    
        [XmlIgnore]
        public bool IssueResolutionTypeSpecified
        {
            get { return this.IssueResolutionType != null; }
            set { }
        }
    
        [XmlElement("IssueResolutionTypeAdditionalDescription", Order = 23)]
        public MISMOString IssueResolutionTypeAdditionalDescription { get; set; }
    
        [XmlIgnore]
        public bool IssueResolutionTypeAdditionalDescriptionSpecified
        {
            get { return this.IssueResolutionTypeAdditionalDescription != null; }
            set { }
        }
    
        [XmlElement("IssueResolutionTypeDeterminationDate", Order = 24)]
        public MISMODate IssueResolutionTypeDeterminationDate { get; set; }
    
        [XmlIgnore]
        public bool IssueResolutionTypeDeterminationDateSpecified
        {
            get { return this.IssueResolutionTypeDeterminationDate != null; }
            set { }
        }
    
        [XmlElement("IssueResolutionTypeOtherDescription", Order = 25)]
        public MISMOString IssueResolutionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool IssueResolutionTypeOtherDescriptionSpecified
        {
            get { return this.IssueResolutionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("IssueSelfIdentifiedIndicator", Order = 26)]
        public MISMOIndicator IssueSelfIdentifiedIndicator { get; set; }
    
        [XmlIgnore]
        public bool IssueSelfIdentifiedIndicatorSpecified
        {
            get { return this.IssueSelfIdentifiedIndicator != null; }
            set { }
        }
    
        [XmlElement("IssueType", Order = 27)]
        public MISMOEnum<IssueBase> IssueType { get; set; }
    
        [XmlIgnore]
        public bool IssueTypeSpecified
        {
            get { return this.IssueType != null; }
            set { }
        }
    
        [XmlElement("IssueTypeAdditionalDescription", Order = 28)]
        public MISMOString IssueTypeAdditionalDescription { get; set; }
    
        [XmlIgnore]
        public bool IssueTypeAdditionalDescriptionSpecified
        {
            get { return this.IssueTypeAdditionalDescription != null; }
            set { }
        }
    
        [XmlElement("IssueTypeOtherDescription", Order = 29)]
        public MISMOString IssueTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool IssueTypeOtherDescriptionSpecified
        {
            get { return this.IssueTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("QualifiedWrittenRequestIndicator", Order = 30)]
        public MISMOIndicator QualifiedWrittenRequestIndicator { get; set; }
    
        [XmlIgnore]
        public bool QualifiedWrittenRequestIndicatorSpecified
        {
            get { return this.QualifiedWrittenRequestIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 31)]
        public COMMUNICATION_EVENT_ISSUE_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
