namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class COMMUNICATION_EVENT_ISSUE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CommunicationEventIssueDetailSpecified
                    || this.CommunicationEventIssueFollowUpsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("COMMUNICATION_EVENT_ISSUE_DETAIL", Order = 0)]
        public COMMUNICATION_EVENT_ISSUE_DETAIL CommunicationEventIssueDetail { get; set; }
    
        [XmlIgnore]
        public bool CommunicationEventIssueDetailSpecified
        {
            get { return this.CommunicationEventIssueDetail != null && this.CommunicationEventIssueDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("COMMUNICATION_EVENT_ISSUE_FOLLOW_UPS", Order = 1)]
        public COMMUNICATION_EVENT_ISSUE_FOLLOW_UPS CommunicationEventIssueFollowUps { get; set; }
    
        [XmlIgnore]
        public bool CommunicationEventIssueFollowUpsSpecified
        {
            get { return this.CommunicationEventIssueFollowUps != null && this.CommunicationEventIssueFollowUps.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public COMMUNICATION_EVENT_ISSUE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
