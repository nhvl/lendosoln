namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MARKET_TREND
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MarketTrendPeriodTypeSpecified
                    || this.MarketTrendsAdverseFinancingIndicatorSpecified
                    || this.MarketTrendsForeclosureActivityIndicatorSpecified
                    || this.MarketTrendsHistoricAnalysisCommentDescriptionSpecified
                    || this.MarketTrendsHistoricPricesTypeSpecified
                    || this.MarketTrendsInterestRatesTypeSpecified
                    || this.MarketTrendsReconciliationCommentDescriptionSpecified
                    || this.MarketTrendsSalesActivityTypeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MarketTrendPeriodType", Order = 0)]
        public MISMOEnum<MarketTrendPeriodBase> MarketTrendPeriodType { get; set; }
    
        [XmlIgnore]
        public bool MarketTrendPeriodTypeSpecified
        {
            get { return this.MarketTrendPeriodType != null; }
            set { }
        }
    
        [XmlElement("MarketTrendsAdverseFinancingIndicator", Order = 1)]
        public MISMOIndicator MarketTrendsAdverseFinancingIndicator { get; set; }
    
        [XmlIgnore]
        public bool MarketTrendsAdverseFinancingIndicatorSpecified
        {
            get { return this.MarketTrendsAdverseFinancingIndicator != null; }
            set { }
        }
    
        [XmlElement("MarketTrendsForeclosureActivityIndicator", Order = 2)]
        public MISMOIndicator MarketTrendsForeclosureActivityIndicator { get; set; }
    
        [XmlIgnore]
        public bool MarketTrendsForeclosureActivityIndicatorSpecified
        {
            get { return this.MarketTrendsForeclosureActivityIndicator != null; }
            set { }
        }
    
        [XmlElement("MarketTrendsHistoricAnalysisCommentDescription", Order = 3)]
        public MISMOString MarketTrendsHistoricAnalysisCommentDescription { get; set; }
    
        [XmlIgnore]
        public bool MarketTrendsHistoricAnalysisCommentDescriptionSpecified
        {
            get { return this.MarketTrendsHistoricAnalysisCommentDescription != null; }
            set { }
        }
    
        [XmlElement("MarketTrendsHistoricPricesType", Order = 4)]
        public MISMOEnum<MarketTrendsHistoricPricesBase> MarketTrendsHistoricPricesType { get; set; }
    
        [XmlIgnore]
        public bool MarketTrendsHistoricPricesTypeSpecified
        {
            get { return this.MarketTrendsHistoricPricesType != null; }
            set { }
        }
    
        [XmlElement("MarketTrendsInterestRatesType", Order = 5)]
        public MISMOEnum<MarketTrendsInterestRatesBase> MarketTrendsInterestRatesType { get; set; }
    
        [XmlIgnore]
        public bool MarketTrendsInterestRatesTypeSpecified
        {
            get { return this.MarketTrendsInterestRatesType != null; }
            set { }
        }
    
        [XmlElement("MarketTrendsReconciliationCommentDescription", Order = 6)]
        public MISMOString MarketTrendsReconciliationCommentDescription { get; set; }
    
        [XmlIgnore]
        public bool MarketTrendsReconciliationCommentDescriptionSpecified
        {
            get { return this.MarketTrendsReconciliationCommentDescription != null; }
            set { }
        }
    
        [XmlElement("MarketTrendsSalesActivityType", Order = 7)]
        public MISMOEnum<MarketTrendsSalesActivityBase> MarketTrendsSalesActivityType { get; set; }
    
        [XmlIgnore]
        public bool MarketTrendsSalesActivityTypeSpecified
        {
            get { return this.MarketTrendsSalesActivityType != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 8)]
        public MARKET_TREND_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
