namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SERVICING_TRANSFER
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ServicingTransferRequestSpecified
                    || this.ServicingTransferResponseSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SERVICING_TRANSFER_REQUEST", Order = 0)]
        public SERVICING_TRANSFER_REQUEST ServicingTransferRequest { get; set; }
    
        [XmlIgnore]
        public bool ServicingTransferRequestSpecified
        {
            get { return this.ServicingTransferRequest != null && this.ServicingTransferRequest.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SERVICING_TRANSFER_RESPONSE", Order = 1)]
        public SERVICING_TRANSFER_RESPONSE ServicingTransferResponse { get; set; }
    
        [XmlIgnore]
        public bool ServicingTransferResponseSpecified
        {
            get { return this.ServicingTransferResponse != null && this.ServicingTransferResponse.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public SERVICING_TRANSFER_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
