namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PRODUCT_COMPONENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ProductComponentDescriptionSpecified
                    || this.ProductComponentIdentifierSpecified
                    || this.ProductComponentNameSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ProductComponentDescription", Order = 0)]
        public MISMOString ProductComponentDescription { get; set; }
    
        [XmlIgnore]
        public bool ProductComponentDescriptionSpecified
        {
            get { return this.ProductComponentDescription != null; }
            set { }
        }
    
        [XmlElement("ProductComponentIdentifier", Order = 1)]
        public MISMOIdentifier ProductComponentIdentifier { get; set; }
    
        [XmlIgnore]
        public bool ProductComponentIdentifierSpecified
        {
            get { return this.ProductComponentIdentifier != null; }
            set { }
        }
    
        [XmlElement("ProductComponentName", Order = 2)]
        public MISMOString ProductComponentName { get; set; }
    
        [XmlIgnore]
        public bool ProductComponentNameSpecified
        {
            get { return this.ProductComponentName != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public PRODUCT_COMPONENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
