namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class PRODUCT_COMPONENTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ProductComponentListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PRODUCT_COMPONENT", Order = 0)]
        public List<PRODUCT_COMPONENT> ProductComponentList { get; set; } = new List<PRODUCT_COMPONENT>();
    
        [XmlIgnore]
        public bool ProductComponentListSpecified
        {
            get { return this.ProductComponentList != null && this.ProductComponentList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public PRODUCT_COMPONENTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
