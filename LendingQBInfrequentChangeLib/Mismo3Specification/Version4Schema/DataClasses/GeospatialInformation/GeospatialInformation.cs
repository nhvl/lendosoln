namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class GEOSPATIAL_INFORMATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.GeospatialPropertyIdentifierSpecified
                    || this.LatitudeIdentifierSpecified
                    || this.LongitudeIdentifierSpecified
                    || this.LotBoundaryIdentifierSpecified
                    || this.NaturalLotLocationIdentifierSpecified
                    || this.NaturalUnitSpaceEntryIdentifierSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("GeospatialPropertyIdentifier", Order = 0)]
        public MISMOIdentifier GeospatialPropertyIdentifier { get; set; }
    
        [XmlIgnore]
        public bool GeospatialPropertyIdentifierSpecified
        {
            get { return this.GeospatialPropertyIdentifier != null; }
            set { }
        }
    
        [XmlElement("LatitudeIdentifier", Order = 1)]
        public MISMOIdentifier LatitudeIdentifier { get; set; }
    
        [XmlIgnore]
        public bool LatitudeIdentifierSpecified
        {
            get { return this.LatitudeIdentifier != null; }
            set { }
        }
    
        [XmlElement("LongitudeIdentifier", Order = 2)]
        public MISMOIdentifier LongitudeIdentifier { get; set; }
    
        [XmlIgnore]
        public bool LongitudeIdentifierSpecified
        {
            get { return this.LongitudeIdentifier != null; }
            set { }
        }
    
        [XmlElement("LotBoundaryIdentifier", Order = 3)]
        public MISMOIdentifier LotBoundaryIdentifier { get; set; }
    
        [XmlIgnore]
        public bool LotBoundaryIdentifierSpecified
        {
            get { return this.LotBoundaryIdentifier != null; }
            set { }
        }
    
        [XmlElement("NaturalLotLocationIdentifier", Order = 4)]
        public MISMOIdentifier NaturalLotLocationIdentifier { get; set; }
    
        [XmlIgnore]
        public bool NaturalLotLocationIdentifierSpecified
        {
            get { return this.NaturalLotLocationIdentifier != null; }
            set { }
        }
    
        [XmlElement("NaturalUnitSpaceEntryIdentifier", Order = 5)]
        public MISMOIdentifier NaturalUnitSpaceEntryIdentifier { get; set; }
    
        [XmlIgnore]
        public bool NaturalUnitSpaceEntryIdentifierSpecified
        {
            get { return this.NaturalUnitSpaceEntryIdentifier != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 6)]
        public GEOSPATIAL_INFORMATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
