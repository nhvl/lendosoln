namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class HIGH_COST_MORTGAGES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.HighCostMortgageListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("HIGH_COST_MORTGAGE", Order = 0)]
        public List<HIGH_COST_MORTGAGE> HighCostMortgageList { get; set; } = new List<HIGH_COST_MORTGAGE>();
    
        [XmlIgnore]
        public bool HighCostMortgageListSpecified
        {
            get { return this.HighCostMortgageList != null && this.HighCostMortgageList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public HIGH_COST_MORTGAGES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
