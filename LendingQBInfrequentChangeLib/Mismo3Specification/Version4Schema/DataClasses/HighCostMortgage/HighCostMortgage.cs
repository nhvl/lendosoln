namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class HIGH_COST_MORTGAGE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AveragePrimeOfferRatePercentSpecified
                    || this.HighCostJurisdictionNameSpecified
                    || this.HighCostJurisdictionTypeSpecified
                    || this.HighCostJurisdictionTypeOtherDescriptionSpecified
                    || this.HOEPA_APRInterestRatePercentSpecified
                    || this.HOEPARateThresholdPercentSpecified
                    || this.HOEPATotalPointsAndFeesThresholdAmountSpecified
                    || this.HOEPATotalPointsAndFeesThresholdPercentSpecified
                    || this.PreDiscountedInterestRatePercentSpecified
                    || this.RegulationZExcludedBonaFideDiscountPointsIndicatorSpecified
                    || this.RegulationZExcludedBonaFideDiscountPointsPercentSpecified
                    || this.RegulationZHighCostLoanDisclosureDeliveryDateSpecified
                    || this.RegulationZTotalAffiliateFeesAmountSpecified
                    || this.RegulationZTotalLoanAmountSpecified
                    || this.RegulationZTotalPointsAndFeesAmountSpecified
                    || this.RegulationZTotalPointsAndFeesPercentSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AveragePrimeOfferRatePercent", Order = 0)]
        public MISMOPercent AveragePrimeOfferRatePercent { get; set; }
    
        [XmlIgnore]
        public bool AveragePrimeOfferRatePercentSpecified
        {
            get { return this.AveragePrimeOfferRatePercent != null; }
            set { }
        }
    
        [XmlElement("HighCostJurisdictionName", Order = 1)]
        public MISMOString HighCostJurisdictionName { get; set; }
    
        [XmlIgnore]
        public bool HighCostJurisdictionNameSpecified
        {
            get { return this.HighCostJurisdictionName != null; }
            set { }
        }
    
        [XmlElement("HighCostJurisdictionType", Order = 2)]
        public MISMOEnum<HighCostJurisdictionBase> HighCostJurisdictionType { get; set; }
    
        [XmlIgnore]
        public bool HighCostJurisdictionTypeSpecified
        {
            get { return this.HighCostJurisdictionType != null; }
            set { }
        }
    
        [XmlElement("HighCostJurisdictionTypeOtherDescription", Order = 3)]
        public MISMOString HighCostJurisdictionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool HighCostJurisdictionTypeOtherDescriptionSpecified
        {
            get { return this.HighCostJurisdictionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("HOEPA_APRInterestRatePercent", Order = 4)]
        public MISMOPercent HOEPA_APRInterestRatePercent { get; set; }
    
        [XmlIgnore]
        public bool HOEPA_APRInterestRatePercentSpecified
        {
            get { return this.HOEPA_APRInterestRatePercent != null; }
            set { }
        }
    
        [XmlElement("HOEPARateThresholdPercent", Order = 5)]
        public MISMOPercent HOEPARateThresholdPercent { get; set; }
    
        [XmlIgnore]
        public bool HOEPARateThresholdPercentSpecified
        {
            get { return this.HOEPARateThresholdPercent != null; }
            set { }
        }
    
        [XmlElement("HOEPATotalPointsAndFeesThresholdAmount", Order = 6)]
        public MISMOAmount HOEPATotalPointsAndFeesThresholdAmount { get; set; }
    
        [XmlIgnore]
        public bool HOEPATotalPointsAndFeesThresholdAmountSpecified
        {
            get { return this.HOEPATotalPointsAndFeesThresholdAmount != null; }
            set { }
        }
    
        [XmlElement("HOEPATotalPointsAndFeesThresholdPercent", Order = 7)]
        public MISMOPercent HOEPATotalPointsAndFeesThresholdPercent { get; set; }
    
        [XmlIgnore]
        public bool HOEPATotalPointsAndFeesThresholdPercentSpecified
        {
            get { return this.HOEPATotalPointsAndFeesThresholdPercent != null; }
            set { }
        }
    
        [XmlElement("PreDiscountedInterestRatePercent", Order = 8)]
        public MISMOPercent PreDiscountedInterestRatePercent { get; set; }
    
        [XmlIgnore]
        public bool PreDiscountedInterestRatePercentSpecified
        {
            get { return this.PreDiscountedInterestRatePercent != null; }
            set { }
        }
    
        [XmlElement("RegulationZExcludedBonaFideDiscountPointsIndicator", Order = 9)]
        public MISMOIndicator RegulationZExcludedBonaFideDiscountPointsIndicator { get; set; }
    
        [XmlIgnore]
        public bool RegulationZExcludedBonaFideDiscountPointsIndicatorSpecified
        {
            get { return this.RegulationZExcludedBonaFideDiscountPointsIndicator != null; }
            set { }
        }
    
        [XmlElement("RegulationZExcludedBonaFideDiscountPointsPercent", Order = 10)]
        public MISMOPercent RegulationZExcludedBonaFideDiscountPointsPercent { get; set; }
    
        [XmlIgnore]
        public bool RegulationZExcludedBonaFideDiscountPointsPercentSpecified
        {
            get { return this.RegulationZExcludedBonaFideDiscountPointsPercent != null; }
            set { }
        }
    
        [XmlElement("RegulationZHighCostLoanDisclosureDeliveryDate", Order = 11)]
        public MISMODate RegulationZHighCostLoanDisclosureDeliveryDate { get; set; }
    
        [XmlIgnore]
        public bool RegulationZHighCostLoanDisclosureDeliveryDateSpecified
        {
            get { return this.RegulationZHighCostLoanDisclosureDeliveryDate != null; }
            set { }
        }
    
        [XmlElement("RegulationZTotalAffiliateFeesAmount", Order = 12)]
        public MISMOAmount RegulationZTotalAffiliateFeesAmount { get; set; }
    
        [XmlIgnore]
        public bool RegulationZTotalAffiliateFeesAmountSpecified
        {
            get { return this.RegulationZTotalAffiliateFeesAmount != null; }
            set { }
        }
    
        [XmlElement("RegulationZTotalLoanAmount", Order = 13)]
        public MISMOAmount RegulationZTotalLoanAmount { get; set; }
    
        [XmlIgnore]
        public bool RegulationZTotalLoanAmountSpecified
        {
            get { return this.RegulationZTotalLoanAmount != null; }
            set { }
        }
    
        [XmlElement("RegulationZTotalPointsAndFeesAmount", Order = 14)]
        public MISMOAmount RegulationZTotalPointsAndFeesAmount { get; set; }
    
        [XmlIgnore]
        public bool RegulationZTotalPointsAndFeesAmountSpecified
        {
            get { return this.RegulationZTotalPointsAndFeesAmount != null; }
            set { }
        }
    
        [XmlElement("RegulationZTotalPointsAndFeesPercent", Order = 15)]
        public MISMOPercent RegulationZTotalPointsAndFeesPercent { get; set; }
    
        [XmlIgnore]
        public bool RegulationZTotalPointsAndFeesPercentSpecified
        {
            get { return this.RegulationZTotalPointsAndFeesPercent != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 16)]
        public HIGH_COST_MORTGAGE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
