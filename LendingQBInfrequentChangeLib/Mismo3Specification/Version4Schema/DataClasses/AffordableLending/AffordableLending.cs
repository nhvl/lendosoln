namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class AFFORDABLE_LENDING
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CommunityLendingProductTypeSpecified
                    || this.CommunityLendingProductTypeOtherDescriptionSpecified
                    || this.CommunitySecondsRepaymentTypeSpecified
                    || this.CommunitySecondsRepaymentTypeOtherDescriptionSpecified
                    || this.FNMNeighborsMortgageEligibilityIndicatorSpecified
                    || this.HUDIncomeLimitAdjustmentPercentSpecified
                    || this.HUDLendingIncomeLimitAmountSpecified
                    || this.HUDMedianIncomeAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CommunityLendingProductType", Order = 0)]
        public MISMOEnum<CommunityLendingProductBase> CommunityLendingProductType { get; set; }
    
        [XmlIgnore]
        public bool CommunityLendingProductTypeSpecified
        {
            get { return this.CommunityLendingProductType != null; }
            set { }
        }
    
        [XmlElement("CommunityLendingProductTypeOtherDescription", Order = 1)]
        public MISMOString CommunityLendingProductTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CommunityLendingProductTypeOtherDescriptionSpecified
        {
            get { return this.CommunityLendingProductTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("CommunitySecondsRepaymentType", Order = 2)]
        public MISMOEnum<CommunitySecondsRepaymentBase> CommunitySecondsRepaymentType { get; set; }
    
        [XmlIgnore]
        public bool CommunitySecondsRepaymentTypeSpecified
        {
            get { return this.CommunitySecondsRepaymentType != null; }
            set { }
        }
    
        [XmlElement("CommunitySecondsRepaymentTypeOtherDescription", Order = 3)]
        public MISMOString CommunitySecondsRepaymentTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CommunitySecondsRepaymentTypeOtherDescriptionSpecified
        {
            get { return this.CommunitySecondsRepaymentTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("FNMNeighborsMortgageEligibilityIndicator", Order = 4)]
        public MISMOIndicator FNMNeighborsMortgageEligibilityIndicator { get; set; }
    
        [XmlIgnore]
        public bool FNMNeighborsMortgageEligibilityIndicatorSpecified
        {
            get { return this.FNMNeighborsMortgageEligibilityIndicator != null; }
            set { }
        }
    
        [XmlElement("HUDIncomeLimitAdjustmentPercent", Order = 5)]
        public MISMOPercent HUDIncomeLimitAdjustmentPercent { get; set; }
    
        [XmlIgnore]
        public bool HUDIncomeLimitAdjustmentPercentSpecified
        {
            get { return this.HUDIncomeLimitAdjustmentPercent != null; }
            set { }
        }
    
        [XmlElement("HUDLendingIncomeLimitAmount", Order = 6)]
        public MISMOAmount HUDLendingIncomeLimitAmount { get; set; }
    
        [XmlIgnore]
        public bool HUDLendingIncomeLimitAmountSpecified
        {
            get { return this.HUDLendingIncomeLimitAmount != null; }
            set { }
        }
    
        [XmlElement("HUDMedianIncomeAmount", Order = 7)]
        public MISMOAmount HUDMedianIncomeAmount { get; set; }
    
        [XmlIgnore]
        public bool HUDMedianIncomeAmountSpecified
        {
            get { return this.HUDMedianIncomeAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 8)]
        public AFFORDABLE_LENDING_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
