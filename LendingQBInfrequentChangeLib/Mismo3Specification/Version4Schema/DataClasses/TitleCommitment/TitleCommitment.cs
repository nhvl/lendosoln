namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class TITLE_COMMITMENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LegalAndVestingsSpecified
                    || this.PartiesSpecified
                    || this.TitleAdditionalExceptionsSpecified
                    || this.TitleAdditionalRequirementsSpecified
                    || this.TitleCommitmentDetailSpecified
                    || this.TitleCommitmentProposedPoliciesSpecified
                    || this.TitleExistingLoansAndLiabilitiesInformationSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LEGAL_AND_VESTINGS", Order = 0)]
        public LEGAL_AND_VESTINGS LegalAndVestings { get; set; }
    
        [XmlIgnore]
        public bool LegalAndVestingsSpecified
        {
            get { return this.LegalAndVestings != null && this.LegalAndVestings.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PARTIES", Order = 1)]
        public PARTIES Parties { get; set; }
    
        [XmlIgnore]
        public bool PartiesSpecified
        {
            get { return this.Parties != null && this.Parties.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("TITLE_ADDITIONAL_EXCEPTIONS", Order = 2)]
        public TITLE_ADDITIONAL_EXCEPTIONS TitleAdditionalExceptions { get; set; }
    
        [XmlIgnore]
        public bool TitleAdditionalExceptionsSpecified
        {
            get { return this.TitleAdditionalExceptions != null && this.TitleAdditionalExceptions.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("TITLE_ADDITIONAL_REQUIREMENTS", Order = 3)]
        public TITLE_ADDITIONAL_REQUIREMENTS TitleAdditionalRequirements { get; set; }
    
        [XmlIgnore]
        public bool TitleAdditionalRequirementsSpecified
        {
            get { return this.TitleAdditionalRequirements != null && this.TitleAdditionalRequirements.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("TITLE_COMMITMENT_DETAIL", Order = 4)]
        public TITLE_COMMITMENT_DETAIL TitleCommitmentDetail { get; set; }
    
        [XmlIgnore]
        public bool TitleCommitmentDetailSpecified
        {
            get { return this.TitleCommitmentDetail != null && this.TitleCommitmentDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("TITLE_COMMITMENT_PROPOSED_POLICIES", Order = 5)]
        public TITLE_COMMITMENT_PROPOSED_POLICIES TitleCommitmentProposedPolicies { get; set; }
    
        [XmlIgnore]
        public bool TitleCommitmentProposedPoliciesSpecified
        {
            get { return this.TitleCommitmentProposedPolicies != null && this.TitleCommitmentProposedPolicies.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("TITLE_EXISTING_LOANS_AND_LIABILITIES_INFORMATION", Order = 6)]
        public TITLE_EXISTING_LOANS_AND_LIABILITIES_INFORMATION TitleExistingLoansAndLiabilitiesInformation { get; set; }
    
        [XmlIgnore]
        public bool TitleExistingLoansAndLiabilitiesInformationSpecified
        {
            get { return this.TitleExistingLoansAndLiabilitiesInformation != null && this.TitleExistingLoansAndLiabilitiesInformation.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 7)]
        public TITLE_COMMITMENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
