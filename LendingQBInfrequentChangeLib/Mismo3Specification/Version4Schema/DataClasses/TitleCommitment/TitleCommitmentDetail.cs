namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class TITLE_COMMITMENT_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.TitleCommitmentIdentifierSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("TitleCommitmentIdentifier", Order = 0)]
        public MISMOIdentifier TitleCommitmentIdentifier { get; set; }
    
        [XmlIgnore]
        public bool TitleCommitmentIdentifierSpecified
        {
            get { return this.TitleCommitmentIdentifier != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public TITLE_COMMITMENT_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
