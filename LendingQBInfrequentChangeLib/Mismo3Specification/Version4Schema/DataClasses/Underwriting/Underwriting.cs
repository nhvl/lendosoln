namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class UNDERWRITING
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AutomatedUnderwritingsSpecified
                    || this.UnderwritingDetailSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AUTOMATED_UNDERWRITINGS", Order = 0)]
        public AUTOMATED_UNDERWRITINGS AutomatedUnderwritings { get; set; }
    
        [XmlIgnore]
        public bool AutomatedUnderwritingsSpecified
        {
            get { return this.AutomatedUnderwritings != null && this.AutomatedUnderwritings.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("UNDERWRITING_DETAIL", Order = 1)]
        public UNDERWRITING_DETAIL UnderwritingDetail { get; set; }
    
        [XmlIgnore]
        public bool UnderwritingDetailSpecified
        {
            get { return this.UnderwritingDetail != null && this.UnderwritingDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public UNDERWRITING_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
