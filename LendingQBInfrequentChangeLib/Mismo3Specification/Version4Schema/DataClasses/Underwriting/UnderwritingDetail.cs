namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class UNDERWRITING_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ContractualAgreementIndicatorSpecified
                    || this.ContractUnderwritingIndicatorSpecified
                    || this.LoanManualUnderwritingIndicatorSpecified
                    || this.LoanManualUnderwritingOrganizationNameSpecified
                    || this.LoanUnderwritingInvestorGuidelinesIndicatorSpecified
                    || this.LoanUnderwritingSubmitterTypeSpecified
                    || this.LoanUnderwritingSubmitterTypeOtherDescriptionSpecified
                    || this.UnderwritingCommentsDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ContractualAgreementIndicator", Order = 0)]
        public MISMOIndicator ContractualAgreementIndicator { get; set; }
    
        [XmlIgnore]
        public bool ContractualAgreementIndicatorSpecified
        {
            get { return this.ContractualAgreementIndicator != null; }
            set { }
        }
    
        [XmlElement("ContractUnderwritingIndicator", Order = 1)]
        public MISMOIndicator ContractUnderwritingIndicator { get; set; }
    
        [XmlIgnore]
        public bool ContractUnderwritingIndicatorSpecified
        {
            get { return this.ContractUnderwritingIndicator != null; }
            set { }
        }
    
        [XmlElement("LoanManualUnderwritingIndicator", Order = 2)]
        public MISMOIndicator LoanManualUnderwritingIndicator { get; set; }
    
        [XmlIgnore]
        public bool LoanManualUnderwritingIndicatorSpecified
        {
            get { return this.LoanManualUnderwritingIndicator != null; }
            set { }
        }
    
        [XmlElement("LoanManualUnderwritingOrganizationName", Order = 3)]
        public MISMOString LoanManualUnderwritingOrganizationName { get; set; }
    
        [XmlIgnore]
        public bool LoanManualUnderwritingOrganizationNameSpecified
        {
            get { return this.LoanManualUnderwritingOrganizationName != null; }
            set { }
        }
    
        [XmlElement("LoanUnderwritingInvestorGuidelinesIndicator", Order = 4)]
        public MISMOIndicator LoanUnderwritingInvestorGuidelinesIndicator { get; set; }
    
        [XmlIgnore]
        public bool LoanUnderwritingInvestorGuidelinesIndicatorSpecified
        {
            get { return this.LoanUnderwritingInvestorGuidelinesIndicator != null; }
            set { }
        }
    
        [XmlElement("LoanUnderwritingSubmitterType", Order = 5)]
        public MISMOEnum<LoanUnderwritingSubmitterBase> LoanUnderwritingSubmitterType { get; set; }
    
        [XmlIgnore]
        public bool LoanUnderwritingSubmitterTypeSpecified
        {
            get { return this.LoanUnderwritingSubmitterType != null; }
            set { }
        }
    
        [XmlElement("LoanUnderwritingSubmitterTypeOtherDescription", Order = 6)]
        public MISMOString LoanUnderwritingSubmitterTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool LoanUnderwritingSubmitterTypeOtherDescriptionSpecified
        {
            get { return this.LoanUnderwritingSubmitterTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("UnderwritingCommentsDescription", Order = 7)]
        public MISMOString UnderwritingCommentsDescription { get; set; }
    
        [XmlIgnore]
        public bool UnderwritingCommentsDescriptionSpecified
        {
            get { return this.UnderwritingCommentsDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 8)]
        public UNDERWRITING_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
