namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ATTIC
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AtticDetailSpecified
                    || this.AtticFeaturesSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ATTIC_DETAIL", Order = 0)]
        public ATTIC_DETAIL AtticDetail { get; set; }
    
        [XmlIgnore]
        public bool AtticDetailSpecified
        {
            get { return this.AtticDetail != null && this.AtticDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("ATTIC_FEATURES", Order = 1)]
        public ATTIC_FEATURES AtticFeatures { get; set; }
    
        [XmlIgnore]
        public bool AtticFeaturesSpecified
        {
            get { return this.AtticFeatures != null && this.AtticFeatures.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public ATTIC_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
