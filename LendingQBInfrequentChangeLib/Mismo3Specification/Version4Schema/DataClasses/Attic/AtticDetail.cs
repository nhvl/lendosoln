namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ATTIC_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ComponentAdjustmentAmountSpecified
                    || this.ConditionRatingDescriptionSpecified
                    || this.ConditionRatingTypeSpecified
                    || this.MaterialDescriptionSpecified
                    || this.QualityRatingDescriptionSpecified
                    || this.QualityRatingTypeSpecified
                    || this.SquareFeetNumberSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ComponentAdjustmentAmount", Order = 0)]
        public MISMOAmount ComponentAdjustmentAmount { get; set; }
    
        [XmlIgnore]
        public bool ComponentAdjustmentAmountSpecified
        {
            get { return this.ComponentAdjustmentAmount != null; }
            set { }
        }
    
        [XmlElement("ConditionRatingDescription", Order = 1)]
        public MISMOString ConditionRatingDescription { get; set; }
    
        [XmlIgnore]
        public bool ConditionRatingDescriptionSpecified
        {
            get { return this.ConditionRatingDescription != null; }
            set { }
        }
    
        [XmlElement("ConditionRatingType", Order = 2)]
        public MISMOEnum<ConditionRatingBase> ConditionRatingType { get; set; }
    
        [XmlIgnore]
        public bool ConditionRatingTypeSpecified
        {
            get { return this.ConditionRatingType != null; }
            set { }
        }
    
        [XmlElement("MaterialDescription", Order = 3)]
        public MISMOString MaterialDescription { get; set; }
    
        [XmlIgnore]
        public bool MaterialDescriptionSpecified
        {
            get { return this.MaterialDescription != null; }
            set { }
        }
    
        [XmlElement("QualityRatingDescription", Order = 4)]
        public MISMOString QualityRatingDescription { get; set; }
    
        [XmlIgnore]
        public bool QualityRatingDescriptionSpecified
        {
            get { return this.QualityRatingDescription != null; }
            set { }
        }
    
        [XmlElement("QualityRatingType", Order = 5)]
        public MISMOEnum<QualityRatingBase> QualityRatingType { get; set; }
    
        [XmlIgnore]
        public bool QualityRatingTypeSpecified
        {
            get { return this.QualityRatingType != null; }
            set { }
        }
    
        [XmlElement("SquareFeetNumber", Order = 6)]
        public MISMONumeric SquareFeetNumber { get; set; }
    
        [XmlIgnore]
        public bool SquareFeetNumberSpecified
        {
            get { return this.SquareFeetNumber != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 7)]
        public ATTIC_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
