namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PROJECT_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AdditionalProjectConsiderationsTypeSpecified
                    || this.AdditionalProjectConsiderationsTypeOtherDescriptionSpecified
                    || this.CondominiumProjectStatusTypeSpecified
                    || this.CondominiumPUDDeclarationsDescriptionSpecified
                    || this.FNMCondominiumProjectManagerProjectIdentifierSpecified
                    || this.ProjectAmenityImprovementsCompletedIndicatorSpecified
                    || this.ProjectAttachmentTypeSpecified
                    || this.ProjectClassificationIdentifierSpecified
                    || this.ProjectCommercialSpaceDescriptionSpecified
                    || this.ProjectCommercialSpaceIndicatorSpecified
                    || this.ProjectCommercialSpacePercentSpecified
                    || this.ProjectCommonElementLeasedIndicatorSpecified
                    || this.ProjectCommonElementLeaseTermsDescriptionSpecified
                    || this.ProjectContainsMultipleDwellingUnitsDataSourceDescriptionSpecified
                    || this.ProjectContainsMultipleDwellingUnitsIndicatorSpecified
                    || this.ProjectDesignTypeSpecified
                    || this.ProjectDesignTypeOtherDescriptionSpecified
                    || this.ProjectDwellingUnitCountSpecified
                    || this.ProjectDwellingUnitsSoldCountSpecified
                    || this.ProjectEligibilityDeterminationTypeSpecified
                    || this.ProjectLegalStructureTypeSpecified
                    || this.ProjectNameSpecified
                    || this.ProjectPrimaryUsageTypeSpecified
                    || this.PUDIndicatorSpecified
                    || this.TwoToFourUnitCondominiumProjectIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AdditionalProjectConsiderationsType", Order = 0)]
        public MISMOEnum<AdditionalProjectConsiderationsBase> AdditionalProjectConsiderationsType { get; set; }
    
        [XmlIgnore]
        public bool AdditionalProjectConsiderationsTypeSpecified
        {
            get { return this.AdditionalProjectConsiderationsType != null; }
            set { }
        }
    
        [XmlElement("AdditionalProjectConsiderationsTypeOtherDescription", Order = 1)]
        public MISMOString AdditionalProjectConsiderationsTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool AdditionalProjectConsiderationsTypeOtherDescriptionSpecified
        {
            get { return this.AdditionalProjectConsiderationsTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("CondominiumProjectStatusType", Order = 2)]
        public MISMOEnum<CondominiumProjectStatusBase> CondominiumProjectStatusType { get; set; }
    
        [XmlIgnore]
        public bool CondominiumProjectStatusTypeSpecified
        {
            get { return this.CondominiumProjectStatusType != null; }
            set { }
        }
    
        [XmlElement("CondominiumPUDDeclarationsDescription", Order = 3)]
        public MISMOString CondominiumPUDDeclarationsDescription { get; set; }
    
        [XmlIgnore]
        public bool CondominiumPUDDeclarationsDescriptionSpecified
        {
            get { return this.CondominiumPUDDeclarationsDescription != null; }
            set { }
        }
    
        [XmlElement("FNMCondominiumProjectManagerProjectIdentifier", Order = 4)]
        public MISMOIdentifier FNMCondominiumProjectManagerProjectIdentifier { get; set; }
    
        [XmlIgnore]
        public bool FNMCondominiumProjectManagerProjectIdentifierSpecified
        {
            get { return this.FNMCondominiumProjectManagerProjectIdentifier != null; }
            set { }
        }
    
        [XmlElement("ProjectAmenityImprovementsCompletedIndicator", Order = 5)]
        public MISMOIndicator ProjectAmenityImprovementsCompletedIndicator { get; set; }
    
        [XmlIgnore]
        public bool ProjectAmenityImprovementsCompletedIndicatorSpecified
        {
            get { return this.ProjectAmenityImprovementsCompletedIndicator != null; }
            set { }
        }
    
        [XmlElement("ProjectAttachmentType", Order = 6)]
        public MISMOEnum<ProjectAttachmentBase> ProjectAttachmentType { get; set; }
    
        [XmlIgnore]
        public bool ProjectAttachmentTypeSpecified
        {
            get { return this.ProjectAttachmentType != null; }
            set { }
        }
    
        [XmlElement("ProjectClassificationIdentifier", Order = 7)]
        public MISMOIdentifier ProjectClassificationIdentifier { get; set; }
    
        [XmlIgnore]
        public bool ProjectClassificationIdentifierSpecified
        {
            get { return this.ProjectClassificationIdentifier != null; }
            set { }
        }
    
        [XmlElement("ProjectCommercialSpaceDescription", Order = 8)]
        public MISMOString ProjectCommercialSpaceDescription { get; set; }
    
        [XmlIgnore]
        public bool ProjectCommercialSpaceDescriptionSpecified
        {
            get { return this.ProjectCommercialSpaceDescription != null; }
            set { }
        }
    
        [XmlElement("ProjectCommercialSpaceIndicator", Order = 9)]
        public MISMOIndicator ProjectCommercialSpaceIndicator { get; set; }
    
        [XmlIgnore]
        public bool ProjectCommercialSpaceIndicatorSpecified
        {
            get { return this.ProjectCommercialSpaceIndicator != null; }
            set { }
        }
    
        [XmlElement("ProjectCommercialSpacePercent", Order = 10)]
        public MISMOPercent ProjectCommercialSpacePercent { get; set; }
    
        [XmlIgnore]
        public bool ProjectCommercialSpacePercentSpecified
        {
            get { return this.ProjectCommercialSpacePercent != null; }
            set { }
        }
    
        [XmlElement("ProjectCommonElementLeasedIndicator", Order = 11)]
        public MISMOIndicator ProjectCommonElementLeasedIndicator { get; set; }
    
        [XmlIgnore]
        public bool ProjectCommonElementLeasedIndicatorSpecified
        {
            get { return this.ProjectCommonElementLeasedIndicator != null; }
            set { }
        }
    
        [XmlElement("ProjectCommonElementLeaseTermsDescription", Order = 12)]
        public MISMOString ProjectCommonElementLeaseTermsDescription { get; set; }
    
        [XmlIgnore]
        public bool ProjectCommonElementLeaseTermsDescriptionSpecified
        {
            get { return this.ProjectCommonElementLeaseTermsDescription != null; }
            set { }
        }
    
        [XmlElement("ProjectContainsMultipleDwellingUnitsDataSourceDescription", Order = 13)]
        public MISMOString ProjectContainsMultipleDwellingUnitsDataSourceDescription { get; set; }
    
        [XmlIgnore]
        public bool ProjectContainsMultipleDwellingUnitsDataSourceDescriptionSpecified
        {
            get { return this.ProjectContainsMultipleDwellingUnitsDataSourceDescription != null; }
            set { }
        }
    
        [XmlElement("ProjectContainsMultipleDwellingUnitsIndicator", Order = 14)]
        public MISMOIndicator ProjectContainsMultipleDwellingUnitsIndicator { get; set; }
    
        [XmlIgnore]
        public bool ProjectContainsMultipleDwellingUnitsIndicatorSpecified
        {
            get { return this.ProjectContainsMultipleDwellingUnitsIndicator != null; }
            set { }
        }
    
        [XmlElement("ProjectDesignType", Order = 15)]
        public MISMOEnum<ProjectDesignBase> ProjectDesignType { get; set; }
    
        [XmlIgnore]
        public bool ProjectDesignTypeSpecified
        {
            get { return this.ProjectDesignType != null; }
            set { }
        }
    
        [XmlElement("ProjectDesignTypeOtherDescription", Order = 16)]
        public MISMOString ProjectDesignTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ProjectDesignTypeOtherDescriptionSpecified
        {
            get { return this.ProjectDesignTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ProjectDwellingUnitCount", Order = 17)]
        public MISMOCount ProjectDwellingUnitCount { get; set; }
    
        [XmlIgnore]
        public bool ProjectDwellingUnitCountSpecified
        {
            get { return this.ProjectDwellingUnitCount != null; }
            set { }
        }
    
        [XmlElement("ProjectDwellingUnitsSoldCount", Order = 18)]
        public MISMOCount ProjectDwellingUnitsSoldCount { get; set; }
    
        [XmlIgnore]
        public bool ProjectDwellingUnitsSoldCountSpecified
        {
            get { return this.ProjectDwellingUnitsSoldCount != null; }
            set { }
        }
    
        [XmlElement("ProjectEligibilityDeterminationType", Order = 19)]
        public MISMOEnum<ProjectEligibilityDeterminationBase> ProjectEligibilityDeterminationType { get; set; }
    
        [XmlIgnore]
        public bool ProjectEligibilityDeterminationTypeSpecified
        {
            get { return this.ProjectEligibilityDeterminationType != null; }
            set { }
        }
    
        [XmlElement("ProjectLegalStructureType", Order = 20)]
        public MISMOEnum<ProjectLegalStructureBase> ProjectLegalStructureType { get; set; }
    
        [XmlIgnore]
        public bool ProjectLegalStructureTypeSpecified
        {
            get { return this.ProjectLegalStructureType != null; }
            set { }
        }
    
        [XmlElement("ProjectName", Order = 21)]
        public MISMOString ProjectName { get; set; }
    
        [XmlIgnore]
        public bool ProjectNameSpecified
        {
            get { return this.ProjectName != null; }
            set { }
        }
    
        [XmlElement("ProjectPrimaryUsageType", Order = 22)]
        public MISMOEnum<ProjectPrimaryUsageBase> ProjectPrimaryUsageType { get; set; }
    
        [XmlIgnore]
        public bool ProjectPrimaryUsageTypeSpecified
        {
            get { return this.ProjectPrimaryUsageType != null; }
            set { }
        }
    
        [XmlElement("PUDIndicator", Order = 23)]
        public MISMOIndicator PUDIndicator { get; set; }
    
        [XmlIgnore]
        public bool PUDIndicatorSpecified
        {
            get { return this.PUDIndicator != null; }
            set { }
        }
    
        [XmlElement("TwoToFourUnitCondominiumProjectIndicator", Order = 24)]
        public MISMOIndicator TwoToFourUnitCondominiumProjectIndicator { get; set; }
    
        [XmlIgnore]
        public bool TwoToFourUnitCondominiumProjectIndicatorSpecified
        {
            get { return this.TwoToFourUnitCondominiumProjectIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 25)]
        public PROJECT_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
