namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PROJECT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DataSourcesSpecified
                    || this.PhasesSpecified
                    || this.ProjectAnalysisSpecified
                    || this.ProjectBlanketFinancingsSpecified
                    || this.PROJECT_CAR_STORAGESSpecified
                    || this.PROJECT_COMMON_ELEMENTSSpecified
                    || this.PROJECT_CONVERSIONSpecified
                    || this.ProjectDetailSpecified
                    || this.ProjectDeveloperSpecified
                    || this.ProjectFinancialInformationSpecified
                    || this.PROJECT_HOUSING_UNIT_INVENTORIESSpecified
                    || this.ProjectManagementSpecified
                    || this.ProjectOwnershipSpecified
                    || this.ProjectRatingsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DATA_SOURCES", Order = 0)]
        public DATA_SOURCES DataSources { get; set; }
    
        [XmlIgnore]
        public bool DataSourcesSpecified
        {
            get { return this.DataSources != null && this.DataSources.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PHASES", Order = 1)]
        public PHASES Phases { get; set; }
    
        [XmlIgnore]
        public bool PhasesSpecified
        {
            get { return this.Phases != null && this.Phases.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PROJECT_ANALYSIS", Order = 2)]
        public PROJECT_ANALYSIS ProjectAnalysis { get; set; }
    
        [XmlIgnore]
        public bool ProjectAnalysisSpecified
        {
            get { return this.ProjectAnalysis != null && this.ProjectAnalysis.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PROJECT_BLANKET_FINANCINGS", Order = 3)]
        public PROJECT_BLANKET_FINANCINGS ProjectBlanketFinancings { get; set; }
    
        [XmlIgnore]
        public bool ProjectBlanketFinancingsSpecified
        {
            get { return this.ProjectBlanketFinancings != null && this.ProjectBlanketFinancings.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PROJECT_CAR_STORAGES", Order = 4)]
        public CAR_STORAGES PROJECT_CAR_STORAGES { get; set; }
    
        [XmlIgnore]
        public bool PROJECT_CAR_STORAGESSpecified
        {
            get { return this.PROJECT_CAR_STORAGES != null; }
            set { }
        }
    
        [XmlElement("PROJECT_COMMON_ELEMENTS", Order = 5)]
        public COMMON_ELEMENTS PROJECT_COMMON_ELEMENTS { get; set; }
    
        [XmlIgnore]
        public bool PROJECT_COMMON_ELEMENTSSpecified
        {
            get { return this.PROJECT_COMMON_ELEMENTS != null; }
            set { }
        }
    
        [XmlElement("PROJECT_CONVERSION", Order = 6)]
        public CONVERSION PROJECT_CONVERSION { get; set; }
    
        [XmlIgnore]
        public bool PROJECT_CONVERSIONSpecified
        {
            get { return this.PROJECT_CONVERSION != null; }
            set { }
        }
    
        [XmlElement("PROJECT_DETAIL", Order = 7)]
        public PROJECT_DETAIL ProjectDetail { get; set; }
    
        [XmlIgnore]
        public bool ProjectDetailSpecified
        {
            get { return this.ProjectDetail != null && this.ProjectDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PROJECT_DEVELOPER", Order = 8)]
        public PROJECT_DEVELOPER ProjectDeveloper { get; set; }
    
        [XmlIgnore]
        public bool ProjectDeveloperSpecified
        {
            get { return this.ProjectDeveloper != null && this.ProjectDeveloper.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PROJECT_FINANCIAL_INFORMATION", Order = 9)]
        public PROJECT_FINANCIAL_INFORMATION ProjectFinancialInformation { get; set; }
    
        [XmlIgnore]
        public bool ProjectFinancialInformationSpecified
        {
            get { return this.ProjectFinancialInformation != null && this.ProjectFinancialInformation.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PROJECT_HOUSING_UNIT_INVENTORIES", Order = 10)]
        public HOUSING_UNIT_INVENTORIES PROJECT_HOUSING_UNIT_INVENTORIES { get; set; }
    
        [XmlIgnore]
        public bool PROJECT_HOUSING_UNIT_INVENTORIESSpecified
        {
            get { return this.PROJECT_HOUSING_UNIT_INVENTORIES != null; }
            set { }
        }
    
        [XmlElement("PROJECT_MANAGEMENT", Order = 11)]
        public PROJECT_MANAGEMENT ProjectManagement { get; set; }
    
        [XmlIgnore]
        public bool ProjectManagementSpecified
        {
            get { return this.ProjectManagement != null && this.ProjectManagement.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PROJECT_OWNERSHIP", Order = 12)]
        public PROJECT_OWNERSHIP ProjectOwnership { get; set; }
    
        [XmlIgnore]
        public bool ProjectOwnershipSpecified
        {
            get { return this.ProjectOwnership != null && this.ProjectOwnership.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PROJECT_RATINGS", Order = 13)]
        public PROJECT_RATINGS ProjectRatings { get; set; }
    
        [XmlIgnore]
        public bool ProjectRatingsSpecified
        {
            get { return this.ProjectRatings != null && this.ProjectRatings.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 14)]
        public PROJECT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
