namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ESTIMATED_PROPERTY_COST
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EstimatedPropertyCostComponentsSpecified
                    || this.EstimatedPropertyCostDetailSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ESTIMATED_PROPERTY_COST_COMPONENTS", Order = 0)]
        public ESTIMATED_PROPERTY_COST_COMPONENTS EstimatedPropertyCostComponents { get; set; }
    
        [XmlIgnore]
        public bool EstimatedPropertyCostComponentsSpecified
        {
            get { return this.EstimatedPropertyCostComponents != null && this.EstimatedPropertyCostComponents.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("ESTIMATED_PROPERTY_COST_DETAIL", Order = 1)]
        public ESTIMATED_PROPERTY_COST_DETAIL EstimatedPropertyCostDetail { get; set; }
    
        [XmlIgnore]
        public bool EstimatedPropertyCostDetailSpecified
        {
            get { return this.EstimatedPropertyCostDetail != null && this.EstimatedPropertyCostDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public ESTIMATED_PROPERTY_COST_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
