namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ESTIMATED_PROPERTY_COST_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ProjectedPaymentEstimatedTaxesInsuranceAssessmentTotalAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ProjectedPaymentEstimatedTaxesInsuranceAssessmentTotalAmount", Order = 0)]
        public MISMOAmount ProjectedPaymentEstimatedTaxesInsuranceAssessmentTotalAmount { get; set; }
    
        [XmlIgnore]
        public bool ProjectedPaymentEstimatedTaxesInsuranceAssessmentTotalAmountSpecified
        {
            get { return this.ProjectedPaymentEstimatedTaxesInsuranceAssessmentTotalAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public ESTIMATED_PROPERTY_COST_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
