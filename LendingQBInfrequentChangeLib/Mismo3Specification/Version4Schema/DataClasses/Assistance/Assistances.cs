namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class ASSISTANCES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AssistanceListSpecified
                    || this.AssistanceSummarySpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ASSISTANCE", Order = 0)]
        public List<ASSISTANCE> AssistanceList { get; set; } = new List<ASSISTANCE>();
    
        [XmlIgnore]
        public bool AssistanceListSpecified
        {
            get { return this.AssistanceList != null && this.AssistanceList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("ASSISTANCE_SUMMARY", Order = 1)]
        public ASSISTANCE_SUMMARY AssistanceSummary { get; set; }
    
        [XmlIgnore]
        public bool AssistanceSummarySpecified
        {
            get { return this.AssistanceSummary != null && this.AssistanceSummary.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public ASSISTANCES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
