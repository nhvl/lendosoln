namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ASSISTANCE_SUMMARY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AssistanceActualDisbursementEndDateSpecified
                    || this.AssistanceScheduledDisbursementEndDateSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AssistanceActualDisbursementEndDate", Order = 0)]
        public MISMODate AssistanceActualDisbursementEndDate { get; set; }
    
        [XmlIgnore]
        public bool AssistanceActualDisbursementEndDateSpecified
        {
            get { return this.AssistanceActualDisbursementEndDate != null; }
            set { }
        }
    
        [XmlElement("AssistanceScheduledDisbursementEndDate", Order = 1)]
        public MISMODate AssistanceScheduledDisbursementEndDate { get; set; }
    
        [XmlIgnore]
        public bool AssistanceScheduledDisbursementEndDateSpecified
        {
            get { return this.AssistanceScheduledDisbursementEndDate != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public ASSISTANCE_SUMMARY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
