namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ASSISTANCE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AssistanceActualDisbursementDateSpecified
                    || this.AssistanceAmountSpecified
                    || this.AssistanceMaximumAmountSpecified
                    || this.AssistancePeriodMonthsCountSpecified
                    || this.AssistanceRecipientTypeSpecified
                    || this.AssistanceRecipientTypeOtherDescriptionSpecified
                    || this.AssistanceScheduledDisbursementDateSpecified
                    || this.AssistanceSourceTypeSpecified
                    || this.AssistanceSourceTypeOtherDescriptionSpecified
                    || this.AssistanceTypeSpecified
                    || this.AssistanceTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AssistanceActualDisbursementDate", Order = 0)]
        public MISMODate AssistanceActualDisbursementDate { get; set; }
    
        [XmlIgnore]
        public bool AssistanceActualDisbursementDateSpecified
        {
            get { return this.AssistanceActualDisbursementDate != null; }
            set { }
        }
    
        [XmlElement("AssistanceAmount", Order = 1)]
        public MISMOAmount AssistanceAmount { get; set; }
    
        [XmlIgnore]
        public bool AssistanceAmountSpecified
        {
            get { return this.AssistanceAmount != null; }
            set { }
        }
    
        [XmlElement("AssistanceMaximumAmount", Order = 2)]
        public MISMOAmount AssistanceMaximumAmount { get; set; }
    
        [XmlIgnore]
        public bool AssistanceMaximumAmountSpecified
        {
            get { return this.AssistanceMaximumAmount != null; }
            set { }
        }
    
        [XmlElement("AssistancePeriodMonthsCount", Order = 3)]
        public MISMOCount AssistancePeriodMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool AssistancePeriodMonthsCountSpecified
        {
            get { return this.AssistancePeriodMonthsCount != null; }
            set { }
        }
    
        [XmlElement("AssistanceRecipientType", Order = 4)]
        public MISMOEnum<AssistanceRecipientBase> AssistanceRecipientType { get; set; }
    
        [XmlIgnore]
        public bool AssistanceRecipientTypeSpecified
        {
            get { return this.AssistanceRecipientType != null; }
            set { }
        }
    
        [XmlElement("AssistanceRecipientTypeOtherDescription", Order = 5)]
        public MISMOString AssistanceRecipientTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool AssistanceRecipientTypeOtherDescriptionSpecified
        {
            get { return this.AssistanceRecipientTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("AssistanceScheduledDisbursementDate", Order = 6)]
        public MISMODate AssistanceScheduledDisbursementDate { get; set; }
    
        [XmlIgnore]
        public bool AssistanceScheduledDisbursementDateSpecified
        {
            get { return this.AssistanceScheduledDisbursementDate != null; }
            set { }
        }
    
        [XmlElement("AssistanceSourceType", Order = 7)]
        public MISMOEnum<AssistanceSourceBase> AssistanceSourceType { get; set; }
    
        [XmlIgnore]
        public bool AssistanceSourceTypeSpecified
        {
            get { return this.AssistanceSourceType != null; }
            set { }
        }
    
        [XmlElement("AssistanceSourceTypeOtherDescription", Order = 8)]
        public MISMOString AssistanceSourceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool AssistanceSourceTypeOtherDescriptionSpecified
        {
            get { return this.AssistanceSourceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("AssistanceType", Order = 9)]
        public MISMOEnum<AssistanceBase> AssistanceType { get; set; }
    
        [XmlIgnore]
        public bool AssistanceTypeSpecified
        {
            get { return this.AssistanceType != null; }
            set { }
        }
    
        [XmlElement("AssistanceTypeOtherDescription", Order = 10)]
        public MISMOString AssistanceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool AssistanceTypeOtherDescriptionSpecified
        {
            get { return this.AssistanceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 11)]
        public ASSISTANCE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
