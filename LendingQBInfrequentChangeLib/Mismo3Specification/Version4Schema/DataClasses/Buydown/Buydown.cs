namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class BUYDOWN
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BuydownFundsSpecified
                    || this.BuydownOccurrencesSpecified
                    || this.BuydownRuleSpecified
                    || this.BuydownSchedulesSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BUYDOWN_FUNDS", Order = 0)]
        public BUYDOWN_FUNDS BuydownFunds { get; set; }
    
        [XmlIgnore]
        public bool BuydownFundsSpecified
        {
            get { return this.BuydownFunds != null && this.BuydownFunds.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("BUYDOWN_OCCURRENCES", Order = 1)]
        public BUYDOWN_OCCURRENCES BuydownOccurrences { get; set; }
    
        [XmlIgnore]
        public bool BuydownOccurrencesSpecified
        {
            get { return this.BuydownOccurrences != null && this.BuydownOccurrences.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("BUYDOWN_RULE", Order = 2)]
        public BUYDOWN_RULE BuydownRule { get; set; }
    
        [XmlIgnore]
        public bool BuydownRuleSpecified
        {
            get { return this.BuydownRule != null && this.BuydownRule.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("BUYDOWN_SCHEDULES", Order = 3)]
        public BUYDOWN_SCHEDULES BuydownSchedules { get; set; }
    
        [XmlIgnore]
        public bool BuydownSchedulesSpecified
        {
            get { return this.BuydownSchedules != null && this.BuydownSchedules.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public BUYDOWN_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
