namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class SYSTEM_SIGNATURES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SystemSignatureListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SYSTEM_SIGNATURE", Order = 0)]
        public List<SYSTEM_SIGNATURE> SystemSignatureList { get; set; } = new List<SYSTEM_SIGNATURE>();
    
        [XmlIgnore]
        public bool SystemSignatureListSpecified
        {
            get { return this.SystemSignatureList != null && this.SystemSignatureList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public SYSTEM_SIGNATURES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
