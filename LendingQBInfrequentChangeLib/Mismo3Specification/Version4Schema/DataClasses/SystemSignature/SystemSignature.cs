namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SYSTEM_SIGNATURE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExtensionSpecified
                    || this.AnySpecified;
            }
        }
    
        [XmlElement("EXTENSION", Order = 0)]
        public SYSTEM_SIGNATURE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("Any", Order = 1)]
        public System.Xml.XmlElement Any { get; set; }
    
        [XmlIgnore]
        public bool AnySpecified
        {
            get { return this.Any != null; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
