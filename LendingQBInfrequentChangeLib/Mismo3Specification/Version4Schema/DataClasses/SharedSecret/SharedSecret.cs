namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SHARED_SECRET
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SharedSecretPromptTextSpecified
                    || this.SharedSecretResponseTextSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SharedSecretPromptText", Order = 0)]
        public MISMOString SharedSecretPromptText { get; set; }
    
        [XmlIgnore]
        public bool SharedSecretPromptTextSpecified
        {
            get { return this.SharedSecretPromptText != null; }
            set { }
        }
    
        [XmlElement("SharedSecretResponseText", Order = 1)]
        public MISMOString SharedSecretResponseText { get; set; }
    
        [XmlIgnore]
        public bool SharedSecretResponseTextSpecified
        {
            get { return this.SharedSecretResponseText != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public SHARED_SECRET_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
