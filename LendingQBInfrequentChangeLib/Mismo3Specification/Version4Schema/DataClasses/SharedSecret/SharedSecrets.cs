namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class SHARED_SECRETS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SharedSecretListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SHARED_SECRET", Order = 0)]
        public List<SHARED_SECRET> SharedSecretList { get; set; } = new List<SHARED_SECRET>();
    
        [XmlIgnore]
        public bool SharedSecretListSpecified
        {
            get { return this.SharedSecretList != null && this.SharedSecretList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public SHARED_SECRETS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
