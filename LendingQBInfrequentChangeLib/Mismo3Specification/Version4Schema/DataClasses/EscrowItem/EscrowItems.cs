namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class ESCROW_ITEMS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EscrowItemListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ESCROW_ITEM", Order = 0)]
        public List<ESCROW_ITEM> EscrowItemList { get; set; } = new List<ESCROW_ITEM>();
    
        [XmlIgnore]
        public bool EscrowItemListSpecified
        {
            get { return this.EscrowItemList != null && this.EscrowItemList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public ESCROW_ITEMS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
