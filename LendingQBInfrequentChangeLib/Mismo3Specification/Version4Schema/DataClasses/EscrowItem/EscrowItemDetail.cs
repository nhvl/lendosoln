namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ESCROW_ITEM_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BorrowerChosenProviderIndicatorSpecified
                    || this.EscrowAnnualPaymentAmountSpecified
                    || this.EscrowCollectedNumberOfMonthsCountSpecified
                    || this.EscrowInsurancePolicyIdentifierSpecified
                    || this.EscrowItemActualTotalAmountSpecified
                    || this.EscrowItemCategoryTypeSpecified
                    || this.EscrowItemCategoryTypeOtherDescriptionSpecified
                    || this.EscrowItemEstimatedTotalAmountSpecified
                    || this.EscrowItemLastDisbursementDateSpecified
                    || this.EscrowItemNextDisbursementDueDateSpecified
                    || this.EscrowItemNextProjectedDisbursementAmountSpecified
                    || this.EscrowItemTotalReserveCollectedAtClosingAmountSpecified
                    || this.EscrowItemTypeSpecified
                    || this.EscrowItemTypeOtherDescriptionSpecified
                    || this.EscrowMonthlyPaymentAmountSpecified
                    || this.EscrowMonthlyPaymentRoundingTypeSpecified
                    || this.EscrowPaidByTypeSpecified
                    || this.EscrowPaymentFrequencyTypeSpecified
                    || this.EscrowPaymentFrequencyTypeOtherDescriptionSpecified
                    || this.EscrowPremiumAmountSpecified
                    || this.EscrowPremiumDurationMonthsCountSpecified
                    || this.EscrowPremiumPaidByTypeSpecified
                    || this.EscrowPremiumPaymentTypeSpecified
                    || this.EscrowPremiumRatePercentSpecified
                    || this.EscrowPremiumRatePercentBasisTypeSpecified
                    || this.EscrowPremiumRatePercentBasisTypeOtherDescriptionSpecified
                    || this.EscrowSpecifiedHUD1LineNumberValueSpecified
                    || this.EscrowTotalDisbursementAmountSpecified
                    || this.FeePaidToTypeSpecified
                    || this.FeePaidToTypeOtherDescriptionSpecified
                    || this.GFEDisclosedEscrowPremiumAmountSpecified
                    || this.IntegratedDisclosureLineNumberValueSpecified
                    || this.IntegratedDisclosureSectionTypeSpecified
                    || this.IntegratedDisclosureSectionTypeOtherDescriptionSpecified
                    || this.RegulationZPointsAndFeesIndicatorSpecified
                    || this.RequiredProviderOfServiceIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BorrowerChosenProviderIndicator", Order = 0)]
        public MISMOIndicator BorrowerChosenProviderIndicator { get; set; }
    
        [XmlIgnore]
        public bool BorrowerChosenProviderIndicatorSpecified
        {
            get { return this.BorrowerChosenProviderIndicator != null; }
            set { }
        }
    
        [XmlElement("EscrowAnnualPaymentAmount", Order = 1)]
        public MISMOAmount EscrowAnnualPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool EscrowAnnualPaymentAmountSpecified
        {
            get { return this.EscrowAnnualPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("EscrowCollectedNumberOfMonthsCount", Order = 2)]
        public MISMOCount EscrowCollectedNumberOfMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool EscrowCollectedNumberOfMonthsCountSpecified
        {
            get { return this.EscrowCollectedNumberOfMonthsCount != null; }
            set { }
        }
    
        [XmlElement("EscrowInsurancePolicyIdentifier", Order = 3)]
        public MISMOIdentifier EscrowInsurancePolicyIdentifier { get; set; }
    
        [XmlIgnore]
        public bool EscrowInsurancePolicyIdentifierSpecified
        {
            get { return this.EscrowInsurancePolicyIdentifier != null; }
            set { }
        }
    
        [XmlElement("EscrowItemActualTotalAmount", Order = 4)]
        public MISMOAmount EscrowItemActualTotalAmount { get; set; }
    
        [XmlIgnore]
        public bool EscrowItemActualTotalAmountSpecified
        {
            get { return this.EscrowItemActualTotalAmount != null; }
            set { }
        }
    
        [XmlElement("EscrowItemCategoryType", Order = 5)]
        public MISMOEnum<EscrowItemCategoryBase> EscrowItemCategoryType { get; set; }
    
        [XmlIgnore]
        public bool EscrowItemCategoryTypeSpecified
        {
            get { return this.EscrowItemCategoryType != null; }
            set { }
        }
    
        [XmlElement("EscrowItemCategoryTypeOtherDescription", Order = 6)]
        public MISMOString EscrowItemCategoryTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool EscrowItemCategoryTypeOtherDescriptionSpecified
        {
            get { return this.EscrowItemCategoryTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EscrowItemEstimatedTotalAmount", Order = 7)]
        public MISMOAmount EscrowItemEstimatedTotalAmount { get; set; }
    
        [XmlIgnore]
        public bool EscrowItemEstimatedTotalAmountSpecified
        {
            get { return this.EscrowItemEstimatedTotalAmount != null; }
            set { }
        }
    
        [XmlElement("EscrowItemLastDisbursementDate", Order = 8)]
        public MISMODate EscrowItemLastDisbursementDate { get; set; }
    
        [XmlIgnore]
        public bool EscrowItemLastDisbursementDateSpecified
        {
            get { return this.EscrowItemLastDisbursementDate != null; }
            set { }
        }
    
        [XmlElement("EscrowItemNextDisbursementDueDate", Order = 9)]
        public MISMODate EscrowItemNextDisbursementDueDate { get; set; }
    
        [XmlIgnore]
        public bool EscrowItemNextDisbursementDueDateSpecified
        {
            get { return this.EscrowItemNextDisbursementDueDate != null; }
            set { }
        }
    
        [XmlElement("EscrowItemNextProjectedDisbursementAmount", Order = 10)]
        public MISMOAmount EscrowItemNextProjectedDisbursementAmount { get; set; }
    
        [XmlIgnore]
        public bool EscrowItemNextProjectedDisbursementAmountSpecified
        {
            get { return this.EscrowItemNextProjectedDisbursementAmount != null; }
            set { }
        }
    
        [XmlElement("EscrowItemTotalReserveCollectedAtClosingAmount", Order = 11)]
        public MISMOAmount EscrowItemTotalReserveCollectedAtClosingAmount { get; set; }
    
        [XmlIgnore]
        public bool EscrowItemTotalReserveCollectedAtClosingAmountSpecified
        {
            get { return this.EscrowItemTotalReserveCollectedAtClosingAmount != null; }
            set { }
        }
    
        [XmlElement("EscrowItemType", Order = 12)]
        public MISMOEnum<EscrowItemBase> EscrowItemType { get; set; }
    
        [XmlIgnore]
        public bool EscrowItemTypeSpecified
        {
            get { return this.EscrowItemType != null; }
            set { }
        }
    
        [XmlElement("EscrowItemTypeOtherDescription", Order = 13)]
        public MISMOString EscrowItemTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool EscrowItemTypeOtherDescriptionSpecified
        {
            get { return this.EscrowItemTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EscrowMonthlyPaymentAmount", Order = 14)]
        public MISMOAmount EscrowMonthlyPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool EscrowMonthlyPaymentAmountSpecified
        {
            get { return this.EscrowMonthlyPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("EscrowMonthlyPaymentRoundingType", Order = 15)]
        public MISMOEnum<EscrowMonthlyPaymentRoundingBase> EscrowMonthlyPaymentRoundingType { get; set; }
    
        [XmlIgnore]
        public bool EscrowMonthlyPaymentRoundingTypeSpecified
        {
            get { return this.EscrowMonthlyPaymentRoundingType != null; }
            set { }
        }
    
        [XmlElement("EscrowPaidByType", Order = 16)]
        public MISMOEnum<EscrowPaidByBase> EscrowPaidByType { get; set; }
    
        [XmlIgnore]
        public bool EscrowPaidByTypeSpecified
        {
            get { return this.EscrowPaidByType != null; }
            set { }
        }
    
        [XmlElement("EscrowPaymentFrequencyType", Order = 17)]
        public MISMOEnum<EscrowPaymentFrequencyBase> EscrowPaymentFrequencyType { get; set; }
    
        [XmlIgnore]
        public bool EscrowPaymentFrequencyTypeSpecified
        {
            get { return this.EscrowPaymentFrequencyType != null; }
            set { }
        }
    
        [XmlElement("EscrowPaymentFrequencyTypeOtherDescription", Order = 18)]
        public MISMOString EscrowPaymentFrequencyTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool EscrowPaymentFrequencyTypeOtherDescriptionSpecified
        {
            get { return this.EscrowPaymentFrequencyTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EscrowPremiumAmount", Order = 19)]
        public MISMOAmount EscrowPremiumAmount { get; set; }
    
        [XmlIgnore]
        public bool EscrowPremiumAmountSpecified
        {
            get { return this.EscrowPremiumAmount != null; }
            set { }
        }
    
        [XmlElement("EscrowPremiumDurationMonthsCount", Order = 20)]
        public MISMOCount EscrowPremiumDurationMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool EscrowPremiumDurationMonthsCountSpecified
        {
            get { return this.EscrowPremiumDurationMonthsCount != null; }
            set { }
        }
    
        [XmlElement("EscrowPremiumPaidByType", Order = 21)]
        public MISMOEnum<EscrowPremiumPaidByBase> EscrowPremiumPaidByType { get; set; }
    
        [XmlIgnore]
        public bool EscrowPremiumPaidByTypeSpecified
        {
            get { return this.EscrowPremiumPaidByType != null; }
            set { }
        }
    
        [XmlElement("EscrowPremiumPaymentType", Order = 22)]
        public MISMOEnum<EscrowPremiumPaymentBase> EscrowPremiumPaymentType { get; set; }
    
        [XmlIgnore]
        public bool EscrowPremiumPaymentTypeSpecified
        {
            get { return this.EscrowPremiumPaymentType != null; }
            set { }
        }
    
        [XmlElement("EscrowPremiumRatePercent", Order = 23)]
        public MISMOPercent EscrowPremiumRatePercent { get; set; }
    
        [XmlIgnore]
        public bool EscrowPremiumRatePercentSpecified
        {
            get { return this.EscrowPremiumRatePercent != null; }
            set { }
        }
    
        [XmlElement("EscrowPremiumRatePercentBasisType", Order = 24)]
        public MISMOEnum<EscrowPremiumRatePercentBasisBase> EscrowPremiumRatePercentBasisType { get; set; }
    
        [XmlIgnore]
        public bool EscrowPremiumRatePercentBasisTypeSpecified
        {
            get { return this.EscrowPremiumRatePercentBasisType != null; }
            set { }
        }
    
        [XmlElement("EscrowPremiumRatePercentBasisTypeOtherDescription", Order = 25)]
        public MISMOString EscrowPremiumRatePercentBasisTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool EscrowPremiumRatePercentBasisTypeOtherDescriptionSpecified
        {
            get { return this.EscrowPremiumRatePercentBasisTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EscrowSpecifiedHUD1LineNumberValue", Order = 26)]
        public MISMOValue EscrowSpecifiedHUD1LineNumberValue { get; set; }
    
        [XmlIgnore]
        public bool EscrowSpecifiedHUD1LineNumberValueSpecified
        {
            get { return this.EscrowSpecifiedHUD1LineNumberValue != null; }
            set { }
        }
    
        [XmlElement("EscrowTotalDisbursementAmount", Order = 27)]
        public MISMOAmount EscrowTotalDisbursementAmount { get; set; }
    
        [XmlIgnore]
        public bool EscrowTotalDisbursementAmountSpecified
        {
            get { return this.EscrowTotalDisbursementAmount != null; }
            set { }
        }
    
        [XmlElement("FeePaidToType", Order = 28)]
        public MISMOEnum<FeePaidToBase> FeePaidToType { get; set; }
    
        [XmlIgnore]
        public bool FeePaidToTypeSpecified
        {
            get { return this.FeePaidToType != null; }
            set { }
        }
    
        [XmlElement("FeePaidToTypeOtherDescription", Order = 29)]
        public MISMOString FeePaidToTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool FeePaidToTypeOtherDescriptionSpecified
        {
            get { return this.FeePaidToTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("GFEDisclosedEscrowPremiumAmount", Order = 30)]
        public MISMOAmount GFEDisclosedEscrowPremiumAmount { get; set; }
    
        [XmlIgnore]
        public bool GFEDisclosedEscrowPremiumAmountSpecified
        {
            get { return this.GFEDisclosedEscrowPremiumAmount != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureLineNumberValue", Order = 31)]
        public MISMOValue IntegratedDisclosureLineNumberValue { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureLineNumberValueSpecified
        {
            get { return this.IntegratedDisclosureLineNumberValue != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureSectionType", Order = 32)]
        public MISMOEnum<IntegratedDisclosureSectionBase> IntegratedDisclosureSectionType { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureSectionTypeSpecified
        {
            get { return this.IntegratedDisclosureSectionType != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureSectionTypeOtherDescription", Order = 33)]
        public MISMOString IntegratedDisclosureSectionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureSectionTypeOtherDescriptionSpecified
        {
            get { return this.IntegratedDisclosureSectionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("RegulationZPointsAndFeesIndicator", Order = 34)]
        public MISMOIndicator RegulationZPointsAndFeesIndicator { get; set; }
    
        [XmlIgnore]
        public bool RegulationZPointsAndFeesIndicatorSpecified
        {
            get { return this.RegulationZPointsAndFeesIndicator != null; }
            set { }
        }
    
        [XmlElement("RequiredProviderOfServiceIndicator", Order = 35)]
        public MISMOIndicator RequiredProviderOfServiceIndicator { get; set; }
    
        [XmlIgnore]
        public bool RequiredProviderOfServiceIndicatorSpecified
        {
            get { return this.RequiredProviderOfServiceIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 36)]
        public ESCROW_ITEM_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
