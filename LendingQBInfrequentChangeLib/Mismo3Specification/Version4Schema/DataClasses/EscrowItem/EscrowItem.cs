namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ESCROW_ITEM
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EscrowItemDetailSpecified
                    || this.EscrowItemDisbursementsSpecified
                    || this.EscrowItemPaymentsSpecified
                    || this.EscrowPaidToSpecified
                    || this.SelectedServiceProviderSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ESCROW_ITEM_DETAIL", Order = 0)]
        public ESCROW_ITEM_DETAIL EscrowItemDetail { get; set; }
    
        [XmlIgnore]
        public bool EscrowItemDetailSpecified
        {
            get { return this.EscrowItemDetail != null && this.EscrowItemDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("ESCROW_ITEM_DISBURSEMENTS", Order = 1)]
        public ESCROW_ITEM_DISBURSEMENTS EscrowItemDisbursements { get; set; }
    
        [XmlIgnore]
        public bool EscrowItemDisbursementsSpecified
        {
            get { return this.EscrowItemDisbursements != null && this.EscrowItemDisbursements.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("ESCROW_ITEM_PAYMENTS", Order = 2)]
        public ESCROW_ITEM_PAYMENTS EscrowItemPayments { get; set; }
    
        [XmlIgnore]
        public bool EscrowItemPaymentsSpecified
        {
            get { return this.EscrowItemPayments != null && this.EscrowItemPayments.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("ESCROW_PAID_TO", Order = 3)]
        public PAID_TO EscrowPaidTo { get; set; }
    
        [XmlIgnore]
        public bool EscrowPaidToSpecified
        {
            get { return this.EscrowPaidTo != null; }
            set { }
        }
    
        [XmlElement("SELECTED_SERVICE_PROVIDER", Order = 4)]
        public SELECTED_SERVICE_PROVIDER SelectedServiceProvider { get; set; }
    
        [XmlIgnore]
        public bool SelectedServiceProviderSpecified
        {
            get { return this.SelectedServiceProvider != null && this.SelectedServiceProvider.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public ESCROW_ITEM_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
