namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class UNIQUE_PROPERTY_IDENTIFICATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PropertyIdentifierSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PropertyIdentifier", Order = 0)]
        public MISMOIdentifier PropertyIdentifier { get; set; }
    
        [XmlIgnore]
        public bool PropertyIdentifierSpecified
        {
            get { return this.PropertyIdentifier != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public UNIQUE_PROPERTY_IDENTIFICATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
