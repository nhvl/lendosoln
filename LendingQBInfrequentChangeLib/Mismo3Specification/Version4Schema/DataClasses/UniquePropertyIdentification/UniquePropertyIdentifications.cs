namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class UNIQUE_PROPERTY_IDENTIFICATIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.UniquePropertyIdentificationListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("UNIQUE_PROPERTY_IDENTIFICATION", Order = 0)]
        public List<UNIQUE_PROPERTY_IDENTIFICATION> UniquePropertyIdentificationList { get; set; } = new List<UNIQUE_PROPERTY_IDENTIFICATION>();
    
        [XmlIgnore]
        public bool UniquePropertyIdentificationListSpecified
        {
            get { return this.UniquePropertyIdentificationList != null && this.UniquePropertyIdentificationList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public UNIQUE_PROPERTY_IDENTIFICATIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
