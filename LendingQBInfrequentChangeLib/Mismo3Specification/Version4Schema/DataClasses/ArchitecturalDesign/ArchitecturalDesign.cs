namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ARCHITECTURAL_DESIGN
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ArchitecturalDesignCategoryTypeSpecified
                    || this.ArchitecturalDesignCategoryTypeOtherDescriptionSpecified
                    || this.ArchitecturalDesignConfigurationTypeSpecified
                    || this.ArchitecturalDesignConfigurationTypeOtherDescriptionSpecified
                    || this.ArchitecturalDesignDescriptionSpecified
                    || this.ComponentAdjustmentAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ArchitecturalDesignCategoryType", Order = 0)]
        public MISMOEnum<ArchitecturalDesignCategoryBase> ArchitecturalDesignCategoryType { get; set; }
    
        [XmlIgnore]
        public bool ArchitecturalDesignCategoryTypeSpecified
        {
            get { return this.ArchitecturalDesignCategoryType != null; }
            set { }
        }
    
        [XmlElement("ArchitecturalDesignCategoryTypeOtherDescription", Order = 1)]
        public MISMOString ArchitecturalDesignCategoryTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ArchitecturalDesignCategoryTypeOtherDescriptionSpecified
        {
            get { return this.ArchitecturalDesignCategoryTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ArchitecturalDesignConfigurationType", Order = 2)]
        public MISMOEnum<ArchitecturalDesignConfigurationBase> ArchitecturalDesignConfigurationType { get; set; }
    
        [XmlIgnore]
        public bool ArchitecturalDesignConfigurationTypeSpecified
        {
            get { return this.ArchitecturalDesignConfigurationType != null; }
            set { }
        }
    
        [XmlElement("ArchitecturalDesignConfigurationTypeOtherDescription", Order = 3)]
        public MISMOString ArchitecturalDesignConfigurationTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ArchitecturalDesignConfigurationTypeOtherDescriptionSpecified
        {
            get { return this.ArchitecturalDesignConfigurationTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ArchitecturalDesignDescription", Order = 4)]
        public MISMOString ArchitecturalDesignDescription { get; set; }
    
        [XmlIgnore]
        public bool ArchitecturalDesignDescriptionSpecified
        {
            get { return this.ArchitecturalDesignDescription != null; }
            set { }
        }
    
        [XmlElement("ComponentAdjustmentAmount", Order = 5)]
        public MISMOAmount ComponentAdjustmentAmount { get; set; }
    
        [XmlIgnore]
        public bool ComponentAdjustmentAmountSpecified
        {
            get { return this.ComponentAdjustmentAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 6)]
        public ARCHITECTURAL_DESIGN_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
