namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class WORKOUT_SUMMARY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LifeOfLoanBorrowerAssistancePaidAmountSpecified
                    || this.LifeOfLoanWorkoutModificationCountSpecified
                    || this.LifeOfLoanWorkoutModificationDefaultedCountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LifeOfLoanBorrowerAssistancePaidAmount", Order = 0)]
        public MISMOAmount LifeOfLoanBorrowerAssistancePaidAmount { get; set; }
    
        [XmlIgnore]
        public bool LifeOfLoanBorrowerAssistancePaidAmountSpecified
        {
            get { return this.LifeOfLoanBorrowerAssistancePaidAmount != null; }
            set { }
        }
    
        [XmlElement("LifeOfLoanWorkoutModificationCount", Order = 1)]
        public MISMOCount LifeOfLoanWorkoutModificationCount { get; set; }
    
        [XmlIgnore]
        public bool LifeOfLoanWorkoutModificationCountSpecified
        {
            get { return this.LifeOfLoanWorkoutModificationCount != null; }
            set { }
        }
    
        [XmlElement("LifeOfLoanWorkoutModificationDefaultedCount", Order = 2)]
        public MISMOCount LifeOfLoanWorkoutModificationDefaultedCount { get; set; }
    
        [XmlIgnore]
        public bool LifeOfLoanWorkoutModificationDefaultedCountSpecified
        {
            get { return this.LifeOfLoanWorkoutModificationDefaultedCount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public WORKOUT_SUMMARY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
