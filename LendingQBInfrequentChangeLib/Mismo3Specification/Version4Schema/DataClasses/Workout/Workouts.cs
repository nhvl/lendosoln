namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class WORKOUTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.WorkoutListSpecified
                    || this.WorkoutSummarySpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("WORKOUT", Order = 0)]
        public List<WORKOUT> WorkoutList { get; set; } = new List<WORKOUT>();
    
        [XmlIgnore]
        public bool WorkoutListSpecified
        {
            get { return this.WorkoutList != null && this.WorkoutList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("WORKOUT_SUMMARY", Order = 1)]
        public WORKOUT_SUMMARY WorkoutSummary { get; set; }
    
        [XmlIgnore]
        public bool WorkoutSummarySpecified
        {
            get { return this.WorkoutSummary != null && this.WorkoutSummary.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public WORKOUTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
