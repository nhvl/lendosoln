namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class WORKOUT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AssistancesSpecified
                    || this.ContributionsSpecified
                    || this.ErrorsSpecified
                    || this.RepaymentPlansSpecified
                    || this.SolicitationsSpecified
                    || this.TrialSpecified
                    || this.WorkoutCommentsSpecified
                    || this.WorkoutDetailSpecified
                    || this.WorkoutStatusesSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ASSISTANCES", Order = 0)]
        public ASSISTANCES Assistances { get; set; }
    
        [XmlIgnore]
        public bool AssistancesSpecified
        {
            get { return this.Assistances != null && this.Assistances.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CONTRIBUTIONS", Order = 1)]
        public CONTRIBUTIONS Contributions { get; set; }
    
        [XmlIgnore]
        public bool ContributionsSpecified
        {
            get { return this.Contributions != null && this.Contributions.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("ERRORS", Order = 2)]
        public ERRORS Errors { get; set; }
    
        [XmlIgnore]
        public bool ErrorsSpecified
        {
            get { return this.Errors != null && this.Errors.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("REPAYMENT_PLANS", Order = 3)]
        public REPAYMENT_PLANS RepaymentPlans { get; set; }
    
        [XmlIgnore]
        public bool RepaymentPlansSpecified
        {
            get { return this.RepaymentPlans != null && this.RepaymentPlans.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SOLICITATIONS", Order = 4)]
        public SOLICITATIONS Solicitations { get; set; }
    
        [XmlIgnore]
        public bool SolicitationsSpecified
        {
            get { return this.Solicitations != null && this.Solicitations.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("TRIAL", Order = 5)]
        public TRIAL Trial { get; set; }
    
        [XmlIgnore]
        public bool TrialSpecified
        {
            get { return this.Trial != null && this.Trial.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("WORKOUT_COMMENTS", Order = 6)]
        public WORKOUT_COMMENTS WorkoutComments { get; set; }
    
        [XmlIgnore]
        public bool WorkoutCommentsSpecified
        {
            get { return this.WorkoutComments != null && this.WorkoutComments.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("WORKOUT_DETAIL", Order = 7)]
        public WORKOUT_DETAIL WorkoutDetail { get; set; }
    
        [XmlIgnore]
        public bool WorkoutDetailSpecified
        {
            get { return this.WorkoutDetail != null && this.WorkoutDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("WORKOUT_STATUSES", Order = 8)]
        public WORKOUT_STATUSES WorkoutStatuses { get; set; }
    
        [XmlIgnore]
        public bool WorkoutStatusesSpecified
        {
            get { return this.WorkoutStatuses != null && this.WorkoutStatuses.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 9)]
        public WORKOUT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
