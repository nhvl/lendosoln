namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class WORKOUT_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ActiveWorkoutDocumentStatusDateSpecified
                    || this.ActiveWorkoutDocumentStatusTypeSpecified
                    || this.ActiveWorkoutDocumentStatusTypeOtherDescriptionSpecified
                    || this.BorrowerContributionReceivedIndicatorSpecified
                    || this.BorrowerContributionRequiredIndicatorSpecified
                    || this.DaysSinceEndOfPriorWorkoutCountSpecified
                    || this.DeedInLieuOfferExpirationDateSpecified
                    || this.DeedInLieuSignedDateSpecified
                    || this.DeedInLieuTransitionDurationTypeSpecified
                    || this.ForbearancePaymentNextDueDateSpecified
                    || this.ForbearancePaymentTermExtensionApprovalDateSpecified
                    || this.ListingRequirementWaivedIndicatorSpecified
                    || this.MaximumBorrowerContributionAmountSpecified
                    || this.MaximumPromissoryNotePaymentAmountSpecified
                    || this.MIWorkoutApprovalConditionTypeSpecified
                    || this.MIWorkoutApprovalConditionTypeOtherDescriptionSpecified
                    || this.MIWorkoutDecisionTypeSpecified
                    || this.MIWorkoutDecisionTypeOtherDescriptionSpecified
                    || this.MIWorkoutDelegationTypeSpecified
                    || this.MIWorkoutDelegationTypeOtherDescriptionSpecified
                    || this.MIWorkoutDenialReasonTypeSpecified
                    || this.MIWorkoutDenialReasonTypeOtherDescriptionSpecified
                    || this.PaymentForbearanceTermMonthsCountSpecified
                    || this.PaymentReductionAmountSpecified
                    || this.PaymentReductionPercentSpecified
                    || this.PaymentReductionSponsorSharePercentSpecified
                    || this.PriorWorkoutSubsequentPaymentDelinquencySeverityCountSpecified
                    || this.RepaymentPlanMaximumMonthsCountSpecified
                    || this.RepaymentPlanPaymentThresholdPercentSpecified
                    || this.RepaymentPlanScheduledPaymentAmountSpecified
                    || this.SubordinateLienOfferAcceptedIndicatorSpecified
                    || this.TotalExpenseForgivenAmountSpecified
                    || this.TotalIndebtednessAmountSpecified
                    || this.TotalSubordinateLienOfferedAmountSpecified
                    || this.TotalSubordinateLienSettlementAmountSpecified
                    || this.WorkoutBorrowerCashContributionAmountSpecified
                    || this.WorkoutEffectiveDateSpecified
                    || this.WorkoutEvaluatedDatetimeSpecified
                    || this.WorkoutPriorityOrderNumberSpecified
                    || this.WorkoutProgramIdentifierSpecified
                    || this.WorkoutQualificationStatusDateSpecified
                    || this.WorkoutQualificationStatusTypeSpecified
                    || this.WorkoutQualificationStatusTypeOtherDescriptionSpecified
                    || this.WorkoutRuleVersionIdentifierSpecified
                    || this.WorkoutScheduledCompletionDateSpecified
                    || this.WorkoutSubmissionDateSpecified
                    || this.WorkoutSubtypeIdentifierSpecified
                    || this.WorkoutSuitabilityNumberSpecified
                    || this.WorkoutTermExtendedDateSpecified
                    || this.WorkoutTypeSpecified
                    || this.WorkoutTypeOtherDescriptionSpecified
                    || this.WorkoutUnsuccessfulReasonTypeSpecified
                    || this.WorkoutUnsuccessfulReasonTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ActiveWorkoutDocumentStatusDate", Order = 0)]
        public MISMODate ActiveWorkoutDocumentStatusDate { get; set; }
    
        [XmlIgnore]
        public bool ActiveWorkoutDocumentStatusDateSpecified
        {
            get { return this.ActiveWorkoutDocumentStatusDate != null; }
            set { }
        }
    
        [XmlElement("ActiveWorkoutDocumentStatusType", Order = 1)]
        public MISMOEnum<ActiveWorkoutDocumentStatusBase> ActiveWorkoutDocumentStatusType { get; set; }
    
        [XmlIgnore]
        public bool ActiveWorkoutDocumentStatusTypeSpecified
        {
            get { return this.ActiveWorkoutDocumentStatusType != null; }
            set { }
        }
    
        [XmlElement("ActiveWorkoutDocumentStatusTypeOtherDescription", Order = 2)]
        public MISMOString ActiveWorkoutDocumentStatusTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ActiveWorkoutDocumentStatusTypeOtherDescriptionSpecified
        {
            get { return this.ActiveWorkoutDocumentStatusTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("BorrowerContributionReceivedIndicator", Order = 3)]
        public MISMOIndicator BorrowerContributionReceivedIndicator { get; set; }
    
        [XmlIgnore]
        public bool BorrowerContributionReceivedIndicatorSpecified
        {
            get { return this.BorrowerContributionReceivedIndicator != null; }
            set { }
        }
    
        [XmlElement("BorrowerContributionRequiredIndicator", Order = 4)]
        public MISMOIndicator BorrowerContributionRequiredIndicator { get; set; }
    
        [XmlIgnore]
        public bool BorrowerContributionRequiredIndicatorSpecified
        {
            get { return this.BorrowerContributionRequiredIndicator != null; }
            set { }
        }
    
        [XmlElement("DaysSinceEndOfPriorWorkoutCount", Order = 5)]
        public MISMOCount DaysSinceEndOfPriorWorkoutCount { get; set; }
    
        [XmlIgnore]
        public bool DaysSinceEndOfPriorWorkoutCountSpecified
        {
            get { return this.DaysSinceEndOfPriorWorkoutCount != null; }
            set { }
        }
    
        [XmlElement("DeedInLieuOfferExpirationDate", Order = 6)]
        public MISMODate DeedInLieuOfferExpirationDate { get; set; }
    
        [XmlIgnore]
        public bool DeedInLieuOfferExpirationDateSpecified
        {
            get { return this.DeedInLieuOfferExpirationDate != null; }
            set { }
        }
    
        [XmlElement("DeedInLieuSignedDate", Order = 7)]
        public MISMODate DeedInLieuSignedDate { get; set; }
    
        [XmlIgnore]
        public bool DeedInLieuSignedDateSpecified
        {
            get { return this.DeedInLieuSignedDate != null; }
            set { }
        }
    
        [XmlElement("DeedInLieuTransitionDurationType", Order = 8)]
        public MISMOEnum<DeedInLieuTransitionDurationBase> DeedInLieuTransitionDurationType { get; set; }
    
        [XmlIgnore]
        public bool DeedInLieuTransitionDurationTypeSpecified
        {
            get { return this.DeedInLieuTransitionDurationType != null; }
            set { }
        }
    
        [XmlElement("ForbearancePaymentNextDueDate", Order = 9)]
        public MISMODate ForbearancePaymentNextDueDate { get; set; }
    
        [XmlIgnore]
        public bool ForbearancePaymentNextDueDateSpecified
        {
            get { return this.ForbearancePaymentNextDueDate != null; }
            set { }
        }
    
        [XmlElement("ForbearancePaymentTermExtensionApprovalDate", Order = 10)]
        public MISMODate ForbearancePaymentTermExtensionApprovalDate { get; set; }
    
        [XmlIgnore]
        public bool ForbearancePaymentTermExtensionApprovalDateSpecified
        {
            get { return this.ForbearancePaymentTermExtensionApprovalDate != null; }
            set { }
        }
    
        [XmlElement("ListingRequirementWaivedIndicator", Order = 11)]
        public MISMOIndicator ListingRequirementWaivedIndicator { get; set; }
    
        [XmlIgnore]
        public bool ListingRequirementWaivedIndicatorSpecified
        {
            get { return this.ListingRequirementWaivedIndicator != null; }
            set { }
        }
    
        [XmlElement("MaximumBorrowerContributionAmount", Order = 12)]
        public MISMOAmount MaximumBorrowerContributionAmount { get; set; }
    
        [XmlIgnore]
        public bool MaximumBorrowerContributionAmountSpecified
        {
            get { return this.MaximumBorrowerContributionAmount != null; }
            set { }
        }
    
        [XmlElement("MaximumPromissoryNotePaymentAmount", Order = 13)]
        public MISMOAmount MaximumPromissoryNotePaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool MaximumPromissoryNotePaymentAmountSpecified
        {
            get { return this.MaximumPromissoryNotePaymentAmount != null; }
            set { }
        }
    
        [XmlElement("MIWorkoutApprovalConditionType", Order = 14)]
        public MISMOEnum<MIWorkoutApprovalConditionBase> MIWorkoutApprovalConditionType { get; set; }
    
        [XmlIgnore]
        public bool MIWorkoutApprovalConditionTypeSpecified
        {
            get { return this.MIWorkoutApprovalConditionType != null; }
            set { }
        }
    
        [XmlElement("MIWorkoutApprovalConditionTypeOtherDescription", Order = 15)]
        public MISMOString MIWorkoutApprovalConditionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MIWorkoutApprovalConditionTypeOtherDescriptionSpecified
        {
            get { return this.MIWorkoutApprovalConditionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MIWorkoutDecisionType", Order = 16)]
        public MISMOEnum<MIWorkoutDecisionBase> MIWorkoutDecisionType { get; set; }
    
        [XmlIgnore]
        public bool MIWorkoutDecisionTypeSpecified
        {
            get { return this.MIWorkoutDecisionType != null; }
            set { }
        }
    
        [XmlElement("MIWorkoutDecisionTypeOtherDescription", Order = 17)]
        public MISMOString MIWorkoutDecisionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MIWorkoutDecisionTypeOtherDescriptionSpecified
        {
            get { return this.MIWorkoutDecisionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MIWorkoutDelegationType", Order = 18)]
        public MISMOEnum<MIWorkoutDelegationBase> MIWorkoutDelegationType { get; set; }
    
        [XmlIgnore]
        public bool MIWorkoutDelegationTypeSpecified
        {
            get { return this.MIWorkoutDelegationType != null; }
            set { }
        }
    
        [XmlElement("MIWorkoutDelegationTypeOtherDescription", Order = 19)]
        public MISMOString MIWorkoutDelegationTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MIWorkoutDelegationTypeOtherDescriptionSpecified
        {
            get { return this.MIWorkoutDelegationTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MIWorkoutDenialReasonType", Order = 20)]
        public MISMOEnum<MIWorkoutDenialReasonBase> MIWorkoutDenialReasonType { get; set; }
    
        [XmlIgnore]
        public bool MIWorkoutDenialReasonTypeSpecified
        {
            get { return this.MIWorkoutDenialReasonType != null; }
            set { }
        }
    
        [XmlElement("MIWorkoutDenialReasonTypeOtherDescription", Order = 21)]
        public MISMOString MIWorkoutDenialReasonTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MIWorkoutDenialReasonTypeOtherDescriptionSpecified
        {
            get { return this.MIWorkoutDenialReasonTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PaymentForbearanceTermMonthsCount", Order = 22)]
        public MISMOCount PaymentForbearanceTermMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool PaymentForbearanceTermMonthsCountSpecified
        {
            get { return this.PaymentForbearanceTermMonthsCount != null; }
            set { }
        }
    
        [XmlElement("PaymentReductionAmount", Order = 23)]
        public MISMOAmount PaymentReductionAmount { get; set; }
    
        [XmlIgnore]
        public bool PaymentReductionAmountSpecified
        {
            get { return this.PaymentReductionAmount != null; }
            set { }
        }
    
        [XmlElement("PaymentReductionPercent", Order = 24)]
        public MISMOPercent PaymentReductionPercent { get; set; }
    
        [XmlIgnore]
        public bool PaymentReductionPercentSpecified
        {
            get { return this.PaymentReductionPercent != null; }
            set { }
        }
    
        [XmlElement("PaymentReductionSponsorSharePercent", Order = 25)]
        public MISMOPercent PaymentReductionSponsorSharePercent { get; set; }
    
        [XmlIgnore]
        public bool PaymentReductionSponsorSharePercentSpecified
        {
            get { return this.PaymentReductionSponsorSharePercent != null; }
            set { }
        }
    
        [XmlElement("PriorWorkoutSubsequentPaymentDelinquencySeverityCount", Order = 26)]
        public MISMOCount PriorWorkoutSubsequentPaymentDelinquencySeverityCount { get; set; }
    
        [XmlIgnore]
        public bool PriorWorkoutSubsequentPaymentDelinquencySeverityCountSpecified
        {
            get { return this.PriorWorkoutSubsequentPaymentDelinquencySeverityCount != null; }
            set { }
        }
    
        [XmlElement("RepaymentPlanMaximumMonthsCount", Order = 27)]
        public MISMOCount RepaymentPlanMaximumMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool RepaymentPlanMaximumMonthsCountSpecified
        {
            get { return this.RepaymentPlanMaximumMonthsCount != null; }
            set { }
        }
    
        [XmlElement("RepaymentPlanPaymentThresholdPercent", Order = 28)]
        public MISMOPercent RepaymentPlanPaymentThresholdPercent { get; set; }
    
        [XmlIgnore]
        public bool RepaymentPlanPaymentThresholdPercentSpecified
        {
            get { return this.RepaymentPlanPaymentThresholdPercent != null; }
            set { }
        }
    
        [XmlElement("RepaymentPlanScheduledPaymentAmount", Order = 29)]
        public MISMOAmount RepaymentPlanScheduledPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool RepaymentPlanScheduledPaymentAmountSpecified
        {
            get { return this.RepaymentPlanScheduledPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("SubordinateLienOfferAcceptedIndicator", Order = 30)]
        public MISMOIndicator SubordinateLienOfferAcceptedIndicator { get; set; }
    
        [XmlIgnore]
        public bool SubordinateLienOfferAcceptedIndicatorSpecified
        {
            get { return this.SubordinateLienOfferAcceptedIndicator != null; }
            set { }
        }
    
        [XmlElement("TotalExpenseForgivenAmount", Order = 31)]
        public MISMOAmount TotalExpenseForgivenAmount { get; set; }
    
        [XmlIgnore]
        public bool TotalExpenseForgivenAmountSpecified
        {
            get { return this.TotalExpenseForgivenAmount != null; }
            set { }
        }
    
        [XmlElement("TotalIndebtednessAmount", Order = 32)]
        public MISMOAmount TotalIndebtednessAmount { get; set; }
    
        [XmlIgnore]
        public bool TotalIndebtednessAmountSpecified
        {
            get { return this.TotalIndebtednessAmount != null; }
            set { }
        }
    
        [XmlElement("TotalSubordinateLienOfferedAmount", Order = 33)]
        public MISMOAmount TotalSubordinateLienOfferedAmount { get; set; }
    
        [XmlIgnore]
        public bool TotalSubordinateLienOfferedAmountSpecified
        {
            get { return this.TotalSubordinateLienOfferedAmount != null; }
            set { }
        }
    
        [XmlElement("TotalSubordinateLienSettlementAmount", Order = 34)]
        public MISMOAmount TotalSubordinateLienSettlementAmount { get; set; }
    
        [XmlIgnore]
        public bool TotalSubordinateLienSettlementAmountSpecified
        {
            get { return this.TotalSubordinateLienSettlementAmount != null; }
            set { }
        }
    
        [XmlElement("WorkoutBorrowerCashContributionAmount", Order = 35)]
        public MISMOAmount WorkoutBorrowerCashContributionAmount { get; set; }
    
        [XmlIgnore]
        public bool WorkoutBorrowerCashContributionAmountSpecified
        {
            get { return this.WorkoutBorrowerCashContributionAmount != null; }
            set { }
        }
    
        [XmlElement("WorkoutEffectiveDate", Order = 36)]
        public MISMODate WorkoutEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool WorkoutEffectiveDateSpecified
        {
            get { return this.WorkoutEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("WorkoutEvaluatedDatetime", Order = 37)]
        public MISMODatetime WorkoutEvaluatedDatetime { get; set; }
    
        [XmlIgnore]
        public bool WorkoutEvaluatedDatetimeSpecified
        {
            get { return this.WorkoutEvaluatedDatetime != null; }
            set { }
        }
    
        [XmlElement("WorkoutPriorityOrderNumber", Order = 38)]
        public MISMONumeric WorkoutPriorityOrderNumber { get; set; }
    
        [XmlIgnore]
        public bool WorkoutPriorityOrderNumberSpecified
        {
            get { return this.WorkoutPriorityOrderNumber != null; }
            set { }
        }
    
        [XmlElement("WorkoutProgramIdentifier", Order = 39)]
        public MISMOIdentifier WorkoutProgramIdentifier { get; set; }
    
        [XmlIgnore]
        public bool WorkoutProgramIdentifierSpecified
        {
            get { return this.WorkoutProgramIdentifier != null; }
            set { }
        }
    
        [XmlElement("WorkoutQualificationStatusDate", Order = 40)]
        public MISMODate WorkoutQualificationStatusDate { get; set; }
    
        [XmlIgnore]
        public bool WorkoutQualificationStatusDateSpecified
        {
            get { return this.WorkoutQualificationStatusDate != null; }
            set { }
        }
    
        [XmlElement("WorkoutQualificationStatusType", Order = 41)]
        public MISMOEnum<WorkoutQualificationStatusBase> WorkoutQualificationStatusType { get; set; }
    
        [XmlIgnore]
        public bool WorkoutQualificationStatusTypeSpecified
        {
            get { return this.WorkoutQualificationStatusType != null; }
            set { }
        }
    
        [XmlElement("WorkoutQualificationStatusTypeOtherDescription", Order = 42)]
        public MISMOString WorkoutQualificationStatusTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool WorkoutQualificationStatusTypeOtherDescriptionSpecified
        {
            get { return this.WorkoutQualificationStatusTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("WorkoutRuleVersionIdentifier", Order = 43)]
        public MISMOIdentifier WorkoutRuleVersionIdentifier { get; set; }
    
        [XmlIgnore]
        public bool WorkoutRuleVersionIdentifierSpecified
        {
            get { return this.WorkoutRuleVersionIdentifier != null; }
            set { }
        }
    
        [XmlElement("WorkoutScheduledCompletionDate", Order = 44)]
        public MISMODate WorkoutScheduledCompletionDate { get; set; }
    
        [XmlIgnore]
        public bool WorkoutScheduledCompletionDateSpecified
        {
            get { return this.WorkoutScheduledCompletionDate != null; }
            set { }
        }
    
        [XmlElement("WorkoutSubmissionDate", Order = 45)]
        public MISMODate WorkoutSubmissionDate { get; set; }
    
        [XmlIgnore]
        public bool WorkoutSubmissionDateSpecified
        {
            get { return this.WorkoutSubmissionDate != null; }
            set { }
        }
    
        [XmlElement("WorkoutSubtypeIdentifier", Order = 46)]
        public MISMOIdentifier WorkoutSubtypeIdentifier { get; set; }
    
        [XmlIgnore]
        public bool WorkoutSubtypeIdentifierSpecified
        {
            get { return this.WorkoutSubtypeIdentifier != null; }
            set { }
        }
    
        [XmlElement("WorkoutSuitabilityNumber", Order = 47)]
        public MISMONumeric WorkoutSuitabilityNumber { get; set; }
    
        [XmlIgnore]
        public bool WorkoutSuitabilityNumberSpecified
        {
            get { return this.WorkoutSuitabilityNumber != null; }
            set { }
        }
    
        [XmlElement("WorkoutTermExtendedDate", Order = 48)]
        public MISMODate WorkoutTermExtendedDate { get; set; }
    
        [XmlIgnore]
        public bool WorkoutTermExtendedDateSpecified
        {
            get { return this.WorkoutTermExtendedDate != null; }
            set { }
        }
    
        [XmlElement("WorkoutType", Order = 49)]
        public MISMOEnum<WorkoutBase> WorkoutType { get; set; }
    
        [XmlIgnore]
        public bool WorkoutTypeSpecified
        {
            get { return this.WorkoutType != null; }
            set { }
        }
    
        [XmlElement("WorkoutTypeOtherDescription", Order = 50)]
        public MISMOString WorkoutTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool WorkoutTypeOtherDescriptionSpecified
        {
            get { return this.WorkoutTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("WorkoutUnsuccessfulReasonType", Order = 51)]
        public MISMOEnum<WorkoutUnsuccessfulReasonBase> WorkoutUnsuccessfulReasonType { get; set; }
    
        [XmlIgnore]
        public bool WorkoutUnsuccessfulReasonTypeSpecified
        {
            get { return this.WorkoutUnsuccessfulReasonType != null; }
            set { }
        }
    
        [XmlElement("WorkoutUnsuccessfulReasonTypeOtherDescription", Order = 52)]
        public MISMOString WorkoutUnsuccessfulReasonTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool WorkoutUnsuccessfulReasonTypeOtherDescriptionSpecified
        {
            get { return this.WorkoutUnsuccessfulReasonTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 53)]
        public WORKOUT_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
