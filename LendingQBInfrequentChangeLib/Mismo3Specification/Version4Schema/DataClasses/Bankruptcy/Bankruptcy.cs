namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class BANKRUPTCY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BankruptcyActionsSpecified
                    || this.BankruptcyDelaysSpecified
                    || this.BankruptcyDetailSpecified
                    || this.BankruptcyDispositionsSpecified
                    || this.BankruptcyResultClassesSpecified
                    || this.BankruptcyStatusesSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BANKRUPTCY_ACTIONS", Order = 0)]
        public BANKRUPTCY_ACTIONS BankruptcyActions { get; set; }
    
        [XmlIgnore]
        public bool BankruptcyActionsSpecified
        {
            get { return this.BankruptcyActions != null && this.BankruptcyActions.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("BANKRUPTCY_DELAYS", Order = 1)]
        public BANKRUPTCY_DELAYS BankruptcyDelays { get; set; }
    
        [XmlIgnore]
        public bool BankruptcyDelaysSpecified
        {
            get { return this.BankruptcyDelays != null && this.BankruptcyDelays.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("BANKRUPTCY_DETAIL", Order = 2)]
        public BANKRUPTCY_DETAIL BankruptcyDetail { get; set; }
    
        [XmlIgnore]
        public bool BankruptcyDetailSpecified
        {
            get { return this.BankruptcyDetail != null && this.BankruptcyDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("BANKRUPTCY_DISPOSITIONS", Order = 3)]
        public BANKRUPTCY_DISPOSITIONS BankruptcyDispositions { get; set; }
    
        [XmlIgnore]
        public bool BankruptcyDispositionsSpecified
        {
            get { return this.BankruptcyDispositions != null && this.BankruptcyDispositions.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("BANKRUPTCY_RESULT_CLASSES", Order = 4)]
        public BANKRUPTCY_RESULT_CLASSES BankruptcyResultClasses { get; set; }
    
        [XmlIgnore]
        public bool BankruptcyResultClassesSpecified
        {
            get { return this.BankruptcyResultClasses != null && this.BankruptcyResultClasses.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("BANKRUPTCY_STATUSES", Order = 5)]
        public BANKRUPTCY_STATUSES BankruptcyStatuses { get; set; }
    
        [XmlIgnore]
        public bool BankruptcyStatusesSpecified
        {
            get { return this.BankruptcyStatuses != null && this.BankruptcyStatuses.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 6)]
        public BANKRUPTCY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
