namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class BANKRUPTCY_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BankruptcyAdjustmentPercentSpecified
                    || this.BankruptcyAssetsAmountSpecified
                    || this.BankruptcyBarDateSpecified
                    || this.BankruptcyCaseIdentifierSpecified
                    || this.BankruptcyChapterTypeSpecified
                    || this.BankruptcyChapterTypeOtherDescriptionSpecified
                    || this.BankruptcyDelayIndicatorSpecified
                    || this.BankruptcyDistrictNameSpecified
                    || this.BankruptcyDivisionNameSpecified
                    || this.BankruptcyExemptAmountSpecified
                    || this.BankruptcyFeeAmountSpecified
                    || this.BankruptcyFiledStateNameSpecified
                    || this.BankruptcyLiabilitiesAmountSpecified
                    || this.BankruptcyRepaymentPercentSpecified
                    || this.BankruptcyTypeSpecified
                    || this.BankruptcyVoluntaryIndicatorSpecified
                    || this.MaterialChangeInLoanTermsRequestedIndicatorSpecified
                    || this.MotionForReliefNotFiledReasonDescriptionSpecified
                    || this.PostPetitionArrearageIndicatorSpecified
                    || this.PostPetitionPaymentAmountSpecified
                    || this.PostPetitionPayToTrusteeIndicatorSpecified
                    || this.PrePetitionArrearageIndicatorSpecified
                    || this.PreviousChapterSevenBankruptcyDischargeIndicatorSpecified
                    || this.PrincipalReductionOfUnsecuredAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BankruptcyAdjustmentPercent", Order = 0)]
        public MISMOPercent BankruptcyAdjustmentPercent { get; set; }
    
        [XmlIgnore]
        public bool BankruptcyAdjustmentPercentSpecified
        {
            get { return this.BankruptcyAdjustmentPercent != null; }
            set { }
        }
    
        [XmlElement("BankruptcyAssetsAmount", Order = 1)]
        public MISMOAmount BankruptcyAssetsAmount { get; set; }
    
        [XmlIgnore]
        public bool BankruptcyAssetsAmountSpecified
        {
            get { return this.BankruptcyAssetsAmount != null; }
            set { }
        }
    
        [XmlElement("BankruptcyBarDate", Order = 2)]
        public MISMODate BankruptcyBarDate { get; set; }
    
        [XmlIgnore]
        public bool BankruptcyBarDateSpecified
        {
            get { return this.BankruptcyBarDate != null; }
            set { }
        }
    
        [XmlElement("BankruptcyCaseIdentifier", Order = 3)]
        public MISMOIdentifier BankruptcyCaseIdentifier { get; set; }
    
        [XmlIgnore]
        public bool BankruptcyCaseIdentifierSpecified
        {
            get { return this.BankruptcyCaseIdentifier != null; }
            set { }
        }
    
        [XmlElement("BankruptcyChapterType", Order = 4)]
        public MISMOEnum<BankruptcyChapterBase> BankruptcyChapterType { get; set; }
    
        [XmlIgnore]
        public bool BankruptcyChapterTypeSpecified
        {
            get { return this.BankruptcyChapterType != null; }
            set { }
        }
    
        [XmlElement("BankruptcyChapterTypeOtherDescription", Order = 5)]
        public MISMOString BankruptcyChapterTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool BankruptcyChapterTypeOtherDescriptionSpecified
        {
            get { return this.BankruptcyChapterTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("BankruptcyDelayIndicator", Order = 6)]
        public MISMOIndicator BankruptcyDelayIndicator { get; set; }
    
        [XmlIgnore]
        public bool BankruptcyDelayIndicatorSpecified
        {
            get { return this.BankruptcyDelayIndicator != null; }
            set { }
        }
    
        [XmlElement("BankruptcyDistrictName", Order = 7)]
        public MISMOString BankruptcyDistrictName { get; set; }
    
        [XmlIgnore]
        public bool BankruptcyDistrictNameSpecified
        {
            get { return this.BankruptcyDistrictName != null; }
            set { }
        }
    
        [XmlElement("BankruptcyDivisionName", Order = 8)]
        public MISMOString BankruptcyDivisionName { get; set; }
    
        [XmlIgnore]
        public bool BankruptcyDivisionNameSpecified
        {
            get { return this.BankruptcyDivisionName != null; }
            set { }
        }
    
        [XmlElement("BankruptcyExemptAmount", Order = 9)]
        public MISMOAmount BankruptcyExemptAmount { get; set; }
    
        [XmlIgnore]
        public bool BankruptcyExemptAmountSpecified
        {
            get { return this.BankruptcyExemptAmount != null; }
            set { }
        }
    
        [XmlElement("BankruptcyFeeAmount", Order = 10)]
        public MISMOAmount BankruptcyFeeAmount { get; set; }
    
        [XmlIgnore]
        public bool BankruptcyFeeAmountSpecified
        {
            get { return this.BankruptcyFeeAmount != null; }
            set { }
        }
    
        [XmlElement("BankruptcyFiledStateName", Order = 11)]
        public MISMOString BankruptcyFiledStateName { get; set; }
    
        [XmlIgnore]
        public bool BankruptcyFiledStateNameSpecified
        {
            get { return this.BankruptcyFiledStateName != null; }
            set { }
        }
    
        [XmlElement("BankruptcyLiabilitiesAmount", Order = 12)]
        public MISMOAmount BankruptcyLiabilitiesAmount { get; set; }
    
        [XmlIgnore]
        public bool BankruptcyLiabilitiesAmountSpecified
        {
            get { return this.BankruptcyLiabilitiesAmount != null; }
            set { }
        }
    
        [XmlElement("BankruptcyRepaymentPercent", Order = 13)]
        public MISMOPercent BankruptcyRepaymentPercent { get; set; }
    
        [XmlIgnore]
        public bool BankruptcyRepaymentPercentSpecified
        {
            get { return this.BankruptcyRepaymentPercent != null; }
            set { }
        }
    
        [XmlElement("BankruptcyType", Order = 14)]
        public MISMOEnum<BankruptcyBase> BankruptcyType { get; set; }
    
        [XmlIgnore]
        public bool BankruptcyTypeSpecified
        {
            get { return this.BankruptcyType != null; }
            set { }
        }
    
        [XmlElement("BankruptcyVoluntaryIndicator", Order = 15)]
        public MISMOIndicator BankruptcyVoluntaryIndicator { get; set; }
    
        [XmlIgnore]
        public bool BankruptcyVoluntaryIndicatorSpecified
        {
            get { return this.BankruptcyVoluntaryIndicator != null; }
            set { }
        }
    
        [XmlElement("MaterialChangeInLoanTermsRequestedIndicator", Order = 16)]
        public MISMOIndicator MaterialChangeInLoanTermsRequestedIndicator { get; set; }
    
        [XmlIgnore]
        public bool MaterialChangeInLoanTermsRequestedIndicatorSpecified
        {
            get { return this.MaterialChangeInLoanTermsRequestedIndicator != null; }
            set { }
        }
    
        [XmlElement("MotionForReliefNotFiledReasonDescription", Order = 17)]
        public MISMOString MotionForReliefNotFiledReasonDescription { get; set; }
    
        [XmlIgnore]
        public bool MotionForReliefNotFiledReasonDescriptionSpecified
        {
            get { return this.MotionForReliefNotFiledReasonDescription != null; }
            set { }
        }
    
        [XmlElement("PostPetitionArrearageIndicator", Order = 18)]
        public MISMOIndicator PostPetitionArrearageIndicator { get; set; }
    
        [XmlIgnore]
        public bool PostPetitionArrearageIndicatorSpecified
        {
            get { return this.PostPetitionArrearageIndicator != null; }
            set { }
        }
    
        [XmlElement("PostPetitionPaymentAmount", Order = 19)]
        public MISMOAmount PostPetitionPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool PostPetitionPaymentAmountSpecified
        {
            get { return this.PostPetitionPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("PostPetitionPayToTrusteeIndicator", Order = 20)]
        public MISMOIndicator PostPetitionPayToTrusteeIndicator { get; set; }
    
        [XmlIgnore]
        public bool PostPetitionPayToTrusteeIndicatorSpecified
        {
            get { return this.PostPetitionPayToTrusteeIndicator != null; }
            set { }
        }
    
        [XmlElement("PrePetitionArrearageIndicator", Order = 21)]
        public MISMOIndicator PrePetitionArrearageIndicator { get; set; }
    
        [XmlIgnore]
        public bool PrePetitionArrearageIndicatorSpecified
        {
            get { return this.PrePetitionArrearageIndicator != null; }
            set { }
        }
    
        [XmlElement("PreviousChapterSevenBankruptcyDischargeIndicator", Order = 22)]
        public MISMOIndicator PreviousChapterSevenBankruptcyDischargeIndicator { get; set; }
    
        [XmlIgnore]
        public bool PreviousChapterSevenBankruptcyDischargeIndicatorSpecified
        {
            get { return this.PreviousChapterSevenBankruptcyDischargeIndicator != null; }
            set { }
        }
    
        [XmlElement("PrincipalReductionOfUnsecuredAmount", Order = 23)]
        public MISMOAmount PrincipalReductionOfUnsecuredAmount { get; set; }
    
        [XmlIgnore]
        public bool PrincipalReductionOfUnsecuredAmountSpecified
        {
            get { return this.PrincipalReductionOfUnsecuredAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 24)]
        public BANKRUPTCY_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
