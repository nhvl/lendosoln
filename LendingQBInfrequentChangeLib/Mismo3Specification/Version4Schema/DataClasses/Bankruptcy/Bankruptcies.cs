namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class BANKRUPTCIES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BankruptcyListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BANKRUPTCY", Order = 0)]
        public List<BANKRUPTCY> BankruptcyList { get; set; } = new List<BANKRUPTCY>();
    
        [XmlIgnore]
        public bool BankruptcyListSpecified
        {
            get { return this.BankruptcyList != null && this.BankruptcyList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public BANKRUPTCIES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
