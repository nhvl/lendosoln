namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PARSED_LEGAL_DESCRIPTION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PlattedLandsSpecified
                    || this.UnplattedLandsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PLATTED_LANDS", Order = 0)]
        public PLATTED_LANDS PlattedLands { get; set; }
    
        [XmlIgnore]
        public bool PlattedLandsSpecified
        {
            get { return this.PlattedLands != null && this.PlattedLands.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("UNPLATTED_LANDS", Order = 1)]
        public UNPLATTED_LANDS UnplattedLands { get; set; }
    
        [XmlIgnore]
        public bool UnplattedLandsSpecified
        {
            get { return this.UnplattedLands != null && this.UnplattedLands.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public PARSED_LEGAL_DESCRIPTION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
