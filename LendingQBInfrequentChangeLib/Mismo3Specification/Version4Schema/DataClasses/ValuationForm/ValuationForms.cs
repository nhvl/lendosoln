namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class VALUATION_FORMS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ValuationFormListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("VALUATION_FORM", Order = 0)]
        public List<VALUATION_FORM> ValuationFormList { get; set; } = new List<VALUATION_FORM>();
    
        [XmlIgnore]
        public bool ValuationFormListSpecified
        {
            get { return this.ValuationFormList != null && this.ValuationFormList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public VALUATION_FORMS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
