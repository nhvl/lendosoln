namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class VALUATION_FORM
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PropertyValuationFormIdentifierSpecified
                    || this.PropertyValuationFormTypeSpecified
                    || this.PropertyValuationFormTypeOtherDescriptionSpecified
                    || this.PropertyValuationFormVersionIdentifierSpecified
                    || this.ValuationSoftwareProductNameSpecified
                    || this.ValuationSoftwareProductVersionIdentifierSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PropertyValuationFormIdentifier", Order = 0)]
        public MISMOIdentifier PropertyValuationFormIdentifier { get; set; }
    
        [XmlIgnore]
        public bool PropertyValuationFormIdentifierSpecified
        {
            get { return this.PropertyValuationFormIdentifier != null; }
            set { }
        }
    
        [XmlElement("PropertyValuationFormType", Order = 1)]
        public MISMOEnum<PropertyValuationFormBase> PropertyValuationFormType { get; set; }
    
        [XmlIgnore]
        public bool PropertyValuationFormTypeSpecified
        {
            get { return this.PropertyValuationFormType != null; }
            set { }
        }
    
        [XmlElement("PropertyValuationFormTypeOtherDescription", Order = 2)]
        public MISMOString PropertyValuationFormTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PropertyValuationFormTypeOtherDescriptionSpecified
        {
            get { return this.PropertyValuationFormTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PropertyValuationFormVersionIdentifier", Order = 3)]
        public MISMOIdentifier PropertyValuationFormVersionIdentifier { get; set; }
    
        [XmlIgnore]
        public bool PropertyValuationFormVersionIdentifierSpecified
        {
            get { return this.PropertyValuationFormVersionIdentifier != null; }
            set { }
        }
    
        [XmlElement("ValuationSoftwareProductName", Order = 4)]
        public MISMOString ValuationSoftwareProductName { get; set; }
    
        [XmlIgnore]
        public bool ValuationSoftwareProductNameSpecified
        {
            get { return this.ValuationSoftwareProductName != null; }
            set { }
        }
    
        [XmlElement("ValuationSoftwareProductVersionIdentifier", Order = 5)]
        public MISMOIdentifier ValuationSoftwareProductVersionIdentifier { get; set; }
    
        [XmlIgnore]
        public bool ValuationSoftwareProductVersionIdentifierSpecified
        {
            get { return this.ValuationSoftwareProductVersionIdentifier != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 6)]
        public VALUATION_FORM_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
