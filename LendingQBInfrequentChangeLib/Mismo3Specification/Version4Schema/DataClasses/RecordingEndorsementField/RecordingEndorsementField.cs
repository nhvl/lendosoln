namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class RECORDING_ENDORSEMENT_FIELD
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.RECORDING_ENDORSEMENT_FIELD_REFERENCESpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("RECORDING_ENDORSEMENT_FIELD_REFERENCE", Order = 0)]
        public FIELD_REFERENCE RECORDING_ENDORSEMENT_FIELD_REFERENCE { get; set; }
    
        [XmlIgnore]
        public bool RECORDING_ENDORSEMENT_FIELD_REFERENCESpecified
        {
            get { return this.RECORDING_ENDORSEMENT_FIELD_REFERENCE != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public RECORDING_ENDORSEMENT_FIELD_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
