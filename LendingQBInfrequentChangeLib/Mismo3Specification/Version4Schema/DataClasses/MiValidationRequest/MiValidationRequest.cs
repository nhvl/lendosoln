namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MI_VALIDATION_REQUEST
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MiValidationRequestDetailSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MI_VALIDATION_REQUEST_DETAIL", Order = 0)]
        public MI_VALIDATION_REQUEST_DETAIL MiValidationRequestDetail { get; set; }
    
        [XmlIgnore]
        public bool MiValidationRequestDetailSpecified
        {
            get { return this.MiValidationRequestDetail != null && this.MiValidationRequestDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public MI_VALIDATION_REQUEST_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
