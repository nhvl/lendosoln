namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MI_VALIDATION_REQUEST_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MIValidationRequestTypeSpecified
                    || this.MIValidationRequestTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MIValidationRequestType", Order = 0)]
        public MISMOEnum<MIValidationRequestBase> MIValidationRequestType { get; set; }
    
        [XmlIgnore]
        public bool MIValidationRequestTypeSpecified
        {
            get { return this.MIValidationRequestType != null; }
            set { }
        }
    
        [XmlElement("MIValidationRequestTypeOtherDescription", Order = 1)]
        public MISMOString MIValidationRequestTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MIValidationRequestTypeOtherDescriptionSpecified
        {
            get { return this.MIValidationRequestTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public MI_VALIDATION_REQUEST_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
