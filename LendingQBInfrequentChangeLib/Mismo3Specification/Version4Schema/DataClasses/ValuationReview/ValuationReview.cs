namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class VALUATION_REVIEW
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ValuationReviewCategoriesSpecified
                    || this.ValuationReviewDetailSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("VALUATION_REVIEW_CATEGORIES", Order = 0)]
        public VALUATION_REVIEW_CATEGORIES ValuationReviewCategories { get; set; }
    
        [XmlIgnore]
        public bool ValuationReviewCategoriesSpecified
        {
            get { return this.ValuationReviewCategories != null && this.ValuationReviewCategories.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("VALUATION_REVIEW_DETAIL", Order = 1)]
        public VALUATION_REVIEW_DETAIL ValuationReviewDetail { get; set; }
    
        [XmlIgnore]
        public bool ValuationReviewDetailSpecified
        {
            get { return this.ValuationReviewDetail != null && this.ValuationReviewDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public VALUATION_REVIEW_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
