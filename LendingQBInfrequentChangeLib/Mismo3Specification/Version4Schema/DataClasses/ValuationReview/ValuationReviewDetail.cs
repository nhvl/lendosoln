namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class VALUATION_REVIEW_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AppraisalIdentifierSpecified
                    || this.AppraisalReviewDisagreementDescriptionSpecified
                    || this.AppraisalReviewExtraordinaryAssumptionsDescriptionSpecified
                    || this.AppraisalReviewImprovementsCompletedIndicatorSpecified
                    || this.AppraisalReviewMarketValueChangeCommentDescriptionSpecified
                    || this.AppraisalReviewOriginalAppraisalEffectiveDateSpecified
                    || this.AppraisalReviewOriginalAppraisedValueAccurateIndicatorSpecified
                    || this.AppraisalReviewOriginalAppraisedValueAmountSpecified
                    || this.AppraisalReviewValueConclusionCommentDescriptionSpecified
                    || this.PropertyMarketValueDecreasedIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AppraisalIdentifier", Order = 0)]
        public MISMOIdentifier AppraisalIdentifier { get; set; }
    
        [XmlIgnore]
        public bool AppraisalIdentifierSpecified
        {
            get { return this.AppraisalIdentifier != null; }
            set { }
        }
    
        [XmlElement("AppraisalReviewDisagreementDescription", Order = 1)]
        public MISMOString AppraisalReviewDisagreementDescription { get; set; }
    
        [XmlIgnore]
        public bool AppraisalReviewDisagreementDescriptionSpecified
        {
            get { return this.AppraisalReviewDisagreementDescription != null; }
            set { }
        }
    
        [XmlElement("AppraisalReviewExtraordinaryAssumptionsDescription", Order = 2)]
        public MISMOString AppraisalReviewExtraordinaryAssumptionsDescription { get; set; }
    
        [XmlIgnore]
        public bool AppraisalReviewExtraordinaryAssumptionsDescriptionSpecified
        {
            get { return this.AppraisalReviewExtraordinaryAssumptionsDescription != null; }
            set { }
        }
    
        [XmlElement("AppraisalReviewImprovementsCompletedIndicator", Order = 3)]
        public MISMOIndicator AppraisalReviewImprovementsCompletedIndicator { get; set; }
    
        [XmlIgnore]
        public bool AppraisalReviewImprovementsCompletedIndicatorSpecified
        {
            get { return this.AppraisalReviewImprovementsCompletedIndicator != null; }
            set { }
        }
    
        [XmlElement("AppraisalReviewMarketValueChangeCommentDescription", Order = 4)]
        public MISMOString AppraisalReviewMarketValueChangeCommentDescription { get; set; }
    
        [XmlIgnore]
        public bool AppraisalReviewMarketValueChangeCommentDescriptionSpecified
        {
            get { return this.AppraisalReviewMarketValueChangeCommentDescription != null; }
            set { }
        }
    
        [XmlElement("AppraisalReviewOriginalAppraisalEffectiveDate", Order = 5)]
        public MISMODate AppraisalReviewOriginalAppraisalEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool AppraisalReviewOriginalAppraisalEffectiveDateSpecified
        {
            get { return this.AppraisalReviewOriginalAppraisalEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("AppraisalReviewOriginalAppraisedValueAccurateIndicator", Order = 6)]
        public MISMOIndicator AppraisalReviewOriginalAppraisedValueAccurateIndicator { get; set; }
    
        [XmlIgnore]
        public bool AppraisalReviewOriginalAppraisedValueAccurateIndicatorSpecified
        {
            get { return this.AppraisalReviewOriginalAppraisedValueAccurateIndicator != null; }
            set { }
        }
    
        [XmlElement("AppraisalReviewOriginalAppraisedValueAmount", Order = 7)]
        public MISMOAmount AppraisalReviewOriginalAppraisedValueAmount { get; set; }
    
        [XmlIgnore]
        public bool AppraisalReviewOriginalAppraisedValueAmountSpecified
        {
            get { return this.AppraisalReviewOriginalAppraisedValueAmount != null; }
            set { }
        }
    
        [XmlElement("AppraisalReviewValueConclusionCommentDescription", Order = 8)]
        public MISMOString AppraisalReviewValueConclusionCommentDescription { get; set; }
    
        [XmlIgnore]
        public bool AppraisalReviewValueConclusionCommentDescriptionSpecified
        {
            get { return this.AppraisalReviewValueConclusionCommentDescription != null; }
            set { }
        }
    
        [XmlElement("PropertyMarketValueDecreasedIndicator", Order = 9)]
        public MISMOIndicator PropertyMarketValueDecreasedIndicator { get; set; }
    
        [XmlIgnore]
        public bool PropertyMarketValueDecreasedIndicatorSpecified
        {
            get { return this.PropertyMarketValueDecreasedIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 10)]
        public VALUATION_REVIEW_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
