namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class TITLE_ENDORSEMENTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.TitleEndorsementListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("TITLE_ENDORSEMENT", Order = 0)]
        public List<TITLE_ENDORSEMENT> TitleEndorsementList { get; set; } = new List<TITLE_ENDORSEMENT>();
    
        [XmlIgnore]
        public bool TitleEndorsementListSpecified
        {
            get { return this.TitleEndorsementList != null && this.TitleEndorsementList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public TITLE_ENDORSEMENTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
