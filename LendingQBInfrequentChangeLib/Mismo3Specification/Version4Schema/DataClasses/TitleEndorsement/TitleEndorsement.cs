namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class TITLE_ENDORSEMENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.TitleEndorsementDescriptionSpecified
                    || this.TitleEndorsementFormIdentifierSpecified
                    || this.TitleEndorsementFormNameSpecified
                    || this.TitleEndorsementIncludedIndicatorSpecified
                    || this.TitleEndorsementSourceTypeSpecified
                    || this.TitleEndorsementSourceTypeOtherDescriptionSpecified
                    || this.TitleEndorsementStatuteDescriptionSpecified
                    || this.TitleEndorsementStatuteIncludedIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("TitleEndorsementDescription", Order = 0)]
        public MISMOString TitleEndorsementDescription { get; set; }
    
        [XmlIgnore]
        public bool TitleEndorsementDescriptionSpecified
        {
            get { return this.TitleEndorsementDescription != null; }
            set { }
        }
    
        [XmlElement("TitleEndorsementFormIdentifier", Order = 1)]
        public MISMOIdentifier TitleEndorsementFormIdentifier { get; set; }
    
        [XmlIgnore]
        public bool TitleEndorsementFormIdentifierSpecified
        {
            get { return this.TitleEndorsementFormIdentifier != null; }
            set { }
        }
    
        [XmlElement("TitleEndorsementFormName", Order = 2)]
        public MISMOString TitleEndorsementFormName { get; set; }
    
        [XmlIgnore]
        public bool TitleEndorsementFormNameSpecified
        {
            get { return this.TitleEndorsementFormName != null; }
            set { }
        }
    
        [XmlElement("TitleEndorsementIncludedIndicator", Order = 3)]
        public MISMOIndicator TitleEndorsementIncludedIndicator { get; set; }
    
        [XmlIgnore]
        public bool TitleEndorsementIncludedIndicatorSpecified
        {
            get { return this.TitleEndorsementIncludedIndicator != null; }
            set { }
        }
    
        [XmlElement("TitleEndorsementSourceType", Order = 4)]
        public MISMOEnum<TitleEndorsementSourceBase> TitleEndorsementSourceType { get; set; }
    
        [XmlIgnore]
        public bool TitleEndorsementSourceTypeSpecified
        {
            get { return this.TitleEndorsementSourceType != null; }
            set { }
        }
    
        [XmlElement("TitleEndorsementSourceTypeOtherDescription", Order = 5)]
        public MISMOString TitleEndorsementSourceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool TitleEndorsementSourceTypeOtherDescriptionSpecified
        {
            get { return this.TitleEndorsementSourceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("TitleEndorsementStatuteDescription", Order = 6)]
        public MISMOString TitleEndorsementStatuteDescription { get; set; }
    
        [XmlIgnore]
        public bool TitleEndorsementStatuteDescriptionSpecified
        {
            get { return this.TitleEndorsementStatuteDescription != null; }
            set { }
        }
    
        [XmlElement("TitleEndorsementStatuteIncludedIndicator", Order = 7)]
        public MISMOIndicator TitleEndorsementStatuteIncludedIndicator { get; set; }
    
        [XmlIgnore]
        public bool TitleEndorsementStatuteIncludedIndicatorSpecified
        {
            get { return this.TitleEndorsementStatuteIncludedIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 8)]
        public TITLE_ENDORSEMENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
