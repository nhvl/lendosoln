namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CONTACT_POINT_SOCIAL_MEDIA
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SocialMediaIdentifierSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SocialMediaIdentifier", Order = 0)]
        public MISMOIdentifier SocialMediaIdentifier { get; set; }
    
        [XmlIgnore]
        public bool SocialMediaIdentifierSpecified
        {
            get { return this.SocialMediaIdentifier != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public CONTACT_POINT_SOCIAL_MEDIA_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
