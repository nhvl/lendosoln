namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class COMPENSATIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CompensationListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("COMPENSATION", Order = 0)]
        public List<COMPENSATION> CompensationList { get; set; } = new List<COMPENSATION>();
    
        [XmlIgnore]
        public bool CompensationListSpecified
        {
            get { return this.CompensationList != null && this.CompensationList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public COMPENSATIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
