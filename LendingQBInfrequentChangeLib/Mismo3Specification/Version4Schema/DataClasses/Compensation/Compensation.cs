namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class COMPENSATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CompensationActualAmountSpecified
                    || this.CompensationEstimatedAmountSpecified
                    || this.CompensationPaidByTypeSpecified
                    || this.CompensationPaidByTypeOtherDescriptionSpecified
                    || this.CompensationPaidToTypeSpecified
                    || this.CompensationPaidToTypeOtherDescriptionSpecified
                    || this.CompensationPercentSpecified
                    || this.CompensationPlanIdentifierSpecified
                    || this.CompensationPlanMaximumAmountSpecified
                    || this.CompensationPlanMinimumAmountSpecified
                    || this.CompensationSpecifiedFixedAmountSpecified
                    || this.CompensationTypeSpecified
                    || this.CompensationTypeOtherDescriptionSpecified
                    || this.GFEAggregationTypeSpecified
                    || this.GFEAggregationTypeOtherDescriptionSpecified
                    || this.GFEDisclosedCompensationAmountSpecified
                    || this.IntegratedDisclosureLineNumberValueSpecified
                    || this.IntegratedDisclosureSectionTypeSpecified
                    || this.IntegratedDisclosureSectionTypeOtherDescriptionSpecified
                    || this.IntegratedDisclosureSubsectionTypeSpecified
                    || this.IntegratedDisclosureSubsectionTypeOtherDescriptionSpecified
                    || this.PaymentIncludedInAPRIndicatorSpecified
                    || this.PaymentIncludedInJurisdictionHighCostIndicatorSpecified
                    || this.RegulationZPointsAndFeesIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CompensationActualAmount", Order = 0)]
        public MISMOAmount CompensationActualAmount { get; set; }
    
        [XmlIgnore]
        public bool CompensationActualAmountSpecified
        {
            get { return this.CompensationActualAmount != null; }
            set { }
        }
    
        [XmlElement("CompensationEstimatedAmount", Order = 1)]
        public MISMOAmount CompensationEstimatedAmount { get; set; }
    
        [XmlIgnore]
        public bool CompensationEstimatedAmountSpecified
        {
            get { return this.CompensationEstimatedAmount != null; }
            set { }
        }
    
        [XmlElement("CompensationPaidByType", Order = 2)]
        public MISMOEnum<CompensationPaidByBase> CompensationPaidByType { get; set; }
    
        [XmlIgnore]
        public bool CompensationPaidByTypeSpecified
        {
            get { return this.CompensationPaidByType != null; }
            set { }
        }
    
        [XmlElement("CompensationPaidByTypeOtherDescription", Order = 3)]
        public MISMOString CompensationPaidByTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CompensationPaidByTypeOtherDescriptionSpecified
        {
            get { return this.CompensationPaidByTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("CompensationPaidToType", Order = 4)]
        public MISMOEnum<CompensationPaidToBase> CompensationPaidToType { get; set; }
    
        [XmlIgnore]
        public bool CompensationPaidToTypeSpecified
        {
            get { return this.CompensationPaidToType != null; }
            set { }
        }
    
        [XmlElement("CompensationPaidToTypeOtherDescription", Order = 5)]
        public MISMOString CompensationPaidToTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CompensationPaidToTypeOtherDescriptionSpecified
        {
            get { return this.CompensationPaidToTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("CompensationPercent", Order = 6)]
        public MISMOPercent CompensationPercent { get; set; }
    
        [XmlIgnore]
        public bool CompensationPercentSpecified
        {
            get { return this.CompensationPercent != null; }
            set { }
        }
    
        [XmlElement("CompensationPlanIdentifier", Order = 7)]
        public MISMOIdentifier CompensationPlanIdentifier { get; set; }
    
        [XmlIgnore]
        public bool CompensationPlanIdentifierSpecified
        {
            get { return this.CompensationPlanIdentifier != null; }
            set { }
        }
    
        [XmlElement("CompensationPlanMaximumAmount", Order = 8)]
        public MISMOAmount CompensationPlanMaximumAmount { get; set; }
    
        [XmlIgnore]
        public bool CompensationPlanMaximumAmountSpecified
        {
            get { return this.CompensationPlanMaximumAmount != null; }
            set { }
        }
    
        [XmlElement("CompensationPlanMinimumAmount", Order = 9)]
        public MISMOAmount CompensationPlanMinimumAmount { get; set; }
    
        [XmlIgnore]
        public bool CompensationPlanMinimumAmountSpecified
        {
            get { return this.CompensationPlanMinimumAmount != null; }
            set { }
        }
    
        [XmlElement("CompensationSpecifiedFixedAmount", Order = 10)]
        public MISMOAmount CompensationSpecifiedFixedAmount { get; set; }
    
        [XmlIgnore]
        public bool CompensationSpecifiedFixedAmountSpecified
        {
            get { return this.CompensationSpecifiedFixedAmount != null; }
            set { }
        }
    
        [XmlElement("CompensationType", Order = 11)]
        public MISMOEnum<CompensationBase> CompensationType { get; set; }
    
        [XmlIgnore]
        public bool CompensationTypeSpecified
        {
            get { return this.CompensationType != null; }
            set { }
        }
    
        [XmlElement("CompensationTypeOtherDescription", Order = 12)]
        public MISMOString CompensationTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CompensationTypeOtherDescriptionSpecified
        {
            get { return this.CompensationTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("GFEAggregationType", Order = 13)]
        public MISMOEnum<GFEAggregationBase> GFEAggregationType { get; set; }
    
        [XmlIgnore]
        public bool GFEAggregationTypeSpecified
        {
            get { return this.GFEAggregationType != null; }
            set { }
        }
    
        [XmlElement("GFEAggregationTypeOtherDescription", Order = 14)]
        public MISMOString GFEAggregationTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool GFEAggregationTypeOtherDescriptionSpecified
        {
            get { return this.GFEAggregationTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("GFEDisclosedCompensationAmount", Order = 15)]
        public MISMOAmount GFEDisclosedCompensationAmount { get; set; }
    
        [XmlIgnore]
        public bool GFEDisclosedCompensationAmountSpecified
        {
            get { return this.GFEDisclosedCompensationAmount != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureLineNumberValue", Order = 16)]
        public MISMOValue IntegratedDisclosureLineNumberValue { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureLineNumberValueSpecified
        {
            get { return this.IntegratedDisclosureLineNumberValue != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureSectionType", Order = 17)]
        public MISMOEnum<IntegratedDisclosureSectionBase> IntegratedDisclosureSectionType { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureSectionTypeSpecified
        {
            get { return this.IntegratedDisclosureSectionType != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureSectionTypeOtherDescription", Order = 18)]
        public MISMOString IntegratedDisclosureSectionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureSectionTypeOtherDescriptionSpecified
        {
            get { return this.IntegratedDisclosureSectionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureSubsectionType", Order = 19)]
        public MISMOEnum<IntegratedDisclosureSubsectionBase> IntegratedDisclosureSubsectionType { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureSubsectionTypeSpecified
        {
            get { return this.IntegratedDisclosureSubsectionType != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureSubsectionTypeOtherDescription", Order = 20)]
        public MISMOString IntegratedDisclosureSubsectionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureSubsectionTypeOtherDescriptionSpecified
        {
            get { return this.IntegratedDisclosureSubsectionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PaymentIncludedInAPRIndicator", Order = 21)]
        public MISMOIndicator PaymentIncludedInAPRIndicator { get; set; }
    
        [XmlIgnore]
        public bool PaymentIncludedInAPRIndicatorSpecified
        {
            get { return this.PaymentIncludedInAPRIndicator != null; }
            set { }
        }
    
        [XmlElement("PaymentIncludedInJurisdictionHighCostIndicator", Order = 22)]
        public MISMOIndicator PaymentIncludedInJurisdictionHighCostIndicator { get; set; }
    
        [XmlIgnore]
        public bool PaymentIncludedInJurisdictionHighCostIndicatorSpecified
        {
            get { return this.PaymentIncludedInJurisdictionHighCostIndicator != null; }
            set { }
        }
    
        [XmlElement("RegulationZPointsAndFeesIndicator", Order = 23)]
        public MISMOIndicator RegulationZPointsAndFeesIndicator { get; set; }
    
        [XmlIgnore]
        public bool RegulationZPointsAndFeesIndicatorSpecified
        {
            get { return this.RegulationZPointsAndFeesIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 24)]
        public COMPENSATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
