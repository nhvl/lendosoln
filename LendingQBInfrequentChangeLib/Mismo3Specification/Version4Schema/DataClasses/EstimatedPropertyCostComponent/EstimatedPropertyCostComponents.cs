namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class ESTIMATED_PROPERTY_COST_COMPONENTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EstimatedPropertyCostComponentListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ESTIMATED_PROPERTY_COST_COMPONENT", Order = 0)]
        public List<ESTIMATED_PROPERTY_COST_COMPONENT> EstimatedPropertyCostComponentList { get; set; } = new List<ESTIMATED_PROPERTY_COST_COMPONENT>();
    
        [XmlIgnore]
        public bool EstimatedPropertyCostComponentListSpecified
        {
            get { return this.EstimatedPropertyCostComponentList != null && this.EstimatedPropertyCostComponentList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public ESTIMATED_PROPERTY_COST_COMPONENTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
