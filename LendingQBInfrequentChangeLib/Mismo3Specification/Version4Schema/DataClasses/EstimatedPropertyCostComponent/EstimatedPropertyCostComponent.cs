namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ESTIMATED_PROPERTY_COST_COMPONENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ProjectedPaymentEscrowedTypeSpecified
                    || this.ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentTypeSpecified
                    || this.ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ProjectedPaymentEscrowedType", Order = 0)]
        public MISMOEnum<ProjectedPaymentEscrowedBase> ProjectedPaymentEscrowedType { get; set; }
    
        [XmlIgnore]
        public bool ProjectedPaymentEscrowedTypeSpecified
        {
            get { return this.ProjectedPaymentEscrowedType != null; }
            set { }
        }
    
        [XmlElement("ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentType", Order = 1)]
        public MISMOEnum<ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentBase> ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentType { get; set; }
    
        [XmlIgnore]
        public bool ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentTypeSpecified
        {
            get { return this.ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentType != null; }
            set { }
        }
    
        [XmlElement("ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentTypeOtherDescription", Order = 2)]
        public MISMOString ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentTypeOtherDescriptionSpecified
        {
            get { return this.ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public ESTIMATED_PROPERTY_COST_COMPONENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
