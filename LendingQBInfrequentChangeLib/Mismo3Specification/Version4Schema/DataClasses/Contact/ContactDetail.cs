namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CONTACT_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AttorneyInFactIndicatorSpecified
                    || this.ContactPartyIdentifierSpecified
                    || this.SignatoryRoleTypeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AttorneyInFactIndicator", Order = 0)]
        public MISMOIndicator AttorneyInFactIndicator { get; set; }
    
        [XmlIgnore]
        public bool AttorneyInFactIndicatorSpecified
        {
            get { return this.AttorneyInFactIndicator != null; }
            set { }
        }
    
        [XmlElement("ContactPartyIdentifier", Order = 1)]
        public MISMOIdentifier ContactPartyIdentifier { get; set; }
    
        [XmlIgnore]
        public bool ContactPartyIdentifierSpecified
        {
            get { return this.ContactPartyIdentifier != null; }
            set { }
        }
    
        [XmlElement("SignatoryRoleType", Order = 2)]
        public MISMOEnum<SignatoryRoleBase> SignatoryRoleType { get; set; }
    
        [XmlIgnore]
        public bool SignatoryRoleTypeSpecified
        {
            get { return this.SignatoryRoleType != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public CONTACT_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
