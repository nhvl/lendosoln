namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CONTACT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ContactDetailSpecified
                    || this.ContactPointsSpecified
                    || this.IdentificationVerificationSpecified
                    || this.NameSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CONTACT_DETAIL", Order = 0)]
        public CONTACT_DETAIL ContactDetail { get; set; }
    
        [XmlIgnore]
        public bool ContactDetailSpecified
        {
            get { return this.ContactDetail != null && this.ContactDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CONTACT_POINTS", Order = 1)]
        public CONTACT_POINTS ContactPoints { get; set; }
    
        [XmlIgnore]
        public bool ContactPointsSpecified
        {
            get { return this.ContactPoints != null && this.ContactPoints.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("IDENTIFICATION_VERIFICATION", Order = 2)]
        public IDENTIFICATION_VERIFICATION IdentificationVerification { get; set; }
    
        [XmlIgnore]
        public bool IdentificationVerificationSpecified
        {
            get { return this.IdentificationVerification != null && this.IdentificationVerification.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("NAME", Order = 3)]
        public NAME Name { get; set; }
    
        [XmlIgnore]
        public bool NameSpecified
        {
            get { return this.Name != null && this.Name.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public CONTACT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
