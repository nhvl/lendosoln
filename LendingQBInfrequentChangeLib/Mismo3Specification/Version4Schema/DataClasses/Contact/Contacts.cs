namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class CONTACTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ContactListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CONTACT", Order = 0)]
        public List<CONTACT> ContactList { get; set; } = new List<CONTACT>();
    
        [XmlIgnore]
        public bool ContactListSpecified
        {
            get { return this.ContactList != null && this.ContactList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public CONTACTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
