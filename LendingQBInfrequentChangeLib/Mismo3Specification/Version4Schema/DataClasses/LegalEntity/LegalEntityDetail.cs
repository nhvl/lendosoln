namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class LEGAL_ENTITY_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EntityHomePageURLSpecified
                    || this.FullNameSpecified
                    || this.GlobalLegalEntityIdentifierSpecified
                    || this.LegalEntityLicensingTypeDescriptionSpecified
                    || this.LegalEntityOrganizedUnderTheLawsOfJurisdictionNameSpecified
                    || this.LegalEntitySuccessorClauseTextDescriptionSpecified
                    || this.LegalEntityTypeSpecified
                    || this.LegalEntityTypeOtherDescriptionSpecified
                    || this.MERSOrganizationIdentifierSpecified
                    || this.RegisteredDomainNameURISpecified
                    || this.RequiredSignatoryCountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("EntityHomePageURL", Order = 0)]
        public MISMOURL EntityHomePageURL { get; set; }
    
        [XmlIgnore]
        public bool EntityHomePageURLSpecified
        {
            get { return this.EntityHomePageURL != null; }
            set { }
        }
    
        [XmlElement("FullName", Order = 1)]
        public MISMOString FullName { get; set; }
    
        [XmlIgnore]
        public bool FullNameSpecified
        {
            get { return this.FullName != null; }
            set { }
        }
    
        [XmlElement("GlobalLegalEntityIdentifier", Order = 2)]
        public MISMOIdentifier GlobalLegalEntityIdentifier { get; set; }
    
        [XmlIgnore]
        public bool GlobalLegalEntityIdentifierSpecified
        {
            get { return this.GlobalLegalEntityIdentifier != null; }
            set { }
        }
    
        [XmlElement("LegalEntityLicensingTypeDescription", Order = 3)]
        public MISMOString LegalEntityLicensingTypeDescription { get; set; }
    
        [XmlIgnore]
        public bool LegalEntityLicensingTypeDescriptionSpecified
        {
            get { return this.LegalEntityLicensingTypeDescription != null; }
            set { }
        }
    
        [XmlElement("LegalEntityOrganizedUnderTheLawsOfJurisdictionName", Order = 4)]
        public MISMOString LegalEntityOrganizedUnderTheLawsOfJurisdictionName { get; set; }
    
        [XmlIgnore]
        public bool LegalEntityOrganizedUnderTheLawsOfJurisdictionNameSpecified
        {
            get { return this.LegalEntityOrganizedUnderTheLawsOfJurisdictionName != null; }
            set { }
        }
    
        [XmlElement("LegalEntitySuccessorClauseTextDescription", Order = 5)]
        public MISMOString LegalEntitySuccessorClauseTextDescription { get; set; }
    
        [XmlIgnore]
        public bool LegalEntitySuccessorClauseTextDescriptionSpecified
        {
            get { return this.LegalEntitySuccessorClauseTextDescription != null; }
            set { }
        }
    
        [XmlElement("LegalEntityType", Order = 6)]
        public MISMOEnum<LegalEntityBase> LegalEntityType { get; set; }
    
        [XmlIgnore]
        public bool LegalEntityTypeSpecified
        {
            get { return this.LegalEntityType != null; }
            set { }
        }
    
        [XmlElement("LegalEntityTypeOtherDescription", Order = 7)]
        public MISMOString LegalEntityTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool LegalEntityTypeOtherDescriptionSpecified
        {
            get { return this.LegalEntityTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MERSOrganizationIdentifier", Order = 8)]
        public MISMOIdentifier MERSOrganizationIdentifier { get; set; }
    
        [XmlIgnore]
        public bool MERSOrganizationIdentifierSpecified
        {
            get { return this.MERSOrganizationIdentifier != null; }
            set { }
        }
    
        [XmlElement("RegisteredDomainNameURI", Order = 9)]
        public MISMOURI RegisteredDomainNameURI { get; set; }
    
        [XmlIgnore]
        public bool RegisteredDomainNameURISpecified
        {
            get { return this.RegisteredDomainNameURI != null; }
            set { }
        }
    
        [XmlElement("RequiredSignatoryCount", Order = 10)]
        public MISMOCount RequiredSignatoryCount { get; set; }
    
        [XmlIgnore]
        public bool RequiredSignatoryCountSpecified
        {
            get { return this.RequiredSignatoryCount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 11)]
        public LEGAL_ENTITY_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
