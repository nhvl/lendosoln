namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class LEGAL_ENTITY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AliasesSpecified
                    || this.ContactsSpecified
                    || this.LegalEntityDetailSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ALIASES", Order = 0)]
        public ALIASES Aliases { get; set; }
    
        [XmlIgnore]
        public bool AliasesSpecified
        {
            get { return this.Aliases != null && this.Aliases.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CONTACTS", Order = 1)]
        public CONTACTS Contacts { get; set; }
    
        [XmlIgnore]
        public bool ContactsSpecified
        {
            get { return this.Contacts != null && this.Contacts.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("LEGAL_ENTITY_DETAIL", Order = 2)]
        public LEGAL_ENTITY_DETAIL LegalEntityDetail { get; set; }
    
        [XmlIgnore]
        public bool LegalEntityDetailSpecified
        {
            get { return this.LegalEntityDetail != null && this.LegalEntityDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public LEGAL_ENTITY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
