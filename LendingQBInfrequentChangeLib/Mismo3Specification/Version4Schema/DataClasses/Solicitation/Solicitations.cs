namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class SOLICITATIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SolicitationListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SOLICITATION", Order = 0)]
        public List<SOLICITATION> SolicitationList { get; set; } = new List<SOLICITATION>();
    
        [XmlIgnore]
        public bool SolicitationListSpecified
        {
            get { return this.SolicitationList != null && this.SolicitationList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public SOLICITATIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
