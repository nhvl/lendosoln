namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SOLICITATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SolicitationDateSpecified
                    || this.SolicitationMethodTypeSpecified
                    || this.SolicitationMethodTypeOtherDescriptionSpecified
                    || this.WorkoutSolicitationProgramIdentifierSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SolicitationDate", Order = 0)]
        public MISMODate SolicitationDate { get; set; }
    
        [XmlIgnore]
        public bool SolicitationDateSpecified
        {
            get { return this.SolicitationDate != null; }
            set { }
        }
    
        [XmlElement("SolicitationMethodType", Order = 1)]
        public MISMOEnum<SolicitationMethodBase> SolicitationMethodType { get; set; }
    
        [XmlIgnore]
        public bool SolicitationMethodTypeSpecified
        {
            get { return this.SolicitationMethodType != null; }
            set { }
        }
    
        [XmlElement("SolicitationMethodTypeOtherDescription", Order = 2)]
        public MISMOString SolicitationMethodTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool SolicitationMethodTypeOtherDescriptionSpecified
        {
            get { return this.SolicitationMethodTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("WorkoutSolicitationProgramIdentifier", Order = 3)]
        public MISMOIdentifier WorkoutSolicitationProgramIdentifier { get; set; }
    
        [XmlIgnore]
        public bool WorkoutSolicitationProgramIdentifierSpecified
        {
            get { return this.WorkoutSolicitationProgramIdentifier != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public SOLICITATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
