namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PROPERTY_TAX
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PropertyTaxDetailSpecified
                    || this.PropertyTaxExemptionsSpecified
                    || this.PropertyTaxSpecialsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PROPERTY_TAX_DETAIL", Order = 0)]
        public PROPERTY_TAX_DETAIL PropertyTaxDetail { get; set; }
    
        [XmlIgnore]
        public bool PropertyTaxDetailSpecified
        {
            get { return this.PropertyTaxDetail != null && this.PropertyTaxDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PROPERTY_TAX_EXEMPTIONS", Order = 1)]
        public PROPERTY_TAX_EXEMPTIONS PropertyTaxExemptions { get; set; }
    
        [XmlIgnore]
        public bool PropertyTaxExemptionsSpecified
        {
            get { return this.PropertyTaxExemptions != null && this.PropertyTaxExemptions.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PROPERTY_TAX_SPECIALS", Order = 2)]
        public PROPERTY_TAX_SPECIALS PropertyTaxSpecials { get; set; }
    
        [XmlIgnore]
        public bool PropertyTaxSpecialsSpecified
        {
            get { return this.PropertyTaxSpecials != null && this.PropertyTaxSpecials.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public PROPERTY_TAX_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
