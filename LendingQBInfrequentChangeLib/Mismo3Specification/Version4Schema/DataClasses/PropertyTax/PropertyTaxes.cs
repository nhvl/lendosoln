namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class PROPERTY_TAXES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PropertyTaxListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PROPERTY_TAX", Order = 0)]
        public List<PROPERTY_TAX> PropertyTaxList { get; set; } = new List<PROPERTY_TAX>();
    
        [XmlIgnore]
        public bool PropertyTaxListSpecified
        {
            get { return this.PropertyTaxList != null && this.PropertyTaxList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public PROPERTY_TAXES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
