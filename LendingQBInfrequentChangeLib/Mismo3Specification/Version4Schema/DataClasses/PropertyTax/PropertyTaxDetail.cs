namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PROPERTY_TAX_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PropertyTaxAssessmentEndYearSpecified
                    || this.PropertyTaxAssessmentStartYearSpecified
                    || this.PropertyTaxAtypicalCommentDescriptionSpecified
                    || this.PropertyTaxCertificateDateSpecified
                    || this.PropertyTaxCountyRateAreaIdentifierSpecified
                    || this.PropertyTaxDelinquentIndicatorSpecified
                    || this.PropertyTaxEndYearSpecified
                    || this.PropertyTaxExemptionIndicatorSpecified
                    || this.PropertyTaxImprovementValueAmountSpecified
                    || this.PropertyTaxLandValueAmountSpecified
                    || this.PropertyTaxTotalAssessedValueAmountSpecified
                    || this.PropertyTaxTotalSpecialTaxAmountSpecified
                    || this.PropertyTaxTotalTaxableValueAmountSpecified
                    || this.PropertyTaxTotalTaxAmountSpecified
                    || this.PropertyTaxTotalTaxWithoutSpecialAssessmentsAmountSpecified
                    || this.PropertyTaxToValueRatePercentSpecified
                    || this.PropertyTaxTypicalTaxIndicatorSpecified
                    || this.PropertyTaxYearIdentifierSpecified
                    || this.TaxAuthorityAccountBillIdentifierSpecified
                    || this.TaxAuthorityTypeSpecified
                    || this.TaxAuthorityTypeOtherDescriptionSpecified
                    || this.TaxContractIdentifierSpecified
                    || this.TaxContractLifeOfLoanIndicatorSpecified
                    || this.TaxContractOrderDateSpecified
                    || this.TaxContractSuffixIdentifierSpecified
                    || this.TaxEscrowedIndicatorSpecified
                    || this.TaxItemExemptionIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PropertyTaxAssessmentEndYear", Order = 0)]
        public MISMOYear PropertyTaxAssessmentEndYear { get; set; }
    
        [XmlIgnore]
        public bool PropertyTaxAssessmentEndYearSpecified
        {
            get { return this.PropertyTaxAssessmentEndYear != null; }
            set { }
        }
    
        [XmlElement("PropertyTaxAssessmentStartYear", Order = 1)]
        public MISMOYear PropertyTaxAssessmentStartYear { get; set; }
    
        [XmlIgnore]
        public bool PropertyTaxAssessmentStartYearSpecified
        {
            get { return this.PropertyTaxAssessmentStartYear != null; }
            set { }
        }
    
        [XmlElement("PropertyTaxAtypicalCommentDescription", Order = 2)]
        public MISMOString PropertyTaxAtypicalCommentDescription { get; set; }
    
        [XmlIgnore]
        public bool PropertyTaxAtypicalCommentDescriptionSpecified
        {
            get { return this.PropertyTaxAtypicalCommentDescription != null; }
            set { }
        }
    
        [XmlElement("PropertyTaxCertificateDate", Order = 3)]
        public MISMODate PropertyTaxCertificateDate { get; set; }
    
        [XmlIgnore]
        public bool PropertyTaxCertificateDateSpecified
        {
            get { return this.PropertyTaxCertificateDate != null; }
            set { }
        }
    
        [XmlElement("PropertyTaxCountyRateAreaIdentifier", Order = 4)]
        public MISMOIdentifier PropertyTaxCountyRateAreaIdentifier { get; set; }
    
        [XmlIgnore]
        public bool PropertyTaxCountyRateAreaIdentifierSpecified
        {
            get { return this.PropertyTaxCountyRateAreaIdentifier != null; }
            set { }
        }
    
        [XmlElement("PropertyTaxDelinquentIndicator", Order = 5)]
        public MISMOIndicator PropertyTaxDelinquentIndicator { get; set; }
    
        [XmlIgnore]
        public bool PropertyTaxDelinquentIndicatorSpecified
        {
            get { return this.PropertyTaxDelinquentIndicator != null; }
            set { }
        }
    
        [XmlElement("PropertyTaxEndYear", Order = 6)]
        public MISMOYear PropertyTaxEndYear { get; set; }
    
        [XmlIgnore]
        public bool PropertyTaxEndYearSpecified
        {
            get { return this.PropertyTaxEndYear != null; }
            set { }
        }
    
        [XmlElement("PropertyTaxExemptionIndicator", Order = 7)]
        public MISMOIndicator PropertyTaxExemptionIndicator { get; set; }
    
        [XmlIgnore]
        public bool PropertyTaxExemptionIndicatorSpecified
        {
            get { return this.PropertyTaxExemptionIndicator != null; }
            set { }
        }
    
        [XmlElement("PropertyTaxImprovementValueAmount", Order = 8)]
        public MISMOAmount PropertyTaxImprovementValueAmount { get; set; }
    
        [XmlIgnore]
        public bool PropertyTaxImprovementValueAmountSpecified
        {
            get { return this.PropertyTaxImprovementValueAmount != null; }
            set { }
        }
    
        [XmlElement("PropertyTaxLandValueAmount", Order = 9)]
        public MISMOAmount PropertyTaxLandValueAmount { get; set; }
    
        [XmlIgnore]
        public bool PropertyTaxLandValueAmountSpecified
        {
            get { return this.PropertyTaxLandValueAmount != null; }
            set { }
        }
    
        [XmlElement("PropertyTaxTotalAssessedValueAmount", Order = 10)]
        public MISMOAmount PropertyTaxTotalAssessedValueAmount { get; set; }
    
        [XmlIgnore]
        public bool PropertyTaxTotalAssessedValueAmountSpecified
        {
            get { return this.PropertyTaxTotalAssessedValueAmount != null; }
            set { }
        }
    
        [XmlElement("PropertyTaxTotalSpecialTaxAmount", Order = 11)]
        public MISMOAmount PropertyTaxTotalSpecialTaxAmount { get; set; }
    
        [XmlIgnore]
        public bool PropertyTaxTotalSpecialTaxAmountSpecified
        {
            get { return this.PropertyTaxTotalSpecialTaxAmount != null; }
            set { }
        }
    
        [XmlElement("PropertyTaxTotalTaxableValueAmount", Order = 12)]
        public MISMOAmount PropertyTaxTotalTaxableValueAmount { get; set; }
    
        [XmlIgnore]
        public bool PropertyTaxTotalTaxableValueAmountSpecified
        {
            get { return this.PropertyTaxTotalTaxableValueAmount != null; }
            set { }
        }
    
        [XmlElement("PropertyTaxTotalTaxAmount", Order = 13)]
        public MISMOAmount PropertyTaxTotalTaxAmount { get; set; }
    
        [XmlIgnore]
        public bool PropertyTaxTotalTaxAmountSpecified
        {
            get { return this.PropertyTaxTotalTaxAmount != null; }
            set { }
        }
    
        [XmlElement("PropertyTaxTotalTaxWithoutSpecialAssessmentsAmount", Order = 14)]
        public MISMOAmount PropertyTaxTotalTaxWithoutSpecialAssessmentsAmount { get; set; }
    
        [XmlIgnore]
        public bool PropertyTaxTotalTaxWithoutSpecialAssessmentsAmountSpecified
        {
            get { return this.PropertyTaxTotalTaxWithoutSpecialAssessmentsAmount != null; }
            set { }
        }
    
        [XmlElement("PropertyTaxToValueRatePercent", Order = 15)]
        public MISMOPercent PropertyTaxToValueRatePercent { get; set; }
    
        [XmlIgnore]
        public bool PropertyTaxToValueRatePercentSpecified
        {
            get { return this.PropertyTaxToValueRatePercent != null; }
            set { }
        }
    
        [XmlElement("PropertyTaxTypicalTaxIndicator", Order = 16)]
        public MISMOIndicator PropertyTaxTypicalTaxIndicator { get; set; }
    
        [XmlIgnore]
        public bool PropertyTaxTypicalTaxIndicatorSpecified
        {
            get { return this.PropertyTaxTypicalTaxIndicator != null; }
            set { }
        }
    
        [XmlElement("PropertyTaxYearIdentifier", Order = 17)]
        public MISMOIdentifier PropertyTaxYearIdentifier { get; set; }
    
        [XmlIgnore]
        public bool PropertyTaxYearIdentifierSpecified
        {
            get { return this.PropertyTaxYearIdentifier != null; }
            set { }
        }
    
        [XmlElement("TaxAuthorityAccountBillIdentifier", Order = 18)]
        public MISMOIdentifier TaxAuthorityAccountBillIdentifier { get; set; }
    
        [XmlIgnore]
        public bool TaxAuthorityAccountBillIdentifierSpecified
        {
            get { return this.TaxAuthorityAccountBillIdentifier != null; }
            set { }
        }
    
        [XmlElement("TaxAuthorityType", Order = 19)]
        public MISMOEnum<TaxAuthorityBase> TaxAuthorityType { get; set; }
    
        [XmlIgnore]
        public bool TaxAuthorityTypeSpecified
        {
            get { return this.TaxAuthorityType != null; }
            set { }
        }
    
        [XmlElement("TaxAuthorityTypeOtherDescription", Order = 20)]
        public MISMOString TaxAuthorityTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool TaxAuthorityTypeOtherDescriptionSpecified
        {
            get { return this.TaxAuthorityTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("TaxContractIdentifier", Order = 21)]
        public MISMOIdentifier TaxContractIdentifier { get; set; }
    
        [XmlIgnore]
        public bool TaxContractIdentifierSpecified
        {
            get { return this.TaxContractIdentifier != null; }
            set { }
        }
    
        [XmlElement("TaxContractLifeOfLoanIndicator", Order = 22)]
        public MISMOIndicator TaxContractLifeOfLoanIndicator { get; set; }
    
        [XmlIgnore]
        public bool TaxContractLifeOfLoanIndicatorSpecified
        {
            get { return this.TaxContractLifeOfLoanIndicator != null; }
            set { }
        }
    
        [XmlElement("TaxContractOrderDate", Order = 23)]
        public MISMODate TaxContractOrderDate { get; set; }
    
        [XmlIgnore]
        public bool TaxContractOrderDateSpecified
        {
            get { return this.TaxContractOrderDate != null; }
            set { }
        }
    
        [XmlElement("TaxContractSuffixIdentifier", Order = 24)]
        public MISMOIdentifier TaxContractSuffixIdentifier { get; set; }
    
        [XmlIgnore]
        public bool TaxContractSuffixIdentifierSpecified
        {
            get { return this.TaxContractSuffixIdentifier != null; }
            set { }
        }
    
        [XmlElement("TaxEscrowedIndicator", Order = 25)]
        public MISMOIndicator TaxEscrowedIndicator { get; set; }
    
        [XmlIgnore]
        public bool TaxEscrowedIndicatorSpecified
        {
            get { return this.TaxEscrowedIndicator != null; }
            set { }
        }
    
        [XmlElement("TaxItemExemptionIndicator", Order = 26)]
        public MISMOIndicator TaxItemExemptionIndicator { get; set; }
    
        [XmlIgnore]
        public bool TaxItemExemptionIndicatorSpecified
        {
            get { return this.TaxItemExemptionIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 27)]
        public PROPERTY_TAX_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
