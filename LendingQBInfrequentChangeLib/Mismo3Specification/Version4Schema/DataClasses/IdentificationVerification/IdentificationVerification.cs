namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class IDENTIFICATION_VERIFICATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BirthInformationSpecified
                    || this.IdentificationVerificationDetailSpecified
                    || this.IdentityDocumentationsSpecified
                    || this.SharedSecretsSpecified
                    || this.SpouseInformationSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BIRTH_INFORMATION", Order = 0)]
        public BIRTH_INFORMATION BirthInformation { get; set; }
    
        [XmlIgnore]
        public bool BirthInformationSpecified
        {
            get { return this.BirthInformation != null && this.BirthInformation.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("IDENTIFICATION_VERIFICATION_DETAIL", Order = 1)]
        public IDENTIFICATION_VERIFICATION_DETAIL IdentificationVerificationDetail { get; set; }
    
        [XmlIgnore]
        public bool IdentificationVerificationDetailSpecified
        {
            get { return this.IdentificationVerificationDetail != null && this.IdentificationVerificationDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("IDENTITY_DOCUMENTATIONS", Order = 2)]
        public IDENTITY_DOCUMENTATIONS IdentityDocumentations { get; set; }
    
        [XmlIgnore]
        public bool IdentityDocumentationsSpecified
        {
            get { return this.IdentityDocumentations != null && this.IdentityDocumentations.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SHARED_SECRETS", Order = 3)]
        public SHARED_SECRETS SharedSecrets { get; set; }
    
        [XmlIgnore]
        public bool SharedSecretsSpecified
        {
            get { return this.SharedSecrets != null && this.SharedSecrets.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SPOUSE_INFORMATION", Order = 4)]
        public SPOUSE_INFORMATION SpouseInformation { get; set; }
    
        [XmlIgnore]
        public bool SpouseInformationSpecified
        {
            get { return this.SpouseInformation != null && this.SpouseInformation.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public IDENTIFICATION_VERIFICATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
