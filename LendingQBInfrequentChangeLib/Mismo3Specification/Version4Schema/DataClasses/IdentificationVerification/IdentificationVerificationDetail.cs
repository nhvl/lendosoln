namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class IDENTIFICATION_VERIFICATION_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CountryOfCitizenshipNameSpecified
                    || this.SSNAssociationsCountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CountryOfCitizenshipName", Order = 0)]
        public MISMOString CountryOfCitizenshipName { get; set; }
    
        [XmlIgnore]
        public bool CountryOfCitizenshipNameSpecified
        {
            get { return this.CountryOfCitizenshipName != null; }
            set { }
        }
    
        [XmlElement("SSNAssociationsCount", Order = 1)]
        public MISMOCount SSNAssociationsCount { get; set; }
    
        [XmlIgnore]
        public bool SSNAssociationsCountSpecified
        {
            get { return this.SSNAssociationsCount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public IDENTIFICATION_VERIFICATION_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
