namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ATTORNEY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AttorneyFirmNameSpecified
                    || this.AttorneyFunctionTypeSpecified
                    || this.AttorneyFunctionTypeOtherDescriptionSpecified
                    || this.RepresentedPartyTypeSpecified
                    || this.RepresentedPartyTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AttorneyFirmName", Order = 0)]
        public MISMOString AttorneyFirmName { get; set; }
    
        [XmlIgnore]
        public bool AttorneyFirmNameSpecified
        {
            get { return this.AttorneyFirmName != null; }
            set { }
        }
    
        [XmlElement("AttorneyFunctionType", Order = 1)]
        public MISMOEnum<AttorneyFunctionBase> AttorneyFunctionType { get; set; }
    
        [XmlIgnore]
        public bool AttorneyFunctionTypeSpecified
        {
            get { return this.AttorneyFunctionType != null; }
            set { }
        }
    
        [XmlElement("AttorneyFunctionTypeOtherDescription", Order = 2)]
        public MISMOString AttorneyFunctionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool AttorneyFunctionTypeOtherDescriptionSpecified
        {
            get { return this.AttorneyFunctionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("RepresentedPartyType", Order = 3)]
        public MISMOEnum<RepresentedPartyBase> RepresentedPartyType { get; set; }
    
        [XmlIgnore]
        public bool RepresentedPartyTypeSpecified
        {
            get { return this.RepresentedPartyType != null; }
            set { }
        }
    
        [XmlElement("RepresentedPartyTypeOtherDescription", Order = 4)]
        public MISMOString RepresentedPartyTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool RepresentedPartyTypeOtherDescriptionSpecified
        {
            get { return this.RepresentedPartyTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public ATTORNEY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
