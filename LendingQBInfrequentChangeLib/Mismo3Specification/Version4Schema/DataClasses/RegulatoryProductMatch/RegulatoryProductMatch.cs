namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class REGULATORY_PRODUCT_MATCH
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.RegulatoryProductMatchTypeSpecified
                    || this.RegulatoryProductMatchTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("RegulatoryProductMatchType", Order = 0)]
        public MISMOEnum<RegulatoryProductMatchBase> RegulatoryProductMatchType { get; set; }
    
        [XmlIgnore]
        public bool RegulatoryProductMatchTypeSpecified
        {
            get { return this.RegulatoryProductMatchType != null; }
            set { }
        }
    
        [XmlElement("RegulatoryProductMatchTypeOtherDescription", Order = 1)]
        public MISMOString RegulatoryProductMatchTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool RegulatoryProductMatchTypeOtherDescriptionSpecified
        {
            get { return this.RegulatoryProductMatchTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public REGULATORY_PRODUCT_MATCH_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
