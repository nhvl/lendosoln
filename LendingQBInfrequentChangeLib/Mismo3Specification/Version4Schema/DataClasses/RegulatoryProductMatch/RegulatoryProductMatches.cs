namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class REGULATORY_PRODUCT_MATCHES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.RegulatoryProductMatchListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("REGULATORY_PRODUCT_MATCH", Order = 0)]
        public List<REGULATORY_PRODUCT_MATCH> RegulatoryProductMatchList { get; set; } = new List<REGULATORY_PRODUCT_MATCH>();
    
        [XmlIgnore]
        public bool RegulatoryProductMatchListSpecified
        {
            get { return this.RegulatoryProductMatchList != null && this.RegulatoryProductMatchList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public REGULATORY_PRODUCT_MATCHES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
