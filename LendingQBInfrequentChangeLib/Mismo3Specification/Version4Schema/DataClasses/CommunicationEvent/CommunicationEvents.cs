namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class COMMUNICATION_EVENTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CommunicationEventListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("COMMUNICATION_EVENT", Order = 0)]
        public List<COMMUNICATION_EVENT> CommunicationEventList { get; set; } = new List<COMMUNICATION_EVENT>();
    
        [XmlIgnore]
        public bool CommunicationEventListSpecified
        {
            get { return this.CommunicationEventList != null && this.CommunicationEventList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public COMMUNICATION_EVENTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
