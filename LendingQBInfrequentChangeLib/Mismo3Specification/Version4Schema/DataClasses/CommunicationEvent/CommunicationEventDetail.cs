namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class COMMUNICATION_EVENT_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EventIdentifierSpecified
                    || this.EventSummaryTextSpecified
                    || this.OnlinePortalAssignedIdentifierSpecified
                    || this.OnlinePortalHostTypeSpecified
                    || this.OnlinePortalHostTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("EventIdentifier", Order = 0)]
        public MISMOIdentifier EventIdentifier { get; set; }
    
        [XmlIgnore]
        public bool EventIdentifierSpecified
        {
            get { return this.EventIdentifier != null; }
            set { }
        }
    
        [XmlElement("EventSummaryText", Order = 1)]
        public MISMOString EventSummaryText { get; set; }
    
        [XmlIgnore]
        public bool EventSummaryTextSpecified
        {
            get { return this.EventSummaryText != null; }
            set { }
        }
    
        [XmlElement("OnlinePortalAssignedIdentifier", Order = 2)]
        public MISMOIdentifier OnlinePortalAssignedIdentifier { get; set; }
    
        [XmlIgnore]
        public bool OnlinePortalAssignedIdentifierSpecified
        {
            get { return this.OnlinePortalAssignedIdentifier != null; }
            set { }
        }
    
        [XmlElement("OnlinePortalHostType", Order = 3)]
        public MISMOEnum<OnlinePortalHostBase> OnlinePortalHostType { get; set; }
    
        [XmlIgnore]
        public bool OnlinePortalHostTypeSpecified
        {
            get { return this.OnlinePortalHostType != null; }
            set { }
        }
    
        [XmlElement("OnlinePortalHostTypeOtherDescription", Order = 4)]
        public MISMOString OnlinePortalHostTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool OnlinePortalHostTypeOtherDescriptionSpecified
        {
            get { return this.OnlinePortalHostTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public COMMUNICATION_EVENT_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
