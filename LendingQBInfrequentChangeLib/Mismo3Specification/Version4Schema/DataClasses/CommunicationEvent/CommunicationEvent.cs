namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class COMMUNICATION_EVENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CommunicationEventDetailSpecified
                    || this.CommunicationEventIssuesSpecified
                    || this.CommunicationEventParticipantsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("COMMUNICATION_EVENT_DETAIL", Order = 0)]
        public COMMUNICATION_EVENT_DETAIL CommunicationEventDetail { get; set; }
    
        [XmlIgnore]
        public bool CommunicationEventDetailSpecified
        {
            get { return this.CommunicationEventDetail != null && this.CommunicationEventDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("COMMUNICATION_EVENT_ISSUES", Order = 1)]
        public COMMUNICATION_EVENT_ISSUES CommunicationEventIssues { get; set; }
    
        [XmlIgnore]
        public bool CommunicationEventIssuesSpecified
        {
            get { return this.CommunicationEventIssues != null && this.CommunicationEventIssues.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("COMMUNICATION_EVENT_PARTICIPANTS", Order = 2)]
        public COMMUNICATION_EVENT_PARTICIPANTS CommunicationEventParticipants { get; set; }
    
        [XmlIgnore]
        public bool CommunicationEventParticipantsSpecified
        {
            get { return this.CommunicationEventParticipants != null && this.CommunicationEventParticipants.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public COMMUNICATION_EVENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
