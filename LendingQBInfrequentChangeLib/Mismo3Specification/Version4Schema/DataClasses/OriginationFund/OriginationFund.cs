namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ORIGINATION_FUND
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.FundsSourceTypeSpecified
                    || this.FundsSourceTypeOtherDescriptionSpecified
                    || this.FundsTypeSpecified
                    || this.FundsTypeOtherDescriptionSpecified
                    || this.OriginationFundsAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("FundsSourceType", Order = 0)]
        public MISMOEnum<FundsSourceBase> FundsSourceType { get; set; }
    
        [XmlIgnore]
        public bool FundsSourceTypeSpecified
        {
            get { return this.FundsSourceType != null; }
            set { }
        }
    
        [XmlElement("FundsSourceTypeOtherDescription", Order = 1)]
        public MISMOString FundsSourceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool FundsSourceTypeOtherDescriptionSpecified
        {
            get { return this.FundsSourceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("FundsType", Order = 2)]
        public MISMOEnum<FundsBase> FundsType { get; set; }
    
        [XmlIgnore]
        public bool FundsTypeSpecified
        {
            get { return this.FundsType != null; }
            set { }
        }
    
        [XmlElement("FundsTypeOtherDescription", Order = 3)]
        public MISMOString FundsTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool FundsTypeOtherDescriptionSpecified
        {
            get { return this.FundsTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("OriginationFundsAmount", Order = 4)]
        public MISMOAmount OriginationFundsAmount { get; set; }
    
        [XmlIgnore]
        public bool OriginationFundsAmountSpecified
        {
            get { return this.OriginationFundsAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public ORIGINATION_FUND_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
