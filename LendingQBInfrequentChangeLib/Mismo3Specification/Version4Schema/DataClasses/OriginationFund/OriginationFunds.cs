namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class ORIGINATION_FUNDS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.OriginationFundListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ORIGINATION_FUND", Order = 0)]
        public List<ORIGINATION_FUND> OriginationFundList { get; set; } = new List<ORIGINATION_FUND>();
    
        [XmlIgnore]
        public bool OriginationFundListSpecified
        {
            get { return this.OriginationFundList != null && this.OriginationFundList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public ORIGINATION_FUNDS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
