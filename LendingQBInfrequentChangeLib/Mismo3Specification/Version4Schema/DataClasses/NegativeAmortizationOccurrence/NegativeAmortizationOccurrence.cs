namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class NEGATIVE_AMORTIZATION_OCCURRENCE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CurrentMonthlyNegativeAmortizationAmountSpecified
                    || this.CurrentNegativeAmortizationBalanceAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CurrentMonthlyNegativeAmortizationAmount", Order = 0)]
        public MISMOAmount CurrentMonthlyNegativeAmortizationAmount { get; set; }
    
        [XmlIgnore]
        public bool CurrentMonthlyNegativeAmortizationAmountSpecified
        {
            get { return this.CurrentMonthlyNegativeAmortizationAmount != null; }
            set { }
        }
    
        [XmlElement("CurrentNegativeAmortizationBalanceAmount", Order = 1)]
        public MISMOAmount CurrentNegativeAmortizationBalanceAmount { get; set; }
    
        [XmlIgnore]
        public bool CurrentNegativeAmortizationBalanceAmountSpecified
        {
            get { return this.CurrentNegativeAmortizationBalanceAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public NEGATIVE_AMORTIZATION_OCCURRENCE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
