namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class NEGATIVE_AMORTIZATION_OCCURRENCES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.NegativeAmortizationOccurrenceListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("NEGATIVE_AMORTIZATION_OCCURRENCE", Order = 0)]
        public List<NEGATIVE_AMORTIZATION_OCCURRENCE> NegativeAmortizationOccurrenceList { get; set; } = new List<NEGATIVE_AMORTIZATION_OCCURRENCE>();
    
        [XmlIgnore]
        public bool NegativeAmortizationOccurrenceListSpecified
        {
            get { return this.NegativeAmortizationOccurrenceList != null && this.NegativeAmortizationOccurrenceList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public NEGATIVE_AMORTIZATION_OCCURRENCES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
