namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class DEAL_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MortgageOriginatorFirstTimeHomeBuyerEligibilityIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MortgageOriginatorFirstTimeHomeBuyerEligibilityIndicator", Order = 0)]
        public MISMOIndicator MortgageOriginatorFirstTimeHomeBuyerEligibilityIndicator { get; set; }
    
        [XmlIgnore]
        public bool MortgageOriginatorFirstTimeHomeBuyerEligibilityIndicatorSpecified
        {
            get { return this.MortgageOriginatorFirstTimeHomeBuyerEligibilityIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public DEAL_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
