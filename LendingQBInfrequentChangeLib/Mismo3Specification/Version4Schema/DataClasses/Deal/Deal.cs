namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    public class DEAL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                if (Mismo3Utilities.ExceedsThreshold(1,
                    new List<bool> {
                        this.ReferenceSpecified,
                        this.AboutVersionsSpecified
                        || this.AssetsSpecified
                        || this.CollateralsSpecified
                        || this.CommunicationEventsSpecified
                        || this.DealDetailSpecified
                        || this.ExpensesSpecified
                        || this.ExtensionSpecified
                        || this.LiabilitiesSpecified
                        || this.LitigationsSpecified
                        || this.LoansSpecified
                        || this.PartiesSpecified
                        || this.RelationshipsSpecified
                        || this.ServicesSpecified
                        || this.SupportingRecordSetsSpecified }))
                {
                    throw new System.Xml.Schema.XmlSchemaValidationException(
                        Mismo3Utilities.GetXSDChoiceExceptionMessage(
                        "DEAL",
                        new List<string> { "REFERENCE", "Any other child element of the DEAL" }));
                }

                return this.AboutVersionsSpecified
                    || this.AssetsSpecified
                    || this.CollateralsSpecified
                    || this.CommunicationEventsSpecified
                    || this.DealDetailSpecified
                    || this.ExpensesSpecified
                    || this.ExtensionSpecified
                    || this.LiabilitiesSpecified
                    || this.LitigationsSpecified
                    || this.LoansSpecified
                    || this.PartiesSpecified
                    || this.ReferenceSpecified
                    || this.RelationshipsSpecified
                    || this.ServicesSpecified
                    || this.SupportingRecordSetsSpecified
                    || this.MISMOLogicalDataDictionaryIdentifierSpecified
                    || this.MISMOReferenceModelIdentifierSpecified;
            }
        }

        [XmlElement("REFERENCE", Order = 0)]
        public REFERENCE Reference { get; set; }

        [XmlIgnore]
        public bool ReferenceSpecified
        {
            get { return this.Reference != null && this.Reference.ShouldSerialize; }
            set { }
        }

        [XmlElement("ABOUT_VERSIONS", Order = 1)]
        public ABOUT_VERSIONS AboutVersions { get; set; }
    
        [XmlIgnore]
        public bool AboutVersionsSpecified
        {
            get { return this.AboutVersions != null && this.AboutVersions.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("ASSETS", Order = 2)]
        public ASSETS Assets { get; set; }
    
        [XmlIgnore]
        public bool AssetsSpecified
        {
            get { return this.Assets != null && this.Assets.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("COLLATERALS", Order = 3)]
        public COLLATERALS Collaterals { get; set; }
    
        [XmlIgnore]
        public bool CollateralsSpecified
        {
            get { return this.Collaterals != null && this.Collaterals.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("COMMUNICATION_EVENTS", Order = 4)]
        public COMMUNICATION_EVENTS CommunicationEvents { get; set; }
    
        [XmlIgnore]
        public bool CommunicationEventsSpecified
        {
            get { return this.CommunicationEvents != null && this.CommunicationEvents.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("DEAL_DETAIL", Order = 5)]
        public DEAL_DETAIL DealDetail { get; set; }
    
        [XmlIgnore]
        public bool DealDetailSpecified
        {
            get { return this.DealDetail != null && this.DealDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXPENSES", Order = 6)]
        public EXPENSES Expenses { get; set; }
    
        [XmlIgnore]
        public bool ExpensesSpecified
        {
            get { return this.Expenses != null && this.Expenses.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("LIABILITIES", Order = 7)]
        public LIABILITIES Liabilities { get; set; }
    
        [XmlIgnore]
        public bool LiabilitiesSpecified
        {
            get { return this.Liabilities != null && this.Liabilities.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("LITIGATIONS", Order = 8)]
        public LITIGATIONS Litigations { get; set; }
    
        [XmlIgnore]
        public bool LitigationsSpecified
        {
            get { return this.Litigations != null && this.Litigations.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("LOANS", Order = 9)]
        public LOANS Loans { get; set; }
    
        [XmlIgnore]
        public bool LoansSpecified
        {
            get { return this.Loans != null && this.Loans.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PARTIES", Order = 10)]
        public PARTIES Parties { get; set; }
    
        [XmlIgnore]
        public bool PartiesSpecified
        {
            get { return this.Parties != null && this.Parties.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("RELATIONSHIPS", Order = 11)]
        public RELATIONSHIPS Relationships { get; set; }
    
        [XmlIgnore]
        public bool RelationshipsSpecified
        {
            get { return this.Relationships != null && this.Relationships.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SERVICES", Order = 12)]
        public SERVICES Services { get; set; }
    
        [XmlIgnore]
        public bool ServicesSpecified
        {
            get { return this.Services != null && this.Services.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SUPPORTING_RECORD_SETS", Order = 13)]
        public SUPPORTING_RECORD_SETS SupportingRecordSets { get; set; }
    
        [XmlIgnore]
        public bool SupportingRecordSetsSpecified
        {
            get { return this.SupportingRecordSets != null && this.SupportingRecordSets.ShouldSerialize; }
            set { }
        }

        [XmlElement("EXTENSION", Order = 14)]
        public DEAL_EXTENSION Extension { get; set; }

        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }

        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    
        [XmlAttribute(AttributeName = "MISMOLogicalDataDictionaryIdentifier")]
        public string MISMOLogicalDataDictionaryIdentifier { get; set; }
    
        [XmlIgnore]
        public bool MISMOLogicalDataDictionaryIdentifierSpecified
        {
            get { return !string.IsNullOrEmpty(this.MISMOLogicalDataDictionaryIdentifier); }
            set { }
        }
    
        [XmlAttribute(AttributeName = "MISMOReferenceModelIdentifier")]
        public string MISMOReferenceModelIdentifier { get; set; }
    
        [XmlIgnore]
        public bool MISMOReferenceModelIdentifierSpecified
        {
            get { return !string.IsNullOrEmpty(this.MISMOReferenceModelIdentifier); }
            set { }
        }
    }
}
