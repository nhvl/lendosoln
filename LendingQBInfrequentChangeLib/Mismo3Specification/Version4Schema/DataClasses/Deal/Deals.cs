namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class DEALS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DealListSpecified
                    || this.PartiesSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DEAL", Order = 0)]
        public List<DEAL> DealList { get; set; } = new List<DEAL>();
    
        [XmlIgnore]
        public bool DealListSpecified
        {
            get { return this.DealList != null && this.DealList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("PARTIES", Order = 1)]
        public PARTIES Parties { get; set; }
    
        [XmlIgnore]
        public bool PartiesSpecified
        {
            get { return this.Parties != null && this.Parties.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public DEALS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
