namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class NEW_IMPROVEMENTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.NewImprovementListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("NEW_IMPROVEMENT", Order = 0)]
        public List<NEW_IMPROVEMENT> NewImprovementList { get; set; } = new List<NEW_IMPROVEMENT>();
    
        [XmlIgnore]
        public bool NewImprovementListSpecified
        {
            get { return this.NewImprovementList != null && this.NewImprovementList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public NEW_IMPROVEMENTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
