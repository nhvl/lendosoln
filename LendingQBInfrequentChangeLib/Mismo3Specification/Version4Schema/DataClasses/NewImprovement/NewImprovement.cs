namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class NEW_IMPROVEMENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.NewImprovementCostAmountSpecified
                    || this.NewImprovementCostDescriptionSpecified
                    || this.NewImprovementTypeSpecified
                    || this.NewImprovementTypeOtherDescriptionSpecified
                    || this.PricePerSquareFootAmountSpecified
                    || this.SquareFeetNumberSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("NewImprovementCostAmount", Order = 0)]
        public MISMOAmount NewImprovementCostAmount { get; set; }
    
        [XmlIgnore]
        public bool NewImprovementCostAmountSpecified
        {
            get { return this.NewImprovementCostAmount != null; }
            set { }
        }
    
        [XmlElement("NewImprovementCostDescription", Order = 1)]
        public MISMOString NewImprovementCostDescription { get; set; }
    
        [XmlIgnore]
        public bool NewImprovementCostDescriptionSpecified
        {
            get { return this.NewImprovementCostDescription != null; }
            set { }
        }
    
        [XmlElement("NewImprovementType", Order = 2)]
        public MISMOEnum<NewImprovementBase> NewImprovementType { get; set; }
    
        [XmlIgnore]
        public bool NewImprovementTypeSpecified
        {
            get { return this.NewImprovementType != null; }
            set { }
        }
    
        [XmlElement("NewImprovementTypeOtherDescription", Order = 3)]
        public MISMOString NewImprovementTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool NewImprovementTypeOtherDescriptionSpecified
        {
            get { return this.NewImprovementTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PricePerSquareFootAmount", Order = 4)]
        public MISMOAmount PricePerSquareFootAmount { get; set; }
    
        [XmlIgnore]
        public bool PricePerSquareFootAmountSpecified
        {
            get { return this.PricePerSquareFootAmount != null; }
            set { }
        }
    
        [XmlElement("SquareFeetNumber", Order = 5)]
        public MISMONumeric SquareFeetNumber { get; set; }
    
        [XmlIgnore]
        public bool SquareFeetNumberSpecified
        {
            get { return this.SquareFeetNumber != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 6)]
        public NEW_IMPROVEMENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
