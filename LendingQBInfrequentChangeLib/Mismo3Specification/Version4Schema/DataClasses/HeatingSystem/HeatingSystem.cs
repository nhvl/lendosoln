namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class HEATING_SYSTEM
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ComponentAdjustmentAmountSpecified
                    || this.ComponentClassificationTypeSpecified
                    || this.ConditionRatingDescriptionSpecified
                    || this.ConditionRatingTypeSpecified
                    || this.HeatingFuelTypeSpecified
                    || this.HeatingFuelTypeOtherDescriptionSpecified
                    || this.HeatingSystemDescriptionSpecified
                    || this.HeatingSystemPrimaryIndicatorSpecified
                    || this.HeatingSystemTypeSpecified
                    || this.HeatingSystemTypeOtherDescriptionSpecified
                    || this.QualityRatingDescriptionSpecified
                    || this.QualityRatingTypeSpecified
                    || this.SystemPermanentIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ComponentAdjustmentAmount", Order = 0)]
        public MISMOAmount ComponentAdjustmentAmount { get; set; }
    
        [XmlIgnore]
        public bool ComponentAdjustmentAmountSpecified
        {
            get { return this.ComponentAdjustmentAmount != null; }
            set { }
        }
    
        [XmlElement("ComponentClassificationType", Order = 1)]
        public MISMOEnum<ComponentClassificationBase> ComponentClassificationType { get; set; }
    
        [XmlIgnore]
        public bool ComponentClassificationTypeSpecified
        {
            get { return this.ComponentClassificationType != null; }
            set { }
        }
    
        [XmlElement("ConditionRatingDescription", Order = 2)]
        public MISMOString ConditionRatingDescription { get; set; }
    
        [XmlIgnore]
        public bool ConditionRatingDescriptionSpecified
        {
            get { return this.ConditionRatingDescription != null; }
            set { }
        }
    
        [XmlElement("ConditionRatingType", Order = 3)]
        public MISMOEnum<ConditionRatingBase> ConditionRatingType { get; set; }
    
        [XmlIgnore]
        public bool ConditionRatingTypeSpecified
        {
            get { return this.ConditionRatingType != null; }
            set { }
        }
    
        [XmlElement("HeatingFuelType", Order = 4)]
        public MISMOEnum<HeatingFuelBase> HeatingFuelType { get; set; }
    
        [XmlIgnore]
        public bool HeatingFuelTypeSpecified
        {
            get { return this.HeatingFuelType != null; }
            set { }
        }
    
        [XmlElement("HeatingFuelTypeOtherDescription", Order = 5)]
        public MISMOString HeatingFuelTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool HeatingFuelTypeOtherDescriptionSpecified
        {
            get { return this.HeatingFuelTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("HeatingSystemDescription", Order = 6)]
        public MISMOString HeatingSystemDescription { get; set; }
    
        [XmlIgnore]
        public bool HeatingSystemDescriptionSpecified
        {
            get { return this.HeatingSystemDescription != null; }
            set { }
        }
    
        [XmlElement("HeatingSystemPrimaryIndicator", Order = 7)]
        public MISMOIndicator HeatingSystemPrimaryIndicator { get; set; }
    
        [XmlIgnore]
        public bool HeatingSystemPrimaryIndicatorSpecified
        {
            get { return this.HeatingSystemPrimaryIndicator != null; }
            set { }
        }
    
        [XmlElement("HeatingSystemType", Order = 8)]
        public MISMOEnum<HeatingSystemBase> HeatingSystemType { get; set; }
    
        [XmlIgnore]
        public bool HeatingSystemTypeSpecified
        {
            get { return this.HeatingSystemType != null; }
            set { }
        }
    
        [XmlElement("HeatingSystemTypeOtherDescription", Order = 9)]
        public MISMOString HeatingSystemTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool HeatingSystemTypeOtherDescriptionSpecified
        {
            get { return this.HeatingSystemTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("QualityRatingDescription", Order = 10)]
        public MISMOString QualityRatingDescription { get; set; }
    
        [XmlIgnore]
        public bool QualityRatingDescriptionSpecified
        {
            get { return this.QualityRatingDescription != null; }
            set { }
        }
    
        [XmlElement("QualityRatingType", Order = 11)]
        public MISMOEnum<QualityRatingBase> QualityRatingType { get; set; }
    
        [XmlIgnore]
        public bool QualityRatingTypeSpecified
        {
            get { return this.QualityRatingType != null; }
            set { }
        }
    
        [XmlElement("SystemPermanentIndicator", Order = 12)]
        public MISMOIndicator SystemPermanentIndicator { get; set; }
    
        [XmlIgnore]
        public bool SystemPermanentIndicatorSpecified
        {
            get { return this.SystemPermanentIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 13)]
        public HEATING_SYSTEM_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
