namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class HEATING_SYSTEMS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.HeatingSystemListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("HEATING_SYSTEM", Order = 0)]
        public List<HEATING_SYSTEM> HeatingSystemList { get; set; } = new List<HEATING_SYSTEM>();
    
        [XmlIgnore]
        public bool HeatingSystemListSpecified
        {
            get { return this.HeatingSystemList != null && this.HeatingSystemList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public HEATING_SYSTEMS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
