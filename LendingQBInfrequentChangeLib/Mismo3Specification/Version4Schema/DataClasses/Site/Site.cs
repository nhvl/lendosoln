namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SITE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.OffSiteImprovementsSpecified
                    || this.SiteDetailSpecified
                    || this.SiteFeaturesSpecified
                    || this.SiteInfluencesSpecified
                    || this.SiteLocationsSpecified
                    || this.SiteSizeLayoutSpecified
                    || this.SiteUtilitiesSpecified
                    || this.SiteViewsSpecified
                    || this.SiteZoningSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("OFF_SITE_IMPROVEMENTS", Order = 0)]
        public OFF_SITE_IMPROVEMENTS OffSiteImprovements { get; set; }
    
        [XmlIgnore]
        public bool OffSiteImprovementsSpecified
        {
            get { return this.OffSiteImprovements != null && this.OffSiteImprovements.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SITE_DETAIL", Order = 1)]
        public SITE_DETAIL SiteDetail { get; set; }
    
        [XmlIgnore]
        public bool SiteDetailSpecified
        {
            get { return this.SiteDetail != null && this.SiteDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SITE_FEATURES", Order = 2)]
        public SITE_FEATURES SiteFeatures { get; set; }
    
        [XmlIgnore]
        public bool SiteFeaturesSpecified
        {
            get { return this.SiteFeatures != null && this.SiteFeatures.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SITE_INFLUENCES", Order = 3)]
        public SITE_INFLUENCES SiteInfluences { get; set; }
    
        [XmlIgnore]
        public bool SiteInfluencesSpecified
        {
            get { return this.SiteInfluences != null && this.SiteInfluences.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SITE_LOCATIONS", Order = 4)]
        public SITE_LOCATIONS SiteLocations { get; set; }
    
        [XmlIgnore]
        public bool SiteLocationsSpecified
        {
            get { return this.SiteLocations != null && this.SiteLocations.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SITE_SIZE_LAYOUT", Order = 5)]
        public SITE_SIZE_LAYOUT SiteSizeLayout { get; set; }
    
        [XmlIgnore]
        public bool SiteSizeLayoutSpecified
        {
            get { return this.SiteSizeLayout != null && this.SiteSizeLayout.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SITE_UTILITIES", Order = 6)]
        public SITE_UTILITIES SiteUtilities { get; set; }
    
        [XmlIgnore]
        public bool SiteUtilitiesSpecified
        {
            get { return this.SiteUtilities != null && this.SiteUtilities.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SITE_VIEWS", Order = 7)]
        public SITE_VIEWS SiteViews { get; set; }
    
        [XmlIgnore]
        public bool SiteViewsSpecified
        {
            get { return this.SiteViews != null && this.SiteViews.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SITE_ZONING", Order = 8)]
        public SITE_ZONING SiteZoning { get; set; }
    
        [XmlIgnore]
        public bool SiteZoningSpecified
        {
            get { return this.SiteZoning != null && this.SiteZoning.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 9)]
        public SITE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
