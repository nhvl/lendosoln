namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SITE_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CharacteristicsAffectMarketabilityDescriptionSpecified
                    || this.ConditionRatingTypeSpecified
                    || this.CornerLotIndicatorSpecified
                    || this.SiteAdverseConditionsIndicatorSpecified
                    || this.SiteHighestBestUseDescriptionSpecified
                    || this.SiteHighestBestUseIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CharacteristicsAffectMarketabilityDescription", Order = 0)]
        public MISMOString CharacteristicsAffectMarketabilityDescription { get; set; }
    
        [XmlIgnore]
        public bool CharacteristicsAffectMarketabilityDescriptionSpecified
        {
            get { return this.CharacteristicsAffectMarketabilityDescription != null; }
            set { }
        }
    
        [XmlElement("ConditionRatingType", Order = 1)]
        public MISMOEnum<ConditionRatingBase> ConditionRatingType { get; set; }
    
        [XmlIgnore]
        public bool ConditionRatingTypeSpecified
        {
            get { return this.ConditionRatingType != null; }
            set { }
        }
    
        [XmlElement("CornerLotIndicator", Order = 2)]
        public MISMOIndicator CornerLotIndicator { get; set; }
    
        [XmlIgnore]
        public bool CornerLotIndicatorSpecified
        {
            get { return this.CornerLotIndicator != null; }
            set { }
        }
    
        [XmlElement("SiteAdverseConditionsIndicator", Order = 3)]
        public MISMOIndicator SiteAdverseConditionsIndicator { get; set; }
    
        [XmlIgnore]
        public bool SiteAdverseConditionsIndicatorSpecified
        {
            get { return this.SiteAdverseConditionsIndicator != null; }
            set { }
        }
    
        [XmlElement("SiteHighestBestUseDescription", Order = 4)]
        public MISMOString SiteHighestBestUseDescription { get; set; }
    
        [XmlIgnore]
        public bool SiteHighestBestUseDescriptionSpecified
        {
            get { return this.SiteHighestBestUseDescription != null; }
            set { }
        }
    
        [XmlElement("SiteHighestBestUseIndicator", Order = 5)]
        public MISMOIndicator SiteHighestBestUseIndicator { get; set; }
    
        [XmlIgnore]
        public bool SiteHighestBestUseIndicatorSpecified
        {
            get { return this.SiteHighestBestUseIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 6)]
        public SITE_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
