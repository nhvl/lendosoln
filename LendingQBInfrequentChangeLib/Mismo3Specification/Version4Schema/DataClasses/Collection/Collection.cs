namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class COLLECTION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BorrowerCollectionResponseIndicatorSpecified
                    || this.CollectionContactDateSpecified
                    || this.CollectionContactMethodIdentifierSpecified
                    || this.CollectionContactMethodTypeSpecified
                    || this.CollectionContactMethodTypeOtherDescriptionSpecified
                    || this.CollectionContactProductiveIndicatorSpecified
                    || this.PromiseToPayIndicatorSpecified
                    || this.RightPartyContactIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BorrowerCollectionResponseIndicator", Order = 0)]
        public MISMOIndicator BorrowerCollectionResponseIndicator { get; set; }
    
        [XmlIgnore]
        public bool BorrowerCollectionResponseIndicatorSpecified
        {
            get { return this.BorrowerCollectionResponseIndicator != null; }
            set { }
        }
    
        [XmlElement("CollectionContactDate", Order = 1)]
        public MISMODate CollectionContactDate { get; set; }
    
        [XmlIgnore]
        public bool CollectionContactDateSpecified
        {
            get { return this.CollectionContactDate != null; }
            set { }
        }
    
        [XmlElement("CollectionContactMethodIdentifier", Order = 2)]
        public MISMOIdentifier CollectionContactMethodIdentifier { get; set; }
    
        [XmlIgnore]
        public bool CollectionContactMethodIdentifierSpecified
        {
            get { return this.CollectionContactMethodIdentifier != null; }
            set { }
        }
    
        [XmlElement("CollectionContactMethodType", Order = 3)]
        public MISMOEnum<CollectionContactMethodBase> CollectionContactMethodType { get; set; }
    
        [XmlIgnore]
        public bool CollectionContactMethodTypeSpecified
        {
            get { return this.CollectionContactMethodType != null; }
            set { }
        }
    
        [XmlElement("CollectionContactMethodTypeOtherDescription", Order = 4)]
        public MISMOString CollectionContactMethodTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CollectionContactMethodTypeOtherDescriptionSpecified
        {
            get { return this.CollectionContactMethodTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("CollectionContactProductiveIndicator", Order = 5)]
        public MISMOIndicator CollectionContactProductiveIndicator { get; set; }
    
        [XmlIgnore]
        public bool CollectionContactProductiveIndicatorSpecified
        {
            get { return this.CollectionContactProductiveIndicator != null; }
            set { }
        }
    
        [XmlElement("PromiseToPayIndicator", Order = 6)]
        public MISMOIndicator PromiseToPayIndicator { get; set; }
    
        [XmlIgnore]
        public bool PromiseToPayIndicatorSpecified
        {
            get { return this.PromiseToPayIndicator != null; }
            set { }
        }
    
        [XmlElement("RightPartyContactIndicator", Order = 7)]
        public MISMOIndicator RightPartyContactIndicator { get; set; }
    
        [XmlIgnore]
        public bool RightPartyContactIndicatorSpecified
        {
            get { return this.RightPartyContactIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 8)]
        public COLLECTION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
