namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class COLLECTIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CollectionListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("COLLECTION", Order = 0)]
        public List<COLLECTION> CollectionList { get; set; } = new List<COLLECTION>();
    
        [XmlIgnore]
        public bool CollectionListSpecified
        {
            get { return this.CollectionList != null && this.CollectionList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public COLLECTIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
