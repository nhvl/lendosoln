namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SYSTEM_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.HeatingCoolingMeteredSeparatelyDescriptionSpecified
                    || this.HeatingCoolingMeteredSeparatelyIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("HeatingCoolingMeteredSeparatelyDescription", Order = 0)]
        public MISMOString HeatingCoolingMeteredSeparatelyDescription { get; set; }
    
        [XmlIgnore]
        public bool HeatingCoolingMeteredSeparatelyDescriptionSpecified
        {
            get { return this.HeatingCoolingMeteredSeparatelyDescription != null; }
            set { }
        }
    
        [XmlElement("HeatingCoolingMeteredSeparatelyIndicator", Order = 1)]
        public MISMOIndicator HeatingCoolingMeteredSeparatelyIndicator { get; set; }
    
        [XmlIgnore]
        public bool HeatingCoolingMeteredSeparatelyIndicatorSpecified
        {
            get { return this.HeatingCoolingMeteredSeparatelyIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public SYSTEM_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
