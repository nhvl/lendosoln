namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SYSTEM
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CoolingSystemsSpecified
                    || this.HeatingSystemsSpecified
                    || this.SecuritySystemsSpecified
                    || this.SolarPanelArraysSpecified
                    || this.SystemDetailSpecified
                    || this.WaterSystemsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("COOLING_SYSTEMS", Order = 0)]
        public COOLING_SYSTEMS CoolingSystems { get; set; }
    
        [XmlIgnore]
        public bool CoolingSystemsSpecified
        {
            get { return this.CoolingSystems != null && this.CoolingSystems.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("HEATING_SYSTEMS", Order = 1)]
        public HEATING_SYSTEMS HeatingSystems { get; set; }
    
        [XmlIgnore]
        public bool HeatingSystemsSpecified
        {
            get { return this.HeatingSystems != null && this.HeatingSystems.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SECURITY_SYSTEMS", Order = 2)]
        public SECURITY_SYSTEMS SecuritySystems { get; set; }
    
        [XmlIgnore]
        public bool SecuritySystemsSpecified
        {
            get { return this.SecuritySystems != null && this.SecuritySystems.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SOLAR_PANEL_ARRAYS", Order = 3)]
        public SOLAR_PANEL_ARRAYS SolarPanelArrays { get; set; }
    
        [XmlIgnore]
        public bool SolarPanelArraysSpecified
        {
            get { return this.SolarPanelArrays != null && this.SolarPanelArrays.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SYSTEM_DETAIL", Order = 4)]
        public SYSTEM_DETAIL SystemDetail { get; set; }
    
        [XmlIgnore]
        public bool SystemDetailSpecified
        {
            get { return this.SystemDetail != null && this.SystemDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("WATER_SYSTEMS", Order = 5)]
        public WATER_SYSTEMS WaterSystems { get; set; }
    
        [XmlIgnore]
        public bool WaterSystemsSpecified
        {
            get { return this.WaterSystems != null && this.WaterSystems.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 6)]
        public SYSTEM_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
