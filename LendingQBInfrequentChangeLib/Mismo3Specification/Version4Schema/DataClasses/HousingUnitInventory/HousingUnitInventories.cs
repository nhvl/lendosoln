namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class HOUSING_UNIT_INVENTORIES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.HousingUnitInventoryListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("HOUSING_UNIT_INVENTORY", Order = 0)]
        public List<HOUSING_UNIT_INVENTORY> HousingUnitInventoryList { get; set; } = new List<HOUSING_UNIT_INVENTORY>();
    
        [XmlIgnore]
        public bool HousingUnitInventoryListSpecified
        {
            get { return this.HousingUnitInventoryList != null && this.HousingUnitInventoryList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public HOUSING_UNIT_INVENTORIES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
