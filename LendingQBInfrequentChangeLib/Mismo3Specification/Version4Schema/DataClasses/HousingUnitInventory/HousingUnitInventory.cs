namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class HOUSING_UNIT_INVENTORY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ConstructionStatusTypeSpecified
                    || this.ConstructionStatusTypeOtherDescriptionSpecified
                    || this.DevelopmentStageDescriptionSpecified
                    || this.DevelopmentStageTotalPhasesCountSpecified
                    || this.LivingUnitCountSpecified
                    || this.LivingUnitPercentSpecified
                    || this.ProjectConstructionStatusDescriptionSpecified
                    || this.ProjectTypeSpecified
                    || this.ProjectUnitDensityPerAcreNumberSpecified
                    || this.UnitOccupancyTypeSpecified
                    || this.UnitOwnedByTypeSpecified
                    || this.UnitRentRateTypeSpecified
                    || this.UnitSaleRentalStatusTypeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ConstructionStatusType", Order = 0)]
        public MISMOEnum<ConstructionStatusBase> ConstructionStatusType { get; set; }
    
        [XmlIgnore]
        public bool ConstructionStatusTypeSpecified
        {
            get { return this.ConstructionStatusType != null; }
            set { }
        }
    
        [XmlElement("ConstructionStatusTypeOtherDescription", Order = 1)]
        public MISMOString ConstructionStatusTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ConstructionStatusTypeOtherDescriptionSpecified
        {
            get { return this.ConstructionStatusTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("DevelopmentStageDescription", Order = 2)]
        public MISMOString DevelopmentStageDescription { get; set; }
    
        [XmlIgnore]
        public bool DevelopmentStageDescriptionSpecified
        {
            get { return this.DevelopmentStageDescription != null; }
            set { }
        }
    
        [XmlElement("DevelopmentStageTotalPhasesCount", Order = 3)]
        public MISMOCount DevelopmentStageTotalPhasesCount { get; set; }
    
        [XmlIgnore]
        public bool DevelopmentStageTotalPhasesCountSpecified
        {
            get { return this.DevelopmentStageTotalPhasesCount != null; }
            set { }
        }
    
        [XmlElement("LivingUnitCount", Order = 4)]
        public MISMOCount LivingUnitCount { get; set; }
    
        [XmlIgnore]
        public bool LivingUnitCountSpecified
        {
            get { return this.LivingUnitCount != null; }
            set { }
        }
    
        [XmlElement("LivingUnitPercent", Order = 5)]
        public MISMOPercent LivingUnitPercent { get; set; }
    
        [XmlIgnore]
        public bool LivingUnitPercentSpecified
        {
            get { return this.LivingUnitPercent != null; }
            set { }
        }
    
        [XmlElement("ProjectConstructionStatusDescription", Order = 6)]
        public MISMOString ProjectConstructionStatusDescription { get; set; }
    
        [XmlIgnore]
        public bool ProjectConstructionStatusDescriptionSpecified
        {
            get { return this.ProjectConstructionStatusDescription != null; }
            set { }
        }
    
        [XmlElement("ProjectType", Order = 7)]
        public MISMOEnum<ProjectBase> ProjectType { get; set; }
    
        [XmlIgnore]
        public bool ProjectTypeSpecified
        {
            get { return this.ProjectType != null; }
            set { }
        }
    
        [XmlElement("ProjectUnitDensityPerAcreNumber", Order = 8)]
        public MISMONumeric ProjectUnitDensityPerAcreNumber { get; set; }
    
        [XmlIgnore]
        public bool ProjectUnitDensityPerAcreNumberSpecified
        {
            get { return this.ProjectUnitDensityPerAcreNumber != null; }
            set { }
        }
    
        [XmlElement("UnitOccupancyType", Order = 9)]
        public MISMOEnum<UnitOccupancyBase> UnitOccupancyType { get; set; }
    
        [XmlIgnore]
        public bool UnitOccupancyTypeSpecified
        {
            get { return this.UnitOccupancyType != null; }
            set { }
        }
    
        [XmlElement("UnitOwnedByType", Order = 10)]
        public MISMOEnum<UnitOwnedByBase> UnitOwnedByType { get; set; }
    
        [XmlIgnore]
        public bool UnitOwnedByTypeSpecified
        {
            get { return this.UnitOwnedByType != null; }
            set { }
        }
    
        [XmlElement("UnitRentRateType", Order = 11)]
        public MISMOEnum<UnitRentRateBase> UnitRentRateType { get; set; }
    
        [XmlIgnore]
        public bool UnitRentRateTypeSpecified
        {
            get { return this.UnitRentRateType != null; }
            set { }
        }
    
        [XmlElement("UnitSaleRentalStatusType", Order = 12)]
        public MISMOEnum<UnitSaleRentalStatusBase> UnitSaleRentalStatusType { get; set; }
    
        [XmlIgnore]
        public bool UnitSaleRentalStatusTypeSpecified
        {
            get { return this.UnitSaleRentalStatusType != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 13)]
        public HOUSING_UNIT_INVENTORY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
