namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class HMDA_ETHNICITY_ORIGINS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.HmdaEthnicityOriginListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("HMDA_ETHNICITY_ORIGIN", Order = 0)]
        public List<HMDA_ETHNICITY_ORIGIN> HmdaEthnicityOriginList { get; set; } = new List<HMDA_ETHNICITY_ORIGIN>();
    
        [XmlIgnore]
        public bool HmdaEthnicityOriginListSpecified
        {
            get { return this.HmdaEthnicityOriginList != null && this.HmdaEthnicityOriginList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public HMDA_ETHNICITY_ORIGINS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
