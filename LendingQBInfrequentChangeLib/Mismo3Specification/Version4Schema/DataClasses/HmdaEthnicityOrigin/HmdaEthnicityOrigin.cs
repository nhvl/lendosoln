namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class HMDA_ETHNICITY_ORIGIN
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.HMDAEthnicityOriginTypeSpecified
                    || this.HMDAEthnicityOriginTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("HMDAEthnicityOriginType", Order = 0)]
        public MISMOEnum<HMDAEthnicityOriginBase> HMDAEthnicityOriginType { get; set; }
    
        [XmlIgnore]
        public bool HMDAEthnicityOriginTypeSpecified
        {
            get { return this.HMDAEthnicityOriginType != null; }
            set { }
        }
    
        [XmlElement("HMDAEthnicityOriginTypeOtherDescription", Order = 1)]
        public MISMOString HMDAEthnicityOriginTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool HMDAEthnicityOriginTypeOtherDescriptionSpecified
        {
            get { return this.HMDAEthnicityOriginTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public HMDA_ETHNICITY_ORIGIN_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
