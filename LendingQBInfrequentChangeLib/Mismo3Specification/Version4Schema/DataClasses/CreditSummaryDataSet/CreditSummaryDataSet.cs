namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CREDIT_SUMMARY_DATA_SET
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditSummaryDataSetNameSpecified
                    || this.CreditSummaryDataSetValueSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CreditSummaryDataSetName", Order = 0)]
        public MISMOString CreditSummaryDataSetName { get; set; }
    
        [XmlIgnore]
        public bool CreditSummaryDataSetNameSpecified
        {
            get { return this.CreditSummaryDataSetName != null; }
            set { }
        }
    
        [XmlElement("CreditSummaryDataSetValue", Order = 1)]
        public MISMOString CreditSummaryDataSetValue { get; set; }
    
        [XmlIgnore]
        public bool CreditSummaryDataSetValueSpecified
        {
            get { return this.CreditSummaryDataSetValue != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public CREDIT_SUMMARY_DATA_SET_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
