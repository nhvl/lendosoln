namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class CREDIT_SUMMARY_DATA_SETS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditSummaryDataSetListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CREDIT_SUMMARY_DATA_SET", Order = 0)]
        public List<CREDIT_SUMMARY_DATA_SET> CreditSummaryDataSetList { get; set; } = new List<CREDIT_SUMMARY_DATA_SET>();
    
        [XmlIgnore]
        public bool CreditSummaryDataSetListSpecified
        {
            get { return this.CreditSummaryDataSetList != null && this.CreditSummaryDataSetList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public CREDIT_SUMMARY_DATA_SETS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
