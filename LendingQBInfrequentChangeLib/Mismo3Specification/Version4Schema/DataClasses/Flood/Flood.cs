namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class FLOOD
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.FloodRequestSpecified
                    || this.FloodResponseSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("FLOOD_REQUEST", Order = 0)]
        public FLOOD_REQUEST FloodRequest { get; set; }
    
        [XmlIgnore]
        public bool FloodRequestSpecified
        {
            get { return this.FloodRequest != null && this.FloodRequest.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("FLOOD_RESPONSE", Order = 1)]
        public FLOOD_RESPONSE FloodResponse { get; set; }
    
        [XmlIgnore]
        public bool FloodResponseSpecified
        {
            get { return this.FloodResponse != null && this.FloodResponse.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public FLOOD_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
