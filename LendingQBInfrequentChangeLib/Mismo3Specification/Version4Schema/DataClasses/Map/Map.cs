namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MAP
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.TemplateSpecified
                    || this.TransformsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("TEMPLATE", Order = 0)]
        public TEMPLATE Template { get; set; }
    
        [XmlIgnore]
        public bool TemplateSpecified
        {
            get { return this.Template != null && this.Template.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("TRANSFORMS", Order = 1)]
        public TRANSFORMS Transforms { get; set; }
    
        [XmlIgnore]
        public bool TransformsSpecified
        {
            get { return this.Transforms != null && this.Transforms.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public MAP_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
