namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class NEGATIVE_AMORTIZATION_RULE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LoanNegativeAmortizationResolutionTypeSpecified
                    || this.LoanNegativeAmortizationResolutionTypeOtherDescriptionSpecified
                    || this.MaximumPrincipalBalanceAmountSpecified
                    || this.NegativeAmortizationLimitAmountSpecified
                    || this.NegativeAmortizationLimitMonthsCountSpecified
                    || this.NegativeAmortizationLimitPercentSpecified
                    || this.NegativeAmortizationMaximumLoanBalanceAmountSpecified
                    || this.NegativeAmortizationMaximumPITIAmountSpecified
                    || this.NegativeAmortizationRecastTypeSpecified
                    || this.NegativeAmortizationTypeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LoanNegativeAmortizationResolutionType", Order = 0)]
        public MISMOEnum<LoanNegativeAmortizationResolutionBase> LoanNegativeAmortizationResolutionType { get; set; }
    
        [XmlIgnore]
        public bool LoanNegativeAmortizationResolutionTypeSpecified
        {
            get { return this.LoanNegativeAmortizationResolutionType != null; }
            set { }
        }
    
        [XmlElement("LoanNegativeAmortizationResolutionTypeOtherDescription", Order = 1)]
        public MISMOString LoanNegativeAmortizationResolutionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool LoanNegativeAmortizationResolutionTypeOtherDescriptionSpecified
        {
            get { return this.LoanNegativeAmortizationResolutionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MaximumPrincipalBalanceAmount", Order = 2)]
        public MISMOAmount MaximumPrincipalBalanceAmount { get; set; }
    
        [XmlIgnore]
        public bool MaximumPrincipalBalanceAmountSpecified
        {
            get { return this.MaximumPrincipalBalanceAmount != null; }
            set { }
        }
    
        [XmlElement("NegativeAmortizationLimitAmount", Order = 3)]
        public MISMOAmount NegativeAmortizationLimitAmount { get; set; }
    
        [XmlIgnore]
        public bool NegativeAmortizationLimitAmountSpecified
        {
            get { return this.NegativeAmortizationLimitAmount != null; }
            set { }
        }
    
        [XmlElement("NegativeAmortizationLimitMonthsCount", Order = 4)]
        public MISMOCount NegativeAmortizationLimitMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool NegativeAmortizationLimitMonthsCountSpecified
        {
            get { return this.NegativeAmortizationLimitMonthsCount != null; }
            set { }
        }
    
        [XmlElement("NegativeAmortizationLimitPercent", Order = 5)]
        public MISMOPercent NegativeAmortizationLimitPercent { get; set; }
    
        [XmlIgnore]
        public bool NegativeAmortizationLimitPercentSpecified
        {
            get { return this.NegativeAmortizationLimitPercent != null; }
            set { }
        }
    
        [XmlElement("NegativeAmortizationMaximumLoanBalanceAmount", Order = 6)]
        public MISMOAmount NegativeAmortizationMaximumLoanBalanceAmount { get; set; }
    
        [XmlIgnore]
        public bool NegativeAmortizationMaximumLoanBalanceAmountSpecified
        {
            get { return this.NegativeAmortizationMaximumLoanBalanceAmount != null; }
            set { }
        }
    
        [XmlElement("NegativeAmortizationMaximumPITIAmount", Order = 7)]
        public MISMOAmount NegativeAmortizationMaximumPITIAmount { get; set; }
    
        [XmlIgnore]
        public bool NegativeAmortizationMaximumPITIAmountSpecified
        {
            get { return this.NegativeAmortizationMaximumPITIAmount != null; }
            set { }
        }
    
        [XmlElement("NegativeAmortizationRecastType", Order = 8)]
        public MISMOEnum<NegativeAmortizationRecastBase> NegativeAmortizationRecastType { get; set; }
    
        [XmlIgnore]
        public bool NegativeAmortizationRecastTypeSpecified
        {
            get { return this.NegativeAmortizationRecastType != null; }
            set { }
        }
    
        [XmlElement("NegativeAmortizationType", Order = 9)]
        public MISMOEnum<NegativeAmortizationBase> NegativeAmortizationType { get; set; }
    
        [XmlIgnore]
        public bool NegativeAmortizationTypeSpecified
        {
            get { return this.NegativeAmortizationType != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 10)]
        public NEGATIVE_AMORTIZATION_RULE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
