namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class VALUE_DEFINITION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DefinitionOfValueTypeSpecified
                    || this.DefinitionOfValueTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DefinitionOfValueType", Order = 0)]
        public MISMOEnum<DefinitionOfValueBase> DefinitionOfValueType { get; set; }
    
        [XmlIgnore]
        public bool DefinitionOfValueTypeSpecified
        {
            get { return this.DefinitionOfValueType != null; }
            set { }
        }
    
        [XmlElement("DefinitionOfValueTypeOtherDescription", Order = 1)]
        public MISMOString DefinitionOfValueTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool DefinitionOfValueTypeOtherDescriptionSpecified
        {
            get { return this.DefinitionOfValueTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public VALUE_DEFINITION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
