namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class VALUE_DEFINITIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ValueDefinitionListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("VALUE_DEFINITION", Order = 0)]
        public List<VALUE_DEFINITION> ValueDefinitionList { get; set; } = new List<VALUE_DEFINITION>();
    
        [XmlIgnore]
        public bool ValueDefinitionListSpecified
        {
            get { return this.ValueDefinitionList != null && this.ValueDefinitionList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public VALUE_DEFINITIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
