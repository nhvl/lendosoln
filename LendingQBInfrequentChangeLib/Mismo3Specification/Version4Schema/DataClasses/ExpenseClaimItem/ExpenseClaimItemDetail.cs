namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class EXPENSE_CLAIM_ITEM_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExpenseClaimCategoryIdentifierSpecified
                    || this.ExpenseClaimClassificationIdentifierSpecified
                    || this.ExpenseClaimItemCancellationDateSpecified
                    || this.ExpenseClaimItemCountSpecified
                    || this.ExpenseClaimItemEscrowedIndicatorSpecified
                    || this.ExpenseClaimItemFromDateSpecified
                    || this.ExpenseClaimItemIdentifierSpecified
                    || this.ExpenseClaimItemInsurancePlacedTypeSpecified
                    || this.ExpenseClaimItemInsurancePlacedTypeOtherDescriptionSpecified
                    || this.ExpenseClaimItemToDateSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ExpenseClaimCategoryIdentifier", Order = 0)]
        public MISMOIdentifier ExpenseClaimCategoryIdentifier { get; set; }
    
        [XmlIgnore]
        public bool ExpenseClaimCategoryIdentifierSpecified
        {
            get { return this.ExpenseClaimCategoryIdentifier != null; }
            set { }
        }
    
        [XmlElement("ExpenseClaimClassificationIdentifier", Order = 1)]
        public MISMOIdentifier ExpenseClaimClassificationIdentifier { get; set; }
    
        [XmlIgnore]
        public bool ExpenseClaimClassificationIdentifierSpecified
        {
            get { return this.ExpenseClaimClassificationIdentifier != null; }
            set { }
        }
    
        [XmlElement("ExpenseClaimItemCancellationDate", Order = 2)]
        public MISMODate ExpenseClaimItemCancellationDate { get; set; }
    
        [XmlIgnore]
        public bool ExpenseClaimItemCancellationDateSpecified
        {
            get { return this.ExpenseClaimItemCancellationDate != null; }
            set { }
        }
    
        [XmlElement("ExpenseClaimItemCount", Order = 3)]
        public MISMOCount ExpenseClaimItemCount { get; set; }
    
        [XmlIgnore]
        public bool ExpenseClaimItemCountSpecified
        {
            get { return this.ExpenseClaimItemCount != null; }
            set { }
        }
    
        [XmlElement("ExpenseClaimItemEscrowedIndicator", Order = 4)]
        public MISMOIndicator ExpenseClaimItemEscrowedIndicator { get; set; }
    
        [XmlIgnore]
        public bool ExpenseClaimItemEscrowedIndicatorSpecified
        {
            get { return this.ExpenseClaimItemEscrowedIndicator != null; }
            set { }
        }
    
        [XmlElement("ExpenseClaimItemFromDate", Order = 5)]
        public MISMODate ExpenseClaimItemFromDate { get; set; }
    
        [XmlIgnore]
        public bool ExpenseClaimItemFromDateSpecified
        {
            get { return this.ExpenseClaimItemFromDate != null; }
            set { }
        }
    
        [XmlElement("ExpenseClaimItemIdentifier", Order = 6)]
        public MISMOIdentifier ExpenseClaimItemIdentifier { get; set; }
    
        [XmlIgnore]
        public bool ExpenseClaimItemIdentifierSpecified
        {
            get { return this.ExpenseClaimItemIdentifier != null; }
            set { }
        }
    
        [XmlElement("ExpenseClaimItemInsurancePlacedType", Order = 7)]
        public MISMOEnum<ExpenseClaimItemInsurancePlacedBase> ExpenseClaimItemInsurancePlacedType { get; set; }
    
        [XmlIgnore]
        public bool ExpenseClaimItemInsurancePlacedTypeSpecified
        {
            get { return this.ExpenseClaimItemInsurancePlacedType != null; }
            set { }
        }
    
        [XmlElement("ExpenseClaimItemInsurancePlacedTypeOtherDescription", Order = 8)]
        public MISMOString ExpenseClaimItemInsurancePlacedTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ExpenseClaimItemInsurancePlacedTypeOtherDescriptionSpecified
        {
            get { return this.ExpenseClaimItemInsurancePlacedTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ExpenseClaimItemToDate", Order = 9)]
        public MISMODate ExpenseClaimItemToDate { get; set; }
    
        [XmlIgnore]
        public bool ExpenseClaimItemToDateSpecified
        {
            get { return this.ExpenseClaimItemToDate != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 10)]
        public EXPENSE_CLAIM_ITEM_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
