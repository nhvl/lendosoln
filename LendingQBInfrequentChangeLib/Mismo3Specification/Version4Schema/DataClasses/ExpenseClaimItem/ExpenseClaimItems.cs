namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class EXPENSE_CLAIM_ITEMS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExpenseClaimItemListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("EXPENSE_CLAIM_ITEM", Order = 0)]
        public List<EXPENSE_CLAIM_ITEM> ExpenseClaimItemList { get; set; } = new List<EXPENSE_CLAIM_ITEM>();
    
        [XmlIgnore]
        public bool ExpenseClaimItemListSpecified
        {
            get { return this.ExpenseClaimItemList != null && this.ExpenseClaimItemList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public EXPENSE_CLAIM_ITEMS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
