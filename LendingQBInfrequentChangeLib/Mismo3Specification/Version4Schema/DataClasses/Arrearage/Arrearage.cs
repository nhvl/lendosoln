namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ARREARAGE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ArrearageComponentsSpecified
                    || this.ArrearageSummarySpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ARREARAGE_COMPONENTS", Order = 0)]
        public ARREARAGE_COMPONENTS ArrearageComponents { get; set; }
    
        [XmlIgnore]
        public bool ArrearageComponentsSpecified
        {
            get { return this.ArrearageComponents != null && this.ArrearageComponents.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("ARREARAGE_SUMMARY", Order = 1)]
        public ARREARAGE_SUMMARY ArrearageSummary { get; set; }
    
        [XmlIgnore]
        public bool ArrearageSummarySpecified
        {
            get { return this.ArrearageSummary != null && this.ArrearageSummary.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public ARREARAGE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
