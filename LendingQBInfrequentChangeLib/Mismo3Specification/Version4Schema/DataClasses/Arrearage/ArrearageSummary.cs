namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ARREARAGE_SUMMARY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MonthlyArrearagePaidAmountSpecified
                    || this.TotalArrearageAmountSpecified
                    || this.TotalInterestArrearageAmountSpecified
                    || this.TotalProjectedInterestArrearageAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MonthlyArrearagePaidAmount", Order = 0)]
        public MISMOAmount MonthlyArrearagePaidAmount { get; set; }
    
        [XmlIgnore]
        public bool MonthlyArrearagePaidAmountSpecified
        {
            get { return this.MonthlyArrearagePaidAmount != null; }
            set { }
        }
    
        [XmlElement("TotalArrearageAmount", Order = 1)]
        public MISMOAmount TotalArrearageAmount { get; set; }
    
        [XmlIgnore]
        public bool TotalArrearageAmountSpecified
        {
            get { return this.TotalArrearageAmount != null; }
            set { }
        }
    
        [XmlElement("TotalInterestArrearageAmount", Order = 2)]
        public MISMOAmount TotalInterestArrearageAmount { get; set; }
    
        [XmlIgnore]
        public bool TotalInterestArrearageAmountSpecified
        {
            get { return this.TotalInterestArrearageAmount != null; }
            set { }
        }
    
        [XmlElement("TotalProjectedInterestArrearageAmount", Order = 3)]
        public MISMOAmount TotalProjectedInterestArrearageAmount { get; set; }
    
        [XmlIgnore]
        public bool TotalProjectedInterestArrearageAmountSpecified
        {
            get { return this.TotalProjectedInterestArrearageAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public ARREARAGE_SUMMARY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
