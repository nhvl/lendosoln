namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class NAME
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EducationalAchievementsDescriptionSpecified
                    || this.FirstNameSpecified
                    || this.FullNameSpecified
                    || this.FunctionalTitleDescriptionSpecified
                    || this.IndividualTitleDescriptionSpecified
                    || this.LastNameSpecified
                    || this.MiddleNameSpecified
                    || this.PrefixNameSpecified
                    || this.SuffixNameSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("EducationalAchievementsDescription", Order = 0)]
        public MISMOString EducationalAchievementsDescription { get; set; }
    
        [XmlIgnore]
        public bool EducationalAchievementsDescriptionSpecified
        {
            get { return this.EducationalAchievementsDescription != null; }
            set { }
        }
    
        [XmlElement("FirstName", Order = 1)]
        public MISMOString FirstName { get; set; }
    
        [XmlIgnore]
        public bool FirstNameSpecified
        {
            get { return this.FirstName != null; }
            set { }
        }
    
        [XmlElement("FullName", Order = 2)]
        public MISMOString FullName { get; set; }
    
        [XmlIgnore]
        public bool FullNameSpecified
        {
            get { return this.FullName != null; }
            set { }
        }
    
        [XmlElement("FunctionalTitleDescription", Order = 3)]
        public MISMOString FunctionalTitleDescription { get; set; }
    
        [XmlIgnore]
        public bool FunctionalTitleDescriptionSpecified
        {
            get { return this.FunctionalTitleDescription != null; }
            set { }
        }
    
        [XmlElement("IndividualTitleDescription", Order = 4)]
        public MISMOString IndividualTitleDescription { get; set; }
    
        [XmlIgnore]
        public bool IndividualTitleDescriptionSpecified
        {
            get { return this.IndividualTitleDescription != null; }
            set { }
        }
    
        [XmlElement("LastName", Order = 5)]
        public MISMOString LastName { get; set; }
    
        [XmlIgnore]
        public bool LastNameSpecified
        {
            get { return this.LastName != null; }
            set { }
        }
    
        [XmlElement("MiddleName", Order = 6)]
        public MISMOString MiddleName { get; set; }
    
        [XmlIgnore]
        public bool MiddleNameSpecified
        {
            get { return this.MiddleName != null; }
            set { }
        }
    
        [XmlElement("PrefixName", Order = 7)]
        public MISMOString PrefixName { get; set; }
    
        [XmlIgnore]
        public bool PrefixNameSpecified
        {
            get { return this.PrefixName != null; }
            set { }
        }
    
        [XmlElement("SuffixName", Order = 8)]
        public MISMOString SuffixName { get; set; }
    
        [XmlIgnore]
        public bool SuffixNameSpecified
        {
            get { return this.SuffixName != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 9)]
        public NAME_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
