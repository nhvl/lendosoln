namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class FORECLOSURE_DELAYS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ForeclosureDelayListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("FORECLOSURE_DELAY", Order = 0)]
        public List<FORECLOSURE_DELAY> ForeclosureDelayList { get; set; } = new List<FORECLOSURE_DELAY>();
    
        [XmlIgnore]
        public bool ForeclosureDelayListSpecified
        {
            get { return this.ForeclosureDelayList != null && this.ForeclosureDelayList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public FORECLOSURE_DELAYS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
