namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class FORECLOSURE_DELAY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ForeclosureDelayActualEndDateSpecified
                    || this.ForeclosureDelayCategoryAdditionalDescriptionSpecified
                    || this.ForeclosureDelayCategoryTypeSpecified
                    || this.ForeclosureDelayCategoryTypeOtherDescriptionSpecified
                    || this.ForeclosureDelayEndReasonTypeSpecified
                    || this.ForeclosureDelayEndReasonTypeAdditionalDescriptionSpecified
                    || this.ForeclosureDelayEndReasonTypeOtherDescriptionSpecified
                    || this.ForeclosureDelayProjectedEndDateSpecified
                    || this.ForeclosureDelayStartDateSpecified
                    || this.LegislativeChangeImpactsForeclosureIndicatorSpecified
                    || this.MediationSuccessfulIndicatorSpecified
                    || this.PropertySeizedIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ForeclosureDelayActualEndDate", Order = 0)]
        public MISMODate ForeclosureDelayActualEndDate { get; set; }
    
        [XmlIgnore]
        public bool ForeclosureDelayActualEndDateSpecified
        {
            get { return this.ForeclosureDelayActualEndDate != null; }
            set { }
        }
    
        [XmlElement("ForeclosureDelayCategoryAdditionalDescription", Order = 1)]
        public MISMOString ForeclosureDelayCategoryAdditionalDescription { get; set; }
    
        [XmlIgnore]
        public bool ForeclosureDelayCategoryAdditionalDescriptionSpecified
        {
            get { return this.ForeclosureDelayCategoryAdditionalDescription != null; }
            set { }
        }
    
        [XmlElement("ForeclosureDelayCategoryType", Order = 2)]
        public MISMOEnum<ForeclosureDelayCategoryBase> ForeclosureDelayCategoryType { get; set; }
    
        [XmlIgnore]
        public bool ForeclosureDelayCategoryTypeSpecified
        {
            get { return this.ForeclosureDelayCategoryType != null; }
            set { }
        }
    
        [XmlElement("ForeclosureDelayCategoryTypeOtherDescription", Order = 3)]
        public MISMOString ForeclosureDelayCategoryTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ForeclosureDelayCategoryTypeOtherDescriptionSpecified
        {
            get { return this.ForeclosureDelayCategoryTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ForeclosureDelayEndReasonType", Order = 4)]
        public MISMOEnum<ForeclosureDelayEndReasonBase> ForeclosureDelayEndReasonType { get; set; }
    
        [XmlIgnore]
        public bool ForeclosureDelayEndReasonTypeSpecified
        {
            get { return this.ForeclosureDelayEndReasonType != null; }
            set { }
        }
    
        [XmlElement("ForeclosureDelayEndReasonTypeAdditionalDescription", Order = 5)]
        public MISMOString ForeclosureDelayEndReasonTypeAdditionalDescription { get; set; }
    
        [XmlIgnore]
        public bool ForeclosureDelayEndReasonTypeAdditionalDescriptionSpecified
        {
            get { return this.ForeclosureDelayEndReasonTypeAdditionalDescription != null; }
            set { }
        }
    
        [XmlElement("ForeclosureDelayEndReasonTypeOtherDescription", Order = 6)]
        public MISMOString ForeclosureDelayEndReasonTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ForeclosureDelayEndReasonTypeOtherDescriptionSpecified
        {
            get { return this.ForeclosureDelayEndReasonTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ForeclosureDelayProjectedEndDate", Order = 7)]
        public MISMODate ForeclosureDelayProjectedEndDate { get; set; }
    
        [XmlIgnore]
        public bool ForeclosureDelayProjectedEndDateSpecified
        {
            get { return this.ForeclosureDelayProjectedEndDate != null; }
            set { }
        }
    
        [XmlElement("ForeclosureDelayStartDate", Order = 8)]
        public MISMODate ForeclosureDelayStartDate { get; set; }
    
        [XmlIgnore]
        public bool ForeclosureDelayStartDateSpecified
        {
            get { return this.ForeclosureDelayStartDate != null; }
            set { }
        }
    
        [XmlElement("LegislativeChangeImpactsForeclosureIndicator", Order = 9)]
        public MISMOIndicator LegislativeChangeImpactsForeclosureIndicator { get; set; }
    
        [XmlIgnore]
        public bool LegislativeChangeImpactsForeclosureIndicatorSpecified
        {
            get { return this.LegislativeChangeImpactsForeclosureIndicator != null; }
            set { }
        }
    
        [XmlElement("MediationSuccessfulIndicator", Order = 10)]
        public MISMOIndicator MediationSuccessfulIndicator { get; set; }
    
        [XmlIgnore]
        public bool MediationSuccessfulIndicatorSpecified
        {
            get { return this.MediationSuccessfulIndicator != null; }
            set { }
        }
    
        [XmlElement("PropertySeizedIndicator", Order = 11)]
        public MISMOIndicator PropertySeizedIndicator { get; set; }
    
        [XmlIgnore]
        public bool PropertySeizedIndicatorSpecified
        {
            get { return this.PropertySeizedIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 12)]
        public FORECLOSURE_DELAY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
