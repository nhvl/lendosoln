namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class FORECLOSURE_DELAY_EXTENSION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MismoSpecified
                    || this.OtherSpecified;
            }
        }
    
        [XmlElement("MISMO", Order = 0)]
        public MISMO_BASE Mismo { get; set; }
    
        [XmlIgnore]
        public bool MismoSpecified
        {
            get { return this.Mismo != null && this.Mismo.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("OTHER", Order = 1)]
        public OTHER_BASE Other { get; set; }
    
        [XmlIgnore]
        public bool OtherSpecified
        {
            get { return this.Other != null && this.Other.ShouldSerialize; }
            set { }
        }
    }
}
