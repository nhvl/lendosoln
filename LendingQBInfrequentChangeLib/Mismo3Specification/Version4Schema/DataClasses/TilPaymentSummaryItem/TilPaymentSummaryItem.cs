namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class TIL_PAYMENT_SUMMARY_ITEM
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.TILEstimatedTotalPaymentAmountSpecified
                    || this.TILInterestPaymentAmountSpecified
                    || this.TILInterestRatePercentSpecified
                    || this.TILNegativeAmortizationFullPrincipalPaymentAmountSpecified
                    || this.TILNegativeAmortizationMinimumPrincipalPaymentAmountSpecified
                    || this.TILPaymentAdjustmentDateSpecified
                    || this.TILPaymentSummaryItemTypeSpecified
                    || this.TILPaymentSummaryItemTypeOtherDescriptionSpecified
                    || this.TILPeriodDurationCountSpecified
                    || this.TILPeriodDurationTypeSpecified
                    || this.TILPrincipalPaymentAmountSpecified
                    || this.TILTaxesAndInsuranceEscrowPaymentAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("TILEstimatedTotalPaymentAmount", Order = 0)]
        public MISMOAmount TILEstimatedTotalPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool TILEstimatedTotalPaymentAmountSpecified
        {
            get { return this.TILEstimatedTotalPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("TILInterestPaymentAmount", Order = 1)]
        public MISMOAmount TILInterestPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool TILInterestPaymentAmountSpecified
        {
            get { return this.TILInterestPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("TILInterestRatePercent", Order = 2)]
        public MISMOPercent TILInterestRatePercent { get; set; }
    
        [XmlIgnore]
        public bool TILInterestRatePercentSpecified
        {
            get { return this.TILInterestRatePercent != null; }
            set { }
        }
    
        [XmlElement("TILNegativeAmortizationFullPrincipalPaymentAmount", Order = 3)]
        public MISMOAmount TILNegativeAmortizationFullPrincipalPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool TILNegativeAmortizationFullPrincipalPaymentAmountSpecified
        {
            get { return this.TILNegativeAmortizationFullPrincipalPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("TILNegativeAmortizationMinimumPrincipalPaymentAmount", Order = 4)]
        public MISMOAmount TILNegativeAmortizationMinimumPrincipalPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool TILNegativeAmortizationMinimumPrincipalPaymentAmountSpecified
        {
            get { return this.TILNegativeAmortizationMinimumPrincipalPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("TILPaymentAdjustmentDate", Order = 5)]
        public MISMODate TILPaymentAdjustmentDate { get; set; }
    
        [XmlIgnore]
        public bool TILPaymentAdjustmentDateSpecified
        {
            get { return this.TILPaymentAdjustmentDate != null; }
            set { }
        }
    
        [XmlElement("TILPaymentSummaryItemType", Order = 6)]
        public MISMOEnum<TILPaymentSummaryItemBase> TILPaymentSummaryItemType { get; set; }
    
        [XmlIgnore]
        public bool TILPaymentSummaryItemTypeSpecified
        {
            get { return this.TILPaymentSummaryItemType != null; }
            set { }
        }
    
        [XmlElement("TILPaymentSummaryItemTypeOtherDescription", Order = 7)]
        public MISMOString TILPaymentSummaryItemTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool TILPaymentSummaryItemTypeOtherDescriptionSpecified
        {
            get { return this.TILPaymentSummaryItemTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("TILPeriodDurationCount", Order = 8)]
        public MISMOCount TILPeriodDurationCount { get; set; }
    
        [XmlIgnore]
        public bool TILPeriodDurationCountSpecified
        {
            get { return this.TILPeriodDurationCount != null; }
            set { }
        }
    
        [XmlElement("TILPeriodDurationType", Order = 9)]
        public MISMOEnum<TILPeriodDurationBase> TILPeriodDurationType { get; set; }
    
        [XmlIgnore]
        public bool TILPeriodDurationTypeSpecified
        {
            get { return this.TILPeriodDurationType != null; }
            set { }
        }
    
        [XmlElement("TILPrincipalPaymentAmount", Order = 10)]
        public MISMOAmount TILPrincipalPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool TILPrincipalPaymentAmountSpecified
        {
            get { return this.TILPrincipalPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("TILTaxesAndInsuranceEscrowPaymentAmount", Order = 11)]
        public MISMOAmount TILTaxesAndInsuranceEscrowPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool TILTaxesAndInsuranceEscrowPaymentAmountSpecified
        {
            get { return this.TILTaxesAndInsuranceEscrowPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 12)]
        public TIL_PAYMENT_SUMMARY_ITEM_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
