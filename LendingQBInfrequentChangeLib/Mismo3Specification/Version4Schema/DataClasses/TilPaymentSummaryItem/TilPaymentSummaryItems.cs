namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class TIL_PAYMENT_SUMMARY_ITEMS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.TilPaymentSummaryItemListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("TIL_PAYMENT_SUMMARY_ITEM", Order = 0)]
        public List<TIL_PAYMENT_SUMMARY_ITEM> TilPaymentSummaryItemList { get; set; } = new List<TIL_PAYMENT_SUMMARY_ITEM>();
    
        [XmlIgnore]
        public bool TilPaymentSummaryItemListSpecified
        {
            get { return this.TilPaymentSummaryItemList != null && this.TilPaymentSummaryItemList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public TIL_PAYMENT_SUMMARY_ITEMS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
