namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class EXPENSE_CLAIM_ITEM_OCCURRENCE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExpenseClaimItemCommentTextSpecified
                    || this.ExpenseClaimItemCompletedDateSpecified
                    || this.ExpenseClaimItemPaidDateSpecified
                    || this.ExpenseClaimItemRequestedAmountSpecified
                    || this.ExpenseClaimItemUnitPriceAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ExpenseClaimItemCommentText", Order = 0)]
        public MISMOString ExpenseClaimItemCommentText { get; set; }
    
        [XmlIgnore]
        public bool ExpenseClaimItemCommentTextSpecified
        {
            get { return this.ExpenseClaimItemCommentText != null; }
            set { }
        }
    
        [XmlElement("ExpenseClaimItemCompletedDate", Order = 1)]
        public MISMODate ExpenseClaimItemCompletedDate { get; set; }
    
        [XmlIgnore]
        public bool ExpenseClaimItemCompletedDateSpecified
        {
            get { return this.ExpenseClaimItemCompletedDate != null; }
            set { }
        }
    
        [XmlElement("ExpenseClaimItemPaidDate", Order = 2)]
        public MISMODate ExpenseClaimItemPaidDate { get; set; }
    
        [XmlIgnore]
        public bool ExpenseClaimItemPaidDateSpecified
        {
            get { return this.ExpenseClaimItemPaidDate != null; }
            set { }
        }
    
        [XmlElement("ExpenseClaimItemRequestedAmount", Order = 3)]
        public MISMOAmount ExpenseClaimItemRequestedAmount { get; set; }
    
        [XmlIgnore]
        public bool ExpenseClaimItemRequestedAmountSpecified
        {
            get { return this.ExpenseClaimItemRequestedAmount != null; }
            set { }
        }
    
        [XmlElement("ExpenseClaimItemUnitPriceAmount", Order = 4)]
        public MISMOAmount ExpenseClaimItemUnitPriceAmount { get; set; }
    
        [XmlIgnore]
        public bool ExpenseClaimItemUnitPriceAmountSpecified
        {
            get { return this.ExpenseClaimItemUnitPriceAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public EXPENSE_CLAIM_ITEM_OCCURRENCE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
