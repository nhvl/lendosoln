namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class EXPENSE_CLAIM_ITEM_OCCURRENCES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExpenseClaimItemOccurrenceListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("EXPENSE_CLAIM_ITEM_OCCURRENCE", Order = 0)]
        public List<EXPENSE_CLAIM_ITEM_OCCURRENCE> ExpenseClaimItemOccurrenceList { get; set; } = new List<EXPENSE_CLAIM_ITEM_OCCURRENCE>();
    
        [XmlIgnore]
        public bool ExpenseClaimItemOccurrenceListSpecified
        {
            get { return this.ExpenseClaimItemOccurrenceList != null && this.ExpenseClaimItemOccurrenceList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public EXPENSE_CLAIM_ITEM_OCCURRENCES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
