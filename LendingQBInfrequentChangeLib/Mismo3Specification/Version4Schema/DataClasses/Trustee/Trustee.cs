namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class TRUSTEE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DeedOfTrustTrusteeIndicatorSpecified
                    || this.TrusteeTypeSpecified
                    || this.TrusteeTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DeedOfTrustTrusteeIndicator", Order = 0)]
        public MISMOIndicator DeedOfTrustTrusteeIndicator { get; set; }
    
        [XmlIgnore]
        public bool DeedOfTrustTrusteeIndicatorSpecified
        {
            get { return this.DeedOfTrustTrusteeIndicator != null; }
            set { }
        }
    
        [XmlElement("TrusteeType", Order = 1)]
        public MISMOEnum<TrusteeBase> TrusteeType { get; set; }
    
        [XmlIgnore]
        public bool TrusteeTypeSpecified
        {
            get { return this.TrusteeType != null; }
            set { }
        }
    
        [XmlElement("TrusteeTypeOtherDescription", Order = 2)]
        public MISMOString TrusteeTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool TrusteeTypeOtherDescriptionSpecified
        {
            get { return this.TrusteeTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public TRUSTEE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
