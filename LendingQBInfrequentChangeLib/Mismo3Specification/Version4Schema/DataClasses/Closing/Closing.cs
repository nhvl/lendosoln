namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CLOSING
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ClosingLocationsSpecified
                    || this.ClosingRequestSpecified
                    || this.ClosingResponseSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CLOSING_LOCATIONS", Order = 0)]
        public CLOSING_LOCATIONS ClosingLocations { get; set; }
    
        [XmlIgnore]
        public bool ClosingLocationsSpecified
        {
            get { return this.ClosingLocations != null && this.ClosingLocations.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CLOSING_REQUEST", Order = 1)]
        public CLOSING_REQUEST ClosingRequest { get; set; }
    
        [XmlIgnore]
        public bool ClosingRequestSpecified
        {
            get { return this.ClosingRequest != null && this.ClosingRequest.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CLOSING_RESPONSE", Order = 2)]
        public CLOSING_RESPONSE ClosingResponse { get; set; }
    
        [XmlIgnore]
        public bool ClosingResponseSpecified
        {
            get { return this.ClosingResponse != null && this.ClosingResponse.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public CLOSING_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
