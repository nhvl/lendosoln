namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class LOAN_STATE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LoanStateDateSpecified
                    || this.LoanStateDatetimeSpecified
                    || this.LoanStateTypeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LoanStateDate", Order = 0)]
        public MISMODate LoanStateDate { get; set; }
    
        [XmlIgnore]
        public bool LoanStateDateSpecified
        {
            get { return this.LoanStateDate != null; }
            set { }
        }
    
        [XmlElement("LoanStateDatetime", Order = 1)]
        public MISMODatetime LoanStateDatetime { get; set; }
    
        [XmlIgnore]
        public bool LoanStateDatetimeSpecified
        {
            get { return this.LoanStateDatetime != null; }
            set { }
        }
    
        [XmlElement("LoanStateType", Order = 2)]
        public MISMOEnum<LoanStateBase> LoanStateType { get; set; }
    
        [XmlIgnore]
        public bool LoanStateTypeSpecified
        {
            get { return this.LoanStateType != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public LOAN_STATE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
