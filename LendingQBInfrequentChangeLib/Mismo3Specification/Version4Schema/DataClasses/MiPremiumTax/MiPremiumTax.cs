namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MI_PREMIUM_TAX
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MIPremiumTaxCodeAmountSpecified
                    || this.MIPremiumTaxCodePercentSpecified
                    || this.MIPremiumTaxCodeTypeSpecified
                    || this.MIPremiumTaxingAuthorityNameSpecified
                    || this.PaymentIncludedInAPRIndicatorSpecified
                    || this.PaymentIncludedInJurisdictionHighCostIndicatorSpecified
                    || this.RegulationZPointsAndFeesIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MIPremiumTaxCodeAmount", Order = 0)]
        public MISMOAmount MIPremiumTaxCodeAmount { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumTaxCodeAmountSpecified
        {
            get { return this.MIPremiumTaxCodeAmount != null; }
            set { }
        }
    
        [XmlElement("MIPremiumTaxCodePercent", Order = 1)]
        public MISMOPercent MIPremiumTaxCodePercent { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumTaxCodePercentSpecified
        {
            get { return this.MIPremiumTaxCodePercent != null; }
            set { }
        }
    
        [XmlElement("MIPremiumTaxCodeType", Order = 2)]
        public MISMOEnum<MIPremiumTaxCodeBase> MIPremiumTaxCodeType { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumTaxCodeTypeSpecified
        {
            get { return this.MIPremiumTaxCodeType != null; }
            set { }
        }
    
        [XmlElement("MIPremiumTaxingAuthorityName", Order = 3)]
        public MISMOString MIPremiumTaxingAuthorityName { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumTaxingAuthorityNameSpecified
        {
            get { return this.MIPremiumTaxingAuthorityName != null; }
            set { }
        }
    
        [XmlElement("PaymentIncludedInAPRIndicator", Order = 4)]
        public MISMOIndicator PaymentIncludedInAPRIndicator { get; set; }
    
        [XmlIgnore]
        public bool PaymentIncludedInAPRIndicatorSpecified
        {
            get { return this.PaymentIncludedInAPRIndicator != null; }
            set { }
        }
    
        [XmlElement("PaymentIncludedInJurisdictionHighCostIndicator", Order = 5)]
        public MISMOIndicator PaymentIncludedInJurisdictionHighCostIndicator { get; set; }
    
        [XmlIgnore]
        public bool PaymentIncludedInJurisdictionHighCostIndicatorSpecified
        {
            get { return this.PaymentIncludedInJurisdictionHighCostIndicator != null; }
            set { }
        }
    
        [XmlElement("RegulationZPointsAndFeesIndicator", Order = 6)]
        public MISMOIndicator RegulationZPointsAndFeesIndicator { get; set; }
    
        [XmlIgnore]
        public bool RegulationZPointsAndFeesIndicatorSpecified
        {
            get { return this.RegulationZPointsAndFeesIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 7)]
        public MI_PREMIUM_TAX_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
