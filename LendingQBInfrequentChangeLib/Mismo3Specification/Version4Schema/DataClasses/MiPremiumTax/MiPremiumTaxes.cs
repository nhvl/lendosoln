namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class MI_PREMIUM_TAXES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MiPremiumTaxListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MI_PREMIUM_TAX", Order = 0)]
        public List<MI_PREMIUM_TAX> MiPremiumTaxList { get; set; } = new List<MI_PREMIUM_TAX>();
    
        [XmlIgnore]
        public bool MiPremiumTaxListSpecified
        {
            get { return this.MiPremiumTaxList != null && this.MiPremiumTaxList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public MI_PREMIUM_TAXES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
