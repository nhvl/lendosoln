namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class RENT_INCLUDES_UTILITIES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.RentIncludesUtilityListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("RENT_INCLUDES_UTILITY", Order = 0)]
        public List<RENT_INCLUDES_UTILITY> RentIncludesUtilityList { get; set; } = new List<RENT_INCLUDES_UTILITY>();
    
        [XmlIgnore]
        public bool RentIncludesUtilityListSpecified
        {
            get { return this.RentIncludesUtilityList != null && this.RentIncludesUtilityList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public RENT_INCLUDES_UTILITIES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
