namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class RENT_INCLUDES_UTILITY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.RentIncludesUtilityTypeSpecified
                    || this.RentIncludesUtilityTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("RentIncludesUtilityType", Order = 0)]
        public MISMOEnum<RentIncludesUtilityBase> RentIncludesUtilityType { get; set; }
    
        [XmlIgnore]
        public bool RentIncludesUtilityTypeSpecified
        {
            get { return this.RentIncludesUtilityType != null; }
            set { }
        }
    
        [XmlElement("RentIncludesUtilityTypeOtherDescription", Order = 1)]
        public MISMOString RentIncludesUtilityTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool RentIncludesUtilityTypeOtherDescriptionSpecified
        {
            get { return this.RentIncludesUtilityTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public RENT_INCLUDES_UTILITY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
