namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class INCOME_DOCUMENTATIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.IncomeDocumentationListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("INCOME_DOCUMENTATION", Order = 0)]
        public List<INCOME_DOCUMENTATION> IncomeDocumentationList { get; set; } = new List<INCOME_DOCUMENTATION>();
    
        [XmlIgnore]
        public bool IncomeDocumentationListSpecified
        {
            get { return this.IncomeDocumentationList != null && this.IncomeDocumentationList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public INCOME_DOCUMENTATIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
