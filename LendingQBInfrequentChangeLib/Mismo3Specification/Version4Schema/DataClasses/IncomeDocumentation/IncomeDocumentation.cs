namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class INCOME_DOCUMENTATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DocumentationStateTypeSpecified
                    || this.DocumentationStateTypeOtherDescriptionSpecified
                    || this.IncomeDocumentTypeSpecified
                    || this.IncomeDocumentTypeOtherDescriptionSpecified
                    || this.IncomeVerificationRangeCountSpecified
                    || this.IncomeVerificationRangeTypeSpecified
                    || this.IncomeVerificationRangeTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DocumentationStateType", Order = 0)]
        public MISMOEnum<DocumentationStateBase> DocumentationStateType { get; set; }
    
        [XmlIgnore]
        public bool DocumentationStateTypeSpecified
        {
            get { return this.DocumentationStateType != null; }
            set { }
        }
    
        [XmlElement("DocumentationStateTypeOtherDescription", Order = 1)]
        public MISMOString DocumentationStateTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool DocumentationStateTypeOtherDescriptionSpecified
        {
            get { return this.DocumentationStateTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("IncomeDocumentType", Order = 2)]
        public MISMOEnum<IncomeDocumentBase> IncomeDocumentType { get; set; }
    
        [XmlIgnore]
        public bool IncomeDocumentTypeSpecified
        {
            get { return this.IncomeDocumentType != null; }
            set { }
        }
    
        [XmlElement("IncomeDocumentTypeOtherDescription", Order = 3)]
        public MISMOString IncomeDocumentTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool IncomeDocumentTypeOtherDescriptionSpecified
        {
            get { return this.IncomeDocumentTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("IncomeVerificationRangeCount", Order = 4)]
        public MISMOCount IncomeVerificationRangeCount { get; set; }
    
        [XmlIgnore]
        public bool IncomeVerificationRangeCountSpecified
        {
            get { return this.IncomeVerificationRangeCount != null; }
            set { }
        }
    
        [XmlElement("IncomeVerificationRangeType", Order = 5)]
        public MISMOEnum<VerificationRangeBase> IncomeVerificationRangeType { get; set; }
    
        [XmlIgnore]
        public bool IncomeVerificationRangeTypeSpecified
        {
            get { return this.IncomeVerificationRangeType != null; }
            set { }
        }
    
        [XmlElement("IncomeVerificationRangeTypeOtherDescription", Order = 6)]
        public MISMOString IncomeVerificationRangeTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool IncomeVerificationRangeTypeOtherDescriptionSpecified
        {
            get { return this.IncomeVerificationRangeTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 7)]
        public INCOME_DOCUMENTATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
