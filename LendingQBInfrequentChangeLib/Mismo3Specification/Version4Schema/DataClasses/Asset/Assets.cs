namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class ASSETS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AssetListSpecified
                    || this.AssetSummarySpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ASSET", Order = 0)]
        public List<ASSET> AssetList { get; set; } = new List<ASSET>();
    
        [XmlIgnore]
        public bool AssetListSpecified
        {
            get { return this.AssetList != null && this.AssetList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("ASSET_SUMMARY", Order = 1)]
        public ASSET_SUMMARY AssetSummary { get; set; }
    
        [XmlIgnore]
        public bool AssetSummarySpecified
        {
            get { return this.AssetSummary != null && this.AssetSummary.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public ASSETS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
