namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ASSET_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AssetAccountIdentifierSpecified
                    || this.AssetAccountInNameOfDescriptionSpecified
                    || this.AssetAccountTypeSpecified
                    || this.AssetAccountTypeOtherDescriptionSpecified
                    || this.AssetCashOrMarketValueAmountSpecified
                    || this.AssetDescriptionSpecified
                    || this.AssetLiquidityIndicatorSpecified
                    || this.AssetNetValueAmountSpecified
                    || this.AssetTypeSpecified
                    || this.AssetTypeOtherDescriptionSpecified
                    || this.AutomobileMakeDescriptionSpecified
                    || this.AutomobileModelYearSpecified
                    || this.FundsSourceTypeSpecified
                    || this.FundsSourceTypeOtherDescriptionSpecified
                    || this.LifeInsuranceFaceValueAmountSpecified
                    || this.StockBondMutualFundShareCountSpecified
                    || this.VerifiedIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AssetAccountIdentifier", Order = 0)]
        public MISMOIdentifier AssetAccountIdentifier { get; set; }
    
        [XmlIgnore]
        public bool AssetAccountIdentifierSpecified
        {
            get { return this.AssetAccountIdentifier != null; }
            set { }
        }
    
        [XmlElement("AssetAccountInNameOfDescription", Order = 1)]
        public MISMOString AssetAccountInNameOfDescription { get; set; }
    
        [XmlIgnore]
        public bool AssetAccountInNameOfDescriptionSpecified
        {
            get { return this.AssetAccountInNameOfDescription != null; }
            set { }
        }
    
        [XmlElement("AssetAccountType", Order = 2)]
        public MISMOEnum<AssetAccountBase> AssetAccountType { get; set; }
    
        [XmlIgnore]
        public bool AssetAccountTypeSpecified
        {
            get { return this.AssetAccountType != null; }
            set { }
        }
    
        [XmlElement("AssetAccountTypeOtherDescription", Order = 3)]
        public MISMOString AssetAccountTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool AssetAccountTypeOtherDescriptionSpecified
        {
            get { return this.AssetAccountTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("AssetCashOrMarketValueAmount", Order = 4)]
        public MISMOAmount AssetCashOrMarketValueAmount { get; set; }
    
        [XmlIgnore]
        public bool AssetCashOrMarketValueAmountSpecified
        {
            get { return this.AssetCashOrMarketValueAmount != null; }
            set { }
        }
    
        [XmlElement("AssetDescription", Order = 5)]
        public MISMOString AssetDescription { get; set; }
    
        [XmlIgnore]
        public bool AssetDescriptionSpecified
        {
            get { return this.AssetDescription != null; }
            set { }
        }
    
        [XmlElement("AssetLiquidityIndicator", Order = 6)]
        public MISMOIndicator AssetLiquidityIndicator { get; set; }
    
        [XmlIgnore]
        public bool AssetLiquidityIndicatorSpecified
        {
            get { return this.AssetLiquidityIndicator != null; }
            set { }
        }
    
        [XmlElement("AssetNetValueAmount", Order = 7)]
        public MISMOAmount AssetNetValueAmount { get; set; }
    
        [XmlIgnore]
        public bool AssetNetValueAmountSpecified
        {
            get { return this.AssetNetValueAmount != null; }
            set { }
        }
    
        [XmlElement("AssetType", Order = 8)]
        public MISMOEnum<AssetBase> AssetType { get; set; }
    
        [XmlIgnore]
        public bool AssetTypeSpecified
        {
            get { return this.AssetType != null; }
            set { }
        }
    
        [XmlElement("AssetTypeOtherDescription", Order = 9)]
        public MISMOString AssetTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool AssetTypeOtherDescriptionSpecified
        {
            get { return this.AssetTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("AutomobileMakeDescription", Order = 10)]
        public MISMOString AutomobileMakeDescription { get; set; }
    
        [XmlIgnore]
        public bool AutomobileMakeDescriptionSpecified
        {
            get { return this.AutomobileMakeDescription != null; }
            set { }
        }
    
        [XmlElement("AutomobileModelYear", Order = 11)]
        public MISMOYear AutomobileModelYear { get; set; }
    
        [XmlIgnore]
        public bool AutomobileModelYearSpecified
        {
            get { return this.AutomobileModelYear != null; }
            set { }
        }
    
        [XmlElement("FundsSourceType", Order = 12)]
        public MISMOEnum<FundsSourceBase> FundsSourceType { get; set; }
    
        [XmlIgnore]
        public bool FundsSourceTypeSpecified
        {
            get { return this.FundsSourceType != null; }
            set { }
        }
    
        [XmlElement("FundsSourceTypeOtherDescription", Order = 13)]
        public MISMOString FundsSourceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool FundsSourceTypeOtherDescriptionSpecified
        {
            get { return this.FundsSourceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("LifeInsuranceFaceValueAmount", Order = 14)]
        public MISMOAmount LifeInsuranceFaceValueAmount { get; set; }
    
        [XmlIgnore]
        public bool LifeInsuranceFaceValueAmountSpecified
        {
            get { return this.LifeInsuranceFaceValueAmount != null; }
            set { }
        }
    
        [XmlElement("StockBondMutualFundShareCount", Order = 15)]
        public MISMOCount StockBondMutualFundShareCount { get; set; }
    
        [XmlIgnore]
        public bool StockBondMutualFundShareCountSpecified
        {
            get { return this.StockBondMutualFundShareCount != null; }
            set { }
        }
    
        [XmlElement("VerifiedIndicator", Order = 16)]
        public MISMOIndicator VerifiedIndicator { get; set; }
    
        [XmlIgnore]
        public bool VerifiedIndicatorSpecified
        {
            get { return this.VerifiedIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 17)]
        public ASSET_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
