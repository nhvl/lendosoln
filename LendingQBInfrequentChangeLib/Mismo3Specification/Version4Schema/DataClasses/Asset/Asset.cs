namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Schema;
    using System.Xml.Serialization;

    public class ASSET
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AssetDetailSpecified
                    || this.AssetDocumentationsSpecified
                    || this.AssetHolderSpecified
                    || this.OwnedPropertySpecified
                    || this.VerificationSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ASSET_DETAIL", Order = 0)]
        public ASSET_DETAIL AssetDetail { get; set; }
    
        [XmlIgnore]
        public bool AssetDetailSpecified
        {
            get { return this.AssetDetail != null && this.AssetDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("ASSET_DOCUMENTATIONS", Order = 1)]
        public ASSET_DOCUMENTATIONS AssetDocumentations { get; set; }
    
        [XmlIgnore]
        public bool AssetDocumentationsSpecified
        {
            get { return this.AssetDocumentations != null && this.AssetDocumentations.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("ASSET_HOLDER", Order = 2)]
        public ASSET_HOLDER AssetHolder { get; set; }
    
        [XmlIgnore]
        public bool AssetHolderSpecified
        {
            get { return this.AssetHolder != null && this.AssetHolder.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("OWNED_PROPERTY", Order = 3)]
        public OWNED_PROPERTY OwnedProperty { get; set; }
    
        [XmlIgnore]
        public bool OwnedPropertySpecified
        {
            get { return this.OwnedProperty != null && this.OwnedProperty.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("VERIFICATION", Order = 4)]
        public VERIFICATION Verification { get; set; }
    
        [XmlIgnore]
        public bool VerificationSpecified
        {
            get { return this.Verification != null && this.Verification.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public ASSET_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }

        [XmlAttribute("label", Form = XmlSchemaForm.Qualified, Namespace = Mismo3Constants.XlinkNamespace)]
        public string XlinkLabel { get; set; }

        [XmlIgnore]
        public bool XlinkLabelSpecified
        {
            get { return !string.IsNullOrEmpty(this.XlinkLabel); }
            set { }
        }
    }
}
