namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ASSET_SUMMARY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.GiftGrantTotalCashOrMarketValueAmountSpecified
                    || this.LiquidAssetTotalCashOrMarketValueAmountSpecified
                    || this.NonLiquidAssetTotalCashOrMarketValueAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("GiftGrantTotalCashOrMarketValueAmount", Order = 0)]
        public MISMOAmount GiftGrantTotalCashOrMarketValueAmount { get; set; }
    
        [XmlIgnore]
        public bool GiftGrantTotalCashOrMarketValueAmountSpecified
        {
            get { return this.GiftGrantTotalCashOrMarketValueAmount != null; }
            set { }
        }
    
        [XmlElement("LiquidAssetTotalCashOrMarketValueAmount", Order = 1)]
        public MISMOAmount LiquidAssetTotalCashOrMarketValueAmount { get; set; }
    
        [XmlIgnore]
        public bool LiquidAssetTotalCashOrMarketValueAmountSpecified
        {
            get { return this.LiquidAssetTotalCashOrMarketValueAmount != null; }
            set { }
        }
    
        [XmlElement("NonLiquidAssetTotalCashOrMarketValueAmount", Order = 2)]
        public MISMOAmount NonLiquidAssetTotalCashOrMarketValueAmount { get; set; }
    
        [XmlIgnore]
        public bool NonLiquidAssetTotalCashOrMarketValueAmountSpecified
        {
            get { return this.NonLiquidAssetTotalCashOrMarketValueAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public ASSET_SUMMARY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
