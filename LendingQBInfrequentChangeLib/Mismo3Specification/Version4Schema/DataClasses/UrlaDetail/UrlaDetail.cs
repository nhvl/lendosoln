namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class URLA_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AdditionalBorrowerAssetsConsideredIndicatorSpecified
                    || this.AdditionalBorrowerAssetsNotConsideredIndicatorSpecified
                    || this.AlterationsImprovementsAndRepairsAmountSpecified
                    || this.ApplicationSignedByLoanOriginatorDateSpecified
                    || this.ApplicationTakenMethodTypeSpecified
                    || this.ARMTypeDescriptionSpecified
                    || this.BorrowerPaidDiscountPointsTotalAmountSpecified
                    || this.BorrowerRequestedInterestRatePercentSpecified
                    || this.BorrowerRequestedLoanAmountSpecified
                    || this.EstimatedClosingCostsAmountSpecified
                    || this.LenderRegistrationIdentifierSpecified
                    || this.MIAndFundingFeeFinancedAmountSpecified
                    || this.MIAndFundingFeeTotalAmountSpecified
                    || this.PrepaidItemsEstimatedAmountSpecified
                    || this.PurchasePriceAmountSpecified
                    || this.PurchasePriceNetAmountSpecified
                    || this.RefinanceImprovementCostsAmountSpecified
                    || this.RefinanceImprovementsTypeSpecified
                    || this.RefinanceIncludingDebtsToBePaidOffAmountSpecified
                    || this.RefinanceProposedImprovementsDescriptionSpecified
                    || this.RefundableApplicationFeeIndicatorSpecified
                    || this.RequiredDepositIndicatorSpecified
                    || this.SellerPaidClosingCostsAmountSpecified
                    || this.URLAStatusDateSpecified
                    || this.URLAStatusTypeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AdditionalBorrowerAssetsConsideredIndicator", Order = 0)]
        public MISMOIndicator AdditionalBorrowerAssetsConsideredIndicator { get; set; }
    
        [XmlIgnore]
        public bool AdditionalBorrowerAssetsConsideredIndicatorSpecified
        {
            get { return this.AdditionalBorrowerAssetsConsideredIndicator != null; }
            set { }
        }
    
        [XmlElement("AdditionalBorrowerAssetsNotConsideredIndicator", Order = 1)]
        public MISMOIndicator AdditionalBorrowerAssetsNotConsideredIndicator { get; set; }
    
        [XmlIgnore]
        public bool AdditionalBorrowerAssetsNotConsideredIndicatorSpecified
        {
            get { return this.AdditionalBorrowerAssetsNotConsideredIndicator != null; }
            set { }
        }
    
        [XmlElement("AlterationsImprovementsAndRepairsAmount", Order = 2)]
        public MISMOAmount AlterationsImprovementsAndRepairsAmount { get; set; }
    
        [XmlIgnore]
        public bool AlterationsImprovementsAndRepairsAmountSpecified
        {
            get { return this.AlterationsImprovementsAndRepairsAmount != null; }
            set { }
        }
    
        [XmlElement("ApplicationSignedByLoanOriginatorDate", Order = 3)]
        public MISMODate ApplicationSignedByLoanOriginatorDate { get; set; }
    
        [XmlIgnore]
        public bool ApplicationSignedByLoanOriginatorDateSpecified
        {
            get { return this.ApplicationSignedByLoanOriginatorDate != null; }
            set { }
        }
    
        [XmlElement("ApplicationTakenMethodType", Order = 4)]
        public MISMOEnum<ApplicationTakenMethodBase> ApplicationTakenMethodType { get; set; }
    
        [XmlIgnore]
        public bool ApplicationTakenMethodTypeSpecified
        {
            get { return this.ApplicationTakenMethodType != null; }
            set { }
        }
    
        [XmlElement("ARMTypeDescription", Order = 5)]
        public MISMOString ARMTypeDescription { get; set; }
    
        [XmlIgnore]
        public bool ARMTypeDescriptionSpecified
        {
            get { return this.ARMTypeDescription != null; }
            set { }
        }
    
        [XmlElement("BorrowerPaidDiscountPointsTotalAmount", Order = 6)]
        public MISMOAmount BorrowerPaidDiscountPointsTotalAmount { get; set; }
    
        [XmlIgnore]
        public bool BorrowerPaidDiscountPointsTotalAmountSpecified
        {
            get { return this.BorrowerPaidDiscountPointsTotalAmount != null; }
            set { }
        }
    
        [XmlElement("BorrowerRequestedInterestRatePercent", Order = 7)]
        public MISMOPercent BorrowerRequestedInterestRatePercent { get; set; }
    
        [XmlIgnore]
        public bool BorrowerRequestedInterestRatePercentSpecified
        {
            get { return this.BorrowerRequestedInterestRatePercent != null; }
            set { }
        }
    
        [XmlElement("BorrowerRequestedLoanAmount", Order = 8)]
        public MISMOAmount BorrowerRequestedLoanAmount { get; set; }
    
        [XmlIgnore]
        public bool BorrowerRequestedLoanAmountSpecified
        {
            get { return this.BorrowerRequestedLoanAmount != null; }
            set { }
        }
    
        [XmlElement("EstimatedClosingCostsAmount", Order = 9)]
        public MISMOAmount EstimatedClosingCostsAmount { get; set; }
    
        [XmlIgnore]
        public bool EstimatedClosingCostsAmountSpecified
        {
            get { return this.EstimatedClosingCostsAmount != null; }
            set { }
        }
    
        [XmlElement("LenderRegistrationIdentifier", Order = 10)]
        public MISMOIdentifier LenderRegistrationIdentifier { get; set; }
    
        [XmlIgnore]
        public bool LenderRegistrationIdentifierSpecified
        {
            get { return this.LenderRegistrationIdentifier != null; }
            set { }
        }
    
        [XmlElement("MIAndFundingFeeFinancedAmount", Order = 11)]
        public MISMOAmount MIAndFundingFeeFinancedAmount { get; set; }
    
        [XmlIgnore]
        public bool MIAndFundingFeeFinancedAmountSpecified
        {
            get { return this.MIAndFundingFeeFinancedAmount != null; }
            set { }
        }
    
        [XmlElement("MIAndFundingFeeTotalAmount", Order = 12)]
        public MISMOAmount MIAndFundingFeeTotalAmount { get; set; }
    
        [XmlIgnore]
        public bool MIAndFundingFeeTotalAmountSpecified
        {
            get { return this.MIAndFundingFeeTotalAmount != null; }
            set { }
        }
    
        [XmlElement("PrepaidItemsEstimatedAmount", Order = 13)]
        public MISMOAmount PrepaidItemsEstimatedAmount { get; set; }
    
        [XmlIgnore]
        public bool PrepaidItemsEstimatedAmountSpecified
        {
            get { return this.PrepaidItemsEstimatedAmount != null; }
            set { }
        }
    
        [XmlElement("PurchasePriceAmount", Order = 14)]
        public MISMOAmount PurchasePriceAmount { get; set; }
    
        [XmlIgnore]
        public bool PurchasePriceAmountSpecified
        {
            get { return this.PurchasePriceAmount != null; }
            set { }
        }
    
        [XmlElement("PurchasePriceNetAmount", Order = 15)]
        public MISMOAmount PurchasePriceNetAmount { get; set; }
    
        [XmlIgnore]
        public bool PurchasePriceNetAmountSpecified
        {
            get { return this.PurchasePriceNetAmount != null; }
            set { }
        }
    
        [XmlElement("RefinanceImprovementCostsAmount", Order = 16)]
        public MISMOAmount RefinanceImprovementCostsAmount { get; set; }
    
        [XmlIgnore]
        public bool RefinanceImprovementCostsAmountSpecified
        {
            get { return this.RefinanceImprovementCostsAmount != null; }
            set { }
        }
    
        [XmlElement("RefinanceImprovementsType", Order = 17)]
        public MISMOEnum<RefinanceImprovementsBase> RefinanceImprovementsType { get; set; }
    
        [XmlIgnore]
        public bool RefinanceImprovementsTypeSpecified
        {
            get { return this.RefinanceImprovementsType != null; }
            set { }
        }
    
        [XmlElement("RefinanceIncludingDebtsToBePaidOffAmount", Order = 18)]
        public MISMOAmount RefinanceIncludingDebtsToBePaidOffAmount { get; set; }
    
        [XmlIgnore]
        public bool RefinanceIncludingDebtsToBePaidOffAmountSpecified
        {
            get { return this.RefinanceIncludingDebtsToBePaidOffAmount != null; }
            set { }
        }
    
        [XmlElement("RefinanceProposedImprovementsDescription", Order = 19)]
        public MISMOString RefinanceProposedImprovementsDescription { get; set; }
    
        [XmlIgnore]
        public bool RefinanceProposedImprovementsDescriptionSpecified
        {
            get { return this.RefinanceProposedImprovementsDescription != null; }
            set { }
        }
    
        [XmlElement("RefundableApplicationFeeIndicator", Order = 20)]
        public MISMOIndicator RefundableApplicationFeeIndicator { get; set; }
    
        [XmlIgnore]
        public bool RefundableApplicationFeeIndicatorSpecified
        {
            get { return this.RefundableApplicationFeeIndicator != null; }
            set { }
        }
    
        [XmlElement("RequiredDepositIndicator", Order = 21)]
        public MISMOIndicator RequiredDepositIndicator { get; set; }
    
        [XmlIgnore]
        public bool RequiredDepositIndicatorSpecified
        {
            get { return this.RequiredDepositIndicator != null; }
            set { }
        }
    
        [XmlElement("SellerPaidClosingCostsAmount", Order = 22)]
        public MISMOAmount SellerPaidClosingCostsAmount { get; set; }
    
        [XmlIgnore]
        public bool SellerPaidClosingCostsAmountSpecified
        {
            get { return this.SellerPaidClosingCostsAmount != null; }
            set { }
        }
    
        [XmlElement("URLAStatusDate", Order = 23)]
        public MISMODate URLAStatusDate { get; set; }
    
        [XmlIgnore]
        public bool URLAStatusDateSpecified
        {
            get { return this.URLAStatusDate != null; }
            set { }
        }
    
        [XmlElement("URLAStatusType", Order = 24)]
        public MISMOEnum<URLAStatusBase> URLAStatusType { get; set; }
    
        [XmlIgnore]
        public bool URLAStatusTypeSpecified
        {
            get { return this.URLAStatusType != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 25)]
        public URLA_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
