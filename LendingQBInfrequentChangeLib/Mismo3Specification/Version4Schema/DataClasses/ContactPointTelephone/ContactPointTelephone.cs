namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    public class CONTACT_POINT_TELEPHONE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {

            get
            {
                if (Mismo3Utilities.ExceedsThreshold(1,
                    new List<bool> {
                        this.ContactPointFaxExtensionValueSpecified || this.ContactPointFaxValueSpecified,
                        this.ContactPointTelephoneExtensionValueSpecified || this.ContactPointTelephoneValueSpecified }))
                {
                    throw new System.Xml.Schema.XmlSchemaValidationException(
                        Mismo3Utilities.GetXSDChoiceExceptionMessage(
                        "CONTACT_POINT_TELEPHONE",
                        new List<string> { "ContactPointFaxExtensionValue and/or ContactPointFaxValue", "ContactPointTelephoneExtensionValue and/or ContactPointTelephoneValue" }));
                }

                return this.ContactPointFaxExtensionValueSpecified
                    || this.ContactPointFaxValueSpecified
                    || this.ContactPointTelephoneExtensionValueSpecified
                    || this.ContactPointTelephoneValueSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ContactPointFaxExtensionValue", Order = 0)]
        public MISMONumericString ContactPointFaxExtensionValue { get; set; }
    
        [XmlIgnore]
        public bool ContactPointFaxExtensionValueSpecified
        {
            get { return this.ContactPointFaxExtensionValue != null; }
            set { }
        }
    
        [XmlElement("ContactPointFaxValue", Order = 1)]
        public MISMONumericString ContactPointFaxValue { get; set; }
    
        [XmlIgnore]
        public bool ContactPointFaxValueSpecified
        {
            get { return this.ContactPointFaxValue != null; }
            set { }
        }
    
        [XmlElement("ContactPointTelephoneExtensionValue", Order = 2)]
        public MISMONumericString ContactPointTelephoneExtensionValue { get; set; }
    
        [XmlIgnore]
        public bool ContactPointTelephoneExtensionValueSpecified
        {
            get { return this.ContactPointTelephoneExtensionValue != null; }
            set { }
        }
    
        [XmlElement("ContactPointTelephoneValue", Order = 3)]
        public MISMONumericString ContactPointTelephoneValue { get; set; }
    
        [XmlIgnore]
        public bool ContactPointTelephoneValueSpecified
        {
            get { return this.ContactPointTelephoneValue != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public CONTACT_POINT_TELEPHONE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
