namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class SITE_LOCATIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SiteLocationListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SITE_LOCATION", Order = 0)]
        public List<SITE_LOCATION> SiteLocationList { get; set; } = new List<SITE_LOCATION>();
    
        [XmlIgnore]
        public bool SiteLocationListSpecified
        {
            get { return this.SiteLocationList != null && this.SiteLocationList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public SITE_LOCATIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
