namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SITE_LOCATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LocationTypeSpecified
                    || this.LocationTypeOtherDescriptionSpecified
                    || this.WalkScoreValueSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LocationType", Order = 0)]
        public MISMOEnum<LocationBase> LocationType { get; set; }
    
        [XmlIgnore]
        public bool LocationTypeSpecified
        {
            get { return this.LocationType != null; }
            set { }
        }
    
        [XmlElement("LocationTypeOtherDescription", Order = 1)]
        public MISMOString LocationTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool LocationTypeOtherDescriptionSpecified
        {
            get { return this.LocationTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("WalkScoreValue", Order = 2)]
        public MISMOValue WalkScoreValue { get; set; }
    
        [XmlIgnore]
        public bool WalkScoreValueSpecified
        {
            get { return this.WalkScoreValue != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public SITE_LOCATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
