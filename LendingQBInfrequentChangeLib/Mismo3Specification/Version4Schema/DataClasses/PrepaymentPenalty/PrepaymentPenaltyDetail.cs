namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PREPAYMENT_PENALTY_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PrepaymentPenaltyWaivedAmountSpecified
                    || this.PrepaymentPenaltyWaivedReasonDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PrepaymentPenaltyWaivedAmount", Order = 0)]
        public MISMOAmount PrepaymentPenaltyWaivedAmount { get; set; }
    
        [XmlIgnore]
        public bool PrepaymentPenaltyWaivedAmountSpecified
        {
            get { return this.PrepaymentPenaltyWaivedAmount != null; }
            set { }
        }
    
        [XmlElement("PrepaymentPenaltyWaivedReasonDescription", Order = 1)]
        public MISMOString PrepaymentPenaltyWaivedReasonDescription { get; set; }
    
        [XmlIgnore]
        public bool PrepaymentPenaltyWaivedReasonDescriptionSpecified
        {
            get { return this.PrepaymentPenaltyWaivedReasonDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public PREPAYMENT_PENALTY_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
