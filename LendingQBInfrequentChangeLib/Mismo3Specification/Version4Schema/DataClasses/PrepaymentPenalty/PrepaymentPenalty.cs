namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PREPAYMENT_PENALTY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PrepaymentPenaltyDetailSpecified
                    || this.PrepaymentPenaltyLifetimeRuleSpecified
                    || this.PrepaymentPenaltyPerChangeRulesSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PREPAYMENT_PENALTY_DETAIL", Order = 0)]
        public PREPAYMENT_PENALTY_DETAIL PrepaymentPenaltyDetail { get; set; }
    
        [XmlIgnore]
        public bool PrepaymentPenaltyDetailSpecified
        {
            get { return this.PrepaymentPenaltyDetail != null && this.PrepaymentPenaltyDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PREPAYMENT_PENALTY_LIFETIME_RULE", Order = 1)]
        public PREPAYMENT_PENALTY_LIFETIME_RULE PrepaymentPenaltyLifetimeRule { get; set; }
    
        [XmlIgnore]
        public bool PrepaymentPenaltyLifetimeRuleSpecified
        {
            get { return this.PrepaymentPenaltyLifetimeRule != null && this.PrepaymentPenaltyLifetimeRule.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PREPAYMENT_PENALTY_PER_CHANGE_RULES", Order = 2)]
        public PREPAYMENT_PENALTY_PER_CHANGE_RULES PrepaymentPenaltyPerChangeRules { get; set; }
    
        [XmlIgnore]
        public bool PrepaymentPenaltyPerChangeRulesSpecified
        {
            get { return this.PrepaymentPenaltyPerChangeRules != null && this.PrepaymentPenaltyPerChangeRules.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public PREPAYMENT_PENALTY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
