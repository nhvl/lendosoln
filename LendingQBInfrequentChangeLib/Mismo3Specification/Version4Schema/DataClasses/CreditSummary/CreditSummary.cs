namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CREDIT_SUMMARY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditSummaryDataSetsSpecified
                    || this.CreditSummaryDetailSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CREDIT_SUMMARY_DATA_SETS", Order = 0)]
        public CREDIT_SUMMARY_DATA_SETS CreditSummaryDataSets { get; set; }
    
        [XmlIgnore]
        public bool CreditSummaryDataSetsSpecified
        {
            get { return this.CreditSummaryDataSets != null && this.CreditSummaryDataSets.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_SUMMARY_DETAIL", Order = 1)]
        public CREDIT_SUMMARY_DETAIL CreditSummaryDetail { get; set; }
    
        [XmlIgnore]
        public bool CreditSummaryDetailSpecified
        {
            get { return this.CreditSummaryDetail != null && this.CreditSummaryDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public CREDIT_SUMMARY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
