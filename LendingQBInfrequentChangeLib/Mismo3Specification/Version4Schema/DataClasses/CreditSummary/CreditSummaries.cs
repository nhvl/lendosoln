namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class CREDIT_SUMMARIES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditSummaryListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CREDIT_SUMMARY", Order = 0)]
        public List<CREDIT_SUMMARY> CreditSummaryList { get; set; } = new List<CREDIT_SUMMARY>();
    
        [XmlIgnore]
        public bool CreditSummaryListSpecified
        {
            get { return this.CreditSummaryList != null && this.CreditSummaryList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public CREDIT_SUMMARIES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
