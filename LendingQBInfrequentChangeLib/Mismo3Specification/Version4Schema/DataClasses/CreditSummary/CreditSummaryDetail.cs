namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CREDIT_SUMMARY_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditSummaryNameSpecified
                    || this.CreditSummaryTextSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CreditSummaryName", Order = 0)]
        public MISMOString CreditSummaryName { get; set; }
    
        [XmlIgnore]
        public bool CreditSummaryNameSpecified
        {
            get { return this.CreditSummaryName != null; }
            set { }
        }
    
        [XmlElement("CreditSummaryText", Order = 1)]
        public MISMOString CreditSummaryText { get; set; }
    
        [XmlIgnore]
        public bool CreditSummaryTextSpecified
        {
            get { return this.CreditSummaryText != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public CREDIT_SUMMARY_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
