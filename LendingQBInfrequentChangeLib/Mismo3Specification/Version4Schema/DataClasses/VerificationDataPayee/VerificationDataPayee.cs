namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class VERIFICATION_DATA_PAYEE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PayeeTotalCountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PayeeTotalCount", Order = 0)]
        public MISMOCount PayeeTotalCount { get; set; }
    
        [XmlIgnore]
        public bool PayeeTotalCountSpecified
        {
            get { return this.PayeeTotalCount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public VERIFICATION_DATA_PAYEE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
