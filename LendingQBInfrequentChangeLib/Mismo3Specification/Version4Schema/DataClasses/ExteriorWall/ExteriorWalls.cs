namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class EXTERIOR_WALLS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExteriorWallListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("EXTERIOR_WALL", Order = 0)]
        public List<EXTERIOR_WALL> ExteriorWallList { get; set; } = new List<EXTERIOR_WALL>();
    
        [XmlIgnore]
        public bool ExteriorWallListSpecified
        {
            get { return this.ExteriorWallList != null && this.ExteriorWallList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public EXTERIOR_WALLS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
