namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class HOUSING_COUNSELING_AGENCY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CounselingTopicsSpecified
                    || this.HousingCounselingAgencyDetailSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("COUNSELING_TOPICS", Order = 0)]
        public COUNSELING_TOPICS CounselingTopics { get; set; }
    
        [XmlIgnore]
        public bool CounselingTopicsSpecified
        {
            get { return this.CounselingTopics != null && this.CounselingTopics.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("HOUSING_COUNSELING_AGENCY_DETAIL", Order = 1)]
        public HOUSING_COUNSELING_AGENCY_DETAIL HousingCounselingAgencyDetail { get; set; }
    
        [XmlIgnore]
        public bool HousingCounselingAgencyDetailSpecified
        {
            get { return this.HousingCounselingAgencyDetail != null && this.HousingCounselingAgencyDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public HOUSING_COUNSELING_AGENCY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
