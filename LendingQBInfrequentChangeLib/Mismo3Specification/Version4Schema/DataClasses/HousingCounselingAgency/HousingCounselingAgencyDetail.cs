namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class HOUSING_COUNSELING_AGENCY_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.HousingCounselingAgencySelectedIndicatorSpecified
                    || this.HousingCounselingDistanceInMilesCountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("HousingCounselingAgencySelectedIndicator", Order = 0)]
        public MISMOIndicator HousingCounselingAgencySelectedIndicator { get; set; }
    
        [XmlIgnore]
        public bool HousingCounselingAgencySelectedIndicatorSpecified
        {
            get { return this.HousingCounselingAgencySelectedIndicator != null; }
            set { }
        }
    
        [XmlElement("HousingCounselingDistanceInMilesCount", Order = 1)]
        public MISMOCount HousingCounselingDistanceInMilesCount { get; set; }
    
        [XmlIgnore]
        public bool HousingCounselingDistanceInMilesCountSpecified
        {
            get { return this.HousingCounselingDistanceInMilesCount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public HOUSING_COUNSELING_AGENCY_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
