namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class COST_APPROACH_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CostAnalysisCommentDescriptionSpecified
                    || this.CostAnalysisTypeSpecified
                    || this.CostAnalysisTypeOtherDescriptionSpecified
                    || this.CostServiceQualityRatingDescriptionSpecified
                    || this.EstimatedRemainingEconomicLifeYearsCountSpecified
                    || this.ManufacturedHomeInstallationCostAmountSpecified
                    || this.NewImprovementCostLocalizedAmountSpecified
                    || this.NewImprovementCostMultiplierPercentSpecified
                    || this.NewImprovementDepreciatedCostAmountSpecified
                    || this.NewImprovementTotalCostAmountSpecified
                    || this.SiteEstimatedValueAmountSpecified
                    || this.SiteEstimatedValueCommentDescriptionSpecified
                    || this.SiteOtherImprovementsAsIsAmountSpecified
                    || this.ValueIndicatedByCostApproachAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CostAnalysisCommentDescription", Order = 0)]
        public MISMOString CostAnalysisCommentDescription { get; set; }
    
        [XmlIgnore]
        public bool CostAnalysisCommentDescriptionSpecified
        {
            get { return this.CostAnalysisCommentDescription != null; }
            set { }
        }
    
        [XmlElement("CostAnalysisType", Order = 1)]
        public MISMOEnum<CostAnalysisBase> CostAnalysisType { get; set; }
    
        [XmlIgnore]
        public bool CostAnalysisTypeSpecified
        {
            get { return this.CostAnalysisType != null; }
            set { }
        }
    
        [XmlElement("CostAnalysisTypeOtherDescription", Order = 2)]
        public MISMOString CostAnalysisTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CostAnalysisTypeOtherDescriptionSpecified
        {
            get { return this.CostAnalysisTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("CostServiceQualityRatingDescription", Order = 3)]
        public MISMOString CostServiceQualityRatingDescription { get; set; }
    
        [XmlIgnore]
        public bool CostServiceQualityRatingDescriptionSpecified
        {
            get { return this.CostServiceQualityRatingDescription != null; }
            set { }
        }
    
        [XmlElement("EstimatedRemainingEconomicLifeYearsCount", Order = 4)]
        public MISMOCount EstimatedRemainingEconomicLifeYearsCount { get; set; }
    
        [XmlIgnore]
        public bool EstimatedRemainingEconomicLifeYearsCountSpecified
        {
            get { return this.EstimatedRemainingEconomicLifeYearsCount != null; }
            set { }
        }
    
        [XmlElement("ManufacturedHomeInstallationCostAmount", Order = 5)]
        public MISMOAmount ManufacturedHomeInstallationCostAmount { get; set; }
    
        [XmlIgnore]
        public bool ManufacturedHomeInstallationCostAmountSpecified
        {
            get { return this.ManufacturedHomeInstallationCostAmount != null; }
            set { }
        }
    
        [XmlElement("NewImprovementCostLocalizedAmount", Order = 6)]
        public MISMOAmount NewImprovementCostLocalizedAmount { get; set; }
    
        [XmlIgnore]
        public bool NewImprovementCostLocalizedAmountSpecified
        {
            get { return this.NewImprovementCostLocalizedAmount != null; }
            set { }
        }
    
        [XmlElement("NewImprovementCostMultiplierPercent", Order = 7)]
        public MISMOPercent NewImprovementCostMultiplierPercent { get; set; }
    
        [XmlIgnore]
        public bool NewImprovementCostMultiplierPercentSpecified
        {
            get { return this.NewImprovementCostMultiplierPercent != null; }
            set { }
        }
    
        [XmlElement("NewImprovementDepreciatedCostAmount", Order = 8)]
        public MISMOAmount NewImprovementDepreciatedCostAmount { get; set; }
    
        [XmlIgnore]
        public bool NewImprovementDepreciatedCostAmountSpecified
        {
            get { return this.NewImprovementDepreciatedCostAmount != null; }
            set { }
        }
    
        [XmlElement("NewImprovementTotalCostAmount", Order = 9)]
        public MISMOAmount NewImprovementTotalCostAmount { get; set; }
    
        [XmlIgnore]
        public bool NewImprovementTotalCostAmountSpecified
        {
            get { return this.NewImprovementTotalCostAmount != null; }
            set { }
        }
    
        [XmlElement("SiteEstimatedValueAmount", Order = 10)]
        public MISMOAmount SiteEstimatedValueAmount { get; set; }
    
        [XmlIgnore]
        public bool SiteEstimatedValueAmountSpecified
        {
            get { return this.SiteEstimatedValueAmount != null; }
            set { }
        }
    
        [XmlElement("SiteEstimatedValueCommentDescription", Order = 11)]
        public MISMOString SiteEstimatedValueCommentDescription { get; set; }
    
        [XmlIgnore]
        public bool SiteEstimatedValueCommentDescriptionSpecified
        {
            get { return this.SiteEstimatedValueCommentDescription != null; }
            set { }
        }
    
        [XmlElement("SiteOtherImprovementsAsIsAmount", Order = 12)]
        public MISMOAmount SiteOtherImprovementsAsIsAmount { get; set; }
    
        [XmlIgnore]
        public bool SiteOtherImprovementsAsIsAmountSpecified
        {
            get { return this.SiteOtherImprovementsAsIsAmount != null; }
            set { }
        }
    
        [XmlElement("ValueIndicatedByCostApproachAmount", Order = 13)]
        public MISMOAmount ValueIndicatedByCostApproachAmount { get; set; }
    
        [XmlIgnore]
        public bool ValueIndicatedByCostApproachAmountSpecified
        {
            get { return this.ValueIndicatedByCostApproachAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 14)]
        public COST_APPROACH_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
