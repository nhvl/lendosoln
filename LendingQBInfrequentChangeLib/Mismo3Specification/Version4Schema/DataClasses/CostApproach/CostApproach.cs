namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class COST_APPROACH
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CostApproachDetailSpecified
                    || this.DepreciationSpecified
                    || this.NewImprovementsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("COST_APPROACH_DETAIL", Order = 0)]
        public COST_APPROACH_DETAIL CostApproachDetail { get; set; }
    
        [XmlIgnore]
        public bool CostApproachDetailSpecified
        {
            get { return this.CostApproachDetail != null && this.CostApproachDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("DEPRECIATION", Order = 1)]
        public DEPRECIATION Depreciation { get; set; }
    
        [XmlIgnore]
        public bool DepreciationSpecified
        {
            get { return this.Depreciation != null && this.Depreciation.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("NEW_IMPROVEMENTS", Order = 2)]
        public NEW_IMPROVEMENTS NewImprovements { get; set; }
    
        [XmlIgnore]
        public bool NewImprovementsSpecified
        {
            get { return this.NewImprovements != null && this.NewImprovements.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public COST_APPROACH_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
