namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class INSPECTION_SERVICE_RESPONSE_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.InspectorFileIdentifierSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("InspectorFileIdentifier", Order = 0)]
        public MISMOIdentifier InspectorFileIdentifier { get; set; }
    
        [XmlIgnore]
        public bool InspectorFileIdentifierSpecified
        {
            get { return this.InspectorFileIdentifier != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public INSPECTION_SERVICE_RESPONSE_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
