namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class INSPECTION_SERVICE_RESPONSE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.InspectionServiceResponseDetailSpecified
                    || this.PropertiesSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("INSPECTION_SERVICE_RESPONSE_DETAIL", Order = 0)]
        public INSPECTION_SERVICE_RESPONSE_DETAIL InspectionServiceResponseDetail { get; set; }
    
        [XmlIgnore]
        public bool InspectionServiceResponseDetailSpecified
        {
            get { return this.InspectionServiceResponseDetail != null && this.InspectionServiceResponseDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PROPERTIES", Order = 1)]
        public PROPERTIES Properties { get; set; }
    
        [XmlIgnore]
        public bool PropertiesSpecified
        {
            get { return this.Properties != null && this.Properties.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public INSPECTION_SERVICE_RESPONSE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
