namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SELECTED_SERVICE_PROVIDER_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SelectedServiceProviderNatureOfRelationshipDescriptionSpecified
                    || this.SelectedServiceProviderOwnershipInterestPercentSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SelectedServiceProviderNatureOfRelationshipDescription", Order = 0)]
        public MISMOString SelectedServiceProviderNatureOfRelationshipDescription { get; set; }
    
        [XmlIgnore]
        public bool SelectedServiceProviderNatureOfRelationshipDescriptionSpecified
        {
            get { return this.SelectedServiceProviderNatureOfRelationshipDescription != null; }
            set { }
        }
    
        [XmlElement("SelectedServiceProviderOwnershipInterestPercent", Order = 1)]
        public MISMOPercent SelectedServiceProviderOwnershipInterestPercent { get; set; }
    
        [XmlIgnore]
        public bool SelectedServiceProviderOwnershipInterestPercentSpecified
        {
            get { return this.SelectedServiceProviderOwnershipInterestPercent != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public SELECTED_SERVICE_PROVIDER_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
