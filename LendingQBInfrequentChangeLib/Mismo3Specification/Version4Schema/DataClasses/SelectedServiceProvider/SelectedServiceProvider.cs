namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    public class SELECTED_SERVICE_PROVIDER
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                if (Mismo3Utilities.ExceedsThreshold(1,
                    new List<bool> { this.IndividualSpecified, this.LegalEntitySpecified }))
                {
                    throw new System.Xml.Schema.XmlSchemaValidationException(
                        Mismo3Utilities.GetXSDChoiceExceptionMessage(
                        "SELECTED_SERVICE_PROVIDER",
                        new List<string> { "INDIVIDUAL", "LEGAL_ENTITY" }));
                }

                return this.IndividualSpecified
                    || this.LegalEntitySpecified
                    || this.AddressSpecified
                    || this.SelectedServiceProviderDetailSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("INDIVIDUAL", Order = 0)]
        public INDIVIDUAL Individual { get; set; }
    
        [XmlIgnore]
        public bool IndividualSpecified
        {
            get { return this.Individual != null && this.Individual.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("LEGAL_ENTITY", Order = 1)]
        public LEGAL_ENTITY LegalEntity { get; set; }
    
        [XmlIgnore]
        public bool LegalEntitySpecified
        {
            get { return this.LegalEntity != null && this.LegalEntity.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("ADDRESS", Order = 2)]
        public ADDRESS Address { get; set; }
    
        [XmlIgnore]
        public bool AddressSpecified
        {
            get { return this.Address != null && this.Address.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SELECTED_SERVICE_PROVIDER_DETAIL", Order = 3)]
        public SELECTED_SERVICE_PROVIDER_DETAIL SelectedServiceProviderDetail { get; set; }
    
        [XmlIgnore]
        public bool SelectedServiceProviderDetailSpecified
        {
            get { return this.SelectedServiceProviderDetail != null && this.SelectedServiceProviderDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public SELECTED_SERVICE_PROVIDER_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
