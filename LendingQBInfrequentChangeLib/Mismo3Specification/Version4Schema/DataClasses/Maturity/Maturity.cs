namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MATURITY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MaturityOccurrencesSpecified
                    || this.MaturityRuleSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MATURITY_OCCURRENCES", Order = 0)]
        public MATURITY_OCCURRENCES MaturityOccurrences { get; set; }
    
        [XmlIgnore]
        public bool MaturityOccurrencesSpecified
        {
            get { return this.MaturityOccurrences != null && this.MaturityOccurrences.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("MATURITY_RULE", Order = 1)]
        public MATURITY_RULE MaturityRule { get; set; }
    
        [XmlIgnore]
        public bool MaturityRuleSpecified
        {
            get { return this.MaturityRule != null && this.MaturityRule.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public MATURITY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
