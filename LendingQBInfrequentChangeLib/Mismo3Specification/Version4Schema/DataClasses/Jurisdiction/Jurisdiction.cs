namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class JURISDICTION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AssignedJudgeNameSpecified
                    || this.CaseFilingIdentifierSpecified
                    || this.CountyCodeSpecified
                    || this.CountyNameSpecified
                    || this.CourtNameSpecified
                    || this.StateCodeSpecified
                    || this.StateNameSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AssignedJudgeName", Order = 0)]
        public MISMOString AssignedJudgeName { get; set; }
    
        [XmlIgnore]
        public bool AssignedJudgeNameSpecified
        {
            get { return this.AssignedJudgeName != null; }
            set { }
        }
    
        [XmlElement("CaseFilingIdentifier", Order = 1)]
        public MISMOIdentifier CaseFilingIdentifier { get; set; }
    
        [XmlIgnore]
        public bool CaseFilingIdentifierSpecified
        {
            get { return this.CaseFilingIdentifier != null; }
            set { }
        }
    
        [XmlElement("CountyCode", Order = 2)]
        public MISMOCode CountyCode { get; set; }
    
        [XmlIgnore]
        public bool CountyCodeSpecified
        {
            get { return this.CountyCode != null; }
            set { }
        }
    
        [XmlElement("CountyName", Order = 3)]
        public MISMOString CountyName { get; set; }
    
        [XmlIgnore]
        public bool CountyNameSpecified
        {
            get { return this.CountyName != null; }
            set { }
        }
    
        [XmlElement("CourtName", Order = 4)]
        public MISMOString CourtName { get; set; }
    
        [XmlIgnore]
        public bool CourtNameSpecified
        {
            get { return this.CourtName != null; }
            set { }
        }
    
        [XmlElement("StateCode", Order = 5)]
        public MISMOCode StateCode { get; set; }
    
        [XmlIgnore]
        public bool StateCodeSpecified
        {
            get { return this.StateCode != null; }
            set { }
        }
    
        [XmlElement("StateName", Order = 6)]
        public MISMOString StateName { get; set; }
    
        [XmlIgnore]
        public bool StateNameSpecified
        {
            get { return this.StateName != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 7)]
        public JURISDICTION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
