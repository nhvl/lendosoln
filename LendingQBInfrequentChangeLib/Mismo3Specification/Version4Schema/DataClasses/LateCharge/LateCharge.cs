namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class LATE_CHARGE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LateChargeOccurrencesSpecified
                    || this.LateChargeRuleSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LATE_CHARGE_OCCURRENCES", Order = 0)]
        public LATE_CHARGE_OCCURRENCES LateChargeOccurrences { get; set; }
    
        [XmlIgnore]
        public bool LateChargeOccurrencesSpecified
        {
            get { return this.LateChargeOccurrences != null && this.LateChargeOccurrences.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("LATE_CHARGE_RULE", Order = 1)]
        public LATE_CHARGE_RULE LateChargeRule { get; set; }
    
        [XmlIgnore]
        public bool LateChargeRuleSpecified
        {
            get { return this.LateChargeRule != null && this.LateChargeRule.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public LATE_CHARGE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
