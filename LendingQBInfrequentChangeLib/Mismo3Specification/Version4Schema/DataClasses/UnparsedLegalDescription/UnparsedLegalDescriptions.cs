namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class UNPARSED_LEGAL_DESCRIPTIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.UnparsedLegalDescriptionListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("UNPARSED_LEGAL_DESCRIPTION", Order = 0)]
        public List<UNPARSED_LEGAL_DESCRIPTION> UnparsedLegalDescriptionList { get; set; } = new List<UNPARSED_LEGAL_DESCRIPTION>();
    
        [XmlIgnore]
        public bool UnparsedLegalDescriptionListSpecified
        {
            get { return this.UnparsedLegalDescriptionList != null && this.UnparsedLegalDescriptionList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public UNPARSED_LEGAL_DESCRIPTIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
