namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class UNPARSED_LEGAL_DESCRIPTION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.UnparsedLegalDescriptionSpecified
                    || this.UnparsedLegalDescriptionTypeSpecified
                    || this.UnparsedLegalDescriptionTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("UnparsedLegalDescription", Order = 0)]
        public MISMOString UnparsedLegalDescription { get; set; }
    
        [XmlIgnore]
        public bool UnparsedLegalDescriptionSpecified
        {
            get { return this.UnparsedLegalDescription != null; }
            set { }
        }
    
        [XmlElement("UnparsedLegalDescriptionType", Order = 1)]
        public MISMOEnum<UnparsedLegalDescriptionBase> UnparsedLegalDescriptionType { get; set; }
    
        [XmlIgnore]
        public bool UnparsedLegalDescriptionTypeSpecified
        {
            get { return this.UnparsedLegalDescriptionType != null; }
            set { }
        }
    
        [XmlElement("UnparsedLegalDescriptionTypeOtherDescription", Order = 2)]
        public MISMOString UnparsedLegalDescriptionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool UnparsedLegalDescriptionTypeOtherDescriptionSpecified
        {
            get { return this.UnparsedLegalDescriptionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public UNPARSED_LEGAL_DESCRIPTION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
