namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PRINCIPAL_AND_INTEREST_PAYMENT_PER_CHANGE_ADJUSTMENT_RULE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AdjustmentRuleTypeSpecified
                    || this.NoPrincipalAndInterestPaymentCapsFeatureDescriptionSpecified
                    || this.PaymentsBetweenPrincipalAndInterestPaymentChangesCountSpecified
                    || this.PerChangeMaximumPrincipalAndInterestPaymentAmountSpecified
                    || this.PerChangeMaximumPrincipalAndInterestPaymentDecreaseAmountSpecified
                    || this.PerChangeMaximumPrincipalAndInterestPaymentDecreasePercentSpecified
                    || this.PerChangeMaximumPrincipalAndInterestPaymentIncreaseAmountSpecified
                    || this.PerChangeMaximumPrincipalAndInterestPaymentIncreasePercentSpecified
                    || this.PerChangeMinimumPrincipalAndInterestPaymentAmountSpecified
                    || this.PerChangePrincipalAndInterestCalculationTypeSpecified
                    || this.PerChangePrincipalAndInterestCalculationTypeOtherDescriptionSpecified
                    || this.PerChangePrincipalAndInterestPaymentAdjustmentAmountSpecified
                    || this.PerChangePrincipalAndInterestPaymentAdjustmentEffectiveDateSpecified
                    || this.PerChangePrincipalAndInterestPaymentAdjustmentEffectiveMonthsCountSpecified
                    || this.PerChangePrincipalAndInterestPaymentAdjustmentFrequencyMonthsCountSpecified
                    || this.PerChangePrincipalAndInterestPaymentAdjustmentPercentSpecified
                    || this.PerChangePrincipalAndInterestPaymentMinimumDecreaseRatePercentSpecified
                    || this.PerChangePrincipalAndInterestPaymentMinimumIncreaseRatePercentSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AdjustmentRuleType", Order = 0)]
        public MISMOEnum<AdjustmentRuleBase> AdjustmentRuleType { get; set; }
    
        [XmlIgnore]
        public bool AdjustmentRuleTypeSpecified
        {
            get { return this.AdjustmentRuleType != null; }
            set { }
        }
    
        [XmlElement("NoPrincipalAndInterestPaymentCapsFeatureDescription", Order = 1)]
        public MISMOString NoPrincipalAndInterestPaymentCapsFeatureDescription { get; set; }
    
        [XmlIgnore]
        public bool NoPrincipalAndInterestPaymentCapsFeatureDescriptionSpecified
        {
            get { return this.NoPrincipalAndInterestPaymentCapsFeatureDescription != null; }
            set { }
        }
    
        [XmlElement("PaymentsBetweenPrincipalAndInterestPaymentChangesCount", Order = 2)]
        public MISMOCount PaymentsBetweenPrincipalAndInterestPaymentChangesCount { get; set; }
    
        [XmlIgnore]
        public bool PaymentsBetweenPrincipalAndInterestPaymentChangesCountSpecified
        {
            get { return this.PaymentsBetweenPrincipalAndInterestPaymentChangesCount != null; }
            set { }
        }
    
        [XmlElement("PerChangeMaximumPrincipalAndInterestPaymentAmount", Order = 3)]
        public MISMOAmount PerChangeMaximumPrincipalAndInterestPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool PerChangeMaximumPrincipalAndInterestPaymentAmountSpecified
        {
            get { return this.PerChangeMaximumPrincipalAndInterestPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("PerChangeMaximumPrincipalAndInterestPaymentDecreaseAmount", Order = 4)]
        public MISMOAmount PerChangeMaximumPrincipalAndInterestPaymentDecreaseAmount { get; set; }
    
        [XmlIgnore]
        public bool PerChangeMaximumPrincipalAndInterestPaymentDecreaseAmountSpecified
        {
            get { return this.PerChangeMaximumPrincipalAndInterestPaymentDecreaseAmount != null; }
            set { }
        }
    
        [XmlElement("PerChangeMaximumPrincipalAndInterestPaymentDecreasePercent", Order = 5)]
        public MISMOPercent PerChangeMaximumPrincipalAndInterestPaymentDecreasePercent { get; set; }
    
        [XmlIgnore]
        public bool PerChangeMaximumPrincipalAndInterestPaymentDecreasePercentSpecified
        {
            get { return this.PerChangeMaximumPrincipalAndInterestPaymentDecreasePercent != null; }
            set { }
        }
    
        [XmlElement("PerChangeMaximumPrincipalAndInterestPaymentIncreaseAmount", Order = 6)]
        public MISMOAmount PerChangeMaximumPrincipalAndInterestPaymentIncreaseAmount { get; set; }
    
        [XmlIgnore]
        public bool PerChangeMaximumPrincipalAndInterestPaymentIncreaseAmountSpecified
        {
            get { return this.PerChangeMaximumPrincipalAndInterestPaymentIncreaseAmount != null; }
            set { }
        }
    
        [XmlElement("PerChangeMaximumPrincipalAndInterestPaymentIncreasePercent", Order = 7)]
        public MISMOPercent PerChangeMaximumPrincipalAndInterestPaymentIncreasePercent { get; set; }
    
        [XmlIgnore]
        public bool PerChangeMaximumPrincipalAndInterestPaymentIncreasePercentSpecified
        {
            get { return this.PerChangeMaximumPrincipalAndInterestPaymentIncreasePercent != null; }
            set { }
        }
    
        [XmlElement("PerChangeMinimumPrincipalAndInterestPaymentAmount", Order = 8)]
        public MISMOAmount PerChangeMinimumPrincipalAndInterestPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool PerChangeMinimumPrincipalAndInterestPaymentAmountSpecified
        {
            get { return this.PerChangeMinimumPrincipalAndInterestPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("PerChangePrincipalAndInterestCalculationType", Order = 9)]
        public MISMOEnum<PerChangePrincipalAndInterestCalculationBase> PerChangePrincipalAndInterestCalculationType { get; set; }
    
        [XmlIgnore]
        public bool PerChangePrincipalAndInterestCalculationTypeSpecified
        {
            get { return this.PerChangePrincipalAndInterestCalculationType != null; }
            set { }
        }
    
        [XmlElement("PerChangePrincipalAndInterestCalculationTypeOtherDescription", Order = 10)]
        public MISMOString PerChangePrincipalAndInterestCalculationTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PerChangePrincipalAndInterestCalculationTypeOtherDescriptionSpecified
        {
            get { return this.PerChangePrincipalAndInterestCalculationTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PerChangePrincipalAndInterestPaymentAdjustmentAmount", Order = 11)]
        public MISMOAmount PerChangePrincipalAndInterestPaymentAdjustmentAmount { get; set; }
    
        [XmlIgnore]
        public bool PerChangePrincipalAndInterestPaymentAdjustmentAmountSpecified
        {
            get { return this.PerChangePrincipalAndInterestPaymentAdjustmentAmount != null; }
            set { }
        }
    
        [XmlElement("PerChangePrincipalAndInterestPaymentAdjustmentEffectiveDate", Order = 12)]
        public MISMODate PerChangePrincipalAndInterestPaymentAdjustmentEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool PerChangePrincipalAndInterestPaymentAdjustmentEffectiveDateSpecified
        {
            get { return this.PerChangePrincipalAndInterestPaymentAdjustmentEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("PerChangePrincipalAndInterestPaymentAdjustmentEffectiveMonthsCount", Order = 13)]
        public MISMOCount PerChangePrincipalAndInterestPaymentAdjustmentEffectiveMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool PerChangePrincipalAndInterestPaymentAdjustmentEffectiveMonthsCountSpecified
        {
            get { return this.PerChangePrincipalAndInterestPaymentAdjustmentEffectiveMonthsCount != null; }
            set { }
        }
    
        [XmlElement("PerChangePrincipalAndInterestPaymentAdjustmentFrequencyMonthsCount", Order = 14)]
        public MISMOCount PerChangePrincipalAndInterestPaymentAdjustmentFrequencyMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool PerChangePrincipalAndInterestPaymentAdjustmentFrequencyMonthsCountSpecified
        {
            get { return this.PerChangePrincipalAndInterestPaymentAdjustmentFrequencyMonthsCount != null; }
            set { }
        }
    
        [XmlElement("PerChangePrincipalAndInterestPaymentAdjustmentPercent", Order = 15)]
        public MISMOPercent PerChangePrincipalAndInterestPaymentAdjustmentPercent { get; set; }
    
        [XmlIgnore]
        public bool PerChangePrincipalAndInterestPaymentAdjustmentPercentSpecified
        {
            get { return this.PerChangePrincipalAndInterestPaymentAdjustmentPercent != null; }
            set { }
        }
    
        [XmlElement("PerChangePrincipalAndInterestPaymentMinimumDecreaseRatePercent", Order = 16)]
        public MISMOPercent PerChangePrincipalAndInterestPaymentMinimumDecreaseRatePercent { get; set; }
    
        [XmlIgnore]
        public bool PerChangePrincipalAndInterestPaymentMinimumDecreaseRatePercentSpecified
        {
            get { return this.PerChangePrincipalAndInterestPaymentMinimumDecreaseRatePercent != null; }
            set { }
        }
    
        [XmlElement("PerChangePrincipalAndInterestPaymentMinimumIncreaseRatePercent", Order = 17)]
        public MISMOPercent PerChangePrincipalAndInterestPaymentMinimumIncreaseRatePercent { get; set; }
    
        [XmlIgnore]
        public bool PerChangePrincipalAndInterestPaymentMinimumIncreaseRatePercentSpecified
        {
            get { return this.PerChangePrincipalAndInterestPaymentMinimumIncreaseRatePercent != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 18)]
        public PRINCIPAL_AND_INTEREST_PAYMENT_PER_CHANGE_ADJUSTMENT_RULE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
