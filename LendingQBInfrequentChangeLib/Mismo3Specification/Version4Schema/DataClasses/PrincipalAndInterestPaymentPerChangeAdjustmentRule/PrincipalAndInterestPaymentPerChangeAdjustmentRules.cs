namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class PRINCIPAL_AND_INTEREST_PAYMENT_PER_CHANGE_ADJUSTMENT_RULES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PrincipalAndInterestPaymentPerChangeAdjustmentRuleListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PRINCIPAL_AND_INTEREST_PAYMENT_PER_CHANGE_ADJUSTMENT_RULE", Order = 0)]
        public List<PRINCIPAL_AND_INTEREST_PAYMENT_PER_CHANGE_ADJUSTMENT_RULE> PrincipalAndInterestPaymentPerChangeAdjustmentRuleList { get; set; } = new List<PRINCIPAL_AND_INTEREST_PAYMENT_PER_CHANGE_ADJUSTMENT_RULE>();
    
        [XmlIgnore]
        public bool PrincipalAndInterestPaymentPerChangeAdjustmentRuleListSpecified
        {
            get { return this.PrincipalAndInterestPaymentPerChangeAdjustmentRuleList != null && this.PrincipalAndInterestPaymentPerChangeAdjustmentRuleList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public PRINCIPAL_AND_INTEREST_PAYMENT_PER_CHANGE_ADJUSTMENT_RULES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
