namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class RESEARCH
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ComparableDataSourceDescriptionSpecified
                    || this.ComparableHasPriorSalesIndicatorSpecified
                    || this.ComparableListingsPriceRangeHighAmountSpecified
                    || this.ComparableListingsPriceRangeLowAmountSpecified
                    || this.ComparableListingsResearchedCountSpecified
                    || this.ComparableSalesPriceRangeHighAmountSpecified
                    || this.ComparableSalesPriceRangeLowAmountSpecified
                    || this.ComparableSalesResearchedCountSpecified
                    || this.SalesHistoryNotResearchedCommentDescriptionSpecified
                    || this.SalesHistoryResearchedIndicatorSpecified
                    || this.SubjectHasPriorSalesIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ComparableDataSourceDescription", Order = 0)]
        public MISMOString ComparableDataSourceDescription { get; set; }
    
        [XmlIgnore]
        public bool ComparableDataSourceDescriptionSpecified
        {
            get { return this.ComparableDataSourceDescription != null; }
            set { }
        }
    
        [XmlElement("ComparableHasPriorSalesIndicator", Order = 1)]
        public MISMOIndicator ComparableHasPriorSalesIndicator { get; set; }
    
        [XmlIgnore]
        public bool ComparableHasPriorSalesIndicatorSpecified
        {
            get { return this.ComparableHasPriorSalesIndicator != null; }
            set { }
        }
    
        [XmlElement("ComparableListingsPriceRangeHighAmount", Order = 2)]
        public MISMOAmount ComparableListingsPriceRangeHighAmount { get; set; }
    
        [XmlIgnore]
        public bool ComparableListingsPriceRangeHighAmountSpecified
        {
            get { return this.ComparableListingsPriceRangeHighAmount != null; }
            set { }
        }
    
        [XmlElement("ComparableListingsPriceRangeLowAmount", Order = 3)]
        public MISMOAmount ComparableListingsPriceRangeLowAmount { get; set; }
    
        [XmlIgnore]
        public bool ComparableListingsPriceRangeLowAmountSpecified
        {
            get { return this.ComparableListingsPriceRangeLowAmount != null; }
            set { }
        }
    
        [XmlElement("ComparableListingsResearchedCount", Order = 4)]
        public MISMOCount ComparableListingsResearchedCount { get; set; }
    
        [XmlIgnore]
        public bool ComparableListingsResearchedCountSpecified
        {
            get { return this.ComparableListingsResearchedCount != null; }
            set { }
        }
    
        [XmlElement("ComparableSalesPriceRangeHighAmount", Order = 5)]
        public MISMOAmount ComparableSalesPriceRangeHighAmount { get; set; }
    
        [XmlIgnore]
        public bool ComparableSalesPriceRangeHighAmountSpecified
        {
            get { return this.ComparableSalesPriceRangeHighAmount != null; }
            set { }
        }
    
        [XmlElement("ComparableSalesPriceRangeLowAmount", Order = 6)]
        public MISMOAmount ComparableSalesPriceRangeLowAmount { get; set; }
    
        [XmlIgnore]
        public bool ComparableSalesPriceRangeLowAmountSpecified
        {
            get { return this.ComparableSalesPriceRangeLowAmount != null; }
            set { }
        }
    
        [XmlElement("ComparableSalesResearchedCount", Order = 7)]
        public MISMOCount ComparableSalesResearchedCount { get; set; }
    
        [XmlIgnore]
        public bool ComparableSalesResearchedCountSpecified
        {
            get { return this.ComparableSalesResearchedCount != null; }
            set { }
        }
    
        [XmlElement("SalesHistoryNotResearchedCommentDescription", Order = 8)]
        public MISMOString SalesHistoryNotResearchedCommentDescription { get; set; }
    
        [XmlIgnore]
        public bool SalesHistoryNotResearchedCommentDescriptionSpecified
        {
            get { return this.SalesHistoryNotResearchedCommentDescription != null; }
            set { }
        }
    
        [XmlElement("SalesHistoryResearchedIndicator", Order = 9)]
        public MISMOIndicator SalesHistoryResearchedIndicator { get; set; }
    
        [XmlIgnore]
        public bool SalesHistoryResearchedIndicatorSpecified
        {
            get { return this.SalesHistoryResearchedIndicator != null; }
            set { }
        }
    
        [XmlElement("SubjectHasPriorSalesIndicator", Order = 10)]
        public MISMOIndicator SubjectHasPriorSalesIndicator { get; set; }
    
        [XmlIgnore]
        public bool SubjectHasPriorSalesIndicatorSpecified
        {
            get { return this.SubjectHasPriorSalesIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 11)]
        public RESEARCH_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
