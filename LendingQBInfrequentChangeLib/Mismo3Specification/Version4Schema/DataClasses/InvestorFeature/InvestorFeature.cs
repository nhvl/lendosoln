namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class INVESTOR_FEATURE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.InvestorFeatureCategoryNameSpecified
                    || this.InvestorFeatureDescriptionSpecified
                    || this.InvestorFeatureIdentifierSpecified
                    || this.InvestorFeatureNameSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("InvestorFeatureCategoryName", Order = 0)]
        public MISMOString InvestorFeatureCategoryName { get; set; }
    
        [XmlIgnore]
        public bool InvestorFeatureCategoryNameSpecified
        {
            get { return this.InvestorFeatureCategoryName != null; }
            set { }
        }
    
        [XmlElement("InvestorFeatureDescription", Order = 1)]
        public MISMOString InvestorFeatureDescription { get; set; }
    
        [XmlIgnore]
        public bool InvestorFeatureDescriptionSpecified
        {
            get { return this.InvestorFeatureDescription != null; }
            set { }
        }
    
        [XmlElement("InvestorFeatureIdentifier", Order = 2)]
        public MISMOIdentifier InvestorFeatureIdentifier { get; set; }
    
        [XmlIgnore]
        public bool InvestorFeatureIdentifierSpecified
        {
            get { return this.InvestorFeatureIdentifier != null; }
            set { }
        }
    
        [XmlElement("InvestorFeatureName", Order = 3)]
        public MISMOString InvestorFeatureName { get; set; }
    
        [XmlIgnore]
        public bool InvestorFeatureNameSpecified
        {
            get { return this.InvestorFeatureName != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public INVESTOR_FEATURE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
