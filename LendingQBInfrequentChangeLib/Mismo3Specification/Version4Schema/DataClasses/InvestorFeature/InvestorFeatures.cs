namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class INVESTOR_FEATURES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.InvestorFeatureListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("INVESTOR_FEATURE", Order = 0)]
        public List<INVESTOR_FEATURE> InvestorFeatureList { get; set; } = new List<INVESTOR_FEATURE>();
    
        [XmlIgnore]
        public bool InvestorFeatureListSpecified
        {
            get { return this.InvestorFeatureList != null && this.InvestorFeatureList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public INVESTOR_FEATURES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
