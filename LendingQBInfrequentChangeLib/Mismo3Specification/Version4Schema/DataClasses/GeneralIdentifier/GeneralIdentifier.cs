namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class GENERAL_IDENTIFIER
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CongressionalDistrictIdentifierSpecified
                    || this.CoreBasedStatisticalAreaCodeSpecified
                    || this.CoreBasedStatisticalAreaDivisionCodeSpecified
                    || this.JudicialDistrictNameSpecified
                    || this.JudicialDivisionNameSpecified
                    || this.MapReferenceIdentifierSpecified
                    || this.MapReferenceSecondIdentifierSpecified
                    || this.MetropolitanDivisionIdentifierSpecified
                    || this.MSAIdentifierSpecified
                    || this.MunicipalityNameSpecified
                    || this.RecordingJurisdictionNameSpecified
                    || this.RecordingJurisdictionTypeSpecified
                    || this.RecordingJurisdictionTypeOtherDescriptionSpecified
                    || this.SchoolDistrictNameSpecified
                    || this.UnincorporatedAreaNameSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CongressionalDistrictIdentifier", Order = 0)]
        public MISMOIdentifier CongressionalDistrictIdentifier { get; set; }
    
        [XmlIgnore]
        public bool CongressionalDistrictIdentifierSpecified
        {
            get { return this.CongressionalDistrictIdentifier != null; }
            set { }
        }
    
        [XmlElement("CoreBasedStatisticalAreaCode", Order = 1)]
        public MISMOCode CoreBasedStatisticalAreaCode { get; set; }
    
        [XmlIgnore]
        public bool CoreBasedStatisticalAreaCodeSpecified
        {
            get { return this.CoreBasedStatisticalAreaCode != null; }
            set { }
        }
    
        [XmlElement("CoreBasedStatisticalAreaDivisionCode", Order = 2)]
        public MISMOCode CoreBasedStatisticalAreaDivisionCode { get; set; }
    
        [XmlIgnore]
        public bool CoreBasedStatisticalAreaDivisionCodeSpecified
        {
            get { return this.CoreBasedStatisticalAreaDivisionCode != null; }
            set { }
        }
    
        [XmlElement("JudicialDistrictName", Order = 3)]
        public MISMOString JudicialDistrictName { get; set; }
    
        [XmlIgnore]
        public bool JudicialDistrictNameSpecified
        {
            get { return this.JudicialDistrictName != null; }
            set { }
        }
    
        [XmlElement("JudicialDivisionName", Order = 4)]
        public MISMOString JudicialDivisionName { get; set; }
    
        [XmlIgnore]
        public bool JudicialDivisionNameSpecified
        {
            get { return this.JudicialDivisionName != null; }
            set { }
        }
    
        [XmlElement("MapReferenceIdentifier", Order = 5)]
        public MISMOIdentifier MapReferenceIdentifier { get; set; }
    
        [XmlIgnore]
        public bool MapReferenceIdentifierSpecified
        {
            get { return this.MapReferenceIdentifier != null; }
            set { }
        }
    
        [XmlElement("MapReferenceSecondIdentifier", Order = 6)]
        public MISMOIdentifier MapReferenceSecondIdentifier { get; set; }
    
        [XmlIgnore]
        public bool MapReferenceSecondIdentifierSpecified
        {
            get { return this.MapReferenceSecondIdentifier != null; }
            set { }
        }
    
        [XmlElement("MetropolitanDivisionIdentifier", Order = 7)]
        public MISMOIdentifier MetropolitanDivisionIdentifier { get; set; }
    
        [XmlIgnore]
        public bool MetropolitanDivisionIdentifierSpecified
        {
            get { return this.MetropolitanDivisionIdentifier != null; }
            set { }
        }
    
        [XmlElement("MSAIdentifier", Order = 8)]
        public MISMOIdentifier MSAIdentifier { get; set; }
    
        [XmlIgnore]
        public bool MSAIdentifierSpecified
        {
            get { return this.MSAIdentifier != null; }
            set { }
        }
    
        [XmlElement("MunicipalityName", Order = 9)]
        public MISMOString MunicipalityName { get; set; }
    
        [XmlIgnore]
        public bool MunicipalityNameSpecified
        {
            get { return this.MunicipalityName != null; }
            set { }
        }
    
        [XmlElement("RecordingJurisdictionName", Order = 10)]
        public MISMOString RecordingJurisdictionName { get; set; }
    
        [XmlIgnore]
        public bool RecordingJurisdictionNameSpecified
        {
            get { return this.RecordingJurisdictionName != null; }
            set { }
        }
    
        [XmlElement("RecordingJurisdictionType", Order = 11)]
        public MISMOEnum<RecordingJurisdictionBase> RecordingJurisdictionType { get; set; }
    
        [XmlIgnore]
        public bool RecordingJurisdictionTypeSpecified
        {
            get { return this.RecordingJurisdictionType != null; }
            set { }
        }
    
        [XmlElement("RecordingJurisdictionTypeOtherDescription", Order = 12)]
        public MISMOString RecordingJurisdictionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool RecordingJurisdictionTypeOtherDescriptionSpecified
        {
            get { return this.RecordingJurisdictionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("SchoolDistrictName", Order = 13)]
        public MISMOString SchoolDistrictName { get; set; }
    
        [XmlIgnore]
        public bool SchoolDistrictNameSpecified
        {
            get { return this.SchoolDistrictName != null; }
            set { }
        }
    
        [XmlElement("UnincorporatedAreaName", Order = 14)]
        public MISMOString UnincorporatedAreaName { get; set; }
    
        [XmlIgnore]
        public bool UnincorporatedAreaNameSpecified
        {
            get { return this.UnincorporatedAreaName != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 15)]
        public GENERAL_IDENTIFIER_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
