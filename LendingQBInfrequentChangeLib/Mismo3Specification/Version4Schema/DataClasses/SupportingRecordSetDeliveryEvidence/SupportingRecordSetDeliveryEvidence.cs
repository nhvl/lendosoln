namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SUPPORTING_RECORD_SET_DELIVERY_EVIDENCE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ForeignObjectsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("FOREIGN_OBJECTS", Order = 0)]
        public FOREIGN_OBJECTS ForeignObjects { get; set; }
    
        [XmlIgnore]
        public bool ForeignObjectsSpecified
        {
            get { return this.ForeignObjects != null && this.ForeignObjects.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public SUPPORTING_RECORD_SET_DELIVERY_EVIDENCE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
