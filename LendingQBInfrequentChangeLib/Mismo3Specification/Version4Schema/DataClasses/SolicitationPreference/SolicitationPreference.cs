namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SOLICITATION_PREFERENCE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CorporateAffiliateSharingAllowedIndicatorSpecified
                    || this.SolicitationByEmailAllowedIndicatorSpecified
                    || this.SolicitationByTelephoneAllowedIndicatorSpecified
                    || this.SolicitationByUSMailAllowedIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CorporateAffiliateSharingAllowedIndicator", Order = 0)]
        public MISMOIndicator CorporateAffiliateSharingAllowedIndicator { get; set; }
    
        [XmlIgnore]
        public bool CorporateAffiliateSharingAllowedIndicatorSpecified
        {
            get { return this.CorporateAffiliateSharingAllowedIndicator != null; }
            set { }
        }
    
        [XmlElement("SolicitationByEmailAllowedIndicator", Order = 1)]
        public MISMOIndicator SolicitationByEmailAllowedIndicator { get; set; }
    
        [XmlIgnore]
        public bool SolicitationByEmailAllowedIndicatorSpecified
        {
            get { return this.SolicitationByEmailAllowedIndicator != null; }
            set { }
        }
    
        [XmlElement("SolicitationByTelephoneAllowedIndicator", Order = 2)]
        public MISMOIndicator SolicitationByTelephoneAllowedIndicator { get; set; }
    
        [XmlIgnore]
        public bool SolicitationByTelephoneAllowedIndicatorSpecified
        {
            get { return this.SolicitationByTelephoneAllowedIndicator != null; }
            set { }
        }
    
        [XmlElement("SolicitationByUSMailAllowedIndicator", Order = 3)]
        public MISMOIndicator SolicitationByUSMailAllowedIndicator { get; set; }
    
        [XmlIgnore]
        public bool SolicitationByUSMailAllowedIndicatorSpecified
        {
            get { return this.SolicitationByUSMailAllowedIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public SOLICITATION_PREFERENCE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
