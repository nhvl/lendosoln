namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SERVICER
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ServicerProgramIdentifierSpecified
                    || this.ServicerRoleEffectiveDateSpecified
                    || this.ServicerTypeSpecified
                    || this.ServicerTypeOtherDescriptionSpecified
                    || this.ServicingTransferEffectiveDateSpecified
                    || this.ServicingTransferRoleTypeSpecified
                    || this.SubservicerRightsTypeSpecified
                    || this.SubservicerRightsTypeOtherDescriptionSpecified
                    || this.TransferOfServicingDisclosureTypeSpecified
                    || this.TransferOfServicingDisclosureTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ServicerProgramIdentifier", Order = 0)]
        public MISMOIdentifier ServicerProgramIdentifier { get; set; }
    
        [XmlIgnore]
        public bool ServicerProgramIdentifierSpecified
        {
            get { return this.ServicerProgramIdentifier != null; }
            set { }
        }
    
        [XmlElement("ServicerRoleEffectiveDate", Order = 1)]
        public MISMODate ServicerRoleEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool ServicerRoleEffectiveDateSpecified
        {
            get { return this.ServicerRoleEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("ServicerType", Order = 2)]
        public MISMOEnum<ServicerBase> ServicerType { get; set; }
    
        [XmlIgnore]
        public bool ServicerTypeSpecified
        {
            get { return this.ServicerType != null; }
            set { }
        }
    
        [XmlElement("ServicerTypeOtherDescription", Order = 3)]
        public MISMOString ServicerTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ServicerTypeOtherDescriptionSpecified
        {
            get { return this.ServicerTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ServicingTransferEffectiveDate", Order = 4)]
        public MISMODate ServicingTransferEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool ServicingTransferEffectiveDateSpecified
        {
            get { return this.ServicingTransferEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("ServicingTransferRoleType", Order = 5)]
        public MISMOEnum<ServicingTransferRoleBase> ServicingTransferRoleType { get; set; }
    
        [XmlIgnore]
        public bool ServicingTransferRoleTypeSpecified
        {
            get { return this.ServicingTransferRoleType != null; }
            set { }
        }
    
        [XmlElement("SubservicerRightsType", Order = 6)]
        public MISMOEnum<SubservicerRightsBase> SubservicerRightsType { get; set; }
    
        [XmlIgnore]
        public bool SubservicerRightsTypeSpecified
        {
            get { return this.SubservicerRightsType != null; }
            set { }
        }
    
        [XmlElement("SubservicerRightsTypeOtherDescription", Order = 7)]
        public MISMOString SubservicerRightsTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool SubservicerRightsTypeOtherDescriptionSpecified
        {
            get { return this.SubservicerRightsTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("TransferOfServicingDisclosureType", Order = 8)]
        public MISMOEnum<TransferOfServicingDisclosureBase> TransferOfServicingDisclosureType { get; set; }
    
        [XmlIgnore]
        public bool TransferOfServicingDisclosureTypeSpecified
        {
            get { return this.TransferOfServicingDisclosureType != null; }
            set { }
        }
    
        [XmlElement("TransferOfServicingDisclosureTypeOtherDescription", Order = 9)]
        public MISMOString TransferOfServicingDisclosureTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool TransferOfServicingDisclosureTypeOtherDescriptionSpecified
        {
            get { return this.TransferOfServicingDisclosureTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 10)]
        public SERVICER_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
