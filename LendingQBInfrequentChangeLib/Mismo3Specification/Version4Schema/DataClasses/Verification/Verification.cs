namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class VERIFICATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.VerificationByNameSpecified
                    || this.VerificationCommentDescriptionSpecified
                    || this.VerificationDateSpecified
                    || this.VerificationMethodTypeSpecified
                    || this.VerificationMethodTypeOtherDescriptionSpecified
                    || this.VerificationStatusTypeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("VerificationByName", Order = 0)]
        public MISMOString VerificationByName { get; set; }
    
        [XmlIgnore]
        public bool VerificationByNameSpecified
        {
            get { return this.VerificationByName != null; }
            set { }
        }
    
        [XmlElement("VerificationCommentDescription", Order = 1)]
        public MISMOString VerificationCommentDescription { get; set; }
    
        [XmlIgnore]
        public bool VerificationCommentDescriptionSpecified
        {
            get { return this.VerificationCommentDescription != null; }
            set { }
        }
    
        [XmlElement("VerificationDate", Order = 2)]
        public MISMODate VerificationDate { get; set; }
    
        [XmlIgnore]
        public bool VerificationDateSpecified
        {
            get { return this.VerificationDate != null; }
            set { }
        }
    
        [XmlElement("VerificationMethodType", Order = 3)]
        public MISMOEnum<VerificationMethodBase> VerificationMethodType { get; set; }
    
        [XmlIgnore]
        public bool VerificationMethodTypeSpecified
        {
            get { return this.VerificationMethodType != null; }
            set { }
        }
    
        [XmlElement("VerificationMethodTypeOtherDescription", Order = 4)]
        public MISMOString VerificationMethodTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool VerificationMethodTypeOtherDescriptionSpecified
        {
            get { return this.VerificationMethodTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("VerificationStatusType", Order = 5)]
        public MISMOEnum<VerificationStatusBase> VerificationStatusType { get; set; }
    
        [XmlIgnore]
        public bool VerificationStatusTypeSpecified
        {
            get { return this.VerificationStatusType != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 6)]
        public VERIFICATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
