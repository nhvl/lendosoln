namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class LANGUAGES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LanguageListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LANGUAGE", Order = 0)]
        public List<LANGUAGE> LanguageList { get; set; } = new List<LANGUAGE>();
    
        [XmlIgnore]
        public bool LanguageListSpecified
        {
            get { return this.LanguageList != null && this.LanguageList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public LANGUAGES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
