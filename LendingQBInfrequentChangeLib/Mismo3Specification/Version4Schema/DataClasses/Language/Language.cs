namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class LANGUAGE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LanguageCodeSpecified
                    || this.LanguagePreferenceIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LanguageCode", Order = 0)]
        public MISMOCode LanguageCode { get; set; }
    
        [XmlIgnore]
        public bool LanguageCodeSpecified
        {
            get { return this.LanguageCode != null; }
            set { }
        }
    
        [XmlElement("LanguagePreferenceIndicator", Order = 1)]
        public MISMOIndicator LanguagePreferenceIndicator { get; set; }
    
        [XmlIgnore]
        public bool LanguagePreferenceIndicatorSpecified
        {
            get { return this.LanguagePreferenceIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public LANGUAGE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
