namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class INVERTER
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.InverterAgeYearsCountSpecified
                    || this.InverterManufacturerNameSpecified
                    || this.InverterWarrantyTermDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("InverterAgeYearsCount", Order = 0)]
        public MISMOCount InverterAgeYearsCount { get; set; }
    
        [XmlIgnore]
        public bool InverterAgeYearsCountSpecified
        {
            get { return this.InverterAgeYearsCount != null; }
            set { }
        }
    
        [XmlElement("InverterManufacturerName", Order = 1)]
        public MISMOString InverterManufacturerName { get; set; }
    
        [XmlIgnore]
        public bool InverterManufacturerNameSpecified
        {
            get { return this.InverterManufacturerName != null; }
            set { }
        }
    
        [XmlElement("InverterWarrantyTermDescription", Order = 2)]
        public MISMOString InverterWarrantyTermDescription { get; set; }
    
        [XmlIgnore]
        public bool InverterWarrantyTermDescriptionSpecified
        {
            get { return this.InverterWarrantyTermDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public INVERTER_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
