namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class INVERTERS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.InverterListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("INVERTER", Order = 0)]
        public List<INVERTER> InverterList { get; set; } = new List<INVERTER>();
    
        [XmlIgnore]
        public bool InverterListSpecified
        {
            get { return this.InverterList != null && this.InverterList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public INVERTERS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
