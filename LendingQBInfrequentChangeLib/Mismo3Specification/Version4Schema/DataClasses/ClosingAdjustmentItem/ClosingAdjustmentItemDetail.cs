namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CLOSING_ADJUSTMENT_ITEM_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ClosingAdjustmentItemAmountSpecified
                    || this.ClosingAdjustmentItemPaidByTypeSpecified
                    || this.ClosingAdjustmentItemPaidOutsideOfClosingIndicatorSpecified
                    || this.ClosingAdjustmentItemTypeSpecified
                    || this.ClosingAdjustmentItemTypeOtherDescriptionSpecified
                    || this.ClosingAdjustmentPaidFromDateSpecified
                    || this.ClosingAdjustmentPaidThroughDateSpecified
                    || this.IntegratedDisclosureLineNumberValueSpecified
                    || this.IntegratedDisclosureSectionTypeSpecified
                    || this.IntegratedDisclosureSectionTypeOtherDescriptionSpecified
                    || this.IntegratedDisclosureSubsectionTypeSpecified
                    || this.IntegratedDisclosureSubsectionTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ClosingAdjustmentItemAmount", Order = 0)]
        public MISMOAmount ClosingAdjustmentItemAmount { get; set; }
    
        [XmlIgnore]
        public bool ClosingAdjustmentItemAmountSpecified
        {
            get { return this.ClosingAdjustmentItemAmount != null; }
            set { }
        }
    
        [XmlElement("ClosingAdjustmentItemPaidByType", Order = 1)]
        public MISMOEnum<ClosingAdjustmentItemPaidByBase> ClosingAdjustmentItemPaidByType { get; set; }
    
        [XmlIgnore]
        public bool ClosingAdjustmentItemPaidByTypeSpecified
        {
            get { return this.ClosingAdjustmentItemPaidByType != null; }
            set { }
        }
    
        [XmlElement("ClosingAdjustmentItemPaidOutsideOfClosingIndicator", Order = 2)]
        public MISMOIndicator ClosingAdjustmentItemPaidOutsideOfClosingIndicator { get; set; }
    
        [XmlIgnore]
        public bool ClosingAdjustmentItemPaidOutsideOfClosingIndicatorSpecified
        {
            get { return this.ClosingAdjustmentItemPaidOutsideOfClosingIndicator != null; }
            set { }
        }
    
        [XmlElement("ClosingAdjustmentItemType", Order = 3)]
        public MISMOEnum<ClosingAdjustmentItemBase> ClosingAdjustmentItemType { get; set; }
    
        [XmlIgnore]
        public bool ClosingAdjustmentItemTypeSpecified
        {
            get { return this.ClosingAdjustmentItemType != null; }
            set { }
        }
    
        [XmlElement("ClosingAdjustmentItemTypeOtherDescription", Order = 4)]
        public MISMOString ClosingAdjustmentItemTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ClosingAdjustmentItemTypeOtherDescriptionSpecified
        {
            get { return this.ClosingAdjustmentItemTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ClosingAdjustmentPaidFromDate", Order = 5)]
        public MISMODate ClosingAdjustmentPaidFromDate { get; set; }
    
        [XmlIgnore]
        public bool ClosingAdjustmentPaidFromDateSpecified
        {
            get { return this.ClosingAdjustmentPaidFromDate != null; }
            set { }
        }
    
        [XmlElement("ClosingAdjustmentPaidThroughDate", Order = 6)]
        public MISMODate ClosingAdjustmentPaidThroughDate { get; set; }
    
        [XmlIgnore]
        public bool ClosingAdjustmentPaidThroughDateSpecified
        {
            get { return this.ClosingAdjustmentPaidThroughDate != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureLineNumberValue", Order = 7)]
        public MISMOValue IntegratedDisclosureLineNumberValue { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureLineNumberValueSpecified
        {
            get { return this.IntegratedDisclosureLineNumberValue != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureSectionType", Order = 8)]
        public MISMOEnum<IntegratedDisclosureSectionBase> IntegratedDisclosureSectionType { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureSectionTypeSpecified
        {
            get { return this.IntegratedDisclosureSectionType != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureSectionTypeOtherDescription", Order = 9)]
        public MISMOString IntegratedDisclosureSectionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureSectionTypeOtherDescriptionSpecified
        {
            get { return this.IntegratedDisclosureSectionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureSubsectionType", Order = 10)]
        public MISMOEnum<IntegratedDisclosureSubsectionBase> IntegratedDisclosureSubsectionType { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureSubsectionTypeSpecified
        {
            get { return this.IntegratedDisclosureSubsectionType != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureSubsectionTypeOtherDescription", Order = 11)]
        public MISMOString IntegratedDisclosureSubsectionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureSubsectionTypeOtherDescriptionSpecified
        {
            get { return this.IntegratedDisclosureSubsectionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 12)]
        public CLOSING_ADJUSTMENT_ITEM_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
