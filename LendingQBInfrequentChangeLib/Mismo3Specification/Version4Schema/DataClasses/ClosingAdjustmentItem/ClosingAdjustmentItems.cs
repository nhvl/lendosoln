namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class CLOSING_ADJUSTMENT_ITEMS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ClosingAdjustmentItemListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CLOSING_ADJUSTMENT_ITEM", Order = 0)]
        public List<CLOSING_ADJUSTMENT_ITEM> ClosingAdjustmentItemList { get; set; } = new List<CLOSING_ADJUSTMENT_ITEM>();
    
        [XmlIgnore]
        public bool ClosingAdjustmentItemListSpecified
        {
            get { return this.ClosingAdjustmentItemList != null && this.ClosingAdjustmentItemList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public CLOSING_ADJUSTMENT_ITEMS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
