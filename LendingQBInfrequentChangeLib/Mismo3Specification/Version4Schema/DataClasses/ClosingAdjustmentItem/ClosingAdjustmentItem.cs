namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CLOSING_ADJUSTMENT_ITEM
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ClosingAdjustmentItemDetailSpecified
                    || this.CLOSING_ADJUSTMENT_ITEM_PAID_BYSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CLOSING_ADJUSTMENT_ITEM_DETAIL", Order = 0)]
        public CLOSING_ADJUSTMENT_ITEM_DETAIL ClosingAdjustmentItemDetail { get; set; }
    
        [XmlIgnore]
        public bool ClosingAdjustmentItemDetailSpecified
        {
            get { return this.ClosingAdjustmentItemDetail != null && this.ClosingAdjustmentItemDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CLOSING_ADJUSTMENT_ITEM_PAID_BY", Order = 1)]
        public PAID_BY CLOSING_ADJUSTMENT_ITEM_PAID_BY { get; set; }
    
        [XmlIgnore]
        public bool CLOSING_ADJUSTMENT_ITEM_PAID_BYSpecified
        {
            get { return this.CLOSING_ADJUSTMENT_ITEM_PAID_BY != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public CLOSING_ADJUSTMENT_ITEM_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
