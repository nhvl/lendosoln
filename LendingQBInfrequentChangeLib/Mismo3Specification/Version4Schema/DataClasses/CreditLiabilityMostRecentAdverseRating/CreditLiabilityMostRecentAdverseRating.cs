namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CREDIT_LIABILITY_MOST_RECENT_ADVERSE_RATING
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditLiabilityMostRecentAdverseRatingAmountSpecified
                    || this.CreditLiabilityMostRecentAdverseRatingCodeSpecified
                    || this.CreditLiabilityMostRecentAdverseRatingDateSpecified
                    || this.CreditLiabilityMostRecentAdverseRatingTypeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CreditLiabilityMostRecentAdverseRatingAmount", Order = 0)]
        public MISMOAmount CreditLiabilityMostRecentAdverseRatingAmount { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityMostRecentAdverseRatingAmountSpecified
        {
            get { return this.CreditLiabilityMostRecentAdverseRatingAmount != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityMostRecentAdverseRatingCode", Order = 1)]
        public MISMOCode CreditLiabilityMostRecentAdverseRatingCode { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityMostRecentAdverseRatingCodeSpecified
        {
            get { return this.CreditLiabilityMostRecentAdverseRatingCode != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityMostRecentAdverseRatingDate", Order = 2)]
        public MISMODate CreditLiabilityMostRecentAdverseRatingDate { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityMostRecentAdverseRatingDateSpecified
        {
            get { return this.CreditLiabilityMostRecentAdverseRatingDate != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityMostRecentAdverseRatingType", Order = 3)]
        public MISMOEnum<CreditLiabilityMostRecentAdverseRatingBase> CreditLiabilityMostRecentAdverseRatingType { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityMostRecentAdverseRatingTypeSpecified
        {
            get { return this.CreditLiabilityMostRecentAdverseRatingType != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public CREDIT_LIABILITY_MOST_RECENT_ADVERSE_RATING_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
