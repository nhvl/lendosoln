namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PRESENT_LAND_USE_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.NeighborhoodLandUseChangeStatusTypeSpecified
                    || this.NeighborhoodLandUseCurrentDescriptionSpecified
                    || this.NeighborhoodLandUseFutureDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("NeighborhoodLandUseChangeStatusType", Order = 0)]
        public MISMOEnum<NeighborhoodLandUseChangeStatusBase> NeighborhoodLandUseChangeStatusType { get; set; }
    
        [XmlIgnore]
        public bool NeighborhoodLandUseChangeStatusTypeSpecified
        {
            get { return this.NeighborhoodLandUseChangeStatusType != null; }
            set { }
        }
    
        [XmlElement("NeighborhoodLandUseCurrentDescription", Order = 1)]
        public MISMOString NeighborhoodLandUseCurrentDescription { get; set; }
    
        [XmlIgnore]
        public bool NeighborhoodLandUseCurrentDescriptionSpecified
        {
            get { return this.NeighborhoodLandUseCurrentDescription != null; }
            set { }
        }
    
        [XmlElement("NeighborhoodLandUseFutureDescription", Order = 2)]
        public MISMOString NeighborhoodLandUseFutureDescription { get; set; }
    
        [XmlIgnore]
        public bool NeighborhoodLandUseFutureDescriptionSpecified
        {
            get { return this.NeighborhoodLandUseFutureDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public PRESENT_LAND_USE_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
