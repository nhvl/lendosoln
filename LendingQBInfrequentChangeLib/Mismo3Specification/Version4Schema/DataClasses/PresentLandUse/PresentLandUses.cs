namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class PRESENT_LAND_USES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PresentLandUseListSpecified
                    || this.PresentLandUseDetailSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PRESENT_LAND_USE", Order = 0)]
        public List<PRESENT_LAND_USE> PresentLandUseList { get; set; } = new List<PRESENT_LAND_USE>();
    
        [XmlIgnore]
        public bool PresentLandUseListSpecified
        {
            get { return this.PresentLandUseList != null && this.PresentLandUseList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("PRESENT_LAND_USE_DETAIL", Order = 1)]
        public PRESENT_LAND_USE_DETAIL PresentLandUseDetail { get; set; }
    
        [XmlIgnore]
        public bool PresentLandUseDetailSpecified
        {
            get { return this.PresentLandUseDetail != null && this.PresentLandUseDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public PRESENT_LAND_USES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
