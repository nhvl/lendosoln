namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PRESENT_LAND_USE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.NeighborhoodLandUseTypeSpecified
                    || this.NeighborhoodLandUseTypeOtherDescriptionSpecified
                    || this.NeighborhoodPresentLandUsePercentSpecified
                    || this.PresentLandUseTotalPercentSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("NeighborhoodLandUseType", Order = 0)]
        public MISMOEnum<NeighborhoodLandUseBase> NeighborhoodLandUseType { get; set; }
    
        [XmlIgnore]
        public bool NeighborhoodLandUseTypeSpecified
        {
            get { return this.NeighborhoodLandUseType != null; }
            set { }
        }
    
        [XmlElement("NeighborhoodLandUseTypeOtherDescription", Order = 1)]
        public MISMOString NeighborhoodLandUseTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool NeighborhoodLandUseTypeOtherDescriptionSpecified
        {
            get { return this.NeighborhoodLandUseTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("NeighborhoodPresentLandUsePercent", Order = 2)]
        public MISMOPercent NeighborhoodPresentLandUsePercent { get; set; }
    
        [XmlIgnore]
        public bool NeighborhoodPresentLandUsePercentSpecified
        {
            get { return this.NeighborhoodPresentLandUsePercent != null; }
            set { }
        }
    
        [XmlElement("PresentLandUseTotalPercent", Order = 3)]
        public MISMOPercent PresentLandUseTotalPercent { get; set; }
    
        [XmlIgnore]
        public bool PresentLandUseTotalPercentSpecified
        {
            get { return this.PresentLandUseTotalPercent != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public PRESENT_LAND_USE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
