namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class REVERSE_MORTGAGE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ReverseGrossLineOfCreditReserveAmountSpecified
                    || this.ReverseInitialPrincipalLimitAmountSpecified
                    || this.ReverseMortgageDisclosedMonthlyServicingFeeAmountSpecified
                    || this.ReverseMortgageInitialAdvanceAmountSpecified
                    || this.ReverseNetLineOfCreditAmountSpecified
                    || this.ReverseNetPrincipalLimitAmountSpecified
                    || this.ReversePaymentPlanTypeSpecified
                    || this.ReversePaymentPlanTypeOtherDescriptionSpecified
                    || this.ReversePaymentTermMonthsCountSpecified
                    || this.ReverseScheduledPaymentAmountSpecified
                    || this.ReverseSetAsideFirstYearPropertyChargesAmountSpecified
                    || this.ReverseSetAsideTaxesAndInsuranceWithholdingAmountSpecified
                    || this.ReverseSetAsideTaxesAndInsuranceWithholdingEndDateSpecified
                    || this.ReverseSetAsideTaxesAndInsuranceWithholdingRatePercentSpecified
                    || this.ReverseSetAsideTaxesAndInsuranceWithholdingStartDateSpecified
                    || this.ReverseTotalAvailableAmountSpecified
                    || this.UPBAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ReverseGrossLineOfCreditReserveAmount", Order = 0)]
        public MISMOAmount ReverseGrossLineOfCreditReserveAmount { get; set; }
    
        [XmlIgnore]
        public bool ReverseGrossLineOfCreditReserveAmountSpecified
        {
            get { return this.ReverseGrossLineOfCreditReserveAmount != null; }
            set { }
        }
    
        [XmlElement("ReverseInitialPrincipalLimitAmount", Order = 1)]
        public MISMOAmount ReverseInitialPrincipalLimitAmount { get; set; }
    
        [XmlIgnore]
        public bool ReverseInitialPrincipalLimitAmountSpecified
        {
            get { return this.ReverseInitialPrincipalLimitAmount != null; }
            set { }
        }
    
        [XmlElement("ReverseMortgageDisclosedMonthlyServicingFeeAmount", Order = 2)]
        public MISMOAmount ReverseMortgageDisclosedMonthlyServicingFeeAmount { get; set; }
    
        [XmlIgnore]
        public bool ReverseMortgageDisclosedMonthlyServicingFeeAmountSpecified
        {
            get { return this.ReverseMortgageDisclosedMonthlyServicingFeeAmount != null; }
            set { }
        }
    
        [XmlElement("ReverseMortgageInitialAdvanceAmount", Order = 3)]
        public MISMOAmount ReverseMortgageInitialAdvanceAmount { get; set; }
    
        [XmlIgnore]
        public bool ReverseMortgageInitialAdvanceAmountSpecified
        {
            get { return this.ReverseMortgageInitialAdvanceAmount != null; }
            set { }
        }
    
        [XmlElement("ReverseNetLineOfCreditAmount", Order = 4)]
        public MISMOAmount ReverseNetLineOfCreditAmount { get; set; }
    
        [XmlIgnore]
        public bool ReverseNetLineOfCreditAmountSpecified
        {
            get { return this.ReverseNetLineOfCreditAmount != null; }
            set { }
        }
    
        [XmlElement("ReverseNetPrincipalLimitAmount", Order = 5)]
        public MISMOAmount ReverseNetPrincipalLimitAmount { get; set; }
    
        [XmlIgnore]
        public bool ReverseNetPrincipalLimitAmountSpecified
        {
            get { return this.ReverseNetPrincipalLimitAmount != null; }
            set { }
        }
    
        [XmlElement("ReversePaymentPlanType", Order = 6)]
        public MISMOEnum<ReversePaymentPlanBase> ReversePaymentPlanType { get; set; }
    
        [XmlIgnore]
        public bool ReversePaymentPlanTypeSpecified
        {
            get { return this.ReversePaymentPlanType != null; }
            set { }
        }
    
        [XmlElement("ReversePaymentPlanTypeOtherDescription", Order = 7)]
        public MISMOString ReversePaymentPlanTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ReversePaymentPlanTypeOtherDescriptionSpecified
        {
            get { return this.ReversePaymentPlanTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ReversePaymentTermMonthsCount", Order = 8)]
        public MISMOCount ReversePaymentTermMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool ReversePaymentTermMonthsCountSpecified
        {
            get { return this.ReversePaymentTermMonthsCount != null; }
            set { }
        }
    
        [XmlElement("ReverseScheduledPaymentAmount", Order = 9)]
        public MISMOAmount ReverseScheduledPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool ReverseScheduledPaymentAmountSpecified
        {
            get { return this.ReverseScheduledPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("ReverseSetAsideFirstYearPropertyChargesAmount", Order = 10)]
        public MISMOAmount ReverseSetAsideFirstYearPropertyChargesAmount { get; set; }
    
        [XmlIgnore]
        public bool ReverseSetAsideFirstYearPropertyChargesAmountSpecified
        {
            get { return this.ReverseSetAsideFirstYearPropertyChargesAmount != null; }
            set { }
        }
    
        [XmlElement("ReverseSetAsideTaxesAndInsuranceWithholdingAmount", Order = 11)]
        public MISMOAmount ReverseSetAsideTaxesAndInsuranceWithholdingAmount { get; set; }
    
        [XmlIgnore]
        public bool ReverseSetAsideTaxesAndInsuranceWithholdingAmountSpecified
        {
            get { return this.ReverseSetAsideTaxesAndInsuranceWithholdingAmount != null; }
            set { }
        }
    
        [XmlElement("ReverseSetAsideTaxesAndInsuranceWithholdingEndDate", Order = 12)]
        public MISMODate ReverseSetAsideTaxesAndInsuranceWithholdingEndDate { get; set; }
    
        [XmlIgnore]
        public bool ReverseSetAsideTaxesAndInsuranceWithholdingEndDateSpecified
        {
            get { return this.ReverseSetAsideTaxesAndInsuranceWithholdingEndDate != null; }
            set { }
        }
    
        [XmlElement("ReverseSetAsideTaxesAndInsuranceWithholdingRatePercent", Order = 13)]
        public MISMOPercent ReverseSetAsideTaxesAndInsuranceWithholdingRatePercent { get; set; }
    
        [XmlIgnore]
        public bool ReverseSetAsideTaxesAndInsuranceWithholdingRatePercentSpecified
        {
            get { return this.ReverseSetAsideTaxesAndInsuranceWithholdingRatePercent != null; }
            set { }
        }
    
        [XmlElement("ReverseSetAsideTaxesAndInsuranceWithholdingStartDate", Order = 14)]
        public MISMODate ReverseSetAsideTaxesAndInsuranceWithholdingStartDate { get; set; }
    
        [XmlIgnore]
        public bool ReverseSetAsideTaxesAndInsuranceWithholdingStartDateSpecified
        {
            get { return this.ReverseSetAsideTaxesAndInsuranceWithholdingStartDate != null; }
            set { }
        }
    
        [XmlElement("ReverseTotalAvailableAmount", Order = 15)]
        public MISMOAmount ReverseTotalAvailableAmount { get; set; }
    
        [XmlIgnore]
        public bool ReverseTotalAvailableAmountSpecified
        {
            get { return this.ReverseTotalAvailableAmount != null; }
            set { }
        }
    
        [XmlElement("UPBAmount", Order = 16)]
        public MISMOAmount UPBAmount { get; set; }
    
        [XmlIgnore]
        public bool UPBAmountSpecified
        {
            get { return this.UPBAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 17)]
        public REVERSE_MORTGAGE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
