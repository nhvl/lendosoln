namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PAYMENT_SCHEDULE_ITEM
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PaymentSchedulePaymentAmountSpecified
                    || this.PaymentSchedulePaymentsCountSpecified
                    || this.PaymentSchedulePaymentStartDateSpecified
                    || this.PaymentSchedulePaymentVaryingToAmountSpecified
                    || this.PaymentSchedulePrincipalAndInterestPaymentAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PaymentSchedulePaymentAmount", Order = 0)]
        public MISMOAmount PaymentSchedulePaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool PaymentSchedulePaymentAmountSpecified
        {
            get { return this.PaymentSchedulePaymentAmount != null; }
            set { }
        }
    
        [XmlElement("PaymentSchedulePaymentsCount", Order = 1)]
        public MISMOCount PaymentSchedulePaymentsCount { get; set; }
    
        [XmlIgnore]
        public bool PaymentSchedulePaymentsCountSpecified
        {
            get { return this.PaymentSchedulePaymentsCount != null; }
            set { }
        }
    
        [XmlElement("PaymentSchedulePaymentStartDate", Order = 2)]
        public MISMODate PaymentSchedulePaymentStartDate { get; set; }
    
        [XmlIgnore]
        public bool PaymentSchedulePaymentStartDateSpecified
        {
            get { return this.PaymentSchedulePaymentStartDate != null; }
            set { }
        }
    
        [XmlElement("PaymentSchedulePaymentVaryingToAmount", Order = 3)]
        public MISMOAmount PaymentSchedulePaymentVaryingToAmount { get; set; }
    
        [XmlIgnore]
        public bool PaymentSchedulePaymentVaryingToAmountSpecified
        {
            get { return this.PaymentSchedulePaymentVaryingToAmount != null; }
            set { }
        }
    
        [XmlElement("PaymentSchedulePrincipalAndInterestPaymentAmount", Order = 4)]
        public MISMOAmount PaymentSchedulePrincipalAndInterestPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool PaymentSchedulePrincipalAndInterestPaymentAmountSpecified
        {
            get { return this.PaymentSchedulePrincipalAndInterestPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public PAYMENT_SCHEDULE_ITEM_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
