namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class PAYMENT_SCHEDULE_ITEMS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PaymentScheduleItemListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PAYMENT_SCHEDULE_ITEM", Order = 0)]
        public List<PAYMENT_SCHEDULE_ITEM> PaymentScheduleItemList { get; set; } = new List<PAYMENT_SCHEDULE_ITEM>();
    
        [XmlIgnore]
        public bool PaymentScheduleItemListSpecified
        {
            get { return this.PaymentScheduleItemList != null && this.PaymentScheduleItemList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public PAYMENT_SCHEDULE_ITEMS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
