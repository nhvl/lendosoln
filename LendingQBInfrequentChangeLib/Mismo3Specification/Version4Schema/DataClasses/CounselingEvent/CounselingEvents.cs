namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class COUNSELING_EVENTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CounselingEventListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("COUNSELING_EVENT", Order = 0)]
        public List<COUNSELING_EVENT> CounselingEventList { get; set; } = new List<COUNSELING_EVENT>();
    
        [XmlIgnore]
        public bool CounselingEventListSpecified
        {
            get { return this.CounselingEventList != null && this.CounselingEventList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public COUNSELING_EVENTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
