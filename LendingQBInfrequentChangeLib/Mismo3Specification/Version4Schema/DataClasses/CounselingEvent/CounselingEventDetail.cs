namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class COUNSELING_EVENT_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CounselingCompletedDateSpecified
                    || this.CounselingConfirmationIndicatorSpecified
                    || this.CounselingDeliveredDateSpecified
                    || this.CounselingFormatTypeSpecified
                    || this.CounselingFormatTypeOtherDescriptionSpecified
                    || this.CounselingInitiatedDateSpecified
                    || this.CounselingProviderTypeSpecified
                    || this.CounselingProviderTypeOtherDescriptionSpecified
                    || this.CounselingSettingTypeSpecified
                    || this.CounselingSettingTypeOtherDescriptionSpecified
                    || this.CounselingTypeSpecified
                    || this.HousingCounselingCertificationIssuedDateSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CounselingCompletedDate", Order = 0)]
        public MISMODate CounselingCompletedDate { get; set; }
    
        [XmlIgnore]
        public bool CounselingCompletedDateSpecified
        {
            get { return this.CounselingCompletedDate != null; }
            set { }
        }
    
        [XmlElement("CounselingConfirmationIndicator", Order = 1)]
        public MISMOIndicator CounselingConfirmationIndicator { get; set; }
    
        [XmlIgnore]
        public bool CounselingConfirmationIndicatorSpecified
        {
            get { return this.CounselingConfirmationIndicator != null; }
            set { }
        }
    
        [XmlElement("CounselingDeliveredDate", Order = 2)]
        public MISMODate CounselingDeliveredDate { get; set; }
    
        [XmlIgnore]
        public bool CounselingDeliveredDateSpecified
        {
            get { return this.CounselingDeliveredDate != null; }
            set { }
        }
    
        [XmlElement("CounselingFormatType", Order = 3)]
        public MISMOEnum<CounselingFormatBase> CounselingFormatType { get; set; }
    
        [XmlIgnore]
        public bool CounselingFormatTypeSpecified
        {
            get { return this.CounselingFormatType != null; }
            set { }
        }
    
        [XmlElement("CounselingFormatTypeOtherDescription", Order = 4)]
        public MISMOString CounselingFormatTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CounselingFormatTypeOtherDescriptionSpecified
        {
            get { return this.CounselingFormatTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("CounselingInitiatedDate", Order = 5)]
        public MISMODate CounselingInitiatedDate { get; set; }
    
        [XmlIgnore]
        public bool CounselingInitiatedDateSpecified
        {
            get { return this.CounselingInitiatedDate != null; }
            set { }
        }
    
        [XmlElement("CounselingProviderType", Order = 6)]
        public MISMOEnum<CounselingProviderBase> CounselingProviderType { get; set; }
    
        [XmlIgnore]
        public bool CounselingProviderTypeSpecified
        {
            get { return this.CounselingProviderType != null; }
            set { }
        }
    
        [XmlElement("CounselingProviderTypeOtherDescription", Order = 7)]
        public MISMOString CounselingProviderTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CounselingProviderTypeOtherDescriptionSpecified
        {
            get { return this.CounselingProviderTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("CounselingSettingType", Order = 8)]
        public MISMOEnum<CounselingSettingBase> CounselingSettingType { get; set; }
    
        [XmlIgnore]
        public bool CounselingSettingTypeSpecified
        {
            get { return this.CounselingSettingType != null; }
            set { }
        }
    
        [XmlElement("CounselingSettingTypeOtherDescription", Order = 9)]
        public MISMOString CounselingSettingTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CounselingSettingTypeOtherDescriptionSpecified
        {
            get { return this.CounselingSettingTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("CounselingType", Order = 10)]
        public MISMOEnum<CounselingBase> CounselingType { get; set; }
    
        [XmlIgnore]
        public bool CounselingTypeSpecified
        {
            get { return this.CounselingType != null; }
            set { }
        }
    
        [XmlElement("HousingCounselingCertificationIssuedDate", Order = 11)]
        public MISMODate HousingCounselingCertificationIssuedDate { get; set; }
    
        [XmlIgnore]
        public bool HousingCounselingCertificationIssuedDateSpecified
        {
            get { return this.HousingCounselingCertificationIssuedDate != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 12)]
        public COUNSELING_EVENT_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
