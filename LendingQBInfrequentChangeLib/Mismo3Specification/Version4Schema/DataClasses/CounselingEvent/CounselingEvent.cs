namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class COUNSELING_EVENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CounselingEventDetailSpecified
                    || this.CounselingTopicsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("COUNSELING_EVENT_DETAIL", Order = 0)]
        public COUNSELING_EVENT_DETAIL CounselingEventDetail { get; set; }
    
        [XmlIgnore]
        public bool CounselingEventDetailSpecified
        {
            get { return this.CounselingEventDetail != null && this.CounselingEventDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("COUNSELING_TOPICS", Order = 1)]
        public COUNSELING_TOPICS CounselingTopics { get; set; }
    
        [XmlIgnore]
        public bool CounselingTopicsSpecified
        {
            get { return this.CounselingTopics != null && this.CounselingTopics.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public COUNSELING_EVENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
