namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class LOAN_PROGRAM
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LoanProgramAffordableIndicatorSpecified
                    || this.LoanProgramIdentifierSpecified
                    || this.LoanProgramSponsoringEntityNameSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LoanProgramAffordableIndicator", Order = 0)]
        public MISMOIndicator LoanProgramAffordableIndicator { get; set; }
    
        [XmlIgnore]
        public bool LoanProgramAffordableIndicatorSpecified
        {
            get { return this.LoanProgramAffordableIndicator != null; }
            set { }
        }
    
        [XmlElement("LoanProgramIdentifier", Order = 1)]
        public MISMOIdentifier LoanProgramIdentifier { get; set; }
    
        [XmlIgnore]
        public bool LoanProgramIdentifierSpecified
        {
            get { return this.LoanProgramIdentifier != null; }
            set { }
        }
    
        [XmlElement("LoanProgramSponsoringEntityName", Order = 2)]
        public MISMOString LoanProgramSponsoringEntityName { get; set; }
    
        [XmlIgnore]
        public bool LoanProgramSponsoringEntityNameSpecified
        {
            get { return this.LoanProgramSponsoringEntityName != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public LOAN_PROGRAM_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
