namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class LOAN_PROGRAMS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LoanProgramListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LOAN_PROGRAM", Order = 0)]
        public List<LOAN_PROGRAM> LoanProgramList { get; set; } = new List<LOAN_PROGRAM>();
    
        [XmlIgnore]
        public bool LoanProgramListSpecified
        {
            get { return this.LoanProgramList != null && this.LoanProgramList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public LOAN_PROGRAMS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
