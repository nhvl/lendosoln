namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class HMDA_RACE_DESIGNATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.HMDARaceDesignationTypeSpecified
                    || this.HMDARaceDesignationTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("HMDARaceDesignationType", Order = 0)]
        public MISMOEnum<HMDARaceDesignationBase> HMDARaceDesignationType { get; set; }
    
        [XmlIgnore]
        public bool HMDARaceDesignationTypeSpecified
        {
            get { return this.HMDARaceDesignationType != null; }
            set { }
        }
    
        [XmlElement("HMDARaceDesignationTypeOtherDescription", Order = 1)]
        public MISMOString HMDARaceDesignationTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool HMDARaceDesignationTypeOtherDescriptionSpecified
        {
            get { return this.HMDARaceDesignationTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public HMDA_RACE_DESIGNATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
