namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class HMDA_RACE_DESIGNATIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.HmdaRaceDesignationListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("HMDA_RACE_DESIGNATION", Order = 0)]
        public List<HMDA_RACE_DESIGNATION> HmdaRaceDesignationList { get; set; } = new List<HMDA_RACE_DESIGNATION>();
    
        [XmlIgnore]
        public bool HmdaRaceDesignationListSpecified
        {
            get { return this.HmdaRaceDesignationList != null && this.HmdaRaceDesignationList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public HMDA_RACE_DESIGNATIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
