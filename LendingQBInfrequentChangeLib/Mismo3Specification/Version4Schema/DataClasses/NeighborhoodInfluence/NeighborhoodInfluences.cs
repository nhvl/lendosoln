namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class NEIGHBORHOOD_INFLUENCES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.NeighborhoodInfluenceListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("NEIGHBORHOOD_INFLUENCE", Order = 0)]
        public List<NEIGHBORHOOD_INFLUENCE> NeighborhoodInfluenceList { get; set; } = new List<NEIGHBORHOOD_INFLUENCE>();
    
        [XmlIgnore]
        public bool NeighborhoodInfluenceListSpecified
        {
            get { return this.NeighborhoodInfluenceList != null && this.NeighborhoodInfluenceList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public NEIGHBORHOOD_INFLUENCES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
