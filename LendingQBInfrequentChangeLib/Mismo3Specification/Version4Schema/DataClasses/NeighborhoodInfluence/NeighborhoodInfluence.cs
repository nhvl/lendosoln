namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class NEIGHBORHOOD_INFLUENCE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ConditionRatingTypeSpecified
                    || this.InfluenceImpactTypeSpecified
                    || this.NeighborhoodInfluenceCommentDescriptionSpecified
                    || this.NeighborhoodInfluenceEstimatedDistanceTextSpecified
                    || this.NeighborhoodInfluenceTypeSpecified
                    || this.NeighborhoodInfluenceTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ConditionRatingType", Order = 0)]
        public MISMOEnum<ConditionRatingBase> ConditionRatingType { get; set; }
    
        [XmlIgnore]
        public bool ConditionRatingTypeSpecified
        {
            get { return this.ConditionRatingType != null; }
            set { }
        }
    
        [XmlElement("InfluenceImpactType", Order = 1)]
        public MISMOEnum<InfluenceImpactBase> InfluenceImpactType { get; set; }
    
        [XmlIgnore]
        public bool InfluenceImpactTypeSpecified
        {
            get { return this.InfluenceImpactType != null; }
            set { }
        }
    
        [XmlElement("NeighborhoodInfluenceCommentDescription", Order = 2)]
        public MISMOString NeighborhoodInfluenceCommentDescription { get; set; }
    
        [XmlIgnore]
        public bool NeighborhoodInfluenceCommentDescriptionSpecified
        {
            get { return this.NeighborhoodInfluenceCommentDescription != null; }
            set { }
        }
    
        [XmlElement("NeighborhoodInfluenceEstimatedDistanceText", Order = 3)]
        public MISMOString NeighborhoodInfluenceEstimatedDistanceText { get; set; }
    
        [XmlIgnore]
        public bool NeighborhoodInfluenceEstimatedDistanceTextSpecified
        {
            get { return this.NeighborhoodInfluenceEstimatedDistanceText != null; }
            set { }
        }
    
        [XmlElement("NeighborhoodInfluenceType", Order = 4)]
        public MISMOEnum<NeighborhoodInfluenceBase> NeighborhoodInfluenceType { get; set; }
    
        [XmlIgnore]
        public bool NeighborhoodInfluenceTypeSpecified
        {
            get { return this.NeighborhoodInfluenceType != null; }
            set { }
        }
    
        [XmlElement("NeighborhoodInfluenceTypeOtherDescription", Order = 5)]
        public MISMOString NeighborhoodInfluenceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool NeighborhoodInfluenceTypeOtherDescriptionSpecified
        {
            get { return this.NeighborhoodInfluenceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 6)]
        public NEIGHBORHOOD_INFLUENCE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
