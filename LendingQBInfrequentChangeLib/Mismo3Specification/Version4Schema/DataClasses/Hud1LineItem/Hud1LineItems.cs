namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class HUD1_LINE_ITEMS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.Hud1LineItemListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("HUD1_LINE_ITEM", Order = 0)]
        public List<HUD1_LINE_ITEM> Hud1LineItemList { get; set; } = new List<HUD1_LINE_ITEM>();
    
        [XmlIgnore]
        public bool Hud1LineItemListSpecified
        {
            get { return this.Hud1LineItemList != null && this.Hud1LineItemList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public HUD1_LINE_ITEMS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
