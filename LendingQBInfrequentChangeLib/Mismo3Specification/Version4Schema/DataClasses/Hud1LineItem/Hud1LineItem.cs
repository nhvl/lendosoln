namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class HUD1_LINE_ITEM
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.HUD1LineItemAdditionalTextDescriptionSpecified
                    || this.HUD1LineItemAmountSpecified
                    || this.HUD1LineItemDescriptionSpecified
                    || this.HUD1LineItemFromDateSpecified
                    || this.HUD1LineItemPaidByTypeSpecified
                    || this.HUD1LineItemToDateSpecified
                    || this.HUD1SpecifiedLineNumberValueSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("HUD1LineItemAdditionalTextDescription", Order = 0)]
        public MISMOString HUD1LineItemAdditionalTextDescription { get; set; }
    
        [XmlIgnore]
        public bool HUD1LineItemAdditionalTextDescriptionSpecified
        {
            get { return this.HUD1LineItemAdditionalTextDescription != null; }
            set { }
        }
    
        [XmlElement("HUD1LineItemAmount", Order = 1)]
        public MISMOAmount HUD1LineItemAmount { get; set; }
    
        [XmlIgnore]
        public bool HUD1LineItemAmountSpecified
        {
            get { return this.HUD1LineItemAmount != null; }
            set { }
        }
    
        [XmlElement("HUD1LineItemDescription", Order = 2)]
        public MISMOString HUD1LineItemDescription { get; set; }
    
        [XmlIgnore]
        public bool HUD1LineItemDescriptionSpecified
        {
            get { return this.HUD1LineItemDescription != null; }
            set { }
        }
    
        [XmlElement("HUD1LineItemFromDate", Order = 3)]
        public MISMODate HUD1LineItemFromDate { get; set; }
    
        [XmlIgnore]
        public bool HUD1LineItemFromDateSpecified
        {
            get { return this.HUD1LineItemFromDate != null; }
            set { }
        }
    
        [XmlElement("HUD1LineItemPaidByType", Order = 4)]
        public MISMOEnum<HUD1LineItemPaidByBase> HUD1LineItemPaidByType { get; set; }
    
        [XmlIgnore]
        public bool HUD1LineItemPaidByTypeSpecified
        {
            get { return this.HUD1LineItemPaidByType != null; }
            set { }
        }
    
        [XmlElement("HUD1LineItemToDate", Order = 5)]
        public MISMODate HUD1LineItemToDate { get; set; }
    
        [XmlIgnore]
        public bool HUD1LineItemToDateSpecified
        {
            get { return this.HUD1LineItemToDate != null; }
            set { }
        }
    
        [XmlElement("HUD1SpecifiedLineNumberValue", Order = 6)]
        public MISMOValue HUD1SpecifiedLineNumberValue { get; set; }
    
        [XmlIgnore]
        public bool HUD1SpecifiedLineNumberValueSpecified
        {
            get { return this.HUD1SpecifiedLineNumberValue != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 7)]
        public HUD1_LINE_ITEM_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
