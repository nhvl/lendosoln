namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class DOCUMENT_SET_USAGES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DocumentSetUsageListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DOCUMENT_SET_USAGE", Order = 0)]
        public List<DOCUMENT_SET_USAGE> DocumentSetUsageList { get; set; } = new List<DOCUMENT_SET_USAGE>();
    
        [XmlIgnore]
        public bool DocumentSetUsageListSpecified
        {
            get { return this.DocumentSetUsageList != null && this.DocumentSetUsageList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public DOCUMENT_SET_USAGES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
