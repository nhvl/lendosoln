namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class DOCUMENT_SET_USAGE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DocumentSetUsageCodeSpecified
                    || this.DocumentSetUsageDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DocumentSetUsageCode", Order = 0)]
        public MISMOCode DocumentSetUsageCode { get; set; }
    
        [XmlIgnore]
        public bool DocumentSetUsageCodeSpecified
        {
            get { return this.DocumentSetUsageCode != null; }
            set { }
        }
    
        [XmlElement("DocumentSetUsageDescription", Order = 1)]
        public MISMOString DocumentSetUsageDescription { get; set; }
    
        [XmlIgnore]
        public bool DocumentSetUsageDescriptionSpecified
        {
            get { return this.DocumentSetUsageDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public DOCUMENT_SET_USAGE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
