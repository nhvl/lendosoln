namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class NOTE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AllongeToNoteSpecified
                    || this.NoteAddendumsSpecified
                    || this.NoteRidersSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ALLONGE_TO_NOTE", Order = 0)]
        public ALLONGE_TO_NOTE AllongeToNote { get; set; }
    
        [XmlIgnore]
        public bool AllongeToNoteSpecified
        {
            get { return this.AllongeToNote != null && this.AllongeToNote.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("NOTE_ADDENDUMS", Order = 1)]
        public NOTE_ADDENDUMS NoteAddendums { get; set; }
    
        [XmlIgnore]
        public bool NoteAddendumsSpecified
        {
            get { return this.NoteAddendums != null && this.NoteAddendums.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("NOTE_RIDERS", Order = 2)]
        public NOTE_RIDERS NoteRiders { get; set; }
    
        [XmlIgnore]
        public bool NoteRidersSpecified
        {
            get { return this.NoteRiders != null && this.NoteRiders.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public NOTE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
