namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class GOVERNMENT_BORROWER
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CAIVRSIdentifierSpecified
                    || this.FHABorrowerCertificationLeadPaintIndicatorSpecified
                    || this.FHABorrowerCertificationOriginalMortgageAmountSpecified
                    || this.FHABorrowerCertificationOwnFourOrMoreDwellingsIndicatorSpecified
                    || this.FHABorrowerCertificationOwnOtherPropertyIndicatorSpecified
                    || this.FHABorrowerCertificationPropertySoldCityNameSpecified
                    || this.FHABorrowerCertificationPropertySoldPostalCodeSpecified
                    || this.FHABorrowerCertificationPropertySoldStateNameSpecified
                    || this.FHABorrowerCertificationPropertySoldStreetAddressLineTextSpecified
                    || this.FHABorrowerCertificationPropertyToBeSoldIndicatorSpecified
                    || this.FHABorrowerCertificationRentalIndicatorSpecified
                    || this.FHABorrowerCertificationSalesPriceAmountSpecified
                    || this.FHA_VABorrowerCertificationSalesPriceExceedsAppraisedValueTypeSpecified
                    || this.VABorrowerCertificationOccupancyTypeSpecified
                    || this.VABorrowerSurvivingSpouseIndicatorSpecified
                    || this.VACoBorrowerNonTaxableIncomeAmountSpecified
                    || this.VACoBorrowerTaxableIncomeAmountSpecified
                    || this.VAFederalTaxAmountSpecified
                    || this.VALocalTaxAmountSpecified
                    || this.VAPrimaryBorrowerNonTaxableIncomeAmountSpecified
                    || this.VAPrimaryBorrowerTaxableIncomeAmountSpecified
                    || this.VASocialSecurityTaxAmountSpecified
                    || this.VAStateTaxAmountSpecified
                    || this.VeteranStatusIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CAIVRSIdentifier", Order = 0)]
        public MISMOIdentifier CAIVRSIdentifier { get; set; }
    
        [XmlIgnore]
        public bool CAIVRSIdentifierSpecified
        {
            get { return this.CAIVRSIdentifier != null; }
            set { }
        }
    
        [XmlElement("FHABorrowerCertificationLeadPaintIndicator", Order = 1)]
        public MISMOIndicator FHABorrowerCertificationLeadPaintIndicator { get; set; }
    
        [XmlIgnore]
        public bool FHABorrowerCertificationLeadPaintIndicatorSpecified
        {
            get { return this.FHABorrowerCertificationLeadPaintIndicator != null; }
            set { }
        }
    
        [XmlElement("FHABorrowerCertificationOriginalMortgageAmount", Order = 2)]
        public MISMOAmount FHABorrowerCertificationOriginalMortgageAmount { get; set; }
    
        [XmlIgnore]
        public bool FHABorrowerCertificationOriginalMortgageAmountSpecified
        {
            get { return this.FHABorrowerCertificationOriginalMortgageAmount != null; }
            set { }
        }
    
        [XmlElement("FHABorrowerCertificationOwnFourOrMoreDwellingsIndicator", Order = 3)]
        public MISMOIndicator FHABorrowerCertificationOwnFourOrMoreDwellingsIndicator { get; set; }
    
        [XmlIgnore]
        public bool FHABorrowerCertificationOwnFourOrMoreDwellingsIndicatorSpecified
        {
            get { return this.FHABorrowerCertificationOwnFourOrMoreDwellingsIndicator != null; }
            set { }
        }
    
        [XmlElement("FHABorrowerCertificationOwnOtherPropertyIndicator", Order = 4)]
        public MISMOIndicator FHABorrowerCertificationOwnOtherPropertyIndicator { get; set; }
    
        [XmlIgnore]
        public bool FHABorrowerCertificationOwnOtherPropertyIndicatorSpecified
        {
            get { return this.FHABorrowerCertificationOwnOtherPropertyIndicator != null; }
            set { }
        }
    
        [XmlElement("FHABorrowerCertificationPropertySoldCityName", Order = 5)]
        public MISMOString FHABorrowerCertificationPropertySoldCityName { get; set; }
    
        [XmlIgnore]
        public bool FHABorrowerCertificationPropertySoldCityNameSpecified
        {
            get { return this.FHABorrowerCertificationPropertySoldCityName != null; }
            set { }
        }
    
        [XmlElement("FHABorrowerCertificationPropertySoldPostalCode", Order = 6)]
        public MISMOCode FHABorrowerCertificationPropertySoldPostalCode { get; set; }
    
        [XmlIgnore]
        public bool FHABorrowerCertificationPropertySoldPostalCodeSpecified
        {
            get { return this.FHABorrowerCertificationPropertySoldPostalCode != null; }
            set { }
        }
    
        [XmlElement("FHABorrowerCertificationPropertySoldStateName", Order = 7)]
        public MISMOString FHABorrowerCertificationPropertySoldStateName { get; set; }
    
        [XmlIgnore]
        public bool FHABorrowerCertificationPropertySoldStateNameSpecified
        {
            get { return this.FHABorrowerCertificationPropertySoldStateName != null; }
            set { }
        }
    
        [XmlElement("FHABorrowerCertificationPropertySoldStreetAddressLineText", Order = 8)]
        public MISMOString FHABorrowerCertificationPropertySoldStreetAddressLineText { get; set; }
    
        [XmlIgnore]
        public bool FHABorrowerCertificationPropertySoldStreetAddressLineTextSpecified
        {
            get { return this.FHABorrowerCertificationPropertySoldStreetAddressLineText != null; }
            set { }
        }
    
        [XmlElement("FHABorrowerCertificationPropertyToBeSoldIndicator", Order = 9)]
        public MISMOIndicator FHABorrowerCertificationPropertyToBeSoldIndicator { get; set; }
    
        [XmlIgnore]
        public bool FHABorrowerCertificationPropertyToBeSoldIndicatorSpecified
        {
            get { return this.FHABorrowerCertificationPropertyToBeSoldIndicator != null; }
            set { }
        }
    
        [XmlElement("FHABorrowerCertificationRentalIndicator", Order = 10)]
        public MISMOIndicator FHABorrowerCertificationRentalIndicator { get; set; }
    
        [XmlIgnore]
        public bool FHABorrowerCertificationRentalIndicatorSpecified
        {
            get { return this.FHABorrowerCertificationRentalIndicator != null; }
            set { }
        }
    
        [XmlElement("FHABorrowerCertificationSalesPriceAmount", Order = 11)]
        public MISMOAmount FHABorrowerCertificationSalesPriceAmount { get; set; }
    
        [XmlIgnore]
        public bool FHABorrowerCertificationSalesPriceAmountSpecified
        {
            get { return this.FHABorrowerCertificationSalesPriceAmount != null; }
            set { }
        }
    
        [XmlElement("FHA_VABorrowerCertificationSalesPriceExceedsAppraisedValueType", Order = 12)]
        public MISMOEnum<FHA_VABorrowerCertificationSalesPriceExceedsAppraisedValueBase> FHA_VABorrowerCertificationSalesPriceExceedsAppraisedValueType { get; set; }
    
        [XmlIgnore]
        public bool FHA_VABorrowerCertificationSalesPriceExceedsAppraisedValueTypeSpecified
        {
            get { return this.FHA_VABorrowerCertificationSalesPriceExceedsAppraisedValueType != null; }
            set { }
        }
    
        [XmlElement("VABorrowerCertificationOccupancyType", Order = 13)]
        public MISMOEnum<VABorrowerCertificationOccupancyBase> VABorrowerCertificationOccupancyType { get; set; }
    
        [XmlIgnore]
        public bool VABorrowerCertificationOccupancyTypeSpecified
        {
            get { return this.VABorrowerCertificationOccupancyType != null; }
            set { }
        }
    
        [XmlElement("VABorrowerSurvivingSpouseIndicator", Order = 14)]
        public MISMOIndicator VABorrowerSurvivingSpouseIndicator { get; set; }
    
        [XmlIgnore]
        public bool VABorrowerSurvivingSpouseIndicatorSpecified
        {
            get { return this.VABorrowerSurvivingSpouseIndicator != null; }
            set { }
        }
    
        [XmlElement("VACoBorrowerNonTaxableIncomeAmount", Order = 15)]
        public MISMOAmount VACoBorrowerNonTaxableIncomeAmount { get; set; }
    
        [XmlIgnore]
        public bool VACoBorrowerNonTaxableIncomeAmountSpecified
        {
            get { return this.VACoBorrowerNonTaxableIncomeAmount != null; }
            set { }
        }
    
        [XmlElement("VACoBorrowerTaxableIncomeAmount", Order = 16)]
        public MISMOAmount VACoBorrowerTaxableIncomeAmount { get; set; }
    
        [XmlIgnore]
        public bool VACoBorrowerTaxableIncomeAmountSpecified
        {
            get { return this.VACoBorrowerTaxableIncomeAmount != null; }
            set { }
        }
    
        [XmlElement("VAFederalTaxAmount", Order = 17)]
        public MISMOAmount VAFederalTaxAmount { get; set; }
    
        [XmlIgnore]
        public bool VAFederalTaxAmountSpecified
        {
            get { return this.VAFederalTaxAmount != null; }
            set { }
        }
    
        [XmlElement("VALocalTaxAmount", Order = 18)]
        public MISMOAmount VALocalTaxAmount { get; set; }
    
        [XmlIgnore]
        public bool VALocalTaxAmountSpecified
        {
            get { return this.VALocalTaxAmount != null; }
            set { }
        }
    
        [XmlElement("VAPrimaryBorrowerNonTaxableIncomeAmount", Order = 19)]
        public MISMOAmount VAPrimaryBorrowerNonTaxableIncomeAmount { get; set; }
    
        [XmlIgnore]
        public bool VAPrimaryBorrowerNonTaxableIncomeAmountSpecified
        {
            get { return this.VAPrimaryBorrowerNonTaxableIncomeAmount != null; }
            set { }
        }
    
        [XmlElement("VAPrimaryBorrowerTaxableIncomeAmount", Order = 20)]
        public MISMOAmount VAPrimaryBorrowerTaxableIncomeAmount { get; set; }
    
        [XmlIgnore]
        public bool VAPrimaryBorrowerTaxableIncomeAmountSpecified
        {
            get { return this.VAPrimaryBorrowerTaxableIncomeAmount != null; }
            set { }
        }
    
        [XmlElement("VASocialSecurityTaxAmount", Order = 21)]
        public MISMOAmount VASocialSecurityTaxAmount { get; set; }
    
        [XmlIgnore]
        public bool VASocialSecurityTaxAmountSpecified
        {
            get { return this.VASocialSecurityTaxAmount != null; }
            set { }
        }
    
        [XmlElement("VAStateTaxAmount", Order = 22)]
        public MISMOAmount VAStateTaxAmount { get; set; }
    
        [XmlIgnore]
        public bool VAStateTaxAmountSpecified
        {
            get { return this.VAStateTaxAmount != null; }
            set { }
        }
    
        [XmlElement("VeteranStatusIndicator", Order = 23)]
        public MISMOIndicator VeteranStatusIndicator { get; set; }
    
        [XmlIgnore]
        public bool VeteranStatusIndicatorSpecified
        {
            get { return this.VeteranStatusIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 24)]
        public GOVERNMENT_BORROWER_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
