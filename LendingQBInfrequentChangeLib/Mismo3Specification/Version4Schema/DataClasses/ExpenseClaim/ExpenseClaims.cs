namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class EXPENSE_CLAIMS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExpenseClaimListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("EXPENSE_CLAIM", Order = 0)]
        public List<EXPENSE_CLAIM> ExpenseClaimList { get; set; } = new List<EXPENSE_CLAIM>();
    
        [XmlIgnore]
        public bool ExpenseClaimListSpecified
        {
            get { return this.ExpenseClaimList != null && this.ExpenseClaimList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public EXPENSE_CLAIMS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
