namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class EXPENSE_CLAIM
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExpenseClaimDetailSpecified
                    || this.ExpenseClaimItemsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("EXPENSE_CLAIM_DETAIL", Order = 0)]
        public EXPENSE_CLAIM_DETAIL ExpenseClaimDetail { get; set; }
    
        [XmlIgnore]
        public bool ExpenseClaimDetailSpecified
        {
            get { return this.ExpenseClaimDetail != null && this.ExpenseClaimDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXPENSE_CLAIM_ITEMS", Order = 1)]
        public EXPENSE_CLAIM_ITEMS ExpenseClaimItems { get; set; }
    
        [XmlIgnore]
        public bool ExpenseClaimItemsSpecified
        {
            get { return this.ExpenseClaimItems != null && this.ExpenseClaimItems.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public EXPENSE_CLAIM_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
