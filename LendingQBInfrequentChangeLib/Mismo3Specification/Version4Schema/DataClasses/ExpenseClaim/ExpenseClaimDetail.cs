namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class EXPENSE_CLAIM_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExpenseClaimCommentTextSpecified
                    || this.ExpenseClaimFormIdentifierSpecified
                    || this.ExpenseClaimIdentifierSpecified
                    || this.ExpenseClaimSubmissionIdentifierSpecified
                    || this.ExpenseClaimSubmissionReasonTypeSpecified
                    || this.ExpenseClaimSubmissionReasonTypeOtherDescriptionSpecified
                    || this.ExpenseClaimSubmissionTypeSpecified
                    || this.ExpenseClaimSubmissionTypeOtherDescriptionSpecified
                    || this.ExpenseClaimTotalAmountSpecified
                    || this.ExpenseClaimTotalNonReimbursableAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ExpenseClaimCommentText", Order = 0)]
        public MISMOString ExpenseClaimCommentText { get; set; }
    
        [XmlIgnore]
        public bool ExpenseClaimCommentTextSpecified
        {
            get { return this.ExpenseClaimCommentText != null; }
            set { }
        }
    
        [XmlElement("ExpenseClaimFormIdentifier", Order = 1)]
        public MISMOIdentifier ExpenseClaimFormIdentifier { get; set; }
    
        [XmlIgnore]
        public bool ExpenseClaimFormIdentifierSpecified
        {
            get { return this.ExpenseClaimFormIdentifier != null; }
            set { }
        }
    
        [XmlElement("ExpenseClaimIdentifier", Order = 2)]
        public MISMOIdentifier ExpenseClaimIdentifier { get; set; }
    
        [XmlIgnore]
        public bool ExpenseClaimIdentifierSpecified
        {
            get { return this.ExpenseClaimIdentifier != null; }
            set { }
        }
    
        [XmlElement("ExpenseClaimSubmissionIdentifier", Order = 3)]
        public MISMOIdentifier ExpenseClaimSubmissionIdentifier { get; set; }
    
        [XmlIgnore]
        public bool ExpenseClaimSubmissionIdentifierSpecified
        {
            get { return this.ExpenseClaimSubmissionIdentifier != null; }
            set { }
        }
    
        [XmlElement("ExpenseClaimSubmissionReasonType", Order = 4)]
        public MISMOEnum<ExpenseClaimSubmissionReasonBase> ExpenseClaimSubmissionReasonType { get; set; }
    
        [XmlIgnore]
        public bool ExpenseClaimSubmissionReasonTypeSpecified
        {
            get { return this.ExpenseClaimSubmissionReasonType != null; }
            set { }
        }
    
        [XmlElement("ExpenseClaimSubmissionReasonTypeOtherDescription", Order = 5)]
        public MISMOString ExpenseClaimSubmissionReasonTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ExpenseClaimSubmissionReasonTypeOtherDescriptionSpecified
        {
            get { return this.ExpenseClaimSubmissionReasonTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ExpenseClaimSubmissionType", Order = 6)]
        public MISMOEnum<ExpenseClaimSubmissionBase> ExpenseClaimSubmissionType { get; set; }
    
        [XmlIgnore]
        public bool ExpenseClaimSubmissionTypeSpecified
        {
            get { return this.ExpenseClaimSubmissionType != null; }
            set { }
        }
    
        [XmlElement("ExpenseClaimSubmissionTypeOtherDescription", Order = 7)]
        public MISMOString ExpenseClaimSubmissionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ExpenseClaimSubmissionTypeOtherDescriptionSpecified
        {
            get { return this.ExpenseClaimSubmissionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ExpenseClaimTotalAmount", Order = 8)]
        public MISMOAmount ExpenseClaimTotalAmount { get; set; }
    
        [XmlIgnore]
        public bool ExpenseClaimTotalAmountSpecified
        {
            get { return this.ExpenseClaimTotalAmount != null; }
            set { }
        }
    
        [XmlElement("ExpenseClaimTotalNonReimbursableAmount", Order = 9)]
        public MISMOAmount ExpenseClaimTotalNonReimbursableAmount { get; set; }
    
        [XmlIgnore]
        public bool ExpenseClaimTotalNonReimbursableAmountSpecified
        {
            get { return this.ExpenseClaimTotalNonReimbursableAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 10)]
        public EXPENSE_CLAIM_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
