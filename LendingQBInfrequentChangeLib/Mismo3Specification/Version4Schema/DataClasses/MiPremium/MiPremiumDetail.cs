namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MI_PREMIUM_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MIPremiumAnnualPaymentAmountSpecified
                    || this.MIPremiumCalculationTypeSpecified
                    || this.MIPremiumCalculationTypeOtherDescriptionSpecified
                    || this.MIPremiumMonthlyPaymentAmountSpecified
                    || this.MIPremiumMonthlyPaymentRoundingTypeSpecified
                    || this.MIPremiumMonthlyPaymentRoundingTypeOtherDescriptionSpecified
                    || this.MIPremiumPeriodTypeSpecified
                    || this.MIPremiumPeriodTypeOtherDescriptionSpecified
                    || this.MIPremiumRateDurationMonthsCountSpecified
                    || this.MIPremiumRatePercentSpecified
                    || this.MIPremiumSequenceTypeSpecified
                    || this.MIPremiumUpfrontPercentSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MIPremiumAnnualPaymentAmount", Order = 0)]
        public MISMOAmount MIPremiumAnnualPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumAnnualPaymentAmountSpecified
        {
            get { return this.MIPremiumAnnualPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("MIPremiumCalculationType", Order = 1)]
        public MISMOEnum<MIPremiumCalculationBase> MIPremiumCalculationType { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumCalculationTypeSpecified
        {
            get { return this.MIPremiumCalculationType != null; }
            set { }
        }
    
        [XmlElement("MIPremiumCalculationTypeOtherDescription", Order = 2)]
        public MISMOString MIPremiumCalculationTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumCalculationTypeOtherDescriptionSpecified
        {
            get { return this.MIPremiumCalculationTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MIPremiumMonthlyPaymentAmount", Order = 3)]
        public MISMOAmount MIPremiumMonthlyPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumMonthlyPaymentAmountSpecified
        {
            get { return this.MIPremiumMonthlyPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("MIPremiumMonthlyPaymentRoundingType", Order = 4)]
        public MISMOEnum<MIPremiumMonthlyPaymentRoundingBase> MIPremiumMonthlyPaymentRoundingType { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumMonthlyPaymentRoundingTypeSpecified
        {
            get { return this.MIPremiumMonthlyPaymentRoundingType != null; }
            set { }
        }
    
        [XmlElement("MIPremiumMonthlyPaymentRoundingTypeOtherDescription", Order = 5)]
        public MISMOString MIPremiumMonthlyPaymentRoundingTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumMonthlyPaymentRoundingTypeOtherDescriptionSpecified
        {
            get { return this.MIPremiumMonthlyPaymentRoundingTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MIPremiumPeriodType", Order = 6)]
        public MISMOEnum<MIPremiumPeriodBase> MIPremiumPeriodType { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumPeriodTypeSpecified
        {
            get { return this.MIPremiumPeriodType != null; }
            set { }
        }
    
        [XmlElement("MIPremiumPeriodTypeOtherDescription", Order = 7)]
        public MISMOString MIPremiumPeriodTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumPeriodTypeOtherDescriptionSpecified
        {
            get { return this.MIPremiumPeriodTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MIPremiumRateDurationMonthsCount", Order = 8)]
        public MISMOCount MIPremiumRateDurationMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumRateDurationMonthsCountSpecified
        {
            get { return this.MIPremiumRateDurationMonthsCount != null; }
            set { }
        }
    
        [XmlElement("MIPremiumRatePercent", Order = 9)]
        public MISMOPercent MIPremiumRatePercent { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumRatePercentSpecified
        {
            get { return this.MIPremiumRatePercent != null; }
            set { }
        }
    
        [XmlElement("MIPremiumSequenceType", Order = 10)]
        public MISMOEnum<MIPremiumSequenceBase> MIPremiumSequenceType { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumSequenceTypeSpecified
        {
            get { return this.MIPremiumSequenceType != null; }
            set { }
        }
    
        [XmlElement("MIPremiumUpfrontPercent", Order = 11)]
        public MISMOPercent MIPremiumUpfrontPercent { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumUpfrontPercentSpecified
        {
            get { return this.MIPremiumUpfrontPercent != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 12)]
        public MI_PREMIUM_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
