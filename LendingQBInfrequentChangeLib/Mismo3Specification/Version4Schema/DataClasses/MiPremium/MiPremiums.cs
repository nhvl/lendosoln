namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class MI_PREMIUMS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MiPremiumListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MI_PREMIUM", Order = 0)]
        public List<MI_PREMIUM> MiPremiumList { get; set; } = new List<MI_PREMIUM>();
    
        [XmlIgnore]
        public bool MiPremiumListSpecified
        {
            get { return this.MiPremiumList != null && this.MiPremiumList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public MI_PREMIUMS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
