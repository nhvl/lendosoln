namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MI_PREMIUM
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MiPremiumDetailSpecified
                    || this.MiPremiumPaymentsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MI_PREMIUM_DETAIL", Order = 0)]
        public MI_PREMIUM_DETAIL MiPremiumDetail { get; set; }
    
        [XmlIgnore]
        public bool MiPremiumDetailSpecified
        {
            get { return this.MiPremiumDetail != null && this.MiPremiumDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("MI_PREMIUM_PAYMENTS", Order = 1)]
        public MI_PREMIUM_PAYMENTS MiPremiumPayments { get; set; }
    
        [XmlIgnore]
        public bool MiPremiumPaymentsSpecified
        {
            get { return this.MiPremiumPayments != null && this.MiPremiumPayments.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public MI_PREMIUM_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
