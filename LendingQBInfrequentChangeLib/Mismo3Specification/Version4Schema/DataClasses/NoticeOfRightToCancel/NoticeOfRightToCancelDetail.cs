namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class NOTICE_OF_RIGHT_TO_CANCEL_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.NoticeOfRightToCancelTransactionDateSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("NoticeOfRightToCancelTransactionDate", Order = 0)]
        public MISMODate NoticeOfRightToCancelTransactionDate { get; set; }
    
        [XmlIgnore]
        public bool NoticeOfRightToCancelTransactionDateSpecified
        {
            get { return this.NoticeOfRightToCancelTransactionDate != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public NOTICE_OF_RIGHT_TO_CANCEL_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
