namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class NOTICE_OF_RIGHT_TO_CANCEL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.NoticeOfRightToCancelDetailSpecified
                    || this.RightToCancelContactSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("NOTICE_OF_RIGHT_TO_CANCEL_DETAIL", Order = 0)]
        public NOTICE_OF_RIGHT_TO_CANCEL_DETAIL NoticeOfRightToCancelDetail { get; set; }
    
        [XmlIgnore]
        public bool NoticeOfRightToCancelDetailSpecified
        {
            get { return this.NoticeOfRightToCancelDetail != null && this.NoticeOfRightToCancelDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("RIGHT_TO_CANCEL_CONTACT", Order = 1)]
        public RIGHT_TO_CANCEL_CONTACT RightToCancelContact { get; set; }
    
        [XmlIgnore]
        public bool RightToCancelContactSpecified
        {
            get { return this.RightToCancelContact != null && this.RightToCancelContact.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public NOTICE_OF_RIGHT_TO_CANCEL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
