namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class CREDIT_TRADE_REFERENCES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditTradeReferenceListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CREDIT_TRADE_REFERENCE", Order = 0)]
        public List<CREDIT_TRADE_REFERENCE> CreditTradeReferenceList { get; set; } = new List<CREDIT_TRADE_REFERENCE>();
    
        [XmlIgnore]
        public bool CreditTradeReferenceListSpecified
        {
            get { return this.CreditTradeReferenceList != null && this.CreditTradeReferenceList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public CREDIT_TRADE_REFERENCES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
