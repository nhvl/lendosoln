namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ENERGY_EFFICIENCY_CONSIDERATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EnergyEfficiencyConsiderationDetailSpecified
                    || this.EnergyEfficiencyFinancialIncentivesSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ENERGY_EFFICIENCY_CONSIDERATION_DETAIL", Order = 0)]
        public ENERGY_EFFICIENCY_CONSIDERATION_DETAIL EnergyEfficiencyConsiderationDetail { get; set; }
    
        [XmlIgnore]
        public bool EnergyEfficiencyConsiderationDetailSpecified
        {
            get { return this.EnergyEfficiencyConsiderationDetail != null && this.EnergyEfficiencyConsiderationDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("ENERGY_EFFICIENCY_FINANCIAL_INCENTIVES", Order = 1)]
        public ENERGY_EFFICIENCY_FINANCIAL_INCENTIVES EnergyEfficiencyFinancialIncentives { get; set; }
    
        [XmlIgnore]
        public bool EnergyEfficiencyFinancialIncentivesSpecified
        {
            get { return this.EnergyEfficiencyFinancialIncentives != null && this.EnergyEfficiencyFinancialIncentives.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public ENERGY_EFFICIENCY_CONSIDERATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
