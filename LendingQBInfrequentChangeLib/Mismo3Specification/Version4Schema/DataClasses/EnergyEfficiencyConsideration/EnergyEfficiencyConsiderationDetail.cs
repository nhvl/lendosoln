namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ENERGY_EFFICIENCY_CONSIDERATION_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DayLightingEnergyEfficiencyItemTypeSpecified
                    || this.DayLightingEnergyEfficiencyItemTypeOtherDescriptionSpecified
                    || this.EnergyEfficientItemsDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DayLightingEnergyEfficiencyItemType", Order = 0)]
        public MISMOEnum<DayLightingEnergyEfficiencyItemBase> DayLightingEnergyEfficiencyItemType { get; set; }
    
        [XmlIgnore]
        public bool DayLightingEnergyEfficiencyItemTypeSpecified
        {
            get { return this.DayLightingEnergyEfficiencyItemType != null; }
            set { }
        }
    
        [XmlElement("DayLightingEnergyEfficiencyItemTypeOtherDescription", Order = 1)]
        public MISMOString DayLightingEnergyEfficiencyItemTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool DayLightingEnergyEfficiencyItemTypeOtherDescriptionSpecified
        {
            get { return this.DayLightingEnergyEfficiencyItemTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EnergyEfficientItemsDescription", Order = 2)]
        public MISMOString EnergyEfficientItemsDescription { get; set; }
    
        [XmlIgnore]
        public bool EnergyEfficientItemsDescriptionSpecified
        {
            get { return this.EnergyEfficientItemsDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public ENERGY_EFFICIENCY_CONSIDERATION_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
