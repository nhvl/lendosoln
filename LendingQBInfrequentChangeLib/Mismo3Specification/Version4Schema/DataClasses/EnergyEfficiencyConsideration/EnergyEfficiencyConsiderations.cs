namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class ENERGY_EFFICIENCY_CONSIDERATIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EnergyEfficiencyConsiderationListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ENERGY_EFFICIENCY_CONSIDERATION", Order = 0)]
        public List<ENERGY_EFFICIENCY_CONSIDERATION> EnergyEfficiencyConsiderationList { get; set; } = new List<ENERGY_EFFICIENCY_CONSIDERATION>();
    
        [XmlIgnore]
        public bool EnergyEfficiencyConsiderationListSpecified
        {
            get { return this.EnergyEfficiencyConsiderationList != null && this.EnergyEfficiencyConsiderationList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public ENERGY_EFFICIENCY_CONSIDERATIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
