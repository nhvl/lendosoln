namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class INTEGRATED_DISCLOSURE_SUBSECTION_PAYMENTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.IntegratedDisclosureSubsectionPaymentListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("INTEGRATED_DISCLOSURE_SUBSECTION_PAYMENT", Order = 0)]
        public List<INTEGRATED_DISCLOSURE_SUBSECTION_PAYMENT> IntegratedDisclosureSubsectionPaymentList { get; set; } = new List<INTEGRATED_DISCLOSURE_SUBSECTION_PAYMENT>();
    
        [XmlIgnore]
        public bool IntegratedDisclosureSubsectionPaymentListSpecified
        {
            get { return this.IntegratedDisclosureSubsectionPaymentList != null && this.IntegratedDisclosureSubsectionPaymentList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public INTEGRATED_DISCLOSURE_SUBSECTION_PAYMENTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
