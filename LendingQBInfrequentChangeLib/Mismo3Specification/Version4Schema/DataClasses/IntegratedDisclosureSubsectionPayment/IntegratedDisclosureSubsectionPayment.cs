namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class INTEGRATED_DISCLOSURE_SUBSECTION_PAYMENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.IntegratedDisclosureSubsectionPaidByTypeSpecified
                    || this.IntegratedDisclosureSubsectionPaymentAmountSpecified
                    || this.IntegratedDisclosureSubsectionPaymentTimingTypeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("IntegratedDisclosureSubsectionPaidByType", Order = 0)]
        public MISMOEnum<IntegratedDisclosureSubsectionPaidByBase> IntegratedDisclosureSubsectionPaidByType { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureSubsectionPaidByTypeSpecified
        {
            get { return this.IntegratedDisclosureSubsectionPaidByType != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureSubsectionPaymentAmount", Order = 1)]
        public MISMOAmount IntegratedDisclosureSubsectionPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureSubsectionPaymentAmountSpecified
        {
            get { return this.IntegratedDisclosureSubsectionPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureSubsectionPaymentTimingType", Order = 2)]
        public MISMOEnum<IntegratedDisclosureSubsectionPaymentTimingBase> IntegratedDisclosureSubsectionPaymentTimingType { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureSubsectionPaymentTimingTypeSpecified
        {
            get { return this.IntegratedDisclosureSubsectionPaymentTimingType != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public INTEGRATED_DISCLOSURE_SUBSECTION_PAYMENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
