namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class HOUSING
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.NeighborhoodBuiltUpRangeTypeSpecified
                    || this.NeighborhoodDemandSupplyTypeSpecified
                    || this.NeighborhoodHousingHighPriceAmountSpecified
                    || this.NeighborhoodHousingLowPriceAmountSpecified
                    || this.NeighborhoodHousingNewestYearsCountSpecified
                    || this.NeighborhoodHousingOldestYearsCountSpecified
                    || this.NeighborhoodHousingPredominantAgeYearsCountSpecified
                    || this.NeighborhoodHousingPredominantOccupancyTypeSpecified
                    || this.NeighborhoodHousingPredominantPriceAmountSpecified
                    || this.NeighborhoodHousingTypeSpecified
                    || this.NeighborhoodHousingTypeOtherDescriptionSpecified
                    || this.NeighborhoodOwnerOccupancyPercentSpecified
                    || this.NeighborhoodPropertyValueTrendTypeSpecified
                    || this.NeighborhoodTypicalMarketingDaysDurationCountSpecified
                    || this.NeighborhoodTypicalMarketingMonthsDurationTypeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("NeighborhoodBuiltUpRangeType", Order = 0)]
        public MISMOEnum<NeighborhoodBuiltUpRangeBase> NeighborhoodBuiltUpRangeType { get; set; }
    
        [XmlIgnore]
        public bool NeighborhoodBuiltUpRangeTypeSpecified
        {
            get { return this.NeighborhoodBuiltUpRangeType != null; }
            set { }
        }
    
        [XmlElement("NeighborhoodDemandSupplyType", Order = 1)]
        public MISMOEnum<NeighborhoodDemandSupplyBase> NeighborhoodDemandSupplyType { get; set; }
    
        [XmlIgnore]
        public bool NeighborhoodDemandSupplyTypeSpecified
        {
            get { return this.NeighborhoodDemandSupplyType != null; }
            set { }
        }
    
        [XmlElement("NeighborhoodHousingHighPriceAmount", Order = 2)]
        public MISMOAmount NeighborhoodHousingHighPriceAmount { get; set; }
    
        [XmlIgnore]
        public bool NeighborhoodHousingHighPriceAmountSpecified
        {
            get { return this.NeighborhoodHousingHighPriceAmount != null; }
            set { }
        }
    
        [XmlElement("NeighborhoodHousingLowPriceAmount", Order = 3)]
        public MISMOAmount NeighborhoodHousingLowPriceAmount { get; set; }
    
        [XmlIgnore]
        public bool NeighborhoodHousingLowPriceAmountSpecified
        {
            get { return this.NeighborhoodHousingLowPriceAmount != null; }
            set { }
        }
    
        [XmlElement("NeighborhoodHousingNewestYearsCount", Order = 4)]
        public MISMOCount NeighborhoodHousingNewestYearsCount { get; set; }
    
        [XmlIgnore]
        public bool NeighborhoodHousingNewestYearsCountSpecified
        {
            get { return this.NeighborhoodHousingNewestYearsCount != null; }
            set { }
        }
    
        [XmlElement("NeighborhoodHousingOldestYearsCount", Order = 5)]
        public MISMOCount NeighborhoodHousingOldestYearsCount { get; set; }
    
        [XmlIgnore]
        public bool NeighborhoodHousingOldestYearsCountSpecified
        {
            get { return this.NeighborhoodHousingOldestYearsCount != null; }
            set { }
        }
    
        [XmlElement("NeighborhoodHousingPredominantAgeYearsCount", Order = 6)]
        public MISMOCount NeighborhoodHousingPredominantAgeYearsCount { get; set; }
    
        [XmlIgnore]
        public bool NeighborhoodHousingPredominantAgeYearsCountSpecified
        {
            get { return this.NeighborhoodHousingPredominantAgeYearsCount != null; }
            set { }
        }
    
        [XmlElement("NeighborhoodHousingPredominantOccupancyType", Order = 7)]
        public MISMOEnum<NeighborhoodHousingPredominantOccupancyBase> NeighborhoodHousingPredominantOccupancyType { get; set; }
    
        [XmlIgnore]
        public bool NeighborhoodHousingPredominantOccupancyTypeSpecified
        {
            get { return this.NeighborhoodHousingPredominantOccupancyType != null; }
            set { }
        }
    
        [XmlElement("NeighborhoodHousingPredominantPriceAmount", Order = 8)]
        public MISMOAmount NeighborhoodHousingPredominantPriceAmount { get; set; }
    
        [XmlIgnore]
        public bool NeighborhoodHousingPredominantPriceAmountSpecified
        {
            get { return this.NeighborhoodHousingPredominantPriceAmount != null; }
            set { }
        }
    
        [XmlElement("NeighborhoodHousingType", Order = 9)]
        public MISMOEnum<NeighborhoodHousingBase> NeighborhoodHousingType { get; set; }
    
        [XmlIgnore]
        public bool NeighborhoodHousingTypeSpecified
        {
            get { return this.NeighborhoodHousingType != null; }
            set { }
        }
    
        [XmlElement("NeighborhoodHousingTypeOtherDescription", Order = 10)]
        public MISMOString NeighborhoodHousingTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool NeighborhoodHousingTypeOtherDescriptionSpecified
        {
            get { return this.NeighborhoodHousingTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("NeighborhoodOwnerOccupancyPercent", Order = 11)]
        public MISMOPercent NeighborhoodOwnerOccupancyPercent { get; set; }
    
        [XmlIgnore]
        public bool NeighborhoodOwnerOccupancyPercentSpecified
        {
            get { return this.NeighborhoodOwnerOccupancyPercent != null; }
            set { }
        }
    
        [XmlElement("NeighborhoodPropertyValueTrendType", Order = 12)]
        public MISMOEnum<NeighborhoodPropertyValueTrendBase> NeighborhoodPropertyValueTrendType { get; set; }
    
        [XmlIgnore]
        public bool NeighborhoodPropertyValueTrendTypeSpecified
        {
            get { return this.NeighborhoodPropertyValueTrendType != null; }
            set { }
        }
    
        [XmlElement("NeighborhoodTypicalMarketingDaysDurationCount", Order = 13)]
        public MISMOCount NeighborhoodTypicalMarketingDaysDurationCount { get; set; }
    
        [XmlIgnore]
        public bool NeighborhoodTypicalMarketingDaysDurationCountSpecified
        {
            get { return this.NeighborhoodTypicalMarketingDaysDurationCount != null; }
            set { }
        }
    
        [XmlElement("NeighborhoodTypicalMarketingMonthsDurationType", Order = 14)]
        public MISMOEnum<NeighborhoodTypicalMarketingMonthsDurationBase> NeighborhoodTypicalMarketingMonthsDurationType { get; set; }
    
        [XmlIgnore]
        public bool NeighborhoodTypicalMarketingMonthsDurationTypeSpecified
        {
            get { return this.NeighborhoodTypicalMarketingMonthsDurationType != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 15)]
        public HOUSING_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
