namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class HOUSINGS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.HousingListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("HOUSING", Order = 0)]
        public List<HOUSING> HousingList { get; set; } = new List<HOUSING>();
    
        [XmlIgnore]
        public bool HousingListSpecified
        {
            get { return this.HousingList != null && this.HousingList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public HOUSINGS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
