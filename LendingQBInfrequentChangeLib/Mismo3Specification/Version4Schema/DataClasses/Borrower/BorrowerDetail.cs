namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class BORROWER_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AutomobilesOwnedCountSpecified
                    || this.BorrowerAgeAtApplicationYearsCountSpecified
                    || this.BorrowerApplicationSignedDateSpecified
                    || this.BorrowerBankruptcyIndicatorSpecified
                    || this.BorrowerBirthDateSpecified
                    || this.BorrowerCharacteristicTypeSpecified
                    || this.BorrowerCharacteristicTypeOtherDescriptionSpecified
                    || this.BorrowerClassificationTypeSpecified
                    || this.BorrowerIsAnIndividualPersonIndicatorSpecified
                    || this.BorrowerMailToAddressSameAsPropertyIndicatorSpecified
                    || this.BorrowerQualifyingIncomeAmountSpecified
                    || this.BorrowerRelationshipTitleTypeSpecified
                    || this.BorrowerRelationshipTitleTypeOtherDescriptionSpecified
                    || this.BorrowerSameAsBuilderIndicatorSpecified
                    || this.BorrowerTotalMortgagedPropertiesCountSpecified
                    || this.CommunityPropertyStateResidentIndicatorSpecified
                    || this.CreditFileBorrowerAgeYearsCountSpecified
                    || this.CreditReportAuthorizationIndicatorSpecified
                    || this.CreditReportIdentifierSpecified
                    || this.DependentCountSpecified
                    || this.DomesticRelationshipIndicatorSpecified
                    || this.DomesticRelationshipStateCodeSpecified
                    || this.DomesticRelationshipTypeSpecified
                    || this.DomesticRelationshipTypeOtherDescriptionSpecified
                    || this.EmploymentStateTypeSpecified
                    || this.EmploymentStateTypeOtherDescriptionSpecified
                    || this.HousingCounselingRequiredIndicatorSpecified
                    || this.IntentToProceedWithLoanTransactionIndicatedDateSpecified
                    || this.JointAssetLiabilityReportingTypeSpecified
                    || this.MaritalStatusTypeSpecified
                    || this.MaritalStatusTypeOtherDescriptionSpecified
                    || this.SchoolingYearsCountSpecified
                    || this.SelfDeclaredMilitaryServiceIndicatorSpecified
                    || this.SignedAuthorizationToRequestTaxRecordsIndicatorSpecified
                    || this.SpousalVABenefitsEligibilityIndicatorSpecified
                    || this.TaxRecordsObtainedIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AutomobilesOwnedCount", Order = 0)]
        public MISMOCount AutomobilesOwnedCount { get; set; }
    
        [XmlIgnore]
        public bool AutomobilesOwnedCountSpecified
        {
            get { return this.AutomobilesOwnedCount != null; }
            set { }
        }
    
        [XmlElement("BorrowerAgeAtApplicationYearsCount", Order = 1)]
        public MISMOCount BorrowerAgeAtApplicationYearsCount { get; set; }
    
        [XmlIgnore]
        public bool BorrowerAgeAtApplicationYearsCountSpecified
        {
            get { return this.BorrowerAgeAtApplicationYearsCount != null; }
            set { }
        }
    
        [XmlElement("BorrowerApplicationSignedDate", Order = 2)]
        public MISMODate BorrowerApplicationSignedDate { get; set; }
    
        [XmlIgnore]
        public bool BorrowerApplicationSignedDateSpecified
        {
            get { return this.BorrowerApplicationSignedDate != null; }
            set { }
        }
    
        [XmlElement("BorrowerBankruptcyIndicator", Order = 3)]
        public MISMOIndicator BorrowerBankruptcyIndicator { get; set; }
    
        [XmlIgnore]
        public bool BorrowerBankruptcyIndicatorSpecified
        {
            get { return this.BorrowerBankruptcyIndicator != null; }
            set { }
        }
    
        [XmlElement("BorrowerBirthDate", Order = 4)]
        public MISMODate BorrowerBirthDate { get; set; }
    
        [XmlIgnore]
        public bool BorrowerBirthDateSpecified
        {
            get { return this.BorrowerBirthDate != null; }
            set { }
        }
    
        [XmlElement("BorrowerCharacteristicType", Order = 5)]
        public MISMOEnum<BorrowerCharacteristicBase> BorrowerCharacteristicType { get; set; }
    
        [XmlIgnore]
        public bool BorrowerCharacteristicTypeSpecified
        {
            get { return this.BorrowerCharacteristicType != null; }
            set { }
        }
    
        [XmlElement("BorrowerCharacteristicTypeOtherDescription", Order = 6)]
        public MISMOString BorrowerCharacteristicTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool BorrowerCharacteristicTypeOtherDescriptionSpecified
        {
            get { return this.BorrowerCharacteristicTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("BorrowerClassificationType", Order = 7)]
        public MISMOEnum<BorrowerClassificationBase> BorrowerClassificationType { get; set; }
    
        [XmlIgnore]
        public bool BorrowerClassificationTypeSpecified
        {
            get { return this.BorrowerClassificationType != null; }
            set { }
        }
    
        [XmlElement("BorrowerIsAnIndividualPersonIndicator", Order = 8)]
        public MISMOIndicator BorrowerIsAnIndividualPersonIndicator { get; set; }
    
        [XmlIgnore]
        public bool BorrowerIsAnIndividualPersonIndicatorSpecified
        {
            get { return this.BorrowerIsAnIndividualPersonIndicator != null; }
            set { }
        }
    
        [XmlElement("BorrowerMailToAddressSameAsPropertyIndicator", Order = 9)]
        public MISMOIndicator BorrowerMailToAddressSameAsPropertyIndicator { get; set; }
    
        [XmlIgnore]
        public bool BorrowerMailToAddressSameAsPropertyIndicatorSpecified
        {
            get { return this.BorrowerMailToAddressSameAsPropertyIndicator != null; }
            set { }
        }
    
        [XmlElement("BorrowerQualifyingIncomeAmount", Order = 10)]
        public MISMOAmount BorrowerQualifyingIncomeAmount { get; set; }
    
        [XmlIgnore]
        public bool BorrowerQualifyingIncomeAmountSpecified
        {
            get { return this.BorrowerQualifyingIncomeAmount != null; }
            set { }
        }
    
        [XmlElement("BorrowerRelationshipTitleType", Order = 11)]
        public MISMOEnum<BorrowerRelationshipTitleBase> BorrowerRelationshipTitleType { get; set; }
    
        [XmlIgnore]
        public bool BorrowerRelationshipTitleTypeSpecified
        {
            get { return this.BorrowerRelationshipTitleType != null; }
            set { }
        }
    
        [XmlElement("BorrowerRelationshipTitleTypeOtherDescription", Order = 12)]
        public MISMOString BorrowerRelationshipTitleTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool BorrowerRelationshipTitleTypeOtherDescriptionSpecified
        {
            get { return this.BorrowerRelationshipTitleTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("BorrowerSameAsBuilderIndicator", Order = 13)]
        public MISMOIndicator BorrowerSameAsBuilderIndicator { get; set; }
    
        [XmlIgnore]
        public bool BorrowerSameAsBuilderIndicatorSpecified
        {
            get { return this.BorrowerSameAsBuilderIndicator != null; }
            set { }
        }
    
        [XmlElement("BorrowerTotalMortgagedPropertiesCount", Order = 14)]
        public MISMOCount BorrowerTotalMortgagedPropertiesCount { get; set; }
    
        [XmlIgnore]
        public bool BorrowerTotalMortgagedPropertiesCountSpecified
        {
            get { return this.BorrowerTotalMortgagedPropertiesCount != null; }
            set { }
        }
    
        [XmlElement("CommunityPropertyStateResidentIndicator", Order = 15)]
        public MISMOIndicator CommunityPropertyStateResidentIndicator { get; set; }
    
        [XmlIgnore]
        public bool CommunityPropertyStateResidentIndicatorSpecified
        {
            get { return this.CommunityPropertyStateResidentIndicator != null; }
            set { }
        }
    
        [XmlElement("CreditFileBorrowerAgeYearsCount", Order = 16)]
        public MISMOCount CreditFileBorrowerAgeYearsCount { get; set; }
    
        [XmlIgnore]
        public bool CreditFileBorrowerAgeYearsCountSpecified
        {
            get { return this.CreditFileBorrowerAgeYearsCount != null; }
            set { }
        }
    
        [XmlElement("CreditReportAuthorizationIndicator", Order = 17)]
        public MISMOIndicator CreditReportAuthorizationIndicator { get; set; }
    
        [XmlIgnore]
        public bool CreditReportAuthorizationIndicatorSpecified
        {
            get { return this.CreditReportAuthorizationIndicator != null; }
            set { }
        }
    
        [XmlElement("CreditReportIdentifier", Order = 18)]
        public MISMOIdentifier CreditReportIdentifier { get; set; }
    
        [XmlIgnore]
        public bool CreditReportIdentifierSpecified
        {
            get { return this.CreditReportIdentifier != null; }
            set { }
        }
    
        [XmlElement("DependentCount", Order = 19)]
        public MISMOCount DependentCount { get; set; }
    
        [XmlIgnore]
        public bool DependentCountSpecified
        {
            get { return this.DependentCount != null; }
            set { }
        }
    
        [XmlElement("DomesticRelationshipIndicator", Order = 20)]
        public MISMOIndicator DomesticRelationshipIndicator { get; set; }
    
        [XmlIgnore]
        public bool DomesticRelationshipIndicatorSpecified
        {
            get { return this.DomesticRelationshipIndicator != null; }
            set { }
        }
    
        [XmlElement("DomesticRelationshipStateCode", Order = 21)]
        public MISMOCode DomesticRelationshipStateCode { get; set; }
    
        [XmlIgnore]
        public bool DomesticRelationshipStateCodeSpecified
        {
            get { return this.DomesticRelationshipStateCode != null; }
            set { }
        }
    
        [XmlElement("DomesticRelationshipType", Order = 22)]
        public MISMOEnum<DomesticRelationshipBase> DomesticRelationshipType { get; set; }
    
        [XmlIgnore]
        public bool DomesticRelationshipTypeSpecified
        {
            get { return this.DomesticRelationshipType != null; }
            set { }
        }
    
        [XmlElement("DomesticRelationshipTypeOtherDescription", Order = 23)]
        public MISMOString DomesticRelationshipTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool DomesticRelationshipTypeOtherDescriptionSpecified
        {
            get { return this.DomesticRelationshipTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EmploymentStateType", Order = 24)]
        public MISMOEnum<EmploymentStateBase> EmploymentStateType { get; set; }
    
        [XmlIgnore]
        public bool EmploymentStateTypeSpecified
        {
            get { return this.EmploymentStateType != null; }
            set { }
        }
    
        [XmlElement("EmploymentStateTypeOtherDescription", Order = 25)]
        public MISMOString EmploymentStateTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool EmploymentStateTypeOtherDescriptionSpecified
        {
            get { return this.EmploymentStateTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("HousingCounselingRequiredIndicator", Order = 26)]
        public MISMOIndicator HousingCounselingRequiredIndicator { get; set; }
    
        [XmlIgnore]
        public bool HousingCounselingRequiredIndicatorSpecified
        {
            get { return this.HousingCounselingRequiredIndicator != null; }
            set { }
        }
    
        [XmlElement("IntentToProceedWithLoanTransactionIndicatedDate", Order = 27)]
        public MISMODate IntentToProceedWithLoanTransactionIndicatedDate { get; set; }
    
        [XmlIgnore]
        public bool IntentToProceedWithLoanTransactionIndicatedDateSpecified
        {
            get { return this.IntentToProceedWithLoanTransactionIndicatedDate != null; }
            set { }
        }
    
        [XmlElement("JointAssetLiabilityReportingType", Order = 28)]
        public MISMOEnum<JointAssetLiabilityReportingBase> JointAssetLiabilityReportingType { get; set; }
    
        [XmlIgnore]
        public bool JointAssetLiabilityReportingTypeSpecified
        {
            get { return this.JointAssetLiabilityReportingType != null; }
            set { }
        }
    
        [XmlElement("MaritalStatusType", Order = 29)]
        public MISMOEnum<MaritalStatusBase> MaritalStatusType { get; set; }
    
        [XmlIgnore]
        public bool MaritalStatusTypeSpecified
        {
            get { return this.MaritalStatusType != null; }
            set { }
        }
    
        [XmlElement("MaritalStatusTypeOtherDescription", Order = 30)]
        public MISMOString MaritalStatusTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MaritalStatusTypeOtherDescriptionSpecified
        {
            get { return this.MaritalStatusTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("SchoolingYearsCount", Order = 31)]
        public MISMOCount SchoolingYearsCount { get; set; }
    
        [XmlIgnore]
        public bool SchoolingYearsCountSpecified
        {
            get { return this.SchoolingYearsCount != null; }
            set { }
        }
    
        [XmlElement("SelfDeclaredMilitaryServiceIndicator", Order = 32)]
        public MISMOIndicator SelfDeclaredMilitaryServiceIndicator { get; set; }
    
        [XmlIgnore]
        public bool SelfDeclaredMilitaryServiceIndicatorSpecified
        {
            get { return this.SelfDeclaredMilitaryServiceIndicator != null; }
            set { }
        }
    
        [XmlElement("SignedAuthorizationToRequestTaxRecordsIndicator", Order = 33)]
        public MISMOIndicator SignedAuthorizationToRequestTaxRecordsIndicator { get; set; }
    
        [XmlIgnore]
        public bool SignedAuthorizationToRequestTaxRecordsIndicatorSpecified
        {
            get { return this.SignedAuthorizationToRequestTaxRecordsIndicator != null; }
            set { }
        }
    
        [XmlElement("SpousalVABenefitsEligibilityIndicator", Order = 34)]
        public MISMOIndicator SpousalVABenefitsEligibilityIndicator { get; set; }
    
        [XmlIgnore]
        public bool SpousalVABenefitsEligibilityIndicatorSpecified
        {
            get { return this.SpousalVABenefitsEligibilityIndicator != null; }
            set { }
        }
    
        [XmlElement("TaxRecordsObtainedIndicator", Order = 35)]
        public MISMOIndicator TaxRecordsObtainedIndicator { get; set; }
    
        [XmlIgnore]
        public bool TaxRecordsObtainedIndicatorSpecified
        {
            get { return this.TaxRecordsObtainedIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 36)]
        public BORROWER_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
