namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Schema;
    using System.Xml.Serialization;

    public class BORROWER
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BankruptciesSpecified
                    || this.BorrowerDetailSpecified
                    || this.CounselingSpecified
                    || this.CreditScoresSpecified
                    || this.CurrentIncomeSpecified
                    || this.DeclarationSpecified
                    || this.DependentsSpecified
                    || this.EmployersSpecified
                    || this.GovernmentBorrowerSpecified
                    || this.GovernmentMonitoringSpecified
                    || this.HardshipDeclarationSpecified
                    || this.HousingExpensesSpecified
                    || this.MilitaryServicesSpecified
                    || this.NearestLivingRelativeSpecified
                    || this.ResidencesSpecified
                    || this.SolicitationPreferenceSpecified
                    || this.SummariesSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BANKRUPTCIES", Order = 0)]
        public BANKRUPTCIES Bankruptcies { get; set; }
    
        [XmlIgnore]
        public bool BankruptciesSpecified
        {
            get { return this.Bankruptcies != null && this.Bankruptcies.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("BORROWER_DETAIL", Order = 1)]
        public BORROWER_DETAIL BorrowerDetail { get; set; }
    
        [XmlIgnore]
        public bool BorrowerDetailSpecified
        {
            get { return this.BorrowerDetail != null && this.BorrowerDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("COUNSELING", Order = 2)]
        public COUNSELING Counseling { get; set; }
    
        [XmlIgnore]
        public bool CounselingSpecified
        {
            get { return this.Counseling != null && this.Counseling.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_SCORES", Order = 3)]
        public CREDIT_SCORES CreditScores { get; set; }
    
        [XmlIgnore]
        public bool CreditScoresSpecified
        {
            get { return this.CreditScores != null && this.CreditScores.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CURRENT_INCOME", Order = 4)]
        public CURRENT_INCOME CurrentIncome { get; set; }
    
        [XmlIgnore]
        public bool CurrentIncomeSpecified
        {
            get { return this.CurrentIncome != null && this.CurrentIncome.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("DECLARATION", Order = 5)]
        public DECLARATION Declaration { get; set; }
    
        [XmlIgnore]
        public bool DeclarationSpecified
        {
            get { return this.Declaration != null && this.Declaration.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("DEPENDENTS", Order = 6)]
        public DEPENDENTS Dependents { get; set; }
    
        [XmlIgnore]
        public bool DependentsSpecified
        {
            get { return this.Dependents != null && this.Dependents.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EMPLOYERS", Order = 7)]
        public EMPLOYERS Employers { get; set; }
    
        [XmlIgnore]
        public bool EmployersSpecified
        {
            get { return this.Employers != null && this.Employers.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("GOVERNMENT_BORROWER", Order = 8)]
        public GOVERNMENT_BORROWER GovernmentBorrower { get; set; }
    
        [XmlIgnore]
        public bool GovernmentBorrowerSpecified
        {
            get { return this.GovernmentBorrower != null && this.GovernmentBorrower.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("GOVERNMENT_MONITORING", Order = 9)]
        public GOVERNMENT_MONITORING GovernmentMonitoring { get; set; }
    
        [XmlIgnore]
        public bool GovernmentMonitoringSpecified
        {
            get { return this.GovernmentMonitoring != null && this.GovernmentMonitoring.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("HARDSHIP_DECLARATION", Order = 10)]
        public HARDSHIP_DECLARATION HardshipDeclaration { get; set; }
    
        [XmlIgnore]
        public bool HardshipDeclarationSpecified
        {
            get { return this.HardshipDeclaration != null && this.HardshipDeclaration.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("HOUSING_EXPENSES", Order = 11)]
        public HOUSING_EXPENSES HousingExpenses { get; set; }
    
        [XmlIgnore]
        public bool HousingExpensesSpecified
        {
            get { return this.HousingExpenses != null && this.HousingExpenses.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("MILITARY_SERVICES", Order = 12)]
        public MILITARY_SERVICES MilitaryServices { get; set; }
    
        [XmlIgnore]
        public bool MilitaryServicesSpecified
        {
            get { return this.MilitaryServices != null && this.MilitaryServices.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("NEAREST_LIVING_RELATIVE", Order = 13)]
        public NEAREST_LIVING_RELATIVE NearestLivingRelative { get; set; }
    
        [XmlIgnore]
        public bool NearestLivingRelativeSpecified
        {
            get { return this.NearestLivingRelative != null && this.NearestLivingRelative.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("RESIDENCES", Order = 14)]
        public RESIDENCES Residences { get; set; }
    
        [XmlIgnore]
        public bool ResidencesSpecified
        {
            get { return this.Residences != null && this.Residences.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SOLICITATION_PREFERENCE", Order = 15)]
        public SOLICITATION_PREFERENCE SolicitationPreference { get; set; }
    
        [XmlIgnore]
        public bool SolicitationPreferenceSpecified
        {
            get { return this.SolicitationPreference != null && this.SolicitationPreference.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SUMMARIES", Order = 16)]
        public SUMMARIES Summaries { get; set; }
    
        [XmlIgnore]
        public bool SummariesSpecified
        {
            get { return this.Summaries != null && this.Summaries.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 17)]
        public BORROWER_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }

        [XmlAttribute("label", Form = XmlSchemaForm.Qualified, Namespace = Mismo3Constants.XlinkNamespace)]
        public string XlinkLabel { get; set; }

        [XmlIgnore]
        public bool XlinkLabelSpecified
        {
            get { return !string.IsNullOrEmpty(this.XlinkLabel); }
            set { }
        }
    }
}
