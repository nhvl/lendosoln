namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class QUALIFICATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BorrowerReservesMonthlyPaymentCountSpecified
                    || this.CombinedLTVRatioConsideredInDecisionIndicatorSpecified
                    || this.CreditScoreConsideredInDecisionIndicatorSpecified
                    || this.HouseholdOccupancyCountSpecified
                    || this.HousingExpenseRatioPercentSpecified
                    || this.IncomeConsideredInDecisionIndicatorSpecified
                    || this.IncomeReportedTypeSpecified
                    || this.ProjectedReservesAmountSpecified
                    || this.QualifyingMIPaymentAmountSpecified
                    || this.QualifyingPaymentAmountSpecified
                    || this.QualifyingPrincipalAndInterestPaymentAmountSpecified
                    || this.QualifyingRatePercentSpecified
                    || this.QualifyingRateTypeSpecified
                    || this.QualifyingRateTypeOtherDescriptionSpecified
                    || this.SubjectPropertyNetCashFlowAmountSpecified
                    || this.SummaryInterestedPartyContributionsPercentSpecified
                    || this.TotalDebtExpenseRatioConsideredInDecisionIndicatorSpecified
                    || this.TotalDebtExpenseRatioPercentSpecified
                    || this.TotalExpensesMonthlyPaymentAmountSpecified
                    || this.TotalLiabilitiesMonthlyPaymentAmountSpecified
                    || this.TotalMonthlyCurrentHousingExpenseAmountSpecified
                    || this.TotalMonthlyIncomeAmountSpecified
                    || this.TotalMonthlyProposedHousingExpenseAmountSpecified
                    || this.TotalNonFirstLienMortgageDebtAmountSpecified
                    || this.TotalVerifiedReservesAmountSpecified
                    || this.VerifiedAssetsTotalAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BorrowerReservesMonthlyPaymentCount", Order = 0)]
        public MISMOCount BorrowerReservesMonthlyPaymentCount { get; set; }
    
        [XmlIgnore]
        public bool BorrowerReservesMonthlyPaymentCountSpecified
        {
            get { return this.BorrowerReservesMonthlyPaymentCount != null; }
            set { }
        }
    
        [XmlElement("CombinedLTVRatioConsideredInDecisionIndicator", Order = 1)]
        public MISMOIndicator CombinedLTVRatioConsideredInDecisionIndicator { get; set; }
    
        [XmlIgnore]
        public bool CombinedLTVRatioConsideredInDecisionIndicatorSpecified
        {
            get { return this.CombinedLTVRatioConsideredInDecisionIndicator != null; }
            set { }
        }
    
        [XmlElement("CreditScoreConsideredInDecisionIndicator", Order = 2)]
        public MISMOIndicator CreditScoreConsideredInDecisionIndicator { get; set; }
    
        [XmlIgnore]
        public bool CreditScoreConsideredInDecisionIndicatorSpecified
        {
            get { return this.CreditScoreConsideredInDecisionIndicator != null; }
            set { }
        }
    
        [XmlElement("HouseholdOccupancyCount", Order = 3)]
        public MISMOCount HouseholdOccupancyCount { get; set; }
    
        [XmlIgnore]
        public bool HouseholdOccupancyCountSpecified
        {
            get { return this.HouseholdOccupancyCount != null; }
            set { }
        }
    
        [XmlElement("HousingExpenseRatioPercent", Order = 4)]
        public MISMOPercent HousingExpenseRatioPercent { get; set; }
    
        [XmlIgnore]
        public bool HousingExpenseRatioPercentSpecified
        {
            get { return this.HousingExpenseRatioPercent != null; }
            set { }
        }
    
        [XmlElement("IncomeConsideredInDecisionIndicator", Order = 5)]
        public MISMOIndicator IncomeConsideredInDecisionIndicator { get; set; }
    
        [XmlIgnore]
        public bool IncomeConsideredInDecisionIndicatorSpecified
        {
            get { return this.IncomeConsideredInDecisionIndicator != null; }
            set { }
        }
    
        [XmlElement("IncomeReportedType", Order = 6)]
        public MISMOEnum<IncomeReportedBase> IncomeReportedType { get; set; }
    
        [XmlIgnore]
        public bool IncomeReportedTypeSpecified
        {
            get { return this.IncomeReportedType != null; }
            set { }
        }
    
        [XmlElement("ProjectedReservesAmount", Order = 7)]
        public MISMOAmount ProjectedReservesAmount { get; set; }
    
        [XmlIgnore]
        public bool ProjectedReservesAmountSpecified
        {
            get { return this.ProjectedReservesAmount != null; }
            set { }
        }
    
        [XmlElement("QualifyingMIPaymentAmount", Order = 8)]
        public MISMOAmount QualifyingMIPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool QualifyingMIPaymentAmountSpecified
        {
            get { return this.QualifyingMIPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("QualifyingPaymentAmount", Order = 9)]
        public MISMOAmount QualifyingPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool QualifyingPaymentAmountSpecified
        {
            get { return this.QualifyingPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("QualifyingPrincipalAndInterestPaymentAmount", Order = 10)]
        public MISMOAmount QualifyingPrincipalAndInterestPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool QualifyingPrincipalAndInterestPaymentAmountSpecified
        {
            get { return this.QualifyingPrincipalAndInterestPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("QualifyingRatePercent", Order = 11)]
        public MISMOPercent QualifyingRatePercent { get; set; }
    
        [XmlIgnore]
        public bool QualifyingRatePercentSpecified
        {
            get { return this.QualifyingRatePercent != null; }
            set { }
        }
    
        [XmlElement("QualifyingRateType", Order = 12)]
        public MISMOEnum<QualifyingRateBase> QualifyingRateType { get; set; }
    
        [XmlIgnore]
        public bool QualifyingRateTypeSpecified
        {
            get { return this.QualifyingRateType != null; }
            set { }
        }
    
        [XmlElement("QualifyingRateTypeOtherDescription", Order = 13)]
        public MISMOString QualifyingRateTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool QualifyingRateTypeOtherDescriptionSpecified
        {
            get { return this.QualifyingRateTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("SubjectPropertyNetCashFlowAmount", Order = 14)]
        public MISMOAmount SubjectPropertyNetCashFlowAmount { get; set; }
    
        [XmlIgnore]
        public bool SubjectPropertyNetCashFlowAmountSpecified
        {
            get { return this.SubjectPropertyNetCashFlowAmount != null; }
            set { }
        }
    
        [XmlElement("SummaryInterestedPartyContributionsPercent", Order = 15)]
        public MISMOPercent SummaryInterestedPartyContributionsPercent { get; set; }
    
        [XmlIgnore]
        public bool SummaryInterestedPartyContributionsPercentSpecified
        {
            get { return this.SummaryInterestedPartyContributionsPercent != null; }
            set { }
        }
    
        [XmlElement("TotalDebtExpenseRatioConsideredInDecisionIndicator", Order = 16)]
        public MISMOIndicator TotalDebtExpenseRatioConsideredInDecisionIndicator { get; set; }
    
        [XmlIgnore]
        public bool TotalDebtExpenseRatioConsideredInDecisionIndicatorSpecified
        {
            get { return this.TotalDebtExpenseRatioConsideredInDecisionIndicator != null; }
            set { }
        }
    
        [XmlElement("TotalDebtExpenseRatioPercent", Order = 17)]
        public MISMOPercent TotalDebtExpenseRatioPercent { get; set; }
    
        [XmlIgnore]
        public bool TotalDebtExpenseRatioPercentSpecified
        {
            get { return this.TotalDebtExpenseRatioPercent != null; }
            set { }
        }
    
        [XmlElement("TotalExpensesMonthlyPaymentAmount", Order = 18)]
        public MISMOAmount TotalExpensesMonthlyPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool TotalExpensesMonthlyPaymentAmountSpecified
        {
            get { return this.TotalExpensesMonthlyPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("TotalLiabilitiesMonthlyPaymentAmount", Order = 19)]
        public MISMOAmount TotalLiabilitiesMonthlyPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool TotalLiabilitiesMonthlyPaymentAmountSpecified
        {
            get { return this.TotalLiabilitiesMonthlyPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("TotalMonthlyCurrentHousingExpenseAmount", Order = 20)]
        public MISMOAmount TotalMonthlyCurrentHousingExpenseAmount { get; set; }
    
        [XmlIgnore]
        public bool TotalMonthlyCurrentHousingExpenseAmountSpecified
        {
            get { return this.TotalMonthlyCurrentHousingExpenseAmount != null; }
            set { }
        }
    
        [XmlElement("TotalMonthlyIncomeAmount", Order = 21)]
        public MISMOAmount TotalMonthlyIncomeAmount { get; set; }
    
        [XmlIgnore]
        public bool TotalMonthlyIncomeAmountSpecified
        {
            get { return this.TotalMonthlyIncomeAmount != null; }
            set { }
        }
    
        [XmlElement("TotalMonthlyProposedHousingExpenseAmount", Order = 22)]
        public MISMOAmount TotalMonthlyProposedHousingExpenseAmount { get; set; }
    
        [XmlIgnore]
        public bool TotalMonthlyProposedHousingExpenseAmountSpecified
        {
            get { return this.TotalMonthlyProposedHousingExpenseAmount != null; }
            set { }
        }
    
        [XmlElement("TotalNonFirstLienMortgageDebtAmount", Order = 23)]
        public MISMOAmount TotalNonFirstLienMortgageDebtAmount { get; set; }
    
        [XmlIgnore]
        public bool TotalNonFirstLienMortgageDebtAmountSpecified
        {
            get { return this.TotalNonFirstLienMortgageDebtAmount != null; }
            set { }
        }
    
        [XmlElement("TotalVerifiedReservesAmount", Order = 24)]
        public MISMOAmount TotalVerifiedReservesAmount { get; set; }
    
        [XmlIgnore]
        public bool TotalVerifiedReservesAmountSpecified
        {
            get { return this.TotalVerifiedReservesAmount != null; }
            set { }
        }
    
        [XmlElement("VerifiedAssetsTotalAmount", Order = 25)]
        public MISMOAmount VerifiedAssetsTotalAmount { get; set; }
    
        [XmlIgnore]
        public bool VerifiedAssetsTotalAmountSpecified
        {
            get { return this.VerifiedAssetsTotalAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 26)]
        public QUALIFICATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
