namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MI_POOL_INSURANCE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MICompanyNameTypeSpecified
                    || this.MICompanyNameTypeOtherDescriptionSpecified
                    || this.MIPoolInsuranceCertificateIdentifierSpecified
                    || this.MIPoolInsuranceMasterCommitmentIdentifierSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MICompanyNameType", Order = 0)]
        public MISMOEnum<MICompanyNameBase> MICompanyNameType { get; set; }
    
        [XmlIgnore]
        public bool MICompanyNameTypeSpecified
        {
            get { return this.MICompanyNameType != null; }
            set { }
        }
    
        [XmlElement("MICompanyNameTypeOtherDescription", Order = 1)]
        public MISMOString MICompanyNameTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MICompanyNameTypeOtherDescriptionSpecified
        {
            get { return this.MICompanyNameTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MIPoolInsuranceCertificateIdentifier", Order = 2)]
        public MISMOIdentifier MIPoolInsuranceCertificateIdentifier { get; set; }
    
        [XmlIgnore]
        public bool MIPoolInsuranceCertificateIdentifierSpecified
        {
            get { return this.MIPoolInsuranceCertificateIdentifier != null; }
            set { }
        }
    
        [XmlElement("MIPoolInsuranceMasterCommitmentIdentifier", Order = 3)]
        public MISMOIdentifier MIPoolInsuranceMasterCommitmentIdentifier { get; set; }
    
        [XmlIgnore]
        public bool MIPoolInsuranceMasterCommitmentIdentifierSpecified
        {
            get { return this.MIPoolInsuranceMasterCommitmentIdentifier != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public MI_POOL_INSURANCE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
