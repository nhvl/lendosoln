namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class DEPRECIATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DepreciationExteriorAmountSpecified
                    || this.DepreciationExteriorPercentSpecified
                    || this.DepreciationFunctionalAmountSpecified
                    || this.DepreciationFunctionalPercentSpecified
                    || this.DepreciationPhysicalAmountSpecified
                    || this.DepreciationPhysicalPercentSpecified
                    || this.DepreciationTotalAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DepreciationExteriorAmount", Order = 0)]
        public MISMOAmount DepreciationExteriorAmount { get; set; }
    
        [XmlIgnore]
        public bool DepreciationExteriorAmountSpecified
        {
            get { return this.DepreciationExteriorAmount != null; }
            set { }
        }
    
        [XmlElement("DepreciationExteriorPercent", Order = 1)]
        public MISMOPercent DepreciationExteriorPercent { get; set; }
    
        [XmlIgnore]
        public bool DepreciationExteriorPercentSpecified
        {
            get { return this.DepreciationExteriorPercent != null; }
            set { }
        }
    
        [XmlElement("DepreciationFunctionalAmount", Order = 2)]
        public MISMOAmount DepreciationFunctionalAmount { get; set; }
    
        [XmlIgnore]
        public bool DepreciationFunctionalAmountSpecified
        {
            get { return this.DepreciationFunctionalAmount != null; }
            set { }
        }
    
        [XmlElement("DepreciationFunctionalPercent", Order = 3)]
        public MISMOPercent DepreciationFunctionalPercent { get; set; }
    
        [XmlIgnore]
        public bool DepreciationFunctionalPercentSpecified
        {
            get { return this.DepreciationFunctionalPercent != null; }
            set { }
        }
    
        [XmlElement("DepreciationPhysicalAmount", Order = 4)]
        public MISMOAmount DepreciationPhysicalAmount { get; set; }
    
        [XmlIgnore]
        public bool DepreciationPhysicalAmountSpecified
        {
            get { return this.DepreciationPhysicalAmount != null; }
            set { }
        }
    
        [XmlElement("DepreciationPhysicalPercent", Order = 5)]
        public MISMOPercent DepreciationPhysicalPercent { get; set; }
    
        [XmlIgnore]
        public bool DepreciationPhysicalPercentSpecified
        {
            get { return this.DepreciationPhysicalPercent != null; }
            set { }
        }
    
        [XmlElement("DepreciationTotalAmount", Order = 6)]
        public MISMOAmount DepreciationTotalAmount { get; set; }
    
        [XmlIgnore]
        public bool DepreciationTotalAmountSpecified
        {
            get { return this.DepreciationTotalAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 7)]
        public DEPRECIATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
