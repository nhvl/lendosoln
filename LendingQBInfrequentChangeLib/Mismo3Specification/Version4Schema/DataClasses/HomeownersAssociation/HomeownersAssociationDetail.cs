namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class HOMEOWNERS_ASSOCIATION_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.HomeownersAssociationAccountIdentifierSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("HomeownersAssociationAccountIdentifier", Order = 0)]
        public MISMOIdentifier HomeownersAssociationAccountIdentifier { get; set; }
    
        [XmlIgnore]
        public bool HomeownersAssociationAccountIdentifierSpecified
        {
            get { return this.HomeownersAssociationAccountIdentifier != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public HOMEOWNERS_ASSOCIATION_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
