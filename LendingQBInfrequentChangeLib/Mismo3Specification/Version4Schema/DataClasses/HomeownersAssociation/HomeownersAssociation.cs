namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class HOMEOWNERS_ASSOCIATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AddressSpecified
                    || this.HomeownersAssociationDetailSpecified
                    || this.LegalEntitySpecified
                    || this.ManagementAgentSpecified
                    || this.VerificationSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ADDRESS", Order = 0)]
        public ADDRESS Address { get; set; }
    
        [XmlIgnore]
        public bool AddressSpecified
        {
            get { return this.Address != null && this.Address.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("HOMEOWNERS_ASSOCIATION_DETAIL", Order = 1)]
        public HOMEOWNERS_ASSOCIATION_DETAIL HomeownersAssociationDetail { get; set; }
    
        [XmlIgnore]
        public bool HomeownersAssociationDetailSpecified
        {
            get { return this.HomeownersAssociationDetail != null && this.HomeownersAssociationDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("LEGAL_ENTITY", Order = 2)]
        public LEGAL_ENTITY LegalEntity { get; set; }
    
        [XmlIgnore]
        public bool LegalEntitySpecified
        {
            get { return this.LegalEntity != null && this.LegalEntity.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("MANAGEMENT_AGENT", Order = 3)]
        public MANAGEMENT_AGENT ManagementAgent { get; set; }
    
        [XmlIgnore]
        public bool ManagementAgentSpecified
        {
            get { return this.ManagementAgent != null && this.ManagementAgent.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("VERIFICATION", Order = 4)]
        public VERIFICATION Verification { get; set; }
    
        [XmlIgnore]
        public bool VerificationSpecified
        {
            get { return this.Verification != null && this.Verification.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public HOMEOWNERS_ASSOCIATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
