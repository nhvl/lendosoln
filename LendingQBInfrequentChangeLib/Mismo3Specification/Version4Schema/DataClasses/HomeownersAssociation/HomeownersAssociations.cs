namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class HOMEOWNERS_ASSOCIATIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.HomeownersAssociationListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("HOMEOWNERS_ASSOCIATION", Order = 0)]
        public List<HOMEOWNERS_ASSOCIATION> HomeownersAssociationList { get; set; } = new List<HOMEOWNERS_ASSOCIATION>();
    
        [XmlIgnore]
        public bool HomeownersAssociationListSpecified
        {
            get { return this.HomeownersAssociationList != null && this.HomeownersAssociationList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public HOMEOWNERS_ASSOCIATIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
