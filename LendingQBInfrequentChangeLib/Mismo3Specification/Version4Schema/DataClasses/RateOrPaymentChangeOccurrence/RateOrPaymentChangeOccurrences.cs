namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class RATE_OR_PAYMENT_CHANGE_OCCURRENCES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.RateOrPaymentChangeOccurrenceListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("RATE_OR_PAYMENT_CHANGE_OCCURRENCE", Order = 0)]
        public List<RATE_OR_PAYMENT_CHANGE_OCCURRENCE> RateOrPaymentChangeOccurrenceList { get; set; } = new List<RATE_OR_PAYMENT_CHANGE_OCCURRENCE>();
    
        [XmlIgnore]
        public bool RateOrPaymentChangeOccurrenceListSpecified
        {
            get { return this.RateOrPaymentChangeOccurrenceList != null && this.RateOrPaymentChangeOccurrenceList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public RATE_OR_PAYMENT_CHANGE_OCCURRENCES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
