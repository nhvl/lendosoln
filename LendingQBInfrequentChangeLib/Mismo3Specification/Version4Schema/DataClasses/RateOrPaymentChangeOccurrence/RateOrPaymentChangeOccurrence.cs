namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class RATE_OR_PAYMENT_CHANGE_OCCURRENCE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AdjustmentChangeEffectiveDueDateSpecified
                    || this.AdjustmentChangeIndexRatePercentSpecified
                    || this.AdjustmentChangeInterestRatePercentSpecified
                    || this.AdjustmentChangeOccurrenceExtendedTermMonthsCountSpecified
                    || this.AdjustmentChangeOccurrencePassThroughRatePercentSpecified
                    || this.AdjustmentChangeOccurrenceTypeSpecified
                    || this.AdjustmentChangeOccurrenceTypeOtherDescriptionSpecified
                    || this.AdjustmentChangePrincipalAndInterestPaymentAmountSpecified
                    || this.AdjustmentChangeProjectedPrincipalBalanceAmountSpecified
                    || this.AdjustmentChangeServiceFeeAmountSpecified
                    || this.AdjustmentChangeServiceFeeRatePercentSpecified
                    || this.AdjustmentRuleTypeSpecified
                    || this.BalloonResetDateSpecified
                    || this.CarryoverRatePercentSpecified
                    || this.ConvertibleStatusTypeSpecified
                    || this.LastRateChangeNotificationDateSpecified
                    || this.LatestConversionEffectiveDateSpecified
                    || this.NextConversionOptionEffectiveDateSpecified
                    || this.NextConversionOptionNoticeDateSpecified
                    || this.NextDemandConversionOptionNoticeDateSpecified
                    || this.NextIgnorePrincipalAndInterestPaymentAdjustmentCapsDateSpecified
                    || this.NextIgnoreRateAdjustmentCapsDateSpecified
                    || this.NextPrincipalAndInterestPaymentChangeEffectiveDateSpecified
                    || this.NextRateAdjustmentEffectiveDateSpecified
                    || this.NextRateAdjustmentEffectiveNoticeDateSpecified
                    || this.RateAdjustmentPercentSpecified
                    || this.ServicerARMPlanIdentifierSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AdjustmentChangeEffectiveDueDate", Order = 0)]
        public MISMODate AdjustmentChangeEffectiveDueDate { get; set; }
    
        [XmlIgnore]
        public bool AdjustmentChangeEffectiveDueDateSpecified
        {
            get { return this.AdjustmentChangeEffectiveDueDate != null; }
            set { }
        }
    
        [XmlElement("AdjustmentChangeIndexRatePercent", Order = 1)]
        public MISMOPercent AdjustmentChangeIndexRatePercent { get; set; }
    
        [XmlIgnore]
        public bool AdjustmentChangeIndexRatePercentSpecified
        {
            get { return this.AdjustmentChangeIndexRatePercent != null; }
            set { }
        }
    
        [XmlElement("AdjustmentChangeInterestRatePercent", Order = 2)]
        public MISMOPercent AdjustmentChangeInterestRatePercent { get; set; }
    
        [XmlIgnore]
        public bool AdjustmentChangeInterestRatePercentSpecified
        {
            get { return this.AdjustmentChangeInterestRatePercent != null; }
            set { }
        }
    
        [XmlElement("AdjustmentChangeOccurrenceExtendedTermMonthsCount", Order = 3)]
        public MISMOCount AdjustmentChangeOccurrenceExtendedTermMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool AdjustmentChangeOccurrenceExtendedTermMonthsCountSpecified
        {
            get { return this.AdjustmentChangeOccurrenceExtendedTermMonthsCount != null; }
            set { }
        }
    
        [XmlElement("AdjustmentChangeOccurrencePassThroughRatePercent", Order = 4)]
        public MISMOPercent AdjustmentChangeOccurrencePassThroughRatePercent { get; set; }
    
        [XmlIgnore]
        public bool AdjustmentChangeOccurrencePassThroughRatePercentSpecified
        {
            get { return this.AdjustmentChangeOccurrencePassThroughRatePercent != null; }
            set { }
        }
    
        [XmlElement("AdjustmentChangeOccurrenceType", Order = 5)]
        public MISMOEnum<AdjustmentChangeOccurrenceBase> AdjustmentChangeOccurrenceType { get; set; }
    
        [XmlIgnore]
        public bool AdjustmentChangeOccurrenceTypeSpecified
        {
            get { return this.AdjustmentChangeOccurrenceType != null; }
            set { }
        }
    
        [XmlElement("AdjustmentChangeOccurrenceTypeOtherDescription", Order = 6)]
        public MISMOString AdjustmentChangeOccurrenceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool AdjustmentChangeOccurrenceTypeOtherDescriptionSpecified
        {
            get { return this.AdjustmentChangeOccurrenceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("AdjustmentChangePrincipalAndInterestPaymentAmount", Order = 7)]
        public MISMOAmount AdjustmentChangePrincipalAndInterestPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool AdjustmentChangePrincipalAndInterestPaymentAmountSpecified
        {
            get { return this.AdjustmentChangePrincipalAndInterestPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("AdjustmentChangeProjectedPrincipalBalanceAmount", Order = 8)]
        public MISMOAmount AdjustmentChangeProjectedPrincipalBalanceAmount { get; set; }
    
        [XmlIgnore]
        public bool AdjustmentChangeProjectedPrincipalBalanceAmountSpecified
        {
            get { return this.AdjustmentChangeProjectedPrincipalBalanceAmount != null; }
            set { }
        }
    
        [XmlElement("AdjustmentChangeServiceFeeAmount", Order = 9)]
        public MISMOAmount AdjustmentChangeServiceFeeAmount { get; set; }
    
        [XmlIgnore]
        public bool AdjustmentChangeServiceFeeAmountSpecified
        {
            get { return this.AdjustmentChangeServiceFeeAmount != null; }
            set { }
        }
    
        [XmlElement("AdjustmentChangeServiceFeeRatePercent", Order = 10)]
        public MISMOPercent AdjustmentChangeServiceFeeRatePercent { get; set; }
    
        [XmlIgnore]
        public bool AdjustmentChangeServiceFeeRatePercentSpecified
        {
            get { return this.AdjustmentChangeServiceFeeRatePercent != null; }
            set { }
        }
    
        [XmlElement("AdjustmentRuleType", Order = 11)]
        public MISMOEnum<AdjustmentRuleBase> AdjustmentRuleType { get; set; }
    
        [XmlIgnore]
        public bool AdjustmentRuleTypeSpecified
        {
            get { return this.AdjustmentRuleType != null; }
            set { }
        }
    
        [XmlElement("BalloonResetDate", Order = 12)]
        public MISMODate BalloonResetDate { get; set; }
    
        [XmlIgnore]
        public bool BalloonResetDateSpecified
        {
            get { return this.BalloonResetDate != null; }
            set { }
        }
    
        [XmlElement("CarryoverRatePercent", Order = 13)]
        public MISMOPercent CarryoverRatePercent { get; set; }
    
        [XmlIgnore]
        public bool CarryoverRatePercentSpecified
        {
            get { return this.CarryoverRatePercent != null; }
            set { }
        }
    
        [XmlElement("ConvertibleStatusType", Order = 14)]
        public MISMOEnum<ConvertibleStatusBase> ConvertibleStatusType { get; set; }
    
        [XmlIgnore]
        public bool ConvertibleStatusTypeSpecified
        {
            get { return this.ConvertibleStatusType != null; }
            set { }
        }
    
        [XmlElement("LastRateChangeNotificationDate", Order = 15)]
        public MISMODate LastRateChangeNotificationDate { get; set; }
    
        [XmlIgnore]
        public bool LastRateChangeNotificationDateSpecified
        {
            get { return this.LastRateChangeNotificationDate != null; }
            set { }
        }
    
        [XmlElement("LatestConversionEffectiveDate", Order = 16)]
        public MISMODate LatestConversionEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool LatestConversionEffectiveDateSpecified
        {
            get { return this.LatestConversionEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("NextConversionOptionEffectiveDate", Order = 17)]
        public MISMODate NextConversionOptionEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool NextConversionOptionEffectiveDateSpecified
        {
            get { return this.NextConversionOptionEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("NextConversionOptionNoticeDate", Order = 18)]
        public MISMODate NextConversionOptionNoticeDate { get; set; }
    
        [XmlIgnore]
        public bool NextConversionOptionNoticeDateSpecified
        {
            get { return this.NextConversionOptionNoticeDate != null; }
            set { }
        }
    
        [XmlElement("NextDemandConversionOptionNoticeDate", Order = 19)]
        public MISMODate NextDemandConversionOptionNoticeDate { get; set; }
    
        [XmlIgnore]
        public bool NextDemandConversionOptionNoticeDateSpecified
        {
            get { return this.NextDemandConversionOptionNoticeDate != null; }
            set { }
        }
    
        [XmlElement("NextIgnorePrincipalAndInterestPaymentAdjustmentCapsDate", Order = 20)]
        public MISMODate NextIgnorePrincipalAndInterestPaymentAdjustmentCapsDate { get; set; }
    
        [XmlIgnore]
        public bool NextIgnorePrincipalAndInterestPaymentAdjustmentCapsDateSpecified
        {
            get { return this.NextIgnorePrincipalAndInterestPaymentAdjustmentCapsDate != null; }
            set { }
        }
    
        [XmlElement("NextIgnoreRateAdjustmentCapsDate", Order = 21)]
        public MISMODate NextIgnoreRateAdjustmentCapsDate { get; set; }
    
        [XmlIgnore]
        public bool NextIgnoreRateAdjustmentCapsDateSpecified
        {
            get { return this.NextIgnoreRateAdjustmentCapsDate != null; }
            set { }
        }
    
        [XmlElement("NextPrincipalAndInterestPaymentChangeEffectiveDate", Order = 22)]
        public MISMODate NextPrincipalAndInterestPaymentChangeEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool NextPrincipalAndInterestPaymentChangeEffectiveDateSpecified
        {
            get { return this.NextPrincipalAndInterestPaymentChangeEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("NextRateAdjustmentEffectiveDate", Order = 23)]
        public MISMODate NextRateAdjustmentEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool NextRateAdjustmentEffectiveDateSpecified
        {
            get { return this.NextRateAdjustmentEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("NextRateAdjustmentEffectiveNoticeDate", Order = 24)]
        public MISMODate NextRateAdjustmentEffectiveNoticeDate { get; set; }
    
        [XmlIgnore]
        public bool NextRateAdjustmentEffectiveNoticeDateSpecified
        {
            get { return this.NextRateAdjustmentEffectiveNoticeDate != null; }
            set { }
        }
    
        [XmlElement("RateAdjustmentPercent", Order = 25)]
        public MISMOPercent RateAdjustmentPercent { get; set; }
    
        [XmlIgnore]
        public bool RateAdjustmentPercentSpecified
        {
            get { return this.RateAdjustmentPercent != null; }
            set { }
        }
    
        [XmlElement("ServicerARMPlanIdentifier", Order = 26)]
        public MISMOIdentifier ServicerARMPlanIdentifier { get; set; }
    
        [XmlIgnore]
        public bool ServicerARMPlanIdentifierSpecified
        {
            get { return this.ServicerARMPlanIdentifier != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 27)]
        public RATE_OR_PAYMENT_CHANGE_OCCURRENCE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
