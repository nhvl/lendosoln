namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ESCROW_DISCLOSURE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EscrowAccountActivityCurrentBalanceAmountSpecified
                    || this.EscrowAccountActivityDisbursementMonthSpecified
                    || this.EscrowAccountActivityDisbursementYearSpecified
                    || this.EscrowAccountActivityPaymentDescriptionTypeSpecified
                    || this.EscrowAccountActivityPaymentDescriptionTypeOtherDescriptionSpecified
                    || this.EscrowAccountActivityPaymentFromEscrowAccountAmountSpecified
                    || this.EscrowAccountActivityPaymentToEscrowAccountAmountSpecified
                    || this.EscrowItemTypeSpecified
                    || this.EscrowItemTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("EscrowAccountActivityCurrentBalanceAmount", Order = 0)]
        public MISMOAmount EscrowAccountActivityCurrentBalanceAmount { get; set; }
    
        [XmlIgnore]
        public bool EscrowAccountActivityCurrentBalanceAmountSpecified
        {
            get { return this.EscrowAccountActivityCurrentBalanceAmount != null; }
            set { }
        }
    
        [XmlElement("EscrowAccountActivityDisbursementMonth", Order = 1)]
        public MISMOMonth EscrowAccountActivityDisbursementMonth { get; set; }
    
        [XmlIgnore]
        public bool EscrowAccountActivityDisbursementMonthSpecified
        {
            get { return this.EscrowAccountActivityDisbursementMonth != null; }
            set { }
        }
    
        [XmlElement("EscrowAccountActivityDisbursementYear", Order = 2)]
        public MISMOYear EscrowAccountActivityDisbursementYear { get; set; }
    
        [XmlIgnore]
        public bool EscrowAccountActivityDisbursementYearSpecified
        {
            get { return this.EscrowAccountActivityDisbursementYear != null; }
            set { }
        }
    
        [XmlElement("EscrowAccountActivityPaymentDescriptionType", Order = 3)]
        public MISMOEnum<EscrowAccountActivityPaymentDescriptionBase> EscrowAccountActivityPaymentDescriptionType { get; set; }
    
        [XmlIgnore]
        public bool EscrowAccountActivityPaymentDescriptionTypeSpecified
        {
            get { return this.EscrowAccountActivityPaymentDescriptionType != null; }
            set { }
        }
    
        [XmlElement("EscrowAccountActivityPaymentDescriptionTypeOtherDescription", Order = 4)]
        public MISMOString EscrowAccountActivityPaymentDescriptionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool EscrowAccountActivityPaymentDescriptionTypeOtherDescriptionSpecified
        {
            get { return this.EscrowAccountActivityPaymentDescriptionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EscrowAccountActivityPaymentFromEscrowAccountAmount", Order = 5)]
        public MISMOAmount EscrowAccountActivityPaymentFromEscrowAccountAmount { get; set; }
    
        [XmlIgnore]
        public bool EscrowAccountActivityPaymentFromEscrowAccountAmountSpecified
        {
            get { return this.EscrowAccountActivityPaymentFromEscrowAccountAmount != null; }
            set { }
        }
    
        [XmlElement("EscrowAccountActivityPaymentToEscrowAccountAmount", Order = 6)]
        public MISMOAmount EscrowAccountActivityPaymentToEscrowAccountAmount { get; set; }
    
        [XmlIgnore]
        public bool EscrowAccountActivityPaymentToEscrowAccountAmountSpecified
        {
            get { return this.EscrowAccountActivityPaymentToEscrowAccountAmount != null; }
            set { }
        }
    
        [XmlElement("EscrowItemType", Order = 7)]
        public MISMOEnum<EscrowItemBase> EscrowItemType { get; set; }
    
        [XmlIgnore]
        public bool EscrowItemTypeSpecified
        {
            get { return this.EscrowItemType != null; }
            set { }
        }
    
        [XmlElement("EscrowItemTypeOtherDescription", Order = 8)]
        public MISMOString EscrowItemTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool EscrowItemTypeOtherDescriptionSpecified
        {
            get { return this.EscrowItemTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 9)]
        public ESCROW_DISCLOSURE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
