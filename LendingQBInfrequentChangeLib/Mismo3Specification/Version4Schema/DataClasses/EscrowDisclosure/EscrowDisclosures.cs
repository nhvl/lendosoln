namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class ESCROW_DISCLOSURES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EscrowDisclosureListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ESCROW_DISCLOSURE", Order = 0)]
        public List<ESCROW_DISCLOSURE> EscrowDisclosureList { get; set; } = new List<ESCROW_DISCLOSURE>();
    
        [XmlIgnore]
        public bool EscrowDisclosureListSpecified
        {
            get { return this.EscrowDisclosureList != null && this.EscrowDisclosureList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public ESCROW_DISCLOSURES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
