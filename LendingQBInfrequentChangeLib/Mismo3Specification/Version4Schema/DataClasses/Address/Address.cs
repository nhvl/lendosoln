namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ADDRESS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AddressAdditionalLineTextSpecified
                    || this.AddressFormatTypeSpecified
                    || this.AddressFormatTypeOtherDescriptionSpecified
                    || this.AddressLineTextSpecified
                    || this.AddressTypeSpecified
                    || this.AddressTypeOtherDescriptionSpecified
                    || this.AddressUnitDesignatorTypeSpecified
                    || this.AddressUnitDesignatorTypeOtherDescriptionSpecified
                    || this.AddressUnitIdentifierSpecified
                    || this.AttentionToNameSpecified
                    || this.CarrierRouteCodeSpecified
                    || this.CityNameSpecified
                    || this.CountryCodeSpecified
                    || this.CountryNameSpecified
                    || this.CountyCodeSpecified
                    || this.CountyNameSpecified
                    || this.DeliveryPointBarCodeCheckValueSpecified
                    || this.DeliveryPointBarCodeValueSpecified
                    || this.HighwayContractRouteIdentifierSpecified
                    || this.MailStopCodeSpecified
                    || this.PlusFourZipCodeSpecified
                    || this.PostalCodeSpecified
                    || this.PostOfficeBoxIdentifierSpecified
                    || this.RuralRouteBoxIdentifierSpecified
                    || this.RuralRouteIdentifierSpecified
                    || this.StateCodeSpecified
                    || this.StateNameSpecified
                    || this.StreetNameSpecified
                    || this.StreetPostDirectionalTextSpecified
                    || this.StreetPreDirectionalTextSpecified
                    || this.StreetPrimaryNumberTextSpecified
                    || this.StreetSuffixTextSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AddressAdditionalLineText", Order = 0)]
        public MISMOString AddressAdditionalLineText { get; set; }
    
        [XmlIgnore]
        public bool AddressAdditionalLineTextSpecified
        {
            get { return this.AddressAdditionalLineText != null; }
            set { }
        }
    
        [XmlElement("AddressFormatType", Order = 1)]
        public MISMOEnum<AddressFormatBase> AddressFormatType { get; set; }
    
        [XmlIgnore]
        public bool AddressFormatTypeSpecified
        {
            get { return this.AddressFormatType != null; }
            set { }
        }
    
        [XmlElement("AddressFormatTypeOtherDescription", Order = 2)]
        public MISMOString AddressFormatTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool AddressFormatTypeOtherDescriptionSpecified
        {
            get { return this.AddressFormatTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("AddressLineText", Order = 3)]
        public MISMOString AddressLineText { get; set; }
    
        [XmlIgnore]
        public bool AddressLineTextSpecified
        {
            get { return this.AddressLineText != null; }
            set { }
        }
    
        [XmlElement("AddressType", Order = 4)]
        public MISMOEnum<AddressBase> AddressType { get; set; }
    
        [XmlIgnore]
        public bool AddressTypeSpecified
        {
            get { return this.AddressType != null; }
            set { }
        }
    
        [XmlElement("AddressTypeOtherDescription", Order = 5)]
        public MISMOString AddressTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool AddressTypeOtherDescriptionSpecified
        {
            get { return this.AddressTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("AddressUnitDesignatorType", Order = 6)]
        public MISMOEnum<AddressUnitDesignatorBase> AddressUnitDesignatorType { get; set; }
    
        [XmlIgnore]
        public bool AddressUnitDesignatorTypeSpecified
        {
            get { return this.AddressUnitDesignatorType != null; }
            set { }
        }
    
        [XmlElement("AddressUnitDesignatorTypeOtherDescription", Order = 7)]
        public MISMOString AddressUnitDesignatorTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool AddressUnitDesignatorTypeOtherDescriptionSpecified
        {
            get { return this.AddressUnitDesignatorTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("AddressUnitIdentifier", Order = 8)]
        public MISMOIdentifier AddressUnitIdentifier { get; set; }
    
        [XmlIgnore]
        public bool AddressUnitIdentifierSpecified
        {
            get { return this.AddressUnitIdentifier != null; }
            set { }
        }
    
        [XmlElement("AttentionToName", Order = 9)]
        public MISMOString AttentionToName { get; set; }
    
        [XmlIgnore]
        public bool AttentionToNameSpecified
        {
            get { return this.AttentionToName != null; }
            set { }
        }
    
        [XmlElement("CarrierRouteCode", Order = 10)]
        public MISMOCode CarrierRouteCode { get; set; }
    
        [XmlIgnore]
        public bool CarrierRouteCodeSpecified
        {
            get { return this.CarrierRouteCode != null; }
            set { }
        }
    
        [XmlElement("CityName", Order = 11)]
        public MISMOString CityName { get; set; }
    
        [XmlIgnore]
        public bool CityNameSpecified
        {
            get { return this.CityName != null; }
            set { }
        }
    
        [XmlElement("CountryCode", Order = 12)]
        public MISMOCode CountryCode { get; set; }
    
        [XmlIgnore]
        public bool CountryCodeSpecified
        {
            get { return this.CountryCode != null; }
            set { }
        }
    
        [XmlElement("CountryName", Order = 13)]
        public MISMOString CountryName { get; set; }
    
        [XmlIgnore]
        public bool CountryNameSpecified
        {
            get { return this.CountryName != null; }
            set { }
        }
    
        [XmlElement("CountyCode", Order = 14)]
        public MISMOCode CountyCode { get; set; }
    
        [XmlIgnore]
        public bool CountyCodeSpecified
        {
            get { return this.CountyCode != null; }
            set { }
        }
    
        [XmlElement("CountyName", Order = 15)]
        public MISMOString CountyName { get; set; }
    
        [XmlIgnore]
        public bool CountyNameSpecified
        {
            get { return this.CountyName != null; }
            set { }
        }
    
        [XmlElement("DeliveryPointBarCodeCheckValue", Order = 16)]
        public MISMOValue DeliveryPointBarCodeCheckValue { get; set; }
    
        [XmlIgnore]
        public bool DeliveryPointBarCodeCheckValueSpecified
        {
            get { return this.DeliveryPointBarCodeCheckValue != null; }
            set { }
        }
    
        [XmlElement("DeliveryPointBarCodeValue", Order = 17)]
        public MISMOValue DeliveryPointBarCodeValue { get; set; }
    
        [XmlIgnore]
        public bool DeliveryPointBarCodeValueSpecified
        {
            get { return this.DeliveryPointBarCodeValue != null; }
            set { }
        }
    
        [XmlElement("HighwayContractRouteIdentifier", Order = 18)]
        public MISMOIdentifier HighwayContractRouteIdentifier { get; set; }
    
        [XmlIgnore]
        public bool HighwayContractRouteIdentifierSpecified
        {
            get { return this.HighwayContractRouteIdentifier != null; }
            set { }
        }
    
        [XmlElement("MailStopCode", Order = 19)]
        public MISMOCode MailStopCode { get; set; }
    
        [XmlIgnore]
        public bool MailStopCodeSpecified
        {
            get { return this.MailStopCode != null; }
            set { }
        }
    
        [XmlElement("PlusFourZipCode", Order = 20)]
        public MISMOCode PlusFourZipCode { get; set; }
    
        [XmlIgnore]
        public bool PlusFourZipCodeSpecified
        {
            get { return this.PlusFourZipCode != null; }
            set { }
        }
    
        [XmlElement("PostalCode", Order = 21)]
        public MISMOCode PostalCode { get; set; }
    
        [XmlIgnore]
        public bool PostalCodeSpecified
        {
            get { return this.PostalCode != null; }
            set { }
        }
    
        [XmlElement("PostOfficeBoxIdentifier", Order = 22)]
        public MISMOIdentifier PostOfficeBoxIdentifier { get; set; }
    
        [XmlIgnore]
        public bool PostOfficeBoxIdentifierSpecified
        {
            get { return this.PostOfficeBoxIdentifier != null; }
            set { }
        }
    
        [XmlElement("RuralRouteBoxIdentifier", Order = 23)]
        public MISMOIdentifier RuralRouteBoxIdentifier { get; set; }
    
        [XmlIgnore]
        public bool RuralRouteBoxIdentifierSpecified
        {
            get { return this.RuralRouteBoxIdentifier != null; }
            set { }
        }
    
        [XmlElement("RuralRouteIdentifier", Order = 24)]
        public MISMOIdentifier RuralRouteIdentifier { get; set; }
    
        [XmlIgnore]
        public bool RuralRouteIdentifierSpecified
        {
            get { return this.RuralRouteIdentifier != null; }
            set { }
        }
    
        [XmlElement("StateCode", Order = 25)]
        public MISMOCode StateCode { get; set; }
    
        [XmlIgnore]
        public bool StateCodeSpecified
        {
            get { return this.StateCode != null; }
            set { }
        }
    
        [XmlElement("StateName", Order = 26)]
        public MISMOString StateName { get; set; }
    
        [XmlIgnore]
        public bool StateNameSpecified
        {
            get { return this.StateName != null; }
            set { }
        }
    
        [XmlElement("StreetName", Order = 27)]
        public MISMOString StreetName { get; set; }
    
        [XmlIgnore]
        public bool StreetNameSpecified
        {
            get { return this.StreetName != null; }
            set { }
        }
    
        [XmlElement("StreetPostDirectionalText", Order = 28)]
        public MISMOString StreetPostDirectionalText { get; set; }
    
        [XmlIgnore]
        public bool StreetPostDirectionalTextSpecified
        {
            get { return this.StreetPostDirectionalText != null; }
            set { }
        }
    
        [XmlElement("StreetPreDirectionalText", Order = 29)]
        public MISMOString StreetPreDirectionalText { get; set; }
    
        [XmlIgnore]
        public bool StreetPreDirectionalTextSpecified
        {
            get { return this.StreetPreDirectionalText != null; }
            set { }
        }
    
        [XmlElement("StreetPrimaryNumberText", Order = 30)]
        public MISMOString StreetPrimaryNumberText { get; set; }
    
        [XmlIgnore]
        public bool StreetPrimaryNumberTextSpecified
        {
            get { return this.StreetPrimaryNumberText != null; }
            set { }
        }
    
        [XmlElement("StreetSuffixText", Order = 31)]
        public MISMOString StreetSuffixText { get; set; }
    
        [XmlIgnore]
        public bool StreetSuffixTextSpecified
        {
            get { return this.StreetSuffixText != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 32)]
        public ADDRESS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
