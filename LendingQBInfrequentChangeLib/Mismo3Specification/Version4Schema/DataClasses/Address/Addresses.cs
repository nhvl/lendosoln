namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class ADDRESSES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AddressListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ADDRESS", Order = 0)]
        public List<ADDRESS> AddressList { get; set; } = new List<ADDRESS>();
    
        [XmlIgnore]
        public bool AddressListSpecified
        {
            get { return this.AddressList != null && this.AddressList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public ADDRESSES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
