namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class RESIDENTIAL_RENT_SCHEDULE_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EstimatedMarketMonthlyRentAmountSpecified
                    || this.EstimatedMarketMonthlyRentEffectiveDateSpecified
                    || this.MarketRentalDataCommentDescriptionSpecified
                    || this.MarketRentReconciliationCommentDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("EstimatedMarketMonthlyRentAmount", Order = 0)]
        public MISMOAmount EstimatedMarketMonthlyRentAmount { get; set; }
    
        [XmlIgnore]
        public bool EstimatedMarketMonthlyRentAmountSpecified
        {
            get { return this.EstimatedMarketMonthlyRentAmount != null; }
            set { }
        }
    
        [XmlElement("EstimatedMarketMonthlyRentEffectiveDate", Order = 1)]
        public MISMODate EstimatedMarketMonthlyRentEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool EstimatedMarketMonthlyRentEffectiveDateSpecified
        {
            get { return this.EstimatedMarketMonthlyRentEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("MarketRentalDataCommentDescription", Order = 2)]
        public MISMOString MarketRentalDataCommentDescription { get; set; }
    
        [XmlIgnore]
        public bool MarketRentalDataCommentDescriptionSpecified
        {
            get { return this.MarketRentalDataCommentDescription != null; }
            set { }
        }
    
        [XmlElement("MarketRentReconciliationCommentDescription", Order = 3)]
        public MISMOString MarketRentReconciliationCommentDescription { get; set; }
    
        [XmlIgnore]
        public bool MarketRentReconciliationCommentDescriptionSpecified
        {
            get { return this.MarketRentReconciliationCommentDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public RESIDENTIAL_RENT_SCHEDULE_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
