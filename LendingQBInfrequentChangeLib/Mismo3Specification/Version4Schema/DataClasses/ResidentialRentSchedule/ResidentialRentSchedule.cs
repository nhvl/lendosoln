namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class RESIDENTIAL_RENT_SCHEDULE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ResidentialRentScheduleDetailSpecified
                    || this.ResidentialRentalSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("RESIDENTIAL_RENT_SCHEDULE_DETAIL", Order = 0)]
        public RESIDENTIAL_RENT_SCHEDULE_DETAIL ResidentialRentScheduleDetail { get; set; }
    
        [XmlIgnore]
        public bool ResidentialRentScheduleDetailSpecified
        {
            get { return this.ResidentialRentScheduleDetail != null && this.ResidentialRentScheduleDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("RESIDENTIAL_RENTAL", Order = 1)]
        public RESIDENTIAL_RENTAL ResidentialRental { get; set; }
    
        [XmlIgnore]
        public bool ResidentialRentalSpecified
        {
            get { return this.ResidentialRental != null && this.ResidentialRental.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public RESIDENTIAL_RENT_SCHEDULE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
