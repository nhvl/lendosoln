namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class REAL_PROPERTY_INTERESTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.RealPropertyInterestListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("REAL_PROPERTY_INTEREST", Order = 0)]
        public List<REAL_PROPERTY_INTEREST> RealPropertyInterestList { get; set; } = new List<REAL_PROPERTY_INTEREST>();
    
        [XmlIgnore]
        public bool RealPropertyInterestListSpecified
        {
            get { return this.RealPropertyInterestList != null && this.RealPropertyInterestList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public REAL_PROPERTY_INTERESTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
