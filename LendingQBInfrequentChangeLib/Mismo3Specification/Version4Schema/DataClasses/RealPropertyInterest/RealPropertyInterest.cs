namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class REAL_PROPERTY_INTEREST
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AppraisedIndicatorSpecified
                    || this.CooperativeUnitSharesCountSpecified
                    || this.PropertyInterestRightsAppraisedTypeSpecified
                    || this.PropertyInterestRightsAppraisedTypeOtherDescriptionSpecified
                    || this.PropertyPartialInterestTypeSpecified
                    || this.PropertyPartialInterestTypeOtherDescriptionSpecified
                    || this.UnitIdentifierSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AppraisedIndicator", Order = 0)]
        public MISMOIndicator AppraisedIndicator { get; set; }
    
        [XmlIgnore]
        public bool AppraisedIndicatorSpecified
        {
            get { return this.AppraisedIndicator != null; }
            set { }
        }
    
        [XmlElement("CooperativeUnitSharesCount", Order = 1)]
        public MISMOCount CooperativeUnitSharesCount { get; set; }
    
        [XmlIgnore]
        public bool CooperativeUnitSharesCountSpecified
        {
            get { return this.CooperativeUnitSharesCount != null; }
            set { }
        }
    
        [XmlElement("PropertyInterestRightsAppraisedType", Order = 2)]
        public MISMOEnum<PropertyInterestRightsAppraisedBase> PropertyInterestRightsAppraisedType { get; set; }
    
        [XmlIgnore]
        public bool PropertyInterestRightsAppraisedTypeSpecified
        {
            get { return this.PropertyInterestRightsAppraisedType != null; }
            set { }
        }
    
        [XmlElement("PropertyInterestRightsAppraisedTypeOtherDescription", Order = 3)]
        public MISMOString PropertyInterestRightsAppraisedTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PropertyInterestRightsAppraisedTypeOtherDescriptionSpecified
        {
            get { return this.PropertyInterestRightsAppraisedTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PropertyPartialInterestType", Order = 4)]
        public MISMOEnum<PropertyPartialInterestBase> PropertyPartialInterestType { get; set; }
    
        [XmlIgnore]
        public bool PropertyPartialInterestTypeSpecified
        {
            get { return this.PropertyPartialInterestType != null; }
            set { }
        }
    
        [XmlElement("PropertyPartialInterestTypeOtherDescription", Order = 5)]
        public MISMOString PropertyPartialInterestTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PropertyPartialInterestTypeOtherDescriptionSpecified
        {
            get { return this.PropertyPartialInterestTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("UnitIdentifier", Order = 6)]
        public MISMOIdentifier UnitIdentifier { get; set; }
    
        [XmlIgnore]
        public bool UnitIdentifierSpecified
        {
            get { return this.UnitIdentifier != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 7)]
        public REAL_PROPERTY_INTEREST_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
