namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class LITIGATION_STATUS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LitigationStatusDateSpecified
                    || this.LitigationStatusTypeSpecified
                    || this.LitigationStatusTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LitigationStatusDate", Order = 0)]
        public MISMODate LitigationStatusDate { get; set; }
    
        [XmlIgnore]
        public bool LitigationStatusDateSpecified
        {
            get { return this.LitigationStatusDate != null; }
            set { }
        }
    
        [XmlElement("LitigationStatusType", Order = 1)]
        public MISMOEnum<LitigationStatusBase> LitigationStatusType { get; set; }
    
        [XmlIgnore]
        public bool LitigationStatusTypeSpecified
        {
            get { return this.LitigationStatusType != null; }
            set { }
        }
    
        [XmlElement("LitigationStatusTypeOtherDescription", Order = 2)]
        public MISMOString LitigationStatusTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool LitigationStatusTypeOtherDescriptionSpecified
        {
            get { return this.LitigationStatusTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public LITIGATION_STATUS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
