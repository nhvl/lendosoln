namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class LITIGATION_STATUSES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LitigationStatusListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LITIGATION_STATUS", Order = 0)]
        public List<LITIGATION_STATUS> LitigationStatusList { get; set; } = new List<LITIGATION_STATUS>();
    
        [XmlIgnore]
        public bool LitigationStatusListSpecified
        {
            get { return this.LitigationStatusList != null && this.LitigationStatusList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public LITIGATION_STATUSES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
