namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PROJECT_OWNERSHIP
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ProjectConcentratedOwnershipDescriptionSpecified
                    || this.ProjectConcentratedOwnershipIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ProjectConcentratedOwnershipDescription", Order = 0)]
        public MISMOString ProjectConcentratedOwnershipDescription { get; set; }
    
        [XmlIgnore]
        public bool ProjectConcentratedOwnershipDescriptionSpecified
        {
            get { return this.ProjectConcentratedOwnershipDescription != null; }
            set { }
        }
    
        [XmlElement("ProjectConcentratedOwnershipIndicator", Order = 1)]
        public MISMOIndicator ProjectConcentratedOwnershipIndicator { get; set; }
    
        [XmlIgnore]
        public bool ProjectConcentratedOwnershipIndicatorSpecified
        {
            get { return this.ProjectConcentratedOwnershipIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public PROJECT_OWNERSHIP_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
