namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class INTEREST_RATE_PERIODIC_ADJUSTMENT_RULES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.InterestRatePeriodicAdjustmentRuleListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("INTEREST_RATE_PERIODIC_ADJUSTMENT_RULE", Order = 0)]
        public List<INTEREST_RATE_PERIODIC_ADJUSTMENT_RULE> InterestRatePeriodicAdjustmentRuleList { get; set; } = new List<INTEREST_RATE_PERIODIC_ADJUSTMENT_RULE>();
    
        [XmlIgnore]
        public bool InterestRatePeriodicAdjustmentRuleListSpecified
        {
            get { return this.InterestRatePeriodicAdjustmentRuleList != null && this.InterestRatePeriodicAdjustmentRuleList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public INTEREST_RATE_PERIODIC_ADJUSTMENT_RULES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
