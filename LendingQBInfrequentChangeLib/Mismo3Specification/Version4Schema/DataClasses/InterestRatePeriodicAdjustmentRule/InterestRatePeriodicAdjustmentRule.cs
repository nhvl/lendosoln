namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class INTEREST_RATE_PERIODIC_ADJUSTMENT_RULE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PeriodicBaseRatePercentSpecified
                    || this.PeriodicEffectiveDateSpecified
                    || this.PeriodicMaximumDecreaseRatePercentSpecified
                    || this.PeriodicMaximumIncreaseRatePercentSpecified
                    || this.PeriodicNextBaseRateSelectionDateSpecified
                    || this.PeriodicPaymentsBetweenBaseRatesCountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PeriodicBaseRatePercent", Order = 0)]
        public MISMOPercent PeriodicBaseRatePercent { get; set; }
    
        [XmlIgnore]
        public bool PeriodicBaseRatePercentSpecified
        {
            get { return this.PeriodicBaseRatePercent != null; }
            set { }
        }
    
        [XmlElement("PeriodicEffectiveDate", Order = 1)]
        public MISMODate PeriodicEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool PeriodicEffectiveDateSpecified
        {
            get { return this.PeriodicEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("PeriodicMaximumDecreaseRatePercent", Order = 2)]
        public MISMOPercent PeriodicMaximumDecreaseRatePercent { get; set; }
    
        [XmlIgnore]
        public bool PeriodicMaximumDecreaseRatePercentSpecified
        {
            get { return this.PeriodicMaximumDecreaseRatePercent != null; }
            set { }
        }
    
        [XmlElement("PeriodicMaximumIncreaseRatePercent", Order = 3)]
        public MISMOPercent PeriodicMaximumIncreaseRatePercent { get; set; }
    
        [XmlIgnore]
        public bool PeriodicMaximumIncreaseRatePercentSpecified
        {
            get { return this.PeriodicMaximumIncreaseRatePercent != null; }
            set { }
        }
    
        [XmlElement("PeriodicNextBaseRateSelectionDate", Order = 4)]
        public MISMODate PeriodicNextBaseRateSelectionDate { get; set; }
    
        [XmlIgnore]
        public bool PeriodicNextBaseRateSelectionDateSpecified
        {
            get { return this.PeriodicNextBaseRateSelectionDate != null; }
            set { }
        }
    
        [XmlElement("PeriodicPaymentsBetweenBaseRatesCount", Order = 5)]
        public MISMOCount PeriodicPaymentsBetweenBaseRatesCount { get; set; }
    
        [XmlIgnore]
        public bool PeriodicPaymentsBetweenBaseRatesCountSpecified
        {
            get { return this.PeriodicPaymentsBetweenBaseRatesCount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 6)]
        public INTEREST_RATE_PERIODIC_ADJUSTMENT_RULE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
