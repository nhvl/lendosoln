namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PRODUCT_CATEGORY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ProductCategoryNameSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ProductCategoryName", Order = 0)]
        public MISMOString ProductCategoryName { get; set; }
    
        [XmlIgnore]
        public bool ProductCategoryNameSpecified
        {
            get { return this.ProductCategoryName != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public PRODUCT_CATEGORY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
