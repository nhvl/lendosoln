namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class URLA
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.UrlaDetailSpecified
                    || this.UrlaTotalSpecified
                    || this.UrlaTotalHousingExpensesSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("URLA_DETAIL", Order = 0)]
        public URLA_DETAIL UrlaDetail { get; set; }
    
        [XmlIgnore]
        public bool UrlaDetailSpecified
        {
            get { return this.UrlaDetail != null && this.UrlaDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("URLA_TOTAL", Order = 1)]
        public URLA_TOTAL UrlaTotal { get; set; }
    
        [XmlIgnore]
        public bool UrlaTotalSpecified
        {
            get { return this.UrlaTotal != null && this.UrlaTotal.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("URLA_TOTAL_HOUSING_EXPENSES", Order = 2)]
        public URLA_TOTAL_HOUSING_EXPENSES UrlaTotalHousingExpenses { get; set; }
    
        [XmlIgnore]
        public bool UrlaTotalHousingExpensesSpecified
        {
            get { return this.UrlaTotalHousingExpenses != null && this.UrlaTotalHousingExpenses.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public URLA_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
