namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ABOUT_VERSION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AboutVersionIdentifierSpecified
                    || this.CreatedDatetimeSpecified
                    || this.DataVersionIdentifierSpecified
                    || this.DataVersionNameSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AboutVersionIdentifier", Order = 0)]
        public MISMOIdentifier AboutVersionIdentifier { get; set; }
    
        [XmlIgnore]
        public bool AboutVersionIdentifierSpecified
        {
            get { return this.AboutVersionIdentifier != null; }
            set { }
        }
    
        [XmlElement("CreatedDatetime", Order = 1)]
        public MISMODatetime CreatedDatetime { get; set; }
    
        [XmlIgnore]
        public bool CreatedDatetimeSpecified
        {
            get { return this.CreatedDatetime != null; }
            set { }
        }
    
        [XmlElement("DataVersionIdentifier", Order = 2)]
        public MISMOIdentifier DataVersionIdentifier { get; set; }
    
        [XmlIgnore]
        public bool DataVersionIdentifierSpecified
        {
            get { return this.DataVersionIdentifier != null; }
            set { }
        }
    
        [XmlElement("DataVersionName", Order = 3)]
        public MISMOString DataVersionName { get; set; }
    
        [XmlIgnore]
        public bool DataVersionNameSpecified
        {
            get { return this.DataVersionName != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public ABOUT_VERSION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
