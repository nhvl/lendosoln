namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class ABOUT_VERSIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AboutVersionListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ABOUT_VERSION", Order = 0)]
        public List<ABOUT_VERSION> AboutVersionList { get; set; } = new List<ABOUT_VERSION>();
    
        [XmlIgnore]
        public bool AboutVersionListSpecified
        {
            get { return this.AboutVersionList != null && this.AboutVersionList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public ABOUT_VERSIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
