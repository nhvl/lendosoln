namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class WORKOUT_COMMENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.WorkoutCommentTextSpecified
                    || this.WorkoutCommentTypeSpecified
                    || this.WorkoutCommentTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("WorkoutCommentText", Order = 0)]
        public MISMOString WorkoutCommentText { get; set; }
    
        [XmlIgnore]
        public bool WorkoutCommentTextSpecified
        {
            get { return this.WorkoutCommentText != null; }
            set { }
        }
    
        [XmlElement("WorkoutCommentType", Order = 1)]
        public MISMOEnum<WorkoutCommentBase> WorkoutCommentType { get; set; }
    
        [XmlIgnore]
        public bool WorkoutCommentTypeSpecified
        {
            get { return this.WorkoutCommentType != null; }
            set { }
        }
    
        [XmlElement("WorkoutCommentTypeOtherDescription", Order = 2)]
        public MISMOString WorkoutCommentTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool WorkoutCommentTypeOtherDescriptionSpecified
        {
            get { return this.WorkoutCommentTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public WORKOUT_COMMENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
