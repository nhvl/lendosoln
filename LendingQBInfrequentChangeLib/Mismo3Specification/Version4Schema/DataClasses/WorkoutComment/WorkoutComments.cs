namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class WORKOUT_COMMENTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.WorkoutCommentListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("WORKOUT_COMMENT", Order = 0)]
        public List<WORKOUT_COMMENT> WorkoutCommentList { get; set; } = new List<WORKOUT_COMMENT>();
    
        [XmlIgnore]
        public bool WorkoutCommentListSpecified
        {
            get { return this.WorkoutCommentList != null && this.WorkoutCommentList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public WORKOUT_COMMENTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
