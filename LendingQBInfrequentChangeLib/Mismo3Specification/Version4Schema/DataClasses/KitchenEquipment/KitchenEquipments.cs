namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class KITCHEN_EQUIPMENTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.KitchenEquipmentListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("KITCHEN_EQUIPMENT", Order = 0)]
        public List<KITCHEN_EQUIPMENT> KitchenEquipmentList { get; set; } = new List<KITCHEN_EQUIPMENT>();
    
        [XmlIgnore]
        public bool KitchenEquipmentListSpecified
        {
            get { return this.KitchenEquipmentList != null && this.KitchenEquipmentList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public KITCHEN_EQUIPMENTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
