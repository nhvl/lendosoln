namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class KITCHEN_EQUIPMENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ComponentAdjustmentAmountSpecified
                    || this.ComponentClassificationTypeSpecified
                    || this.ConditionRatingDescriptionSpecified
                    || this.ConditionRatingTypeSpecified
                    || this.KitchenEquipmentCountSpecified
                    || this.KitchenEquipmentDescriptionSpecified
                    || this.KitchenEquipmentMakeModelDescriptionSpecified
                    || this.KitchenEquipmentTypeSpecified
                    || this.KitchenEquipmentTypeOtherDescriptionSpecified
                    || this.QualityRatingDescriptionSpecified
                    || this.QualityRatingTypeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ComponentAdjustmentAmount", Order = 0)]
        public MISMOAmount ComponentAdjustmentAmount { get; set; }
    
        [XmlIgnore]
        public bool ComponentAdjustmentAmountSpecified
        {
            get { return this.ComponentAdjustmentAmount != null; }
            set { }
        }
    
        [XmlElement("ComponentClassificationType", Order = 1)]
        public MISMOEnum<ComponentClassificationBase> ComponentClassificationType { get; set; }
    
        [XmlIgnore]
        public bool ComponentClassificationTypeSpecified
        {
            get { return this.ComponentClassificationType != null; }
            set { }
        }
    
        [XmlElement("ConditionRatingDescription", Order = 2)]
        public MISMOString ConditionRatingDescription { get; set; }
    
        [XmlIgnore]
        public bool ConditionRatingDescriptionSpecified
        {
            get { return this.ConditionRatingDescription != null; }
            set { }
        }
    
        [XmlElement("ConditionRatingType", Order = 3)]
        public MISMOEnum<ConditionRatingBase> ConditionRatingType { get; set; }
    
        [XmlIgnore]
        public bool ConditionRatingTypeSpecified
        {
            get { return this.ConditionRatingType != null; }
            set { }
        }
    
        [XmlElement("KitchenEquipmentCount", Order = 4)]
        public MISMOCount KitchenEquipmentCount { get; set; }
    
        [XmlIgnore]
        public bool KitchenEquipmentCountSpecified
        {
            get { return this.KitchenEquipmentCount != null; }
            set { }
        }
    
        [XmlElement("KitchenEquipmentDescription", Order = 5)]
        public MISMOString KitchenEquipmentDescription { get; set; }
    
        [XmlIgnore]
        public bool KitchenEquipmentDescriptionSpecified
        {
            get { return this.KitchenEquipmentDescription != null; }
            set { }
        }
    
        [XmlElement("KitchenEquipmentMakeModelDescription", Order = 6)]
        public MISMOString KitchenEquipmentMakeModelDescription { get; set; }
    
        [XmlIgnore]
        public bool KitchenEquipmentMakeModelDescriptionSpecified
        {
            get { return this.KitchenEquipmentMakeModelDescription != null; }
            set { }
        }
    
        [XmlElement("KitchenEquipmentType", Order = 7)]
        public MISMOEnum<KitchenEquipmentBase> KitchenEquipmentType { get; set; }
    
        [XmlIgnore]
        public bool KitchenEquipmentTypeSpecified
        {
            get { return this.KitchenEquipmentType != null; }
            set { }
        }
    
        [XmlElement("KitchenEquipmentTypeOtherDescription", Order = 8)]
        public MISMOString KitchenEquipmentTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool KitchenEquipmentTypeOtherDescriptionSpecified
        {
            get { return this.KitchenEquipmentTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("QualityRatingDescription", Order = 9)]
        public MISMOString QualityRatingDescription { get; set; }
    
        [XmlIgnore]
        public bool QualityRatingDescriptionSpecified
        {
            get { return this.QualityRatingDescription != null; }
            set { }
        }
    
        [XmlElement("QualityRatingType", Order = 10)]
        public MISMOEnum<QualityRatingBase> QualityRatingType { get; set; }
    
        [XmlIgnore]
        public bool QualityRatingTypeSpecified
        {
            get { return this.QualityRatingType != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 11)]
        public KITCHEN_EQUIPMENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
