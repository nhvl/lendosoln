namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class CREDIT_SCORE_MODELS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditScoreModelListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CREDIT_SCORE_MODEL", Order = 0)]
        public List<CREDIT_SCORE_MODEL> CreditScoreModelList { get; set; } = new List<CREDIT_SCORE_MODEL>();
    
        [XmlIgnore]
        public bool CreditScoreModelListSpecified
        {
            get { return this.CreditScoreModelList != null && this.CreditScoreModelList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public CREDIT_SCORE_MODELS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
