namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CREDIT_SCORE_MODEL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditScoreHistogramIntervalsSpecified
                    || this.CreditScoreModelDetailSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CREDIT_SCORE_HISTOGRAM_INTERVALS", Order = 0)]
        public CREDIT_SCORE_HISTOGRAM_INTERVALS CreditScoreHistogramIntervals { get; set; }
    
        [XmlIgnore]
        public bool CreditScoreHistogramIntervalsSpecified
        {
            get { return this.CreditScoreHistogramIntervals != null && this.CreditScoreHistogramIntervals.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_SCORE_MODEL_DETAIL", Order = 1)]
        public CREDIT_SCORE_MODEL_DETAIL CreditScoreModelDetail { get; set; }
    
        [XmlIgnore]
        public bool CreditScoreModelDetailSpecified
        {
            get { return this.CreditScoreModelDetail != null && this.CreditScoreModelDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public CREDIT_SCORE_MODEL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
