namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CREDIT_SCORE_MODEL_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditScoreCategoryTypeSpecified
                    || this.CreditScoreCategoryTypeOtherDescriptionSpecified
                    || this.CreditScoreMaximumValueSpecified
                    || this.CreditScoreMinimumValueSpecified
                    || this.CreditScoreModelNameTypeSpecified
                    || this.CreditScoreModelNameTypeOtherDescriptionSpecified
                    || this.CreditScoreModelRangeOfScoresDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CreditScoreCategoryType", Order = 0)]
        public MISMOEnum<CreditScoreCategoryBase> CreditScoreCategoryType { get; set; }
    
        [XmlIgnore]
        public bool CreditScoreCategoryTypeSpecified
        {
            get { return this.CreditScoreCategoryType != null; }
            set { }
        }
    
        [XmlElement("CreditScoreCategoryTypeOtherDescription", Order = 1)]
        public MISMOString CreditScoreCategoryTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CreditScoreCategoryTypeOtherDescriptionSpecified
        {
            get { return this.CreditScoreCategoryTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("CreditScoreMaximumValue", Order = 2)]
        public MISMOValue CreditScoreMaximumValue { get; set; }
    
        [XmlIgnore]
        public bool CreditScoreMaximumValueSpecified
        {
            get { return this.CreditScoreMaximumValue != null; }
            set { }
        }
    
        [XmlElement("CreditScoreMinimumValue", Order = 3)]
        public MISMOValue CreditScoreMinimumValue { get; set; }
    
        [XmlIgnore]
        public bool CreditScoreMinimumValueSpecified
        {
            get { return this.CreditScoreMinimumValue != null; }
            set { }
        }
    
        [XmlElement("CreditScoreModelNameType", Order = 4)]
        public MISMOEnum<CreditScoreModelNameBase> CreditScoreModelNameType { get; set; }
    
        [XmlIgnore]
        public bool CreditScoreModelNameTypeSpecified
        {
            get { return this.CreditScoreModelNameType != null; }
            set { }
        }
    
        [XmlElement("CreditScoreModelNameTypeOtherDescription", Order = 5)]
        public MISMOString CreditScoreModelNameTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CreditScoreModelNameTypeOtherDescriptionSpecified
        {
            get { return this.CreditScoreModelNameTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("CreditScoreModelRangeOfScoresDescription", Order = 6)]
        public MISMOString CreditScoreModelRangeOfScoresDescription { get; set; }
    
        [XmlIgnore]
        public bool CreditScoreModelRangeOfScoresDescriptionSpecified
        {
            get { return this.CreditScoreModelRangeOfScoresDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 7)]
        public CREDIT_SCORE_MODEL_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
