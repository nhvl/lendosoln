namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class DEFENDANT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PrimaryDefendantIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PrimaryDefendantIndicator", Order = 0)]
        public MISMOIndicator PrimaryDefendantIndicator { get; set; }
    
        [XmlIgnore]
        public bool PrimaryDefendantIndicatorSpecified
        {
            get { return this.PrimaryDefendantIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public DEFENDANT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
