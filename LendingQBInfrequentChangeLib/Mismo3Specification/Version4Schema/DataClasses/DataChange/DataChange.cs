namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class DATA_CHANGE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DataItemChangeRequestSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DATA_ITEM_CHANGE_REQUEST", Order = 0)]
        public DATA_ITEM_CHANGE_REQUEST DataItemChangeRequest { get; set; }
    
        [XmlIgnore]
        public bool DataItemChangeRequestSpecified
        {
            get { return this.DataItemChangeRequest != null && this.DataItemChangeRequest.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public DATA_CHANGE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
