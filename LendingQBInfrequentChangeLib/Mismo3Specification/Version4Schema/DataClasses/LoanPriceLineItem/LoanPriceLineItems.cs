namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class LOAN_PRICE_LINE_ITEMS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LoanPriceLineItemListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LOAN_PRICE_LINE_ITEM", Order = 0)]
        public List<LOAN_PRICE_LINE_ITEM> LoanPriceLineItemList { get; set; } = new List<LOAN_PRICE_LINE_ITEM>();
    
        [XmlIgnore]
        public bool LoanPriceLineItemListSpecified
        {
            get { return this.LoanPriceLineItemList != null && this.LoanPriceLineItemList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public LOAN_PRICE_LINE_ITEMS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
