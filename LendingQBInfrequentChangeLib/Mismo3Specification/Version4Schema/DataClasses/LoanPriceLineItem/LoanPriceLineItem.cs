namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class LOAN_PRICE_LINE_ITEM
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LoanPriceLineItemAmountSpecified
                    || this.LoanPriceLineItemCategoryDescriptionSpecified
                    || this.LoanPriceLineItemPercentSpecified
                    || this.LoanPriceLineItemTypeSpecified
                    || this.LoanPriceLineItemTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LoanPriceLineItemAmount", Order = 0)]
        public MISMOAmount LoanPriceLineItemAmount { get; set; }
    
        [XmlIgnore]
        public bool LoanPriceLineItemAmountSpecified
        {
            get { return this.LoanPriceLineItemAmount != null; }
            set { }
        }
    
        [XmlElement("LoanPriceLineItemCategoryDescription", Order = 1)]
        public MISMOString LoanPriceLineItemCategoryDescription { get; set; }
    
        [XmlIgnore]
        public bool LoanPriceLineItemCategoryDescriptionSpecified
        {
            get { return this.LoanPriceLineItemCategoryDescription != null; }
            set { }
        }
    
        [XmlElement("LoanPriceLineItemPercent", Order = 2)]
        public MISMOPercent LoanPriceLineItemPercent { get; set; }
    
        [XmlIgnore]
        public bool LoanPriceLineItemPercentSpecified
        {
            get { return this.LoanPriceLineItemPercent != null; }
            set { }
        }
    
        [XmlElement("LoanPriceLineItemType", Order = 3)]
        public MISMOEnum<LoanPriceLineItemBase> LoanPriceLineItemType { get; set; }
    
        [XmlIgnore]
        public bool LoanPriceLineItemTypeSpecified
        {
            get { return this.LoanPriceLineItemType != null; }
            set { }
        }
    
        [XmlElement("LoanPriceLineItemTypeOtherDescription", Order = 4)]
        public MISMOString LoanPriceLineItemTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool LoanPriceLineItemTypeOtherDescriptionSpecified
        {
            get { return this.LoanPriceLineItemTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public LOAN_PRICE_LINE_ITEM_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
