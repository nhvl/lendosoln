namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class EMPLOYERS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EmployerListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("EMPLOYER", Order = 0)]
        public List<EMPLOYER> EmployerList { get; set; } = new List<EMPLOYER>();
    
        [XmlIgnore]
        public bool EmployerListSpecified
        {
            get { return this.EmployerList != null && this.EmployerList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public EMPLOYERS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
