namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Xml.Schema;
    using System.Xml.Serialization;

    public class EMPLOYER
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                if (Mismo3Utilities.ExceedsThreshold(1,
                    new List<bool> { this.IndividualSpecified, this.LegalEntitySpecified }))
                {
                    throw new System.Xml.Schema.XmlSchemaValidationException(
                        Mismo3Utilities.GetXSDChoiceExceptionMessage(
                        "EMPLOYER",
                        new List<string> { "INDIVIDUAL", "LEGAL_ENTITY" }));
                }

                return this.IndividualSpecified
                    || this.LegalEntitySpecified
                    || this.AddressSpecified
                    || this.CreditCommentsSpecified
                    || this.EmploymentSpecified
                    || this.EmploymentDocumentationsSpecified
                    || this.VerificationSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("INDIVIDUAL", Order = 0)]
        public INDIVIDUAL Individual { get; set; }
    
        [XmlIgnore]
        public bool IndividualSpecified
        {
            get { return this.Individual != null && this.Individual.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("LEGAL_ENTITY", Order = 1)]
        public LEGAL_ENTITY LegalEntity { get; set; }
    
        [XmlIgnore]
        public bool LegalEntitySpecified
        {
            get { return this.LegalEntity != null && this.LegalEntity.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("ADDRESS", Order = 2)]
        public ADDRESS Address { get; set; }
    
        [XmlIgnore]
        public bool AddressSpecified
        {
            get { return this.Address != null && this.Address.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_COMMENTS", Order = 3)]
        public CREDIT_COMMENTS CreditComments { get; set; }
    
        [XmlIgnore]
        public bool CreditCommentsSpecified
        {
            get { return this.CreditComments != null && this.CreditComments.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EMPLOYMENT", Order = 4)]
        public EMPLOYMENT Employment { get; set; }
    
        [XmlIgnore]
        public bool EmploymentSpecified
        {
            get { return this.Employment != null && this.Employment.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EMPLOYMENT_DOCUMENTATIONS", Order = 5)]
        public EMPLOYMENT_DOCUMENTATIONS EmploymentDocumentations { get; set; }
    
        [XmlIgnore]
        public bool EmploymentDocumentationsSpecified
        {
            get { return this.EmploymentDocumentations != null && this.EmploymentDocumentations.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("VERIFICATION", Order = 6)]
        public VERIFICATION Verification { get; set; }
    
        [XmlIgnore]
        public bool VerificationSpecified
        {
            get { return this.Verification != null && this.Verification.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 7)]
        public EMPLOYER_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }

        [XmlAttribute("label", Form = XmlSchemaForm.Qualified, Namespace = Mismo3Constants.XlinkNamespace)]
        public string XlinkLabel { get; set; }

        [XmlIgnore]
        public bool XlinkLabelSpecified
        {
            get { return !string.IsNullOrEmpty(this.XlinkLabel); }
            set { }
        }
    }
}
