namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class ESCROW_ITEM_DISBURSEMENTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EscrowItemDisbursementListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ESCROW_ITEM_DISBURSEMENT", Order = 0)]
        public List<ESCROW_ITEM_DISBURSEMENT> EscrowItemDisbursementList { get; set; } = new List<ESCROW_ITEM_DISBURSEMENT>();
    
        [XmlIgnore]
        public bool EscrowItemDisbursementListSpecified
        {
            get { return this.EscrowItemDisbursementList != null && this.EscrowItemDisbursementList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public ESCROW_ITEM_DISBURSEMENTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
