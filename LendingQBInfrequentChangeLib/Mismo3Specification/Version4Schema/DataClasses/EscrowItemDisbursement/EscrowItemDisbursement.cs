namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ESCROW_ITEM_DISBURSEMENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EscrowItemDisbursedDateSpecified
                    || this.EscrowItemDisbursementAmountSpecified
                    || this.EscrowItemDisbursementDateSpecified
                    || this.EscrowItemDisbursementTermMonthsCountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("EscrowItemDisbursedDate", Order = 0)]
        public MISMODate EscrowItemDisbursedDate { get; set; }
    
        [XmlIgnore]
        public bool EscrowItemDisbursedDateSpecified
        {
            get { return this.EscrowItemDisbursedDate != null; }
            set { }
        }
    
        [XmlElement("EscrowItemDisbursementAmount", Order = 1)]
        public MISMOAmount EscrowItemDisbursementAmount { get; set; }
    
        [XmlIgnore]
        public bool EscrowItemDisbursementAmountSpecified
        {
            get { return this.EscrowItemDisbursementAmount != null; }
            set { }
        }
    
        [XmlElement("EscrowItemDisbursementDate", Order = 2)]
        public MISMODate EscrowItemDisbursementDate { get; set; }
    
        [XmlIgnore]
        public bool EscrowItemDisbursementDateSpecified
        {
            get { return this.EscrowItemDisbursementDate != null; }
            set { }
        }
    
        [XmlElement("EscrowItemDisbursementTermMonthsCount", Order = 3)]
        public MISMOCount EscrowItemDisbursementTermMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool EscrowItemDisbursementTermMonthsCountSpecified
        {
            get { return this.EscrowItemDisbursementTermMonthsCount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public ESCROW_ITEM_DISBURSEMENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
