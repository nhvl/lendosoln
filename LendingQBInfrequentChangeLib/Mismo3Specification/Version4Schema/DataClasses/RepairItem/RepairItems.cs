namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class REPAIR_ITEMS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.RepairItemListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("REPAIR_ITEM", Order = 0)]
        public List<REPAIR_ITEM> RepairItemList { get; set; } = new List<REPAIR_ITEM>();
    
        [XmlIgnore]
        public bool RepairItemListSpecified
        {
            get { return this.RepairItemList != null && this.RepairItemList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public REPAIR_ITEMS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
