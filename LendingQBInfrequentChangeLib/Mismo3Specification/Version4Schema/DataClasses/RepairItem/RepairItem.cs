namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class REPAIR_ITEM
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.RepairItemCostAmountSpecified
                    || this.RepairItemDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("RepairItemCostAmount", Order = 0)]
        public MISMOAmount RepairItemCostAmount { get; set; }
    
        [XmlIgnore]
        public bool RepairItemCostAmountSpecified
        {
            get { return this.RepairItemCostAmount != null; }
            set { }
        }
    
        [XmlElement("RepairItemDescription", Order = 1)]
        public MISMOString RepairItemDescription { get; set; }
    
        [XmlIgnore]
        public bool RepairItemDescriptionSpecified
        {
            get { return this.RepairItemDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public REPAIR_ITEM_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
