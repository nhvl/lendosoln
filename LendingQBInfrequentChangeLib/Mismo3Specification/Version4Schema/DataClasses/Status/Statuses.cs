namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class STATUSES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.StatusListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("STATUS", Order = 0)]
        public List<STATUS> StatusList { get; set; } = new List<STATUS>();
    
        [XmlIgnore]
        public bool StatusListSpecified
        {
            get { return this.StatusList != null && this.StatusList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public STATUSES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
