namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class STATUS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.StatusCodeSpecified
                    || this.StatusConditionDescriptionSpecified
                    || this.StatusDescriptionSpecified
                    || this.StatusNameSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("StatusCode", Order = 0)]
        public MISMOCode StatusCode { get; set; }
    
        [XmlIgnore]
        public bool StatusCodeSpecified
        {
            get { return this.StatusCode != null; }
            set { }
        }
    
        [XmlElement("StatusConditionDescription", Order = 1)]
        public MISMOString StatusConditionDescription { get; set; }
    
        [XmlIgnore]
        public bool StatusConditionDescriptionSpecified
        {
            get { return this.StatusConditionDescription != null; }
            set { }
        }
    
        [XmlElement("StatusDescription", Order = 2)]
        public MISMOString StatusDescription { get; set; }
    
        [XmlIgnore]
        public bool StatusDescriptionSpecified
        {
            get { return this.StatusDescription != null; }
            set { }
        }
    
        [XmlElement("StatusName", Order = 3)]
        public MISMOString StatusName { get; set; }
    
        [XmlIgnore]
        public bool StatusNameSpecified
        {
            get { return this.StatusName != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public STATUS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
