namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class BUYDOWN_RULE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BuydownBaseDateTypeSpecified
                    || this.BuydownBaseDateTypeOtherDescriptionSpecified
                    || this.BuydownCalculationTypeSpecified
                    || this.BuydownChangeFrequencyMonthsCountSpecified
                    || this.BuydownDurationMonthsCountSpecified
                    || this.BuydownIncreaseRatePercentSpecified
                    || this.BuydownInitialDiscountPercentSpecified
                    || this.BuydownLenderFundingIndicatorSpecified
                    || this.BuydownPayoffDisbursementTypeSpecified
                    || this.BuydownReflectedInNoteIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BuydownBaseDateType", Order = 0)]
        public MISMOEnum<BuydownBaseDateBase> BuydownBaseDateType { get; set; }
    
        [XmlIgnore]
        public bool BuydownBaseDateTypeSpecified
        {
            get { return this.BuydownBaseDateType != null; }
            set { }
        }
    
        [XmlElement("BuydownBaseDateTypeOtherDescription", Order = 1)]
        public MISMOString BuydownBaseDateTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool BuydownBaseDateTypeOtherDescriptionSpecified
        {
            get { return this.BuydownBaseDateTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("BuydownCalculationType", Order = 2)]
        public MISMOEnum<BuydownCalculationBase> BuydownCalculationType { get; set; }
    
        [XmlIgnore]
        public bool BuydownCalculationTypeSpecified
        {
            get { return this.BuydownCalculationType != null; }
            set { }
        }
    
        [XmlElement("BuydownChangeFrequencyMonthsCount", Order = 3)]
        public MISMOCount BuydownChangeFrequencyMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool BuydownChangeFrequencyMonthsCountSpecified
        {
            get { return this.BuydownChangeFrequencyMonthsCount != null; }
            set { }
        }
    
        [XmlElement("BuydownDurationMonthsCount", Order = 4)]
        public MISMOCount BuydownDurationMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool BuydownDurationMonthsCountSpecified
        {
            get { return this.BuydownDurationMonthsCount != null; }
            set { }
        }
    
        [XmlElement("BuydownIncreaseRatePercent", Order = 5)]
        public MISMOPercent BuydownIncreaseRatePercent { get; set; }
    
        [XmlIgnore]
        public bool BuydownIncreaseRatePercentSpecified
        {
            get { return this.BuydownIncreaseRatePercent != null; }
            set { }
        }
    
        [XmlElement("BuydownInitialDiscountPercent", Order = 6)]
        public MISMOPercent BuydownInitialDiscountPercent { get; set; }
    
        [XmlIgnore]
        public bool BuydownInitialDiscountPercentSpecified
        {
            get { return this.BuydownInitialDiscountPercent != null; }
            set { }
        }
    
        [XmlElement("BuydownLenderFundingIndicator", Order = 7)]
        public MISMOIndicator BuydownLenderFundingIndicator { get; set; }
    
        [XmlIgnore]
        public bool BuydownLenderFundingIndicatorSpecified
        {
            get { return this.BuydownLenderFundingIndicator != null; }
            set { }
        }
    
        [XmlElement("BuydownPayoffDisbursementType", Order = 8)]
        public MISMOEnum<BuydownPayoffDisbursementBase> BuydownPayoffDisbursementType { get; set; }
    
        [XmlIgnore]
        public bool BuydownPayoffDisbursementTypeSpecified
        {
            get { return this.BuydownPayoffDisbursementType != null; }
            set { }
        }
    
        [XmlElement("BuydownReflectedInNoteIndicator", Order = 9)]
        public MISMOIndicator BuydownReflectedInNoteIndicator { get; set; }
    
        [XmlIgnore]
        public bool BuydownReflectedInNoteIndicatorSpecified
        {
            get { return this.BuydownReflectedInNoteIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 10)]
        public BUYDOWN_RULE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
