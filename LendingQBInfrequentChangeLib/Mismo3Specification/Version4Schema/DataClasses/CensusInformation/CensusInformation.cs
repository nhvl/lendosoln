namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CENSUS_INFORMATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CensusBlockGroupIdentifierSpecified
                    || this.CensusBlockIdentifierSpecified
                    || this.CensusTractBaseIdentifierSpecified
                    || this.CensusTractIdentifierSpecified
                    || this.CensusTractSuffixIdentifierSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CensusBlockGroupIdentifier", Order = 0)]
        public MISMOIdentifier CensusBlockGroupIdentifier { get; set; }
    
        [XmlIgnore]
        public bool CensusBlockGroupIdentifierSpecified
        {
            get { return this.CensusBlockGroupIdentifier != null; }
            set { }
        }
    
        [XmlElement("CensusBlockIdentifier", Order = 1)]
        public MISMOIdentifier CensusBlockIdentifier { get; set; }
    
        [XmlIgnore]
        public bool CensusBlockIdentifierSpecified
        {
            get { return this.CensusBlockIdentifier != null; }
            set { }
        }
    
        [XmlElement("CensusTractBaseIdentifier", Order = 2)]
        public MISMOIdentifier CensusTractBaseIdentifier { get; set; }
    
        [XmlIgnore]
        public bool CensusTractBaseIdentifierSpecified
        {
            get { return this.CensusTractBaseIdentifier != null; }
            set { }
        }
    
        [XmlElement("CensusTractIdentifier", Order = 3)]
        public MISMOIdentifier CensusTractIdentifier { get; set; }
    
        [XmlIgnore]
        public bool CensusTractIdentifierSpecified
        {
            get { return this.CensusTractIdentifier != null; }
            set { }
        }
    
        [XmlElement("CensusTractSuffixIdentifier", Order = 4)]
        public MISMOIdentifier CensusTractSuffixIdentifier { get; set; }
    
        [XmlIgnore]
        public bool CensusTractSuffixIdentifierSpecified
        {
            get { return this.CensusTractSuffixIdentifier != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public CENSUS_INFORMATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
