namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class DELINQUENCY_STATUS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DelinquentPaymentCountSpecified
                    || this.LoanDelinquencyStatusDateSpecified
                    || this.LoanDelinquencyStatusTypeSpecified
                    || this.LoanDelinquencyStatusTypeOtherDescriptionSpecified
                    || this.PaymentDelinquencyStatusTypeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DelinquentPaymentCount", Order = 0)]
        public MISMOCount DelinquentPaymentCount { get; set; }
    
        [XmlIgnore]
        public bool DelinquentPaymentCountSpecified
        {
            get { return this.DelinquentPaymentCount != null; }
            set { }
        }
    
        [XmlElement("LoanDelinquencyStatusDate", Order = 1)]
        public MISMODate LoanDelinquencyStatusDate { get; set; }
    
        [XmlIgnore]
        public bool LoanDelinquencyStatusDateSpecified
        {
            get { return this.LoanDelinquencyStatusDate != null; }
            set { }
        }
    
        [XmlElement("LoanDelinquencyStatusType", Order = 2)]
        public MISMOEnum<LoanDelinquencyStatusBase> LoanDelinquencyStatusType { get; set; }
    
        [XmlIgnore]
        public bool LoanDelinquencyStatusTypeSpecified
        {
            get { return this.LoanDelinquencyStatusType != null; }
            set { }
        }
    
        [XmlElement("LoanDelinquencyStatusTypeOtherDescription", Order = 3)]
        public MISMOString LoanDelinquencyStatusTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool LoanDelinquencyStatusTypeOtherDescriptionSpecified
        {
            get { return this.LoanDelinquencyStatusTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PaymentDelinquencyStatusType", Order = 4)]
        public MISMOEnum<PaymentDelinquencyStatusBase> PaymentDelinquencyStatusType { get; set; }
    
        [XmlIgnore]
        public bool PaymentDelinquencyStatusTypeSpecified
        {
            get { return this.PaymentDelinquencyStatusType != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public DELINQUENCY_STATUS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
