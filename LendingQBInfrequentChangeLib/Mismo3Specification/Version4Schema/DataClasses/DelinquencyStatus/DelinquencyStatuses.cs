namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class DELINQUENCY_STATUSES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DelinquencyStatusListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DELINQUENCY_STATUS", Order = 0)]
        public List<DELINQUENCY_STATUS> DelinquencyStatusList { get; set; } = new List<DELINQUENCY_STATUS>();
    
        [XmlIgnore]
        public bool DelinquencyStatusListSpecified
        {
            get { return this.DelinquencyStatusList != null && this.DelinquencyStatusList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public DELINQUENCY_STATUSES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
