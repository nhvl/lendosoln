namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class BUILDING_PERMITS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BuildingPermitListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BUILDING_PERMIT", Order = 0)]
        public List<BUILDING_PERMIT> BuildingPermitList { get; set; } = new List<BUILDING_PERMIT>();
    
        [XmlIgnore]
        public bool BuildingPermitListSpecified
        {
            get { return this.BuildingPermitList != null && this.BuildingPermitList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public BUILDING_PERMITS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
