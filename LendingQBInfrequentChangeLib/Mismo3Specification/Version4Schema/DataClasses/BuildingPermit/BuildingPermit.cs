namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class BUILDING_PERMIT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BuildingPermitLevelTypeSpecified
                    || this.BuildingPermitLevelTypeOtherDescriptionSpecified
                    || this.BuildingPermitProposedUseDescriptionSpecified
                    || this.BuildingPermitReferenceIdentifierSpecified
                    || this.BuildingPermitStatusDateSpecified
                    || this.BuildingPermitStatusDescriptionSpecified
                    || this.BuildingPermitStatusTypeSpecified
                    || this.BuildingPermitStatusTypeOtherDescriptionSpecified
                    || this.BuildingPermitTradeDescriptionSpecified
                    || this.BuildingPermitUsageStandardDescriptionSpecified
                    || this.BuildingPermitUsageStandardTypeSpecified
                    || this.BuildingPermitUsageStandardTypeOtherDescriptionSpecified
                    || this.BuildingPermitWorkPurposeDescriptionSpecified
                    || this.BuildingPermitWorkPurposeTypeSpecified
                    || this.BuildingPermitWorkPurposeTypeOtherDescriptionSpecified
                    || this.BuildingPermitWorkTypeSpecified
                    || this.BuildingPermitWorkTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BuildingPermitLevelType", Order = 0)]
        public MISMOEnum<BuildingPermitLevelBase> BuildingPermitLevelType { get; set; }
    
        [XmlIgnore]
        public bool BuildingPermitLevelTypeSpecified
        {
            get { return this.BuildingPermitLevelType != null; }
            set { }
        }
    
        [XmlElement("BuildingPermitLevelTypeOtherDescription", Order = 1)]
        public MISMOString BuildingPermitLevelTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool BuildingPermitLevelTypeOtherDescriptionSpecified
        {
            get { return this.BuildingPermitLevelTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("BuildingPermitProposedUseDescription", Order = 2)]
        public MISMOString BuildingPermitProposedUseDescription { get; set; }
    
        [XmlIgnore]
        public bool BuildingPermitProposedUseDescriptionSpecified
        {
            get { return this.BuildingPermitProposedUseDescription != null; }
            set { }
        }
    
        [XmlElement("BuildingPermitReferenceIdentifier", Order = 3)]
        public MISMOIdentifier BuildingPermitReferenceIdentifier { get; set; }
    
        [XmlIgnore]
        public bool BuildingPermitReferenceIdentifierSpecified
        {
            get { return this.BuildingPermitReferenceIdentifier != null; }
            set { }
        }
    
        [XmlElement("BuildingPermitStatusDate", Order = 4)]
        public MISMODate BuildingPermitStatusDate { get; set; }
    
        [XmlIgnore]
        public bool BuildingPermitStatusDateSpecified
        {
            get { return this.BuildingPermitStatusDate != null; }
            set { }
        }
    
        [XmlElement("BuildingPermitStatusDescription", Order = 5)]
        public MISMOString BuildingPermitStatusDescription { get; set; }
    
        [XmlIgnore]
        public bool BuildingPermitStatusDescriptionSpecified
        {
            get { return this.BuildingPermitStatusDescription != null; }
            set { }
        }
    
        [XmlElement("BuildingPermitStatusType", Order = 6)]
        public MISMOEnum<BuildingPermitStatusBase> BuildingPermitStatusType { get; set; }
    
        [XmlIgnore]
        public bool BuildingPermitStatusTypeSpecified
        {
            get { return this.BuildingPermitStatusType != null; }
            set { }
        }
    
        [XmlElement("BuildingPermitStatusTypeOtherDescription", Order = 7)]
        public MISMOString BuildingPermitStatusTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool BuildingPermitStatusTypeOtherDescriptionSpecified
        {
            get { return this.BuildingPermitStatusTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("BuildingPermitTradeDescription", Order = 8)]
        public MISMOString BuildingPermitTradeDescription { get; set; }
    
        [XmlIgnore]
        public bool BuildingPermitTradeDescriptionSpecified
        {
            get { return this.BuildingPermitTradeDescription != null; }
            set { }
        }
    
        [XmlElement("BuildingPermitUsageStandardDescription", Order = 9)]
        public MISMOString BuildingPermitUsageStandardDescription { get; set; }
    
        [XmlIgnore]
        public bool BuildingPermitUsageStandardDescriptionSpecified
        {
            get { return this.BuildingPermitUsageStandardDescription != null; }
            set { }
        }
    
        [XmlElement("BuildingPermitUsageStandardType", Order = 10)]
        public MISMOEnum<BuildingPermitUsageStandardBase> BuildingPermitUsageStandardType { get; set; }
    
        [XmlIgnore]
        public bool BuildingPermitUsageStandardTypeSpecified
        {
            get { return this.BuildingPermitUsageStandardType != null; }
            set { }
        }
    
        [XmlElement("BuildingPermitUsageStandardTypeOtherDescription", Order = 11)]
        public MISMOString BuildingPermitUsageStandardTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool BuildingPermitUsageStandardTypeOtherDescriptionSpecified
        {
            get { return this.BuildingPermitUsageStandardTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("BuildingPermitWorkPurposeDescription", Order = 12)]
        public MISMOString BuildingPermitWorkPurposeDescription { get; set; }
    
        [XmlIgnore]
        public bool BuildingPermitWorkPurposeDescriptionSpecified
        {
            get { return this.BuildingPermitWorkPurposeDescription != null; }
            set { }
        }
    
        [XmlElement("BuildingPermitWorkPurposeType", Order = 13)]
        public MISMOEnum<BuildingPermitWorkPurposeBase> BuildingPermitWorkPurposeType { get; set; }
    
        [XmlIgnore]
        public bool BuildingPermitWorkPurposeTypeSpecified
        {
            get { return this.BuildingPermitWorkPurposeType != null; }
            set { }
        }
    
        [XmlElement("BuildingPermitWorkPurposeTypeOtherDescription", Order = 14)]
        public MISMOString BuildingPermitWorkPurposeTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool BuildingPermitWorkPurposeTypeOtherDescriptionSpecified
        {
            get { return this.BuildingPermitWorkPurposeTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("BuildingPermitWorkType", Order = 15)]
        public MISMOEnum<BuildingPermitWorkBase> BuildingPermitWorkType { get; set; }
    
        [XmlIgnore]
        public bool BuildingPermitWorkTypeSpecified
        {
            get { return this.BuildingPermitWorkType != null; }
            set { }
        }
    
        [XmlElement("BuildingPermitWorkTypeOtherDescription", Order = 16)]
        public MISMOString BuildingPermitWorkTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool BuildingPermitWorkTypeOtherDescriptionSpecified
        {
            get { return this.BuildingPermitWorkTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 17)]
        public BUILDING_PERMIT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
