namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class PRORATION_ITEMS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ProrationItemListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PRORATION_ITEM", Order = 0)]
        public List<PRORATION_ITEM> ProrationItemList { get; set; } = new List<PRORATION_ITEM>();
    
        [XmlIgnore]
        public bool ProrationItemListSpecified
        {
            get { return this.ProrationItemList != null && this.ProrationItemList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public PRORATION_ITEMS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
