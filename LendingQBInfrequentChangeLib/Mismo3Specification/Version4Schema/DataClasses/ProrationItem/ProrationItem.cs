namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PRORATION_ITEM
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.IntegratedDisclosureLineNumberValueSpecified
                    || this.IntegratedDisclosureSectionTypeSpecified
                    || this.IntegratedDisclosureSectionTypeOtherDescriptionSpecified
                    || this.IntegratedDisclosureSubsectionTypeSpecified
                    || this.IntegratedDisclosureSubsectionTypeOtherDescriptionSpecified
                    || this.ProrationItemAmountSpecified
                    || this.ProrationItemPaidFromDateSpecified
                    || this.ProrationItemPaidThroughDateSpecified
                    || this.ProrationItemTypeSpecified
                    || this.ProrationItemTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("IntegratedDisclosureLineNumberValue", Order = 0)]
        public MISMOValue IntegratedDisclosureLineNumberValue { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureLineNumberValueSpecified
        {
            get { return this.IntegratedDisclosureLineNumberValue != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureSectionType", Order = 1)]
        public MISMOEnum<IntegratedDisclosureSectionBase> IntegratedDisclosureSectionType { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureSectionTypeSpecified
        {
            get { return this.IntegratedDisclosureSectionType != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureSectionTypeOtherDescription", Order = 2)]
        public MISMOString IntegratedDisclosureSectionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureSectionTypeOtherDescriptionSpecified
        {
            get { return this.IntegratedDisclosureSectionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureSubsectionType", Order = 3)]
        public MISMOEnum<IntegratedDisclosureSubsectionBase> IntegratedDisclosureSubsectionType { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureSubsectionTypeSpecified
        {
            get { return this.IntegratedDisclosureSubsectionType != null; }
            set { }
        }
    
        [XmlElement("IntegratedDisclosureSubsectionTypeOtherDescription", Order = 4)]
        public MISMOString IntegratedDisclosureSubsectionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureSubsectionTypeOtherDescriptionSpecified
        {
            get { return this.IntegratedDisclosureSubsectionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ProrationItemAmount", Order = 5)]
        public MISMOAmount ProrationItemAmount { get; set; }
    
        [XmlIgnore]
        public bool ProrationItemAmountSpecified
        {
            get { return this.ProrationItemAmount != null; }
            set { }
        }
    
        [XmlElement("ProrationItemPaidFromDate", Order = 6)]
        public MISMODate ProrationItemPaidFromDate { get; set; }
    
        [XmlIgnore]
        public bool ProrationItemPaidFromDateSpecified
        {
            get { return this.ProrationItemPaidFromDate != null; }
            set { }
        }
    
        [XmlElement("ProrationItemPaidThroughDate", Order = 7)]
        public MISMODate ProrationItemPaidThroughDate { get; set; }
    
        [XmlIgnore]
        public bool ProrationItemPaidThroughDateSpecified
        {
            get { return this.ProrationItemPaidThroughDate != null; }
            set { }
        }
    
        [XmlElement("ProrationItemType", Order = 8)]
        public MISMOEnum<ProrationItemBase> ProrationItemType { get; set; }
    
        [XmlIgnore]
        public bool ProrationItemTypeSpecified
        {
            get { return this.ProrationItemType != null; }
            set { }
        }
    
        [XmlElement("ProrationItemTypeOtherDescription", Order = 9)]
        public MISMOString ProrationItemTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ProrationItemTypeOtherDescriptionSpecified
        {
            get { return this.ProrationItemTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 10)]
        public PRORATION_ITEM_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
