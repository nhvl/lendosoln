namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class OWNED_PROPERTY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.OwnedPropertyDetailSpecified
                    || this.PropertySpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("OWNED_PROPERTY_DETAIL", Order = 0)]
        public OWNED_PROPERTY_DETAIL OwnedPropertyDetail { get; set; }
    
        [XmlIgnore]
        public bool OwnedPropertyDetailSpecified
        {
            get { return this.OwnedPropertyDetail != null && this.OwnedPropertyDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PROPERTY", Order = 1)]
        public PROPERTY Property { get; set; }
    
        [XmlIgnore]
        public bool PropertySpecified
        {
            get { return this.Property != null && this.Property.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public OWNED_PROPERTY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
