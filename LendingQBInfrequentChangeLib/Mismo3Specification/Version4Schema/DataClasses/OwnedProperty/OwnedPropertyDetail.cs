namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class OWNED_PROPERTY_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.OwnedPropertyDispositionStatusTypeSpecified
                    || this.OwnedPropertyLienInstallmentAmountSpecified
                    || this.OwnedPropertyLienUPBAmountSpecified
                    || this.OwnedPropertyMaintenanceExpenseAmountSpecified
                    || this.OwnedPropertyOwnedUnitCountSpecified
                    || this.OwnedPropertyRentalIncomeGrossAmountSpecified
                    || this.OwnedPropertyRentalIncomeNetAmountSpecified
                    || this.OwnedPropertySubjectIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("OwnedPropertyDispositionStatusType", Order = 0)]
        public MISMOEnum<OwnedPropertyDispositionStatusBase> OwnedPropertyDispositionStatusType { get; set; }
    
        [XmlIgnore]
        public bool OwnedPropertyDispositionStatusTypeSpecified
        {
            get { return this.OwnedPropertyDispositionStatusType != null; }
            set { }
        }
    
        [XmlElement("OwnedPropertyLienInstallmentAmount", Order = 1)]
        public MISMOAmount OwnedPropertyLienInstallmentAmount { get; set; }
    
        [XmlIgnore]
        public bool OwnedPropertyLienInstallmentAmountSpecified
        {
            get { return this.OwnedPropertyLienInstallmentAmount != null; }
            set { }
        }
    
        [XmlElement("OwnedPropertyLienUPBAmount", Order = 2)]
        public MISMOAmount OwnedPropertyLienUPBAmount { get; set; }
    
        [XmlIgnore]
        public bool OwnedPropertyLienUPBAmountSpecified
        {
            get { return this.OwnedPropertyLienUPBAmount != null; }
            set { }
        }
    
        [XmlElement("OwnedPropertyMaintenanceExpenseAmount", Order = 3)]
        public MISMOAmount OwnedPropertyMaintenanceExpenseAmount { get; set; }
    
        [XmlIgnore]
        public bool OwnedPropertyMaintenanceExpenseAmountSpecified
        {
            get { return this.OwnedPropertyMaintenanceExpenseAmount != null; }
            set { }
        }
    
        [XmlElement("OwnedPropertyOwnedUnitCount", Order = 4)]
        public MISMOCount OwnedPropertyOwnedUnitCount { get; set; }
    
        [XmlIgnore]
        public bool OwnedPropertyOwnedUnitCountSpecified
        {
            get { return this.OwnedPropertyOwnedUnitCount != null; }
            set { }
        }
    
        [XmlElement("OwnedPropertyRentalIncomeGrossAmount", Order = 5)]
        public MISMOAmount OwnedPropertyRentalIncomeGrossAmount { get; set; }
    
        [XmlIgnore]
        public bool OwnedPropertyRentalIncomeGrossAmountSpecified
        {
            get { return this.OwnedPropertyRentalIncomeGrossAmount != null; }
            set { }
        }
    
        [XmlElement("OwnedPropertyRentalIncomeNetAmount", Order = 6)]
        public MISMOAmount OwnedPropertyRentalIncomeNetAmount { get; set; }
    
        [XmlIgnore]
        public bool OwnedPropertyRentalIncomeNetAmountSpecified
        {
            get { return this.OwnedPropertyRentalIncomeNetAmount != null; }
            set { }
        }
    
        [XmlElement("OwnedPropertySubjectIndicator", Order = 7)]
        public MISMOIndicator OwnedPropertySubjectIndicator { get; set; }
    
        [XmlIgnore]
        public bool OwnedPropertySubjectIndicatorSpecified
        {
            get { return this.OwnedPropertySubjectIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 8)]
        public OWNED_PROPERTY_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
