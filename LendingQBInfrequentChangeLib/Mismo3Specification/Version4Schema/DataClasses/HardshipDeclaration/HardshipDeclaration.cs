namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class HARDSHIP_DECLARATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CashReservesInsufficientIndicatorSpecified
                    || this.ExpensesIncreasedIndicatorSpecified
                    || this.HardshipExplanationDescriptionSpecified
                    || this.HouseholdFinancialCircumstancesChangedIndicatorSpecified
                    || this.IncomeReducedOrLostIndicatorSpecified
                    || this.MonthlyDebtPaymentsExcessiveIndicatorSpecified
                    || this.OtherHardshipReasonsIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CashReservesInsufficientIndicator", Order = 0)]
        public MISMOIndicator CashReservesInsufficientIndicator { get; set; }
    
        [XmlIgnore]
        public bool CashReservesInsufficientIndicatorSpecified
        {
            get { return this.CashReservesInsufficientIndicator != null; }
            set { }
        }
    
        [XmlElement("ExpensesIncreasedIndicator", Order = 1)]
        public MISMOIndicator ExpensesIncreasedIndicator { get; set; }
    
        [XmlIgnore]
        public bool ExpensesIncreasedIndicatorSpecified
        {
            get { return this.ExpensesIncreasedIndicator != null; }
            set { }
        }
    
        [XmlElement("HardshipExplanationDescription", Order = 2)]
        public MISMOString HardshipExplanationDescription { get; set; }
    
        [XmlIgnore]
        public bool HardshipExplanationDescriptionSpecified
        {
            get { return this.HardshipExplanationDescription != null; }
            set { }
        }
    
        [XmlElement("HouseholdFinancialCircumstancesChangedIndicator", Order = 3)]
        public MISMOIndicator HouseholdFinancialCircumstancesChangedIndicator { get; set; }
    
        [XmlIgnore]
        public bool HouseholdFinancialCircumstancesChangedIndicatorSpecified
        {
            get { return this.HouseholdFinancialCircumstancesChangedIndicator != null; }
            set { }
        }
    
        [XmlElement("IncomeReducedOrLostIndicator", Order = 4)]
        public MISMOIndicator IncomeReducedOrLostIndicator { get; set; }
    
        [XmlIgnore]
        public bool IncomeReducedOrLostIndicatorSpecified
        {
            get { return this.IncomeReducedOrLostIndicator != null; }
            set { }
        }
    
        [XmlElement("MonthlyDebtPaymentsExcessiveIndicator", Order = 5)]
        public MISMOIndicator MonthlyDebtPaymentsExcessiveIndicator { get; set; }
    
        [XmlIgnore]
        public bool MonthlyDebtPaymentsExcessiveIndicatorSpecified
        {
            get { return this.MonthlyDebtPaymentsExcessiveIndicator != null; }
            set { }
        }
    
        [XmlElement("OtherHardshipReasonsIndicator", Order = 6)]
        public MISMOIndicator OtherHardshipReasonsIndicator { get; set; }
    
        [XmlIgnore]
        public bool OtherHardshipReasonsIndicatorSpecified
        {
            get { return this.OtherHardshipReasonsIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 7)]
        public HARDSHIP_DECLARATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
