namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class STOP_CODES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.StopCodeListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("STOP_CODE", Order = 0)]
        public List<STOP_CODE> StopCodeList { get; set; } = new List<STOP_CODE>();
    
        [XmlIgnore]
        public bool StopCodeListSpecified
        {
            get { return this.StopCodeList != null && this.StopCodeList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public STOP_CODES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
