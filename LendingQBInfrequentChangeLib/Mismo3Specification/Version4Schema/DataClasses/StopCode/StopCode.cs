namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class STOP_CODE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.StopCodeActionTypeSpecified
                    || this.StopCodeActionTypeOtherDescriptionSpecified
                    || this.StopCodeConditionTypeSpecified
                    || this.StopCodeConditionTypeOtherDescriptionSpecified
                    || this.StopCodeExpirationDateSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("StopCodeActionType", Order = 0)]
        public MISMOEnum<StopCodeActionBase> StopCodeActionType { get; set; }
    
        [XmlIgnore]
        public bool StopCodeActionTypeSpecified
        {
            get { return this.StopCodeActionType != null; }
            set { }
        }
    
        [XmlElement("StopCodeActionTypeOtherDescription", Order = 1)]
        public MISMOString StopCodeActionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool StopCodeActionTypeOtherDescriptionSpecified
        {
            get { return this.StopCodeActionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("StopCodeConditionType", Order = 2)]
        public MISMOEnum<StopCodeConditionBase> StopCodeConditionType { get; set; }
    
        [XmlIgnore]
        public bool StopCodeConditionTypeSpecified
        {
            get { return this.StopCodeConditionType != null; }
            set { }
        }
    
        [XmlElement("StopCodeConditionTypeOtherDescription", Order = 3)]
        public MISMOString StopCodeConditionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool StopCodeConditionTypeOtherDescriptionSpecified
        {
            get { return this.StopCodeConditionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("StopCodeExpirationDate", Order = 4)]
        public MISMODate StopCodeExpirationDate { get; set; }
    
        [XmlIgnore]
        public bool StopCodeExpirationDateSpecified
        {
            get { return this.StopCodeExpirationDate != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public STOP_CODE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
