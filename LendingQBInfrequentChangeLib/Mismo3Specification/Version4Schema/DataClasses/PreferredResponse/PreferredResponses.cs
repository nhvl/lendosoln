namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class PREFERRED_RESPONSES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PreferredResponseListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PREFERRED_RESPONSE", Order = 0)]
        public List<PREFERRED_RESPONSE> PreferredResponseList { get; set; } = new List<PREFERRED_RESPONSE>();
    
        [XmlIgnore]
        public bool PreferredResponseListSpecified
        {
            get { return this.PreferredResponseList != null && this.PreferredResponseList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public PREFERRED_RESPONSES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
