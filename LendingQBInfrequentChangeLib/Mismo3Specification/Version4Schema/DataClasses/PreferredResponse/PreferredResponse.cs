namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PREFERRED_RESPONSE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MIMETypeIdentifierSpecified
                    || this.PreferredResponseDestinationDescriptionSpecified
                    || this.PreferredResponseFormatTypeSpecified
                    || this.PreferredResponseFormatTypeOtherDescriptionSpecified
                    || this.PreferredResponseMethodTypeSpecified
                    || this.PreferredResponseMethodTypeOtherDescriptionSpecified
                    || this.PreferredResponseUseEmbeddedFileIndicatorSpecified
                    || this.PreferredResponseVersionIdentifierSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MIMETypeIdentifier", Order = 0)]
        public MISMOIdentifier MIMETypeIdentifier { get; set; }
    
        [XmlIgnore]
        public bool MIMETypeIdentifierSpecified
        {
            get { return this.MIMETypeIdentifier != null; }
            set { }
        }
    
        [XmlElement("PreferredResponseDestinationDescription", Order = 1)]
        public MISMOString PreferredResponseDestinationDescription { get; set; }
    
        [XmlIgnore]
        public bool PreferredResponseDestinationDescriptionSpecified
        {
            get { return this.PreferredResponseDestinationDescription != null; }
            set { }
        }
    
        [XmlElement("PreferredResponseFormatType", Order = 2)]
        public MISMOEnum<PreferredResponseFormatBase> PreferredResponseFormatType { get; set; }
    
        [XmlIgnore]
        public bool PreferredResponseFormatTypeSpecified
        {
            get { return this.PreferredResponseFormatType != null; }
            set { }
        }
    
        [XmlElement("PreferredResponseFormatTypeOtherDescription", Order = 3)]
        public MISMOString PreferredResponseFormatTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PreferredResponseFormatTypeOtherDescriptionSpecified
        {
            get { return this.PreferredResponseFormatTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PreferredResponseMethodType", Order = 4)]
        public MISMOEnum<PreferredResponseMethodBase> PreferredResponseMethodType { get; set; }
    
        [XmlIgnore]
        public bool PreferredResponseMethodTypeSpecified
        {
            get { return this.PreferredResponseMethodType != null; }
            set { }
        }
    
        [XmlElement("PreferredResponseMethodTypeOtherDescription", Order = 5)]
        public MISMOString PreferredResponseMethodTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PreferredResponseMethodTypeOtherDescriptionSpecified
        {
            get { return this.PreferredResponseMethodTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PreferredResponseUseEmbeddedFileIndicator", Order = 6)]
        public MISMOIndicator PreferredResponseUseEmbeddedFileIndicator { get; set; }
    
        [XmlIgnore]
        public bool PreferredResponseUseEmbeddedFileIndicatorSpecified
        {
            get { return this.PreferredResponseUseEmbeddedFileIndicator != null; }
            set { }
        }
    
        [XmlElement("PreferredResponseVersionIdentifier", Order = 7)]
        public MISMOIdentifier PreferredResponseVersionIdentifier { get; set; }
    
        [XmlIgnore]
        public bool PreferredResponseVersionIdentifierSpecified
        {
            get { return this.PreferredResponseVersionIdentifier != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 8)]
        public PREFERRED_RESPONSE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
