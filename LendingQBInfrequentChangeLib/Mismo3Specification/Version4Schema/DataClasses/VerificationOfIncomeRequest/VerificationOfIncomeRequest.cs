namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class VERIFICATION_OF_INCOME_REQUEST
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.RequestForTaxReturnDocumentationSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("REQUEST_FOR_TAX_RETURN_DOCUMENTATION", Order = 0)]
        public REQUEST_FOR_TAX_RETURN_DOCUMENTATION RequestForTaxReturnDocumentation { get; set; }
    
        [XmlIgnore]
        public bool RequestForTaxReturnDocumentationSpecified
        {
            get { return this.RequestForTaxReturnDocumentation != null && this.RequestForTaxReturnDocumentation.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public VERIFICATION_OF_INCOME_REQUEST_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
