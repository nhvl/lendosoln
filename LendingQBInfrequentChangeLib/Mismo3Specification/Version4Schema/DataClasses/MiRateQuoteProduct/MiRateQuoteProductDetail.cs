namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MI_RATE_QUOTE_PRODUCT_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BaseLoanAmountSpecified
                    || this.FiveYearCostComparisonAmountSpecified
                    || this.HousingCostOverFiveYearsAmountSpecified
                    || this.MIDurationTypeSpecified
                    || this.MIDurationTypeOtherDescriptionSpecified
                    || this.MIEligibleIndicatorSpecified
                    || this.MIPremiumFinancedAmountSpecified
                    || this.MIPremiumFinancedIndicatorSpecified
                    || this.MIPremiumRefundableTypeSpecified
                    || this.MIPremiumRefundableTypeOtherDescriptionSpecified
                    || this.MIPremiumSourceTypeSpecified
                    || this.MIPremiumSourceTypeOtherDescriptionSpecified
                    || this.MIPremiumUpfrontAmountSpecified
                    || this.MIPremiumUpfrontPercentSpecified
                    || this.MIProductDescriptionSpecified
                    || this.MIRateFoundIndicatorSpecified
                    || this.MIRenewalCalculationTypeSpecified
                    || this.MIRenewalCalculationTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BaseLoanAmount", Order = 0)]
        public MISMOAmount BaseLoanAmount { get; set; }
    
        [XmlIgnore]
        public bool BaseLoanAmountSpecified
        {
            get { return this.BaseLoanAmount != null; }
            set { }
        }
    
        [XmlElement("FiveYearCostComparisonAmount", Order = 1)]
        public MISMOAmount FiveYearCostComparisonAmount { get; set; }
    
        [XmlIgnore]
        public bool FiveYearCostComparisonAmountSpecified
        {
            get { return this.FiveYearCostComparisonAmount != null; }
            set { }
        }
    
        [XmlElement("HousingCostOverFiveYearsAmount", Order = 2)]
        public MISMOAmount HousingCostOverFiveYearsAmount { get; set; }
    
        [XmlIgnore]
        public bool HousingCostOverFiveYearsAmountSpecified
        {
            get { return this.HousingCostOverFiveYearsAmount != null; }
            set { }
        }
    
        [XmlElement("MIDurationType", Order = 3)]
        public MISMOEnum<MIDurationBase> MIDurationType { get; set; }
    
        [XmlIgnore]
        public bool MIDurationTypeSpecified
        {
            get { return this.MIDurationType != null; }
            set { }
        }
    
        [XmlElement("MIDurationTypeOtherDescription", Order = 4)]
        public MISMOString MIDurationTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MIDurationTypeOtherDescriptionSpecified
        {
            get { return this.MIDurationTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MIEligibleIndicator", Order = 5)]
        public MISMOIndicator MIEligibleIndicator { get; set; }
    
        [XmlIgnore]
        public bool MIEligibleIndicatorSpecified
        {
            get { return this.MIEligibleIndicator != null; }
            set { }
        }
    
        [XmlElement("MIPremiumFinancedAmount", Order = 6)]
        public MISMOAmount MIPremiumFinancedAmount { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumFinancedAmountSpecified
        {
            get { return this.MIPremiumFinancedAmount != null; }
            set { }
        }
    
        [XmlElement("MIPremiumFinancedIndicator", Order = 7)]
        public MISMOIndicator MIPremiumFinancedIndicator { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumFinancedIndicatorSpecified
        {
            get { return this.MIPremiumFinancedIndicator != null; }
            set { }
        }
    
        [XmlElement("MIPremiumRefundableType", Order = 8)]
        public MISMOEnum<MIPremiumRefundableBase> MIPremiumRefundableType { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumRefundableTypeSpecified
        {
            get { return this.MIPremiumRefundableType != null; }
            set { }
        }
    
        [XmlElement("MIPremiumRefundableTypeOtherDescription", Order = 9)]
        public MISMOString MIPremiumRefundableTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumRefundableTypeOtherDescriptionSpecified
        {
            get { return this.MIPremiumRefundableTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MIPremiumSourceType", Order = 10)]
        public MISMOEnum<MIPremiumSourceBase> MIPremiumSourceType { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumSourceTypeSpecified
        {
            get { return this.MIPremiumSourceType != null; }
            set { }
        }
    
        [XmlElement("MIPremiumSourceTypeOtherDescription", Order = 11)]
        public MISMOString MIPremiumSourceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumSourceTypeOtherDescriptionSpecified
        {
            get { return this.MIPremiumSourceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MIPremiumUpfrontAmount", Order = 12)]
        public MISMOAmount MIPremiumUpfrontAmount { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumUpfrontAmountSpecified
        {
            get { return this.MIPremiumUpfrontAmount != null; }
            set { }
        }
    
        [XmlElement("MIPremiumUpfrontPercent", Order = 13)]
        public MISMOPercent MIPremiumUpfrontPercent { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumUpfrontPercentSpecified
        {
            get { return this.MIPremiumUpfrontPercent != null; }
            set { }
        }
    
        [XmlElement("MIProductDescription", Order = 14)]
        public MISMOString MIProductDescription { get; set; }
    
        [XmlIgnore]
        public bool MIProductDescriptionSpecified
        {
            get { return this.MIProductDescription != null; }
            set { }
        }
    
        [XmlElement("MIRateFoundIndicator", Order = 15)]
        public MISMOIndicator MIRateFoundIndicator { get; set; }
    
        [XmlIgnore]
        public bool MIRateFoundIndicatorSpecified
        {
            get { return this.MIRateFoundIndicator != null; }
            set { }
        }
    
        [XmlElement("MIRenewalCalculationType", Order = 16)]
        public MISMOEnum<MIRenewalCalculationBase> MIRenewalCalculationType { get; set; }
    
        [XmlIgnore]
        public bool MIRenewalCalculationTypeSpecified
        {
            get { return this.MIRenewalCalculationType != null; }
            set { }
        }
    
        [XmlElement("MIRenewalCalculationTypeOtherDescription", Order = 17)]
        public MISMOString MIRenewalCalculationTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MIRenewalCalculationTypeOtherDescriptionSpecified
        {
            get { return this.MIRenewalCalculationTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 18)]
        public MI_RATE_QUOTE_PRODUCT_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
