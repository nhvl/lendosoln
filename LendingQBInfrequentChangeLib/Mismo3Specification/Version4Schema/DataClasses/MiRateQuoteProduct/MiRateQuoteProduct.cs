namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MI_RATE_QUOTE_PRODUCT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.HousingExpensesSpecified
                    || this.LoanIdentifiersSpecified
                    || this.MiPremiumTaxesSpecified
                    || this.MiPremiumsSpecified
                    || this.MiRateQuoteProductDetailSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("HOUSING_EXPENSES", Order = 0)]
        public HOUSING_EXPENSES HousingExpenses { get; set; }
    
        [XmlIgnore]
        public bool HousingExpensesSpecified
        {
            get { return this.HousingExpenses != null && this.HousingExpenses.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("LOAN_IDENTIFIERS", Order = 1)]
        public LOAN_IDENTIFIERS LoanIdentifiers { get; set; }
    
        [XmlIgnore]
        public bool LoanIdentifiersSpecified
        {
            get { return this.LoanIdentifiers != null && this.LoanIdentifiers.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("MI_PREMIUM_TAXES", Order = 2)]
        public MI_PREMIUM_TAXES MiPremiumTaxes { get; set; }
    
        [XmlIgnore]
        public bool MiPremiumTaxesSpecified
        {
            get { return this.MiPremiumTaxes != null && this.MiPremiumTaxes.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("MI_PREMIUMS", Order = 3)]
        public MI_PREMIUMS MiPremiums { get; set; }
    
        [XmlIgnore]
        public bool MiPremiumsSpecified
        {
            get { return this.MiPremiums != null && this.MiPremiums.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("MI_RATE_QUOTE_PRODUCT_DETAIL", Order = 4)]
        public MI_RATE_QUOTE_PRODUCT_DETAIL MiRateQuoteProductDetail { get; set; }
    
        [XmlIgnore]
        public bool MiRateQuoteProductDetailSpecified
        {
            get { return this.MiRateQuoteProductDetail != null && this.MiRateQuoteProductDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public MI_RATE_QUOTE_PRODUCT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
