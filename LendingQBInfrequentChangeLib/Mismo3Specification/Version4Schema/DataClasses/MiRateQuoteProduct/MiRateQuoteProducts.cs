namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class MI_RATE_QUOTE_PRODUCTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MiRateQuoteProductListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MI_RATE_QUOTE_PRODUCT", Order = 0)]
        public List<MI_RATE_QUOTE_PRODUCT> MiRateQuoteProductList { get; set; } = new List<MI_RATE_QUOTE_PRODUCT>();
    
        [XmlIgnore]
        public bool MiRateQuoteProductListSpecified
        {
            get { return this.MiRateQuoteProductList != null && this.MiRateQuoteProductList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public MI_RATE_QUOTE_PRODUCTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
