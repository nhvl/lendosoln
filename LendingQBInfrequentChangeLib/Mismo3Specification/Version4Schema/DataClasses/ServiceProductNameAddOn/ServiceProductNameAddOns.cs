namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class SERVICE_PRODUCT_NAME_ADD_ONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ServiceProductNameAddOnListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SERVICE_PRODUCT_NAME_ADD_ON", Order = 0)]
        public List<SERVICE_PRODUCT_NAME_ADD_ON> ServiceProductNameAddOnList { get; set; } = new List<SERVICE_PRODUCT_NAME_ADD_ON>();
    
        [XmlIgnore]
        public bool ServiceProductNameAddOnListSpecified
        {
            get { return this.ServiceProductNameAddOnList != null && this.ServiceProductNameAddOnList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public SERVICE_PRODUCT_NAME_ADD_ONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
