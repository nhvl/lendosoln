namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SERVICE_PRODUCT_NAME_ADD_ON
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ServiceProductNameAddOnDescriptionSpecified
                    || this.ServiceProductNameAddOnIdentifierSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ServiceProductNameAddOnDescription", Order = 0)]
        public MISMOString ServiceProductNameAddOnDescription { get; set; }
    
        [XmlIgnore]
        public bool ServiceProductNameAddOnDescriptionSpecified
        {
            get { return this.ServiceProductNameAddOnDescription != null; }
            set { }
        }
    
        [XmlElement("ServiceProductNameAddOnIdentifier", Order = 1)]
        public MISMOIdentifier ServiceProductNameAddOnIdentifier { get; set; }
    
        [XmlIgnore]
        public bool ServiceProductNameAddOnIdentifierSpecified
        {
            get { return this.ServiceProductNameAddOnIdentifier != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public SERVICE_PRODUCT_NAME_ADD_ON_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
