namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MI_RATE_QUOTE_RESPONSE_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BorrowerRequestedInterestRatePercentSpecified
                    || this.LoanIdentifierSpecified
                    || this.LoanIdentifierTypeSpecified
                    || this.LoanIdentifierTypeOtherDescriptionSpecified
                    || this.MIApplicationTypeSpecified
                    || this.MIApplicationTypeOtherDescriptionSpecified
                    || this.MICompanyNameTypeSpecified
                    || this.MICompanyNameTypeOtherDescriptionSpecified
                    || this.MICoveragePercentSpecified
                    || this.MILenderIdentifierSpecified
                    || this.MIRateQuoteDisclaimerTextSpecified
                    || this.MIRateQuoteExpirationDateSpecified
                    || this.MIServiceTypeSpecified
                    || this.MIServiceTypeOtherDescriptionSpecified
                    || this.MITransactionIdentifierSpecified
                    || this.RateQuoteAllProductIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BorrowerRequestedInterestRatePercent", Order = 0)]
        public MISMOPercent BorrowerRequestedInterestRatePercent { get; set; }
    
        [XmlIgnore]
        public bool BorrowerRequestedInterestRatePercentSpecified
        {
            get { return this.BorrowerRequestedInterestRatePercent != null; }
            set { }
        }
    
        [XmlElement("LoanIdentifier", Order = 1)]
        public MISMOIdentifier LoanIdentifier { get; set; }
    
        [XmlIgnore]
        public bool LoanIdentifierSpecified
        {
            get { return this.LoanIdentifier != null; }
            set { }
        }
    
        [XmlElement("LoanIdentifierType", Order = 2)]
        public MISMOEnum<LoanIdentifierBase> LoanIdentifierType { get; set; }
    
        [XmlIgnore]
        public bool LoanIdentifierTypeSpecified
        {
            get { return this.LoanIdentifierType != null; }
            set { }
        }
    
        [XmlElement("LoanIdentifierTypeOtherDescription", Order = 3)]
        public MISMOString LoanIdentifierTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool LoanIdentifierTypeOtherDescriptionSpecified
        {
            get { return this.LoanIdentifierTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MIApplicationType", Order = 4)]
        public MISMOEnum<MIApplicationBase> MIApplicationType { get; set; }
    
        [XmlIgnore]
        public bool MIApplicationTypeSpecified
        {
            get { return this.MIApplicationType != null; }
            set { }
        }
    
        [XmlElement("MIApplicationTypeOtherDescription", Order = 5)]
        public MISMOString MIApplicationTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MIApplicationTypeOtherDescriptionSpecified
        {
            get { return this.MIApplicationTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MICompanyNameType", Order = 6)]
        public MISMOEnum<MICompanyNameBase> MICompanyNameType { get; set; }
    
        [XmlIgnore]
        public bool MICompanyNameTypeSpecified
        {
            get { return this.MICompanyNameType != null; }
            set { }
        }
    
        [XmlElement("MICompanyNameTypeOtherDescription", Order = 7)]
        public MISMOString MICompanyNameTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MICompanyNameTypeOtherDescriptionSpecified
        {
            get { return this.MICompanyNameTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MICoveragePercent", Order = 8)]
        public MISMOPercent MICoveragePercent { get; set; }
    
        [XmlIgnore]
        public bool MICoveragePercentSpecified
        {
            get { return this.MICoveragePercent != null; }
            set { }
        }
    
        [XmlElement("MILenderIdentifier", Order = 9)]
        public MISMOIdentifier MILenderIdentifier { get; set; }
    
        [XmlIgnore]
        public bool MILenderIdentifierSpecified
        {
            get { return this.MILenderIdentifier != null; }
            set { }
        }
    
        [XmlElement("MIRateQuoteDisclaimerText", Order = 10)]
        public MISMOString MIRateQuoteDisclaimerText { get; set; }
    
        [XmlIgnore]
        public bool MIRateQuoteDisclaimerTextSpecified
        {
            get { return this.MIRateQuoteDisclaimerText != null; }
            set { }
        }
    
        [XmlElement("MIRateQuoteExpirationDate", Order = 11)]
        public MISMODate MIRateQuoteExpirationDate { get; set; }
    
        [XmlIgnore]
        public bool MIRateQuoteExpirationDateSpecified
        {
            get { return this.MIRateQuoteExpirationDate != null; }
            set { }
        }
    
        [XmlElement("MIServiceType", Order = 12)]
        public MISMOEnum<MIServiceBase> MIServiceType { get; set; }
    
        [XmlIgnore]
        public bool MIServiceTypeSpecified
        {
            get { return this.MIServiceType != null; }
            set { }
        }
    
        [XmlElement("MIServiceTypeOtherDescription", Order = 13)]
        public MISMOString MIServiceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MIServiceTypeOtherDescriptionSpecified
        {
            get { return this.MIServiceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MITransactionIdentifier", Order = 14)]
        public MISMOIdentifier MITransactionIdentifier { get; set; }
    
        [XmlIgnore]
        public bool MITransactionIdentifierSpecified
        {
            get { return this.MITransactionIdentifier != null; }
            set { }
        }
    
        [XmlElement("RateQuoteAllProductIndicator", Order = 15)]
        public MISMOIndicator RateQuoteAllProductIndicator { get; set; }
    
        [XmlIgnore]
        public bool RateQuoteAllProductIndicatorSpecified
        {
            get { return this.RateQuoteAllProductIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 16)]
        public MI_RATE_QUOTE_RESPONSE_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
