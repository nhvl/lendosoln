namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MI_RATE_QUOTE_RESPONSE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MiRateQuoteProductsSpecified
                    || this.MiRateQuoteResponseDetailSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MI_RATE_QUOTE_PRODUCTS", Order = 0)]
        public MI_RATE_QUOTE_PRODUCTS MiRateQuoteProducts { get; set; }
    
        [XmlIgnore]
        public bool MiRateQuoteProductsSpecified
        {
            get { return this.MiRateQuoteProducts != null && this.MiRateQuoteProducts.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("MI_RATE_QUOTE_RESPONSE_DETAIL", Order = 1)]
        public MI_RATE_QUOTE_RESPONSE_DETAIL MiRateQuoteResponseDetail { get; set; }
    
        [XmlIgnore]
        public bool MiRateQuoteResponseDetailSpecified
        {
            get { return this.MiRateQuoteResponseDetail != null && this.MiRateQuoteResponseDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public MI_RATE_QUOTE_RESPONSE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
