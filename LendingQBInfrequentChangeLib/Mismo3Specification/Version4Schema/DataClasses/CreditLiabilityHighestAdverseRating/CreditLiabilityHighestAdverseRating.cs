namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CREDIT_LIABILITY_HIGHEST_ADVERSE_RATING
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditLiabilityHighestAdverseRatingAmountSpecified
                    || this.CreditLiabilityHighestAdverseRatingCodeSpecified
                    || this.CreditLiabilityHighestAdverseRatingDateSpecified
                    || this.CreditLiabilityHighestAdverseRatingTypeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CreditLiabilityHighestAdverseRatingAmount", Order = 0)]
        public MISMOAmount CreditLiabilityHighestAdverseRatingAmount { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityHighestAdverseRatingAmountSpecified
        {
            get { return this.CreditLiabilityHighestAdverseRatingAmount != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityHighestAdverseRatingCode", Order = 1)]
        public MISMOCode CreditLiabilityHighestAdverseRatingCode { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityHighestAdverseRatingCodeSpecified
        {
            get { return this.CreditLiabilityHighestAdverseRatingCode != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityHighestAdverseRatingDate", Order = 2)]
        public MISMODate CreditLiabilityHighestAdverseRatingDate { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityHighestAdverseRatingDateSpecified
        {
            get { return this.CreditLiabilityHighestAdverseRatingDate != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityHighestAdverseRatingType", Order = 3)]
        public MISMOEnum<CreditLiabilityHighestAdverseRatingBase> CreditLiabilityHighestAdverseRatingType { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityHighestAdverseRatingTypeSpecified
        {
            get { return this.CreditLiabilityHighestAdverseRatingType != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public CREDIT_LIABILITY_HIGHEST_ADVERSE_RATING_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
