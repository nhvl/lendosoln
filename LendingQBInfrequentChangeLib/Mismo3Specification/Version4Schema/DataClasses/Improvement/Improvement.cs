namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class IMPROVEMENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CarStoragesSpecified
                    || this.ComparisonToNeighborhoodsSpecified
                    || this.GreenBuildingCertificationsSpecified
                    || this.ImprovementFeatureSpecified
                    || this.InteriorSpecified
                    || this.PropertyPreservationsSpecified
                    || this.StructureSpecified
                    || this.SystemSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CAR_STORAGES", Order = 0)]
        public CAR_STORAGES CarStorages { get; set; }
    
        [XmlIgnore]
        public bool CarStoragesSpecified
        {
            get { return this.CarStorages != null && this.CarStorages.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("COMPARISON_TO_NEIGHBORHOODS", Order = 1)]
        public COMPARISON_TO_NEIGHBORHOODS ComparisonToNeighborhoods { get; set; }
    
        [XmlIgnore]
        public bool ComparisonToNeighborhoodsSpecified
        {
            get { return this.ComparisonToNeighborhoods != null && this.ComparisonToNeighborhoods.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("GREEN_BUILDING_CERTIFICATIONS", Order = 2)]
        public GREEN_BUILDING_CERTIFICATIONS GreenBuildingCertifications { get; set; }
    
        [XmlIgnore]
        public bool GreenBuildingCertificationsSpecified
        {
            get { return this.GreenBuildingCertifications != null && this.GreenBuildingCertifications.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("IMPROVEMENT_FEATURE", Order = 3)]
        public IMPROVEMENT_FEATURE ImprovementFeature { get; set; }
    
        [XmlIgnore]
        public bool ImprovementFeatureSpecified
        {
            get { return this.ImprovementFeature != null && this.ImprovementFeature.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("INTERIOR", Order = 4)]
        public INTERIOR Interior { get; set; }
    
        [XmlIgnore]
        public bool InteriorSpecified
        {
            get { return this.Interior != null && this.Interior.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PROPERTY_PRESERVATIONS", Order = 5)]
        public PROPERTY_PRESERVATIONS PropertyPreservations { get; set; }
    
        [XmlIgnore]
        public bool PropertyPreservationsSpecified
        {
            get { return this.PropertyPreservations != null && this.PropertyPreservations.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("STRUCTURE", Order = 6)]
        public STRUCTURE Structure { get; set; }
    
        [XmlIgnore]
        public bool StructureSpecified
        {
            get { return this.Structure != null && this.Structure.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SYSTEM", Order = 7)]
        public SYSTEM System { get; set; }
    
        [XmlIgnore]
        public bool SystemSpecified
        {
            get { return this.System != null && this.System.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 8)]
        public IMPROVEMENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
