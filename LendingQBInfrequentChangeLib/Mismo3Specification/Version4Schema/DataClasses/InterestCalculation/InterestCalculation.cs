namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class INTEREST_CALCULATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.InterestCalculationOccurrencesSpecified
                    || this.InterestCalculationRulesSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("INTEREST_CALCULATION_OCCURRENCES", Order = 0)]
        public INTEREST_CALCULATION_OCCURRENCES InterestCalculationOccurrences { get; set; }
    
        [XmlIgnore]
        public bool InterestCalculationOccurrencesSpecified
        {
            get { return this.InterestCalculationOccurrences != null && this.InterestCalculationOccurrences.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("INTEREST_CALCULATION_RULES", Order = 1)]
        public INTEREST_CALCULATION_RULES InterestCalculationRules { get; set; }
    
        [XmlIgnore]
        public bool InterestCalculationRulesSpecified
        {
            get { return this.InterestCalculationRules != null && this.InterestCalculationRules.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public INTEREST_CALCULATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
