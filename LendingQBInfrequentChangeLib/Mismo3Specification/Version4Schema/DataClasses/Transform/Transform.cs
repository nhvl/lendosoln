namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    public class TRANSFORM
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                if (Mismo3Utilities.ExceedsThreshold(1,
                    new List<bool> { this.TransformFilesSpecified, this.TransformPagesSpecified }))
                {
                    throw new System.Xml.Schema.XmlSchemaValidationException(
                        Mismo3Utilities.GetXSDChoiceExceptionMessage(
                        "TRANSFORM",
                        new List<string> { "TRANSFORM_FILES", "TRANSFORM_PAGES" }));
                }

                return this.TransformFilesSpecified
                    || this.TransformPagesSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("TRANSFORM_FILES", Order = 0)]
        public TRANSFORM_FILES TransformFiles { get; set; }
    
        [XmlIgnore]
        public bool TransformFilesSpecified
        {
            get { return this.TransformFiles != null && this.TransformFiles.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("TRANSFORM_PAGES", Order = 1)]
        public TRANSFORM_PAGES TransformPages { get; set; }
    
        [XmlIgnore]
        public bool TransformPagesSpecified
        {
            get { return this.TransformPages != null && this.TransformPages.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public TRANSFORM_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
