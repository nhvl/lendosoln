namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class TRANSFORMS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.TransformListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("TRANSFORM", Order = 0)]
        public List<TRANSFORM> TransformList { get; set; } = new List<TRANSFORM>();
    
        [XmlIgnore]
        public bool TransformListSpecified
        {
            get { return this.TransformList != null && this.TransformList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public TRANSFORMS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
