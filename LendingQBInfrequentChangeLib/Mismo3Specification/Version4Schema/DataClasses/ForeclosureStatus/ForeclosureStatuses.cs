namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class FORECLOSURE_STATUSES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ForeclosureStatusListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("FORECLOSURE_STATUS", Order = 0)]
        public List<FORECLOSURE_STATUS> ForeclosureStatusList { get; set; } = new List<FORECLOSURE_STATUS>();
    
        [XmlIgnore]
        public bool ForeclosureStatusListSpecified
        {
            get { return this.ForeclosureStatusList != null && this.ForeclosureStatusList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public FORECLOSURE_STATUSES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
