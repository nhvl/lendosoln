namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class FORECLOSURE_STATUS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ForeclosureStatusDateSpecified
                    || this.ForeclosureStatusTypeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ForeclosureStatusDate", Order = 0)]
        public MISMODate ForeclosureStatusDate { get; set; }
    
        [XmlIgnore]
        public bool ForeclosureStatusDateSpecified
        {
            get { return this.ForeclosureStatusDate != null; }
            set { }
        }
    
        [XmlElement("ForeclosureStatusType", Order = 1)]
        public MISMOEnum<ForeclosureStatusBase> ForeclosureStatusType { get; set; }
    
        [XmlIgnore]
        public bool ForeclosureStatusTypeSpecified
        {
            get { return this.ForeclosureStatusType != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public FORECLOSURE_STATUS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
