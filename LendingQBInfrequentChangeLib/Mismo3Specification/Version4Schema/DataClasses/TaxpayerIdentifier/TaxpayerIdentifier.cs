namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class TAXPAYER_IDENTIFIER
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CanadianSocialInsuranceNumberIdentifierSpecified
                    || this.SSNCertificationIndicatorSpecified
                    || this.TaxpayerIdentifierTypeSpecified
                    || this.TaxpayerIdentifierValueSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CanadianSocialInsuranceNumberIdentifier", Order = 0)]
        public MISMOIdentifier CanadianSocialInsuranceNumberIdentifier { get; set; }
    
        [XmlIgnore]
        public bool CanadianSocialInsuranceNumberIdentifierSpecified
        {
            get { return this.CanadianSocialInsuranceNumberIdentifier != null; }
            set { }
        }
    
        [XmlElement("SSNCertificationIndicator", Order = 1)]
        public MISMOIndicator SSNCertificationIndicator { get; set; }
    
        [XmlIgnore]
        public bool SSNCertificationIndicatorSpecified
        {
            get { return this.SSNCertificationIndicator != null; }
            set { }
        }
    
        [XmlElement("TaxpayerIdentifierType", Order = 2)]
        public MISMOEnum<TaxpayerIdentifierBase> TaxpayerIdentifierType { get; set; }
    
        [XmlIgnore]
        public bool TaxpayerIdentifierTypeSpecified
        {
            get { return this.TaxpayerIdentifierType != null; }
            set { }
        }
    
        [XmlElement("TaxpayerIdentifierValue", Order = 3)]
        public MISMONumericString TaxpayerIdentifierValue { get; set; }
    
        [XmlIgnore]
        public bool TaxpayerIdentifierValueSpecified
        {
            get { return this.TaxpayerIdentifierValue != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public TAXPAYER_IDENTIFIER_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
