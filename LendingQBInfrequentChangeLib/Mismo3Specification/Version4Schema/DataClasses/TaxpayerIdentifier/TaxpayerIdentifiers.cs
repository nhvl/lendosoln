namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class TAXPAYER_IDENTIFIERS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.TaxpayerIdentifierListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("TAXPAYER_IDENTIFIER", Order = 0)]
        public List<TAXPAYER_IDENTIFIER> TaxpayerIdentifierList { get; set; } = new List<TAXPAYER_IDENTIFIER>();
    
        [XmlIgnore]
        public bool TaxpayerIdentifierListSpecified
        {
            get { return this.TaxpayerIdentifierList != null && this.TaxpayerIdentifierList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public TAXPAYER_IDENTIFIERS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
