namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class INSPECTION_SERVICE_REQUEST
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.InspectionServiceRequestDetailSpecified
                    || this.PropertiesSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("INSPECTION_SERVICE_REQUEST_DETAIL", Order = 0)]
        public INSPECTION_SERVICE_REQUEST_DETAIL InspectionServiceRequestDetail { get; set; }
    
        [XmlIgnore]
        public bool InspectionServiceRequestDetailSpecified
        {
            get { return this.InspectionServiceRequestDetail != null && this.InspectionServiceRequestDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PROPERTIES", Order = 1)]
        public PROPERTIES Properties { get; set; }
    
        [XmlIgnore]
        public bool PropertiesSpecified
        {
            get { return this.Properties != null && this.Properties.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public INSPECTION_SERVICE_REQUEST_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
