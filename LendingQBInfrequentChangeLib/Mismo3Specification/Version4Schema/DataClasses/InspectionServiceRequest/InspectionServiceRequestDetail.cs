namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class INSPECTION_SERVICE_REQUEST_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.InspectionReportFormatDescriptionSpecified
                    || this.InspectionRequestActionTypeSpecified
                    || this.InspectionRequestActionTypeOtherDescriptionSpecified
                    || this.InspectorFileIdentifierSpecified
                    || this.OrderingSystemNameSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("InspectionReportFormatDescription", Order = 0)]
        public MISMOString InspectionReportFormatDescription { get; set; }
    
        [XmlIgnore]
        public bool InspectionReportFormatDescriptionSpecified
        {
            get { return this.InspectionReportFormatDescription != null; }
            set { }
        }
    
        [XmlElement("InspectionRequestActionType", Order = 1)]
        public MISMOEnum<InspectionRequestActionBase> InspectionRequestActionType { get; set; }
    
        [XmlIgnore]
        public bool InspectionRequestActionTypeSpecified
        {
            get { return this.InspectionRequestActionType != null; }
            set { }
        }
    
        [XmlElement("InspectionRequestActionTypeOtherDescription", Order = 2)]
        public MISMOString InspectionRequestActionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool InspectionRequestActionTypeOtherDescriptionSpecified
        {
            get { return this.InspectionRequestActionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("InspectorFileIdentifier", Order = 3)]
        public MISMOIdentifier InspectorFileIdentifier { get; set; }
    
        [XmlIgnore]
        public bool InspectorFileIdentifierSpecified
        {
            get { return this.InspectorFileIdentifier != null; }
            set { }
        }
    
        [XmlElement("OrderingSystemName", Order = 4)]
        public MISMOString OrderingSystemName { get; set; }
    
        [XmlIgnore]
        public bool OrderingSystemNameSpecified
        {
            get { return this.OrderingSystemName != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public INSPECTION_SERVICE_REQUEST_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
