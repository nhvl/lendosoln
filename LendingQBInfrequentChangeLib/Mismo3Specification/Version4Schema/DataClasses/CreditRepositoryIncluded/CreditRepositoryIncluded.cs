namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CREDIT_REPOSITORY_INCLUDED
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditRepositoryIncludedEquifaxIndicatorSpecified
                    || this.CreditRepositoryIncludedExperianIndicatorSpecified
                    || this.CreditRepositoryIncludedOtherRepositoryNameSpecified
                    || this.CreditRepositoryIncludedTransUnionIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CreditRepositoryIncludedEquifaxIndicator", Order = 0)]
        public MISMOIndicator CreditRepositoryIncludedEquifaxIndicator { get; set; }
    
        [XmlIgnore]
        public bool CreditRepositoryIncludedEquifaxIndicatorSpecified
        {
            get { return this.CreditRepositoryIncludedEquifaxIndicator != null; }
            set { }
        }
    
        [XmlElement("CreditRepositoryIncludedExperianIndicator", Order = 1)]
        public MISMOIndicator CreditRepositoryIncludedExperianIndicator { get; set; }
    
        [XmlIgnore]
        public bool CreditRepositoryIncludedExperianIndicatorSpecified
        {
            get { return this.CreditRepositoryIncludedExperianIndicator != null; }
            set { }
        }
    
        [XmlElement("CreditRepositoryIncludedOtherRepositoryName", Order = 2)]
        public MISMOString CreditRepositoryIncludedOtherRepositoryName { get; set; }
    
        [XmlIgnore]
        public bool CreditRepositoryIncludedOtherRepositoryNameSpecified
        {
            get { return this.CreditRepositoryIncludedOtherRepositoryName != null; }
            set { }
        }
    
        [XmlElement("CreditRepositoryIncludedTransUnionIndicator", Order = 3)]
        public MISMOIndicator CreditRepositoryIncludedTransUnionIndicator { get; set; }
    
        [XmlIgnore]
        public bool CreditRepositoryIncludedTransUnionIndicatorSpecified
        {
            get { return this.CreditRepositoryIncludedTransUnionIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public CREDIT_REPOSITORY_INCLUDED_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
