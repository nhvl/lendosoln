namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class REGULATORY_PRODUCT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.RegulatoryProductDetailSpecified
                    || this.RegulatoryProductMatchesSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("REGULATORY_PRODUCT_DETAIL", Order = 0)]
        public REGULATORY_PRODUCT_DETAIL RegulatoryProductDetail { get; set; }
    
        [XmlIgnore]
        public bool RegulatoryProductDetailSpecified
        {
            get { return this.RegulatoryProductDetail != null && this.RegulatoryProductDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("REGULATORY_PRODUCT_MATCHES", Order = 1)]
        public REGULATORY_PRODUCT_MATCHES RegulatoryProductMatches { get; set; }
    
        [XmlIgnore]
        public bool RegulatoryProductMatchesSpecified
        {
            get { return this.RegulatoryProductMatches != null && this.RegulatoryProductMatches.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public REGULATORY_PRODUCT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
