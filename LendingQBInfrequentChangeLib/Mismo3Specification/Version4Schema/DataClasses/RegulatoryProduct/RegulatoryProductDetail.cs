namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class REGULATORY_PRODUCT_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditRepositorySourceTypeSpecified
                    || this.CreditRepositorySourceTypeOtherDescriptionSpecified
                    || this.RegulatoryProductDisclaimerTextSpecified
                    || this.RegulatoryProductProviderDescriptionSpecified
                    || this.RegulatoryProductResultStatusTypeSpecified
                    || this.RegulatoryProductResultStatusTypeOtherDescriptionSpecified
                    || this.RegulatoryProductResultTextSpecified
                    || this.RegulatoryProductSourceTypeSpecified
                    || this.RegulatoryProductSourceTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CreditRepositorySourceType", Order = 0)]
        public MISMOEnum<CreditRepositorySourceBase> CreditRepositorySourceType { get; set; }
    
        [XmlIgnore]
        public bool CreditRepositorySourceTypeSpecified
        {
            get { return this.CreditRepositorySourceType != null; }
            set { }
        }
    
        [XmlElement("CreditRepositorySourceTypeOtherDescription", Order = 1)]
        public MISMOString CreditRepositorySourceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CreditRepositorySourceTypeOtherDescriptionSpecified
        {
            get { return this.CreditRepositorySourceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("RegulatoryProductDisclaimerText", Order = 2)]
        public MISMOString RegulatoryProductDisclaimerText { get; set; }
    
        [XmlIgnore]
        public bool RegulatoryProductDisclaimerTextSpecified
        {
            get { return this.RegulatoryProductDisclaimerText != null; }
            set { }
        }
    
        [XmlElement("RegulatoryProductProviderDescription", Order = 3)]
        public MISMOString RegulatoryProductProviderDescription { get; set; }
    
        [XmlIgnore]
        public bool RegulatoryProductProviderDescriptionSpecified
        {
            get { return this.RegulatoryProductProviderDescription != null; }
            set { }
        }
    
        [XmlElement("RegulatoryProductResultStatusType", Order = 4)]
        public MISMOEnum<RegulatoryProductResultStatusBase> RegulatoryProductResultStatusType { get; set; }
    
        [XmlIgnore]
        public bool RegulatoryProductResultStatusTypeSpecified
        {
            get { return this.RegulatoryProductResultStatusType != null; }
            set { }
        }
    
        [XmlElement("RegulatoryProductResultStatusTypeOtherDescription", Order = 5)]
        public MISMOString RegulatoryProductResultStatusTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool RegulatoryProductResultStatusTypeOtherDescriptionSpecified
        {
            get { return this.RegulatoryProductResultStatusTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("RegulatoryProductResultText", Order = 6)]
        public MISMOString RegulatoryProductResultText { get; set; }
    
        [XmlIgnore]
        public bool RegulatoryProductResultTextSpecified
        {
            get { return this.RegulatoryProductResultText != null; }
            set { }
        }
    
        [XmlElement("RegulatoryProductSourceType", Order = 7)]
        public MISMOEnum<RegulatoryProductSourceBase> RegulatoryProductSourceType { get; set; }
    
        [XmlIgnore]
        public bool RegulatoryProductSourceTypeSpecified
        {
            get { return this.RegulatoryProductSourceType != null; }
            set { }
        }
    
        [XmlElement("RegulatoryProductSourceTypeOtherDescription", Order = 8)]
        public MISMOString RegulatoryProductSourceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool RegulatoryProductSourceTypeOtherDescriptionSpecified
        {
            get { return this.RegulatoryProductSourceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 9)]
        public REGULATORY_PRODUCT_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
