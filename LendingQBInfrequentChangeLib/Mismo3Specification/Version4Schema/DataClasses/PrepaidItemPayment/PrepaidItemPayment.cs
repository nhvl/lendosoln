namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PREPAID_ITEM_PAYMENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PaymentFinancedIndicatorSpecified
                    || this.PaymentIncludedInAPRIndicatorSpecified
                    || this.PaymentIncludedInJurisdictionHighCostIndicatorSpecified
                    || this.PrepaidFinanceChargeIndicatorSpecified
                    || this.PrepaidItemActualPaymentAmountSpecified
                    || this.PrepaidItemEstimatedPaymentAmountSpecified
                    || this.PrepaidItemPaymentPaidByTypeSpecified
                    || this.PrepaidItemPaymentPaidByTypeOtherDescriptionSpecified
                    || this.PrepaidItemPaymentTimingTypeSpecified
                    || this.PrepaidItemPaymentTimingTypeOtherDescriptionSpecified
                    || this.RegulationZPointsAndFeesIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PaymentFinancedIndicator", Order = 0)]
        public MISMOIndicator PaymentFinancedIndicator { get; set; }
    
        [XmlIgnore]
        public bool PaymentFinancedIndicatorSpecified
        {
            get { return this.PaymentFinancedIndicator != null; }
            set { }
        }
    
        [XmlElement("PaymentIncludedInAPRIndicator", Order = 1)]
        public MISMOIndicator PaymentIncludedInAPRIndicator { get; set; }
    
        [XmlIgnore]
        public bool PaymentIncludedInAPRIndicatorSpecified
        {
            get { return this.PaymentIncludedInAPRIndicator != null; }
            set { }
        }
    
        [XmlElement("PaymentIncludedInJurisdictionHighCostIndicator", Order = 2)]
        public MISMOIndicator PaymentIncludedInJurisdictionHighCostIndicator { get; set; }
    
        [XmlIgnore]
        public bool PaymentIncludedInJurisdictionHighCostIndicatorSpecified
        {
            get { return this.PaymentIncludedInJurisdictionHighCostIndicator != null; }
            set { }
        }
    
        [XmlElement("PrepaidFinanceChargeIndicator", Order = 3)]
        public MISMOIndicator PrepaidFinanceChargeIndicator { get; set; }
    
        [XmlIgnore]
        public bool PrepaidFinanceChargeIndicatorSpecified
        {
            get { return this.PrepaidFinanceChargeIndicator != null; }
            set { }
        }
    
        [XmlElement("PrepaidItemActualPaymentAmount", Order = 4)]
        public MISMOAmount PrepaidItemActualPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool PrepaidItemActualPaymentAmountSpecified
        {
            get { return this.PrepaidItemActualPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("PrepaidItemEstimatedPaymentAmount", Order = 5)]
        public MISMOAmount PrepaidItemEstimatedPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool PrepaidItemEstimatedPaymentAmountSpecified
        {
            get { return this.PrepaidItemEstimatedPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("PrepaidItemPaymentPaidByType", Order = 6)]
        public MISMOEnum<PrepaidItemPaymentPaidByBase> PrepaidItemPaymentPaidByType { get; set; }
    
        [XmlIgnore]
        public bool PrepaidItemPaymentPaidByTypeSpecified
        {
            get { return this.PrepaidItemPaymentPaidByType != null; }
            set { }
        }
    
        [XmlElement("PrepaidItemPaymentPaidByTypeOtherDescription", Order = 7)]
        public MISMOString PrepaidItemPaymentPaidByTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PrepaidItemPaymentPaidByTypeOtherDescriptionSpecified
        {
            get { return this.PrepaidItemPaymentPaidByTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PrepaidItemPaymentTimingType", Order = 8)]
        public MISMOEnum<PrepaidItemPaymentTimingBase> PrepaidItemPaymentTimingType { get; set; }
    
        [XmlIgnore]
        public bool PrepaidItemPaymentTimingTypeSpecified
        {
            get { return this.PrepaidItemPaymentTimingType != null; }
            set { }
        }
    
        [XmlElement("PrepaidItemPaymentTimingTypeOtherDescription", Order = 9)]
        public MISMOString PrepaidItemPaymentTimingTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PrepaidItemPaymentTimingTypeOtherDescriptionSpecified
        {
            get { return this.PrepaidItemPaymentTimingTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("RegulationZPointsAndFeesIndicator", Order = 10)]
        public MISMOIndicator RegulationZPointsAndFeesIndicator { get; set; }
    
        [XmlIgnore]
        public bool RegulationZPointsAndFeesIndicatorSpecified
        {
            get { return this.RegulationZPointsAndFeesIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 11)]
        public PREPAID_ITEM_PAYMENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
