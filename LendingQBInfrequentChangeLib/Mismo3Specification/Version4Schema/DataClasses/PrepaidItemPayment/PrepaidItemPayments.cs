namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class PREPAID_ITEM_PAYMENTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PrepaidItemPaymentListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PREPAID_ITEM_PAYMENT", Order = 0)]
        public List<PREPAID_ITEM_PAYMENT> PrepaidItemPaymentList { get; set; } = new List<PREPAID_ITEM_PAYMENT>();
    
        [XmlIgnore]
        public bool PrepaidItemPaymentListSpecified
        {
            get { return this.PrepaidItemPaymentList != null && this.PrepaidItemPaymentList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public PREPAID_ITEM_PAYMENTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
