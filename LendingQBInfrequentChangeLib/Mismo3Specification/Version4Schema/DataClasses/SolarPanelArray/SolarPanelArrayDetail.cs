namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SOLAR_PANEL_ARRAY_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SolarPanelArrayAgeYearsCountSpecified
                    || this.SolarPanelArrayAzimuthDescriptionSpecified
                    || this.SolarPanelArrayEstimatedKilowattHoursOutputPerYearNumberSpecified
                    || this.SolarPanelArrayEstimatedOutputSourceDescriptionSpecified
                    || this.SolarPanelArrayLocationTypeSpecified
                    || this.SolarPanelArrayLocationTypeOtherDescriptionSpecified
                    || this.SolarPanelArrayManufacturerNameSpecified
                    || this.SolarPanelArrayMaximumKilowattOutputNumberSpecified
                    || this.SolarPanelArrayOwnershipTypeSpecified
                    || this.SolarPanelArrayTiltSlopeDescriptionSpecified
                    || this.SolarPanelArrayWarrantyTermsDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SolarPanelArrayAgeYearsCount", Order = 0)]
        public MISMOCount SolarPanelArrayAgeYearsCount { get; set; }
    
        [XmlIgnore]
        public bool SolarPanelArrayAgeYearsCountSpecified
        {
            get { return this.SolarPanelArrayAgeYearsCount != null; }
            set { }
        }
    
        [XmlElement("SolarPanelArrayAzimuthDescription", Order = 1)]
        public MISMOString SolarPanelArrayAzimuthDescription { get; set; }
    
        [XmlIgnore]
        public bool SolarPanelArrayAzimuthDescriptionSpecified
        {
            get { return this.SolarPanelArrayAzimuthDescription != null; }
            set { }
        }
    
        [XmlElement("SolarPanelArrayEstimatedKilowattHoursOutputPerYearNumber", Order = 2)]
        public MISMONumeric SolarPanelArrayEstimatedKilowattHoursOutputPerYearNumber { get; set; }
    
        [XmlIgnore]
        public bool SolarPanelArrayEstimatedKilowattHoursOutputPerYearNumberSpecified
        {
            get { return this.SolarPanelArrayEstimatedKilowattHoursOutputPerYearNumber != null; }
            set { }
        }
    
        [XmlElement("SolarPanelArrayEstimatedOutputSourceDescription", Order = 3)]
        public MISMOString SolarPanelArrayEstimatedOutputSourceDescription { get; set; }
    
        [XmlIgnore]
        public bool SolarPanelArrayEstimatedOutputSourceDescriptionSpecified
        {
            get { return this.SolarPanelArrayEstimatedOutputSourceDescription != null; }
            set { }
        }
    
        [XmlElement("SolarPanelArrayLocationType", Order = 4)]
        public MISMOEnum<SolarPanelArrayLocationBase> SolarPanelArrayLocationType { get; set; }
    
        [XmlIgnore]
        public bool SolarPanelArrayLocationTypeSpecified
        {
            get { return this.SolarPanelArrayLocationType != null; }
            set { }
        }
    
        [XmlElement("SolarPanelArrayLocationTypeOtherDescription", Order = 5)]
        public MISMOString SolarPanelArrayLocationTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool SolarPanelArrayLocationTypeOtherDescriptionSpecified
        {
            get { return this.SolarPanelArrayLocationTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("SolarPanelArrayManufacturerName", Order = 6)]
        public MISMOString SolarPanelArrayManufacturerName { get; set; }
    
        [XmlIgnore]
        public bool SolarPanelArrayManufacturerNameSpecified
        {
            get { return this.SolarPanelArrayManufacturerName != null; }
            set { }
        }
    
        [XmlElement("SolarPanelArrayMaximumKilowattOutputNumber", Order = 7)]
        public MISMONumeric SolarPanelArrayMaximumKilowattOutputNumber { get; set; }
    
        [XmlIgnore]
        public bool SolarPanelArrayMaximumKilowattOutputNumberSpecified
        {
            get { return this.SolarPanelArrayMaximumKilowattOutputNumber != null; }
            set { }
        }
    
        [XmlElement("SolarPanelArrayOwnershipType", Order = 8)]
        public MISMOEnum<SolarPanelArrayOwnershipBase> SolarPanelArrayOwnershipType { get; set; }
    
        [XmlIgnore]
        public bool SolarPanelArrayOwnershipTypeSpecified
        {
            get { return this.SolarPanelArrayOwnershipType != null; }
            set { }
        }
    
        [XmlElement("SolarPanelArrayTiltSlopeDescription", Order = 9)]
        public MISMOString SolarPanelArrayTiltSlopeDescription { get; set; }
    
        [XmlIgnore]
        public bool SolarPanelArrayTiltSlopeDescriptionSpecified
        {
            get { return this.SolarPanelArrayTiltSlopeDescription != null; }
            set { }
        }
    
        [XmlElement("SolarPanelArrayWarrantyTermsDescription", Order = 10)]
        public MISMOString SolarPanelArrayWarrantyTermsDescription { get; set; }
    
        [XmlIgnore]
        public bool SolarPanelArrayWarrantyTermsDescriptionSpecified
        {
            get { return this.SolarPanelArrayWarrantyTermsDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 11)]
        public SOLAR_PANEL_ARRAY_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
