namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SOLAR_PANEL_ARRAY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.InvertersSpecified
                    || this.SolarPanelArrayDetailSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("INVERTERS", Order = 0)]
        public INVERTERS Inverters { get; set; }
    
        [XmlIgnore]
        public bool InvertersSpecified
        {
            get { return this.Inverters != null && this.Inverters.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SOLAR_PANEL_ARRAY_DETAIL", Order = 1)]
        public SOLAR_PANEL_ARRAY_DETAIL SolarPanelArrayDetail { get; set; }
    
        [XmlIgnore]
        public bool SolarPanelArrayDetailSpecified
        {
            get { return this.SolarPanelArrayDetail != null && this.SolarPanelArrayDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public SOLAR_PANEL_ARRAY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
