namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class SOLAR_PANEL_ARRAYS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SolarPanelArrayListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SOLAR_PANEL_ARRAY", Order = 0)]
        public List<SOLAR_PANEL_ARRAY> SolarPanelArrayList { get; set; } = new List<SOLAR_PANEL_ARRAY>();
    
        [XmlIgnore]
        public bool SolarPanelArrayListSpecified
        {
            get { return this.SolarPanelArrayList != null && this.SolarPanelArrayList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public SOLAR_PANEL_ARRAYS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
