namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class PROVIDED_SERVICES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ProvidedServiceListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PROVIDED_SERVICE", Order = 0)]
        public List<PROVIDED_SERVICE> ProvidedServiceList { get; set; } = new List<PROVIDED_SERVICE>();
    
        [XmlIgnore]
        public bool ProvidedServiceListSpecified
        {
            get { return this.ProvidedServiceList != null && this.ProvidedServiceList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public PROVIDED_SERVICES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
