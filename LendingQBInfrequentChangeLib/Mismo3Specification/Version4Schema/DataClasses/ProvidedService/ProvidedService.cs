namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PROVIDED_SERVICE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.FeeTypeSpecified
                    || this.FeeTypeOtherDescriptionSpecified
                    || this.ProvidedServiceBorrowerCanShopForIndicatorSpecified
                    || this.ProvidedServiceDescriptionSpecified
                    || this.ProvidedServiceEstimatedCostAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("FeeType", Order = 0)]
        public MISMOEnum<FeeBase> FeeType { get; set; }
    
        [XmlIgnore]
        public bool FeeTypeSpecified
        {
            get { return this.FeeType != null; }
            set { }
        }
    
        [XmlElement("FeeTypeOtherDescription", Order = 1)]
        public MISMOString FeeTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool FeeTypeOtherDescriptionSpecified
        {
            get { return this.FeeTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ProvidedServiceBorrowerCanShopForIndicator", Order = 2)]
        public MISMOIndicator ProvidedServiceBorrowerCanShopForIndicator { get; set; }
    
        [XmlIgnore]
        public bool ProvidedServiceBorrowerCanShopForIndicatorSpecified
        {
            get { return this.ProvidedServiceBorrowerCanShopForIndicator != null; }
            set { }
        }
    
        [XmlElement("ProvidedServiceDescription", Order = 3)]
        public MISMOString ProvidedServiceDescription { get; set; }
    
        [XmlIgnore]
        public bool ProvidedServiceDescriptionSpecified
        {
            get { return this.ProvidedServiceDescription != null; }
            set { }
        }
    
        [XmlElement("ProvidedServiceEstimatedCostAmount", Order = 4)]
        public MISMOAmount ProvidedServiceEstimatedCostAmount { get; set; }
    
        [XmlIgnore]
        public bool ProvidedServiceEstimatedCostAmountSpecified
        {
            get { return this.ProvidedServiceEstimatedCostAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public PROVIDED_SERVICE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
