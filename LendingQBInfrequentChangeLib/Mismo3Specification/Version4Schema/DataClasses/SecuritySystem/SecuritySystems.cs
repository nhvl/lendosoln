namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class SECURITY_SYSTEMS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SecuritySystemListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SECURITY_SYSTEM", Order = 0)]
        public List<SECURITY_SYSTEM> SecuritySystemList { get; set; } = new List<SECURITY_SYSTEM>();
    
        [XmlIgnore]
        public bool SecuritySystemListSpecified
        {
            get { return this.SecuritySystemList != null && this.SecuritySystemList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public SECURITY_SYSTEMS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
