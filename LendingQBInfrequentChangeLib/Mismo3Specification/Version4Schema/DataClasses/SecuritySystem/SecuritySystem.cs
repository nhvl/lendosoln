namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SECURITY_SYSTEM
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SecuritySystemTypeSpecified
                    || this.SecuritySystemTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SecuritySystemType", Order = 0)]
        public MISMOEnum<SecuritySystemBase> SecuritySystemType { get; set; }
    
        [XmlIgnore]
        public bool SecuritySystemTypeSpecified
        {
            get { return this.SecuritySystemType != null; }
            set { }
        }
    
        [XmlElement("SecuritySystemTypeOtherDescription", Order = 1)]
        public MISMOString SecuritySystemTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool SecuritySystemTypeOtherDescriptionSpecified
        {
            get { return this.SecuritySystemTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public SECURITY_SYSTEM_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
