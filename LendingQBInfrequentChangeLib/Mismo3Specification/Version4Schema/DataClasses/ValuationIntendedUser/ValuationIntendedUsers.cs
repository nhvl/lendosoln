namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class VALUATION_INTENDED_USERS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ValuationIntendedUserListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("VALUATION_INTENDED_USER", Order = 0)]
        public List<VALUATION_INTENDED_USER> ValuationIntendedUserList { get; set; } = new List<VALUATION_INTENDED_USER>();
    
        [XmlIgnore]
        public bool ValuationIntendedUserListSpecified
        {
            get { return this.ValuationIntendedUserList != null && this.ValuationIntendedUserList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public VALUATION_INTENDED_USERS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
