namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class VALUATION_INTENDED_USER
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ValuationIntendedUserDescriptionSpecified
                    || this.ValuationIntendedUserTypeSpecified
                    || this.ValuationIntendedUserTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ValuationIntendedUserDescription", Order = 0)]
        public MISMOString ValuationIntendedUserDescription { get; set; }
    
        [XmlIgnore]
        public bool ValuationIntendedUserDescriptionSpecified
        {
            get { return this.ValuationIntendedUserDescription != null; }
            set { }
        }
    
        [XmlElement("ValuationIntendedUserType", Order = 1)]
        public MISMOEnum<ValuationIntendedUserBase> ValuationIntendedUserType { get; set; }
    
        [XmlIgnore]
        public bool ValuationIntendedUserTypeSpecified
        {
            get { return this.ValuationIntendedUserType != null; }
            set { }
        }
    
        [XmlElement("ValuationIntendedUserTypeOtherDescription", Order = 2)]
        public MISMOString ValuationIntendedUserTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ValuationIntendedUserTypeOtherDescriptionSpecified
        {
            get { return this.ValuationIntendedUserTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public VALUATION_INTENDED_USER_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
