namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class TAX_RETURN_DOCUMENTATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.TaxReturnDocumentationDetailSpecified
                    || this.TaxReturnDocumentationPeriodsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("TAX_RETURN_DOCUMENTATION_DETAIL", Order = 0)]
        public TAX_RETURN_DOCUMENTATION_DETAIL TaxReturnDocumentationDetail { get; set; }
    
        [XmlIgnore]
        public bool TaxReturnDocumentationDetailSpecified
        {
            get { return this.TaxReturnDocumentationDetail != null && this.TaxReturnDocumentationDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("TAX_RETURN_DOCUMENTATION_PERIODS", Order = 1)]
        public TAX_RETURN_DOCUMENTATION_PERIODS TaxReturnDocumentationPeriods { get; set; }
    
        [XmlIgnore]
        public bool TaxReturnDocumentationPeriodsSpecified
        {
            get { return this.TaxReturnDocumentationPeriods != null && this.TaxReturnDocumentationPeriods.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public TAX_RETURN_DOCUMENTATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
