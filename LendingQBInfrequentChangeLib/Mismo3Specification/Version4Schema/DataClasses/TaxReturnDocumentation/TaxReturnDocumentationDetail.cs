namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class TAX_RETURN_DOCUMENTATION_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AccountTranscriptIndicatorSpecified
                    || this.InformationalFormSeriesTranscriptIndicatorSpecified
                    || this.IRSDocumentTypeSpecified
                    || this.IRSDocumentTypeOtherDescriptionSpecified
                    || this.OtherTranscriptDescriptionSpecified
                    || this.RecordOfAccountIndicatorSpecified
                    || this.ReturnTranscriptIndicatorSpecified
                    || this.VerificationOfNonfilingIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AccountTranscriptIndicator", Order = 0)]
        public MISMOIndicator AccountTranscriptIndicator { get; set; }
    
        [XmlIgnore]
        public bool AccountTranscriptIndicatorSpecified
        {
            get { return this.AccountTranscriptIndicator != null; }
            set { }
        }
    
        [XmlElement("InformationalFormSeriesTranscriptIndicator", Order = 1)]
        public MISMOIndicator InformationalFormSeriesTranscriptIndicator { get; set; }
    
        [XmlIgnore]
        public bool InformationalFormSeriesTranscriptIndicatorSpecified
        {
            get { return this.InformationalFormSeriesTranscriptIndicator != null; }
            set { }
        }
    
        [XmlElement("IRSDocumentType", Order = 2)]
        public MISMOEnum<IRSDocumentBase> IRSDocumentType { get; set; }
    
        [XmlIgnore]
        public bool IRSDocumentTypeSpecified
        {
            get { return this.IRSDocumentType != null; }
            set { }
        }
    
        [XmlElement("IRSDocumentTypeOtherDescription", Order = 3)]
        public MISMOString IRSDocumentTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool IRSDocumentTypeOtherDescriptionSpecified
        {
            get { return this.IRSDocumentTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("OtherTranscriptDescription", Order = 4)]
        public MISMOString OtherTranscriptDescription { get; set; }
    
        [XmlIgnore]
        public bool OtherTranscriptDescriptionSpecified
        {
            get { return this.OtherTranscriptDescription != null; }
            set { }
        }
    
        [XmlElement("RecordOfAccountIndicator", Order = 5)]
        public MISMOIndicator RecordOfAccountIndicator { get; set; }
    
        [XmlIgnore]
        public bool RecordOfAccountIndicatorSpecified
        {
            get { return this.RecordOfAccountIndicator != null; }
            set { }
        }
    
        [XmlElement("ReturnTranscriptIndicator", Order = 6)]
        public MISMOIndicator ReturnTranscriptIndicator { get; set; }
    
        [XmlIgnore]
        public bool ReturnTranscriptIndicatorSpecified
        {
            get { return this.ReturnTranscriptIndicator != null; }
            set { }
        }
    
        [XmlElement("VerificationOfNonfilingIndicator", Order = 7)]
        public MISMOIndicator VerificationOfNonfilingIndicator { get; set; }
    
        [XmlIgnore]
        public bool VerificationOfNonfilingIndicatorSpecified
        {
            get { return this.VerificationOfNonfilingIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 8)]
        public TAX_RETURN_DOCUMENTATION_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
