namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class TAX_RETURN_DOCUMENTATIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.TaxReturnDocumentationListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("TAX_RETURN_DOCUMENTATION", Order = 0)]
        public List<TAX_RETURN_DOCUMENTATION> TaxReturnDocumentationList { get; set; } = new List<TAX_RETURN_DOCUMENTATION>();
    
        [XmlIgnore]
        public bool TaxReturnDocumentationListSpecified
        {
            get { return this.TaxReturnDocumentationList != null && this.TaxReturnDocumentationList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public TAX_RETURN_DOCUMENTATIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
