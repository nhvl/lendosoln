namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SITE_SIZE_LAYOUT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ConditionRatingTypeSpecified
                    || this.LengthFeetNumberSpecified
                    || this.LotSizeAreaNumberSpecified
                    || this.SiteDimensionsDescriptionSpecified
                    || this.SiteShapeDescriptionSpecified
                    || this.SiteSizeShapeTopographyAcceptableIndicatorSpecified
                    || this.SiteSizeShapeTopographyNotAcceptableDescriptionSpecified
                    || this.SiteTopographyDescriptionSpecified
                    || this.UnitOfMeasureTypeSpecified
                    || this.UnitOfMeasureTypeOtherDescriptionSpecified
                    || this.WidthFeetNumberSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ConditionRatingType", Order = 0)]
        public MISMOEnum<ConditionRatingBase> ConditionRatingType { get; set; }
    
        [XmlIgnore]
        public bool ConditionRatingTypeSpecified
        {
            get { return this.ConditionRatingType != null; }
            set { }
        }
    
        [XmlElement("LengthFeetNumber", Order = 1)]
        public MISMONumeric LengthFeetNumber { get; set; }
    
        [XmlIgnore]
        public bool LengthFeetNumberSpecified
        {
            get { return this.LengthFeetNumber != null; }
            set { }
        }
    
        [XmlElement("LotSizeAreaNumber", Order = 2)]
        public MISMONumeric LotSizeAreaNumber { get; set; }
    
        [XmlIgnore]
        public bool LotSizeAreaNumberSpecified
        {
            get { return this.LotSizeAreaNumber != null; }
            set { }
        }
    
        [XmlElement("SiteDimensionsDescription", Order = 3)]
        public MISMOString SiteDimensionsDescription { get; set; }
    
        [XmlIgnore]
        public bool SiteDimensionsDescriptionSpecified
        {
            get { return this.SiteDimensionsDescription != null; }
            set { }
        }
    
        [XmlElement("SiteShapeDescription", Order = 4)]
        public MISMOString SiteShapeDescription { get; set; }
    
        [XmlIgnore]
        public bool SiteShapeDescriptionSpecified
        {
            get { return this.SiteShapeDescription != null; }
            set { }
        }
    
        [XmlElement("SiteSizeShapeTopographyAcceptableIndicator", Order = 5)]
        public MISMOIndicator SiteSizeShapeTopographyAcceptableIndicator { get; set; }
    
        [XmlIgnore]
        public bool SiteSizeShapeTopographyAcceptableIndicatorSpecified
        {
            get { return this.SiteSizeShapeTopographyAcceptableIndicator != null; }
            set { }
        }
    
        [XmlElement("SiteSizeShapeTopographyNotAcceptableDescription", Order = 6)]
        public MISMOString SiteSizeShapeTopographyNotAcceptableDescription { get; set; }
    
        [XmlIgnore]
        public bool SiteSizeShapeTopographyNotAcceptableDescriptionSpecified
        {
            get { return this.SiteSizeShapeTopographyNotAcceptableDescription != null; }
            set { }
        }
    
        [XmlElement("SiteTopographyDescription", Order = 7)]
        public MISMOString SiteTopographyDescription { get; set; }
    
        [XmlIgnore]
        public bool SiteTopographyDescriptionSpecified
        {
            get { return this.SiteTopographyDescription != null; }
            set { }
        }
    
        [XmlElement("UnitOfMeasureType", Order = 8)]
        public MISMOEnum<UnitOfMeasureBase> UnitOfMeasureType { get; set; }
    
        [XmlIgnore]
        public bool UnitOfMeasureTypeSpecified
        {
            get { return this.UnitOfMeasureType != null; }
            set { }
        }
    
        [XmlElement("UnitOfMeasureTypeOtherDescription", Order = 9)]
        public MISMOString UnitOfMeasureTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool UnitOfMeasureTypeOtherDescriptionSpecified
        {
            get { return this.UnitOfMeasureTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("WidthFeetNumber", Order = 10)]
        public MISMONumeric WidthFeetNumber { get; set; }
    
        [XmlIgnore]
        public bool WidthFeetNumberSpecified
        {
            get { return this.WidthFeetNumber != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 11)]
        public SITE_SIZE_LAYOUT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
