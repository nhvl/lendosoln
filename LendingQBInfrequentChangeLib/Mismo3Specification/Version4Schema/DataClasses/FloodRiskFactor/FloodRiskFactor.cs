namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class FLOOD_RISK_FACTOR
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BaseFloodElevationFeetNumberSpecified
                    || this.FloodDepthFeetNumberSpecified
                    || this.PropertyElevationFeetNumberSpecified
                    || this.SpecialFloodHazardAreaDistanceFeetNumberSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BaseFloodElevationFeetNumber", Order = 0)]
        public MISMONumeric BaseFloodElevationFeetNumber { get; set; }
    
        [XmlIgnore]
        public bool BaseFloodElevationFeetNumberSpecified
        {
            get { return this.BaseFloodElevationFeetNumber != null; }
            set { }
        }
    
        [XmlElement("FloodDepthFeetNumber", Order = 1)]
        public MISMONumeric FloodDepthFeetNumber { get; set; }
    
        [XmlIgnore]
        public bool FloodDepthFeetNumberSpecified
        {
            get { return this.FloodDepthFeetNumber != null; }
            set { }
        }
    
        [XmlElement("PropertyElevationFeetNumber", Order = 2)]
        public MISMONumeric PropertyElevationFeetNumber { get; set; }
    
        [XmlIgnore]
        public bool PropertyElevationFeetNumberSpecified
        {
            get { return this.PropertyElevationFeetNumber != null; }
            set { }
        }
    
        [XmlElement("SpecialFloodHazardAreaDistanceFeetNumber", Order = 3)]
        public MISMONumeric SpecialFloodHazardAreaDistanceFeetNumber { get; set; }
    
        [XmlIgnore]
        public bool SpecialFloodHazardAreaDistanceFeetNumberSpecified
        {
            get { return this.SpecialFloodHazardAreaDistanceFeetNumber != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public FLOOD_RISK_FACTOR_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
