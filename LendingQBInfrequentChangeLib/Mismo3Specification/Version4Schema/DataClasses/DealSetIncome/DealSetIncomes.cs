namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class DEAL_SET_INCOMES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DealSetIncomeListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DEAL_SET_INCOME", Order = 0)]
        public List<DEAL_SET_INCOME> DealSetIncomeList { get; set; } = new List<DEAL_SET_INCOME>();
    
        [XmlIgnore]
        public bool DealSetIncomeListSpecified
        {
            get { return this.DealSetIncomeList != null && this.DealSetIncomeList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public DEAL_SET_INCOMES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
