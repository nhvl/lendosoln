namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class DEAL_SET_INCOME
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DealSetIncomeAmountSpecified
                    || this.DealSetIncomeTypeSpecified
                    || this.DealSetIncomeTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DealSetIncomeAmount", Order = 0)]
        public MISMOAmount DealSetIncomeAmount { get; set; }
    
        [XmlIgnore]
        public bool DealSetIncomeAmountSpecified
        {
            get { return this.DealSetIncomeAmount != null; }
            set { }
        }
    
        [XmlElement("DealSetIncomeType", Order = 1)]
        public MISMOEnum<DealSetIncomeBase> DealSetIncomeType { get; set; }
    
        [XmlIgnore]
        public bool DealSetIncomeTypeSpecified
        {
            get { return this.DealSetIncomeType != null; }
            set { }
        }
    
        [XmlElement("DealSetIncomeTypeOtherDescription", Order = 2)]
        public MISMOString DealSetIncomeTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool DealSetIncomeTypeOtherDescriptionSpecified
        {
            get { return this.DealSetIncomeTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public DEAL_SET_INCOME_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
