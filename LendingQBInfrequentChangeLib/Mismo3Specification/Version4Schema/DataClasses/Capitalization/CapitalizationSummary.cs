namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CAPITALIZATION_SUMMARY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ContributionToReduceCapitalizedAdvancesAmountSpecified
                    || this.ContributionToReduceCapitalizedDelinquentInterestAmountSpecified
                    || this.TotalCapitalizedAmountSpecified
                    || this.TotalCapitalizedFeesAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ContributionToReduceCapitalizedAdvancesAmount", Order = 0)]
        public MISMOAmount ContributionToReduceCapitalizedAdvancesAmount { get; set; }
    
        [XmlIgnore]
        public bool ContributionToReduceCapitalizedAdvancesAmountSpecified
        {
            get { return this.ContributionToReduceCapitalizedAdvancesAmount != null; }
            set { }
        }
    
        [XmlElement("ContributionToReduceCapitalizedDelinquentInterestAmount", Order = 1)]
        public MISMOAmount ContributionToReduceCapitalizedDelinquentInterestAmount { get; set; }
    
        [XmlIgnore]
        public bool ContributionToReduceCapitalizedDelinquentInterestAmountSpecified
        {
            get { return this.ContributionToReduceCapitalizedDelinquentInterestAmount != null; }
            set { }
        }
    
        [XmlElement("TotalCapitalizedAmount", Order = 2)]
        public MISMOAmount TotalCapitalizedAmount { get; set; }
    
        [XmlIgnore]
        public bool TotalCapitalizedAmountSpecified
        {
            get { return this.TotalCapitalizedAmount != null; }
            set { }
        }
    
        [XmlElement("TotalCapitalizedFeesAmount", Order = 3)]
        public MISMOAmount TotalCapitalizedFeesAmount { get; set; }
    
        [XmlIgnore]
        public bool TotalCapitalizedFeesAmountSpecified
        {
            get { return this.TotalCapitalizedFeesAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public CAPITALIZATION_SUMMARY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
