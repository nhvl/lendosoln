namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CAPITALIZATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CapitalizationAmountSpecified
                    || this.CapitalizationChargeTypeSpecified
                    || this.CapitalizationChargeTypeOtherDescriptionSpecified
                    || this.CapitalizationCommentTextSpecified
                    || this.CapitalizationDateSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CapitalizationAmount", Order = 0)]
        public MISMOAmount CapitalizationAmount { get; set; }
    
        [XmlIgnore]
        public bool CapitalizationAmountSpecified
        {
            get { return this.CapitalizationAmount != null; }
            set { }
        }
    
        [XmlElement("CapitalizationChargeType", Order = 1)]
        public MISMOEnum<CapitalizationChargeBase> CapitalizationChargeType { get; set; }
    
        [XmlIgnore]
        public bool CapitalizationChargeTypeSpecified
        {
            get { return this.CapitalizationChargeType != null; }
            set { }
        }
    
        [XmlElement("CapitalizationChargeTypeOtherDescription", Order = 2)]
        public MISMOString CapitalizationChargeTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CapitalizationChargeTypeOtherDescriptionSpecified
        {
            get { return this.CapitalizationChargeTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("CapitalizationCommentText", Order = 3)]
        public MISMOString CapitalizationCommentText { get; set; }
    
        [XmlIgnore]
        public bool CapitalizationCommentTextSpecified
        {
            get { return this.CapitalizationCommentText != null; }
            set { }
        }
    
        [XmlElement("CapitalizationDate", Order = 4)]
        public MISMODate CapitalizationDate { get; set; }
    
        [XmlIgnore]
        public bool CapitalizationDateSpecified
        {
            get { return this.CapitalizationDate != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public CAPITALIZATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
