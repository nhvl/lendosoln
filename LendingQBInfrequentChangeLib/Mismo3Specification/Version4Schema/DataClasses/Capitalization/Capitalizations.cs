namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class CAPITALIZATIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CapitalizationListSpecified
                    || this.CapitalizationSummarySpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CAPITALIZATION", Order = 0)]
        public List<CAPITALIZATION> CapitalizationList { get; set; } = new List<CAPITALIZATION>();
    
        [XmlIgnore]
        public bool CapitalizationListSpecified
        {
            get { return this.CapitalizationList != null && this.CapitalizationList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("CAPITALIZATION_SUMMARY", Order = 1)]
        public CAPITALIZATION_SUMMARY CapitalizationSummary { get; set; }
    
        [XmlIgnore]
        public bool CapitalizationSummarySpecified
        {
            get { return this.CapitalizationSummary != null && this.CapitalizationSummary.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public CAPITALIZATIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
