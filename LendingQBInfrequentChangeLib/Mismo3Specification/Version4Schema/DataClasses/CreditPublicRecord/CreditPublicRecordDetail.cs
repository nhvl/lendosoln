namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CREDIT_PUBLIC_RECORD_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditPublicRecordAccountOwnershipTypeSpecified
                    || this.CreditPublicRecordAttorneyNameSpecified
                    || this.CreditPublicRecordConsumerDisputeIndicatorSpecified
                    || this.CreditPublicRecordCourtNameSpecified
                    || this.CreditPublicRecordDefendantNameSpecified
                    || this.CreditPublicRecordDerogatoryDataIndicatorSpecified
                    || this.CreditPublicRecordDispositionDateSpecified
                    || this.CreditPublicRecordDispositionTypeSpecified
                    || this.CreditPublicRecordDispositionTypeOtherDescriptionSpecified
                    || this.CreditPublicRecordDocketIdentifierSpecified
                    || this.CreditPublicRecordFiledDateSpecified
                    || this.CreditPublicRecordLegalObligationAmountSpecified
                    || this.CreditPublicRecordManualUpdateIndicatorSpecified
                    || this.CreditPublicRecordPaidDateSpecified
                    || this.CreditPublicRecordPaymentFrequencyTypeSpecified
                    || this.CreditPublicRecordPaymentFrequencyTypeOtherDescriptionSpecified
                    || this.CreditPublicRecordPlaintiffNameSpecified
                    || this.CreditPublicRecordReportedDateSpecified
                    || this.CreditPublicRecordSettledDateSpecified
                    || this.CreditPublicRecordTypeSpecified
                    || this.CreditPublicRecordTypeOtherDescriptionSpecified
                    || this.DuplicateGroupIdentifierSpecified
                    || this.PrimaryRecordIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CreditPublicRecordAccountOwnershipType", Order = 0)]
        public MISMOEnum<CreditPublicRecordAccountOwnershipBase> CreditPublicRecordAccountOwnershipType { get; set; }
    
        [XmlIgnore]
        public bool CreditPublicRecordAccountOwnershipTypeSpecified
        {
            get { return this.CreditPublicRecordAccountOwnershipType != null; }
            set { }
        }
    
        [XmlElement("CreditPublicRecordAttorneyName", Order = 1)]
        public MISMOString CreditPublicRecordAttorneyName { get; set; }
    
        [XmlIgnore]
        public bool CreditPublicRecordAttorneyNameSpecified
        {
            get { return this.CreditPublicRecordAttorneyName != null; }
            set { }
        }
    
        [XmlElement("CreditPublicRecordConsumerDisputeIndicator", Order = 2)]
        public MISMOIndicator CreditPublicRecordConsumerDisputeIndicator { get; set; }
    
        [XmlIgnore]
        public bool CreditPublicRecordConsumerDisputeIndicatorSpecified
        {
            get { return this.CreditPublicRecordConsumerDisputeIndicator != null; }
            set { }
        }
    
        [XmlElement("CreditPublicRecordCourtName", Order = 3)]
        public MISMOString CreditPublicRecordCourtName { get; set; }
    
        [XmlIgnore]
        public bool CreditPublicRecordCourtNameSpecified
        {
            get { return this.CreditPublicRecordCourtName != null; }
            set { }
        }
    
        [XmlElement("CreditPublicRecordDefendantName", Order = 4)]
        public MISMOString CreditPublicRecordDefendantName { get; set; }
    
        [XmlIgnore]
        public bool CreditPublicRecordDefendantNameSpecified
        {
            get { return this.CreditPublicRecordDefendantName != null; }
            set { }
        }
    
        [XmlElement("CreditPublicRecordDerogatoryDataIndicator", Order = 5)]
        public MISMOIndicator CreditPublicRecordDerogatoryDataIndicator { get; set; }
    
        [XmlIgnore]
        public bool CreditPublicRecordDerogatoryDataIndicatorSpecified
        {
            get { return this.CreditPublicRecordDerogatoryDataIndicator != null; }
            set { }
        }
    
        [XmlElement("CreditPublicRecordDispositionDate", Order = 6)]
        public MISMODate CreditPublicRecordDispositionDate { get; set; }
    
        [XmlIgnore]
        public bool CreditPublicRecordDispositionDateSpecified
        {
            get { return this.CreditPublicRecordDispositionDate != null; }
            set { }
        }
    
        [XmlElement("CreditPublicRecordDispositionType", Order = 7)]
        public MISMOEnum<CreditPublicRecordDispositionBase> CreditPublicRecordDispositionType { get; set; }
    
        [XmlIgnore]
        public bool CreditPublicRecordDispositionTypeSpecified
        {
            get { return this.CreditPublicRecordDispositionType != null; }
            set { }
        }
    
        [XmlElement("CreditPublicRecordDispositionTypeOtherDescription", Order = 8)]
        public MISMOString CreditPublicRecordDispositionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CreditPublicRecordDispositionTypeOtherDescriptionSpecified
        {
            get { return this.CreditPublicRecordDispositionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("CreditPublicRecordDocketIdentifier", Order = 9)]
        public MISMOIdentifier CreditPublicRecordDocketIdentifier { get; set; }
    
        [XmlIgnore]
        public bool CreditPublicRecordDocketIdentifierSpecified
        {
            get { return this.CreditPublicRecordDocketIdentifier != null; }
            set { }
        }
    
        [XmlElement("CreditPublicRecordFiledDate", Order = 10)]
        public MISMODate CreditPublicRecordFiledDate { get; set; }
    
        [XmlIgnore]
        public bool CreditPublicRecordFiledDateSpecified
        {
            get { return this.CreditPublicRecordFiledDate != null; }
            set { }
        }
    
        [XmlElement("CreditPublicRecordLegalObligationAmount", Order = 11)]
        public MISMOAmount CreditPublicRecordLegalObligationAmount { get; set; }
    
        [XmlIgnore]
        public bool CreditPublicRecordLegalObligationAmountSpecified
        {
            get { return this.CreditPublicRecordLegalObligationAmount != null; }
            set { }
        }
    
        [XmlElement("CreditPublicRecordManualUpdateIndicator", Order = 12)]
        public MISMOIndicator CreditPublicRecordManualUpdateIndicator { get; set; }
    
        [XmlIgnore]
        public bool CreditPublicRecordManualUpdateIndicatorSpecified
        {
            get { return this.CreditPublicRecordManualUpdateIndicator != null; }
            set { }
        }
    
        [XmlElement("CreditPublicRecordPaidDate", Order = 13)]
        public MISMODate CreditPublicRecordPaidDate { get; set; }
    
        [XmlIgnore]
        public bool CreditPublicRecordPaidDateSpecified
        {
            get { return this.CreditPublicRecordPaidDate != null; }
            set { }
        }
    
        [XmlElement("CreditPublicRecordPaymentFrequencyType", Order = 14)]
        public MISMOEnum<CreditPublicRecordPaymentFrequencyBase> CreditPublicRecordPaymentFrequencyType { get; set; }
    
        [XmlIgnore]
        public bool CreditPublicRecordPaymentFrequencyTypeSpecified
        {
            get { return this.CreditPublicRecordPaymentFrequencyType != null; }
            set { }
        }
    
        [XmlElement("CreditPublicRecordPaymentFrequencyTypeOtherDescription", Order = 15)]
        public MISMOString CreditPublicRecordPaymentFrequencyTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CreditPublicRecordPaymentFrequencyTypeOtherDescriptionSpecified
        {
            get { return this.CreditPublicRecordPaymentFrequencyTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("CreditPublicRecordPlaintiffName", Order = 16)]
        public MISMOString CreditPublicRecordPlaintiffName { get; set; }
    
        [XmlIgnore]
        public bool CreditPublicRecordPlaintiffNameSpecified
        {
            get { return this.CreditPublicRecordPlaintiffName != null; }
            set { }
        }
    
        [XmlElement("CreditPublicRecordReportedDate", Order = 17)]
        public MISMODate CreditPublicRecordReportedDate { get; set; }
    
        [XmlIgnore]
        public bool CreditPublicRecordReportedDateSpecified
        {
            get { return this.CreditPublicRecordReportedDate != null; }
            set { }
        }
    
        [XmlElement("CreditPublicRecordSettledDate", Order = 18)]
        public MISMODate CreditPublicRecordSettledDate { get; set; }
    
        [XmlIgnore]
        public bool CreditPublicRecordSettledDateSpecified
        {
            get { return this.CreditPublicRecordSettledDate != null; }
            set { }
        }
    
        [XmlElement("CreditPublicRecordType", Order = 19)]
        public MISMOEnum<CreditPublicRecordBase> CreditPublicRecordType { get; set; }
    
        [XmlIgnore]
        public bool CreditPublicRecordTypeSpecified
        {
            get { return this.CreditPublicRecordType != null; }
            set { }
        }
    
        [XmlElement("CreditPublicRecordTypeOtherDescription", Order = 20)]
        public MISMOString CreditPublicRecordTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CreditPublicRecordTypeOtherDescriptionSpecified
        {
            get { return this.CreditPublicRecordTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("DuplicateGroupIdentifier", Order = 21)]
        public MISMOIdentifier DuplicateGroupIdentifier { get; set; }
    
        [XmlIgnore]
        public bool DuplicateGroupIdentifierSpecified
        {
            get { return this.DuplicateGroupIdentifier != null; }
            set { }
        }
    
        [XmlElement("PrimaryRecordIndicator", Order = 22)]
        public MISMOIndicator PrimaryRecordIndicator { get; set; }
    
        [XmlIgnore]
        public bool PrimaryRecordIndicatorSpecified
        {
            get { return this.PrimaryRecordIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 23)]
        public CREDIT_PUBLIC_RECORD_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
