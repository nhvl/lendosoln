namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CREDIT_PUBLIC_RECORD
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BankruptcySpecified
                    || this.ContactsSpecified
                    || this.CreditCommentsSpecified
                    || this.CreditPublicRecordDetailSpecified
                    || this.CreditRepositoriesSpecified
                    || this.VerificationSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BANKRUPTCY", Order = 0)]
        public BANKRUPTCY Bankruptcy { get; set; }
    
        [XmlIgnore]
        public bool BankruptcySpecified
        {
            get { return this.Bankruptcy != null && this.Bankruptcy.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CONTACTS", Order = 1)]
        public CONTACTS Contacts { get; set; }
    
        [XmlIgnore]
        public bool ContactsSpecified
        {
            get { return this.Contacts != null && this.Contacts.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_COMMENTS", Order = 2)]
        public CREDIT_COMMENTS CreditComments { get; set; }
    
        [XmlIgnore]
        public bool CreditCommentsSpecified
        {
            get { return this.CreditComments != null && this.CreditComments.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_PUBLIC_RECORD_DETAIL", Order = 3)]
        public CREDIT_PUBLIC_RECORD_DETAIL CreditPublicRecordDetail { get; set; }
    
        [XmlIgnore]
        public bool CreditPublicRecordDetailSpecified
        {
            get { return this.CreditPublicRecordDetail != null && this.CreditPublicRecordDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_REPOSITORIES", Order = 4)]
        public CREDIT_REPOSITORIES CreditRepositories { get; set; }
    
        [XmlIgnore]
        public bool CreditRepositoriesSpecified
        {
            get { return this.CreditRepositories != null && this.CreditRepositories.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("VERIFICATION", Order = 5)]
        public VERIFICATION Verification { get; set; }
    
        [XmlIgnore]
        public bool VerificationSpecified
        {
            get { return this.Verification != null && this.Verification.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 6)]
        public CREDIT_PUBLIC_RECORD_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
