namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class CREDIT_PUBLIC_RECORDS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditPublicRecordListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CREDIT_PUBLIC_RECORD", Order = 0)]
        public List<CREDIT_PUBLIC_RECORD> CreditPublicRecordList { get; set; } = new List<CREDIT_PUBLIC_RECORD>();
    
        [XmlIgnore]
        public bool CreditPublicRecordListSpecified
        {
            get { return this.CreditPublicRecordList != null && this.CreditPublicRecordList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public CREDIT_PUBLIC_RECORDS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
