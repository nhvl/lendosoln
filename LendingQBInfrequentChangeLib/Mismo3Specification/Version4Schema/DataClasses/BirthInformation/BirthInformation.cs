namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class BIRTH_INFORMATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BirthDateSpecified
                    || this.CityTownVillageOfBirthNameSpecified
                    || this.CountryOfBirthNameSpecified
                    || this.MothersMaidenNameSpecified
                    || this.StateProvinceOfBirthNameSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BirthDate", Order = 0)]
        public MISMODate BirthDate { get; set; }
    
        [XmlIgnore]
        public bool BirthDateSpecified
        {
            get { return this.BirthDate != null; }
            set { }
        }
    
        [XmlElement("CityTownVillageOfBirthName", Order = 1)]
        public MISMOString CityTownVillageOfBirthName { get; set; }
    
        [XmlIgnore]
        public bool CityTownVillageOfBirthNameSpecified
        {
            get { return this.CityTownVillageOfBirthName != null; }
            set { }
        }
    
        [XmlElement("CountryOfBirthName", Order = 2)]
        public MISMOString CountryOfBirthName { get; set; }
    
        [XmlIgnore]
        public bool CountryOfBirthNameSpecified
        {
            get { return this.CountryOfBirthName != null; }
            set { }
        }
    
        [XmlElement("MothersMaidenName", Order = 3)]
        public MISMOString MothersMaidenName { get; set; }
    
        [XmlIgnore]
        public bool MothersMaidenNameSpecified
        {
            get { return this.MothersMaidenName != null; }
            set { }
        }
    
        [XmlElement("StateProvinceOfBirthName", Order = 4)]
        public MISMOString StateProvinceOfBirthName { get; set; }
    
        [XmlIgnore]
        public bool StateProvinceOfBirthNameSpecified
        {
            get { return this.StateProvinceOfBirthName != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public BIRTH_INFORMATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
