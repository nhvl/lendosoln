namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class BUYDOWN_SCHEDULES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BuydownScheduleListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BUYDOWN_SCHEDULE", Order = 0)]
        public List<BUYDOWN_SCHEDULE> BuydownScheduleList { get; set; } = new List<BUYDOWN_SCHEDULE>();
    
        [XmlIgnore]
        public bool BuydownScheduleListSpecified
        {
            get { return this.BuydownScheduleList != null && this.BuydownScheduleList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public BUYDOWN_SCHEDULES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
