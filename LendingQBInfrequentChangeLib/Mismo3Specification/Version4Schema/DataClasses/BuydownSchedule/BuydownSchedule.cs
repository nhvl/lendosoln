namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class BUYDOWN_SCHEDULE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BuydownScheduleAdjustmentPercentSpecified
                    || this.BuydownSchedulePeriodicPaymentAmountSpecified
                    || this.BuydownSchedulePeriodicPaymentEffectiveDateSpecified
                    || this.BuydownSchedulePeriodicPaymentsCountSpecified
                    || this.BuydownSchedulePeriodIdentifierSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BuydownScheduleAdjustmentPercent", Order = 0)]
        public MISMOPercent BuydownScheduleAdjustmentPercent { get; set; }
    
        [XmlIgnore]
        public bool BuydownScheduleAdjustmentPercentSpecified
        {
            get { return this.BuydownScheduleAdjustmentPercent != null; }
            set { }
        }
    
        [XmlElement("BuydownSchedulePeriodicPaymentAmount", Order = 1)]
        public MISMOAmount BuydownSchedulePeriodicPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool BuydownSchedulePeriodicPaymentAmountSpecified
        {
            get { return this.BuydownSchedulePeriodicPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("BuydownSchedulePeriodicPaymentEffectiveDate", Order = 2)]
        public MISMODate BuydownSchedulePeriodicPaymentEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool BuydownSchedulePeriodicPaymentEffectiveDateSpecified
        {
            get { return this.BuydownSchedulePeriodicPaymentEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("BuydownSchedulePeriodicPaymentsCount", Order = 3)]
        public MISMOCount BuydownSchedulePeriodicPaymentsCount { get; set; }
    
        [XmlIgnore]
        public bool BuydownSchedulePeriodicPaymentsCountSpecified
        {
            get { return this.BuydownSchedulePeriodicPaymentsCount != null; }
            set { }
        }
    
        [XmlElement("BuydownSchedulePeriodIdentifier", Order = 4)]
        public MISMOIdentifier BuydownSchedulePeriodIdentifier { get; set; }
    
        [XmlIgnore]
        public bool BuydownSchedulePeriodIdentifierSpecified
        {
            get { return this.BuydownSchedulePeriodIdentifier != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public BUYDOWN_SCHEDULE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
