namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PREPAYMENT_PENALTY_LIFETIME_RULE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PrepaymentFinanceChargeRefundableIndicatorSpecified
                    || this.PrepaymentPenaltyDisclosureDateSpecified
                    || this.PrepaymentPenaltyExpirationDateSpecified
                    || this.PrepaymentPenaltyExpirationMonthsCountSpecified
                    || this.PrepaymentPenaltyMaximumLifeOfLoanAmountSpecified
                    || this.PrepaymentPenaltyTextDescriptionSpecified
                    || this.PrepaymentPenaltyWaiverTypeSpecified
                    || this.PrepaymentPenaltyWaiverTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PrepaymentFinanceChargeRefundableIndicator", Order = 0)]
        public MISMOIndicator PrepaymentFinanceChargeRefundableIndicator { get; set; }
    
        [XmlIgnore]
        public bool PrepaymentFinanceChargeRefundableIndicatorSpecified
        {
            get { return this.PrepaymentFinanceChargeRefundableIndicator != null; }
            set { }
        }
    
        [XmlElement("PrepaymentPenaltyDisclosureDate", Order = 1)]
        public MISMODate PrepaymentPenaltyDisclosureDate { get; set; }
    
        [XmlIgnore]
        public bool PrepaymentPenaltyDisclosureDateSpecified
        {
            get { return this.PrepaymentPenaltyDisclosureDate != null; }
            set { }
        }
    
        [XmlElement("PrepaymentPenaltyExpirationDate", Order = 2)]
        public MISMODate PrepaymentPenaltyExpirationDate { get; set; }
    
        [XmlIgnore]
        public bool PrepaymentPenaltyExpirationDateSpecified
        {
            get { return this.PrepaymentPenaltyExpirationDate != null; }
            set { }
        }
    
        [XmlElement("PrepaymentPenaltyExpirationMonthsCount", Order = 3)]
        public MISMOCount PrepaymentPenaltyExpirationMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool PrepaymentPenaltyExpirationMonthsCountSpecified
        {
            get { return this.PrepaymentPenaltyExpirationMonthsCount != null; }
            set { }
        }
    
        [XmlElement("PrepaymentPenaltyMaximumLifeOfLoanAmount", Order = 4)]
        public MISMOAmount PrepaymentPenaltyMaximumLifeOfLoanAmount { get; set; }
    
        [XmlIgnore]
        public bool PrepaymentPenaltyMaximumLifeOfLoanAmountSpecified
        {
            get { return this.PrepaymentPenaltyMaximumLifeOfLoanAmount != null; }
            set { }
        }
    
        [XmlElement("PrepaymentPenaltyTextDescription", Order = 5)]
        public MISMOString PrepaymentPenaltyTextDescription { get; set; }
    
        [XmlIgnore]
        public bool PrepaymentPenaltyTextDescriptionSpecified
        {
            get { return this.PrepaymentPenaltyTextDescription != null; }
            set { }
        }
    
        [XmlElement("PrepaymentPenaltyWaiverType", Order = 6)]
        public MISMOEnum<PrepaymentPenaltyWaiverBase> PrepaymentPenaltyWaiverType { get; set; }
    
        [XmlIgnore]
        public bool PrepaymentPenaltyWaiverTypeSpecified
        {
            get { return this.PrepaymentPenaltyWaiverType != null; }
            set { }
        }
    
        [XmlElement("PrepaymentPenaltyWaiverTypeOtherDescription", Order = 7)]
        public MISMOString PrepaymentPenaltyWaiverTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PrepaymentPenaltyWaiverTypeOtherDescriptionSpecified
        {
            get { return this.PrepaymentPenaltyWaiverTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 8)]
        public PREPAYMENT_PENALTY_LIFETIME_RULE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
