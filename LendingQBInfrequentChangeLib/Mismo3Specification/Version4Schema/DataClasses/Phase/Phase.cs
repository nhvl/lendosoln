namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PHASE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PhaseCarStoragesSpecified
                    || this.PhaseCommonElementsSpecified
                    || this.PhaseConversionSpecified
                    || this.PhaseDetailSpecified
                    || this.PhaseHousingUnitInventoriesSpecified
                    || this.PhaseStructuresSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PHASE_CAR_STORAGES", Order = 0)]
        public CAR_STORAGES PhaseCarStorages { get; set; }
    
        [XmlIgnore]
        public bool PhaseCarStoragesSpecified
        {
            get { return this.PhaseCarStorages != null; }
            set { }
        }
    
        [XmlElement("PHASE_COMMON_ELEMENTS", Order = 1)]
        public COMMON_ELEMENTS PhaseCommonElements { get; set; }
    
        [XmlIgnore]
        public bool PhaseCommonElementsSpecified
        {
            get { return this.PhaseCommonElements != null; }
            set { }
        }
    
        [XmlElement("PHASE_CONVERSION", Order = 2)]
        public CONVERSION PhaseConversion { get; set; }
    
        [XmlIgnore]
        public bool PhaseConversionSpecified
        {
            get { return this.PhaseConversion != null; }
            set { }
        }
    
        [XmlElement("PHASE_DETAIL", Order = 3)]
        public PHASE_DETAIL PhaseDetail { get; set; }
    
        [XmlIgnore]
        public bool PhaseDetailSpecified
        {
            get { return this.PhaseDetail != null && this.PhaseDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PHASE_HOUSING_UNIT_INVENTORIES", Order = 4)]
        public HOUSING_UNIT_INVENTORIES PhaseHousingUnitInventories { get; set; }
    
        [XmlIgnore]
        public bool PhaseHousingUnitInventoriesSpecified
        {
            get { return this.PhaseHousingUnitInventories != null; }
            set { }
        }
    
        [XmlElement("PHASE_STRUCTURES", Order = 5)]
        public PHASE_STRUCTURES PhaseStructures { get; set; }
    
        [XmlIgnore]
        public bool PhaseStructuresSpecified
        {
            get { return this.PhaseStructures != null && this.PhaseStructures.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 6)]
        public PHASE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
