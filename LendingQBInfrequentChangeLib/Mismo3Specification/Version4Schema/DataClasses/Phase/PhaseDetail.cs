namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PHASE_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ProjectPhaseIdentifierSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ProjectPhaseIdentifier", Order = 0)]
        public MISMOIdentifier ProjectPhaseIdentifier { get; set; }
    
        [XmlIgnore]
        public bool ProjectPhaseIdentifierSpecified
        {
            get { return this.ProjectPhaseIdentifier != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public PHASE_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
