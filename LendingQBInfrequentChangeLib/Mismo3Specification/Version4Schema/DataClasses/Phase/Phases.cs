namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class PHASES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PhaseListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PHASE", Order = 0)]
        public List<PHASE> PhaseList { get; set; } = new List<PHASE>();
    
        [XmlIgnore]
        public bool PhaseListSpecified
        {
            get { return this.PhaseList != null && this.PhaseList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public PHASES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
