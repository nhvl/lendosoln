namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class VALUATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ValuationRequestSpecified
                    || this.ValuationResponseSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("VALUATION_REQUEST", Order = 0)]
        public VALUATION_REQUEST ValuationRequest { get; set; }
    
        [XmlIgnore]
        public bool ValuationRequestSpecified
        {
            get { return this.ValuationRequest != null && this.ValuationRequest.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("VALUATION_RESPONSE", Order = 1)]
        public VALUATION_RESPONSE ValuationResponse { get; set; }
    
        [XmlIgnore]
        public bool ValuationResponseSpecified
        {
            get { return this.ValuationResponse != null && this.ValuationResponse.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public VALUATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
