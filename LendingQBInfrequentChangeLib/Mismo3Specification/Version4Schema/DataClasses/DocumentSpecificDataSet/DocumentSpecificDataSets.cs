namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class DOCUMENT_SPECIFIC_DATA_SETS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DocumentSpecificDataSetListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DOCUMENT_SPECIFIC_DATA_SET", Order = 0)]
        public List<DOCUMENT_SPECIFIC_DATA_SET> DocumentSpecificDataSetList { get; set; } = new List<DOCUMENT_SPECIFIC_DATA_SET>();
    
        [XmlIgnore]
        public bool DocumentSpecificDataSetListSpecified
        {
            get { return this.DocumentSpecificDataSetList != null && this.DocumentSpecificDataSetList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public DOCUMENT_SPECIFIC_DATA_SETS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
