namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    public class DOCUMENT_SPECIFIC_DATA_SET
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                if (Mismo3Utilities.ExceedsThreshold(1,
                    new List<bool> {
                        this.AssignmentSpecified,
                        this.GfeSpecified,
                        this.Hud1Specified,
                        this.IntegratedDisclosureSpecified,
                        this.NoteSpecified,
                        this.NoticeOfRightToCancelSpecified,
                        this.SecurityInstrumentSpecified,
                        this.TilDisclosureSpecified,
                        this.UrlaSpecified }))
                {
                    throw new System.Xml.Schema.XmlSchemaValidationException(
                        Mismo3Utilities.GetXSDChoiceExceptionMessage(
                        "DOCUMENT_SPECIFIC_DATA_SET",
                        new List<string> { "ASSIGNMENT", "GFE", "HUD1", "INTEGRATED_DISCLOSURE", "NOTE", "NOTICE_OF_RIGHT_TO_CANCEL", "SECURITY_INSTRUMENT", "TIL_DISCLOSURE", "URLA" }));
                }

                return this.AssignmentSpecified
                    || this.GfeSpecified
                    || this.Hud1Specified
                    || this.IntegratedDisclosureSpecified
                    || this.NoteSpecified
                    || this.NoticeOfRightToCancelSpecified
                    || this.SecurityInstrumentSpecified
                    || this.TilDisclosureSpecified
                    || this.UrlaSpecified
                    || this.DocumentClassesSpecified
                    || this.ExecutionSpecified
                    || this.RecordingEndorsementsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ASSIGNMENT", Order = 0)]
        public ASSIGNMENT Assignment { get; set; }
    
        [XmlIgnore]
        public bool AssignmentSpecified
        {
            get { return this.Assignment != null && this.Assignment.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("GFE", Order = 1)]
        public GFE Gfe { get; set; }
    
        [XmlIgnore]
        public bool GfeSpecified
        {
            get { return this.Gfe != null && this.Gfe.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("HUD1", Order = 2)]
        public HUD1 Hud1 { get; set; }
    
        [XmlIgnore]
        public bool Hud1Specified
        {
            get { return this.Hud1 != null && this.Hud1.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("INTEGRATED_DISCLOSURE", Order = 3)]
        public INTEGRATED_DISCLOSURE IntegratedDisclosure { get; set; }
    
        [XmlIgnore]
        public bool IntegratedDisclosureSpecified
        {
            get { return this.IntegratedDisclosure != null && this.IntegratedDisclosure.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("NOTE", Order = 4)]
        public NOTE Note { get; set; }
    
        [XmlIgnore]
        public bool NoteSpecified
        {
            get { return this.Note != null && this.Note.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("NOTICE_OF_RIGHT_TO_CANCEL", Order = 5)]
        public NOTICE_OF_RIGHT_TO_CANCEL NoticeOfRightToCancel { get; set; }
    
        [XmlIgnore]
        public bool NoticeOfRightToCancelSpecified
        {
            get { return this.NoticeOfRightToCancel != null && this.NoticeOfRightToCancel.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SECURITY_INSTRUMENT", Order = 6)]
        public SECURITY_INSTRUMENT SecurityInstrument { get; set; }
    
        [XmlIgnore]
        public bool SecurityInstrumentSpecified
        {
            get { return this.SecurityInstrument != null && this.SecurityInstrument.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("TIL_DISCLOSURE", Order = 7)]
        public TIL_DISCLOSURE TilDisclosure { get; set; }
    
        [XmlIgnore]
        public bool TilDisclosureSpecified
        {
            get { return this.TilDisclosure != null && this.TilDisclosure.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("URLA", Order = 8)]
        public URLA Urla { get; set; }
    
        [XmlIgnore]
        public bool UrlaSpecified
        {
            get { return this.Urla != null && this.Urla.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("DOCUMENT_CLASSES", Order = 9)]
        public DOCUMENT_CLASSES DocumentClasses { get; set; }
    
        [XmlIgnore]
        public bool DocumentClassesSpecified
        {
            get { return this.DocumentClasses != null && this.DocumentClasses.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXECUTION", Order = 10)]
        public EXECUTION Execution { get; set; }
    
        [XmlIgnore]
        public bool ExecutionSpecified
        {
            get { return this.Execution != null && this.Execution.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("RECORDING_ENDORSEMENTS", Order = 11)]
        public RECORDING_ENDORSEMENTS RecordingEndorsements { get; set; }
    
        [XmlIgnore]
        public bool RecordingEndorsementsSpecified
        {
            get { return this.RecordingEndorsements != null && this.RecordingEndorsements.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 12)]
        public DOCUMENT_SPECIFIC_DATA_SET_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
