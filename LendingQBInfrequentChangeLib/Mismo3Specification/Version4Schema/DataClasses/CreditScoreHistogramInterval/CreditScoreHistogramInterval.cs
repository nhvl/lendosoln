namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CREDIT_SCORE_HISTOGRAM_INTERVAL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditScoreHistogramIntervalHighValueSpecified
                    || this.CreditScoreHistogramIntervalLowValueSpecified
                    || this.CreditScoreHistogramIntervalScoreRangeOccurrencePercentSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CreditScoreHistogramIntervalHighValue", Order = 0)]
        public MISMOValue CreditScoreHistogramIntervalHighValue { get; set; }
    
        [XmlIgnore]
        public bool CreditScoreHistogramIntervalHighValueSpecified
        {
            get { return this.CreditScoreHistogramIntervalHighValue != null; }
            set { }
        }
    
        [XmlElement("CreditScoreHistogramIntervalLowValue", Order = 1)]
        public MISMOValue CreditScoreHistogramIntervalLowValue { get; set; }
    
        [XmlIgnore]
        public bool CreditScoreHistogramIntervalLowValueSpecified
        {
            get { return this.CreditScoreHistogramIntervalLowValue != null; }
            set { }
        }
    
        [XmlElement("CreditScoreHistogramIntervalScoreRangeOccurrencePercent", Order = 2)]
        public MISMOPercent CreditScoreHistogramIntervalScoreRangeOccurrencePercent { get; set; }
    
        [XmlIgnore]
        public bool CreditScoreHistogramIntervalScoreRangeOccurrencePercentSpecified
        {
            get { return this.CreditScoreHistogramIntervalScoreRangeOccurrencePercent != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public CREDIT_SCORE_HISTOGRAM_INTERVAL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
