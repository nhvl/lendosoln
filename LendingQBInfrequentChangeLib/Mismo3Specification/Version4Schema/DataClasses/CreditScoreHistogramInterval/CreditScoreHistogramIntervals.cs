namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class CREDIT_SCORE_HISTOGRAM_INTERVALS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditScoreHistogramIntervalListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CREDIT_SCORE_HISTOGRAM_INTERVAL", Order = 0)]
        public List<CREDIT_SCORE_HISTOGRAM_INTERVAL> CreditScoreHistogramIntervalList { get; set; } = new List<CREDIT_SCORE_HISTOGRAM_INTERVAL>();
    
        [XmlIgnore]
        public bool CreditScoreHistogramIntervalListSpecified
        {
            get { return this.CreditScoreHistogramIntervalList != null && this.CreditScoreHistogramIntervalList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public CREDIT_SCORE_HISTOGRAM_INTERVALS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
