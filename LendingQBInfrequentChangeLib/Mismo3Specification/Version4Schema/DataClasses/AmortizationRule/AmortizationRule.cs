namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class AMORTIZATION_RULE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AmortizationTypeSpecified
                    || this.AmortizationTypeOtherDescriptionSpecified
                    || this.LoanAmortizationMaximumTermMonthsCountSpecified
                    || this.LoanAmortizationPeriodCountSpecified
                    || this.LoanAmortizationPeriodTypeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AmortizationType", Order = 0)]
        public MISMOEnum<AmortizationBase> AmortizationType { get; set; }
    
        [XmlIgnore]
        public bool AmortizationTypeSpecified
        {
            get { return this.AmortizationType != null; }
            set { }
        }
    
        [XmlElement("AmortizationTypeOtherDescription", Order = 1)]
        public MISMOString AmortizationTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool AmortizationTypeOtherDescriptionSpecified
        {
            get { return this.AmortizationTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("LoanAmortizationMaximumTermMonthsCount", Order = 2)]
        public MISMOCount LoanAmortizationMaximumTermMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool LoanAmortizationMaximumTermMonthsCountSpecified
        {
            get { return this.LoanAmortizationMaximumTermMonthsCount != null; }
            set { }
        }
    
        [XmlElement("LoanAmortizationPeriodCount", Order = 3)]
        public MISMOCount LoanAmortizationPeriodCount { get; set; }
    
        [XmlIgnore]
        public bool LoanAmortizationPeriodCountSpecified
        {
            get { return this.LoanAmortizationPeriodCount != null; }
            set { }
        }
    
        [XmlElement("LoanAmortizationPeriodType", Order = 4)]
        public MISMOEnum<LoanAmortizationPeriodBase> LoanAmortizationPeriodType { get; set; }
    
        [XmlIgnore]
        public bool LoanAmortizationPeriodTypeSpecified
        {
            get { return this.LoanAmortizationPeriodType != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public AMORTIZATION_RULE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
