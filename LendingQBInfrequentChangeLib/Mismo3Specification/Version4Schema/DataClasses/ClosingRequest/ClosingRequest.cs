namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CLOSING_REQUEST
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ClosingRequestDetailSpecified
                    || this.ClosingRevisionsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CLOSING_REQUEST_DETAIL", Order = 0)]
        public CLOSING_REQUEST_DETAIL ClosingRequestDetail { get; set; }
    
        [XmlIgnore]
        public bool ClosingRequestDetailSpecified
        {
            get { return this.ClosingRequestDetail != null && this.ClosingRequestDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CLOSING_REVISIONS", Order = 1)]
        public CLOSING_REVISIONS ClosingRevisions { get; set; }
    
        [XmlIgnore]
        public bool ClosingRevisionsSpecified
        {
            get { return this.ClosingRevisions != null && this.ClosingRevisions.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public CLOSING_REQUEST_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
