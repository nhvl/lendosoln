namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CLOSING_REQUEST_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ClosingScheduledDatetimeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ClosingScheduledDatetime", Order = 0)]
        public MISMODatetime ClosingScheduledDatetime { get; set; }
    
        [XmlIgnore]
        public bool ClosingScheduledDatetimeSpecified
        {
            get { return this.ClosingScheduledDatetime != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public CLOSING_REQUEST_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
