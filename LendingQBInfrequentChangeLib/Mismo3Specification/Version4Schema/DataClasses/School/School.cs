namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SCHOOL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SchoolDescriptionSpecified
                    || this.SchoolDistrictNameSpecified
                    || this.SchoolDistrictURLSpecified
                    || this.SchoolMaximumGradeTypeSpecified
                    || this.SchoolMinimumGradeTypeSpecified
                    || this.SchoolNameSpecified
                    || this.SchoolTypeSpecified
                    || this.SchoolTypeGradeCountSpecified
                    || this.SchoolTypeOtherDescriptionSpecified
                    || this.SchoolURLSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SchoolDescription", Order = 0)]
        public MISMOString SchoolDescription { get; set; }
    
        [XmlIgnore]
        public bool SchoolDescriptionSpecified
        {
            get { return this.SchoolDescription != null; }
            set { }
        }
    
        [XmlElement("SchoolDistrictName", Order = 1)]
        public MISMOString SchoolDistrictName { get; set; }
    
        [XmlIgnore]
        public bool SchoolDistrictNameSpecified
        {
            get { return this.SchoolDistrictName != null; }
            set { }
        }
    
        [XmlElement("SchoolDistrictURL", Order = 2)]
        public MISMOURL SchoolDistrictURL { get; set; }
    
        [XmlIgnore]
        public bool SchoolDistrictURLSpecified
        {
            get { return this.SchoolDistrictURL != null; }
            set { }
        }
    
        [XmlElement("SchoolMaximumGradeType", Order = 3)]
        public MISMOEnum<SchoolGradeBase> SchoolMaximumGradeType { get; set; }
    
        [XmlIgnore]
        public bool SchoolMaximumGradeTypeSpecified
        {
            get { return this.SchoolMaximumGradeType != null; }
            set { }
        }
    
        [XmlElement("SchoolMinimumGradeType", Order = 4)]
        public MISMOEnum<SchoolGradeBase> SchoolMinimumGradeType { get; set; }
    
        [XmlIgnore]
        public bool SchoolMinimumGradeTypeSpecified
        {
            get { return this.SchoolMinimumGradeType != null; }
            set { }
        }
    
        [XmlElement("SchoolName", Order = 5)]
        public MISMOString SchoolName { get; set; }
    
        [XmlIgnore]
        public bool SchoolNameSpecified
        {
            get { return this.SchoolName != null; }
            set { }
        }
    
        [XmlElement("SchoolType", Order = 6)]
        public MISMOEnum<SchoolBase> SchoolType { get; set; }
    
        [XmlIgnore]
        public bool SchoolTypeSpecified
        {
            get { return this.SchoolType != null; }
            set { }
        }
    
        [XmlElement("SchoolTypeGradeCount", Order = 7)]
        public MISMOCount SchoolTypeGradeCount { get; set; }
    
        [XmlIgnore]
        public bool SchoolTypeGradeCountSpecified
        {
            get { return this.SchoolTypeGradeCount != null; }
            set { }
        }
    
        [XmlElement("SchoolTypeOtherDescription", Order = 8)]
        public MISMOString SchoolTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool SchoolTypeOtherDescriptionSpecified
        {
            get { return this.SchoolTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("SchoolURL", Order = 9)]
        public MISMOURL SchoolURL { get; set; }
    
        [XmlIgnore]
        public bool SchoolURLSpecified
        {
            get { return this.SchoolURL != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 10)]
        public SCHOOL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
