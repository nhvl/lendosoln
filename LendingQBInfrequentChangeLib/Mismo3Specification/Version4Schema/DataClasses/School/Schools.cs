namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class SCHOOLS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SchoolListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SCHOOL", Order = 0)]
        public List<SCHOOL> SchoolList { get; set; } = new List<SCHOOL>();
    
        [XmlIgnore]
        public bool SchoolListSpecified
        {
            get { return this.SchoolList != null && this.SchoolList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public SCHOOLS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
