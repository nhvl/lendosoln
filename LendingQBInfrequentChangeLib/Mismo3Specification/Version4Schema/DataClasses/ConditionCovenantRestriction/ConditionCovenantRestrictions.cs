namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class CONDITION_COVENANT_RESTRICTIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ConditionCovenantRestrictionListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CONDITION_COVENANT_RESTRICTION", Order = 0)]
        public List<CONDITION_COVENANT_RESTRICTION> ConditionCovenantRestrictionList { get; set; } = new List<CONDITION_COVENANT_RESTRICTION>();
    
        [XmlIgnore]
        public bool ConditionCovenantRestrictionListSpecified
        {
            get { return this.ConditionCovenantRestrictionList != null && this.ConditionCovenantRestrictionList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public CONDITION_COVENANT_RESTRICTIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
