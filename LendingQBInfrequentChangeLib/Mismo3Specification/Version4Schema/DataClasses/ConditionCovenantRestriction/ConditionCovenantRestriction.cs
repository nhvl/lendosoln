namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CONDITION_COVENANT_RESTRICTION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CovenantDescriptionSpecified
                    || this.CovenantTypeSpecified
                    || this.CovenantTypeOtherDescriptionSpecified
                    || this.RestrictionDescriptionSpecified
                    || this.RestrictionTypeSpecified
                    || this.RestrictionTypeOtherDescriptionSpecified
                    || this.ServitudeSignificanceTypeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CovenantDescription", Order = 0)]
        public MISMOString CovenantDescription { get; set; }
    
        [XmlIgnore]
        public bool CovenantDescriptionSpecified
        {
            get { return this.CovenantDescription != null; }
            set { }
        }
    
        [XmlElement("CovenantType", Order = 1)]
        public MISMOEnum<CovenantBase> CovenantType { get; set; }
    
        [XmlIgnore]
        public bool CovenantTypeSpecified
        {
            get { return this.CovenantType != null; }
            set { }
        }
    
        [XmlElement("CovenantTypeOtherDescription", Order = 2)]
        public MISMOString CovenantTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CovenantTypeOtherDescriptionSpecified
        {
            get { return this.CovenantTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("RestrictionDescription", Order = 3)]
        public MISMOString RestrictionDescription { get; set; }
    
        [XmlIgnore]
        public bool RestrictionDescriptionSpecified
        {
            get { return this.RestrictionDescription != null; }
            set { }
        }
    
        [XmlElement("RestrictionType", Order = 4)]
        public MISMOEnum<RestrictionBase> RestrictionType { get; set; }
    
        [XmlIgnore]
        public bool RestrictionTypeSpecified
        {
            get { return this.RestrictionType != null; }
            set { }
        }
    
        [XmlElement("RestrictionTypeOtherDescription", Order = 5)]
        public MISMOString RestrictionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool RestrictionTypeOtherDescriptionSpecified
        {
            get { return this.RestrictionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ServitudeSignificanceType", Order = 6)]
        public MISMOEnum<ServitudeSignificanceBase> ServitudeSignificanceType { get; set; }
    
        [XmlIgnore]
        public bool ServitudeSignificanceTypeSpecified
        {
            get { return this.ServitudeSignificanceType != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 7)]
        public CONDITION_COVENANT_RESTRICTION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
