namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PRINCIPAL_AND_INTEREST_PAYMENT_PERIODIC_ADJUSTMENT_RULE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AdjustmentRuleTypeSpecified
                    || this.PeriodicEffectiveDateSpecified
                    || this.PeriodicEffectiveMonthsCountSpecified
                    || this.PrincipalAndInterestPaymentAnnualCapAmountSpecified
                    || this.PrincipalAndInterestRecastMonthsCountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AdjustmentRuleType", Order = 0)]
        public MISMOEnum<AdjustmentRuleBase> AdjustmentRuleType { get; set; }
    
        [XmlIgnore]
        public bool AdjustmentRuleTypeSpecified
        {
            get { return this.AdjustmentRuleType != null; }
            set { }
        }
    
        [XmlElement("PeriodicEffectiveDate", Order = 1)]
        public MISMODate PeriodicEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool PeriodicEffectiveDateSpecified
        {
            get { return this.PeriodicEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("PeriodicEffectiveMonthsCount", Order = 2)]
        public MISMOCount PeriodicEffectiveMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool PeriodicEffectiveMonthsCountSpecified
        {
            get { return this.PeriodicEffectiveMonthsCount != null; }
            set { }
        }
    
        [XmlElement("PrincipalAndInterestPaymentAnnualCapAmount", Order = 3)]
        public MISMOAmount PrincipalAndInterestPaymentAnnualCapAmount { get; set; }
    
        [XmlIgnore]
        public bool PrincipalAndInterestPaymentAnnualCapAmountSpecified
        {
            get { return this.PrincipalAndInterestPaymentAnnualCapAmount != null; }
            set { }
        }
    
        [XmlElement("PrincipalAndInterestRecastMonthsCount", Order = 4)]
        public MISMOCount PrincipalAndInterestRecastMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool PrincipalAndInterestRecastMonthsCountSpecified
        {
            get { return this.PrincipalAndInterestRecastMonthsCount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public PRINCIPAL_AND_INTEREST_PAYMENT_PERIODIC_ADJUSTMENT_RULE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
