namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class AUDIT_TRAIL_ENTRY_EVIDENCE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ForeignObjectSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("FOREIGN_OBJECT", Order = 0)]
        public FOREIGN_OBJECT ForeignObject { get; set; }
    
        [XmlIgnore]
        public bool ForeignObjectSpecified
        {
            get { return this.ForeignObject != null && this.ForeignObject.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public AUDIT_TRAIL_ENTRY_EVIDENCE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
