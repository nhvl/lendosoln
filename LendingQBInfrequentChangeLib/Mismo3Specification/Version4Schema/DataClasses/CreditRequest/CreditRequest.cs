namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CREDIT_REQUEST
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditInquiriesSpecified
                    || this.CreditLiabilitiesSpecified
                    || this.CreditPublicRecordsSpecified
                    || this.CreditRequestDataInformationSpecified
                    || this.CreditRequestDatasSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CREDIT_INQUIRIES", Order = 0)]
        public CREDIT_INQUIRIES CreditInquiries { get; set; }
    
        [XmlIgnore]
        public bool CreditInquiriesSpecified
        {
            get { return this.CreditInquiries != null && this.CreditInquiries.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_LIABILITIES", Order = 1)]
        public CREDIT_LIABILITIES CreditLiabilities { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilitiesSpecified
        {
            get { return this.CreditLiabilities != null && this.CreditLiabilities.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_PUBLIC_RECORDS", Order = 2)]
        public CREDIT_PUBLIC_RECORDS CreditPublicRecords { get; set; }
    
        [XmlIgnore]
        public bool CreditPublicRecordsSpecified
        {
            get { return this.CreditPublicRecords != null && this.CreditPublicRecords.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_REQUEST_DATA_INFORMATION", Order = 3)]
        public CREDIT_REQUEST_DATA_INFORMATION CreditRequestDataInformation { get; set; }
    
        [XmlIgnore]
        public bool CreditRequestDataInformationSpecified
        {
            get { return this.CreditRequestDataInformation != null && this.CreditRequestDataInformation.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_REQUEST_DATAS", Order = 4)]
        public CREDIT_REQUEST_DATAS CreditRequestDatas { get; set; }
    
        [XmlIgnore]
        public bool CreditRequestDatasSpecified
        {
            get { return this.CreditRequestDatas != null && this.CreditRequestDatas.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public CREDIT_REQUEST_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
