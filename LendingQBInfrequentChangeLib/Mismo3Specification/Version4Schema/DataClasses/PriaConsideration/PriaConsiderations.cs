namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class PRIA_CONSIDERATIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PriaConsiderationListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PRIA_CONSIDERATION", Order = 0)]
        public List<PRIA_CONSIDERATION> PriaConsiderationList { get; set; } = new List<PRIA_CONSIDERATION>();
    
        [XmlIgnore]
        public bool PriaConsiderationListSpecified
        {
            get { return this.PriaConsiderationList != null && this.PriaConsiderationList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public PRIA_CONSIDERATIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
