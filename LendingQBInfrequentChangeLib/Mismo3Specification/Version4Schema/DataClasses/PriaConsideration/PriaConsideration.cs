namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PRIA_CONSIDERATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ConsiderationAmountSpecified
                    || this.ConsiderationTypeSpecified
                    || this.ConsiderationTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ConsiderationAmount", Order = 0)]
        public MISMOAmount ConsiderationAmount { get; set; }
    
        [XmlIgnore]
        public bool ConsiderationAmountSpecified
        {
            get { return this.ConsiderationAmount != null; }
            set { }
        }
    
        [XmlElement("ConsiderationType", Order = 1)]
        public MISMOEnum<ConsiderationBase> ConsiderationType { get; set; }
    
        [XmlIgnore]
        public bool ConsiderationTypeSpecified
        {
            get { return this.ConsiderationType != null; }
            set { }
        }
    
        [XmlElement("ConsiderationTypeOtherDescription", Order = 2)]
        public MISMOString ConsiderationTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ConsiderationTypeOtherDescriptionSpecified
        {
            get { return this.ConsiderationTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public PRIA_CONSIDERATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
