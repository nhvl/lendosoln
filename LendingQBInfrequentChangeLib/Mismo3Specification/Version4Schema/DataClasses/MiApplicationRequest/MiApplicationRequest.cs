namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MI_APPLICATION_REQUEST
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MiApplicationSpecified
                    || this.MiApplicationRequestDetailSpecified
                    || this.MiProductsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MI_APPLICATION", Order = 0)]
        public MI_APPLICATION MiApplication { get; set; }
    
        [XmlIgnore]
        public bool MiApplicationSpecified
        {
            get { return this.MiApplication != null && this.MiApplication.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("MI_APPLICATION_REQUEST_DETAIL", Order = 1)]
        public MI_APPLICATION_REQUEST_DETAIL MiApplicationRequestDetail { get; set; }
    
        [XmlIgnore]
        public bool MiApplicationRequestDetailSpecified
        {
            get { return this.MiApplicationRequestDetail != null && this.MiApplicationRequestDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("MI_PRODUCTS", Order = 2)]
        public MI_PRODUCTS MiProducts { get; set; }
    
        [XmlIgnore]
        public bool MiProductsSpecified
        {
            get { return this.MiProducts != null && this.MiProducts.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public MI_APPLICATION_REQUEST_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
