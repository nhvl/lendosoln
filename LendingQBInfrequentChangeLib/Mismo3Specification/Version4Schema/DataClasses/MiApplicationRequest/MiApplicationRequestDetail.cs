namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MI_APPLICATION_REQUEST_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.InvestorProgramNameTypeSpecified
                    || this.InvestorProgramNameTypeOtherDescriptionSpecified
                    || this.MIApplicationRequestTypeSpecified
                    || this.MIApplicationRequestTypeOtherDescriptionSpecified
                    || this.MICertificateTypeSpecified
                    || this.MICompanyNameTypeSpecified
                    || this.MICompanyNameTypeOtherDescriptionSpecified
                    || this.MICoveragePlanTypeSpecified
                    || this.MICoveragePlanTypeOtherDescriptionSpecified
                    || this.RateQuoteAllProductIndicatorSpecified
                    || this.RateQuoteProductComparisonIndicatorSpecified
                    || this.RateQuoteTypeSpecified
                    || this.RateQuoteTypeOtherDescriptionSpecified
                    || this.SubordinateFinancingIsNewIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("InvestorProgramNameType", Order = 0)]
        public MISMOEnum<InvestorProgramNameBase> InvestorProgramNameType { get; set; }
    
        [XmlIgnore]
        public bool InvestorProgramNameTypeSpecified
        {
            get { return this.InvestorProgramNameType != null; }
            set { }
        }
    
        [XmlElement("InvestorProgramNameTypeOtherDescription", Order = 1)]
        public MISMOString InvestorProgramNameTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool InvestorProgramNameTypeOtherDescriptionSpecified
        {
            get { return this.InvestorProgramNameTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MIApplicationRequestType", Order = 2)]
        public MISMOEnum<MIApplicationRequestBase> MIApplicationRequestType { get; set; }
    
        [XmlIgnore]
        public bool MIApplicationRequestTypeSpecified
        {
            get { return this.MIApplicationRequestType != null; }
            set { }
        }
    
        [XmlElement("MIApplicationRequestTypeOtherDescription", Order = 3)]
        public MISMOString MIApplicationRequestTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MIApplicationRequestTypeOtherDescriptionSpecified
        {
            get { return this.MIApplicationRequestTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MICertificateType", Order = 4)]
        public MISMOEnum<MICertificateBase> MICertificateType { get; set; }
    
        [XmlIgnore]
        public bool MICertificateTypeSpecified
        {
            get { return this.MICertificateType != null; }
            set { }
        }
    
        [XmlElement("MICompanyNameType", Order = 5)]
        public MISMOEnum<MICompanyNameBase> MICompanyNameType { get; set; }
    
        [XmlIgnore]
        public bool MICompanyNameTypeSpecified
        {
            get { return this.MICompanyNameType != null; }
            set { }
        }
    
        [XmlElement("MICompanyNameTypeOtherDescription", Order = 6)]
        public MISMOString MICompanyNameTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MICompanyNameTypeOtherDescriptionSpecified
        {
            get { return this.MICompanyNameTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MICoveragePlanType", Order = 7)]
        public MISMOEnum<MICoveragePlanBase> MICoveragePlanType { get; set; }
    
        [XmlIgnore]
        public bool MICoveragePlanTypeSpecified
        {
            get { return this.MICoveragePlanType != null; }
            set { }
        }
    
        [XmlElement("MICoveragePlanTypeOtherDescription", Order = 8)]
        public MISMOString MICoveragePlanTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MICoveragePlanTypeOtherDescriptionSpecified
        {
            get { return this.MICoveragePlanTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("RateQuoteAllProductIndicator", Order = 9)]
        public MISMOIndicator RateQuoteAllProductIndicator { get; set; }
    
        [XmlIgnore]
        public bool RateQuoteAllProductIndicatorSpecified
        {
            get { return this.RateQuoteAllProductIndicator != null; }
            set { }
        }
    
        [XmlElement("RateQuoteProductComparisonIndicator", Order = 10)]
        public MISMOIndicator RateQuoteProductComparisonIndicator { get; set; }
    
        [XmlIgnore]
        public bool RateQuoteProductComparisonIndicatorSpecified
        {
            get { return this.RateQuoteProductComparisonIndicator != null; }
            set { }
        }
    
        [XmlElement("RateQuoteType", Order = 11)]
        public MISMOEnum<RateQuoteBase> RateQuoteType { get; set; }
    
        [XmlIgnore]
        public bool RateQuoteTypeSpecified
        {
            get { return this.RateQuoteType != null; }
            set { }
        }
    
        [XmlElement("RateQuoteTypeOtherDescription", Order = 12)]
        public MISMOString RateQuoteTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool RateQuoteTypeOtherDescriptionSpecified
        {
            get { return this.RateQuoteTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("SubordinateFinancingIsNewIndicator", Order = 13)]
        public MISMOIndicator SubordinateFinancingIsNewIndicator { get; set; }
    
        [XmlIgnore]
        public bool SubordinateFinancingIsNewIndicatorSpecified
        {
            get { return this.SubordinateFinancingIsNewIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 14)]
        public MI_APPLICATION_REQUEST_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
