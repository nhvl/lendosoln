namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class HUD1_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.HUD1CashToOrFromBorrowerIndicatorSpecified
                    || this.HUD1CashToOrFromSellerIndicatorSpecified
                    || this.HUD1ConventionalInsuredIndicatorSpecified
                    || this.HUD1FileNumberIdentifierSpecified
                    || this.HUD1SettlementDateSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("HUD1CashToOrFromBorrowerIndicator", Order = 0)]
        public MISMOIndicator HUD1CashToOrFromBorrowerIndicator { get; set; }
    
        [XmlIgnore]
        public bool HUD1CashToOrFromBorrowerIndicatorSpecified
        {
            get { return this.HUD1CashToOrFromBorrowerIndicator != null; }
            set { }
        }
    
        [XmlElement("HUD1CashToOrFromSellerIndicator", Order = 1)]
        public MISMOIndicator HUD1CashToOrFromSellerIndicator { get; set; }
    
        [XmlIgnore]
        public bool HUD1CashToOrFromSellerIndicatorSpecified
        {
            get { return this.HUD1CashToOrFromSellerIndicator != null; }
            set { }
        }
    
        [XmlElement("HUD1ConventionalInsuredIndicator", Order = 2)]
        public MISMOIndicator HUD1ConventionalInsuredIndicator { get; set; }
    
        [XmlIgnore]
        public bool HUD1ConventionalInsuredIndicatorSpecified
        {
            get { return this.HUD1ConventionalInsuredIndicator != null; }
            set { }
        }
    
        [XmlElement("HUD1FileNumberIdentifier", Order = 3)]
        public MISMOIdentifier HUD1FileNumberIdentifier { get; set; }
    
        [XmlIgnore]
        public bool HUD1FileNumberIdentifierSpecified
        {
            get { return this.HUD1FileNumberIdentifier != null; }
            set { }
        }
    
        [XmlElement("HUD1SettlementDate", Order = 4)]
        public MISMODate HUD1SettlementDate { get; set; }
    
        [XmlIgnore]
        public bool HUD1SettlementDateSpecified
        {
            get { return this.HUD1SettlementDate != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public HUD1_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
