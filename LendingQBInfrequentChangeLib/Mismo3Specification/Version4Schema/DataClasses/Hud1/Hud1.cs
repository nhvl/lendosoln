namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class HUD1
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.Hud1DetailSpecified
                    || this.Hud1LineItemsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("HUD1_DETAIL", Order = 0)]
        public HUD1_DETAIL Hud1Detail { get; set; }
    
        [XmlIgnore]
        public bool Hud1DetailSpecified
        {
            get { return this.Hud1Detail != null && this.Hud1Detail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("HUD1_LINE_ITEMS", Order = 1)]
        public HUD1_LINE_ITEMS Hud1LineItems { get; set; }
    
        [XmlIgnore]
        public bool Hud1LineItemsSpecified
        {
            get { return this.Hud1LineItems != null && this.Hud1LineItems.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public HUD1_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
