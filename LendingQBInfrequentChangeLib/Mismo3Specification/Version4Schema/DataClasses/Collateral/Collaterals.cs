namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class COLLATERALS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CollateralListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("COLLATERAL", Order = 0)]
        public List<COLLATERAL> CollateralList { get; set; } = new List<COLLATERAL>();
    
        [XmlIgnore]
        public bool CollateralListSpecified
        {
            get { return this.CollateralList != null && this.CollateralList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public COLLATERALS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
