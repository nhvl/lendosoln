namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Xml.Schema;
    using System.Xml.Serialization;

    public class COLLATERAL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                if (Mismo3Utilities.ExceedsThreshold(1, new List<bool> { this.PledgedAssetSpecified, this.SubjectPropertySpecified }))
                {
                    throw new System.Xml.Schema.XmlSchemaValidationException(
                        Mismo3Utilities.GetXSDChoiceExceptionMessage(
                        "COLLATERAL",
                        new List<string> { "PLEDGED_ASSET", "SUBJECT_PROPERTY" }));
                }

                return this.CollateralDetailSpecified
                    || this.ExtensionSpecified
                    || this.PledgedAssetSpecified
                    || this.SubjectPropertySpecified;
            }
        }
    
        [XmlElement("PLEDGED_ASSET", Order = 0)]
        public ASSET PledgedAsset { get; set; }
    
        [XmlIgnore]
        public bool PledgedAssetSpecified
        {
            get { return this.PledgedAsset != null; }
            set { }
        }
    
        [XmlElement("SUBJECT_PROPERTY", Order = 1)]
        public PROPERTY SubjectProperty { get; set; }
    
        [XmlIgnore]
        public bool SubjectPropertySpecified
        {
            get { return this.SubjectProperty != null; }
            set { }
        }
    
        [XmlElement("COLLATERAL_DETAIL", Order = 2)]
        public COLLATERAL_DETAIL CollateralDetail { get; set; }
    
        [XmlIgnore]
        public bool CollateralDetailSpecified
        {
            get { return this.CollateralDetail != null && this.CollateralDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public COLLATERAL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }

        [XmlAttribute("label", Form = XmlSchemaForm.Qualified, Namespace = Mismo3Constants.XlinkNamespace)]
        public string XlinkLabel { get; set; }

        [XmlIgnore]
        public bool XlinkLabelSpecified
        {
            get { return !string.IsNullOrEmpty(this.XlinkLabel); }
            set { }
        }
    }
}
