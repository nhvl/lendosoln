namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class COLLATERAL_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LienPriorityExceptionTypeSpecified
                    || this.LienPriorityExceptionTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LienPriorityExceptionType", Order = 0)]
        public MISMOEnum<LienPriorityBase> LienPriorityExceptionType { get; set; }
    
        [XmlIgnore]
        public bool LienPriorityExceptionTypeSpecified
        {
            get { return this.LienPriorityExceptionType != null; }
            set { }
        }
    
        [XmlElement("LienPriorityExceptionTypeOtherDescription", Order = 1)]
        public MISMOString LienPriorityExceptionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool LienPriorityExceptionTypeOtherDescriptionSpecified
        {
            get { return this.LienPriorityExceptionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public COLLATERAL_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
