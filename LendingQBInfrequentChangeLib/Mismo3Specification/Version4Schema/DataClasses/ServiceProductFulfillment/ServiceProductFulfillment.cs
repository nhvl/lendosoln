namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SERVICE_PRODUCT_FULFILLMENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ContactPointsSpecified
                    || this.ServiceProductFulfillmentDetailSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CONTACT_POINTS", Order = 0)]
        public CONTACT_POINTS ContactPoints { get; set; }
    
        [XmlIgnore]
        public bool ContactPointsSpecified
        {
            get { return this.ContactPoints != null && this.ContactPoints.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SERVICE_PRODUCT_FULFILLMENT_DETAIL", Order = 1)]
        public SERVICE_PRODUCT_FULFILLMENT_DETAIL ServiceProductFulfillmentDetail { get; set; }
    
        [XmlIgnore]
        public bool ServiceProductFulfillmentDetailSpecified
        {
            get { return this.ServiceProductFulfillmentDetail != null && this.ServiceProductFulfillmentDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public SERVICE_PRODUCT_FULFILLMENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
