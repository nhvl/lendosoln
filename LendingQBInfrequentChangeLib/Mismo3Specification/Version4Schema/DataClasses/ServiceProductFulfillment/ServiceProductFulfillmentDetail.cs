namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SERVICE_PRODUCT_FULFILLMENT_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ServiceActualCompletionDateSpecified
                    || this.ServiceActualPriceAmountSpecified
                    || this.ServiceCancellationPriceAmountSpecified
                    || this.ServiceEstimatedCompletionDateSpecified
                    || this.ServiceEstimatedOffHoldDateSpecified
                    || this.ServiceEstimatedPriceAmountSpecified
                    || this.ServiceRangeEndDateSpecified
                    || this.ServiceRangeStartDateSpecified
                    || this.ServiceRequestedCompletionDateSpecified
                    || this.VendorOrderIdentifierSpecified
                    || this.VendorTransactionIdentifierSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ServiceActualCompletionDate", Order = 0)]
        public MISMODate ServiceActualCompletionDate { get; set; }
    
        [XmlIgnore]
        public bool ServiceActualCompletionDateSpecified
        {
            get { return this.ServiceActualCompletionDate != null; }
            set { }
        }
    
        [XmlElement("ServiceActualPriceAmount", Order = 1)]
        public MISMOAmount ServiceActualPriceAmount { get; set; }
    
        [XmlIgnore]
        public bool ServiceActualPriceAmountSpecified
        {
            get { return this.ServiceActualPriceAmount != null; }
            set { }
        }
    
        [XmlElement("ServiceCancellationPriceAmount", Order = 2)]
        public MISMOAmount ServiceCancellationPriceAmount { get; set; }
    
        [XmlIgnore]
        public bool ServiceCancellationPriceAmountSpecified
        {
            get { return this.ServiceCancellationPriceAmount != null; }
            set { }
        }
    
        [XmlElement("ServiceEstimatedCompletionDate", Order = 3)]
        public MISMODate ServiceEstimatedCompletionDate { get; set; }
    
        [XmlIgnore]
        public bool ServiceEstimatedCompletionDateSpecified
        {
            get { return this.ServiceEstimatedCompletionDate != null; }
            set { }
        }
    
        [XmlElement("ServiceEstimatedOffHoldDate", Order = 4)]
        public MISMODate ServiceEstimatedOffHoldDate { get; set; }
    
        [XmlIgnore]
        public bool ServiceEstimatedOffHoldDateSpecified
        {
            get { return this.ServiceEstimatedOffHoldDate != null; }
            set { }
        }
    
        [XmlElement("ServiceEstimatedPriceAmount", Order = 5)]
        public MISMOAmount ServiceEstimatedPriceAmount { get; set; }
    
        [XmlIgnore]
        public bool ServiceEstimatedPriceAmountSpecified
        {
            get { return this.ServiceEstimatedPriceAmount != null; }
            set { }
        }
    
        [XmlElement("ServiceRangeEndDate", Order = 6)]
        public MISMODate ServiceRangeEndDate { get; set; }
    
        [XmlIgnore]
        public bool ServiceRangeEndDateSpecified
        {
            get { return this.ServiceRangeEndDate != null; }
            set { }
        }
    
        [XmlElement("ServiceRangeStartDate", Order = 7)]
        public MISMODate ServiceRangeStartDate { get; set; }
    
        [XmlIgnore]
        public bool ServiceRangeStartDateSpecified
        {
            get { return this.ServiceRangeStartDate != null; }
            set { }
        }
    
        [XmlElement("ServiceRequestedCompletionDate", Order = 8)]
        public MISMODate ServiceRequestedCompletionDate { get; set; }
    
        [XmlIgnore]
        public bool ServiceRequestedCompletionDateSpecified
        {
            get { return this.ServiceRequestedCompletionDate != null; }
            set { }
        }
    
        [XmlElement("VendorOrderIdentifier", Order = 9)]
        public MISMOIdentifier VendorOrderIdentifier { get; set; }
    
        [XmlIgnore]
        public bool VendorOrderIdentifierSpecified
        {
            get { return this.VendorOrderIdentifier != null; }
            set { }
        }
    
        [XmlElement("VendorTransactionIdentifier", Order = 10)]
        public MISMOIdentifier VendorTransactionIdentifier { get; set; }
    
        [XmlIgnore]
        public bool VendorTransactionIdentifierSpecified
        {
            get { return this.VendorTransactionIdentifier != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 11)]
        public SERVICE_PRODUCT_FULFILLMENT_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
