﻿namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class MCL_SERVICE_PREFERRED_RESPONSE_FORMATS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ServicePreferredResponseFormatSpecified;
            }
        }

        [XmlElement("SERVICE_PREFERRED_RESPONSE_FORMAT")]
        public List<MCL_SERVICE_PREFERRED_RESPONSE_FORMAT> ServicePreferredResponseFormat = new List<MCL_SERVICE_PREFERRED_RESPONSE_FORMAT>();

        [XmlIgnore]
        public bool ServicePreferredResponseFormatSpecified
        {
            get
            {
                return this.ServicePreferredResponseFormat != null && this.ServicePreferredResponseFormat.Any(c => c != null && c.ShouldSerialize);
            }
            set
            {
            }
        }
    }
}