﻿namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class MCL_TRANSACTIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.TransactionListSpecified;
            }
        }

        [XmlElement(Order = 0, ElementName = "TRANSACTION", Namespace = Mismo3Constants.MclExtensionNamespace)]
        public List<MCL_TRANSACTION> TransactionList;

        [XmlIgnore]
        public bool TransactionListSpecified
        {
            get { return this.TransactionList != null && this.TransactionList.Any(transaction => transaction.ShouldSerialize); }
            set { }
        }
    }
}
