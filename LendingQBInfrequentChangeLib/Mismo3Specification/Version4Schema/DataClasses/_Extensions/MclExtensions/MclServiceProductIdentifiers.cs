﻿namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class MCL_SERVICE_PRODUCT_IDENTIFIERS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ServiceProductIdentifierSpecified;
            }
        }

        /// <summary>
        /// Generic container to keep not-so-common identifiers from various sources.
        /// </summary>
        [XmlElement("SERVICE_PRODUCT_IDENTIFIER")]
        public List<MCL_SERVICE_PRODUCT_IDENTIFIER> ServiceProductIdentifier = new List<MCL_SERVICE_PRODUCT_IDENTIFIER>();

        [XmlIgnore]
        public bool ServiceProductIdentifierSpecified
        {
            get { return this.ServiceProductIdentifier != null && this.ServiceProductIdentifier.Count(c => c != null && c.ShouldSerialize) > 0; }
            set { }
        }
    }
}