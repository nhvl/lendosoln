﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MCL_TRANSACTION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AmountSpecified;
            }
        }

        [XmlElement(Order = 0, Namespace = Mismo3Constants.MclExtensionNamespace)]
        public MISMOAmount Amount;

        [XmlIgnore]
        public bool AmountSpecified
        {
            get { return this.Amount != null; }
            set { }
        }

        [XmlElement(Order = 1, Namespace = Mismo3Constants.MclExtensionNamespace)]
        public MISMODate PostedDate;

        [XmlIgnore]
        public bool PostedDateSpecified
        {
            get { return this.PostedDate != null; }
            set { }
        }

        [XmlElement(Order = 2, Namespace = Mismo3Constants.MclExtensionNamespace)]
        public MISMOString Description;

        [XmlIgnore]
        public bool DescriptionSpecified
        {
            get { return this.Description != null; }
            set { }
        }

        [XmlElement(Order = 3, Namespace = Mismo3Constants.MclExtensionNamespace)]
        public MISMOString Memo;

        [XmlIgnore]
        public bool MemoSpecified
        {
            get { return this.Memo != null; }
            set { }
        }

        [XmlElement(Order = 4, Namespace = Mismo3Constants.MclExtensionNamespace)]
        public MISMOIdentifier InstitutionTransactionId;

        [XmlIgnore]
        public bool InstitutionTransactionIdSpecified
        {
            get { return this.InstitutionTransactionId != null; }
            set { }
        }

        [XmlElement(Order = 5, Namespace = Mismo3Constants.MclExtensionNamespace)]
        public MISMOString Category;

        [XmlIgnore]
        public bool CategorySpecified
        {
            get { return this.Category != null; }
            set { }
        }

        [XmlElement(Order = 6, Namespace = Mismo3Constants.MclExtensionNamespace)]
        public MISMOString Type;

        [XmlIgnore]
        public bool TypeSpecified
        {
            get { return this.Type != null; }
            set { }
        }
    }
}
