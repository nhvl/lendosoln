﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MCL_SERVICE_PRODUCT_DETAIL_EXTENSION : OTHER_BASE
    {
        [XmlIgnore]
        public override bool ShouldSerialize
        {
            get
            {
                return this.PreferredResponseFormatTypeSpecified  || this.ServicePreferredResponseFormatsSpecified;
            }
        }

        [XmlElement(Namespace = Mismo3Constants.MclExtensionNamespace)]
        public MISMOString PreferredResponseFormatType;

        [XmlIgnore]
        public bool PreferredResponseFormatTypeSpecified
        {
            get { return this.PreferredResponseFormatType != null; }
            set { }
        }

        [XmlElement("SERVICE_PREFERRED_RESPONSE_FORMATS", Namespace = Mismo3Constants.MclExtensionNamespace)]
        public MCL_SERVICE_PREFERRED_RESPONSE_FORMATS ServicePreferredResponseFormats;

        [XmlIgnore]
        public bool ServicePreferredResponseFormatsSpecified
        {
            get
            {
                return this.ServicePreferredResponseFormats != null && this.ServicePreferredResponseFormats.ShouldSerialize;
            }
            set
            {
            }
        }
    }
}
