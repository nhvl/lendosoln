﻿namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    using MCL;

    namespace MCL
    {
        public enum ProductIdentifierTypeEnum
        {
            /// <summary>
            /// A catchall value to account for a blank field in LQB.
            /// </summary>
            [XmlEnum("")]
            Blank,

            [XmlEnum("AUSReferenceNumber")]
            AUSReferenceNumber,

            [XmlEnum("VendorReferenceNumber")]
            VendorReferenceNumber
        }
    }

    public class MCL_SERVICE_PRODUCT_IDENTIFIER_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ProductIdentifierSpecified
                    || this.ProductIdentifierTypeSpecified
                    || this.SourceNameSpecified
                    || this.SourceIdentifierSpecified;
            }
        }

        /// <summary>
        /// Unique product identifier for the current identifier type and source.
        /// </summary>
        [XmlElement("ProductIdentifier")]
        public MISMOString ProductIdentifier;

        [XmlIgnore]
        public bool ProductIdentifierSpecified
        {
            get { return this.ProductIdentifier != null; }
            set { }
        }

        /// <summary>
        /// Type of product identifier.
        /// </summary>
        [XmlElement("ProductIdentifierType")]
        public MISMOEnum<ProductIdentifierTypeEnum> ProductIdentifierType;

        [XmlIgnore]
        public bool ProductIdentifierTypeSpecified
        {
            get { return this.ProductIdentifierType != null && this.ProductIdentifierType.EnumValue != ProductIdentifierTypeEnum.Blank; }
            set { }
        }

        /// <summary>
        /// Name of source of product identifier. Note that this name can change and <seealso cref="SourceIdentifier"/> should be used to filter by source.
        /// </summary>
        [XmlElement("SourceName")]
        public MISMOString SourceName;

        [XmlIgnore]
        public bool SourceNameSpecified
        {
            get { return this.SourceName != null; }
            set { }
        }

        /// <summary>
        /// Unique identifier for the source of product identifier. Only integer values are used at the moment.<para/>
        /// Possible values as of version 201707: 1 (MeridianLink), 2 (DataVerify), 3 (VeriTax), 4 (NCS), 5 (IncoCheck), 6 (UCS).
        /// </summary>
        [XmlElement("SourceIdentifier")]
        public MISMOString SourceIdentifier;

        [XmlIgnore]
        public bool SourceIdentifierSpecified
        {
            get { return this.SourceIdentifier != null; }
            set { }
        }
    }
}