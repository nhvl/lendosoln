﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;
    using MCL;

    namespace MCL
    {
        public enum ReportStatusEnum
        {
            /// <summary>
            /// A catchall value to account for a blank field in LQB.
            /// </summary>
            [XmlEnum("")]
            Blank,

            [XmlEnum("InProgress")]
            InProgress,

            [XmlEnum("Ready")]
            Ready
        }
    }

    public class MCL_SERVICE_PRODUCT_FULFILLMENT_DETAIL_EXTENSION : OTHER_BASE
    {
        [XmlIgnore]
        public override bool ShouldSerialize
        {
            get
            {
                return this.ReportStatusDescriptionSpecified ||
                       this.ReportStatusSpecified;
            }
        }

        [XmlElement(Namespace = Mismo3Constants.MclExtensionNamespace)]
        public MISMOEnum<ReportStatusEnum> ReportStatus;

        [XmlIgnore]
        public bool ReportStatusSpecified
        {
            get
            {
                return this.ReportStatus != null && this.ReportStatus.EnumValue != ReportStatusEnum.Blank;
            }
            set
            {
            }
        }

        [XmlElement(Namespace = Mismo3Constants.MclExtensionNamespace)]
        public MISMOString ReportStatusDescription;

        [XmlIgnore]
        public bool ReportStatusDescriptionSpecified
        {
            get
            {
                return this.ReportStatusDescription != null;
            }
            set
            {
            }
        }
    }
}
