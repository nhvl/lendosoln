﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MCL_ASSET_DETAIL_EXTENSION : OTHER_BASE
    {
        [XmlIgnore]
        public override bool ShouldSerialize
        {
            get
            {
                return this.TransactionsSpecified;
            }
        }

        [XmlElement(ElementName = "TRANSACTIONS", Namespace = Mismo3Constants.MclExtensionNamespace)]
        public MCL_TRANSACTIONS Transactions;

        [XmlIgnore]
        public bool TransactionsSpecified
        {
            get { return this.Transactions != null; }
            set { }
        }
    }
}
