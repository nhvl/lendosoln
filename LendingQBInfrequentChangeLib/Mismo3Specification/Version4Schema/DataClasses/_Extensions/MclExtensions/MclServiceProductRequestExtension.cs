﻿namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class MCL_SERVICE_PRODUCT_REQUEST_EXTENSION : OTHER_BASE
    {
        [XmlIgnore]
        public override bool ShouldSerialize
        {
            get
            {
                return this.ServiceProductIdentifiersSpecified;
            }
        }

        [XmlElement("SERVICE_PRODUCT_IDENTIFIERS", Namespace = Mismo3Constants.MclExtensionNamespace)]
        public MCL_SERVICE_PRODUCT_IDENTIFIERS ServiceProductIdentifiers;

        [XmlIgnore]
        public bool ServiceProductIdentifiersSpecified
        {
            get { return this.ServiceProductIdentifiers != null && this.ServiceProductIdentifiers.ShouldSerialize; }
            set { }
        }
    }
}
