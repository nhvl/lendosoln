﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MCL_SERVICE_PREFERRED_RESPONSE_FORMAT_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PreferredResponseFormatTypeSpecified;
            }
        }

        [XmlElement(Namespace = Mismo3Constants.MclExtensionNamespace)]
        public MISMOString PreferredResponseFormatType;

        [XmlIgnore]
        public bool PreferredResponseFormatTypeSpecified
        {
            get
            {
                return this.PreferredResponseFormatType != null;
            }
            set
            {
            }
        }
    }
}