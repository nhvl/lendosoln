﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MCL_STATUS_EXTENSION : OTHER_BASE
    {
        [XmlIgnore]
        public override bool ShouldSerialize
        {
            get
            {
                return this.StatusDateTimeSpecified;
            }
        }

        [XmlElement(Namespace = Mismo3Constants.MclExtensionNamespace)]
        public MISMODatetime StatusDateTime;

        [XmlIgnore]
        public bool StatusDateTimeSpecified
        {
            get { return this.StatusDateTime != null; }
            set { }
        }
    }
}
