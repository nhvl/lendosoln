﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MCL_SERVICE_PREFERRED_RESPONSE_FORMAT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ServicePreferredResponseFormatDetailSpecified;
            }
        }

        [XmlElement("SERVICE_PREFERRED_RESPONSE_FORMAT_DETAIL")]
        public MCL_SERVICE_PREFERRED_RESPONSE_FORMAT_DETAIL ServicePreferredResponseFormatDetail;

        [XmlIgnore]
        public bool ServicePreferredResponseFormatDetailSpecified
        {
            get
            {
                return this.ServicePreferredResponseFormatDetail != null && this.ServicePreferredResponseFormatDetail.ShouldSerialize;
            }
            set
            {
            }
        }
    }
}