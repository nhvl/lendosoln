﻿namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class MCL_SERVICE_PRODUCT_IDENTIFIER
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ServiceProductIdentifierDetailSpecified;
            }
        }

        [XmlElement("SERVICE_PRODUCT_IDENTIFIER_DETAIL")]
        public MCL_SERVICE_PRODUCT_IDENTIFIER_DETAIL ServiceProductIdentifierDetail;

        [XmlIgnore]
        public bool ServiceProductIdentifierDetailSpecified
        {
            get { return this.ServiceProductIdentifierDetail != null && this.ServiceProductIdentifierDetail.ShouldSerialize; }
            set { }
        }
    }
}