﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MCL_VERIFICATION_OF_INCOME_REQUEST_EXTENSION : OTHER_BASE
    {
        [XmlIgnore]
        public override bool ShouldSerialize
        {
            get
            {
                return this.FinancialAccountHistoryLengthSpecified
                    || this.RefreshPeriodSpecified
                    || this.SocialSecurityNumberLastFourDigitsSpecified
                    || this.EmploymentVerificationTypeSpecified
                    || this.VerificationOfIncomeRequestActionTypeSpecified;
            }
        }

        [XmlElement(Namespace = Mismo3Constants.MclExtensionNamespace)]
        public MISMOString EmploymentVerificationType;

        [XmlIgnore]
        public bool EmploymentVerificationTypeSpecified
        {
            get { return this.EmploymentVerificationType != null; }
            set { }
        }

        [XmlElement(Namespace = Mismo3Constants.MclExtensionNamespace)]
        public MISMOString FinancialAccountHistoryLength;

        [XmlIgnore]
        public bool FinancialAccountHistoryLengthSpecified
        {
            get { return this.FinancialAccountHistoryLength != null; }
            set { }
        }

        [XmlElement(Namespace = Mismo3Constants.MclExtensionNamespace)]
        public MISMOString PreviousFileNumber;

        [XmlIgnore]
        public bool PreviousFileNumberSpecified
        {
            get { return this.PreviousFileNumber != null; }
            set { }
        }

        [XmlElement(Namespace = Mismo3Constants.MclExtensionNamespace)]
        public MISMOString RefreshPeriod;

        [XmlIgnore]
        public bool RefreshPeriodSpecified
        {
            get { return this.RefreshPeriod != null; }
            set { }
        }

        [XmlElement(Namespace = Mismo3Constants.MclExtensionNamespace)]
        public MISMOString SocialSecurityNumberLastFourDigits;

        [XmlIgnore]
        public bool SocialSecurityNumberLastFourDigitsSpecified
        {
            get { return this.SocialSecurityNumberLastFourDigits != null; }
            set { }
        }

        [XmlElement(Namespace = Mismo3Constants.MclExtensionNamespace)]
        public MISMOString VerificationOfIncomeRequestActionType;

        [XmlIgnore]
        public bool VerificationOfIncomeRequestActionTypeSpecified
        {
            get { return this.VerificationOfIncomeRequestActionType != null; }
            set { }
        }
    }
}
