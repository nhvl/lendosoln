﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MCL_SERVICE_PREFERRED_RESPONSE_FORMATS_EXTENSION : OTHER_BASE
    {
        [XmlIgnore]
        public override bool ShouldSerialize
        {
            get
            {
                return this.ServicePreferredResponseFormatsSpecified;
            }
        }

        [XmlElement("SERVICE_PREFERRED_RESPONSE_FORMATS", Namespace = Mismo3Constants.MclExtensionNamespace)]
        public MCL_SERVICE_PREFERRED_RESPONSE_FORMATS ServicePreferredResponseFormats;

        [XmlIgnore]
        public bool ServicePreferredResponseFormatsSpecified
        {
            get
            {
                return this.ServicePreferredResponseFormats != null && this.ServicePreferredResponseFormats.ShouldSerialize;
            }
            set
            {
            }
        }
    }
}
