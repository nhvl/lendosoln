﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MCL_BORROWER_EXTENSION : OTHER_BASE
    {
        [XmlIgnore]
        public override bool ShouldSerialize
        {
            get
            {
                return this.AreTaxesSelfPreparedSpecified;
            }
        }

        [XmlElement(Namespace = Mismo3Constants.MclExtensionNamespace)]
        public MISMOIndicator AreTaxesSelfPrepared;

        [XmlIgnore]
        public bool AreTaxesSelfPreparedSpecified
        {
            get { return this.AreTaxesSelfPrepared != null; }
            set { }
        }
    }
}
