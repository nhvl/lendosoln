﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml;
    using System.Xml.Serialization;

    public class ULAD_LANGUAGE_EXTENSION : OTHER_BASE
    {
        [XmlIgnore]
        public override bool ShouldSerialize
        {
            get
            {
                return this.LanguageCodeOtherDescriptionSpecified
                    || this.LanguageRefusalIndicatorSpecified;
            }
        }

        [XmlElement("LanguageCodeOtherDescription", Namespace = Mismo3Constants.UladExtensionNamespace)]
        public MISMOString LanguageCodeOtherDescription { get; set; }

        [XmlIgnore]
        public bool LanguageCodeOtherDescriptionSpecified
        {
            get { return this.LanguageCodeOtherDescription != null; }
            set { }
        }

        [XmlElement("LanguageRefusalIndicator", Namespace = Mismo3Constants.UladExtensionNamespace)]
        public MISMOIndicator LanguageRefusalIndicator { get; set; }

        [XmlIgnore]
        public bool LanguageRefusalIndicatorSpecified
        {
            get { return this.LanguageRefusalIndicator != null; }
            set { }
        }
    }
}
