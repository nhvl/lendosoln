namespace Mismo3Specification.Version4Schema
{
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the EscrowDetailCushionNumberOfMonths element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the EscrowDetailCushionNumberOfMonths element has a value to serialize.</value>
    public class MISMO_ESCROW_ITEM_DETAIL_EXTENSION : MISMO_BASE
    {
        /// <summary>
        /// Gets a value indicating whether the MISMO_ESCROW_ITEM_DETAIL_EXTENSION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public override bool ShouldSerialize
        {
            get
            {
                return this.EscrowDetailCushionNumberOfMonthsSpecified;
            }
        }

        /// <summary>
        /// The maximum number of months allowed in the escrow cushion (per expense).
        /// </summary>
        [XmlElement("EscrowDetailCushionNumberOfMonths")]
        public MISMOCount EscrowDetailCushionNumberOfMonths;

        /// <summary>
        /// Gets or sets a value indicating whether the EscrowDetailCushionNumberOfMonths element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the EscrowDetailCushionNumberOfMonths element has a value to serialize.</value>
        [XmlIgnore]
        public bool EscrowDetailCushionNumberOfMonthsSpecified
        {
            get
            {
                return this.EscrowDetailCushionNumberOfMonths != null;
            }
        }
    }
}
