﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A MISMO extension to the CONSTRUCTION element.
    /// </summary>
    public class MISMO_CONSTRUCTION_EXTENSION : MISMO_BASE
    {
        /// <summary>
        /// Gets a value indicating whether the element should be serialized.
        /// </summary>
        public override bool ShouldSerialize
        {
            get
            {
                return this.AdjustmentSpecified;
            }
        }

        /// <summary>
        /// A MISMO adjustment element.
        /// </summary>
        [XmlElement("Adjustment", Namespace = Mismo3Constants.MismoNamespace)]
        public ADJUSTMENT Adjustment;

        /// <summary>
        /// Gets a value indicating whether the <see cref="Adjustment"/> element has data to serialize.
        /// </summary>
        [XmlIgnore]
        public bool AdjustmentSpecified
        {
            get { return this.Adjustment != null && this.Adjustment.ShouldSerialize; }
            set { }
        }
    }
}