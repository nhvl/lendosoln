﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MISMO_PROPERTY_TAX_EXTENSION : MISMO_BASE
    {
        [XmlIgnore]
        public override bool ShouldSerialize
        {
            get
            {
                return this.EscrowItemDisbursementsSpecified;
            }
        }

        [XmlElement("ESCROW_ITEM_DISBURSEMENTS")]
        public ESCROW_ITEM_DISBURSEMENTS EscrowItemDisbursements;

        [XmlIgnore]
        public bool EscrowItemDisbursementsSpecified
        {
            get { return this.EscrowItemDisbursements != null && this.EscrowItemDisbursements.ShouldSerialize; }
            set { }
        }
    }
}