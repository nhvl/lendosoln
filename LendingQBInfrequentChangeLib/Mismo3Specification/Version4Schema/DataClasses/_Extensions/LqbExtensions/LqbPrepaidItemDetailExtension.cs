namespace Mismo3Specification.Version4Schema
{
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// A proprietary extension to the PREPAID_ITEM_DETAIL element.
    /// </summary>
    public partial class LQB_PREPAID_ITEM_DETAIL_EXTENSION : OTHER_BASE
    {
        /// <summary>
        /// Gets a value indicating whether the LQB_PREPAID_ITEM_DETAIL_EXTENSION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public override bool ShouldSerialize
        {
            get
            {
                return this.PrepaidItemSpecifiedHud1LineNumberValueSpecified
                    || this.ClosingCostFeeTypeIdSpecified
                    || this.PrepaidItemPerDiemUnroundedAmountSpecified;
            }
        }

        /// <summary>
        /// The corresponding HUD line number for this prepaid item.
        /// </summary>
        [XmlElement("PrepaidItemSpecifiedHUD1LineNumberValue", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOValue PrepaidItemSpecifiedHud1LineNumberValue { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaidItemSpecifiedHUD1LineNumberValue has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaidItemSpecifiedHUD1LineNumberValue has been assigned a value.</value>
        public bool PrepaidItemSpecifiedHud1LineNumberValueSpecified
        {
            get
            {
                return this.PrepaidItemSpecifiedHud1LineNumberValue != null;
            }
        }

        /// <summary>
        /// The unique closing cost type ID of the fee.
        /// </summary>
        [XmlElement("ClosingCostFeeTypeId", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString ClosingCostFeeTypeId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the ClosingCostFeeTypeID element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ClosingCostFeeTypeID element has been assigned a value.</value>
        [XmlIgnore]
        public bool ClosingCostFeeTypeIdSpecified
        {
            get { return this.ClosingCostFeeTypeId != null; }
            set { }
        }

        /// <summary>
        /// The unrounded per diem amount. Some vendors want the full unrounded amount to six digits
        /// of precision, but a <see cref="MISMOAmount"/> can only have two-digit precision, so we need
        /// to send this as an extension.
        /// </summary>
        [XmlElement(Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMONumeric PrepaidItemPerDiemUnroundedAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the PrepaidItemPerDiemUnroundedAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the PrepaidItemPerDiemUnroundedAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool PrepaidItemPerDiemUnroundedAmountSpecified
        {
            get { return this.PrepaidItemPerDiemUnroundedAmount != null; }
            set { }
        }
    }
}
