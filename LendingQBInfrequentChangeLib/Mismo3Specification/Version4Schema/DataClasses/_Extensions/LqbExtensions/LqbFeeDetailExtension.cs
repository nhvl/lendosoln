namespace Mismo3Specification.Version4Schema
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the ClosingCostFeeTypeID element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the ClosingCostFeeTypeID element has been assigned a value.</value>
    public class LQB_FEE_DETAIL_EXTENSION : OTHER_BASE
    {
        /// <summary>
        /// Gets a value indicating whether the LQB_FEE_DETAIL_EXTENSION element should be serialized.
        /// </summary>
        public override bool ShouldSerialize
        {
            get
            {
                return this.DFLPIndicatorSpecified
                    || this.OriginalDescriptionSpecified
                    || this.ClosingCostFeeTypeIDSpecified
                    || this.CanShopSpecified
                    || this.DidShopSpecified;
            }
        }

        /// <summary>
        /// An indicator of whether the DFLP checkbox has been checked for this fee.
        /// </summary>
        [XmlElement("DFLPIndicator", Namespace = "http://www.lendingqb.com")]
        public MISMOIndicator DFLPIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the DFLPIndicator element has a value to serialize.
        /// </summary>
        [XmlIgnore]
        public bool DFLPIndicatorSpecified
        {
            get { return this.DFLPIndicator != null; }
        }

        /// <summary>
        /// The unmodified description of the fee.
        /// </summary>
        [XmlElement("OriginalDescription", Namespace = "http://www.lendingqb.com")]
        public MISMOString OriginalDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the OriginalDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the OriginalDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool OriginalDescriptionSpecified
        {
            get { return this.OriginalDescription != null; }
            set { }
        }

        /// <summary>
        /// The unique closing cost type ID of the fee.
        /// </summary>
        [XmlElement("ClosingCostFeeTypeID", Namespace = "http://www.lendingqb.com")]
        public MISMOString ClosingCostFeeTypeID;

        /// <summary>
        /// Gets or sets a value indicating whether the ClosingCostFeeTypeID element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ClosingCostFeeTypeID element has been assigned a value.</value>
        [XmlIgnore]
        public bool ClosingCostFeeTypeIDSpecified
        {
            get { return this.ClosingCostFeeTypeID != null; }
            set { }
        }

        /// <summary>
        /// An indicator of whether the Can Shop checkbox has been checked for this fee.
        /// </summary>
        [XmlElement("CanShop", Namespace = "http://www.lendingqb.com")]
        public MISMOIndicator CanShop;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="CanShop"/> element has a value to serialize.
        /// </summary>
        [XmlIgnore]
        public bool CanShopSpecified
        {
            get { return this.CanShop != null; }
        }

        /// <summary>
        /// An indicator of whether the Did Shop checkbox has been checked for this fee.
        /// </summary>
        [XmlElement("DidShop", Namespace = "http://www.lendingqb.com")]
        public MISMOIndicator DidShop;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="DidShop"/> element has a value to serialize.
        /// </summary>
        [XmlIgnore]
        public bool DidShopSpecified
        {
            get { return this.DidShop != null; }
        }
    }
}
