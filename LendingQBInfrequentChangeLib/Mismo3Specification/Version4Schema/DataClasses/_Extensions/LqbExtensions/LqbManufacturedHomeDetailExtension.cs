namespace Mismo3Specification.Version4Schema
{
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the CertificateOfTitleType element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the CertificateOfTitleType element has been assigned a value.</value>
    public class LQB_MANUFACTURED_HOME_DETAIL_EXTENSION : OTHER_BASE
    {
        /// <summary>
        /// Gets a value indicating whether the LQB_MANUFACTURED_HOME_DETAIL_EXTENSION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public override bool ShouldSerialize
        {
            get
            {
                return this.CertificateOfTitleTypeSpecified;
            }
        }

        /// <summary>
        /// An enumeration indicating the certificate of title type for a manufactured home.
        /// </summary>
        [XmlElement("CertificateOfTitleType", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbCertificateOfTitleBase> CertificateOfTitleType;

        /// <summary>
        /// Gets or sets a value indicating whether the CertificateOfTitleType element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the CertificateOfTitleType element has been assigned a value.</value>
        [XmlIgnore]
        public bool CertificateOfTitleTypeSpecified
        {
            get { return this.CertificateOfTitleType != null; }
            set { }
        }
    }
}
