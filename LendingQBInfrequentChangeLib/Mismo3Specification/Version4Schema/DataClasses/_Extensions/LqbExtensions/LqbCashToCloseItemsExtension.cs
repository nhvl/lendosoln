namespace Mismo3Specification.Version4Schema
{
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// A container for details about the collection of cash to close items.
    /// </summary>
    public partial class LQB_CASH_TO_CLOSE_ITEMS_EXTENSION : OTHER_BASE
    {
        /// <summary>
        /// Gets a value indicating whether the LQB_CASH_TO_CLOSE_ITEMS_EXTENSION container should be serialized.
        /// </summary>
        public override bool ShouldSerialize
        {
            get { return LqbCashToCloseDetail != null && LqbCashToCloseDetail.ShouldSerialize; }
        }

        /// <summary>
        /// A container for details about the collection of cash to close items.
        /// </summary>
        [XmlElement("CASH_TO_CLOSE_DETAIL", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public LQB_CASH_TO_CLOSE_DETAIL LqbCashToCloseDetail;
    }
}
