namespace Mismo3Specification.Version4Schema
{
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets a value indicating whether the payment identifier has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the payment identifier element has a value to serialize.</value>
    public class LQB_ESCROW_ITEM_PAYMENT_EXTENSION : OTHER_BASE
    {
        /// <summary>
        /// Gets a value indicating whether the <see cref="LQB_ESCROW_ITEM_PAYMENT_EXTENSION"/> container should be serialized.
        /// </summary>
        [XmlIgnore]
        public override bool ShouldSerialize
        {
            get
            {
                return this.IdSpecified;
            }
        }

        /// <summary>
        /// A unique identifier for the payment.
        /// </summary>
        [XmlElement("Id", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString Id;

        /// <summary>
        /// Gets a value indicating whether the payment identifier has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the payment identifier element has a value to serialize.</value>
        [XmlIgnore]
        public bool IdSpecified
        {
            get
            {
                return this.Id != null;
            }
        }
    }
}
