﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class LQB_SERVICE_PRODUCT_DETAIL_EXTENSION : OTHER_BASE
    {
        [XmlIgnore]
        public override bool ShouldSerialize
        {
            get
            {
                return this.PreferredResponseFormatTypeSpecified;
            }
        }

        [XmlElement(Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString PreferredResponseFormatType;

        [XmlIgnore]
        public bool PreferredResponseFormatTypeSpecified
        {
            get { return this.PreferredResponseFormatType != null; }
            set { }
        }
    }
}
