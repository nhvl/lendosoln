﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public partial class LQB_HMDA_ETHNICITY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get { return this.HMDAEthnicityTypeSpecified; }
        }

        [XmlElement(nameof(HMDAEthnicityType), Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<HMDAEthnicityBase> HMDAEthnicityType;

        [XmlIgnore]
        public bool HMDAEthnicityTypeSpecified
        {
            get { return this.HMDAEthnicityType != null; }
            set { }
        }
    }
}
