namespace Mismo3Specification.Version4Schema
{
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// The LQB_ESTIMATED_PROPERTY_COST_COMPONENT_EXTENSION container.
    /// </summary>
    public partial class LQB_ESTIMATED_PROPERTY_COST_COMPONENT_EXTENSION : OTHER_BASE
    {
        [XmlIgnore]
        public override bool ShouldSerialize
        {
            get
            {
                return ExpenseDescriptionSpecified || AnnualAmountSpecified;
            }
        }

        /// <summary>
        /// A container for description of the expense. The namespace reference ensures that the $$lendingqb$$ prefix will be applied to the element name on serialization.
        /// </summary>
        [XmlElement("ExpenseDescription", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString ExpenseDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the ExpenseDescription element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the ExpenseDescription element has been assigned a value.</value>
        [XmlIgnore]
        public bool ExpenseDescriptionSpecified
        {
            get { return this.ExpenseDescription != null; }
            set { }
        }

        /// <summary>
        /// A container for the annual payment amount of the expense. The namespace reference ensures that the $$lendingqb$$ prefix will be applied to the element name on serialization.
        /// </summary>
        [XmlElement("AnnualAmount", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOAmount AnnualAmount = null;

        /// <summary>
        /// Gets or sets a value indicating whether the AnnualAmount element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the AnnualAmount element has been assigned a value.</value>
        [XmlIgnore]
        public bool AnnualAmountSpecified
        {
            get { return this.AnnualAmount != null; }
            set { }
        }
    }
}
