﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the GuarantyPercent has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the GuarantyPercent has been assigned a value.</value>
    public partial class LQB_GOVERNMENT_LOAN_EXTENSION : OTHER_BASE
    {
        /// <summary>
        /// Gets a value indicating whether the LQB_GOVERNMENT_LOAN_EXTENSION container should be serialized.
        /// </summary>
        [XmlIgnore]
        override public bool ShouldSerialize
        {
            get
            {
                return FHA_VALoanPurposeTypeSpecified ||
                    GovernmentAnnualPremiumAmountSpecified ||
                    GovernmentAnnualPremiumPercentSpecified ||
                    GovernmentUpfrontPremiumAmountSpecified ||
                    GovernmentUpfrontPremiumPercentSpecified ||
                    GuarantyAmountSpecified ||
                    GuarantyPercentSpecified;
            }
        }

        /// <summary>
        /// Represents the purpose type of this loan as it applies to FHA and VA loans.
        /// </summary>
        [XmlElement("FHAVALoanPurposeType", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<FHA_VALoanPurposeBase> FHA_VALoanPurposeType;

        /// <summary>
        /// Gets or sets a value indicating whether the FHA_VALoanPurposeType enum has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the FHA_VALoanPurposeType enum has been assigned a value other than Blank.</value>
        [XmlIgnore]
        public bool FHA_VALoanPurposeTypeSpecified
        {
            get
            {
                return this.FHA_VALoanPurposeType != null && FHA_VALoanPurposeType.EnumValue != FHA_VALoanPurposeBase.Blank;
            }
        }

        /// <summary>
        /// The annual Mortgage Insurance premium amount. Used by Ginnie Mae in the PDD.
        /// </summary>
        [XmlElement("GovernmentAnnualPremiumAmount")]
        public MISMOAmount GovernmentAnnualPremiumAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the GovernmentAnnualPremiumAmount has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GovernmentAnnualPremiumAmount has been assigned a value.</value>
        [XmlIgnore]
        public bool GovernmentAnnualPremiumAmountSpecified
        {
            get
            {
                return this.GovernmentAnnualPremiumAmount != null;
            }
        }

        /// <summary>
        /// The annual Mortgage Insurance premium as a percent of the loan amount. Used by Ginnie Mae in the PDD.
        /// </summary>
        [XmlElement("GovernmentAnnualPremiumPercent")]
        public MISMOPercent GovernmentAnnualPremiumPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the GovernmentAnnualPremiumPercent has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GovernmentAnnualPremiumPercent has been assigned a value.</value>
        [XmlIgnore]
        public bool GovernmentAnnualPremiumPercentSpecified
        {
            get
            {
                return this.GovernmentAnnualPremiumPercent != null;
            }
        }

        /// <summary>
        /// The upfront Mortgage Insurance premium amount. Used by Ginnie Mae in the PDD.
        /// </summary>
        [XmlElement("GovernmentUpfrontPremiumAmount")]
        public MISMOAmount GovernmentUpfrontPremiumAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the GovernmentUpfrontPremiumAmount has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GovernmentUpfrontPremiumAmount has been assigned a value.</value>
        [XmlIgnore]
        public bool GovernmentUpfrontPremiumAmountSpecified
        {
            get
            {
                return this.GovernmentUpfrontPremiumAmount != null;
            }
        }

        /// <summary>
        /// The upfront Mortgage Insurance premium as a percent of the loan amount. Used by Ginnie Mae in the PDD.
        /// </summary>
        [XmlElement("GovernmentUpfrontPremiumPercent")]
        public MISMOPercent GovernmentUpfrontPremiumPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the GovernmentUpfrontPremiumPercent has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GovernmentUpfrontPremiumPercent has been assigned a value.</value>
        [XmlIgnore]
        public bool GovernmentUpfrontPremiumPercentSpecified
        {
            get
            {
                return this.GovernmentUpfrontPremiumPercent != null;
            }
        }

        /// <summary>
        /// The dollar amount  of the original loan guaranteed by the government agency in the event of loss due to foreclosure. Used by Ginnie Mae in the PDD.
        /// </summary>
        [XmlElement("GuarantyAmount")]
        public MISMOAmount GuarantyAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the GuarantyAmount has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GuarantyAmount has been assigned a value.</value>
        [XmlIgnore]
        public bool GuarantyAmountSpecified
        {
            get
            {
                return this.GuarantyAmount != null;
            }
        }

        /// <summary>
        /// The percent of the loan amount that the government agency guarantees in the event of loss due to foreclosure. Used by Ginnie Mae in the PDD.
        /// </summary>
        [XmlElement("GuarantyPercent")]
        public MISMOPercent GuarantyPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the GuarantyPercent has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the GuarantyPercent has been assigned a value.</value>
        [XmlIgnore]
        public bool GuarantyPercentSpecified
        {
            get
            {
                return this.GuarantyPercent != null;
            }
        }
    }
}
