namespace Mismo3Specification.Version4Schema
{
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// The LQB_PROJECTED_PAYMENT_EXTENSION container.
    /// </summary>
    public class LQB_PROJECTED_PAYMENT_EXTENSION : OTHER_BASE
    {
        /// <summary>
        /// Gets a value indicating whether the LQB_PROJECTED_PAYMENT_EXTENSION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public override bool ShouldSerialize
        {
            get
            {
                return this.InterestOnlyIndicatorSpecified;
            }
        }

        /// <summary>
        /// Indicates whether the projected payment includes an interest-only payment.
        /// </summary>
        [XmlElement("InterestOnlyIndicator", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOIndicator InterestOnlyIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the InterestOnlyIndicator element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the InterestOnlyIndicator element has a value to serialize.</value>
        [XmlIgnore]
        public bool InterestOnlyIndicatorSpecified
        {
            get
            {
                return this.InterestOnlyIndicator != null;
            }
        }
    }
}
