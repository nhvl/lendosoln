﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class LQB_SERVICE_EXTENSION : OTHER_BASE
    {
        [XmlIgnore]
        public override bool ShouldSerialize
        {
            get
            {
                return this.AdditionalNotesSpecified;
            }
        }

        [XmlElement(Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString AdditionalNotes;

        [XmlIgnore]
        public bool AdditionalNotesSpecified
        {
            get { return this.AdditionalNotes != null; }
            set { }
        }
    }
}
