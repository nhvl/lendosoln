﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public partial class LQB_GOVERNMENT_MONITORING_EXTENSION : OTHER_BASE
    {
        [XmlIgnore]
        public override bool ShouldSerialize
        {
            get
            {
                return this.GenderTypeSpecified ||
                       this.HMDAEthnicitiesSpecified ||
                       this.ApplicationTakenMethodTypeSpecified || 
                       this.DecisionCreditScoreSpecified;
                }
        }

        [XmlElement(nameof(GenderType), Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<HMDAGenderBase> GenderType;

        [XmlIgnore]
        public bool GenderTypeSpecified
        {
            get
            {
                return this.GenderType != null;
            }
            set
            {
            }
        }

        [XmlElement("HMDA_ETHNICITIES", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public LQB_HMDA_ETHNICITIES HMDAEthnicities;

        [XmlIgnore]
        public bool HMDAEthnicitiesSpecified
        {
            get
            {
                return this.HMDAEthnicities != null && this.HMDAEthnicities.ShouldSerialize;
            }
            set
            {
            }
        }

        [XmlElement(nameof(ApplicationTakenMethodType), Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<ApplicationTakenMethodBase> ApplicationTakenMethodType;

        [XmlIgnore]
        public bool ApplicationTakenMethodTypeSpecified
        {
            get { return this.ApplicationTakenMethodType != null; }
            set { }
        }

        /// <summary>
        /// Contains the borrower's decision credit score.
        /// </summary>
        [XmlElement("DECISION_CREDIT_SCORE", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public LQB_DECISION_CREDIT_SCORE DecisionCreditScore;

        /// <summary>
        /// Indicates whether the <see cref="DecisionCreditScore"/> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="DecisionCreditScore"/> element has a value to serialize.</value>
        [XmlIgnore]
        public bool DecisionCreditScoreSpecified
        {
            get { return this.DecisionCreditScore != null; }
            set { }
        }
    }
}
