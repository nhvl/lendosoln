﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// An extension container to hold borrower decision credit score data.
    /// </summary>
    public class LQB_DECISION_CREDIT_SCORE
    {
        /// <summary>
        /// Indicates whether the element should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DecisionCreditScoreValueSpecified
                    || this.DecisionCreditRepositorySourceTypeSpecified;
            }
        }

        /// <summary>
        /// The decision credit score.
        /// </summary>
        [XmlElement("DecisionCreditScoreValue", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOValue DecisionCreditScoreValue;

        /// <summary>
        /// Indicates whether the <see cref="DecisionCreditScoreValue"/> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="DecisionCreditScoreValue"/> element has a value to serialize.</value>
        [XmlIgnore]
        public bool DecisionCreditScoreValueSpecified
        {
            get { return this.DecisionCreditScoreValue != null; }
            set { }
        }

        /// <summary>
        /// The bureau who provided the decision credit score.
        /// </summary>
        [XmlElement("DecisionCreditRepositorySourceType", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<CreditRepositorySourceBase> DecisionCreditRepositorySourceType;

        /// <summary>
        /// Indicates whether the <see cref="DecisionCreditRepositorySourceType"/> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="DecisionCreditRepositorySourceType"/> element has a value to serialize.</value>
        [XmlIgnore]
        public bool DecisionCreditRepositorySourceTypeSpecified
        {
            get { return this.DecisionCreditRepositorySourceType != null; }
            set { }
        }

        /// <summary>
        /// The credit model used to determine the decision score.
        /// </summary>
        [XmlElement("DecisionCreditModelName", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString DecisionCreditModelName;

        /// <summary>
        /// Indicates whether the <see cref="DecisionCreditModelName"/> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="DecisionCreditModelName"/> element has a value to serialize.</value>
        [XmlIgnore]
        public bool DecisionCreditModelNameSpecified
        {
            get { return this.DecisionCreditModelName != null; }
            set { }
        }
    }
}
