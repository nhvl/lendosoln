namespace Mismo3Specification.Version4Schema
{
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Gets or sets a value indicating whether the IncludedInDisclosure element has a value to serialize.
    /// </summary>
    /// <value>A boolean indicating whether the IncludedInDisclosure element has a value to serialize.</value>
    public class LQB_ROLE_DETAIL_EXTENSION : OTHER_BASE
    {
        /// <summary>
        /// Gets a value indicating whether the LQB_ROLE_DETAIL_EXTENSION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public override bool ShouldSerialize
        {
            get
            {
                return this.IncludedInDisclosureSpecified;
            }
        }

        /// <summary>
        /// Indicates whether a party is listed on the Loan Estimate or Closing Disclosure.
        /// </summary>
        [XmlElement("IncludedInDisclosure", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOIndicator IncludedInDisclosure;

        /// <summary>
        /// Gets or sets a value indicating whether the IncludedInDisclosure element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the IncludedInDisclosure element has a value to serialize.</value>
        [XmlIgnore]
        public bool IncludedInDisclosureSpecified
        {
            get
            {
                return this.IncludedInDisclosure != null;
            }
        }
    }
}
