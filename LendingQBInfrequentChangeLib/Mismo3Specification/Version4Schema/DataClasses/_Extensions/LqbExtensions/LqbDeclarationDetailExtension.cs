namespace Mismo3Specification.Version4Schema
{
    using System.Xml;
    using System.Xml.Serialization;
	
    /// <summary>
    /// Extends the <see cref="DECLARATION_DETAIL"/> container
    /// with additional datapoints.
    /// </summary>
	public class LQB_DECLARATION_DETAIL_EXTENSION : OTHER_BASE
	{
        /// <summary>
        /// Gets a value indicating whether the <see cref="LQB_DECLARATION_DETAIL_EXTENSION"/> container should be serialized.
        /// </summary>
        /// <value>Whether to serialize as XML.</value>
        [XmlIgnore]
        public override bool ShouldSerialize
        {
            get
            {
                return this.SpecialBorrowerSellerRelationshipIndicatorSpecified;
            }
        }

        /// <summary>
        /// Gets or sets the indicator for the whether the transaction has a special borrower or seller relationship.
        /// </summary>
        /// <value>Party's agent ID.</value>
        [XmlElement(nameof(SpecialBorrowerSellerRelationshipIndicator), Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOIndicator SpecialBorrowerSellerRelationshipIndicator { get; set; }

        /// <summary>
        /// Gets a value indicating whether the <see cref="AgentId"/> element has a value to serialize.
        /// </summary>
        /// <value>Whether the <see cref="AgentId"/> field should be XML-serialized.</value>
        [XmlIgnore]
        public bool SpecialBorrowerSellerRelationshipIndicatorSpecified
        {
            get
            {
                return this.SpecialBorrowerSellerRelationshipIndicator != null;
            }
        }
    }
}