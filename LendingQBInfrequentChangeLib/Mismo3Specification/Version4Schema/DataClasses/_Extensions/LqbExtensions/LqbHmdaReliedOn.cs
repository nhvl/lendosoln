﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// An extension container to hold HMDA relied-on data.
    /// </summary>
    public class LQB_HMDA_RELIED_ON
    {
        /// <summary>
        /// Indicates whether the element should be serialized.
        /// </summary>
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.TotalAnnualIncomeReliedOnIndicatorSpecified
                    || this.ReliedOnTotalAnnualIncomeAmountSpecified
                    || this.DebtToIncomeRatioReliedOnIndicatorSpecified
                    || this.ReliedOnDebtToIncomeRatioPercentSpecified
                    || this.CombinedLoanToValueRatioReliedOnIndicatorSpecified
                    || this.ReliedOnCombinedLoanToValueRatioPercentSpecified
                    || this.PropertyValueReliedOnIndicatorSpecified
                    || this.ReliedOnPropertyValueAmountSpecified
                    || this.CreditScoreModeReliedOnIndicatorSpecified
                    || this.ReliedOnCreditScoreModeTypeSpecified;
            }
        }

        /// <summary>
        /// Indicates whether there is a relied-on value for Total Annual Income.
        /// </summary>
        [XmlElement("TotalAnnualIncomeReliedOnIndicator", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOIndicator TotalAnnualIncomeReliedOnIndicator;

        /// <summary>
        /// Indicates whether the <see cref="TotalAnnualIncomeReliedOnIndicator"/> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="TotalAnnualIncomeReliedOnIndicator"/> element has a value to serialize.</value>
        [XmlIgnore]
        public bool TotalAnnualIncomeReliedOnIndicatorSpecified
        {
            get { return this.TotalAnnualIncomeReliedOnIndicator != null; }
            set { }
        }

        /// <summary>
        /// The relied-on amount of total annual income.
        /// </summary>
        [XmlElement("ReliedOnTotalAnnualIncomeAmount", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOAmount ReliedOnTotalAnnualIncomeAmount;

        /// <summary>
        /// Indicates whether the <see cref="ReliedOnTotalAnnualIncomeAmount"/> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="ReliedOnTotalAnnualIncomeAmount"/> element has a value to serialize.</value>
        [XmlIgnore]
        public bool ReliedOnTotalAnnualIncomeAmountSpecified
        {
            get { return this.ReliedOnTotalAnnualIncomeAmount != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether there is a relied-on DTI ratio value.
        /// </summary>
        [XmlElement("DebtToIncomeRatioReliedOnIndicator", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOIndicator DebtToIncomeRatioReliedOnIndicator;

        /// <summary>
        /// Indicates whether the <see cref="DebtToIncomeRatioReliedOnIndicator"/> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="DebtToIncomeRatioReliedOnIndicator"/> element has a value to serialize.</value>
        [XmlIgnore]
        public bool DebtToIncomeRatioReliedOnIndicatorSpecified
        {
            get { return this.DebtToIncomeRatioReliedOnIndicator != null; }
            set { }
        }

        /// <summary>
        /// The relied-on DTI ratio value.
        /// </summary>
        [XmlElement("ReliedOnDebtToIncomeRatioPercent", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOPercent ReliedOnDebtToIncomeRatioPercent;

        /// <summary>
        /// Indicates whether the <see cref="ReliedOnDebtToIncomeRatioPercent"/> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="ReliedOnDebtToIncomeRatioPercent"/> element has a value to serialize.</value>
        [XmlIgnore]
        public bool ReliedOnDebtToIncomeRatioPercentSpecified
        {
            get { return this.ReliedOnDebtToIncomeRatioPercent != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether there is a relied-on CLTV value.
        /// </summary>
        [XmlElement("CombinedLoanToValueRatioReliedOnIndicator", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOIndicator CombinedLoanToValueRatioReliedOnIndicator;

        /// <summary>
        /// Indicates whether the <see cref="CombinedLoanToValueRatioReliedOnIndicator"/> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="CombinedLoanToValueRatioReliedOnIndicator"/> element has a value to serialize.</value>
        [XmlIgnore]
        public bool CombinedLoanToValueRatioReliedOnIndicatorSpecified
        {
            get { return this.CombinedLoanToValueRatioReliedOnIndicator != null; }
            set { }
        }

        /// <summary>
        /// The relied-on CLTV value.
        /// </summary>
        [XmlElement("ReliedOnCombinedLoanToValueRatioPercent", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOPercent ReliedOnCombinedLoanToValueRatioPercent;

        /// <summary>
        /// Indicates whether the <see cref="ReliedOnCombinedLoanToValueRatioPercent"/> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="ReliedOnCombinedLoanToValueRatioPercent"/> element has a value to serialize.</value>
        [XmlIgnore]
        public bool ReliedOnCombinedLoanToValueRatioPercentSpecified
        {
            get { return this.ReliedOnCombinedLoanToValueRatioPercent != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether there is a relied-on property value.
        /// </summary>
        [XmlElement("PropertyValueReliedOnIndicator", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOIndicator PropertyValueReliedOnIndicator;

        /// <summary>
        /// Indicates whether the <see cref="PropertyValueReliedOnIndicator"/> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="PropertyValueReliedOnIndicator"/> element has a value to serialize.</value>
        [XmlIgnore]
        public bool PropertyValueReliedOnIndicatorSpecified
        {
            get { return this.PropertyValueReliedOnIndicator != null; }
            set { }
        }

        /// <summary>
        /// The relied-on property value.
        /// </summary>
        [XmlElement("ReliedOnPropertyValueAmount", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOAmount ReliedOnPropertyValueAmount;

        /// <summary>
        /// Indicates whether the <see cref="ReliedOnPropertyValueAmount"/> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="ReliedOnPropertyValueAmount"/> element has a value to serialize.</value>
        [XmlIgnore]
        public bool ReliedOnPropertyValueAmountSpecified
        {
            get { return this.ReliedOnPropertyValueAmount != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether there is a relied-on credit score mode.
        /// </summary>
        [XmlElement("CreditScoreModeReliedOnIndicator", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOIndicator CreditScoreModeReliedOnIndicator;

        /// <summary>
        /// Indicates whether the <see cref="CreditScoreModeReliedOnIndicator"/> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="CreditScoreModeReliedOnIndicator"/> element has a value to serialize.</value>
        [XmlIgnore]
        public bool CreditScoreModeReliedOnIndicatorSpecified
        {
            get { return this.CreditScoreModeReliedOnIndicator != null; }
            set { }
        }

        /// <summary>
        /// The relied-on credit score mode.
        /// </summary>
        [XmlElement("ReliedOnCreditScoreModeType", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbReliedOnCreditScoreModeBase> ReliedOnCreditScoreModeType;

        /// <summary>
        /// Indicates whether the <see cref="ReliedOnCreditScoreModeType"/> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="ReliedOnCreditScoreModeType"/> element has a value to serialize.</value>
        [XmlIgnore]
        public bool ReliedOnCreditScoreModeTypeSpecified
        {
            get { return this.ReliedOnCreditScoreModeType != null; }
            set { }
        }
    }
}
