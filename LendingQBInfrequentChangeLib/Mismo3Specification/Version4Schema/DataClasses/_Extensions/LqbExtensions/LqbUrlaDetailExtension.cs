namespace Mismo3Specification.Version4Schema
{
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// The LQB_URLA_DETAIL_EXTENSION container.
    /// </summary>
    public class LQB_URLA_DETAIL_EXTENSION : OTHER_BASE
    {
        /// <summary>
        /// Gets a value indicaiting whether the LQB_URLA_DETAIL_EXTENSION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public override bool ShouldSerialize
        {
            get
            {
                return this.MannerInWhichTitleHeldSpecified
                    || this.LandIfAcquiredSeparatelyAmountSpecified;
            }
        }

        /// <summary>
        /// A string describing how the title will be held.
        /// </summary>
        [XmlElement("MannerInWhichTitleHeld", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString MannerInWhichTitleHeld;

        /// <summary>
        /// Gets a value indicating whether MannerInWhichTitleHeld has a value set.
        /// </summary>
        [XmlIgnore]
        public bool MannerInWhichTitleHeldSpecified
        {
            get
            {
                return MannerInWhichTitleHeld != null;
            }
        }


        /// <summary>
        /// Land if acquired separately amount.
        /// </summary>
        [XmlElement("LandIfAcquiredSeparatelyAmount", Namespace = "http://www.lendingqb.com")]
        public MISMOAmount LandIfAcquiredSeparatelyAmount;

        /// <summary>
        /// Gets a value indicating whether LandIfAcquiredSeparatelyAmount is specified.
        /// </summary>
        [XmlIgnore]
        public bool LandIfAcquiredSeparatelyAmountSpecified
        {
            get
            {
                return LandIfAcquiredSeparatelyAmount != null;
            }
        }
    }
}
