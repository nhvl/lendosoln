﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Defines an LQB-specific extension to the PAID_TO elements.
    /// </summary>
    public class LQB_PAID_TO_EXTENSION : OTHER_BASE
    {
        /// <summary>
        /// Gets a value indicating whether the <see cref="LQB_PAID_TO_EXTENSION"/> container should be serialized.
        /// </summary>
        /// <value>Whether to serialize as XML.</value>
        [XmlIgnore]
        public override bool ShouldSerialize
        {
            get
            {
                return this.BeneficiaryAgentIdSpecified;
            }
        }

        /// <summary>
        /// Gets or sets the LQB Agent ID of the beneficiary of the PAID_TO element.
        /// </summary>
        /// <value>ID of the beneficiary of a fee.</value> 
        [XmlElement(nameof(BeneficiaryAgentId), Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOIdentifier BeneficiaryAgentId { get; set; }

        /// <summary>
        /// Gets a value indicating whether the <see cref="BeneficiaryAgentId"/> element has a value to serialize.
        /// </summary>
        /// <value>Whether the <see cref="BeneficiaryAgentId"/> field should be XML-serialized.</value>
        [XmlIgnore]
        public bool BeneficiaryAgentIdSpecified
        {
            get
            {
                return this.BeneficiaryAgentId != null;
            }
        }
    }
}