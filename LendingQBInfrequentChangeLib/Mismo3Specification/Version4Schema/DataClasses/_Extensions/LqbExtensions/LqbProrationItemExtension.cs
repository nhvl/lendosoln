﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class LQB_PRORATION_ITEM_EXTENSION : OTHER_BASE
    {
        /// <summary>
        /// Gets a value indicating whether the element should be serialized.
        /// </summary>
        [XmlIgnore]
        public override bool ShouldSerialize
        {
            get
            {
                return this.ExcludeFromLECDForThisLienSpecified;
            }
        }

        /// <summary>
        /// Indicates whether the closing adjustment should be excluded from the LE/CD.
        /// </summary>
        [XmlElement("ExcludeFromLECDForThisLien", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOIndicator ExcludeFromLECDForThisLien;

        /// <summary>
        /// Gets or sets a value indicating whether the ExcludeFromLECDForThisLien element has a value to serialize.
        /// </summary>
        [XmlIgnore]
        public bool ExcludeFromLECDForThisLienSpecified
        {
            get { return this.ExcludeFromLECDForThisLien != null; }
        }
    }
}
