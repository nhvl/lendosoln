﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public partial class LQB_HMDA_LAR_DATA
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.GlobalLegalEntityIdentifierSpecified
                    || this.LoanIdentifierSpecified
                    || this.ApplicationReceivedDateSpecified
                    || this.MortgageTypeSpecified
                    || this.HMDAPurposeOfLoanTypeSpecified
                    || this.HMDAPreapprovalTypeSpecified
                    || this.ConstructionMethodTypeSpecified
                    || this.PropertyUsageTypeSpecified
                    || this.BorrowerRequestedLoanAmountSpecified
                    || this.HMDADispositionTypeSpecified
                    || this.HMDADispositionDateSpecified
                    || this.AddressLineTextSpecified
                    || this.CityNameSpecified
                    || this.StateCodeSpecified
                    || this.PostalCodeSpecified
                    || this.FIPSCountyCodeSpecified
                    || this.CensusTractIdentifierSpecified
                    || this.BorrowerHMDAEthnicityType1Specified
                    || this.BorrowerHMDAEthnicityType2Specified
                    || this.BorrowerHMDAEthnicityType3Specified
                    || this.BorrowerHMDAEthnicityType4Specified
                    || this.BorrowerHMDAEthnicityType5Specified
                    || this.BorrowerHMDAEthnicityTypeOtherDescriptionSpecified
                    || this.CoborrowerHMDAEthnicityType1Specified
                    || this.CoborrowerHMDAEthnicityType2Specified
                    || this.CoborrowerHMDAEthnicityType3Specified
                    || this.CoborrowerHMDAEthnicityType4Specified
                    || this.CoborrowerHMDAEthnicityType5Specified
                    || this.CoborrowerHMDAEthnicityTypeOtherDescriptionSpecified
                    || this.BorrowerHMDAEthnicityCollectedBasedOnVisualObservationOrSurnameTypeSpecified
                    || this.CoborrowerHMDAEthnicityCollectedBasedOnVisualObservationOrSurnameTypeSpecified
                    || this.BorrowerHMDARaceType1Specified
                    || this.BorrowerHMDARaceType2Specified
                    || this.BorrowerHMDARaceType3Specified
                    || this.BorrowerHMDARaceType4Specified
                    || this.BorrowerHMDARaceType5Specified
                    || this.BorrowerHMDAEnrolledOrPrincipalTribeSpecified
                    || this.BorrowerHMDARaceDesignationTypeOtherAsianDescriptionSpecified
                    || this.BorrowerHMDARaceDesignationTypeOtherPacificIslanderDescriptionSpecified
                    || this.CoborrowerHMDARaceType1Specified
                    || this.CoborrowerHMDARaceType2Specified
                    || this.CoborrowerHMDARaceType3Specified
                    || this.CoborrowerHMDARaceType4Specified
                    || this.CoborrowerHMDARaceType5Specified
                    || this.CoborrowerHMDAEnrolledOrPrincipalTribeSpecified
                    || this.CoborrowerHMDARaceDesignationTypeOtherAsianDescriptionSpecified
                    || this.CoborrowerHMDARaceDesignationTypeOtherPacificIslanderDescriptionSpecified
                    || this.BorrowerHMDARaceCollectedBasedOnVisualObservationOrSurnameTypeSpecified
                    || this.CoborrowerHMDARaceCollectedBasedOnVisualObservationOrSurnameTypeSpecified
                    || this.BorrowerHMDAGenderTypeSpecified
                    || this.CoborrowerHMDAGenderTypeSpecified
                    || this.BorrowerHMDAGenderCollectedBasedOnVisualObservationOrSurnameTypeSpecified
                    || this.CoborrowerHMDAGenderCollectedBasedOnVisualObservationOrSurnameTypeSpecified
                    || this.BorrowerAgeAtApplicationYearsCountSpecified
                    || this.CoborrowerAgeAtApplicationYearsCountSpecified
                    || this.TotalMonthlyIncomeAmountSpecified
                    || this.HMDAPurchaserTypeSpecified
                    || this.HMDARateSpreadPercentSpecified
                    || this.HMDAHOEPALoanStatusTypeSpecified
                    || this.LienPriorityTypeSpecified
                    || this.BorrowerCreditScoreValueSpecified
                    || this.BorrowerCreditScoreModelNameTypeSpecified
                    || this.BorrowerCreditScoreModelNameTypeOtherDescriptionSpecified
                    || this.CoborrowerCreditScoreValueSpecified
                    || this.CoborrowerCreditScoreModelNameTypeSpecified
                    || this.CoborrowerCreditScoreModelNameTypeOtherDescriptionSpecified
                    || this.HMDAReasonForDenialType1Specified
                    || this.HMDAReasonForDenialType2Specified
                    || this.HMDAReasonForDenialType3Specified
                    || this.HMDAReasonForDenialType4Specified
                    || this.ReasonForDenialTypeOtherDescriptionSpecified
                    || this.TotalLoanCostsAmountSpecified
                    || this.TotalPointsAndFeesAmountSpecified
                    || this.OriginationChargesAmountSpecified
                    || this.DiscountPointsAmountSpecified
                    || this.LenderCreditsAmountSpecified
                    || this.NoteRatePercentSpecified
                    || this.PrepaymentPenaltyExpirationMonthsCountSpecified
                    || this.TotalDebtExpenseRatioPercentSpecified
                    || this.CombinedLTVRatioPercentSpecified
                    || this.LoanMaturityPeriodCountSpecified
                    || this.FirstRateChangeMonthsCountSpecified
                    || this.BalloonPaymentIndicatorSpecified
                    || this.InterestOnlyIndicatorSpecified
                    || this.NegativeAmortizationIndicatorSpecified
                    || this.HMDAOtherNonAmortizingFeaturesIndicatorSpecified
                    || this.PropertyValuationAmountSpecified
                    || this.ManufacturedHomeSecuredPropertyTypeSpecified
                    || this.ManufacturedHomeLandPropertyInterestTypeSpecified
                    || this.FinancedUnitCountSpecified
                    || this.AffordableUnitsCountSpecified
                    || this.HMDAApplicationSubmissionTypeSpecified
                    || this.HMDACoveredLoanInitiallyPayableToReportingInstitutionStatusTypeSpecified
                    || this.LoanOriginatorNMLSIdentifierSpecified
                    || this.AutomatedUnderwritingSystemType1Specified
                    || this.AutomatedUnderwritingSystemType2Specified
                    || this.AutomatedUnderwritingSystemType3Specified
                    || this.AutomatedUnderwritingSystemType4Specified
                    || this.AutomatedUnderwritingSystemType5Specified
                    || this.AutomatedUnderwritingSystemTypeOtherDescriptionSpecified
                    || this.AutomatedUnderwritingSystemResultType1Specified
                    || this.AutomatedUnderwritingSystemResultType2Specified
                    || this.AutomatedUnderwritingSystemResultType3Specified
                    || this.AutomatedUnderwritingSystemResultType4Specified
                    || this.AutomatedUnderwritingSystemResultType5Specified
                    || this.AutomatedUnderwritingSystemResultTypeOtherDescriptionSpecified
                    || this.ReverseMortgageIndicatorSpecified
                    || this.OpenEndCreditIndicatorSpecified
                    || this.HMDABusinessPurposeIndicatorSpecified;
            }
        }

        /// <summary>
        /// The global legal entity identifier (LEI).
        /// </summary>
        [XmlElement("GlobalLegalEntityIdentifier", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString GlobalLegalEntityIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="GlobalLegalEntityIdentifier" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="GlobalLegalEntityIdentifier" /> element has a value to serialize.
        [XmlIgnore]
        public bool GlobalLegalEntityIdentifierSpecified
        {
            get { return GlobalLegalEntityIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The universal loan identifier (ULI).
        /// </summary>
        [XmlElement("LoanIdentifier", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString LoanIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="LoanIdentifier" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="LoanIdentifier" /> element has a value to serialize.
        [XmlIgnore]
        public bool LoanIdentifierSpecified
        {
            get { return LoanIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The date the application was received.
        /// </summary>
        [XmlElement("ApplicationReceivedDate", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString ApplicationReceivedDate;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="ApplicationReceivedDate" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="ApplicationReceivedDate" /> element has a value to serialize.
        [XmlIgnore]
        public bool ApplicationReceivedDateSpecified
        {
            get { return ApplicationReceivedDate != null; }
            set { }
        }

        /// <summary>
        /// The loan type.
        /// </summary>
        [XmlElement("MortgageType", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<MortgageBase> MortgageType;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="MortgageType" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="MortgageType" /> element has a value to serialize.
        [XmlIgnore]
        public bool MortgageTypeSpecified
        {
            get { return MortgageType != null; }
            set { }
        }

        /// <summary>
        /// The purpose of the loan.
        /// </summary>
        [XmlElement("HMDAPurposeOfLoanType", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<Mismo3Specification.Version4Schema.HMDAPurposeOfLoanBase> HMDAPurposeOfLoanType;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="HMDAPurposeOfLoanType" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="HMDAPurposeOfLoanType" /> element has a value to serialize.
        [XmlIgnore]
        public bool HMDAPurposeOfLoanTypeSpecified
        {
            get { return HMDAPurposeOfLoanType != null; }
            set { }
        }

        /// <summary>
        /// The preapproval type.
        /// </summary>
        [XmlElement("HMDAPreapprovalType", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<HMDAPreapprovalBase> HMDAPreapprovalType;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="HMDAPreapprovalType" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="HMDAPreapprovalType" /> element has a value to serialize.
        [XmlIgnore]
        public bool HMDAPreapprovalTypeSpecified
        {
            get { return HMDAPreapprovalType != null; }
            set { }
        }

        /// <summary>
        /// The construction method of the property.
        /// </summary>
        [XmlElement("ConstructionMethodType", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<ConstructionMethodBase> ConstructionMethodType;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="ConstructionMethodType" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="ConstructionMethodType" /> element has a value to serialize.
        [XmlIgnore]
        public bool ConstructionMethodTypeSpecified
        {
            get { return ConstructionMethodType != null; }
            set { }
        }

        /// <summary>
        /// The intended occupancy type of the property.
        /// </summary>
        [XmlElement("PropertyUsageType", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<PropertyUsageBase> PropertyUsageType;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="PropertyUsageType" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="PropertyUsageType" /> element has a value to serialize.
        [XmlIgnore]
        public bool PropertyUsageTypeSpecified
        {
            get { return PropertyUsageType != null; }
            set { }
        }

        /// <summary>
        /// The total loan amount.
        /// </summary>
        [XmlElement("BorrowerRequestedLoanAmount", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString BorrowerRequestedLoanAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="BorrowerRequestedLoanAmount" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="BorrowerRequestedLoanAmount" /> element has a value to serialize.
        [XmlIgnore]
        public bool BorrowerRequestedLoanAmountSpecified
        {
            get { return BorrowerRequestedLoanAmount != null; }
            set { }
        }

        /// <summary>
        /// The HMDA action taken.
        /// </summary>
        [XmlElement("HMDADispositionType", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<Mismo3Specification.Version4Schema.HMDADispositionBase> HMDADispositionType;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="HMDADispositionType" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="HMDADispositionType" /> element has a value to serialize.
        [XmlIgnore]
        public bool HMDADispositionTypeSpecified
        {
            get { return HMDADispositionType != null; }
            set { }
        }

        /// <summary>
        /// The date of the HMDA action.
        /// </summary>
        [XmlElement("HMDADispositionDate", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString HMDADispositionDate;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="HMDADispositionDate" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="HMDADispositionDate" /> element has a value to serialize.
        [XmlIgnore]
        public bool HMDADispositionDateSpecified
        {
            get { return HMDADispositionDate != null; }
            set { }
        }

        /// <summary>
        /// The subject property address.
        /// </summary>
        [XmlElement("AddressLineText", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString AddressLineText;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="AddressLineText" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="AddressLineText" /> element has a value to serialize.
        [XmlIgnore]
        public bool AddressLineTextSpecified
        {
            get { return AddressLineText != null; }
            set { }
        }

        /// <summary>
        /// The subject property city.
        /// </summary>
        [XmlElement("CityName", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString CityName;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="CityName" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="CityName" /> element has a value to serialize.
        [XmlIgnore]
        public bool CityNameSpecified
        {
            get { return CityName != null; }
            set { }
        }

        /// <summary>
        /// The subject property state.
        /// </summary>
        [XmlElement("StateCode", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString StateCode;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="StateCode" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="StateCode" /> element has a value to serialize.
        [XmlIgnore]
        public bool StateCodeSpecified
        {
            get { return StateCode != null; }
            set { }
        }

        /// <summary>
        /// The subject property ZIP code.
        /// </summary>
        [XmlElement("PostalCode", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString PostalCode;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="PostalCode" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="PostalCode" /> element has a value to serialize.
        [XmlIgnore]
        public bool PostalCodeSpecified
        {
            get { return PostalCode != null; }
            set { }
        }

        /// <summary>
        /// The subject property county code.
        /// </summary>
        [XmlElement("FIPSCountyCode", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString FIPSCountyCode;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="FIPSCountyCode" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="FIPSCountyCode" /> element has a value to serialize.
        [XmlIgnore]
        public bool FIPSCountyCodeSpecified
        {
            get { return FIPSCountyCode != null; }
            set { }
        }

        /// <summary>
        /// The subject property census tract.
        /// </summary>
        [XmlElement("CensusTractIdentifier", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString CensusTractIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="CensusTractIdentifier" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="CensusTractIdentifier" /> element has a value to serialize.
        [XmlIgnore]
        public bool CensusTractIdentifierSpecified
        {
            get { return CensusTractIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The first ethnicity of the borrower.
        /// </summary>
        [XmlElement("BorrowerHMDAEthnicityType1", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbHmdaEthnicityAndOriginBase> BorrowerHMDAEthnicityType1;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="BorrowerHMDAEthnicityType1" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="BorrowerHMDAEthnicityType1" /> element has a value to serialize.
        [XmlIgnore]
        public bool BorrowerHMDAEthnicityType1Specified
        {
            get { return BorrowerHMDAEthnicityType1 != null; }
            set { }
        }

        /// <summary>
        /// The second ethnicity of the borrower.
        /// </summary>
        [XmlElement("BorrowerHMDAEthnicityType2", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbHmdaEthnicityAndOriginBase> BorrowerHMDAEthnicityType2;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="BorrowerHMDAEthnicityType2" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="BorrowerHMDAEthnicityType2" /> element has a value to serialize.
        [XmlIgnore]
        public bool BorrowerHMDAEthnicityType2Specified
        {
            get { return BorrowerHMDAEthnicityType2 != null; }
            set { }
        }

        /// <summary>
        /// The third ethnicity of the borrower.
        /// </summary>
        [XmlElement("BorrowerHMDAEthnicityType3", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbHmdaEthnicityAndOriginBase> BorrowerHMDAEthnicityType3;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="BorrowerHMDAEthnicityType3" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="BorrowerHMDAEthnicityType3" /> element has a value to serialize.
        [XmlIgnore]
        public bool BorrowerHMDAEthnicityType3Specified
        {
            get { return BorrowerHMDAEthnicityType3 != null; }
            set { }
        }

        /// <summary>
        /// The fourth ethnicity type of the borrower.
        /// </summary>
        [XmlElement("BorrowerHMDAEthnicityType4", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbHmdaEthnicityAndOriginBase> BorrowerHMDAEthnicityType4;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="BorrowerHMDAEthnicityType4" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="BorrowerHMDAEthnicityType4" /> element has a value to serialize.
        [XmlIgnore]
        public bool BorrowerHMDAEthnicityType4Specified
        {
            get { return BorrowerHMDAEthnicityType4 != null; }
            set { }
        }

        /// <summary>
        /// The fifth ethnicity type of the borrower.
        /// </summary>
        [XmlElement("BorrowerHMDAEthnicityType5", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbHmdaEthnicityAndOriginBase> BorrowerHMDAEthnicityType5;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="BorrowerHMDAEthnicityType5" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="BorrowerHMDAEthnicityType5" /> element has a value to serialize.
        [XmlIgnore]
        public bool BorrowerHMDAEthnicityType5Specified
        {
            get { return BorrowerHMDAEthnicityType5 != null; }
            set { }
        }

        /// <summary>
        /// The other description for the borrower's ethnicity.
        /// </summary>
        [XmlElement("BorrowerHMDAEthnicityTypeOtherDescription", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString BorrowerHMDAEthnicityTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="BorrowerHMDAEthnicityTypeOtherDescription" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="BorrowerHMDAEthnicityTypeOtherDescription" /> element has a value to serialize.
        [XmlIgnore]
        public bool BorrowerHMDAEthnicityTypeOtherDescriptionSpecified
        {
            get { return BorrowerHMDAEthnicityTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The coborrower's first ethnicity type.
        /// </summary>
        [XmlElement("CoborrowerHMDAEthnicityType1", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbHmdaEthnicityAndOriginBase> CoborrowerHMDAEthnicityType1;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="CoborrowerHMDAEthnicityType1" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="CoborrowerHMDAEthnicityType1" /> element has a value to serialize.
        [XmlIgnore]
        public bool CoborrowerHMDAEthnicityType1Specified
        {
            get { return CoborrowerHMDAEthnicityType1 != null; }
            set { }
        }

        /// <summary>
        /// The coborrower's second ethnicity type.
        /// </summary>
        [XmlElement("CoborrowerHMDAEthnicityType2", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbHmdaEthnicityAndOriginBase> CoborrowerHMDAEthnicityType2;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="CoborrowerHMDAEthnicityType2" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="CoborrowerHMDAEthnicityType2" /> element has a value to serialize.
        [XmlIgnore]
        public bool CoborrowerHMDAEthnicityType2Specified
        {
            get { return CoborrowerHMDAEthnicityType2 != null; }
            set { }
        }

        /// <summary>
        /// The coborrower's third ethnicity type.
        /// </summary>
        [XmlElement("CoborrowerHMDAEthnicityType3", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbHmdaEthnicityAndOriginBase> CoborrowerHMDAEthnicityType3;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="CoborrowerHMDAEthnicityType3" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="CoborrowerHMDAEthnicityType3" /> element has a value to serialize.
        [XmlIgnore]
        public bool CoborrowerHMDAEthnicityType3Specified
        {
            get { return CoborrowerHMDAEthnicityType3 != null; }
            set { }
        }

        /// <summary>
        /// The coborrower's fourth ethnicity type.
        /// </summary>
        [XmlElement("CoborrowerHMDAEthnicityType4", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbHmdaEthnicityAndOriginBase> CoborrowerHMDAEthnicityType4;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="CoborrowerHMDAEthnicityType4" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="CoborrowerHMDAEthnicityType4" /> element has a value to serialize.
        [XmlIgnore]
        public bool CoborrowerHMDAEthnicityType4Specified
        {
            get { return CoborrowerHMDAEthnicityType4 != null; }
            set { }
        }

        /// <summary>
        /// The coborrower's fifth ethnicity type.
        /// </summary>
        [XmlElement("CoborrowerHMDAEthnicityType5", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbHmdaEthnicityAndOriginBase> CoborrowerHMDAEthnicityType5;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="CoborrowerHMDAEthnicityType5" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="CoborrowerHMDAEthnicityType5" /> element has a value to serialize.
        [XmlIgnore]
        public bool CoborrowerHMDAEthnicityType5Specified
        {
            get { return CoborrowerHMDAEthnicityType5 != null; }
            set { }
        }

        /// <summary>
        /// The other description for the coborrower's ethnicity.
        /// </summary>
        [XmlElement("CoborrowerHMDAEthnicityTypeOtherDescription", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString CoborrowerHMDAEthnicityTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="CoborrowerHMDAEthnicityTypeOtherDescription" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="CoborrowerHMDAEthnicityTypeOtherDescription" /> element has a value to serialize.
        [XmlIgnore]
        public bool CoborrowerHMDAEthnicityTypeOtherDescriptionSpecified
        {
            get { return CoborrowerHMDAEthnicityTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the borrower ethnicity was collected by visual observation or surname.
        /// </summary>
        [XmlElement("BorrowerHMDAEthnicityCollectedBasedOnVisualObservationOrSurnameType", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbHmdaCollectedBasedOnVisualObservationOrSurnameBase> BorrowerHMDAEthnicityCollectedBasedOnVisualObservationOrSurnameType;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="BorrowerHMDAEthnicityCollectedBasedOnVisualObservationOrSurnameType" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="BorrowerHMDAEthnicityCollectedBasedOnVisualObservationOrSurnameType" /> element has a value to serialize.
        [XmlIgnore]
        public bool BorrowerHMDAEthnicityCollectedBasedOnVisualObservationOrSurnameTypeSpecified
        {
            get { return BorrowerHMDAEthnicityCollectedBasedOnVisualObservationOrSurnameType != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the coborrower ethnicity was collected by visual observation or surname.
        /// </summary>
        [XmlElement("CoborrowerHMDAEthnicityCollectedBasedOnVisualObservationOrSurnameType", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbHmdaCollectedBasedOnVisualObservationOrSurnameBase> CoborrowerHMDAEthnicityCollectedBasedOnVisualObservationOrSurnameType;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="CoborrowerHMDAEthnicityCollectedBasedOnVisualObservationOrSurnameType" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="CoborrowerHMDAEthnicityCollectedBasedOnVisualObservationOrSurnameType" /> element has a value to serialize.
        [XmlIgnore]
        public bool CoborrowerHMDAEthnicityCollectedBasedOnVisualObservationOrSurnameTypeSpecified
        {
            get { return CoborrowerHMDAEthnicityCollectedBasedOnVisualObservationOrSurnameType != null; }
            set { }
        }

        /// <summary>
        /// The borrower's first race type.
        /// </summary>
        [XmlElement("BorrowerHMDARaceType1", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbHmdaRaceAndDesignationBase> BorrowerHMDARaceType1;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="BorrowerHMDARaceType1" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="BorrowerHMDARaceType1" /> element has a value to serialize.
        [XmlIgnore]
        public bool BorrowerHMDARaceType1Specified
        {
            get { return BorrowerHMDARaceType1 != null; }
            set { }
        }

        /// <summary>
        /// The borrower's second race type.
        /// </summary>
        [XmlElement("BorrowerHMDARaceType2", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbHmdaRaceAndDesignationBase> BorrowerHMDARaceType2;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="BorrowerHMDARaceType2" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="BorrowerHMDARaceType2" /> element has a value to serialize.
        [XmlIgnore]
        public bool BorrowerHMDARaceType2Specified
        {
            get { return BorrowerHMDARaceType2 != null; }
            set { }
        }

        /// <summary>
        /// The borrower's third race type.
        /// </summary>
        [XmlElement("BorrowerHMDARaceType3", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbHmdaRaceAndDesignationBase> BorrowerHMDARaceType3;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="BorrowerHMDARaceType3" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="BorrowerHMDARaceType3" /> element has a value to serialize.
        [XmlIgnore]
        public bool BorrowerHMDARaceType3Specified
        {
            get { return BorrowerHMDARaceType3 != null; }
            set { }
        }

        /// <summary>
        /// The borrower's fourth race type.
        /// </summary>
        [XmlElement("BorrowerHMDARaceType4", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbHmdaRaceAndDesignationBase> BorrowerHMDARaceType4;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="BorrowerHMDARaceType4" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="BorrowerHMDARaceType4" /> element has a value to serialize.
        [XmlIgnore]
        public bool BorrowerHMDARaceType4Specified
        {
            get { return BorrowerHMDARaceType4 != null; }
            set { }
        }

        /// <summary>
        /// The borrower's fifth race type.
        /// </summary>
        [XmlElement("BorrowerHMDARaceType5", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbHmdaRaceAndDesignationBase> BorrowerHMDARaceType5;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="BorrowerHMDARaceType5" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="BorrowerHMDARaceType5" /> element has a value to serialize.
        [XmlIgnore]
        public bool BorrowerHMDARaceType5Specified
        {
            get { return BorrowerHMDARaceType5 != null; }
            set { }
        }

        /// <summary>
        /// The borrower's enrolled or principal tribe if they are Native American.
        /// </summary>
        [XmlElement("BorrowerHMDAEnrolledOrPrincipalTribe", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString BorrowerHMDAEnrolledOrPrincipalTribe;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="BorrowerHMDAEnrolledOrPrincipalTribe" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="BorrowerHMDAEnrolledOrPrincipalTribe" /> element has a value to serialize.
        [XmlIgnore]
        public bool BorrowerHMDAEnrolledOrPrincipalTribeSpecified
        {
            get { return BorrowerHMDAEnrolledOrPrincipalTribe != null; }
            set { }
        }

        /// <summary>
        /// Any uncaptured race designation types if the borrower is Asian.
        /// </summary>
        [XmlElement("BorrowerHMDARaceDesignationTypeOtherAsianDescription", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString BorrowerHMDARaceDesignationTypeOtherAsianDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="BorrowerHMDARaceDesignationTypeOtherAsianDescription" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="BorrowerHMDARaceDesignationTypeOtherAsianDescription" /> element has a value to serialize.
        [XmlIgnore]
        public bool BorrowerHMDARaceDesignationTypeOtherAsianDescriptionSpecified
        {
            get { return BorrowerHMDARaceDesignationTypeOtherAsianDescription != null; }
            set { }
        }

        /// <summary>
        /// Any uncaptured race designation types if the borrower is a Pacific Islander.
        /// </summary>
        [XmlElement("BorrowerHMDARaceDesignationTypeOtherPacificIslanderDescription", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString BorrowerHMDARaceDesignationTypeOtherPacificIslanderDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="BorrowerHMDARaceDesignationTypeOtherPacificIslanderDescription" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="BorrowerHMDARaceDesignationTypeOtherPacificIslanderDescription" /> element has a value to serialize.
        [XmlIgnore]
        public bool BorrowerHMDARaceDesignationTypeOtherPacificIslanderDescriptionSpecified
        {
            get { return BorrowerHMDARaceDesignationTypeOtherPacificIslanderDescription != null; }
            set { }
        }

        /// <summary>
        /// The coborrower's first race type.
        /// </summary>
        [XmlElement("CoborrowerHMDARaceType1", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbHmdaRaceAndDesignationBase> CoborrowerHMDARaceType1;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="CoborrowerHMDARaceType1" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="CoborrowerHMDARaceType1" /> element has a value to serialize.
        [XmlIgnore]
        public bool CoborrowerHMDARaceType1Specified
        {
            get { return CoborrowerHMDARaceType1 != null; }
            set { }
        }

        /// <summary>
        /// The coborrower's second race type.
        /// </summary>
        [XmlElement("CoborrowerHMDARaceType2", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbHmdaRaceAndDesignationBase> CoborrowerHMDARaceType2;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="CoborrowerHMDARaceType2" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="CoborrowerHMDARaceType2" /> element has a value to serialize.
        [XmlIgnore]
        public bool CoborrowerHMDARaceType2Specified
        {
            get { return CoborrowerHMDARaceType2 != null; }
            set { }
        }

        /// <summary>
        /// The coborrower's third race type.
        /// </summary>
        [XmlElement("CoborrowerHMDARaceType3", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbHmdaRaceAndDesignationBase> CoborrowerHMDARaceType3;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="CoborrowerHMDARaceType3" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="CoborrowerHMDARaceType3" /> element has a value to serialize.
        [XmlIgnore]
        public bool CoborrowerHMDARaceType3Specified
        {
            get { return CoborrowerHMDARaceType3 != null; }
            set { }
        }

        /// <summary>
        /// The coborrower's fourth race type.
        /// </summary>
        [XmlElement("CoborrowerHMDARaceType4", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbHmdaRaceAndDesignationBase> CoborrowerHMDARaceType4;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="CoborrowerHMDARaceType4" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="CoborrowerHMDARaceType4" /> element has a value to serialize.
        [XmlIgnore]
        public bool CoborrowerHMDARaceType4Specified
        {
            get { return CoborrowerHMDARaceType4 != null; }
            set { }
        }

        /// <summary>
        /// The coborrower's fifth race type.
        /// </summary>
        [XmlElement("CoborrowerHMDARaceType5", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbHmdaRaceAndDesignationBase> CoborrowerHMDARaceType5;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="CoborrowerHMDARaceType5" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="CoborrowerHMDARaceType5" /> element has a value to serialize.
        [XmlIgnore]
        public bool CoborrowerHMDARaceType5Specified
        {
            get { return CoborrowerHMDARaceType5 != null; }
            set { }
        }

        /// <summary>
        /// The coborrower's enrolled or principal tribe, if they are Native American.
        /// </summary>
        [XmlElement("CoborrowerHMDAEnrolledOrPrincipalTribe", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString CoborrowerHMDAEnrolledOrPrincipalTribe;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="CoborrowerHMDAEnrolledOrPrincipalTribe" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="CoborrowerHMDAEnrolledOrPrincipalTribe" /> element has a value to serialize.
        [XmlIgnore]
        public bool CoborrowerHMDAEnrolledOrPrincipalTribeSpecified
        {
            get { return CoborrowerHMDAEnrolledOrPrincipalTribe != null; }
            set { }
        }

        /// <summary>
        /// Any race designations not otherwise captured, if the borrower is Asian.
        /// </summary>
        [XmlElement("CoborrowerHMDARaceDesignationTypeOtherAsianDescription", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString CoborrowerHMDARaceDesignationTypeOtherAsianDescription;


        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="CoborrowerHMDARaceDesignationTypeOtherAsianDescription" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="CoborrowerHMDARaceDesignationTypeOtherAsianDescription" /> element has a value to serialize.
        [XmlIgnore]
        public bool CoborrowerHMDARaceDesignationTypeOtherAsianDescriptionSpecified
        {
            get { return CoborrowerHMDARaceDesignationTypeOtherAsianDescription != null; }
            set { }
        }

        /// <summary>
        /// Any race designations not otherwise captured, if the borrower is a Pacific Islander
        /// </summary>
        [XmlElement("CoborrowerHMDARaceDesignationTypeOtherPacificIslanderDescription", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString CoborrowerHMDARaceDesignationTypeOtherPacificIslanderDescription;


        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="CoborrowerHMDARaceDesignationTypeOtherPacificIslanderDescription" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="CoborrowerHMDARaceDesignationTypeOtherPacificIslanderDescription" /> element has a value to serialize.
        [XmlIgnore]
        public bool CoborrowerHMDARaceDesignationTypeOtherPacificIslanderDescriptionSpecified
        {
            get { return CoborrowerHMDARaceDesignationTypeOtherPacificIslanderDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the borrower's race was collected based on visual observation or surname.
        /// </summary>
        [XmlElement("BorrowerHMDARaceCollectedBasedOnVisualObservationOrSurnameType", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbHmdaCollectedBasedOnVisualObservationOrSurnameBase> BorrowerHMDARaceCollectedBasedOnVisualObservationOrSurnameType;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="BorrowerHMDARaceCollectedBasedOnVisualObservationOrSurnameType" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="BorrowerHMDARaceCollectedBasedOnVisualObservationOrSurnameType" /> element has a value to serialize.
        [XmlIgnore]
        public bool BorrowerHMDARaceCollectedBasedOnVisualObservationOrSurnameTypeSpecified
        {
            get { return BorrowerHMDARaceCollectedBasedOnVisualObservationOrSurnameType != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the coborrower's race was collected based on visual observation or surname.
        /// </summary>
        [XmlElement("CoborrowerHMDARaceCollectedBasedOnVisualObservationOrSurnameType", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbHmdaCollectedBasedOnVisualObservationOrSurnameBase> CoborrowerHMDARaceCollectedBasedOnVisualObservationOrSurnameType;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="CoborrowerHMDARaceCollectedBasedOnVisualObservationOrSurnameType" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="CoborrowerHMDARaceCollectedBasedOnVisualObservationOrSurnameType" /> element has a value to serialize.
        [XmlIgnore]
        public bool CoborrowerHMDARaceCollectedBasedOnVisualObservationOrSurnameTypeSpecified
        {
            get { return CoborrowerHMDARaceCollectedBasedOnVisualObservationOrSurnameType != null; }
            set { }
        }

        /// <summary>
        /// The borrower's gender, as defined by HMDA.
        /// </summary>
        [XmlElement("BorrowerHMDAGenderType", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbHmdaGenderBase> BorrowerHMDAGenderType;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="BorrowerHMDAGenderType" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="BorrowerHMDAGenderType" /> element has a value to serialize.
        [XmlIgnore]
        public bool BorrowerHMDAGenderTypeSpecified
        {
            get { return BorrowerHMDAGenderType != null; }
            set { }
        }

        /// <summary>
        /// The coborrower's gender, as defined by HMDA.
        /// </summary>
        [XmlElement("CoborrowerHMDAGenderType", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbHmdaGenderBase> CoborrowerHMDAGenderType;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="CoborrowerHMDAGenderType" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="CoborrowerHMDAGenderType" /> element has a value to serialize.
        [XmlIgnore]
        public bool CoborrowerHMDAGenderTypeSpecified
        {
            get { return CoborrowerHMDAGenderType != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the borrower's gender was collected by observation or surname.
        /// </summary>
        [XmlElement("BorrowerHMDAGenderCollectedBasedOnVisualObservationOrSurnameType", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbHmdaCollectedBasedOnVisualObservationOrSurnameBase> BorrowerHMDAGenderCollectedBasedOnVisualObservationOrSurnameType;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="BorrowerHMDAGenderCollectedBasedOnVisualObservationOrSurnameType" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="BorrowerHMDAGenderCollectedBasedOnVisualObservationOrSurnameType" /> element has a value to serialize.
        [XmlIgnore]
        public bool BorrowerHMDAGenderCollectedBasedOnVisualObservationOrSurnameTypeSpecified
        {
            get { return BorrowerHMDAGenderCollectedBasedOnVisualObservationOrSurnameType != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the coborrower's gender was collected by observation or surname.
        /// </summary>
        [XmlElement("CoborrowerHMDAGenderCollectedBasedOnVisualObservationOrSurnameType", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbHmdaCollectedBasedOnVisualObservationOrSurnameBase> CoborrowerHMDAGenderCollectedBasedOnVisualObservationOrSurnameType;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="CoborrowerHMDAGenderCollectedBasedOnVisualObservationOrSurnameType" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="CoborrowerHMDAGenderCollectedBasedOnVisualObservationOrSurnameType" /> element has a value to serialize.
        [XmlIgnore]
        public bool CoborrowerHMDAGenderCollectedBasedOnVisualObservationOrSurnameTypeSpecified
        {
            get { return CoborrowerHMDAGenderCollectedBasedOnVisualObservationOrSurnameType != null; }
            set { }
        }

        /// <summary>
        /// The borrower's age at the time of application.
        /// </summary>
        [XmlElement("BorrowerAgeAtApplicationYearsCount", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString BorrowerAgeAtApplicationYearsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="BorrowerAgeAtApplicationYearsCount" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="BorrowerAgeAtApplicationYearsCount" /> element has a value to serialize.
        [XmlIgnore]
        public bool BorrowerAgeAtApplicationYearsCountSpecified
        {
            get { return BorrowerAgeAtApplicationYearsCount != null; }
            set { }
        }

        /// <summary>
        /// The coborrower's age at the time of application.
        /// </summary>
        [XmlElement("CoborrowerAgeAtApplicationYearsCount", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString CoborrowerAgeAtApplicationYearsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="CoborrowerAgeAtApplicationYearsCount" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="CoborrowerAgeAtApplicationYearsCount" /> element has a value to serialize.
        [XmlIgnore]
        public bool CoborrowerAgeAtApplicationYearsCountSpecified
        {
            get { return CoborrowerAgeAtApplicationYearsCount != null; }
            set { }
        }

        /// <summary>
        /// The total monthly income of the applicants.
        /// </summary>
        [XmlElement("TotalMonthlyIncomeAmount", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString TotalMonthlyIncomeAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="TotalMonthlyIncomeAmount" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="TotalMonthlyIncomeAmount" /> element has a value to serialize.
        [XmlIgnore]
        public bool TotalMonthlyIncomeAmountSpecified
        {
            get { return TotalMonthlyIncomeAmount != null; }
            set { }
        }

        /// <summary>
        /// Indicates the type of institution that purchased the loan.
        /// </summary>
        [XmlElement("HMDAPurchaserType", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbHmdaPurchaserBase> HMDAPurchaserType;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="HMDAPurchaserType" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="HMDAPurchaserType" /> element has a value to serialize.
        [XmlIgnore]
        public bool HMDAPurchaserTypeSpecified
        {
            get { return HMDAPurchaserType != null; }
            set { }
        }

        /// <summary>
        /// The loan's rate spread percent.
        /// </summary>
        [XmlElement("HMDARateSpreadPercent", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString HMDARateSpreadPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="HMDARateSpreadPercent" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="HMDARateSpreadPercent" /> element has a value to serialize.
        [XmlIgnore]
        public bool HMDARateSpreadPercentSpecified
        {
            get { return HMDARateSpreadPercent != null; }
            set { }
        }

        /// <summary>
        /// The loan's HOEPA status indicator.
        /// </summary>
        [XmlElement("HMDAHOEPALoanStatusType", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbHmdaHoepaLoanStatusBase> HMDAHOEPALoanStatusType;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="HMDAHOEPALoanStatusType" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="HMDAHOEPALoanStatusType" /> element has a value to serialize.
        [XmlIgnore]
        public bool HMDAHOEPALoanStatusTypeSpecified
        {
            get { return HMDAHOEPALoanStatusType != null; }
            set { }
        }

        /// <summary>
        /// The lien priority of the loan.
        /// </summary>
        [XmlElement("LienPriorityType", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbLienPriorityBase> LienPriorityType;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="LienPriorityType" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="LienPriorityType" /> element has a value to serialize.
        [XmlIgnore]
        public bool LienPriorityTypeSpecified
        {
            get { return LienPriorityType != null; }
            set { }
        }

        /// <summary>
        /// The borrower's credit score value.
        /// </summary>
        [XmlElement("BorrowerCreditScoreValue", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString BorrowerCreditScoreValue;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="BorrowerCreditScoreValue" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="BorrowerCreditScoreValue" /> element has a value to serialize.
        [XmlIgnore]
        public bool BorrowerCreditScoreValueSpecified
        {
            get { return BorrowerCreditScoreValue != null; }
            set { }
        }

        /// <summary>
        /// The coborrower's credit score value.
        /// </summary>
        [XmlElement("CoborrowerCreditScoreValue", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString CoborrowerCreditScoreValue;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="CoborrowerCreditScoreValue" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="CoborrowerCreditScoreValue" /> element has a value to serialize.
        [XmlIgnore]
        public bool CoborrowerCreditScoreValueSpecified
        {
            get { return CoborrowerCreditScoreValue != null; }
            set { }
        }

        /// <summary>
        /// The borrower's credit score model type.
        /// </summary>
        [XmlElement("BorrowerCreditScoreModelNameType", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbHmdaCreditScoreModelNameBase> BorrowerCreditScoreModelNameType;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="BorrowerCreditScoreModelNameType" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="BorrowerCreditScoreModelNameType" /> element has a value to serialize.
        [XmlIgnore]
        public bool BorrowerCreditScoreModelNameTypeSpecified
        {
            get { return BorrowerCreditScoreModelNameType != null; }
            set { }
        }

        /// <summary>
        /// The other description for the borrower's credit score model type.
        /// </summary>
        [XmlElement("BorrowerCreditScoreModelNameTypeOtherDescription", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString BorrowerCreditScoreModelNameTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="BorrowerCreditScoreModelNameTypeOtherDescription" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="BorrowerCreditScoreModelNameTypeOtherDescription" /> element has a value to serialize.
        [XmlIgnore]
        public bool BorrowerCreditScoreModelNameTypeOtherDescriptionSpecified
        {
            get { return BorrowerCreditScoreModelNameTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The coborrower's credit score model type.
        /// </summary>
        [XmlElement("CoborrowerCreditScoreModelNameType", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbHmdaCreditScoreModelNameBase> CoborrowerCreditScoreModelNameType;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="CoborrowerCreditScoreModelNameType" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="CoborrowerCreditScoreModelNameType" /> element has a value to serialize.
        [XmlIgnore]
        public bool CoborrowerCreditScoreModelNameTypeSpecified
        {
            get { return CoborrowerCreditScoreModelNameType != null; }
            set { }
        }

        /// <summary>
        /// The other description for the coborrower's credit score model type.
        /// </summary>
        [XmlElement("CoborrowerCreditScoreModelNameTypeOtherDescription", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString CoborrowerCreditScoreModelNameTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="CoborrowerCreditScoreModelNameTypeOtherDescription" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="CoborrowerCreditScoreModelNameTypeOtherDescription" /> element has a value to serialize.
        [XmlIgnore]
        public bool CoborrowerCreditScoreModelNameTypeOtherDescriptionSpecified
        {
            get { return CoborrowerCreditScoreModelNameTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The first denial reason for the application.
        /// </summary>
        [XmlElement("HMDAReasonForDenialType1", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<Mismo3Specification.Version4Schema.HMDAReasonForDenialBase> HMDAReasonForDenialType1;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="HMDAReasonForDenialType1" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="HMDAReasonForDenialType1" /> element has a value to serialize.
        [XmlIgnore]
        public bool HMDAReasonForDenialType1Specified
        {
            get { return HMDAReasonForDenialType1 != null; }
            set { }
        }

        /// <summary>
        /// The second denial reason for the application.
        /// </summary>
        [XmlElement("HMDAReasonForDenialType2", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<Mismo3Specification.Version4Schema.HMDAReasonForDenialBase> HMDAReasonForDenialType2;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="HMDAReasonForDenialType2" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="HMDAReasonForDenialType2" /> element has a value to serialize.
        [XmlIgnore]
        public bool HMDAReasonForDenialType2Specified
        {
            get { return HMDAReasonForDenialType2 != null; }
            set { }
        }

        /// <summary>
        /// The third denial reason for the application.
        /// </summary>
        [XmlElement("HMDAReasonForDenialType3", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<Mismo3Specification.Version4Schema.HMDAReasonForDenialBase> HMDAReasonForDenialType3;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="HMDAReasonForDenialType3" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="HMDAReasonForDenialType3" /> element has a value to serialize.
        [XmlIgnore]
        public bool HMDAReasonForDenialType3Specified
        {
            get { return HMDAReasonForDenialType3 != null; }
            set { }
        }

        /// <summary>
        /// The fourth denial reason for the application.
        /// </summary>
        [XmlElement("HMDAReasonForDenialType4", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<Mismo3Specification.Version4Schema.HMDAReasonForDenialBase> HMDAReasonForDenialType4;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="HMDAReasonForDenialType4" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="HMDAReasonForDenialType4" /> element has a value to serialize.
        [XmlIgnore]
        public bool HMDAReasonForDenialType4Specified
        {
            get { return HMDAReasonForDenialType4 != null; }
            set { }
        }

        /// <summary>
        /// A denial reason not captured in the MISMO denial reason enum.
        /// </summary>
        [XmlElement("HMDAReasonForDenialTypeOtherDescription", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString HMDAReasonForDenialTypeOtherDescription;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="HMDAReasonForDenialTypeOtherDescription" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="HMDAReasonForDenialTypeOtherDescription" /> element has a value to serialize.
        [XmlIgnore]
        public bool ReasonForDenialTypeOtherDescriptionSpecified
        {
            get { return HMDAReasonForDenialTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The total loan costs for a TRID loan.
        /// </summary>
        [XmlElement("TotalLoanCostsAmount", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString TotalLoanCostsAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="TotalLoanCostsAmount" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="TotalLoanCostsAmount" /> element has a value to serialize.
        [XmlIgnore]
        public bool TotalLoanCostsAmountSpecified
        {
            get { return TotalLoanCostsAmount != null; }
            set { }
        }

        /// <summary>
        /// The total points and fees amount for a GFE loan.
        /// </summary>
        [XmlElement("TotalPointsAndFeesAmount", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString TotalPointsAndFeesAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="TotalPointsAndFeesAmount" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="TotalPointsAndFeesAmount" /> element has a value to serialize.
        [XmlIgnore]
        public bool TotalPointsAndFeesAmountSpecified
        {
            get { return TotalPointsAndFeesAmount != null; }
            set { }
        }

        /// <summary>
        /// The origination charges for the loan.
        /// </summary>
        [XmlElement("OriginationChargesAmount", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString OriginationChargesAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="OriginationChargesAmount" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="OriginationChargesAmount" /> element has a value to serialize.
        [XmlIgnore]
        public bool OriginationChargesAmountSpecified
        {
            get { return OriginationChargesAmount != null; }
            set { }
        }

        /// <summary>
        /// The discount points on the loan.
        /// </summary>
        [XmlElement("DiscountPointsAmount", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString DiscountPointsAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="DiscountPointsAmount" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="DiscountPointsAmount" /> element has a value to serialize.
        [XmlIgnore]
        public bool DiscountPointsAmountSpecified
        {
            get { return DiscountPointsAmount != null; }
            set { }
        }

        /// <summary>
        /// The lender credit amount.
        /// </summary>
        [XmlElement("LenderCreditsAmount", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString LenderCreditsAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="LenderCreditsAmount" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="LenderCreditsAmount" /> element has a value to serialize.
        [XmlIgnore]
        public bool LenderCreditsAmountSpecified
        {
            get { return LenderCreditsAmount != null; }
            set { }
        }

        /// <summary>
        /// The loan's note rate.
        /// </summary>
        [XmlElement("NoteRatePercent", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString NoteRatePercent;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="NoteRatePercent" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="NoteRatePercent" /> element has a value to serialize.
        [XmlIgnore]
        public bool NoteRatePercentSpecified
        {
            get { return NoteRatePercent != null; }
            set { }
        }

        /// <summary>
        /// The number of months before the prepayment penalty expires.
        /// </summary>
        [XmlElement("PrepaymentPenaltyExpirationMonthsCount", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString PrepaymentPenaltyExpirationMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="PrepaymentPenaltyExpirationMonthsCount" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="PrepaymentPenaltyExpirationMonthsCount" /> element has a value to serialize.
        [XmlIgnore]
        public bool PrepaymentPenaltyExpirationMonthsCountSpecified
        {
            get { return PrepaymentPenaltyExpirationMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// The debt-to-income ratio.
        /// </summary>
        [XmlElement("TotalDebtExpenseRatioPercent", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString TotalDebtExpenseRatioPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="TotalDebtExpenseRatioPercent" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="TotalDebtExpenseRatioPercent" /> element has a value to serialize.
        [XmlIgnore]
        public bool TotalDebtExpenseRatioPercentSpecified
        {
            get { return TotalDebtExpenseRatioPercent != null; }
            set { }
        }

        /// <summary>
        /// The Combined LTV ratio.
        /// </summary>
        [XmlElement("CombinedLTVRatioPercent", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString CombinedLTVRatioPercent;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="CombinedLTVRatioPercent" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="CombinedLTVRatioPercent" /> element has a value to serialize.
        [XmlIgnore]
        public bool CombinedLTVRatioPercentSpecified
        {
            get { return CombinedLTVRatioPercent != null; }
            set { }
        }

        /// <summary>
        /// The loan term in months.
        /// </summary>
        [XmlElement("LoanMaturityPeriodCount", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString LoanMaturityPeriodCount;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="LoanMaturityPeriodCount" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="LoanMaturityPeriodCount" /> element has a value to serialize.
        [XmlIgnore]
        public bool LoanMaturityPeriodCountSpecified
        {
            get { return LoanMaturityPeriodCount != null; }
            set { }
        }

        /// <summary>
        /// The number of months until the first rate change of an ARM.
        /// </summary>
        [XmlElement("FirstRateChangeMonthsCount", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString FirstRateChangeMonthsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="FirstRateChangeMonthsCount" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="FirstRateChangeMonthsCount" /> element has a value to serialize.
        [XmlIgnore]
        public bool FirstRateChangeMonthsCountSpecified
        {
            get { return FirstRateChangeMonthsCount != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the loan includes a balloon payment.
        /// </summary>
        [XmlElement("BalloonPaymentIndicator", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOIndicator BalloonPaymentIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="BalloonPaymentIndicator" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="BalloonPaymentIndicator" /> element has a value to serialize.
        [XmlIgnore]
        public bool BalloonPaymentIndicatorSpecified
        {
            get { return BalloonPaymentIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the loan includes interest-only payments.
        /// </summary>
        [XmlElement("InterestOnlyIndicator", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOIndicator InterestOnlyIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="InterestOnlyIndicator" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="InterestOnlyIndicator" /> element has a value to serialize.
        [XmlIgnore]
        public bool InterestOnlyIndicatorSpecified
        {
            get { return InterestOnlyIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the loan includes negative amortization features.
        /// </summary>
        [XmlElement("NegativeAmortizationIndicator", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOIndicator NegativeAmortizationIndicator;


        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="NegativeAmortizationIndicator" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="NegativeAmortizationIndicator" /> element has a value to serialize.
        [XmlIgnore]
        public bool NegativeAmortizationIndicatorSpecified
        {
            get { return NegativeAmortizationIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates any other non-amortizing features on the loan.
        /// </summary>
        [XmlElement("HMDAOtherNonAmortizingFeaturesIndicator", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOIndicator HMDAOtherNonAmortizingFeaturesIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="HMDAOtherNonAmortizingFeaturesIndicator" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="HMDAOtherNonAmortizingFeaturesIndicator" /> element has a value to serialize.
        [XmlIgnore]
        public bool HMDAOtherNonAmortizingFeaturesIndicatorSpecified
        {
            get { return HMDAOtherNonAmortizingFeaturesIndicator != null; }
            set { }
        }

        /// <summary>
        /// The subject property valuation.
        /// </summary>
        [XmlElement("PropertyValuationAmount", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString PropertyValuationAmount;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="PropertyValuationAmount" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="PropertyValuationAmount" /> element has a value to serialize.
        [XmlIgnore]
        public bool PropertyValuationAmountSpecified
        {
            get { return PropertyValuationAmount != null; }
            set { }
        }

        /// <summary>
        /// Indicates the secured property type of a manufactured home.
        /// </summary>
        [XmlElement("ManufacturedHomeSecuredPropertyType", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbManufacturedHomeSecuredPropertyBase> ManufacturedHomeSecuredPropertyType;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="ManufacturedHomeSecuredPropertyType" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="ManufacturedHomeSecuredPropertyType" /> element has a value to serialize.
        [XmlIgnore]
        public bool ManufacturedHomeSecuredPropertyTypeSpecified
        {
            get { return ManufacturedHomeSecuredPropertyType != null; }
            set { }
        }

        /// <summary>
        /// The property interest type of the land containing a manufactured home.
        /// </summary>
        [XmlElement("ManufacturedHomeLandPropertyInterestType", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbManufacturedHomeLandPropertyInterestBase> ManufacturedHomeLandPropertyInterestType;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="ManufacturedHomeLandPropertyInterestType" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="ManufacturedHomeLandPropertyInterestType" /> element has a value to serialize.
        [XmlIgnore]
        public bool ManufacturedHomeLandPropertyInterestTypeSpecified
        {
            get { return ManufacturedHomeLandPropertyInterestType != null; }
            set { }
        }

        /// <summary>
        /// The number of financed units.
        /// </summary>
        [XmlElement("FinancedUnitCount", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString FinancedUnitCount;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="FinancedUnitCount" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="FinancedUnitCount" /> element has a value to serialize.
        [XmlIgnore]
        public bool FinancedUnitCountSpecified
        {
            get { return FinancedUnitCount != null; }
            set { }
        }

        /// <summary>
        /// The number of affordable units.
        /// </summary>
        [XmlElement("AffordableUnitsCount", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString AffordableUnitsCount;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="AffordableUnitsCount" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="AffordableUnitsCount" /> element has a value to serialize.
        [XmlIgnore]
        public bool AffordableUnitsCountSpecified
        {
            get { return AffordableUnitsCount != null; }
            set { }
        }

        /// <summary>
        /// Indicates how the application was submitted.
        /// </summary>
        [XmlElement("HMDAApplicationSubmissionType", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<Mismo3Specification.Version4Schema.HMDAApplicationSubmissionBase> HMDAApplicationSubmissionType;


        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="HMDAApplicationSubmissionType" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="HMDAApplicationSubmissionType" /> element has a value to serialize.
        [XmlIgnore]
        public bool HMDAApplicationSubmissionTypeSpecified
        {
            get { return HMDAApplicationSubmissionType != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the loan is initially payable to the reporting institution.
        /// </summary>
        [XmlElement("HMDACoveredLoanInitiallyPayableToReportingInstitutionStatusType", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<Mismo3Specification.Version4Schema.HMDACoveredLoanInitiallyPayableToReportingInstitutionStatusBase> HMDACoveredLoanInitiallyPayableToReportingInstitutionStatusType;


        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="HMDACoveredLoanInitiallyPayableToReportingInstitutionStatusType" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="HMDACoveredLoanInitiallyPayableToReportingInstitutionStatusType" /> element has a value to serialize.
        [XmlIgnore]
        public bool HMDACoveredLoanInitiallyPayableToReportingInstitutionStatusTypeSpecified
        {
            get { return HMDACoveredLoanInitiallyPayableToReportingInstitutionStatusType != null; }
            set { }
        }

        /// <summary>
        /// The LO's NMLS identifier.
        /// </summary>
        [XmlElement("LoanOriginatorNMLSIdentifier", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString LoanOriginatorNMLSIdentifier;


        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="LoanOriginatorNMLSIdentifier" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="LoanOriginatorNMLSIdentifier" /> element has a value to serialize.
        [XmlIgnore]
        public bool LoanOriginatorNMLSIdentifierSpecified
        {
            get { return LoanOriginatorNMLSIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The first AUS used to underwrite the loan.
        /// </summary>
        [XmlElement("AutomatedUnderwritingSystemType1", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbHmdaAutomatedUnderwritingSystemBase> AutomatedUnderwritingSystemType1;


        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="AutomatedUnderwritingSystemType1" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="AutomatedUnderwritingSystemType1" /> element has a value to serialize.
        [XmlIgnore]
        public bool AutomatedUnderwritingSystemType1Specified
        {
            get { return AutomatedUnderwritingSystemType1 != null; }
            set { }
        }

        /// <summary>
        /// The second AUS used to underwrite the loan.
        /// </summary>
        [XmlElement("AutomatedUnderwritingSystemType2", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbHmdaAutomatedUnderwritingSystemBase> AutomatedUnderwritingSystemType2;


        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="AutomatedUnderwritingSystemType2" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="AutomatedUnderwritingSystemType2" /> element has a value to serialize.
        [XmlIgnore]
        public bool AutomatedUnderwritingSystemType2Specified
        {
            get { return AutomatedUnderwritingSystemType2 != null; }
            set { }
        }

        /// <summary>
        /// The third AUS used to underwrite the loan.
        /// </summary>
        [XmlElement("AutomatedUnderwritingSystemType3", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbHmdaAutomatedUnderwritingSystemBase> AutomatedUnderwritingSystemType3;


        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="AutomatedUnderwritingSystemType3" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="AutomatedUnderwritingSystemType3" /> element has a value to serialize.
        [XmlIgnore]
        public bool AutomatedUnderwritingSystemType3Specified
        {
            get { return AutomatedUnderwritingSystemType3 != null; }
            set { }
        }

        /// <summary>
        /// The fourth AUS used to underwrite the loan.
        /// </summary>
        [XmlElement("AutomatedUnderwritingSystemType4", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbHmdaAutomatedUnderwritingSystemBase> AutomatedUnderwritingSystemType4;


        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="AutomatedUnderwritingSystemType4" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="AutomatedUnderwritingSystemType4" /> element has a value to serialize.
        [XmlIgnore]
        public bool AutomatedUnderwritingSystemType4Specified
        {
            get { return AutomatedUnderwritingSystemType4 != null; }
            set { }
        }

        /// <summary>
        /// The fifth AUS used to underwrite the loan.
        /// </summary>
        [XmlElement("AutomatedUnderwritingSystemType5", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbHmdaAutomatedUnderwritingSystemBase> AutomatedUnderwritingSystemType5;


        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="AutomatedUnderwritingSystemType5" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="AutomatedUnderwritingSystemType5" /> element has a value to serialize.
        [XmlIgnore]
        public bool AutomatedUnderwritingSystemType5Specified
        {
            get { return AutomatedUnderwritingSystemType5 != null; }
            set { }
        }

        /// <summary>
        /// An other description to capture any unusual AUS used to underwrite the loan.
        /// </summary>
        [XmlElement("AutomatedUnderwritingSystemTypeOtherDescription", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString AutomatedUnderwritingSystemTypeOtherDescription;


        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="AutomatedUnderwritingSystemTypeOtherDescription" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="AutomatedUnderwritingSystemTypeOtherDescription" /> element has a value to serialize.
        [XmlIgnore]
        public bool AutomatedUnderwritingSystemTypeOtherDescriptionSpecified
        {
            get { return AutomatedUnderwritingSystemTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// The result from the first AUS used to underwrite the loan.
        /// </summary>
        [XmlElement("AutomatedUnderwritingSystemResultType1", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbHmdaAutomatedUnderwritingSystemResult> AutomatedUnderwritingSystemResultType1;


        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="AutomatedUnderwritingSystemResultType1" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="AutomatedUnderwritingSystemResultType1" /> element has a value to serialize.
        [XmlIgnore]
        public bool AutomatedUnderwritingSystemResultType1Specified
        {
            get { return AutomatedUnderwritingSystemResultType1 != null; }
            set { }
        }

        /// <summary>
        /// The result from the second AUS used to underwrite the loan.
        /// </summary>
        [XmlElement("AutomatedUnderwritingSystemResultType2", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbHmdaAutomatedUnderwritingSystemResult> AutomatedUnderwritingSystemResultType2;


        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="AutomatedUnderwritingSystemResultType2" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="AutomatedUnderwritingSystemResultType2" /> element has a value to serialize.
        [XmlIgnore]
        public bool AutomatedUnderwritingSystemResultType2Specified
        {
            get { return AutomatedUnderwritingSystemResultType2 != null; }
            set { }
        }

        /// <summary>
        /// The result from the third AUS used to underwrite the loan.
        /// </summary>
        [XmlElement("AutomatedUnderwritingSystemResultType3", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbHmdaAutomatedUnderwritingSystemResult> AutomatedUnderwritingSystemResultType3;


        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="AutomatedUnderwritingSystemResultType3" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="AutomatedUnderwritingSystemResultType3" /> element has a value to serialize.
        [XmlIgnore]
        public bool AutomatedUnderwritingSystemResultType3Specified
        {
            get { return AutomatedUnderwritingSystemResultType3 != null; }
            set { }
        }

        /// <summary>
        /// The result from the fourth AUS used to underwrite the loan.
        /// </summary>
        [XmlElement("AutomatedUnderwritingSystemResultType4", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbHmdaAutomatedUnderwritingSystemResult> AutomatedUnderwritingSystemResultType4;


        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="AutomatedUnderwritingSystemResultType4" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="AutomatedUnderwritingSystemResultType4" /> element has a value to serialize.
        [XmlIgnore]
        public bool AutomatedUnderwritingSystemResultType4Specified
        {
            get { return AutomatedUnderwritingSystemResultType4 != null; }
            set { }
        }

        /// <summary>
        /// The result from the fifth AUS used to underwrite the loan.
        /// </summary>
        [XmlElement("AutomatedUnderwritingSystemResultType5", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbHmdaAutomatedUnderwritingSystemResult> AutomatedUnderwritingSystemResultType5;


        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="AutomatedUnderwritingSystemResultType5" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="AutomatedUnderwritingSystemResultType5" /> element has a value to serialize.
        [XmlIgnore]
        public bool AutomatedUnderwritingSystemResultType5Specified
        {
            get { return AutomatedUnderwritingSystemResultType5 != null; }
            set { }
        }

        /// <summary>
        /// An other description to capture any unusual AUS result.
        /// </summary>
        [XmlElement("AutomatedUnderwritingSystemResultTypeOtherDescription", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString AutomatedUnderwritingSystemResultTypeOtherDescription;


        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="AutomatedUnderwritingSystemResultTypeOtherDescription" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="AutomatedUnderwritingSystemResultTypeOtherDescription" /> element has a value to serialize.
        [XmlIgnore]
        public bool AutomatedUnderwritingSystemResultTypeOtherDescriptionSpecified
        {
            get { return AutomatedUnderwritingSystemResultTypeOtherDescription != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the loan is a reverse mortgage.
        /// </summary>
        [XmlElement("ReverseMortgageIndicator", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOIndicator ReverseMortgageIndicator;


        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="ReverseMortgageIndicator" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="ReverseMortgageIndicator" /> element has a value to serialize.
        [XmlIgnore]
        public bool ReverseMortgageIndicatorSpecified
        {
            get { return ReverseMortgageIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the loan is open-ended credit (such as a HELOC).
        /// </summary>
        [XmlElement("OpenEndCreditIndicator", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOIndicator OpenEndCreditIndicator;


        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="OpenEndCreditIndicator" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="OpenEndCreditIndicator" /> element has a value to serialize.
        [XmlIgnore]
        public bool OpenEndCreditIndicatorSpecified
        {
            get { return OpenEndCreditIndicator != null; }
            set { }
        }

        /// <summary>
        /// Indicates whether the loan is being taken out for a business purpose.
        /// </summary>
        [XmlElement("HMDABusinessPurposeIndicator", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOIndicator HMDABusinessPurposeIndicator;


        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="HMDABusinessPurposeIndicator" /> element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the <see cref="HMDABusinessPurposeIndicator" /> element has a value to serialize.
        [XmlIgnore]
        public bool HMDABusinessPurposeIndicatorSpecified
        {
            get { return HMDABusinessPurposeIndicator != null; }
            set { }
        }
    }
}
