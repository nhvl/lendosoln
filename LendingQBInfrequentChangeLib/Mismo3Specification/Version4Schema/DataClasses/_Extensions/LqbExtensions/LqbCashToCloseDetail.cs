namespace Mismo3Specification.Version4Schema
{
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// The LQB_CASH_TO_CLOSE_DETAIL container.
    /// </summary>
    public partial class LQB_CASH_TO_CLOSE_DETAIL
    {
        /// <summary>
        /// Specifies the calculation method for cash to close items.
        /// </summary>
        [XmlElement("CalculationMethodType", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<CashToCloseCalculationMethodBase> CalculationMethodType;

        /// <summary>
        /// Determines whether the CalculationMethodType has a value to serialize.
        /// </summary>
        /// <returns>A boolean indicating whether the CalculationMethodType element has been assigned a value.</returns>
        public bool CalculationMethodTypeSpecified()
        {
            return CalculationMethodType != null && CalculationMethodType.EnumValue != CashToCloseCalculationMethodBase.Blank;
        }

        /// <summary>
        /// Gets a value indicating whether the LQB_CASH_TO_CLOSE_DETAIL container should be serialized.
        /// </summary>
        /// <value>A value indicating whether the LQB_CASH_TO_CLOSE_DETAIL container should be serialized.</value>
        [XmlIgnore]
        public bool ShouldSerialize 
        {
            get { return CalculationMethodTypeSpecified(); }
            set { }
        }
    }
}
