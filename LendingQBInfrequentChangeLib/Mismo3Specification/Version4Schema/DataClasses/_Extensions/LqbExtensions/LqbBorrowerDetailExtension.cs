﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class LQB_BORROWER_DETAIL_EXTENSION : OTHER_BASE
    {
        [XmlIgnore]
        public override bool ShouldSerialize
        {
            get
            {
                return this.FinancialInstitutionMemberIdentifierSpecified;
            }
        }

        [XmlElement("FinancialInstitutionMemberIdentifier", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOIdentifier FinancialInstitutionMemberIdentifier;

        [XmlIgnore]
        public bool FinancialInstitutionMemberIdentifierSpecified
        {
            get
            {
                return this.FinancialInstitutionMemberIdentifier != null;
            }
        }
    }
}
