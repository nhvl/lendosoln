﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// A custom extension to the MISMO HMDA_LOAN element.
    /// </summary>
    public partial class LQB_HMDA_LOAN_EXTENSION : OTHER_BASE
    {
        /// <summary>
        /// Indicates whether the LQB_HMDA_LOAN_EXTENSION element has a value to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the LQB_HMDA_LOAN_EXTENSION element has a value to serialize.</value>
        [XmlIgnore]
        override public bool ShouldSerialize
        {
            get
            {
                return this.HmdaLarDataSpecified
                    || this.HmdaReliedOnSpecified;
            }
        }

        /// <summary>
        /// The HMDA LAR data.
        /// </summary>
        [XmlElement("HMDA_LAR_DATA", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public LQB_HMDA_LAR_DATA HmdaLarData;

        /// <summary>
        /// Indicates whether the HMDA_LAR_DATA element has data to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HMDA_LAR_DATA element has data to serialize.</value>
        [XmlIgnore]
        public bool HmdaLarDataSpecified
        {
            get { return this.HmdaLarData != null && this.HmdaLarData.ShouldSerialize; }
            set { }
        }

        /// <summary>
        /// The HMDA relied-on data.
        /// </summary>
        [XmlElement("HMDA_RELIED_ON", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public LQB_HMDA_RELIED_ON HmdaReliedOn;

        /// <summary>
        /// Indicates whether the HMDA_RELIED_ON element has data to serialize.
        /// </summary>
        /// <value>A boolean indicating whether the HMDA_RELIED_ON element has data to serialize.</value>
        [XmlIgnore]
        public bool HmdaReliedOnSpecified
        {
            get { return this.HmdaReliedOn != null && this.HmdaReliedOn.ShouldSerialize; }
            set { }
        }
    }
}
