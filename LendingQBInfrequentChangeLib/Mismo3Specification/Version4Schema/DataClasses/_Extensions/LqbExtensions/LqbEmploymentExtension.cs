﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class LQB_EMPLOYMENT_EXTENSION : OTHER_BASE
    {
        [XmlIgnore]
        public override bool ShouldSerialize
        {
            get
            {
                return this.EmployeeIdentifierSpecified
                    || this.SalaryKeySpecified;
            }
        }

        [XmlElement(Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOIdentifier EmployeeIdentifier;

        [XmlIgnore]
        public bool EmployeeIdentifierSpecified
        {
            get { return this.EmployeeIdentifier != null; }
            set { }
        }

        [XmlElement(Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString SalaryKey;

        [XmlIgnore]
        public bool SalaryKeySpecified
        {
            get { return this.SalaryKey != null; }
            set { }
        }
    }
}
