﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class LQB_LEGAL_ENTITY_DETAIL_EXTENSION : OTHER_BASE
    {
        [XmlIgnore]
        public override bool ShouldSerialize
        {
            get
            {
                return EmployerCodeSpecified;
            }
        }

        [XmlElement(Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOCode EmployerCode;

        [XmlIgnore]
        public bool EmployerCodeSpecified
        {
            get { return this.EmployerCode != null; }
            set { }
        }
    }
}
