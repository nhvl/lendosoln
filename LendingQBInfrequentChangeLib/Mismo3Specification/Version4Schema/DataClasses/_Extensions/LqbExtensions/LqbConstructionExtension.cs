﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// A proprietary extension to the CONSTRUCTION element.
    /// </summary>
    public class LQB_CONSTRUCTION_EXTENSION : OTHER_BASE
    {
        /// <summary>
        /// Gets a value indicating whether the element should be serialized.
        /// </summary>
        [XmlIgnore]
        public override bool ShouldSerialize
        {
            get
            {
                return this.ConstructionPurposeTypeSpecified
                    || this.ConstructionLoanAmortizationTypeSpecified
                    || this.ConstructionInitialAdvanceAmountSpecified
                    || this.ConstructionRequiredInterestReserveIndicatorSpecified
                    || this.ConstructionPhaseInterestAccrualTypeSpecified
                    || this.ConstructionLoanDateSpecified
                    || this.ConstructionPeriodEndDateSpecified
                    || this.ConstructionFirstPaymentDueDateSpecified;
            }
        }

        /// <summary>
        /// The purpose of a construction loan.
        /// </summary>
        [XmlElement("ConstructionPurposeType", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbConstructionPurposeBase> ConstructionPurposeType;

        /// <summary>
        /// Gets a value indicating whether the <see cref="ConstructionPurposeType"/> element has data to serialize.
        /// </summary>
        [XmlIgnore]
        public bool ConstructionPurposeTypeSpecified
        {
            get
            {
                return this.ConstructionPurposeType != null;
            }
        }

        /// <summary>
        /// The amortization type of a construction loan.
        /// </summary>
        [XmlElement("ConstructionLoanAmortizationType", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbConstructionLoanAmortizationBase> ConstructionLoanAmortizationType;

        /// <summary>
        /// Gets a value indicating whether the <see cref="ConstructionLoanAmortizationType"/> element has data to serialize.
        /// </summary>
        [XmlIgnore]
        public bool ConstructionLoanAmortizationTypeSpecified
        {
            get
            {
                return this.ConstructionLoanAmortizationType != null;
            }
        }

        /// <summary>
        /// The initial advance amount for a construction loan.
        /// </summary>
        [XmlElement("ConstructionInitialAdvanceAmount", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOAmount ConstructionInitialAdvanceAmount;

        /// <summary>
        /// Gets a value indicating whether the <see cref="ConstructionInitialAdvanceAmount"/> element has data to serialize.
        /// </summary>
        [XmlIgnore]
        public bool ConstructionInitialAdvanceAmountSpecified
        {
            get
            {
                return this.ConstructionInitialAdvanceAmount != null;
            }
        }

        /// <summary>
        /// Indicates whether the construction loan requires an interest reserve.
        /// </summary>
        [XmlElement("ConstructionRequiredInterestReserveIndicator", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOIndicator ConstructionRequiredInterestReserveIndicator;

        /// <summary>
        /// Gets a value indicating whether the <see cref="ConstructionRequiredInterestReserveIndicator"/> element has data to serialize.
        /// </summary>
        [XmlIgnore]
        public bool ConstructionRequiredInterestReserveIndicatorSpecified
        {
            get
            {
                return this.ConstructionRequiredInterestReserveIndicator != null;
            }
        }

        /// <summary>
        /// The interest accrual type during the construction phase.
        /// </summary>
        [XmlElement("ConstructionPhaseInterestAccrualType", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbConstructionPhaseInterestAccrualBase> ConstructionPhaseInterestAccrualType;

        /// <summary>
        /// Gets a value indicating whether the <see cref="ConstructionPhaseInterestAccrualType"/> element has data to serialize.
        /// </summary>
        [XmlIgnore]
        public bool ConstructionPhaseInterestAccrualTypeSpecified
        {
            get
            {
                return this.ConstructionPhaseInterestAccrualType != null;
            }
        }

        /// <summary>
        /// The construction loan date.
        /// </summary>
        [XmlElement("ConstructionLoanDate", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMODate ConstructionLoanDate;

        /// <summary>
        /// Gets a value indicating whether the <see cref="ConstructionLoanDate"/> element has data to serialize.
        /// </summary>
        [XmlIgnore]
        public bool ConstructionLoanDateSpecified
        {
            get
            {
                return this.ConstructionLoanDate != null;
            }
        }

        /// <summary>
        /// The end date of the construction phase.
        /// </summary>
        [XmlElement("ConstructionPeriodEndDate", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMODate ConstructionPeriodEndDate;

        /// <summary>
        /// Gets a value indicating whether the <see cref="ConstructionPeriodEndDate"/> element has data to serialize.
        /// </summary>
        [XmlIgnore]
        public bool ConstructionPeriodEndDateSpecified
        {
            get
            {
                return this.ConstructionPeriodEndDate != null;
            }
        }

        /// <summary>
        /// The due date of the first construction loan payment.
        /// </summary>
        [XmlElement("ConstructionFirstPaymentDueDate", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMODate ConstructionFirstPaymentDueDate;

        /// <summary>
        /// Gets a value indicating whether the <see cref="ConstructionFirstPaymentDueDate"/> element has data to serialize.
        /// </summary>
        [XmlIgnore]
        public bool ConstructionFirstPaymentDueDateSpecified
        {
            get
            {
                return this.ConstructionFirstPaymentDueDate != null;
            }
        }

        /// <summary>
        /// The original cost of the land.
        /// </summary>
        [XmlElement("LandCostAmount", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOAmount LandCostAmount;

        /// <summary>
        /// Gets a value indicating whether the <see cref="LandCostAmount"/> element has data to serialize.
        /// </summary>
        [XmlIgnore]
        public bool LandCostAmountSpecified
        {
            get
            {
                return this.LandCostAmount != null;
            }
        }

        /// <summary>
        /// The value of the lot.
        /// </summary>
        [XmlElement("LotValueAmount", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOAmount LotValueAmount;

        /// <summary>
        /// Gets a value indicating whether the <see cref="LotValueAmount"/> element has data to serialize.
        /// </summary>
        [XmlIgnore]
        public bool LotValueAmountSpecified
        {
            get
            {
                return this.LotValueAmount != null;
            }
        }

        /// <summary>
        /// The date the lot was acquired.
        /// </summary>
        [XmlElement("LotAcquiredDate", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMODate LotAcquiredDate;

        /// <summary>
        /// Gets a value indicating whether the <see cref="LotAcquiredDate"/> element has data to serialize.
        /// </summary>
        [XmlIgnore]
        public bool LotAcquiredDateSpecified
        {
            get
            {
                return this.LotAcquiredDate != null;
            }
        }

        /// <summary>
        /// The owner of the lot.
        /// </summary>
        [XmlElement("LotOwnerType", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbLotOwnerBase> LotOwnerType;

        /// <summary>
        /// Gets a value indicating whether the <see cref="LotOwnerType"/> element has data to serialize.
        /// </summary>
        [XmlIgnore]
        public bool LotOwnerTypeSpecified
        {
            get
            {
                return this.LotOwnerType != null;
            }
        }

        /// <summary>
        /// The subsequently paid finance charge amount.
        /// </summary>
        [XmlElement("SubsequentlyPaidFinanceChargeAmount", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOAmount SubsequentlyPaidFinanceChargeAmount;

        /// <summary>
        /// Gets a value indicating whether the <see cref="SubsequentlyPaidFinanceChargeAmount"/> element has data to serialize.
        /// </summary>
        [XmlIgnore]
        public bool SubsequentlyPaidFinanceChargeAmountSpecified
        {
            get
            {
                return this.SubsequentlyPaidFinanceChargeAmount != null;
            }
        }

        /// <summary>
        /// The amount of construction interest to be paid.
        /// </summary>
        [XmlElement("ConstructionInterestAmount", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOAmount ConstructionInterestAmount;

        /// <summary>
        /// Gets a value indicating whether the <see cref="ConstructionInterestAmount"/> element has data to serialize.
        /// </summary>
        [XmlIgnore]
        public bool ConstructionInterestAmountSpecified
        {
            get
            {
                return this.ConstructionInterestAmount != null;
            }
        }

        /// <summary>
        /// The date construction interest will accrue.
        /// </summary>
        [XmlElement("ConstructionInterestAccrualDate", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMODate ConstructionInterestAccrualDate;

        /// <summary>
        /// Gets a value indicating whether the <see cref="ConstructionInterestAccrualDate"/> element has data to serialize.
        /// </summary>
        [XmlIgnore]
        public bool ConstructionInterestAccrualDateSpecified
        {
            get
            {
                return this.ConstructionInterestAccrualDate != null;
            }
        }

        /// <summary>
        /// The construction disclosure type.
        /// </summary>
        [XmlElement("ConstructionDisclosureType", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbConstructionDisclosureBase> ConstructionDisclosureType;

        /// <summary>
        /// Gets a value indicating whether the <see cref="ConstructionDisclosureType"/> element has data to serialize.
        /// </summary>
        [XmlIgnore]
        public bool ConstructionDisclosureTypeSpecified
        {
            get
            {
                return this.ConstructionDisclosureType != null;
            }
        }
    }
}