﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// Specifies the relied-on credit score mode.
    /// </summary>
    public enum LqbReliedOnCreditScoreModeBase
    {
        /// <summary>
        /// Indicates an applicant's individual score.
        /// </summary>
        [XmlEnum("ApplicantsIndividualScore")]
        ApplicantsIndividualScore,

        /// <summary>
        /// Indicates a single score for the whole application.
        /// </summary>
        [XmlEnum("SingleScore")]
        SingleScore,
        
        /// <summary>
        /// A catchall value to account for a blank field in LQB.
        /// </summary>
        [XmlEnum("")]
        Blank
    }
}
