namespace Mismo3Specification.Version4Schema
{
    using System.Xml;
    using System.Xml.Serialization;


    /// <summary>
    /// A proprietary extension to the CLOSING_INFORMATION_DETAIL element.
    /// </summary>
    public class LQB_CLOSING_INFORMATION_DETAIL_EXTENSION : OTHER_BASE
    {
        /// <summary>
        /// Gets a value indicating whether the LQB_ESCROW_ITEM_DETAIL_EXTENSION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public override bool ShouldSerialize
        {
            get
            {
                return this.EstimatedPrepaidDaysCountSpecified;
            }
        }

        /// <summary>
        /// The number of days for which per-diem interest is paid.
        /// </summary>
        [XmlElement("EstimatedPrepaidDaysCount", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString EstimatedPrepaidDaysCount { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the EstimatedPrepaidDaysCount element has a value to serialize.
        /// </summary>
        [XmlIgnore]
        public bool EstimatedPrepaidDaysCountSpecified
        {
            get { return this.EstimatedPrepaidDaysCount != null; }
        }

        /// <summary>
        /// The number of additional interest days requiring consent.
        /// </summary>
        [XmlElement("AdditionalInterestDaysRequiringConsentCount", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOCount AdditionalInterestDaysRequiringConsentCount { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the AdditionalInterestDaysRequiringConsentCount element has a value to serialize.
        /// </summary>
        [XmlIgnore]
        public bool AdditionalInterestDaysRequiringConsentCountSpecified
        {
            get { return this.AdditionalInterestDaysRequiringConsentCount != null; }
        }
    }
}
