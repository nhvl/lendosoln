﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    /// <summary>
    /// LQB extension for the INVESTOR_LOAN_INFORMATION container.
    /// </summary>
    public class LQB_INVESTOR_LOAN_INFORMATION_EXTENSION : OTHER_BASE
    {
        /// <summary>
        /// Gets a value indicating whether the element should be serialized;
        /// </summary>
        [XmlIgnore]
        public override bool ShouldSerialize
        {
            get
            {
                return this.InvestorProgramIdentifierSpecified;
            }
        }

        /// <summary>
        /// The identifier for the locked investor loan program.
        /// </summary>
        [XmlElement("InvestorProgramIdentifier", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOIdentifier InvestorProgramIdentifier { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the InvestorProgramIdentifier element has a value to serialize.
        /// </summary>
        [XmlIgnore]
        public bool InvestorProgramIdentifierSpecified
        {
            get { return this.InvestorProgramIdentifier != null; }
            set { }
        }
    }
}