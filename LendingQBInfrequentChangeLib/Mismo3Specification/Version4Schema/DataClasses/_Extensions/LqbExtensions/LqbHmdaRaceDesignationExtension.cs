﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class LQB_HMDA_RACE_DESIGNATION_EXTENSION : OTHER_BASE
    {
        [XmlIgnore]
        public override bool ShouldSerialize
        {
            get
            {
                return this.HMDARaceDesignationTypeSpecified
                    || this.HMDARaceDesignationTypeOtherAsianDescriptionSpecified
                    || this.HMDARaceDesignationTypeOtherPacificIslanderDescriptionSpecified;
            }
        }

        [XmlElement(nameof(HMDARaceDesignationType), Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbHMDARaceDesignationBase> HMDARaceDesignationType
        {
            get; set;
        }

        [XmlIgnore]
        public bool HMDARaceDesignationTypeSpecified
        {
            get
            {
                return this.HMDARaceDesignationType != null;
            }
            set
            {
            }
        }

        [XmlElement(nameof(HMDARaceDesignationTypeOtherAsianDescription), Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString HMDARaceDesignationTypeOtherAsianDescription;

        [XmlIgnore]
        public bool HMDARaceDesignationTypeOtherAsianDescriptionSpecified
        {
            get
            {
                return this.HMDARaceDesignationTypeOtherAsianDescription != null;
            }
            set
            {
            }
        }

        [XmlElement(nameof(HMDARaceDesignationTypeOtherPacificIslanderDescription), Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOString HMDARaceDesignationTypeOtherPacificIslanderDescription;

        [XmlIgnore]
        public bool HMDARaceDesignationTypeOtherPacificIslanderDescriptionSpecified
        {
            get
            {
                return this.HMDARaceDesignationTypeOtherPacificIslanderDescription != null;
            }
            set
            {
            }
        }
    }
}
