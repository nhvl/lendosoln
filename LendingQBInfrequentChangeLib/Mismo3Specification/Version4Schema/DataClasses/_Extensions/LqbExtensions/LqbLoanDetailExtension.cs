namespace Mismo3Specification.Version4Schema
{
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// A container holding loan details.
    /// </summary>
    public partial class LQB_LOAN_DETAIL_EXTENSION : OTHER_BASE
    {
        /// <summary>
        /// Indicates whether the element should be serialized.
        /// </summary>
        [XmlIgnore]
        public override bool ShouldSerialize
        {
            get
            {
                return this.LoanOriginationChannelTypeSpecified
                    || this.TridTargetRegulationVersionTypeSpecified
                    || this.LoanVersionIdentifierSpecified
                    || this.IntentToProceedDateSpecified
                    || this.NewConcurrentOtherFinancingIndicatorSpecified
                    || this.LoanBeingRefinancedUsedToConstructAlterRepairIndicatorSpecified;
            }
        }

        /// <summary>
        /// Represents the type of loan origination channel. Represented by <see cref="DataAccess.CPageData.sBranchChannelT"./>
        /// </summary>
        [XmlElement("LoanOriginationChannelType", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbLoanOriginationChannelBase> LoanOriginationChannelType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the Loan Origination channel type has been specified.
        /// </summary>
        [XmlIgnore]
        public bool LoanOriginationChannelTypeSpecified
        {
            get { return LoanOriginationChannelType != null && LoanOriginationChannelType.EnumValue != LqbLoanOriginationChannelBase.Blank; }
            set { }
        }

        /// <summary>
        /// Represents the target TRID regulation version.
        /// </summary>
        [XmlElement("TridTargetRegulationVersionType", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbTridTargetRegulationVersionBase> TridTargetRegulationVersionType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the TRID target regulation version type has been specified.
        /// </summary>
        [XmlIgnore]
        public bool TridTargetRegulationVersionTypeSpecified
        {
            get { return this.TridTargetRegulationVersionType != null && this.TridTargetRegulationVersionType.EnumValue != LqbTridTargetRegulationVersionBase.Blank; }
            set { }
        }

        /// <summary>
        /// Represents the current loan version.
        /// </summary>
        [XmlElement("LoanVersionIdentifier", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOIdentifier LoanVersionIdentifier { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the loan version has been specified.
        /// </summary>
        [XmlIgnore]
        public bool LoanVersionIdentifierSpecified
        {
            get { return this.LoanVersionIdentifier != null; }
            set {  }
        }

        /// <summary>
        /// Represets the intent to proceed date.
        /// </summary>
        [XmlElement("IntentToProceedDate", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMODate IntentToProceedDate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether IntentToProceedDate is specified.
        /// </summary>
        [XmlIgnore]
        public bool IntentToProceedDateSpecified
        {
            get { return this.IntentToProceedDate != null; }
            set { }
        }

        /// <summary>
        /// The new concurrent other financing indicator.
        /// </summary>
        [XmlElement("NewConcurrentOtherFinancingIndicator", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOIndicator NewConcurrentOtherFinancingIndicator { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether NewConcurrentOtherFinancingIndicator is specified.
        /// </summary>
        [XmlIgnore]
        public bool NewConcurrentOtherFinancingIndicatorSpecified
        {
            get { return this.NewConcurrentOtherFinancingIndicator != null; }
            set { }
        }

        /// <summary>
        /// The new loan refinances an interim loan to construct, alter, or repair the home
        /// </summary>
        [XmlElement("LoanBeingRefinancedUsedToConstructAlterRepairIndicator", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOIndicator LoanBeingRefinancedUsedToConstructAlterRepairIndicator;

        /// <summary>
        /// Gets or sets a value indicating whether LoanBeingRefinancedUsedToConstructAlterRepairIndicator is specified.
        /// </summary>
        [XmlIgnore]
        public bool LoanBeingRefinancedUsedToConstructAlterRepairIndicatorSpecified
        {
            get
            {
                return this.LoanBeingRefinancedUsedToConstructAlterRepairIndicator != null;
            }

            set
            {
            }
        }
    }
}
