﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class LQB_DOCUMENTATION_EXTENSION : OTHER_BASE
    {
        /// <summary>
        /// Gets a value indicating whether the LQB_DOCUMENTATION_EXTENSION container should be serialized.
        /// </summary>
        [XmlIgnore]
        public override bool ShouldSerialize
        {
            get
            {
                return DocumentTypeName != null;
            }
        }

        /// <summary>
        /// An enum representing the document type.
        /// </summary>
        [XmlElement("DocumentTypeName", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOEnum<LqbDocumentTypeNameBase> DocumentTypeName;
    }
}
