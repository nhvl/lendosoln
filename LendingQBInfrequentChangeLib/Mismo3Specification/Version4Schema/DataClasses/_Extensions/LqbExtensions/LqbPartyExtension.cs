﻿namespace Mismo3Specification.Version4Schema
{
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// An LQB-specific extension to the PARTY element.
    /// </summary>
    public class LQB_PARTY_EXTENSION : OTHER_BASE
    {
        /// <summary>
        /// Gets a value indicating whether the <see cref="LQB_PARTY_EXTENSION"/> container should be serialized.
        /// </summary>
        /// <value>Whether to serialize as XML.</value>
        [XmlIgnore]
        public override bool ShouldSerialize
        {
            get
            {
                return this.AgentIdSpecified
                    || this.ConsumerIdSpecified
                    || this.EmployeeIdentifierSpecified
                    || this.BranchCodeSpecified;
            }
        }

        /// <summary>
        /// Gets or sets the LQB agent ID used to identify the agent in the loan file.
        /// </summary>
        /// <value>Party's agent ID.</value>
        [XmlElement(nameof(AgentId), Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOIdentifier AgentId { get; set; }
        
        /// <summary>
        /// Gets a value indicating whether the <see cref="AgentId"/> element has a value to serialize.
        /// </summary>
        /// <value>Whether the <see cref="AgentId"/> field should be XML-serialized.</value>
        [XmlIgnore]
        public bool AgentIdSpecified
        {
            get
            {
                return this.AgentId != null;
            }
        }

        /// <summary>
        /// The borrower-level consumer ID.
        /// </summary>
        [XmlElement("ConsumerID", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOIdentifier ConsumerId;

        /// <summary>
        /// Gets or sets a value indicating whether the Consumer ID element has a value to serialize.
        /// </summary>
        [XmlIgnore]
        public bool ConsumerIdSpecified
        {
            get { return this.ConsumerId != null; }
            set { }
        }

        /// <summary>
        /// The employee ID associated with an individual.
        /// </summary>
        [XmlElement("EmployeeIdentifier", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOIdentifier EmployeeIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the Employer Identifier element has a value to serialize.
        /// </summary>
        [XmlIgnore]
        public bool EmployeeIdentifierSpecified
        {
            get { return this.EmployeeIdentifier != null; }
            set { }
        }

        /// <summary>
        /// The branch code associated with an entity.
        /// </summary>
        [XmlElement("BranchCode", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOCode BranchCode;

        /// <summary>
        /// Gets or sets a value indicating whether the Branch Code element has a value to serialize.
        /// </summary>
        [XmlIgnore]
        public bool BranchCodeSpecified
        {
            get { return this.BranchCode != null; }
            set { }
        }

        /// <summary>
        /// A unique identifier for a company.
        /// </summary>
        [XmlElement("CompanyIdentifier", Namespace = Mismo3Constants.LqbExtensionNamespace)]
        public MISMOIdentifier CompanyIdentifier;

        /// <summary>
        /// Gets or sets a value indicating whether the CompanyIdentifier element has a value to serialize.
        /// </summary>
        [XmlIgnore]
        public bool CompanyIdentifierSpecified
        {
            get { return this.CompanyIdentifier != null; }
            set { }
        }
    }
}