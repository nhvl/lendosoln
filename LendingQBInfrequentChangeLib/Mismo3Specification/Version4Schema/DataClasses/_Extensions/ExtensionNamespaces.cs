﻿namespace Mismo3Specification.Version4Schema
{
    using System;
    using System.Xml.Serialization;

    /// <summary>
    /// Manages the different sets of XML namespaces we use for our MISMO implementations.
    /// </summary>
    public static class ExtensionNamespaces
    {
        /// <summary>
        /// Gets the standard namespaces for LendingQB MISMO.
        /// </summary>
        /// <value>The standard namespaces for LendingQB MISMO.</value>
        public static XmlSerializerNamespaces LQBNamespaces
        {
            get
            {
                var lqbNamespaces = new XmlSerializerNamespaces();
                lqbNamespaces.Add(string.Empty, Mismo3Constants.MismoNamespace);
                lqbNamespaces.Add("xlink", Mismo3Constants.XlinkNamespace);
                lqbNamespaces.Add("lendingqb", Mismo3Constants.LqbExtensionNamespace);
                lqbNamespaces.Add("ULAD", Mismo3Constants.UladExtensionNamespace);
                return lqbNamespaces;
            }
        }

        /// <summary>
        /// Gets attribute overrides to disambiguate deserialization of LQB extensions.
        /// </summary>
        /// <value>A set of XML attribute overrides</value>
        public static XmlAttributeOverrides LQBOverrides
        {
            get
            {
                var overrides = new XmlAttributeOverrides();

                overrides.Add(typeof(BORROWER_EXTENSION), nameof(BORROWER_EXTENSION.Other),
                    GenerateAttribute("OTHER", order: 1, type: typeof(LQB_BORROWER_EXTENSION)));

                overrides.Add(typeof(EMPLOYMENT_EXTENSION), nameof(EMPLOYMENT_EXTENSION.Other),
                    GenerateAttribute("OTHER", order: 1, type: typeof(LQB_EMPLOYMENT_EXTENSION)));

                overrides.Add(typeof(LEGAL_ENTITY_DETAIL_EXTENSION), nameof(LEGAL_ENTITY_DETAIL_EXTENSION.Other),
                    GenerateAttribute("OTHER", order: 1, type: typeof(LQB_LEGAL_ENTITY_DETAIL_EXTENSION)));

                overrides.Add(typeof(SERVICE_EXTENSION), nameof(SERVICE_EXTENSION.Other),
                    GenerateAttribute("OTHER", order: 1, type: typeof(LQB_SERVICE_EXTENSION)));

                overrides.Add(typeof(SERVICE_PRODUCT_DETAIL_EXTENSION), nameof(SERVICE_PRODUCT_DETAIL_EXTENSION.Other),
                    GenerateAttribute("OTHER", order: 1, type: typeof(LQB_SERVICE_PRODUCT_DETAIL_EXTENSION)));

                // For now, we'll just override the MCL version so we don't need to copy the code; we'll do that at the first divergence of LQB/MCL Mismo for this extension.
                overrides.Add(typeof(SERVICE_PRODUCT_REQUEST_EXTENSION), nameof(SERVICE_PRODUCT_REQUEST_EXTENSION.Other),
                    GenerateAttribute("OTHER", order: 1, type: typeof(MCL_SERVICE_PRODUCT_REQUEST_EXTENSION)));

                overrides.Add(typeof(MCL_SERVICE_PRODUCT_REQUEST_EXTENSION), nameof(MCL_SERVICE_PRODUCT_REQUEST_EXTENSION.ServiceProductIdentifiers),
                    GenerateAttribute("SERVICE_PRODUCT_IDENTIFIERS", order: 0, @namespace: Mismo3Constants.LqbExtensionNamespace));

                overrides.Add(typeof(VERIFICATION_OF_INCOME_REQUEST_EXTENSION), nameof(VERIFICATION_OF_INCOME_REQUEST_EXTENSION.Other),
                    GenerateAttribute("OTHER", order: 1, type: typeof(LQB_VERIFICATION_OF_INCOME_REQUEST_EXTENSION)));

                return overrides;
            }
        }

        /// <summary>
        /// Gets a set of namespaces for integrating with MCL Smart API.
        /// </summary>
        /// <value>A set of namespaces for integrationg with MCL Smart API.</value>
        public static XmlSerializerNamespaces MCLNamespaces
        {
            get
            {
                var mclNamespaces = new XmlSerializerNamespaces();
                mclNamespaces.Add(string.Empty, Mismo3Constants.MismoNamespace);
                mclNamespaces.Add("p2", Mismo3Constants.XlinkNamespace);
                mclNamespaces.Add("p3", Mismo3Constants.MclExtensionNamespace);
                return mclNamespaces;
            }
        }

        /// <summary>
        /// Gets attribute overrides to disambiguate deserialization of MCL extensions.
        /// </summary>
        /// <value>A set of XML attribute overrides</value>
        public static XmlAttributeOverrides MCLOverrides
        {
            get
            {
                var overrides = new XmlAttributeOverrides();

                overrides.Add(typeof(BORROWER_EXTENSION), nameof(BORROWER_EXTENSION.Other),
                    GenerateAttribute("OTHER", order: 1, type: typeof(MCL_BORROWER_EXTENSION)));

                overrides.Add(typeof(EMPLOYMENT_EXTENSION), nameof(EMPLOYMENT_EXTENSION.Other),
                    GenerateAttribute("OTHER", order: 1, type: typeof(MCL_EMPLOYMENT_EXTENSION)));

                overrides.Add(typeof(LEGAL_ENTITY_DETAIL_EXTENSION), nameof(LEGAL_ENTITY_DETAIL_EXTENSION.Other),
                    GenerateAttribute("OTHER", order: 1, type: typeof(MCL_LEGAL_ENTITY_DETAIL_EXTENSION)));

                overrides.Add(typeof(SERVICE_EXTENSION), nameof(SERVICE_EXTENSION.Other),
                    GenerateAttribute("OTHER", order: 1, type: typeof(MCL_SERVICE_EXTENSION)));

                overrides.Add(typeof(SERVICE_PRODUCT_DETAIL_EXTENSION), nameof(SERVICE_PRODUCT_DETAIL_EXTENSION.Other),
                    GenerateAttribute("OTHER", order: 1, type: typeof(MCL_SERVICE_PRODUCT_DETAIL_EXTENSION)));

                overrides.Add(typeof(SERVICE_PRODUCT_REQUEST_EXTENSION), nameof(SERVICE_PRODUCT_REQUEST_EXTENSION.Other),
                    GenerateAttribute("OTHER", order: 1, type: typeof(MCL_SERVICE_PRODUCT_REQUEST_EXTENSION)));

                overrides.Add(typeof(VERIFICATION_OF_INCOME_REQUEST_EXTENSION), nameof(VERIFICATION_OF_INCOME_REQUEST_EXTENSION.Other),
                    GenerateAttribute("OTHER", order: 1, type: typeof(MCL_VERIFICATION_OF_INCOME_REQUEST_EXTENSION)));

                overrides.Add(typeof(SERVICE_PRODUCT_FULFILLMENT_DETAIL_EXTENSION), nameof(SERVICE_PRODUCT_FULFILLMENT_DETAIL_EXTENSION.Other),
                    GenerateAttribute("OTHER", order: 1, type: typeof(MCL_SERVICE_PRODUCT_FULFILLMENT_DETAIL_EXTENSION)));

                return overrides;
            }
        }

        /// <summary>
        /// Generates an <see cref="XmlElementAttribute"/> for overriding.
        /// </summary>
        /// <param name="elementName">The element name.</param>
        /// <param name="order">The order integer.</param>
        /// <param name="type">The element type.</param>
        /// <param name="namespace">The namespace of the element.</param>
        /// <returns>An <see cref="XmlAttributes"/> set containing an <see cref="XmlElementAttribute"/>.</returns>
        private static XmlAttributes GenerateAttribute(string elementName, int order, Type type = default(Type), string @namespace = default(string))
        {
            return new XmlAttributes
            {
                XmlElements =
                {
                    new XmlElementAttribute()
                    {
                        ElementName = elementName,
                        Order = order,
                        Type = type,
                        Namespace = @namespace,
                    }
                }
            };
        }
    }
}
