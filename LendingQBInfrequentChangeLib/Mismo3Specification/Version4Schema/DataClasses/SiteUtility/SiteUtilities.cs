namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class SITE_UTILITIES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SiteUtilityListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SITE_UTILITY", Order = 0)]
        public List<SITE_UTILITY> SiteUtilityList { get; set; } = new List<SITE_UTILITY>();
    
        [XmlIgnore]
        public bool SiteUtilityListSpecified
        {
            get { return this.SiteUtilityList != null && this.SiteUtilityList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public SITE_UTILITIES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
