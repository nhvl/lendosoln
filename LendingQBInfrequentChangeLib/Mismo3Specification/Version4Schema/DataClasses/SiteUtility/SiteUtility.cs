namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SITE_UTILITY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SiteUndergroundUtilitiesIndicatorSpecified
                    || this.SiteUtilityAlternativeEnergySourceTypeSpecified
                    || this.SiteUtilityAlternativeEnergySourceTypeOtherDescriptionSpecified
                    || this.SiteUtilityNonPublicOwnershipDescriptionSpecified
                    || this.SiteUtilityNotTypicalDescriptionSpecified
                    || this.SiteUtilityOwnershipTypeSpecified
                    || this.SiteUtilityTypeSpecified
                    || this.SiteUtilityTypeOtherDescriptionSpecified
                    || this.UtilityTypicalIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SiteUndergroundUtilitiesIndicator", Order = 0)]
        public MISMOIndicator SiteUndergroundUtilitiesIndicator { get; set; }
    
        [XmlIgnore]
        public bool SiteUndergroundUtilitiesIndicatorSpecified
        {
            get { return this.SiteUndergroundUtilitiesIndicator != null; }
            set { }
        }
    
        [XmlElement("SiteUtilityAlternativeEnergySourceType", Order = 1)]
        public MISMOEnum<SiteUtilityAlternativeEnergySourceBase> SiteUtilityAlternativeEnergySourceType { get; set; }
    
        [XmlIgnore]
        public bool SiteUtilityAlternativeEnergySourceTypeSpecified
        {
            get { return this.SiteUtilityAlternativeEnergySourceType != null; }
            set { }
        }
    
        [XmlElement("SiteUtilityAlternativeEnergySourceTypeOtherDescription", Order = 2)]
        public MISMOString SiteUtilityAlternativeEnergySourceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool SiteUtilityAlternativeEnergySourceTypeOtherDescriptionSpecified
        {
            get { return this.SiteUtilityAlternativeEnergySourceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("SiteUtilityNonPublicOwnershipDescription", Order = 3)]
        public MISMOString SiteUtilityNonPublicOwnershipDescription { get; set; }
    
        [XmlIgnore]
        public bool SiteUtilityNonPublicOwnershipDescriptionSpecified
        {
            get { return this.SiteUtilityNonPublicOwnershipDescription != null; }
            set { }
        }
    
        [XmlElement("SiteUtilityNotTypicalDescription", Order = 4)]
        public MISMOString SiteUtilityNotTypicalDescription { get; set; }
    
        [XmlIgnore]
        public bool SiteUtilityNotTypicalDescriptionSpecified
        {
            get { return this.SiteUtilityNotTypicalDescription != null; }
            set { }
        }
    
        [XmlElement("SiteUtilityOwnershipType", Order = 5)]
        public MISMOEnum<SiteUtilityOwnershipBase> SiteUtilityOwnershipType { get; set; }
    
        [XmlIgnore]
        public bool SiteUtilityOwnershipTypeSpecified
        {
            get { return this.SiteUtilityOwnershipType != null; }
            set { }
        }
    
        [XmlElement("SiteUtilityType", Order = 6)]
        public MISMOEnum<SiteUtilityBase> SiteUtilityType { get; set; }
    
        [XmlIgnore]
        public bool SiteUtilityTypeSpecified
        {
            get { return this.SiteUtilityType != null; }
            set { }
        }
    
        [XmlElement("SiteUtilityTypeOtherDescription", Order = 7)]
        public MISMOString SiteUtilityTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool SiteUtilityTypeOtherDescriptionSpecified
        {
            get { return this.SiteUtilityTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("UtilityTypicalIndicator", Order = 8)]
        public MISMOIndicator UtilityTypicalIndicator { get; set; }
    
        [XmlIgnore]
        public bool UtilityTypicalIndicatorSpecified
        {
            get { return this.UtilityTypicalIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 9)]
        public SITE_UTILITY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
