namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SITE_ZONING
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PropertyZoningCategoryTypeSpecified
                    || this.PropertyZoningCategoryTypeOtherDescriptionSpecified
                    || this.SiteZoningClassificationDescriptionSpecified
                    || this.SiteZoningClassificationIdentifierSpecified
                    || this.SiteZoningComplianceDescriptionSpecified
                    || this.SiteZoningComplianceTypeSpecified
                    || this.ZoningPermitRebuildToCurrentDensityIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PropertyZoningCategoryType", Order = 0)]
        public MISMOEnum<PropertyZoningCategoryBase> PropertyZoningCategoryType { get; set; }
    
        [XmlIgnore]
        public bool PropertyZoningCategoryTypeSpecified
        {
            get { return this.PropertyZoningCategoryType != null; }
            set { }
        }
    
        [XmlElement("PropertyZoningCategoryTypeOtherDescription", Order = 1)]
        public MISMOString PropertyZoningCategoryTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PropertyZoningCategoryTypeOtherDescriptionSpecified
        {
            get { return this.PropertyZoningCategoryTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("SiteZoningClassificationDescription", Order = 2)]
        public MISMOString SiteZoningClassificationDescription { get; set; }
    
        [XmlIgnore]
        public bool SiteZoningClassificationDescriptionSpecified
        {
            get { return this.SiteZoningClassificationDescription != null; }
            set { }
        }
    
        [XmlElement("SiteZoningClassificationIdentifier", Order = 3)]
        public MISMOIdentifier SiteZoningClassificationIdentifier { get; set; }
    
        [XmlIgnore]
        public bool SiteZoningClassificationIdentifierSpecified
        {
            get { return this.SiteZoningClassificationIdentifier != null; }
            set { }
        }
    
        [XmlElement("SiteZoningComplianceDescription", Order = 4)]
        public MISMOString SiteZoningComplianceDescription { get; set; }
    
        [XmlIgnore]
        public bool SiteZoningComplianceDescriptionSpecified
        {
            get { return this.SiteZoningComplianceDescription != null; }
            set { }
        }
    
        [XmlElement("SiteZoningComplianceType", Order = 5)]
        public MISMOEnum<SiteZoningComplianceBase> SiteZoningComplianceType { get; set; }
    
        [XmlIgnore]
        public bool SiteZoningComplianceTypeSpecified
        {
            get { return this.SiteZoningComplianceType != null; }
            set { }
        }
    
        [XmlElement("ZoningPermitRebuildToCurrentDensityIndicator", Order = 6)]
        public MISMOIndicator ZoningPermitRebuildToCurrentDensityIndicator { get; set; }
    
        [XmlIgnore]
        public bool ZoningPermitRebuildToCurrentDensityIndicatorSpecified
        {
            get { return this.ZoningPermitRebuildToCurrentDensityIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 7)]
        public SITE_ZONING_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
