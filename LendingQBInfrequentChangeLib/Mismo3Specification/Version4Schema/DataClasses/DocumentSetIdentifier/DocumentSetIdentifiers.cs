namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class DOCUMENT_SET_IDENTIFIERS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DocumentSetIdentifierListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DOCUMENT_SET_IDENTIFIER", Order = 0)]
        public List<DOCUMENT_SET_IDENTIFIER> DocumentSetIdentifierList { get; set; } = new List<DOCUMENT_SET_IDENTIFIER>();
    
        [XmlIgnore]
        public bool DocumentSetIdentifierListSpecified
        {
            get { return this.DocumentSetIdentifierList != null && this.DocumentSetIdentifierList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public DOCUMENT_SET_IDENTIFIERS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
