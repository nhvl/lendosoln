namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class DOCUMENTATIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DocumentationListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DOCUMENTATION", Order = 0)]
        public List<DOCUMENTATION> DocumentationList { get; set; } = new List<DOCUMENTATION>();
    
        [XmlIgnore]
        public bool DocumentationListSpecified
        {
            get { return this.DocumentationList != null && this.DocumentationList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public DOCUMENTATIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
