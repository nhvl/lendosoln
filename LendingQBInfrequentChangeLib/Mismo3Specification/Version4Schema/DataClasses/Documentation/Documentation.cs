namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class DOCUMENTATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AssetDocumentationLevelIdentifierSpecified
                    || this.AssetDocumentTypeSpecified
                    || this.AssetDocumentTypeOtherDescriptionSpecified
                    || this.AssetVerificationRangeCountSpecified
                    || this.AssetVerificationRangeTypeSpecified
                    || this.AssetVerificationRangeTypeOtherDescriptionSpecified
                    || this.DocumentationStateTypeSpecified
                    || this.DocumentationStateTypeOtherDescriptionSpecified
                    || this.EmploymentDocumentationLevelIdentifierSpecified
                    || this.EmploymentDocumentTypeSpecified
                    || this.EmploymentDocumentTypeOtherDescriptionSpecified
                    || this.EmploymentVerificationRangeCountSpecified
                    || this.EmploymentVerificationRangeTypeSpecified
                    || this.EmploymentVerificationRangeTypeOtherDescriptionSpecified
                    || this.IncomeDocumentationLevelIdentifierSpecified
                    || this.IncomeDocumentTypeSpecified
                    || this.IncomeDocumentTypeOtherDescriptionSpecified
                    || this.IncomeVerificationRangeCountSpecified
                    || this.IncomeVerificationRangeTypeSpecified
                    || this.IncomeVerificationRangeTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AssetDocumentationLevelIdentifier", Order = 0)]
        public MISMOIdentifier AssetDocumentationLevelIdentifier { get; set; }
    
        [XmlIgnore]
        public bool AssetDocumentationLevelIdentifierSpecified
        {
            get { return this.AssetDocumentationLevelIdentifier != null; }
            set { }
        }
    
        [XmlElement("AssetDocumentType", Order = 1)]
        public MISMOEnum<AssetDocumentBase> AssetDocumentType { get; set; }
    
        [XmlIgnore]
        public bool AssetDocumentTypeSpecified
        {
            get { return this.AssetDocumentType != null; }
            set { }
        }
    
        [XmlElement("AssetDocumentTypeOtherDescription", Order = 2)]
        public MISMOString AssetDocumentTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool AssetDocumentTypeOtherDescriptionSpecified
        {
            get { return this.AssetDocumentTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("AssetVerificationRangeCount", Order = 3)]
        public MISMOCount AssetVerificationRangeCount { get; set; }
    
        [XmlIgnore]
        public bool AssetVerificationRangeCountSpecified
        {
            get { return this.AssetVerificationRangeCount != null; }
            set { }
        }
    
        [XmlElement("AssetVerificationRangeType", Order = 4)]
        public MISMOEnum<VerificationRangeBase> AssetVerificationRangeType { get; set; }
    
        [XmlIgnore]
        public bool AssetVerificationRangeTypeSpecified
        {
            get { return this.AssetVerificationRangeType != null; }
            set { }
        }
    
        [XmlElement("AssetVerificationRangeTypeOtherDescription", Order = 5)]
        public MISMOString AssetVerificationRangeTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool AssetVerificationRangeTypeOtherDescriptionSpecified
        {
            get { return this.AssetVerificationRangeTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("DocumentationStateType", Order = 6)]
        public MISMOEnum<DocumentationStateBase> DocumentationStateType { get; set; }
    
        [XmlIgnore]
        public bool DocumentationStateTypeSpecified
        {
            get { return this.DocumentationStateType != null; }
            set { }
        }
    
        [XmlElement("DocumentationStateTypeOtherDescription", Order = 7)]
        public MISMOString DocumentationStateTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool DocumentationStateTypeOtherDescriptionSpecified
        {
            get { return this.DocumentationStateTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EmploymentDocumentationLevelIdentifier", Order = 8)]
        public MISMOIdentifier EmploymentDocumentationLevelIdentifier { get; set; }
    
        [XmlIgnore]
        public bool EmploymentDocumentationLevelIdentifierSpecified
        {
            get { return this.EmploymentDocumentationLevelIdentifier != null; }
            set { }
        }
    
        [XmlElement("EmploymentDocumentType", Order = 9)]
        public MISMOEnum<EmploymentDocumentBase> EmploymentDocumentType { get; set; }
    
        [XmlIgnore]
        public bool EmploymentDocumentTypeSpecified
        {
            get { return this.EmploymentDocumentType != null; }
            set { }
        }
    
        [XmlElement("EmploymentDocumentTypeOtherDescription", Order = 10)]
        public MISMOString EmploymentDocumentTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool EmploymentDocumentTypeOtherDescriptionSpecified
        {
            get { return this.EmploymentDocumentTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EmploymentVerificationRangeCount", Order = 11)]
        public MISMOCount EmploymentVerificationRangeCount { get; set; }
    
        [XmlIgnore]
        public bool EmploymentVerificationRangeCountSpecified
        {
            get { return this.EmploymentVerificationRangeCount != null; }
            set { }
        }
    
        [XmlElement("EmploymentVerificationRangeType", Order = 12)]
        public MISMOEnum<VerificationRangeBase> EmploymentVerificationRangeType { get; set; }
    
        [XmlIgnore]
        public bool EmploymentVerificationRangeTypeSpecified
        {
            get { return this.EmploymentVerificationRangeType != null; }
            set { }
        }
    
        [XmlElement("EmploymentVerificationRangeTypeOtherDescription", Order = 13)]
        public MISMOString EmploymentVerificationRangeTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool EmploymentVerificationRangeTypeOtherDescriptionSpecified
        {
            get { return this.EmploymentVerificationRangeTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("IncomeDocumentationLevelIdentifier", Order = 14)]
        public MISMOIdentifier IncomeDocumentationLevelIdentifier { get; set; }
    
        [XmlIgnore]
        public bool IncomeDocumentationLevelIdentifierSpecified
        {
            get { return this.IncomeDocumentationLevelIdentifier != null; }
            set { }
        }
    
        [XmlElement("IncomeDocumentType", Order = 15)]
        public MISMOEnum<IncomeDocumentBase> IncomeDocumentType { get; set; }
    
        [XmlIgnore]
        public bool IncomeDocumentTypeSpecified
        {
            get { return this.IncomeDocumentType != null; }
            set { }
        }
    
        [XmlElement("IncomeDocumentTypeOtherDescription", Order = 16)]
        public MISMOString IncomeDocumentTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool IncomeDocumentTypeOtherDescriptionSpecified
        {
            get { return this.IncomeDocumentTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("IncomeVerificationRangeCount", Order = 17)]
        public MISMOCount IncomeVerificationRangeCount { get; set; }
    
        [XmlIgnore]
        public bool IncomeVerificationRangeCountSpecified
        {
            get { return this.IncomeVerificationRangeCount != null; }
            set { }
        }
    
        [XmlElement("IncomeVerificationRangeType", Order = 18)]
        public MISMOEnum<VerificationRangeBase> IncomeVerificationRangeType { get; set; }
    
        [XmlIgnore]
        public bool IncomeVerificationRangeTypeSpecified
        {
            get { return this.IncomeVerificationRangeType != null; }
            set { }
        }
    
        [XmlElement("IncomeVerificationRangeTypeOtherDescription", Order = 19)]
        public MISMOString IncomeVerificationRangeTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool IncomeVerificationRangeTypeOtherDescriptionSpecified
        {
            get { return this.IncomeVerificationRangeTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 20)]
        public DOCUMENTATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
