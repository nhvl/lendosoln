namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MI_PREMIUM_PAYMENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MIPremiumActualPaymentAmountSpecified
                    || this.MIPremiumBlendedPaymentAndTaxAmountSpecified
                    || this.MIPremiumBlendedPaymentAndTaxPercentSpecified
                    || this.MIPremiumEstimatedPaymentAmountSpecified
                    || this.MIPremiumPaymentPaidByTypeSpecified
                    || this.MIPremiumPaymentPaidByTypeOtherDescriptionSpecified
                    || this.MIPremiumPaymentTimingTypeSpecified
                    || this.MIPremiumPaymentTimingTypeOtherDescriptionSpecified
                    || this.PaymentFinancedIndicatorSpecified
                    || this.PaymentIncludedInAPRIndicatorSpecified
                    || this.PaymentIncludedInJurisdictionHighCostIndicatorSpecified
                    || this.PrepaidFinanceChargeIndicatorSpecified
                    || this.RegulationZPointsAndFeesIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MIPremiumActualPaymentAmount", Order = 0)]
        public MISMOAmount MIPremiumActualPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumActualPaymentAmountSpecified
        {
            get { return this.MIPremiumActualPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("MIPremiumBlendedPaymentAndTaxAmount", Order = 1)]
        public MISMOAmount MIPremiumBlendedPaymentAndTaxAmount { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumBlendedPaymentAndTaxAmountSpecified
        {
            get { return this.MIPremiumBlendedPaymentAndTaxAmount != null; }
            set { }
        }
    
        [XmlElement("MIPremiumBlendedPaymentAndTaxPercent", Order = 2)]
        public MISMOPercent MIPremiumBlendedPaymentAndTaxPercent { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumBlendedPaymentAndTaxPercentSpecified
        {
            get { return this.MIPremiumBlendedPaymentAndTaxPercent != null; }
            set { }
        }
    
        [XmlElement("MIPremiumEstimatedPaymentAmount", Order = 3)]
        public MISMOAmount MIPremiumEstimatedPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumEstimatedPaymentAmountSpecified
        {
            get { return this.MIPremiumEstimatedPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("MIPremiumPaymentPaidByType", Order = 4)]
        public MISMOEnum<MIPremiumPaymentPaidByBase> MIPremiumPaymentPaidByType { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumPaymentPaidByTypeSpecified
        {
            get { return this.MIPremiumPaymentPaidByType != null; }
            set { }
        }
    
        [XmlElement("MIPremiumPaymentPaidByTypeOtherDescription", Order = 5)]
        public MISMOString MIPremiumPaymentPaidByTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumPaymentPaidByTypeOtherDescriptionSpecified
        {
            get { return this.MIPremiumPaymentPaidByTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MIPremiumPaymentTimingType", Order = 6)]
        public MISMOEnum<MIPremiumPaymentTimingBase> MIPremiumPaymentTimingType { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumPaymentTimingTypeSpecified
        {
            get { return this.MIPremiumPaymentTimingType != null; }
            set { }
        }
    
        [XmlElement("MIPremiumPaymentTimingTypeOtherDescription", Order = 7)]
        public MISMOString MIPremiumPaymentTimingTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumPaymentTimingTypeOtherDescriptionSpecified
        {
            get { return this.MIPremiumPaymentTimingTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PaymentFinancedIndicator", Order = 8)]
        public MISMOIndicator PaymentFinancedIndicator { get; set; }
    
        [XmlIgnore]
        public bool PaymentFinancedIndicatorSpecified
        {
            get { return this.PaymentFinancedIndicator != null; }
            set { }
        }
    
        [XmlElement("PaymentIncludedInAPRIndicator", Order = 9)]
        public MISMOIndicator PaymentIncludedInAPRIndicator { get; set; }
    
        [XmlIgnore]
        public bool PaymentIncludedInAPRIndicatorSpecified
        {
            get { return this.PaymentIncludedInAPRIndicator != null; }
            set { }
        }
    
        [XmlElement("PaymentIncludedInJurisdictionHighCostIndicator", Order = 10)]
        public MISMOIndicator PaymentIncludedInJurisdictionHighCostIndicator { get; set; }
    
        [XmlIgnore]
        public bool PaymentIncludedInJurisdictionHighCostIndicatorSpecified
        {
            get { return this.PaymentIncludedInJurisdictionHighCostIndicator != null; }
            set { }
        }
    
        [XmlElement("PrepaidFinanceChargeIndicator", Order = 11)]
        public MISMOIndicator PrepaidFinanceChargeIndicator { get; set; }
    
        [XmlIgnore]
        public bool PrepaidFinanceChargeIndicatorSpecified
        {
            get { return this.PrepaidFinanceChargeIndicator != null; }
            set { }
        }
    
        [XmlElement("RegulationZPointsAndFeesIndicator", Order = 12)]
        public MISMOIndicator RegulationZPointsAndFeesIndicator { get; set; }
    
        [XmlIgnore]
        public bool RegulationZPointsAndFeesIndicatorSpecified
        {
            get { return this.RegulationZPointsAndFeesIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 13)]
        public MI_PREMIUM_PAYMENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
