namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class MI_PREMIUM_PAYMENTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MiPremiumPaymentListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MI_PREMIUM_PAYMENT", Order = 0)]
        public List<MI_PREMIUM_PAYMENT> MiPremiumPaymentList { get; set; } = new List<MI_PREMIUM_PAYMENT>();
    
        [XmlIgnore]
        public bool MiPremiumPaymentListSpecified
        {
            get { return this.MiPremiumPaymentList != null && this.MiPremiumPaymentList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public MI_PREMIUM_PAYMENTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
