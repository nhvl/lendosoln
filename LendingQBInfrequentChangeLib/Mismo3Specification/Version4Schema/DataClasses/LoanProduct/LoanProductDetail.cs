namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class LOAN_PRODUCT_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AntiSteeringComparisonTypeSpecified
                    || this.AntiSteeringComparisonTypeOtherDescriptionSpecified
                    || this.BorrowerSelectedAntiSteeringComparisonTypeSpecified
                    || this.BorrowerSelectedAntiSteeringComparisonTypeOtherDescriptionSpecified
                    || this.DiscountPointsTotalAmountSpecified
                    || this.FNMHomeImprovementProductTypeSpecified
                    || this.GFEComparisonTypeSpecified
                    || this.GFEComparisonTypeOtherDescriptionSpecified
                    || this.LoanProductStatusTypeSpecified
                    || this.NegotiatedRequirementDescriptionSpecified
                    || this.OriginationPointsOrFeesTotalAmountSpecified
                    || this.OriginationPointsOrFeesTotalRatePercentSpecified
                    || this.ProductDescriptionSpecified
                    || this.ProductIdentifierSpecified
                    || this.ProductNameSpecified
                    || this.ProductProviderNameSpecified
                    || this.ProductProviderTypeSpecified
                    || this.ProductProviderTypeOtherDescriptionSpecified
                    || this.RefinanceProgramIdentifierSpecified
                    || this.SettlementChargesAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AntiSteeringComparisonType", Order = 0)]
        public MISMOEnum<AntiSteeringComparisonBase> AntiSteeringComparisonType { get; set; }
    
        [XmlIgnore]
        public bool AntiSteeringComparisonTypeSpecified
        {
            get { return this.AntiSteeringComparisonType != null; }
            set { }
        }
    
        [XmlElement("AntiSteeringComparisonTypeOtherDescription", Order = 1)]
        public MISMOString AntiSteeringComparisonTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool AntiSteeringComparisonTypeOtherDescriptionSpecified
        {
            get { return this.AntiSteeringComparisonTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("BorrowerSelectedAntiSteeringComparisonType", Order = 2)]
        public MISMOEnum<AntiSteeringComparisonBase> BorrowerSelectedAntiSteeringComparisonType { get; set; }
    
        [XmlIgnore]
        public bool BorrowerSelectedAntiSteeringComparisonTypeSpecified
        {
            get { return this.BorrowerSelectedAntiSteeringComparisonType != null; }
            set { }
        }
    
        [XmlElement("BorrowerSelectedAntiSteeringComparisonTypeOtherDescription", Order = 3)]
        public MISMOString BorrowerSelectedAntiSteeringComparisonTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool BorrowerSelectedAntiSteeringComparisonTypeOtherDescriptionSpecified
        {
            get { return this.BorrowerSelectedAntiSteeringComparisonTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("DiscountPointsTotalAmount", Order = 4)]
        public MISMOAmount DiscountPointsTotalAmount { get; set; }
    
        [XmlIgnore]
        public bool DiscountPointsTotalAmountSpecified
        {
            get { return this.DiscountPointsTotalAmount != null; }
            set { }
        }
    
        [XmlElement("FNMHomeImprovementProductType", Order = 5)]
        public MISMOEnum<FNMHomeImprovementProductBase> FNMHomeImprovementProductType { get; set; }
    
        [XmlIgnore]
        public bool FNMHomeImprovementProductTypeSpecified
        {
            get { return this.FNMHomeImprovementProductType != null; }
            set { }
        }
    
        [XmlElement("GFEComparisonType", Order = 6)]
        public MISMOEnum<GFEComparisonBase> GFEComparisonType { get; set; }
    
        [XmlIgnore]
        public bool GFEComparisonTypeSpecified
        {
            get { return this.GFEComparisonType != null; }
            set { }
        }
    
        [XmlElement("GFEComparisonTypeOtherDescription", Order = 7)]
        public MISMOString GFEComparisonTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool GFEComparisonTypeOtherDescriptionSpecified
        {
            get { return this.GFEComparisonTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("LoanProductStatusType", Order = 8)]
        public MISMOEnum<LoanProductStatusBase> LoanProductStatusType { get; set; }
    
        [XmlIgnore]
        public bool LoanProductStatusTypeSpecified
        {
            get { return this.LoanProductStatusType != null; }
            set { }
        }
    
        [XmlElement("NegotiatedRequirementDescription", Order = 9)]
        public MISMOString NegotiatedRequirementDescription { get; set; }
    
        [XmlIgnore]
        public bool NegotiatedRequirementDescriptionSpecified
        {
            get { return this.NegotiatedRequirementDescription != null; }
            set { }
        }
    
        [XmlElement("OriginationPointsOrFeesTotalAmount", Order = 10)]
        public MISMOAmount OriginationPointsOrFeesTotalAmount { get; set; }
    
        [XmlIgnore]
        public bool OriginationPointsOrFeesTotalAmountSpecified
        {
            get { return this.OriginationPointsOrFeesTotalAmount != null; }
            set { }
        }
    
        [XmlElement("OriginationPointsOrFeesTotalRatePercent", Order = 11)]
        public MISMOPercent OriginationPointsOrFeesTotalRatePercent { get; set; }
    
        [XmlIgnore]
        public bool OriginationPointsOrFeesTotalRatePercentSpecified
        {
            get { return this.OriginationPointsOrFeesTotalRatePercent != null; }
            set { }
        }
    
        [XmlElement("ProductDescription", Order = 12)]
        public MISMOString ProductDescription { get; set; }
    
        [XmlIgnore]
        public bool ProductDescriptionSpecified
        {
            get { return this.ProductDescription != null; }
            set { }
        }
    
        [XmlElement("ProductIdentifier", Order = 13)]
        public MISMOIdentifier ProductIdentifier { get; set; }
    
        [XmlIgnore]
        public bool ProductIdentifierSpecified
        {
            get { return this.ProductIdentifier != null; }
            set { }
        }
    
        [XmlElement("ProductName", Order = 14)]
        public MISMOString ProductName { get; set; }
    
        [XmlIgnore]
        public bool ProductNameSpecified
        {
            get { return this.ProductName != null; }
            set { }
        }
    
        [XmlElement("ProductProviderName", Order = 15)]
        public MISMOString ProductProviderName { get; set; }
    
        [XmlIgnore]
        public bool ProductProviderNameSpecified
        {
            get { return this.ProductProviderName != null; }
            set { }
        }
    
        [XmlElement("ProductProviderType", Order = 16)]
        public MISMOEnum<ProductProviderBase> ProductProviderType { get; set; }
    
        [XmlIgnore]
        public bool ProductProviderTypeSpecified
        {
            get { return this.ProductProviderType != null; }
            set { }
        }
    
        [XmlElement("ProductProviderTypeOtherDescription", Order = 17)]
        public MISMOString ProductProviderTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ProductProviderTypeOtherDescriptionSpecified
        {
            get { return this.ProductProviderTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("RefinanceProgramIdentifier", Order = 18)]
        public MISMOIdentifier RefinanceProgramIdentifier { get; set; }
    
        [XmlIgnore]
        public bool RefinanceProgramIdentifierSpecified
        {
            get { return this.RefinanceProgramIdentifier != null; }
            set { }
        }
    
        [XmlElement("SettlementChargesAmount", Order = 19)]
        public MISMOAmount SettlementChargesAmount { get; set; }
    
        [XmlIgnore]
        public bool SettlementChargesAmountSpecified
        {
            get { return this.SettlementChargesAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 20)]
        public LOAN_PRODUCT_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
