namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class LOAN_PRODUCT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DisqualificationReasonsSpecified
                    || this.LoanPriceQuotesSpecified
                    || this.LoanProductDetailSpecified
                    || this.LocksSpecified
                    || this.ProductCategorySpecified
                    || this.ProductComponentsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DISQUALIFICATION_REASONS", Order = 0)]
        public DISQUALIFICATION_REASONS DisqualificationReasons { get; set; }
    
        [XmlIgnore]
        public bool DisqualificationReasonsSpecified
        {
            get { return this.DisqualificationReasons != null && this.DisqualificationReasons.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("LOAN_PRICE_QUOTES", Order = 1)]
        public LOAN_PRICE_QUOTES LoanPriceQuotes { get; set; }
    
        [XmlIgnore]
        public bool LoanPriceQuotesSpecified
        {
            get { return this.LoanPriceQuotes != null && this.LoanPriceQuotes.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("LOAN_PRODUCT_DETAIL", Order = 2)]
        public LOAN_PRODUCT_DETAIL LoanProductDetail { get; set; }
    
        [XmlIgnore]
        public bool LoanProductDetailSpecified
        {
            get { return this.LoanProductDetail != null && this.LoanProductDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("LOCKS", Order = 3)]
        public LOCKS Locks { get; set; }
    
        [XmlIgnore]
        public bool LocksSpecified
        {
            get { return this.Locks != null && this.Locks.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PRODUCT_CATEGORY", Order = 4)]
        public PRODUCT_CATEGORY ProductCategory { get; set; }
    
        [XmlIgnore]
        public bool ProductCategorySpecified
        {
            get { return this.ProductCategory != null && this.ProductCategory.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PRODUCT_COMPONENTS", Order = 5)]
        public PRODUCT_COMPONENTS ProductComponents { get; set; }
    
        [XmlIgnore]
        public bool ProductComponentsSpecified
        {
            get { return this.ProductComponents != null && this.ProductComponents.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 6)]
        public LOAN_PRODUCT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
