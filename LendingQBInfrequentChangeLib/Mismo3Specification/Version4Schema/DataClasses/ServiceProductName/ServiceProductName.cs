namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SERVICE_PRODUCT_NAME
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ServiceProductNameAddOnsSpecified
                    || this.ServiceProductNameDetailSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SERVICE_PRODUCT_NAME_ADD_ONS", Order = 0)]
        public SERVICE_PRODUCT_NAME_ADD_ONS ServiceProductNameAddOns { get; set; }
    
        [XmlIgnore]
        public bool ServiceProductNameAddOnsSpecified
        {
            get { return this.ServiceProductNameAddOns != null && this.ServiceProductNameAddOns.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SERVICE_PRODUCT_NAME_DETAIL", Order = 1)]
        public SERVICE_PRODUCT_NAME_DETAIL ServiceProductNameDetail { get; set; }
    
        [XmlIgnore]
        public bool ServiceProductNameDetailSpecified
        {
            get { return this.ServiceProductNameDetail != null && this.ServiceProductNameDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public SERVICE_PRODUCT_NAME_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
