namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SERVICE_PRODUCT_NAME_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ServiceProductNameDescriptionSpecified
                    || this.ServiceProductNameIdentifierSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ServiceProductNameDescription", Order = 0)]
        public MISMOString ServiceProductNameDescription { get; set; }
    
        [XmlIgnore]
        public bool ServiceProductNameDescriptionSpecified
        {
            get { return this.ServiceProductNameDescription != null; }
            set { }
        }
    
        [XmlElement("ServiceProductNameIdentifier", Order = 1)]
        public MISMOIdentifier ServiceProductNameIdentifier { get; set; }
    
        [XmlIgnore]
        public bool ServiceProductNameIdentifierSpecified
        {
            get { return this.ServiceProductNameIdentifier != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public SERVICE_PRODUCT_NAME_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
