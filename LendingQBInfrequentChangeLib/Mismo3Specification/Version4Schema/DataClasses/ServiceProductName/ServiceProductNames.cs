namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class SERVICE_PRODUCT_NAMES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ServiceProductNameListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SERVICE_PRODUCT_NAME", Order = 0)]
        public List<SERVICE_PRODUCT_NAME> ServiceProductNameList { get; set; } = new List<SERVICE_PRODUCT_NAME>();
    
        [XmlIgnore]
        public bool ServiceProductNameListSpecified
        {
            get { return this.ServiceProductNameList != null && this.ServiceProductNameList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public SERVICE_PRODUCT_NAMES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
