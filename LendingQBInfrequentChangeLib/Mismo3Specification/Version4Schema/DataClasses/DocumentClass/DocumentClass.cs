namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class DOCUMENT_CLASS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DocumentTypeSpecified
                    || this.DocumentTypeOtherDescriptionSpecified
                    || this.RecordingJurisdictionDocumentCodeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DocumentType", Order = 0)]
        public MISMOEnum<DocumentBase> DocumentType { get; set; }
    
        [XmlIgnore]
        public bool DocumentTypeSpecified
        {
            get { return this.DocumentType != null; }
            set { }
        }
    
        [XmlElement("DocumentTypeOtherDescription", Order = 1)]
        public MISMOString DocumentTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool DocumentTypeOtherDescriptionSpecified
        {
            get { return this.DocumentTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("RecordingJurisdictionDocumentCode", Order = 2)]
        public MISMOCode RecordingJurisdictionDocumentCode { get; set; }
    
        [XmlIgnore]
        public bool RecordingJurisdictionDocumentCodeSpecified
        {
            get { return this.RecordingJurisdictionDocumentCode != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public DOCUMENT_CLASS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
