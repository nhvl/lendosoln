namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class DECLARATION_EXPLANATIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DeclarationExplanationListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DECLARATION_EXPLANATION", Order = 0)]
        public List<DECLARATION_EXPLANATION> DeclarationExplanationList { get; set; } = new List<DECLARATION_EXPLANATION>();
    
        [XmlIgnore]
        public bool DeclarationExplanationListSpecified
        {
            get { return this.DeclarationExplanationList != null && this.DeclarationExplanationList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public DECLARATION_EXPLANATIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
