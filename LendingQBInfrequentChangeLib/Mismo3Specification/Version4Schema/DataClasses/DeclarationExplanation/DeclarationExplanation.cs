namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class DECLARATION_EXPLANATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DeclarationExplanationDescriptionSpecified
                    || this.DeclarationExplanationTypeSpecified
                    || this.DeclarationExplanationTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DeclarationExplanationDescription", Order = 0)]
        public MISMOString DeclarationExplanationDescription { get; set; }
    
        [XmlIgnore]
        public bool DeclarationExplanationDescriptionSpecified
        {
            get { return this.DeclarationExplanationDescription != null; }
            set { }
        }
    
        [XmlElement("DeclarationExplanationType", Order = 1)]
        public MISMOEnum<DeclarationExplanationBase> DeclarationExplanationType { get; set; }
    
        [XmlIgnore]
        public bool DeclarationExplanationTypeSpecified
        {
            get { return this.DeclarationExplanationType != null; }
            set { }
        }
    
        [XmlElement("DeclarationExplanationTypeOtherDescription", Order = 2)]
        public MISMOString DeclarationExplanationTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool DeclarationExplanationTypeOtherDescriptionSpecified
        {
            get { return this.DeclarationExplanationTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public DECLARATION_EXPLANATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
