namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MI_VALIDATION_RESPONSE_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MICancellationDateSpecified
                    || this.MICertificateIdentifierSpecified
                    || this.MIPolicyStatusTypeSpecified
                    || this.MIPolicyStatusTypeOtherDescriptionSpecified
                    || this.MIPremiumFinancedAmountSpecified
                    || this.ReasonForMIPolicyCancellationTypeSpecified
                    || this.ReasonForMIPolicyCancellationTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MICancellationDate", Order = 0)]
        public MISMODate MICancellationDate { get; set; }
    
        [XmlIgnore]
        public bool MICancellationDateSpecified
        {
            get { return this.MICancellationDate != null; }
            set { }
        }
    
        [XmlElement("MICertificateIdentifier", Order = 1)]
        public MISMOIdentifier MICertificateIdentifier { get; set; }
    
        [XmlIgnore]
        public bool MICertificateIdentifierSpecified
        {
            get { return this.MICertificateIdentifier != null; }
            set { }
        }
    
        [XmlElement("MIPolicyStatusType", Order = 2)]
        public MISMOEnum<MIPolicyStatusBase> MIPolicyStatusType { get; set; }
    
        [XmlIgnore]
        public bool MIPolicyStatusTypeSpecified
        {
            get { return this.MIPolicyStatusType != null; }
            set { }
        }
    
        [XmlElement("MIPolicyStatusTypeOtherDescription", Order = 3)]
        public MISMOString MIPolicyStatusTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MIPolicyStatusTypeOtherDescriptionSpecified
        {
            get { return this.MIPolicyStatusTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MIPremiumFinancedAmount", Order = 4)]
        public MISMOAmount MIPremiumFinancedAmount { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumFinancedAmountSpecified
        {
            get { return this.MIPremiumFinancedAmount != null; }
            set { }
        }
    
        [XmlElement("ReasonForMIPolicyCancellationType", Order = 5)]
        public MISMOEnum<ReasonForMIPolicyCancellationBase> ReasonForMIPolicyCancellationType { get; set; }
    
        [XmlIgnore]
        public bool ReasonForMIPolicyCancellationTypeSpecified
        {
            get { return this.ReasonForMIPolicyCancellationType != null; }
            set { }
        }
    
        [XmlElement("ReasonForMIPolicyCancellationTypeOtherDescription", Order = 6)]
        public MISMOString ReasonForMIPolicyCancellationTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ReasonForMIPolicyCancellationTypeOtherDescriptionSpecified
        {
            get { return this.ReasonForMIPolicyCancellationTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 7)]
        public MI_VALIDATION_RESPONSE_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
