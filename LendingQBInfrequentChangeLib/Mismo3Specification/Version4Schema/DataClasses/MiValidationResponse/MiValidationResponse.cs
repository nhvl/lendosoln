namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MI_VALIDATION_RESPONSE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LoanIdentifiersSpecified
                    || this.MiProductSpecified
                    || this.MiValidationResponseDetailSpecified
                    || this.PartiesSpecified
                    || this.PropertiesSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LOAN_IDENTIFIERS", Order = 0)]
        public LOAN_IDENTIFIERS LoanIdentifiers { get; set; }
    
        [XmlIgnore]
        public bool LoanIdentifiersSpecified
        {
            get { return this.LoanIdentifiers != null && this.LoanIdentifiers.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("MI_PRODUCT", Order = 1)]
        public MI_PRODUCT MiProduct { get; set; }
    
        [XmlIgnore]
        public bool MiProductSpecified
        {
            get { return this.MiProduct != null && this.MiProduct.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("MI_VALIDATION_RESPONSE_DETAIL", Order = 2)]
        public MI_VALIDATION_RESPONSE_DETAIL MiValidationResponseDetail { get; set; }
    
        [XmlIgnore]
        public bool MiValidationResponseDetailSpecified
        {
            get { return this.MiValidationResponseDetail != null && this.MiValidationResponseDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PARTIES", Order = 3)]
        public PARTIES Parties { get; set; }
    
        [XmlIgnore]
        public bool PartiesSpecified
        {
            get { return this.Parties != null && this.Parties.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PROPERTIES", Order = 4)]
        public PROPERTIES Properties { get; set; }
    
        [XmlIgnore]
        public bool PropertiesSpecified
        {
            get { return this.Properties != null && this.Properties.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public MI_VALIDATION_RESPONSE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
