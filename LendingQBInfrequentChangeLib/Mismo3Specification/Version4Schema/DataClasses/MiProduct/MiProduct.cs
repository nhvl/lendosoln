namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MI_PRODUCT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MICoveragePercentSpecified
                    || this.MIDurationTypeSpecified
                    || this.MIDurationTypeOtherDescriptionSpecified
                    || this.MIInitialPremiumAtClosingTypeSpecified
                    || this.MIInitialPremiumAtClosingTypeOtherDescriptionSpecified
                    || this.MILenderSpecialProgramDescriptionSpecified
                    || this.MIPremiumCalculationTypeSpecified
                    || this.MIPremiumCalculationTypeOtherDescriptionSpecified
                    || this.MIPremiumFinancedIndicatorSpecified
                    || this.MIPremiumPaymentTypeSpecified
                    || this.MIPremiumPaymentTypeOtherDescriptionSpecified
                    || this.MIPremiumRatePlanTypeSpecified
                    || this.MIPremiumRatePlanTypeOtherDescriptionSpecified
                    || this.MIPremiumRefundableTypeSpecified
                    || this.MIPremiumRefundableTypeOtherDescriptionSpecified
                    || this.MIPremiumSourceTypeSpecified
                    || this.MIPremiumSourceTypeOtherDescriptionSpecified
                    || this.MIPremiumUpfrontPercentSpecified
                    || this.MISpecialPricingDescriptionSpecified
                    || this.MISubPrimeProgramTypeSpecified
                    || this.MISubPrimeProgramTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MICoveragePercent", Order = 0)]
        public MISMOPercent MICoveragePercent { get; set; }
    
        [XmlIgnore]
        public bool MICoveragePercentSpecified
        {
            get { return this.MICoveragePercent != null; }
            set { }
        }
    
        [XmlElement("MIDurationType", Order = 1)]
        public MISMOEnum<MIDurationBase> MIDurationType { get; set; }
    
        [XmlIgnore]
        public bool MIDurationTypeSpecified
        {
            get { return this.MIDurationType != null; }
            set { }
        }
    
        [XmlElement("MIDurationTypeOtherDescription", Order = 2)]
        public MISMOString MIDurationTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MIDurationTypeOtherDescriptionSpecified
        {
            get { return this.MIDurationTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MIInitialPremiumAtClosingType", Order = 3)]
        public MISMOEnum<MIInitialPremiumAtClosingBase> MIInitialPremiumAtClosingType { get; set; }
    
        [XmlIgnore]
        public bool MIInitialPremiumAtClosingTypeSpecified
        {
            get { return this.MIInitialPremiumAtClosingType != null; }
            set { }
        }
    
        [XmlElement("MIInitialPremiumAtClosingTypeOtherDescription", Order = 4)]
        public MISMOString MIInitialPremiumAtClosingTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MIInitialPremiumAtClosingTypeOtherDescriptionSpecified
        {
            get { return this.MIInitialPremiumAtClosingTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MILenderSpecialProgramDescription", Order = 5)]
        public MISMOString MILenderSpecialProgramDescription { get; set; }
    
        [XmlIgnore]
        public bool MILenderSpecialProgramDescriptionSpecified
        {
            get { return this.MILenderSpecialProgramDescription != null; }
            set { }
        }
    
        [XmlElement("MIPremiumCalculationType", Order = 6)]
        public MISMOEnum<MIPremiumCalculationBase> MIPremiumCalculationType { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumCalculationTypeSpecified
        {
            get { return this.MIPremiumCalculationType != null; }
            set { }
        }
    
        [XmlElement("MIPremiumCalculationTypeOtherDescription", Order = 7)]
        public MISMOString MIPremiumCalculationTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumCalculationTypeOtherDescriptionSpecified
        {
            get { return this.MIPremiumCalculationTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MIPremiumFinancedIndicator", Order = 8)]
        public MISMOIndicator MIPremiumFinancedIndicator { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumFinancedIndicatorSpecified
        {
            get { return this.MIPremiumFinancedIndicator != null; }
            set { }
        }
    
        [XmlElement("MIPremiumPaymentType", Order = 9)]
        public MISMOEnum<MIPremiumPaymentBase> MIPremiumPaymentType { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumPaymentTypeSpecified
        {
            get { return this.MIPremiumPaymentType != null; }
            set { }
        }
    
        [XmlElement("MIPremiumPaymentTypeOtherDescription", Order = 10)]
        public MISMOString MIPremiumPaymentTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumPaymentTypeOtherDescriptionSpecified
        {
            get { return this.MIPremiumPaymentTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MIPremiumRatePlanType", Order = 11)]
        public MISMOEnum<MIPremiumRatePlanBase> MIPremiumRatePlanType { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumRatePlanTypeSpecified
        {
            get { return this.MIPremiumRatePlanType != null; }
            set { }
        }
    
        [XmlElement("MIPremiumRatePlanTypeOtherDescription", Order = 12)]
        public MISMOString MIPremiumRatePlanTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumRatePlanTypeOtherDescriptionSpecified
        {
            get { return this.MIPremiumRatePlanTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MIPremiumRefundableType", Order = 13)]
        public MISMOEnum<MIPremiumRefundableBase> MIPremiumRefundableType { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumRefundableTypeSpecified
        {
            get { return this.MIPremiumRefundableType != null; }
            set { }
        }
    
        [XmlElement("MIPremiumRefundableTypeOtherDescription", Order = 14)]
        public MISMOString MIPremiumRefundableTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumRefundableTypeOtherDescriptionSpecified
        {
            get { return this.MIPremiumRefundableTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MIPremiumSourceType", Order = 15)]
        public MISMOEnum<MIPremiumSourceBase> MIPremiumSourceType { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumSourceTypeSpecified
        {
            get { return this.MIPremiumSourceType != null; }
            set { }
        }
    
        [XmlElement("MIPremiumSourceTypeOtherDescription", Order = 16)]
        public MISMOString MIPremiumSourceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumSourceTypeOtherDescriptionSpecified
        {
            get { return this.MIPremiumSourceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MIPremiumUpfrontPercent", Order = 17)]
        public MISMOPercent MIPremiumUpfrontPercent { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumUpfrontPercentSpecified
        {
            get { return this.MIPremiumUpfrontPercent != null; }
            set { }
        }
    
        [XmlElement("MISpecialPricingDescription", Order = 18)]
        public MISMOString MISpecialPricingDescription { get; set; }
    
        [XmlIgnore]
        public bool MISpecialPricingDescriptionSpecified
        {
            get { return this.MISpecialPricingDescription != null; }
            set { }
        }
    
        [XmlElement("MISubPrimeProgramType", Order = 19)]
        public MISMOEnum<MISubPrimeProgramBase> MISubPrimeProgramType { get; set; }
    
        [XmlIgnore]
        public bool MISubPrimeProgramTypeSpecified
        {
            get { return this.MISubPrimeProgramType != null; }
            set { }
        }
    
        [XmlElement("MISubPrimeProgramTypeOtherDescription", Order = 20)]
        public MISMOString MISubPrimeProgramTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MISubPrimeProgramTypeOtherDescriptionSpecified
        {
            get { return this.MISubPrimeProgramTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 21)]
        public MI_PRODUCT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
