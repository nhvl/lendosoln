namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class MI_PRODUCTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MiPremiumTaxesSpecified
                    || this.MiPremiumsSpecified
                    || this.MiProductListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MI_PREMIUM_TAXES", Order = 0)]
        public MI_PREMIUM_TAXES MiPremiumTaxes { get; set; }
    
        [XmlIgnore]
        public bool MiPremiumTaxesSpecified
        {
            get { return this.MiPremiumTaxes != null && this.MiPremiumTaxes.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("MI_PREMIUMS", Order = 1)]
        public MI_PREMIUMS MiPremiums { get; set; }
    
        [XmlIgnore]
        public bool MiPremiumsSpecified
        {
            get { return this.MiPremiums != null && this.MiPremiums.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("MI_PRODUCT", Order = 2)]
        public List<MI_PRODUCT> MiProductList { get; set; } = new List<MI_PRODUCT>();
    
        [XmlIgnore]
        public bool MiProductListSpecified
        {
            get { return this.MiProductList != null && this.MiProductList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public MI_PRODUCTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
