namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class HMDA_LOAN_DENIALS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.HmdaLoanDenialListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("HMDA_LOAN_DENIAL", Order = 0)]
        public List<HMDA_LOAN_DENIAL> HmdaLoanDenialList { get; set; } = new List<HMDA_LOAN_DENIAL>();
    
        [XmlIgnore]
        public bool HmdaLoanDenialListSpecified
        {
            get { return this.HmdaLoanDenialList != null && this.HmdaLoanDenialList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public HMDA_LOAN_DENIALS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
