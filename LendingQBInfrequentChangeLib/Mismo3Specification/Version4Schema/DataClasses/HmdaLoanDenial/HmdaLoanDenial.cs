namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class HMDA_LOAN_DENIAL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.HMDAReasonForDenialTypeSpecified
                    || this.HMDAReasonForDenialTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("HMDAReasonForDenialType", Order = 0)]
        public MISMOEnum<HMDAReasonForDenialBase> HMDAReasonForDenialType { get; set; }
    
        [XmlIgnore]
        public bool HMDAReasonForDenialTypeSpecified
        {
            get { return this.HMDAReasonForDenialType != null; }
            set { }
        }
    
        [XmlElement("HMDAReasonForDenialTypeOtherDescription", Order = 1)]
        public MISMOString HMDAReasonForDenialTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool HMDAReasonForDenialTypeOtherDescriptionSpecified
        {
            get { return this.HMDAReasonForDenialTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public HMDA_LOAN_DENIAL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
