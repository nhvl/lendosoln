namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class DOCUMENT_FORM_CONTRIBUTOR
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DocumentFormContributorCodeSpecified
                    || this.DocumentFormContributorDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DocumentFormContributorCode", Order = 0)]
        public MISMOCode DocumentFormContributorCode { get; set; }
    
        [XmlIgnore]
        public bool DocumentFormContributorCodeSpecified
        {
            get { return this.DocumentFormContributorCode != null; }
            set { }
        }
    
        [XmlElement("DocumentFormContributorDescription", Order = 1)]
        public MISMOString DocumentFormContributorDescription { get; set; }
    
        [XmlIgnore]
        public bool DocumentFormContributorDescriptionSpecified
        {
            get { return this.DocumentFormContributorDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public DOCUMENT_FORM_CONTRIBUTOR_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
