namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class DOCUMENT_FORM_CONTRIBUTORS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DocumentFormContributorListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DOCUMENT_FORM_CONTRIBUTOR", Order = 0)]
        public List<DOCUMENT_FORM_CONTRIBUTOR> DocumentFormContributorList { get; set; } = new List<DOCUMENT_FORM_CONTRIBUTOR>();
    
        [XmlIgnore]
        public bool DocumentFormContributorListSpecified
        {
            get { return this.DocumentFormContributorList != null && this.DocumentFormContributorList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public DOCUMENT_FORM_CONTRIBUTORS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
