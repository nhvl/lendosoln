namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ENVIRONMENTAL_CONDITION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EnvironmentalConditionIdentifiedDateSpecified
                    || this.EnvironmentalConditionIdentifiedDescriptionSpecified
                    || this.EnvironmentalConditionTypeSpecified
                    || this.EnvironmentalConditionTypeOtherDescriptionSpecified
                    || this.IndoorAirQualityMitigationTypeSpecified
                    || this.IndoorAirQualityMitigationTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("EnvironmentalConditionIdentifiedDate", Order = 0)]
        public MISMODate EnvironmentalConditionIdentifiedDate { get; set; }
    
        [XmlIgnore]
        public bool EnvironmentalConditionIdentifiedDateSpecified
        {
            get { return this.EnvironmentalConditionIdentifiedDate != null; }
            set { }
        }
    
        [XmlElement("EnvironmentalConditionIdentifiedDescription", Order = 1)]
        public MISMOString EnvironmentalConditionIdentifiedDescription { get; set; }
    
        [XmlIgnore]
        public bool EnvironmentalConditionIdentifiedDescriptionSpecified
        {
            get { return this.EnvironmentalConditionIdentifiedDescription != null; }
            set { }
        }
    
        [XmlElement("EnvironmentalConditionType", Order = 2)]
        public MISMOEnum<EnvironmentalConditionBase> EnvironmentalConditionType { get; set; }
    
        [XmlIgnore]
        public bool EnvironmentalConditionTypeSpecified
        {
            get { return this.EnvironmentalConditionType != null; }
            set { }
        }
    
        [XmlElement("EnvironmentalConditionTypeOtherDescription", Order = 3)]
        public MISMOString EnvironmentalConditionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool EnvironmentalConditionTypeOtherDescriptionSpecified
        {
            get { return this.EnvironmentalConditionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("IndoorAirQualityMitigationType", Order = 4)]
        public MISMOEnum<IndoorAirQualityMitigationBase> IndoorAirQualityMitigationType { get; set; }
    
        [XmlIgnore]
        public bool IndoorAirQualityMitigationTypeSpecified
        {
            get { return this.IndoorAirQualityMitigationType != null; }
            set { }
        }
    
        [XmlElement("IndoorAirQualityMitigationTypeOtherDescription", Order = 5)]
        public MISMOString IndoorAirQualityMitigationTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool IndoorAirQualityMitigationTypeOtherDescriptionSpecified
        {
            get { return this.IndoorAirQualityMitigationTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 6)]
        public ENVIRONMENTAL_CONDITION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
