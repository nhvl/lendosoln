namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class ENVIRONMENTAL_CONDITIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EnvironmentalConditionListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ENVIRONMENTAL_CONDITION", Order = 0)]
        public List<ENVIRONMENTAL_CONDITION> EnvironmentalConditionList { get; set; } = new List<ENVIRONMENTAL_CONDITION>();
    
        [XmlIgnore]
        public bool EnvironmentalConditionListSpecified
        {
            get { return this.EnvironmentalConditionList != null && this.EnvironmentalConditionList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public ENVIRONMENTAL_CONDITIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
