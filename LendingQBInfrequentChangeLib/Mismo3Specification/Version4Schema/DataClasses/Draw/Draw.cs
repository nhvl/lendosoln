namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class DRAW
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DrawActivitiesSpecified
                    || this.DrawRuleSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DRAW_ACTIVITIES", Order = 0)]
        public DRAW_ACTIVITIES DrawActivities { get; set; }
    
        [XmlIgnore]
        public bool DrawActivitiesSpecified
        {
            get { return this.DrawActivities != null && this.DrawActivities.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("DRAW_RULE", Order = 1)]
        public DRAW_RULE DrawRule { get; set; }
    
        [XmlIgnore]
        public bool DrawRuleSpecified
        {
            get { return this.DrawRule != null && this.DrawRule.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public DRAW_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
