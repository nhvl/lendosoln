namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class HMDA_RACE_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.HMDARaceTypeSpecified
                    || this.HMDARaceTypeAdditionalDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("HMDARaceType", Order = 0)]
        public MISMOEnum<HMDARaceBase> HMDARaceType { get; set; }
    
        [XmlIgnore]
        public bool HMDARaceTypeSpecified
        {
            get { return this.HMDARaceType != null; }
            set { }
        }
    
        [XmlElement("HMDARaceTypeAdditionalDescription", Order = 1)]
        public MISMOString HMDARaceTypeAdditionalDescription { get; set; }
    
        [XmlIgnore]
        public bool HMDARaceTypeAdditionalDescriptionSpecified
        {
            get { return this.HMDARaceTypeAdditionalDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public HMDA_RACE_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
