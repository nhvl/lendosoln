namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class HMDA_RACE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.HmdaRaceDesignationsSpecified
                    || this.HmdaRaceDetailSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("HMDA_RACE_DESIGNATIONS", Order = 0)]
        public HMDA_RACE_DESIGNATIONS HmdaRaceDesignations { get; set; }
    
        [XmlIgnore]
        public bool HmdaRaceDesignationsSpecified
        {
            get { return this.HmdaRaceDesignations != null && this.HmdaRaceDesignations.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("HMDA_RACE_DETAIL", Order = 1)]
        public HMDA_RACE_DETAIL HmdaRaceDetail { get; set; }
    
        [XmlIgnore]
        public bool HmdaRaceDetailSpecified
        {
            get { return this.HmdaRaceDetail != null && this.HmdaRaceDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public HMDA_RACE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
