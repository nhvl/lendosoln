namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class HMDA_RACES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.HmdaRaceListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("HMDA_RACE", Order = 0)]
        public List<HMDA_RACE> HmdaRaceList { get; set; } = new List<HMDA_RACE>();
    
        [XmlIgnore]
        public bool HmdaRaceListSpecified
        {
            get { return this.HmdaRaceList != null && this.HmdaRaceList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public HMDA_RACES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
