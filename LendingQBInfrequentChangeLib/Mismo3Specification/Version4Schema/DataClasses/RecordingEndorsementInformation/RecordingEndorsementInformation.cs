namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class RECORDING_ENDORSEMENT_INFORMATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CountyOfRecordationNameSpecified
                    || this.FirstPageNumberValueSpecified
                    || this.InstrumentNumberIdentifierSpecified
                    || this.LastPageNumberValueSpecified
                    || this.OfficeOfRecordationTypeSpecified
                    || this.OfficeOfRecordationTypeOtherDescriptionSpecified
                    || this.PRIARecordingJurisdictionIdentifierSpecified
                    || this.RecordedDatetimeSpecified
                    || this.RecordingEndorsementIdentifierSpecified
                    || this.RecordingEndorsementTypeSpecified
                    || this.RecordingEndorsementTypeOtherDescriptionSpecified
                    || this.RecordingOfficersNameSpecified
                    || this.RecordingPagesCountSpecified
                    || this.StateOfRecordationNameSpecified
                    || this.VolumeIdentifierSpecified
                    || this.VolumeTypeSpecified
                    || this.VolumeTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CountyOfRecordationName", Order = 0)]
        public MISMOString CountyOfRecordationName { get; set; }
    
        [XmlIgnore]
        public bool CountyOfRecordationNameSpecified
        {
            get { return this.CountyOfRecordationName != null; }
            set { }
        }
    
        [XmlElement("FirstPageNumberValue", Order = 1)]
        public MISMOValue FirstPageNumberValue { get; set; }
    
        [XmlIgnore]
        public bool FirstPageNumberValueSpecified
        {
            get { return this.FirstPageNumberValue != null; }
            set { }
        }
    
        [XmlElement("InstrumentNumberIdentifier", Order = 2)]
        public MISMOIdentifier InstrumentNumberIdentifier { get; set; }
    
        [XmlIgnore]
        public bool InstrumentNumberIdentifierSpecified
        {
            get { return this.InstrumentNumberIdentifier != null; }
            set { }
        }
    
        [XmlElement("LastPageNumberValue", Order = 3)]
        public MISMOValue LastPageNumberValue { get; set; }
    
        [XmlIgnore]
        public bool LastPageNumberValueSpecified
        {
            get { return this.LastPageNumberValue != null; }
            set { }
        }
    
        [XmlElement("OfficeOfRecordationType", Order = 4)]
        public MISMOEnum<OfficeOfRecordationBase> OfficeOfRecordationType { get; set; }
    
        [XmlIgnore]
        public bool OfficeOfRecordationTypeSpecified
        {
            get { return this.OfficeOfRecordationType != null; }
            set { }
        }
    
        [XmlElement("OfficeOfRecordationTypeOtherDescription", Order = 5)]
        public MISMOString OfficeOfRecordationTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool OfficeOfRecordationTypeOtherDescriptionSpecified
        {
            get { return this.OfficeOfRecordationTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PRIARecordingJurisdictionIdentifier", Order = 6)]
        public MISMOIdentifier PRIARecordingJurisdictionIdentifier { get; set; }
    
        [XmlIgnore]
        public bool PRIARecordingJurisdictionIdentifierSpecified
        {
            get { return this.PRIARecordingJurisdictionIdentifier != null; }
            set { }
        }
    
        [XmlElement("RecordedDatetime", Order = 7)]
        public MISMODatetime RecordedDatetime { get; set; }
    
        [XmlIgnore]
        public bool RecordedDatetimeSpecified
        {
            get { return this.RecordedDatetime != null; }
            set { }
        }
    
        [XmlElement("RecordingEndorsementIdentifier", Order = 8)]
        public MISMOIdentifier RecordingEndorsementIdentifier { get; set; }
    
        [XmlIgnore]
        public bool RecordingEndorsementIdentifierSpecified
        {
            get { return this.RecordingEndorsementIdentifier != null; }
            set { }
        }
    
        [XmlElement("RecordingEndorsementType", Order = 9)]
        public MISMOEnum<RecordingEndorsementBase> RecordingEndorsementType { get; set; }
    
        [XmlIgnore]
        public bool RecordingEndorsementTypeSpecified
        {
            get { return this.RecordingEndorsementType != null; }
            set { }
        }
    
        [XmlElement("RecordingEndorsementTypeOtherDescription", Order = 10)]
        public MISMOString RecordingEndorsementTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool RecordingEndorsementTypeOtherDescriptionSpecified
        {
            get { return this.RecordingEndorsementTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("RecordingOfficersName", Order = 11)]
        public MISMOString RecordingOfficersName { get; set; }
    
        [XmlIgnore]
        public bool RecordingOfficersNameSpecified
        {
            get { return this.RecordingOfficersName != null; }
            set { }
        }
    
        [XmlElement("RecordingPagesCount", Order = 12)]
        public MISMOCount RecordingPagesCount { get; set; }
    
        [XmlIgnore]
        public bool RecordingPagesCountSpecified
        {
            get { return this.RecordingPagesCount != null; }
            set { }
        }
    
        [XmlElement("StateOfRecordationName", Order = 13)]
        public MISMOString StateOfRecordationName { get; set; }
    
        [XmlIgnore]
        public bool StateOfRecordationNameSpecified
        {
            get { return this.StateOfRecordationName != null; }
            set { }
        }
    
        [XmlElement("VolumeIdentifier", Order = 14)]
        public MISMOIdentifier VolumeIdentifier { get; set; }
    
        [XmlIgnore]
        public bool VolumeIdentifierSpecified
        {
            get { return this.VolumeIdentifier != null; }
            set { }
        }
    
        [XmlElement("VolumeType", Order = 15)]
        public MISMOEnum<VolumeBase> VolumeType { get; set; }
    
        [XmlIgnore]
        public bool VolumeTypeSpecified
        {
            get { return this.VolumeType != null; }
            set { }
        }
    
        [XmlElement("VolumeTypeOtherDescription", Order = 16)]
        public MISMOString VolumeTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool VolumeTypeOtherDescriptionSpecified
        {
            get { return this.VolumeTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 17)]
        public RECORDING_ENDORSEMENT_INFORMATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
