namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class CONTRIBUTIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ContributionListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CONTRIBUTION", Order = 0)]
        public List<CONTRIBUTION> ContributionList { get; set; } = new List<CONTRIBUTION>();
    
        [XmlIgnore]
        public bool ContributionListSpecified
        {
            get { return this.ContributionList != null && this.ContributionList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public CONTRIBUTIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
