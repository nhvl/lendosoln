namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CONTRIBUTION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ActualContributionAmountSpecified
                    || this.ContributionSourceTypeSpecified
                    || this.ContributionSourceTypeOtherDescriptionSpecified
                    || this.ContributionTypeSpecified
                    || this.ContributionTypeOtherDescriptionSpecified
                    || this.EstimatedContributionAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ActualContributionAmount", Order = 0)]
        public MISMOAmount ActualContributionAmount { get; set; }
    
        [XmlIgnore]
        public bool ActualContributionAmountSpecified
        {
            get { return this.ActualContributionAmount != null; }
            set { }
        }
    
        [XmlElement("ContributionSourceType", Order = 1)]
        public MISMOEnum<ContributionSourceBase> ContributionSourceType { get; set; }
    
        [XmlIgnore]
        public bool ContributionSourceTypeSpecified
        {
            get { return this.ContributionSourceType != null; }
            set { }
        }
    
        [XmlElement("ContributionSourceTypeOtherDescription", Order = 2)]
        public MISMOString ContributionSourceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ContributionSourceTypeOtherDescriptionSpecified
        {
            get { return this.ContributionSourceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ContributionType", Order = 3)]
        public MISMOEnum<ContributionBase> ContributionType { get; set; }
    
        [XmlIgnore]
        public bool ContributionTypeSpecified
        {
            get { return this.ContributionType != null; }
            set { }
        }
    
        [XmlElement("ContributionTypeOtherDescription", Order = 4)]
        public MISMOString ContributionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ContributionTypeOtherDescriptionSpecified
        {
            get { return this.ContributionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EstimatedContributionAmount", Order = 5)]
        public MISMOAmount EstimatedContributionAmount { get; set; }
    
        [XmlIgnore]
        public bool EstimatedContributionAmountSpecified
        {
            get { return this.EstimatedContributionAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 6)]
        public CONTRIBUTION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
