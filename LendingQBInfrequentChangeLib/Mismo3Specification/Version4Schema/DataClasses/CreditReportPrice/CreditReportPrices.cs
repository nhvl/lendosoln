namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class CREDIT_REPORT_PRICES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditReportPriceListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CREDIT_REPORT_PRICE", Order = 0)]
        public List<CREDIT_REPORT_PRICE> CreditReportPriceList { get; set; } = new List<CREDIT_REPORT_PRICE>();
    
        [XmlIgnore]
        public bool CreditReportPriceListSpecified
        {
            get { return this.CreditReportPriceList != null && this.CreditReportPriceList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public CREDIT_REPORT_PRICES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
