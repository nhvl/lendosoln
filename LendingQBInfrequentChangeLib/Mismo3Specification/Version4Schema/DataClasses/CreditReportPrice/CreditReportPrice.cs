namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CREDIT_REPORT_PRICE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditReportPriceAmountSpecified
                    || this.CreditReportPriceTypeSpecified
                    || this.CreditReportPriceTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CreditReportPriceAmount", Order = 0)]
        public MISMOAmount CreditReportPriceAmount { get; set; }
    
        [XmlIgnore]
        public bool CreditReportPriceAmountSpecified
        {
            get { return this.CreditReportPriceAmount != null; }
            set { }
        }
    
        [XmlElement("CreditReportPriceType", Order = 1)]
        public MISMOEnum<CreditReportPriceBase> CreditReportPriceType { get; set; }
    
        [XmlIgnore]
        public bool CreditReportPriceTypeSpecified
        {
            get { return this.CreditReportPriceType != null; }
            set { }
        }
    
        [XmlElement("CreditReportPriceTypeOtherDescription", Order = 2)]
        public MISMOString CreditReportPriceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CreditReportPriceTypeOtherDescriptionSpecified
        {
            get { return this.CreditReportPriceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public CREDIT_REPORT_PRICE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
