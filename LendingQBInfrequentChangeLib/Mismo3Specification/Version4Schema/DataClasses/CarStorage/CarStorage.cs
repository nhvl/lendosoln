namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CAR_STORAGE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AdequateIndicatorSpecified
                    || this.CarStorageAttachmentTypeSpecified
                    || this.CarStorageDescriptionSpecified
                    || this.CarStorageIndicatorSpecified
                    || this.CarStorageTypeSpecified
                    || this.CarStorageTypeOtherDescriptionSpecified
                    || this.ComponentAdjustmentAmountSpecified
                    || this.ConditionRatingDescriptionSpecified
                    || this.ConditionRatingTypeSpecified
                    || this.MaterialDescriptionSpecified
                    || this.ParkingSpaceIdentifierSpecified
                    || this.ParkingSpacesCountSpecified
                    || this.ProjectCarStorageAdequacyEffectOnMarketabilityDescriptionSpecified
                    || this.ProjectCarStorageDescriptionSpecified
                    || this.ProjectCarStorageSpacesToUnitsRatioPercentSpecified
                    || this.ProjectGuestParkingIndicatorSpecified
                    || this.ProjectGuestParkingSpacesCountSpecified
                    || this.ProjectParkingSpaceAssignmentTypeSpecified
                    || this.ProjectParkingSpaceAssignmentTypeOtherDescriptionSpecified
                    || this.QualityRatingDescriptionSpecified
                    || this.QualityRatingTypeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AdequateIndicator", Order = 0)]
        public MISMOIndicator AdequateIndicator { get; set; }
    
        [XmlIgnore]
        public bool AdequateIndicatorSpecified
        {
            get { return this.AdequateIndicator != null; }
            set { }
        }
    
        [XmlElement("CarStorageAttachmentType", Order = 1)]
        public MISMOEnum<CarStorageAttachmentBase> CarStorageAttachmentType { get; set; }
    
        [XmlIgnore]
        public bool CarStorageAttachmentTypeSpecified
        {
            get { return this.CarStorageAttachmentType != null; }
            set { }
        }
    
        [XmlElement("CarStorageDescription", Order = 2)]
        public MISMOString CarStorageDescription { get; set; }
    
        [XmlIgnore]
        public bool CarStorageDescriptionSpecified
        {
            get { return this.CarStorageDescription != null; }
            set { }
        }
    
        [XmlElement("CarStorageIndicator", Order = 3)]
        public MISMOIndicator CarStorageIndicator { get; set; }
    
        [XmlIgnore]
        public bool CarStorageIndicatorSpecified
        {
            get { return this.CarStorageIndicator != null; }
            set { }
        }
    
        [XmlElement("CarStorageType", Order = 4)]
        public MISMOEnum<CarStorageBase> CarStorageType { get; set; }
    
        [XmlIgnore]
        public bool CarStorageTypeSpecified
        {
            get { return this.CarStorageType != null; }
            set { }
        }
    
        [XmlElement("CarStorageTypeOtherDescription", Order = 5)]
        public MISMOString CarStorageTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CarStorageTypeOtherDescriptionSpecified
        {
            get { return this.CarStorageTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ComponentAdjustmentAmount", Order = 6)]
        public MISMOAmount ComponentAdjustmentAmount { get; set; }
    
        [XmlIgnore]
        public bool ComponentAdjustmentAmountSpecified
        {
            get { return this.ComponentAdjustmentAmount != null; }
            set { }
        }
    
        [XmlElement("ConditionRatingDescription", Order = 7)]
        public MISMOString ConditionRatingDescription { get; set; }
    
        [XmlIgnore]
        public bool ConditionRatingDescriptionSpecified
        {
            get { return this.ConditionRatingDescription != null; }
            set { }
        }
    
        [XmlElement("ConditionRatingType", Order = 8)]
        public MISMOEnum<ConditionRatingBase> ConditionRatingType { get; set; }
    
        [XmlIgnore]
        public bool ConditionRatingTypeSpecified
        {
            get { return this.ConditionRatingType != null; }
            set { }
        }
    
        [XmlElement("MaterialDescription", Order = 9)]
        public MISMOString MaterialDescription { get; set; }
    
        [XmlIgnore]
        public bool MaterialDescriptionSpecified
        {
            get { return this.MaterialDescription != null; }
            set { }
        }
    
        [XmlElement("ParkingSpaceIdentifier", Order = 10)]
        public MISMOIdentifier ParkingSpaceIdentifier { get; set; }
    
        [XmlIgnore]
        public bool ParkingSpaceIdentifierSpecified
        {
            get { return this.ParkingSpaceIdentifier != null; }
            set { }
        }
    
        [XmlElement("ParkingSpacesCount", Order = 11)]
        public MISMOCount ParkingSpacesCount { get; set; }
    
        [XmlIgnore]
        public bool ParkingSpacesCountSpecified
        {
            get { return this.ParkingSpacesCount != null; }
            set { }
        }
    
        [XmlElement("ProjectCarStorageAdequacyEffectOnMarketabilityDescription", Order = 12)]
        public MISMOString ProjectCarStorageAdequacyEffectOnMarketabilityDescription { get; set; }
    
        [XmlIgnore]
        public bool ProjectCarStorageAdequacyEffectOnMarketabilityDescriptionSpecified
        {
            get { return this.ProjectCarStorageAdequacyEffectOnMarketabilityDescription != null; }
            set { }
        }
    
        [XmlElement("ProjectCarStorageDescription", Order = 13)]
        public MISMOString ProjectCarStorageDescription { get; set; }
    
        [XmlIgnore]
        public bool ProjectCarStorageDescriptionSpecified
        {
            get { return this.ProjectCarStorageDescription != null; }
            set { }
        }
    
        [XmlElement("ProjectCarStorageSpacesToUnitsRatioPercent", Order = 14)]
        public MISMOPercent ProjectCarStorageSpacesToUnitsRatioPercent { get; set; }
    
        [XmlIgnore]
        public bool ProjectCarStorageSpacesToUnitsRatioPercentSpecified
        {
            get { return this.ProjectCarStorageSpacesToUnitsRatioPercent != null; }
            set { }
        }
    
        [XmlElement("ProjectGuestParkingIndicator", Order = 15)]
        public MISMOIndicator ProjectGuestParkingIndicator { get; set; }
    
        [XmlIgnore]
        public bool ProjectGuestParkingIndicatorSpecified
        {
            get { return this.ProjectGuestParkingIndicator != null; }
            set { }
        }
    
        [XmlElement("ProjectGuestParkingSpacesCount", Order = 16)]
        public MISMOCount ProjectGuestParkingSpacesCount { get; set; }
    
        [XmlIgnore]
        public bool ProjectGuestParkingSpacesCountSpecified
        {
            get { return this.ProjectGuestParkingSpacesCount != null; }
            set { }
        }
    
        [XmlElement("ProjectParkingSpaceAssignmentType", Order = 17)]
        public MISMOEnum<ProjectParkingSpaceAssignmentBase> ProjectParkingSpaceAssignmentType { get; set; }
    
        [XmlIgnore]
        public bool ProjectParkingSpaceAssignmentTypeSpecified
        {
            get { return this.ProjectParkingSpaceAssignmentType != null; }
            set { }
        }
    
        [XmlElement("ProjectParkingSpaceAssignmentTypeOtherDescription", Order = 18)]
        public MISMOString ProjectParkingSpaceAssignmentTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ProjectParkingSpaceAssignmentTypeOtherDescriptionSpecified
        {
            get { return this.ProjectParkingSpaceAssignmentTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("QualityRatingDescription", Order = 19)]
        public MISMOString QualityRatingDescription { get; set; }
    
        [XmlIgnore]
        public bool QualityRatingDescriptionSpecified
        {
            get { return this.QualityRatingDescription != null; }
            set { }
        }
    
        [XmlElement("QualityRatingType", Order = 20)]
        public MISMOEnum<QualityRatingBase> QualityRatingType { get; set; }
    
        [XmlIgnore]
        public bool QualityRatingTypeSpecified
        {
            get { return this.QualityRatingType != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 21)]
        public CAR_STORAGE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
