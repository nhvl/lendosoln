namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class CAR_STORAGES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CarStorageListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CAR_STORAGE", Order = 0)]
        public List<CAR_STORAGE> CarStorageList { get; set; } = new List<CAR_STORAGE>();
    
        [XmlIgnore]
        public bool CarStorageListSpecified
        {
            get { return this.CarStorageList != null && this.CarStorageList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public CAR_STORAGES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
