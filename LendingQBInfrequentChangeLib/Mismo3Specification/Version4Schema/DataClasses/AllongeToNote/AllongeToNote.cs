namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ALLONGE_TO_NOTE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AllongeToNoteDateSpecified
                    || this.AllongeToNoteExecutedByDescriptionSpecified
                    || this.AllongeToNoteInFavorOfDescriptionSpecified
                    || this.AllongeToNotePayToTheOrderOfDescriptionSpecified
                    || this.AllongeToNoteWithoutRecourseDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AllongeToNoteDate", Order = 0)]
        public MISMODate AllongeToNoteDate { get; set; }
    
        [XmlIgnore]
        public bool AllongeToNoteDateSpecified
        {
            get { return this.AllongeToNoteDate != null; }
            set { }
        }
    
        [XmlElement("AllongeToNoteExecutedByDescription", Order = 1)]
        public MISMOString AllongeToNoteExecutedByDescription { get; set; }
    
        [XmlIgnore]
        public bool AllongeToNoteExecutedByDescriptionSpecified
        {
            get { return this.AllongeToNoteExecutedByDescription != null; }
            set { }
        }
    
        [XmlElement("AllongeToNoteInFavorOfDescription", Order = 2)]
        public MISMOString AllongeToNoteInFavorOfDescription { get; set; }
    
        [XmlIgnore]
        public bool AllongeToNoteInFavorOfDescriptionSpecified
        {
            get { return this.AllongeToNoteInFavorOfDescription != null; }
            set { }
        }
    
        [XmlElement("AllongeToNotePayToTheOrderOfDescription", Order = 3)]
        public MISMOString AllongeToNotePayToTheOrderOfDescription { get; set; }
    
        [XmlIgnore]
        public bool AllongeToNotePayToTheOrderOfDescriptionSpecified
        {
            get { return this.AllongeToNotePayToTheOrderOfDescription != null; }
            set { }
        }
    
        [XmlElement("AllongeToNoteWithoutRecourseDescription", Order = 4)]
        public MISMOString AllongeToNoteWithoutRecourseDescription { get; set; }
    
        [XmlIgnore]
        public bool AllongeToNoteWithoutRecourseDescriptionSpecified
        {
            get { return this.AllongeToNoteWithoutRecourseDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public ALLONGE_TO_NOTE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
