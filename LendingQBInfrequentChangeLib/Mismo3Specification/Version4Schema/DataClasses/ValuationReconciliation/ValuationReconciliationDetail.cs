namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class VALUATION_RECONCILIATION_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ValuationReconciliationConditionsCommentDescriptionSpecified
                    || this.ValuationReconciliationSummaryCommentDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ValuationReconciliationConditionsCommentDescription", Order = 0)]
        public MISMOString ValuationReconciliationConditionsCommentDescription { get; set; }
    
        [XmlIgnore]
        public bool ValuationReconciliationConditionsCommentDescriptionSpecified
        {
            get { return this.ValuationReconciliationConditionsCommentDescription != null; }
            set { }
        }
    
        [XmlElement("ValuationReconciliationSummaryCommentDescription", Order = 1)]
        public MISMOString ValuationReconciliationSummaryCommentDescription { get; set; }
    
        [XmlIgnore]
        public bool ValuationReconciliationSummaryCommentDescriptionSpecified
        {
            get { return this.ValuationReconciliationSummaryCommentDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public VALUATION_RECONCILIATION_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
