namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class VALUATION_RECONCILIATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.RepairSpecified
                    || this.ValuationReconciliationDetailSpecified
                    || this.ValuationReconciliationSummariesSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("REPAIR", Order = 0)]
        public REPAIR Repair { get; set; }
    
        [XmlIgnore]
        public bool RepairSpecified
        {
            get { return this.Repair != null && this.Repair.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("VALUATION_RECONCILIATION_DETAIL", Order = 1)]
        public VALUATION_RECONCILIATION_DETAIL ValuationReconciliationDetail { get; set; }
    
        [XmlIgnore]
        public bool ValuationReconciliationDetailSpecified
        {
            get { return this.ValuationReconciliationDetail != null && this.ValuationReconciliationDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("VALUATION_RECONCILIATION_SUMMARIES", Order = 2)]
        public VALUATION_RECONCILIATION_SUMMARIES ValuationReconciliationSummaries { get; set; }
    
        [XmlIgnore]
        public bool ValuationReconciliationSummariesSpecified
        {
            get { return this.ValuationReconciliationSummaries != null && this.ValuationReconciliationSummaries.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public VALUATION_RECONCILIATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
