namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CONTRACT_VARIANCE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ContractVarianceCodeSpecified
                    || this.ContractVarianceCodeIssuerTypeSpecified
                    || this.ContractVarianceCodeIssuerTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ContractVarianceCode", Order = 0)]
        public MISMOCode ContractVarianceCode { get; set; }
    
        [XmlIgnore]
        public bool ContractVarianceCodeSpecified
        {
            get { return this.ContractVarianceCode != null; }
            set { }
        }
    
        [XmlElement("ContractVarianceCodeIssuerType", Order = 1)]
        public MISMOEnum<ContractVarianceCodeIssuerBase> ContractVarianceCodeIssuerType { get; set; }
    
        [XmlIgnore]
        public bool ContractVarianceCodeIssuerTypeSpecified
        {
            get { return this.ContractVarianceCodeIssuerType != null; }
            set { }
        }
    
        [XmlElement("ContractVarianceCodeIssuerTypeOtherDescription", Order = 2)]
        public MISMOString ContractVarianceCodeIssuerTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ContractVarianceCodeIssuerTypeOtherDescriptionSpecified
        {
            get { return this.ContractVarianceCodeIssuerTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public CONTRACT_VARIANCE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
