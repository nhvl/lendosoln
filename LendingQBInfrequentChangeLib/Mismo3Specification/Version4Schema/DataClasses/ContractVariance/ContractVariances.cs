namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class CONTRACT_VARIANCES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ContractVarianceListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CONTRACT_VARIANCE", Order = 0)]
        public List<CONTRACT_VARIANCE> ContractVarianceList { get; set; } = new List<CONTRACT_VARIANCE>();
    
        [XmlIgnore]
        public bool ContractVarianceListSpecified
        {
            get { return this.ContractVarianceList != null && this.ContractVarianceList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public CONTRACT_VARIANCES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
