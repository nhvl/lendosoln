namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class BOUNDARIES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BoundaryListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BOUNDARY", Order = 0)]
        public List<BOUNDARY> BoundaryList { get; set; } = new List<BOUNDARY>();
    
        [XmlIgnore]
        public bool BoundaryListSpecified
        {
            get { return this.BoundaryList != null && this.BoundaryList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public BOUNDARIES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
