namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class BOUNDARY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BoundaryDirectionTypeSpecified
                    || this.BoundaryDirectionTypeOtherDescriptionSpecified
                    || this.BoundaryGeospatialCoordinatesDescriptionSpecified
                    || this.BoundaryTypeSpecified
                    || this.BoundaryTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BoundaryDirectionType", Order = 0)]
        public MISMOEnum<BoundaryDirectionBase> BoundaryDirectionType { get; set; }
    
        [XmlIgnore]
        public bool BoundaryDirectionTypeSpecified
        {
            get { return this.BoundaryDirectionType != null; }
            set { }
        }
    
        [XmlElement("BoundaryDirectionTypeOtherDescription", Order = 1)]
        public MISMOString BoundaryDirectionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool BoundaryDirectionTypeOtherDescriptionSpecified
        {
            get { return this.BoundaryDirectionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("BoundaryGeospatialCoordinatesDescription", Order = 2)]
        public MISMOString BoundaryGeospatialCoordinatesDescription { get; set; }
    
        [XmlIgnore]
        public bool BoundaryGeospatialCoordinatesDescriptionSpecified
        {
            get { return this.BoundaryGeospatialCoordinatesDescription != null; }
            set { }
        }
    
        [XmlElement("BoundaryType", Order = 3)]
        public MISMOEnum<BoundaryBase> BoundaryType { get; set; }
    
        [XmlIgnore]
        public bool BoundaryTypeSpecified
        {
            get { return this.BoundaryType != null; }
            set { }
        }
    
        [XmlElement("BoundaryTypeOtherDescription", Order = 4)]
        public MISMOString BoundaryTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool BoundaryTypeOtherDescriptionSpecified
        {
            get { return this.BoundaryTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public BOUNDARY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
