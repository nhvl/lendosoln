namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SERVICING_TRANSFEROR
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ServicingTransferorInactiveIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ServicingTransferorInactiveIndicator", Order = 0)]
        public MISMOIndicator ServicingTransferorInactiveIndicator { get; set; }
    
        [XmlIgnore]
        public bool ServicingTransferorInactiveIndicatorSpecified
        {
            get { return this.ServicingTransferorInactiveIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public SERVICING_TRANSFEROR_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
