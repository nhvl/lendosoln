namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CREDIT_LIABILITY_LATE_COUNT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditLiabilityLateCountDetailSpecified
                    || this.PeriodicLateCountsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CREDIT_LIABILITY_LATE_COUNT_DETAIL", Order = 0)]
        public CREDIT_LIABILITY_LATE_COUNT_DETAIL CreditLiabilityLateCountDetail { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityLateCountDetailSpecified
        {
            get { return this.CreditLiabilityLateCountDetail != null && this.CreditLiabilityLateCountDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PERIODIC_LATE_COUNTS", Order = 1)]
        public PERIODIC_LATE_COUNTS PeriodicLateCounts { get; set; }
    
        [XmlIgnore]
        public bool PeriodicLateCountsSpecified
        {
            get { return this.PeriodicLateCounts != null && this.PeriodicLateCounts.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public CREDIT_LIABILITY_LATE_COUNT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
