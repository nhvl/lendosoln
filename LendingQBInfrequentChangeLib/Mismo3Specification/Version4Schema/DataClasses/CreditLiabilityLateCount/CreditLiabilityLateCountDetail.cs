namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CREDIT_LIABILITY_LATE_COUNT_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditLiability120DaysLateCountSpecified
                    || this.CreditLiability30DaysLateCountSpecified
                    || this.CreditLiability60DaysLateCountSpecified
                    || this.CreditLiability90DaysLateCountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CreditLiability120DaysLateCount", Order = 0)]
        public MISMOCount CreditLiability120DaysLateCount { get; set; }
    
        [XmlIgnore]
        public bool CreditLiability120DaysLateCountSpecified
        {
            get { return this.CreditLiability120DaysLateCount != null; }
            set { }
        }
    
        [XmlElement("CreditLiability30DaysLateCount", Order = 1)]
        public MISMOCount CreditLiability30DaysLateCount { get; set; }
    
        [XmlIgnore]
        public bool CreditLiability30DaysLateCountSpecified
        {
            get { return this.CreditLiability30DaysLateCount != null; }
            set { }
        }
    
        [XmlElement("CreditLiability60DaysLateCount", Order = 2)]
        public MISMOCount CreditLiability60DaysLateCount { get; set; }
    
        [XmlIgnore]
        public bool CreditLiability60DaysLateCountSpecified
        {
            get { return this.CreditLiability60DaysLateCount != null; }
            set { }
        }
    
        [XmlElement("CreditLiability90DaysLateCount", Order = 3)]
        public MISMOCount CreditLiability90DaysLateCount { get; set; }
    
        [XmlIgnore]
        public bool CreditLiability90DaysLateCountSpecified
        {
            get { return this.CreditLiability90DaysLateCount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public CREDIT_LIABILITY_LATE_COUNT_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
