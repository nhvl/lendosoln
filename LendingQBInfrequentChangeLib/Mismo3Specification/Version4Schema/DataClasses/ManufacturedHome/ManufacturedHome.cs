namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MANUFACTURED_HOME
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ManufacturedHomeDetailSpecified
                    || this.ManufacturedHomeIdentificationSpecified
                    || this.ManufacturedHomeSectionsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MANUFACTURED_HOME_DETAIL", Order = 0)]
        public MANUFACTURED_HOME_DETAIL ManufacturedHomeDetail { get; set; }
    
        [XmlIgnore]
        public bool ManufacturedHomeDetailSpecified
        {
            get { return this.ManufacturedHomeDetail != null && this.ManufacturedHomeDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("MANUFACTURED_HOME_IDENTIFICATION", Order = 1)]
        public MANUFACTURED_HOME_IDENTIFICATION ManufacturedHomeIdentification { get; set; }
    
        [XmlIgnore]
        public bool ManufacturedHomeIdentificationSpecified
        {
            get { return this.ManufacturedHomeIdentification != null && this.ManufacturedHomeIdentification.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("MANUFACTURED_HOME_SECTIONS", Order = 2)]
        public MANUFACTURED_HOME_SECTIONS ManufacturedHomeSections { get; set; }
    
        [XmlIgnore]
        public bool ManufacturedHomeSectionsSpecified
        {
            get { return this.ManufacturedHomeSections != null && this.ManufacturedHomeSections.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public MANUFACTURED_HOME_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
