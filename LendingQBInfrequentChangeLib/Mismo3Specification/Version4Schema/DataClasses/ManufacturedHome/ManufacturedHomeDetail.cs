namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MANUFACTURED_HOME_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LengthFeetNumberSpecified
                    || this.ManufacturedHomeAttachedToFoundationDescriptionSpecified
                    || this.ManufacturedHomeAttachedToFoundationIndicatorSpecified
                    || this.ManufacturedHomeConditionDescriptionTypeSpecified
                    || this.ManufacturedHomeConnectedToUtilitiesIndicatorSpecified
                    || this.ManufacturedHomeConnectionToUtilitiesDescriptionSpecified
                    || this.ManufacturedHomeConstructionQualityRatingTypeSpecified
                    || this.ManufacturedHomeConstructionQualitySourceDescriptionSpecified
                    || this.ManufacturedHomeDeckDescriptionSpecified
                    || this.ManufacturedHomeHUDDataPlateAttachedIndicatorSpecified
                    || this.ManufacturedHomeHUDDataPlateDataSourceDescriptionSpecified
                    || this.ManufacturedHomeHUDDataPlateIdentifierSpecified
                    || this.ManufacturedHomeInstalledDateSpecified
                    || this.ManufacturedHomeInstallerNameSpecified
                    || this.ManufacturedHomeInteriorSpaceAcceptabilityToMarketDescriptionSpecified
                    || this.ManufacturedHomeInteriorSpaceAcceptableToMarketIndicatorSpecified
                    || this.ManufacturedHomeInvoiceReviewDescriptionSpecified
                    || this.ManufacturedHomeInvoiceReviewedIndicatorSpecified
                    || this.ManufacturedHomeManufacturerNameSpecified
                    || this.ManufacturedHomeManufactureYearSpecified
                    || this.ManufacturedHomeMeetsHUDRequirementsForLocationDescriptionSpecified
                    || this.ManufacturedHomeMeetsHUDRequirementsForLocationIndicatorSpecified
                    || this.ManufacturedHomeModelIdentifierSpecified
                    || this.ManufacturedHomeModelYearSpecified
                    || this.ManufacturedHomeModificationsDescriptionSpecified
                    || this.ManufacturedHomePorchDescriptionSpecified
                    || this.ManufacturedHomeRetailerNameSpecified
                    || this.ManufacturedHomeSectionCountSpecified
                    || this.ManufacturedHomeSerialNumberIdentifierSpecified
                    || this.ManufacturedHomeTipOutDescriptionSpecified
                    || this.ManufacturedHomeWaniganDescriptionSpecified
                    || this.ManufacturedHomeWheelsDescriptionSpecified
                    || this.ManufacturedHomeWheelsRemovedIndicatorSpecified
                    || this.ManufacturedHomeWidthTypeSpecified
                    || this.MobileHomeParkIndicatorSpecified
                    || this.SquareFeetNumberSpecified
                    || this.WidthFeetNumberSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LengthFeetNumber", Order = 0)]
        public MISMONumeric LengthFeetNumber { get; set; }
    
        [XmlIgnore]
        public bool LengthFeetNumberSpecified
        {
            get { return this.LengthFeetNumber != null; }
            set { }
        }
    
        [XmlElement("ManufacturedHomeAttachedToFoundationDescription", Order = 1)]
        public MISMOString ManufacturedHomeAttachedToFoundationDescription { get; set; }
    
        [XmlIgnore]
        public bool ManufacturedHomeAttachedToFoundationDescriptionSpecified
        {
            get { return this.ManufacturedHomeAttachedToFoundationDescription != null; }
            set { }
        }
    
        [XmlElement("ManufacturedHomeAttachedToFoundationIndicator", Order = 2)]
        public MISMOIndicator ManufacturedHomeAttachedToFoundationIndicator { get; set; }
    
        [XmlIgnore]
        public bool ManufacturedHomeAttachedToFoundationIndicatorSpecified
        {
            get { return this.ManufacturedHomeAttachedToFoundationIndicator != null; }
            set { }
        }
    
        [XmlElement("ManufacturedHomeConditionDescriptionType", Order = 3)]
        public MISMOEnum<ManufacturedHomeConditionDescriptionBase> ManufacturedHomeConditionDescriptionType { get; set; }
    
        [XmlIgnore]
        public bool ManufacturedHomeConditionDescriptionTypeSpecified
        {
            get { return this.ManufacturedHomeConditionDescriptionType != null; }
            set { }
        }
    
        [XmlElement("ManufacturedHomeConnectedToUtilitiesIndicator", Order = 4)]
        public MISMOIndicator ManufacturedHomeConnectedToUtilitiesIndicator { get; set; }
    
        [XmlIgnore]
        public bool ManufacturedHomeConnectedToUtilitiesIndicatorSpecified
        {
            get { return this.ManufacturedHomeConnectedToUtilitiesIndicator != null; }
            set { }
        }
    
        [XmlElement("ManufacturedHomeConnectionToUtilitiesDescription", Order = 5)]
        public MISMOString ManufacturedHomeConnectionToUtilitiesDescription { get; set; }
    
        [XmlIgnore]
        public bool ManufacturedHomeConnectionToUtilitiesDescriptionSpecified
        {
            get { return this.ManufacturedHomeConnectionToUtilitiesDescription != null; }
            set { }
        }
    
        [XmlElement("ManufacturedHomeConstructionQualityRatingType", Order = 6)]
        public MISMOEnum<ManufacturedHomeConstructionQualityRatingBase> ManufacturedHomeConstructionQualityRatingType { get; set; }
    
        [XmlIgnore]
        public bool ManufacturedHomeConstructionQualityRatingTypeSpecified
        {
            get { return this.ManufacturedHomeConstructionQualityRatingType != null; }
            set { }
        }
    
        [XmlElement("ManufacturedHomeConstructionQualitySourceDescription", Order = 7)]
        public MISMOString ManufacturedHomeConstructionQualitySourceDescription { get; set; }
    
        [XmlIgnore]
        public bool ManufacturedHomeConstructionQualitySourceDescriptionSpecified
        {
            get { return this.ManufacturedHomeConstructionQualitySourceDescription != null; }
            set { }
        }
    
        [XmlElement("ManufacturedHomeDeckDescription", Order = 8)]
        public MISMOString ManufacturedHomeDeckDescription { get; set; }
    
        [XmlIgnore]
        public bool ManufacturedHomeDeckDescriptionSpecified
        {
            get { return this.ManufacturedHomeDeckDescription != null; }
            set { }
        }
    
        [XmlElement("ManufacturedHomeHUDDataPlateAttachedIndicator", Order = 9)]
        public MISMOIndicator ManufacturedHomeHUDDataPlateAttachedIndicator { get; set; }
    
        [XmlIgnore]
        public bool ManufacturedHomeHUDDataPlateAttachedIndicatorSpecified
        {
            get { return this.ManufacturedHomeHUDDataPlateAttachedIndicator != null; }
            set { }
        }
    
        [XmlElement("ManufacturedHomeHUDDataPlateDataSourceDescription", Order = 10)]
        public MISMOString ManufacturedHomeHUDDataPlateDataSourceDescription { get; set; }
    
        [XmlIgnore]
        public bool ManufacturedHomeHUDDataPlateDataSourceDescriptionSpecified
        {
            get { return this.ManufacturedHomeHUDDataPlateDataSourceDescription != null; }
            set { }
        }
    
        [XmlElement("ManufacturedHomeHUDDataPlateIdentifier", Order = 11)]
        public MISMOIdentifier ManufacturedHomeHUDDataPlateIdentifier { get; set; }
    
        [XmlIgnore]
        public bool ManufacturedHomeHUDDataPlateIdentifierSpecified
        {
            get { return this.ManufacturedHomeHUDDataPlateIdentifier != null; }
            set { }
        }
    
        [XmlElement("ManufacturedHomeInstalledDate", Order = 12)]
        public MISMODate ManufacturedHomeInstalledDate { get; set; }
    
        [XmlIgnore]
        public bool ManufacturedHomeInstalledDateSpecified
        {
            get { return this.ManufacturedHomeInstalledDate != null; }
            set { }
        }
    
        [XmlElement("ManufacturedHomeInstallerName", Order = 13)]
        public MISMOString ManufacturedHomeInstallerName { get; set; }
    
        [XmlIgnore]
        public bool ManufacturedHomeInstallerNameSpecified
        {
            get { return this.ManufacturedHomeInstallerName != null; }
            set { }
        }
    
        [XmlElement("ManufacturedHomeInteriorSpaceAcceptabilityToMarketDescription", Order = 14)]
        public MISMOString ManufacturedHomeInteriorSpaceAcceptabilityToMarketDescription { get; set; }
    
        [XmlIgnore]
        public bool ManufacturedHomeInteriorSpaceAcceptabilityToMarketDescriptionSpecified
        {
            get { return this.ManufacturedHomeInteriorSpaceAcceptabilityToMarketDescription != null; }
            set { }
        }
    
        [XmlElement("ManufacturedHomeInteriorSpaceAcceptableToMarketIndicator", Order = 15)]
        public MISMOIndicator ManufacturedHomeInteriorSpaceAcceptableToMarketIndicator { get; set; }
    
        [XmlIgnore]
        public bool ManufacturedHomeInteriorSpaceAcceptableToMarketIndicatorSpecified
        {
            get { return this.ManufacturedHomeInteriorSpaceAcceptableToMarketIndicator != null; }
            set { }
        }
    
        [XmlElement("ManufacturedHomeInvoiceReviewDescription", Order = 16)]
        public MISMOString ManufacturedHomeInvoiceReviewDescription { get; set; }
    
        [XmlIgnore]
        public bool ManufacturedHomeInvoiceReviewDescriptionSpecified
        {
            get { return this.ManufacturedHomeInvoiceReviewDescription != null; }
            set { }
        }
    
        [XmlElement("ManufacturedHomeInvoiceReviewedIndicator", Order = 17)]
        public MISMOIndicator ManufacturedHomeInvoiceReviewedIndicator { get; set; }
    
        [XmlIgnore]
        public bool ManufacturedHomeInvoiceReviewedIndicatorSpecified
        {
            get { return this.ManufacturedHomeInvoiceReviewedIndicator != null; }
            set { }
        }
    
        [XmlElement("ManufacturedHomeManufacturerName", Order = 18)]
        public MISMOString ManufacturedHomeManufacturerName { get; set; }
    
        [XmlIgnore]
        public bool ManufacturedHomeManufacturerNameSpecified
        {
            get { return this.ManufacturedHomeManufacturerName != null; }
            set { }
        }
    
        [XmlElement("ManufacturedHomeManufactureYear", Order = 19)]
        public MISMOYear ManufacturedHomeManufactureYear { get; set; }
    
        [XmlIgnore]
        public bool ManufacturedHomeManufactureYearSpecified
        {
            get { return this.ManufacturedHomeManufactureYear != null; }
            set { }
        }
    
        [XmlElement("ManufacturedHomeMeetsHUDRequirementsForLocationDescription", Order = 20)]
        public MISMOString ManufacturedHomeMeetsHUDRequirementsForLocationDescription { get; set; }
    
        [XmlIgnore]
        public bool ManufacturedHomeMeetsHUDRequirementsForLocationDescriptionSpecified
        {
            get { return this.ManufacturedHomeMeetsHUDRequirementsForLocationDescription != null; }
            set { }
        }
    
        [XmlElement("ManufacturedHomeMeetsHUDRequirementsForLocationIndicator", Order = 21)]
        public MISMOIndicator ManufacturedHomeMeetsHUDRequirementsForLocationIndicator { get; set; }
    
        [XmlIgnore]
        public bool ManufacturedHomeMeetsHUDRequirementsForLocationIndicatorSpecified
        {
            get { return this.ManufacturedHomeMeetsHUDRequirementsForLocationIndicator != null; }
            set { }
        }
    
        [XmlElement("ManufacturedHomeModelIdentifier", Order = 22)]
        public MISMOIdentifier ManufacturedHomeModelIdentifier { get; set; }
    
        [XmlIgnore]
        public bool ManufacturedHomeModelIdentifierSpecified
        {
            get { return this.ManufacturedHomeModelIdentifier != null; }
            set { }
        }
    
        [XmlElement("ManufacturedHomeModelYear", Order = 23)]
        public MISMOYear ManufacturedHomeModelYear { get; set; }
    
        [XmlIgnore]
        public bool ManufacturedHomeModelYearSpecified
        {
            get { return this.ManufacturedHomeModelYear != null; }
            set { }
        }
    
        [XmlElement("ManufacturedHomeModificationsDescription", Order = 24)]
        public MISMOString ManufacturedHomeModificationsDescription { get; set; }
    
        [XmlIgnore]
        public bool ManufacturedHomeModificationsDescriptionSpecified
        {
            get { return this.ManufacturedHomeModificationsDescription != null; }
            set { }
        }
    
        [XmlElement("ManufacturedHomePorchDescription", Order = 25)]
        public MISMOString ManufacturedHomePorchDescription { get; set; }
    
        [XmlIgnore]
        public bool ManufacturedHomePorchDescriptionSpecified
        {
            get { return this.ManufacturedHomePorchDescription != null; }
            set { }
        }
    
        [XmlElement("ManufacturedHomeRetailerName", Order = 26)]
        public MISMOString ManufacturedHomeRetailerName { get; set; }
    
        [XmlIgnore]
        public bool ManufacturedHomeRetailerNameSpecified
        {
            get { return this.ManufacturedHomeRetailerName != null; }
            set { }
        }
    
        [XmlElement("ManufacturedHomeSectionCount", Order = 27)]
        public MISMOCount ManufacturedHomeSectionCount { get; set; }
    
        [XmlIgnore]
        public bool ManufacturedHomeSectionCountSpecified
        {
            get { return this.ManufacturedHomeSectionCount != null; }
            set { }
        }
    
        [XmlElement("ManufacturedHomeSerialNumberIdentifier", Order = 28)]
        public MISMOIdentifier ManufacturedHomeSerialNumberIdentifier { get; set; }
    
        [XmlIgnore]
        public bool ManufacturedHomeSerialNumberIdentifierSpecified
        {
            get { return this.ManufacturedHomeSerialNumberIdentifier != null; }
            set { }
        }
    
        [XmlElement("ManufacturedHomeTipOutDescription", Order = 29)]
        public MISMOString ManufacturedHomeTipOutDescription { get; set; }
    
        [XmlIgnore]
        public bool ManufacturedHomeTipOutDescriptionSpecified
        {
            get { return this.ManufacturedHomeTipOutDescription != null; }
            set { }
        }
    
        [XmlElement("ManufacturedHomeWaniganDescription", Order = 30)]
        public MISMOString ManufacturedHomeWaniganDescription { get; set; }
    
        [XmlIgnore]
        public bool ManufacturedHomeWaniganDescriptionSpecified
        {
            get { return this.ManufacturedHomeWaniganDescription != null; }
            set { }
        }
    
        [XmlElement("ManufacturedHomeWheelsDescription", Order = 31)]
        public MISMOString ManufacturedHomeWheelsDescription { get; set; }
    
        [XmlIgnore]
        public bool ManufacturedHomeWheelsDescriptionSpecified
        {
            get { return this.ManufacturedHomeWheelsDescription != null; }
            set { }
        }
    
        [XmlElement("ManufacturedHomeWheelsRemovedIndicator", Order = 32)]
        public MISMOIndicator ManufacturedHomeWheelsRemovedIndicator { get; set; }
    
        [XmlIgnore]
        public bool ManufacturedHomeWheelsRemovedIndicatorSpecified
        {
            get { return this.ManufacturedHomeWheelsRemovedIndicator != null; }
            set { }
        }
    
        [XmlElement("ManufacturedHomeWidthType", Order = 33)]
        public MISMOEnum<ManufacturedHomeWidthBase> ManufacturedHomeWidthType { get; set; }
    
        [XmlIgnore]
        public bool ManufacturedHomeWidthTypeSpecified
        {
            get { return this.ManufacturedHomeWidthType != null; }
            set { }
        }
    
        [XmlElement("MobileHomeParkIndicator", Order = 34)]
        public MISMOIndicator MobileHomeParkIndicator { get; set; }
    
        [XmlIgnore]
        public bool MobileHomeParkIndicatorSpecified
        {
            get { return this.MobileHomeParkIndicator != null; }
            set { }
        }
    
        [XmlElement("SquareFeetNumber", Order = 35)]
        public MISMONumeric SquareFeetNumber { get; set; }
    
        [XmlIgnore]
        public bool SquareFeetNumberSpecified
        {
            get { return this.SquareFeetNumber != null; }
            set { }
        }
    
        [XmlElement("WidthFeetNumber", Order = 36)]
        public MISMONumeric WidthFeetNumber { get; set; }
    
        [XmlIgnore]
        public bool WidthFeetNumberSpecified
        {
            get { return this.WidthFeetNumber != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 37)]
        public MANUFACTURED_HOME_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
