namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class DESIGNATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AppraiserDesignationAssociationNameTypeSpecified
                    || this.AppraiserDesignationTypeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AppraiserDesignationAssociationNameType", Order = 0)]
        public MISMOEnum<AppraiserDesignationAssociationNameBase> AppraiserDesignationAssociationNameType { get; set; }
    
        [XmlIgnore]
        public bool AppraiserDesignationAssociationNameTypeSpecified
        {
            get { return this.AppraiserDesignationAssociationNameType != null; }
            set { }
        }
    
        [XmlElement("AppraiserDesignationType", Order = 1)]
        public MISMOEnum<AppraiserDesignationBase> AppraiserDesignationType { get; set; }
    
        [XmlIgnore]
        public bool AppraiserDesignationTypeSpecified
        {
            get { return this.AppraiserDesignationType != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public DESIGNATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
