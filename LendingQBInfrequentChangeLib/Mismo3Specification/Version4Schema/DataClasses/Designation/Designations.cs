namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class DESIGNATIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DesignationListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DESIGNATION", Order = 0)]
        public List<DESIGNATION> DesignationList { get; set; } = new List<DESIGNATION>();
    
        [XmlIgnore]
        public bool DesignationListSpecified
        {
            get { return this.DesignationList != null && this.DesignationList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public DESIGNATIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
