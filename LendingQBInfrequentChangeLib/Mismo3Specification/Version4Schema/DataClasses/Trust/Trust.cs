namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class TRUST
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.TrustClassificationTypeSpecified
                    || this.TrustClassificationTypeOtherDescriptionSpecified
                    || this.TrustEstablishedDateSpecified
                    || this.TrustEstablishedStateNameSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("TrustClassificationType", Order = 0)]
        public MISMOEnum<TrustClassificationBase> TrustClassificationType { get; set; }
    
        [XmlIgnore]
        public bool TrustClassificationTypeSpecified
        {
            get { return this.TrustClassificationType != null; }
            set { }
        }
    
        [XmlElement("TrustClassificationTypeOtherDescription", Order = 1)]
        public MISMOString TrustClassificationTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool TrustClassificationTypeOtherDescriptionSpecified
        {
            get { return this.TrustClassificationTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("TrustEstablishedDate", Order = 2)]
        public MISMODate TrustEstablishedDate { get; set; }
    
        [XmlIgnore]
        public bool TrustEstablishedDateSpecified
        {
            get { return this.TrustEstablishedDate != null; }
            set { }
        }
    
        [XmlElement("TrustEstablishedStateName", Order = 3)]
        public MISMOString TrustEstablishedStateName { get; set; }
    
        [XmlIgnore]
        public bool TrustEstablishedStateNameSpecified
        {
            get { return this.TrustEstablishedStateName != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public TRUST_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
