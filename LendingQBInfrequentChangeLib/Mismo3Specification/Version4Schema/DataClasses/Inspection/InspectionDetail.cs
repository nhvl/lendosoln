namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class INSPECTION_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExteriorInspectionSufficientIndicatorSpecified
                    || this.InspectionDateSpecified
                    || this.PropertyInaccessibleIndicatorSpecified
                    || this.PropertyInspectionOrderDateSpecified
                    || this.PropertyInspectionPurposeTypeSpecified
                    || this.PropertyInspectionPurposeTypeOtherDescriptionSpecified
                    || this.PropertyInspectionRequestCommentDescriptionSpecified
                    || this.PropertyInspectionResultCommentDescriptionSpecified
                    || this.PropertyInspectionTypeSpecified
                    || this.PropertyInspectionTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ExteriorInspectionSufficientIndicator", Order = 0)]
        public MISMOIndicator ExteriorInspectionSufficientIndicator { get; set; }
    
        [XmlIgnore]
        public bool ExteriorInspectionSufficientIndicatorSpecified
        {
            get { return this.ExteriorInspectionSufficientIndicator != null; }
            set { }
        }
    
        [XmlElement("InspectionDate", Order = 1)]
        public MISMODate InspectionDate { get; set; }
    
        [XmlIgnore]
        public bool InspectionDateSpecified
        {
            get { return this.InspectionDate != null; }
            set { }
        }
    
        [XmlElement("PropertyInaccessibleIndicator", Order = 2)]
        public MISMOIndicator PropertyInaccessibleIndicator { get; set; }
    
        [XmlIgnore]
        public bool PropertyInaccessibleIndicatorSpecified
        {
            get { return this.PropertyInaccessibleIndicator != null; }
            set { }
        }
    
        [XmlElement("PropertyInspectionOrderDate", Order = 3)]
        public MISMODate PropertyInspectionOrderDate { get; set; }
    
        [XmlIgnore]
        public bool PropertyInspectionOrderDateSpecified
        {
            get { return this.PropertyInspectionOrderDate != null; }
            set { }
        }
    
        [XmlElement("PropertyInspectionPurposeType", Order = 4)]
        public MISMOEnum<PropertyInspectionPurposeBase> PropertyInspectionPurposeType { get; set; }
    
        [XmlIgnore]
        public bool PropertyInspectionPurposeTypeSpecified
        {
            get { return this.PropertyInspectionPurposeType != null; }
            set { }
        }
    
        [XmlElement("PropertyInspectionPurposeTypeOtherDescription", Order = 5)]
        public MISMOString PropertyInspectionPurposeTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PropertyInspectionPurposeTypeOtherDescriptionSpecified
        {
            get { return this.PropertyInspectionPurposeTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PropertyInspectionRequestCommentDescription", Order = 6)]
        public MISMOString PropertyInspectionRequestCommentDescription { get; set; }
    
        [XmlIgnore]
        public bool PropertyInspectionRequestCommentDescriptionSpecified
        {
            get { return this.PropertyInspectionRequestCommentDescription != null; }
            set { }
        }
    
        [XmlElement("PropertyInspectionResultCommentDescription", Order = 7)]
        public MISMOString PropertyInspectionResultCommentDescription { get; set; }
    
        [XmlIgnore]
        public bool PropertyInspectionResultCommentDescriptionSpecified
        {
            get { return this.PropertyInspectionResultCommentDescription != null; }
            set { }
        }
    
        [XmlElement("PropertyInspectionType", Order = 8)]
        public MISMOEnum<PropertyInspectionBase> PropertyInspectionType { get; set; }
    
        [XmlIgnore]
        public bool PropertyInspectionTypeSpecified
        {
            get { return this.PropertyInspectionType != null; }
            set { }
        }
    
        [XmlElement("PropertyInspectionTypeOtherDescription", Order = 9)]
        public MISMOString PropertyInspectionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PropertyInspectionTypeOtherDescriptionSpecified
        {
            get { return this.PropertyInspectionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 10)]
        public INSPECTION_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
