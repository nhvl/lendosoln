namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class INSPECTION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ImagesSpecified
                    || this.InspectionDetailSpecified
                    || this.PropertyPhotoInformationsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("IMAGES", Order = 0)]
        public IMAGES Images { get; set; }
    
        [XmlIgnore]
        public bool ImagesSpecified
        {
            get { return this.Images != null && this.Images.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("INSPECTION_DETAIL", Order = 1)]
        public INSPECTION_DETAIL InspectionDetail { get; set; }
    
        [XmlIgnore]
        public bool InspectionDetailSpecified
        {
            get { return this.InspectionDetail != null && this.InspectionDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PROPERTY_PHOTO_INFORMATIONS", Order = 2)]
        public PROPERTY_PHOTO_INFORMATIONS PropertyPhotoInformations { get; set; }
    
        [XmlIgnore]
        public bool PropertyPhotoInformationsSpecified
        {
            get { return this.PropertyPhotoInformations != null && this.PropertyPhotoInformations.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public INSPECTION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
