namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class INSPECTIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.InspectionListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("INSPECTION", Order = 0)]
        public List<INSPECTION> InspectionList { get; set; } = new List<INSPECTION>();
    
        [XmlIgnore]
        public bool InspectionListSpecified
        {
            get { return this.InspectionList != null && this.InspectionList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public INSPECTIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
