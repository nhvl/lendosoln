namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class RECORDING_ERRORS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.RecordingErrorListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("RECORDING_ERROR", Order = 0)]
        public List<RECORDING_ERROR> RecordingErrorList { get; set; } = new List<RECORDING_ERROR>();
    
        [XmlIgnore]
        public bool RecordingErrorListSpecified
        {
            get { return this.RecordingErrorList != null && this.RecordingErrorList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public RECORDING_ERRORS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
