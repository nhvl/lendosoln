namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class RECORDING_ERROR
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.RecordingErrorDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("RecordingErrorDescription", Order = 0)]
        public MISMOString RecordingErrorDescription { get; set; }
    
        [XmlIgnore]
        public bool RecordingErrorDescriptionSpecified
        {
            get { return this.RecordingErrorDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public RECORDING_ERROR_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
