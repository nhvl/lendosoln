namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ROOM
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.FloorCoveringSpecified
                    || this.RoomDetailSpecified
                    || this.RoomFeaturesSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("FLOOR_COVERING", Order = 0)]
        public FLOOR_COVERING FloorCovering { get; set; }
    
        [XmlIgnore]
        public bool FloorCoveringSpecified
        {
            get { return this.FloorCovering != null && this.FloorCovering.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("ROOM_DETAIL", Order = 1)]
        public ROOM_DETAIL RoomDetail { get; set; }
    
        [XmlIgnore]
        public bool RoomDetailSpecified
        {
            get { return this.RoomDetail != null && this.RoomDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("ROOM_FEATURES", Order = 2)]
        public ROOM_FEATURES RoomFeatures { get; set; }
    
        [XmlIgnore]
        public bool RoomFeaturesSpecified
        {
            get { return this.RoomFeatures != null && this.RoomFeatures.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public ROOM_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
