namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class ROOMS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.RoomListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ROOM", Order = 0)]
        public List<ROOM> RoomList { get; set; } = new List<ROOM>();
    
        [XmlIgnore]
        public bool RoomListSpecified
        {
            get { return this.RoomList != null && this.RoomList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public ROOMS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
