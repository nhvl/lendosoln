namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ROOM_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AboveGradeIndicatorSpecified
                    || this.ComponentAdjustmentAmountSpecified
                    || this.ConditionRatingDescriptionSpecified
                    || this.ConditionRatingTypeSpecified
                    || this.LengthFeetNumberSpecified
                    || this.LevelTypeSpecified
                    || this.LevelTypeOtherDescriptionSpecified
                    || this.RoomTypeSpecified
                    || this.RoomTypeOtherDescriptionSpecified
                    || this.RoomTypeSummaryCountSpecified
                    || this.SquareFeetNumberSpecified
                    || this.WidthFeetNumberSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AboveGradeIndicator", Order = 0)]
        public MISMOIndicator AboveGradeIndicator { get; set; }
    
        [XmlIgnore]
        public bool AboveGradeIndicatorSpecified
        {
            get { return this.AboveGradeIndicator != null; }
            set { }
        }
    
        [XmlElement("ComponentAdjustmentAmount", Order = 1)]
        public MISMOAmount ComponentAdjustmentAmount { get; set; }
    
        [XmlIgnore]
        public bool ComponentAdjustmentAmountSpecified
        {
            get { return this.ComponentAdjustmentAmount != null; }
            set { }
        }
    
        [XmlElement("ConditionRatingDescription", Order = 2)]
        public MISMOString ConditionRatingDescription { get; set; }
    
        [XmlIgnore]
        public bool ConditionRatingDescriptionSpecified
        {
            get { return this.ConditionRatingDescription != null; }
            set { }
        }
    
        [XmlElement("ConditionRatingType", Order = 3)]
        public MISMOEnum<ConditionRatingBase> ConditionRatingType { get; set; }
    
        [XmlIgnore]
        public bool ConditionRatingTypeSpecified
        {
            get { return this.ConditionRatingType != null; }
            set { }
        }
    
        [XmlElement("LengthFeetNumber", Order = 4)]
        public MISMONumeric LengthFeetNumber { get; set; }
    
        [XmlIgnore]
        public bool LengthFeetNumberSpecified
        {
            get { return this.LengthFeetNumber != null; }
            set { }
        }
    
        [XmlElement("LevelType", Order = 5)]
        public MISMOEnum<LevelBase> LevelType { get; set; }
    
        [XmlIgnore]
        public bool LevelTypeSpecified
        {
            get { return this.LevelType != null; }
            set { }
        }
    
        [XmlElement("LevelTypeOtherDescription", Order = 6)]
        public MISMOString LevelTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool LevelTypeOtherDescriptionSpecified
        {
            get { return this.LevelTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("RoomType", Order = 7)]
        public MISMOEnum<RoomBase> RoomType { get; set; }
    
        [XmlIgnore]
        public bool RoomTypeSpecified
        {
            get { return this.RoomType != null; }
            set { }
        }
    
        [XmlElement("RoomTypeOtherDescription", Order = 8)]
        public MISMOString RoomTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool RoomTypeOtherDescriptionSpecified
        {
            get { return this.RoomTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("RoomTypeSummaryCount", Order = 9)]
        public MISMOCount RoomTypeSummaryCount { get; set; }
    
        [XmlIgnore]
        public bool RoomTypeSummaryCountSpecified
        {
            get { return this.RoomTypeSummaryCount != null; }
            set { }
        }
    
        [XmlElement("SquareFeetNumber", Order = 10)]
        public MISMONumeric SquareFeetNumber { get; set; }
    
        [XmlIgnore]
        public bool SquareFeetNumberSpecified
        {
            get { return this.SquareFeetNumber != null; }
            set { }
        }
    
        [XmlElement("WidthFeetNumber", Order = 11)]
        public MISMONumeric WidthFeetNumber { get; set; }
    
        [XmlIgnore]
        public bool WidthFeetNumberSpecified
        {
            get { return this.WidthFeetNumber != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 12)]
        public ROOM_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
