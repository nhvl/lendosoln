namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ESCROW_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EscrowAccountInitialBalanceAmountSpecified
                    || this.EscrowAccountMinimumBalanceAmountSpecified
                    || this.EscrowAccountRolloverAmountSpecified
                    || this.EscrowAccountShortageAmountSpecified
                    || this.EscrowAccountShortageRepaymentPeriodYearsCountSpecified
                    || this.EscrowAggregateAccountingAdjustmentAmountSpecified
                    || this.EscrowBalanceAmountSpecified
                    || this.EscrowCushionNumberOfMonthsCountSpecified
                    || this.GFEDisclosedInitialEscrowBalanceAmountSpecified
                    || this.InitialEscrowDepositIncludesAllInsuranceIndicatorSpecified
                    || this.InitialEscrowDepositIncludesAllPropertyTaxesIndicatorSpecified
                    || this.InitialEscrowDepositIncludesOtherDescriptionSpecified
                    || this.InitialEscrowProjectionStatementDateSpecified
                    || this.InterestOnEscrowAccruedAmountSpecified
                    || this.InterestOnEscrowAccruedThroughDateSpecified
                    || this.InterestOnEscrowBackupWithholdingIndicatorSpecified
                    || this.InterestOnEscrowIndicatorSpecified
                    || this.InterestOnEscrowYearToDatePostedAmountSpecified
                    || this.InterestOnRestrictedEscrowIndicatorSpecified
                    || this.LastEscrowAnalysisDateSpecified
                    || this.LastInterestOnEscrowPostedDateSpecified
                    || this.MIEscrowIncludedInAggregateIndicatorSpecified
                    || this.RestrictedEscrowBalanceAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("EscrowAccountInitialBalanceAmount", Order = 0)]
        public MISMOAmount EscrowAccountInitialBalanceAmount { get; set; }
    
        [XmlIgnore]
        public bool EscrowAccountInitialBalanceAmountSpecified
        {
            get { return this.EscrowAccountInitialBalanceAmount != null; }
            set { }
        }
    
        [XmlElement("EscrowAccountMinimumBalanceAmount", Order = 1)]
        public MISMOAmount EscrowAccountMinimumBalanceAmount { get; set; }
    
        [XmlIgnore]
        public bool EscrowAccountMinimumBalanceAmountSpecified
        {
            get { return this.EscrowAccountMinimumBalanceAmount != null; }
            set { }
        }
    
        [XmlElement("EscrowAccountRolloverAmount", Order = 2)]
        public MISMOAmount EscrowAccountRolloverAmount { get; set; }
    
        [XmlIgnore]
        public bool EscrowAccountRolloverAmountSpecified
        {
            get { return this.EscrowAccountRolloverAmount != null; }
            set { }
        }
    
        [XmlElement("EscrowAccountShortageAmount", Order = 3)]
        public MISMOAmount EscrowAccountShortageAmount { get; set; }
    
        [XmlIgnore]
        public bool EscrowAccountShortageAmountSpecified
        {
            get { return this.EscrowAccountShortageAmount != null; }
            set { }
        }
    
        [XmlElement("EscrowAccountShortageRepaymentPeriodYearsCount", Order = 4)]
        public MISMOCount EscrowAccountShortageRepaymentPeriodYearsCount { get; set; }
    
        [XmlIgnore]
        public bool EscrowAccountShortageRepaymentPeriodYearsCountSpecified
        {
            get { return this.EscrowAccountShortageRepaymentPeriodYearsCount != null; }
            set { }
        }
    
        [XmlElement("EscrowAggregateAccountingAdjustmentAmount", Order = 5)]
        public MISMOAmount EscrowAggregateAccountingAdjustmentAmount { get; set; }
    
        [XmlIgnore]
        public bool EscrowAggregateAccountingAdjustmentAmountSpecified
        {
            get { return this.EscrowAggregateAccountingAdjustmentAmount != null; }
            set { }
        }
    
        [XmlElement("EscrowBalanceAmount", Order = 6)]
        public MISMOAmount EscrowBalanceAmount { get; set; }
    
        [XmlIgnore]
        public bool EscrowBalanceAmountSpecified
        {
            get { return this.EscrowBalanceAmount != null; }
            set { }
        }
    
        [XmlElement("EscrowCushionNumberOfMonthsCount", Order = 7)]
        public MISMOCount EscrowCushionNumberOfMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool EscrowCushionNumberOfMonthsCountSpecified
        {
            get { return this.EscrowCushionNumberOfMonthsCount != null; }
            set { }
        }
    
        [XmlElement("GFEDisclosedInitialEscrowBalanceAmount", Order = 8)]
        public MISMOAmount GFEDisclosedInitialEscrowBalanceAmount { get; set; }
    
        [XmlIgnore]
        public bool GFEDisclosedInitialEscrowBalanceAmountSpecified
        {
            get { return this.GFEDisclosedInitialEscrowBalanceAmount != null; }
            set { }
        }
    
        [XmlElement("InitialEscrowDepositIncludesAllInsuranceIndicator", Order = 9)]
        public MISMOIndicator InitialEscrowDepositIncludesAllInsuranceIndicator { get; set; }
    
        [XmlIgnore]
        public bool InitialEscrowDepositIncludesAllInsuranceIndicatorSpecified
        {
            get { return this.InitialEscrowDepositIncludesAllInsuranceIndicator != null; }
            set { }
        }
    
        [XmlElement("InitialEscrowDepositIncludesAllPropertyTaxesIndicator", Order = 10)]
        public MISMOIndicator InitialEscrowDepositIncludesAllPropertyTaxesIndicator { get; set; }
    
        [XmlIgnore]
        public bool InitialEscrowDepositIncludesAllPropertyTaxesIndicatorSpecified
        {
            get { return this.InitialEscrowDepositIncludesAllPropertyTaxesIndicator != null; }
            set { }
        }
    
        [XmlElement("InitialEscrowDepositIncludesOtherDescription", Order = 11)]
        public MISMOString InitialEscrowDepositIncludesOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool InitialEscrowDepositIncludesOtherDescriptionSpecified
        {
            get { return this.InitialEscrowDepositIncludesOtherDescription != null; }
            set { }
        }
    
        [XmlElement("InitialEscrowProjectionStatementDate", Order = 12)]
        public MISMODate InitialEscrowProjectionStatementDate { get; set; }
    
        [XmlIgnore]
        public bool InitialEscrowProjectionStatementDateSpecified
        {
            get { return this.InitialEscrowProjectionStatementDate != null; }
            set { }
        }
    
        [XmlElement("InterestOnEscrowAccruedAmount", Order = 13)]
        public MISMOAmount InterestOnEscrowAccruedAmount { get; set; }
    
        [XmlIgnore]
        public bool InterestOnEscrowAccruedAmountSpecified
        {
            get { return this.InterestOnEscrowAccruedAmount != null; }
            set { }
        }
    
        [XmlElement("InterestOnEscrowAccruedThroughDate", Order = 14)]
        public MISMODate InterestOnEscrowAccruedThroughDate { get; set; }
    
        [XmlIgnore]
        public bool InterestOnEscrowAccruedThroughDateSpecified
        {
            get { return this.InterestOnEscrowAccruedThroughDate != null; }
            set { }
        }
    
        [XmlElement("InterestOnEscrowBackupWithholdingIndicator", Order = 15)]
        public MISMOIndicator InterestOnEscrowBackupWithholdingIndicator { get; set; }
    
        [XmlIgnore]
        public bool InterestOnEscrowBackupWithholdingIndicatorSpecified
        {
            get { return this.InterestOnEscrowBackupWithholdingIndicator != null; }
            set { }
        }
    
        [XmlElement("InterestOnEscrowIndicator", Order = 16)]
        public MISMOIndicator InterestOnEscrowIndicator { get; set; }
    
        [XmlIgnore]
        public bool InterestOnEscrowIndicatorSpecified
        {
            get { return this.InterestOnEscrowIndicator != null; }
            set { }
        }
    
        [XmlElement("InterestOnEscrowYearToDatePostedAmount", Order = 17)]
        public MISMOAmount InterestOnEscrowYearToDatePostedAmount { get; set; }
    
        [XmlIgnore]
        public bool InterestOnEscrowYearToDatePostedAmountSpecified
        {
            get { return this.InterestOnEscrowYearToDatePostedAmount != null; }
            set { }
        }
    
        [XmlElement("InterestOnRestrictedEscrowIndicator", Order = 18)]
        public MISMOIndicator InterestOnRestrictedEscrowIndicator { get; set; }
    
        [XmlIgnore]
        public bool InterestOnRestrictedEscrowIndicatorSpecified
        {
            get { return this.InterestOnRestrictedEscrowIndicator != null; }
            set { }
        }
    
        [XmlElement("LastEscrowAnalysisDate", Order = 19)]
        public MISMODate LastEscrowAnalysisDate { get; set; }
    
        [XmlIgnore]
        public bool LastEscrowAnalysisDateSpecified
        {
            get { return this.LastEscrowAnalysisDate != null; }
            set { }
        }
    
        [XmlElement("LastInterestOnEscrowPostedDate", Order = 20)]
        public MISMODate LastInterestOnEscrowPostedDate { get; set; }
    
        [XmlIgnore]
        public bool LastInterestOnEscrowPostedDateSpecified
        {
            get { return this.LastInterestOnEscrowPostedDate != null; }
            set { }
        }
    
        [XmlElement("MIEscrowIncludedInAggregateIndicator", Order = 21)]
        public MISMOIndicator MIEscrowIncludedInAggregateIndicator { get; set; }
    
        [XmlIgnore]
        public bool MIEscrowIncludedInAggregateIndicatorSpecified
        {
            get { return this.MIEscrowIncludedInAggregateIndicator != null; }
            set { }
        }
    
        [XmlElement("RestrictedEscrowBalanceAmount", Order = 22)]
        public MISMOAmount RestrictedEscrowBalanceAmount { get; set; }
    
        [XmlIgnore]
        public bool RestrictedEscrowBalanceAmountSpecified
        {
            get { return this.RestrictedEscrowBalanceAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 23)]
        public ESCROW_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
