namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ESCROW
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EscrowDetailSpecified
                    || this.EscrowDisclosuresSpecified
                    || this.EscrowItemsSpecified
                    || this.HoldbackItemsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ESCROW_DETAIL", Order = 0)]
        public ESCROW_DETAIL EscrowDetail { get; set; }
    
        [XmlIgnore]
        public bool EscrowDetailSpecified
        {
            get { return this.EscrowDetail != null && this.EscrowDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("ESCROW_DISCLOSURES", Order = 1)]
        public ESCROW_DISCLOSURES EscrowDisclosures { get; set; }
    
        [XmlIgnore]
        public bool EscrowDisclosuresSpecified
        {
            get { return this.EscrowDisclosures != null && this.EscrowDisclosures.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("ESCROW_ITEMS", Order = 2)]
        public ESCROW_ITEMS EscrowItems { get; set; }
    
        [XmlIgnore]
        public bool EscrowItemsSpecified
        {
            get { return this.EscrowItems != null && this.EscrowItems.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("HOLDBACK_ITEMS", Order = 3)]
        public HOLDBACK_ITEMS HoldbackItems { get; set; }
    
        [XmlIgnore]
        public bool HoldbackItemsSpecified
        {
            get { return this.HoldbackItems != null && this.HoldbackItems.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public ESCROW_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
