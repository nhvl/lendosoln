namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class RESIDENTIAL_RENTAL_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DataSourceDescriptionSpecified
                    || this.ExpenseAdjustedMonthlyRentAmountSpecified
                    || this.LeaseExpirationDateSpecified
                    || this.LeaseStartDateSpecified
                    || this.MarketAdjustedMonthlyRentAmountSpecified
                    || this.MonthlyFurnitureRentAmountSpecified
                    || this.MonthlyRentAmountSpecified
                    || this.RentalMonthlyUtilitiesCostAmountSpecified
                    || this.RentTotalAdjustmentAmountSpecified
                    || this.RentTotalAdjustmentGrossPercentSpecified
                    || this.RentTotalAdjustmentNetPercentSpecified
                    || this.RentTotalAdjustmentPositiveIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DataSourceDescription", Order = 0)]
        public MISMOString DataSourceDescription { get; set; }
    
        [XmlIgnore]
        public bool DataSourceDescriptionSpecified
        {
            get { return this.DataSourceDescription != null; }
            set { }
        }
    
        [XmlElement("ExpenseAdjustedMonthlyRentAmount", Order = 1)]
        public MISMOAmount ExpenseAdjustedMonthlyRentAmount { get; set; }
    
        [XmlIgnore]
        public bool ExpenseAdjustedMonthlyRentAmountSpecified
        {
            get { return this.ExpenseAdjustedMonthlyRentAmount != null; }
            set { }
        }
    
        [XmlElement("LeaseExpirationDate", Order = 2)]
        public MISMODate LeaseExpirationDate { get; set; }
    
        [XmlIgnore]
        public bool LeaseExpirationDateSpecified
        {
            get { return this.LeaseExpirationDate != null; }
            set { }
        }
    
        [XmlElement("LeaseStartDate", Order = 3)]
        public MISMODate LeaseStartDate { get; set; }
    
        [XmlIgnore]
        public bool LeaseStartDateSpecified
        {
            get { return this.LeaseStartDate != null; }
            set { }
        }
    
        [XmlElement("MarketAdjustedMonthlyRentAmount", Order = 4)]
        public MISMOAmount MarketAdjustedMonthlyRentAmount { get; set; }
    
        [XmlIgnore]
        public bool MarketAdjustedMonthlyRentAmountSpecified
        {
            get { return this.MarketAdjustedMonthlyRentAmount != null; }
            set { }
        }
    
        [XmlElement("MonthlyFurnitureRentAmount", Order = 5)]
        public MISMOAmount MonthlyFurnitureRentAmount { get; set; }
    
        [XmlIgnore]
        public bool MonthlyFurnitureRentAmountSpecified
        {
            get { return this.MonthlyFurnitureRentAmount != null; }
            set { }
        }
    
        [XmlElement("MonthlyRentAmount", Order = 6)]
        public MISMOAmount MonthlyRentAmount { get; set; }
    
        [XmlIgnore]
        public bool MonthlyRentAmountSpecified
        {
            get { return this.MonthlyRentAmount != null; }
            set { }
        }
    
        [XmlElement("RentalMonthlyUtilitiesCostAmount", Order = 7)]
        public MISMOAmount RentalMonthlyUtilitiesCostAmount { get; set; }
    
        [XmlIgnore]
        public bool RentalMonthlyUtilitiesCostAmountSpecified
        {
            get { return this.RentalMonthlyUtilitiesCostAmount != null; }
            set { }
        }
    
        [XmlElement("RentTotalAdjustmentAmount", Order = 8)]
        public MISMOAmount RentTotalAdjustmentAmount { get; set; }
    
        [XmlIgnore]
        public bool RentTotalAdjustmentAmountSpecified
        {
            get { return this.RentTotalAdjustmentAmount != null; }
            set { }
        }
    
        [XmlElement("RentTotalAdjustmentGrossPercent", Order = 9)]
        public MISMOPercent RentTotalAdjustmentGrossPercent { get; set; }
    
        [XmlIgnore]
        public bool RentTotalAdjustmentGrossPercentSpecified
        {
            get { return this.RentTotalAdjustmentGrossPercent != null; }
            set { }
        }
    
        [XmlElement("RentTotalAdjustmentNetPercent", Order = 10)]
        public MISMOPercent RentTotalAdjustmentNetPercent { get; set; }
    
        [XmlIgnore]
        public bool RentTotalAdjustmentNetPercentSpecified
        {
            get { return this.RentTotalAdjustmentNetPercent != null; }
            set { }
        }
    
        [XmlElement("RentTotalAdjustmentPositiveIndicator", Order = 11)]
        public MISMOIndicator RentTotalAdjustmentPositiveIndicator { get; set; }
    
        [XmlIgnore]
        public bool RentTotalAdjustmentPositiveIndicatorSpecified
        {
            get { return this.RentTotalAdjustmentPositiveIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 12)]
        public RESIDENTIAL_RENTAL_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
