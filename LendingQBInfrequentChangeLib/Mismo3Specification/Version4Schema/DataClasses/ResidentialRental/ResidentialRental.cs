namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class RESIDENTIAL_RENTAL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.RentAdjustmentsSpecified
                    || this.ResidentialRentalDetailSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("RENT_ADJUSTMENTS", Order = 0)]
        public RENT_ADJUSTMENTS RentAdjustments { get; set; }
    
        [XmlIgnore]
        public bool RentAdjustmentsSpecified
        {
            get { return this.RentAdjustments != null && this.RentAdjustments.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("RESIDENTIAL_RENTAL_DETAIL", Order = 1)]
        public RESIDENTIAL_RENTAL_DETAIL ResidentialRentalDetail { get; set; }
    
        [XmlIgnore]
        public bool ResidentialRentalDetailSpecified
        {
            get { return this.ResidentialRentalDetail != null && this.ResidentialRentalDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public RESIDENTIAL_RENTAL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
