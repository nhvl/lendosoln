namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MI_APPLICATION_RESPONSE_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MI_LTVPercentSpecified
                    || this.MIApplicationTypeSpecified
                    || this.MIApplicationTypeOtherDescriptionSpecified
                    || this.MICertificateExpirationDateSpecified
                    || this.MICertificateIdentifierSpecified
                    || this.MICertificateTypeSpecified
                    || this.MICommentDescriptionSpecified
                    || this.MICompanyNameTypeSpecified
                    || this.MICompanyNameTypeOtherDescriptionSpecified
                    || this.MICoveragePercentSpecified
                    || this.MICoveragePlanTypeSpecified
                    || this.MICoveragePlanTypeOtherDescriptionSpecified
                    || this.MIDecisionTypeSpecified
                    || this.MIDurationTypeSpecified
                    || this.MIDurationTypeOtherDescriptionSpecified
                    || this.MIInitialPremiumAtClosingTypeSpecified
                    || this.MIInitialPremiumAtClosingTypeOtherDescriptionSpecified
                    || this.MILenderIdentifierSpecified
                    || this.MIPremiumFromClosingAmountSpecified
                    || this.MIPremiumPaymentTypeSpecified
                    || this.MIPremiumPaymentTypeOtherDescriptionSpecified
                    || this.MIPremiumRatePlanTypeSpecified
                    || this.MIPremiumRatePlanTypeOtherDescriptionSpecified
                    || this.MIPremiumRefundableTypeSpecified
                    || this.MIPremiumRefundableTypeOtherDescriptionSpecified
                    || this.MIPremiumSourceTypeSpecified
                    || this.MIPremiumSourceTypeOtherDescriptionSpecified
                    || this.MIServiceTypeSpecified
                    || this.MIServiceTypeOtherDescriptionSpecified
                    || this.MISourceTypeSpecified
                    || this.MISourceTypeOtherDescriptionSpecified
                    || this.MITransactionIdentifierSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MI_LTVPercent", Order = 0)]
        public MISMOPercent MI_LTVPercent { get; set; }
    
        [XmlIgnore]
        public bool MI_LTVPercentSpecified
        {
            get { return this.MI_LTVPercent != null; }
            set { }
        }
    
        [XmlElement("MIApplicationType", Order = 1)]
        public MISMOEnum<MIApplicationBase> MIApplicationType { get; set; }
    
        [XmlIgnore]
        public bool MIApplicationTypeSpecified
        {
            get { return this.MIApplicationType != null; }
            set { }
        }
    
        [XmlElement("MIApplicationTypeOtherDescription", Order = 2)]
        public MISMOString MIApplicationTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MIApplicationTypeOtherDescriptionSpecified
        {
            get { return this.MIApplicationTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MICertificateExpirationDate", Order = 3)]
        public MISMODate MICertificateExpirationDate { get; set; }
    
        [XmlIgnore]
        public bool MICertificateExpirationDateSpecified
        {
            get { return this.MICertificateExpirationDate != null; }
            set { }
        }
    
        [XmlElement("MICertificateIdentifier", Order = 4)]
        public MISMOIdentifier MICertificateIdentifier { get; set; }
    
        [XmlIgnore]
        public bool MICertificateIdentifierSpecified
        {
            get { return this.MICertificateIdentifier != null; }
            set { }
        }
    
        [XmlElement("MICertificateType", Order = 5)]
        public MISMOEnum<MICertificateBase> MICertificateType { get; set; }
    
        [XmlIgnore]
        public bool MICertificateTypeSpecified
        {
            get { return this.MICertificateType != null; }
            set { }
        }
    
        [XmlElement("MICommentDescription", Order = 6)]
        public MISMOString MICommentDescription { get; set; }
    
        [XmlIgnore]
        public bool MICommentDescriptionSpecified
        {
            get { return this.MICommentDescription != null; }
            set { }
        }
    
        [XmlElement("MICompanyNameType", Order = 7)]
        public MISMOEnum<MICompanyNameBase> MICompanyNameType { get; set; }
    
        [XmlIgnore]
        public bool MICompanyNameTypeSpecified
        {
            get { return this.MICompanyNameType != null; }
            set { }
        }
    
        [XmlElement("MICompanyNameTypeOtherDescription", Order = 8)]
        public MISMOString MICompanyNameTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MICompanyNameTypeOtherDescriptionSpecified
        {
            get { return this.MICompanyNameTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MICoveragePercent", Order = 9)]
        public MISMOPercent MICoveragePercent { get; set; }
    
        [XmlIgnore]
        public bool MICoveragePercentSpecified
        {
            get { return this.MICoveragePercent != null; }
            set { }
        }
    
        [XmlElement("MICoveragePlanType", Order = 10)]
        public MISMOEnum<MICoveragePlanBase> MICoveragePlanType { get; set; }
    
        [XmlIgnore]
        public bool MICoveragePlanTypeSpecified
        {
            get { return this.MICoveragePlanType != null; }
            set { }
        }
    
        [XmlElement("MICoveragePlanTypeOtherDescription", Order = 11)]
        public MISMOString MICoveragePlanTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MICoveragePlanTypeOtherDescriptionSpecified
        {
            get { return this.MICoveragePlanTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MIDecisionType", Order = 12)]
        public MISMOEnum<MIDecisionBase> MIDecisionType { get; set; }
    
        [XmlIgnore]
        public bool MIDecisionTypeSpecified
        {
            get { return this.MIDecisionType != null; }
            set { }
        }
    
        [XmlElement("MIDurationType", Order = 13)]
        public MISMOEnum<MIDurationBase> MIDurationType { get; set; }
    
        [XmlIgnore]
        public bool MIDurationTypeSpecified
        {
            get { return this.MIDurationType != null; }
            set { }
        }
    
        [XmlElement("MIDurationTypeOtherDescription", Order = 14)]
        public MISMOString MIDurationTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MIDurationTypeOtherDescriptionSpecified
        {
            get { return this.MIDurationTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MIInitialPremiumAtClosingType", Order = 15)]
        public MISMOEnum<MIInitialPremiumAtClosingBase> MIInitialPremiumAtClosingType { get; set; }
    
        [XmlIgnore]
        public bool MIInitialPremiumAtClosingTypeSpecified
        {
            get { return this.MIInitialPremiumAtClosingType != null; }
            set { }
        }
    
        [XmlElement("MIInitialPremiumAtClosingTypeOtherDescription", Order = 16)]
        public MISMOString MIInitialPremiumAtClosingTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MIInitialPremiumAtClosingTypeOtherDescriptionSpecified
        {
            get { return this.MIInitialPremiumAtClosingTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MILenderIdentifier", Order = 17)]
        public MISMOIdentifier MILenderIdentifier { get; set; }
    
        [XmlIgnore]
        public bool MILenderIdentifierSpecified
        {
            get { return this.MILenderIdentifier != null; }
            set { }
        }
    
        [XmlElement("MIPremiumFromClosingAmount", Order = 18)]
        public MISMOAmount MIPremiumFromClosingAmount { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumFromClosingAmountSpecified
        {
            get { return this.MIPremiumFromClosingAmount != null; }
            set { }
        }
    
        [XmlElement("MIPremiumPaymentType", Order = 19)]
        public MISMOEnum<MIPremiumPaymentBase> MIPremiumPaymentType { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumPaymentTypeSpecified
        {
            get { return this.MIPremiumPaymentType != null; }
            set { }
        }
    
        [XmlElement("MIPremiumPaymentTypeOtherDescription", Order = 20)]
        public MISMOString MIPremiumPaymentTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumPaymentTypeOtherDescriptionSpecified
        {
            get { return this.MIPremiumPaymentTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MIPremiumRatePlanType", Order = 21)]
        public MISMOEnum<MIPremiumRatePlanBase> MIPremiumRatePlanType { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumRatePlanTypeSpecified
        {
            get { return this.MIPremiumRatePlanType != null; }
            set { }
        }
    
        [XmlElement("MIPremiumRatePlanTypeOtherDescription", Order = 22)]
        public MISMOString MIPremiumRatePlanTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumRatePlanTypeOtherDescriptionSpecified
        {
            get { return this.MIPremiumRatePlanTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MIPremiumRefundableType", Order = 23)]
        public MISMOEnum<MIPremiumRefundableBase> MIPremiumRefundableType { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumRefundableTypeSpecified
        {
            get { return this.MIPremiumRefundableType != null; }
            set { }
        }
    
        [XmlElement("MIPremiumRefundableTypeOtherDescription", Order = 24)]
        public MISMOString MIPremiumRefundableTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumRefundableTypeOtherDescriptionSpecified
        {
            get { return this.MIPremiumRefundableTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MIPremiumSourceType", Order = 25)]
        public MISMOEnum<MIPremiumSourceBase> MIPremiumSourceType { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumSourceTypeSpecified
        {
            get { return this.MIPremiumSourceType != null; }
            set { }
        }
    
        [XmlElement("MIPremiumSourceTypeOtherDescription", Order = 26)]
        public MISMOString MIPremiumSourceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MIPremiumSourceTypeOtherDescriptionSpecified
        {
            get { return this.MIPremiumSourceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MIServiceType", Order = 27)]
        public MISMOEnum<MIServiceBase> MIServiceType { get; set; }
    
        [XmlIgnore]
        public bool MIServiceTypeSpecified
        {
            get { return this.MIServiceType != null; }
            set { }
        }
    
        [XmlElement("MIServiceTypeOtherDescription", Order = 28)]
        public MISMOString MIServiceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MIServiceTypeOtherDescriptionSpecified
        {
            get { return this.MIServiceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MISourceType", Order = 29)]
        public MISMOEnum<MISourceBase> MISourceType { get; set; }
    
        [XmlIgnore]
        public bool MISourceTypeSpecified
        {
            get { return this.MISourceType != null; }
            set { }
        }
    
        [XmlElement("MISourceTypeOtherDescription", Order = 30)]
        public MISMOString MISourceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MISourceTypeOtherDescriptionSpecified
        {
            get { return this.MISourceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MITransactionIdentifier", Order = 31)]
        public MISMOIdentifier MITransactionIdentifier { get; set; }
    
        [XmlIgnore]
        public bool MITransactionIdentifierSpecified
        {
            get { return this.MITransactionIdentifier != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 32)]
        public MI_APPLICATION_RESPONSE_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
