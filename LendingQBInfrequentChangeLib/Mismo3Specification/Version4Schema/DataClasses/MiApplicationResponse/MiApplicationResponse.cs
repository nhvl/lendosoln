namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MI_APPLICATION_RESPONSE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ConditionsSpecified
                    || this.ContactPointsSpecified
                    || this.LoanIdentifiersSpecified
                    || this.MiApplicationResponseDetailSpecified
                    || this.MiPremiumTaxesSpecified
                    || this.MiPremiumsSpecified
                    || this.PartiesSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CONDITIONS", Order = 0)]
        public CONDITIONS Conditions { get; set; }
    
        [XmlIgnore]
        public bool ConditionsSpecified
        {
            get { return this.Conditions != null && this.Conditions.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CONTACT_POINTS", Order = 1)]
        public CONTACT_POINTS ContactPoints { get; set; }
    
        [XmlIgnore]
        public bool ContactPointsSpecified
        {
            get { return this.ContactPoints != null && this.ContactPoints.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("LOAN_IDENTIFIERS", Order = 2)]
        public LOAN_IDENTIFIERS LoanIdentifiers { get; set; }
    
        [XmlIgnore]
        public bool LoanIdentifiersSpecified
        {
            get { return this.LoanIdentifiers != null && this.LoanIdentifiers.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("MI_APPLICATION_RESPONSE_DETAIL", Order = 3)]
        public MI_APPLICATION_RESPONSE_DETAIL MiApplicationResponseDetail { get; set; }
    
        [XmlIgnore]
        public bool MiApplicationResponseDetailSpecified
        {
            get { return this.MiApplicationResponseDetail != null && this.MiApplicationResponseDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("MI_PREMIUM_TAXES", Order = 4)]
        public MI_PREMIUM_TAXES MiPremiumTaxes { get; set; }
    
        [XmlIgnore]
        public bool MiPremiumTaxesSpecified
        {
            get { return this.MiPremiumTaxes != null && this.MiPremiumTaxes.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("MI_PREMIUMS", Order = 5)]
        public MI_PREMIUMS MiPremiums { get; set; }
    
        [XmlIgnore]
        public bool MiPremiumsSpecified
        {
            get { return this.MiPremiums != null && this.MiPremiums.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PARTIES", Order = 6)]
        public PARTIES Parties { get; set; }
    
        [XmlIgnore]
        public bool PartiesSpecified
        {
            get { return this.Parties != null && this.Parties.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 7)]
        public MI_APPLICATION_RESPONSE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
