namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class NOTE_ADDENDUM
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.NoteAddendumTypeSpecified
                    || this.NoteAddendumTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("NoteAddendumType", Order = 0)]
        public MISMOEnum<NoteAddendumBase> NoteAddendumType { get; set; }
    
        [XmlIgnore]
        public bool NoteAddendumTypeSpecified
        {
            get { return this.NoteAddendumType != null; }
            set { }
        }
    
        [XmlElement("NoteAddendumTypeOtherDescription", Order = 1)]
        public MISMOString NoteAddendumTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool NoteAddendumTypeOtherDescriptionSpecified
        {
            get { return this.NoteAddendumTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public NOTE_ADDENDUM_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
