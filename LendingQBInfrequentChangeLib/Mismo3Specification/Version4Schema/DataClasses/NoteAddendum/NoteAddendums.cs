namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class NOTE_ADDENDUMS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.NoteAddendumListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("NOTE_ADDENDUM", Order = 0)]
        public List<NOTE_ADDENDUM> NoteAddendumList { get; set; } = new List<NOTE_ADDENDUM>();
    
        [XmlIgnore]
        public bool NoteAddendumListSpecified
        {
            get { return this.NoteAddendumList != null && this.NoteAddendumList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public NOTE_ADDENDUMS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
