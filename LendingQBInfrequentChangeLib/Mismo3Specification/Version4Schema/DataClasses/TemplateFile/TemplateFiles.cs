namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class TEMPLATE_FILES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.TemplateFileListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("TEMPLATE_FILE", Order = 0)]
        public List<TEMPLATE_FILE> TemplateFileList { get; set; } = new List<TEMPLATE_FILE>();
    
        [XmlIgnore]
        public bool TemplateFileListSpecified
        {
            get { return this.TemplateFileList != null && this.TemplateFileList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public TEMPLATE_FILES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
