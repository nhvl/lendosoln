namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SERVITUDE_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.IndividualPropertyServitudeDescriptionSpecified
                    || this.PrivatelyCreatedServitudeTypeSpecified
                    || this.PrivatelyCreatedServitudeTypeOtherDescriptionSpecified
                    || this.ServitudeDescriptionSpecified
                    || this.ServitudeTypeSpecified
                    || this.ServitudeTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("IndividualPropertyServitudeDescription", Order = 0)]
        public MISMOString IndividualPropertyServitudeDescription { get; set; }
    
        [XmlIgnore]
        public bool IndividualPropertyServitudeDescriptionSpecified
        {
            get { return this.IndividualPropertyServitudeDescription != null; }
            set { }
        }
    
        [XmlElement("PrivatelyCreatedServitudeType", Order = 1)]
        public MISMOEnum<PrivatelyCreatedServitudeBase> PrivatelyCreatedServitudeType { get; set; }
    
        [XmlIgnore]
        public bool PrivatelyCreatedServitudeTypeSpecified
        {
            get { return this.PrivatelyCreatedServitudeType != null; }
            set { }
        }
    
        [XmlElement("PrivatelyCreatedServitudeTypeOtherDescription", Order = 2)]
        public MISMOString PrivatelyCreatedServitudeTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PrivatelyCreatedServitudeTypeOtherDescriptionSpecified
        {
            get { return this.PrivatelyCreatedServitudeTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ServitudeDescription", Order = 3)]
        public MISMOString ServitudeDescription { get; set; }
    
        [XmlIgnore]
        public bool ServitudeDescriptionSpecified
        {
            get { return this.ServitudeDescription != null; }
            set { }
        }
    
        [XmlElement("ServitudeType", Order = 4)]
        public MISMOEnum<ServitudeBase> ServitudeType { get; set; }
    
        [XmlIgnore]
        public bool ServitudeTypeSpecified
        {
            get { return this.ServitudeType != null; }
            set { }
        }
    
        [XmlElement("ServitudeTypeOtherDescription", Order = 5)]
        public MISMOString ServitudeTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ServitudeTypeOtherDescriptionSpecified
        {
            get { return this.ServitudeTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 6)]
        public SERVITUDE_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
