namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class SERVITUDES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ServitudeListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SERVITUDE", Order = 0)]
        public List<SERVITUDE> ServitudeList { get; set; } = new List<SERVITUDE>();
    
        [XmlIgnore]
        public bool ServitudeListSpecified
        {
            get { return this.ServitudeList != null && this.ServitudeList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public SERVITUDES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
