namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SERVITUDE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ConditionCovenantRestrictionsSpecified
                    || this.ServitudeDetailSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CONDITION_COVENANT_RESTRICTIONS", Order = 0)]
        public CONDITION_COVENANT_RESTRICTIONS ConditionCovenantRestrictions { get; set; }
    
        [XmlIgnore]
        public bool ConditionCovenantRestrictionsSpecified
        {
            get { return this.ConditionCovenantRestrictions != null && this.ConditionCovenantRestrictions.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SERVITUDE_DETAIL", Order = 1)]
        public SERVITUDE_DETAIL ServitudeDetail { get; set; }
    
        [XmlIgnore]
        public bool ServitudeDetailSpecified
        {
            get { return this.ServitudeDetail != null && this.ServitudeDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public SERVITUDE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
