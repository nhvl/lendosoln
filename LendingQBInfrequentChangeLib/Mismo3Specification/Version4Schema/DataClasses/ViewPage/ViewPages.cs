namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class VIEW_PAGES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ViewPageListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("VIEW_PAGE", Order = 0)]
        public List<VIEW_PAGE> ViewPageList { get; set; } = new List<VIEW_PAGE>();
    
        [XmlIgnore]
        public bool ViewPageListSpecified
        {
            get { return this.ViewPageList != null && this.ViewPageList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public VIEW_PAGES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
