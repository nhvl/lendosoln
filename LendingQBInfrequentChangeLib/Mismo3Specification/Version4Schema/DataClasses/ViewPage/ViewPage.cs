namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class VIEW_PAGE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ViewPageFilesSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("VIEW_PAGE_FILES", Order = 0)]
        public VIEW_PAGE_FILES ViewPageFiles { get; set; }
    
        [XmlIgnore]
        public bool ViewPageFilesSpecified
        {
            get { return this.ViewPageFiles != null && this.ViewPageFiles.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public VIEW_PAGE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
