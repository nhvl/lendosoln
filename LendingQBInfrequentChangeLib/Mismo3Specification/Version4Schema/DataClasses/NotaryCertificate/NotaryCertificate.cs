namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class NOTARY_CERTIFICATE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.NotaryCertificateDetailSpecified
                    || this.NotaryCertificateSignerIdentificationSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("NOTARY_CERTIFICATE_DETAIL", Order = 0)]
        public NOTARY_CERTIFICATE_DETAIL NotaryCertificateDetail { get; set; }
    
        [XmlIgnore]
        public bool NotaryCertificateDetailSpecified
        {
            get { return this.NotaryCertificateDetail != null && this.NotaryCertificateDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("NOTARY_CERTIFICATE_SIGNER_IDENTIFICATION", Order = 1)]
        public NOTARY_CERTIFICATE_SIGNER_IDENTIFICATION NotaryCertificateSignerIdentification { get; set; }
    
        [XmlIgnore]
        public bool NotaryCertificateSignerIdentificationSpecified
        {
            get { return this.NotaryCertificateSignerIdentification != null && this.NotaryCertificateSignerIdentification.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public NOTARY_CERTIFICATE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
