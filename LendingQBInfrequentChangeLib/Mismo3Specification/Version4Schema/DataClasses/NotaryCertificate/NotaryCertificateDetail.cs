namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class NOTARY_CERTIFICATE_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.NotaryAppearanceDateSpecified
                    || this.NotaryAppearedBeforeNamesDescriptionSpecified
                    || this.NotaryAppearedBeforeTitlesDescriptionSpecified
                    || this.NotaryCertificateSignerCompanyNameSpecified
                    || this.NotaryCertificateSignerFullNameSpecified
                    || this.NotaryCertificateSignerTitleDescriptionSpecified
                    || this.NotaryCertificateSigningCountyNameSpecified
                    || this.NotaryCertificateSigningStateNameSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("NotaryAppearanceDate", Order = 0)]
        public MISMODate NotaryAppearanceDate { get; set; }
    
        [XmlIgnore]
        public bool NotaryAppearanceDateSpecified
        {
            get { return this.NotaryAppearanceDate != null; }
            set { }
        }
    
        [XmlElement("NotaryAppearedBeforeNamesDescription", Order = 1)]
        public MISMOString NotaryAppearedBeforeNamesDescription { get; set; }
    
        [XmlIgnore]
        public bool NotaryAppearedBeforeNamesDescriptionSpecified
        {
            get { return this.NotaryAppearedBeforeNamesDescription != null; }
            set { }
        }
    
        [XmlElement("NotaryAppearedBeforeTitlesDescription", Order = 2)]
        public MISMOString NotaryAppearedBeforeTitlesDescription { get; set; }
    
        [XmlIgnore]
        public bool NotaryAppearedBeforeTitlesDescriptionSpecified
        {
            get { return this.NotaryAppearedBeforeTitlesDescription != null; }
            set { }
        }
    
        [XmlElement("NotaryCertificateSignerCompanyName", Order = 3)]
        public MISMOString NotaryCertificateSignerCompanyName { get; set; }
    
        [XmlIgnore]
        public bool NotaryCertificateSignerCompanyNameSpecified
        {
            get { return this.NotaryCertificateSignerCompanyName != null; }
            set { }
        }
    
        [XmlElement("NotaryCertificateSignerFullName", Order = 4)]
        public MISMOString NotaryCertificateSignerFullName { get; set; }
    
        [XmlIgnore]
        public bool NotaryCertificateSignerFullNameSpecified
        {
            get { return this.NotaryCertificateSignerFullName != null; }
            set { }
        }
    
        [XmlElement("NotaryCertificateSignerTitleDescription", Order = 5)]
        public MISMOString NotaryCertificateSignerTitleDescription { get; set; }
    
        [XmlIgnore]
        public bool NotaryCertificateSignerTitleDescriptionSpecified
        {
            get { return this.NotaryCertificateSignerTitleDescription != null; }
            set { }
        }
    
        [XmlElement("NotaryCertificateSigningCountyName", Order = 6)]
        public MISMOString NotaryCertificateSigningCountyName { get; set; }
    
        [XmlIgnore]
        public bool NotaryCertificateSigningCountyNameSpecified
        {
            get { return this.NotaryCertificateSigningCountyName != null; }
            set { }
        }
    
        [XmlElement("NotaryCertificateSigningStateName", Order = 7)]
        public MISMOString NotaryCertificateSigningStateName { get; set; }
    
        [XmlIgnore]
        public bool NotaryCertificateSigningStateNameSpecified
        {
            get { return this.NotaryCertificateSigningStateName != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 8)]
        public NOTARY_CERTIFICATE_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
