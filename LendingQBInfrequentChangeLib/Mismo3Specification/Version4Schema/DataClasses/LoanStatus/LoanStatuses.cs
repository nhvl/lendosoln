namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class LOAN_STATUSES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LoanStatusListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LOAN_STATUS", Order = 0)]
        public List<LOAN_STATUS> LoanStatusList { get; set; } = new List<LOAN_STATUS>();
    
        [XmlIgnore]
        public bool LoanStatusListSpecified
        {
            get { return this.LoanStatusList != null && this.LoanStatusList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public LOAN_STATUSES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
