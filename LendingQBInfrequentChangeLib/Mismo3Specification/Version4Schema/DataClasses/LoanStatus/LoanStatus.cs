namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class LOAN_STATUS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LoanStatusDateSpecified
                    || this.LoanStatusIdentifierSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LoanStatusDate", Order = 0)]
        public MISMODate LoanStatusDate { get; set; }
    
        [XmlIgnore]
        public bool LoanStatusDateSpecified
        {
            get { return this.LoanStatusDate != null; }
            set { }
        }
    
        [XmlElement("LoanStatusIdentifier", Order = 1)]
        public MISMOIdentifier LoanStatusIdentifier { get; set; }
    
        [XmlIgnore]
        public bool LoanStatusIdentifierSpecified
        {
            get { return this.LoanStatusIdentifier != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public LOAN_STATUS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
