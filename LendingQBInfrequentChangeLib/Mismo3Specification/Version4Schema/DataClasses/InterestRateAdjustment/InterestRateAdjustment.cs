namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class INTEREST_RATE_ADJUSTMENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.IndexRulesSpecified
                    || this.InterestRateLifetimeAdjustmentRuleSpecified
                    || this.InterestRatePerChangeAdjustmentRulesSpecified
                    || this.InterestRatePeriodicAdjustmentRulesSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("INDEX_RULES", Order = 0)]
        public INDEX_RULES IndexRules { get; set; }
    
        [XmlIgnore]
        public bool IndexRulesSpecified
        {
            get { return this.IndexRules != null && this.IndexRules.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("INTEREST_RATE_LIFETIME_ADJUSTMENT_RULE", Order = 1)]
        public INTEREST_RATE_LIFETIME_ADJUSTMENT_RULE InterestRateLifetimeAdjustmentRule { get; set; }
    
        [XmlIgnore]
        public bool InterestRateLifetimeAdjustmentRuleSpecified
        {
            get { return this.InterestRateLifetimeAdjustmentRule != null && this.InterestRateLifetimeAdjustmentRule.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULES", Order = 2)]
        public INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULES InterestRatePerChangeAdjustmentRules { get; set; }
    
        [XmlIgnore]
        public bool InterestRatePerChangeAdjustmentRulesSpecified
        {
            get { return this.InterestRatePerChangeAdjustmentRules != null && this.InterestRatePerChangeAdjustmentRules.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("INTEREST_RATE_PERIODIC_ADJUSTMENT_RULES", Order = 3)]
        public INTEREST_RATE_PERIODIC_ADJUSTMENT_RULES InterestRatePeriodicAdjustmentRules { get; set; }
    
        [XmlIgnore]
        public bool InterestRatePeriodicAdjustmentRulesSpecified
        {
            get { return this.InterestRatePeriodicAdjustmentRules != null && this.InterestRatePeriodicAdjustmentRules.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public INTEREST_RATE_ADJUSTMENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
