namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class GREEN_BUILDING_CERTIFICATIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.GreenBuildingCertificationListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("GREEN_BUILDING_CERTIFICATION", Order = 0)]
        public List<GREEN_BUILDING_CERTIFICATION> GreenBuildingCertificationList { get; set; } = new List<GREEN_BUILDING_CERTIFICATION>();
    
        [XmlIgnore]
        public bool GreenBuildingCertificationListSpecified
        {
            get { return this.GreenBuildingCertificationList != null && this.GreenBuildingCertificationList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public GREEN_BUILDING_CERTIFICATIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
