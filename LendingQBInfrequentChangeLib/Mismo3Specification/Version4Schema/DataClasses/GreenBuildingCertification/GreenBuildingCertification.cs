namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class GREEN_BUILDING_CERTIFICATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.GreenCertificationAssociationNameSpecified
                    || this.GreenCertificationLevelAwardedDateSpecified
                    || this.GreenCertificationLevelNameSpecified
                    || this.GreenCertificationLevelScoreValueSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("GreenCertificationAssociationName", Order = 0)]
        public MISMOString GreenCertificationAssociationName { get; set; }
    
        [XmlIgnore]
        public bool GreenCertificationAssociationNameSpecified
        {
            get { return this.GreenCertificationAssociationName != null; }
            set { }
        }
    
        [XmlElement("GreenCertificationLevelAwardedDate", Order = 1)]
        public MISMODate GreenCertificationLevelAwardedDate { get; set; }
    
        [XmlIgnore]
        public bool GreenCertificationLevelAwardedDateSpecified
        {
            get { return this.GreenCertificationLevelAwardedDate != null; }
            set { }
        }
    
        [XmlElement("GreenCertificationLevelName", Order = 2)]
        public MISMOString GreenCertificationLevelName { get; set; }
    
        [XmlIgnore]
        public bool GreenCertificationLevelNameSpecified
        {
            get { return this.GreenCertificationLevelName != null; }
            set { }
        }
    
        [XmlElement("GreenCertificationLevelScoreValue", Order = 3)]
        public MISMOValue GreenCertificationLevelScoreValue { get; set; }
    
        [XmlIgnore]
        public bool GreenCertificationLevelScoreValueSpecified
        {
            get { return this.GreenCertificationLevelScoreValue != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public GREEN_BUILDING_CERTIFICATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
