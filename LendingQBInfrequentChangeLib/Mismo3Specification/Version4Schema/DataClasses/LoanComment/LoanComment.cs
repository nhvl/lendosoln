namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class LOAN_COMMENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LoanCommentDatetimeSpecified
                    || this.LoanCommentSourceDescriptionSpecified
                    || this.LoanCommentTextSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LoanCommentDatetime", Order = 0)]
        public MISMODatetime LoanCommentDatetime { get; set; }
    
        [XmlIgnore]
        public bool LoanCommentDatetimeSpecified
        {
            get { return this.LoanCommentDatetime != null; }
            set { }
        }
    
        [XmlElement("LoanCommentSourceDescription", Order = 1)]
        public MISMOString LoanCommentSourceDescription { get; set; }
    
        [XmlIgnore]
        public bool LoanCommentSourceDescriptionSpecified
        {
            get { return this.LoanCommentSourceDescription != null; }
            set { }
        }
    
        [XmlElement("LoanCommentText", Order = 2)]
        public MISMOString LoanCommentText { get; set; }
    
        [XmlIgnore]
        public bool LoanCommentTextSpecified
        {
            get { return this.LoanCommentText != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public LOAN_COMMENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
