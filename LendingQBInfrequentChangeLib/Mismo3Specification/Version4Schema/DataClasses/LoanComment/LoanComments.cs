namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class LOAN_COMMENTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LoanCommentListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LOAN_COMMENT", Order = 0)]
        public List<LOAN_COMMENT> LoanCommentList { get; set; } = new List<LOAN_COMMENT>();
    
        [XmlIgnore]
        public bool LoanCommentListSpecified
        {
            get { return this.LoanCommentList != null && this.LoanCommentList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public LOAN_COMMENTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
