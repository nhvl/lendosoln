namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CREDIT_RESPONSE_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditRatingCodeTypeSpecified
                    || this.CreditRatingCodeTypeOtherDescriptionSpecified
                    || this.CreditReportFirstIssuedDateSpecified
                    || this.CreditReportIdentifierSpecified
                    || this.CreditReportLastUpdatedDateSpecified
                    || this.CreditReportMergeTypeSpecified
                    || this.CreditReportTransactionIdentifierSpecified
                    || this.CreditReportTypeSpecified
                    || this.CreditReportTypeOtherDescriptionSpecified
                    || this.PaymentPatternRatingCodeTypeSpecified
                    || this.PaymentPatternRatingCodeTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CreditRatingCodeType", Order = 0)]
        public MISMOEnum<CreditRatingCodeBase> CreditRatingCodeType { get; set; }
    
        [XmlIgnore]
        public bool CreditRatingCodeTypeSpecified
        {
            get { return this.CreditRatingCodeType != null; }
            set { }
        }
    
        [XmlElement("CreditRatingCodeTypeOtherDescription", Order = 1)]
        public MISMOString CreditRatingCodeTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CreditRatingCodeTypeOtherDescriptionSpecified
        {
            get { return this.CreditRatingCodeTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("CreditReportFirstIssuedDate", Order = 2)]
        public MISMODate CreditReportFirstIssuedDate { get; set; }
    
        [XmlIgnore]
        public bool CreditReportFirstIssuedDateSpecified
        {
            get { return this.CreditReportFirstIssuedDate != null; }
            set { }
        }
    
        [XmlElement("CreditReportIdentifier", Order = 3)]
        public MISMOIdentifier CreditReportIdentifier { get; set; }
    
        [XmlIgnore]
        public bool CreditReportIdentifierSpecified
        {
            get { return this.CreditReportIdentifier != null; }
            set { }
        }
    
        [XmlElement("CreditReportLastUpdatedDate", Order = 4)]
        public MISMODate CreditReportLastUpdatedDate { get; set; }
    
        [XmlIgnore]
        public bool CreditReportLastUpdatedDateSpecified
        {
            get { return this.CreditReportLastUpdatedDate != null; }
            set { }
        }
    
        [XmlElement("CreditReportMergeType", Order = 5)]
        public MISMOEnum<CreditReportMergeBase> CreditReportMergeType { get; set; }
    
        [XmlIgnore]
        public bool CreditReportMergeTypeSpecified
        {
            get { return this.CreditReportMergeType != null; }
            set { }
        }
    
        [XmlElement("CreditReportTransactionIdentifier", Order = 6)]
        public MISMOIdentifier CreditReportTransactionIdentifier { get; set; }
    
        [XmlIgnore]
        public bool CreditReportTransactionIdentifierSpecified
        {
            get { return this.CreditReportTransactionIdentifier != null; }
            set { }
        }
    
        [XmlElement("CreditReportType", Order = 7)]
        public MISMOEnum<CreditReportBase> CreditReportType { get; set; }
    
        [XmlIgnore]
        public bool CreditReportTypeSpecified
        {
            get { return this.CreditReportType != null; }
            set { }
        }
    
        [XmlElement("CreditReportTypeOtherDescription", Order = 8)]
        public MISMOString CreditReportTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CreditReportTypeOtherDescriptionSpecified
        {
            get { return this.CreditReportTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PaymentPatternRatingCodeType", Order = 9)]
        public MISMOEnum<PaymentPatternRatingCodeBase> PaymentPatternRatingCodeType { get; set; }
    
        [XmlIgnore]
        public bool PaymentPatternRatingCodeTypeSpecified
        {
            get { return this.PaymentPatternRatingCodeType != null; }
            set { }
        }
    
        [XmlElement("PaymentPatternRatingCodeTypeOtherDescription", Order = 10)]
        public MISMOString PaymentPatternRatingCodeTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PaymentPatternRatingCodeTypeOtherDescriptionSpecified
        {
            get { return this.PaymentPatternRatingCodeTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 11)]
        public CREDIT_RESPONSE_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
