namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CREDIT_RESPONSE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditBureauSpecified
                    || this.CreditCommentsSpecified
                    || this.CreditConsumerReferralsSpecified
                    || this.CreditErrorMessagesSpecified
                    || this.CreditFilesSpecified
                    || this.CreditInquiriesSpecified
                    || this.CreditLiabilitiesSpecified
                    || this.CreditPublicRecordsSpecified
                    || this.CreditReportPricesSpecified
                    || this.CreditRepositoryIncludedSpecified
                    || this.CreditRequestDataSpecified
                    || this.CreditResponseAlertMessagesSpecified
                    || this.CreditResponseDataInformationSpecified
                    || this.CreditResponseDetailSpecified
                    || this.CreditScoreModelsSpecified
                    || this.CreditScoresSpecified
                    || this.CreditSummariesSpecified
                    || this.CreditTradeReferencesSpecified
                    || this.LoansSpecified
                    || this.PartiesSpecified
                    || this.RegulatoryProductsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CREDIT_BUREAU", Order = 0)]
        public CREDIT_BUREAU CreditBureau { get; set; }
    
        [XmlIgnore]
        public bool CreditBureauSpecified
        {
            get { return this.CreditBureau != null && this.CreditBureau.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_COMMENTS", Order = 1)]
        public CREDIT_COMMENTS CreditComments { get; set; }
    
        [XmlIgnore]
        public bool CreditCommentsSpecified
        {
            get { return this.CreditComments != null && this.CreditComments.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_CONSUMER_REFERRALS", Order = 2)]
        public CREDIT_CONSUMER_REFERRALS CreditConsumerReferrals { get; set; }
    
        [XmlIgnore]
        public bool CreditConsumerReferralsSpecified
        {
            get { return this.CreditConsumerReferrals != null && this.CreditConsumerReferrals.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_ERROR_MESSAGES", Order = 3)]
        public CREDIT_ERROR_MESSAGES CreditErrorMessages { get; set; }
    
        [XmlIgnore]
        public bool CreditErrorMessagesSpecified
        {
            get { return this.CreditErrorMessages != null && this.CreditErrorMessages.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_FILES", Order = 4)]
        public CREDIT_FILES CreditFiles { get; set; }
    
        [XmlIgnore]
        public bool CreditFilesSpecified
        {
            get { return this.CreditFiles != null && this.CreditFiles.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_INQUIRIES", Order = 5)]
        public CREDIT_INQUIRIES CreditInquiries { get; set; }
    
        [XmlIgnore]
        public bool CreditInquiriesSpecified
        {
            get { return this.CreditInquiries != null && this.CreditInquiries.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_LIABILITIES", Order = 6)]
        public CREDIT_LIABILITIES CreditLiabilities { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilitiesSpecified
        {
            get { return this.CreditLiabilities != null && this.CreditLiabilities.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_PUBLIC_RECORDS", Order = 7)]
        public CREDIT_PUBLIC_RECORDS CreditPublicRecords { get; set; }
    
        [XmlIgnore]
        public bool CreditPublicRecordsSpecified
        {
            get { return this.CreditPublicRecords != null && this.CreditPublicRecords.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_REPORT_PRICES", Order = 8)]
        public CREDIT_REPORT_PRICES CreditReportPrices { get; set; }
    
        [XmlIgnore]
        public bool CreditReportPricesSpecified
        {
            get { return this.CreditReportPrices != null && this.CreditReportPrices.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_REPOSITORY_INCLUDED", Order = 9)]
        public CREDIT_REPOSITORY_INCLUDED CreditRepositoryIncluded { get; set; }
    
        [XmlIgnore]
        public bool CreditRepositoryIncludedSpecified
        {
            get { return this.CreditRepositoryIncluded != null && this.CreditRepositoryIncluded.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_REQUEST_DATA", Order = 10)]
        public CREDIT_REQUEST_DATA CreditRequestData { get; set; }
    
        [XmlIgnore]
        public bool CreditRequestDataSpecified
        {
            get { return this.CreditRequestData != null && this.CreditRequestData.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_RESPONSE_ALERT_MESSAGES", Order = 11)]
        public CREDIT_RESPONSE_ALERT_MESSAGES CreditResponseAlertMessages { get; set; }
    
        [XmlIgnore]
        public bool CreditResponseAlertMessagesSpecified
        {
            get { return this.CreditResponseAlertMessages != null && this.CreditResponseAlertMessages.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_RESPONSE_DATA_INFORMATION", Order = 12)]
        public CREDIT_RESPONSE_DATA_INFORMATION CreditResponseDataInformation { get; set; }
    
        [XmlIgnore]
        public bool CreditResponseDataInformationSpecified
        {
            get { return this.CreditResponseDataInformation != null && this.CreditResponseDataInformation.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_RESPONSE_DETAIL", Order = 13)]
        public CREDIT_RESPONSE_DETAIL CreditResponseDetail { get; set; }
    
        [XmlIgnore]
        public bool CreditResponseDetailSpecified
        {
            get { return this.CreditResponseDetail != null && this.CreditResponseDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_SCORE_MODELS", Order = 14)]
        public CREDIT_SCORE_MODELS CreditScoreModels { get; set; }
    
        [XmlIgnore]
        public bool CreditScoreModelsSpecified
        {
            get { return this.CreditScoreModels != null && this.CreditScoreModels.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_SCORES", Order = 15)]
        public CREDIT_SCORES CreditScores { get; set; }
    
        [XmlIgnore]
        public bool CreditScoresSpecified
        {
            get { return this.CreditScores != null && this.CreditScores.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_SUMMARIES", Order = 16)]
        public CREDIT_SUMMARIES CreditSummaries { get; set; }
    
        [XmlIgnore]
        public bool CreditSummariesSpecified
        {
            get { return this.CreditSummaries != null && this.CreditSummaries.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_TRADE_REFERENCES", Order = 17)]
        public CREDIT_TRADE_REFERENCES CreditTradeReferences { get; set; }
    
        [XmlIgnore]
        public bool CreditTradeReferencesSpecified
        {
            get { return this.CreditTradeReferences != null && this.CreditTradeReferences.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("LOANS", Order = 18)]
        public LOANS Loans { get; set; }
    
        [XmlIgnore]
        public bool LoansSpecified
        {
            get { return this.Loans != null && this.Loans.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PARTIES", Order = 19)]
        public PARTIES Parties { get; set; }
    
        [XmlIgnore]
        public bool PartiesSpecified
        {
            get { return this.Parties != null && this.Parties.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("REGULATORY_PRODUCTS", Order = 20)]
        public REGULATORY_PRODUCTS RegulatoryProducts { get; set; }
    
        [XmlIgnore]
        public bool RegulatoryProductsSpecified
        {
            get { return this.RegulatoryProducts != null && this.RegulatoryProducts.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 21)]
        public CREDIT_RESPONSE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
