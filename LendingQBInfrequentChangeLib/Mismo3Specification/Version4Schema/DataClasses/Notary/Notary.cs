namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class NOTARY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.NotaryCommissionBondNumberIdentifierSpecified
                    || this.NotaryCommissionCityNameSpecified
                    || this.NotaryCommissionCountyNameSpecified
                    || this.NotaryCommissionExpirationDateSpecified
                    || this.NotaryCommissionNumberIdentifierSpecified
                    || this.NotaryCommissionStateNameSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("NotaryCommissionBondNumberIdentifier", Order = 0)]
        public MISMOIdentifier NotaryCommissionBondNumberIdentifier { get; set; }
    
        [XmlIgnore]
        public bool NotaryCommissionBondNumberIdentifierSpecified
        {
            get { return this.NotaryCommissionBondNumberIdentifier != null; }
            set { }
        }
    
        [XmlElement("NotaryCommissionCityName", Order = 1)]
        public MISMOString NotaryCommissionCityName { get; set; }
    
        [XmlIgnore]
        public bool NotaryCommissionCityNameSpecified
        {
            get { return this.NotaryCommissionCityName != null; }
            set { }
        }
    
        [XmlElement("NotaryCommissionCountyName", Order = 2)]
        public MISMOString NotaryCommissionCountyName { get; set; }
    
        [XmlIgnore]
        public bool NotaryCommissionCountyNameSpecified
        {
            get { return this.NotaryCommissionCountyName != null; }
            set { }
        }
    
        [XmlElement("NotaryCommissionExpirationDate", Order = 3)]
        public MISMODate NotaryCommissionExpirationDate { get; set; }
    
        [XmlIgnore]
        public bool NotaryCommissionExpirationDateSpecified
        {
            get { return this.NotaryCommissionExpirationDate != null; }
            set { }
        }
    
        [XmlElement("NotaryCommissionNumberIdentifier", Order = 4)]
        public MISMOIdentifier NotaryCommissionNumberIdentifier { get; set; }
    
        [XmlIgnore]
        public bool NotaryCommissionNumberIdentifierSpecified
        {
            get { return this.NotaryCommissionNumberIdentifier != null; }
            set { }
        }
    
        [XmlElement("NotaryCommissionStateName", Order = 5)]
        public MISMOString NotaryCommissionStateName { get; set; }
    
        [XmlIgnore]
        public bool NotaryCommissionStateNameSpecified
        {
            get { return this.NotaryCommissionStateName != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 6)]
        public NOTARY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
