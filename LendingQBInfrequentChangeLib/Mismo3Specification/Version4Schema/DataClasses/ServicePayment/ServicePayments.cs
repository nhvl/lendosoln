namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class SERVICE_PAYMENTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ServicePaymentListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SERVICE_PAYMENT", Order = 0)]
        public List<SERVICE_PAYMENT> ServicePaymentList { get; set; } = new List<SERVICE_PAYMENT>();
    
        [XmlIgnore]
        public bool ServicePaymentListSpecified
        {
            get { return this.ServicePaymentList != null && this.ServicePaymentList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public SERVICE_PAYMENTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
