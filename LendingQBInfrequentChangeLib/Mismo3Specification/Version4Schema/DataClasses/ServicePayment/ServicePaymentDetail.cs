namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SERVICE_PAYMENT_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.InternalAccountIdentifierSpecified
                    || this.ServicePaymentAccountIdentifierSpecified
                    || this.ServicePaymentAmountSpecified
                    || this.ServicePaymentCreditAccountExpirationDateSpecified
                    || this.ServicePaymentCreditMethodTypeSpecified
                    || this.ServicePaymentCreditMethodTypeOtherDescriptionSpecified
                    || this.ServicePaymentMethodTypeSpecified
                    || this.ServicePaymentMethodTypeOtherDescriptionSpecified
                    || this.ServicePaymentOnAccountIdentifierSpecified
                    || this.ServicePaymentOnAccountMaximumDebitAmountSpecified
                    || this.ServicePaymentOnAccountMinimumBalanceAmountSpecified
                    || this.ServicePaymentReferenceIdentifierSpecified
                    || this.ServicePaymentSecondaryCreditAccountIdentifierSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("InternalAccountIdentifier", Order = 0)]
        public MISMOIdentifier InternalAccountIdentifier { get; set; }
    
        [XmlIgnore]
        public bool InternalAccountIdentifierSpecified
        {
            get { return this.InternalAccountIdentifier != null; }
            set { }
        }
    
        [XmlElement("ServicePaymentAccountIdentifier", Order = 1)]
        public MISMOIdentifier ServicePaymentAccountIdentifier { get; set; }
    
        [XmlIgnore]
        public bool ServicePaymentAccountIdentifierSpecified
        {
            get { return this.ServicePaymentAccountIdentifier != null; }
            set { }
        }
    
        [XmlElement("ServicePaymentAmount", Order = 2)]
        public MISMOAmount ServicePaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool ServicePaymentAmountSpecified
        {
            get { return this.ServicePaymentAmount != null; }
            set { }
        }
    
        [XmlElement("ServicePaymentCreditAccountExpirationDate", Order = 3)]
        public MISMODate ServicePaymentCreditAccountExpirationDate { get; set; }
    
        [XmlIgnore]
        public bool ServicePaymentCreditAccountExpirationDateSpecified
        {
            get { return this.ServicePaymentCreditAccountExpirationDate != null; }
            set { }
        }
    
        [XmlElement("ServicePaymentCreditMethodType", Order = 4)]
        public MISMOEnum<ServicePaymentCreditMethodBase> ServicePaymentCreditMethodType { get; set; }
    
        [XmlIgnore]
        public bool ServicePaymentCreditMethodTypeSpecified
        {
            get { return this.ServicePaymentCreditMethodType != null; }
            set { }
        }
    
        [XmlElement("ServicePaymentCreditMethodTypeOtherDescription", Order = 5)]
        public MISMOString ServicePaymentCreditMethodTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ServicePaymentCreditMethodTypeOtherDescriptionSpecified
        {
            get { return this.ServicePaymentCreditMethodTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ServicePaymentMethodType", Order = 6)]
        public MISMOEnum<ServicePaymentMethodBase> ServicePaymentMethodType { get; set; }
    
        [XmlIgnore]
        public bool ServicePaymentMethodTypeSpecified
        {
            get { return this.ServicePaymentMethodType != null; }
            set { }
        }
    
        [XmlElement("ServicePaymentMethodTypeOtherDescription", Order = 7)]
        public MISMOString ServicePaymentMethodTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ServicePaymentMethodTypeOtherDescriptionSpecified
        {
            get { return this.ServicePaymentMethodTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ServicePaymentOnAccountIdentifier", Order = 8)]
        public MISMOIdentifier ServicePaymentOnAccountIdentifier { get; set; }
    
        [XmlIgnore]
        public bool ServicePaymentOnAccountIdentifierSpecified
        {
            get { return this.ServicePaymentOnAccountIdentifier != null; }
            set { }
        }
    
        [XmlElement("ServicePaymentOnAccountMaximumDebitAmount", Order = 9)]
        public MISMOAmount ServicePaymentOnAccountMaximumDebitAmount { get; set; }
    
        [XmlIgnore]
        public bool ServicePaymentOnAccountMaximumDebitAmountSpecified
        {
            get { return this.ServicePaymentOnAccountMaximumDebitAmount != null; }
            set { }
        }
    
        [XmlElement("ServicePaymentOnAccountMinimumBalanceAmount", Order = 10)]
        public MISMOAmount ServicePaymentOnAccountMinimumBalanceAmount { get; set; }
    
        [XmlIgnore]
        public bool ServicePaymentOnAccountMinimumBalanceAmountSpecified
        {
            get { return this.ServicePaymentOnAccountMinimumBalanceAmount != null; }
            set { }
        }
    
        [XmlElement("ServicePaymentReferenceIdentifier", Order = 11)]
        public MISMOIdentifier ServicePaymentReferenceIdentifier { get; set; }
    
        [XmlIgnore]
        public bool ServicePaymentReferenceIdentifierSpecified
        {
            get { return this.ServicePaymentReferenceIdentifier != null; }
            set { }
        }
    
        [XmlElement("ServicePaymentSecondaryCreditAccountIdentifier", Order = 12)]
        public MISMOIdentifier ServicePaymentSecondaryCreditAccountIdentifier { get; set; }
    
        [XmlIgnore]
        public bool ServicePaymentSecondaryCreditAccountIdentifierSpecified
        {
            get { return this.ServicePaymentSecondaryCreditAccountIdentifier != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 13)]
        public SERVICE_PAYMENT_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
