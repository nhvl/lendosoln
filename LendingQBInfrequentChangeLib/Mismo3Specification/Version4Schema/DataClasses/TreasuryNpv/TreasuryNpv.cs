namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class TREASURY_NPV
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LoanRiskPremiumPercentSpecified
                    || this.PayForPerformanceQualificationIndicatorSpecified
                    || this.TreasuryNPVAssociationFeesPreWorkoutAmountSpecified
                    || this.TreasuryNPVBaseVersionIdentifierSpecified
                    || this.TreasuryNPVCurrentEstimatedDefaultRatePercentSpecified
                    || this.TreasuryNPVDateSpecified
                    || this.TreasuryNPVLoanAmortizationTypeSpecified
                    || this.TreasuryNPVLoanAmortizationTypeOtherDescriptionSpecified
                    || this.TreasuryNPVMonthlyHazardAndFloodInsuranceAmountSpecified
                    || this.TreasuryNPVMonthsPastDueCountSpecified
                    || this.TreasuryNPVPositiveTestResultIndicatorSpecified
                    || this.TreasuryNPVPostWorkoutEstimatedDefaultRatePercentSpecified
                    || this.TreasuryNPVResultPostModificationAmountSpecified
                    || this.TreasuryNPVResultPreModificationAmountSpecified
                    || this.TreasuryNPVRunDateSpecified
                    || this.TreasuryNPVSuccessfulRunIndicatorSpecified
                    || this.TreasuryNPVSystemExceptionIndicatorSpecified
                    || this.TreasuryNPVWaterfallTestIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LoanRiskPremiumPercent", Order = 0)]
        public MISMOPercent LoanRiskPremiumPercent { get; set; }
    
        [XmlIgnore]
        public bool LoanRiskPremiumPercentSpecified
        {
            get { return this.LoanRiskPremiumPercent != null; }
            set { }
        }
    
        [XmlElement("PayForPerformanceQualificationIndicator", Order = 1)]
        public MISMOIndicator PayForPerformanceQualificationIndicator { get; set; }
    
        [XmlIgnore]
        public bool PayForPerformanceQualificationIndicatorSpecified
        {
            get { return this.PayForPerformanceQualificationIndicator != null; }
            set { }
        }
    
        [XmlElement("TreasuryNPVAssociationFeesPreWorkoutAmount", Order = 2)]
        public MISMOAmount TreasuryNPVAssociationFeesPreWorkoutAmount { get; set; }
    
        [XmlIgnore]
        public bool TreasuryNPVAssociationFeesPreWorkoutAmountSpecified
        {
            get { return this.TreasuryNPVAssociationFeesPreWorkoutAmount != null; }
            set { }
        }
    
        [XmlElement("TreasuryNPVBaseVersionIdentifier", Order = 3)]
        public MISMOIdentifier TreasuryNPVBaseVersionIdentifier { get; set; }
    
        [XmlIgnore]
        public bool TreasuryNPVBaseVersionIdentifierSpecified
        {
            get { return this.TreasuryNPVBaseVersionIdentifier != null; }
            set { }
        }
    
        [XmlElement("TreasuryNPVCurrentEstimatedDefaultRatePercent", Order = 4)]
        public MISMOPercent TreasuryNPVCurrentEstimatedDefaultRatePercent { get; set; }
    
        [XmlIgnore]
        public bool TreasuryNPVCurrentEstimatedDefaultRatePercentSpecified
        {
            get { return this.TreasuryNPVCurrentEstimatedDefaultRatePercent != null; }
            set { }
        }
    
        [XmlElement("TreasuryNPVDate", Order = 5)]
        public MISMODate TreasuryNPVDate { get; set; }
    
        [XmlIgnore]
        public bool TreasuryNPVDateSpecified
        {
            get { return this.TreasuryNPVDate != null; }
            set { }
        }
    
        [XmlElement("TreasuryNPVLoanAmortizationType", Order = 6)]
        public MISMOEnum<TreasuryNPVLoanAmortizationBase> TreasuryNPVLoanAmortizationType { get; set; }
    
        [XmlIgnore]
        public bool TreasuryNPVLoanAmortizationTypeSpecified
        {
            get { return this.TreasuryNPVLoanAmortizationType != null; }
            set { }
        }
    
        [XmlElement("TreasuryNPVLoanAmortizationTypeOtherDescription", Order = 7)]
        public MISMOString TreasuryNPVLoanAmortizationTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool TreasuryNPVLoanAmortizationTypeOtherDescriptionSpecified
        {
            get { return this.TreasuryNPVLoanAmortizationTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("TreasuryNPVMonthlyHazardAndFloodInsuranceAmount", Order = 8)]
        public MISMOAmount TreasuryNPVMonthlyHazardAndFloodInsuranceAmount { get; set; }
    
        [XmlIgnore]
        public bool TreasuryNPVMonthlyHazardAndFloodInsuranceAmountSpecified
        {
            get { return this.TreasuryNPVMonthlyHazardAndFloodInsuranceAmount != null; }
            set { }
        }
    
        [XmlElement("TreasuryNPVMonthsPastDueCount", Order = 9)]
        public MISMOCount TreasuryNPVMonthsPastDueCount { get; set; }
    
        [XmlIgnore]
        public bool TreasuryNPVMonthsPastDueCountSpecified
        {
            get { return this.TreasuryNPVMonthsPastDueCount != null; }
            set { }
        }
    
        [XmlElement("TreasuryNPVPositiveTestResultIndicator", Order = 10)]
        public MISMOIndicator TreasuryNPVPositiveTestResultIndicator { get; set; }
    
        [XmlIgnore]
        public bool TreasuryNPVPositiveTestResultIndicatorSpecified
        {
            get { return this.TreasuryNPVPositiveTestResultIndicator != null; }
            set { }
        }
    
        [XmlElement("TreasuryNPVPostWorkoutEstimatedDefaultRatePercent", Order = 11)]
        public MISMOPercent TreasuryNPVPostWorkoutEstimatedDefaultRatePercent { get; set; }
    
        [XmlIgnore]
        public bool TreasuryNPVPostWorkoutEstimatedDefaultRatePercentSpecified
        {
            get { return this.TreasuryNPVPostWorkoutEstimatedDefaultRatePercent != null; }
            set { }
        }
    
        [XmlElement("TreasuryNPVResultPostModificationAmount", Order = 12)]
        public MISMOAmount TreasuryNPVResultPostModificationAmount { get; set; }
    
        [XmlIgnore]
        public bool TreasuryNPVResultPostModificationAmountSpecified
        {
            get { return this.TreasuryNPVResultPostModificationAmount != null; }
            set { }
        }
    
        [XmlElement("TreasuryNPVResultPreModificationAmount", Order = 13)]
        public MISMOAmount TreasuryNPVResultPreModificationAmount { get; set; }
    
        [XmlIgnore]
        public bool TreasuryNPVResultPreModificationAmountSpecified
        {
            get { return this.TreasuryNPVResultPreModificationAmount != null; }
            set { }
        }
    
        [XmlElement("TreasuryNPVRunDate", Order = 14)]
        public MISMODate TreasuryNPVRunDate { get; set; }
    
        [XmlIgnore]
        public bool TreasuryNPVRunDateSpecified
        {
            get { return this.TreasuryNPVRunDate != null; }
            set { }
        }
    
        [XmlElement("TreasuryNPVSuccessfulRunIndicator", Order = 15)]
        public MISMOIndicator TreasuryNPVSuccessfulRunIndicator { get; set; }
    
        [XmlIgnore]
        public bool TreasuryNPVSuccessfulRunIndicatorSpecified
        {
            get { return this.TreasuryNPVSuccessfulRunIndicator != null; }
            set { }
        }
    
        [XmlElement("TreasuryNPVSystemExceptionIndicator", Order = 16)]
        public MISMOIndicator TreasuryNPVSystemExceptionIndicator { get; set; }
    
        [XmlIgnore]
        public bool TreasuryNPVSystemExceptionIndicatorSpecified
        {
            get { return this.TreasuryNPVSystemExceptionIndicator != null; }
            set { }
        }
    
        [XmlElement("TreasuryNPVWaterfallTestIndicator", Order = 17)]
        public MISMOIndicator TreasuryNPVWaterfallTestIndicator { get; set; }
    
        [XmlIgnore]
        public bool TreasuryNPVWaterfallTestIndicatorSpecified
        {
            get { return this.TreasuryNPVWaterfallTestIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 18)]
        public TREASURY_NPV_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
