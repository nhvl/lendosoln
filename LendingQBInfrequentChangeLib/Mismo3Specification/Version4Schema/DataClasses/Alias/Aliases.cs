namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class ALIASES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AliasListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ALIAS", Order = 0)]
        public List<ALIAS> AliasList { get; set; } = new List<ALIAS>();
    
        [XmlIgnore]
        public bool AliasListSpecified
        {
            get { return this.AliasList != null && this.AliasList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public ALIASES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
