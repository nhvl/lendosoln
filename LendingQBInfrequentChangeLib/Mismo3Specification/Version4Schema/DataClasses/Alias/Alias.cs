namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ALIAS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AliasDetailSpecified
                    || this.NameSpecified
                    || this.TaxpayerIdentifiersSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ALIAS_DETAIL", Order = 0)]
        public ALIAS_DETAIL AliasDetail { get; set; }
    
        [XmlIgnore]
        public bool AliasDetailSpecified
        {
            get { return this.AliasDetail != null && this.AliasDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("NAME", Order = 1)]
        public NAME Name { get; set; }
    
        [XmlIgnore]
        public bool NameSpecified
        {
            get { return this.Name != null && this.Name.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("TAXPAYER_IDENTIFIERS", Order = 2)]
        public TAXPAYER_IDENTIFIERS TaxpayerIdentifiers { get; set; }
    
        [XmlIgnore]
        public bool TaxpayerIdentifiersSpecified
        {
            get { return this.TaxpayerIdentifiers != null && this.TaxpayerIdentifiers.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public ALIAS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
