namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ALIAS_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AliasAccountIdentifierSpecified
                    || this.AliasCreditorNameSpecified
                    || this.AliasTaxReturnIndicatorSpecified
                    || this.AliasTypeSpecified
                    || this.AliasTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AliasAccountIdentifier", Order = 0)]
        public MISMOIdentifier AliasAccountIdentifier { get; set; }
    
        [XmlIgnore]
        public bool AliasAccountIdentifierSpecified
        {
            get { return this.AliasAccountIdentifier != null; }
            set { }
        }
    
        [XmlElement("AliasCreditorName", Order = 1)]
        public MISMOString AliasCreditorName { get; set; }
    
        [XmlIgnore]
        public bool AliasCreditorNameSpecified
        {
            get { return this.AliasCreditorName != null; }
            set { }
        }
    
        [XmlElement("AliasTaxReturnIndicator", Order = 2)]
        public MISMOIndicator AliasTaxReturnIndicator { get; set; }
    
        [XmlIgnore]
        public bool AliasTaxReturnIndicatorSpecified
        {
            get { return this.AliasTaxReturnIndicator != null; }
            set { }
        }
    
        [XmlElement("AliasType", Order = 3)]
        public MISMOEnum<AliasBase> AliasType { get; set; }
    
        [XmlIgnore]
        public bool AliasTypeSpecified
        {
            get { return this.AliasType != null; }
            set { }
        }
    
        [XmlElement("AliasTypeOtherDescription", Order = 4)]
        public MISMOString AliasTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool AliasTypeOtherDescriptionSpecified
        {
            get { return this.AliasTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public ALIAS_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
