namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class NOTE_RIDER
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.NoteRiderTypeSpecified
                    || this.NoteRiderTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("NoteRiderType", Order = 0)]
        public MISMOEnum<NoteRiderBase> NoteRiderType { get; set; }
    
        [XmlIgnore]
        public bool NoteRiderTypeSpecified
        {
            get { return this.NoteRiderType != null; }
            set { }
        }
    
        [XmlElement("NoteRiderTypeOtherDescription", Order = 1)]
        public MISMOString NoteRiderTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool NoteRiderTypeOtherDescriptionSpecified
        {
            get { return this.NoteRiderTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public NOTE_RIDER_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
