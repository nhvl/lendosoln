namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class NOTE_RIDERS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.NoteRiderListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("NOTE_RIDER", Order = 0)]
        public List<NOTE_RIDER> NoteRiderList { get; set; } = new List<NOTE_RIDER>();
    
        [XmlIgnore]
        public bool NoteRiderListSpecified
        {
            get { return this.NoteRiderList != null && this.NoteRiderList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public NOTE_RIDERS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
