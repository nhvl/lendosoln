namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ASSIGNMENT_PARAMETER
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AssignmentConditionTypeSpecified
                    || this.AssignmentConditionTypeOtherDescriptionSpecified
                    || this.AssignmentDescriptionSpecified
                    || this.AssignmentParameterPriorityTypeSpecified
                    || this.AssignmentParameterSourceTypeSpecified
                    || this.AssignmentParameterSourceTypeOtherDescriptionSpecified
                    || this.AssignmentParameterTypeSpecified
                    || this.AssignmentParameterTypeOtherDescriptionSpecified
                    || this.VendorSpecialtyTypeSpecified
                    || this.VendorSpecialtyTypeOtherDescriptionSpecified
                    || this.VendorWatchListIndicationDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AssignmentConditionType", Order = 0)]
        public MISMOEnum<AssignmentConditionBase> AssignmentConditionType { get; set; }
    
        [XmlIgnore]
        public bool AssignmentConditionTypeSpecified
        {
            get { return this.AssignmentConditionType != null; }
            set { }
        }
    
        [XmlElement("AssignmentConditionTypeOtherDescription", Order = 1)]
        public MISMOString AssignmentConditionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool AssignmentConditionTypeOtherDescriptionSpecified
        {
            get { return this.AssignmentConditionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("AssignmentDescription", Order = 2)]
        public MISMOString AssignmentDescription { get; set; }
    
        [XmlIgnore]
        public bool AssignmentDescriptionSpecified
        {
            get { return this.AssignmentDescription != null; }
            set { }
        }
    
        [XmlElement("AssignmentParameterPriorityType", Order = 3)]
        public MISMOEnum<AssignmentParameterPriorityBase> AssignmentParameterPriorityType { get; set; }
    
        [XmlIgnore]
        public bool AssignmentParameterPriorityTypeSpecified
        {
            get { return this.AssignmentParameterPriorityType != null; }
            set { }
        }
    
        [XmlElement("AssignmentParameterSourceType", Order = 4)]
        public MISMOEnum<AssignmentParameterSourceBase> AssignmentParameterSourceType { get; set; }
    
        [XmlIgnore]
        public bool AssignmentParameterSourceTypeSpecified
        {
            get { return this.AssignmentParameterSourceType != null; }
            set { }
        }
    
        [XmlElement("AssignmentParameterSourceTypeOtherDescription", Order = 5)]
        public MISMOString AssignmentParameterSourceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool AssignmentParameterSourceTypeOtherDescriptionSpecified
        {
            get { return this.AssignmentParameterSourceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("AssignmentParameterType", Order = 6)]
        public MISMOEnum<AssignmentParameterBase> AssignmentParameterType { get; set; }
    
        [XmlIgnore]
        public bool AssignmentParameterTypeSpecified
        {
            get { return this.AssignmentParameterType != null; }
            set { }
        }
    
        [XmlElement("AssignmentParameterTypeOtherDescription", Order = 7)]
        public MISMOString AssignmentParameterTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool AssignmentParameterTypeOtherDescriptionSpecified
        {
            get { return this.AssignmentParameterTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("VendorSpecialtyType", Order = 8)]
        public MISMOEnum<VendorSpecialtyBase> VendorSpecialtyType { get; set; }
    
        [XmlIgnore]
        public bool VendorSpecialtyTypeSpecified
        {
            get { return this.VendorSpecialtyType != null; }
            set { }
        }
    
        [XmlElement("VendorSpecialtyTypeOtherDescription", Order = 9)]
        public MISMOString VendorSpecialtyTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool VendorSpecialtyTypeOtherDescriptionSpecified
        {
            get { return this.VendorSpecialtyTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("VendorWatchListIndicationDescription", Order = 10)]
        public MISMOString VendorWatchListIndicationDescription { get; set; }
    
        [XmlIgnore]
        public bool VendorWatchListIndicationDescriptionSpecified
        {
            get { return this.VendorWatchListIndicationDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 11)]
        public ASSIGNMENT_PARAMETER_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
