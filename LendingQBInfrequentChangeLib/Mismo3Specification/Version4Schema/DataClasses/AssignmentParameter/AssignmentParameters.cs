namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class ASSIGNMENT_PARAMETERS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AssignmentParameterListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ASSIGNMENT_PARAMETER", Order = 0)]
        public List<ASSIGNMENT_PARAMETER> AssignmentParameterList { get; set; } = new List<ASSIGNMENT_PARAMETER>();
    
        [XmlIgnore]
        public bool AssignmentParameterListSpecified
        {
            get { return this.AssignmentParameterList != null && this.AssignmentParameterList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public ASSIGNMENT_PARAMETERS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
