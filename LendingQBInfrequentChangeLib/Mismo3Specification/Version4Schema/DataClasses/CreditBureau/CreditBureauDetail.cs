namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CREDIT_BUREAU_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditBureauDisclaimerTextSpecified
                    || this.CreditBureauNameSpecified
                    || this.CreditBureauTechnologyProviderNameSpecified
                    || this.FNMCreditBureauIdentifierSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CreditBureauDisclaimerText", Order = 0)]
        public MISMOString CreditBureauDisclaimerText { get; set; }
    
        [XmlIgnore]
        public bool CreditBureauDisclaimerTextSpecified
        {
            get { return this.CreditBureauDisclaimerText != null; }
            set { }
        }
    
        [XmlElement("CreditBureauName", Order = 1)]
        public MISMOString CreditBureauName { get; set; }
    
        [XmlIgnore]
        public bool CreditBureauNameSpecified
        {
            get { return this.CreditBureauName != null; }
            set { }
        }
    
        [XmlElement("CreditBureauTechnologyProviderName", Order = 2)]
        public MISMOString CreditBureauTechnologyProviderName { get; set; }
    
        [XmlIgnore]
        public bool CreditBureauTechnologyProviderNameSpecified
        {
            get { return this.CreditBureauTechnologyProviderName != null; }
            set { }
        }
    
        [XmlElement("FNMCreditBureauIdentifier", Order = 3)]
        public MISMOIdentifier FNMCreditBureauIdentifier { get; set; }
    
        [XmlIgnore]
        public bool FNMCreditBureauIdentifierSpecified
        {
            get { return this.FNMCreditBureauIdentifier != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public CREDIT_BUREAU_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
