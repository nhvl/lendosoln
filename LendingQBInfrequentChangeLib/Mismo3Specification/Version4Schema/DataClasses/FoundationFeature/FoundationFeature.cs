namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class FOUNDATION_FEATURE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ComponentAdjustmentAmountSpecified
                    || this.ComponentClassificationTypeSpecified
                    || this.ConditionRatingDescriptionSpecified
                    || this.ConditionRatingTypeSpecified
                    || this.FoundationFeatureDescriptionSpecified
                    || this.FoundationFeatureTypeSpecified
                    || this.FoundationFeatureTypeOtherDescriptionSpecified
                    || this.QualityRatingDescriptionSpecified
                    || this.QualityRatingTypeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ComponentAdjustmentAmount", Order = 0)]
        public MISMOAmount ComponentAdjustmentAmount { get; set; }
    
        [XmlIgnore]
        public bool ComponentAdjustmentAmountSpecified
        {
            get { return this.ComponentAdjustmentAmount != null; }
            set { }
        }
    
        [XmlElement("ComponentClassificationType", Order = 1)]
        public MISMOEnum<ComponentClassificationBase> ComponentClassificationType { get; set; }
    
        [XmlIgnore]
        public bool ComponentClassificationTypeSpecified
        {
            get { return this.ComponentClassificationType != null; }
            set { }
        }
    
        [XmlElement("ConditionRatingDescription", Order = 2)]
        public MISMOString ConditionRatingDescription { get; set; }
    
        [XmlIgnore]
        public bool ConditionRatingDescriptionSpecified
        {
            get { return this.ConditionRatingDescription != null; }
            set { }
        }
    
        [XmlElement("ConditionRatingType", Order = 3)]
        public MISMOEnum<ConditionRatingBase> ConditionRatingType { get; set; }
    
        [XmlIgnore]
        public bool ConditionRatingTypeSpecified
        {
            get { return this.ConditionRatingType != null; }
            set { }
        }
    
        [XmlElement("FoundationFeatureDescription", Order = 4)]
        public MISMOString FoundationFeatureDescription { get; set; }
    
        [XmlIgnore]
        public bool FoundationFeatureDescriptionSpecified
        {
            get { return this.FoundationFeatureDescription != null; }
            set { }
        }
    
        [XmlElement("FoundationFeatureType", Order = 5)]
        public MISMOEnum<FoundationFeatureBase> FoundationFeatureType { get; set; }
    
        [XmlIgnore]
        public bool FoundationFeatureTypeSpecified
        {
            get { return this.FoundationFeatureType != null; }
            set { }
        }
    
        [XmlElement("FoundationFeatureTypeOtherDescription", Order = 6)]
        public MISMOString FoundationFeatureTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool FoundationFeatureTypeOtherDescriptionSpecified
        {
            get { return this.FoundationFeatureTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("QualityRatingDescription", Order = 7)]
        public MISMOString QualityRatingDescription { get; set; }
    
        [XmlIgnore]
        public bool QualityRatingDescriptionSpecified
        {
            get { return this.QualityRatingDescription != null; }
            set { }
        }
    
        [XmlElement("QualityRatingType", Order = 8)]
        public MISMOEnum<QualityRatingBase> QualityRatingType { get; set; }
    
        [XmlIgnore]
        public bool QualityRatingTypeSpecified
        {
            get { return this.QualityRatingType != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 9)]
        public FOUNDATION_FEATURE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
