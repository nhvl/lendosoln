namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class FOUNDATION_FEATURES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.FoundationFeatureListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("FOUNDATION_FEATURE", Order = 0)]
        public List<FOUNDATION_FEATURE> FoundationFeatureList { get; set; } = new List<FOUNDATION_FEATURE>();
    
        [XmlIgnore]
        public bool FoundationFeatureListSpecified
        {
            get { return this.FoundationFeatureList != null && this.FoundationFeatureList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public FOUNDATION_FEATURES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
