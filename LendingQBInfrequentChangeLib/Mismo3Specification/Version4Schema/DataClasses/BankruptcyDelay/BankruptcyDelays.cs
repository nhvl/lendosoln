namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class BANKRUPTCY_DELAYS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BankruptcyDelayListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BANKRUPTCY_DELAY", Order = 0)]
        public List<BANKRUPTCY_DELAY> BankruptcyDelayList { get; set; } = new List<BANKRUPTCY_DELAY>();
    
        [XmlIgnore]
        public bool BankruptcyDelayListSpecified
        {
            get { return this.BankruptcyDelayList != null && this.BankruptcyDelayList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public BANKRUPTCY_DELAYS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
