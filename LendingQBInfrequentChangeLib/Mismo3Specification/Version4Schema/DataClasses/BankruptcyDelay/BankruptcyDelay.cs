namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class BANKRUPTCY_DELAY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BankruptcyDelayCategoryTypeSpecified
                    || this.BankruptcyDelayCategoryTypeAdditionalDescriptionSpecified
                    || this.BankruptcyDelayCategoryTypeOtherDescriptionSpecified
                    || this.BankruptcyDelayEndDateSpecified
                    || this.BankruptcyDelayStartDateSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BankruptcyDelayCategoryType", Order = 0)]
        public MISMOEnum<BankruptcyDelayCategoryBase> BankruptcyDelayCategoryType { get; set; }
    
        [XmlIgnore]
        public bool BankruptcyDelayCategoryTypeSpecified
        {
            get { return this.BankruptcyDelayCategoryType != null; }
            set { }
        }
    
        [XmlElement("BankruptcyDelayCategoryTypeAdditionalDescription", Order = 1)]
        public MISMOString BankruptcyDelayCategoryTypeAdditionalDescription { get; set; }
    
        [XmlIgnore]
        public bool BankruptcyDelayCategoryTypeAdditionalDescriptionSpecified
        {
            get { return this.BankruptcyDelayCategoryTypeAdditionalDescription != null; }
            set { }
        }
    
        [XmlElement("BankruptcyDelayCategoryTypeOtherDescription", Order = 2)]
        public MISMOString BankruptcyDelayCategoryTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool BankruptcyDelayCategoryTypeOtherDescriptionSpecified
        {
            get { return this.BankruptcyDelayCategoryTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("BankruptcyDelayEndDate", Order = 3)]
        public MISMODate BankruptcyDelayEndDate { get; set; }
    
        [XmlIgnore]
        public bool BankruptcyDelayEndDateSpecified
        {
            get { return this.BankruptcyDelayEndDate != null; }
            set { }
        }
    
        [XmlElement("BankruptcyDelayStartDate", Order = 4)]
        public MISMODate BankruptcyDelayStartDate { get; set; }
    
        [XmlIgnore]
        public bool BankruptcyDelayStartDateSpecified
        {
            get { return this.BankruptcyDelayStartDate != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public BANKRUPTCY_DELAY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
