namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    public class VIEW
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                if (Mismo3Utilities.ExceedsThreshold(1,
                    new List<bool> { this.ViewFilesSpecified, this.ViewPagesSpecified }))
                {
                    throw new System.Xml.Schema.XmlSchemaValidationException(
                        Mismo3Utilities.GetXSDChoiceExceptionMessage(
                        "VIEW",
                        new List<string> { "VIEW_FILES", "VIEW_PAGES" }));
                }

                return this.ViewFilesSpecified
                    || this.ViewPagesSpecified
                    || this.ViewDetailSpecified
                    || this.ViewFieldsSpecified
                    || this.ViewIdentifiersSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("VIEW_FILES", Order = 0)]
        public VIEW_FILES ViewFiles { get; set; }
    
        [XmlIgnore]
        public bool ViewFilesSpecified
        {
            get { return this.ViewFiles != null && this.ViewFiles.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("VIEW_PAGES", Order = 1)]
        public VIEW_PAGES ViewPages { get; set; }
    
        [XmlIgnore]
        public bool ViewPagesSpecified
        {
            get { return this.ViewPages != null && this.ViewPages.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("VIEW_DETAIL", Order = 2)]
        public VIEW_DETAIL ViewDetail { get; set; }
    
        [XmlIgnore]
        public bool ViewDetailSpecified
        {
            get { return this.ViewDetail != null && this.ViewDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("VIEW_FIELDS", Order = 3)]
        public VIEW_FIELDS ViewFields { get; set; }
    
        [XmlIgnore]
        public bool ViewFieldsSpecified
        {
            get { return this.ViewFields != null && this.ViewFields.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("VIEW_IDENTIFIERS", Order = 4)]
        public VIEW_IDENTIFIERS ViewIdentifiers { get; set; }
    
        [XmlIgnore]
        public bool ViewIdentifiersSpecified
        {
            get { return this.ViewIdentifiers != null && this.ViewIdentifiers.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public VIEW_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
