namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class VIEW_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ViewCommentDescriptionSpecified
                    || this.ViewIsRecordedIndicatorSpecified
                    || this.ViewPageCountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ViewCommentDescription", Order = 0)]
        public MISMOString ViewCommentDescription { get; set; }
    
        [XmlIgnore]
        public bool ViewCommentDescriptionSpecified
        {
            get { return this.ViewCommentDescription != null; }
            set { }
        }
    
        [XmlElement("ViewIsRecordedIndicator", Order = 1)]
        public MISMOIndicator ViewIsRecordedIndicator { get; set; }
    
        [XmlIgnore]
        public bool ViewIsRecordedIndicatorSpecified
        {
            get { return this.ViewIsRecordedIndicator != null; }
            set { }
        }
    
        [XmlElement("ViewPageCount", Order = 2)]
        public MISMOCount ViewPageCount { get; set; }
    
        [XmlIgnore]
        public bool ViewPageCountSpecified
        {
            get { return this.ViewPageCount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public VIEW_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
