namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class VIEWS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ViewListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("VIEW", Order = 0)]
        public List<VIEW> ViewList { get; set; } = new List<VIEW>();
    
        [XmlIgnore]
        public bool ViewListSpecified
        {
            get { return this.ViewList != null && this.ViewList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public VIEWS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
