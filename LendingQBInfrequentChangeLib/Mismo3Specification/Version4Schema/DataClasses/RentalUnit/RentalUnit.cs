namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class RENTAL_UNIT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MonthlyRentAmountSpecified
                    || this.SquareFeetNumberSpecified
                    || this.TotalBathroomCountSpecified
                    || this.TotalBedroomCountSpecified
                    || this.TotalRoomCountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MonthlyRentAmount", Order = 0)]
        public MISMOAmount MonthlyRentAmount { get; set; }
    
        [XmlIgnore]
        public bool MonthlyRentAmountSpecified
        {
            get { return this.MonthlyRentAmount != null; }
            set { }
        }
    
        [XmlElement("SquareFeetNumber", Order = 1)]
        public MISMONumeric SquareFeetNumber { get; set; }
    
        [XmlIgnore]
        public bool SquareFeetNumberSpecified
        {
            get { return this.SquareFeetNumber != null; }
            set { }
        }
    
        [XmlElement("TotalBathroomCount", Order = 2)]
        public MISMOCount TotalBathroomCount { get; set; }
    
        [XmlIgnore]
        public bool TotalBathroomCountSpecified
        {
            get { return this.TotalBathroomCount != null; }
            set { }
        }
    
        [XmlElement("TotalBedroomCount", Order = 3)]
        public MISMOCount TotalBedroomCount { get; set; }
    
        [XmlIgnore]
        public bool TotalBedroomCountSpecified
        {
            get { return this.TotalBedroomCount != null; }
            set { }
        }
    
        [XmlElement("TotalRoomCount", Order = 4)]
        public MISMOCount TotalRoomCount { get; set; }
    
        [XmlIgnore]
        public bool TotalRoomCountSpecified
        {
            get { return this.TotalRoomCount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public RENTAL_UNIT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
