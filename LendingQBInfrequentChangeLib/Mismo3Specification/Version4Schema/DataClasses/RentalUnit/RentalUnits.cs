namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class RENTAL_UNITS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.RentalUnitListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("RENTAL_UNIT", Order = 0)]
        public List<RENTAL_UNIT> RentalUnitList { get; set; } = new List<RENTAL_UNIT>();
    
        [XmlIgnore]
        public bool RentalUnitListSpecified
        {
            get { return this.RentalUnitList != null && this.RentalUnitList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public RENTAL_UNITS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
