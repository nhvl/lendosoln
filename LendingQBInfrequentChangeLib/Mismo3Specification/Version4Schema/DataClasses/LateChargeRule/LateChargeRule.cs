namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class LATE_CHARGE_RULE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LateChargeAmountSpecified
                    || this.LateChargeGracePeriodDaysCountSpecified
                    || this.LateChargeMaximumAmountSpecified
                    || this.LateChargeMinimumAmountSpecified
                    || this.LateChargeRatePercentSpecified
                    || this.LateChargeTypeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LateChargeAmount", Order = 0)]
        public MISMOAmount LateChargeAmount { get; set; }
    
        [XmlIgnore]
        public bool LateChargeAmountSpecified
        {
            get { return this.LateChargeAmount != null; }
            set { }
        }
    
        [XmlElement("LateChargeGracePeriodDaysCount", Order = 1)]
        public MISMOCount LateChargeGracePeriodDaysCount { get; set; }
    
        [XmlIgnore]
        public bool LateChargeGracePeriodDaysCountSpecified
        {
            get { return this.LateChargeGracePeriodDaysCount != null; }
            set { }
        }
    
        [XmlElement("LateChargeMaximumAmount", Order = 2)]
        public MISMOAmount LateChargeMaximumAmount { get; set; }
    
        [XmlIgnore]
        public bool LateChargeMaximumAmountSpecified
        {
            get { return this.LateChargeMaximumAmount != null; }
            set { }
        }
    
        [XmlElement("LateChargeMinimumAmount", Order = 3)]
        public MISMOAmount LateChargeMinimumAmount { get; set; }
    
        [XmlIgnore]
        public bool LateChargeMinimumAmountSpecified
        {
            get { return this.LateChargeMinimumAmount != null; }
            set { }
        }
    
        [XmlElement("LateChargeRatePercent", Order = 4)]
        public MISMOPercent LateChargeRatePercent { get; set; }
    
        [XmlIgnore]
        public bool LateChargeRatePercentSpecified
        {
            get { return this.LateChargeRatePercent != null; }
            set { }
        }
    
        [XmlElement("LateChargeType", Order = 5)]
        public MISMOEnum<LateChargeBase> LateChargeType { get; set; }
    
        [XmlIgnore]
        public bool LateChargeTypeSpecified
        {
            get { return this.LateChargeType != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 6)]
        public LATE_CHARGE_RULE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
