namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class DELINQUENCY_EVENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LoanDelinquencyEventDateSpecified
                    || this.LoanDelinquencyEventTypeSpecified
                    || this.LoanDelinquencyEventTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LoanDelinquencyEventDate", Order = 0)]
        public MISMODate LoanDelinquencyEventDate { get; set; }
    
        [XmlIgnore]
        public bool LoanDelinquencyEventDateSpecified
        {
            get { return this.LoanDelinquencyEventDate != null; }
            set { }
        }
    
        [XmlElement("LoanDelinquencyEventType", Order = 1)]
        public MISMOEnum<LoanDelinquencyEventBase> LoanDelinquencyEventType { get; set; }
    
        [XmlIgnore]
        public bool LoanDelinquencyEventTypeSpecified
        {
            get { return this.LoanDelinquencyEventType != null; }
            set { }
        }
    
        [XmlElement("LoanDelinquencyEventTypeOtherDescription", Order = 2)]
        public MISMOString LoanDelinquencyEventTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool LoanDelinquencyEventTypeOtherDescriptionSpecified
        {
            get { return this.LoanDelinquencyEventTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public DELINQUENCY_EVENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
