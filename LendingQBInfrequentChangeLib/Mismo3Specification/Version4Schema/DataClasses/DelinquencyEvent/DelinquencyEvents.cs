namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class DELINQUENCY_EVENTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DelinquencyEventListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DELINQUENCY_EVENT", Order = 0)]
        public List<DELINQUENCY_EVENT> DelinquencyEventList { get; set; } = new List<DELINQUENCY_EVENT>();
    
        [XmlIgnore]
        public bool DelinquencyEventListSpecified
        {
            get { return this.DelinquencyEventList != null && this.DelinquencyEventList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public DELINQUENCY_EVENTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
