namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class DATA_MODIFICATION_REQUEST
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DataItemChangeRequestSpecified
                    || this.DataModificationRequestDetailSpecified
                    || this.DealSetSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DATA_ITEM_CHANGE_REQUEST", Order = 0)]
        public DATA_ITEM_CHANGE_REQUEST DataItemChangeRequest { get; set; }
    
        [XmlIgnore]
        public bool DataItemChangeRequestSpecified
        {
            get { return this.DataItemChangeRequest != null && this.DataItemChangeRequest.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("DATA_MODIFICATION_REQUEST_DETAIL", Order = 1)]
        public DATA_MODIFICATION_REQUEST_DETAIL DataModificationRequestDetail { get; set; }
    
        [XmlIgnore]
        public bool DataModificationRequestDetailSpecified
        {
            get { return this.DataModificationRequestDetail != null && this.DataModificationRequestDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("DEAL_SET", Order = 2)]
        public DEAL_SET DealSet { get; set; }
    
        [XmlIgnore]
        public bool DealSetSpecified
        {
            get { return this.DealSet != null && this.DealSet.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public DATA_MODIFICATION_REQUEST_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
