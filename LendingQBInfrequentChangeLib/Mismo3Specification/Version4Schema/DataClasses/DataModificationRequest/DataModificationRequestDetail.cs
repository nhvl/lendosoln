namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class DATA_MODIFICATION_REQUEST_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.RequestDatetimeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("RequestDatetime", Order = 0)]
        public MISMODatetime RequestDatetime { get; set; }
    
        [XmlIgnore]
        public bool RequestDatetimeSpecified
        {
            get { return this.RequestDatetime != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public DATA_MODIFICATION_REQUEST_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
