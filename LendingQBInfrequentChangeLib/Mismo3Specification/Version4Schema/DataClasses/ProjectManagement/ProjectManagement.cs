namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PROJECT_MANAGEMENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ProjectManagementTypeSpecified
                    || this.ProjectManagementTypeOtherDescriptionSpecified
                    || this.ProjectPartOfMasterAssociationDescriptionSpecified
                    || this.ProjectPartOfMasterAssociationIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ProjectManagementType", Order = 0)]
        public MISMOEnum<ProjectManagementBase> ProjectManagementType { get; set; }
    
        [XmlIgnore]
        public bool ProjectManagementTypeSpecified
        {
            get { return this.ProjectManagementType != null; }
            set { }
        }
    
        [XmlElement("ProjectManagementTypeOtherDescription", Order = 1)]
        public MISMOString ProjectManagementTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ProjectManagementTypeOtherDescriptionSpecified
        {
            get { return this.ProjectManagementTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ProjectPartOfMasterAssociationDescription", Order = 2)]
        public MISMOString ProjectPartOfMasterAssociationDescription { get; set; }
    
        [XmlIgnore]
        public bool ProjectPartOfMasterAssociationDescriptionSpecified
        {
            get { return this.ProjectPartOfMasterAssociationDescription != null; }
            set { }
        }
    
        [XmlElement("ProjectPartOfMasterAssociationIndicator", Order = 3)]
        public MISMOIndicator ProjectPartOfMasterAssociationIndicator { get; set; }
    
        [XmlIgnore]
        public bool ProjectPartOfMasterAssociationIndicatorSpecified
        {
            get { return this.ProjectPartOfMasterAssociationIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public PROJECT_MANAGEMENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
