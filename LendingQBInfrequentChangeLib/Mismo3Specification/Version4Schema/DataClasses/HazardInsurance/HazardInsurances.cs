namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class HAZARD_INSURANCES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.HazardInsuranceListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("HAZARD_INSURANCE", Order = 0)]
        public List<HAZARD_INSURANCE> HazardInsuranceList { get; set; } = new List<HAZARD_INSURANCE>();
    
        [XmlIgnore]
        public bool HazardInsuranceListSpecified
        {
            get { return this.HazardInsuranceList != null && this.HazardInsuranceList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public HAZARD_INSURANCES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
