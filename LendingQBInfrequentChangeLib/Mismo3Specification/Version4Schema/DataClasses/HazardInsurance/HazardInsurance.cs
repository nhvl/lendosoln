namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class HAZARD_INSURANCE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.FloodContractFeeAmountSpecified
                    || this.HazardInsuranceCoverageAmountSpecified
                    || this.HazardInsuranceCoverageTypeSpecified
                    || this.HazardInsuranceCoverageTypeOtherDescriptionSpecified
                    || this.HazardInsuranceEffectiveDateSpecified
                    || this.HazardInsuranceEscrowedIndicatorSpecified
                    || this.HazardInsuranceExpirationDateSpecified
                    || this.HazardInsuranceNextPremiumDueDateSpecified
                    || this.HazardInsuranceNonStandardPolicyTypeSpecified
                    || this.HazardInsuranceNonStandardPolicyTypeOtherDescriptionSpecified
                    || this.HazardInsurancePolicyCancellationDateSpecified
                    || this.HazardInsurancePolicyIdentifierSpecified
                    || this.HazardInsurancePremiumAmountSpecified
                    || this.HazardInsurancePremiumMonthsCountSpecified
                    || this.HazardInsuranceReplacementValueIndicatorSpecified
                    || this.InsuranceRequiredIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("FloodContractFeeAmount", Order = 0)]
        public MISMOAmount FloodContractFeeAmount { get; set; }
    
        [XmlIgnore]
        public bool FloodContractFeeAmountSpecified
        {
            get { return this.FloodContractFeeAmount != null; }
            set { }
        }
    
        [XmlElement("HazardInsuranceCoverageAmount", Order = 1)]
        public MISMOAmount HazardInsuranceCoverageAmount { get; set; }
    
        [XmlIgnore]
        public bool HazardInsuranceCoverageAmountSpecified
        {
            get { return this.HazardInsuranceCoverageAmount != null; }
            set { }
        }
    
        [XmlElement("HazardInsuranceCoverageType", Order = 2)]
        public MISMOEnum<HazardInsuranceCoverageBase> HazardInsuranceCoverageType { get; set; }
    
        [XmlIgnore]
        public bool HazardInsuranceCoverageTypeSpecified
        {
            get { return this.HazardInsuranceCoverageType != null; }
            set { }
        }
    
        [XmlElement("HazardInsuranceCoverageTypeOtherDescription", Order = 3)]
        public MISMOString HazardInsuranceCoverageTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool HazardInsuranceCoverageTypeOtherDescriptionSpecified
        {
            get { return this.HazardInsuranceCoverageTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("HazardInsuranceEffectiveDate", Order = 4)]
        public MISMODate HazardInsuranceEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool HazardInsuranceEffectiveDateSpecified
        {
            get { return this.HazardInsuranceEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("HazardInsuranceEscrowedIndicator", Order = 5)]
        public MISMOIndicator HazardInsuranceEscrowedIndicator { get; set; }
    
        [XmlIgnore]
        public bool HazardInsuranceEscrowedIndicatorSpecified
        {
            get { return this.HazardInsuranceEscrowedIndicator != null; }
            set { }
        }
    
        [XmlElement("HazardInsuranceExpirationDate", Order = 6)]
        public MISMODate HazardInsuranceExpirationDate { get; set; }
    
        [XmlIgnore]
        public bool HazardInsuranceExpirationDateSpecified
        {
            get { return this.HazardInsuranceExpirationDate != null; }
            set { }
        }
    
        [XmlElement("HazardInsuranceNextPremiumDueDate", Order = 7)]
        public MISMODate HazardInsuranceNextPremiumDueDate { get; set; }
    
        [XmlIgnore]
        public bool HazardInsuranceNextPremiumDueDateSpecified
        {
            get { return this.HazardInsuranceNextPremiumDueDate != null; }
            set { }
        }
    
        [XmlElement("HazardInsuranceNonStandardPolicyType", Order = 8)]
        public MISMOEnum<HazardInsuranceNonStandardPolicyBase> HazardInsuranceNonStandardPolicyType { get; set; }
    
        [XmlIgnore]
        public bool HazardInsuranceNonStandardPolicyTypeSpecified
        {
            get { return this.HazardInsuranceNonStandardPolicyType != null; }
            set { }
        }
    
        [XmlElement("HazardInsuranceNonStandardPolicyTypeOtherDescription", Order = 9)]
        public MISMOString HazardInsuranceNonStandardPolicyTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool HazardInsuranceNonStandardPolicyTypeOtherDescriptionSpecified
        {
            get { return this.HazardInsuranceNonStandardPolicyTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("HazardInsurancePolicyCancellationDate", Order = 10)]
        public MISMODate HazardInsurancePolicyCancellationDate { get; set; }
    
        [XmlIgnore]
        public bool HazardInsurancePolicyCancellationDateSpecified
        {
            get { return this.HazardInsurancePolicyCancellationDate != null; }
            set { }
        }
    
        [XmlElement("HazardInsurancePolicyIdentifier", Order = 11)]
        public MISMOIdentifier HazardInsurancePolicyIdentifier { get; set; }
    
        [XmlIgnore]
        public bool HazardInsurancePolicyIdentifierSpecified
        {
            get { return this.HazardInsurancePolicyIdentifier != null; }
            set { }
        }
    
        [XmlElement("HazardInsurancePremiumAmount", Order = 12)]
        public MISMOAmount HazardInsurancePremiumAmount { get; set; }
    
        [XmlIgnore]
        public bool HazardInsurancePremiumAmountSpecified
        {
            get { return this.HazardInsurancePremiumAmount != null; }
            set { }
        }
    
        [XmlElement("HazardInsurancePremiumMonthsCount", Order = 13)]
        public MISMOCount HazardInsurancePremiumMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool HazardInsurancePremiumMonthsCountSpecified
        {
            get { return this.HazardInsurancePremiumMonthsCount != null; }
            set { }
        }
    
        [XmlElement("HazardInsuranceReplacementValueIndicator", Order = 14)]
        public MISMOIndicator HazardInsuranceReplacementValueIndicator { get; set; }
    
        [XmlIgnore]
        public bool HazardInsuranceReplacementValueIndicatorSpecified
        {
            get { return this.HazardInsuranceReplacementValueIndicator != null; }
            set { }
        }
    
        [XmlElement("InsuranceRequiredIndicator", Order = 15)]
        public MISMOIndicator InsuranceRequiredIndicator { get; set; }
    
        [XmlIgnore]
        public bool InsuranceRequiredIndicatorSpecified
        {
            get { return this.InsuranceRequiredIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 16)]
        public HAZARD_INSURANCE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
