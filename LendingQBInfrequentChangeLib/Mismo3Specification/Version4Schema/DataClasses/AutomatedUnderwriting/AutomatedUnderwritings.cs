namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class AUTOMATED_UNDERWRITINGS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AutomatedUnderwritingListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AUTOMATED_UNDERWRITING", Order = 0)]
        public List<AUTOMATED_UNDERWRITING> AutomatedUnderwritingList { get; set; } = new List<AUTOMATED_UNDERWRITING>();
    
        [XmlIgnore]
        public bool AutomatedUnderwritingListSpecified
        {
            get { return this.AutomatedUnderwritingList != null && this.AutomatedUnderwritingList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public AUTOMATED_UNDERWRITINGS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
