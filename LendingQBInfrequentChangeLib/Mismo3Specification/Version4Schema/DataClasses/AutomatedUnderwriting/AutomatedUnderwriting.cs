namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class AUTOMATED_UNDERWRITING
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AutomatedUnderwritingCaseIdentifierSpecified
                    || this.AutomatedUnderwritingDecisionDatetimeSpecified
                    || this.AutomatedUnderwritingEvaluationStatusDescriptionSpecified
                    || this.AutomatedUnderwritingMethodVersionIdentifierSpecified
                    || this.AutomatedUnderwritingProcessDescriptionSpecified
                    || this.AutomatedUnderwritingRecommendationDescriptionSpecified
                    || this.AutomatedUnderwritingSystemDocumentWaiverIndicatorSpecified
                    || this.AutomatedUnderwritingSystemResultValueSpecified
                    || this.AutomatedUnderwritingSystemTypeSpecified
                    || this.AutomatedUnderwritingSystemTypeOtherDescriptionSpecified
                    || this.DesktopUnderwriterRecommendationTypeSpecified
                    || this.FHAPreReviewResultsValueSpecified
                    || this.FREPurchaseEligibilityTypeSpecified
                    || this.LoanProspectorCreditRiskClassificationDescriptionSpecified
                    || this.LoanProspectorDocumentationClassificationDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AutomatedUnderwritingCaseIdentifier", Order = 0)]
        public MISMOIdentifier AutomatedUnderwritingCaseIdentifier { get; set; }
    
        [XmlIgnore]
        public bool AutomatedUnderwritingCaseIdentifierSpecified
        {
            get { return this.AutomatedUnderwritingCaseIdentifier != null; }
            set { }
        }
    
        [XmlElement("AutomatedUnderwritingDecisionDatetime", Order = 1)]
        public MISMODatetime AutomatedUnderwritingDecisionDatetime { get; set; }
    
        [XmlIgnore]
        public bool AutomatedUnderwritingDecisionDatetimeSpecified
        {
            get { return this.AutomatedUnderwritingDecisionDatetime != null; }
            set { }
        }
    
        [XmlElement("AutomatedUnderwritingEvaluationStatusDescription", Order = 2)]
        public MISMOString AutomatedUnderwritingEvaluationStatusDescription { get; set; }
    
        [XmlIgnore]
        public bool AutomatedUnderwritingEvaluationStatusDescriptionSpecified
        {
            get { return this.AutomatedUnderwritingEvaluationStatusDescription != null; }
            set { }
        }
    
        [XmlElement("AutomatedUnderwritingMethodVersionIdentifier", Order = 3)]
        public MISMOIdentifier AutomatedUnderwritingMethodVersionIdentifier { get; set; }
    
        [XmlIgnore]
        public bool AutomatedUnderwritingMethodVersionIdentifierSpecified
        {
            get { return this.AutomatedUnderwritingMethodVersionIdentifier != null; }
            set { }
        }
    
        [XmlElement("AutomatedUnderwritingProcessDescription", Order = 4)]
        public MISMOString AutomatedUnderwritingProcessDescription { get; set; }
    
        [XmlIgnore]
        public bool AutomatedUnderwritingProcessDescriptionSpecified
        {
            get { return this.AutomatedUnderwritingProcessDescription != null; }
            set { }
        }
    
        [XmlElement("AutomatedUnderwritingRecommendationDescription", Order = 5)]
        public MISMOString AutomatedUnderwritingRecommendationDescription { get; set; }
    
        [XmlIgnore]
        public bool AutomatedUnderwritingRecommendationDescriptionSpecified
        {
            get { return this.AutomatedUnderwritingRecommendationDescription != null; }
            set { }
        }
    
        [XmlElement("AutomatedUnderwritingSystemDocumentWaiverIndicator", Order = 6)]
        public MISMOIndicator AutomatedUnderwritingSystemDocumentWaiverIndicator { get; set; }
    
        [XmlIgnore]
        public bool AutomatedUnderwritingSystemDocumentWaiverIndicatorSpecified
        {
            get { return this.AutomatedUnderwritingSystemDocumentWaiverIndicator != null; }
            set { }
        }
    
        [XmlElement("AutomatedUnderwritingSystemResultValue", Order = 7)]
        public MISMOValue AutomatedUnderwritingSystemResultValue { get; set; }
    
        [XmlIgnore]
        public bool AutomatedUnderwritingSystemResultValueSpecified
        {
            get { return this.AutomatedUnderwritingSystemResultValue != null; }
            set { }
        }
    
        [XmlElement("AutomatedUnderwritingSystemType", Order = 8)]
        public MISMOEnum<AutomatedUnderwritingSystemBase> AutomatedUnderwritingSystemType { get; set; }
    
        [XmlIgnore]
        public bool AutomatedUnderwritingSystemTypeSpecified
        {
            get { return this.AutomatedUnderwritingSystemType != null; }
            set { }
        }
    
        [XmlElement("AutomatedUnderwritingSystemTypeOtherDescription", Order = 9)]
        public MISMOString AutomatedUnderwritingSystemTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool AutomatedUnderwritingSystemTypeOtherDescriptionSpecified
        {
            get { return this.AutomatedUnderwritingSystemTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("DesktopUnderwriterRecommendationType", Order = 10)]
        public MISMOEnum<DesktopUnderwriterRecommendationBase> DesktopUnderwriterRecommendationType { get; set; }
    
        [XmlIgnore]
        public bool DesktopUnderwriterRecommendationTypeSpecified
        {
            get { return this.DesktopUnderwriterRecommendationType != null; }
            set { }
        }
    
        [XmlElement("FHAPreReviewResultsValue", Order = 11)]
        public MISMOString FHAPreReviewResultsValue { get; set; }
    
        [XmlIgnore]
        public bool FHAPreReviewResultsValueSpecified
        {
            get { return this.FHAPreReviewResultsValue != null; }
            set { }
        }
    
        [XmlElement("FREPurchaseEligibilityType", Order = 12)]
        public MISMOEnum<FREPurchaseEligibilityBase> FREPurchaseEligibilityType { get; set; }
    
        [XmlIgnore]
        public bool FREPurchaseEligibilityTypeSpecified
        {
            get { return this.FREPurchaseEligibilityType != null; }
            set { }
        }
    
        [XmlElement("LoanProspectorCreditRiskClassificationDescription", Order = 13)]
        public MISMOString LoanProspectorCreditRiskClassificationDescription { get; set; }
    
        [XmlIgnore]
        public bool LoanProspectorCreditRiskClassificationDescriptionSpecified
        {
            get { return this.LoanProspectorCreditRiskClassificationDescription != null; }
            set { }
        }
    
        [XmlElement("LoanProspectorDocumentationClassificationDescription", Order = 14)]
        public MISMOString LoanProspectorDocumentationClassificationDescription { get; set; }
    
        [XmlIgnore]
        public bool LoanProspectorDocumentationClassificationDescriptionSpecified
        {
            get { return this.LoanProspectorDocumentationClassificationDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 15)]
        public AUTOMATED_UNDERWRITING_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
