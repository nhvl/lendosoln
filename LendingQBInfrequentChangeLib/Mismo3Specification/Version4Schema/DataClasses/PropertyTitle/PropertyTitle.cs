namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PROPERTY_TITLE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ClearAndMarketablePropertyTitleIndicatorSpecified
                    || this.PropertyTitleTransferInspectionRequiredDescriptionSpecified
                    || this.PropertyTitleTransferInspectionRequiredIndicatorSpecified
                    || this.SellerPropertyAcquisitionDateSpecified
                    || this.TitleHolderCountSpecified
                    || this.TitlePreliminaryReportDateSpecified
                    || this.TitleProcessInsuranceTypeSpecified
                    || this.TitleProcessTypeSpecified
                    || this.TitleProcessTypeOtherDescriptionSpecified
                    || this.TitleReportItemsDescriptionSpecified
                    || this.TitleReportRequiredEndorsementsDescriptionSpecified
                    || this.TitleTransferredPastSixMonthsIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ClearAndMarketablePropertyTitleIndicator", Order = 0)]
        public MISMOIndicator ClearAndMarketablePropertyTitleIndicator { get; set; }
    
        [XmlIgnore]
        public bool ClearAndMarketablePropertyTitleIndicatorSpecified
        {
            get { return this.ClearAndMarketablePropertyTitleIndicator != null; }
            set { }
        }
    
        [XmlElement("PropertyTitleTransferInspectionRequiredDescription", Order = 1)]
        public MISMOString PropertyTitleTransferInspectionRequiredDescription { get; set; }
    
        [XmlIgnore]
        public bool PropertyTitleTransferInspectionRequiredDescriptionSpecified
        {
            get { return this.PropertyTitleTransferInspectionRequiredDescription != null; }
            set { }
        }
    
        [XmlElement("PropertyTitleTransferInspectionRequiredIndicator", Order = 2)]
        public MISMOIndicator PropertyTitleTransferInspectionRequiredIndicator { get; set; }
    
        [XmlIgnore]
        public bool PropertyTitleTransferInspectionRequiredIndicatorSpecified
        {
            get { return this.PropertyTitleTransferInspectionRequiredIndicator != null; }
            set { }
        }
    
        [XmlElement("SellerPropertyAcquisitionDate", Order = 3)]
        public MISMODate SellerPropertyAcquisitionDate { get; set; }
    
        [XmlIgnore]
        public bool SellerPropertyAcquisitionDateSpecified
        {
            get { return this.SellerPropertyAcquisitionDate != null; }
            set { }
        }
    
        [XmlElement("TitleHolderCount", Order = 4)]
        public MISMOCount TitleHolderCount { get; set; }
    
        [XmlIgnore]
        public bool TitleHolderCountSpecified
        {
            get { return this.TitleHolderCount != null; }
            set { }
        }
    
        [XmlElement("TitlePreliminaryReportDate", Order = 5)]
        public MISMODate TitlePreliminaryReportDate { get; set; }
    
        [XmlIgnore]
        public bool TitlePreliminaryReportDateSpecified
        {
            get { return this.TitlePreliminaryReportDate != null; }
            set { }
        }
    
        [XmlElement("TitleProcessInsuranceType", Order = 6)]
        public MISMOEnum<TitleProcessInsuranceBase> TitleProcessInsuranceType { get; set; }
    
        [XmlIgnore]
        public bool TitleProcessInsuranceTypeSpecified
        {
            get { return this.TitleProcessInsuranceType != null; }
            set { }
        }
    
        [XmlElement("TitleProcessType", Order = 7)]
        public MISMOEnum<TitleProcessBase> TitleProcessType { get; set; }
    
        [XmlIgnore]
        public bool TitleProcessTypeSpecified
        {
            get { return this.TitleProcessType != null; }
            set { }
        }
    
        [XmlElement("TitleProcessTypeOtherDescription", Order = 8)]
        public MISMOString TitleProcessTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool TitleProcessTypeOtherDescriptionSpecified
        {
            get { return this.TitleProcessTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("TitleReportItemsDescription", Order = 9)]
        public MISMOString TitleReportItemsDescription { get; set; }
    
        [XmlIgnore]
        public bool TitleReportItemsDescriptionSpecified
        {
            get { return this.TitleReportItemsDescription != null; }
            set { }
        }
    
        [XmlElement("TitleReportRequiredEndorsementsDescription", Order = 10)]
        public MISMOString TitleReportRequiredEndorsementsDescription { get; set; }
    
        [XmlIgnore]
        public bool TitleReportRequiredEndorsementsDescriptionSpecified
        {
            get { return this.TitleReportRequiredEndorsementsDescription != null; }
            set { }
        }
    
        [XmlElement("TitleTransferredPastSixMonthsIndicator", Order = 11)]
        public MISMOIndicator TitleTransferredPastSixMonthsIndicator { get; set; }
    
        [XmlIgnore]
        public bool TitleTransferredPastSixMonthsIndicatorSpecified
        {
            get { return this.TitleTransferredPastSixMonthsIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 12)]
        public PROPERTY_TITLE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
