namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CLOSING_INSTRUCTION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ClosingInstructionDetailSpecified
                    || this.ConditionsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CLOSING_INSTRUCTION_DETAIL", Order = 0)]
        public CLOSING_INSTRUCTION_DETAIL ClosingInstructionDetail { get; set; }
    
        [XmlIgnore]
        public bool ClosingInstructionDetailSpecified
        {
            get { return this.ClosingInstructionDetail != null && this.ClosingInstructionDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CONDITIONS", Order = 1)]
        public CONDITIONS Conditions { get; set; }
    
        [XmlIgnore]
        public bool ConditionsSpecified
        {
            get { return this.Conditions != null && this.Conditions.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public CLOSING_INSTRUCTION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
