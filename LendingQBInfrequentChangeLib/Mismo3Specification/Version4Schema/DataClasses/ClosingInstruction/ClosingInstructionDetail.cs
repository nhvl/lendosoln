namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CLOSING_INSTRUCTION_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ClosingInstructionsConsolidatedClosingConditionsDescriptionSpecified
                    || this.ClosingInstructionsPropertyTaxMessageDescriptionSpecified
                    || this.ClosingInstructionsTermiteReportRequiredIndicatorSpecified
                    || this.FundingCutoffTimeSpecified
                    || this.HoursDocumentsNeededPriorToDisbursementCountSpecified
                    || this.LeadBasedPaintCertificationRequiredIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ClosingInstructionsConsolidatedClosingConditionsDescription", Order = 0)]
        public MISMOString ClosingInstructionsConsolidatedClosingConditionsDescription { get; set; }
    
        [XmlIgnore]
        public bool ClosingInstructionsConsolidatedClosingConditionsDescriptionSpecified
        {
            get { return this.ClosingInstructionsConsolidatedClosingConditionsDescription != null; }
            set { }
        }
    
        [XmlElement("ClosingInstructionsPropertyTaxMessageDescription", Order = 1)]
        public MISMOString ClosingInstructionsPropertyTaxMessageDescription { get; set; }
    
        [XmlIgnore]
        public bool ClosingInstructionsPropertyTaxMessageDescriptionSpecified
        {
            get { return this.ClosingInstructionsPropertyTaxMessageDescription != null; }
            set { }
        }
    
        [XmlElement("ClosingInstructionsTermiteReportRequiredIndicator", Order = 2)]
        public MISMOIndicator ClosingInstructionsTermiteReportRequiredIndicator { get; set; }
    
        [XmlIgnore]
        public bool ClosingInstructionsTermiteReportRequiredIndicatorSpecified
        {
            get { return this.ClosingInstructionsTermiteReportRequiredIndicator != null; }
            set { }
        }
    
        [XmlElement("FundingCutoffTime", Order = 3)]
        public MISMOTime FundingCutoffTime { get; set; }
    
        [XmlIgnore]
        public bool FundingCutoffTimeSpecified
        {
            get { return this.FundingCutoffTime != null; }
            set { }
        }
    
        [XmlElement("HoursDocumentsNeededPriorToDisbursementCount", Order = 4)]
        public MISMOCount HoursDocumentsNeededPriorToDisbursementCount { get; set; }
    
        [XmlIgnore]
        public bool HoursDocumentsNeededPriorToDisbursementCountSpecified
        {
            get { return this.HoursDocumentsNeededPriorToDisbursementCount != null; }
            set { }
        }
    
        [XmlElement("LeadBasedPaintCertificationRequiredIndicator", Order = 5)]
        public MISMOIndicator LeadBasedPaintCertificationRequiredIndicator { get; set; }
    
        [XmlIgnore]
        public bool LeadBasedPaintCertificationRequiredIndicatorSpecified
        {
            get { return this.LeadBasedPaintCertificationRequiredIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 6)]
        public CLOSING_INSTRUCTION_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
