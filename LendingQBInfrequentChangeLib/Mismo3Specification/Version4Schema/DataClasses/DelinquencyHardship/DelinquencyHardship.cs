namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class DELINQUENCY_HARDSHIP
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.HardshipContactDateSpecified
                    || this.HardshipDurationTypeSpecified
                    || this.HardshipDurationTypeOtherDescriptionSpecified
                    || this.HardshipEndDateSpecified
                    || this.HardshipReasonCommentTextSpecified
                    || this.HardshipResolvedIndicatorSpecified
                    || this.HardshipStartDateSpecified
                    || this.LoanDelinquencyReasonTypeSpecified
                    || this.LoanDelinquencyReasonTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("HardshipContactDate", Order = 0)]
        public MISMODate HardshipContactDate { get; set; }
    
        [XmlIgnore]
        public bool HardshipContactDateSpecified
        {
            get { return this.HardshipContactDate != null; }
            set { }
        }
    
        [XmlElement("HardshipDurationType", Order = 1)]
        public MISMOEnum<HardshipDurationBase> HardshipDurationType { get; set; }
    
        [XmlIgnore]
        public bool HardshipDurationTypeSpecified
        {
            get { return this.HardshipDurationType != null; }
            set { }
        }
    
        [XmlElement("HardshipDurationTypeOtherDescription", Order = 2)]
        public MISMOString HardshipDurationTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool HardshipDurationTypeOtherDescriptionSpecified
        {
            get { return this.HardshipDurationTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("HardshipEndDate", Order = 3)]
        public MISMODate HardshipEndDate { get; set; }
    
        [XmlIgnore]
        public bool HardshipEndDateSpecified
        {
            get { return this.HardshipEndDate != null; }
            set { }
        }
    
        [XmlElement("HardshipReasonCommentText", Order = 4)]
        public MISMOString HardshipReasonCommentText { get; set; }
    
        [XmlIgnore]
        public bool HardshipReasonCommentTextSpecified
        {
            get { return this.HardshipReasonCommentText != null; }
            set { }
        }
    
        [XmlElement("HardshipResolvedIndicator", Order = 5)]
        public MISMOIndicator HardshipResolvedIndicator { get; set; }
    
        [XmlIgnore]
        public bool HardshipResolvedIndicatorSpecified
        {
            get { return this.HardshipResolvedIndicator != null; }
            set { }
        }
    
        [XmlElement("HardshipStartDate", Order = 6)]
        public MISMODate HardshipStartDate { get; set; }
    
        [XmlIgnore]
        public bool HardshipStartDateSpecified
        {
            get { return this.HardshipStartDate != null; }
            set { }
        }
    
        [XmlElement("LoanDelinquencyReasonType", Order = 7)]
        public MISMOEnum<LoanDelinquencyReasonBase> LoanDelinquencyReasonType { get; set; }
    
        [XmlIgnore]
        public bool LoanDelinquencyReasonTypeSpecified
        {
            get { return this.LoanDelinquencyReasonType != null; }
            set { }
        }
    
        [XmlElement("LoanDelinquencyReasonTypeOtherDescription", Order = 8)]
        public MISMOString LoanDelinquencyReasonTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool LoanDelinquencyReasonTypeOtherDescriptionSpecified
        {
            get { return this.LoanDelinquencyReasonTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 9)]
        public DELINQUENCY_HARDSHIP_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
