namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class DELINQUENCY_HARDSHIPS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DelinquencyHardshipListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DELINQUENCY_HARDSHIP", Order = 0)]
        public List<DELINQUENCY_HARDSHIP> DelinquencyHardshipList { get; set; } = new List<DELINQUENCY_HARDSHIP>();
    
        [XmlIgnore]
        public bool DelinquencyHardshipListSpecified
        {
            get { return this.DelinquencyHardshipList != null && this.DelinquencyHardshipList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public DELINQUENCY_HARDSHIPS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
