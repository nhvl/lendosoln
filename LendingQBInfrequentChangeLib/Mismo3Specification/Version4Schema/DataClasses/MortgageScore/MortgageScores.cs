namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class MORTGAGE_SCORES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MortgageScoreListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MORTGAGE_SCORE", Order = 0)]
        public List<MORTGAGE_SCORE> MortgageScoreList { get; set; } = new List<MORTGAGE_SCORE>();
    
        [XmlIgnore]
        public bool MortgageScoreListSpecified
        {
            get { return this.MortgageScoreList != null && this.MortgageScoreList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public MORTGAGE_SCORES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
