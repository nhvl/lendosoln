namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MORTGAGE_SCORE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MortgageScoreDateSpecified
                    || this.MortgageScoreTypeSpecified
                    || this.MortgageScoreTypeOtherDescriptionSpecified
                    || this.MortgageScoreValueSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MortgageScoreDate", Order = 0)]
        public MISMODate MortgageScoreDate { get; set; }
    
        [XmlIgnore]
        public bool MortgageScoreDateSpecified
        {
            get { return this.MortgageScoreDate != null; }
            set { }
        }
    
        [XmlElement("MortgageScoreType", Order = 1)]
        public MISMOEnum<MortgageScoreBase> MortgageScoreType { get; set; }
    
        [XmlIgnore]
        public bool MortgageScoreTypeSpecified
        {
            get { return this.MortgageScoreType != null; }
            set { }
        }
    
        [XmlElement("MortgageScoreTypeOtherDescription", Order = 2)]
        public MISMOString MortgageScoreTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MortgageScoreTypeOtherDescriptionSpecified
        {
            get { return this.MortgageScoreTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MortgageScoreValue", Order = 3)]
        public MISMOValue MortgageScoreValue { get; set; }
    
        [XmlIgnore]
        public bool MortgageScoreValueSpecified
        {
            get { return this.MortgageScoreValue != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public MORTGAGE_SCORE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
