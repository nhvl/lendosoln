namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class BUYDOWN_OCCURRENCES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BuydownOccurrenceListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BUYDOWN_OCCURRENCE", Order = 0)]
        public List<BUYDOWN_OCCURRENCE> BuydownOccurrenceList { get; set; } = new List<BUYDOWN_OCCURRENCE>();
    
        [XmlIgnore]
        public bool BuydownOccurrenceListSpecified
        {
            get { return this.BuydownOccurrenceList != null && this.BuydownOccurrenceList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public BUYDOWN_OCCURRENCES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
