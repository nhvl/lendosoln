namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class BUYDOWN_OCCURRENCE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BuydownInitialEffectiveInterestRatePercentSpecified
                    || this.BuydownOccurrenceEffectiveDateSpecified
                    || this.RemainingBuydownBalanceAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BuydownInitialEffectiveInterestRatePercent", Order = 0)]
        public MISMOPercent BuydownInitialEffectiveInterestRatePercent { get; set; }
    
        [XmlIgnore]
        public bool BuydownInitialEffectiveInterestRatePercentSpecified
        {
            get { return this.BuydownInitialEffectiveInterestRatePercent != null; }
            set { }
        }
    
        [XmlElement("BuydownOccurrenceEffectiveDate", Order = 1)]
        public MISMODate BuydownOccurrenceEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool BuydownOccurrenceEffectiveDateSpecified
        {
            get { return this.BuydownOccurrenceEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("RemainingBuydownBalanceAmount", Order = 2)]
        public MISMOAmount RemainingBuydownBalanceAmount { get; set; }
    
        [XmlIgnore]
        public bool RemainingBuydownBalanceAmountSpecified
        {
            get { return this.RemainingBuydownBalanceAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public BUYDOWN_OCCURRENCE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
