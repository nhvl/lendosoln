namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class TRANSFORM_PAGES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.TransformPageListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("TRANSFORM_PAGE", Order = 0)]
        public List<TRANSFORM_PAGE> TransformPageList { get; set; } = new List<TRANSFORM_PAGE>();
    
        [XmlIgnore]
        public bool TransformPageListSpecified
        {
            get { return this.TransformPageList != null && this.TransformPageList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public TRANSFORM_PAGES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
