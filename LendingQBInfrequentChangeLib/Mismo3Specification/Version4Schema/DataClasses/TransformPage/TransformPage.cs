namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class TRANSFORM_PAGE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.TransformPageFilesSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("TRANSFORM_PAGE_FILES", Order = 0)]
        public TRANSFORM_PAGE_FILES TransformPageFiles { get; set; }
    
        [XmlIgnore]
        public bool TransformPageFilesSpecified
        {
            get { return this.TransformPageFiles != null && this.TransformPageFiles.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public TRANSFORM_PAGE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
