namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MI_APPLICATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MIApplicationTypeSpecified
                    || this.MIApplicationTypeOtherDescriptionSpecified
                    || this.MICertificateIdentifierSpecified
                    || this.MILenderIdentifierSpecified
                    || this.MIServiceTypeSpecified
                    || this.MIServiceTypeOtherDescriptionSpecified
                    || this.MITransactionIdentifierSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MIApplicationType", Order = 0)]
        public MISMOEnum<MIApplicationBase> MIApplicationType { get; set; }
    
        [XmlIgnore]
        public bool MIApplicationTypeSpecified
        {
            get { return this.MIApplicationType != null; }
            set { }
        }
    
        [XmlElement("MIApplicationTypeOtherDescription", Order = 1)]
        public MISMOString MIApplicationTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MIApplicationTypeOtherDescriptionSpecified
        {
            get { return this.MIApplicationTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MICertificateIdentifier", Order = 2)]
        public MISMOIdentifier MICertificateIdentifier { get; set; }
    
        [XmlIgnore]
        public bool MICertificateIdentifierSpecified
        {
            get { return this.MICertificateIdentifier != null; }
            set { }
        }
    
        [XmlElement("MILenderIdentifier", Order = 3)]
        public MISMOIdentifier MILenderIdentifier { get; set; }
    
        [XmlIgnore]
        public bool MILenderIdentifierSpecified
        {
            get { return this.MILenderIdentifier != null; }
            set { }
        }
    
        [XmlElement("MIServiceType", Order = 4)]
        public MISMOEnum<MIServiceBase> MIServiceType { get; set; }
    
        [XmlIgnore]
        public bool MIServiceTypeSpecified
        {
            get { return this.MIServiceType != null; }
            set { }
        }
    
        [XmlElement("MIServiceTypeOtherDescription", Order = 5)]
        public MISMOString MIServiceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MIServiceTypeOtherDescriptionSpecified
        {
            get { return this.MIServiceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MITransactionIdentifier", Order = 6)]
        public MISMOIdentifier MITransactionIdentifier { get; set; }
    
        [XmlIgnore]
        public bool MITransactionIdentifierSpecified
        {
            get { return this.MITransactionIdentifier != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 7)]
        public MI_APPLICATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
