namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class REASON
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ReasonCodeSpecified
                    || this.ReasonCodeDescriptionSpecified
                    || this.ReasonCommentTextSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ReasonCode", Order = 0)]
        public MISMOCode ReasonCode { get; set; }
    
        [XmlIgnore]
        public bool ReasonCodeSpecified
        {
            get { return this.ReasonCode != null; }
            set { }
        }
    
        [XmlElement("ReasonCodeDescription", Order = 1)]
        public MISMOString ReasonCodeDescription { get; set; }
    
        [XmlIgnore]
        public bool ReasonCodeDescriptionSpecified
        {
            get { return this.ReasonCodeDescription != null; }
            set { }
        }
    
        [XmlElement("ReasonCommentText", Order = 2)]
        public MISMOString ReasonCommentText { get; set; }
    
        [XmlIgnore]
        public bool ReasonCommentTextSpecified
        {
            get { return this.ReasonCommentText != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public REASON_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
