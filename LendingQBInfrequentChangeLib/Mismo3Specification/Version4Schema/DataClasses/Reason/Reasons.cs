namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class REASONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ReasonListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("REASON", Order = 0)]
        public List<REASON> ReasonList { get; set; } = new List<REASON>();
    
        [XmlIgnore]
        public bool ReasonListSpecified
        {
            get { return this.ReasonList != null && this.ReasonList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public REASONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
