namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class INTEREST_CALCULATION_RULE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.InitialUnearnedInterestAmountSpecified
                    || this.InterestAccrualTypeSpecified
                    || this.InterestCalculationBasisDaysInPeriodTypeSpecified
                    || this.InterestCalculationBasisDaysInPeriodTypeOtherDescriptionSpecified
                    || this.InterestCalculationBasisDaysInYearCountTypeSpecified
                    || this.InterestCalculationBasisTypeSpecified
                    || this.InterestCalculationBasisTypeOtherDescriptionSpecified
                    || this.InterestCalculationEffectiveDateSpecified
                    || this.InterestCalculationEffectiveMonthsCountSpecified
                    || this.InterestCalculationExpirationDateSpecified
                    || this.InterestCalculationPeriodAdjustmentIndicatorSpecified
                    || this.InterestCalculationPeriodTypeSpecified
                    || this.InterestCalculationPurposeTypeSpecified
                    || this.InterestCalculationPurposeTypeOtherDescriptionSpecified
                    || this.InterestCalculationTypeSpecified
                    || this.InterestCalculationTypeOtherDescriptionSpecified
                    || this.InterestInAdvanceIndicatorSpecified
                    || this.LoanInterestAccrualStartDateSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("InitialUnearnedInterestAmount", Order = 0)]
        public MISMOAmount InitialUnearnedInterestAmount { get; set; }
    
        [XmlIgnore]
        public bool InitialUnearnedInterestAmountSpecified
        {
            get { return this.InitialUnearnedInterestAmount != null; }
            set { }
        }
    
        [XmlElement("InterestAccrualType", Order = 1)]
        public MISMOEnum<InterestAccrualBase> InterestAccrualType { get; set; }
    
        [XmlIgnore]
        public bool InterestAccrualTypeSpecified
        {
            get { return this.InterestAccrualType != null; }
            set { }
        }
    
        [XmlElement("InterestCalculationBasisDaysInPeriodType", Order = 2)]
        public MISMOEnum<InterestCalculationBasisDaysInPeriodBase> InterestCalculationBasisDaysInPeriodType { get; set; }
    
        [XmlIgnore]
        public bool InterestCalculationBasisDaysInPeriodTypeSpecified
        {
            get { return this.InterestCalculationBasisDaysInPeriodType != null; }
            set { }
        }
    
        [XmlElement("InterestCalculationBasisDaysInPeriodTypeOtherDescription", Order = 3)]
        public MISMOString InterestCalculationBasisDaysInPeriodTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool InterestCalculationBasisDaysInPeriodTypeOtherDescriptionSpecified
        {
            get { return this.InterestCalculationBasisDaysInPeriodTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("InterestCalculationBasisDaysInYearCountType", Order = 4)]
        public MISMOEnum<InterestCalculationBasisDaysInYearCountBase> InterestCalculationBasisDaysInYearCountType { get; set; }
    
        [XmlIgnore]
        public bool InterestCalculationBasisDaysInYearCountTypeSpecified
        {
            get { return this.InterestCalculationBasisDaysInYearCountType != null; }
            set { }
        }
    
        [XmlElement("InterestCalculationBasisType", Order = 5)]
        public MISMOEnum<InterestCalculationBasisBase> InterestCalculationBasisType { get; set; }
    
        [XmlIgnore]
        public bool InterestCalculationBasisTypeSpecified
        {
            get { return this.InterestCalculationBasisType != null; }
            set { }
        }
    
        [XmlElement("InterestCalculationBasisTypeOtherDescription", Order = 6)]
        public MISMOString InterestCalculationBasisTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool InterestCalculationBasisTypeOtherDescriptionSpecified
        {
            get { return this.InterestCalculationBasisTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("InterestCalculationEffectiveDate", Order = 7)]
        public MISMODate InterestCalculationEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool InterestCalculationEffectiveDateSpecified
        {
            get { return this.InterestCalculationEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("InterestCalculationEffectiveMonthsCount", Order = 8)]
        public MISMOCount InterestCalculationEffectiveMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool InterestCalculationEffectiveMonthsCountSpecified
        {
            get { return this.InterestCalculationEffectiveMonthsCount != null; }
            set { }
        }
    
        [XmlElement("InterestCalculationExpirationDate", Order = 9)]
        public MISMODate InterestCalculationExpirationDate { get; set; }
    
        [XmlIgnore]
        public bool InterestCalculationExpirationDateSpecified
        {
            get { return this.InterestCalculationExpirationDate != null; }
            set { }
        }
    
        [XmlElement("InterestCalculationPeriodAdjustmentIndicator", Order = 10)]
        public MISMOIndicator InterestCalculationPeriodAdjustmentIndicator { get; set; }
    
        [XmlIgnore]
        public bool InterestCalculationPeriodAdjustmentIndicatorSpecified
        {
            get { return this.InterestCalculationPeriodAdjustmentIndicator != null; }
            set { }
        }
    
        [XmlElement("InterestCalculationPeriodType", Order = 11)]
        public MISMOEnum<InterestCalculationPeriodBase> InterestCalculationPeriodType { get; set; }
    
        [XmlIgnore]
        public bool InterestCalculationPeriodTypeSpecified
        {
            get { return this.InterestCalculationPeriodType != null; }
            set { }
        }
    
        [XmlElement("InterestCalculationPurposeType", Order = 12)]
        public MISMOEnum<InterestCalculationPurposeBase> InterestCalculationPurposeType { get; set; }
    
        [XmlIgnore]
        public bool InterestCalculationPurposeTypeSpecified
        {
            get { return this.InterestCalculationPurposeType != null; }
            set { }
        }
    
        [XmlElement("InterestCalculationPurposeTypeOtherDescription", Order = 13)]
        public MISMOString InterestCalculationPurposeTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool InterestCalculationPurposeTypeOtherDescriptionSpecified
        {
            get { return this.InterestCalculationPurposeTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("InterestCalculationType", Order = 14)]
        public MISMOEnum<InterestCalculationBase> InterestCalculationType { get; set; }
    
        [XmlIgnore]
        public bool InterestCalculationTypeSpecified
        {
            get { return this.InterestCalculationType != null; }
            set { }
        }
    
        [XmlElement("InterestCalculationTypeOtherDescription", Order = 15)]
        public MISMOString InterestCalculationTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool InterestCalculationTypeOtherDescriptionSpecified
        {
            get { return this.InterestCalculationTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("InterestInAdvanceIndicator", Order = 16)]
        public MISMOIndicator InterestInAdvanceIndicator { get; set; }
    
        [XmlIgnore]
        public bool InterestInAdvanceIndicatorSpecified
        {
            get { return this.InterestInAdvanceIndicator != null; }
            set { }
        }
    
        [XmlElement("LoanInterestAccrualStartDate", Order = 17)]
        public MISMODate LoanInterestAccrualStartDate { get; set; }
    
        [XmlIgnore]
        public bool LoanInterestAccrualStartDateSpecified
        {
            get { return this.LoanInterestAccrualStartDate != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 18)]
        public INTEREST_CALCULATION_RULE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
