namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class INTEREST_CALCULATION_RULES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.InterestCalculationRuleListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("INTEREST_CALCULATION_RULE", Order = 0)]
        public List<INTEREST_CALCULATION_RULE> InterestCalculationRuleList { get; set; } = new List<INTEREST_CALCULATION_RULE>();
    
        [XmlIgnore]
        public bool InterestCalculationRuleListSpecified
        {
            get { return this.InterestCalculationRuleList != null && this.InterestCalculationRuleList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public INTEREST_CALCULATION_RULES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
