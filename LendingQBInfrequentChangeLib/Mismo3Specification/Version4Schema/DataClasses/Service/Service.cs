namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Xml.Schema;
    using System.Xml.Serialization;

    public class SERVICE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                if (Mismo3Utilities.ExceedsThreshold(1,
                    new List<bool> {
                        this.AutomatedUnderwritingSystemSpecified,
                        this.ClaimSpecified,
                        this.ClosingSpecified,
                        this.CreditSpecified,
                        this.DataChangeSpecified,
                        this.DocumentManagementSpecified,
                        this.FloodSpecified,
                        this.FraudSpecified,
                        this.InspectionServiceSpecified,
                        this.LoanDeliverySpecified,
                        this.MersServiceSpecified,
                        this.MiSpecified,
                        this.PriaSpecified,
                        this.TaxSpecified,
                        this.TitleSpecified,
                        this.ValuationSpecified,
                        this.VerificationOfIncomeSpecified }))
                {
                    throw new System.Xml.Schema.XmlSchemaValidationException(
                        Mismo3Utilities.GetXSDChoiceExceptionMessage(
                        "SERVICE",
                        new List<string> {
                            "AUTOMATED_UNDERWRITING_SYSTEM",
                            "CLAIM",
                            "CLOSING",
                            "CREDIT",
                            "DATA_CHANGE",
                            "DOCUMENT_MANAGEMENT",
                            "FLOOD",
                            "FRAUD",
                            "INSPECTION_SERVICE",
                            "LOAN_DELIVERY",
                            "MERS_SERVICE",
                            "MI",
                            "PRIA",
                            "TAX",
                            "TITLE",
                            "VALUATION",
                            "VERIFICATION_OF_INCOME"}));
                }

                return this.AutomatedUnderwritingSystemSpecified
                    || this.ClaimSpecified
                    || this.ClosingSpecified
                    || this.CreditSpecified
                    || this.DataChangeSpecified
                    || this.DocumentManagementSpecified
                    || this.FloodSpecified
                    || this.FraudSpecified
                    || this.InspectionServiceSpecified
                    || this.LoanDeliverySpecified
                    || this.MersServiceSpecified
                    || this.MiSpecified
                    || this.PriaSpecified
                    || this.TaxSpecified
                    || this.TitleSpecified
                    || this.ValuationSpecified
                    || this.VerificationOfIncomeSpecified
                    || this.ErrorsSpecified
                    || this.ReasonsSpecified
                    || this.RelationshipsSpecified
                    || this.ReportingInformationSpecified
                    || this.ServicePaymentsSpecified
                    || this.ServiceProductSpecified
                    || this.ServiceProductFulfillmentSpecified
                    || this.StatusesSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AUTOMATED_UNDERWRITING_SYSTEM", Order = 0)]
        public AUTOMATED_UNDERWRITING_SYSTEM AutomatedUnderwritingSystem { get; set; }
    
        [XmlIgnore]
        public bool AutomatedUnderwritingSystemSpecified
        {
            get { return this.AutomatedUnderwritingSystem != null && this.AutomatedUnderwritingSystem.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CLAIM", Order = 1)]
        public CLAIM Claim { get; set; }
    
        [XmlIgnore]
        public bool ClaimSpecified
        {
            get { return this.Claim != null && this.Claim.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CLOSING", Order = 2)]
        public CLOSING Closing { get; set; }
    
        [XmlIgnore]
        public bool ClosingSpecified
        {
            get { return this.Closing != null && this.Closing.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT", Order = 3)]
        public CREDIT Credit { get; set; }
    
        [XmlIgnore]
        public bool CreditSpecified
        {
            get { return this.Credit != null && this.Credit.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("DATA_CHANGE", Order = 4)]
        public DATA_CHANGE DataChange { get; set; }
    
        [XmlIgnore]
        public bool DataChangeSpecified
        {
            get { return this.DataChange != null && this.DataChange.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("DOCUMENT_MANAGEMENT", Order = 5)]
        public DOCUMENT_MANAGEMENT DocumentManagement { get; set; }
    
        [XmlIgnore]
        public bool DocumentManagementSpecified
        {
            get { return this.DocumentManagement != null && this.DocumentManagement.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("FLOOD", Order = 6)]
        public FLOOD Flood { get; set; }
    
        [XmlIgnore]
        public bool FloodSpecified
        {
            get { return this.Flood != null && this.Flood.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("FRAUD", Order = 7)]
        public FRAUD Fraud { get; set; }
    
        [XmlIgnore]
        public bool FraudSpecified
        {
            get { return this.Fraud != null && this.Fraud.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("INSPECTION_SERVICE", Order = 8)]
        public INSPECTION_SERVICE InspectionService { get; set; }
    
        [XmlIgnore]
        public bool InspectionServiceSpecified
        {
            get { return this.InspectionService != null && this.InspectionService.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("LOAN_DELIVERY", Order = 9)]
        public LOAN_DELIVERY LoanDelivery { get; set; }
    
        [XmlIgnore]
        public bool LoanDeliverySpecified
        {
            get { return this.LoanDelivery != null && this.LoanDelivery.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("MERS_SERVICE", Order = 10)]
        public MERS_SERVICE MersService { get; set; }
    
        [XmlIgnore]
        public bool MersServiceSpecified
        {
            get { return this.MersService != null && this.MersService.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("MI", Order = 11)]
        public MI Mi { get; set; }
    
        [XmlIgnore]
        public bool MiSpecified
        {
            get { return this.Mi != null && this.Mi.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PRIA", Order = 12)]
        public PRIA Pria { get; set; }
    
        [XmlIgnore]
        public bool PriaSpecified
        {
            get { return this.Pria != null && this.Pria.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("TAX", Order = 13)]
        public TAX Tax { get; set; }
    
        [XmlIgnore]
        public bool TaxSpecified
        {
            get { return this.Tax != null && this.Tax.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("TITLE", Order = 14)]
        public TITLE Title { get; set; }
    
        [XmlIgnore]
        public bool TitleSpecified
        {
            get { return this.Title != null && this.Title.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("VALUATION", Order = 15)]
        public VALUATION Valuation { get; set; }
    
        [XmlIgnore]
        public bool ValuationSpecified
        {
            get { return this.Valuation != null && this.Valuation.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("VERIFICATION_OF_INCOME", Order = 16)]
        public VERIFICATION_OF_INCOME VerificationOfIncome { get; set; }
    
        [XmlIgnore]
        public bool VerificationOfIncomeSpecified
        {
            get { return this.VerificationOfIncome != null && this.VerificationOfIncome.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("ERRORS", Order = 19)]
        public ERRORS Errors { get; set; }
    
        [XmlIgnore]
        public bool ErrorsSpecified
        {
            get { return this.Errors != null && this.Errors.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("REASONS", Order = 20)]
        public REASONS Reasons { get; set; }
    
        [XmlIgnore]
        public bool ReasonsSpecified
        {
            get { return this.Reasons != null && this.Reasons.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("RELATIONSHIPS", Order = 21)]
        public RELATIONSHIPS Relationships { get; set; }
    
        [XmlIgnore]
        public bool RelationshipsSpecified
        {
            get { return this.Relationships != null && this.Relationships.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("REPORTING_INFORMATION", Order = 22)]
        public REPORTING_INFORMATION ReportingInformation { get; set; }
    
        [XmlIgnore]
        public bool ReportingInformationSpecified
        {
            get { return this.ReportingInformation != null && this.ReportingInformation.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SERVICE_PAYMENTS", Order = 23)]
        public SERVICE_PAYMENTS ServicePayments { get; set; }
    
        [XmlIgnore]
        public bool ServicePaymentsSpecified
        {
            get { return this.ServicePayments != null && this.ServicePayments.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SERVICE_PRODUCT", Order = 24)]
        public SERVICE_PRODUCT ServiceProduct { get; set; }
    
        [XmlIgnore]
        public bool ServiceProductSpecified
        {
            get { return this.ServiceProduct != null && this.ServiceProduct.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SERVICE_PRODUCT_FULFILLMENT", Order = 25)]
        public SERVICE_PRODUCT_FULFILLMENT ServiceProductFulfillment { get; set; }
    
        [XmlIgnore]
        public bool ServiceProductFulfillmentSpecified
        {
            get { return this.ServiceProductFulfillment != null && this.ServiceProductFulfillment.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("STATUSES", Order = 26)]
        public STATUSES Statuses { get; set; }
    
        [XmlIgnore]
        public bool StatusesSpecified
        {
            get { return this.Statuses != null && this.Statuses.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 27)]
        public SERVICE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }

        [XmlAttribute("label", Form = XmlSchemaForm.Qualified, Namespace = Mismo3Constants.XlinkNamespace)]
        public string XlinkLabel { get; set; }

        [XmlIgnore]
        public bool XlinkLabelSpecified
        {
            get { return !string.IsNullOrEmpty(this.XlinkLabel); }
            set { }
        }
    }
}
