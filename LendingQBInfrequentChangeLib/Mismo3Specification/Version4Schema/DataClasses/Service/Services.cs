namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class SERVICES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ForeignObjectsSpecified
                    || this.ServiceListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("FOREIGN_OBJECTS", Order = 0)]
        public FOREIGN_OBJECTS ForeignObjects { get; set; }
    
        [XmlIgnore]
        public bool ForeignObjectsSpecified
        {
            get { return this.ForeignObjects != null && this.ForeignObjects.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("SERVICE", Order = 1)]
        public List<SERVICE> ServiceList { get; set; } = new List<SERVICE>();
    
        [XmlIgnore]
        public bool ServiceListSpecified
        {
            get { return this.ServiceList != null && this.ServiceList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public SERVICES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
