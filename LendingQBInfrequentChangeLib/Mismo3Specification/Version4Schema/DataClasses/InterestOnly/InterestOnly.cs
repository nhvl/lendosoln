namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class INTEREST_ONLY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.InterestOnlyEndDateSpecified
                    || this.InterestOnlyMonthlyPaymentAmountSpecified
                    || this.InterestOnlyTermMonthsCountSpecified
                    || this.InterestOnlyTermPaymentsCountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("InterestOnlyEndDate", Order = 0)]
        public MISMODate InterestOnlyEndDate { get; set; }
    
        [XmlIgnore]
        public bool InterestOnlyEndDateSpecified
        {
            get { return this.InterestOnlyEndDate != null; }
            set { }
        }
    
        [XmlElement("InterestOnlyMonthlyPaymentAmount", Order = 1)]
        public MISMOAmount InterestOnlyMonthlyPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool InterestOnlyMonthlyPaymentAmountSpecified
        {
            get { return this.InterestOnlyMonthlyPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("InterestOnlyTermMonthsCount", Order = 2)]
        public MISMOCount InterestOnlyTermMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool InterestOnlyTermMonthsCountSpecified
        {
            get { return this.InterestOnlyTermMonthsCount != null; }
            set { }
        }
    
        [XmlElement("InterestOnlyTermPaymentsCount", Order = 3)]
        public MISMOCount InterestOnlyTermPaymentsCount { get; set; }
    
        [XmlIgnore]
        public bool InterestOnlyTermPaymentsCountSpecified
        {
            get { return this.InterestOnlyTermPaymentsCount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public INTEREST_ONLY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
