namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class DEAL_SET_EXPENSE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DealSetExpenseAmountSpecified
                    || this.DealSetExpenseTypeSpecified
                    || this.DealSetExpenseTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DealSetExpenseAmount", Order = 0)]
        public MISMOAmount DealSetExpenseAmount { get; set; }
    
        [XmlIgnore]
        public bool DealSetExpenseAmountSpecified
        {
            get { return this.DealSetExpenseAmount != null; }
            set { }
        }
    
        [XmlElement("DealSetExpenseType", Order = 1)]
        public MISMOEnum<DealSetExpenseBase> DealSetExpenseType { get; set; }
    
        [XmlIgnore]
        public bool DealSetExpenseTypeSpecified
        {
            get { return this.DealSetExpenseType != null; }
            set { }
        }
    
        [XmlElement("DealSetExpenseTypeOtherDescription", Order = 2)]
        public MISMOString DealSetExpenseTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool DealSetExpenseTypeOtherDescriptionSpecified
        {
            get { return this.DealSetExpenseTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public DEAL_SET_EXPENSE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
