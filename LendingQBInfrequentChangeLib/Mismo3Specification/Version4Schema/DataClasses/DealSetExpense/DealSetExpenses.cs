namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class DEAL_SET_EXPENSES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DealSetExpenseListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DEAL_SET_EXPENSE", Order = 0)]
        public List<DEAL_SET_EXPENSE> DealSetExpenseList { get; set; } = new List<DEAL_SET_EXPENSE>();
    
        [XmlIgnore]
        public bool DealSetExpenseListSpecified
        {
            get { return this.DealSetExpenseList != null && this.DealSetExpenseList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public DEAL_SET_EXPENSES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
