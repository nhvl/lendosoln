namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PROPERTY_OWNER
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.OwnershipPercentSpecified
                    || this.PropertyOwnerStatusTypeSpecified
                    || this.RelationshipVestingTypeSpecified
                    || this.RelationshipVestingTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("OwnershipPercent", Order = 0)]
        public MISMOPercent OwnershipPercent { get; set; }
    
        [XmlIgnore]
        public bool OwnershipPercentSpecified
        {
            get { return this.OwnershipPercent != null; }
            set { }
        }
    
        [XmlElement("PropertyOwnerStatusType", Order = 1)]
        public MISMOEnum<PropertyOwnerStatusBase> PropertyOwnerStatusType { get; set; }
    
        [XmlIgnore]
        public bool PropertyOwnerStatusTypeSpecified
        {
            get { return this.PropertyOwnerStatusType != null; }
            set { }
        }
    
        [XmlElement("RelationshipVestingType", Order = 2)]
        public MISMOEnum<RelationshipVestingBase> RelationshipVestingType { get; set; }
    
        [XmlIgnore]
        public bool RelationshipVestingTypeSpecified
        {
            get { return this.RelationshipVestingType != null; }
            set { }
        }
    
        [XmlElement("RelationshipVestingTypeOtherDescription", Order = 3)]
        public MISMOString RelationshipVestingTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool RelationshipVestingTypeOtherDescriptionSpecified
        {
            get { return this.RelationshipVestingTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public PROPERTY_OWNER_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
