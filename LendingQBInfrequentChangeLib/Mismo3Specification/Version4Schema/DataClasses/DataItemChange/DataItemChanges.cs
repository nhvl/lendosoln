namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class DATA_ITEM_CHANGES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DataItemChangeListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DATA_ITEM_CHANGE", Order = 0)]
        public List<DATA_ITEM_CHANGE> DataItemChangeList { get; set; } = new List<DATA_ITEM_CHANGE>();
    
        [XmlIgnore]
        public bool DataItemChangeListSpecified
        {
            get { return this.DataItemChangeList != null && this.DataItemChangeList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public DATA_ITEM_CHANGES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
