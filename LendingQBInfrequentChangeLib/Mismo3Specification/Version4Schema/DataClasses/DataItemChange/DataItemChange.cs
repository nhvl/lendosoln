namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class DATA_ITEM_CHANGE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DataItemChangeEffectiveDateSpecified
                    || this.DataItemChangeExpirationDateSpecified
                    || this.DataItemChangePriorValueSpecified
                    || this.DataItemChangeTypeSpecified
                    || this.DataItemChangeValueSpecified
                    || this.DataItemChangeXPathSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DataItemChangeEffectiveDate", Order = 0)]
        public MISMODate DataItemChangeEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool DataItemChangeEffectiveDateSpecified
        {
            get { return this.DataItemChangeEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("DataItemChangeExpirationDate", Order = 1)]
        public MISMODate DataItemChangeExpirationDate { get; set; }
    
        [XmlIgnore]
        public bool DataItemChangeExpirationDateSpecified
        {
            get { return this.DataItemChangeExpirationDate != null; }
            set { }
        }
    
        [XmlElement("DataItemChangePriorValue", Order = 2)]
        public MISMOValue DataItemChangePriorValue { get; set; }
    
        [XmlIgnore]
        public bool DataItemChangePriorValueSpecified
        {
            get { return this.DataItemChangePriorValue != null; }
            set { }
        }
    
        [XmlElement("DataItemChangeType", Order = 3)]
        public MISMOEnum<DataItemChangeBase> DataItemChangeType { get; set; }
    
        [XmlIgnore]
        public bool DataItemChangeTypeSpecified
        {
            get { return this.DataItemChangeType != null; }
            set { }
        }
    
        [XmlElement("DataItemChangeValue", Order = 4)]
        public MISMOValue DataItemChangeValue { get; set; }
    
        [XmlIgnore]
        public bool DataItemChangeValueSpecified
        {
            get { return this.DataItemChangeValue != null; }
            set { }
        }
    
        [XmlElement("DataItemChangeXPath", Order = 5)]
        public MISMOXPath DataItemChangeXPath { get; set; }
    
        [XmlIgnore]
        public bool DataItemChangeXPathSpecified
        {
            get { return this.DataItemChangeXPath != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 6)]
        public DATA_ITEM_CHANGE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
