namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class LOAN_PRICE_QUOTE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LoanPriceLineItemsSpecified
                    || this.LoanPriceQuoteDetailSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LOAN_PRICE_LINE_ITEMS", Order = 0)]
        public LOAN_PRICE_LINE_ITEMS LoanPriceLineItems { get; set; }
    
        [XmlIgnore]
        public bool LoanPriceLineItemsSpecified
        {
            get { return this.LoanPriceLineItems != null && this.LoanPriceLineItems.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("LOAN_PRICE_QUOTE_DETAIL", Order = 1)]
        public LOAN_PRICE_QUOTE_DETAIL LoanPriceQuoteDetail { get; set; }
    
        [XmlIgnore]
        public bool LoanPriceQuoteDetailSpecified
        {
            get { return this.LoanPriceQuoteDetail != null && this.LoanPriceQuoteDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public LOAN_PRICE_QUOTE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
