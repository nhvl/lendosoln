namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class LOAN_PRICE_QUOTE_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.FinancingStructureDescriptionSpecified
                    || this.LoanAllInPricePercentSpecified
                    || this.LoanBasePricePercentSpecified
                    || this.LoanPricePercentFormatTypeSpecified
                    || this.LoanPriceQuoteDatetimeSpecified
                    || this.LoanPriceQuoteExpirationDatetimeSpecified
                    || this.LoanPriceQuoteIdentifierSpecified
                    || this.LoanPriceQuoteInterestRatePercentSpecified
                    || this.LoanPriceQuoteTypeSpecified
                    || this.LoanPriceQuoteTypeOtherDescriptionSpecified
                    || this.RateLockTypeSpecified
                    || this.ServicingReleasePremiumPercentSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("FinancingStructureDescription", Order = 0)]
        public MISMOString FinancingStructureDescription { get; set; }
    
        [XmlIgnore]
        public bool FinancingStructureDescriptionSpecified
        {
            get { return this.FinancingStructureDescription != null; }
            set { }
        }
    
        [XmlElement("LoanAllInPricePercent", Order = 1)]
        public MISMOPercent LoanAllInPricePercent { get; set; }
    
        [XmlIgnore]
        public bool LoanAllInPricePercentSpecified
        {
            get { return this.LoanAllInPricePercent != null; }
            set { }
        }
    
        [XmlElement("LoanBasePricePercent", Order = 2)]
        public MISMOPercent LoanBasePricePercent { get; set; }
    
        [XmlIgnore]
        public bool LoanBasePricePercentSpecified
        {
            get { return this.LoanBasePricePercent != null; }
            set { }
        }
    
        [XmlElement("LoanPricePercentFormatType", Order = 3)]
        public MISMOEnum<LoanPricePercentFormatBase> LoanPricePercentFormatType { get; set; }
    
        [XmlIgnore]
        public bool LoanPricePercentFormatTypeSpecified
        {
            get { return this.LoanPricePercentFormatType != null; }
            set { }
        }
    
        [XmlElement("LoanPriceQuoteDatetime", Order = 4)]
        public MISMODatetime LoanPriceQuoteDatetime { get; set; }
    
        [XmlIgnore]
        public bool LoanPriceQuoteDatetimeSpecified
        {
            get { return this.LoanPriceQuoteDatetime != null; }
            set { }
        }
    
        [XmlElement("LoanPriceQuoteExpirationDatetime", Order = 5)]
        public MISMODatetime LoanPriceQuoteExpirationDatetime { get; set; }
    
        [XmlIgnore]
        public bool LoanPriceQuoteExpirationDatetimeSpecified
        {
            get { return this.LoanPriceQuoteExpirationDatetime != null; }
            set { }
        }
    
        [XmlElement("LoanPriceQuoteIdentifier", Order = 6)]
        public MISMOIdentifier LoanPriceQuoteIdentifier { get; set; }
    
        [XmlIgnore]
        public bool LoanPriceQuoteIdentifierSpecified
        {
            get { return this.LoanPriceQuoteIdentifier != null; }
            set { }
        }
    
        [XmlElement("LoanPriceQuoteInterestRatePercent", Order = 7)]
        public MISMOPercent LoanPriceQuoteInterestRatePercent { get; set; }
    
        [XmlIgnore]
        public bool LoanPriceQuoteInterestRatePercentSpecified
        {
            get { return this.LoanPriceQuoteInterestRatePercent != null; }
            set { }
        }
    
        [XmlElement("LoanPriceQuoteType", Order = 8)]
        public MISMOEnum<LoanPriceQuoteBase> LoanPriceQuoteType { get; set; }
    
        [XmlIgnore]
        public bool LoanPriceQuoteTypeSpecified
        {
            get { return this.LoanPriceQuoteType != null; }
            set { }
        }
    
        [XmlElement("LoanPriceQuoteTypeOtherDescription", Order = 9)]
        public MISMOString LoanPriceQuoteTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool LoanPriceQuoteTypeOtherDescriptionSpecified
        {
            get { return this.LoanPriceQuoteTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("RateLockType", Order = 10)]
        public MISMOEnum<RateLockBase> RateLockType { get; set; }
    
        [XmlIgnore]
        public bool RateLockTypeSpecified
        {
            get { return this.RateLockType != null; }
            set { }
        }
    
        [XmlElement("ServicingReleasePremiumPercent", Order = 11)]
        public MISMOPercent ServicingReleasePremiumPercent { get; set; }
    
        [XmlIgnore]
        public bool ServicingReleasePremiumPercentSpecified
        {
            get { return this.ServicingReleasePremiumPercent != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 12)]
        public LOAN_PRICE_QUOTE_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
