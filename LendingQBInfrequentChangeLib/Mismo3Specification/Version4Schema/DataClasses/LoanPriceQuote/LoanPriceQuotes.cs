namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class LOAN_PRICE_QUOTES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LoanPriceQuoteListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LOAN_PRICE_QUOTE", Order = 0)]
        public List<LOAN_PRICE_QUOTE> LoanPriceQuoteList { get; set; } = new List<LOAN_PRICE_QUOTE>();
    
        [XmlIgnore]
        public bool LoanPriceQuoteListSpecified
        {
            get { return this.LoanPriceQuoteList != null && this.LoanPriceQuoteList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public LOAN_PRICE_QUOTES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
