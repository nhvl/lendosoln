namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class DATA_VERSIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DataVersionListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DATA_VERSION", Order = 0)]
        public List<DATA_VERSION> DataVersionList { get; set; } = new List<DATA_VERSION>();
    
        [XmlIgnore]
        public bool DataVersionListSpecified
        {
            get { return this.DataVersionList != null && this.DataVersionList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public DATA_VERSIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
