namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class CLOSING_INFORMATION_REVISIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ClosingInformationRevisionListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CLOSING_INFORMATION_REVISION", Order = 0)]
        public List<CLOSING_INFORMATION_REVISION> ClosingInformationRevisionList { get; set; } = new List<CLOSING_INFORMATION_REVISION>();
    
        [XmlIgnore]
        public bool ClosingInformationRevisionListSpecified
        {
            get { return this.ClosingInformationRevisionList != null && this.ClosingInformationRevisionList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public CLOSING_INFORMATION_REVISIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
