namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CLOSING_INFORMATION_REVISION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ClosingInformationRevisionInstructionsTextSpecified
                    || this.ClosingInformationRevisionReasonTypeSpecified
                    || this.ClosingInformationRevisionReasonTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ClosingInformationRevisionInstructionsText", Order = 0)]
        public MISMOString ClosingInformationRevisionInstructionsText { get; set; }
    
        [XmlIgnore]
        public bool ClosingInformationRevisionInstructionsTextSpecified
        {
            get { return this.ClosingInformationRevisionInstructionsText != null; }
            set { }
        }
    
        [XmlElement("ClosingInformationRevisionReasonType", Order = 1)]
        public MISMOEnum<ClosingInformationRevisionReasonBase> ClosingInformationRevisionReasonType { get; set; }
    
        [XmlIgnore]
        public bool ClosingInformationRevisionReasonTypeSpecified
        {
            get { return this.ClosingInformationRevisionReasonType != null; }
            set { }
        }
    
        [XmlElement("ClosingInformationRevisionReasonTypeOtherDescription", Order = 2)]
        public MISMOString ClosingInformationRevisionReasonTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ClosingInformationRevisionReasonTypeOtherDescriptionSpecified
        {
            get { return this.ClosingInformationRevisionReasonTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public CLOSING_INFORMATION_REVISION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
