namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class INDIVIDUAL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AliasesSpecified
                    || this.ContactPointsSpecified
                    || this.IdentificationVerificationSpecified
                    || this.NameSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ALIASES", Order = 0)]
        public ALIASES Aliases { get; set; }
    
        [XmlIgnore]
        public bool AliasesSpecified
        {
            get { return this.Aliases != null && this.Aliases.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CONTACT_POINTS", Order = 1)]
        public CONTACT_POINTS ContactPoints { get; set; }
    
        [XmlIgnore]
        public bool ContactPointsSpecified
        {
            get { return this.ContactPoints != null && this.ContactPoints.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("IDENTIFICATION_VERIFICATION", Order = 2)]
        public IDENTIFICATION_VERIFICATION IdentificationVerification { get; set; }
    
        [XmlIgnore]
        public bool IdentificationVerificationSpecified
        {
            get { return this.IdentificationVerification != null && this.IdentificationVerification.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("NAME", Order = 3)]
        public NAME Name { get; set; }
    
        [XmlIgnore]
        public bool NameSpecified
        {
            get { return this.Name != null && this.Name.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public INDIVIDUAL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
