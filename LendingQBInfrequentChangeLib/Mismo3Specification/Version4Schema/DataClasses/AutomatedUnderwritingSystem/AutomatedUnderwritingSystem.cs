namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class AUTOMATED_UNDERWRITING_SYSTEM
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AutomatedUnderwritingSystemRequestSpecified
                    || this.AutomatedUnderwritingSystemResponseSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AUTOMATED_UNDERWRITING_SYSTEM_REQUEST", Order = 0)]
        public AUTOMATED_UNDERWRITING_SYSTEM_REQUEST AutomatedUnderwritingSystemRequest { get; set; }
    
        [XmlIgnore]
        public bool AutomatedUnderwritingSystemRequestSpecified
        {
            get { return this.AutomatedUnderwritingSystemRequest != null && this.AutomatedUnderwritingSystemRequest.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("AUTOMATED_UNDERWRITING_SYSTEM_RESPONSE", Order = 1)]
        public AUTOMATED_UNDERWRITING_SYSTEM_RESPONSE AutomatedUnderwritingSystemResponse { get; set; }
    
        [XmlIgnore]
        public bool AutomatedUnderwritingSystemResponseSpecified
        {
            get { return this.AutomatedUnderwritingSystemResponse != null && this.AutomatedUnderwritingSystemResponse.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public AUTOMATED_UNDERWRITING_SYSTEM_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
