namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class FEE_SUMMARY_TOTAL_FEES_PAID_BY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.FeeSummaryTotalFeesPaidByAmountSpecified
                    || this.FeeSummaryTotalFeesPaidByTypeSpecified
                    || this.FeeSummaryTotalFeesPaidByTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("FeeSummaryTotalFeesPaidByAmount", Order = 0)]
        public MISMOAmount FeeSummaryTotalFeesPaidByAmount { get; set; }
    
        [XmlIgnore]
        public bool FeeSummaryTotalFeesPaidByAmountSpecified
        {
            get { return this.FeeSummaryTotalFeesPaidByAmount != null; }
            set { }
        }
    
        [XmlElement("FeeSummaryTotalFeesPaidByType", Order = 1)]
        public MISMOEnum<FeeSummaryTotalFeesPaidByBase> FeeSummaryTotalFeesPaidByType { get; set; }
    
        [XmlIgnore]
        public bool FeeSummaryTotalFeesPaidByTypeSpecified
        {
            get { return this.FeeSummaryTotalFeesPaidByType != null; }
            set { }
        }
    
        [XmlElement("FeeSummaryTotalFeesPaidByTypeOtherDescription", Order = 2)]
        public MISMOString FeeSummaryTotalFeesPaidByTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool FeeSummaryTotalFeesPaidByTypeOtherDescriptionSpecified
        {
            get { return this.FeeSummaryTotalFeesPaidByTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public FEE_SUMMARY_TOTAL_FEES_PAID_BY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
