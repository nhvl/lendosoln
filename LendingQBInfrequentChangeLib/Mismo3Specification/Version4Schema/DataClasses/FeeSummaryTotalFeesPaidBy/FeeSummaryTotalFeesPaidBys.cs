namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class FEE_SUMMARY_TOTAL_FEES_PAID_BYS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.FeeSummaryTotalFeesPaidByListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("FEE_SUMMARY_TOTAL_FEES_PAID_BY", Order = 0)]
        public List<FEE_SUMMARY_TOTAL_FEES_PAID_BY> FeeSummaryTotalFeesPaidByList { get; set; } = new List<FEE_SUMMARY_TOTAL_FEES_PAID_BY>();
    
        [XmlIgnore]
        public bool FeeSummaryTotalFeesPaidByListSpecified
        {
            get { return this.FeeSummaryTotalFeesPaidByList != null && this.FeeSummaryTotalFeesPaidByList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public FEE_SUMMARY_TOTAL_FEES_PAID_BYS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
