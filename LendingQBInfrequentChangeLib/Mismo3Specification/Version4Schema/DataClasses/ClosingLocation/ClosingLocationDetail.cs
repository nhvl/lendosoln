namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CLOSING_LOCATION_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ClosingEventLocationDescriptionSpecified
                    || this.ClosingEventLocationTypeSpecified
                    || this.ClosingEventLocationTypeOtherDescriptionSpecified
                    || this.ClosingEventNoAgentPresentIndicatorSpecified
                    || this.ClosingEventOnlineIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ClosingEventLocationDescription", Order = 0)]
        public MISMOString ClosingEventLocationDescription { get; set; }
    
        [XmlIgnore]
        public bool ClosingEventLocationDescriptionSpecified
        {
            get { return this.ClosingEventLocationDescription != null; }
            set { }
        }
    
        [XmlElement("ClosingEventLocationType", Order = 1)]
        public MISMOEnum<ClosingEventLocationBase> ClosingEventLocationType { get; set; }
    
        [XmlIgnore]
        public bool ClosingEventLocationTypeSpecified
        {
            get { return this.ClosingEventLocationType != null; }
            set { }
        }
    
        [XmlElement("ClosingEventLocationTypeOtherDescription", Order = 2)]
        public MISMOString ClosingEventLocationTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ClosingEventLocationTypeOtherDescriptionSpecified
        {
            get { return this.ClosingEventLocationTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ClosingEventNoAgentPresentIndicator", Order = 3)]
        public MISMOIndicator ClosingEventNoAgentPresentIndicator { get; set; }
    
        [XmlIgnore]
        public bool ClosingEventNoAgentPresentIndicatorSpecified
        {
            get { return this.ClosingEventNoAgentPresentIndicator != null; }
            set { }
        }
    
        [XmlElement("ClosingEventOnlineIndicator", Order = 4)]
        public MISMOIndicator ClosingEventOnlineIndicator { get; set; }
    
        [XmlIgnore]
        public bool ClosingEventOnlineIndicatorSpecified
        {
            get { return this.ClosingEventOnlineIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public CLOSING_LOCATION_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
