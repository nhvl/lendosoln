namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CLOSING_LOCATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AddressSpecified
                    || this.ClosingLocationDetailSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ADDRESS", Order = 0)]
        public ADDRESS Address { get; set; }
    
        [XmlIgnore]
        public bool AddressSpecified
        {
            get { return this.Address != null && this.Address.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CLOSING_LOCATION_DETAIL", Order = 1)]
        public CLOSING_LOCATION_DETAIL ClosingLocationDetail { get; set; }
    
        [XmlIgnore]
        public bool ClosingLocationDetailSpecified
        {
            get { return this.ClosingLocationDetail != null && this.ClosingLocationDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public CLOSING_LOCATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
