namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class CLOSING_LOCATIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ClosingLocationListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CLOSING_LOCATION", Order = 0)]
        public List<CLOSING_LOCATION> ClosingLocationList { get; set; } = new List<CLOSING_LOCATION>();
    
        [XmlIgnore]
        public bool ClosingLocationListSpecified
        {
            get { return this.ClosingLocationList != null && this.ClosingLocationList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public CLOSING_LOCATIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
