namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class EXECUTION_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ActualSignatureTypeSpecified
                    || this.ActualSignatureTypeOtherDescriptionSpecified
                    || this.ExecutionDateSpecified
                    || this.ExecutionDatetimeSpecified
                    || this.ExecutionJudicialDistrictNameSpecified
                    || this.ExecutionJudicialDistrictTypeSpecified
                    || this.ExecutionJudicialDistrictTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ActualSignatureType", Order = 0)]
        public MISMOEnum<SignatureBase> ActualSignatureType { get; set; }
    
        [XmlIgnore]
        public bool ActualSignatureTypeSpecified
        {
            get { return this.ActualSignatureType != null; }
            set { }
        }
    
        [XmlElement("ActualSignatureTypeOtherDescription", Order = 1)]
        public MISMOString ActualSignatureTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ActualSignatureTypeOtherDescriptionSpecified
        {
            get { return this.ActualSignatureTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ExecutionDate", Order = 2)]
        public MISMODate ExecutionDate { get; set; }
    
        [XmlIgnore]
        public bool ExecutionDateSpecified
        {
            get { return this.ExecutionDate != null; }
            set { }
        }
    
        [XmlElement("ExecutionDatetime", Order = 3)]
        public MISMODatetime ExecutionDatetime { get; set; }
    
        [XmlIgnore]
        public bool ExecutionDatetimeSpecified
        {
            get { return this.ExecutionDatetime != null; }
            set { }
        }
    
        [XmlElement("ExecutionJudicialDistrictName", Order = 4)]
        public MISMOString ExecutionJudicialDistrictName { get; set; }
    
        [XmlIgnore]
        public bool ExecutionJudicialDistrictNameSpecified
        {
            get { return this.ExecutionJudicialDistrictName != null; }
            set { }
        }
    
        [XmlElement("ExecutionJudicialDistrictType", Order = 5)]
        public MISMOEnum<ExecutionJudicialDistrictBase> ExecutionJudicialDistrictType { get; set; }
    
        [XmlIgnore]
        public bool ExecutionJudicialDistrictTypeSpecified
        {
            get { return this.ExecutionJudicialDistrictType != null; }
            set { }
        }
    
        [XmlElement("ExecutionJudicialDistrictTypeOtherDescription", Order = 6)]
        public MISMOString ExecutionJudicialDistrictTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ExecutionJudicialDistrictTypeOtherDescriptionSpecified
        {
            get { return this.ExecutionJudicialDistrictTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 7)]
        public EXECUTION_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
