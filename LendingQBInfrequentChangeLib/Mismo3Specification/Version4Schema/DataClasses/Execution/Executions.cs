namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class EXECUTIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExecutionListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("EXECUTION", Order = 0)]
        public List<EXECUTION> ExecutionList { get; set; } = new List<EXECUTION>();
    
        [XmlIgnore]
        public bool ExecutionListSpecified
        {
            get { return this.ExecutionList != null && this.ExecutionList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public EXECUTIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
