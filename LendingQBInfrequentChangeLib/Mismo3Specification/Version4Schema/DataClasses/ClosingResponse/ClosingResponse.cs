namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CLOSING_RESPONSE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ClosingLocationsSpecified
                    || this.ClosingResponseDetailSpecified
                    || this.ClosingRevisionsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CLOSING_LOCATIONS", Order = 0)]
        public CLOSING_LOCATIONS ClosingLocations { get; set; }
    
        [XmlIgnore]
        public bool ClosingLocationsSpecified
        {
            get { return this.ClosingLocations != null && this.ClosingLocations.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CLOSING_RESPONSE_DETAIL", Order = 1)]
        public CLOSING_RESPONSE_DETAIL ClosingResponseDetail { get; set; }
    
        [XmlIgnore]
        public bool ClosingResponseDetailSpecified
        {
            get { return this.ClosingResponseDetail != null && this.ClosingResponseDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CLOSING_REVISIONS", Order = 2)]
        public CLOSING_REVISIONS ClosingRevisions { get; set; }
    
        [XmlIgnore]
        public bool ClosingRevisionsSpecified
        {
            get { return this.ClosingRevisions != null && this.ClosingRevisions.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public CLOSING_RESPONSE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
