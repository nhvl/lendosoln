namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CLOSING_RESPONSE_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ClosingScheduledDatetimeSpecified
                    || this.LoanScheduledClosingDateSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ClosingScheduledDatetime", Order = 0)]
        public MISMODatetime ClosingScheduledDatetime { get; set; }
    
        [XmlIgnore]
        public bool ClosingScheduledDatetimeSpecified
        {
            get { return this.ClosingScheduledDatetime != null; }
            set { }
        }
    
        [XmlElement("LoanScheduledClosingDate", Order = 1)]
        public MISMODate LoanScheduledClosingDate { get; set; }
    
        [XmlIgnore]
        public bool LoanScheduledClosingDateSpecified
        {
            get { return this.LoanScheduledClosingDate != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public CLOSING_RESPONSE_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
