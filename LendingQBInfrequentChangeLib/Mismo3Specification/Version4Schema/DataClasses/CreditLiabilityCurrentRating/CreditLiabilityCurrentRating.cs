namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CREDIT_LIABILITY_CURRENT_RATING
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditLiabilityCurrentRatingCodeSpecified
                    || this.CreditLiabilityCurrentRatingTypeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CreditLiabilityCurrentRatingCode", Order = 0)]
        public MISMOCode CreditLiabilityCurrentRatingCode { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityCurrentRatingCodeSpecified
        {
            get { return this.CreditLiabilityCurrentRatingCode != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityCurrentRatingType", Order = 1)]
        public MISMOEnum<CreditLiabilityCurrentRatingBase> CreditLiabilityCurrentRatingType { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityCurrentRatingTypeSpecified
        {
            get { return this.CreditLiabilityCurrentRatingType != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public CREDIT_LIABILITY_CURRENT_RATING_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
