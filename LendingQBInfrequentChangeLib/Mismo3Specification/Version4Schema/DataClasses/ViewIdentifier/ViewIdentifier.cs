namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class VIEW_IDENTIFIER
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ViewIdentifierSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ViewIdentifier", Order = 0)]
        public MISMOIdentifier ViewIdentifier { get; set; }
    
        [XmlIgnore]
        public bool ViewIdentifierSpecified
        {
            get { return this.ViewIdentifier != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public VIEW_IDENTIFIER_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
