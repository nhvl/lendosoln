namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class VIEW_IDENTIFIERS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ViewIdentifierListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("VIEW_IDENTIFIER", Order = 0)]
        public List<VIEW_IDENTIFIER> ViewIdentifierList { get; set; } = new List<VIEW_IDENTIFIER>();
    
        [XmlIgnore]
        public bool ViewIdentifierListSpecified
        {
            get { return this.ViewIdentifierList != null && this.ViewIdentifierList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public VIEW_IDENTIFIERS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
