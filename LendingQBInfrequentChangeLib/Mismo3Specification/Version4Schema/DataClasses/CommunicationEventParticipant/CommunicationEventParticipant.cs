namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class COMMUNICATION_EVENT_PARTICIPANT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EventParticipantTypeSpecified
                    || this.EventParticipantTypeAdditionalDescriptionSpecified
                    || this.EventParticipantTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("EventParticipantType", Order = 0)]
        public MISMOEnum<EventParticipantBase> EventParticipantType { get; set; }
    
        [XmlIgnore]
        public bool EventParticipantTypeSpecified
        {
            get { return this.EventParticipantType != null; }
            set { }
        }
    
        [XmlElement("EventParticipantTypeAdditionalDescription", Order = 1)]
        public MISMOString EventParticipantTypeAdditionalDescription { get; set; }
    
        [XmlIgnore]
        public bool EventParticipantTypeAdditionalDescriptionSpecified
        {
            get { return this.EventParticipantTypeAdditionalDescription != null; }
            set { }
        }
    
        [XmlElement("EventParticipantTypeOtherDescription", Order = 2)]
        public MISMOString EventParticipantTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool EventParticipantTypeOtherDescriptionSpecified
        {
            get { return this.EventParticipantTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public COMMUNICATION_EVENT_PARTICIPANT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
