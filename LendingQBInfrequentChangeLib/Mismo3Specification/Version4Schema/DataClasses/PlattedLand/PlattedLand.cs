namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PLATTED_LAND
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AppurtenanceDescriptionSpecified
                    || this.AppurtenanceIdentifierSpecified
                    || this.AppurtenanceTypeSpecified
                    || this.AppurtenanceTypeOtherDescriptionSpecified
                    || this.PlatBlockIdentifierSpecified
                    || this.PlatBookIdentifierSpecified
                    || this.PlatBuildingIdentifierSpecified
                    || this.PlatIdentifierSpecified
                    || this.PlatInstrumentIdentifierSpecified
                    || this.PlatLotIdentifierSpecified
                    || this.PlatNameSpecified
                    || this.PlatPageIdentifierSpecified
                    || this.PlatTypeSpecified
                    || this.PlatTypeOtherDescriptionSpecified
                    || this.PlatUnitIdentifierSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AppurtenanceDescription", Order = 0)]
        public MISMOString AppurtenanceDescription { get; set; }
    
        [XmlIgnore]
        public bool AppurtenanceDescriptionSpecified
        {
            get { return this.AppurtenanceDescription != null; }
            set { }
        }
    
        [XmlElement("AppurtenanceIdentifier", Order = 1)]
        public MISMOIdentifier AppurtenanceIdentifier { get; set; }
    
        [XmlIgnore]
        public bool AppurtenanceIdentifierSpecified
        {
            get { return this.AppurtenanceIdentifier != null; }
            set { }
        }
    
        [XmlElement("AppurtenanceType", Order = 2)]
        public MISMOEnum<AppurtenanceBase> AppurtenanceType { get; set; }
    
        [XmlIgnore]
        public bool AppurtenanceTypeSpecified
        {
            get { return this.AppurtenanceType != null; }
            set { }
        }
    
        [XmlElement("AppurtenanceTypeOtherDescription", Order = 3)]
        public MISMOString AppurtenanceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool AppurtenanceTypeOtherDescriptionSpecified
        {
            get { return this.AppurtenanceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PlatBlockIdentifier", Order = 4)]
        public MISMOIdentifier PlatBlockIdentifier { get; set; }
    
        [XmlIgnore]
        public bool PlatBlockIdentifierSpecified
        {
            get { return this.PlatBlockIdentifier != null; }
            set { }
        }
    
        [XmlElement("PlatBookIdentifier", Order = 5)]
        public MISMOIdentifier PlatBookIdentifier { get; set; }
    
        [XmlIgnore]
        public bool PlatBookIdentifierSpecified
        {
            get { return this.PlatBookIdentifier != null; }
            set { }
        }
    
        [XmlElement("PlatBuildingIdentifier", Order = 6)]
        public MISMOIdentifier PlatBuildingIdentifier { get; set; }
    
        [XmlIgnore]
        public bool PlatBuildingIdentifierSpecified
        {
            get { return this.PlatBuildingIdentifier != null; }
            set { }
        }
    
        [XmlElement("PlatIdentifier", Order = 7)]
        public MISMOIdentifier PlatIdentifier { get; set; }
    
        [XmlIgnore]
        public bool PlatIdentifierSpecified
        {
            get { return this.PlatIdentifier != null; }
            set { }
        }
    
        [XmlElement("PlatInstrumentIdentifier", Order = 8)]
        public MISMOIdentifier PlatInstrumentIdentifier { get; set; }
    
        [XmlIgnore]
        public bool PlatInstrumentIdentifierSpecified
        {
            get { return this.PlatInstrumentIdentifier != null; }
            set { }
        }
    
        [XmlElement("PlatLotIdentifier", Order = 9)]
        public MISMOIdentifier PlatLotIdentifier { get; set; }
    
        [XmlIgnore]
        public bool PlatLotIdentifierSpecified
        {
            get { return this.PlatLotIdentifier != null; }
            set { }
        }
    
        [XmlElement("PlatName", Order = 10)]
        public MISMOString PlatName { get; set; }
    
        [XmlIgnore]
        public bool PlatNameSpecified
        {
            get { return this.PlatName != null; }
            set { }
        }
    
        [XmlElement("PlatPageIdentifier", Order = 11)]
        public MISMOIdentifier PlatPageIdentifier { get; set; }
    
        [XmlIgnore]
        public bool PlatPageIdentifierSpecified
        {
            get { return this.PlatPageIdentifier != null; }
            set { }
        }
    
        [XmlElement("PlatType", Order = 12)]
        public MISMOEnum<PlatBase> PlatType { get; set; }
    
        [XmlIgnore]
        public bool PlatTypeSpecified
        {
            get { return this.PlatType != null; }
            set { }
        }
    
        [XmlElement("PlatTypeOtherDescription", Order = 13)]
        public MISMOString PlatTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PlatTypeOtherDescriptionSpecified
        {
            get { return this.PlatTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PlatUnitIdentifier", Order = 14)]
        public MISMOIdentifier PlatUnitIdentifier { get; set; }
    
        [XmlIgnore]
        public bool PlatUnitIdentifierSpecified
        {
            get { return this.PlatUnitIdentifier != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 15)]
        public PLATTED_LAND_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
