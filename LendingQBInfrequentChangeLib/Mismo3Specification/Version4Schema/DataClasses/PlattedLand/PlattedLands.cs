namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class PLATTED_LANDS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PlattedLandListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PLATTED_LAND", Order = 0)]
        public List<PLATTED_LAND> PlattedLandList { get; set; } = new List<PLATTED_LAND>();
    
        [XmlIgnore]
        public bool PlattedLandListSpecified
        {
            get { return this.PlattedLandList != null && this.PlattedLandList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public PLATTED_LANDS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
