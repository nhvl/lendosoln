namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class DATA_SOURCE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DataSourceDescriptionSpecified
                    || this.DataSourceEffectiveDateSpecified
                    || this.DataSourceNameSpecified
                    || this.DataSourceTypeSpecified
                    || this.DataSourceTypeOtherDescriptionSpecified
                    || this.DataSourceURLSpecified
                    || this.DataSourceVerificationDescriptionSpecified
                    || this.DataSourceVersionIdentifierSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DataSourceDescription", Order = 0)]
        public MISMOString DataSourceDescription { get; set; }
    
        [XmlIgnore]
        public bool DataSourceDescriptionSpecified
        {
            get { return this.DataSourceDescription != null; }
            set { }
        }
    
        [XmlElement("DataSourceEffectiveDate", Order = 1)]
        public MISMODate DataSourceEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool DataSourceEffectiveDateSpecified
        {
            get { return this.DataSourceEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("DataSourceName", Order = 2)]
        public MISMOString DataSourceName { get; set; }
    
        [XmlIgnore]
        public bool DataSourceNameSpecified
        {
            get { return this.DataSourceName != null; }
            set { }
        }
    
        [XmlElement("DataSourceType", Order = 3)]
        public MISMOEnum<DataSourceBase> DataSourceType { get; set; }
    
        [XmlIgnore]
        public bool DataSourceTypeSpecified
        {
            get { return this.DataSourceType != null; }
            set { }
        }
    
        [XmlElement("DataSourceTypeOtherDescription", Order = 4)]
        public MISMOString DataSourceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool DataSourceTypeOtherDescriptionSpecified
        {
            get { return this.DataSourceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("DataSourceURL", Order = 5)]
        public MISMOURL DataSourceURL { get; set; }
    
        [XmlIgnore]
        public bool DataSourceURLSpecified
        {
            get { return this.DataSourceURL != null; }
            set { }
        }
    
        [XmlElement("DataSourceVerificationDescription", Order = 6)]
        public MISMOString DataSourceVerificationDescription { get; set; }
    
        [XmlIgnore]
        public bool DataSourceVerificationDescriptionSpecified
        {
            get { return this.DataSourceVerificationDescription != null; }
            set { }
        }
    
        [XmlElement("DataSourceVersionIdentifier", Order = 7)]
        public MISMOIdentifier DataSourceVersionIdentifier { get; set; }
    
        [XmlIgnore]
        public bool DataSourceVersionIdentifierSpecified
        {
            get { return this.DataSourceVersionIdentifier != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 8)]
        public DATA_SOURCE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
