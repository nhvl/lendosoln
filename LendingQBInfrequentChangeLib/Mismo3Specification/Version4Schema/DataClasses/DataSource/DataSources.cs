namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class DATA_SOURCES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DataSourceListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DATA_SOURCE", Order = 0)]
        public List<DATA_SOURCE> DataSourceList { get; set; } = new List<DATA_SOURCE>();
    
        [XmlIgnore]
        public bool DataSourceListSpecified
        {
            get { return this.DataSourceList != null && this.DataSourceList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public DATA_SOURCES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
