namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class REPAYMENT_PLAN
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PromiseToPayAmountSpecified
                    || this.PromiseToPayDueDateSpecified
                    || this.RepaymentPlanDownPaymentAmountSpecified
                    || this.RepaymentPlanEndDateSpecified
                    || this.RepaymentPlanMonthlyDueAmountSpecified
                    || this.RepaymentPlanNextDueDateSpecified
                    || this.RepaymentPlanStartDateSpecified
                    || this.RepaymentPlanTermMonthsCountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PromiseToPayAmount", Order = 0)]
        public MISMOAmount PromiseToPayAmount { get; set; }
    
        [XmlIgnore]
        public bool PromiseToPayAmountSpecified
        {
            get { return this.PromiseToPayAmount != null; }
            set { }
        }
    
        [XmlElement("PromiseToPayDueDate", Order = 1)]
        public MISMODate PromiseToPayDueDate { get; set; }
    
        [XmlIgnore]
        public bool PromiseToPayDueDateSpecified
        {
            get { return this.PromiseToPayDueDate != null; }
            set { }
        }
    
        [XmlElement("RepaymentPlanDownPaymentAmount", Order = 2)]
        public MISMOAmount RepaymentPlanDownPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool RepaymentPlanDownPaymentAmountSpecified
        {
            get { return this.RepaymentPlanDownPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("RepaymentPlanEndDate", Order = 3)]
        public MISMODate RepaymentPlanEndDate { get; set; }
    
        [XmlIgnore]
        public bool RepaymentPlanEndDateSpecified
        {
            get { return this.RepaymentPlanEndDate != null; }
            set { }
        }
    
        [XmlElement("RepaymentPlanMonthlyDueAmount", Order = 4)]
        public MISMOAmount RepaymentPlanMonthlyDueAmount { get; set; }
    
        [XmlIgnore]
        public bool RepaymentPlanMonthlyDueAmountSpecified
        {
            get { return this.RepaymentPlanMonthlyDueAmount != null; }
            set { }
        }
    
        [XmlElement("RepaymentPlanNextDueDate", Order = 5)]
        public MISMODate RepaymentPlanNextDueDate { get; set; }
    
        [XmlIgnore]
        public bool RepaymentPlanNextDueDateSpecified
        {
            get { return this.RepaymentPlanNextDueDate != null; }
            set { }
        }
    
        [XmlElement("RepaymentPlanStartDate", Order = 6)]
        public MISMODate RepaymentPlanStartDate { get; set; }
    
        [XmlIgnore]
        public bool RepaymentPlanStartDateSpecified
        {
            get { return this.RepaymentPlanStartDate != null; }
            set { }
        }
    
        [XmlElement("RepaymentPlanTermMonthsCount", Order = 7)]
        public MISMOCount RepaymentPlanTermMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool RepaymentPlanTermMonthsCountSpecified
        {
            get { return this.RepaymentPlanTermMonthsCount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 8)]
        public REPAYMENT_PLAN_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
