namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class REPAYMENT_PLANS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.RepaymentPlanListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("REPAYMENT_PLAN", Order = 0)]
        public List<REPAYMENT_PLAN> RepaymentPlanList { get; set; } = new List<REPAYMENT_PLAN>();
    
        [XmlIgnore]
        public bool RepaymentPlanListSpecified
        {
            get { return this.RepaymentPlanList != null && this.RepaymentPlanList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public REPAYMENT_PLANS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
