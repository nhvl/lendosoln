namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CLAIM
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ClaimRequestSpecified
                    || this.ClaimResponseSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CLAIM_REQUEST", Order = 0)]
        public CLAIM_REQUEST ClaimRequest { get; set; }
    
        [XmlIgnore]
        public bool ClaimRequestSpecified
        {
            get { return this.ClaimRequest != null && this.ClaimRequest.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CLAIM_RESPONSE", Order = 1)]
        public CLAIM_RESPONSE ClaimResponse { get; set; }
    
        [XmlIgnore]
        public bool ClaimResponseSpecified
        {
            get { return this.ClaimResponse != null && this.ClaimResponse.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public CLAIM_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
