namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ERROR_MESSAGE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ErrorMessageCategoryCodeSpecified
                    || this.ErrorMessageCodeSpecified
                    || this.ErrorMessageTextSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ErrorMessageCategoryCode", Order = 0)]
        public MISMOCode ErrorMessageCategoryCode { get; set; }
    
        [XmlIgnore]
        public bool ErrorMessageCategoryCodeSpecified
        {
            get { return this.ErrorMessageCategoryCode != null; }
            set { }
        }
    
        [XmlElement("ErrorMessageCode", Order = 1)]
        public MISMOCode ErrorMessageCode { get; set; }
    
        [XmlIgnore]
        public bool ErrorMessageCodeSpecified
        {
            get { return this.ErrorMessageCode != null; }
            set { }
        }
    
        [XmlElement("ErrorMessageText", Order = 2)]
        public MISMOString ErrorMessageText { get; set; }
    
        [XmlIgnore]
        public bool ErrorMessageTextSpecified
        {
            get { return this.ErrorMessageText != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public ERROR_MESSAGE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
