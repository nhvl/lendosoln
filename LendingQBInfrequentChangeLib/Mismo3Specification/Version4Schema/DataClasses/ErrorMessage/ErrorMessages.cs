namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class ERROR_MESSAGES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ErrorMessageListSpecified
                    || this.ErrorMessageSummarySpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ERROR_MESSAGE", Order = 0)]
        public List<ERROR_MESSAGE> ErrorMessageList { get; set; } = new List<ERROR_MESSAGE>();
    
        [XmlIgnore]
        public bool ErrorMessageListSpecified
        {
            get { return this.ErrorMessageList != null && this.ErrorMessageList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("ERROR_MESSAGE_SUMMARY", Order = 1)]
        public ERROR_MESSAGE_SUMMARY ErrorMessageSummary { get; set; }
    
        [XmlIgnore]
        public bool ErrorMessageSummarySpecified
        {
            get { return this.ErrorMessageSummary != null && this.ErrorMessageSummary.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public ERROR_MESSAGES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
