namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ERROR_MESSAGE_SUMMARY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ErrorCodeSpecified
                    || this.ErrorSummaryTextSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ErrorCode", Order = 0)]
        public MISMOCode ErrorCode { get; set; }
    
        [XmlIgnore]
        public bool ErrorCodeSpecified
        {
            get { return this.ErrorCode != null; }
            set { }
        }
    
        [XmlElement("ErrorSummaryText", Order = 1)]
        public MISMOString ErrorSummaryText { get; set; }
    
        [XmlIgnore]
        public bool ErrorSummaryTextSpecified
        {
            get { return this.ErrorSummaryText != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public ERROR_MESSAGE_SUMMARY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
