namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class ASSUMABILITY_OCCURRENCES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AssumabilityOccurrenceListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ASSUMABILITY_OCCURRENCE", Order = 0)]
        public List<ASSUMABILITY_OCCURRENCE> AssumabilityOccurrenceList { get; set; } = new List<ASSUMABILITY_OCCURRENCE>();
    
        [XmlIgnore]
        public bool AssumabilityOccurrenceListSpecified
        {
            get { return this.AssumabilityOccurrenceList != null && this.AssumabilityOccurrenceList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public ASSUMABILITY_OCCURRENCES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
