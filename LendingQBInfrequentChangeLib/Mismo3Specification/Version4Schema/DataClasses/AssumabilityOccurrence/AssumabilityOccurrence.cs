namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ASSUMABILITY_OCCURRENCE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AssumptionDateSpecified
                    || this.ReleaseOfLiabilityIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AssumptionDate", Order = 0)]
        public MISMODate AssumptionDate { get; set; }
    
        [XmlIgnore]
        public bool AssumptionDateSpecified
        {
            get { return this.AssumptionDate != null; }
            set { }
        }
    
        [XmlElement("ReleaseOfLiabilityIndicator", Order = 1)]
        public MISMOIndicator ReleaseOfLiabilityIndicator { get; set; }
    
        [XmlIgnore]
        public bool ReleaseOfLiabilityIndicatorSpecified
        {
            get { return this.ReleaseOfLiabilityIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public ASSUMABILITY_OCCURRENCE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
