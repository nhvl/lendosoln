namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class UNIT_CHARGE_UTILITY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.UnitChargeUtilityIncludedInAssessmentIndicatorSpecified
                    || this.UnitChargeUtilityTypeSpecified
                    || this.UnitChargeUtilityTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("UnitChargeUtilityIncludedInAssessmentIndicator", Order = 0)]
        public MISMOIndicator UnitChargeUtilityIncludedInAssessmentIndicator { get; set; }
    
        [XmlIgnore]
        public bool UnitChargeUtilityIncludedInAssessmentIndicatorSpecified
        {
            get { return this.UnitChargeUtilityIncludedInAssessmentIndicator != null; }
            set { }
        }
    
        [XmlElement("UnitChargeUtilityType", Order = 1)]
        public MISMOEnum<UnitChargeUtilityBase> UnitChargeUtilityType { get; set; }
    
        [XmlIgnore]
        public bool UnitChargeUtilityTypeSpecified
        {
            get { return this.UnitChargeUtilityType != null; }
            set { }
        }
    
        [XmlElement("UnitChargeUtilityTypeOtherDescription", Order = 2)]
        public MISMOString UnitChargeUtilityTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool UnitChargeUtilityTypeOtherDescriptionSpecified
        {
            get { return this.UnitChargeUtilityTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public UNIT_CHARGE_UTILITY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
