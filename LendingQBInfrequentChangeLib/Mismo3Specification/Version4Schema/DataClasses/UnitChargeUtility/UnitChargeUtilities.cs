namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class UNIT_CHARGE_UTILITIES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.UnitChargeUtilityListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("UNIT_CHARGE_UTILITY", Order = 0)]
        public List<UNIT_CHARGE_UTILITY> UnitChargeUtilityList { get; set; } = new List<UNIT_CHARGE_UTILITY>();
    
        [XmlIgnore]
        public bool UnitChargeUtilityListSpecified
        {
            get { return this.UnitChargeUtilityList != null && this.UnitChargeUtilityList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public UNIT_CHARGE_UTILITIES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
