namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class LENDER
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditUnionIndicatorSpecified
                    || this.LenderDocumentsOrderedByNameSpecified
                    || this.LenderFunderNameSpecified
                    || this.RegulatoryAgencyLenderIdentifierSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CreditUnionIndicator", Order = 0)]
        public MISMOIndicator CreditUnionIndicator { get; set; }
    
        [XmlIgnore]
        public bool CreditUnionIndicatorSpecified
        {
            get { return this.CreditUnionIndicator != null; }
            set { }
        }
    
        [XmlElement("LenderDocumentsOrderedByName", Order = 1)]
        public MISMOString LenderDocumentsOrderedByName { get; set; }
    
        [XmlIgnore]
        public bool LenderDocumentsOrderedByNameSpecified
        {
            get { return this.LenderDocumentsOrderedByName != null; }
            set { }
        }
    
        [XmlElement("LenderFunderName", Order = 2)]
        public MISMOString LenderFunderName { get; set; }
    
        [XmlIgnore]
        public bool LenderFunderNameSpecified
        {
            get { return this.LenderFunderName != null; }
            set { }
        }
    
        [XmlElement("RegulatoryAgencyLenderIdentifier", Order = 3)]
        public MISMOIdentifier RegulatoryAgencyLenderIdentifier { get; set; }
    
        [XmlIgnore]
        public bool RegulatoryAgencyLenderIdentifierSpecified
        {
            get { return this.RegulatoryAgencyLenderIdentifier != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public LENDER_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
