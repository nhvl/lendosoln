namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class INVESTOR_REPORTING_ADDITIONAL_CHARGE_SUMMARY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.TotalInvestorReportingAdditionalChargesAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("TotalInvestorReportingAdditionalChargesAmount", Order = 0)]
        public MISMOAmount TotalInvestorReportingAdditionalChargesAmount { get; set; }
    
        [XmlIgnore]
        public bool TotalInvestorReportingAdditionalChargesAmountSpecified
        {
            get { return this.TotalInvestorReportingAdditionalChargesAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public INVESTOR_REPORTING_ADDITIONAL_CHARGE_SUMMARY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
