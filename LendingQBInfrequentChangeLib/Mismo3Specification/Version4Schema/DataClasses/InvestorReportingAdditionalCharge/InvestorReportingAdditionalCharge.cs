namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class INVESTOR_REPORTING_ADDITIONAL_CHARGE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.InvestorReportingAdditionalChargeAmountSpecified
                    || this.InvestorReportingAdditionalChargeAssessedToPartyTypeSpecified
                    || this.InvestorReportingAdditionalChargeAssessedToPartyTypeOtherDescriptionSpecified
                    || this.InvestorReportingAdditionalChargeDateSpecified
                    || this.InvestorReportingAdditionalChargeReversalIndicatorSpecified
                    || this.InvestorReportingAdditionalChargeTypeSpecified
                    || this.InvestorReportingAdditionalChargeTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("InvestorReportingAdditionalChargeAmount", Order = 0)]
        public MISMOAmount InvestorReportingAdditionalChargeAmount { get; set; }
    
        [XmlIgnore]
        public bool InvestorReportingAdditionalChargeAmountSpecified
        {
            get { return this.InvestorReportingAdditionalChargeAmount != null; }
            set { }
        }
    
        [XmlElement("InvestorReportingAdditionalChargeAssessedToPartyType", Order = 1)]
        public MISMOEnum<InvestorReportingAdditionalChargeAssessedToPartyBase> InvestorReportingAdditionalChargeAssessedToPartyType { get; set; }
    
        [XmlIgnore]
        public bool InvestorReportingAdditionalChargeAssessedToPartyTypeSpecified
        {
            get { return this.InvestorReportingAdditionalChargeAssessedToPartyType != null; }
            set { }
        }
    
        [XmlElement("InvestorReportingAdditionalChargeAssessedToPartyTypeOtherDescription", Order = 2)]
        public MISMOString InvestorReportingAdditionalChargeAssessedToPartyTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool InvestorReportingAdditionalChargeAssessedToPartyTypeOtherDescriptionSpecified
        {
            get { return this.InvestorReportingAdditionalChargeAssessedToPartyTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("InvestorReportingAdditionalChargeDate", Order = 3)]
        public MISMODate InvestorReportingAdditionalChargeDate { get; set; }
    
        [XmlIgnore]
        public bool InvestorReportingAdditionalChargeDateSpecified
        {
            get { return this.InvestorReportingAdditionalChargeDate != null; }
            set { }
        }
    
        [XmlElement("InvestorReportingAdditionalChargeReversalIndicator", Order = 4)]
        public MISMOIndicator InvestorReportingAdditionalChargeReversalIndicator { get; set; }
    
        [XmlIgnore]
        public bool InvestorReportingAdditionalChargeReversalIndicatorSpecified
        {
            get { return this.InvestorReportingAdditionalChargeReversalIndicator != null; }
            set { }
        }
    
        [XmlElement("InvestorReportingAdditionalChargeType", Order = 5)]
        public MISMOEnum<InvestorReportingAdditionalChargeBase> InvestorReportingAdditionalChargeType { get; set; }
    
        [XmlIgnore]
        public bool InvestorReportingAdditionalChargeTypeSpecified
        {
            get { return this.InvestorReportingAdditionalChargeType != null; }
            set { }
        }
    
        [XmlElement("InvestorReportingAdditionalChargeTypeOtherDescription", Order = 6)]
        public MISMOString InvestorReportingAdditionalChargeTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool InvestorReportingAdditionalChargeTypeOtherDescriptionSpecified
        {
            get { return this.InvestorReportingAdditionalChargeTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 7)]
        public INVESTOR_REPORTING_ADDITIONAL_CHARGE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
