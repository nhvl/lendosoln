namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class INVESTOR_REPORTING_ADDITIONAL_CHARGES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.InvestorReportingAdditionalChargeListSpecified
                    || this.InvestorReportingAdditionalChargeSummarySpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("INVESTOR_REPORTING_ADDITIONAL_CHARGE", Order = 0)]
        public List<INVESTOR_REPORTING_ADDITIONAL_CHARGE> InvestorReportingAdditionalChargeList { get; set; } = new List<INVESTOR_REPORTING_ADDITIONAL_CHARGE>();
    
        [XmlIgnore]
        public bool InvestorReportingAdditionalChargeListSpecified
        {
            get { return this.InvestorReportingAdditionalChargeList != null && this.InvestorReportingAdditionalChargeList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("INVESTOR_REPORTING_ADDITIONAL_CHARGE_SUMMARY", Order = 1)]
        public INVESTOR_REPORTING_ADDITIONAL_CHARGE_SUMMARY InvestorReportingAdditionalChargeSummary { get; set; }
    
        [XmlIgnore]
        public bool InvestorReportingAdditionalChargeSummarySpecified
        {
            get { return this.InvestorReportingAdditionalChargeSummary != null && this.InvestorReportingAdditionalChargeSummary.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public INVESTOR_REPORTING_ADDITIONAL_CHARGES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
