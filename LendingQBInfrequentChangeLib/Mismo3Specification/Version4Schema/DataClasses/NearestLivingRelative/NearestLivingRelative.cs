namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class NEAREST_LIVING_RELATIVE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AddressSpecified
                    || this.ContactPointsSpecified
                    || this.NameSpecified
                    || this.NearestLivingRelativeDetailSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ADDRESS", Order = 0)]
        public ADDRESS Address { get; set; }
    
        [XmlIgnore]
        public bool AddressSpecified
        {
            get { return this.Address != null && this.Address.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CONTACT_POINTS", Order = 1)]
        public CONTACT_POINTS ContactPoints { get; set; }
    
        [XmlIgnore]
        public bool ContactPointsSpecified
        {
            get { return this.ContactPoints != null && this.ContactPoints.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("NAME", Order = 2)]
        public NAME Name { get; set; }
    
        [XmlIgnore]
        public bool NameSpecified
        {
            get { return this.Name != null && this.Name.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("NEAREST_LIVING_RELATIVE_DETAIL", Order = 3)]
        public NEAREST_LIVING_RELATIVE_DETAIL NearestLivingRelativeDetail { get; set; }
    
        [XmlIgnore]
        public bool NearestLivingRelativeDetailSpecified
        {
            get { return this.NearestLivingRelativeDetail != null && this.NearestLivingRelativeDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public NEAREST_LIVING_RELATIVE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
