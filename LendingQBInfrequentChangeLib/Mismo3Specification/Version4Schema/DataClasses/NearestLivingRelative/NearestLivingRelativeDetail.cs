namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class NEAREST_LIVING_RELATIVE_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BorrowerNearestLivingRelativeRelationshipDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BorrowerNearestLivingRelativeRelationshipDescription", Order = 0)]
        public MISMOString BorrowerNearestLivingRelativeRelationshipDescription { get; set; }
    
        [XmlIgnore]
        public bool BorrowerNearestLivingRelativeRelationshipDescriptionSpecified
        {
            get { return this.BorrowerNearestLivingRelativeRelationshipDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public NEAREST_LIVING_RELATIVE_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
