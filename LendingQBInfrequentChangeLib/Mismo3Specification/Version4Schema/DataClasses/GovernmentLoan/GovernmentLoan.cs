namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class GOVERNMENT_LOAN
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AgencyProgramDescriptionSpecified
                    || this.BorrowerFinancedFHADiscountPointsAmountSpecified
                    || this.BorrowerFundingFeePercentSpecified
                    || this.BorrowerHomeInspectionChosenIndicatorSpecified
                    || this.BorrowerPaidFHA_VAClosingCostsAmountSpecified
                    || this.BorrowerPaidFHA_VAClosingCostsPercentSpecified
                    || this.DaysToFHA_MIEligibilityCountSpecified
                    || this.FHA_MIPremiumRefundAmountSpecified
                    || this.FHAAlimonyLiabilityTreatmentTypeSpecified
                    || this.FHAAnniversaryDateSpecified
                    || this.FHAAnnualPremiumAmountSpecified
                    || this.FHAAnnualPremiumPercentSpecified
                    || this.FHAAssignmentDateSpecified
                    || this.FHAAutomatedDataProcessingIdentifierSpecified
                    || this.FHACoverageRenewalRatePercentSpecified
                    || this.FHAEndorsementDateSpecified
                    || this.FHAEnergyRelatedRepairsOrImprovementsAmountSpecified
                    || this.FHAGeneralServicesAdministrationIdentifierSpecified
                    || this.FHAInsuranceProgramTypeSpecified
                    || this.FHALimitedDenialParticipationIdentifierSpecified
                    || this.FHALoanLenderIdentifierSpecified
                    || this.FHALoanSponsorIdentifierSpecified
                    || this.FHANonOwnerOccupancyRiderRule248IndicatorSpecified
                    || this.FHAPendingPremiumAmountSpecified
                    || this.FHAPendingPremiumChangeDateSpecified
                    || this.FHAPremiumAnniversaryYearToDateRemittanceAmountSpecified
                    || this.FHARefinanceInterestOnExistingLienAmountSpecified
                    || this.FHARefinanceOriginalExistingFHACaseIdentifierSpecified
                    || this.FHARefinanceOriginalExistingUpfrontMIPremiumAmountSpecified
                    || this.FHAUnderwriterComputerizedHomesUnderwritingSystemIdentifierSpecified
                    || this.FHAUpfrontPremiumAmountSpecified
                    || this.FHAUpfrontPremiumPercentSpecified
                    || this.GovernmentLoanApplicationTypeSpecified
                    || this.GovernmentMortgageCreditCertificateAmountSpecified
                    || this.GovernmentRefinanceTypeSpecified
                    || this.GovernmentRefinanceTypeOtherDescriptionSpecified
                    || this.HUDAdequateAvailableAssetsIndicatorSpecified
                    || this.HUDAdequateEffectiveIncomeIndicatorSpecified
                    || this.HUDCreditCharacteristicsIndicatorSpecified
                    || this.HUDStableEffectiveIncomeIndicatorSpecified
                    || this.MasterCertificateOfReasonableValueIdentifierSpecified
                    || this.OtherPartyPaidFHA_VAClosingCostsAmountSpecified
                    || this.OtherPartyPaidFHA_VAClosingCostsPercentSpecified
                    || this.PreviousVAHomeLoanIndicatorSpecified
                    || this.PropertyEnergyEfficientHomeIndicatorSpecified
                    || this.RuralHousingConditionalGuarantyExpirationDateSpecified
                    || this.RuralHousingConditionalGuarantyInterestRatePercentSpecified
                    || this.SectionOfActTypeSpecified
                    || this.SectionOfActTypeOtherDescriptionSpecified
                    || this.SellerPaidFHA_VAClosingCostsPercentSpecified
                    || this.SoldUnderHUDSingleFamilyPropertyDispositionProgramIndicatorSpecified
                    || this.USDA198021RuralHousingAdjustedAnnualHouseholdIncomeAmountSpecified
                    || this.VAAppraisalTypeSpecified
                    || this.VAAppraisalTypeOtherDescriptionSpecified
                    || this.VABorrowerCoBorrowerMarriedIndicatorSpecified
                    || this.VAClaimFolderIdentifierSpecified
                    || this.VAEntitlementAmountSpecified
                    || this.VAEntitlementIdentifierSpecified
                    || this.VAFundingFeeExemptionTypeSpecified
                    || this.VAFundingFeeExemptionTypeOtherDescriptionSpecified
                    || this.VAHouseholdSizeCountSpecified
                    || this.VALoanProcedureTypeSpecified
                    || this.VALoanProcedureTypeOtherDescriptionSpecified
                    || this.VALoanProgramTypeSpecified
                    || this.VALoanProgramTypeOtherDescriptionSpecified
                    || this.VAMaintenanceExpenseMonthlyAmountSpecified
                    || this.VAReasonableValueImprovementsCompletionIndicatorSpecified
                    || this.VAResidualIncomeAmountSpecified
                    || this.VAResidualIncomeGuidelineAmountSpecified
                    || this.VATitleVestingTypeSpecified
                    || this.VATitleVestingTypeOtherDescriptionSpecified
                    || this.VAUtilityExpenseMonthlyAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AgencyProgramDescription", Order = 0)]
        public MISMOString AgencyProgramDescription { get; set; }
    
        [XmlIgnore]
        public bool AgencyProgramDescriptionSpecified
        {
            get { return this.AgencyProgramDescription != null; }
            set { }
        }
    
        [XmlElement("BorrowerFinancedFHADiscountPointsAmount", Order = 1)]
        public MISMOAmount BorrowerFinancedFHADiscountPointsAmount { get; set; }
    
        [XmlIgnore]
        public bool BorrowerFinancedFHADiscountPointsAmountSpecified
        {
            get { return this.BorrowerFinancedFHADiscountPointsAmount != null; }
            set { }
        }
    
        [XmlElement("BorrowerFundingFeePercent", Order = 2)]
        public MISMOPercent BorrowerFundingFeePercent { get; set; }
    
        [XmlIgnore]
        public bool BorrowerFundingFeePercentSpecified
        {
            get { return this.BorrowerFundingFeePercent != null; }
            set { }
        }
    
        [XmlElement("BorrowerHomeInspectionChosenIndicator", Order = 3)]
        public MISMOIndicator BorrowerHomeInspectionChosenIndicator { get; set; }
    
        [XmlIgnore]
        public bool BorrowerHomeInspectionChosenIndicatorSpecified
        {
            get { return this.BorrowerHomeInspectionChosenIndicator != null; }
            set { }
        }
    
        [XmlElement("BorrowerPaidFHA_VAClosingCostsAmount", Order = 4)]
        public MISMOAmount BorrowerPaidFHA_VAClosingCostsAmount { get; set; }
    
        [XmlIgnore]
        public bool BorrowerPaidFHA_VAClosingCostsAmountSpecified
        {
            get { return this.BorrowerPaidFHA_VAClosingCostsAmount != null; }
            set { }
        }
    
        [XmlElement("BorrowerPaidFHA_VAClosingCostsPercent", Order = 5)]
        public MISMOPercent BorrowerPaidFHA_VAClosingCostsPercent { get; set; }
    
        [XmlIgnore]
        public bool BorrowerPaidFHA_VAClosingCostsPercentSpecified
        {
            get { return this.BorrowerPaidFHA_VAClosingCostsPercent != null; }
            set { }
        }
    
        [XmlElement("DaysToFHA_MIEligibilityCount", Order = 6)]
        public MISMOCount DaysToFHA_MIEligibilityCount { get; set; }
    
        [XmlIgnore]
        public bool DaysToFHA_MIEligibilityCountSpecified
        {
            get { return this.DaysToFHA_MIEligibilityCount != null; }
            set { }
        }
    
        [XmlElement("FHA_MIPremiumRefundAmount", Order = 7)]
        public MISMOAmount FHA_MIPremiumRefundAmount { get; set; }
    
        [XmlIgnore]
        public bool FHA_MIPremiumRefundAmountSpecified
        {
            get { return this.FHA_MIPremiumRefundAmount != null; }
            set { }
        }
    
        [XmlElement("FHAAlimonyLiabilityTreatmentType", Order = 8)]
        public MISMOEnum<FHAAlimonyLiabilityTreatmentBase> FHAAlimonyLiabilityTreatmentType { get; set; }
    
        [XmlIgnore]
        public bool FHAAlimonyLiabilityTreatmentTypeSpecified
        {
            get { return this.FHAAlimonyLiabilityTreatmentType != null; }
            set { }
        }
    
        [XmlElement("FHAAnniversaryDate", Order = 9)]
        public MISMODate FHAAnniversaryDate { get; set; }
    
        [XmlIgnore]
        public bool FHAAnniversaryDateSpecified
        {
            get { return this.FHAAnniversaryDate != null; }
            set { }
        }
    
        [XmlElement("FHAAnnualPremiumAmount", Order = 10)]
        public MISMOAmount FHAAnnualPremiumAmount { get; set; }
    
        [XmlIgnore]
        public bool FHAAnnualPremiumAmountSpecified
        {
            get { return this.FHAAnnualPremiumAmount != null; }
            set { }
        }
    
        [XmlElement("FHAAnnualPremiumPercent", Order = 11)]
        public MISMOPercent FHAAnnualPremiumPercent { get; set; }
    
        [XmlIgnore]
        public bool FHAAnnualPremiumPercentSpecified
        {
            get { return this.FHAAnnualPremiumPercent != null; }
            set { }
        }
    
        [XmlElement("FHAAssignmentDate", Order = 12)]
        public MISMODate FHAAssignmentDate { get; set; }
    
        [XmlIgnore]
        public bool FHAAssignmentDateSpecified
        {
            get { return this.FHAAssignmentDate != null; }
            set { }
        }
    
        [XmlElement("FHAAutomatedDataProcessingIdentifier", Order = 13)]
        public MISMOIdentifier FHAAutomatedDataProcessingIdentifier { get; set; }
    
        [XmlIgnore]
        public bool FHAAutomatedDataProcessingIdentifierSpecified
        {
            get { return this.FHAAutomatedDataProcessingIdentifier != null; }
            set { }
        }
    
        [XmlElement("FHACoverageRenewalRatePercent", Order = 14)]
        public MISMOPercent FHACoverageRenewalRatePercent { get; set; }
    
        [XmlIgnore]
        public bool FHACoverageRenewalRatePercentSpecified
        {
            get { return this.FHACoverageRenewalRatePercent != null; }
            set { }
        }
    
        [XmlElement("FHAEndorsementDate", Order = 15)]
        public MISMODate FHAEndorsementDate { get; set; }
    
        [XmlIgnore]
        public bool FHAEndorsementDateSpecified
        {
            get { return this.FHAEndorsementDate != null; }
            set { }
        }
    
        [XmlElement("FHAEnergyRelatedRepairsOrImprovementsAmount", Order = 16)]
        public MISMOAmount FHAEnergyRelatedRepairsOrImprovementsAmount { get; set; }
    
        [XmlIgnore]
        public bool FHAEnergyRelatedRepairsOrImprovementsAmountSpecified
        {
            get { return this.FHAEnergyRelatedRepairsOrImprovementsAmount != null; }
            set { }
        }
    
        [XmlElement("FHAGeneralServicesAdministrationIdentifier", Order = 17)]
        public MISMOIdentifier FHAGeneralServicesAdministrationIdentifier { get; set; }
    
        [XmlIgnore]
        public bool FHAGeneralServicesAdministrationIdentifierSpecified
        {
            get { return this.FHAGeneralServicesAdministrationIdentifier != null; }
            set { }
        }
    
        [XmlElement("FHAInsuranceProgramType", Order = 18)]
        public MISMOEnum<FHAInsuranceProgramBase> FHAInsuranceProgramType { get; set; }
    
        [XmlIgnore]
        public bool FHAInsuranceProgramTypeSpecified
        {
            get { return this.FHAInsuranceProgramType != null; }
            set { }
        }
    
        [XmlElement("FHALimitedDenialParticipationIdentifier", Order = 19)]
        public MISMOIdentifier FHALimitedDenialParticipationIdentifier { get; set; }
    
        [XmlIgnore]
        public bool FHALimitedDenialParticipationIdentifierSpecified
        {
            get { return this.FHALimitedDenialParticipationIdentifier != null; }
            set { }
        }
    
        [XmlElement("FHALoanLenderIdentifier", Order = 20)]
        public MISMOIdentifier FHALoanLenderIdentifier { get; set; }
    
        [XmlIgnore]
        public bool FHALoanLenderIdentifierSpecified
        {
            get { return this.FHALoanLenderIdentifier != null; }
            set { }
        }
    
        [XmlElement("FHALoanSponsorIdentifier", Order = 21)]
        public MISMOIdentifier FHALoanSponsorIdentifier { get; set; }
    
        [XmlIgnore]
        public bool FHALoanSponsorIdentifierSpecified
        {
            get { return this.FHALoanSponsorIdentifier != null; }
            set { }
        }
    
        [XmlElement("FHANonOwnerOccupancyRiderRule248Indicator", Order = 22)]
        public MISMOIndicator FHANonOwnerOccupancyRiderRule248Indicator { get; set; }
    
        [XmlIgnore]
        public bool FHANonOwnerOccupancyRiderRule248IndicatorSpecified
        {
            get { return this.FHANonOwnerOccupancyRiderRule248Indicator != null; }
            set { }
        }
    
        [XmlElement("FHAPendingPremiumAmount", Order = 23)]
        public MISMOAmount FHAPendingPremiumAmount { get; set; }
    
        [XmlIgnore]
        public bool FHAPendingPremiumAmountSpecified
        {
            get { return this.FHAPendingPremiumAmount != null; }
            set { }
        }
    
        [XmlElement("FHAPendingPremiumChangeDate", Order = 24)]
        public MISMODate FHAPendingPremiumChangeDate { get; set; }
    
        [XmlIgnore]
        public bool FHAPendingPremiumChangeDateSpecified
        {
            get { return this.FHAPendingPremiumChangeDate != null; }
            set { }
        }
    
        [XmlElement("FHAPremiumAnniversaryYearToDateRemittanceAmount", Order = 25)]
        public MISMOAmount FHAPremiumAnniversaryYearToDateRemittanceAmount { get; set; }
    
        [XmlIgnore]
        public bool FHAPremiumAnniversaryYearToDateRemittanceAmountSpecified
        {
            get { return this.FHAPremiumAnniversaryYearToDateRemittanceAmount != null; }
            set { }
        }
    
        [XmlElement("FHARefinanceInterestOnExistingLienAmount", Order = 26)]
        public MISMOAmount FHARefinanceInterestOnExistingLienAmount { get; set; }
    
        [XmlIgnore]
        public bool FHARefinanceInterestOnExistingLienAmountSpecified
        {
            get { return this.FHARefinanceInterestOnExistingLienAmount != null; }
            set { }
        }
    
        [XmlElement("FHARefinanceOriginalExistingFHACaseIdentifier", Order = 27)]
        public MISMOIdentifier FHARefinanceOriginalExistingFHACaseIdentifier { get; set; }
    
        [XmlIgnore]
        public bool FHARefinanceOriginalExistingFHACaseIdentifierSpecified
        {
            get { return this.FHARefinanceOriginalExistingFHACaseIdentifier != null; }
            set { }
        }
    
        [XmlElement("FHARefinanceOriginalExistingUpfrontMIPremiumAmount", Order = 28)]
        public MISMOAmount FHARefinanceOriginalExistingUpfrontMIPremiumAmount { get; set; }
    
        [XmlIgnore]
        public bool FHARefinanceOriginalExistingUpfrontMIPremiumAmountSpecified
        {
            get { return this.FHARefinanceOriginalExistingUpfrontMIPremiumAmount != null; }
            set { }
        }
    
        [XmlElement("FHAUnderwriterComputerizedHomesUnderwritingSystemIdentifier", Order = 29)]
        public MISMOIdentifier FHAUnderwriterComputerizedHomesUnderwritingSystemIdentifier { get; set; }
    
        [XmlIgnore]
        public bool FHAUnderwriterComputerizedHomesUnderwritingSystemIdentifierSpecified
        {
            get { return this.FHAUnderwriterComputerizedHomesUnderwritingSystemIdentifier != null; }
            set { }
        }
    
        [XmlElement("FHAUpfrontPremiumAmount", Order = 30)]
        public MISMOAmount FHAUpfrontPremiumAmount { get; set; }
    
        [XmlIgnore]
        public bool FHAUpfrontPremiumAmountSpecified
        {
            get { return this.FHAUpfrontPremiumAmount != null; }
            set { }
        }
    
        [XmlElement("FHAUpfrontPremiumPercent", Order = 31)]
        public MISMOPercent FHAUpfrontPremiumPercent { get; set; }
    
        [XmlIgnore]
        public bool FHAUpfrontPremiumPercentSpecified
        {
            get { return this.FHAUpfrontPremiumPercent != null; }
            set { }
        }
    
        [XmlElement("GovernmentLoanApplicationType", Order = 32)]
        public MISMOEnum<GovernmentLoanApplicationBase> GovernmentLoanApplicationType { get; set; }
    
        [XmlIgnore]
        public bool GovernmentLoanApplicationTypeSpecified
        {
            get { return this.GovernmentLoanApplicationType != null; }
            set { }
        }
    
        [XmlElement("GovernmentMortgageCreditCertificateAmount", Order = 33)]
        public MISMOAmount GovernmentMortgageCreditCertificateAmount { get; set; }
    
        [XmlIgnore]
        public bool GovernmentMortgageCreditCertificateAmountSpecified
        {
            get { return this.GovernmentMortgageCreditCertificateAmount != null; }
            set { }
        }
    
        [XmlElement("GovernmentRefinanceType", Order = 34)]
        public MISMOEnum<GovernmentRefinanceBase> GovernmentRefinanceType { get; set; }
    
        [XmlIgnore]
        public bool GovernmentRefinanceTypeSpecified
        {
            get { return this.GovernmentRefinanceType != null; }
            set { }
        }
    
        [XmlElement("GovernmentRefinanceTypeOtherDescription", Order = 35)]
        public MISMOString GovernmentRefinanceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool GovernmentRefinanceTypeOtherDescriptionSpecified
        {
            get { return this.GovernmentRefinanceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("HUDAdequateAvailableAssetsIndicator", Order = 36)]
        public MISMOIndicator HUDAdequateAvailableAssetsIndicator { get; set; }
    
        [XmlIgnore]
        public bool HUDAdequateAvailableAssetsIndicatorSpecified
        {
            get { return this.HUDAdequateAvailableAssetsIndicator != null; }
            set { }
        }
    
        [XmlElement("HUDAdequateEffectiveIncomeIndicator", Order = 37)]
        public MISMOIndicator HUDAdequateEffectiveIncomeIndicator { get; set; }
    
        [XmlIgnore]
        public bool HUDAdequateEffectiveIncomeIndicatorSpecified
        {
            get { return this.HUDAdequateEffectiveIncomeIndicator != null; }
            set { }
        }
    
        [XmlElement("HUDCreditCharacteristicsIndicator", Order = 38)]
        public MISMOIndicator HUDCreditCharacteristicsIndicator { get; set; }
    
        [XmlIgnore]
        public bool HUDCreditCharacteristicsIndicatorSpecified
        {
            get { return this.HUDCreditCharacteristicsIndicator != null; }
            set { }
        }
    
        [XmlElement("HUDStableEffectiveIncomeIndicator", Order = 39)]
        public MISMOIndicator HUDStableEffectiveIncomeIndicator { get; set; }
    
        [XmlIgnore]
        public bool HUDStableEffectiveIncomeIndicatorSpecified
        {
            get { return this.HUDStableEffectiveIncomeIndicator != null; }
            set { }
        }
    
        [XmlElement("MasterCertificateOfReasonableValueIdentifier", Order = 40)]
        public MISMOIdentifier MasterCertificateOfReasonableValueIdentifier { get; set; }
    
        [XmlIgnore]
        public bool MasterCertificateOfReasonableValueIdentifierSpecified
        {
            get { return this.MasterCertificateOfReasonableValueIdentifier != null; }
            set { }
        }
    
        [XmlElement("OtherPartyPaidFHA_VAClosingCostsAmount", Order = 41)]
        public MISMOAmount OtherPartyPaidFHA_VAClosingCostsAmount { get; set; }
    
        [XmlIgnore]
        public bool OtherPartyPaidFHA_VAClosingCostsAmountSpecified
        {
            get { return this.OtherPartyPaidFHA_VAClosingCostsAmount != null; }
            set { }
        }
    
        [XmlElement("OtherPartyPaidFHA_VAClosingCostsPercent", Order = 42)]
        public MISMOPercent OtherPartyPaidFHA_VAClosingCostsPercent { get; set; }
    
        [XmlIgnore]
        public bool OtherPartyPaidFHA_VAClosingCostsPercentSpecified
        {
            get { return this.OtherPartyPaidFHA_VAClosingCostsPercent != null; }
            set { }
        }
    
        [XmlElement("PreviousVAHomeLoanIndicator", Order = 43)]
        public MISMOIndicator PreviousVAHomeLoanIndicator { get; set; }
    
        [XmlIgnore]
        public bool PreviousVAHomeLoanIndicatorSpecified
        {
            get { return this.PreviousVAHomeLoanIndicator != null; }
            set { }
        }
    
        [XmlElement("PropertyEnergyEfficientHomeIndicator", Order = 44)]
        public MISMOIndicator PropertyEnergyEfficientHomeIndicator { get; set; }
    
        [XmlIgnore]
        public bool PropertyEnergyEfficientHomeIndicatorSpecified
        {
            get { return this.PropertyEnergyEfficientHomeIndicator != null; }
            set { }
        }
    
        [XmlElement("RuralHousingConditionalGuarantyExpirationDate", Order = 45)]
        public MISMODate RuralHousingConditionalGuarantyExpirationDate { get; set; }
    
        [XmlIgnore]
        public bool RuralHousingConditionalGuarantyExpirationDateSpecified
        {
            get { return this.RuralHousingConditionalGuarantyExpirationDate != null; }
            set { }
        }
    
        [XmlElement("RuralHousingConditionalGuarantyInterestRatePercent", Order = 46)]
        public MISMOPercent RuralHousingConditionalGuarantyInterestRatePercent { get; set; }
    
        [XmlIgnore]
        public bool RuralHousingConditionalGuarantyInterestRatePercentSpecified
        {
            get { return this.RuralHousingConditionalGuarantyInterestRatePercent != null; }
            set { }
        }
    
        [XmlElement("SectionOfActType", Order = 47)]
        public MISMOEnum<SectionOfActBase> SectionOfActType { get; set; }
    
        [XmlIgnore]
        public bool SectionOfActTypeSpecified
        {
            get { return this.SectionOfActType != null; }
            set { }
        }
    
        [XmlElement("SectionOfActTypeOtherDescription", Order = 48)]
        public MISMOString SectionOfActTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool SectionOfActTypeOtherDescriptionSpecified
        {
            get { return this.SectionOfActTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("SellerPaidFHA_VAClosingCostsPercent", Order = 49)]
        public MISMOPercent SellerPaidFHA_VAClosingCostsPercent { get; set; }
    
        [XmlIgnore]
        public bool SellerPaidFHA_VAClosingCostsPercentSpecified
        {
            get { return this.SellerPaidFHA_VAClosingCostsPercent != null; }
            set { }
        }
    
        [XmlElement("SoldUnderHUDSingleFamilyPropertyDispositionProgramIndicator", Order = 50)]
        public MISMOIndicator SoldUnderHUDSingleFamilyPropertyDispositionProgramIndicator { get; set; }
    
        [XmlIgnore]
        public bool SoldUnderHUDSingleFamilyPropertyDispositionProgramIndicatorSpecified
        {
            get { return this.SoldUnderHUDSingleFamilyPropertyDispositionProgramIndicator != null; }
            set { }
        }
    
        [XmlElement("USDA198021RuralHousingAdjustedAnnualHouseholdIncomeAmount", Order = 51)]
        public MISMOAmount USDA198021RuralHousingAdjustedAnnualHouseholdIncomeAmount { get; set; }
    
        [XmlIgnore]
        public bool USDA198021RuralHousingAdjustedAnnualHouseholdIncomeAmountSpecified
        {
            get { return this.USDA198021RuralHousingAdjustedAnnualHouseholdIncomeAmount != null; }
            set { }
        }
    
        [XmlElement("VAAppraisalType", Order = 52)]
        public MISMOEnum<VAAppraisalBase> VAAppraisalType { get; set; }
    
        [XmlIgnore]
        public bool VAAppraisalTypeSpecified
        {
            get { return this.VAAppraisalType != null; }
            set { }
        }
    
        [XmlElement("VAAppraisalTypeOtherDescription", Order = 53)]
        public MISMOString VAAppraisalTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool VAAppraisalTypeOtherDescriptionSpecified
        {
            get { return this.VAAppraisalTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("VABorrowerCoBorrowerMarriedIndicator", Order = 54)]
        public MISMOIndicator VABorrowerCoBorrowerMarriedIndicator { get; set; }
    
        [XmlIgnore]
        public bool VABorrowerCoBorrowerMarriedIndicatorSpecified
        {
            get { return this.VABorrowerCoBorrowerMarriedIndicator != null; }
            set { }
        }
    
        [XmlElement("VAClaimFolderIdentifier", Order = 55)]
        public MISMOIdentifier VAClaimFolderIdentifier { get; set; }
    
        [XmlIgnore]
        public bool VAClaimFolderIdentifierSpecified
        {
            get { return this.VAClaimFolderIdentifier != null; }
            set { }
        }
    
        [XmlElement("VAEntitlementAmount", Order = 56)]
        public MISMOAmount VAEntitlementAmount { get; set; }
    
        [XmlIgnore]
        public bool VAEntitlementAmountSpecified
        {
            get { return this.VAEntitlementAmount != null; }
            set { }
        }
    
        [XmlElement("VAEntitlementIdentifier", Order = 57)]
        public MISMOIdentifier VAEntitlementIdentifier { get; set; }
    
        [XmlIgnore]
        public bool VAEntitlementIdentifierSpecified
        {
            get { return this.VAEntitlementIdentifier != null; }
            set { }
        }
    
        [XmlElement("VAFundingFeeExemptionType", Order = 58)]
        public MISMOEnum<VAFundingFeeExemptionBase> VAFundingFeeExemptionType { get; set; }
    
        [XmlIgnore]
        public bool VAFundingFeeExemptionTypeSpecified
        {
            get { return this.VAFundingFeeExemptionType != null; }
            set { }
        }
    
        [XmlElement("VAFundingFeeExemptionTypeOtherDescription", Order = 59)]
        public MISMOString VAFundingFeeExemptionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool VAFundingFeeExemptionTypeOtherDescriptionSpecified
        {
            get { return this.VAFundingFeeExemptionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("VAHouseholdSizeCount", Order = 60)]
        public MISMOCount VAHouseholdSizeCount { get; set; }
    
        [XmlIgnore]
        public bool VAHouseholdSizeCountSpecified
        {
            get { return this.VAHouseholdSizeCount != null; }
            set { }
        }
    
        [XmlElement("VALoanProcedureType", Order = 61)]
        public MISMOEnum<VALoanProcedureBase> VALoanProcedureType { get; set; }
    
        [XmlIgnore]
        public bool VALoanProcedureTypeSpecified
        {
            get { return this.VALoanProcedureType != null; }
            set { }
        }
    
        [XmlElement("VALoanProcedureTypeOtherDescription", Order = 62)]
        public MISMOString VALoanProcedureTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool VALoanProcedureTypeOtherDescriptionSpecified
        {
            get { return this.VALoanProcedureTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("VALoanProgramType", Order = 63)]
        public MISMOEnum<VALoanProgramBase> VALoanProgramType { get; set; }
    
        [XmlIgnore]
        public bool VALoanProgramTypeSpecified
        {
            get { return this.VALoanProgramType != null; }
            set { }
        }
    
        [XmlElement("VALoanProgramTypeOtherDescription", Order = 64)]
        public MISMOString VALoanProgramTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool VALoanProgramTypeOtherDescriptionSpecified
        {
            get { return this.VALoanProgramTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("VAMaintenanceExpenseMonthlyAmount", Order = 65)]
        public MISMOAmount VAMaintenanceExpenseMonthlyAmount { get; set; }
    
        [XmlIgnore]
        public bool VAMaintenanceExpenseMonthlyAmountSpecified
        {
            get { return this.VAMaintenanceExpenseMonthlyAmount != null; }
            set { }
        }
    
        [XmlElement("VAReasonableValueImprovementsCompletionIndicator", Order = 66)]
        public MISMOIndicator VAReasonableValueImprovementsCompletionIndicator { get; set; }
    
        [XmlIgnore]
        public bool VAReasonableValueImprovementsCompletionIndicatorSpecified
        {
            get { return this.VAReasonableValueImprovementsCompletionIndicator != null; }
            set { }
        }
    
        [XmlElement("VAResidualIncomeAmount", Order = 67)]
        public MISMOAmount VAResidualIncomeAmount { get; set; }
    
        [XmlIgnore]
        public bool VAResidualIncomeAmountSpecified
        {
            get { return this.VAResidualIncomeAmount != null; }
            set { }
        }
    
        [XmlElement("VAResidualIncomeGuidelineAmount", Order = 68)]
        public MISMOAmount VAResidualIncomeGuidelineAmount { get; set; }
    
        [XmlIgnore]
        public bool VAResidualIncomeGuidelineAmountSpecified
        {
            get { return this.VAResidualIncomeGuidelineAmount != null; }
            set { }
        }
    
        [XmlElement("VATitleVestingType", Order = 69)]
        public MISMOEnum<VATitleVestingBase> VATitleVestingType { get; set; }
    
        [XmlIgnore]
        public bool VATitleVestingTypeSpecified
        {
            get { return this.VATitleVestingType != null; }
            set { }
        }
    
        [XmlElement("VATitleVestingTypeOtherDescription", Order = 70)]
        public MISMOString VATitleVestingTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool VATitleVestingTypeOtherDescriptionSpecified
        {
            get { return this.VATitleVestingTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("VAUtilityExpenseMonthlyAmount", Order = 71)]
        public MISMOAmount VAUtilityExpenseMonthlyAmount { get; set; }
    
        [XmlIgnore]
        public bool VAUtilityExpenseMonthlyAmountSpecified
        {
            get { return this.VAUtilityExpenseMonthlyAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 72)]
        public GOVERNMENT_LOAN_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
