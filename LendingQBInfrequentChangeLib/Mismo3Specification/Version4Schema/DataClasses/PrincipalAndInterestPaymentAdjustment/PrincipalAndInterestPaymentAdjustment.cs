namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PRINCIPAL_AND_INTEREST_PAYMENT_ADJUSTMENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.IndexRulesSpecified
                    || this.PrincipalAndInterestAdjustmentLimitedPaymentOptionsSpecified
                    || this.PrincipalAndInterestPaymentLifetimeAdjustmentRuleSpecified
                    || this.PrincipalAndInterestPaymentPerChangeAdjustmentRulesSpecified
                    || this.PrincipalAndInterestPaymentPeriodicAdjustmentRulesSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("INDEX_RULES", Order = 0)]
        public INDEX_RULES IndexRules { get; set; }
    
        [XmlIgnore]
        public bool IndexRulesSpecified
        {
            get { return this.IndexRules != null && this.IndexRules.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PRINCIPAL_AND_INTEREST_ADJUSTMENT_LIMITED_PAYMENT_OPTIONS", Order = 1)]
        public PRINCIPAL_AND_INTEREST_ADJUSTMENT_LIMITED_PAYMENT_OPTIONS PrincipalAndInterestAdjustmentLimitedPaymentOptions { get; set; }
    
        [XmlIgnore]
        public bool PrincipalAndInterestAdjustmentLimitedPaymentOptionsSpecified
        {
            get { return this.PrincipalAndInterestAdjustmentLimitedPaymentOptions != null && this.PrincipalAndInterestAdjustmentLimitedPaymentOptions.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PRINCIPAL_AND_INTEREST_PAYMENT_LIFETIME_ADJUSTMENT_RULE", Order = 2)]
        public PRINCIPAL_AND_INTEREST_PAYMENT_LIFETIME_ADJUSTMENT_RULE PrincipalAndInterestPaymentLifetimeAdjustmentRule { get; set; }
    
        [XmlIgnore]
        public bool PrincipalAndInterestPaymentLifetimeAdjustmentRuleSpecified
        {
            get { return this.PrincipalAndInterestPaymentLifetimeAdjustmentRule != null && this.PrincipalAndInterestPaymentLifetimeAdjustmentRule.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PRINCIPAL_AND_INTEREST_PAYMENT_PER_CHANGE_ADJUSTMENT_RULES", Order = 3)]
        public PRINCIPAL_AND_INTEREST_PAYMENT_PER_CHANGE_ADJUSTMENT_RULES PrincipalAndInterestPaymentPerChangeAdjustmentRules { get; set; }
    
        [XmlIgnore]
        public bool PrincipalAndInterestPaymentPerChangeAdjustmentRulesSpecified
        {
            get { return this.PrincipalAndInterestPaymentPerChangeAdjustmentRules != null && this.PrincipalAndInterestPaymentPerChangeAdjustmentRules.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PRINCIPAL_AND_INTEREST_PAYMENT_PERIODIC_ADJUSTMENT_RULES", Order = 4)]
        public PRINCIPAL_AND_INTEREST_PAYMENT_PERIODIC_ADJUSTMENT_RULES PrincipalAndInterestPaymentPeriodicAdjustmentRules { get; set; }
    
        [XmlIgnore]
        public bool PrincipalAndInterestPaymentPeriodicAdjustmentRulesSpecified
        {
            get { return this.PrincipalAndInterestPaymentPeriodicAdjustmentRules != null && this.PrincipalAndInterestPaymentPeriodicAdjustmentRules.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public PRINCIPAL_AND_INTEREST_PAYMENT_ADJUSTMENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
