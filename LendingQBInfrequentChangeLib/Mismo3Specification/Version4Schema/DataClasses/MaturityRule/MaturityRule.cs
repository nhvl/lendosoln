namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MATURITY_RULE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BiweeklyComparableMonthlyMaturityDateSpecified
                    || this.LoanMaturityDateSpecified
                    || this.LoanMaturityPeriodCountSpecified
                    || this.LoanMaturityPeriodTypeSpecified
                    || this.LoanTermMaximumMonthsCountSpecified
                    || this.MaximumMaturityTermExtensionCountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BiweeklyComparableMonthlyMaturityDate", Order = 0)]
        public MISMODate BiweeklyComparableMonthlyMaturityDate { get; set; }
    
        [XmlIgnore]
        public bool BiweeklyComparableMonthlyMaturityDateSpecified
        {
            get { return this.BiweeklyComparableMonthlyMaturityDate != null; }
            set { }
        }
    
        [XmlElement("LoanMaturityDate", Order = 1)]
        public MISMODate LoanMaturityDate { get; set; }
    
        [XmlIgnore]
        public bool LoanMaturityDateSpecified
        {
            get { return this.LoanMaturityDate != null; }
            set { }
        }
    
        [XmlElement("LoanMaturityPeriodCount", Order = 2)]
        public MISMOCount LoanMaturityPeriodCount { get; set; }
    
        [XmlIgnore]
        public bool LoanMaturityPeriodCountSpecified
        {
            get { return this.LoanMaturityPeriodCount != null; }
            set { }
        }
    
        [XmlElement("LoanMaturityPeriodType", Order = 3)]
        public MISMOEnum<LoanMaturityPeriodBase> LoanMaturityPeriodType { get; set; }
    
        [XmlIgnore]
        public bool LoanMaturityPeriodTypeSpecified
        {
            get { return this.LoanMaturityPeriodType != null; }
            set { }
        }
    
        [XmlElement("LoanTermMaximumMonthsCount", Order = 4)]
        public MISMOCount LoanTermMaximumMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool LoanTermMaximumMonthsCountSpecified
        {
            get { return this.LoanTermMaximumMonthsCount != null; }
            set { }
        }
    
        [XmlElement("MaximumMaturityTermExtensionCount", Order = 5)]
        public MISMOCount MaximumMaturityTermExtensionCount { get; set; }
    
        [XmlIgnore]
        public bool MaximumMaturityTermExtensionCountSpecified
        {
            get { return this.MaximumMaturityTermExtensionCount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 6)]
        public MATURITY_RULE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
