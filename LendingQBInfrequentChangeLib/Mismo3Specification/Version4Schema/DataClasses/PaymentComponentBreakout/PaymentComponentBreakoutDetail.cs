namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PAYMENT_COMPONENT_BREAKOUT_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BuydownSubsidyPaymentAmountSpecified
                    || this.BuydownSubsidyPaymentEffectiveDateSpecified
                    || this.EscrowShortagePaymentAmountSpecified
                    || this.HomeownersAssociationDuesPaymentAmountSpecified
                    || this.HUD235SubsidyPaymentAmountSpecified
                    || this.HUD235SubsidyPaymentEffectiveDateSpecified
                    || this.InsurancePaymentAmountSpecified
                    || this.InterestPaymentAmountSpecified
                    || this.PaymentStateTypeSpecified
                    || this.PaymentStateTypeOtherDescriptionSpecified
                    || this.PrincipalAndInterestPaymentAmountSpecified
                    || this.PrincipalAndInterestPaymentEffectiveDateSpecified
                    || this.PrincipalPaymentAmountSpecified
                    || this.SCRASubsidyPaymentAmountSpecified
                    || this.TaxAndInsurancePaymentAmountSpecified
                    || this.TaxAndInsurancePaymentEffectiveDateSpecified
                    || this.TaxesPaymentAmountSpecified
                    || this.TotalEscrowPaymentAmountSpecified
                    || this.TotalOptionalProductsPaymentAmountSpecified
                    || this.TotalOptionalProductsPaymentEffectiveDateSpecified
                    || this.TotalPaymentAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BuydownSubsidyPaymentAmount", Order = 0)]
        public MISMOAmount BuydownSubsidyPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool BuydownSubsidyPaymentAmountSpecified
        {
            get { return this.BuydownSubsidyPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("BuydownSubsidyPaymentEffectiveDate", Order = 1)]
        public MISMODate BuydownSubsidyPaymentEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool BuydownSubsidyPaymentEffectiveDateSpecified
        {
            get { return this.BuydownSubsidyPaymentEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("EscrowShortagePaymentAmount", Order = 2)]
        public MISMOAmount EscrowShortagePaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool EscrowShortagePaymentAmountSpecified
        {
            get { return this.EscrowShortagePaymentAmount != null; }
            set { }
        }
    
        [XmlElement("HomeownersAssociationDuesPaymentAmount", Order = 3)]
        public MISMOAmount HomeownersAssociationDuesPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool HomeownersAssociationDuesPaymentAmountSpecified
        {
            get { return this.HomeownersAssociationDuesPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("HUD235SubsidyPaymentAmount", Order = 4)]
        public MISMOAmount HUD235SubsidyPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool HUD235SubsidyPaymentAmountSpecified
        {
            get { return this.HUD235SubsidyPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("HUD235SubsidyPaymentEffectiveDate", Order = 5)]
        public MISMODate HUD235SubsidyPaymentEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool HUD235SubsidyPaymentEffectiveDateSpecified
        {
            get { return this.HUD235SubsidyPaymentEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("InsurancePaymentAmount", Order = 6)]
        public MISMOAmount InsurancePaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool InsurancePaymentAmountSpecified
        {
            get { return this.InsurancePaymentAmount != null; }
            set { }
        }
    
        [XmlElement("InterestPaymentAmount", Order = 7)]
        public MISMOAmount InterestPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool InterestPaymentAmountSpecified
        {
            get { return this.InterestPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("PaymentStateType", Order = 8)]
        public MISMOEnum<PaymentStateBase> PaymentStateType { get; set; }
    
        [XmlIgnore]
        public bool PaymentStateTypeSpecified
        {
            get { return this.PaymentStateType != null; }
            set { }
        }
    
        [XmlElement("PaymentStateTypeOtherDescription", Order = 9)]
        public MISMOString PaymentStateTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PaymentStateTypeOtherDescriptionSpecified
        {
            get { return this.PaymentStateTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PrincipalAndInterestPaymentAmount", Order = 10)]
        public MISMOAmount PrincipalAndInterestPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool PrincipalAndInterestPaymentAmountSpecified
        {
            get { return this.PrincipalAndInterestPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("PrincipalAndInterestPaymentEffectiveDate", Order = 11)]
        public MISMODate PrincipalAndInterestPaymentEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool PrincipalAndInterestPaymentEffectiveDateSpecified
        {
            get { return this.PrincipalAndInterestPaymentEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("PrincipalPaymentAmount", Order = 12)]
        public MISMOAmount PrincipalPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool PrincipalPaymentAmountSpecified
        {
            get { return this.PrincipalPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("SCRASubsidyPaymentAmount", Order = 13)]
        public MISMOAmount SCRASubsidyPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool SCRASubsidyPaymentAmountSpecified
        {
            get { return this.SCRASubsidyPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("TaxAndInsurancePaymentAmount", Order = 14)]
        public MISMOAmount TaxAndInsurancePaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool TaxAndInsurancePaymentAmountSpecified
        {
            get { return this.TaxAndInsurancePaymentAmount != null; }
            set { }
        }
    
        [XmlElement("TaxAndInsurancePaymentEffectiveDate", Order = 15)]
        public MISMODate TaxAndInsurancePaymentEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool TaxAndInsurancePaymentEffectiveDateSpecified
        {
            get { return this.TaxAndInsurancePaymentEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("TaxesPaymentAmount", Order = 16)]
        public MISMOAmount TaxesPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool TaxesPaymentAmountSpecified
        {
            get { return this.TaxesPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("TotalEscrowPaymentAmount", Order = 17)]
        public MISMOAmount TotalEscrowPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool TotalEscrowPaymentAmountSpecified
        {
            get { return this.TotalEscrowPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("TotalOptionalProductsPaymentAmount", Order = 18)]
        public MISMOAmount TotalOptionalProductsPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool TotalOptionalProductsPaymentAmountSpecified
        {
            get { return this.TotalOptionalProductsPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("TotalOptionalProductsPaymentEffectiveDate", Order = 19)]
        public MISMODate TotalOptionalProductsPaymentEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool TotalOptionalProductsPaymentEffectiveDateSpecified
        {
            get { return this.TotalOptionalProductsPaymentEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("TotalPaymentAmount", Order = 20)]
        public MISMOAmount TotalPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool TotalPaymentAmountSpecified
        {
            get { return this.TotalPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 21)]
        public PAYMENT_COMPONENT_BREAKOUT_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
