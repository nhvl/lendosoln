namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PAYMENT_COMPONENT_BREAKOUT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AdditionalChargesSpecified
                    || this.PaymentComponentBreakoutDetailSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ADDITIONAL_CHARGES", Order = 0)]
        public ADDITIONAL_CHARGES AdditionalCharges { get; set; }
    
        [XmlIgnore]
        public bool AdditionalChargesSpecified
        {
            get { return this.AdditionalCharges != null && this.AdditionalCharges.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PAYMENT_COMPONENT_BREAKOUT_DETAIL", Order = 1)]
        public PAYMENT_COMPONENT_BREAKOUT_DETAIL PaymentComponentBreakoutDetail { get; set; }
    
        [XmlIgnore]
        public bool PaymentComponentBreakoutDetailSpecified
        {
            get { return this.PaymentComponentBreakoutDetail != null && this.PaymentComponentBreakoutDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public PAYMENT_COMPONENT_BREAKOUT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
