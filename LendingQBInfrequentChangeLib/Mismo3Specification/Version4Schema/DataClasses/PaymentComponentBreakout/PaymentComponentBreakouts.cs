namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class PAYMENT_COMPONENT_BREAKOUTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PaymentComponentBreakoutListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PAYMENT_COMPONENT_BREAKOUT", Order = 0)]
        public List<PAYMENT_COMPONENT_BREAKOUT> PaymentComponentBreakoutList { get; set; } = new List<PAYMENT_COMPONENT_BREAKOUT>();
    
        [XmlIgnore]
        public bool PaymentComponentBreakoutListSpecified
        {
            get { return this.PaymentComponentBreakoutList != null && this.PaymentComponentBreakoutList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public PAYMENT_COMPONENT_BREAKOUTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
