namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SUPPORTING_RECORD_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.StructuredDataDefinitionTextSpecified
                    || this.SupportingRecordAsOfDateSpecified
                    || this.SupportingRecordConsumerComplaintIndicatorSpecified
                    || this.SupportingRecordEndDateSpecified
                    || this.SupportingRecordIdentifierSpecified
                    || this.SupportingRecordNotProvidedToBorrowerIndicatorSpecified
                    || this.SupportingRecordReviewFindingsTextSpecified
                    || this.SupportingRecordStartDateSpecified
                    || this.SupportingRecordTypeSpecified
                    || this.SupportingRecordTypeAdditionalDescriptionSpecified
                    || this.SupportingRecordTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("StructuredDataDefinitionText", Order = 0)]
        public MISMOString StructuredDataDefinitionText { get; set; }
    
        [XmlIgnore]
        public bool StructuredDataDefinitionTextSpecified
        {
            get { return this.StructuredDataDefinitionText != null; }
            set { }
        }
    
        [XmlElement("SupportingRecordAsOfDate", Order = 1)]
        public MISMODate SupportingRecordAsOfDate { get; set; }
    
        [XmlIgnore]
        public bool SupportingRecordAsOfDateSpecified
        {
            get { return this.SupportingRecordAsOfDate != null; }
            set { }
        }
    
        [XmlElement("SupportingRecordConsumerComplaintIndicator", Order = 2)]
        public MISMOIndicator SupportingRecordConsumerComplaintIndicator { get; set; }
    
        [XmlIgnore]
        public bool SupportingRecordConsumerComplaintIndicatorSpecified
        {
            get { return this.SupportingRecordConsumerComplaintIndicator != null; }
            set { }
        }
    
        [XmlElement("SupportingRecordEndDate", Order = 3)]
        public MISMODate SupportingRecordEndDate { get; set; }
    
        [XmlIgnore]
        public bool SupportingRecordEndDateSpecified
        {
            get { return this.SupportingRecordEndDate != null; }
            set { }
        }
    
        [XmlElement("SupportingRecordIdentifier", Order = 4)]
        public MISMOIdentifier SupportingRecordIdentifier { get; set; }
    
        [XmlIgnore]
        public bool SupportingRecordIdentifierSpecified
        {
            get { return this.SupportingRecordIdentifier != null; }
            set { }
        }
    
        [XmlElement("SupportingRecordNotProvidedToBorrowerIndicator", Order = 5)]
        public MISMOIndicator SupportingRecordNotProvidedToBorrowerIndicator { get; set; }
    
        [XmlIgnore]
        public bool SupportingRecordNotProvidedToBorrowerIndicatorSpecified
        {
            get { return this.SupportingRecordNotProvidedToBorrowerIndicator != null; }
            set { }
        }
    
        [XmlElement("SupportingRecordReviewFindingsText", Order = 6)]
        public MISMOString SupportingRecordReviewFindingsText { get; set; }
    
        [XmlIgnore]
        public bool SupportingRecordReviewFindingsTextSpecified
        {
            get { return this.SupportingRecordReviewFindingsText != null; }
            set { }
        }
    
        [XmlElement("SupportingRecordStartDate", Order = 7)]
        public MISMODate SupportingRecordStartDate { get; set; }
    
        [XmlIgnore]
        public bool SupportingRecordStartDateSpecified
        {
            get { return this.SupportingRecordStartDate != null; }
            set { }
        }
    
        [XmlElement("SupportingRecordType", Order = 8)]
        public MISMOEnum<SupportingRecordBase> SupportingRecordType { get; set; }
    
        [XmlIgnore]
        public bool SupportingRecordTypeSpecified
        {
            get { return this.SupportingRecordType != null; }
            set { }
        }
    
        [XmlElement("SupportingRecordTypeAdditionalDescription", Order = 9)]
        public MISMOString SupportingRecordTypeAdditionalDescription { get; set; }
    
        [XmlIgnore]
        public bool SupportingRecordTypeAdditionalDescriptionSpecified
        {
            get { return this.SupportingRecordTypeAdditionalDescription != null; }
            set { }
        }
    
        [XmlElement("SupportingRecordTypeOtherDescription", Order = 10)]
        public MISMOString SupportingRecordTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool SupportingRecordTypeOtherDescriptionSpecified
        {
            get { return this.SupportingRecordTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 11)]
        public SUPPORTING_RECORD_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
