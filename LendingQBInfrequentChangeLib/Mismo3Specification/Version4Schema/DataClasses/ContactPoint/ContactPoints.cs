namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class CONTACT_POINTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ContactPointListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CONTACT_POINT", Order = 0)]
        public List<CONTACT_POINT> ContactPointList { get; set; } = new List<CONTACT_POINT>();
    
        [XmlIgnore]
        public bool ContactPointListSpecified
        {
            get { return this.ContactPointList != null && this.ContactPointList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public CONTACT_POINTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
