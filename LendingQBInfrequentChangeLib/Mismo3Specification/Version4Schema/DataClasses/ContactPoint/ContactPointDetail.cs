namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CONTACT_POINT_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ContactPointPreferenceIndicatorSpecified
                    || this.ContactPointRoleTypeSpecified
                    || this.ContactPointRoleTypeOtherDescriptionSpecified
                    || this.RequestNoContactIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ContactPointPreferenceIndicator", Order = 0)]
        public MISMOIndicator ContactPointPreferenceIndicator { get; set; }
    
        [XmlIgnore]
        public bool ContactPointPreferenceIndicatorSpecified
        {
            get { return this.ContactPointPreferenceIndicator != null; }
            set { }
        }
    
        [XmlElement("ContactPointRoleType", Order = 1)]
        public MISMOEnum<ContactPointRoleBase> ContactPointRoleType { get; set; }
    
        [XmlIgnore]
        public bool ContactPointRoleTypeSpecified
        {
            get { return this.ContactPointRoleType != null; }
            set { }
        }
    
        [XmlElement("ContactPointRoleTypeOtherDescription", Order = 2)]
        public MISMOString ContactPointRoleTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ContactPointRoleTypeOtherDescriptionSpecified
        {
            get { return this.ContactPointRoleTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("RequestNoContactIndicator", Order = 3)]
        public MISMOIndicator RequestNoContactIndicator { get; set; }
    
        [XmlIgnore]
        public bool RequestNoContactIndicatorSpecified
        {
            get { return this.RequestNoContactIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public CONTACT_POINT_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
