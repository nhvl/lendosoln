namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    public class CONTACT_POINT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                if (Mismo3Utilities.ExceedsThreshold(1,
                    new List<bool> { this.ContactPointEmailSpecified, this.ContactPointSocialMediaSpecified, this.ContactPointTelephoneSpecified, this.OtherContactPointSpecified }))
                {
                    throw new System.Xml.Schema.XmlSchemaValidationException(
                        Mismo3Utilities.GetXSDChoiceExceptionMessage(
                        "CONTACT_POINT",
                        new List<string> { "CONTACT_POINT_EMAIL", "CONTACT_POINT_SOCIAL_MEDIA", "CONTACT_POINT_TELEPHONE", "OTHER_CONTACT_POINT" }));
                }

                return this.ContactPointEmailSpecified
                    || this.ContactPointSocialMediaSpecified
                    || this.ContactPointTelephoneSpecified
                    || this.OtherContactPointSpecified
                    || this.ContactPointDetailSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CONTACT_POINT_EMAIL", Order = 0)]
        public CONTACT_POINT_EMAIL ContactPointEmail { get; set; }
    
        [XmlIgnore]
        public bool ContactPointEmailSpecified
        {
            get { return this.ContactPointEmail != null && this.ContactPointEmail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CONTACT_POINT_SOCIAL_MEDIA", Order = 1)]
        public CONTACT_POINT_SOCIAL_MEDIA ContactPointSocialMedia { get; set; }
    
        [XmlIgnore]
        public bool ContactPointSocialMediaSpecified
        {
            get { return this.ContactPointSocialMedia != null && this.ContactPointSocialMedia.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CONTACT_POINT_TELEPHONE", Order = 2)]
        public CONTACT_POINT_TELEPHONE ContactPointTelephone { get; set; }
    
        [XmlIgnore]
        public bool ContactPointTelephoneSpecified
        {
            get { return this.ContactPointTelephone != null && this.ContactPointTelephone.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("OTHER_CONTACT_POINT", Order = 3)]
        public OTHER_CONTACT_POINT OtherContactPoint { get; set; }
    
        [XmlIgnore]
        public bool OtherContactPointSpecified
        {
            get { return this.OtherContactPoint != null && this.OtherContactPoint.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CONTACT_POINT_DETAIL", Order = 4)]
        public CONTACT_POINT_DETAIL ContactPointDetail { get; set; }
    
        [XmlIgnore]
        public bool ContactPointDetailSpecified
        {
            get { return this.ContactPointDetail != null && this.ContactPointDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public CONTACT_POINT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
