namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class MONETARY_EVENT_SUMMARIES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MonetaryEventSummaryListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MONETARY_EVENT_SUMMARY", Order = 0)]
        public List<MONETARY_EVENT_SUMMARY> MonetaryEventSummaryList { get; set; } = new List<MONETARY_EVENT_SUMMARY>();
    
        [XmlIgnore]
        public bool MonetaryEventSummaryListSpecified
        {
            get { return this.MonetaryEventSummaryList != null && this.MonetaryEventSummaryList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public MONETARY_EVENT_SUMMARIES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
