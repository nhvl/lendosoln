namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MONETARY_EVENT_SUMMARY_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SummaryMonetaryEventBorrowerIncentiveCurtailmentAmountSpecified
                    || this.SummaryMonetaryEventDeferredPrincipalCurtailmentAmountSpecified
                    || this.SummaryMonetaryEventDeferredUPBAmountSpecified
                    || this.SummaryMonetaryEventEscrowPaymentAmountSpecified
                    || this.SummaryMonetaryEventGrossInterestAmountSpecified
                    || this.SummaryMonetaryEventGrossPrincipalAmountSpecified
                    || this.SummaryMonetaryEventInterestBearingUPBAmountSpecified
                    || this.SummaryMonetaryEventInterestPaidThroughDateSpecified
                    || this.SummaryMonetaryEventInvestorSpecialHandlingDescriptionSpecified
                    || this.SummaryMonetaryEventLastPaidInstallmentAppliedDateSpecified
                    || this.SummaryMonetaryEventLastPaidInstallmentDueDateSpecified
                    || this.SummaryMonetaryEventNetInterestAmountSpecified
                    || this.SummaryMonetaryEventNetPrincipalAmountSpecified
                    || this.SummaryMonetaryEventOptionalProductsPaymentAmountSpecified
                    || this.SummaryMonetaryEventScheduledUPBAmountSpecified
                    || this.SummaryMonetaryEventUPBAmountSpecified
                    || this.SummaryMonetaryInvestorRemittanceAmountSpecified
                    || this.SummaryMonetaryInvestorRemittanceEffectiveDateSpecified
                    || this.SuspenseBalanceAmountSpecified
                    || this.UnremittedOptionalProductsPremiumBalanceAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SummaryMonetaryEventBorrowerIncentiveCurtailmentAmount", Order = 0)]
        public MISMOAmount SummaryMonetaryEventBorrowerIncentiveCurtailmentAmount { get; set; }
    
        [XmlIgnore]
        public bool SummaryMonetaryEventBorrowerIncentiveCurtailmentAmountSpecified
        {
            get { return this.SummaryMonetaryEventBorrowerIncentiveCurtailmentAmount != null; }
            set { }
        }
    
        [XmlElement("SummaryMonetaryEventDeferredPrincipalCurtailmentAmount", Order = 1)]
        public MISMOAmount SummaryMonetaryEventDeferredPrincipalCurtailmentAmount { get; set; }
    
        [XmlIgnore]
        public bool SummaryMonetaryEventDeferredPrincipalCurtailmentAmountSpecified
        {
            get { return this.SummaryMonetaryEventDeferredPrincipalCurtailmentAmount != null; }
            set { }
        }
    
        [XmlElement("SummaryMonetaryEventDeferredUPBAmount", Order = 2)]
        public MISMOAmount SummaryMonetaryEventDeferredUPBAmount { get; set; }
    
        [XmlIgnore]
        public bool SummaryMonetaryEventDeferredUPBAmountSpecified
        {
            get { return this.SummaryMonetaryEventDeferredUPBAmount != null; }
            set { }
        }
    
        [XmlElement("SummaryMonetaryEventEscrowPaymentAmount", Order = 3)]
        public MISMOAmount SummaryMonetaryEventEscrowPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool SummaryMonetaryEventEscrowPaymentAmountSpecified
        {
            get { return this.SummaryMonetaryEventEscrowPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("SummaryMonetaryEventGrossInterestAmount", Order = 4)]
        public MISMOAmount SummaryMonetaryEventGrossInterestAmount { get; set; }
    
        [XmlIgnore]
        public bool SummaryMonetaryEventGrossInterestAmountSpecified
        {
            get { return this.SummaryMonetaryEventGrossInterestAmount != null; }
            set { }
        }
    
        [XmlElement("SummaryMonetaryEventGrossPrincipalAmount", Order = 5)]
        public MISMOAmount SummaryMonetaryEventGrossPrincipalAmount { get; set; }
    
        [XmlIgnore]
        public bool SummaryMonetaryEventGrossPrincipalAmountSpecified
        {
            get { return this.SummaryMonetaryEventGrossPrincipalAmount != null; }
            set { }
        }
    
        [XmlElement("SummaryMonetaryEventInterestBearingUPBAmount", Order = 6)]
        public MISMOAmount SummaryMonetaryEventInterestBearingUPBAmount { get; set; }
    
        [XmlIgnore]
        public bool SummaryMonetaryEventInterestBearingUPBAmountSpecified
        {
            get { return this.SummaryMonetaryEventInterestBearingUPBAmount != null; }
            set { }
        }
    
        [XmlElement("SummaryMonetaryEventInterestPaidThroughDate", Order = 7)]
        public MISMODate SummaryMonetaryEventInterestPaidThroughDate { get; set; }
    
        [XmlIgnore]
        public bool SummaryMonetaryEventInterestPaidThroughDateSpecified
        {
            get { return this.SummaryMonetaryEventInterestPaidThroughDate != null; }
            set { }
        }
    
        [XmlElement("SummaryMonetaryEventInvestorSpecialHandlingDescription", Order = 8)]
        public MISMOString SummaryMonetaryEventInvestorSpecialHandlingDescription { get; set; }
    
        [XmlIgnore]
        public bool SummaryMonetaryEventInvestorSpecialHandlingDescriptionSpecified
        {
            get { return this.SummaryMonetaryEventInvestorSpecialHandlingDescription != null; }
            set { }
        }
    
        [XmlElement("SummaryMonetaryEventLastPaidInstallmentAppliedDate", Order = 9)]
        public MISMODate SummaryMonetaryEventLastPaidInstallmentAppliedDate { get; set; }
    
        [XmlIgnore]
        public bool SummaryMonetaryEventLastPaidInstallmentAppliedDateSpecified
        {
            get { return this.SummaryMonetaryEventLastPaidInstallmentAppliedDate != null; }
            set { }
        }
    
        [XmlElement("SummaryMonetaryEventLastPaidInstallmentDueDate", Order = 10)]
        public MISMODate SummaryMonetaryEventLastPaidInstallmentDueDate { get; set; }
    
        [XmlIgnore]
        public bool SummaryMonetaryEventLastPaidInstallmentDueDateSpecified
        {
            get { return this.SummaryMonetaryEventLastPaidInstallmentDueDate != null; }
            set { }
        }
    
        [XmlElement("SummaryMonetaryEventNetInterestAmount", Order = 11)]
        public MISMOAmount SummaryMonetaryEventNetInterestAmount { get; set; }
    
        [XmlIgnore]
        public bool SummaryMonetaryEventNetInterestAmountSpecified
        {
            get { return this.SummaryMonetaryEventNetInterestAmount != null; }
            set { }
        }
    
        [XmlElement("SummaryMonetaryEventNetPrincipalAmount", Order = 12)]
        public MISMOAmount SummaryMonetaryEventNetPrincipalAmount { get; set; }
    
        [XmlIgnore]
        public bool SummaryMonetaryEventNetPrincipalAmountSpecified
        {
            get { return this.SummaryMonetaryEventNetPrincipalAmount != null; }
            set { }
        }
    
        [XmlElement("SummaryMonetaryEventOptionalProductsPaymentAmount", Order = 13)]
        public MISMOAmount SummaryMonetaryEventOptionalProductsPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool SummaryMonetaryEventOptionalProductsPaymentAmountSpecified
        {
            get { return this.SummaryMonetaryEventOptionalProductsPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("SummaryMonetaryEventScheduledUPBAmount", Order = 14)]
        public MISMOAmount SummaryMonetaryEventScheduledUPBAmount { get; set; }
    
        [XmlIgnore]
        public bool SummaryMonetaryEventScheduledUPBAmountSpecified
        {
            get { return this.SummaryMonetaryEventScheduledUPBAmount != null; }
            set { }
        }
    
        [XmlElement("SummaryMonetaryEventUPBAmount", Order = 15)]
        public MISMOAmount SummaryMonetaryEventUPBAmount { get; set; }
    
        [XmlIgnore]
        public bool SummaryMonetaryEventUPBAmountSpecified
        {
            get { return this.SummaryMonetaryEventUPBAmount != null; }
            set { }
        }
    
        [XmlElement("SummaryMonetaryInvestorRemittanceAmount", Order = 16)]
        public MISMOAmount SummaryMonetaryInvestorRemittanceAmount { get; set; }
    
        [XmlIgnore]
        public bool SummaryMonetaryInvestorRemittanceAmountSpecified
        {
            get { return this.SummaryMonetaryInvestorRemittanceAmount != null; }
            set { }
        }
    
        [XmlElement("SummaryMonetaryInvestorRemittanceEffectiveDate", Order = 17)]
        public MISMODate SummaryMonetaryInvestorRemittanceEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool SummaryMonetaryInvestorRemittanceEffectiveDateSpecified
        {
            get { return this.SummaryMonetaryInvestorRemittanceEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("SuspenseBalanceAmount", Order = 18)]
        public MISMOAmount SuspenseBalanceAmount { get; set; }
    
        [XmlIgnore]
        public bool SuspenseBalanceAmountSpecified
        {
            get { return this.SuspenseBalanceAmount != null; }
            set { }
        }
    
        [XmlElement("UnremittedOptionalProductsPremiumBalanceAmount", Order = 19)]
        public MISMOAmount UnremittedOptionalProductsPremiumBalanceAmount { get; set; }
    
        [XmlIgnore]
        public bool UnremittedOptionalProductsPremiumBalanceAmountSpecified
        {
            get { return this.UnremittedOptionalProductsPremiumBalanceAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 20)]
        public MONETARY_EVENT_SUMMARY_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
