namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class NEGATIVE_AMORTIZATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.NegativeAmortizationOccurrencesSpecified
                    || this.NegativeAmortizationRuleSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("NEGATIVE_AMORTIZATION_OCCURRENCES", Order = 0)]
        public NEGATIVE_AMORTIZATION_OCCURRENCES NegativeAmortizationOccurrences { get; set; }
    
        [XmlIgnore]
        public bool NegativeAmortizationOccurrencesSpecified
        {
            get { return this.NegativeAmortizationOccurrences != null && this.NegativeAmortizationOccurrences.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("NEGATIVE_AMORTIZATION_RULE", Order = 1)]
        public NEGATIVE_AMORTIZATION_RULE NegativeAmortizationRule { get; set; }
    
        [XmlIgnore]
        public bool NegativeAmortizationRuleSpecified
        {
            get { return this.NegativeAmortizationRule != null && this.NegativeAmortizationRule.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public NEGATIVE_AMORTIZATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
