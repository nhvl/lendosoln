namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class PROJECTED_PAYMENTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ProjectedPaymentListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PROJECTED_PAYMENT", Order = 0)]
        public List<PROJECTED_PAYMENT> ProjectedPaymentList { get; set; } = new List<PROJECTED_PAYMENT>();
    
        [XmlIgnore]
        public bool ProjectedPaymentListSpecified
        {
            get { return this.ProjectedPaymentList != null && this.ProjectedPaymentList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public PROJECTED_PAYMENTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
