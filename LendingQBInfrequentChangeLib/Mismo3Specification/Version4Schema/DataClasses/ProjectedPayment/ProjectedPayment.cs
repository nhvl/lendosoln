namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PROJECTED_PAYMENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PaymentFrequencyTypeSpecified
                    || this.PaymentFrequencyTypeOtherDescriptionSpecified
                    || this.ProjectedPaymentCalculationPeriodEndNumberSpecified
                    || this.ProjectedPaymentCalculationPeriodStartNumberSpecified
                    || this.ProjectedPaymentCalculationPeriodTermTypeSpecified
                    || this.ProjectedPaymentCalculationPeriodTermTypeOtherDescriptionSpecified
                    || this.ProjectedPaymentEstimatedEscrowPaymentAmountSpecified
                    || this.ProjectedPaymentEstimatedTotalMaximumPaymentAmountSpecified
                    || this.ProjectedPaymentEstimatedTotalMinimumPaymentAmountSpecified
                    || this.ProjectedPaymentMIPaymentAmountSpecified
                    || this.ProjectedPaymentPrincipalAndInterestMaximumPaymentAmountSpecified
                    || this.ProjectedPaymentPrincipalAndInterestMinimumPaymentAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PaymentFrequencyType", Order = 0)]
        public MISMOEnum<PaymentFrequencyBase> PaymentFrequencyType { get; set; }
    
        [XmlIgnore]
        public bool PaymentFrequencyTypeSpecified
        {
            get { return this.PaymentFrequencyType != null; }
            set { }
        }
    
        [XmlElement("PaymentFrequencyTypeOtherDescription", Order = 1)]
        public MISMOString PaymentFrequencyTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PaymentFrequencyTypeOtherDescriptionSpecified
        {
            get { return this.PaymentFrequencyTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ProjectedPaymentCalculationPeriodEndNumber", Order = 2)]
        public MISMONumeric ProjectedPaymentCalculationPeriodEndNumber { get; set; }
    
        [XmlIgnore]
        public bool ProjectedPaymentCalculationPeriodEndNumberSpecified
        {
            get { return this.ProjectedPaymentCalculationPeriodEndNumber != null; }
            set { }
        }
    
        [XmlElement("ProjectedPaymentCalculationPeriodStartNumber", Order = 3)]
        public MISMONumeric ProjectedPaymentCalculationPeriodStartNumber { get; set; }
    
        [XmlIgnore]
        public bool ProjectedPaymentCalculationPeriodStartNumberSpecified
        {
            get { return this.ProjectedPaymentCalculationPeriodStartNumber != null; }
            set { }
        }
    
        [XmlElement("ProjectedPaymentCalculationPeriodTermType", Order = 4)]
        public MISMOEnum<ProjectedPaymentCalculationPeriodTermBase> ProjectedPaymentCalculationPeriodTermType { get; set; }
    
        [XmlIgnore]
        public bool ProjectedPaymentCalculationPeriodTermTypeSpecified
        {
            get { return this.ProjectedPaymentCalculationPeriodTermType != null; }
            set { }
        }
    
        [XmlElement("ProjectedPaymentCalculationPeriodTermTypeOtherDescription", Order = 5)]
        public MISMOString ProjectedPaymentCalculationPeriodTermTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ProjectedPaymentCalculationPeriodTermTypeOtherDescriptionSpecified
        {
            get { return this.ProjectedPaymentCalculationPeriodTermTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ProjectedPaymentEstimatedEscrowPaymentAmount", Order = 6)]
        public MISMOAmount ProjectedPaymentEstimatedEscrowPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool ProjectedPaymentEstimatedEscrowPaymentAmountSpecified
        {
            get { return this.ProjectedPaymentEstimatedEscrowPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("ProjectedPaymentEstimatedTotalMaximumPaymentAmount", Order = 7)]
        public MISMOAmount ProjectedPaymentEstimatedTotalMaximumPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool ProjectedPaymentEstimatedTotalMaximumPaymentAmountSpecified
        {
            get { return this.ProjectedPaymentEstimatedTotalMaximumPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("ProjectedPaymentEstimatedTotalMinimumPaymentAmount", Order = 8)]
        public MISMOAmount ProjectedPaymentEstimatedTotalMinimumPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool ProjectedPaymentEstimatedTotalMinimumPaymentAmountSpecified
        {
            get { return this.ProjectedPaymentEstimatedTotalMinimumPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("ProjectedPaymentMIPaymentAmount", Order = 9)]
        public MISMOAmount ProjectedPaymentMIPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool ProjectedPaymentMIPaymentAmountSpecified
        {
            get { return this.ProjectedPaymentMIPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("ProjectedPaymentPrincipalAndInterestMaximumPaymentAmount", Order = 10)]
        public MISMOAmount ProjectedPaymentPrincipalAndInterestMaximumPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool ProjectedPaymentPrincipalAndInterestMaximumPaymentAmountSpecified
        {
            get { return this.ProjectedPaymentPrincipalAndInterestMaximumPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("ProjectedPaymentPrincipalAndInterestMinimumPaymentAmount", Order = 11)]
        public MISMOAmount ProjectedPaymentPrincipalAndInterestMinimumPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool ProjectedPaymentPrincipalAndInterestMinimumPaymentAmountSpecified
        {
            get { return this.ProjectedPaymentPrincipalAndInterestMinimumPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 12)]
        public PROJECTED_PAYMENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
