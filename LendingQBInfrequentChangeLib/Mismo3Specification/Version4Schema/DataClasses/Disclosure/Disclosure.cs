namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class DISCLOSURE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DisclosureDetailSpecified
                    || this.DisclosureReasonsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DISCLOSURE_DETAIL", Order = 0)]
        public DISCLOSURE_DETAIL DisclosureDetail { get; set; }
    
        [XmlIgnore]
        public bool DisclosureDetailSpecified
        {
            get { return this.DisclosureDetail != null && this.DisclosureDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("DISCLOSURE_REASONS", Order = 1)]
        public DISCLOSURE_REASONS DisclosureReasons { get; set; }
    
        [XmlIgnore]
        public bool DisclosureReasonsSpecified
        {
            get { return this.DisclosureReasons != null && this.DisclosureReasons.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public DISCLOSURE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
