namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class DISCLOSURE_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DisclosureDeliveryMethodTypeSpecified
                    || this.DisclosureDeliveryMethodTypeOtherDescriptionSpecified
                    || this.DisclosureDueDateSpecified
                    || this.DisclosureIssuanceTimingTypeSpecified
                    || this.DisclosureIssuanceTypeSpecified
                    || this.DisclosureIssuanceTypeOtherDescriptionSpecified
                    || this.DisclosureIssuedDateSpecified
                    || this.DisclosureReceivedDateSpecified
                    || this.DocumentTypeSpecified
                    || this.DocumentTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DisclosureDeliveryMethodType", Order = 0)]
        public MISMOEnum<DisclosureDeliveryMethodBase> DisclosureDeliveryMethodType { get; set; }
    
        [XmlIgnore]
        public bool DisclosureDeliveryMethodTypeSpecified
        {
            get { return this.DisclosureDeliveryMethodType != null; }
            set { }
        }
    
        [XmlElement("DisclosureDeliveryMethodTypeOtherDescription", Order = 1)]
        public MISMOString DisclosureDeliveryMethodTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool DisclosureDeliveryMethodTypeOtherDescriptionSpecified
        {
            get { return this.DisclosureDeliveryMethodTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("DisclosureDueDate", Order = 2)]
        public MISMODate DisclosureDueDate { get; set; }
    
        [XmlIgnore]
        public bool DisclosureDueDateSpecified
        {
            get { return this.DisclosureDueDate != null; }
            set { }
        }
    
        [XmlElement("DisclosureIssuanceTimingType", Order = 3)]
        public MISMOEnum<DisclosureIssuanceTimingBase> DisclosureIssuanceTimingType { get; set; }
    
        [XmlIgnore]
        public bool DisclosureIssuanceTimingTypeSpecified
        {
            get { return this.DisclosureIssuanceTimingType != null; }
            set { }
        }
    
        [XmlElement("DisclosureIssuanceType", Order = 4)]
        public MISMOEnum<DisclosureIssuanceBase> DisclosureIssuanceType { get; set; }
    
        [XmlIgnore]
        public bool DisclosureIssuanceTypeSpecified
        {
            get { return this.DisclosureIssuanceType != null; }
            set { }
        }
    
        [XmlElement("DisclosureIssuanceTypeOtherDescription", Order = 5)]
        public MISMOString DisclosureIssuanceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool DisclosureIssuanceTypeOtherDescriptionSpecified
        {
            get { return this.DisclosureIssuanceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("DisclosureIssuedDate", Order = 6)]
        public MISMODate DisclosureIssuedDate { get; set; }
    
        [XmlIgnore]
        public bool DisclosureIssuedDateSpecified
        {
            get { return this.DisclosureIssuedDate != null; }
            set { }
        }
    
        [XmlElement("DisclosureReceivedDate", Order = 7)]
        public MISMODate DisclosureReceivedDate { get; set; }
    
        [XmlIgnore]
        public bool DisclosureReceivedDateSpecified
        {
            get { return this.DisclosureReceivedDate != null; }
            set { }
        }
    
        [XmlElement("DocumentType", Order = 8)]
        public MISMOEnum<DocumentBase> DocumentType { get; set; }
    
        [XmlIgnore]
        public bool DocumentTypeSpecified
        {
            get { return this.DocumentType != null; }
            set { }
        }
    
        [XmlElement("DocumentTypeOtherDescription", Order = 9)]
        public MISMOString DocumentTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool DocumentTypeOtherDescriptionSpecified
        {
            get { return this.DocumentTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 10)]
        public DISCLOSURE_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
