namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class DISCLOSURES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DisclosureListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DISCLOSURE", Order = 0)]
        public List<DISCLOSURE> DisclosureList { get; set; } = new List<DISCLOSURE>();
    
        [XmlIgnore]
        public bool DisclosureListSpecified
        {
            get { return this.DisclosureList != null && this.DisclosureList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public DISCLOSURES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
