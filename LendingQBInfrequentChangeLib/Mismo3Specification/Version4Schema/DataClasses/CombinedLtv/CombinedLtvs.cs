namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class COMBINED_LTVS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CombinedLtvListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("COMBINED_LTV", Order = 0)]
        public List<COMBINED_LTV> CombinedLtvList { get; set; } = new List<COMBINED_LTV>();
    
        [XmlIgnore]
        public bool CombinedLtvListSpecified
        {
            get { return this.CombinedLtvList != null && this.CombinedLtvList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public COMBINED_LTVS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
