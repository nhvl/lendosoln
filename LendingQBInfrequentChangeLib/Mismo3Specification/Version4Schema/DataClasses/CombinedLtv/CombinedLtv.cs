namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class COMBINED_LTV
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CombinedLTVRatioPercentSpecified
                    || this.HomeEquityCombinedLTVRatioPercentSpecified
                    || this.LTVCalculationDateSpecified
                    || this.OriginalLTVIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CombinedLTVRatioPercent", Order = 0)]
        public MISMOPercent CombinedLTVRatioPercent { get; set; }
    
        [XmlIgnore]
        public bool CombinedLTVRatioPercentSpecified
        {
            get { return this.CombinedLTVRatioPercent != null; }
            set { }
        }
    
        [XmlElement("HomeEquityCombinedLTVRatioPercent", Order = 1)]
        public MISMOPercent HomeEquityCombinedLTVRatioPercent { get; set; }
    
        [XmlIgnore]
        public bool HomeEquityCombinedLTVRatioPercentSpecified
        {
            get { return this.HomeEquityCombinedLTVRatioPercent != null; }
            set { }
        }
    
        [XmlElement("LTVCalculationDate", Order = 2)]
        public MISMODate LTVCalculationDate { get; set; }
    
        [XmlIgnore]
        public bool LTVCalculationDateSpecified
        {
            get { return this.LTVCalculationDate != null; }
            set { }
        }
    
        [XmlElement("OriginalLTVIndicator", Order = 3)]
        public MISMOIndicator OriginalLTVIndicator { get; set; }
    
        [XmlIgnore]
        public bool OriginalLTVIndicatorSpecified
        {
            get { return this.OriginalLTVIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public COMBINED_LTV_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
