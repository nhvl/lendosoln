namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SALES_COMPARISON_APPROACH
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SalesComparisonCommentDescriptionSpecified
                    || this.SalesComparisonValuePerBedroomAmountSpecified
                    || this.SalesComparisonValuePerGrossBuildingAreaAmountSpecified
                    || this.SalesComparisonValuePerRoomAmountSpecified
                    || this.SalesComparisonValuePerUnitAmountSpecified
                    || this.ValueIndicatedBySalesComparisonAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SalesComparisonCommentDescription", Order = 0)]
        public MISMOString SalesComparisonCommentDescription { get; set; }
    
        [XmlIgnore]
        public bool SalesComparisonCommentDescriptionSpecified
        {
            get { return this.SalesComparisonCommentDescription != null; }
            set { }
        }
    
        [XmlElement("SalesComparisonValuePerBedroomAmount", Order = 1)]
        public MISMOAmount SalesComparisonValuePerBedroomAmount { get; set; }
    
        [XmlIgnore]
        public bool SalesComparisonValuePerBedroomAmountSpecified
        {
            get { return this.SalesComparisonValuePerBedroomAmount != null; }
            set { }
        }
    
        [XmlElement("SalesComparisonValuePerGrossBuildingAreaAmount", Order = 2)]
        public MISMOAmount SalesComparisonValuePerGrossBuildingAreaAmount { get; set; }
    
        [XmlIgnore]
        public bool SalesComparisonValuePerGrossBuildingAreaAmountSpecified
        {
            get { return this.SalesComparisonValuePerGrossBuildingAreaAmount != null; }
            set { }
        }
    
        [XmlElement("SalesComparisonValuePerRoomAmount", Order = 3)]
        public MISMOAmount SalesComparisonValuePerRoomAmount { get; set; }
    
        [XmlIgnore]
        public bool SalesComparisonValuePerRoomAmountSpecified
        {
            get { return this.SalesComparisonValuePerRoomAmount != null; }
            set { }
        }
    
        [XmlElement("SalesComparisonValuePerUnitAmount", Order = 4)]
        public MISMOAmount SalesComparisonValuePerUnitAmount { get; set; }
    
        [XmlIgnore]
        public bool SalesComparisonValuePerUnitAmountSpecified
        {
            get { return this.SalesComparisonValuePerUnitAmount != null; }
            set { }
        }
    
        [XmlElement("ValueIndicatedBySalesComparisonAmount", Order = 5)]
        public MISMOAmount ValueIndicatedBySalesComparisonAmount { get; set; }
    
        [XmlIgnore]
        public bool ValueIndicatedBySalesComparisonAmountSpecified
        {
            get { return this.ValueIndicatedBySalesComparisonAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 6)]
        public SALES_COMPARISON_APPROACH_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
