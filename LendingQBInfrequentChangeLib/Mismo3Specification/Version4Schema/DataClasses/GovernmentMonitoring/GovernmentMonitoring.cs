namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class GOVERNMENT_MONITORING
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.GovernmentMonitoringDetailSpecified
                    || this.HmdaEthnicityOriginsSpecified
                    || this.HmdaRacesSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("GOVERNMENT_MONITORING_DETAIL", Order = 0)]
        public GOVERNMENT_MONITORING_DETAIL GovernmentMonitoringDetail { get; set; }
    
        [XmlIgnore]
        public bool GovernmentMonitoringDetailSpecified
        {
            get { return this.GovernmentMonitoringDetail != null && this.GovernmentMonitoringDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("HMDA_ETHNICITY_ORIGINS", Order = 1)]
        public HMDA_ETHNICITY_ORIGINS HmdaEthnicityOrigins { get; set; }
    
        [XmlIgnore]
        public bool HmdaEthnicityOriginsSpecified
        {
            get { return this.HmdaEthnicityOrigins != null && this.HmdaEthnicityOrigins.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("HMDA_RACES", Order = 2)]
        public HMDA_RACES HmdaRaces { get; set; }
    
        [XmlIgnore]
        public bool HmdaRacesSpecified
        {
            get { return this.HmdaRaces != null && this.HmdaRaces.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public GOVERNMENT_MONITORING_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
