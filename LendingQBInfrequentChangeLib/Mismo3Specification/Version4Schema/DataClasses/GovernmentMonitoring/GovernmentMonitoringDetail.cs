namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class GOVERNMENT_MONITORING_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.GenderTypeSpecified
                    || this.HMDAEthnicityCollectedBasedOnVisualObservationOrSurnameIndicatorSpecified
                    || this.HMDAEthnicityRefusalIndicatorSpecified
                    || this.HMDAEthnicityTypeSpecified
                    || this.HMDAGenderCollectedBasedOnVisualObservationOrNameIndicatorSpecified
                    || this.HMDAGenderRefusalIndicatorSpecified
                    || this.HMDARaceCollectedBasedOnVisualObservationOrSurnameIndicatorSpecified
                    || this.HMDARaceRefusalIndicatorSpecified
                    || this.HMDARefusalIndicatorSpecified
                    || this.RaceNationalOriginRefusalIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("GenderType", Order = 0)]
        public MISMOEnum<GenderBase> GenderType { get; set; }
    
        [XmlIgnore]
        public bool GenderTypeSpecified
        {
            get { return this.GenderType != null; }
            set { }
        }
    
        [XmlElement("HMDAEthnicityCollectedBasedOnVisualObservationOrSurnameIndicator", Order = 1)]
        public MISMOIndicator HMDAEthnicityCollectedBasedOnVisualObservationOrSurnameIndicator { get; set; }
    
        [XmlIgnore]
        public bool HMDAEthnicityCollectedBasedOnVisualObservationOrSurnameIndicatorSpecified
        {
            get { return this.HMDAEthnicityCollectedBasedOnVisualObservationOrSurnameIndicator != null; }
            set { }
        }
    
        [XmlElement("HMDAEthnicityRefusalIndicator", Order = 2)]
        public MISMOIndicator HMDAEthnicityRefusalIndicator { get; set; }
    
        [XmlIgnore]
        public bool HMDAEthnicityRefusalIndicatorSpecified
        {
            get { return this.HMDAEthnicityRefusalIndicator != null; }
            set { }
        }
    
        [XmlElement("HMDAEthnicityType", Order = 3)]
        public MISMOEnum<HMDAEthnicityBase> HMDAEthnicityType { get; set; }
    
        [XmlIgnore]
        public bool HMDAEthnicityTypeSpecified
        {
            get { return this.HMDAEthnicityType != null; }
            set { }
        }
    
        [XmlElement("HMDAGenderCollectedBasedOnVisualObservationOrNameIndicator", Order = 4)]
        public MISMOIndicator HMDAGenderCollectedBasedOnVisualObservationOrNameIndicator { get; set; }
    
        [XmlIgnore]
        public bool HMDAGenderCollectedBasedOnVisualObservationOrNameIndicatorSpecified
        {
            get { return this.HMDAGenderCollectedBasedOnVisualObservationOrNameIndicator != null; }
            set { }
        }
    
        [XmlElement("HMDAGenderRefusalIndicator", Order = 5)]
        public MISMOIndicator HMDAGenderRefusalIndicator { get; set; }
    
        [XmlIgnore]
        public bool HMDAGenderRefusalIndicatorSpecified
        {
            get { return this.HMDAGenderRefusalIndicator != null; }
            set { }
        }
    
        [XmlElement("HMDARaceCollectedBasedOnVisualObservationOrSurnameIndicator", Order = 6)]
        public MISMOIndicator HMDARaceCollectedBasedOnVisualObservationOrSurnameIndicator { get; set; }
    
        [XmlIgnore]
        public bool HMDARaceCollectedBasedOnVisualObservationOrSurnameIndicatorSpecified
        {
            get { return this.HMDARaceCollectedBasedOnVisualObservationOrSurnameIndicator != null; }
            set { }
        }
    
        [XmlElement("HMDARaceRefusalIndicator", Order = 7)]
        public MISMOIndicator HMDARaceRefusalIndicator { get; set; }
    
        [XmlIgnore]
        public bool HMDARaceRefusalIndicatorSpecified
        {
            get { return this.HMDARaceRefusalIndicator != null; }
            set { }
        }
    
        [XmlElement("HMDARefusalIndicator", Order = 8)]
        public MISMOIndicator HMDARefusalIndicator { get; set; }
    
        [XmlIgnore]
        public bool HMDARefusalIndicatorSpecified
        {
            get { return this.HMDARefusalIndicator != null; }
            set { }
        }
    
        [XmlElement("RaceNationalOriginRefusalIndicator", Order = 9)]
        public MISMOIndicator RaceNationalOriginRefusalIndicator { get; set; }
    
        [XmlIgnore]
        public bool RaceNationalOriginRefusalIndicatorSpecified
        {
            get { return this.RaceNationalOriginRefusalIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 10)]
        public GOVERNMENT_MONITORING_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
