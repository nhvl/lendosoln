namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class COMPARABLE_ADJUSTMENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ComparableAdjustmentAmountSpecified
                    || this.ComparableAdjustmentDescriptionSpecified
                    || this.ComparableAdjustmentTypeSpecified
                    || this.ComparableAdjustmentTypeOtherDescriptionSpecified
                    || this.SalesComparisonUnitOfMeasureDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ComparableAdjustmentAmount", Order = 0)]
        public MISMOAmount ComparableAdjustmentAmount { get; set; }
    
        [XmlIgnore]
        public bool ComparableAdjustmentAmountSpecified
        {
            get { return this.ComparableAdjustmentAmount != null; }
            set { }
        }
    
        [XmlElement("ComparableAdjustmentDescription", Order = 1)]
        public MISMOString ComparableAdjustmentDescription { get; set; }
    
        [XmlIgnore]
        public bool ComparableAdjustmentDescriptionSpecified
        {
            get { return this.ComparableAdjustmentDescription != null; }
            set { }
        }
    
        [XmlElement("ComparableAdjustmentType", Order = 2)]
        public MISMOEnum<ComparableAdjustmentBase> ComparableAdjustmentType { get; set; }
    
        [XmlIgnore]
        public bool ComparableAdjustmentTypeSpecified
        {
            get { return this.ComparableAdjustmentType != null; }
            set { }
        }
    
        [XmlElement("ComparableAdjustmentTypeOtherDescription", Order = 3)]
        public MISMOString ComparableAdjustmentTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ComparableAdjustmentTypeOtherDescriptionSpecified
        {
            get { return this.ComparableAdjustmentTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("SalesComparisonUnitOfMeasureDescription", Order = 4)]
        public MISMOString SalesComparisonUnitOfMeasureDescription { get; set; }
    
        [XmlIgnore]
        public bool SalesComparisonUnitOfMeasureDescriptionSpecified
        {
            get { return this.SalesComparisonUnitOfMeasureDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public COMPARABLE_ADJUSTMENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
