namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class COMPARABLE_ADJUSTMENTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ComparableAdjustmentListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("COMPARABLE_ADJUSTMENT", Order = 0)]
        public List<COMPARABLE_ADJUSTMENT> ComparableAdjustmentList { get; set; } = new List<COMPARABLE_ADJUSTMENT>();
    
        [XmlIgnore]
        public bool ComparableAdjustmentListSpecified
        {
            get { return this.ComparableAdjustmentList != null && this.ComparableAdjustmentList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public COMPARABLE_ADJUSTMENTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
