namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class HOLDBACK_ITEM
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EscrowHoldbackAmountSpecified
                    || this.EscrowHoldbackTypeSpecified
                    || this.EscrowHoldbackTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("EscrowHoldbackAmount", Order = 0)]
        public MISMOAmount EscrowHoldbackAmount { get; set; }
    
        [XmlIgnore]
        public bool EscrowHoldbackAmountSpecified
        {
            get { return this.EscrowHoldbackAmount != null; }
            set { }
        }
    
        [XmlElement("EscrowHoldbackType", Order = 1)]
        public MISMOEnum<EscrowHoldbackBase> EscrowHoldbackType { get; set; }
    
        [XmlIgnore]
        public bool EscrowHoldbackTypeSpecified
        {
            get { return this.EscrowHoldbackType != null; }
            set { }
        }
    
        [XmlElement("EscrowHoldbackTypeOtherDescription", Order = 2)]
        public MISMOString EscrowHoldbackTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool EscrowHoldbackTypeOtherDescriptionSpecified
        {
            get { return this.EscrowHoldbackTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public HOLDBACK_ITEM_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
