namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class HOLDBACK_ITEMS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.HoldbackItemListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("HOLDBACK_ITEM", Order = 0)]
        public List<HOLDBACK_ITEM> HoldbackItemList { get; set; } = new List<HOLDBACK_ITEM>();
    
        [XmlIgnore]
        public bool HoldbackItemListSpecified
        {
            get { return this.HoldbackItemList != null && this.HoldbackItemList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public HOLDBACK_ITEMS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
