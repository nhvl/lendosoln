namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class PROJECT_RATINGS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ProjectRatingListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PROJECT_RATING", Order = 0)]
        public List<PROJECT_RATING> ProjectRatingList { get; set; } = new List<PROJECT_RATING>();
    
        [XmlIgnore]
        public bool ProjectRatingListSpecified
        {
            get { return this.ProjectRatingList != null && this.ProjectRatingList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public PROJECT_RATINGS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
