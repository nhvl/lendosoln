namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PROJECT_RATING
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ConditionRatingTypeSpecified
                    || this.ProjectAspectTypeSpecified
                    || this.ProjectAspectTypeOtherDescriptionSpecified
                    || this.ProjectRatingCommentDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ConditionRatingType", Order = 0)]
        public MISMOEnum<ConditionRatingBase> ConditionRatingType { get; set; }
    
        [XmlIgnore]
        public bool ConditionRatingTypeSpecified
        {
            get { return this.ConditionRatingType != null; }
            set { }
        }
    
        [XmlElement("ProjectAspectType", Order = 1)]
        public MISMOEnum<ProjectAspectBase> ProjectAspectType { get; set; }
    
        [XmlIgnore]
        public bool ProjectAspectTypeSpecified
        {
            get { return this.ProjectAspectType != null; }
            set { }
        }
    
        [XmlElement("ProjectAspectTypeOtherDescription", Order = 2)]
        public MISMOString ProjectAspectTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ProjectAspectTypeOtherDescriptionSpecified
        {
            get { return this.ProjectAspectTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ProjectRatingCommentDescription", Order = 3)]
        public MISMOString ProjectRatingCommentDescription { get; set; }
    
        [XmlIgnore]
        public bool ProjectRatingCommentDescriptionSpecified
        {
            get { return this.ProjectRatingCommentDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public PROJECT_RATING_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
