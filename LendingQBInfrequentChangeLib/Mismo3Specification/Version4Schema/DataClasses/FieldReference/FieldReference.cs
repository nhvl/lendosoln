namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class FIELD_REFERENCE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.FieldHeightNumberSpecified
                    || this.FieldPlaceholderIdentifierSpecified
                    || this.FieldWidthNumberSpecified
                    || this.MeasurementUnitTypeSpecified
                    || this.OffsetFromLeftNumberSpecified
                    || this.OffsetFromTopNumberSpecified
                    || this.PageNumberValueSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("FieldHeightNumber", Order = 0)]
        public MISMONumeric FieldHeightNumber { get; set; }
    
        [XmlIgnore]
        public bool FieldHeightNumberSpecified
        {
            get { return this.FieldHeightNumber != null; }
            set { }
        }
    
        [XmlElement("FieldPlaceholderIdentifier", Order = 1)]
        public MISMOIdentifier FieldPlaceholderIdentifier { get; set; }
    
        [XmlIgnore]
        public bool FieldPlaceholderIdentifierSpecified
        {
            get { return this.FieldPlaceholderIdentifier != null; }
            set { }
        }
    
        [XmlElement("FieldWidthNumber", Order = 2)]
        public MISMONumeric FieldWidthNumber { get; set; }
    
        [XmlIgnore]
        public bool FieldWidthNumberSpecified
        {
            get { return this.FieldWidthNumber != null; }
            set { }
        }
    
        [XmlElement("MeasurementUnitType", Order = 3)]
        public MISMOEnum<MeasurementUnitBase> MeasurementUnitType { get; set; }
    
        [XmlIgnore]
        public bool MeasurementUnitTypeSpecified
        {
            get { return this.MeasurementUnitType != null; }
            set { }
        }
    
        [XmlElement("OffsetFromLeftNumber", Order = 4)]
        public MISMONumeric OffsetFromLeftNumber { get; set; }
    
        [XmlIgnore]
        public bool OffsetFromLeftNumberSpecified
        {
            get { return this.OffsetFromLeftNumber != null; }
            set { }
        }
    
        [XmlElement("OffsetFromTopNumber", Order = 5)]
        public MISMONumeric OffsetFromTopNumber { get; set; }
    
        [XmlIgnore]
        public bool OffsetFromTopNumberSpecified
        {
            get { return this.OffsetFromTopNumber != null; }
            set { }
        }
    
        [XmlElement("PageNumberValue", Order = 6)]
        public MISMOValue PageNumberValue { get; set; }
    
        [XmlIgnore]
        public bool PageNumberValueSpecified
        {
            get { return this.PageNumberValue != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 7)]
        public FIELD_REFERENCE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
