namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class GFE_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.GFECreditOrChargeForChosenInterestRateTypeSpecified
                    || this.GFECreditOrChargeForChosenInterestRateTypeOtherDescriptionSpecified
                    || this.GFEDisclosureDateSpecified
                    || this.GFEInterestRateAvailableThroughDateSpecified
                    || this.GFELoanOriginatorFeePaymentCreditTypeSpecified
                    || this.GFELoanOriginatorFeePaymentCreditTypeOtherDescriptionSpecified
                    || this.GFEMonthlyPaymentFirstIncreasePaymentAmountSpecified
                    || this.GFEMonthlyPaymentMaximumAmountSpecified
                    || this.GFERateLockMinimumDaysPriorToSettlementCountSpecified
                    || this.GFERateLockPeriodDaysCountSpecified
                    || this.GFERedisclosureReasonDateSpecified
                    || this.GFERedisclosureReasonDescriptionSpecified
                    || this.GFESettlementChargesAvailableThroughDateSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("GFECreditOrChargeForChosenInterestRateType", Order = 0)]
        public MISMOEnum<GFECreditOrChargeForChosenInterestRateBase> GFECreditOrChargeForChosenInterestRateType { get; set; }
    
        [XmlIgnore]
        public bool GFECreditOrChargeForChosenInterestRateTypeSpecified
        {
            get { return this.GFECreditOrChargeForChosenInterestRateType != null; }
            set { }
        }
    
        [XmlElement("GFECreditOrChargeForChosenInterestRateTypeOtherDescription", Order = 1)]
        public MISMOString GFECreditOrChargeForChosenInterestRateTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool GFECreditOrChargeForChosenInterestRateTypeOtherDescriptionSpecified
        {
            get { return this.GFECreditOrChargeForChosenInterestRateTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("GFEDisclosureDate", Order = 2)]
        public MISMODate GFEDisclosureDate { get; set; }
    
        [XmlIgnore]
        public bool GFEDisclosureDateSpecified
        {
            get { return this.GFEDisclosureDate != null; }
            set { }
        }
    
        [XmlElement("GFEInterestRateAvailableThroughDate", Order = 3)]
        public MISMODate GFEInterestRateAvailableThroughDate { get; set; }
    
        [XmlIgnore]
        public bool GFEInterestRateAvailableThroughDateSpecified
        {
            get { return this.GFEInterestRateAvailableThroughDate != null; }
            set { }
        }
    
        [XmlElement("GFELoanOriginatorFeePaymentCreditType", Order = 4)]
        public MISMOEnum<GFELoanOriginatorFeePaymentCreditBase> GFELoanOriginatorFeePaymentCreditType { get; set; }
    
        [XmlIgnore]
        public bool GFELoanOriginatorFeePaymentCreditTypeSpecified
        {
            get { return this.GFELoanOriginatorFeePaymentCreditType != null; }
            set { }
        }
    
        [XmlElement("GFELoanOriginatorFeePaymentCreditTypeOtherDescription", Order = 5)]
        public MISMOString GFELoanOriginatorFeePaymentCreditTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool GFELoanOriginatorFeePaymentCreditTypeOtherDescriptionSpecified
        {
            get { return this.GFELoanOriginatorFeePaymentCreditTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("GFEMonthlyPaymentFirstIncreasePaymentAmount", Order = 6)]
        public MISMOAmount GFEMonthlyPaymentFirstIncreasePaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool GFEMonthlyPaymentFirstIncreasePaymentAmountSpecified
        {
            get { return this.GFEMonthlyPaymentFirstIncreasePaymentAmount != null; }
            set { }
        }
    
        [XmlElement("GFEMonthlyPaymentMaximumAmount", Order = 7)]
        public MISMOAmount GFEMonthlyPaymentMaximumAmount { get; set; }
    
        [XmlIgnore]
        public bool GFEMonthlyPaymentMaximumAmountSpecified
        {
            get { return this.GFEMonthlyPaymentMaximumAmount != null; }
            set { }
        }
    
        [XmlElement("GFERateLockMinimumDaysPriorToSettlementCount", Order = 8)]
        public MISMOCount GFERateLockMinimumDaysPriorToSettlementCount { get; set; }
    
        [XmlIgnore]
        public bool GFERateLockMinimumDaysPriorToSettlementCountSpecified
        {
            get { return this.GFERateLockMinimumDaysPriorToSettlementCount != null; }
            set { }
        }
    
        [XmlElement("GFERateLockPeriodDaysCount", Order = 9)]
        public MISMOCount GFERateLockPeriodDaysCount { get; set; }
    
        [XmlIgnore]
        public bool GFERateLockPeriodDaysCountSpecified
        {
            get { return this.GFERateLockPeriodDaysCount != null; }
            set { }
        }
    
        [XmlElement("GFERedisclosureReasonDate", Order = 10)]
        public MISMODate GFERedisclosureReasonDate { get; set; }
    
        [XmlIgnore]
        public bool GFERedisclosureReasonDateSpecified
        {
            get { return this.GFERedisclosureReasonDate != null; }
            set { }
        }
    
        [XmlElement("GFERedisclosureReasonDescription", Order = 11)]
        public MISMOString GFERedisclosureReasonDescription { get; set; }
    
        [XmlIgnore]
        public bool GFERedisclosureReasonDescriptionSpecified
        {
            get { return this.GFERedisclosureReasonDescription != null; }
            set { }
        }
    
        [XmlElement("GFESettlementChargesAvailableThroughDate", Order = 12)]
        public MISMODate GFESettlementChargesAvailableThroughDate { get; set; }
    
        [XmlIgnore]
        public bool GFESettlementChargesAvailableThroughDateSpecified
        {
            get { return this.GFESettlementChargesAvailableThroughDate != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 13)]
        public GFE_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
