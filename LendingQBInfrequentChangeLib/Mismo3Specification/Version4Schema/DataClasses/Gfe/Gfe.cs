namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class GFE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.GfeDetailSpecified
                    || this.GfeSectionSummariesSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("GFE_DETAIL", Order = 0)]
        public GFE_DETAIL GfeDetail { get; set; }
    
        [XmlIgnore]
        public bool GfeDetailSpecified
        {
            get { return this.GfeDetail != null && this.GfeDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("GFE_SECTION_SUMMARIES", Order = 1)]
        public GFE_SECTION_SUMMARIES GfeSectionSummaries { get; set; }
    
        [XmlIgnore]
        public bool GfeSectionSummariesSpecified
        {
            get { return this.GfeSectionSummaries != null && this.GfeSectionSummaries.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public GFE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
