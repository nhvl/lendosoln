namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class CREDIT_COMMENTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditCommentListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CREDIT_COMMENT", Order = 0)]
        public List<CREDIT_COMMENT> CreditCommentList { get; set; } = new List<CREDIT_COMMENT>();
    
        [XmlIgnore]
        public bool CreditCommentListSpecified
        {
            get { return this.CreditCommentList != null && this.CreditCommentList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public CREDIT_COMMENTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
