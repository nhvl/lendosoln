namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CREDIT_COMMENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditCommentCodeSpecified
                    || this.CreditCommentCodeSourceTypeSpecified
                    || this.CreditCommentCodeSourceTypeOtherDescriptionSpecified
                    || this.CreditCommentReportedDateSpecified
                    || this.CreditCommentSourceTypeSpecified
                    || this.CreditCommentTextSpecified
                    || this.CreditCommentTypeSpecified
                    || this.CreditCommentTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CreditCommentCode", Order = 0)]
        public MISMOCode CreditCommentCode { get; set; }
    
        [XmlIgnore]
        public bool CreditCommentCodeSpecified
        {
            get { return this.CreditCommentCode != null; }
            set { }
        }
    
        [XmlElement("CreditCommentCodeSourceType", Order = 1)]
        public MISMOEnum<CreditCommentCodeSourceBase> CreditCommentCodeSourceType { get; set; }
    
        [XmlIgnore]
        public bool CreditCommentCodeSourceTypeSpecified
        {
            get { return this.CreditCommentCodeSourceType != null; }
            set { }
        }
    
        [XmlElement("CreditCommentCodeSourceTypeOtherDescription", Order = 2)]
        public MISMOString CreditCommentCodeSourceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CreditCommentCodeSourceTypeOtherDescriptionSpecified
        {
            get { return this.CreditCommentCodeSourceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("CreditCommentReportedDate", Order = 3)]
        public MISMODate CreditCommentReportedDate { get; set; }
    
        [XmlIgnore]
        public bool CreditCommentReportedDateSpecified
        {
            get { return this.CreditCommentReportedDate != null; }
            set { }
        }
    
        [XmlElement("CreditCommentSourceType", Order = 4)]
        public MISMOEnum<CreditCommentSourceBase> CreditCommentSourceType { get; set; }
    
        [XmlIgnore]
        public bool CreditCommentSourceTypeSpecified
        {
            get { return this.CreditCommentSourceType != null; }
            set { }
        }
    
        [XmlElement("CreditCommentText", Order = 5)]
        public MISMOString CreditCommentText { get; set; }
    
        [XmlIgnore]
        public bool CreditCommentTextSpecified
        {
            get { return this.CreditCommentText != null; }
            set { }
        }
    
        [XmlElement("CreditCommentType", Order = 6)]
        public MISMOEnum<CreditCommentBase> CreditCommentType { get; set; }
    
        [XmlIgnore]
        public bool CreditCommentTypeSpecified
        {
            get { return this.CreditCommentType != null; }
            set { }
        }
    
        [XmlElement("CreditCommentTypeOtherDescription", Order = 7)]
        public MISMOString CreditCommentTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CreditCommentTypeOtherDescriptionSpecified
        {
            get { return this.CreditCommentTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 8)]
        public CREDIT_COMMENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
