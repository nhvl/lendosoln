namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MONETARY_EVENT_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LoanPartialPrepaymentAmountSpecified
                    || this.MonetaryEventAppliedDateSpecified
                    || this.MonetaryEventBorrowerIncentiveCurtailmentAmountSpecified
                    || this.MonetaryEventDeferredInterestAmountSpecified
                    || this.MonetaryEventDeferredPrincipalCurtailmentAmountSpecified
                    || this.MonetaryEventDeferredUPBAmountSpecified
                    || this.MonetaryEventDueDateSpecified
                    || this.MonetaryEventEscrowPaymentAmountSpecified
                    || this.MonetaryEventExceptionInterestAmountSpecified
                    || this.MonetaryEventGrossInterestAmountSpecified
                    || this.MonetaryEventGrossPrincipalAmountSpecified
                    || this.MonetaryEventInterestBearingUPBAmountSpecified
                    || this.MonetaryEventInterestPaidThroughDateSpecified
                    || this.MonetaryEventInvestorRemittanceAmountSpecified
                    || this.MonetaryEventInvestorRemittanceEffectiveDateSpecified
                    || this.MonetaryEventNetInterestAmountSpecified
                    || this.MonetaryEventNetPrincipalAmountSpecified
                    || this.MonetaryEventOptionalProductsPaymentAmountSpecified
                    || this.MonetaryEventPaymentAmountSpecified
                    || this.MonetaryEventPaymentIdentifierSpecified
                    || this.MonetaryEventReversalIndicatorSpecified
                    || this.MonetaryEventScheduledUPBAmountSpecified
                    || this.MonetaryEventSubsequentRecoveryAmountSpecified
                    || this.MonetaryEventTotalPaymentAmountSpecified
                    || this.MonetaryEventTypeSpecified
                    || this.MonetaryEventTypeOtherDescriptionSpecified
                    || this.MonetaryEventUPBAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LoanPartialPrepaymentAmount", Order = 0)]
        public MISMOAmount LoanPartialPrepaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool LoanPartialPrepaymentAmountSpecified
        {
            get { return this.LoanPartialPrepaymentAmount != null; }
            set { }
        }
    
        [XmlElement("MonetaryEventAppliedDate", Order = 1)]
        public MISMODate MonetaryEventAppliedDate { get; set; }
    
        [XmlIgnore]
        public bool MonetaryEventAppliedDateSpecified
        {
            get { return this.MonetaryEventAppliedDate != null; }
            set { }
        }
    
        [XmlElement("MonetaryEventBorrowerIncentiveCurtailmentAmount", Order = 2)]
        public MISMOAmount MonetaryEventBorrowerIncentiveCurtailmentAmount { get; set; }
    
        [XmlIgnore]
        public bool MonetaryEventBorrowerIncentiveCurtailmentAmountSpecified
        {
            get { return this.MonetaryEventBorrowerIncentiveCurtailmentAmount != null; }
            set { }
        }
    
        [XmlElement("MonetaryEventDeferredInterestAmount", Order = 3)]
        public MISMOAmount MonetaryEventDeferredInterestAmount { get; set; }
    
        [XmlIgnore]
        public bool MonetaryEventDeferredInterestAmountSpecified
        {
            get { return this.MonetaryEventDeferredInterestAmount != null; }
            set { }
        }
    
        [XmlElement("MonetaryEventDeferredPrincipalCurtailmentAmount", Order = 4)]
        public MISMOAmount MonetaryEventDeferredPrincipalCurtailmentAmount { get; set; }
    
        [XmlIgnore]
        public bool MonetaryEventDeferredPrincipalCurtailmentAmountSpecified
        {
            get { return this.MonetaryEventDeferredPrincipalCurtailmentAmount != null; }
            set { }
        }
    
        [XmlElement("MonetaryEventDeferredUPBAmount", Order = 5)]
        public MISMOAmount MonetaryEventDeferredUPBAmount { get; set; }
    
        [XmlIgnore]
        public bool MonetaryEventDeferredUPBAmountSpecified
        {
            get { return this.MonetaryEventDeferredUPBAmount != null; }
            set { }
        }
    
        [XmlElement("MonetaryEventDueDate", Order = 6)]
        public MISMODate MonetaryEventDueDate { get; set; }
    
        [XmlIgnore]
        public bool MonetaryEventDueDateSpecified
        {
            get { return this.MonetaryEventDueDate != null; }
            set { }
        }
    
        [XmlElement("MonetaryEventEscrowPaymentAmount", Order = 7)]
        public MISMOAmount MonetaryEventEscrowPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool MonetaryEventEscrowPaymentAmountSpecified
        {
            get { return this.MonetaryEventEscrowPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("MonetaryEventExceptionInterestAmount", Order = 8)]
        public MISMOAmount MonetaryEventExceptionInterestAmount { get; set; }
    
        [XmlIgnore]
        public bool MonetaryEventExceptionInterestAmountSpecified
        {
            get { return this.MonetaryEventExceptionInterestAmount != null; }
            set { }
        }
    
        [XmlElement("MonetaryEventGrossInterestAmount", Order = 9)]
        public MISMOAmount MonetaryEventGrossInterestAmount { get; set; }
    
        [XmlIgnore]
        public bool MonetaryEventGrossInterestAmountSpecified
        {
            get { return this.MonetaryEventGrossInterestAmount != null; }
            set { }
        }
    
        [XmlElement("MonetaryEventGrossPrincipalAmount", Order = 10)]
        public MISMOAmount MonetaryEventGrossPrincipalAmount { get; set; }
    
        [XmlIgnore]
        public bool MonetaryEventGrossPrincipalAmountSpecified
        {
            get { return this.MonetaryEventGrossPrincipalAmount != null; }
            set { }
        }
    
        [XmlElement("MonetaryEventInterestBearingUPBAmount", Order = 11)]
        public MISMOAmount MonetaryEventInterestBearingUPBAmount { get; set; }
    
        [XmlIgnore]
        public bool MonetaryEventInterestBearingUPBAmountSpecified
        {
            get { return this.MonetaryEventInterestBearingUPBAmount != null; }
            set { }
        }
    
        [XmlElement("MonetaryEventInterestPaidThroughDate", Order = 12)]
        public MISMODate MonetaryEventInterestPaidThroughDate { get; set; }
    
        [XmlIgnore]
        public bool MonetaryEventInterestPaidThroughDateSpecified
        {
            get { return this.MonetaryEventInterestPaidThroughDate != null; }
            set { }
        }
    
        [XmlElement("MonetaryEventInvestorRemittanceAmount", Order = 13)]
        public MISMOAmount MonetaryEventInvestorRemittanceAmount { get; set; }
    
        [XmlIgnore]
        public bool MonetaryEventInvestorRemittanceAmountSpecified
        {
            get { return this.MonetaryEventInvestorRemittanceAmount != null; }
            set { }
        }
    
        [XmlElement("MonetaryEventInvestorRemittanceEffectiveDate", Order = 14)]
        public MISMODate MonetaryEventInvestorRemittanceEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool MonetaryEventInvestorRemittanceEffectiveDateSpecified
        {
            get { return this.MonetaryEventInvestorRemittanceEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("MonetaryEventNetInterestAmount", Order = 15)]
        public MISMOAmount MonetaryEventNetInterestAmount { get; set; }
    
        [XmlIgnore]
        public bool MonetaryEventNetInterestAmountSpecified
        {
            get { return this.MonetaryEventNetInterestAmount != null; }
            set { }
        }
    
        [XmlElement("MonetaryEventNetPrincipalAmount", Order = 16)]
        public MISMOAmount MonetaryEventNetPrincipalAmount { get; set; }
    
        [XmlIgnore]
        public bool MonetaryEventNetPrincipalAmountSpecified
        {
            get { return this.MonetaryEventNetPrincipalAmount != null; }
            set { }
        }
    
        [XmlElement("MonetaryEventOptionalProductsPaymentAmount", Order = 17)]
        public MISMOAmount MonetaryEventOptionalProductsPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool MonetaryEventOptionalProductsPaymentAmountSpecified
        {
            get { return this.MonetaryEventOptionalProductsPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("MonetaryEventPaymentAmount", Order = 18)]
        public MISMOAmount MonetaryEventPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool MonetaryEventPaymentAmountSpecified
        {
            get { return this.MonetaryEventPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("MonetaryEventPaymentIdentifier", Order = 19)]
        public MISMOIdentifier MonetaryEventPaymentIdentifier { get; set; }
    
        [XmlIgnore]
        public bool MonetaryEventPaymentIdentifierSpecified
        {
            get { return this.MonetaryEventPaymentIdentifier != null; }
            set { }
        }
    
        [XmlElement("MonetaryEventReversalIndicator", Order = 20)]
        public MISMOIndicator MonetaryEventReversalIndicator { get; set; }
    
        [XmlIgnore]
        public bool MonetaryEventReversalIndicatorSpecified
        {
            get { return this.MonetaryEventReversalIndicator != null; }
            set { }
        }
    
        [XmlElement("MonetaryEventScheduledUPBAmount", Order = 21)]
        public MISMOAmount MonetaryEventScheduledUPBAmount { get; set; }
    
        [XmlIgnore]
        public bool MonetaryEventScheduledUPBAmountSpecified
        {
            get { return this.MonetaryEventScheduledUPBAmount != null; }
            set { }
        }
    
        [XmlElement("MonetaryEventSubsequentRecoveryAmount", Order = 22)]
        public MISMOAmount MonetaryEventSubsequentRecoveryAmount { get; set; }
    
        [XmlIgnore]
        public bool MonetaryEventSubsequentRecoveryAmountSpecified
        {
            get { return this.MonetaryEventSubsequentRecoveryAmount != null; }
            set { }
        }
    
        [XmlElement("MonetaryEventTotalPaymentAmount", Order = 23)]
        public MISMOAmount MonetaryEventTotalPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool MonetaryEventTotalPaymentAmountSpecified
        {
            get { return this.MonetaryEventTotalPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("MonetaryEventType", Order = 24)]
        public MISMOEnum<MonetaryEventBase> MonetaryEventType { get; set; }
    
        [XmlIgnore]
        public bool MonetaryEventTypeSpecified
        {
            get { return this.MonetaryEventType != null; }
            set { }
        }
    
        [XmlElement("MonetaryEventTypeOtherDescription", Order = 25)]
        public MISMOString MonetaryEventTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool MonetaryEventTypeOtherDescriptionSpecified
        {
            get { return this.MonetaryEventTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MonetaryEventUPBAmount", Order = 26)]
        public MISMOAmount MonetaryEventUPBAmount { get; set; }
    
        [XmlIgnore]
        public bool MonetaryEventUPBAmountSpecified
        {
            get { return this.MonetaryEventUPBAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 27)]
        public MONETARY_EVENT_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
