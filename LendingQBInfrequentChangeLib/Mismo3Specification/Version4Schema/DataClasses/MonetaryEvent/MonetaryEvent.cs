namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MONETARY_EVENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.InvestorReportingAdditionalChargesSpecified
                    || this.MonetaryEventDetailSpecified
                    || this.StatusChangeEventsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("INVESTOR_REPORTING_ADDITIONAL_CHARGES", Order = 0)]
        public INVESTOR_REPORTING_ADDITIONAL_CHARGES InvestorReportingAdditionalCharges { get; set; }
    
        [XmlIgnore]
        public bool InvestorReportingAdditionalChargesSpecified
        {
            get { return this.InvestorReportingAdditionalCharges != null && this.InvestorReportingAdditionalCharges.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("MONETARY_EVENT_DETAIL", Order = 1)]
        public MONETARY_EVENT_DETAIL MonetaryEventDetail { get; set; }
    
        [XmlIgnore]
        public bool MonetaryEventDetailSpecified
        {
            get { return this.MonetaryEventDetail != null && this.MonetaryEventDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("STATUS_CHANGE_EVENTS", Order = 2)]
        public STATUS_CHANGE_EVENTS StatusChangeEvents { get; set; }
    
        [XmlIgnore]
        public bool StatusChangeEventsSpecified
        {
            get { return this.StatusChangeEvents != null && this.StatusChangeEvents.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public MONETARY_EVENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
