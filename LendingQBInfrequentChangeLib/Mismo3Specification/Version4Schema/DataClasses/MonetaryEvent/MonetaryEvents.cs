namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class MONETARY_EVENTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MonetaryEventListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MONETARY_EVENT", Order = 0)]
        public List<MONETARY_EVENT> MonetaryEventList { get; set; } = new List<MONETARY_EVENT>();
    
        [XmlIgnore]
        public bool MonetaryEventListSpecified
        {
            get { return this.MonetaryEventList != null && this.MonetaryEventList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public MONETARY_EVENTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
