namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PROPERTY_TAX_EXEMPTION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PropertyTaxExemptionDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PropertyTaxExemptionDescription", Order = 0)]
        public MISMOString PropertyTaxExemptionDescription { get; set; }
    
        [XmlIgnore]
        public bool PropertyTaxExemptionDescriptionSpecified
        {
            get { return this.PropertyTaxExemptionDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public PROPERTY_TAX_EXEMPTION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
