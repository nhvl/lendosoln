namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class PROPERTY_TAX_EXEMPTIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PropertyTaxExemptionListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PROPERTY_TAX_EXEMPTION", Order = 0)]
        public List<PROPERTY_TAX_EXEMPTION> PropertyTaxExemptionList { get; set; } = new List<PROPERTY_TAX_EXEMPTION>();
    
        [XmlIgnore]
        public bool PropertyTaxExemptionListSpecified
        {
            get { return this.PropertyTaxExemptionList != null && this.PropertyTaxExemptionList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public PROPERTY_TAX_EXEMPTIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
