namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class DISCLOSURE_ON_SERVICER
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ServicerDaysOfTheWeekDescriptionSpecified
                    || this.ServicerDirectInquiryToDescriptionSpecified
                    || this.ServicerHoursOfTheDayDescriptionSpecified
                    || this.ServicerInquiryTelephoneValueSpecified
                    || this.ServicerPaymentAcceptanceEndDateSpecified
                    || this.ServicerPaymentAcceptanceStartDateSpecified
                    || this.ServicingTransferEffectiveDateSpecified
                    || this.TransferOfServicingDisclosureTypeSpecified
                    || this.TransferOfServicingDisclosureTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ServicerDaysOfTheWeekDescription", Order = 0)]
        public MISMOString ServicerDaysOfTheWeekDescription { get; set; }
    
        [XmlIgnore]
        public bool ServicerDaysOfTheWeekDescriptionSpecified
        {
            get { return this.ServicerDaysOfTheWeekDescription != null; }
            set { }
        }
    
        [XmlElement("ServicerDirectInquiryToDescription", Order = 1)]
        public MISMOString ServicerDirectInquiryToDescription { get; set; }
    
        [XmlIgnore]
        public bool ServicerDirectInquiryToDescriptionSpecified
        {
            get { return this.ServicerDirectInquiryToDescription != null; }
            set { }
        }
    
        [XmlElement("ServicerHoursOfTheDayDescription", Order = 2)]
        public MISMOString ServicerHoursOfTheDayDescription { get; set; }
    
        [XmlIgnore]
        public bool ServicerHoursOfTheDayDescriptionSpecified
        {
            get { return this.ServicerHoursOfTheDayDescription != null; }
            set { }
        }
    
        [XmlElement("ServicerInquiryTelephoneValue", Order = 3)]
        public MISMONumericString ServicerInquiryTelephoneValue { get; set; }
    
        [XmlIgnore]
        public bool ServicerInquiryTelephoneValueSpecified
        {
            get { return this.ServicerInquiryTelephoneValue != null; }
            set { }
        }
    
        [XmlElement("ServicerPaymentAcceptanceEndDate", Order = 4)]
        public MISMODate ServicerPaymentAcceptanceEndDate { get; set; }
    
        [XmlIgnore]
        public bool ServicerPaymentAcceptanceEndDateSpecified
        {
            get { return this.ServicerPaymentAcceptanceEndDate != null; }
            set { }
        }
    
        [XmlElement("ServicerPaymentAcceptanceStartDate", Order = 5)]
        public MISMODate ServicerPaymentAcceptanceStartDate { get; set; }
    
        [XmlIgnore]
        public bool ServicerPaymentAcceptanceStartDateSpecified
        {
            get { return this.ServicerPaymentAcceptanceStartDate != null; }
            set { }
        }
    
        [XmlElement("ServicingTransferEffectiveDate", Order = 6)]
        public MISMODate ServicingTransferEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool ServicingTransferEffectiveDateSpecified
        {
            get { return this.ServicingTransferEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("TransferOfServicingDisclosureType", Order = 7)]
        public MISMOEnum<TransferOfServicingDisclosureBase> TransferOfServicingDisclosureType { get; set; }
    
        [XmlIgnore]
        public bool TransferOfServicingDisclosureTypeSpecified
        {
            get { return this.TransferOfServicingDisclosureType != null; }
            set { }
        }
    
        [XmlElement("TransferOfServicingDisclosureTypeOtherDescription", Order = 8)]
        public MISMOString TransferOfServicingDisclosureTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool TransferOfServicingDisclosureTypeOtherDescriptionSpecified
        {
            get { return this.TransferOfServicingDisclosureTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 9)]
        public DISCLOSURE_ON_SERVICER_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
