namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class WATER_SYSTEMS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.WaterSystemListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("WATER_SYSTEM", Order = 0)]
        public List<WATER_SYSTEM> WaterSystemList { get; set; } = new List<WATER_SYSTEM>();
    
        [XmlIgnore]
        public bool WaterSystemListSpecified
        {
            get { return this.WaterSystemList != null && this.WaterSystemList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public WATER_SYSTEMS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
