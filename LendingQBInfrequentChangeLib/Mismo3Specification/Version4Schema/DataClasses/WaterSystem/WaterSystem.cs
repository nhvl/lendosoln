namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class WATER_SYSTEM
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.WaterHeaterTypeSpecified
                    || this.WaterHeaterTypeOtherDescriptionSpecified
                    || this.WaterTreatmentTypeSpecified
                    || this.WaterTreatmentTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("WaterHeaterType", Order = 0)]
        public MISMOEnum<WaterHeaterBase> WaterHeaterType { get; set; }
    
        [XmlIgnore]
        public bool WaterHeaterTypeSpecified
        {
            get { return this.WaterHeaterType != null; }
            set { }
        }
    
        [XmlElement("WaterHeaterTypeOtherDescription", Order = 1)]
        public MISMOString WaterHeaterTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool WaterHeaterTypeOtherDescriptionSpecified
        {
            get { return this.WaterHeaterTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("WaterTreatmentType", Order = 2)]
        public MISMOEnum<WaterTreatmentBase> WaterTreatmentType { get; set; }
    
        [XmlIgnore]
        public bool WaterTreatmentTypeSpecified
        {
            get { return this.WaterTreatmentType != null; }
            set { }
        }
    
        [XmlElement("WaterTreatmentTypeOtherDescription", Order = 3)]
        public MISMOString WaterTreatmentTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool WaterTreatmentTypeOtherDescriptionSpecified
        {
            get { return this.WaterTreatmentTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public WATER_SYSTEM_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
