namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class BANKRUPTCY_DISPOSITION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BankruptcyDispositionDateSpecified
                    || this.BankruptcyDispositionTypeSpecified
                    || this.BankruptcyDispositionTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BankruptcyDispositionDate", Order = 0)]
        public MISMODate BankruptcyDispositionDate { get; set; }
    
        [XmlIgnore]
        public bool BankruptcyDispositionDateSpecified
        {
            get { return this.BankruptcyDispositionDate != null; }
            set { }
        }
    
        [XmlElement("BankruptcyDispositionType", Order = 1)]
        public MISMOEnum<BankruptcyDispositionBase> BankruptcyDispositionType { get; set; }
    
        [XmlIgnore]
        public bool BankruptcyDispositionTypeSpecified
        {
            get { return this.BankruptcyDispositionType != null; }
            set { }
        }
    
        [XmlElement("BankruptcyDispositionTypeOtherDescription", Order = 2)]
        public MISMOString BankruptcyDispositionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool BankruptcyDispositionTypeOtherDescriptionSpecified
        {
            get { return this.BankruptcyDispositionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public BANKRUPTCY_DISPOSITION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
