namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class BANKRUPTCY_DISPOSITIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BankruptcyDispositionListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BANKRUPTCY_DISPOSITION", Order = 0)]
        public List<BANKRUPTCY_DISPOSITION> BankruptcyDispositionList { get; set; } = new List<BANKRUPTCY_DISPOSITION>();
    
        [XmlIgnore]
        public bool BankruptcyDispositionListSpecified
        {
            get { return this.BankruptcyDispositionList != null && this.BankruptcyDispositionList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public BANKRUPTCY_DISPOSITIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
