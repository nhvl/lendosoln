namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CHARGE_OFF
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ChargeOffDetailSpecified
                    || this.ChargeOffItemsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CHARGE_OFF_DETAIL", Order = 0)]
        public CHARGE_OFF_DETAIL ChargeOffDetail { get; set; }
    
        [XmlIgnore]
        public bool ChargeOffDetailSpecified
        {
            get { return this.ChargeOffDetail != null && this.ChargeOffDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CHARGE_OFF_ITEMS", Order = 1)]
        public CHARGE_OFF_ITEMS ChargeOffItems { get; set; }
    
        [XmlIgnore]
        public bool ChargeOffItemsSpecified
        {
            get { return this.ChargeOffItems != null && this.ChargeOffItems.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public CHARGE_OFF_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
