namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CHARGE_OFF_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ChargeOffDateSpecified
                    || this.ChargeOffReasonDescriptionSpecified
                    || this.ChargeOffReasonIdentifierSpecified
                    || this.LienReleaseIndicatorSpecified
                    || this.TotalChargeOffAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ChargeOffDate", Order = 0)]
        public MISMODate ChargeOffDate { get; set; }
    
        [XmlIgnore]
        public bool ChargeOffDateSpecified
        {
            get { return this.ChargeOffDate != null; }
            set { }
        }
    
        [XmlElement("ChargeOffReasonDescription", Order = 1)]
        public MISMOString ChargeOffReasonDescription { get; set; }
    
        [XmlIgnore]
        public bool ChargeOffReasonDescriptionSpecified
        {
            get { return this.ChargeOffReasonDescription != null; }
            set { }
        }
    
        [XmlElement("ChargeOffReasonIdentifier", Order = 2)]
        public MISMOIdentifier ChargeOffReasonIdentifier { get; set; }
    
        [XmlIgnore]
        public bool ChargeOffReasonIdentifierSpecified
        {
            get { return this.ChargeOffReasonIdentifier != null; }
            set { }
        }
    
        [XmlElement("LienReleaseIndicator", Order = 3)]
        public MISMOIndicator LienReleaseIndicator { get; set; }
    
        [XmlIgnore]
        public bool LienReleaseIndicatorSpecified
        {
            get { return this.LienReleaseIndicator != null; }
            set { }
        }
    
        [XmlElement("TotalChargeOffAmount", Order = 4)]
        public MISMOAmount TotalChargeOffAmount { get; set; }
    
        [XmlIgnore]
        public bool TotalChargeOffAmountSpecified
        {
            get { return this.TotalChargeOffAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public CHARGE_OFF_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
