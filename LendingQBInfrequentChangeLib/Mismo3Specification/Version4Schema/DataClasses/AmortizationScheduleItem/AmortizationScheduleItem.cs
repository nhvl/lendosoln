namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class AMORTIZATION_SCHEDULE_ITEM
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AmortizationScheduleEndingBalanceAmountSpecified
                    || this.AmortizationScheduleInterestPortionAmountSpecified
                    || this.AmortizationScheduleInterestRatePercentSpecified
                    || this.AmortizationScheduleLoanToValuePercentSpecified
                    || this.AmortizationScheduleMIPaymentAmountSpecified
                    || this.AmortizationSchedulePaymentAmountSpecified
                    || this.AmortizationSchedulePaymentDueDateSpecified
                    || this.AmortizationSchedulePaymentNumberSpecified
                    || this.AmortizationSchedulePrincipalPortionAmountSpecified
                    || this.AmortizationScheduleUSDAAnnualFeePaymentAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AmortizationScheduleEndingBalanceAmount", Order = 0)]
        public MISMOAmount AmortizationScheduleEndingBalanceAmount { get; set; }
    
        [XmlIgnore]
        public bool AmortizationScheduleEndingBalanceAmountSpecified
        {
            get { return this.AmortizationScheduleEndingBalanceAmount != null; }
            set { }
        }
    
        [XmlElement("AmortizationScheduleInterestPortionAmount", Order = 1)]
        public MISMOAmount AmortizationScheduleInterestPortionAmount { get; set; }
    
        [XmlIgnore]
        public bool AmortizationScheduleInterestPortionAmountSpecified
        {
            get { return this.AmortizationScheduleInterestPortionAmount != null; }
            set { }
        }
    
        [XmlElement("AmortizationScheduleInterestRatePercent", Order = 2)]
        public MISMOPercent AmortizationScheduleInterestRatePercent { get; set; }
    
        [XmlIgnore]
        public bool AmortizationScheduleInterestRatePercentSpecified
        {
            get { return this.AmortizationScheduleInterestRatePercent != null; }
            set { }
        }
    
        [XmlElement("AmortizationScheduleLoanToValuePercent", Order = 3)]
        public MISMOPercent AmortizationScheduleLoanToValuePercent { get; set; }
    
        [XmlIgnore]
        public bool AmortizationScheduleLoanToValuePercentSpecified
        {
            get { return this.AmortizationScheduleLoanToValuePercent != null; }
            set { }
        }
    
        [XmlElement("AmortizationScheduleMIPaymentAmount", Order = 4)]
        public MISMOAmount AmortizationScheduleMIPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool AmortizationScheduleMIPaymentAmountSpecified
        {
            get { return this.AmortizationScheduleMIPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("AmortizationSchedulePaymentAmount", Order = 5)]
        public MISMOAmount AmortizationSchedulePaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool AmortizationSchedulePaymentAmountSpecified
        {
            get { return this.AmortizationSchedulePaymentAmount != null; }
            set { }
        }
    
        [XmlElement("AmortizationSchedulePaymentDueDate", Order = 6)]
        public MISMODate AmortizationSchedulePaymentDueDate { get; set; }
    
        [XmlIgnore]
        public bool AmortizationSchedulePaymentDueDateSpecified
        {
            get { return this.AmortizationSchedulePaymentDueDate != null; }
            set { }
        }
    
        [XmlElement("AmortizationSchedulePaymentNumber", Order = 7)]
        public MISMONumeric AmortizationSchedulePaymentNumber { get; set; }
    
        [XmlIgnore]
        public bool AmortizationSchedulePaymentNumberSpecified
        {
            get { return this.AmortizationSchedulePaymentNumber != null; }
            set { }
        }
    
        [XmlElement("AmortizationSchedulePrincipalPortionAmount", Order = 8)]
        public MISMOAmount AmortizationSchedulePrincipalPortionAmount { get; set; }
    
        [XmlIgnore]
        public bool AmortizationSchedulePrincipalPortionAmountSpecified
        {
            get { return this.AmortizationSchedulePrincipalPortionAmount != null; }
            set { }
        }
    
        [XmlElement("AmortizationScheduleUSDAAnnualFeePaymentAmount", Order = 9)]
        public MISMOAmount AmortizationScheduleUSDAAnnualFeePaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool AmortizationScheduleUSDAAnnualFeePaymentAmountSpecified
        {
            get { return this.AmortizationScheduleUSDAAnnualFeePaymentAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 10)]
        public AMORTIZATION_SCHEDULE_ITEM_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
