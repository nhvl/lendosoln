namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class AMORTIZATION_SCHEDULE_ITEMS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AmortizationScheduleItemListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AMORTIZATION_SCHEDULE_ITEM", Order = 0)]
        public List<AMORTIZATION_SCHEDULE_ITEM> AmortizationScheduleItemList { get; set; } = new List<AMORTIZATION_SCHEDULE_ITEM>();
    
        [XmlIgnore]
        public bool AmortizationScheduleItemListSpecified
        {
            get { return this.AmortizationScheduleItemList != null && this.AmortizationScheduleItemList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public AMORTIZATION_SCHEDULE_ITEMS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
