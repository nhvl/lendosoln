namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ERROR_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ErrorCategoryCodeSpecified
                    || this.RuleCodeSpecified
                    || this.RuleDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ErrorCategoryCode", Order = 0)]
        public MISMOCode ErrorCategoryCode { get; set; }
    
        [XmlIgnore]
        public bool ErrorCategoryCodeSpecified
        {
            get { return this.ErrorCategoryCode != null; }
            set { }
        }
    
        [XmlElement("RuleCode", Order = 1)]
        public MISMOCode RuleCode { get; set; }
    
        [XmlIgnore]
        public bool RuleCodeSpecified
        {
            get { return this.RuleCode != null; }
            set { }
        }
    
        [XmlElement("RuleDescription", Order = 2)]
        public MISMOString RuleDescription { get; set; }
    
        [XmlIgnore]
        public bool RuleDescriptionSpecified
        {
            get { return this.RuleDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public ERROR_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
