namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ERROR
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ErrorDetailSpecified
                    || this.ErrorMessagesSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ERROR_DETAIL", Order = 0)]
        public ERROR_DETAIL ErrorDetail { get; set; }
    
        [XmlIgnore]
        public bool ErrorDetailSpecified
        {
            get { return this.ErrorDetail != null && this.ErrorDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("ERROR_MESSAGES", Order = 1)]
        public ERROR_MESSAGES ErrorMessages { get; set; }
    
        [XmlIgnore]
        public bool ErrorMessagesSpecified
        {
            get { return this.ErrorMessages != null && this.ErrorMessages.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public ERROR_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
