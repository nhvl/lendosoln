namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class ERRORS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ErrorListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ERROR", Order = 0)]
        public List<ERROR> ErrorList { get; set; } = new List<ERROR>();
    
        [XmlIgnore]
        public bool ErrorListSpecified
        {
            get { return this.ErrorList != null && this.ErrorList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public ERRORS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
