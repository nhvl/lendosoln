namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class REVIEW_APPRAISER
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ReviewAppraiserCompanyNameSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ReviewAppraiserCompanyName", Order = 0)]
        public MISMOString ReviewAppraiserCompanyName { get; set; }
    
        [XmlIgnore]
        public bool ReviewAppraiserCompanyNameSpecified
        {
            get { return this.ReviewAppraiserCompanyName != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public REVIEW_APPRAISER_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
