namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class DECLARATION_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AlimonyChildSupportObligationIndicatorSpecified
                    || this.BankruptcyIndicatorSpecified
                    || this.BorrowerFirstTimeHomebuyerIndicatorSpecified
                    || this.BorrowerHousingCounselorConsentIndicatorSpecified
                    || this.CitizenshipResidencyTypeSpecified
                    || this.CoMakerEndorserOfNoteIndicatorSpecified
                    || this.FHASecondaryResidenceIndicatorSpecified
                    || this.HomeownerPastThreeYearsTypeSpecified
                    || this.IntentToOccupyTypeSpecified
                    || this.LoanForeclosureOrJudgmentIndicatorSpecified
                    || this.OutstandingJudgmentsIndicatorSpecified
                    || this.PartyToLawsuitIndicatorSpecified
                    || this.PresentlyDelinquentIndicatorSpecified
                    || this.PriorPropertyDeedInLieuConveyedIndicatorSpecified
                    || this.PriorPropertyForeclosureCompletedIndicatorSpecified
                    || this.PriorPropertyShortSaleCompletedIndicatorSpecified
                    || this.PriorPropertyTitleTypeSpecified
                    || this.PriorPropertyUsageTypeSpecified
                    || this.PropertyProposedCleanEnergyLienIndicatorSpecified
                    || this.UndisclosedBorrowedFundsAmountSpecified
                    || this.UndisclosedBorrowedFundsIndicatorSpecified
                    || this.UndisclosedComakerOfNoteIndicatorSpecified
                    || this.UndisclosedCreditApplicationIndicatorSpecified
                    || this.UndisclosedMortgageApplicationIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AlimonyChildSupportObligationIndicator", Order = 0)]
        public MISMOIndicator AlimonyChildSupportObligationIndicator { get; set; }
    
        [XmlIgnore]
        public bool AlimonyChildSupportObligationIndicatorSpecified
        {
            get { return this.AlimonyChildSupportObligationIndicator != null; }
            set { }
        }
    
        [XmlElement("BankruptcyIndicator", Order = 1)]
        public MISMOIndicator BankruptcyIndicator { get; set; }
    
        [XmlIgnore]
        public bool BankruptcyIndicatorSpecified
        {
            get { return this.BankruptcyIndicator != null; }
            set { }
        }
    
        [XmlElement("BorrowerFirstTimeHomebuyerIndicator", Order = 2)]
        public MISMOIndicator BorrowerFirstTimeHomebuyerIndicator { get; set; }
    
        [XmlIgnore]
        public bool BorrowerFirstTimeHomebuyerIndicatorSpecified
        {
            get { return this.BorrowerFirstTimeHomebuyerIndicator != null; }
            set { }
        }
    
        [XmlElement("BorrowerHousingCounselorConsentIndicator", Order = 3)]
        public MISMOIndicator BorrowerHousingCounselorConsentIndicator { get; set; }
    
        [XmlIgnore]
        public bool BorrowerHousingCounselorConsentIndicatorSpecified
        {
            get { return this.BorrowerHousingCounselorConsentIndicator != null; }
            set { }
        }
    
        [XmlElement("CitizenshipResidencyType", Order = 4)]
        public MISMOEnum<CitizenshipResidencyBase> CitizenshipResidencyType { get; set; }
    
        [XmlIgnore]
        public bool CitizenshipResidencyTypeSpecified
        {
            get { return this.CitizenshipResidencyType != null; }
            set { }
        }
    
        [XmlElement("CoMakerEndorserOfNoteIndicator", Order = 5)]
        public MISMOIndicator CoMakerEndorserOfNoteIndicator { get; set; }
    
        [XmlIgnore]
        public bool CoMakerEndorserOfNoteIndicatorSpecified
        {
            get { return this.CoMakerEndorserOfNoteIndicator != null; }
            set { }
        }
    
        [XmlElement("FHASecondaryResidenceIndicator", Order = 6)]
        public MISMOIndicator FHASecondaryResidenceIndicator { get; set; }
    
        [XmlIgnore]
        public bool FHASecondaryResidenceIndicatorSpecified
        {
            get { return this.FHASecondaryResidenceIndicator != null; }
            set { }
        }
    
        [XmlElement("HomeownerPastThreeYearsType", Order = 7)]
        public MISMOEnum<HomeownerPastThreeYearsBase> HomeownerPastThreeYearsType { get; set; }
    
        [XmlIgnore]
        public bool HomeownerPastThreeYearsTypeSpecified
        {
            get { return this.HomeownerPastThreeYearsType != null; }
            set { }
        }
    
        [XmlElement("IntentToOccupyType", Order = 8)]
        public MISMOEnum<IntentToOccupyBase> IntentToOccupyType { get; set; }
    
        [XmlIgnore]
        public bool IntentToOccupyTypeSpecified
        {
            get { return this.IntentToOccupyType != null; }
            set { }
        }
    
        [XmlElement("LoanForeclosureOrJudgmentIndicator", Order = 9)]
        public MISMOIndicator LoanForeclosureOrJudgmentIndicator { get; set; }
    
        [XmlIgnore]
        public bool LoanForeclosureOrJudgmentIndicatorSpecified
        {
            get { return this.LoanForeclosureOrJudgmentIndicator != null; }
            set { }
        }
    
        [XmlElement("OutstandingJudgmentsIndicator", Order = 10)]
        public MISMOIndicator OutstandingJudgmentsIndicator { get; set; }
    
        [XmlIgnore]
        public bool OutstandingJudgmentsIndicatorSpecified
        {
            get { return this.OutstandingJudgmentsIndicator != null; }
            set { }
        }
    
        [XmlElement("PartyToLawsuitIndicator", Order = 11)]
        public MISMOIndicator PartyToLawsuitIndicator { get; set; }
    
        [XmlIgnore]
        public bool PartyToLawsuitIndicatorSpecified
        {
            get { return this.PartyToLawsuitIndicator != null; }
            set { }
        }
    
        [XmlElement("PresentlyDelinquentIndicator", Order = 12)]
        public MISMOIndicator PresentlyDelinquentIndicator { get; set; }
    
        [XmlIgnore]
        public bool PresentlyDelinquentIndicatorSpecified
        {
            get { return this.PresentlyDelinquentIndicator != null; }
            set { }
        }
    
        [XmlElement("PriorPropertyDeedInLieuConveyedIndicator", Order = 13)]
        public MISMOIndicator PriorPropertyDeedInLieuConveyedIndicator { get; set; }
    
        [XmlIgnore]
        public bool PriorPropertyDeedInLieuConveyedIndicatorSpecified
        {
            get { return this.PriorPropertyDeedInLieuConveyedIndicator != null; }
            set { }
        }
    
        [XmlElement("PriorPropertyForeclosureCompletedIndicator", Order = 14)]
        public MISMOIndicator PriorPropertyForeclosureCompletedIndicator { get; set; }
    
        [XmlIgnore]
        public bool PriorPropertyForeclosureCompletedIndicatorSpecified
        {
            get { return this.PriorPropertyForeclosureCompletedIndicator != null; }
            set { }
        }
    
        [XmlElement("PriorPropertyShortSaleCompletedIndicator", Order = 15)]
        public MISMOIndicator PriorPropertyShortSaleCompletedIndicator { get; set; }
    
        [XmlIgnore]
        public bool PriorPropertyShortSaleCompletedIndicatorSpecified
        {
            get { return this.PriorPropertyShortSaleCompletedIndicator != null; }
            set { }
        }
    
        [XmlElement("PriorPropertyTitleType", Order = 16)]
        public MISMOEnum<PriorPropertyTitleBase> PriorPropertyTitleType { get; set; }
    
        [XmlIgnore]
        public bool PriorPropertyTitleTypeSpecified
        {
            get { return this.PriorPropertyTitleType != null; }
            set { }
        }
    
        [XmlElement("PriorPropertyUsageType", Order = 17)]
        public MISMOEnum<PriorPropertyUsageBase> PriorPropertyUsageType { get; set; }
    
        [XmlIgnore]
        public bool PriorPropertyUsageTypeSpecified
        {
            get { return this.PriorPropertyUsageType != null; }
            set { }
        }
    
        [XmlElement("PropertyProposedCleanEnergyLienIndicator", Order = 18)]
        public MISMOIndicator PropertyProposedCleanEnergyLienIndicator { get; set; }
    
        [XmlIgnore]
        public bool PropertyProposedCleanEnergyLienIndicatorSpecified
        {
            get { return this.PropertyProposedCleanEnergyLienIndicator != null; }
            set { }
        }
    
        [XmlElement("UndisclosedBorrowedFundsAmount", Order = 19)]
        public MISMOAmount UndisclosedBorrowedFundsAmount { get; set; }
    
        [XmlIgnore]
        public bool UndisclosedBorrowedFundsAmountSpecified
        {
            get { return this.UndisclosedBorrowedFundsAmount != null; }
            set { }
        }
    
        [XmlElement("UndisclosedBorrowedFundsIndicator", Order = 20)]
        public MISMOIndicator UndisclosedBorrowedFundsIndicator { get; set; }
    
        [XmlIgnore]
        public bool UndisclosedBorrowedFundsIndicatorSpecified
        {
            get { return this.UndisclosedBorrowedFundsIndicator != null; }
            set { }
        }
    
        [XmlElement("UndisclosedComakerOfNoteIndicator", Order = 21)]
        public MISMOIndicator UndisclosedComakerOfNoteIndicator { get; set; }
    
        [XmlIgnore]
        public bool UndisclosedComakerOfNoteIndicatorSpecified
        {
            get { return this.UndisclosedComakerOfNoteIndicator != null; }
            set { }
        }
    
        [XmlElement("UndisclosedCreditApplicationIndicator", Order = 22)]
        public MISMOIndicator UndisclosedCreditApplicationIndicator { get; set; }
    
        [XmlIgnore]
        public bool UndisclosedCreditApplicationIndicatorSpecified
        {
            get { return this.UndisclosedCreditApplicationIndicator != null; }
            set { }
        }
    
        [XmlElement("UndisclosedMortgageApplicationIndicator", Order = 23)]
        public MISMOIndicator UndisclosedMortgageApplicationIndicator { get; set; }
    
        [XmlIgnore]
        public bool UndisclosedMortgageApplicationIndicatorSpecified
        {
            get { return this.UndisclosedMortgageApplicationIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 24)]
        public DECLARATION_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
