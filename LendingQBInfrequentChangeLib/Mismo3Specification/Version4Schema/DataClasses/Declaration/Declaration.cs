namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class DECLARATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DeclarationDetailSpecified
                    || this.DeclarationExplanationsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DECLARATION_DETAIL", Order = 0)]
        public DECLARATION_DETAIL DeclarationDetail { get; set; }
    
        [XmlIgnore]
        public bool DeclarationDetailSpecified
        {
            get { return this.DeclarationDetail != null && this.DeclarationDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("DECLARATION_EXPLANATIONS", Order = 1)]
        public DECLARATION_EXPLANATIONS DeclarationExplanations { get; set; }
    
        [XmlIgnore]
        public bool DeclarationExplanationsSpecified
        {
            get { return this.DeclarationExplanations != null && this.DeclarationExplanations.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public DECLARATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
