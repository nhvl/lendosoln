namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class INTEREST_CALCULATION_OCCURRENCE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CurrentAccruedInterestAmountSpecified
                    || this.InterestDueFromClosingAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CurrentAccruedInterestAmount", Order = 0)]
        public MISMOAmount CurrentAccruedInterestAmount { get; set; }
    
        [XmlIgnore]
        public bool CurrentAccruedInterestAmountSpecified
        {
            get { return this.CurrentAccruedInterestAmount != null; }
            set { }
        }
    
        [XmlElement("InterestDueFromClosingAmount", Order = 1)]
        public MISMOAmount InterestDueFromClosingAmount { get; set; }
    
        [XmlIgnore]
        public bool InterestDueFromClosingAmountSpecified
        {
            get { return this.InterestDueFromClosingAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public INTEREST_CALCULATION_OCCURRENCE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
