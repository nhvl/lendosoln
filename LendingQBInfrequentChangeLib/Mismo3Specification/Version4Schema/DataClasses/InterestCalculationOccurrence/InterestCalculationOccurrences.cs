namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class INTEREST_CALCULATION_OCCURRENCES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.InterestCalculationOccurrenceListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("INTEREST_CALCULATION_OCCURRENCE", Order = 0)]
        public List<INTEREST_CALCULATION_OCCURRENCE> InterestCalculationOccurrenceList { get; set; } = new List<INTEREST_CALCULATION_OCCURRENCE>();
    
        [XmlIgnore]
        public bool InterestCalculationOccurrenceListSpecified
        {
            get { return this.InterestCalculationOccurrenceList != null && this.InterestCalculationOccurrenceList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public INTEREST_CALCULATION_OCCURRENCES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
