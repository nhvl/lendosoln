namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PAYEE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PayeeRemittanceTypeSpecified
                    || this.PayeeRemittanceTypeOtherDescriptionSpecified
                    || this.PayeeTypeSpecified
                    || this.PayeeTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PayeeRemittanceType", Order = 0)]
        public MISMOEnum<PayeeRemittanceBase> PayeeRemittanceType { get; set; }
    
        [XmlIgnore]
        public bool PayeeRemittanceTypeSpecified
        {
            get { return this.PayeeRemittanceType != null; }
            set { }
        }
    
        [XmlElement("PayeeRemittanceTypeOtherDescription", Order = 1)]
        public MISMOString PayeeRemittanceTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PayeeRemittanceTypeOtherDescriptionSpecified
        {
            get { return this.PayeeRemittanceTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PayeeType", Order = 2)]
        public MISMOEnum<PayeeBase> PayeeType { get; set; }
    
        [XmlIgnore]
        public bool PayeeTypeSpecified
        {
            get { return this.PayeeType != null; }
            set { }
        }
    
        [XmlElement("PayeeTypeOtherDescription", Order = 3)]
        public MISMOString PayeeTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool PayeeTypeOtherDescriptionSpecified
        {
            get { return this.PayeeTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public PAYEE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
