namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class BANKRUPTCY_ACTION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.BankruptcyActionDateSpecified
                    || this.BankruptcyActionTypeSpecified
                    || this.BankruptcyActionTypeOtherDescriptionSpecified
                    || this.BankruptcyAttorneyServiceReferralTypeSpecified
                    || this.BankruptcyAttorneyServiceReferralTypeOtherDescriptionSpecified
                    || this.BankruptcyPaymentChangeNoticeReasonTypeSpecified
                    || this.BankruptcyPaymentChangeNoticeReasonTypeOtherDescriptionSpecified
                    || this.BankruptcyPlanAmendedTypeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("BankruptcyActionDate", Order = 0)]
        public MISMODate BankruptcyActionDate { get; set; }
    
        [XmlIgnore]
        public bool BankruptcyActionDateSpecified
        {
            get { return this.BankruptcyActionDate != null; }
            set { }
        }
    
        [XmlElement("BankruptcyActionType", Order = 1)]
        public MISMOEnum<BankruptcyActionBase> BankruptcyActionType { get; set; }
    
        [XmlIgnore]
        public bool BankruptcyActionTypeSpecified
        {
            get { return this.BankruptcyActionType != null; }
            set { }
        }
    
        [XmlElement("BankruptcyActionTypeOtherDescription", Order = 2)]
        public MISMOString BankruptcyActionTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool BankruptcyActionTypeOtherDescriptionSpecified
        {
            get { return this.BankruptcyActionTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("BankruptcyAttorneyServiceReferralType", Order = 3)]
        public MISMOEnum<BankruptcyAttorneyServiceReferralBase> BankruptcyAttorneyServiceReferralType { get; set; }
    
        [XmlIgnore]
        public bool BankruptcyAttorneyServiceReferralTypeSpecified
        {
            get { return this.BankruptcyAttorneyServiceReferralType != null; }
            set { }
        }
    
        [XmlElement("BankruptcyAttorneyServiceReferralTypeOtherDescription", Order = 4)]
        public MISMOString BankruptcyAttorneyServiceReferralTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool BankruptcyAttorneyServiceReferralTypeOtherDescriptionSpecified
        {
            get { return this.BankruptcyAttorneyServiceReferralTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("BankruptcyPaymentChangeNoticeReasonType", Order = 5)]
        public MISMOEnum<BankruptcyPaymentChangeNoticeReasonBase> BankruptcyPaymentChangeNoticeReasonType { get; set; }
    
        [XmlIgnore]
        public bool BankruptcyPaymentChangeNoticeReasonTypeSpecified
        {
            get { return this.BankruptcyPaymentChangeNoticeReasonType != null; }
            set { }
        }
    
        [XmlElement("BankruptcyPaymentChangeNoticeReasonTypeOtherDescription", Order = 6)]
        public MISMOString BankruptcyPaymentChangeNoticeReasonTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool BankruptcyPaymentChangeNoticeReasonTypeOtherDescriptionSpecified
        {
            get { return this.BankruptcyPaymentChangeNoticeReasonTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("BankruptcyPlanAmendedType", Order = 7)]
        public MISMOEnum<BankruptcyPlanAmendedBase> BankruptcyPlanAmendedType { get; set; }
    
        [XmlIgnore]
        public bool BankruptcyPlanAmendedTypeSpecified
        {
            get { return this.BankruptcyPlanAmendedType != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 8)]
        public BANKRUPTCY_ACTION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
