namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class WINDOWS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.WindowListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("WINDOW", Order = 0)]
        public List<WINDOW> WindowList { get; set; } = new List<WINDOW>();
    
        [XmlIgnore]
        public bool WindowListSpecified
        {
            get { return this.WindowList != null && this.WindowList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public WINDOWS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
