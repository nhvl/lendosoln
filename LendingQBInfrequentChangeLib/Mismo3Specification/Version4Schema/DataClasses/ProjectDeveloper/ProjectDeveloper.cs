namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PROJECT_DEVELOPER
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ProjectDeveloperControlOfProjectManagementDescriptionSpecified
                    || this.ProjectDeveloperControlsProjectManagementIndicatorSpecified
                    || this.ProjectDeveloperTransferConcessionsDescriptionSpecified
                    || this.ProjectDeveloperTransferConcessionsIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ProjectDeveloperControlOfProjectManagementDescription", Order = 0)]
        public MISMOString ProjectDeveloperControlOfProjectManagementDescription { get; set; }
    
        [XmlIgnore]
        public bool ProjectDeveloperControlOfProjectManagementDescriptionSpecified
        {
            get { return this.ProjectDeveloperControlOfProjectManagementDescription != null; }
            set { }
        }
    
        [XmlElement("ProjectDeveloperControlsProjectManagementIndicator", Order = 1)]
        public MISMOIndicator ProjectDeveloperControlsProjectManagementIndicator { get; set; }
    
        [XmlIgnore]
        public bool ProjectDeveloperControlsProjectManagementIndicatorSpecified
        {
            get { return this.ProjectDeveloperControlsProjectManagementIndicator != null; }
            set { }
        }
    
        [XmlElement("ProjectDeveloperTransferConcessionsDescription", Order = 2)]
        public MISMOString ProjectDeveloperTransferConcessionsDescription { get; set; }
    
        [XmlIgnore]
        public bool ProjectDeveloperTransferConcessionsDescriptionSpecified
        {
            get { return this.ProjectDeveloperTransferConcessionsDescription != null; }
            set { }
        }
    
        [XmlElement("ProjectDeveloperTransferConcessionsIndicator", Order = 3)]
        public MISMOIndicator ProjectDeveloperTransferConcessionsIndicator { get; set; }
    
        [XmlIgnore]
        public bool ProjectDeveloperTransferConcessionsIndicatorSpecified
        {
            get { return this.ProjectDeveloperTransferConcessionsIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public PROJECT_DEVELOPER_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
