namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ACH
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ACH_ABARoutingAndTransitIdentifierSpecified
                    || this.ACHAccountTypeSpecified
                    || this.ACHAdditionalEscrowAmountSpecified
                    || this.ACHAdditionalPrincipalAmountSpecified
                    || this.ACHBankAccountIdentifierSpecified
                    || this.ACHBankAccountPurposeTypeSpecified
                    || this.ACHBankAccountPurposeTypeOtherDescriptionSpecified
                    || this.ACHDraftAfterDueDateDayCountSpecified
                    || this.ACHDraftFrequencyTypeSpecified
                    || this.ACHDraftingPartyNameSpecified
                    || this.ACHInstitutionTelegraphicAbbreviationNameSpecified
                    || this.ACHPendingDraftEffectiveDateSpecified
                    || this.ACHReceiverSubaccountNameSpecified
                    || this.ACHTypeSpecified
                    || this.ACHWireAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ACH_ABARoutingAndTransitIdentifier", Order = 0)]
        public MISMOIdentifier ACH_ABARoutingAndTransitIdentifier { get; set; }
    
        [XmlIgnore]
        public bool ACH_ABARoutingAndTransitIdentifierSpecified
        {
            get { return this.ACH_ABARoutingAndTransitIdentifier != null; }
            set { }
        }
    
        [XmlElement("ACHAccountType", Order = 1)]
        public MISMOEnum<ACHAccountBase> ACHAccountType { get; set; }
    
        [XmlIgnore]
        public bool ACHAccountTypeSpecified
        {
            get { return this.ACHAccountType != null; }
            set { }
        }
    
        [XmlElement("ACHAdditionalEscrowAmount", Order = 2)]
        public MISMOAmount ACHAdditionalEscrowAmount { get; set; }
    
        [XmlIgnore]
        public bool ACHAdditionalEscrowAmountSpecified
        {
            get { return this.ACHAdditionalEscrowAmount != null; }
            set { }
        }
    
        [XmlElement("ACHAdditionalPrincipalAmount", Order = 3)]
        public MISMOAmount ACHAdditionalPrincipalAmount { get; set; }
    
        [XmlIgnore]
        public bool ACHAdditionalPrincipalAmountSpecified
        {
            get { return this.ACHAdditionalPrincipalAmount != null; }
            set { }
        }
    
        [XmlElement("ACHBankAccountIdentifier", Order = 4)]
        public MISMOIdentifier ACHBankAccountIdentifier { get; set; }
    
        [XmlIgnore]
        public bool ACHBankAccountIdentifierSpecified
        {
            get { return this.ACHBankAccountIdentifier != null; }
            set { }
        }
    
        [XmlElement("ACHBankAccountPurposeType", Order = 5)]
        public MISMOEnum<ACHBankAccountPurposeBase> ACHBankAccountPurposeType { get; set; }
    
        [XmlIgnore]
        public bool ACHBankAccountPurposeTypeSpecified
        {
            get { return this.ACHBankAccountPurposeType != null; }
            set { }
        }
    
        [XmlElement("ACHBankAccountPurposeTypeOtherDescription", Order = 6)]
        public MISMOString ACHBankAccountPurposeTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ACHBankAccountPurposeTypeOtherDescriptionSpecified
        {
            get { return this.ACHBankAccountPurposeTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ACHDraftAfterDueDateDayCount", Order = 7)]
        public MISMOCount ACHDraftAfterDueDateDayCount { get; set; }
    
        [XmlIgnore]
        public bool ACHDraftAfterDueDateDayCountSpecified
        {
            get { return this.ACHDraftAfterDueDateDayCount != null; }
            set { }
        }
    
        [XmlElement("ACHDraftFrequencyType", Order = 8)]
        public MISMOEnum<ACHDraftFrequencyBase> ACHDraftFrequencyType { get; set; }
    
        [XmlIgnore]
        public bool ACHDraftFrequencyTypeSpecified
        {
            get { return this.ACHDraftFrequencyType != null; }
            set { }
        }
    
        [XmlElement("ACHDraftingPartyName", Order = 9)]
        public MISMOString ACHDraftingPartyName { get; set; }
    
        [XmlIgnore]
        public bool ACHDraftingPartyNameSpecified
        {
            get { return this.ACHDraftingPartyName != null; }
            set { }
        }
    
        [XmlElement("ACHInstitutionTelegraphicAbbreviationName", Order = 10)]
        public MISMOString ACHInstitutionTelegraphicAbbreviationName { get; set; }
    
        [XmlIgnore]
        public bool ACHInstitutionTelegraphicAbbreviationNameSpecified
        {
            get { return this.ACHInstitutionTelegraphicAbbreviationName != null; }
            set { }
        }
    
        [XmlElement("ACHPendingDraftEffectiveDate", Order = 11)]
        public MISMODate ACHPendingDraftEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool ACHPendingDraftEffectiveDateSpecified
        {
            get { return this.ACHPendingDraftEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("ACHReceiverSubaccountName", Order = 12)]
        public MISMOString ACHReceiverSubaccountName { get; set; }
    
        [XmlIgnore]
        public bool ACHReceiverSubaccountNameSpecified
        {
            get { return this.ACHReceiverSubaccountName != null; }
            set { }
        }
    
        [XmlElement("ACHType", Order = 13)]
        public MISMOEnum<ACHBase> ACHType { get; set; }
    
        [XmlIgnore]
        public bool ACHTypeSpecified
        {
            get { return this.ACHType != null; }
            set { }
        }
    
        [XmlElement("ACHWireAmount", Order = 14)]
        public MISMOAmount ACHWireAmount { get; set; }
    
        [XmlIgnore]
        public bool ACHWireAmountSpecified
        {
            get { return this.ACHWireAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 15)]
        public ACH_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
