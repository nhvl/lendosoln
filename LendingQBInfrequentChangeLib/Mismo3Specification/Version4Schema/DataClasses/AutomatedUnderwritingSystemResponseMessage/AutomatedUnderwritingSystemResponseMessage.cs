namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class AUTOMATED_UNDERWRITING_SYSTEM_RESPONSE_MESSAGE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AutomatedUnderwritingSystemMessageDescriptionSpecified
                    || this.AutomatedUnderwritingSystemMessageValueSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AutomatedUnderwritingSystemMessageDescription", Order = 0)]
        public MISMOString AutomatedUnderwritingSystemMessageDescription { get; set; }
    
        [XmlIgnore]
        public bool AutomatedUnderwritingSystemMessageDescriptionSpecified
        {
            get { return this.AutomatedUnderwritingSystemMessageDescription != null; }
            set { }
        }
    
        [XmlElement("AutomatedUnderwritingSystemMessageValue", Order = 1)]
        public MISMOValue AutomatedUnderwritingSystemMessageValue { get; set; }
    
        [XmlIgnore]
        public bool AutomatedUnderwritingSystemMessageValueSpecified
        {
            get { return this.AutomatedUnderwritingSystemMessageValue != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public AUTOMATED_UNDERWRITING_SYSTEM_RESPONSE_MESSAGE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
