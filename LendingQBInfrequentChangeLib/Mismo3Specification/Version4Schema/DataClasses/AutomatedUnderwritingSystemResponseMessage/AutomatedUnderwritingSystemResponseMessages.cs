namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class AUTOMATED_UNDERWRITING_SYSTEM_RESPONSE_MESSAGES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AutomatedUnderwritingSystemResponseMessageListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AUTOMATED_UNDERWRITING_SYSTEM_RESPONSE_MESSAGE", Order = 0)]
        public List<AUTOMATED_UNDERWRITING_SYSTEM_RESPONSE_MESSAGE> AutomatedUnderwritingSystemResponseMessageList { get; set; } = new List<AUTOMATED_UNDERWRITING_SYSTEM_RESPONSE_MESSAGE>();
    
        [XmlIgnore]
        public bool AutomatedUnderwritingSystemResponseMessageListSpecified
        {
            get { return this.AutomatedUnderwritingSystemResponseMessageList != null && this.AutomatedUnderwritingSystemResponseMessageList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public AUTOMATED_UNDERWRITING_SYSTEM_RESPONSE_MESSAGES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
