namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class COUNSELING_TOPICS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CounselingTopicListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("COUNSELING_TOPIC", Order = 0)]
        public List<COUNSELING_TOPIC> CounselingTopicList { get; set; } = new List<COUNSELING_TOPIC>();
    
        [XmlIgnore]
        public bool CounselingTopicListSpecified
        {
            get { return this.CounselingTopicList != null && this.CounselingTopicList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public COUNSELING_TOPICS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
