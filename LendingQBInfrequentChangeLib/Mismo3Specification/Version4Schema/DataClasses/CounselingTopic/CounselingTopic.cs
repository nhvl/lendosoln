namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class COUNSELING_TOPIC
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CounselingTopicTypeSpecified
                    || this.CounselingTopicTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CounselingTopicType", Order = 0)]
        public MISMOEnum<CounselingTopicBase> CounselingTopicType { get; set; }
    
        [XmlIgnore]
        public bool CounselingTopicTypeSpecified
        {
            get { return this.CounselingTopicType != null; }
            set { }
        }
    
        [XmlElement("CounselingTopicTypeOtherDescription", Order = 1)]
        public MISMOString CounselingTopicTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CounselingTopicTypeOtherDescriptionSpecified
        {
            get { return this.CounselingTopicTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public COUNSELING_TOPIC_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
