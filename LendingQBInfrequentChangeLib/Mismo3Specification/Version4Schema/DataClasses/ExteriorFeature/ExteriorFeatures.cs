namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class EXTERIOR_FEATURES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ExteriorFeatureListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("EXTERIOR_FEATURE", Order = 0)]
        public List<EXTERIOR_FEATURE> ExteriorFeatureList { get; set; } = new List<EXTERIOR_FEATURE>();
    
        [XmlIgnore]
        public bool ExteriorFeatureListSpecified
        {
            get { return this.ExteriorFeatureList != null && this.ExteriorFeatureList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public EXTERIOR_FEATURES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
