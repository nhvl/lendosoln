namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class EXTERIOR_FEATURE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ComponentAdjustmentAmountSpecified
                    || this.ConditionRatingDescriptionSpecified
                    || this.ConditionRatingTypeSpecified
                    || this.ExteriorFeatureDescriptionSpecified
                    || this.ExteriorFeatureTypeSpecified
                    || this.ExteriorFeatureTypeOtherDescriptionSpecified
                    || this.MaterialDescriptionSpecified
                    || this.QualityRatingDescriptionSpecified
                    || this.QualityRatingTypeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ComponentAdjustmentAmount", Order = 0)]
        public MISMOAmount ComponentAdjustmentAmount { get; set; }
    
        [XmlIgnore]
        public bool ComponentAdjustmentAmountSpecified
        {
            get { return this.ComponentAdjustmentAmount != null; }
            set { }
        }
    
        [XmlElement("ConditionRatingDescription", Order = 1)]
        public MISMOString ConditionRatingDescription { get; set; }
    
        [XmlIgnore]
        public bool ConditionRatingDescriptionSpecified
        {
            get { return this.ConditionRatingDescription != null; }
            set { }
        }
    
        [XmlElement("ConditionRatingType", Order = 2)]
        public MISMOEnum<ConditionRatingBase> ConditionRatingType { get; set; }
    
        [XmlIgnore]
        public bool ConditionRatingTypeSpecified
        {
            get { return this.ConditionRatingType != null; }
            set { }
        }
    
        [XmlElement("ExteriorFeatureDescription", Order = 3)]
        public MISMOString ExteriorFeatureDescription { get; set; }
    
        [XmlIgnore]
        public bool ExteriorFeatureDescriptionSpecified
        {
            get { return this.ExteriorFeatureDescription != null; }
            set { }
        }
    
        [XmlElement("ExteriorFeatureType", Order = 4)]
        public MISMOEnum<ExteriorFeatureBase> ExteriorFeatureType { get; set; }
    
        [XmlIgnore]
        public bool ExteriorFeatureTypeSpecified
        {
            get { return this.ExteriorFeatureType != null; }
            set { }
        }
    
        [XmlElement("ExteriorFeatureTypeOtherDescription", Order = 5)]
        public MISMOString ExteriorFeatureTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ExteriorFeatureTypeOtherDescriptionSpecified
        {
            get { return this.ExteriorFeatureTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MaterialDescription", Order = 6)]
        public MISMOString MaterialDescription { get; set; }
    
        [XmlIgnore]
        public bool MaterialDescriptionSpecified
        {
            get { return this.MaterialDescription != null; }
            set { }
        }
    
        [XmlElement("QualityRatingDescription", Order = 7)]
        public MISMOString QualityRatingDescription { get; set; }
    
        [XmlIgnore]
        public bool QualityRatingDescriptionSpecified
        {
            get { return this.QualityRatingDescription != null; }
            set { }
        }
    
        [XmlElement("QualityRatingType", Order = 8)]
        public MISMOEnum<QualityRatingBase> QualityRatingType { get; set; }
    
        [XmlIgnore]
        public bool QualityRatingTypeSpecified
        {
            get { return this.QualityRatingType != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 9)]
        public EXTERIOR_FEATURE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
