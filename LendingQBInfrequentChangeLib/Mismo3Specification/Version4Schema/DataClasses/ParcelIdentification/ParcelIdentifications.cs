namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class PARCEL_IDENTIFICATIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ParcelIdentificationListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PARCEL_IDENTIFICATION", Order = 0)]
        public List<PARCEL_IDENTIFICATION> ParcelIdentificationList { get; set; } = new List<PARCEL_IDENTIFICATION>();
    
        [XmlIgnore]
        public bool ParcelIdentificationListSpecified
        {
            get { return this.ParcelIdentificationList != null && this.ParcelIdentificationList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public PARCEL_IDENTIFICATIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
