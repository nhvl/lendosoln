namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PARCEL_IDENTIFICATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ParcelIdentificationTypeSpecified
                    || this.ParcelIdentificationTypeOtherDescriptionSpecified
                    || this.ParcelIdentifierSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ParcelIdentificationType", Order = 0)]
        public MISMOEnum<ParcelIdentificationBase> ParcelIdentificationType { get; set; }
    
        [XmlIgnore]
        public bool ParcelIdentificationTypeSpecified
        {
            get { return this.ParcelIdentificationType != null; }
            set { }
        }
    
        [XmlElement("ParcelIdentificationTypeOtherDescription", Order = 1)]
        public MISMOString ParcelIdentificationTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool ParcelIdentificationTypeOtherDescriptionSpecified
        {
            get { return this.ParcelIdentificationTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("ParcelIdentifier", Order = 2)]
        public MISMOIdentifier ParcelIdentifier { get; set; }
    
        [XmlIgnore]
        public bool ParcelIdentifierSpecified
        {
            get { return this.ParcelIdentifier != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public PARCEL_IDENTIFICATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
