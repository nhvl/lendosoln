namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class REPAIR
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.RepairItemsSpecified
                    || this.RepairSummarySpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("REPAIR_ITEMS", Order = 0)]
        public REPAIR_ITEMS RepairItems { get; set; }
    
        [XmlIgnore]
        public bool RepairItemsSpecified
        {
            get { return this.RepairItems != null && this.RepairItems.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("REPAIR_SUMMARY", Order = 1)]
        public REPAIR_SUMMARY RepairSummary { get; set; }
    
        [XmlIgnore]
        public bool RepairSummarySpecified
        {
            get { return this.RepairSummary != null && this.RepairSummary.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public REPAIR_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
