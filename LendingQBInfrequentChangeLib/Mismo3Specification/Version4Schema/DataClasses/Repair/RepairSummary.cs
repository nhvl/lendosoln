namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class REPAIR_SUMMARY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.RepairsDescriptionSpecified
                    || this.RepairsTotalCostAmountSpecified
                    || this.RepairsTotalCostPercentSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("RepairsDescription", Order = 0)]
        public MISMOString RepairsDescription { get; set; }
    
        [XmlIgnore]
        public bool RepairsDescriptionSpecified
        {
            get { return this.RepairsDescription != null; }
            set { }
        }
    
        [XmlElement("RepairsTotalCostAmount", Order = 1)]
        public MISMOAmount RepairsTotalCostAmount { get; set; }
    
        [XmlIgnore]
        public bool RepairsTotalCostAmountSpecified
        {
            get { return this.RepairsTotalCostAmount != null; }
            set { }
        }
    
        [XmlElement("RepairsTotalCostPercent", Order = 2)]
        public MISMOPercent RepairsTotalCostPercent { get; set; }
    
        [XmlIgnore]
        public bool RepairsTotalCostPercentSpecified
        {
            get { return this.RepairsTotalCostPercent != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public REPAIR_SUMMARY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
