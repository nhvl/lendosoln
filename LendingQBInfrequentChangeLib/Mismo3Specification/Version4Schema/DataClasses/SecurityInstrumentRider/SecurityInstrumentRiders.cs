namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class SECURITY_INSTRUMENT_RIDERS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SecurityInstrumentRiderListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SECURITY_INSTRUMENT_RIDER", Order = 0)]
        public List<SECURITY_INSTRUMENT_RIDER> SecurityInstrumentRiderList { get; set; } = new List<SECURITY_INSTRUMENT_RIDER>();
    
        [XmlIgnore]
        public bool SecurityInstrumentRiderListSpecified
        {
            get { return this.SecurityInstrumentRiderList != null && this.SecurityInstrumentRiderList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public SECURITY_INSTRUMENT_RIDERS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
