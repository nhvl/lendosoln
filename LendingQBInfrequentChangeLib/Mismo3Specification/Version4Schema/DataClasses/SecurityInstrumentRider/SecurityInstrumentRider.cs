namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class SECURITY_INSTRUMENT_RIDER
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SecurityInstrumentRiderTypeSpecified
                    || this.SecurityInstrumentRiderTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("SecurityInstrumentRiderType", Order = 0)]
        public MISMOEnum<SecurityInstrumentRiderBase> SecurityInstrumentRiderType { get; set; }
    
        [XmlIgnore]
        public bool SecurityInstrumentRiderTypeSpecified
        {
            get { return this.SecurityInstrumentRiderType != null; }
            set { }
        }
    
        [XmlElement("SecurityInstrumentRiderTypeOtherDescription", Order = 1)]
        public MISMOString SecurityInstrumentRiderTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool SecurityInstrumentRiderTypeOtherDescriptionSpecified
        {
            get { return this.SecurityInstrumentRiderTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public SECURITY_INSTRUMENT_RIDER_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
