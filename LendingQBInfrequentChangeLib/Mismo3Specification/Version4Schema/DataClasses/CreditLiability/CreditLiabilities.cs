namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class CREDIT_LIABILITIES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditLiabilityListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CREDIT_LIABILITY", Order = 0)]
        public List<CREDIT_LIABILITY> CreditLiabilityList { get; set; } = new List<CREDIT_LIABILITY>();
    
        [XmlIgnore]
        public bool CreditLiabilityListSpecified
        {
            get { return this.CreditLiabilityList != null && this.CreditLiabilityList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public CREDIT_LIABILITIES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
