namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CREDIT_LIABILITY_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditBusinessTypeSpecified
                    || this.CreditCounselingIndicatorSpecified
                    || this.CreditLiabilityAccountBalanceDateSpecified
                    || this.CreditLiabilityAccountClosedDateSpecified
                    || this.CreditLiabilityAccountIdentifierSpecified
                    || this.CreditLiabilityAccountOpenedDateSpecified
                    || this.CreditLiabilityAccountOwnershipTypeSpecified
                    || this.CreditLiabilityAccountPaidDateSpecified
                    || this.CreditLiabilityAccountReportedDateSpecified
                    || this.CreditLiabilityAccountStatusDateSpecified
                    || this.CreditLiabilityAccountStatusTypeSpecified
                    || this.CreditLiabilityAccountTypeSpecified
                    || this.CreditLiabilityActualPaymentAmountSpecified
                    || this.CreditLiabilityBalloonPaymentAmountSpecified
                    || this.CreditLiabilityBalloonPaymentDueDateSpecified
                    || this.CreditLiabilityChargeOffAmountSpecified
                    || this.CreditLiabilityChargeOffDateSpecified
                    || this.CreditLiabilityCollateralDescriptionSpecified
                    || this.CreditLiabilityConsumerDisputeIndicatorSpecified
                    || this.CreditLiabilityCreditLimitAmountSpecified
                    || this.CreditLiabilityDerogatoryDataIndicatorSpecified
                    || this.CreditLiabilityHighBalanceAmountSpecified
                    || this.CreditLiabilityLastActivityDateSpecified
                    || this.CreditLiabilityLastPaymentDateSpecified
                    || this.CreditLiabilityManualUpdateIndicatorSpecified
                    || this.CreditLiabilityMonthlyPaymentAmountSpecified
                    || this.CreditLiabilityMonthsRemainingCountSpecified
                    || this.CreditLiabilityMonthsReviewedCountSpecified
                    || this.CreditLiabilityOriginalCreditorNameSpecified
                    || this.CreditLiabilityPastDueAmountSpecified
                    || this.CreditLiabilityTermsDescriptionSpecified
                    || this.CreditLiabilityTermsMonthsCountSpecified
                    || this.CreditLiabilityTermsSourceTypeSpecified
                    || this.CreditLiabilityUnpaidBalanceAmountSpecified
                    || this.CreditLoanTypeSpecified
                    || this.CreditLoanTypeOtherDescriptionSpecified
                    || this.DeferredPaymentAmountSpecified
                    || this.DeferredPaymentDateSpecified
                    || this.DetailCreditBusinessTypeSpecified
                    || this.DuplicateGroupIdentifierSpecified
                    || this.PrimaryRecordIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CreditBusinessType", Order = 0)]
        public MISMOEnum<CreditBusinessBase> CreditBusinessType { get; set; }
    
        [XmlIgnore]
        public bool CreditBusinessTypeSpecified
        {
            get { return this.CreditBusinessType != null; }
            set { }
        }
    
        [XmlElement("CreditCounselingIndicator", Order = 1)]
        public MISMOIndicator CreditCounselingIndicator { get; set; }
    
        [XmlIgnore]
        public bool CreditCounselingIndicatorSpecified
        {
            get { return this.CreditCounselingIndicator != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityAccountBalanceDate", Order = 2)]
        public MISMODate CreditLiabilityAccountBalanceDate { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityAccountBalanceDateSpecified
        {
            get { return this.CreditLiabilityAccountBalanceDate != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityAccountClosedDate", Order = 3)]
        public MISMODate CreditLiabilityAccountClosedDate { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityAccountClosedDateSpecified
        {
            get { return this.CreditLiabilityAccountClosedDate != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityAccountIdentifier", Order = 4)]
        public MISMOIdentifier CreditLiabilityAccountIdentifier { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityAccountIdentifierSpecified
        {
            get { return this.CreditLiabilityAccountIdentifier != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityAccountOpenedDate", Order = 5)]
        public MISMODate CreditLiabilityAccountOpenedDate { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityAccountOpenedDateSpecified
        {
            get { return this.CreditLiabilityAccountOpenedDate != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityAccountOwnershipType", Order = 6)]
        public MISMOEnum<CreditLiabilityAccountOwnershipBase> CreditLiabilityAccountOwnershipType { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityAccountOwnershipTypeSpecified
        {
            get { return this.CreditLiabilityAccountOwnershipType != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityAccountPaidDate", Order = 7)]
        public MISMODate CreditLiabilityAccountPaidDate { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityAccountPaidDateSpecified
        {
            get { return this.CreditLiabilityAccountPaidDate != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityAccountReportedDate", Order = 8)]
        public MISMODate CreditLiabilityAccountReportedDate { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityAccountReportedDateSpecified
        {
            get { return this.CreditLiabilityAccountReportedDate != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityAccountStatusDate", Order = 9)]
        public MISMODate CreditLiabilityAccountStatusDate { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityAccountStatusDateSpecified
        {
            get { return this.CreditLiabilityAccountStatusDate != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityAccountStatusType", Order = 10)]
        public MISMOEnum<CreditLiabilityAccountStatusBase> CreditLiabilityAccountStatusType { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityAccountStatusTypeSpecified
        {
            get { return this.CreditLiabilityAccountStatusType != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityAccountType", Order = 11)]
        public MISMOEnum<CreditLiabilityAccountBase> CreditLiabilityAccountType { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityAccountTypeSpecified
        {
            get { return this.CreditLiabilityAccountType != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityActualPaymentAmount", Order = 12)]
        public MISMOAmount CreditLiabilityActualPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityActualPaymentAmountSpecified
        {
            get { return this.CreditLiabilityActualPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityBalloonPaymentAmount", Order = 13)]
        public MISMOAmount CreditLiabilityBalloonPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityBalloonPaymentAmountSpecified
        {
            get { return this.CreditLiabilityBalloonPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityBalloonPaymentDueDate", Order = 14)]
        public MISMODate CreditLiabilityBalloonPaymentDueDate { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityBalloonPaymentDueDateSpecified
        {
            get { return this.CreditLiabilityBalloonPaymentDueDate != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityChargeOffAmount", Order = 15)]
        public MISMOAmount CreditLiabilityChargeOffAmount { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityChargeOffAmountSpecified
        {
            get { return this.CreditLiabilityChargeOffAmount != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityChargeOffDate", Order = 16)]
        public MISMODate CreditLiabilityChargeOffDate { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityChargeOffDateSpecified
        {
            get { return this.CreditLiabilityChargeOffDate != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityCollateralDescription", Order = 17)]
        public MISMOString CreditLiabilityCollateralDescription { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityCollateralDescriptionSpecified
        {
            get { return this.CreditLiabilityCollateralDescription != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityConsumerDisputeIndicator", Order = 18)]
        public MISMOIndicator CreditLiabilityConsumerDisputeIndicator { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityConsumerDisputeIndicatorSpecified
        {
            get { return this.CreditLiabilityConsumerDisputeIndicator != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityCreditLimitAmount", Order = 19)]
        public MISMOAmount CreditLiabilityCreditLimitAmount { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityCreditLimitAmountSpecified
        {
            get { return this.CreditLiabilityCreditLimitAmount != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityDerogatoryDataIndicator", Order = 20)]
        public MISMOIndicator CreditLiabilityDerogatoryDataIndicator { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityDerogatoryDataIndicatorSpecified
        {
            get { return this.CreditLiabilityDerogatoryDataIndicator != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityHighBalanceAmount", Order = 21)]
        public MISMOAmount CreditLiabilityHighBalanceAmount { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityHighBalanceAmountSpecified
        {
            get { return this.CreditLiabilityHighBalanceAmount != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityLastActivityDate", Order = 22)]
        public MISMODate CreditLiabilityLastActivityDate { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityLastActivityDateSpecified
        {
            get { return this.CreditLiabilityLastActivityDate != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityLastPaymentDate", Order = 23)]
        public MISMODate CreditLiabilityLastPaymentDate { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityLastPaymentDateSpecified
        {
            get { return this.CreditLiabilityLastPaymentDate != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityManualUpdateIndicator", Order = 24)]
        public MISMOIndicator CreditLiabilityManualUpdateIndicator { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityManualUpdateIndicatorSpecified
        {
            get { return this.CreditLiabilityManualUpdateIndicator != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityMonthlyPaymentAmount", Order = 25)]
        public MISMOAmount CreditLiabilityMonthlyPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityMonthlyPaymentAmountSpecified
        {
            get { return this.CreditLiabilityMonthlyPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityMonthsRemainingCount", Order = 26)]
        public MISMOCount CreditLiabilityMonthsRemainingCount { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityMonthsRemainingCountSpecified
        {
            get { return this.CreditLiabilityMonthsRemainingCount != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityMonthsReviewedCount", Order = 27)]
        public MISMOCount CreditLiabilityMonthsReviewedCount { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityMonthsReviewedCountSpecified
        {
            get { return this.CreditLiabilityMonthsReviewedCount != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityOriginalCreditorName", Order = 28)]
        public MISMOString CreditLiabilityOriginalCreditorName { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityOriginalCreditorNameSpecified
        {
            get { return this.CreditLiabilityOriginalCreditorName != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityPastDueAmount", Order = 29)]
        public MISMOAmount CreditLiabilityPastDueAmount { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityPastDueAmountSpecified
        {
            get { return this.CreditLiabilityPastDueAmount != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityTermsDescription", Order = 30)]
        public MISMOString CreditLiabilityTermsDescription { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityTermsDescriptionSpecified
        {
            get { return this.CreditLiabilityTermsDescription != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityTermsMonthsCount", Order = 31)]
        public MISMOCount CreditLiabilityTermsMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityTermsMonthsCountSpecified
        {
            get { return this.CreditLiabilityTermsMonthsCount != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityTermsSourceType", Order = 32)]
        public MISMOEnum<CreditLiabilityTermsSourceBase> CreditLiabilityTermsSourceType { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityTermsSourceTypeSpecified
        {
            get { return this.CreditLiabilityTermsSourceType != null; }
            set { }
        }
    
        [XmlElement("CreditLiabilityUnpaidBalanceAmount", Order = 33)]
        public MISMOAmount CreditLiabilityUnpaidBalanceAmount { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityUnpaidBalanceAmountSpecified
        {
            get { return this.CreditLiabilityUnpaidBalanceAmount != null; }
            set { }
        }
    
        [XmlElement("CreditLoanType", Order = 34)]
        public MISMOEnum<CreditLoanBase> CreditLoanType { get; set; }
    
        [XmlIgnore]
        public bool CreditLoanTypeSpecified
        {
            get { return this.CreditLoanType != null; }
            set { }
        }
    
        [XmlElement("CreditLoanTypeOtherDescription", Order = 35)]
        public MISMOString CreditLoanTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool CreditLoanTypeOtherDescriptionSpecified
        {
            get { return this.CreditLoanTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("DeferredPaymentAmount", Order = 36)]
        public MISMOAmount DeferredPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool DeferredPaymentAmountSpecified
        {
            get { return this.DeferredPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("DeferredPaymentDate", Order = 37)]
        public MISMODate DeferredPaymentDate { get; set; }
    
        [XmlIgnore]
        public bool DeferredPaymentDateSpecified
        {
            get { return this.DeferredPaymentDate != null; }
            set { }
        }
    
        [XmlElement("DetailCreditBusinessType", Order = 38)]
        public MISMOEnum<DetailCreditBusinessBase> DetailCreditBusinessType { get; set; }
    
        [XmlIgnore]
        public bool DetailCreditBusinessTypeSpecified
        {
            get { return this.DetailCreditBusinessType != null; }
            set { }
        }
    
        [XmlElement("DuplicateGroupIdentifier", Order = 39)]
        public MISMOIdentifier DuplicateGroupIdentifier { get; set; }
    
        [XmlIgnore]
        public bool DuplicateGroupIdentifierSpecified
        {
            get { return this.DuplicateGroupIdentifier != null; }
            set { }
        }
    
        [XmlElement("PrimaryRecordIndicator", Order = 40)]
        public MISMOIndicator PrimaryRecordIndicator { get; set; }
    
        [XmlIgnore]
        public bool PrimaryRecordIndicatorSpecified
        {
            get { return this.PrimaryRecordIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 41)]
        public CREDIT_LIABILITY_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
