namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CREDIT_LIABILITY
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CreditCommentsSpecified
                    || this.CreditLiabilityCreditorSpecified
                    || this.CreditLiabilityCurrentRatingSpecified
                    || this.CreditLiabilityDetailSpecified
                    || this.CreditLiabilityHighestAdverseRatingSpecified
                    || this.CreditLiabilityLateCountSpecified
                    || this.CreditLiabilityMostRecentAdverseRatingSpecified
                    || this.CreditLiabilityPaymentPatternSpecified
                    || this.CreditLiabilityPriorAdverseRatingsSpecified
                    || this.CreditLiabilityPriorInformationsSpecified
                    || this.CreditRepositoriesSpecified
                    || this.VerificationSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CREDIT_COMMENTS", Order = 0)]
        public CREDIT_COMMENTS CreditComments { get; set; }
    
        [XmlIgnore]
        public bool CreditCommentsSpecified
        {
            get { return this.CreditComments != null && this.CreditComments.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_LIABILITY_CREDITOR", Order = 1)]
        public CREDIT_LIABILITY_CREDITOR CreditLiabilityCreditor { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityCreditorSpecified
        {
            get { return this.CreditLiabilityCreditor != null && this.CreditLiabilityCreditor.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_LIABILITY_CURRENT_RATING", Order = 2)]
        public CREDIT_LIABILITY_CURRENT_RATING CreditLiabilityCurrentRating { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityCurrentRatingSpecified
        {
            get { return this.CreditLiabilityCurrentRating != null && this.CreditLiabilityCurrentRating.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_LIABILITY_DETAIL", Order = 3)]
        public CREDIT_LIABILITY_DETAIL CreditLiabilityDetail { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityDetailSpecified
        {
            get { return this.CreditLiabilityDetail != null && this.CreditLiabilityDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_LIABILITY_HIGHEST_ADVERSE_RATING", Order = 4)]
        public CREDIT_LIABILITY_HIGHEST_ADVERSE_RATING CreditLiabilityHighestAdverseRating { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityHighestAdverseRatingSpecified
        {
            get { return this.CreditLiabilityHighestAdverseRating != null && this.CreditLiabilityHighestAdverseRating.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_LIABILITY_LATE_COUNT", Order = 5)]
        public CREDIT_LIABILITY_LATE_COUNT CreditLiabilityLateCount { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityLateCountSpecified
        {
            get { return this.CreditLiabilityLateCount != null && this.CreditLiabilityLateCount.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_LIABILITY_MOST_RECENT_ADVERSE_RATING", Order = 6)]
        public CREDIT_LIABILITY_MOST_RECENT_ADVERSE_RATING CreditLiabilityMostRecentAdverseRating { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityMostRecentAdverseRatingSpecified
        {
            get { return this.CreditLiabilityMostRecentAdverseRating != null && this.CreditLiabilityMostRecentAdverseRating.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_LIABILITY_PAYMENT_PATTERN", Order = 7)]
        public CREDIT_LIABILITY_PAYMENT_PATTERN CreditLiabilityPaymentPattern { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityPaymentPatternSpecified
        {
            get { return this.CreditLiabilityPaymentPattern != null && this.CreditLiabilityPaymentPattern.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_LIABILITY_PRIOR_ADVERSE_RATINGS", Order = 8)]
        public CREDIT_LIABILITY_PRIOR_ADVERSE_RATINGS CreditLiabilityPriorAdverseRatings { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityPriorAdverseRatingsSpecified
        {
            get { return this.CreditLiabilityPriorAdverseRatings != null && this.CreditLiabilityPriorAdverseRatings.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_LIABILITY_PRIOR_INFORMATIONS", Order = 9)]
        public CREDIT_LIABILITY_PRIOR_INFORMATIONS CreditLiabilityPriorInformations { get; set; }
    
        [XmlIgnore]
        public bool CreditLiabilityPriorInformationsSpecified
        {
            get { return this.CreditLiabilityPriorInformations != null && this.CreditLiabilityPriorInformations.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CREDIT_REPOSITORIES", Order = 10)]
        public CREDIT_REPOSITORIES CreditRepositories { get; set; }
    
        [XmlIgnore]
        public bool CreditRepositoriesSpecified
        {
            get { return this.CreditRepositories != null && this.CreditRepositories.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("VERIFICATION", Order = 11)]
        public VERIFICATION Verification { get; set; }
    
        [XmlIgnore]
        public bool VerificationSpecified
        {
            get { return this.Verification != null && this.Verification.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 12)]
        public CREDIT_LIABILITY_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
