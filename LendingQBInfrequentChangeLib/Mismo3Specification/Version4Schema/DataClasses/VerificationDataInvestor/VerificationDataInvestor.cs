namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class VERIFICATION_DATA_INVESTOR
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.InvestorTotalCountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("InvestorTotalCount", Order = 0)]
        public MISMOCount InvestorTotalCount { get; set; }
    
        [XmlIgnore]
        public bool InvestorTotalCountSpecified
        {
            get { return this.InvestorTotalCount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public VERIFICATION_DATA_INVESTOR_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
