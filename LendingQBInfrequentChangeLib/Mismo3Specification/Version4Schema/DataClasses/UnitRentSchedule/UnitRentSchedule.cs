namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class UNIT_RENT_SCHEDULE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LeaseExpirationDateSpecified
                    || this.LeaseStartDateSpecified
                    || this.UnitActualRentAmountSpecified
                    || this.UnitFurnishedActualRentAmountSpecified
                    || this.UnitFurnishedMarketRentAmountSpecified
                    || this.UnitMarketRentAmountSpecified
                    || this.UnitUnfurnishedActualRentAmountSpecified
                    || this.UnitUnfurnishedMarketRentAmountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LeaseExpirationDate", Order = 0)]
        public MISMODate LeaseExpirationDate { get; set; }
    
        [XmlIgnore]
        public bool LeaseExpirationDateSpecified
        {
            get { return this.LeaseExpirationDate != null; }
            set { }
        }
    
        [XmlElement("LeaseStartDate", Order = 1)]
        public MISMODate LeaseStartDate { get; set; }
    
        [XmlIgnore]
        public bool LeaseStartDateSpecified
        {
            get { return this.LeaseStartDate != null; }
            set { }
        }
    
        [XmlElement("UnitActualRentAmount", Order = 2)]
        public MISMOAmount UnitActualRentAmount { get; set; }
    
        [XmlIgnore]
        public bool UnitActualRentAmountSpecified
        {
            get { return this.UnitActualRentAmount != null; }
            set { }
        }
    
        [XmlElement("UnitFurnishedActualRentAmount", Order = 3)]
        public MISMOAmount UnitFurnishedActualRentAmount { get; set; }
    
        [XmlIgnore]
        public bool UnitFurnishedActualRentAmountSpecified
        {
            get { return this.UnitFurnishedActualRentAmount != null; }
            set { }
        }
    
        [XmlElement("UnitFurnishedMarketRentAmount", Order = 4)]
        public MISMOAmount UnitFurnishedMarketRentAmount { get; set; }
    
        [XmlIgnore]
        public bool UnitFurnishedMarketRentAmountSpecified
        {
            get { return this.UnitFurnishedMarketRentAmount != null; }
            set { }
        }
    
        [XmlElement("UnitMarketRentAmount", Order = 5)]
        public MISMOAmount UnitMarketRentAmount { get; set; }
    
        [XmlIgnore]
        public bool UnitMarketRentAmountSpecified
        {
            get { return this.UnitMarketRentAmount != null; }
            set { }
        }
    
        [XmlElement("UnitUnfurnishedActualRentAmount", Order = 6)]
        public MISMOAmount UnitUnfurnishedActualRentAmount { get; set; }
    
        [XmlIgnore]
        public bool UnitUnfurnishedActualRentAmountSpecified
        {
            get { return this.UnitUnfurnishedActualRentAmount != null; }
            set { }
        }
    
        [XmlElement("UnitUnfurnishedMarketRentAmount", Order = 7)]
        public MISMOAmount UnitUnfurnishedMarketRentAmount { get; set; }
    
        [XmlIgnore]
        public bool UnitUnfurnishedMarketRentAmountSpecified
        {
            get { return this.UnitUnfurnishedMarketRentAmount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 8)]
        public UNIT_RENT_SCHEDULE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
