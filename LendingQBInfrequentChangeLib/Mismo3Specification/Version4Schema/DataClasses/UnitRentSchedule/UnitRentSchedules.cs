namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class UNIT_RENT_SCHEDULES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.UnitRentScheduleListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("UNIT_RENT_SCHEDULE", Order = 0)]
        public List<UNIT_RENT_SCHEDULE> UnitRentScheduleList { get; set; } = new List<UNIT_RENT_SCHEDULE>();
    
        [XmlIgnore]
        public bool UnitRentScheduleListSpecified
        {
            get { return this.UnitRentScheduleList != null && this.UnitRentScheduleList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public UNIT_RENT_SCHEDULES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
