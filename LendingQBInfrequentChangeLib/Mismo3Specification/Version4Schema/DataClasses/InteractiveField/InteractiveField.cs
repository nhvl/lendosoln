namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class INTERACTIVE_FIELD
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.InteractiveFieldDetailSpecified
                    || this.INTERACTIVE_FIELD_REFERENCESpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("INTERACTIVE_FIELD_DETAIL", Order = 0)]
        public INTERACTIVE_FIELD_DETAIL InteractiveFieldDetail { get; set; }
    
        [XmlIgnore]
        public bool InteractiveFieldDetailSpecified
        {
            get { return this.InteractiveFieldDetail != null && this.InteractiveFieldDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("INTERACTIVE_FIELD_REFERENCE", Order = 1)]
        public FIELD_REFERENCE INTERACTIVE_FIELD_REFERENCE { get; set; }
    
        [XmlIgnore]
        public bool INTERACTIVE_FIELD_REFERENCESpecified
        {
            get { return this.INTERACTIVE_FIELD_REFERENCE != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public INTERACTIVE_FIELD_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
