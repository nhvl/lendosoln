namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class INTERACTIVE_FIELD_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MutuallyExclusiveGroupNameSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MutuallyExclusiveGroupName", Order = 0)]
        public MISMOString MutuallyExclusiveGroupName { get; set; }
    
        [XmlIgnore]
        public bool MutuallyExclusiveGroupNameSpecified
        {
            get { return this.MutuallyExclusiveGroupName != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public INTERACTIVE_FIELD_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
