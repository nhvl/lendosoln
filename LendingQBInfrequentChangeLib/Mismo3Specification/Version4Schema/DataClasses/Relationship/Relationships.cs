namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class RELATIONSHIPS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.RelationshipListSpecified;
            }
        }
    
        [XmlElement("RELATIONSHIP", Order = 0)]
        public List<RELATIONSHIP> RelationshipList { get; set; } = new List<RELATIONSHIP>();
    
        [XmlIgnore]
        public bool RelationshipListSpecified
        {
            get { return this.RelationshipList != null && this.RelationshipList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    }
}
