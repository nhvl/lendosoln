namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class RELATIONSHIP
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.SequenceNumberStringifiedSpecified
                    || this.ArcroleSpecified
                    || this.FromSpecified
                    || this.ToSpecified;
            }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    
        [XmlAttribute(AttributeName = "arcrole", Namespace = Mismo3Constants.XlinkNamespace)]
        public string Arcrole { get; set; }
    
        [XmlIgnore]
        public bool ArcroleSpecified
        {
            get { return !string.IsNullOrEmpty(this.Arcrole); }
            set { }
        }
    
        [XmlAttribute(AttributeName = "from", Namespace = Mismo3Constants.XlinkNamespace)]
        public string From { get; set; }
    
        [XmlIgnore]
        public bool FromSpecified
        {
            get { return !string.IsNullOrEmpty(this.From); }
            set { }
        }
    
        [XmlAttribute(AttributeName = "to", Namespace = Mismo3Constants.XlinkNamespace)]
        public string To { get; set; }
    
        [XmlIgnore]
        public bool ToSpecified
        {
            get { return !string.IsNullOrEmpty(this.To); }
            set { }
        }
    }
}
