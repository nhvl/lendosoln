namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class DATA_MODIFICATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DataModificationRequestSpecified
                    || this.DataModificationResponseSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DATA_MODIFICATION_REQUEST", Order = 0)]
        public DATA_MODIFICATION_REQUEST DataModificationRequest { get; set; }
    
        [XmlIgnore]
        public bool DataModificationRequestSpecified
        {
            get { return this.DataModificationRequest != null && this.DataModificationRequest.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("DATA_MODIFICATION_RESPONSE", Order = 1)]
        public DATA_MODIFICATION_RESPONSE DataModificationResponse { get; set; }
    
        [XmlIgnore]
        public bool DataModificationResponseSpecified
        {
            get { return this.DataModificationResponse != null && this.DataModificationResponse.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public DATA_MODIFICATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
