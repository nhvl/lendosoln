namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class DATA_ITEM_CHANGE_REQUEST
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DataItemChangeContextsSpecified
                    || this.DataItemChangeRequestDetailSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DATA_ITEM_CHANGE_CONTEXTS", Order = 0)]
        public DATA_ITEM_CHANGE_CONTEXTS DataItemChangeContexts { get; set; }
    
        [XmlIgnore]
        public bool DataItemChangeContextsSpecified
        {
            get { return this.DataItemChangeContexts != null && this.DataItemChangeContexts.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("DATA_ITEM_CHANGE_REQUEST_DETAIL", Order = 1)]
        public DATA_ITEM_CHANGE_REQUEST_DETAIL DataItemChangeRequestDetail { get; set; }
    
        [XmlIgnore]
        public bool DataItemChangeRequestDetailSpecified
        {
            get { return this.DataItemChangeRequestDetail != null && this.DataItemChangeRequestDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public DATA_ITEM_CHANGE_REQUEST_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
