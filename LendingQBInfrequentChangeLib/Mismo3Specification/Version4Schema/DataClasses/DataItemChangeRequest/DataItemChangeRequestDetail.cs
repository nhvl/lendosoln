namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class DATA_ITEM_CHANGE_REQUEST_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DataItemChangeAbsoluteXPathSpecified
                    || this.RequestDatetimeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DataItemChangeAbsoluteXPath", Order = 0)]
        public MISMOXPath DataItemChangeAbsoluteXPath { get; set; }
    
        [XmlIgnore]
        public bool DataItemChangeAbsoluteXPathSpecified
        {
            get { return this.DataItemChangeAbsoluteXPath != null; }
            set { }
        }
    
        [XmlElement("RequestDatetime", Order = 1)]
        public MISMODatetime RequestDatetime { get; set; }
    
        [XmlIgnore]
        public bool RequestDatetimeSpecified
        {
            get { return this.RequestDatetime != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public DATA_ITEM_CHANGE_REQUEST_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
