namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class VALUATION_RESPONSE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.DataSourcesSpecified
                    || this.PartiesSpecified
                    || this.PropertiesSpecified
                    || this.ValuationReportSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("DATA_SOURCES", Order = 0)]
        public DATA_SOURCES DataSources { get; set; }
    
        [XmlIgnore]
        public bool DataSourcesSpecified
        {
            get { return this.DataSources != null && this.DataSources.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PARTIES", Order = 1)]
        public PARTIES Parties { get; set; }
    
        [XmlIgnore]
        public bool PartiesSpecified
        {
            get { return this.Parties != null && this.Parties.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PROPERTIES", Order = 2)]
        public PROPERTIES Properties { get; set; }
    
        [XmlIgnore]
        public bool PropertiesSpecified
        {
            get { return this.Properties != null && this.Properties.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("VALUATION_REPORT", Order = 3)]
        public VALUATION_REPORT ValuationReport { get; set; }
    
        [XmlIgnore]
        public bool ValuationReportSpecified
        {
            get { return this.ValuationReport != null && this.ValuationReport.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public VALUATION_RESPONSE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
