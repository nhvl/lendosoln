namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CONTACT_POINT_EMAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ContactPointEmailValueSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ContactPointEmailValue", Order = 0)]
        public MISMOValue ContactPointEmailValue { get; set; }
    
        [XmlIgnore]
        public bool ContactPointEmailValueSpecified
        {
            get { return this.ContactPointEmailValue != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public CONTACT_POINT_EMAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
