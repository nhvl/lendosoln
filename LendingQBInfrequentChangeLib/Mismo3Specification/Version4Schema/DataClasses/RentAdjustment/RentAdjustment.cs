namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class RENT_ADJUSTMENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.RentAdjustmentAmountSpecified
                    || this.RentAdjustmentDescriptionSpecified
                    || this.RentAdjustmentTypeSpecified
                    || this.RentAdjustmentTypeOtherDescriptionSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("RentAdjustmentAmount", Order = 0)]
        public MISMOAmount RentAdjustmentAmount { get; set; }
    
        [XmlIgnore]
        public bool RentAdjustmentAmountSpecified
        {
            get { return this.RentAdjustmentAmount != null; }
            set { }
        }
    
        [XmlElement("RentAdjustmentDescription", Order = 1)]
        public MISMOString RentAdjustmentDescription { get; set; }
    
        [XmlIgnore]
        public bool RentAdjustmentDescriptionSpecified
        {
            get { return this.RentAdjustmentDescription != null; }
            set { }
        }
    
        [XmlElement("RentAdjustmentType", Order = 2)]
        public MISMOEnum<RentAdjustmentBase> RentAdjustmentType { get; set; }
    
        [XmlIgnore]
        public bool RentAdjustmentTypeSpecified
        {
            get { return this.RentAdjustmentType != null; }
            set { }
        }
    
        [XmlElement("RentAdjustmentTypeOtherDescription", Order = 3)]
        public MISMOString RentAdjustmentTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool RentAdjustmentTypeOtherDescriptionSpecified
        {
            get { return this.RentAdjustmentTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 4)]
        public RENT_ADJUSTMENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
