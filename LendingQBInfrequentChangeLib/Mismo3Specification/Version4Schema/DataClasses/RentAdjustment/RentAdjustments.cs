namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class RENT_ADJUSTMENTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.RentAdjustmentListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("RENT_ADJUSTMENT", Order = 0)]
        public List<RENT_ADJUSTMENT> RentAdjustmentList { get; set; } = new List<RENT_ADJUSTMENT>();
    
        [XmlIgnore]
        public bool RentAdjustmentListSpecified
        {
            get { return this.RentAdjustmentList != null && this.RentAdjustmentList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public RENT_ADJUSTMENTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
