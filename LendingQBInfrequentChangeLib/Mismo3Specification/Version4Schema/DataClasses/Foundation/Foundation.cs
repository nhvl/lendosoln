namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class FOUNDATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.FoundationDetailSpecified
                    || this.FoundationFeaturesSpecified
                    || this.FoundationWallsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("FOUNDATION_DETAIL", Order = 0)]
        public FOUNDATION_DETAIL FoundationDetail { get; set; }
    
        [XmlIgnore]
        public bool FoundationDetailSpecified
        {
            get { return this.FoundationDetail != null && this.FoundationDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("FOUNDATION_FEATURES", Order = 1)]
        public FOUNDATION_FEATURES FoundationFeatures { get; set; }
    
        [XmlIgnore]
        public bool FoundationFeaturesSpecified
        {
            get { return this.FoundationFeatures != null && this.FoundationFeatures.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("FOUNDATION_WALLS", Order = 2)]
        public FOUNDATION_WALLS FoundationWalls { get; set; }
    
        [XmlIgnore]
        public bool FoundationWallsSpecified
        {
            get { return this.FoundationWalls != null && this.FoundationWalls.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 3)]
        public FOUNDATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
