namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class FOUNDATIONS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.FoundationListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("FOUNDATION", Order = 0)]
        public List<FOUNDATION> FoundationList { get; set; } = new List<FOUNDATION>();
    
        [XmlIgnore]
        public bool FoundationListSpecified
        {
            get { return this.FoundationList != null && this.FoundationList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public FOUNDATIONS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
