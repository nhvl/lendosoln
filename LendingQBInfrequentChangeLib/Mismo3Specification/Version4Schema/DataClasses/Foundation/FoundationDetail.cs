namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class FOUNDATION_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ComponentAdjustmentAmountSpecified
                    || this.ConditionRatingDescriptionSpecified
                    || this.ConditionRatingTypeSpecified
                    || this.FoundationConditionDescriptionSpecified
                    || this.FoundationDeficienciesTypeSpecified
                    || this.FoundationDeficienciesTypeOtherDescriptionSpecified
                    || this.FoundationDescriptionSpecified
                    || this.FoundationExistsIndicatorSpecified
                    || this.FoundationMaterialTypeSpecified
                    || this.FoundationMaterialTypeOtherDescriptionSpecified
                    || this.FoundationTypeSpecified
                    || this.FoundationTypeOtherDescriptionSpecified
                    || this.MaterialDescriptionSpecified
                    || this.QualityRatingDescriptionSpecified
                    || this.QualityRatingTypeSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ComponentAdjustmentAmount", Order = 0)]
        public MISMOAmount ComponentAdjustmentAmount { get; set; }
    
        [XmlIgnore]
        public bool ComponentAdjustmentAmountSpecified
        {
            get { return this.ComponentAdjustmentAmount != null; }
            set { }
        }
    
        [XmlElement("ConditionRatingDescription", Order = 1)]
        public MISMOString ConditionRatingDescription { get; set; }
    
        [XmlIgnore]
        public bool ConditionRatingDescriptionSpecified
        {
            get { return this.ConditionRatingDescription != null; }
            set { }
        }
    
        [XmlElement("ConditionRatingType", Order = 2)]
        public MISMOEnum<ConditionRatingBase> ConditionRatingType { get; set; }
    
        [XmlIgnore]
        public bool ConditionRatingTypeSpecified
        {
            get { return this.ConditionRatingType != null; }
            set { }
        }
    
        [XmlElement("FoundationConditionDescription", Order = 3)]
        public MISMOString FoundationConditionDescription { get; set; }
    
        [XmlIgnore]
        public bool FoundationConditionDescriptionSpecified
        {
            get { return this.FoundationConditionDescription != null; }
            set { }
        }
    
        [XmlElement("FoundationDeficienciesType", Order = 4)]
        public MISMOEnum<FoundationDeficienciesBase> FoundationDeficienciesType { get; set; }
    
        [XmlIgnore]
        public bool FoundationDeficienciesTypeSpecified
        {
            get { return this.FoundationDeficienciesType != null; }
            set { }
        }
    
        [XmlElement("FoundationDeficienciesTypeOtherDescription", Order = 5)]
        public MISMOString FoundationDeficienciesTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool FoundationDeficienciesTypeOtherDescriptionSpecified
        {
            get { return this.FoundationDeficienciesTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("FoundationDescription", Order = 6)]
        public MISMOString FoundationDescription { get; set; }
    
        [XmlIgnore]
        public bool FoundationDescriptionSpecified
        {
            get { return this.FoundationDescription != null; }
            set { }
        }
    
        [XmlElement("FoundationExistsIndicator", Order = 7)]
        public MISMOIndicator FoundationExistsIndicator { get; set; }
    
        [XmlIgnore]
        public bool FoundationExistsIndicatorSpecified
        {
            get { return this.FoundationExistsIndicator != null; }
            set { }
        }
    
        [XmlElement("FoundationMaterialType", Order = 8)]
        public MISMOEnum<FoundationMaterialBase> FoundationMaterialType { get; set; }
    
        [XmlIgnore]
        public bool FoundationMaterialTypeSpecified
        {
            get { return this.FoundationMaterialType != null; }
            set { }
        }
    
        [XmlElement("FoundationMaterialTypeOtherDescription", Order = 9)]
        public MISMOString FoundationMaterialTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool FoundationMaterialTypeOtherDescriptionSpecified
        {
            get { return this.FoundationMaterialTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("FoundationType", Order = 10)]
        public MISMOEnum<FoundationBase> FoundationType { get; set; }
    
        [XmlIgnore]
        public bool FoundationTypeSpecified
        {
            get { return this.FoundationType != null; }
            set { }
        }
    
        [XmlElement("FoundationTypeOtherDescription", Order = 11)]
        public MISMOString FoundationTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool FoundationTypeOtherDescriptionSpecified
        {
            get { return this.FoundationTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("MaterialDescription", Order = 12)]
        public MISMOString MaterialDescription { get; set; }
    
        [XmlIgnore]
        public bool MaterialDescriptionSpecified
        {
            get { return this.MaterialDescription != null; }
            set { }
        }
    
        [XmlElement("QualityRatingDescription", Order = 13)]
        public MISMOString QualityRatingDescription { get; set; }
    
        [XmlIgnore]
        public bool QualityRatingDescriptionSpecified
        {
            get { return this.QualityRatingDescription != null; }
            set { }
        }
    
        [XmlElement("QualityRatingType", Order = 14)]
        public MISMOEnum<QualityRatingBase> QualityRatingType { get; set; }
    
        [XmlIgnore]
        public bool QualityRatingTypeSpecified
        {
            get { return this.QualityRatingType != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 15)]
        public FOUNDATION_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
