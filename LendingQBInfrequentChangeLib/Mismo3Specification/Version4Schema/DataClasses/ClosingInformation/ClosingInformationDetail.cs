namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CLOSING_INFORMATION_DETAIL
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.CashFromBorrowerAtClosingAmountSpecified
                    || this.CashFromSellerAtClosingAmountSpecified
                    || this.CashToBorrowerAtClosingAmountSpecified
                    || this.CashToSellerAtClosingAmountSpecified
                    || this.ClosingAgentOrderNumberIdentifierSpecified
                    || this.ClosingDateSpecified
                    || this.ClosingDocumentReceivedDateSpecified
                    || this.ClosingDocumentsExpirationDateSpecified
                    || this.ClosingDocumentsExpirationTimeSpecified
                    || this.CurrentRateSetDateSpecified
                    || this.DisbursementDateSpecified
                    || this.DocumentOrderClassificationTypeSpecified
                    || this.DocumentPreparationDateSpecified
                    || this.EstimatedPrepaidDaysCountSpecified
                    || this.EstimatedPrepaidDaysPaidByTypeSpecified
                    || this.EstimatedPrepaidDaysPaidByTypeOtherDescriptionSpecified
                    || this.FundByDateSpecified
                    || this.LoanEstimatedClosingDateSpecified
                    || this.LoanScheduledClosingDateSpecified
                    || this.RescissionDateSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CashFromBorrowerAtClosingAmount", Order = 0)]
        public MISMOAmount CashFromBorrowerAtClosingAmount { get; set; }
    
        [XmlIgnore]
        public bool CashFromBorrowerAtClosingAmountSpecified
        {
            get { return this.CashFromBorrowerAtClosingAmount != null; }
            set { }
        }
    
        [XmlElement("CashFromSellerAtClosingAmount", Order = 1)]
        public MISMOAmount CashFromSellerAtClosingAmount { get; set; }
    
        [XmlIgnore]
        public bool CashFromSellerAtClosingAmountSpecified
        {
            get { return this.CashFromSellerAtClosingAmount != null; }
            set { }
        }
    
        [XmlElement("CashToBorrowerAtClosingAmount", Order = 2)]
        public MISMOAmount CashToBorrowerAtClosingAmount { get; set; }
    
        [XmlIgnore]
        public bool CashToBorrowerAtClosingAmountSpecified
        {
            get { return this.CashToBorrowerAtClosingAmount != null; }
            set { }
        }
    
        [XmlElement("CashToSellerAtClosingAmount", Order = 3)]
        public MISMOAmount CashToSellerAtClosingAmount { get; set; }
    
        [XmlIgnore]
        public bool CashToSellerAtClosingAmountSpecified
        {
            get { return this.CashToSellerAtClosingAmount != null; }
            set { }
        }
    
        [XmlElement("ClosingAgentOrderNumberIdentifier", Order = 4)]
        public MISMOIdentifier ClosingAgentOrderNumberIdentifier { get; set; }
    
        [XmlIgnore]
        public bool ClosingAgentOrderNumberIdentifierSpecified
        {
            get { return this.ClosingAgentOrderNumberIdentifier != null; }
            set { }
        }
    
        [XmlElement("ClosingDate", Order = 5)]
        public MISMODate ClosingDate { get; set; }
    
        [XmlIgnore]
        public bool ClosingDateSpecified
        {
            get { return this.ClosingDate != null; }
            set { }
        }
    
        [XmlElement("ClosingDocumentReceivedDate", Order = 6)]
        public MISMODate ClosingDocumentReceivedDate { get; set; }
    
        [XmlIgnore]
        public bool ClosingDocumentReceivedDateSpecified
        {
            get { return this.ClosingDocumentReceivedDate != null; }
            set { }
        }
    
        [XmlElement("ClosingDocumentsExpirationDate", Order = 7)]
        public MISMODate ClosingDocumentsExpirationDate { get; set; }
    
        [XmlIgnore]
        public bool ClosingDocumentsExpirationDateSpecified
        {
            get { return this.ClosingDocumentsExpirationDate != null; }
            set { }
        }
    
        [XmlElement("ClosingDocumentsExpirationTime", Order = 8)]
        public MISMOTime ClosingDocumentsExpirationTime { get; set; }
    
        [XmlIgnore]
        public bool ClosingDocumentsExpirationTimeSpecified
        {
            get { return this.ClosingDocumentsExpirationTime != null; }
            set { }
        }
    
        [XmlElement("CurrentRateSetDate", Order = 9)]
        public MISMODate CurrentRateSetDate { get; set; }
    
        [XmlIgnore]
        public bool CurrentRateSetDateSpecified
        {
            get { return this.CurrentRateSetDate != null; }
            set { }
        }
    
        [XmlElement("DisbursementDate", Order = 10)]
        public MISMODate DisbursementDate { get; set; }
    
        [XmlIgnore]
        public bool DisbursementDateSpecified
        {
            get { return this.DisbursementDate != null; }
            set { }
        }
    
        [XmlElement("DocumentOrderClassificationType", Order = 11)]
        public MISMOEnum<DocumentOrderClassificationBase> DocumentOrderClassificationType { get; set; }
    
        [XmlIgnore]
        public bool DocumentOrderClassificationTypeSpecified
        {
            get { return this.DocumentOrderClassificationType != null; }
            set { }
        }
    
        [XmlElement("DocumentPreparationDate", Order = 12)]
        public MISMODate DocumentPreparationDate { get; set; }
    
        [XmlIgnore]
        public bool DocumentPreparationDateSpecified
        {
            get { return this.DocumentPreparationDate != null; }
            set { }
        }
    
        [XmlElement("EstimatedPrepaidDaysCount", Order = 13)]
        public MISMOCount EstimatedPrepaidDaysCount { get; set; }
    
        [XmlIgnore]
        public bool EstimatedPrepaidDaysCountSpecified
        {
            get { return this.EstimatedPrepaidDaysCount != null; }
            set { }
        }
    
        [XmlElement("EstimatedPrepaidDaysPaidByType", Order = 14)]
        public MISMOEnum<EstimatedPrepaidDaysPaidByBase> EstimatedPrepaidDaysPaidByType { get; set; }
    
        [XmlIgnore]
        public bool EstimatedPrepaidDaysPaidByTypeSpecified
        {
            get { return this.EstimatedPrepaidDaysPaidByType != null; }
            set { }
        }
    
        [XmlElement("EstimatedPrepaidDaysPaidByTypeOtherDescription", Order = 15)]
        public MISMOString EstimatedPrepaidDaysPaidByTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool EstimatedPrepaidDaysPaidByTypeOtherDescriptionSpecified
        {
            get { return this.EstimatedPrepaidDaysPaidByTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("FundByDate", Order = 16)]
        public MISMODate FundByDate { get; set; }
    
        [XmlIgnore]
        public bool FundByDateSpecified
        {
            get { return this.FundByDate != null; }
            set { }
        }
    
        [XmlElement("LoanEstimatedClosingDate", Order = 17)]
        public MISMODate LoanEstimatedClosingDate { get; set; }
    
        [XmlIgnore]
        public bool LoanEstimatedClosingDateSpecified
        {
            get { return this.LoanEstimatedClosingDate != null; }
            set { }
        }
    
        [XmlElement("LoanScheduledClosingDate", Order = 18)]
        public MISMODate LoanScheduledClosingDate { get; set; }
    
        [XmlIgnore]
        public bool LoanScheduledClosingDateSpecified
        {
            get { return this.LoanScheduledClosingDate != null; }
            set { }
        }
    
        [XmlElement("RescissionDate", Order = 19)]
        public MISMODate RescissionDate { get; set; }
    
        [XmlIgnore]
        public bool RescissionDateSpecified
        {
            get { return this.RescissionDate != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 20)]
        public CLOSING_INFORMATION_DETAIL_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
