namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class CLOSING_INFORMATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ClosingAdjustmentItemsSpecified
                    || this.ClosingCostFundsSpecified
                    || this.ClosingInformationDetailSpecified
                    || this.ClosingInstructionSpecified
                    || this.CollectedOtherFundsSpecified
                    || this.CompensationsSpecified
                    || this.PrepaidItemsSpecified
                    || this.ProrationItemsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("CLOSING_ADJUSTMENT_ITEMS", Order = 0)]
        public CLOSING_ADJUSTMENT_ITEMS ClosingAdjustmentItems { get; set; }
    
        [XmlIgnore]
        public bool ClosingAdjustmentItemsSpecified
        {
            get { return this.ClosingAdjustmentItems != null && this.ClosingAdjustmentItems.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CLOSING_COST_FUNDS", Order = 1)]
        public CLOSING_COST_FUNDS ClosingCostFunds { get; set; }
    
        [XmlIgnore]
        public bool ClosingCostFundsSpecified
        {
            get { return this.ClosingCostFunds != null && this.ClosingCostFunds.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CLOSING_INFORMATION_DETAIL", Order = 2)]
        public CLOSING_INFORMATION_DETAIL ClosingInformationDetail { get; set; }
    
        [XmlIgnore]
        public bool ClosingInformationDetailSpecified
        {
            get { return this.ClosingInformationDetail != null && this.ClosingInformationDetail.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("CLOSING_INSTRUCTION", Order = 3)]
        public CLOSING_INSTRUCTION ClosingInstruction { get; set; }
    
        [XmlIgnore]
        public bool ClosingInstructionSpecified
        {
            get { return this.ClosingInstruction != null && this.ClosingInstruction.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("COLLECTED_OTHER_FUNDS", Order = 4)]
        public COLLECTED_OTHER_FUNDS CollectedOtherFunds { get; set; }
    
        [XmlIgnore]
        public bool CollectedOtherFundsSpecified
        {
            get { return this.CollectedOtherFunds != null && this.CollectedOtherFunds.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("COMPENSATIONS", Order = 5)]
        public COMPENSATIONS Compensations { get; set; }
    
        [XmlIgnore]
        public bool CompensationsSpecified
        {
            get { return this.Compensations != null && this.Compensations.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PREPAID_ITEMS", Order = 6)]
        public PREPAID_ITEMS PrepaidItems { get; set; }
    
        [XmlIgnore]
        public bool PrepaidItemsSpecified
        {
            get { return this.PrepaidItems != null && this.PrepaidItems.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PRORATION_ITEMS", Order = 7)]
        public PRORATION_ITEMS ProrationItems { get; set; }
    
        [XmlIgnore]
        public bool ProrationItemsSpecified
        {
            get { return this.ProrationItems != null && this.ProrationItems.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 8)]
        public CLOSING_INFORMATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
