namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class PROJECT_FINANCIAL_INFORMATION
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ProjectPerUnitFinancingAmountSpecified
                    || this.ProjectPerUnitLienAmountSpecified
                    || this.ProjectStockTransferFeeDescriptionSpecified
                    || this.ProjectStockTransferFeeIndicatorSpecified
                    || this.ProjectTaxAbatementsOrExemptionsDescriptionSpecified
                    || this.ProjectTaxAbatementsOrExemptionsIndicatorSpecified
                    || this.ProjectTotalSharesCountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ProjectPerUnitFinancingAmount", Order = 0)]
        public MISMOAmount ProjectPerUnitFinancingAmount { get; set; }
    
        [XmlIgnore]
        public bool ProjectPerUnitFinancingAmountSpecified
        {
            get { return this.ProjectPerUnitFinancingAmount != null; }
            set { }
        }
    
        [XmlElement("ProjectPerUnitLienAmount", Order = 1)]
        public MISMOAmount ProjectPerUnitLienAmount { get; set; }
    
        [XmlIgnore]
        public bool ProjectPerUnitLienAmountSpecified
        {
            get { return this.ProjectPerUnitLienAmount != null; }
            set { }
        }
    
        [XmlElement("ProjectStockTransferFeeDescription", Order = 2)]
        public MISMOString ProjectStockTransferFeeDescription { get; set; }
    
        [XmlIgnore]
        public bool ProjectStockTransferFeeDescriptionSpecified
        {
            get { return this.ProjectStockTransferFeeDescription != null; }
            set { }
        }
    
        [XmlElement("ProjectStockTransferFeeIndicator", Order = 3)]
        public MISMOIndicator ProjectStockTransferFeeIndicator { get; set; }
    
        [XmlIgnore]
        public bool ProjectStockTransferFeeIndicatorSpecified
        {
            get { return this.ProjectStockTransferFeeIndicator != null; }
            set { }
        }
    
        [XmlElement("ProjectTaxAbatementsOrExemptionsDescription", Order = 4)]
        public MISMOString ProjectTaxAbatementsOrExemptionsDescription { get; set; }
    
        [XmlIgnore]
        public bool ProjectTaxAbatementsOrExemptionsDescriptionSpecified
        {
            get { return this.ProjectTaxAbatementsOrExemptionsDescription != null; }
            set { }
        }
    
        [XmlElement("ProjectTaxAbatementsOrExemptionsIndicator", Order = 5)]
        public MISMOIndicator ProjectTaxAbatementsOrExemptionsIndicator { get; set; }
    
        [XmlIgnore]
        public bool ProjectTaxAbatementsOrExemptionsIndicatorSpecified
        {
            get { return this.ProjectTaxAbatementsOrExemptionsIndicator != null; }
            set { }
        }
    
        [XmlElement("ProjectTotalSharesCount", Order = 6)]
        public MISMOCount ProjectTotalSharesCount { get; set; }
    
        [XmlIgnore]
        public bool ProjectTotalSharesCountSpecified
        {
            get { return this.ProjectTotalSharesCount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 7)]
        public PROJECT_FINANCIAL_INFORMATION_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
