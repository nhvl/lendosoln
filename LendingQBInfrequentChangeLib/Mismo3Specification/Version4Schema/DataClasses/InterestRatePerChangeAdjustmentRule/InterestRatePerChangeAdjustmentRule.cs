namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AdjustmentRuleTypeSpecified
                    || this.NoInterestRateCapsFeatureDescriptionSpecified
                    || this.PaymentsBetweenRateChangeNoticesCountSpecified
                    || this.PaymentsBetweenRateChangesCountSpecified
                    || this.PaymentsToFirstInterestRateValueCountSpecified
                    || this.PerChangeCeilingRatePercentSpecified
                    || this.PerChangeFactorOfIndexPercentSpecified
                    || this.PerChangeFloorRatePercentSpecified
                    || this.PerChangeInterestRateRoundingTimingTypeSpecified
                    || this.PerChangeMaximumDecreaseRatePercentSpecified
                    || this.PerChangeMaximumIncreaseRatePercentSpecified
                    || this.PerChangeMinimumDecreaseRatePercentSpecified
                    || this.PerChangeMinimumIncreaseRatePercentSpecified
                    || this.PerChangeRateAdjustmentCalculationMethodDescriptionSpecified
                    || this.PerChangeRateAdjustmentEffectiveDateSpecified
                    || this.PerChangeRateAdjustmentEffectiveMonthsCountSpecified
                    || this.PerChangeRateAdjustmentFrequencyMonthsCountSpecified
                    || this.PerChangeRateAdjustmentPaymentsCountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AdjustmentRuleType", Order = 0)]
        public MISMOEnum<AdjustmentRuleBase> AdjustmentRuleType { get; set; }
    
        [XmlIgnore]
        public bool AdjustmentRuleTypeSpecified
        {
            get { return this.AdjustmentRuleType != null; }
            set { }
        }
    
        [XmlElement("NoInterestRateCapsFeatureDescription", Order = 1)]
        public MISMOString NoInterestRateCapsFeatureDescription { get; set; }
    
        [XmlIgnore]
        public bool NoInterestRateCapsFeatureDescriptionSpecified
        {
            get { return this.NoInterestRateCapsFeatureDescription != null; }
            set { }
        }
    
        [XmlElement("PaymentsBetweenRateChangeNoticesCount", Order = 2)]
        public MISMOCount PaymentsBetweenRateChangeNoticesCount { get; set; }
    
        [XmlIgnore]
        public bool PaymentsBetweenRateChangeNoticesCountSpecified
        {
            get { return this.PaymentsBetweenRateChangeNoticesCount != null; }
            set { }
        }
    
        [XmlElement("PaymentsBetweenRateChangesCount", Order = 3)]
        public MISMOCount PaymentsBetweenRateChangesCount { get; set; }
    
        [XmlIgnore]
        public bool PaymentsBetweenRateChangesCountSpecified
        {
            get { return this.PaymentsBetweenRateChangesCount != null; }
            set { }
        }
    
        [XmlElement("PaymentsToFirstInterestRateValueCount", Order = 4)]
        public MISMOCount PaymentsToFirstInterestRateValueCount { get; set; }
    
        [XmlIgnore]
        public bool PaymentsToFirstInterestRateValueCountSpecified
        {
            get { return this.PaymentsToFirstInterestRateValueCount != null; }
            set { }
        }
    
        [XmlElement("PerChangeCeilingRatePercent", Order = 5)]
        public MISMOPercent PerChangeCeilingRatePercent { get; set; }
    
        [XmlIgnore]
        public bool PerChangeCeilingRatePercentSpecified
        {
            get { return this.PerChangeCeilingRatePercent != null; }
            set { }
        }
    
        [XmlElement("PerChangeFactorOfIndexPercent", Order = 6)]
        public MISMOPercent PerChangeFactorOfIndexPercent { get; set; }
    
        [XmlIgnore]
        public bool PerChangeFactorOfIndexPercentSpecified
        {
            get { return this.PerChangeFactorOfIndexPercent != null; }
            set { }
        }
    
        [XmlElement("PerChangeFloorRatePercent", Order = 7)]
        public MISMOPercent PerChangeFloorRatePercent { get; set; }
    
        [XmlIgnore]
        public bool PerChangeFloorRatePercentSpecified
        {
            get { return this.PerChangeFloorRatePercent != null; }
            set { }
        }
    
        [XmlElement("PerChangeInterestRateRoundingTimingType", Order = 8)]
        public MISMOEnum<PerChangeInterestRateRoundingTimingBase> PerChangeInterestRateRoundingTimingType { get; set; }
    
        [XmlIgnore]
        public bool PerChangeInterestRateRoundingTimingTypeSpecified
        {
            get { return this.PerChangeInterestRateRoundingTimingType != null; }
            set { }
        }
    
        [XmlElement("PerChangeMaximumDecreaseRatePercent", Order = 9)]
        public MISMOPercent PerChangeMaximumDecreaseRatePercent { get; set; }
    
        [XmlIgnore]
        public bool PerChangeMaximumDecreaseRatePercentSpecified
        {
            get { return this.PerChangeMaximumDecreaseRatePercent != null; }
            set { }
        }
    
        [XmlElement("PerChangeMaximumIncreaseRatePercent", Order = 10)]
        public MISMOPercent PerChangeMaximumIncreaseRatePercent { get; set; }
    
        [XmlIgnore]
        public bool PerChangeMaximumIncreaseRatePercentSpecified
        {
            get { return this.PerChangeMaximumIncreaseRatePercent != null; }
            set { }
        }
    
        [XmlElement("PerChangeMinimumDecreaseRatePercent", Order = 11)]
        public MISMOPercent PerChangeMinimumDecreaseRatePercent { get; set; }
    
        [XmlIgnore]
        public bool PerChangeMinimumDecreaseRatePercentSpecified
        {
            get { return this.PerChangeMinimumDecreaseRatePercent != null; }
            set { }
        }
    
        [XmlElement("PerChangeMinimumIncreaseRatePercent", Order = 12)]
        public MISMOPercent PerChangeMinimumIncreaseRatePercent { get; set; }
    
        [XmlIgnore]
        public bool PerChangeMinimumIncreaseRatePercentSpecified
        {
            get { return this.PerChangeMinimumIncreaseRatePercent != null; }
            set { }
        }
    
        [XmlElement("PerChangeRateAdjustmentCalculationMethodDescription", Order = 13)]
        public MISMOString PerChangeRateAdjustmentCalculationMethodDescription { get; set; }
    
        [XmlIgnore]
        public bool PerChangeRateAdjustmentCalculationMethodDescriptionSpecified
        {
            get { return this.PerChangeRateAdjustmentCalculationMethodDescription != null; }
            set { }
        }
    
        [XmlElement("PerChangeRateAdjustmentEffectiveDate", Order = 14)]
        public MISMODate PerChangeRateAdjustmentEffectiveDate { get; set; }
    
        [XmlIgnore]
        public bool PerChangeRateAdjustmentEffectiveDateSpecified
        {
            get { return this.PerChangeRateAdjustmentEffectiveDate != null; }
            set { }
        }
    
        [XmlElement("PerChangeRateAdjustmentEffectiveMonthsCount", Order = 15)]
        public MISMOCount PerChangeRateAdjustmentEffectiveMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool PerChangeRateAdjustmentEffectiveMonthsCountSpecified
        {
            get { return this.PerChangeRateAdjustmentEffectiveMonthsCount != null; }
            set { }
        }
    
        [XmlElement("PerChangeRateAdjustmentFrequencyMonthsCount", Order = 16)]
        public MISMOCount PerChangeRateAdjustmentFrequencyMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool PerChangeRateAdjustmentFrequencyMonthsCountSpecified
        {
            get { return this.PerChangeRateAdjustmentFrequencyMonthsCount != null; }
            set { }
        }
    
        [XmlElement("PerChangeRateAdjustmentPaymentsCount", Order = 17)]
        public MISMOCount PerChangeRateAdjustmentPaymentsCount { get; set; }
    
        [XmlIgnore]
        public bool PerChangeRateAdjustmentPaymentsCountSpecified
        {
            get { return this.PerChangeRateAdjustmentPaymentsCount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 18)]
        public INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
