namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.InterestRatePerChangeAdjustmentRuleListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULE", Order = 0)]
        public List<INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULE> InterestRatePerChangeAdjustmentRuleList { get; set; } = new List<INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULE>();
    
        [XmlIgnore]
        public bool InterestRatePerChangeAdjustmentRuleListSpecified
        {
            get { return this.InterestRatePerChangeAdjustmentRuleList != null && this.InterestRatePerChangeAdjustmentRuleList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
