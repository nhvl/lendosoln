namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class TITLE_ADDITIONAL_REQUIREMENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.TitleAdditionalRequirementDescriptionSpecified
                    || this.TitleAdditionalRequirementToBeRecordedIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("TitleAdditionalRequirementDescription", Order = 0)]
        public MISMOString TitleAdditionalRequirementDescription { get; set; }
    
        [XmlIgnore]
        public bool TitleAdditionalRequirementDescriptionSpecified
        {
            get { return this.TitleAdditionalRequirementDescription != null; }
            set { }
        }
    
        [XmlElement("TitleAdditionalRequirementToBeRecordedIndicator", Order = 1)]
        public MISMOIndicator TitleAdditionalRequirementToBeRecordedIndicator { get; set; }
    
        [XmlIgnore]
        public bool TitleAdditionalRequirementToBeRecordedIndicatorSpecified
        {
            get { return this.TitleAdditionalRequirementToBeRecordedIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public TITLE_ADDITIONAL_REQUIREMENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
