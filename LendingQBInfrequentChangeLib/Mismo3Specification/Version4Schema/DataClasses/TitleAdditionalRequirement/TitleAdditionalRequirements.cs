namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class TITLE_ADDITIONAL_REQUIREMENTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.TitleAdditionalRequirementListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("TITLE_ADDITIONAL_REQUIREMENT", Order = 0)]
        public List<TITLE_ADDITIONAL_REQUIREMENT> TitleAdditionalRequirementList { get; set; } = new List<TITLE_ADDITIONAL_REQUIREMENT>();
    
        [XmlIgnore]
        public bool TitleAdditionalRequirementListSpecified
        {
            get { return this.TitleAdditionalRequirementList != null && this.TitleAdditionalRequirementList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public TITLE_ADDITIONAL_REQUIREMENTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
