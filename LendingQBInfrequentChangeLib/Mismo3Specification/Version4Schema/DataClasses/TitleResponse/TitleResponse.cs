namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class TITLE_RESPONSE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.AgentValidationSpecified
                    || this.ExecutionsSpecified
                    || this.PartiesSpecified
                    || this.RemittancesSpecified
                    || this.TitleProductsSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("AGENT_VALIDATION", Order = 0)]
        public AGENT_VALIDATION AgentValidation { get; set; }
    
        [XmlIgnore]
        public bool AgentValidationSpecified
        {
            get { return this.AgentValidation != null && this.AgentValidation.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXECUTIONS", Order = 1)]
        public EXECUTIONS Executions { get; set; }
    
        [XmlIgnore]
        public bool ExecutionsSpecified
        {
            get { return this.Executions != null && this.Executions.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("PARTIES", Order = 2)]
        public PARTIES Parties { get; set; }
    
        [XmlIgnore]
        public bool PartiesSpecified
        {
            get { return this.Parties != null && this.Parties.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("REMITTANCES", Order = 3)]
        public REMITTANCES Remittances { get; set; }
    
        [XmlIgnore]
        public bool RemittancesSpecified
        {
            get { return this.Remittances != null && this.Remittances.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("TITLE_PRODUCTS", Order = 4)]
        public TITLE_PRODUCTS TitleProducts { get; set; }
    
        [XmlIgnore]
        public bool TitleProductsSpecified
        {
            get { return this.TitleProducts != null && this.TitleProducts.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 5)]
        public TITLE_RESPONSE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
