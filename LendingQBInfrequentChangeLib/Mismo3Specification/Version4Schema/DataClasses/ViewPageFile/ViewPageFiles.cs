namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class VIEW_PAGE_FILES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.ViewPageFileListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("VIEW_PAGE_FILE", Order = 0)]
        public List<VIEW_PAGE_FILE> ViewPageFileList { get; set; } = new List<VIEW_PAGE_FILE>();
    
        [XmlIgnore]
        public bool ViewPageFileListSpecified
        {
            get { return this.ViewPageFileList != null && this.ViewPageFileList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public VIEW_PAGE_FILES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
