namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class ESCROW_ITEM_PAYMENT
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EscrowItemActualPaymentAmountSpecified
                    || this.EscrowItemEstimatedPaymentAmountSpecified
                    || this.EscrowItemPaymentPaidByTypeSpecified
                    || this.EscrowItemPaymentPaidByTypeOtherDescriptionSpecified
                    || this.EscrowItemPaymentTimingTypeSpecified
                    || this.EscrowItemPaymentTimingTypeOtherDescriptionSpecified
                    || this.PaymentFinancedIndicatorSpecified
                    || this.PaymentIncludedInAPRIndicatorSpecified
                    || this.PaymentIncludedInJurisdictionHighCostIndicatorSpecified
                    || this.PrepaidFinanceChargeIndicatorSpecified
                    || this.RegulationZPointsAndFeesIndicatorSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("EscrowItemActualPaymentAmount", Order = 0)]
        public MISMOAmount EscrowItemActualPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool EscrowItemActualPaymentAmountSpecified
        {
            get { return this.EscrowItemActualPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("EscrowItemEstimatedPaymentAmount", Order = 1)]
        public MISMOAmount EscrowItemEstimatedPaymentAmount { get; set; }
    
        [XmlIgnore]
        public bool EscrowItemEstimatedPaymentAmountSpecified
        {
            get { return this.EscrowItemEstimatedPaymentAmount != null; }
            set { }
        }
    
        [XmlElement("EscrowItemPaymentPaidByType", Order = 2)]
        public MISMOEnum<EscrowItemPaymentPaidByBase> EscrowItemPaymentPaidByType { get; set; }
    
        [XmlIgnore]
        public bool EscrowItemPaymentPaidByTypeSpecified
        {
            get { return this.EscrowItemPaymentPaidByType != null; }
            set { }
        }
    
        [XmlElement("EscrowItemPaymentPaidByTypeOtherDescription", Order = 3)]
        public MISMOString EscrowItemPaymentPaidByTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool EscrowItemPaymentPaidByTypeOtherDescriptionSpecified
        {
            get { return this.EscrowItemPaymentPaidByTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("EscrowItemPaymentTimingType", Order = 4)]
        public MISMOEnum<EscrowItemPaymentTimingBase> EscrowItemPaymentTimingType { get; set; }
    
        [XmlIgnore]
        public bool EscrowItemPaymentTimingTypeSpecified
        {
            get { return this.EscrowItemPaymentTimingType != null; }
            set { }
        }
    
        [XmlElement("EscrowItemPaymentTimingTypeOtherDescription", Order = 5)]
        public MISMOString EscrowItemPaymentTimingTypeOtherDescription { get; set; }
    
        [XmlIgnore]
        public bool EscrowItemPaymentTimingTypeOtherDescriptionSpecified
        {
            get { return this.EscrowItemPaymentTimingTypeOtherDescription != null; }
            set { }
        }
    
        [XmlElement("PaymentFinancedIndicator", Order = 6)]
        public MISMOIndicator PaymentFinancedIndicator { get; set; }
    
        [XmlIgnore]
        public bool PaymentFinancedIndicatorSpecified
        {
            get { return this.PaymentFinancedIndicator != null; }
            set { }
        }
    
        [XmlElement("PaymentIncludedInAPRIndicator", Order = 7)]
        public MISMOIndicator PaymentIncludedInAPRIndicator { get; set; }
    
        [XmlIgnore]
        public bool PaymentIncludedInAPRIndicatorSpecified
        {
            get { return this.PaymentIncludedInAPRIndicator != null; }
            set { }
        }
    
        [XmlElement("PaymentIncludedInJurisdictionHighCostIndicator", Order = 8)]
        public MISMOIndicator PaymentIncludedInJurisdictionHighCostIndicator { get; set; }
    
        [XmlIgnore]
        public bool PaymentIncludedInJurisdictionHighCostIndicatorSpecified
        {
            get { return this.PaymentIncludedInJurisdictionHighCostIndicator != null; }
            set { }
        }
    
        [XmlElement("PrepaidFinanceChargeIndicator", Order = 9)]
        public MISMOIndicator PrepaidFinanceChargeIndicator { get; set; }
    
        [XmlIgnore]
        public bool PrepaidFinanceChargeIndicatorSpecified
        {
            get { return this.PrepaidFinanceChargeIndicator != null; }
            set { }
        }
    
        [XmlElement("RegulationZPointsAndFeesIndicator", Order = 10)]
        public MISMOIndicator RegulationZPointsAndFeesIndicator { get; set; }
    
        [XmlIgnore]
        public bool RegulationZPointsAndFeesIndicatorSpecified
        {
            get { return this.RegulationZPointsAndFeesIndicator != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 11)]
        public ESCROW_ITEM_PAYMENT_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
