namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class ESCROW_ITEM_PAYMENTS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.EscrowItemPaymentListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("ESCROW_ITEM_PAYMENT", Order = 0)]
        public List<ESCROW_ITEM_PAYMENT> EscrowItemPaymentList { get; set; } = new List<ESCROW_ITEM_PAYMENT>();
    
        [XmlIgnore]
        public bool EscrowItemPaymentListSpecified
        {
            get { return this.EscrowItemPaymentList != null && this.EscrowItemPaymentList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public ESCROW_ITEM_PAYMENTS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
