namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class RETURN_TO
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PreferredResponsesSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PREFERRED_RESPONSES", Order = 0)]
        public PREFERRED_RESPONSES PreferredResponses { get; set; }
    
        [XmlIgnore]
        public bool PreferredResponsesSpecified
        {
            get { return this.PreferredResponses != null && this.PreferredResponses.ShouldSerialize; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public RETURN_TO_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
