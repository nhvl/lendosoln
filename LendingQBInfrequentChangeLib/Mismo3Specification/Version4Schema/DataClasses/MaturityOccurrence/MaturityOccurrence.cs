namespace Mismo3Specification.Version4Schema
{
    using System.Xml.Serialization;

    public class MATURITY_OCCURRENCE
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.LoanRemainingMaturityTermMonthsCountSpecified
                    || this.LoanTermExtensionMonthsCountSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("LoanRemainingMaturityTermMonthsCount", Order = 0)]
        public MISMOCount LoanRemainingMaturityTermMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool LoanRemainingMaturityTermMonthsCountSpecified
        {
            get { return this.LoanRemainingMaturityTermMonthsCount != null; }
            set { }
        }
    
        [XmlElement("LoanTermExtensionMonthsCount", Order = 1)]
        public MISMOCount LoanTermExtensionMonthsCount { get; set; }
    
        [XmlIgnore]
        public bool LoanTermExtensionMonthsCountSpecified
        {
            get { return this.LoanTermExtensionMonthsCount != null; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 2)]
        public MATURITY_OCCURRENCE_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    
        [XmlIgnore]
        public int? SequenceNumber { get; set; } = null;
    
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberStringified
        {
            get { return this.SequenceNumber?.ToString() ?? string.Empty; }
            set { }
        }
    
        [XmlIgnore]
        public bool SequenceNumberStringifiedSpecified
        {
            get { return !string.IsNullOrEmpty(this.SequenceNumberStringified); }
            set { }
        }
    }
}
