namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class MATURITY_OCCURRENCES
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.MaturityOccurrenceListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("MATURITY_OCCURRENCE", Order = 0)]
        public List<MATURITY_OCCURRENCE> MaturityOccurrenceList { get; set; } = new List<MATURITY_OCCURRENCE>();
    
        [XmlIgnore]
        public bool MaturityOccurrenceListSpecified
        {
            get { return this.MaturityOccurrenceList != null && this.MaturityOccurrenceList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public MATURITY_OCCURRENCES_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
