namespace Mismo3Specification.Version4Schema
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;

    public class PARTY_ROLE_IDENTIFIERS
    {
        [XmlIgnore]
        public bool ShouldSerialize
        {
            get
            {
                return this.PartyRoleIdentifierListSpecified
                    || this.ExtensionSpecified;
            }
        }
    
        [XmlElement("PARTY_ROLE_IDENTIFIER", Order = 0)]
        public List<PARTY_ROLE_IDENTIFIER> PartyRoleIdentifierList { get; set; } = new List<PARTY_ROLE_IDENTIFIER>();
    
        [XmlIgnore]
        public bool PartyRoleIdentifierListSpecified
        {
            get { return this.PartyRoleIdentifierList != null && this.PartyRoleIdentifierList.Count(item => item != null && item.ShouldSerialize) > 0; }
            set { }
        }
    
        [XmlElement("EXTENSION", Order = 1)]
        public PARTY_ROLE_IDENTIFIERS_EXTENSION Extension { get; set; }
    
        [XmlIgnore]
        public bool ExtensionSpecified
        {
            get { return this.Extension != null && this.Extension.ShouldSerialize; }
            set { }
        }
    }
}
